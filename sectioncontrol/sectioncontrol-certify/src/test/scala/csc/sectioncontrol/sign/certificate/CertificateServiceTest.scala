/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.sign.certificate

import akka.util.duration._
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.curator.CuratorTestServer
import csc.sectioncontrol.messages.certificates._
import csc.sectioncontrol.storage.Paths
import org.apache.curator.framework.CuratorFramework
import csc.akkautils.DirectLogging
import csc.config.Path
import csc.sectioncontrol.messages.certificates.TypeCertificate
import csc.sectioncontrol.messages.certificates.MeasurementMethodType
import scala.Some
import csc.sectioncontrol.messages.certificates.MeasurementMethodInstallation
import csc.sectioncontrol.messages.certificates.ComponentCertificate
import csc.curator.utils.CuratorToolsImpl

class CertificateServiceTest extends WordSpec with MustMatchers with CuratorTestServer with DirectLogging {

  var curator: Option[CuratorFramework] = None
  var zookeeperClient: CuratorToolsImpl = _

  val startTime = System.currentTimeMillis() - 3.days.toMillis
  val entry1 = new MeasurementMethodInstallation(typeId = "entry1", timeFrom = startTime)
  val entry2 = new MeasurementMethodInstallation(typeId = "entry2", timeFrom = startTime + 1.day.toMillis)
  val entry3 = new MeasurementMethodInstallation(typeId = "entry3", timeFrom = startTime + 2.day.toMillis)
  val entry4 = new MeasurementMethodInstallation(typeId = "entry4", timeFrom = startTime + 3.day.toMillis)

  val entryError = new MeasurementMethodInstallation(typeId = "entry999", timeFrom = startTime)

  val globalType1 = new MeasurementMethodType(typeDesignation = "XX123",
    category = "A",
    unitSpeed = "km/h",
    unitRedLight = "s",
    unitLength = "m",
    restrictiveConditions = "",
    displayRange = "20-250 km/h",
    temperatureRange = "temperatuurbereik -10 to 50 graden Celsius",
    permissibleError = "toelatbare fout 3%",
    typeCertificate = new TypeCertificate("eg33-cert", 0,
      List(ComponentCertificate("matcher1", "blah1"),
        ComponentCertificate("vr2", "blah2"))))

  val globalType2 = new MeasurementMethodType(typeDesignation = "XX123",
    category = "A",
    unitSpeed = "km/h",
    unitRedLight = "s",
    unitLength = "m",
    restrictiveConditions = "",
    displayRange = "20-250 km/h",
    temperatureRange = "temperatuurbereik -10 to 50 graden Celsius",
    permissibleError = "toelatbare fout 3%",
    typeCertificate = new TypeCertificate("eg33-cert", 1,
      List(ComponentCertificate("matcher1", "blah1"),
        ComponentCertificate("vr2", "blah2"))))

  val globalType3 = new MeasurementMethodType(typeDesignation = "XX123",
    category = "A",
    unitSpeed = "km/h",
    unitRedLight = "s",
    unitLength = "m",
    restrictiveConditions = "",
    displayRange = "20-250 km/h",
    temperatureRange = "temperatuurbereik -10 to 50 graden Celsius",
    permissibleError = "toelatbare fout 3%",
    typeCertificate = new TypeCertificate("eg33-cert", 3,
      List(ComponentCertificate("matcher1", "blah1"), ComponentCertificate("vr2", "blah2"))))

  val globalType4 = new MeasurementMethodType(typeDesignation = "XX123",
    category = "A",
    unitSpeed = "km/h",
    unitRedLight = "s",
    unitLength = "m",
    restrictiveConditions = "",
    displayRange = "20-250 km/h",
    temperatureRange = "temperatuurbereik -10 to 50 graden Celsius",
    permissibleError = "toelatbare fout 3%",
    typeCertificate = new TypeCertificate("eg33-cert", 3,
      List(ComponentCertificate("matcher1", "blah1"))))

  val location1 = new LocationCertificate(id = "Loc 123", inspectionDate = startTime, validTime = startTime + 1.day.toMillis, components = List())
  val location2 = new LocationCertificate(id = "Loc 223", inspectionDate = startTime + 1.day.toMillis, validTime = startTime + 2.day.toMillis, components = List())
  val location3 = new LocationCertificate(id = "Loc 323", inspectionDate = startTime + 2.day.toMillis - 1.hour.toMillis, validTime = startTime + 3.day.toMillis, components = List())
  val location4 = new LocationCertificate(id = "Loc 423", inspectionDate = startTime + 3.day.toMillis - 1.hour.toMillis, validTime = startTime + 4.day.toMillis, components = List(
    ComponentCertificate("Configuratiezegel", "ditisdechecksum")))
  val location5 = new LocationCertificate(id = "Loc 523", inspectionDate = startTime + 4.day.toMillis - 1.hour.toMillis, validTime = startTime + 5.day.toMillis, components = List(
    ComponentCertificate("Configuratiezegel", "ditisdechecksum"), ComponentCertificate("Camerazegel", "ditisdechecksumvandecamera")))

  override def beforeEach() {
    super.beforeEach()

    curator = clientScope
    zookeeperClient = new CuratorToolsImpl(curator, log)

    val path = Paths.Systems.getInstallCertificatePath("test")
    zookeeperClient.put(path + "/entry1", entry1)
    zookeeperClient.put(path + "/entry2", entry2)
    zookeeperClient.put(path + "/entry3", entry3)
    zookeeperClient.put(path + "/entry4", entry4)

    val pathGlobal1 = Path(Paths.Configuration.getCertificateMeasurementMethodTypePath(entry1.typeId))
    zookeeperClient.put(pathGlobal1, globalType1)
    val pathGlobal2 = Path(Paths.Configuration.getCertificateMeasurementMethodTypePath(entry2.typeId))
    zookeeperClient.put(pathGlobal2, globalType2)
    val pathGlobal3 = Path(Paths.Configuration.getCertificateMeasurementMethodTypePath(entry3.typeId))
    zookeeperClient.put(pathGlobal3, globalType3)
    val pathGlobal4 = Path(Paths.Configuration.getCertificateMeasurementMethodTypePath(entry4.typeId))
    zookeeperClient.put(pathGlobal4, globalType4)

    val pathLocal = Path(Paths.Systems.getLocationCertificatePath("test"))
    zookeeperClient.put(pathLocal / "entry1", location1)
    zookeeperClient.put(pathLocal / "entry2", location2)
    zookeeperClient.put(pathLocal / "entry3", location3)
    zookeeperClient.put(pathLocal / "entry4", location4)
    zookeeperClient.put(pathLocal / "entry5", location5)

    //error situation
    val pathError = Paths.Systems.getInstallCertificatePath("test-error")
    zookeeperClient.put(pathError + "/entry1", entryError)

  }

  "getAllInstalledMeasurementMethodTypes" must {
    "retrieve all installed measurementMethodTypes sorted by time" in {
      val result = CertificateService.getAllInstalledMeasurementMethodTypes(zookeeperClient, "test")
      result must be(result.sortBy(_.timeFrom))
    }
  }
  "getMeasurementMethodInstall" must {
    "fail if given time is before time of existing entries" in {
      val result = CertificateService.getMeasurementMethodInstall(zookeeperClient, "test", startTime - 1)
      result must be(None)
    }
    "return first install with 4 existing entries" in {
      val result = CertificateService.getMeasurementMethodInstall(zookeeperClient, "test", startTime)
      result must be(Some(entry1))
    }
    "return second install with 4 existing entries" in {
      val result = CertificateService.getMeasurementMethodInstall(zookeeperClient, "test", startTime + 2.day.toMillis - 1)
      result must be(Some(entry2))
    }
    "return last install with 4 existing entries" in {
      val result = CertificateService.getMeasurementMethodInstall(zookeeperClient, "test", startTime + 4.day.toMillis)
      result must be(Some(entry4))
    }
    "return None if there are no certificates for the given system" in {
      val result = CertificateService.getMeasurementMethodInstall(zookeeperClient, "notexists", startTime + 4.day.toMillis)
      result must be(None)
    }
  }
  "getMeasurementMethodType" must {
    "fail if given time is before time of existing entries" in {
      val result = CertificateService.getInstalledMeasurementMethodType(zookeeperClient, "test", startTime - 1)
      result must be(None)
    }
    "return first type with 4 existing entries" in {
      val result = CertificateService.getInstalledMeasurementMethodType(zookeeperClient, "test", startTime)
      result must be(Some(globalType1))
    }
    "return second type with 4 existing entries" in {
      val result = CertificateService.getInstalledMeasurementMethodType(zookeeperClient, "test", startTime + 2.day.toMillis - 1)
      result must be(Some(globalType2))
    }
    "return last type with 4 existing entries" in {
      val result = CertificateService.getInstalledMeasurementMethodType(zookeeperClient, "test", startTime + 4.day.toMillis)
      result must be(Some(globalType4))
    }

    // TODO: Incomprehensible. What's the difference between the next 2 tests.
    "fail no global entries" in {
      val result = CertificateService.getInstalledMeasurementMethodType(zookeeperClient, "test-error", startTime)
      result must be(None)
    }
    "fail no entries" in {
      val result = CertificateService.getInstalledMeasurementMethodType(zookeeperClient, "notexists", startTime + 4.day.toMillis)
      result must be(None)
    }
  }
  "getLocationCertificate" must {
    "fail if given time is before time of existing entries" in {
      val result = CertificateService.getLocationCertificate(zookeeperClient, "test", startTime - 1)
      result must be(None)
    }
    "return first certificate with 3 existing entries" in {
      val result = CertificateService.getLocationCertificate(zookeeperClient, "test", startTime)
      result must be(Some(location1))
    }
    "return last certificate with 2 overlap entries" in {
      val result = CertificateService.getLocationCertificate(zookeeperClient, "test", startTime + 2.day.toMillis - 1)
      result must be(Some(location3))
    }
    "fail time after existing entries" in {
      val result = CertificateService.getLocationCertificate(zookeeperClient, "test", startTime + 5.days.toMillis)
      result must be(None)
    }
    "return None if there are no certificates for the given system" in {
      val result = CertificateService.getLocationCertificate(zookeeperClient, "notexists", startTime - 1)
      result must be(None)
    }
  }
  "ActiveChecksums" must {
    "store and retrieve" in {
      val active = ActiveCertificate(startTime, ComponentCertificate("compname", "1234"))
      CertificateService.saveActiveCertificate(zookeeperClient, "test", active)
      val result = CertificateService.getActiveCertificate(zookeeperClient, "test", "compname")
      result.isDefined must be(true)
      result.get must be(active)
    }
    "calculate software seal" in {
      val componentCertificates = Seq(ComponentCertificate("component1", "please hash this value"),
        ComponentCertificate("component2", "hash this as well"))

      val hash = CertificateService.calculateSeal(componentCertificates)
      hash.length must be(32)
      hash must be("19719b13692773f30c280fca46ce2d1a")
    }
    "retrieve measurementmethodtype and calculate active software seal" in {
      val componentCertificate = CertificateService.getActiveSoftwareCertificate(zookeeperClient, "test", startTime + 10)
      componentCertificate must be(ComponentCertificate("Softwarezegel", ""))
    }
    "must return None if there's no componentcertificate for a given system and component" in {
      val result = CertificateService.getActiveCertificate(zookeeperClient, "notexists", "compname")
      result.isEmpty must be(true)
    }
    "delete old jar versions" in {
      val active = ActiveCertificate(startTime, ComponentCertificate("compname_1.1", "1234"))
      CertificateService.saveActiveCertificate(zookeeperClient, "test2", active)
      val active2 = ActiveCertificate(startTime, ComponentCertificate("compname_1.2", "1234"))
      CertificateService.saveActiveCertificate(zookeeperClient, "test2", active2)
      CertificateService.deleteOldActiveCertificatesVersions(zookeeperClient, "test2", "compname_1.2")
      //new certificate must still exists
      val result = CertificateService.getActiveCertificate(zookeeperClient, "test2", "compname_1.2")
      result.isDefined must be(true)
      result.get must be(active2)

      val result2 = CertificateService.getActiveCertificate(zookeeperClient, "test2", "compname_1.1")
      result2.isEmpty must be(true)
    }
  }

  "CertificateService.getActiveConfigurationCertificate" must {
    "return a componentCertificate with an blank seal when no active ConfigurationCertificate can be found in zookeeper." in {
      val componentCertificate = CertificateService.getActiveConfigurationCertificate(zookeeperClient, "test", startTime + 3.day.toMillis)
      componentCertificate must be(ComponentCertificate("Configuratiezegel", ""))
    }

    "return the active configuration(Location) certificate with One ConfigurationCertificate present in zookeeper." in {
      // If there is only one ComponentCertificate the seal(checksum) of that component will be used.
      // Create an active certificate for the Configuratiezegel. Is linked to location4 through startTime + 3
      val active = ActiveCertificate(startTime + 3, ComponentCertificate("Configuratiezegel", "ditisdechecksum"))
      CertificateService.saveActiveCertificate(zookeeperClient, "test", active)

      val componentCertificate = CertificateService.getActiveConfigurationCertificate(zookeeperClient, "test", startTime + 3.day.toMillis)
      componentCertificate must be(ComponentCertificate("Configuratiezegel", "ditisdechecksum"))
    }

    "return the active configuration(Location) certificate with Two ConfigurationCertificate's present in zookeeper." in {
      // Create an active certificate for the Configuratiezegel. Is linked to location4 through startTime + 4
      val seal = CertificateService.calculateSeal(List(ComponentCertificate("Configuratiezegel", "ditisdechecksum"), ComponentCertificate("Camerazegel", "ditisdechecksumvandecamera")))

      val active1 = ActiveCertificate(startTime + 4, ComponentCertificate("Configuratiezegel", "ditisdechecksum"))
      CertificateService.saveActiveCertificate(zookeeperClient, "test", active1)

      val active2 = ActiveCertificate(startTime + 4, ComponentCertificate("Camerazegel", "ditisdechecksumvandecamera"))
      CertificateService.saveActiveCertificate(zookeeperClient, "test", active2)

      val componentCertificate = CertificateService.getActiveConfigurationCertificate(zookeeperClient, "test", startTime + 4.day.toMillis)

      componentCertificate must be(ComponentCertificate("Configuratiezegel", seal))
    }
  }

  "CertificateService.getActiveConfigurationCertificateComponents" must {
    "return two active configuration(Location) components with two active ConfigurationCertificate's in zookeeper." in {
      // Create an active certificate for the Configuratiezegel. Is linked to location4 through startTime + 4
      val activeComponentCertificates = List(ComponentCertificate("Configuratiezegel", "ditisdechecksum"), ComponentCertificate("Camerazegel", "ditisdechecksumvandecamera"))

      val active1 = ActiveCertificate(startTime + 4, ComponentCertificate("Configuratiezegel", "ditisdechecksum"))
      CertificateService.saveActiveCertificate(zookeeperClient, "test", active1)

      val active2 = ActiveCertificate(startTime + 4, ComponentCertificate("Camerazegel", "ditisdechecksumvandecamera"))
      CertificateService.saveActiveCertificate(zookeeperClient, "test", active2)

      val activeConfigurationCertificateComponents = CertificateService.getActiveConfigurationCertificateComponents(zookeeperClient, "test", startTime + 4.day.toMillis)

      activeConfigurationCertificateComponents.length must be(2)
      activeConfigurationCertificateComponents.foreach(c ⇒
        activeComponentCertificates.contains(c.componentCertificate) must be(true))
    }
  }

  "CertificateService.getActiveSoftwareCertificate" must {
    "return a componentCertificate with an blank seal when no active SoftwareCertificate can be found in zookeeper." in {
      val active = ActiveCertificate(startTime + 3, ComponentCertificate("matcher1", "blah1"))
      CertificateService.saveActiveCertificate(zookeeperClient, "test", active)

      val componentCertificate = CertificateService.getActiveSoftwareCertificate(zookeeperClient, "test", startTime + 3.day.toMillis)
      componentCertificate must be(ComponentCertificate("Softwarezegel", "blah1"))
    }

    "return the active software(Type) certificate with One ComponentCertificate present in zookeeper." in {
      val active = ActiveCertificate(startTime + 3, ComponentCertificate("matcher1", "blah1"))
      CertificateService.saveActiveCertificate(zookeeperClient, "test", active)

      val componentCertificate = CertificateService.getActiveSoftwareCertificate(zookeeperClient, "test", startTime + 3.day.toMillis)
      componentCertificate must be(ComponentCertificate("Softwarezegel", "blah1"))
    }

    "return the active software(Type) certificate with Two ComponentCertificate's present in zookeeper." in {
      val seal = CertificateService.calculateSeal(List(ComponentCertificate("matcher1", "blah1"), ComponentCertificate("vr2", "blah2")))

      val active1 = ActiveCertificate(startTime, ComponentCertificate("matcher1", "blah1"))
      CertificateService.saveActiveCertificate(zookeeperClient, "test", active1)

      val active2 = ActiveCertificate(startTime, ComponentCertificate("vr2", "blah2"))
      CertificateService.saveActiveCertificate(zookeeperClient, "test", active2)

      val componentCertificate = CertificateService.getActiveSoftwareCertificate(zookeeperClient, "test", startTime)

      componentCertificate must be(ComponentCertificate("Softwarezegel", seal))
    }
  }

  "CertificateService.getActiveSoftwareCertificateComponents" must {
    "return two active software(Type) components with two active SoftwareCertificate's in zookeeper." in {
      val activeComponentCertificates = List(ComponentCertificate("matcher1", "blah1"), ComponentCertificate("vr2", "blah2"))

      val active1 = ActiveCertificate(startTime, ComponentCertificate("matcher1", "blah1"))
      CertificateService.saveActiveCertificate(zookeeperClient, "test", active1)

      val active2 = ActiveCertificate(startTime, ComponentCertificate("vr2", "blah2"))
      CertificateService.saveActiveCertificate(zookeeperClient, "test", active2)

      val activeSoftwareCertificateComponents = CertificateService.getActiveSoftwareCertificateComponents(zookeeperClient, "test", startTime)

      activeSoftwareCertificateComponents.length must be(2)
      activeSoftwareCertificateComponents.foreach(s ⇒
        activeComponentCertificates.contains(s.componentCertificate) must be(true))
    }
  }

  "CertificateService.getInstalledConfigurationCertificate" must {
    "return one software(Type) component certificate for globalType1 with the correct seal." in {
      val seal = CertificateService.calculateSeal(List(ComponentCertificate("matcher1", "blah1"), ComponentCertificate("vr2", "blah2")))
      // Get globalType1
      val installedSoftwareCertificate = CertificateService.getInstalledSoftwareCertificate(zookeeperClient, "test", startTime)
      installedSoftwareCertificate.checksum must be(seal)
      installedSoftwareCertificate.name must be("Softwarezegel")

    }
  }

  "CertificateService.getInstalledSoftwareCertificate" must {
    "return one configuration(Location) component certificate for location5 with the correct seal." in {
      val seal = CertificateService.calculateSeal(List(
        ComponentCertificate("Configuratiezegel", "ditisdechecksum"), ComponentCertificate("Camerazegel", "ditisdechecksumvandecamera")))
      // Get location5
      val installedConfigurationCertificate = CertificateService.getInstalledConfigurationCertificate(zookeeperClient, "test", startTime + 4.day.toMillis)
      installedConfigurationCertificate.checksum must be(seal)
      installedConfigurationCertificate.name must be("Configuratiezegel")
    }
  }

  "CertificateService.calculateSeal" must {
    "return a empty string when called without ComponentCertificate's." in {
      val hash = CertificateService.calculateSeal(Nil)
      hash.length must be(0)
      hash must be("")
    }

    "return the checksum of the ComponentCertificate when called with one ComponentCertificate." in {
      val componentCertificates = Seq(ComponentCertificate("component1", "givemesomehash"))
      val hash = CertificateService.calculateSeal(componentCertificates)
      hash must be("givemesomehash")
    }

    "return a combined hash when called with multiple ComponentCertificate's." in {
      val componentCertificates = Seq(ComponentCertificate("component1", "please hash this value"), ComponentCertificate("component2", "hash this as well"))
      val hash = CertificateService.calculateSeal(componentCertificates)
      hash.length must be(32)
      hash must be("19719b13692773f30c280fca46ce2d1a")
    }
  }
}