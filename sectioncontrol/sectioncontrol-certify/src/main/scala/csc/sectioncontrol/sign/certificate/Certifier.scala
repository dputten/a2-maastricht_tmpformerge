/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.sign.certificate

import java.text.SimpleDateFormat
import java.util.Date

import akka.actor.{ Actor, ActorSystem, Props }
import akka.event.LoggingAdapter

import csc.curator.utils.Curator

import csc.sectioncontrol.sign.SoftwareChecksum
import csc.sectioncontrol.messages.certificates.{ ComponentCertificate, ActiveCertificate }
import csc.sectioncontrol.messages.SystemEvent

//TODO Vehicle registration must be also certified as a component
/**
 * Certifier should be mixed with those MultiSystemBoot classes which expect to keep their JAR Certificate in ZooKeeper
 * (Matcher, Enforce-NL etc)
 * req 54: provide checksum (softwarezegel)
 * req 85: check checksum
 */
trait Certifier {
  var log: LoggingAdapter

  def getRuntimeRef = this.getClass

  /**
   * Create an ActiveCertificate for the JAR file and put it to ZooKeeper under Paths.Systems.getActiveCertificatesPath
   * Check if the generated ActiveCertificate matches the certificate inside TypeCertificate with the
   * same name. Errors are published to the system event stream.
   */
  def createAndRegisterCertificate(actorSystem: ActorSystem, systemId: String,
                                   curator: Curator, providedModule: Option[String] = None, providedSha: Option[String] = None) {
    //create, register and check the JAR certificate
    val module: String = providedModule match {
      case None           ⇒ moduleName
      case Some(provided) ⇒ provided
    }

    actorSystem.actorOf(Props(new Actor() {
      protected def receive = {
        case message: ShaMessage ⇒
          val checkSum: SoftwareChecksum = message.sha match {
            case None           ⇒ SoftwareChecksum.createHashFromRuntimeClass(getRuntimeRef).getOrElse(SoftwareChecksum(message.module, "No checksum available"))
            case Some(provided) ⇒ new SoftwareChecksum(message.module, provided)
          }
          val now = System.currentTimeMillis()
          val componentCertificate = new ComponentCertificate(checkSum.fileName, checkSum.checksum)
          val certificate = ActiveCertificate(now, componentCertificate)
          CertificateService.deleteOldActiveCertificatesVersions(curator, systemId, certificate.componentCertificate.name)
          CertificateService.saveActiveCertificate(curator, systemId, certificate)
          //compare with provided
          val measureType = CertificateService.getAllInstalledComponentCertificates(curator, systemId, certificate.time)
          measureType match {
            case Right(components) ⇒ {
              //find the certificate with the same name and check its checksum
              components.find(provided ⇒ componentCertificate.name == provided.name) match {
                case None ⇒ {
                  //When no component certificate found inside TypeCertificate
                  // the component doesn't needed to be checked
                  log.warning("Het component certificaat (%s) is onbekend in Type Certificate".format(checkSum.fileName))
                }
                case Some(c) ⇒ {
                  if (c.checksum != componentCertificate.checksum) {
                    //certificates do not match
                    //req 85: Een trajectcontrolesysteem moet de waarde van de softwarezegels elke dag na
                    //het verstrijken van de meetdag bepalen voordat de data in de betreffende map
                    //wordt gezet.
                    val error = "Actieve certificaat %s is ongelijk aan inhoud van Type Certificate (%s != %s)".format(
                      checkSum.fileName, c.checksum, componentCertificate.checksum)
                    context.system.eventStream.publish(SystemEvent(checkSum.fileName, System.currentTimeMillis(),
                      systemId, "Alarm", "system", Some(error)))
                  }
                }
              }

            }
            case Left(msg) ⇒
              //log warning- no TypeCertificate found in ZooKeeper
              val timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss.SSS")
              context.system.eventStream.publish(SystemEvent(checkSum.fileName, System.currentTimeMillis(),
                systemId, "Alarm", "system", Some(msg + " niet gevonden voor systeem %s met tijd %s:".format(systemId, timeFormat.format(new Date(now))))))
          }
          context.stop(self)
      }

    }), module + "_" + systemId + "_Certifier") ! ShaMessage(module, providedSha)
  }

  case class ShaMessage(module: String, sha: Option[String])

  protected def moduleName: String
}
