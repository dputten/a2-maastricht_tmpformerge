/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.sign.certificate

import csc.config.Path
import csc.curator.utils.Curator
import csc.sectioncontrol.sign.Signing
import csc.sectioncontrol.storage.Paths
import org.slf4j.LoggerFactory
import scala.Left
import csc.sectioncontrol.messages.certificates.LocationCertificate
import csc.curator.utils.Versioned
import csc.sectioncontrol.messages.certificates.MeasurementMethodType
import scala.Some
import scala.Right
import csc.sectioncontrol.messages.certificates.MeasurementMethodInstallation
import csc.sectioncontrol.messages.certificates.ActiveCertificate
import csc.sectioncontrol.messages.certificates.ComponentCertificate
import csc.sectioncontrol.messages.certificates.MeasurementMethodInstallationType

object CertificateService extends CertificateService {

  private val log = LoggerFactory.getLogger(this.getClass.getName)

  def getActiveCertificatesPath(systemId: String) = "/ctes/systems/%s/activeCertificates".format(systemId)

  def getAllInstalledMeasurementMethodTypes(curator: Curator, systemId: String): Seq[MeasurementMethodInstallationType] = {
    val path = Path(Paths.Systems.getInstallCertificatePath(systemId))
    val children = curator.getChildren(path)

    val install = children.flatMap(curator.get[MeasurementMethodInstallation](_))
      .sortBy(_.timeFrom)

    install.flatMap(inst ⇒ {
      val path = Paths.Configuration.getCertificateMeasurementMethodTypePath(inst.typeId)
      curator.get[MeasurementMethodType](path).map(new MeasurementMethodInstallationType(inst.timeFrom, _))
    })
  }

  def getInstalledMeasurementMethodType(curator: Curator, systemId: String, time: Long): Option[MeasurementMethodType] = {
    val install = getMeasurementMethodInstall(curator, systemId, time)
    install.flatMap(install ⇒ {
      val path = Paths.Configuration.getCertificateMeasurementMethodTypePath(install.typeId)
      curator.get[MeasurementMethodType](path)
    })
  }

  // Id in measurementMethodInstallation acts like a foreign key to ctes\configuration\certificates
  def getMeasurementMethodInstall(curator: Curator, systemId: String, time: Long): Option[MeasurementMethodInstallation] = {
    val path = Path(Paths.Systems.getInstallCertificatePath(systemId))
    val children = curator.getChildren(path)

    val measurementMethodInstallation = children.flatMap(curator.get[MeasurementMethodInstallation](_))
    measurementMethodInstallation.sortBy(_.timeFrom).reverse.find(_.timeFrom <= time)
  }

  def getLocationCertificate(curator: Curator, systemId: String, time: Long): Option[LocationCertificate] = {
    val path = Path(Paths.Systems.getLocationCertificatePath(systemId))
    val children = curator.getChildren(path)

    val install = children.flatMap(curator.get[LocationCertificate](_))
    getCurrentLocationCertificate(install, time)
  }

  def getLocationCertificates(curator: Curator, systemId: String): Seq[LocationCertificate] = {
    val path = Path(Paths.Systems.getLocationCertificatePath(systemId))
    val children = curator.getChildren(path)
    children.flatMap(curator.get[LocationCertificate](_))
  }

  def getCurrentCertificate(cert: Seq[MeasurementMethodInstallationType], time: Long): Option[MeasurementMethodInstallationType] = {
    cert.sortBy(_.timeFrom).reverse.find(_.timeFrom <= time)
  }

  override def getCurrentCertificate(curator: Curator, systemId: String, time: Long): Option[MeasurementMethodInstallationType] = {
    val cert = getAllInstalledMeasurementMethodTypes(curator, systemId)
    getCurrentCertificate(cert, time)
  }

  def getCurrentLocationCertificate(cert: Seq[LocationCertificate], time: Long): Option[LocationCertificate] = {
    cert.sortBy(_.inspectionDate).reverse.find(loc ⇒ loc.inspectionDate <= time && loc.validTime > time)
  }

  def getActiveCertificate(curator: Curator, systemId: String, componentName: String): Option[ActiveCertificate] = {
    val path = Path(Paths.Systems.getActiveCertificatesPath(systemId)) / componentName
    curator.get[ActiveCertificate](path.toString())
  }

  def getAllActiveCertificates(curator: Curator, systemId: String): Seq[ActiveCertificate] = {
    val path = Path(getActiveCertificatesPath(systemId))
    val componentNames = curator.getChildNames(path)
    componentNames.flatMap(name ⇒ getActiveCertificate(curator, systemId, name))
  }

  override def saveActiveCertificate(curator: Curator, systemId: String, certificate: ActiveCertificate) {
    val path = Path(Paths.Systems.getActiveCertificatesPath(systemId)) / certificate.componentCertificate.name
    curator.getVersioned[ActiveCertificate](path.toString()) match {
      case Some(Versioned(_, v)) ⇒
        curator.set(path.toString(), certificate, v)
      case None ⇒
        curator.put(path.toString(), certificate)
    }
  }

  def deleteOldActiveCertificatesVersions(curator: Curator, systemId: String, compName: String) {
    val pos = compName.indexOf("_")
    if (pos > 0) {
      val baseName = compName.substring(0, pos)
      val path = Paths.Systems.getActiveCertificatesPath(systemId)
      val moduleNames = curator.getChildNames(path)

      val oldVersions = moduleNames.filter(name ⇒ name != compName && name.startsWith(baseName))

      oldVersions.foreach(name ⇒ curator.delete(path + "/" + name))
    }
  }

  def getAllInstalledComponentCertificates(curator: Curator, systemId: String, time: Long): Either[String, List[ComponentCertificate]] = {
    //get typecertificate components
    val typeCert = getInstalledMeasurementMethodType(curator, systemId, time)
    //get location components
    val location = getLocationCertificate(curator, systemId, time)
    //join lists
    (typeCert.map(_.typeCertificate.components), location.map(_.components)) match {
      case (Some(l1), Some(l2)) ⇒ Right(l1 ++ l2)
      case (None, Some(_))      ⇒ Left("Type-certificaat")
      case (Some(_), None)      ⇒ Left("Locatie-certificaat")
      case (None, None)         ⇒ Left("Type en locatie-certificaat")
    }
  }

  override def getActiveConfigurationCertificate(curator: Curator, systemId: String, time: Long = System.currentTimeMillis()): ComponentCertificate = {
    log.info("Calculating active configuratiezegel seal for {} at {}", systemId, time)
    val seal = getLocationCertificate(curator, systemId, time) match {
      case None ⇒ ""
      case Some(locationCertificate) ⇒
        val activeCertificates = locationCertificate.components flatMap { (component) ⇒
          log.info("configuratiezegel seal using component <" + component.name + ">")
          getActiveCertificate(curator, systemId, component.name)
        }
        calculateSeal(activeCertificates.map(_.componentCertificate))
    }

    log.info("Active configuratiezegel seal=<" + seal + ">")
    ComponentCertificate("Configuratiezegel", seal)
  }

  def getActiveConfigurationCertificateComponents(curator: Curator, systemId: String, time: Long = System.currentTimeMillis()): List[ActiveCertificate] = {
    getLocationCertificate(curator, systemId, time) match {
      case None ⇒ Nil
      case Some(locationCertificate) ⇒
        locationCertificate.components.flatMap(component ⇒ {
          getActiveCertificate(curator, systemId, component.name)
        })
    }
  }

  def getActiveSoftwareCertificateComponents(curator: Curator, systemId: String, time: Long = System.currentTimeMillis()): List[ActiveCertificate] = {
    getInstalledMeasurementMethodType(curator, systemId, time) match {
      case None ⇒ Nil
      case Some(measurementMethodType) ⇒
        measurementMethodType.typeCertificate.components.flatMap(component ⇒ {
          getActiveCertificate(curator, systemId, component.name)
        })
    }
  }

  override def getActiveSoftwareCertificate(curator: Curator, systemId: String, time: Long = System.currentTimeMillis()): ComponentCertificate = {
    log.info("Calculating active software seal for {} at {}", systemId, time)
    val seal = getInstalledMeasurementMethodType(curator, systemId, time) match {
      case None ⇒ ""
      case Some(measurementMethodType) ⇒
        val activeCertificates = measurementMethodType.typeCertificate.components flatMap { (component) ⇒
          log.info("software seal using component <" + component.name + ">")
          getActiveCertificate(curator, systemId, component.name)
        }
        calculateSeal(activeCertificates.map(_.componentCertificate))
    }

    log.info("Active software seal=<" + seal + ">")
    ComponentCertificate("Softwarezegel", seal)
  }

  def getInstalledSoftwareCertificate(curator: Curator, systemId: String, time: Long = System.currentTimeMillis()): ComponentCertificate = {
    log.info("Calculating installed software seal for {} at {}", systemId, time)
    val seal = getInstalledMeasurementMethodType(curator, systemId, time) match {
      case None ⇒ ""
      case Some(measurementMethodType) ⇒
        calculateSeal(measurementMethodType.typeCertificate.components)
    }

    log.info("Installed software seal=<" + seal + ">")
    ComponentCertificate("Softwarezegel", seal)
  }

  def getInstalledConfigurationCertificate(curator: Curator, systemId: String, time: Long = System.currentTimeMillis()): ComponentCertificate = {
    log.info("Calculating installed configuration seal for {} at {}", systemId, time)
    val seal = getLocationCertificate(curator, systemId, time) match {
      case None ⇒ ""
      case Some(locationCertificate) ⇒
        calculateSeal(locationCertificate.components)
    }

    log.info("Installed configuration seal=<" + seal + ">")
    ComponentCertificate("Configuratiezegel", seal)
  }

  /**
   * Only create a new hash if there are multiple certificates
   */
  def calculateSeal(componentCertificates: Seq[ComponentCertificate]): String = {
    val appendedChecksums = componentCertificates.sortBy(_.name).foldLeft("") { (accu: String, cert: ComponentCertificate) ⇒
      accu + cert.checksum
    }

    componentCertificates.length match {
      case 0 ⇒ ""
      case 1 ⇒ componentCertificates(0).checksum
      case _ ⇒ Signing.calculateHash(Some(appendedChecksums.getBytes("UTF-8")))
    }
  }

}

trait CertificateService {

  def getActiveConfigurationCertificate(curator: Curator, systemId: String, time: Long = System.currentTimeMillis()): ComponentCertificate

  def getActiveSoftwareCertificate(curator: Curator, systemId: String, time: Long = System.currentTimeMillis()): ComponentCertificate

  def getCurrentCertificate(curator: Curator, systemId: String, time: Long): Option[MeasurementMethodInstallationType]
  def saveActiveCertificate(curator: Curator, systemId: String, certificate: ActiveCertificate): Unit
}
