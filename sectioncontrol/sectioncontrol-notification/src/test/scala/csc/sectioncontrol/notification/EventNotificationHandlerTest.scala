/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.notification

import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers
import akka.actor.{ ActorRef, ActorSystem }
import akka.testkit.{ TestActorRef, TestProbe }
import akka.util.duration._
import collection.immutable.HashMap
import csc.dlog.Event
import csc.config.Path
import net.liftweb.json.DefaultFormats
import csc.curator.utils.{ CuratorToolsImpl, Curator }
import csc.curator.CuratorTestServer
import csc.akkautils.DirectLoggingAdapter
import net.liftweb.json.DefaultFormats
/**
 * Test the EventNotificationHandler actor.
 */
class EventNotificationHandlerTest extends WordSpec with MustMatchers with CuratorTestServer with BeforeAndAfterAll {
  implicit val testSystem = ActorSystem("EventNotificationHandlerTest")

  var curator: Curator = _

  override def beforeAll() {
    super.beforeAll()

  }

  override def afterAll() {
    testSystem.shutdown()
    super.afterAll()
  }

  override def beforeEach() {
    super.beforeEach()
    curator = new CuratorToolsImpl(clientScope, new DirectLoggingAdapter(this.getClass.getName), DefaultFormats)
  }

  override def afterEach() {
    curator.deleteRecursive("/ctes")
    super.afterEach()
  }

  val testEvent = new Event { val timestamp = 123456L; val eventType = "testEvent" }
  val otherTestEvent = new Event { val timestamp = 121212L; val eventType = "otherTestEvent" }
  val testSmsEvent = new Event { val timestamp = 234567L; val eventType = "testSmsEvent" }
  val testEmailEvent = new Event { val timestamp = 345678L; val eventType = "testEmailEvent" }
  val noTestEvent = new Event { val timestamp = 654321L; val eventType = "noTestEvent" }

  def createEventNotificationHandlerActor(notifiers: Map[NotifierId, ActorRef]): ActorRef = {
    val systemId = "SystemId"
    TestActorRef(new EventNotificationHandler(systemId = systemId, curator = curator, notifiers = notifiers))
  }

  "The EventNotificationHandler actor" when {

    "testEvent's are configured to be notified by sms" must {
      "send an incoming TestEvent to the SmsNotifier" in {
        // Prepare config
        val stateConfig = new NotificationStateConfig(isEnabled = true)
        val eventsConfig = new NotifierEventsConfig(events = Seq("testEvent"))
        setSmsOnlyConfig(stateConfig, eventsConfig)
        // Create tested actor
        val smsNotifierProbe = TestProbe()
        val otherNotifierProbe = TestProbe()
        val notifiers: Map[NotifierId, ActorRef] = HashMap("sms" -> smsNotifierProbe.ref, "other" -> otherNotifierProbe.ref)
        val eventNotificationHandlerActorRef = createEventNotificationHandlerActor(notifiers = notifiers)

        // Send message to EventNotificationHandler
        eventNotificationHandlerActorRef ! testEvent
        // Check expected results
        smsNotifierProbe.expectMsg(500 millis, testEvent)
      }

      "send all incoming testEvent to the SmsNotifier" in {
        // Prepare config
        val stateConfig = new NotificationStateConfig(isEnabled = true)
        val eventsConfig = new NotifierEventsConfig(events = Seq("testEvent"))
        setSmsOnlyConfig(stateConfig, eventsConfig)
        // Create tested actor
        val smsNotifierProbe = TestProbe()
        val otherNotifierProbe = TestProbe()
        val notifiers: Map[NotifierId, ActorRef] = HashMap("sms" -> smsNotifierProbe.ref, "other" -> otherNotifierProbe.ref)
        val eventNotificationHandlerActorRef = createEventNotificationHandlerActor(notifiers = notifiers)

        // Send message to EventNotificationHandler
        eventNotificationHandlerActorRef ! testEvent
        eventNotificationHandlerActorRef ! testEvent
        eventNotificationHandlerActorRef ! testEvent
        // Check expected results
        val expectedMessages = smsNotifierProbe.receiveN(3, 500 millis)
        expectedMessages must be(Seq(testEvent, testEvent, testEvent))
      }

      "send nothing to other configured notifiers" in {
        // Prepare config
        val stateConfig = new NotificationStateConfig(isEnabled = true)
        val eventsConfig = new NotifierEventsConfig(events = Seq("testEvent"))
        setSmsOnlyConfig(stateConfig, eventsConfig)
        // Create tested actor
        val smsNotifierProbe = TestProbe()
        val otherNotifierProbe = TestProbe()
        val notifiers: Map[NotifierId, ActorRef] = HashMap("sms" -> smsNotifierProbe.ref, "other" -> otherNotifierProbe.ref)
        val eventNotificationHandlerActorRef = createEventNotificationHandlerActor(notifiers = notifiers)

        // Send message to EventNotificationHandler
        eventNotificationHandlerActorRef ! testEvent
        // Check expected results
        otherNotifierProbe.expectNoMsg(500 millis)
      }

      "ignore an incoming noTestEvent" in {
        // Prepare config
        val stateConfig = new NotificationStateConfig(isEnabled = true)
        val eventsConfig = new NotifierEventsConfig(events = Seq("testEvent"))
        setSmsOnlyConfig(stateConfig, eventsConfig)
        // Create tested actor
        val smsNotifierProbe = TestProbe()
        val otherNotifierProbe = TestProbe()
        val notifiers: Map[NotifierId, ActorRef] = HashMap("sms" -> smsNotifierProbe.ref, "other" -> otherNotifierProbe.ref)
        val eventNotificationHandlerActorRef = createEventNotificationHandlerActor(notifiers = notifiers)

        // Send message to EventNotificationHandler
        eventNotificationHandlerActorRef ! noTestEvent
        // Check expected results
        smsNotifierProbe.expectNoMsg(500 millis)
        otherNotifierProbe.expectNoMsg(500 millis)
      }
    }

    "testEvent's are configured to be notified by sms and email" must {
      "send an incoming testEvent to the SmsNotifier and the EmailNotifier" in {
        // Prepare config
        val stateConfig = new NotificationStateConfig(isEnabled = true)
        val eventsConfig = new NotifierEventsConfig(events = Seq("testEvent"))
        setSmsAndEmailConfig(stateConfig, eventsConfig, eventsConfig)
        // Create tested actor
        val smsNotifierProbe = TestProbe()
        val emailNotifierProbe = TestProbe()
        val otherNotifierProbe = TestProbe()
        val notifiers: Map[NotifierId, ActorRef] = HashMap("sms" -> smsNotifierProbe.ref, "email" -> emailNotifierProbe.ref, "other" -> otherNotifierProbe.ref)
        val eventNotificationHandlerActorRef = createEventNotificationHandlerActor(notifiers = notifiers)

        // Send message to EventNotificationHandler
        eventNotificationHandlerActorRef ! testEvent
        // Check expected results
        smsNotifierProbe.expectMsg(500 millis, testEvent)
        emailNotifierProbe.expectMsg(500 millis, testEvent)
      }

      "send all incoming testEvent to the SmsNotifier and the EmailNotifier" in {
        // Prepare config
        val stateConfig = new NotificationStateConfig(isEnabled = true)
        val eventsConfig = new NotifierEventsConfig(events = Seq("testEvent"))
        setSmsAndEmailConfig(stateConfig, eventsConfig, eventsConfig)
        // Create tested actor
        val smsNotifierProbe = TestProbe()
        val emailNotifierProbe = TestProbe()
        val otherNotifierProbe = TestProbe()
        val notifiers: Map[NotifierId, ActorRef] = HashMap("sms" -> smsNotifierProbe.ref, "email" -> emailNotifierProbe.ref, "other" -> otherNotifierProbe.ref)
        val eventNotificationHandlerActorRef = createEventNotificationHandlerActor(notifiers = notifiers)

        // Send message to EventNotificationHandler
        eventNotificationHandlerActorRef ! testEvent
        eventNotificationHandlerActorRef ! testEvent
        eventNotificationHandlerActorRef ! testEvent
        // Check expected results
        val expectedMessagesSmsNotifier = smsNotifierProbe.receiveN(3, 500 millis)
        val expectedMessagesEmailNotifier = emailNotifierProbe.receiveN(3, 500 millis)
        expectedMessagesSmsNotifier must be(Seq(testEvent, testEvent, testEvent))
        expectedMessagesEmailNotifier must be(Seq(testEvent, testEvent, testEvent))
      }

      "send nothing to other configured notifiers" in {
        // Prepare config
        val stateConfig = new NotificationStateConfig(isEnabled = true)
        val eventsConfig = new NotifierEventsConfig(events = Seq("testEvent"))
        setSmsAndEmailConfig(stateConfig, eventsConfig, eventsConfig)
        // Create tested actor
        val smsNotifierProbe = TestProbe()
        val emailNotifierProbe = TestProbe()
        val otherNotifierProbe = TestProbe()
        val notifiers: Map[NotifierId, ActorRef] = HashMap("sms" -> smsNotifierProbe.ref, "email" -> emailNotifierProbe.ref, "other" -> otherNotifierProbe.ref)
        val eventNotificationHandlerActorRef = createEventNotificationHandlerActor(notifiers = notifiers)

        // Send message to EventNotificationHandler
        eventNotificationHandlerActorRef ! testEvent
        // Check expected results
        otherNotifierProbe.expectNoMsg(500 millis)
      }

      "ignore an incoming noTestEvent" in {
        // Prepare config
        val stateConfig = new NotificationStateConfig(isEnabled = true)
        val eventsConfig = new NotifierEventsConfig(events = Seq("testEvent"))
        setSmsAndEmailConfig(stateConfig, eventsConfig, eventsConfig)
        // Create tested actor
        val smsNotifierProbe = TestProbe()
        val emailNotifierProbe = TestProbe()
        val otherNotifierProbe = TestProbe()
        val notifiers: Map[NotifierId, ActorRef] = HashMap("sms" -> smsNotifierProbe.ref, "email" -> emailNotifierProbe.ref, "other" -> otherNotifierProbe.ref)
        val eventNotificationHandlerActorRef = createEventNotificationHandlerActor(notifiers = notifiers)

        // Send message to EventNotificationHandler
        eventNotificationHandlerActorRef ! noTestEvent
        // Check expected results
        smsNotifierProbe.expectNoMsg(500 millis)
        emailNotifierProbe.expectNoMsg(500 millis)
        otherNotifierProbe.expectNoMsg(500 millis)
      }
    }

    "testEvent's and otherTestEvent's are configured to be notified by sms and email" must {
      "send an incoming testEvent to the SmsNotifier and the EmailNotifier" in {
        // Prepare config
        val stateConfig = new NotificationStateConfig(isEnabled = true)
        val eventsConfig = new NotifierEventsConfig(events = Seq("testEvent", "otherTestEvent"))
        setSmsAndEmailConfig(stateConfig, eventsConfig, eventsConfig)
        // Create tested actor
        val smsNotifierProbe = TestProbe()
        val emailNotifierProbe = TestProbe()
        val otherNotifierProbe = TestProbe()
        val notifiers: Map[NotifierId, ActorRef] = HashMap("sms" -> smsNotifierProbe.ref, "email" -> emailNotifierProbe.ref, "other" -> otherNotifierProbe.ref)
        val eventNotificationHandlerActorRef = createEventNotificationHandlerActor(notifiers = notifiers)

        // Send message to EventNotificationHandler
        eventNotificationHandlerActorRef ! testEvent
        // Check expected results
        smsNotifierProbe.expectMsg(500 millis, testEvent)
        emailNotifierProbe.expectMsg(500 millis, testEvent)
      }

      "send an incoming otherTestEvent to the SmsNotifier and the EmailNotifier" in {
        // Prepare config
        val stateConfig = new NotificationStateConfig(isEnabled = true)
        val eventsConfig = new NotifierEventsConfig(events = Seq("testEvent", "otherTestEvent"))
        setSmsAndEmailConfig(stateConfig, eventsConfig, eventsConfig)
        // Create tested actor
        val smsNotifierProbe = TestProbe()
        val emailNotifierProbe = TestProbe()
        val otherNotifierProbe = TestProbe()
        val notifiers: Map[NotifierId, ActorRef] = HashMap("sms" -> smsNotifierProbe.ref, "email" -> emailNotifierProbe.ref, "other" -> otherNotifierProbe.ref)
        val eventNotificationHandlerActorRef = createEventNotificationHandlerActor(notifiers = notifiers)

        // Send message to EventNotificationHandler
        eventNotificationHandlerActorRef ! otherTestEvent
        // Check expected results
        smsNotifierProbe.expectMsg(500 millis, otherTestEvent)
        emailNotifierProbe.expectMsg(500 millis, otherTestEvent)
      }

      "send all incoming testEvents en otherTestEvents to the SmsNotifier and the EmailNotifier" in {
        // Prepare config
        val stateConfig = new NotificationStateConfig(isEnabled = true)
        val eventsConfig = new NotifierEventsConfig(events = Seq("testEvent", "otherTestEvent"))
        setSmsAndEmailConfig(stateConfig, eventsConfig, eventsConfig)
        // Create tested actor
        val smsNotifierProbe = TestProbe()
        val emailNotifierProbe = TestProbe()
        val otherNotifierProbe = TestProbe()
        val notifiers: Map[NotifierId, ActorRef] = HashMap("sms" -> smsNotifierProbe.ref, "email" -> emailNotifierProbe.ref, "other" -> otherNotifierProbe.ref)
        val eventNotificationHandlerActorRef = createEventNotificationHandlerActor(notifiers = notifiers)

        // Send message to EventNotificationHandler
        eventNotificationHandlerActorRef ! testEvent
        eventNotificationHandlerActorRef ! otherTestEvent
        eventNotificationHandlerActorRef ! testEvent
        eventNotificationHandlerActorRef ! testEvent
        eventNotificationHandlerActorRef ! otherTestEvent
        // Check expected results
        val expectedMessagesSmsNotifier = smsNotifierProbe.receiveN(5, 500 millis)
        val expectedMessagesEmailNotifier = emailNotifierProbe.receiveN(5, 500 millis)
        expectedMessagesSmsNotifier must be(Seq(testEvent, otherTestEvent, testEvent, testEvent, otherTestEvent))
        expectedMessagesEmailNotifier must be(Seq(testEvent, otherTestEvent, testEvent, testEvent, otherTestEvent))
      }

      "send nothing to other configured notifiers" in {
        // Prepare config
        val stateConfig = new NotificationStateConfig(isEnabled = true)
        val eventsConfig = new NotifierEventsConfig(events = Seq("testEvent", "otherTestEvent"))
        setSmsAndEmailConfig(stateConfig, eventsConfig, eventsConfig)
        // Create tested actor
        val smsNotifierProbe = TestProbe()
        val emailNotifierProbe = TestProbe()
        val otherNotifierProbe = TestProbe()
        val notifiers: Map[NotifierId, ActorRef] = HashMap("sms" -> smsNotifierProbe.ref, "email" -> emailNotifierProbe.ref, "other" -> otherNotifierProbe.ref)
        val eventNotificationHandlerActorRef = createEventNotificationHandlerActor(notifiers = notifiers)

        // Send message to EventNotificationHandler
        eventNotificationHandlerActorRef ! testEvent
        eventNotificationHandlerActorRef ! otherTestEvent
        eventNotificationHandlerActorRef ! testEvent
        eventNotificationHandlerActorRef ! testEvent
        eventNotificationHandlerActorRef ! otherTestEvent
        // Check expected results
        otherNotifierProbe.expectNoMsg(500 millis)
      }

      "ignore an incoming noTestEvent" in {
        // Prepare config
        val stateConfig = new NotificationStateConfig(isEnabled = true)
        val eventsConfig = new NotifierEventsConfig(events = Seq("testEvent", "otherTestEvent"))
        setSmsAndEmailConfig(stateConfig, eventsConfig, eventsConfig)
        // Create tested actor
        val smsNotifierProbe = TestProbe()
        val emailNotifierProbe = TestProbe()
        val otherNotifierProbe = TestProbe()
        val notifiers: Map[NotifierId, ActorRef] = HashMap("sms" -> smsNotifierProbe.ref, "email" -> emailNotifierProbe.ref, "other" -> otherNotifierProbe.ref)
        val eventNotificationHandlerActorRef = createEventNotificationHandlerActor(notifiers = notifiers)

        // Send message to EventNotificationHandler
        eventNotificationHandlerActorRef ! noTestEvent
        // Check expected results
        smsNotifierProbe.expectNoMsg(500 millis)
        emailNotifierProbe.expectNoMsg(500 millis)
        otherNotifierProbe.expectNoMsg(500 millis)
      }
    }

    "testSmsEvent's are configured to be notified by sms and testEmailEvent's are configured to be notified by email" must {
      "send an incoming testSmsEvent to the SmsNotifier" in {
        // Prepare config
        val stateConfig = new NotificationStateConfig(isEnabled = true)
        val smsEventsConfig = new NotifierEventsConfig(events = Seq("testSmsEvent"))
        val emailEventsConfig = new NotifierEventsConfig(events = Seq("testEmailEvent"))
        setSmsAndEmailConfig(stateConfig, smsEventsConfig, emailEventsConfig)
        // Create tested actor
        val smsNotifierProbe = TestProbe()
        val emailNotifierProbe = TestProbe()
        val otherNotifierProbe = TestProbe()
        val notifiers: Map[NotifierId, ActorRef] = HashMap("sms" -> smsNotifierProbe.ref, "email" -> emailNotifierProbe.ref, "other" -> otherNotifierProbe.ref)
        val eventNotificationHandlerActorRef = createEventNotificationHandlerActor(notifiers = notifiers)

        // Send message to EventNotificationHandler
        eventNotificationHandlerActorRef ! testSmsEvent
        // Check expected results
        smsNotifierProbe.expectMsg(500 millis, testSmsEvent)
        emailNotifierProbe.expectNoMsg(500 millis)
      }

      "send an incoming testEmailEvent to the EmailNotifier" in {
        // Prepare config
        val stateConfig = new NotificationStateConfig(isEnabled = true)
        val smsEventsConfig = new NotifierEventsConfig(events = Seq("testSmsEvent"))
        val emailEventsConfig = new NotifierEventsConfig(events = Seq("testEmailEvent"))
        setSmsAndEmailConfig(stateConfig, smsEventsConfig, emailEventsConfig)
        // Create tested actor
        val smsNotifierProbe = TestProbe()
        val emailNotifierProbe = TestProbe()
        val otherNotifierProbe = TestProbe()
        val notifiers: Map[NotifierId, ActorRef] = HashMap("sms" -> smsNotifierProbe.ref, "email" -> emailNotifierProbe.ref, "other" -> otherNotifierProbe.ref)
        val eventNotificationHandlerActorRef = createEventNotificationHandlerActor(notifiers = notifiers)

        // Send message to EventNotificationHandler
        eventNotificationHandlerActorRef ! testEmailEvent
        smsNotifierProbe.expectNoMsg(500 millis)
        emailNotifierProbe.expectMsg(500 millis, testEmailEvent)
      }

      "send all incoming testSmsEvent's to the SmsNotifier and all incoming testEmailEvent's to the EmailNotifier" in {
        // Prepare config
        val stateConfig = new NotificationStateConfig(isEnabled = true)
        val smsEventsConfig = new NotifierEventsConfig(events = Seq("testSmsEvent"))
        val emailEventsConfig = new NotifierEventsConfig(events = Seq("testEmailEvent"))
        setSmsAndEmailConfig(stateConfig, smsEventsConfig, emailEventsConfig)
        // Create tested actor
        val smsNotifierProbe = TestProbe()
        val emailNotifierProbe = TestProbe()
        val otherNotifierProbe = TestProbe()
        val notifiers: Map[NotifierId, ActorRef] = HashMap("sms" -> smsNotifierProbe.ref, "email" -> emailNotifierProbe.ref, "other" -> otherNotifierProbe.ref)
        val eventNotificationHandlerActorRef = createEventNotificationHandlerActor(notifiers = notifiers)

        // Send message to EventNotificationHandler
        eventNotificationHandlerActorRef ! testSmsEvent
        eventNotificationHandlerActorRef ! testEmailEvent
        eventNotificationHandlerActorRef ! testEmailEvent
        eventNotificationHandlerActorRef ! testSmsEvent
        eventNotificationHandlerActorRef ! testSmsEvent
        // Check expected results
        val expectedMessagesSmsNotifier = smsNotifierProbe.receiveN(3, 500 millis)
        val expectedMessagesEmailNotifier = emailNotifierProbe.receiveN(2, 500 millis)
        expectedMessagesSmsNotifier must be(Seq(testSmsEvent, testSmsEvent, testSmsEvent))
        expectedMessagesEmailNotifier must be(Seq(testEmailEvent, testEmailEvent))
      }

      "send nothing to other configured notifiers" in {
        // Prepare config
        val stateConfig = new NotificationStateConfig(isEnabled = true)
        val smsEventsConfig = new NotifierEventsConfig(events = Seq("testSmsEvent"))
        val emailEventsConfig = new NotifierEventsConfig(events = Seq("testEmailEvent"))
        setSmsAndEmailConfig(stateConfig, smsEventsConfig, emailEventsConfig)
        // Create tested actor
        val smsNotifierProbe = TestProbe()
        val emailNotifierProbe = TestProbe()
        val otherNotifierProbe = TestProbe()
        val notifiers: Map[NotifierId, ActorRef] = HashMap("sms" -> smsNotifierProbe.ref, "email" -> emailNotifierProbe.ref, "other" -> otherNotifierProbe.ref)
        val eventNotificationHandlerActorRef = createEventNotificationHandlerActor(notifiers = notifiers)

        // Send message to EventNotificationHandler
        eventNotificationHandlerActorRef ! testSmsEvent
        eventNotificationHandlerActorRef ! testEmailEvent
        eventNotificationHandlerActorRef ! testEmailEvent
        eventNotificationHandlerActorRef ! testSmsEvent
        // Check expected results
        otherNotifierProbe.expectNoMsg(500 millis)
      }

      "ignore an incoming noTestEvent" in {
        // Prepare config
        val stateConfig = new NotificationStateConfig(isEnabled = true)
        val smsEventsConfig = new NotifierEventsConfig(events = Seq("testSmsEvent"))
        val emailEventsConfig = new NotifierEventsConfig(events = Seq("testEmailEvent"))
        setSmsAndEmailConfig(stateConfig, smsEventsConfig, emailEventsConfig)
        // Create tested actor
        val smsNotifierProbe = TestProbe()
        val emailNotifierProbe = TestProbe()
        val otherNotifierProbe = TestProbe()
        val notifiers: Map[NotifierId, ActorRef] = HashMap("sms" -> smsNotifierProbe.ref, "email" -> emailNotifierProbe.ref, "other" -> otherNotifierProbe.ref)
        val eventNotificationHandlerActorRef = createEventNotificationHandlerActor(notifiers = notifiers)

        // Send message to EventNotificationHandler
        eventNotificationHandlerActorRef ! noTestEvent
        // Check expected results
        smsNotifierProbe.expectNoMsg(500 millis)
        emailNotifierProbe.expectNoMsg(500 millis)
        otherNotifierProbe.expectNoMsg(500 millis)
      }
    }

    "notification is disabled" must {
      "ignore all incoming events" in {
        // Prepare config
        val stateConfig = new NotificationStateConfig(isEnabled = false)
        val eventsConfig = new NotifierEventsConfig(events = Seq("testEvent"))
        setSmsAndEmailConfig(stateConfig, eventsConfig, eventsConfig)
        // Create tested actor
        val smsNotifierProbe = TestProbe()
        val emailNotifierProbe = TestProbe()
        val otherNotifierProbe = TestProbe()
        val notifiers: Map[NotifierId, ActorRef] = HashMap("sms" -> smsNotifierProbe.ref, "email" -> emailNotifierProbe.ref, "other" -> otherNotifierProbe.ref)
        val eventNotificationHandlerActorRef = createEventNotificationHandlerActor(notifiers = notifiers)

        // Send message to EventNotificationHandler
        eventNotificationHandlerActorRef ! testEvent
        eventNotificationHandlerActorRef ! noTestEvent
        eventNotificationHandlerActorRef ! testEvent
        // Check expected results
        smsNotifierProbe.expectNoMsg(500 millis)
        emailNotifierProbe.expectNoMsg(500 millis)
        otherNotifierProbe.expectNoMsg(500 millis)
      }
    }
  }

  def setSmsOnlyConfig(stateConfig: NotificationStateConfig, smsEventsConfig: NotifierEventsConfig) {
    curator.put(Path("/ctes/configuration/notification/state"), stateConfig)
    curator.put(Path("/ctes/configuration/notification/notifiers/sms/events"), smsEventsConfig)
  }

  def setSmsAndEmailConfig(stateConfig: NotificationStateConfig, smsEventsConfig: NotifierEventsConfig, emailEventsConfig: NotifierEventsConfig) {
    curator.put(Path("/ctes/configuration/notification/state"), stateConfig)
    curator.put(Path("/ctes/configuration/notification/notifiers/sms/events"), smsEventsConfig)
    curator.put(Path("/ctes/configuration/notification/notifiers/email/events"), emailEventsConfig)
  }
}
