/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.notification

import akka.actor.ActorSystem
import csc.config.Path
import csc.json.lift.EnumerationSerializer
import email.{ EmailData, TestUtils }
import net.liftweb.json.DefaultFormats
import csc.dlog.Event
import akka.testkit.TestActorRef
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers
import csc.curator.CuratorTestServer
import org.jvnet.mock_javamail.Mailbox
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storage.ZkCorridorInfo
import scala.Some
import csc.sectioncontrol.storage.ZkCorridor
import org.apache.hadoop.hbase.util.Bytes
import csc.sectioncontrol.messages.{ EsaProviderType, SystemEvent }
import csc.sectioncontrol.notification.SmsByEmailNotifierConfig._
import csc.curator.utils.{ CuratorToolsImpl, Curator }
import csc.akkautils.DirectLoggingAdapter
/**
 * Test van SmsByEmailNotifier actor.
 */
class SmsByEmailNotifierTest extends WordSpec with MustMatchers with TestUtils with CuratorTestServer with BeforeAndAfterAll {
  implicit val testSystem = ActorSystem("SmsByEmailNotifierTest")

  val systemId = "systemId"

  var curator: Curator = _
  var testActorRef: TestActorRef[SmsByEmailNotifier] = _

  var eventCountBefore: Int = _

  override protected def beforeAll() {
    super.beforeAll()
    implicit def string2ByteArray(s: String): Array[Byte] = Bytes.toBytes(s)
  }

  override protected def afterAll() {
    Mailbox.clearAll()
    //curator.curator.map(_.close)
    testSystem.shutdown()
    super.afterAll()
  }

  override def beforeEach() {
    super.beforeEach()
    val formats = DefaultFormats + new EnumerationSerializer(ServiceType, ZkCaseFileType, EsaProviderType,
      ConfigType)
    curator = new CuratorToolsImpl(clientScope, new DirectLoggingAdapter(this.getClass.getName), formats)
    curator.createEmptyPath("/ctes")
    curator.createEmptyPath("/ctes/configuration")
    curator.createEmptyPath("/ctes/configuration/notification")
    curator.createEmptyPath("/ctes/configuration/notification/notifiers/SmsByEmail")
    curator.createEmptyPath("/ctes/systems")
    curator.createEmptyPath("/ctes/systems/systemId")
    curator.createEmptyPath("/ctes/systems/systemId/notifications/SmsByEmail")
    curator.createEmptyPath("/ctes/systems/systemId/corridors/corridorId")

    //    zkClient.get.createPersistent("/ctes/configuration/notification", true)
    //    zkClient.get.createPersistent("/ctes/configuration/notification/notifiers/SmsByEmail", true)
    //    zkClient.get.createPersistent("/ctes/systems", true)
    //    zkClient.get.createPersistent("/ctes/systems/systemId", true)
    //    zkClient.get.createPersistent("/ctes/systems/systemId/notifications/SmsByEmail", true)
    //    zkClient.get.createPersistent("/ctes/systems/systemId/corridors/corridorId", true)

    Mailbox.clearAll()

    eventCountBefore = getNumberOfEvents
    testActorRef = TestActorRef(new SmsByEmailNotifier(systemId = systemId, curator = curator, notifierId = "SmsByEmail"))
  }

  override def afterEach() {
    testActorRef.stop()
    //curator.deleteRecursive("/ctes")
    super.afterEach()
  }

  val openEvent = new Event { val timestamp = 1362122402051L; val eventType = "Open" }
  val outOfRangeEvent = new Event { val timestamp = 1362122402051L; val eventType = "OutOfRange" }
  val humidityOutOfRangeEvent = new Event { val timestamp = 1362122402051L; val eventType = "HumidityOutOfRange" }
  val leaningEvent = new Event { val timestamp = 1362122402051L; val eventType = "Leaning" }

  val unknownEvent = new Event { val timestamp = 1362122402051L; val eventType = "Unknown" }

  val bodyTemplate = "Molest melding voor flitspaal <hhm-id>:\nReden: <reason>\nLocatie: <location>\nTijd: <date-time>"

  def createSmsByEmailNotifierConfig: SmsByEmailNotifierConfig = {
    new SmsByEmailNotifierConfig(
      combineToOneEmail = false,
      emailSubject = Some("emailSubject"),
      emailBodyTemplate = bodyTemplate,
      fromEmailAddress = "traffic@csc.com",
      smsGatewayDomain = "gateway.com",
      smtpHost = "localhost",
      smtpPort = None,
      smtpUserName = None,
      smtpPassword = None,
      smtpProperties = Some(Map()))
  }

  def createCorridorInfoWith1LocationLine = new ZkCorridorInfo(
    corridorId = 1,
    locationCode = "22",
    reportId = "TCS A2 Links  39.7 - 54.9",
    reportHtId = "A020011",
    reportPerformance = true,
    locationLine1 = "Trafficlaan 254, linkerzijde",
    locationLine2 = None)

  def createCorridorInfoWith2LocationLines = new ZkCorridorInfo(
    corridorId = 1,
    locationCode = "22",
    reportId = "TCS A2 Links  39.7 - 54.9",
    reportHtId = "A020011",
    reportPerformance = true,
    locationLine1 = "Kruispunt N265 - Flitsweg",
    locationLine2 = Some("Noordelijke richting"))

  def createSystemConfig = new ZkSystem(
    id = "testSystemId",
    name = "testName",
    title = "testName",
    mtmRouteId = "testMtmRouteId",
    violationPrefixId = "A123",
    location = ZkSystemLocation(
      description = "testDescription",
      viewingDirection = Some(""),
      region = "testRegion",
      roadNumber = "testRoadNumber",
      roadPart = "testRoadPart",
      systemLocation = "Amsterdam"),
    serialNumber = SerialNumber("testSerialNumber"),
    orderNumber = 13,
    maxSpeed = 15,
    compressionFactor = 17,
    retentionTimes = ZkSystemRetentionTimes(
      nrDaysKeepTrafficData = 18,
      nrDaysKeepViolations = 19,
      nrDaysRemindCaseFiles = 3),
    pardonTimes = ZkSystemPardonTimes(pardonTimeEsa_secs = 200, pardonTimeTechnical_secs = 201),
    esaProviderType = EsaProviderType.NoProvider,
    minimumViolationTimeSeparation = 0)

  def createCorridorConfig(corridorInfo: ZkCorridorInfo) = new ZkCorridor(
    id = "corridorId",
    name = "corridorName",
    roadType = 3,
    serviceType = ServiceType.RedLight,
    caseFileType = ZkCaseFileType.HHMVS40,
    isInsideUrbanArea = true,
    dynamaxMqId = "dynamaxMqId",
    approvedSpeeds = Seq(30, 40, 50, 60, 70, 80),
    pshtm = new ZkPSHTMIdentification(
      useYear = true,
      useMonth = true,
      useDay = true,
      useReportingOfficerId = true,
      useNumberOfTheDay = true,
      useLocationCode = true,
      useHHMCode = true,
      useTypeViolation = true,
      useFixedCharacter = true,
      fixedCharacter = "#"),
    info = corridorInfo,
    startSectionId = "startSectionId",
    endSectionId = "endSectionId",
    allSectionIds = Seq("sectionId1, sectionId2"),
    direction = ZkDirection(directionFrom = "testDrivingDirectionFrom",
      directionTo = "testDrivingDirectionTo"),
    radarCode = 14,
    roadCode = 20,
    dutyType = "",
    deploymentCode = "",
    codeText = "")

  def createSystemSmsByEmailNotifierConfig(phoneNumbers: Seq[String]): SystemSmsByEmailNotifierConfig = {
    new SystemSmsByEmailNotifierConfig(phoneNumbers)
  }

  "The SmsByEmailNotifier actor" when {

    "receiving an Open event and configured for one phonenumer" must {
      "send one email to the sms-gateway, containing an Open notification, and send a Send SystemEvent" in {
        // Prepare config
        setSmsByEmailConfig(createSmsByEmailNotifierConfig)
        setSystemSmsByEmailNotifierConfig(createSystemSmsByEmailNotifierConfig(Seq("0611223344")))
        setSystemConfig(createSystemConfig)
        setSystemCorridorConfig(createCorridorConfig(createCorridorInfoWith2LocationLines))

        // Send message to SmsByEmailNotifier
        testActorRef ! openEvent
        // Check expected results
        checkNotificationEmail(Seq("0611223344"), openEvent)
        checkSystemEventsCreated(1, "Send")
      }
    }

    "receiving an OutOfRange event and configured for one phonenumer" must {
      "send one email to the sms-gateway, containing an OutOfRange notification, and send a Send SystemEvent" in {
        // Prepare config
        setSmsByEmailConfig(createSmsByEmailNotifierConfig)
        setSystemSmsByEmailNotifierConfig(createSystemSmsByEmailNotifierConfig(Seq("0611223344")))
        setSystemConfig(createSystemConfig)
        setSystemCorridorConfig(createCorridorConfig(createCorridorInfoWith2LocationLines))

        // Send message to SmsByEmailNotifier
        testActorRef ! outOfRangeEvent
        // Check expected results
        checkNotificationEmail(Seq("0611223344"), outOfRangeEvent)
        checkSystemEventsCreated(1, "Send")
      }
    }

    "receiving an HumidityOutOfRange event and configured for one phonenumer" must {
      "send one email to the sms-gateway, containing an HumidityOutOfRange notification, and send a Send SystemEvent" in {
        // Prepare config
        setSmsByEmailConfig(createSmsByEmailNotifierConfig)
        setSystemSmsByEmailNotifierConfig(createSystemSmsByEmailNotifierConfig(Seq("0611223344")))
        setSystemConfig(createSystemConfig)
        setSystemCorridorConfig(createCorridorConfig(createCorridorInfoWith2LocationLines))

        // Send message to SmsByEmailNotifier
        testActorRef ! humidityOutOfRangeEvent
        // Check expected results
        checkNotificationEmail(Seq("0611223344"), humidityOutOfRangeEvent)
        checkSystemEventsCreated(1, "Send")
      }
    }

    "receiving an Leaning event and configured for one phonenumer" must {
      "send one email to the sms-gateway, containing an Leaning notification, and send a Send SystemEvent" in {
        // Prepare config
        setSmsByEmailConfig(createSmsByEmailNotifierConfig)
        setSystemSmsByEmailNotifierConfig(createSystemSmsByEmailNotifierConfig(Seq("0611223344")))
        setSystemConfig(createSystemConfig)
        setSystemCorridorConfig(createCorridorConfig(createCorridorInfoWith2LocationLines))

        // Send message to SmsByEmailNotifier
        testActorRef ! leaningEvent
        // Check expected results
        checkNotificationEmail(Seq("0611223344"), leaningEvent)
        checkSystemEventsCreated(1, "Send")
      }
    }

    "receiving an unknown event and configured for one phonenumer" must {
      "send one email to the sms-gateway (with the eventType as reason for the notification),  and send a Send SystemEvent" in {
        // Prepare config
        setSmsByEmailConfig(createSmsByEmailNotifierConfig)
        setSystemSmsByEmailNotifierConfig(createSystemSmsByEmailNotifierConfig(Seq("0611223344")))
        setSystemConfig(createSystemConfig)
        setSystemCorridorConfig(createCorridorConfig(createCorridorInfoWith2LocationLines))

        // Send message to SmsByEmailNotifier
        testActorRef ! unknownEvent
        // Check expected results
        checkNotificationEmail(Seq("0611223344"), unknownEvent)
        checkSystemEventsCreated(1, "Send")
      }
    }

    "receiving an Open event and configured for three phonenumers" must {
      "send 3 emails to the sms-gateway, containing an Open notification, and send 3 Send SystemEvent" in {
        // Prepare config
        val phoneNumbers = Seq("0611223344", "0622334455", "0633445566")
        setSmsByEmailConfig(createSmsByEmailNotifierConfig)
        setSystemSmsByEmailNotifierConfig(createSystemSmsByEmailNotifierConfig(phoneNumbers))
        setSystemConfig(createSystemConfig)
        setSystemCorridorConfig(createCorridorConfig(createCorridorInfoWith2LocationLines))

        // Send message to SmsByEmailNotifier
        testActorRef ! openEvent
        // Check expected results
        phoneNumbers.foreach(phoneNumbers ⇒ checkNotificationEmail(Seq(phoneNumbers), openEvent))
        checkSystemEventsCreated(3, "Send")
      }
      "send 1 email to 3 recipients using the same sms-gateway, containing an Open notification, and send 1 Send SystemEvent" in {
        // Prepare config
        val phoneNumbers = Seq("0611223344", "0622334455", "0633445566")
        setSmsByEmailConfig(createSmsByEmailNotifierConfig.copy(combineToOneEmail = true))
        setSystemSmsByEmailNotifierConfig(createSystemSmsByEmailNotifierConfig(phoneNumbers))
        setSystemConfig(createSystemConfig)
        setSystemCorridorConfig(createCorridorConfig(createCorridorInfoWith2LocationLines))

        // Send message to SmsByEmailNotifier
        testActorRef ! openEvent
        // Check expected results
        checkNotificationEmail(phoneNumbers, openEvent)
        checkSystemEventsCreated(1, "Send")
      }
    }

    "receiving an OutOfRange event and configured for three phonenumers" must {
      "send 3 emails to the sms-gateway, containing an OutOfRange notification, and send 3 Send SystemEvent" in {
        // Prepare config
        val phoneNumbers = Seq("0611223344", "0622334455", "0633445566")
        setSmsByEmailConfig(createSmsByEmailNotifierConfig)
        setSystemSmsByEmailNotifierConfig(createSystemSmsByEmailNotifierConfig(phoneNumbers))
        setSystemConfig(createSystemConfig)
        setSystemCorridorConfig(createCorridorConfig(createCorridorInfoWith2LocationLines))

        // Send message to SmsByEmailNotifier
        testActorRef ! outOfRangeEvent
        // Check expected results
        phoneNumbers.foreach(phoneNumbers ⇒ checkNotificationEmail(Seq(phoneNumbers), outOfRangeEvent))
        checkSystemEventsCreated(3, "Send")
      }
      "send 1 email to 3 recipients using the same sms-gateway, containing an OutOfRange notification, and send 1 Send SystemEvent" in {
        // Prepare config
        val phoneNumbers = Seq("0611223344", "0622334455", "0633445566")
        setSmsByEmailConfig(createSmsByEmailNotifierConfig.copy(combineToOneEmail = true))
        setSystemSmsByEmailNotifierConfig(createSystemSmsByEmailNotifierConfig(phoneNumbers))
        setSystemConfig(createSystemConfig)
        setSystemCorridorConfig(createCorridorConfig(createCorridorInfoWith2LocationLines))

        // Send message to SmsByEmailNotifier
        testActorRef ! outOfRangeEvent
        // Check expected results
        checkNotificationEmail(phoneNumbers, outOfRangeEvent)
        checkSystemEventsCreated(1, "Send")
      }
    }

    "receiving an HumidityOutOfRange event and configured for three phonenumers" must {
      "send 3 emails to the sms-gateway, containing an HumidityOutOfRange notification, and send 3 Send SystemEvent" in {
        // Prepare config
        val phoneNumbers = Seq("0611223344", "0622334455", "0633445566")
        setSmsByEmailConfig(createSmsByEmailNotifierConfig)
        setSystemSmsByEmailNotifierConfig(createSystemSmsByEmailNotifierConfig(phoneNumbers))
        setSystemConfig(createSystemConfig)
        setSystemCorridorConfig(createCorridorConfig(createCorridorInfoWith2LocationLines))

        // Send message to SmsByEmailNotifier
        testActorRef ! humidityOutOfRangeEvent
        // Check expected results
        phoneNumbers.foreach(phoneNumbers ⇒ checkNotificationEmail(Seq(phoneNumbers), humidityOutOfRangeEvent))
        checkSystemEventsCreated(3, "Send")
      }
      "send 1 email to 3 recipients using the same sms-gateway, containing an HumidityOutOfRange notification, and send 1 Send SystemEvent" in {
        // Prepare config
        val phoneNumbers = Seq("0611223344", "0622334455", "0633445566")
        setSmsByEmailConfig(createSmsByEmailNotifierConfig.copy(combineToOneEmail = true))
        setSystemSmsByEmailNotifierConfig(createSystemSmsByEmailNotifierConfig(phoneNumbers))
        setSystemConfig(createSystemConfig)
        setSystemCorridorConfig(createCorridorConfig(createCorridorInfoWith2LocationLines))

        // Send message to SmsByEmailNotifier
        testActorRef ! humidityOutOfRangeEvent
        // Check expected results
        checkNotificationEmail(phoneNumbers, humidityOutOfRangeEvent)
        checkSystemEventsCreated(1, "Send")
      }
    }

    "receiving an Leaning event and configured for three phonenumers" must {
      "send 3 emails to the sms-gateway, containing an Leaning notification, and send 3 Send SystemEvent" in {
        // Prepare config
        val phoneNumbers = Seq("0611223344", "0622334455", "0633445566")
        setSmsByEmailConfig(createSmsByEmailNotifierConfig)
        setSystemSmsByEmailNotifierConfig(createSystemSmsByEmailNotifierConfig(phoneNumbers))
        setSystemConfig(createSystemConfig)
        setSystemCorridorConfig(createCorridorConfig(createCorridorInfoWith2LocationLines))

        // Send message to SmsByEmailNotifier
        testActorRef ! leaningEvent
        // Check expected results
        phoneNumbers.foreach(phoneNumbers ⇒ checkNotificationEmail(Seq(phoneNumbers), leaningEvent))
        checkSystemEventsCreated(3, "Send")
      }
      "send 1 email to 3 recipients using the same sms-gateway, containing an Leaning notification, and send 1 Send SystemEvent" in {
        // Prepare config
        val phoneNumbers = Seq("0611223344", "0622334455", "0633445566")
        setSmsByEmailConfig(createSmsByEmailNotifierConfig.copy(combineToOneEmail = true))
        setSystemSmsByEmailNotifierConfig(createSystemSmsByEmailNotifierConfig(phoneNumbers))
        setSystemConfig(createSystemConfig)
        setSystemCorridorConfig(createCorridorConfig(createCorridorInfoWith2LocationLines))

        // Send message to SmsByEmailNotifier
        testActorRef ! leaningEvent
        // Check expected results
        checkNotificationEmail(phoneNumbers, leaningEvent)
        checkSystemEventsCreated(1, "Send")
      }
    }

    "sending emails fails" must {
      "send one Failed SystemEvent" in {
        // Prepare config
        setSmsByEmailConfig(createSmsByEmailNotifierConfig)
        setSystemSmsByEmailNotifierConfig(createSystemSmsByEmailNotifierConfig(Seq("@878g@87t@")))
        setSystemConfig(createSystemConfig)
        setSystemCorridorConfig(createCorridorConfig(createCorridorInfoWith2LocationLines))

        // Send message to SmsByEmailNotifier
        testActorRef ! openEvent
        // Check expected results
        checkSystemEventsCreated(1, "Failed")
      }
    }

  }

  def checkNotificationEmail(phoneNumbers: Seq[String], event: Event) {
    val expectedToAddresses = phoneNumbers.map(_ + "@gateway.com")
    expectedToAddresses.foreach {
      expectedToAddress ⇒
        val emailData = new EmailData(
          subject = "emailSubject",
          bodyText = getBodyText(event),
          to = expectedToAddresses,
          cc = None,
          bcc = None,
          from = Some("traffic@csc.com"))
        val inbox = Mailbox.get(expectedToAddress)
        val email = getEmail(inbox.get(0))
        checkEmail(email, emailData)
    }
  }

  def getBodyText(event: Event): String = event.eventType match {
    case "Open"               ⇒ "Molest melding voor flitspaal A123:\nReden: Geopende deur\nLocatie: Kruispunt N265 - Flitsweg, Noordelijke richting\nTijd: 01-03-2013 08:20:02 CET"
    case "OutOfRange"         ⇒ "Molest melding voor flitspaal A123:\nReden: Temperatuur buiten bereik\nLocatie: Kruispunt N265 - Flitsweg, Noordelijke richting\nTijd: 01-03-2013 08:20:02 CET"
    case "HumidityOutOfRange" ⇒ "Molest melding voor flitspaal A123:\nReden: Relatieve vochtigheid te hoog\nLocatie: Kruispunt N265 - Flitsweg, Noordelijke richting\nTijd: 01-03-2013 08:20:02 CET"
    case "Leaning"            ⇒ "Molest melding voor flitspaal A123:\nReden: Scheefstand\nLocatie: Kruispunt N265 - Flitsweg, Noordelijke richting\nTijd: 01-03-2013 08:20:02 CET"
    case "Unknown"            ⇒ "Molest melding voor flitspaal A123:\nReden: Unknown\nLocatie: Kruispunt N265 - Flitsweg, Noordelijke richting\nTijd: 01-03-2013 08:20:02 CET"
  }

  def setSmsByEmailConfig(smsByEmailConfig: SmsByEmailNotifierConfig) {
    try {
      curator.put(Path("/ctes/configuration/notification/notifiers/SmsByEmail/config"), smsByEmailConfig)
    } catch {
      case e: Exception ⇒ fail("Error while storing SmsByEmailNotifierConfig in ZooKeeper")
    }
  }

  def setSystemSmsByEmailNotifierConfig(systemSmsByEmailNotifierConfig: SystemSmsByEmailNotifierConfig) {
    try {
      curator.put(Path("/ctes/systems/systemId/notifications/SmsByEmail/config"), systemSmsByEmailNotifierConfig)
    } catch {
      case e: Exception ⇒ fail("Error while storing SystemSmsByEmailNotifierConfig in ZooKeeper")
    }
  }

  def setSystemConfig(systemConfig: ZkSystem) {
    try {
      curator.put(Path("/ctes/systems/systemId/config"), systemConfig)
    } catch {
      case e: Exception ⇒ fail("Error while storing ZkSystem in ZooKeeper")
    }
  }

  def setSystemCorridorConfig(corridorConfig: ZkCorridor) {
    try {
      curator.put(Path("/ctes/systems/systemId/corridors/corridorId/config"), corridorConfig)
    } catch {
      case e: Exception ⇒ fail("Error while storing ZkCorridor in ZooKeeper")
    }
  }

  val pathToSystem = Paths.Systems.getSystemPath(systemId)
  val pathToEvents = Paths.Systems.getLogEventsPath(systemId)

  def getNumberOfEvents: Int = {
    if (curator.exists(pathToSystem) && !curator.exists(pathToEvents))
      0
    else {
      curator.getChildNames(pathToEvents).size
    }
  }

  def getEvent(name: String): Option[SystemEvent] = {
    curator.get[SystemEvent](Path(pathToEvents) / name)
  }

  def checkSystemEventsCreated(createdCount: Int, eventType: String) {
    getNumberOfEvents must be(eventCountBefore + createdCount)
    val eventNames = curator.getChildNames(pathToEvents).toArray
    (1 to createdCount).foreach { i ⇒
      getEvent(eventNames(getNumberOfEvents - i)) match {
        case Some(event) ⇒
          event.componentId must be("SmsByEmailNotifier")
          event.systemId must be(systemId)
          event.eventType must be("Info")
          eventType match {
            case "Send"   ⇒ event.reason.startsWith("Molest sms verstuurd naar") must be(true)
            case "Failed" ⇒ event.reason.startsWith("Versturen molest sms naar ") must be(true)
          }

        case None ⇒
          fail("No event found")
      }
    }
  }

}
