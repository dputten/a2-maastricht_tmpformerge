/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.notification.email

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import javax.mail.internet.InternetAddress
import javax.mail.{ Session, Part, Multipart, Message }
import org.apache.commons.io.IOUtils
import org.jvnet.mock_javamail.Mailbox
import java.util.Properties

/**
 * Test for EmailSender trait
 */
class EmailSenderTest extends WordSpec with MustMatchers with EmailSender with TestUtils {

  "An EmailSender" when {
    "sendEmail is called with one to-address" must {
      val toAddress = "to@testsystem.com"
      val fromAddress = "sender@sendingsystem.com"
      val emailData = new EmailData(
        subject = "test mail",
        bodyText = "This is a test mail.",
        to = Seq(toAddress),
        cc = None,
        bcc = None,
        from = Some(fromAddress))
      val mailServerData = new MailServerData(
        host = "localhost",
        port = None,
        requiresAuthentication = false,
        username = None,
        password = None,
        properties = Some(Map()))

      "send the email to the to-address" in {
        Mailbox.clearAll()

        sendEmail(emailData, mailServerData) match {
          case Left(exception) ⇒ fail(exception)
          case Right(_) ⇒
            val inbox = Mailbox.get(toAddress)
            val email = getEmail(inbox.get(0))
            checkEmail(email, emailData)
        }
      }
    }

    "sendEmail is called with multiple to-addresses" must {
      val toAddresses = Seq("to1@testsystem.com", "to2@testsystem.com", "to3@testsystem.com")
      val fromAddress = "sender@sendingsystem.com"
      val emailData = new EmailData(
        subject = "test mail",
        bodyText = "This is a test mail.",
        to = toAddresses,
        cc = None,
        bcc = None,
        from = Some(fromAddress))
      val mailServerData = new MailServerData(
        host = "localhost",
        port = None,
        requiresAuthentication = true,
        username = None,
        password = None,
        properties = Some(Map()))
      "send the email to each to-address" in {
        Mailbox.clearAll()

        sendEmail(emailData, mailServerData) match {
          case Left(exception) ⇒ fail(exception)
          case Right(_)        ⇒ checkEmailForAddresses(toAddresses, emailData)
        }
      }
    }

    "sendEmail is called with multiple to- and cc-addresses" must {
      val toAddresses = Seq("to1@testsystem.com", "to2@testsystem.com", "to3@testsystem.com")
      val ccAddresses = Seq("ccer1@testsystem.com", "ccer2@testsystem.com", "ccer3@testsystem.com")
      val fromAddress = "sender@sendingsystem.com"
      val emailData = new EmailData(
        subject = "test mail",
        bodyText = "This is a test mail.",
        to = toAddresses,
        cc = Some(ccAddresses),
        bcc = None,
        from = Some(fromAddress))
      val mailServerData = new MailServerData(
        host = "localhost",
        port = None,
        requiresAuthentication = false,
        username = None,
        password = None,
        properties = Some(Map()))
      "send the email to each to-address and each cc-address" in {
        Mailbox.clearAll()

        sendEmail(emailData, mailServerData) match {
          case Left(exception) ⇒ fail(exception)
          case Right(_) ⇒
            checkEmailForAddresses(toAddresses, emailData)
            checkEmailForAddresses(ccAddresses, emailData)
        }
      }
    }

    "sendEmail is called with multiple to-, cc- and bcc-addresses" must {
      val toAddresses = Seq("to1@testsystem.com", "to2@testsystem.com", "to3@testsystem.com")
      val ccAddresses = Seq("ccer1@testsystem.com", "ccer2@testsystem.com", "ccer3@testsystem.com")
      val bccAddresses = Seq("bccer1@testsystem.com", "bccer2@testsystem.com", "bccer3@testsystem.com")
      val fromAddress = "sender@sendingsystem.com"
      val emailData = new EmailData(
        subject = "test mail",
        bodyText = "This is a test mail.",
        to = toAddresses,
        cc = Some(ccAddresses),
        bcc = Some(bccAddresses),
        from = Some(fromAddress))
      val mailServerData = new MailServerData(
        host = "localhost",
        port = None,
        requiresAuthentication = true,
        username = None,
        password = None,
        properties = Some(Map()))
      "send the email to each to-address and each cc-address and each bcc-address" in {
        Mailbox.clearAll()

        sendEmail(emailData, mailServerData) match {
          case Left(exception) ⇒ fail(exception)
          case Right(_) ⇒
            checkEmailForAddresses(toAddresses, emailData)
            checkEmailForAddresses(ccAddresses, emailData)
            checkEmailForAddresses(bccAddresses, emailData)
        }
      }
    }

    "sendEmail is called without a from-address" must {
      val toAddress = "to@testsystem.com"
      val emailData = new EmailData(
        subject = "test mail",
        bodyText = "This is a test mail.",
        to = Seq(toAddress),
        cc = None,
        bcc = None,
        from = None)
      val mailServerData = new MailServerData(
        host = "localhost",
        port = None,
        requiresAuthentication = false,
        username = None,
        password = None,
        properties = Some(Map()))

      "send an email with the local address as from-address" in {
        Mailbox.clearAll()

        sendEmail(emailData, mailServerData) match {
          case Left(exception) ⇒ fail(exception)
          case Right(_) ⇒
            val inbox = Mailbox.get(toAddress)
            val email = getEmail(inbox.get(0))

            email.from.length must be(1)
            email.from(0) must be(InternetAddress.getLocalAddress(Session.getInstance(new Properties())))
        }
      }
    }

  }
}
