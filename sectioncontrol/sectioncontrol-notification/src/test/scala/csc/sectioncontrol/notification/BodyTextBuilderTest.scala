/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.notification

import org.scalatest.{ BeforeAndAfterEach, WordSpec }
import org.scalatest.matchers.MustMatchers
import csc.dlog.Event
import csc.sectioncontrol.storage._
import scala.Some
import csc.sectioncontrol.storage.ZkCorridorInfo
import scala.Some
import csc.sectioncontrol.storage.ZkPSHTMIdentification
import csc.sectioncontrol.storage.ZkSystem
import csc.sectioncontrol.storage.ZkCorridor
import csc.sectioncontrol.messages.EsaProviderType

/**
 * Test van  en BodyTextBuilder class.
 */
class BodyTextBuilderTest extends WordSpec with MustMatchers with BeforeAndAfterEach {
  val systemId = "systemId"
  val openEvent = new Event { val timestamp = 1362122402051L; val eventType = "Open" }
  val outOfRangeEvent = new Event { val timestamp = 1362122402051L; val eventType = "OutOfRange" }
  val humidityOutOfRangeEvent = new Event { val timestamp = 1362122402051L; val eventType = "HumidityOutOfRange" }
  val leaningEvent = new Event { val timestamp = 1362122402051L; val eventType = "Leaning" }

  val bodyTemplate = "Molest melding voor flitspaal <hhm-id>:\nReden: <reason>\nLocatie: <location>\nTijd: <date-time>"

  def createCorridorInfoWith1LocationLine = new ZkCorridorInfo(
    corridorId = 1,
    locationCode = "22",
    reportId = "TCS A2 Links  39.7 - 54.9",
    reportHtId = "A020011",
    reportPerformance = true,
    locationLine1 = "Trafficlaan 254, linkerzijde",
    locationLine2 = None)

  def createCorridorInfoWith2LocationLines = new ZkCorridorInfo(
    corridorId = 1,
    locationCode = "22",
    reportId = "TCS A2 Links  39.7 - 54.9",
    reportHtId = "A020011",
    reportPerformance = true,
    locationLine1 = "Kruispunt N265 - Flitsweg",
    locationLine2 = Some("Noordelijke richting"))

  def createSystemConfig = new ZkSystem(
    id = "testSystemId",
    name = "testName",
    title = "testTitle",
    mtmRouteId = "testMtmRouteId",
    violationPrefixId = "A123",
    location = ZkSystemLocation(
      description = "testDescription",
      viewingDirection = Some(""),
      region = "testRegion",
      roadNumber = "testRoadNumber",
      roadPart = "testRoadPart",
      systemLocation = "Amsterdam"),
    serialNumber = SerialNumber("testSerialNumber"),
    orderNumber = 13,
    maxSpeed = 15,
    compressionFactor = 17,
    retentionTimes = ZkSystemRetentionTimes(
      nrDaysKeepTrafficData = 18,
      nrDaysKeepViolations = 19,
      nrDaysRemindCaseFiles = 3),
    pardonTimes = ZkSystemPardonTimes(pardonTimeEsa_secs = 200, pardonTimeTechnical_secs = 201),
    esaProviderType = EsaProviderType.NoProvider,
    minimumViolationTimeSeparation = 0)

  def createCorridorConfig(corridorInfo: ZkCorridorInfo) = new ZkCorridor(
    id = "corridorId",
    name = "corridorName",
    roadType = 3,
    serviceType = ServiceType.RedLight,
    caseFileType = ZkCaseFileType.HHMVS40,
    isInsideUrbanArea = true,
    dynamaxMqId = "dynamaxMqId",
    approvedSpeeds = Seq(30, 40, 50, 60, 70, 80),
    pshtm = new ZkPSHTMIdentification(
      useYear = true,
      useMonth = true,
      useDay = true,
      useReportingOfficerId = true,
      useNumberOfTheDay = true,
      useLocationCode = true,
      useHHMCode = true,
      useTypeViolation = true,
      useFixedCharacter = true,
      fixedCharacter = "#"),
    info = corridorInfo,
    startSectionId = "startSectionId",
    endSectionId = "endSectionId",
    allSectionIds = Seq("sectionId1, sectionId2"),
    direction = ZkDirection(directionFrom = "testDrivingDirectionFrom",
      directionTo = "testDrivingDirectionTo"),
    radarCode = 14,
    roadCode = 20,
    dutyType = "",
    deploymentCode = "",
    codeText = "")

  "A BodyTextBuilderTest object" when {
    "configured with a single locationLine" must {
      "return a correct bodyText for an Open event" in {
        val testBodyTextBuilder = new BodyTextBuilder(
          systemId = systemId,
          event = openEvent,
          template = bodyTemplate,
          systemConfig = Some(createSystemConfig),
          corridorConfig = Some(createCorridorConfig(createCorridorInfoWith1LocationLine)))
        val expectedBodyText = "Molest melding voor flitspaal A123:\nReden: Geopende deur\nLocatie: Trafficlaan 254, linkerzijde\nTijd: 01-03-2013 08:20:02 CET"
        testBodyTextBuilder.getBodyText must be(expectedBodyText)
      }
      "return a correct bodyText for an OutOfRange event" in {
        val testBodyTextBuilder = new BodyTextBuilder(
          systemId = systemId,
          event = outOfRangeEvent,
          template = bodyTemplate,
          systemConfig = Some(createSystemConfig),
          corridorConfig = Some(createCorridorConfig(createCorridorInfoWith1LocationLine)))
        val expectedBodyText = "Molest melding voor flitspaal A123:\nReden: Temperatuur buiten bereik\nLocatie: Trafficlaan 254, linkerzijde\nTijd: 01-03-2013 08:20:02 CET"
        testBodyTextBuilder.getBodyText must be(expectedBodyText)
      }
      "return a correct bodyText for a HumidityOutOfRange event" in {
        val testBodyTextBuilder = new BodyTextBuilder(
          systemId = systemId,
          event = humidityOutOfRangeEvent,
          template = bodyTemplate,
          systemConfig = Some(createSystemConfig),
          corridorConfig = Some(createCorridorConfig(createCorridorInfoWith1LocationLine)))
        val expectedBodyText = "Molest melding voor flitspaal A123:\nReden: Relatieve vochtigheid te hoog\nLocatie: Trafficlaan 254, linkerzijde\nTijd: 01-03-2013 08:20:02 CET"
        testBodyTextBuilder.getBodyText must be(expectedBodyText)
      }
      "return a correct bodyText for a Leaning event" in {
        val testBodyTextBuilder = new BodyTextBuilder(
          systemId = systemId,
          event = leaningEvent,
          template = bodyTemplate,
          systemConfig = Some(createSystemConfig),
          corridorConfig = Some(createCorridorConfig(createCorridorInfoWith1LocationLine)))
        val expectedBodyText = "Molest melding voor flitspaal A123:\nReden: Scheefstand\nLocatie: Trafficlaan 254, linkerzijde\nTijd: 01-03-2013 08:20:02 CET"
        testBodyTextBuilder.getBodyText must be(expectedBodyText)
      }
    }

    "configured with two locationLines" must {
      "return a correct bodyText for an Open event" in {
        val testBodyTextBuilder = new BodyTextBuilder(
          systemId = systemId,
          event = openEvent,
          template = bodyTemplate,
          systemConfig = Some(createSystemConfig),
          corridorConfig = Some(createCorridorConfig(createCorridorInfoWith2LocationLines)))
        val expectedBodyText = "Molest melding voor flitspaal A123:\nReden: Geopende deur\nLocatie: Kruispunt N265 - Flitsweg, Noordelijke richting\nTijd: 01-03-2013 08:20:02 CET"
        testBodyTextBuilder.getBodyText must be(expectedBodyText)
      }
      "return a correct bodyText for an OutOfRange event" in {
        val testBodyTextBuilder = new BodyTextBuilder(
          systemId = systemId,
          event = outOfRangeEvent,
          template = bodyTemplate,
          systemConfig = Some(createSystemConfig),
          corridorConfig = Some(createCorridorConfig(createCorridorInfoWith2LocationLines)))
        val expectedBodyText = "Molest melding voor flitspaal A123:\nReden: Temperatuur buiten bereik\nLocatie: Kruispunt N265 - Flitsweg, Noordelijke richting\nTijd: 01-03-2013 08:20:02 CET"
        testBodyTextBuilder.getBodyText must be(expectedBodyText)
      }
      "return a correct bodyText for a HumidityOutOfRange event" in {
        val testBodyTextBuilder = new BodyTextBuilder(
          systemId = systemId,
          event = humidityOutOfRangeEvent,
          template = bodyTemplate,
          systemConfig = Some(createSystemConfig),
          corridorConfig = Some(createCorridorConfig(createCorridorInfoWith2LocationLines)))
        val expectedBodyText = "Molest melding voor flitspaal A123:\nReden: Relatieve vochtigheid te hoog\nLocatie: Kruispunt N265 - Flitsweg, Noordelijke richting\nTijd: 01-03-2013 08:20:02 CET"
        testBodyTextBuilder.getBodyText must be(expectedBodyText)
      }
      "return a correct bodyText for a Leaning event" in {
        val testBodyTextBuilder = new BodyTextBuilder(
          systemId = systemId,
          event = leaningEvent,
          template = bodyTemplate,
          systemConfig = Some(createSystemConfig),
          corridorConfig = Some(createCorridorConfig(createCorridorInfoWith2LocationLines)))
        val expectedBodyText = "Molest melding voor flitspaal A123:\nReden: Scheefstand\nLocatie: Kruispunt N265 - Flitsweg, Noordelijke richting\nTijd: 01-03-2013 08:20:02 CET"
        testBodyTextBuilder.getBodyText must be(expectedBodyText)
      }
    }

    "missing a SystemConfiguration" must {
      "return a correct bodyText for an Open event" in {
        val testBodyTextBuilder = new BodyTextBuilder(
          systemId = systemId,
          event = openEvent,
          template = bodyTemplate,
          systemConfig = None,
          corridorConfig = Some(createCorridorConfig(createCorridorInfoWith1LocationLine)))
        val expectedBodyText = "Molest melding voor flitspaal systemId:\nReden: Geopende deur\nLocatie: Trafficlaan 254, linkerzijde\nTijd: 01-03-2013 08:20:02 CET"
        testBodyTextBuilder.getBodyText must be(expectedBodyText)
      }
      "return a correct bodyText for an OutOfRange event" in {
        val testBodyTextBuilder = new BodyTextBuilder(
          systemId = systemId,
          event = outOfRangeEvent,
          template = bodyTemplate,
          systemConfig = None,
          corridorConfig = Some(createCorridorConfig(createCorridorInfoWith1LocationLine)))
        val expectedBodyText = "Molest melding voor flitspaal systemId:\nReden: Temperatuur buiten bereik\nLocatie: Trafficlaan 254, linkerzijde\nTijd: 01-03-2013 08:20:02 CET"
        testBodyTextBuilder.getBodyText must be(expectedBodyText)
      }
      "return a correct bodyText for a HumidityOutOfRange event" in {
        val testBodyTextBuilder = new BodyTextBuilder(
          systemId = systemId,
          event = humidityOutOfRangeEvent,
          template = bodyTemplate,
          systemConfig = None,
          corridorConfig = Some(createCorridorConfig(createCorridorInfoWith1LocationLine)))
        val expectedBodyText = "Molest melding voor flitspaal systemId:\nReden: Relatieve vochtigheid te hoog\nLocatie: Trafficlaan 254, linkerzijde\nTijd: 01-03-2013 08:20:02 CET"
        testBodyTextBuilder.getBodyText must be(expectedBodyText)
      }
      "return a correct bodyText for a Leaning event" in {
        val testBodyTextBuilder = new BodyTextBuilder(
          systemId = systemId,
          event = leaningEvent,
          template = bodyTemplate,
          systemConfig = None,
          corridorConfig = Some(createCorridorConfig(createCorridorInfoWith1LocationLine)))
        val expectedBodyText = "Molest melding voor flitspaal systemId:\nReden: Scheefstand\nLocatie: Trafficlaan 254, linkerzijde\nTijd: 01-03-2013 08:20:02 CET"
        testBodyTextBuilder.getBodyText must be(expectedBodyText)
      }
    }

    "missing a CorridorConfiguration" must {
      "return a correct bodyText for an Open event" in {
        val testBodyTextBuilder = new BodyTextBuilder(
          systemId = systemId,
          event = openEvent,
          template = bodyTemplate,
          systemConfig = Some(createSystemConfig),
          corridorConfig = None)
        val expectedBodyText = "Molest melding voor flitspaal A123:\nReden: Geopende deur\nLocatie: onbekend\nTijd: 01-03-2013 08:20:02 CET"
        testBodyTextBuilder.getBodyText must be(expectedBodyText)
      }
      "return a correct bodyText for an OutOfRange event" in {
        val testBodyTextBuilder = new BodyTextBuilder(
          systemId = systemId,
          event = outOfRangeEvent,
          template = bodyTemplate,
          systemConfig = Some(createSystemConfig),
          corridorConfig = None)
        val expectedBodyText = "Molest melding voor flitspaal A123:\nReden: Temperatuur buiten bereik\nLocatie: onbekend\nTijd: 01-03-2013 08:20:02 CET"
        testBodyTextBuilder.getBodyText must be(expectedBodyText)
      }
      "return a correct bodyText for a HumidityOutOfRange event" in {
        val testBodyTextBuilder = new BodyTextBuilder(
          systemId = systemId,
          event = humidityOutOfRangeEvent,
          template = bodyTemplate,
          systemConfig = Some(createSystemConfig),
          corridorConfig = None)
        val expectedBodyText = "Molest melding voor flitspaal A123:\nReden: Relatieve vochtigheid te hoog\nLocatie: onbekend\nTijd: 01-03-2013 08:20:02 CET"
        testBodyTextBuilder.getBodyText must be(expectedBodyText)
      }
      "return a correct bodyText for a Leaning event" in {
        val testBodyTextBuilder = new BodyTextBuilder(
          systemId = systemId,
          event = leaningEvent,
          template = bodyTemplate,
          systemConfig = Some(createSystemConfig),
          corridorConfig = None)
        val expectedBodyText = "Molest melding voor flitspaal A123:\nReden: Scheefstand\nLocatie: onbekend\nTijd: 01-03-2013 08:20:02 CET"
        testBodyTextBuilder.getBodyText must be(expectedBodyText)
      }
    }
  }
}
