/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.notification.email

import javax.mail.internet.InternetAddress
import org.jvnet.mock_javamail.Mailbox
import javax.mail.{ Part, Multipart, Message, Session }
import java.util.Properties
import org.apache.commons.io.IOUtils
import org.scalatest.matchers.MustMatchers

/**
 * Some utility methods for testing.
 */
trait TestUtils { this: MustMatchers ⇒

  case class ReturnedEmailInfo(from: Seq[InternetAddress],
                               to: Seq[InternetAddress],
                               cc: Seq[InternetAddress],
                               bcc: Seq[InternetAddress],
                               subject: Option[String],
                               body: EmailBody)

  case class EmailBody(bodyText: Option[String] = None,
                       fileName: Option[String] = None,
                       contentType: Option[String] = None,
                       attachment: Option[Array[Byte]] = None)

  def asInternetAddress(value: String) = new InternetAddress(value)
  def asInternetAddresses(values: Seq[String]) = values.map(asInternetAddress(_))

  def checkEmailForAddresses(addresses: Seq[String], emailData: EmailData) {
    for (
      address ← addresses
    ) {
      val inbox = Mailbox.get(address)
      val email = getEmail(inbox.get(0))
      checkEmail(email, emailData)
    }
  }

  def checkEmail(email: ReturnedEmailInfo, emailData: EmailData) {
    def cc_in: Seq[InternetAddress] = emailData.cc match {
      case Some(addresses) ⇒ asInternetAddresses(addresses)
      case None            ⇒ Seq()
    }
    def bcc_in: Seq[InternetAddress] = emailData.bcc match {
      case Some(addresses) ⇒ asInternetAddresses(addresses)
      case None            ⇒ Seq()
    }
    def from_in: Seq[InternetAddress] = emailData.from match {
      case Some(address) ⇒ Seq(asInternetAddress(address))
      case None          ⇒ Seq(InternetAddress.getLocalAddress(Session.getInstance(new Properties())))
    }
    email.body.bodyText must be(Some(emailData.bodyText))
    email.subject must be(Some(emailData.subject))
    email.to must be(asInternetAddresses(emailData.to))
    email.cc must be(cc_in)
    email.bcc must be(bcc_in)
    email.from must be(from_in)
  }

  def getEmail(msg: Message): ReturnedEmailInfo = {
    val from = Option(msg.getFrom).map(_.asInstanceOf[Array[InternetAddress]].toList).getOrElse(Seq())
    val to = Option(msg.getRecipients(Message.RecipientType.TO)).map(_.asInstanceOf[Array[InternetAddress]].toList).getOrElse(Seq())
    val cc = Option(msg.getRecipients(Message.RecipientType.CC)).map(_.asInstanceOf[Array[InternetAddress]].toList).getOrElse(Seq())
    val bcc = Option(msg.getRecipients(Message.RecipientType.BCC)).map(_.asInstanceOf[Array[InternetAddress]].toList).getOrElse(Seq())
    val subject = Option(msg.getSubject).filter(_.nonEmpty)
    val body = getBody(msg)

    ReturnedEmailInfo(
      from = from,
      to = to,
      cc = cc,
      bcc = bcc,
      subject = subject,
      body = body)
  }

  private def getBody(msg: Message): EmailBody = {
    msg.getContent match {
      case multipart: Multipart ⇒ getMulitipartBody(multipart)
      case text: String         ⇒ new EmailBody(bodyText = Some(text))
    }
  }

  private def getMulitipartBody(multipart: Multipart): EmailBody = {
    var emailBody = EmailBody()
    for (i ← 0 until multipart.getCount) {
      val msgPart = multipart.getBodyPart(i)
      Option(msgPart.getDisposition) match {
        case Some(dp) if dp.equalsIgnoreCase(Part.ATTACHMENT) || dp.equalsIgnoreCase(Part.INLINE) ⇒
          emailBody = emailBody.copy(
            fileName = Option(msgPart.getFileName).filter(_.nonEmpty),
            contentType = Option(msgPart.getContentType).filter(_.nonEmpty),
            attachment = Option(IOUtils.toByteArray(msgPart.getInputStream)).map(_.toArray[Byte]).filter(_.nonEmpty))
        case None ⇒
          emailBody = emailBody.copy(bodyText = Option(msgPart.getContent.toString).filter(_.nonEmpty))
        case _ ⇒
      }
    }

    emailBody
  }

}
