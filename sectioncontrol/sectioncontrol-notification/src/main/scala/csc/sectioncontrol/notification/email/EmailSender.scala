/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.notification.email

import javax.mail._
import javax.mail.internet.{ MimeBodyPart, MimeMultipart, InternetAddress, MimeMessage }
import java.util.{ Properties, Date }
import scala.{ Array, Left, Right }

/**
 * Email data
 * @param subject subject for an email
 * @param bodyText body text for an email
 * @param to email addresses of receivers of this mail
 * @param cc email addresses of receivers of a cc of this mail
 * @param bcc email addresses of receivers of a bcc of this mail
 * @param from email addresses of sender
 */
case class EmailData(
  subject: String,
  bodyText: String = "",
  to: Seq[String],
  cc: Option[Seq[String]] = None,
  bcc: Option[Seq[String]] = None,
  from: Option[String])

/**
 * Mail server data
 * @param host host name of smtp server
 * @param port port used for smtp. (If ommitted, the default port, i.e. 25, will be used.)
 * @param requiresAuthentication indicate if smtp authentication is required
 * @param username used for smtp authentication if requiresAuthentication is true
 * @param password used for smtp authentication if requiresAuthentication is true
 * @param properties additional properties that can be set regarding email
 */
case class MailServerData(
  host: String,
  port: Option[Int] = None,
  requiresAuthentication: Boolean = false,
  username: Option[String] = None,
  password: Option[String] = None,
  properties: Option[Map[String, String]]) {

  def toProperties: Properties = {
    val resultProperties = new Properties
    def setProperty(key: String, value: String) = resultProperties.setProperty(key, value)

    setProperty("mail.smtp.host", host)
    if (port.isDefined) setProperty("mail.smtp.port", port.get.toString)
    setProperty("mail.smtp.auth", requiresAuthentication.toString)

    for (
      props ← properties;
      (key, value) ← props
    ) setProperty(key, value)

    resultProperties
  }
}

/**
 * Trait used for sending emails. (Based on EmailSender trait in csc.sectioncontrol.reports)
 *
 * Please Note: currently there is no support for attachments.
 */
trait EmailSender {
  def sendEmail(emailData: EmailData, mailServerData: MailServerData): Either[Throwable, Boolean] = {
    try {
      val properties = mailServerData.toProperties

      val auth: Authenticator = if (mailServerData.requiresAuthentication) {
        new Authenticator() {
          override def getPasswordAuthentication: PasswordAuthentication = {
            new PasswordAuthentication(mailServerData.username.getOrElse(""), mailServerData.password.getOrElse(""))
          }
        }
      } else {
        null
      }

      Transport.send(createMessage(emailData, properties, auth))

      Right(true)
    } catch {
      case e: Exception ⇒ Left(e)
    }
  }

  private def getAddressArray(addresses: Seq[String]): Array[Address] = {
    addresses.map(new InternetAddress(_)).toArray
  }

  private def createMessage(emailData: EmailData, properties: Properties, auth: Authenticator): MimeMessage = {
    val session = Session.getInstance(properties, auth)

    val message = new MimeMessage(session)
    // to
    message.setRecipients(Message.RecipientType.TO, getAddressArray(emailData.to))
    // cc
    for (
      addresses ← emailData.cc; if (!addresses.isEmpty)
    ) message.setRecipients(Message.RecipientType.CC, getAddressArray(addresses))
    // bcc
    for (
      addresses ← emailData.bcc; if (!addresses.isEmpty)
    ) message.setRecipients(Message.RecipientType.BCC, getAddressArray(addresses))
    // from
    emailData.from match {
      case Some(emailUrl) ⇒ message.setFrom(new InternetAddress(emailUrl))
      case None           ⇒ message.setFrom()
    }
    // subject
    message.setSubject(emailData.subject)
    // content
    message.setText(emailData.bodyText)
    // sent date
    message.setSentDate(new Date())

    message
  }
}
