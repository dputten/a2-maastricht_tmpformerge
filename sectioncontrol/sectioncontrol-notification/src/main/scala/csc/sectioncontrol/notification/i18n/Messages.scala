/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

package csc.sectioncontrol.notification.i18n

import java.text.MessageFormat

object Messages {
  val messages = Map(
    "csc.sectioncontrol.notification.event.Open" -> "Geopende deur",
    "csc.sectioncontrol.notification.event.OutOfRange" -> "Temperatuur buiten bereik",
    "csc.sectioncontrol.notification.event.HumidityOutOfRange" -> "Relatieve vochtigheid te hoog",
    "csc.sectioncontrol.notification.event.Leaning" -> "Scheefstand",
    "csc.sectioncontrol.notification.smsByEmail.systemevent.Send" -> "Molest sms verstuurd naar %s (via e-mail). (Alarm = %s)",
    "csc.sectioncontrol.notification.smsByEmail.systemevent.Failed" -> "Versturen molest sms naar %s (via e-mail) is niet gelukt. (Alarm = %s)")

  /**
   * Translates a message.
   *
   * Uses `java.text.MessageFormat` internally to format the message.
   *
   * @param key the message key
   * @param args the message arguments
   * @return the formatted message or a default rendering if the key wasn’t defined
   */
  def apply(key: String, args: Any*): String = {
    translate(key, args).getOrElse(noMatch(key, args))
  }

  /**
   * Translates a message.
   *
   * Uses `java.text.MessageFormat` internally to format the message.
   *
   * @param key the message key
   * @param args the message arguments
   * @return the formatted message, if this key was defined
   */
  private def translate(key: String, args: Seq[Any]): Option[String] = {
    if (messages.contains(key)) {
      Some(new MessageFormat(messages(key)).format(args.map(_.asInstanceOf[java.lang.Object]).toArray))
    } else {
      None
    }
  }

  private def noMatch(key: String, args: Seq[Any]) = key
}
