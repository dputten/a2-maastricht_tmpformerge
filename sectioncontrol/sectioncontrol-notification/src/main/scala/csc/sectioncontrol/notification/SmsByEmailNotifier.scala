/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.notification

import java.util.{ TimeZone, Date }
import java.text.SimpleDateFormat

import email.{ EmailData, MailServerData, EmailSender }
import csc.dlog.Event
import csc.config.Path
import csc.curator.utils.{ Curator, CuratorActor }

import csc.sectioncontrol.storage.{ ZkSystem, ZkCorridor, Paths }
import i18n.Messages
import csc.sectioncontrol.messages.SystemEvent

/**
 * Sends an email for all configured phone numbers to a sms-gateway for each Event received.
 * (Any filtering on events should happen outside this actor.)
 * @param systemId The id of the system
 * @param curator ZooKeeper curator
 */
class SmsByEmailNotifier(val notifierId: NotifierId, val systemId: String, val curator: Curator) extends CuratorActor with EmailSender {
  private val maxTries = 3

  private def now = System.currentTimeMillis()

  def receive = {
    case event: Event ⇒ {
      val combineToOneEmail = getSmsByEmailNotifierConfig.map(_.combineToOneEmail).getOrElse(false)

      if (combineToOneEmail) {
        doSendMail(getPhoneNumbers, event)
      } else {
        getPhoneNumbers.foreach(phoneNumber ⇒ doSendMail(Seq(phoneNumber), event))
      }
    }
  }

  /**
   * Send an email to the sms-gateway, in which the given event will be notified.
   * Will always result in a system event, either SmsByEmailSend or SmsByEmailFailed.
   * (The reason for any failure should be logged.)
   * @param phoneNumbers The phone numbers that should receive the sms
   * @param event The event to be notified
   */
  private def doSendMail(phoneNumbers: Seq[String], event: Event) {
    prepareMailAndSend(phoneNumbers: Seq[String], event: Event) match {
      case true  ⇒ sendEvent(smsByEmailEvent("Send", phoneNumbers, event))
      case false ⇒ sendEvent(smsByEmailEvent("Failed", phoneNumbers, event))
    }
  }

  private def smsByEmailEvent(resultText: String, phoneNumbers: Seq[String], event: Event): SystemEvent = SystemEvent(
    componentId = "SmsByEmailNotifier",
    timestamp = now,
    systemId = systemId,
    eventType = "Info",
    userId = "system",
    reason = Some(Messages("csc.sectioncontrol.notification.smsByEmail.systemevent." + resultText).format(phoneNumbers.mkString(";"), Messages("csc.sectioncontrol.notification.event." + event.eventType))))

  private def prepareMailAndSend(phoneNumbers: Seq[String], event: Event): Boolean = {
    (
      for (
        emailData ← getEmailData(phoneNumbers, event);
        mailServerData ← getMailServerData
      ) yield startSendingEmail(emailData, mailServerData)).getOrElse(false)
  }

  private def startSendingEmail(emailData: EmailData, mailServerData: MailServerData): Boolean = {
    def sendingMailTry(tryNumber: Int): Boolean = {
      if (tryNumber > maxTries)
        false
      else {
        sendEmail(emailData, mailServerData) match {
          case Right(_) ⇒
            true
          case Left(exception) ⇒
            logFailedTry(exception.getMessage)
            sendingMailTry(tryNumber + 1)
        }
      }
    }
    sendingMailTry(1)
  }

  private def getEmailData(phoneNumbers: Seq[String], event: Event): Option[EmailData] = {
    for (config ← getSmsByEmailNotifierConfig)
      yield createEmailData(phoneNumbers, event, config)
  }

  private def createEmailData(phoneNumbers: Seq[String], event: Event, config: SmsByEmailNotifierConfig): EmailData = {
    val bodyText = new BodyTextBuilder(
      systemId = systemId,
      event = event,
      template = config.emailBodyTemplate,
      systemConfig = getSystemConfig,
      corridorConfig = getFirstCorridorConfig).getBodyText
    new EmailData(
      subject = config.emailSubject.getOrElse(""),
      bodyText = bodyText,
      to = phoneNumbers.map(phoneNumber ⇒ "%s@%s".format(phoneNumber, config.smsGatewayDomain)),
      cc = None,
      bcc = None,
      from = Some(config.fromEmailAddress))
  }

  private def getMailServerData: Option[MailServerData] = {
    for (config ← getSmsByEmailNotifierConfig)
      yield new MailServerData(
      host = config.smtpHost,
      port = config.smtpPort,
      requiresAuthentication = (config.smtpUserName.isDefined || config.smtpPassword.isDefined),
      username = config.smtpUserName,
      password = config.smtpPassword,
      properties = config.smtpProperties)
  }

  private def sendEvent(systemEvent: SystemEvent) {
    val pathToSystem = Paths.Systems.getSystemPath(systemId)
    val pathToEvents = Paths.Systems.getLogEventsPath(systemId)

    if (curator.exists(pathToSystem) && !curator.exists(pathToEvents)) {
      curator.createEmptyPath(pathToEvents)
    }
    curator.appendEvent(Path(pathToEvents) / "qn-", systemEvent)
  }

  private def logFailedTry(message: String) {
    log.warning("Sending sms by email failed for system %s: %s".format(systemId, message))
  }

  private def getPhoneNumbers: Seq[String] = {
    getSystemSmsByEmailNotifierConfig match {
      case Some(config) ⇒ config.phoneNumbers
      case None ⇒
        log.info("No SmsByEmail phonenumbers configured for system %s.".format(systemId))
        Seq()
    }
  }

  private def getSystemSmsByEmailNotifierConfig: Option[SystemSmsByEmailNotifierConfig] = {
    curator.get[SystemSmsByEmailNotifierConfig](Paths.Systems.getNotifierConfigPath(systemId, notifierId))
  }

  private def getSmsByEmailNotifierConfig: Option[SmsByEmailNotifierConfig] = {
    val config = curator.get[SmsByEmailNotifierConfig](Paths.Configuration.getNotifierConfigPath(notifierId))
    if (config.isEmpty)
      log.error("No configuration present for %s notifier".format(notifierId))
    config
  }

  private def getSystemConfig: Option[ZkSystem] = {
    val config = curator.get[ZkSystem](Paths.Systems.getConfigPath(systemId))
    if (config.isEmpty)
      log.error("No configuration present for %s system".format(systemId))
    config
  }

  private def getFirstCorridorConfig: Option[ZkCorridor] = {
    val corridorIds = curator.getChildNames(Paths.Corridors.getDefaultPath(systemId))
    if (corridorIds.isEmpty) {
      log.error("No corridor configuration for system %s.".format(systemId))
      None
    } else {
      val config = curator.get[ZkCorridor](Paths.Corridors.getConfigPath(systemId, corridorIds.head))
      if (config.isEmpty)
        log.error("No corridor configuration for system %s.".format(systemId))
      config
    }
  }
}

private[notification] class BodyTextBuilder(
  val systemId: String,
  val event: Event,
  val template: String,
  val systemConfig: Option[ZkSystem],
  val corridorConfig: Option[ZkCorridor]) {
  val replacements = Seq(
    ("<hhm-id>", getHhmId),
    ("<reason>", translateEvent(event.eventType)),
    ("<location>", getLocationText),
    ("<date-time>", dateToString(new Date(event.timestamp), "dd-MM-yyyy HH:mm:ss z")))

  def getBodyText: String = {
    replacements.foldLeft(template) {
      case (result, (token, text)) ⇒ result.replaceAll(token, text)
    }
  }

  private def getHhmId: String = {
    systemConfig match {
      case Some(config) ⇒ config.violationPrefixId
      case None         ⇒ systemId
    }
  }

  private def getLocationText: String = {
    corridorConfig match {
      case Some(config) ⇒
        val location1Text = config.info.locationLine1.trim
        val location2Text = config.info.locationLine2
        location2Text match {
          case Some(text) ⇒ "%s, %s".format(location1Text, text.trim)
          case None       ⇒ location1Text
        }
      case None ⇒
        "onbekend"
    }
  }

  private def translateEvent(eventType: EventType) = {
    val key = "csc.sectioncontrol.notification.event." + eventType
    val message = Messages("csc.sectioncontrol.notification.event." + eventType)
    if (message == key) eventType else message
  }

  private def dateToString(date: Date, format: String, timeZone: String = "CET"): String = { //TODO: als extension inbouwen (zie cts_sensor)
    val dateFormat = new SimpleDateFormat(format)
    dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone))

    dateFormat.format(date)
  }
}
