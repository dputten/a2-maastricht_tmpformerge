/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.notification

case class NotificationStateConfig(val isEnabled: Boolean)

case class NotifierEventsConfig(val events: Seq[EventType]) {
  def isActive(eventType: EventType) = events.contains(eventType)
}

case class SystemSmsByEmailNotifierConfig(val phoneNumbers: Seq[String])

case class SmsByEmailNotifierConfig(
  combineToOneEmail: Boolean,
  emailSubject: Option[String] = None,
  emailBodyTemplate: String,
  fromEmailAddress: String,
  smsGatewayDomain: String,
  smtpHost: String,
  smtpPort: Option[Int],
  smtpUserName: Option[String],
  smtpPassword: Option[String],
  smtpProperties: Option[Map[String, String]])
