/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol

/**
 * Some notification related definitions
 */
package object notification {
  type EventType = String
  type NotifierId = String
}
