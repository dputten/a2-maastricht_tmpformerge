/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.notification

import akka.actor.ActorRef

import csc.dlog.Event
import csc.curator.utils.{ CuratorActor, Curator }

import csc.sectioncontrol.storage.Paths

/**
 * Passes the received Event's to the configured notifiers, based on the type of the event.
 * @param systemId The id of the system
 * @param curator ZooKeeper curator
 * @param notifiers Map of available notifiers, key is NotifierId
 */
class EventNotificationHandler(val systemId: String, val curator: Curator, val notifiers: Map[NotifierId, ActorRef]) extends CuratorActor {

  def receive = {
    case msg: Event ⇒ {
      if (notificationEnabled) {
        for (
          notifierId ← getNotifierIds(msg.eventType); // Get notifierId's for event
          notifier ← getNotifier(notifierId) // Get notifier actor for notifierId
        ) notifier ! msg
      }
    }
  }

  /**
   * Get the notifier for given notifier id.
   * @param notifierId  The notifier id
   * @return The notifier for given notifier id
   */
  def getNotifier(notifierId: NotifierId): Option[ActorRef] = {
    for (notifierEntry ← notifiers.find(_._1 == notifierId))
      yield notifierEntry._2
  }

  /**
   * Get the Id's of all configured notifiers for given event type.
   * @param eventType The event type
   * @return The Id's of all configured notifiers for given event type
   */
  def getNotifierIds(eventType: EventType): Seq[NotifierId] = {
    getConfiguredNotifierIds.filter(notifierId ⇒ notifierIsActiveForEvent(notifierId, eventType))
  }

  /**
   * Return whether given notifier is configured to be used for given event type.
   * @param notifierId The id of the notifier
   * @param eventType  The event type
   * @return Whether given notifier is configured to be used for given event type
   */
  def notifierIsActiveForEvent(notifierId: NotifierId, eventType: EventType): Boolean = {
    getNotifierEventConfig(notifierId) match {
      case Some(notifierEventsConfig) ⇒ notifierEventsConfig.isActive(eventType)
      case None                       ⇒ false
    }
  }

  def notificationEnabled: Boolean = {
    getNotificationStateConfig match {
      case Some(config) ⇒ config.isEnabled
      case None ⇒ {
        log.info("No notification configuration found. Notification will be disabled.")
        false
      }
    }
  }

  private def getNotificationStateConfig: Option[NotificationStateConfig] = {
    curator.get[NotificationStateConfig](Paths.Configuration.getNotificationStatePath)
  }

  private def getConfiguredNotifierIds: Seq[NotifierId] = {
    curator.getChildNames(Paths.Configuration.getNotifiersRootPath)
  }

  private def getNotifierEventConfig(notifierId: NotifierId): Option[NotifierEventsConfig] = {
    curator.get[NotifierEventsConfig](Paths.Configuration.getNotifierEventsPath(notifierId))
  }
}
