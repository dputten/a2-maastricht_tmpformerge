package csc.mtm

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import org.slf4j.LoggerFactory

class TimeTest extends WordSpec with MustMatchers {
  private val log = LoggerFactory.getLogger(this.getClass.getName)

  "Timestamps" must {
    "apply timestamps" in {
      val input = getClass.getResourceAsStream("/2012-02-07-MTM-raw.txt")
      val worker = new Timestamps(input)
      val result = worker.newContent
      input.close
      val applied = getClass.getResourceAsStream("/2012-02-07-MTM-applied.txt")
      val applied_data: Array[Byte] = Stream.continually(applied.read).takeWhile(-1 !=).map(_.toByte).toArray
      val applied_str = new String(applied_data, "UTF-8")
      val zipped = applied_str.split("\n") zip result.split("\n")
      zipped.foreach { case (f, s) ⇒ if (f != s) log.info(f, s) }
      result.trim must be(applied_str.trim)
    }

  }

}
