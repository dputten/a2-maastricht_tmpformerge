/*
* Copyright (C) 2012 CSC. <http://www.csc.com>
*/
package csc.mtm

import java.io.{ FileOutputStream, FileInputStream, InputStream }
import util.matching.Regex
import java.text.SimpleDateFormat
import java.util
import csc.sectioncontrol.mtm.nl.ProcessMtmData

object Util {
  def main(args: Array[String]) {
    if (args.size != 2) {
      println("Usage: java -cp test-mtm-assembly-X.jar csc.mtm.Util  <command> <arg>")
      println("command: prepare <filename>")
      println("command: time <UTC timestamp in milliseconds>")
      System.exit(-1)
    }
    args(0) match {
      case "prepare" ⇒ new Rewriter(args(1))
      case "time" ⇒
        val converter = new TimeConverter(args(1))
        val human = converter.convert
        println(human)
      case error ⇒ throw new RuntimeException("Unsupported command: " + error)
    }
    println("Ok.")
  }
}

class TimeConverter(timestamp: String) {
  if (!timestamp.matches("\\d+")) throw new RuntimeException("Unexpected timestamp: " + timestamp)
  val date = new util.Date(timestamp.toLong)
  val formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
  formatter.setTimeZone(java.util.TimeZone.getTimeZone("Europe/Amsterdam"))
  val readable = formatter.format(date)

  def convert: String = readable
}

class Timestamps(input: InputStream) {
  //|MSI|2012/02/07 06:37:08|1328593028|       383| A20 R  28.100 |             3|50       |
  //|OS|2012/02/07 00:00:00|1328569244|       387| A20 R  28.410 |nee|nee|on-line|  goed| ja| ja| ja| ja| ja|    goed|
  //|TOP|2012/02/07 00:00:00|1328569200|      41|                 1|      41|
  val Entry = new Regex("""\|(MSI|OS|TOP)\|(\d\d\d\d/\d\d/\d\d \d\d:\d\d:\d\d)\|(\d+?)\|(.+)""")
  val formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
  formatter.setTimeZone(java.util.TimeZone.getTimeZone("Europe/Amsterdam"))

  val lines = scala.io.Source.fromInputStream(input).getLines

  private def fixTime: List[String] = {
    lines.map(line ⇒ line match {
      case Entry(name, date, timestamp, rest) ⇒
        val parsed: util.Date = formatter.parse(date)
        "|%s|%s|%s|%s".format(name, date, parsed.getTime / 1000, rest)
      case _ ⇒ line
    }).toList
  }

  def newContent: String = {
    val fixed = fixTime.mkString("\n")
    val hashIndex = fixed.lastIndexOf(ProcessMtmData.MARK)
    val hashed: String = if (hashIndex == -1) {
      println("No HASHCODE found in MTM file: " + fixed)
      throw new RuntimeException("No HASHCODE found in MTM file.")
    } else {
      val (content, _) = fixed.splitAt(hashIndex + ProcessMtmData.MARK.size)
      val hash = ProcessMtmData.md5SumString(content.getBytes("UTF-8"))
      content + hash + "\n"
    }
    hashed
  }
}

class Rewriter(filename: String) {
  val file = new java.io.File(filename)
  val input = new FileInputStream(file)
  val worker = new Timestamps(input)
  val newContent = worker.newContent
  input.close
  val output = new FileOutputStream(file)
  output.write(newContent.getBytes("UTF-8"))
  output.close
}

