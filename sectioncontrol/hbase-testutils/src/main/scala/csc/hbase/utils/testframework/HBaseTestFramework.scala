/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.hbase.utils.testframework

import java.util
import java.io.File
import java.net.URI

import org.scalatest.{ Suite, BeforeAndAfterAll }

import org.apache.commons.io.FileUtils

import org.apache.hadoop.fs.Path
import org.apache.hadoop.hbase.{ HConstants, HBaseTestingUtility }
import org.apache.hadoop.hbase.client._
import org.apache.hadoop.conf.Configuration

/**
 * For testing with HBase, one local server is started and stopped.
 * Mix in this trait in you ScalaTest classes to use it.
 */
trait HBaseTestFramework extends BeforeAndAfterAll {
  this: BeforeAndAfterAll with Suite ⇒

  protected val testDir = "file:///tmp/csc.hbase.utils.testframework.TestHBase"
  protected val testZkDir = "/tmp/csc.hbase.utils.testframework.TestHBaseZk"

  protected val numberOfRegionServers = 1
  protected var testUtil: HBaseTestingUtility = _
  protected var hbaseConfig: Configuration = _

  protected def startZkCluster = true

  protected def getHbaseConfig = testUtil.getMiniHBaseCluster.getConfiguration
  protected def hbaseZkServers = hbaseConfig.get("hbase.zookeeper.quorum")
  protected def hbaseZkClientPort = hbaseConfig.get("hbase.zookeeper.property.clientPort")

  protected def zkConnectionString = "%s:%s".format(hbaseZkServers, hbaseZkClientPort)

  override protected def beforeAll() {
    super.beforeAll()

    val testDirFile = new File(new URI(testDir))
    if (testDirFile.exists())
      FileUtils.deleteDirectory(testDirFile)
    FileUtils.forceMkdir(testDirFile)
    if (new File(testZkDir).exists())
      FileUtils.deleteDirectory(new File(testZkDir))
    testUtil = new HBaseTestingUtility() {
      override def createRootDir() = {
        val hbaseRoot = new Path(testDir)
        getConfiguration.set(HConstants.HBASE_DIR, hbaseRoot.toString)
        hbaseRoot
      }
    }
    testUtil.getConfiguration.set(HConstants.ZOOKEEPER_DATA_DIR, testZkDir)

    if (startZkCluster) {
      testUtil.startMiniZKCluster()
    }
    testUtil.startMiniHBaseCluster(1, 1)
    hbaseConfig = getHbaseConfig
  }

  override protected def afterAll() {
    // hack to make sure the master connection is really closed after shutting down the cluster.
    val masterConfig = testUtil.getMiniHBaseCluster.getMaster.getConfiguration
    ///HConnectionManager.deleteConnection(masterConfig, true)

    // RV Note:
    // The 3 lines below have been commented, because of "Reconnect errors" that occur during test runs.
    // Since doing this these errors haven't occurred anymore. If this continues to be the case, these lines may be removed.
    testUtil.shutdownMiniHBaseCluster()
    //   if (startZkCluster) {
    //     testUtil.shutdownMiniZKCluster()
    //   }
    super.afterAll()
  }

  def truncateTable(table: HTable) {
    import collection.JavaConversions._

    val rs = table.getScanner(new Scan())
    try {
      val delRows = rs.map(r ⇒ new Delete(r.getRow()))
      //created the list this way because the delete uses remove on this list
      //Which is unsupported when using a scala toList
      val list = new util.ArrayList[Delete]()
      for (row ← delRows) {
        list.add(row)
      }
      table.delete(list)
    } finally {
      rs.close()
    }
  }
}
