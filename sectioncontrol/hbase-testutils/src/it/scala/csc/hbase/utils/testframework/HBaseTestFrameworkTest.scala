/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.hbase.utils.testframework

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

class HBaseTestFrameworkTest extends WordSpec with MustMatchers with HBaseTestFramework {
  "HBaseTestFramework" must {
    "start internal hbase instance" in {
      val admin = testUtil.getHBaseAdmin
      admin.isMasterRunning must be(true)
      admin.getClusterStatus.getServers.size() must be(numberOfRegionServers)
      admin.close()
    }
  }
}
