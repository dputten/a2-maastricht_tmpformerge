package csc.sectioncontrol.enforce.common.nl.casefiles

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 12/11/14.
 */

/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

import csc.sectioncontrol.enforce.nl.casefiles.HHMVS40
import csc.sectioncontrol.enforce.common.nl.services.{ RedLight, SpeedFixed, SectionControl, Service }
import csc.sectioncontrol.enforce.common.nl.messages.HeaderInfo
import csc.sectioncontrol.enforce.nl.casefiles.CaseFile

/*
Represents the IRS HHM v1.4. It uses v4.0 internally
 */
case class HHMVS20() extends CaseFile {
  // the output is identical to v4.0
  private val v40: CaseFile = new HHMVS40

  /**
   * indicates if the one case file record must contain additional info
   * @return
   */
  def showExtraInfo: Boolean = v40.showExtraInfo

  /**
   * the 'standby' text for the mtmInfo output file.
   * @return stand-by string
   */
  override def mtmInfoStandBy: String = "stand-by"

  /**
   * the last time value of a mtmInfo file differs per file format. (24:00:00 or 23:59:59)
   * @param dayEnd long represent the end of the day (time)
   * @return the the given dayEnd of a slight modification based on the case file version
   */
  override def mtmInfoLastMoment(dayEnd: Long): Long = dayEnd - 1000

  /**
   * returns the correct header that autostats must contain. this can differs given service
   * @param service corridor service
   * @param totalAutomatic total amount of auto violations
   * @param totalManual total amount of manual violations
   * @param totalMobi total amount of mobi violations
   * @return Same as HHM v4.0
   */
  def createStats(service: Service, totalAutomatic: Int, totalManual: Int, totalMobi: Int): Seq[String] = {
    v40.createStats(service, totalAutomatic, totalManual, totalMobi)
  }

  /**
   * returns the correct header that each output of a case file must contain. this can differs given service
   * @param service corridor service
   * @param header the values of that must be added to the header static data
   * @return Same as HHM v4.0
   */
  def createHeader(service: Service, header: HeaderInfo): Seq[String] = createHeader(service, header, "v2.0")
}
