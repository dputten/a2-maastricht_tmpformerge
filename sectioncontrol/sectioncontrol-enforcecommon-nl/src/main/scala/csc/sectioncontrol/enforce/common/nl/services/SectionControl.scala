/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

package csc.sectioncontrol.enforce.common.nl.services

import csc.sectioncontrol.enforce.common.nl.violations.VehicleSpeed
import csc.sectioncontrol.messages.{ VehicleClassifiedRecord, VehicleImageType, VehicleMetadata }
import csc.sectioncontrol.qualify.SectionControlQualifier
import csc.sectioncontrol.storage.ServiceType

/**
 * Represents the SectionControl service
 * @param flgFileName filename that must be created within each auto/mobi and hand folder. Default "traject.flg"
 * @param dynamicEnforcement indicates if all possible violations are violations if dynamic speed is different that enforced speed. Default false.
 * indicated that all violations must be pardoned if it enforced speed differs
 */
case class SectionControl(flgFileName: Option[String] = Some("traject.flg"),
                          dynamicEnforcement: Boolean = false) extends Service {

  lazy val qualifier = new SectionControlQualifier

  /**
   * the type of this service
   * @return ServiceType
   */
  def serviceType: ServiceType.Value = ServiceType.SectionControl

  /**
   * indicates of the second image for output of the case file is required
   * @return boolean
   */
  def requireSecondImage: Boolean = true

  def getImage1Ref(entry: VehicleMetadata, exit: Option[VehicleMetadata]) = Some((VehicleImageType.Overview, entry))

  def getImage2Ref(entry: VehicleMetadata, exit: Option[VehicleMetadata]) = exit.map((VehicleImageType.Overview, _))

  /**
   * determines if a given classified/vehicle record is in violation.
   * @param vcr the vehicle classified record
   * @param speed all information needed to determine a violation
   * @return a tuple of boolean and the speed. THe boolean indicates if it is a violation. The Int represent the enforced speed
   */
  def isViolation(vcr: VehicleClassifiedRecord, speed: VehicleSpeed): (Boolean, Int) = {
    qualifier.isViolation(
      speed.measuredSpeed,
      speed.scheduleSpeedLimit,
      speed.dynamicSpeedLimit,
      speed.vehicleClassSpeedLimit,
      speed.speedMargins,
      dynamicEnforcement)
  }
}