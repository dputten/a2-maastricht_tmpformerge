package csc.sectioncontrol.enforce.nl.casefiles

/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

import csc.sectioncontrol.enforce.common.nl.messages.HeaderInfo
import csc.sectioncontrol.enforce.common.nl.services.Service

/*
Represents the IRS HHM v1.4. It uses v4.0 internally
 */
case class HHMVS14() extends CaseFile {
  // the output is identical to v4.0
  private val v40: CaseFile = new HHMVS40

  /**
   * indicates if the one case file record must contain additional info
   * @return
   */
  def showExtraInfo: Boolean = v40.showExtraInfo

  /**
   * the 'standby' text for the mtmInfo output file.
   * @return Same as HHM v4.0
   */
  override def mtmInfoStandBy: String = v40.mtmInfoStandBy

  /**
   * returns the correct header that autostats must contain. this can differs given service
   * @param service corridor service
   * @param totalAutomatic total amount of auto violations
   * @param totalManual total amount of manual violations
   * @param totalMobi total amount of mobi violations
   * @return Same as HHM v4.0
   */
  def createStats(service: Service, totalAutomatic: Int, totalManual: Int, totalMobi: Int): Seq[String] = {
    v40.createStats(service, totalAutomatic, totalManual, totalMobi)
  }

  /**
   * returns the correct header that each output of a case file must contain. this can differs given service
   * @param service corridor service
   * @param header the values of that must be added to the header static data
   * @return Same as HHM v4.0
   */
  def createHeader(service: Service, header: HeaderInfo): Seq[String] = createHeader(service, header, "v1.4")
}
