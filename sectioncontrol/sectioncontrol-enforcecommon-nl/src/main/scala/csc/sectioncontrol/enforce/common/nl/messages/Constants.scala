/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.common.nl.messages

/**
 * Constants used internally in the package csc.sectioncontrol.enforce.violations
 *
 * @author Maarten Hazewinkel
 */
object Constants {
  val fileExportCharSet = java.nio.charset.Charset.forName("ISO-8859-1")
}
