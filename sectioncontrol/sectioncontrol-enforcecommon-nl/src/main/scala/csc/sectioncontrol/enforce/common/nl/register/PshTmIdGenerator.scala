/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.common.nl.register

import java.text.SimpleDateFormat
import java.util.Date

import csc.sectioncontrol.common.DayUtility

import scala.util.parsing.combinator.RegexParsers
import csc.sectioncontrol.enforce.violations.Implicits._
import csc.sectioncontrol.storage.ServiceType

/**
 * Generate a PSH-TM-ID value based on a format string.
 *
 * The format string can consist of fixed uppercase text and numbers and 7 possible types of fields.
 * Fields are indicated by enclosing them in square brackets: "[YY]"
 *
 * Supported fields are:
 * - YY: The last 2 digits of the year
 * - MM: The month number
 * - DD: The day number within the month
 * - NNN: The day number within the year
 * - VVVV: The 4 digits of the reporting officer code (which is of format AB1234)
 * - LLLL: 4 digits of the location code
 * - HHHH: 4 Chars of the HHM code or systemName
 * - T: The type of violation. Always 3 (section control)
 *
 * @author Maarten Hazewinkel
 */
// Implements requirements Req053 Req065
object PshTmIdGenerator {
  /**
   * Generate a PSH-TM-ID value based on a format string.
   *
   * The format string can consist of fixed uppercase text and numbers and 7 possible types of fields.
   * Fields are indicated by enclosing them in square brackets: "[YY]"
   *
   * Supported fields are:
   * - YY: The last 2 digits of the year
   * - MM: The month number
   * - DD: The day number within the month
   * - NNN: The day number within the year
   * - VVVV: The 4 digits of the reporting officer code (which is of format AB1234)
   * - LLLL: 4 digits of the location code
   * - HHHH: 4 Chars of the HHM code or systemName
   * - T: The type of violation (service type)
   *
   * @param format The format string
   * @param day The day as milliseconds since unix epoch (Jan 1 1970)
   * @param reportingOfficerCode The reporting officer code
   * @param locationCode The location code
   * @param serviceType The servicetype
   * @param systemName The system name
   * @param dayEndTime the day end time in msec
   * @return
   */
  def apply(format: String, day: Long, reportingOfficerCode: String, locationCode: String, serviceType: ServiceType.Value, systemName: String, dayEndTime: Long = -1, roadPart: String): String = {
    val elements = FormatParser.parseAll(FormatParser.pshTmId, format).getOrElse(List(TextElement("ParseError")))
    def formatDateAs(format: String): String = {
      val df = new SimpleDateFormat(format)
      df.setTimeZone(DayUtility.fileExportTimeZone)

      val corDay = if (dayEndTime <= 0 || day < dayEndTime) {
        day
      } else {
        //when it is endof day than use the previous day
        day - 1
      }
      df.format(new Date(corDay))
    }
    def codeForServiceType(serviceType: ServiceType.Value): String = serviceType match {
      case ServiceType.SpeedFixed ⇒ "1"
      case ServiceType.RedLight   ⇒ "2"
      case ServiceType.SectionControl ⇒ {
        if (roadPart.toUpperCase.startsWith("R")) {
          "4"
        } else {
          "3"
        }
      }
      case _ ⇒ ""
    }
    elements.map(_ match {
      case t: TextElement          ⇒ t.text
      case YearElement             ⇒ formatDateAs("yy")
      case MonthElement            ⇒ formatDateAs("MM")
      case DayElement              ⇒ formatDateAs("dd")
      case DayInYearElement        ⇒ formatDateAs("DDD")
      case ReportingOfficerElement ⇒ reportingOfficerCode.substring(2)
      case LocationElement         ⇒ locationCode.padLeft(5, ' ').takeRight(4)
      case HHMIDElement            ⇒ formatText(4, ' ', systemName)
      case ViolationTypeElement    ⇒ codeForServiceType(serviceType)
    }).mkString
  }

  private[register] def formatText(len: Int, padChar: Char, hhmName: String): String = {
    if (hhmName.length > len) {
      hhmName.substring(0, len)
    } else {
      var name = hhmName
      for (i ← hhmName.length until len) {
        name = padChar + name
      }
      name
    }
  }

  private[register] object FormatParser extends RegexParsers {
    def pshTmId: Parser[List[Element]] = rep(text | placeholder)

    def text: Parser[TextElement] = """[A-Z0-9][A-Z0-9]*""".r ^^ (t ⇒ TextElement(t))
    def placeholder: Parser[PlaceHolder] = "[" ~> field <~ "]"
    def field: Parser[PlaceHolder] =
      "YY" ^^ (_ ⇒ YearElement) |
        "MM" ^^ (_ ⇒ MonthElement) |
        "DD" ^^ (_ ⇒ DayElement) |
        "NNN" ^^ (_ ⇒ DayInYearElement) |
        "VVVV" ^^ (_ ⇒ ReportingOfficerElement) |
        "LLLL" ^^ (_ ⇒ LocationElement) |
        "HHHH" ^^ (_ ⇒ HHMIDElement) |
        "T" ^^ (_ ⇒ ViolationTypeElement)
  }

  private[register] sealed trait Element
  private[register] case class TextElement(text: String) extends Element
  private[register] trait PlaceHolder extends Element
  private[register] case object YearElement extends PlaceHolder
  private[register] case object MonthElement extends PlaceHolder
  private[register] case object DayElement extends PlaceHolder
  private[register] case object DayInYearElement extends PlaceHolder
  private[register] case object ReportingOfficerElement extends PlaceHolder
  private[register] case object LocationElement extends PlaceHolder
  private[register] case object ViolationTypeElement extends PlaceHolder
  private[register] case object HHMIDElement extends PlaceHolder
}

