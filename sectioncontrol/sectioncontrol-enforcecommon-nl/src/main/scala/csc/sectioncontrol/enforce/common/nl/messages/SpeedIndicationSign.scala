/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.common.nl.messages

import csc.sectioncontrol.messages.SpeedIndicatorType

case class SpeedIndicationSign(signIndicator: Boolean = false,
                               indicatorType: Option[SpeedIndicatorType.Value] = None,
                               indicatedSpeed: Option[Int] = None) {
  require(indicatorType != null, "indicatorType is null")

  if (signIndicator) {
    require(indicatorType.isDefined, "indicatorType (Soort Bord) must be present when signIndicator (indicatie bord) is true")
    require(indicatedSpeed.isDefined, "indicatedSpeed (Bordsnelheid) must be present when signIndicator (indicatie bord) is true")
  }

  if (!signIndicator) {
    require(indicatorType.isEmpty, "indicatorType (Soort Bord) must not be present when signIndicator (indicatie bord) is false")
  }

  if (indicatedSpeed.isDefined)
    require(indicatedSpeed.get >= 0 && indicatedSpeed.get < 1000, "indicatedSpeed (Bordsnelheid) must be between 0 and 999")
}