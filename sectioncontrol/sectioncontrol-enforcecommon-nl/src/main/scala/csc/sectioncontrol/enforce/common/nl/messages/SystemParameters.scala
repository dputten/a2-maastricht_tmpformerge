/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.common.nl.messages

import csc.sectioncontrol.enforce.violations.Checks._
/**
 * Case class containing the specified system parameters that are reported in the calibration
 * test results.
 *
 * @author Maarten Hazewinkel
 */
case class SystemParameters(activeCorridor: String,
                            configuredSpeedLimit: Int,
                            mtmScanIntervalSeconds: Int,
                            violationRetentionDays: Int,
                            imageCompressionFactor: Int,
                            location: String,
                            dutyNumber: String,
                            thresholdTimeSeconds: Int,
                            thresholdSpeed: Int,
                            pardonTimeSeconds: Int) {
  require(allNotNull(activeCorridor, location, dutyNumber),
    nullValuesErrorMessage)
}