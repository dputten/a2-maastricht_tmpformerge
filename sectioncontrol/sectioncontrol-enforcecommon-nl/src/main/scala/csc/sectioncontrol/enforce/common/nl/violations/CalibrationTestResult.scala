/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.common.nl.violations

import csc.sectioncontrol.enforce.common.nl.messages.SystemParameters
import csc.sectioncontrol.enforce.common.nl.register.PshTmIdGenerator
import csc.sectioncontrol.enforce.violations.Checks._
import csc.sectioncontrol.enforce.violations.Implicits._
import csc.sectioncontrol.messages.certificates.ComponentCertificate
import java.text.SimpleDateFormat
import csc.sectioncontrol.storage.ServiceType

/**
 * Case class to hold the result of a single calibration test, including the test photos.
 *
 * @author Maarten Hazewinkel
 */
case class CalibrationTestResult(testedOk: Boolean,
                                 time: Long,
                                 reportingOfficerCode: String,
                                 cameraCount: Int,
                                 softwareSeals: Seq[ComponentCertificate],
                                 serialNumbers: Seq[String],
                                 parameters: SystemParameters,
                                 serviceType: ServiceType.Value,
                                 testPhotos: Seq[CalibrationImage]) {
  require(allNotNull(softwareSeals, serialNumbers, parameters, testPhotos, reportingOfficerCode),
    nullValuesErrorMessage)
  require(cameraCount >= 0,
    "cameraCount must be >= 0 " + this)
  require(softwareSeals.length >= 1,
    "at least one component certificate must be present " + this)
  require(serialNumbers.length >= 1,
    "at least one serial number must be present " + this)
  require(!serialNumbers.map(_.length > 0).contains(false),
    "empty serial numbers are not allowed " + this)
  require(testPhotos.length <= cameraCount,
    "cannot have more testPhotos than cameras " + this)
  require(testPhotos.map(_.cameraId).toSet.size == testPhotos.size,
    "all photos must have a distinct cameraId " + this)
  require("^[A-Z0-9]{6}$".r.findFirstIn(reportingOfficerCode).isDefined,
    "reportingOfficerCode must be 6 charakters (Verbalistantcode) " + this)

  def toDataLine(globalParameters: ViolationGroupParameters, dayEndTime: Long): String = {
    val timeFormat = new SimpleDateFormat("HHmmss")
    val dayEndTimeFormat = new SimpleDateFormat("kkmmss")
    val formattedTime = if (time < dayEndTime) {
      timeFormat(time)
    } else {
      dayEndTimeFormat(time - 1)
    }

    List(if (testedOk) "1" else "0",
      formattedTime,
      globalParameters.systemName.toLowerCase,
      PshTmIdGenerator(globalParameters.pshTmIdFormat, time, reportingOfficerCode, globalParameters.corridorInfo.locationCode, serviceType, globalParameters.pshtmId, dayEndTime, globalParameters.roadPart),
      cameraCount,
      softwareSeals.map(s ⇒ s.name + " " + s.checksum).mkString(";"),
      serialNumbers.mkString(";"),
      parameters.productIterator.mkString(",")).mkString(",")
  }
}

/**
 * Public trait specifying the CalibrationImage. Sealed to limit implementations to this file.
 *
 * @author Maarten Hazewinkel
 */
sealed trait CalibrationImage {
  def cameraId: Int
  def time: Long
}
/**
 * Public factory for creating CalibrationImage instances
 *
 * @author Maarten Hazewinkel
 */
object CalibrationImage {
  def apply(cameraId: Int, time: Long, image: Array[Byte]) = CalibrationTestPhoto(cameraId, time, image)
}

/**
 * CalibrationImage implementation that has cached the image data on disk.
 *
 * @author Maarten Hazewinkel
 */
case class CachedCalibrationImage(cameraId: Int,
                                  time: Long,
                                  imageFileName: String)
  extends CalibrationImage {
  require(allNotNull(imageFileName),
    nullValuesErrorMessage)
  require(cameraId >= 0,
    "cameraId must be at least 0")
}

/**
 * CalibrationImage implementation that holds the image data in memory.
 *
 * @author Maarten Hazewinkel
 */
case class CalibrationTestPhoto(cameraId: Int,
                                time: Long,
                                image: Array[Byte])
  extends CalibrationImage {
  require(allNotNull(image),
    nullValuesErrorMessage)
  require(cameraId >= 0,
    "cameraId must be at least 0")
  //require(isJFIF(image),
  //"image must be a JPEG image")
}