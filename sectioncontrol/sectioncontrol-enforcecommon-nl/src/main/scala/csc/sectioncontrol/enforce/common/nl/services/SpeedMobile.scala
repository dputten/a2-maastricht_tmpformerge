/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

package csc.sectioncontrol.enforce.common.nl.services

import csc.sectioncontrol.enforce.common.nl.violations.VehicleSpeed
import csc.sectioncontrol.messages.{ VehicleImageType, VehicleClassifiedRecord, VehicleMetadata }
import csc.sectioncontrol.storage.ServiceType

/**
 * Represents the SpeedMobile service
 * @param flgFileName filename that must be created within each auto/mobi and hand folder. Default "snelheid.flg"
 * @param dynamicEnforcement indicates if all possible violations are violations if dynamic speed is different that enforced speed. Default false.
 * indicated that all violations must be pardoned if it enforced speed differs
 */
case class SpeedMobile(flgFileName: Option[String] = Some("snelheid.flg"),
                       dynamicEnforcement: Boolean = false) extends Service {

  /**
   * the type of this service
   * @return ServiceType
   */
  def serviceType: ServiceType.Value = ServiceType.SpeedMobile

  /**
   * indicates of the second image for output of the case file is required
   * @return boolean
   */
  def requireSecondImage: Boolean = false

  def getImage1Ref(entry: VehicleMetadata, exit: Option[VehicleMetadata]) = None

  def getImage2Ref(entry: VehicleMetadata, exit: Option[VehicleMetadata]) = None

  /**
   * determines if a given classified/vehicle record is in violation. Not used by this service
   * @param vcr the vehicle classified record
   * @param speed all information needed to determine a violation
   * @return a tuple of boolean and the speed. For this service it's always (false, 0)
   */
  def isViolation(vcr: VehicleClassifiedRecord, speed: VehicleSpeed): (Boolean, Int) = (false, 0)
}