/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

package csc.sectioncontrol.enforce.common.nl.services

import csc.sectioncontrol.enforce.common.nl.violations.VehicleSpeed
import csc.sectioncontrol.messages.VehicleImageType
import csc.sectioncontrol.messages.VehicleImageType._
import csc.sectioncontrol.messages._
import csc.sectioncontrol.qualify.RedLightQualifier
import csc.sectioncontrol.storage.ServiceType

/**
 * Represents the RedLight service
 * @param flgFileName filename that must be created within each auto/mobi and hand folder. Default "roodlicht.flg"
 * @param dynamicEnforcement indicates if all possible violations are violations if dynamic speed is different that enforced speed. Default false.
 * indicated that all violations must be pardoned if it enforced speed differs
 * @param factNumber indicates the factnumber, Required as output when service is redlight
 */
case class RedLight(flgFileName: Option[String] = Some("roodlicht.flg"),
                    dynamicEnforcement: Boolean = false,
                    pardonRedTime: Long,
                    minimumYellowTime: Long,
                    factNumber: String) extends Service {

  lazy val qualifier = new RedLightQualifier

  /**
   * used for ctres report (drempeltijd)
   * @return the minimumYellowTime
   */
  override def thresholdTime = minimumYellowTime.toInt

  /**
   * used for ctres report (drempelsnelheid)
   * @return enforcedSpeedLimit
   */
  override def thresholdSpeed(enforcedSpeedLimit: Int = 0): Int = enforcedSpeedLimit

  /**
   * used for ctres report (pardontijd)
   * @return pardonRedTime
   */
  override def pardonTime: Int = pardonRedTime.toInt

  /**
   * the type of this service
   * @return ServiceType
   */
  def serviceType: ServiceType.Value = ServiceType.RedLight

  /**
   * indicates of the second image for output of the case file is required
   * @return boolean
   */
  def requireSecondImage: Boolean = true

  /**
   * determines if a given classified/vehicle record is in violation.
   * @param vcr the vehicle classified record
   * @param speed all information needed to determine a violation
   * @return a tuple of boolean and the speed. THe boolean indicates if it is a violation. The Int represent the enforced speed
   */
  def isViolation(vcr: VehicleClassifiedRecord, speed: VehicleSpeed): (Boolean, Int) = {
    val firstImage = getOriginalImage1(vcr.speedRecord.entry, vcr.speedRecord.exit)
    val secondImage = getOriginalImage2(vcr.speedRecord.entry, vcr.speedRecord.exit)

    qualifier.isViolation(firstImage, secondImage, pardonRedTime, minimumYellowTime, speed.measuredSpeed, speed.scheduleSpeedLimit)
  }

  def getOriginalImage1(entry: VehicleMetadata, exit: Option[VehicleMetadata]): Option[VehicleImage] =
    Service.getOriginalImage(getImage1Ref(entry, exit))

  def getOriginalImage2(entry: VehicleMetadata, exit: Option[VehicleMetadata]): Option[VehicleImage] =
    Service.getOriginalImage(getImage2Ref(entry, exit))

  def getImage1Ref(entry: VehicleMetadata, exit: Option[VehicleMetadata]) = Some((VehicleImageType.RedLight, entry))

  def getImage2Ref(entry: VehicleMetadata, exit: Option[VehicleMetadata]) = Some((VehicleImageType.OverviewRedLight, entry))

  /**
   * retrieves the timeRed from a image
   * @param images the images of passage
   * @return optional long
   */
  def timeRed(images: Seq[VehicleImage]): Option[Long] = {
    images.find(_.imageType.toString == VehicleImageType.RedLight.toString).flatMap(_.timeRed)
  }

  /**
   * retrieves the timeYellow from a image
   * @param images the images of passage
   * @return optional long
   */
  def timeYellow(images: Seq[VehicleImage]): Option[Long] = {
    images.find(_.imageType.toString == VehicleImageType.RedLight.toString).flatMap(_.timeYellow)
  }

}
