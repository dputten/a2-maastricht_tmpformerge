package csc.sectioncontrol.enforce.nl.casefiles

/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

import csc.sectioncontrol.enforce.common.nl.messages.HeaderInfo
import csc.sectioncontrol.enforce.common.nl.services.{ SectionControl, Service }

/*
Represents the TC VS v3.3
 */
case class TCVS33() extends CaseFile {
  /**
   * indicates if the one case file record must contain additional info
   * @return
   */
  def showExtraInfo: Boolean = false

  /**
   * the last time value of a mtmInfo file differs per file format. (23:59:59)
   * @param dayEnd long represent the end of the day (time)
   * @return the given day end minus 1 millisecond
   */
  override def mtmInfoLastMoment(dayEnd: Long): Long = dayEnd - 1000 //must end 23:59:59

  /**
   * the 'standby' text for the mtmInfo output file.
   * @return
   */
  override def mtmInfoStandBy: String = "standby"

  /**
   * returns the correct header that autostats must contain. this can differs given service
   * @param service corridor service
   * @param totalAutomatic total amount of auto violations
   * @param totalManual total amount of manual violations
   * @param totalMobi total amount of mobi violations
   * @return a sequence of lines that represents the header of the autostats file
   */
  def createStats(service: Service, totalAutomatic: Int, totalManual: Int, totalMobi: Int): Seq[String] = {
    Seq(
      makeLabel("Totaal aantal beelden") + (totalManual + totalAutomatic),
      makeLabel("Nummerbord onleesbaar") + totalManual,
      makeLabel("Geldige kentekens") + totalAutomatic,
      makeLabel("Automatisch herk. zaken") + totalAutomatic)
  }

  /**
   * returns the correct header that each output of a case file must contain. this differs per service
   * @param service corridor service
   * @param header the values of that must be added to the header static data
   * @return a sequence of lines that represents the header of a case file
   */
  def createHeader(service: Service, header: HeaderInfo): Seq[String] = {
    service match {
      case x: SectionControl ⇒
        Seq(
          "# Trajectcontrolesysteem %s".format(header.systemName),
          "# Categorie %s meetmiddel".format(header.methodType.category),
          "# NMi typegoedkeuringsnummer %s".format(header.methodType.typeCertificate.id),
          "# Serienummer %s".format(header.serialNumber),
          "# Snelheden in %s".format(header.methodType.unitSpeed),
          "# Trajectlengtes in %s".format(header.methodType.unitLength))
      case _ ⇒ Nil
    }
  }
}