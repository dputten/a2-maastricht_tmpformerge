package csc.sectioncontrol.enforce.nl.casefiles

/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

import csc.sectioncontrol.enforce.violations.Implicits
import csc.sectioncontrol.enforce.common.nl.messages.HeaderInfo
import csc.sectioncontrol.enforce.common.nl.services.{ RedLight, SpeedFixed, SectionControl, Service }

/*
abstract class that represent the interface for each case file version must implement
 */
abstract class CaseFile {

  import Implicits._

  /**
   * indicates if the one case file record must contain additional info
   * @return
   */
  def showExtraInfo: Boolean

  /**
   * the 'standby' text with the mtmInfo file differs per file format.
   * This returns the correct name used for the given service
   * @return
   */
  def mtmInfoStandBy: String = ""

  /**
   * the last time value of a mtmInfo file differs per file format. (24:00:00 or 23:59:59)
   * @param dayEnd long represent the end of the day (time)
   * @return the the given dayEnd of a slight modification based on the case file version
   */
  def mtmInfoLastMoment(dayEnd: Long): Long = dayEnd

  /**
   * returns the correct header that each output of a case file must contain. this differs per case
   * file version and the service given
   * @param service corridor service
   * @param header the values of that must be added to the header static data
   * @return a sequence of lines that represents the header of a case file
   */
  def createHeader(service: Service, header: HeaderInfo): Seq[String]

  def createHeader(service: Service, header: HeaderInfo, interfaceVersion: String): Seq[String] = {
    service match {
      case x: SectionControl ⇒
        Seq(
          "# Trajectcontrolesysteem %s".format(header.systemName),
          "# Categorie %s meetmiddel".format(header.methodType.category),
          "# Typegoedkeuringsnummer %s".format(header.methodType.typeCertificate.id),
          "# Serienummer %s".format(header.serialNumber),
          "# Interfaceversie %s".format(interfaceVersion),
          "# Snelheden in %s".format(header.methodType.unitSpeed),
          "# Trajectlengtes in %s".format(header.methodType.unitLength))
      case x: SpeedFixed ⇒
        Seq(
          "# Snelheidscontrole %s".format(header.systemName),
          "# Categorie %s meetmiddel".format(header.methodType.category),
          "# Typegoedkeuringsnummer %s".format(header.methodType.typeCertificate.id),
          "# Serienummer %s".format(header.serialNumber),
          "# Interfaceversie %s".format(interfaceVersion),
          "# Snelheden in %s".format(header.methodType.unitSpeed))
      case x: RedLight ⇒
        Seq(
          "# Roodlichtcontrole %s".format(header.systemName),
          "# Categorie %s meetmiddel".format(header.methodType.category),
          "# Typegoedkeuringsnummer %s".format(header.methodType.typeCertificate.id),
          "# Serienummer %s".format(header.serialNumber),
          "# Interfaceversie %s".format(interfaceVersion),
          "# Rood- en geeltijden in %s".format(header.methodType.unitRedLight))
      case _ ⇒ Nil
    }

  }
  /**
   * returns the correct header that autostats must contain. this can differs per case
   * file version and the service given
   * @param service corridor service
   * @param totalAutomatic total amount of auto violations
   * @param totalManual total amount of manual violations
   * @param totalMobi total amount of mobi violations
   * @return a sequence of lines that represents the header of the autostats file
   */
  def createStats(service: Service, totalAutomatic: Int, totalManual: Int, totalMobi: Int): Seq[String]

  //util function that pads strings with empty space
  protected def makeLabel(string: String) = string.padRight(25, ' ') + "=\t"
}