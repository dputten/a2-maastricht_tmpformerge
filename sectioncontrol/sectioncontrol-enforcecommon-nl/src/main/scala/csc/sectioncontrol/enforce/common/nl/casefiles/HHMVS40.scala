package csc.sectioncontrol.enforce.nl.casefiles

/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

import csc.sectioncontrol.enforce.common.nl.services._
import csc.sectioncontrol.enforce.common.nl.messages.HeaderInfo

/*
Represents the IRS HHM v4.0.
 */
case class HHMVS40() extends CaseFile {
  /**
   * indicates if the one case file record must contain additional info
   * @return
   */
  def showExtraInfo: Boolean = true

  /**
   * the 'standby' text for the mtmInfo output file.
   * @return
   */
  override def mtmInfoStandBy: String = "stand-by"

  /**
   * returns the correct header that autostats must contain. this can differs given service
   * @param service corridor service
   * @param totalAutomatic total amount of auto violations
   * @param totalManual total amount of manual violations
   * @param totalMobi total amount of mobi violations
   * @return a sequence of lines that represents the header of the autostats file
   */
  def createStats(service: Service, totalAutomatic: Int, totalManual: Int, totalMobi: Int): Seq[String] = {
    Seq(
      makeLabel("Totaal aantal beelden") + (totalManual + totalAutomatic + totalMobi),
      makeLabel("Nummerbord onleesbaar") + totalManual,
      makeLabel("Geldige kentekens") + totalAutomatic,
      makeLabel("Automatisch herk. zaken") + totalAutomatic,
      makeLabel("Mobiel verwerkte zaken") + totalMobi)
  }

  /**
   * returns the correct header that each output of a case file must contain. this differs per service
   * @param service corridor service
   * @param header the values of that must be added to the header static data
   * @return a sequence of lines that represents the header of a case file
   */
  def createHeader(service: Service, header: HeaderInfo): Seq[String] = createHeader(service, header, "v1.4")
}