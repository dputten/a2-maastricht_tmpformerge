/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.common.nl.violations

import csc.sectioncontrol.enforce.violations.Checks._
import csc.sectioncontrol.messages.certificates.ComponentCertificate
import csc.sectioncontrol.enforce.Enumerations.RoadType
import csc.sectioncontrol.enforce.common.nl.messages.HeaderInfo
import csc.sectioncontrol.enforce.register.CountryCodes

/**
 * Case class to hold values that are constant for all Violations during an entire EnforcementSession
 * on a Corridor.
 *
 * @author Maarten Hazewinkel
 */
case class ViolationGroupParameters(systemId: String,
                                    systemName: String,
                                    countryCodes: CountryCodes,
                                    routeId: String,
                                    corridorInfo: CorridorInfo,
                                    mtmRouteId: String,
                                    violationsDate: Long,
                                    radarCode: Int,
                                    pshTmIdFormat: String,
                                    hardwareSoftwareSeals: Seq[ComponentCertificate],
                                    headerInfo: HeaderInfo,
                                    roadType: RoadType.Value = RoadType.Highway,
                                    roadPart: String,
                                    insideTown: Boolean = false,
                                    dutyType: Option[String] = None,
                                    deploymentCode: Option[String] = None,
                                    pshtmId: String) {
  require(allNotNull(systemId, routeId, corridorInfo, mtmRouteId, roadType,
    dutyType, deploymentCode, pshTmIdFormat,
    hardwareSoftwareSeals, headerInfo),
    nullValuesErrorMessage)
  require(systemId.length > 0,
    "systemId must be filled (calibration test report contents)")
  require(routeId.length == 4,
    "routeId must be 4 characters long (main export directory name part TTTT)")
  require("^[a-z][0-9]{3}[a-z]$".r.findFirstIn(mtmRouteId).isDefined,
    "mtmRouteId must be like x999x (mtm export file name)")
  require(dutyType.getOrElse("xxxx").length <= 4,
    "dutyType must be at most 4 characters (Soort dienst)")
  require(deploymentCode.getOrElse("xx").length <= 2,
    "deploymentCode must be at most 2 characters (Inzet)")
  require(radarCode >= 0 && radarCode <= 9,
    "radarCode must be between 0 and 9 (Radarcode)")
  require(hardwareSoftwareSeals.length >= 2,
    "At least one software and one hardware ComponentCertificate must be specified (Hard- en softwarezegels)")
}