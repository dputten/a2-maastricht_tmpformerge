/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.common.nl.violations

import csc.sectioncontrol.enforce.violations.Checks._

/**
 * Case class to hold the details on a single Corridor that are relevant for Violation registration.
 *
 * @author Maarten Hazewinkel
 */
case class CorridorInfo(corridorId: Int,
                        corridorEntryLocation: String,
                        corridorExitLocation: String,
                        corridorLength: Double,
                        roadCode: Int,
                        locationCode: String,
                        locationLine1: String,
                        locationLine2: Option[String] = None,
                        drivingDirectionFrom: Option[String] = None,
                        drivingDirectionTo: Option[String] = None) {
  require(allNotNull(corridorEntryLocation, corridorExitLocation, drivingDirectionFrom, drivingDirectionTo,
    locationLine1, locationLine2),
    nullValuesErrorMessage)
  require(0 <= corridorId && corridorId <= 9999,
    "corridorId must be between 0 and 9999 (main export directory name part nnnn)")
  require(corridorEntryLocation.length >= 1 && corridorEntryLocation.length <= 6,
    "corridorEntryLocation must be between 1 and 6 characters (Kmp passagepunt)")
  require(corridorExitLocation.length >= 1 && corridorExitLocation.length <= 6,
    "corridorExitLocation must be between 1 and 6 characters (Kmp tweede passagepunt)")
  require(corridorLength > 0,
    "corridorLength must be greater than 0 (Lengte controletraject)")
  require(roadCode >= 0 && roadCode <= 9999,
    "roadCode must be between 0 and 9999 (Wegcode)")
  require(drivingDirectionFrom.getOrElse("x").length <= 25,
    "drivingDirectionFrom must be at most 25 characters (Richting van)")
  require(drivingDirectionTo.getOrElse("x").length <= 25,
    "drivingDirectionTo must be at most 25 characters (Richting naar)")
  require(locationLine1.length >= 1 && locationLine1.length <= 45,
    "locationLine1 must be at between 1 and 45 characters (Locatie1)")
  require(locationLine2.getOrElse("x").length <= 45,
    "locationLine2 must be at most 45 characters (Locatie2)")
  require(locationCode.matches("[0-9 ]{5}"),
    "locationCode must exist of 5 numbers and/or spaces (Pleegplaats")
}
