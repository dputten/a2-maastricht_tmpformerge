/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

package csc.sectioncontrol.enforce.common.nl.services

import csc.sectioncontrol.enforce.common.nl.violations.VehicleSpeed
import csc.sectioncontrol.messages.VehicleImageType.VehicleImageType
import csc.sectioncontrol.messages._
import csc.sectioncontrol.storage.ServiceType

/**
 * Represents the base class for a service (RedLight, SectionControl etc)
 */
abstract class Service {

  type ImageRef = Option[Pair[VehicleImageType, VehicleMetadata]]
  //filename that must be created within each auto/mobi and hand folder.
  val flgFileName: Option[String]

  // TODO review logic in light of ambition level. Current implementation disables enforcement if
  // dynamic speed differs from the speed set in the schedule. This according to Req 27.
  // Previous implementation used the lower of the schedule speed and the dynamic speed according to
  // ambition level 4.
  val dynamicEnforcement: Boolean

  /**
   * the type of this service
   * @return ServiceType
   */
  def serviceType: ServiceType.Value

  /**
   * indicates of the second image for output of the case file is required
   * @return boolean
   */
  def requireSecondImage: Boolean

  /**
   * used for ctres report (drempeltijd)
   * @return 0
   */
  def thresholdTime: Int = 0

  /**
   * used for ctres report (drempelsnelheid)
   * @return 0
   */
  def thresholdSpeed(enforcedSpeedLimit: Int = 0): Int = 0

  /**
   * used for ctres report (pardontijd)
   * @return 0
   */
  def pardonTime: Int = 0

  /**
   * determines if a given classified/vehicle record is in violation. Not used by this service
   * @param vcr the vehicle classified record
   * @param speed all information needed to determine a violation
   * @return a tuple of boolean and the speed. THe boolean indicates if it is a violation. The Int represent the enforced speed
   */
  def isViolation(vcr: VehicleClassifiedRecord, speed: VehicleSpeed): (Boolean, Int)

  def getImage1Ref(entry: VehicleMetadata, exit: Option[VehicleMetadata]): Service.ImageRef

  def getImage2Ref(entry: VehicleMetadata, exit: Option[VehicleMetadata]): Service.ImageRef

}

object Service {

  type ImageRef = Option[Pair[VehicleImageType, VehicleMetadata]]

  def getOriginalImage(ref: ImageRef): Option[VehicleImage] = {
    //mapping the target image to its original source
    val tp = getOriginalImageType(ref.get._1)
    ref.get._2.getImage(tp)
  }

  def getOriginalImageType(tp: VehicleImageType): VehicleImageType = tp match {
    case VehicleImageType.OverviewRedLight ⇒ VehicleImageType.Overview
    case VehicleImageType.OverviewSpeed    ⇒ VehicleImageType.Overview
    case any                               ⇒ any
  }

}