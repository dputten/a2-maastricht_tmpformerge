/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.common.nl.messages

import csc.sectioncontrol.messages._
import csc.sectioncontrol.enforce.nl.casefiles.CaseFile
import scala.Predef._
import csc.sectioncontrol.common.Rounder
import java.text.SimpleDateFormat
import csc.sectioncontrol.enforce.common.nl.services._
import csc.sectioncontrol.enforce.common.nl.services.SpeedFixed
import csc.sectioncontrol.enforce.common.nl.services.SectionControl
import csc.sectioncontrol.enforce.violations.Implicits._
import csc.sectioncontrol.messages.IndicatorReason
import csc.sectioncontrol.messages.VehicleImage
import csc.sectioncontrol.enforce.common.nl.services.RedLight
import csc.sectioncontrol.enforce.common.nl.violations.ViolationGroupParameters
import csc.sectioncontrol.enforce.register.CountryCodes
import csc.sectioncontrol.enforce.common.nl.casefiles.HHMVS20

/**
 * Case class to hold specific details of a single Violation being registered.
 *
 * @author Maarten Hazewinkel
 */
case class ViolationData(countryCodes: CountryCodes,
                         processingIndicator: ProcessingIndicator.Value,
                         reason: IndicatorReason,
                         exportLicensePlate: String,
                         vehicleClass: Option[VehicleCode.Value],
                         measuredSpeed: Int,
                         vehicleCountryCode: Option[String],
                         indicatedSpeed: SpeedIndicationSign,
                         corridorEntryTime: Long,
                         corridorExitTime: Long,
                         violationJarSha: String, //SHA of the JAR file
                         military: Boolean = false,
                         manualAccordingToSpec: Boolean = false,
                         reportingOfficerCode: String,
                         pshTmId: String,
                         indicationRoadworks: Option[Boolean] = None,
                         indicationDanger: Option[Boolean] = None,
                         indicationActualWork: Option[Boolean] = None,
                         invertDrivingDirection: Option[Boolean] = None,
                         textCode: Option[Int] = None,
                         images: Seq[VehicleImage]) extends Rounder {
  import ProcessingIndicator._

  require(processingIndicator != null, "processingIndicator is null")
  require(exportLicensePlate != null, "exportLicensePlate is null")
  require(vehicleClass != null, "vehicleClass is null")
  require(vehicleCountryCode != null, "vehicleCountryCode is null")
  require(indicatedSpeed != null, "indicatedSpeed is null")
  require(reportingOfficerCode != null, "reportingOfficerCode is null")
  require(indicationRoadworks != null, "indicationRoadworks is null")
  require(indicationDanger != null, "indicationDanger is null")
  require(indicationActualWork != null, "indicationActualWork is null")
  require(invertDrivingDirection != null, "invertDrivingDirection is null")
  require(textCode != null, "textCode is null")

  if (processingIndicator == Automatic || processingIndicator == Mobi)
    require(exportLicensePlate.length > 0,
      "licensePlate (Kenteken voertuig) is required for Automatic or Other processingIndicator (Indicatie verwerking) " + this)
  require(exportLicensePlate.length <= 12,
    "licensePlate must be at most 12 characters (Kenteken voertuig). Formatted licensePlate = " + exportLicensePlate)
  if (processingIndicator == Automatic || processingIndicator == Mobi)
    require(vehicleClass.isDefined && vehicleClass.get != null,
      "vehicleClass (Voertuigklasse) is required for Automatic or Other processingIndicator (Indicatie verwerking) " + this)
  require(measuredSpeed > 0,
    "measuredSpeed must be greater than 0 (Gemeten snelheid) " + this)
  if (processingIndicator == Automatic || processingIndicator == Mobi)
    require(vehicleCountryCode.isDefined && vehicleCountryCode.get != null,
      "vehicleCountryCode (Landcode voertuig) is required for Automatic or Other processingIndicator (Indicatie verwerking) " + this)
  indicationRoadworks match {
    case Some(true) ⇒
      require(indicatedSpeed.indicatedSpeed.isDefined,
        "if indicationRoadworks (Indicatie werkzaamheden) is true then indicatedSpeed must also be set (Indicatiebord) " + this)
      require(indicationDanger.isDefined,
        "if indicationRoadworks (Indicatie werkzaamheden) is true then indicationDanger must also be set (Indicatie gevaarselement) " + this)
      require(indicationActualWork.isDefined,
        "if indicationRoadworks (Indicatie werkzaamheden) is true then indicationActualWork must also be set (Indicatie werkelijk gewerkt) " + this)
    case Some(false) ⇒
      require(indicationDanger.isEmpty,
        "if indicationRoadworks (Indicatie werkzaamheden) is false then indicationDanger must not be set (Indicatie gevaarselement) " + this)
      require(indicationActualWork.isEmpty,
        "if indicationRoadworks (Indicatie werkzaamheden) is false then indicationActualWork must not be set (Indicatie werkelijk gewerkt) " + this)
    case None ⇒
  }
  if (indicatedSpeed.signIndicator) {
    require(indicatedSpeed.indicatorType.isDefined, "indicatorType (Soort Bord) must be present when signIndicator (indicatie bord) is true")
    require(indicatedSpeed.indicatedSpeed.isDefined, "indicatedSpeed (Bordsnelheid) must be present when signIndicator (indicatie bord) is true")
  }
  if (indicatedSpeed.indicatedSpeed.isDefined)
    require(indicatedSpeed.indicatedSpeed.get >= 0 && indicatedSpeed.indicatedSpeed.get < 1000, "indicatedSpeed (Bordsnelheid) must be besteen 0 and 999")

  textCode.foreach { (code) ⇒
    require(code >= 0 && code <= 99,
      "textCode must be between 0 and 99 or not be set (Codetekst) " + this)
  }

  def time = corridorExitTime

  def makeStatusCode(usedCountryCode: String, indicatorReason: IndicatorReason): String = {
    def makeBitmask = {
      var reason = indicatorReason
      if (exportLicensePlate.isEmpty) reason = reason.setUnreliableLicense()
      if (vehicleClass.isEmpty) reason = reason.setUnreliableClassification()
      if (usedCountryCode.trim().isEmpty) reason = reason.setUnreliableCountry()
      reason.mask.toString
    }

    if (processingIndicator == Mobi) "B" else makeBitmask
  }

  /**
   * formats red- and yellow time to SS.M
   * @param time option long
   * @return 99.9 if value is higher than 99.9 else value in SS.M format
   */
  def formatRedYellowTime(time: Option[Long]): String = {
    //reduce the number of decimals to 1. No rounding may occur
    val toOneDecimal = ((time.getOrElse(0L) / 100) * 100).toDouble / 1000

    //everything higher than 99.9 must become 99.9
    val lowestValue = toOneDecimal.min(99.9)

    //always to SS.M => 9.1 must be 09.1
    lowestValue.toString.padLeft(4, '0')
  }

  def toDataLine(recordNum: String, parameters: ViolationGroupParameters, caseFile: CaseFile, service: Service): String = {
    val DateFormat = new SimpleDateFormat("yyyyMMdd")
    val TimeFormat = new SimpleDateFormat("HHmm")
    val DetailedTimeFormat = new SimpleDateFormat("HHmmss.SSS")
    val formattedCountry = formattedCountryCode

    val (redLight, speedFixed, sectionControl, targetGroup) = service match {
      case x: RedLight       ⇒ (Some(x), None, None, None)
      case x: SpeedFixed     ⇒ (None, Some(x), None, None)
      case x: SectionControl ⇒ (None, None, Some(x), None)
      case x: TargetGroup    ⇒ (None, None, None, Some(x))
      case _                 ⇒ (None, None, None, None)
    }

    val (isRedLight, isSpeedFixed, isSectionControl) = service match {
      case x: RedLight       ⇒ (true, false, false)
      case x: SpeedFixed     ⇒ (false, true, false)
      case x: SectionControl ⇒ (false, false, true)
      case _                 ⇒ (false, false, false)
    }

    def lengthSectionControl = if (isSectionControl)
      parameters.corridorInfo.corridorLength.toInt.toString.padLeft(5, ' ')
    else
      "".padLeft(5, ' ')

    def exitTime = if (isSectionControl)
      pad(DetailedTimeFormat(corridorExitTime), 10)
    else
      "".padLeft(10, ' ')

    def exitLocation = if (isSectionControl)
      parameters.corridorInfo.corridorExitLocation.padLeft(6, ' ')
    else
      "".padLeft(6, ' ')

    def vehicleSpeed = if (isRedLight)
      pad("", 3)
    else
      pad(measuredSpeed, 3)

    def speedSign = if ((isSpeedFixed || isSectionControl) && indicatedSpeed.signIndicator)
      pad(indicatedSpeed.indicatedSpeed.getOrElse(0), 3)
    else pad(0, 3)

    def signIndicator = {
      val ch = if (isSpeedFixed || isSectionControl) {
        if (indicatedSpeed.signIndicator) "J" else "N"
      } else " "
      pad(ch, 1)
    }

    def indicatorType = {
      val ch = if (isRedLight) {
        "  "
      } else pad(if (indicatedSpeed.signIndicator) indicatedSpeed.indicatorType.get.toString else "", 2)
      pad(ch, 2)
    }

    val part1 = List(pad(processingIndicator match {
      // Indicatie verwerking
      case Automatic ⇒ "A"
      case Manual    ⇒ "."
      case Mobi      ⇒ "V"
    }, 1),

      //Recordnummer
      pad(recordNum, 4),

      //Datum overtreding
      pad(DateFormat(time), 8),

      //Tijdstip overtreding
      pad(TimeFormat(time), 4),

      //Kenteken voertuig
      pad(exportLicensePlate, 12),

      //Voertuigklasse
      pad(if (vehicleClass.isDefined) vehicleClass.get.toString else "", 2),

      //Gemeten snelheid
      vehicleSpeed,

      //Landcode voertuig
      formattedCountry,

      //Bordsnelheid
      speedSign,

      //Indicatiebord
      signIndicator,

      //Soort bord
      indicatorType,

      //Kmp passagepunt (of huisnummer)
      parameters.corridorInfo.corridorEntryLocation.padLeft(6, ' '),

      //Tijdstip passage
      pad(DetailedTimeFormat(corridorEntryTime), 10),

      //Kmp tweede passagepunt (of huisnummer)
      exitLocation,

      //Tijdstip passage tweede passagepunt
      exitTime,

      //Lengte controletraject
      lengthSectionControl,
      //parameters.corridorInfo.corridorLength.toInt.toString.padLeft(5, ' '),

      //Statuscode
      pad(makeStatusCode(formattedCountry, reason), 1),

      //Locatie1
      pad(parameters.corridorInfo.locationLine1, 45),

      //Locatie2
      pad(parameters.corridorInfo.locationLine2.getOrElse(""), 45),

      //Wegcode
      pad(parameters.corridorInfo.roadCode, 4),

      //Soort weg
      pad(parameters.roadType.toString, 1),

      //Indicatie binnen de bebouwde kom
      pad(if (parameters.insideTown) "J" else "N", 1))

    val (redTime: String, yellowTime: String) = service match {
      case redLight: RedLight ⇒
        (
          formatRedYellowTime(redLight.timeRed(images)),
          formatRedYellowTime(redLight.timeYellow(images)))
      case _ ⇒ ("", "")
    }

    val part2: List[String] = if (caseFile.showExtraInfo) {
      List(
        // Indicatie werkzaamheden
        pad(if (indicationRoadworks.isEmpty) "" else if (indicationRoadworks.get) "J" else "N", 1),

        // Indicatie gevaarselement
        pad(if (indicationDanger.isEmpty) "" else if (indicationDanger.get) "J" else "N", 1),

        // Indicatie werkelijk gewerkt
        pad(if (indicationActualWork.isEmpty) "" else if (indicationActualWork.get) "J" else "N", 1),

        // Wisselen rijrichting
        pad(if (invertDrivingDirection.getOrElse(false)) "W" else "", 1),

        // Roodtijd
        pad(redTime, 4),

        // Geeltijd
        pad(yellowTime, 4),

        // Verbalisantcode
        pad(reportingOfficerCode, 6))
    } else
      Nil

    val part3: List[String] = List(pad(parameters.dutyType.getOrElse(""), 4),
      pad(parameters.deploymentCode.getOrElse(""), 2),
      pad(parameters.corridorInfo.locationCode, 5),
      pad(parameters.corridorInfo.drivingDirectionFrom.getOrElse(""), 25),
      pad(parameters.corridorInfo.drivingDirectionTo.getOrElse(""), 25),
      pad(parameters.radarCode, 1),
      caseFile match {
        case vs20: HHMVS20 ⇒ if (manualAccordingToSpec) " J" else " N"
        case _             ⇒ if (textCode.isDefined) pad(textCode.get, 2) else pad("", 2)
      })

    val factNumber = redLight.map(_.factNumber).orElse(targetGroup.map(_.factNumber)).getOrElse("")

    val part4: List[String] = if (caseFile.showExtraInfo) {
      List(
        pad(if (targetGroup.isDefined) "D" else "", 1),
        pad(factNumber, 8),
        pad(pshTmId, 10).take(10))
    } else
      Nil

    val part5: List[String] = List(parameters.hardwareSoftwareSeals.map(s ⇒ s.name + " " + s.checksum).mkString(start = "", sep = ",", end = ", "))

    (part1 ++ part2 ++ part3 ++ part4 ++ part5).mkString(" ")
  }

  def formattedCountryCode: String = {
    val value = if (military) {
      "MIL"
    } else {
      if (vehicleCountryCode.isDefined) {
        val unCode = countryCodes.translateIsoToUN(vehicleCountryCode.get)
        unCode.substring(0, math.min(3, unCode.length))
      } else {
        ""
      }
    }
    pad(value, 3)
  }

  private def pad(value: Int, fieldSize: Int) = value.toString.padLeft(fieldSize, '0')

  private def pad(value: String, fieldSize: Int) = value.padRight(fieldSize, ' ')
}
