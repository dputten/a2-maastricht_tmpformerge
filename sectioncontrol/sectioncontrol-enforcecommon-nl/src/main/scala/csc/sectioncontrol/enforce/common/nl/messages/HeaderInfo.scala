/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.common.nl.messages

import csc.sectioncontrol.messages.certificates.MeasurementMethodType

/**
 * Contains all data required to fill the header of a service
 * @param systemName name of system
 * @param serialNumber serial number
 */
case class HeaderInfo(systemName: String, serialNumber: String, methodType: MeasurementMethodType)