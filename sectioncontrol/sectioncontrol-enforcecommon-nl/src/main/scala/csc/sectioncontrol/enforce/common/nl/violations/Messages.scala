/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.common.nl.violations

import csc.sectioncontrol.enforce.register.{ CountryCodes, MinimumSeparation }
import csc.sectioncontrol.messages.certificates.{ ComponentCertificate, LocationCertificate, MeasurementMethodInstallationType }

import collection.SortedSet
import csc.sectioncontrol.messages._
import csc.sectioncontrol.enforce.{ Enumerations, SpeedIndicatorStatus }
import csc.sectioncontrol.enforce.nl.casefiles.{ CaseFile, HHMVS40 }
import csc.sectioncontrol.enforce.common.nl.services.Service
import csc.sectioncontrol.common.DayUtility
import csc.sectioncontrol.messages.SpeedIndicatorType
import org.slf4j.LoggerFactory

/**
 * An enriched violation record that contains all the additional data needed for registration
 * with the LPTV.
 *
 * @param vvr The basic VehicleViolationRecord
 * @param corridorData The (fixed) configuration information on the corridor in which the violation occurred
 * @param globalConfig The global system configuration information
 * @param corridorSchedule The configuration for the enforcement session/schedule in effect for time of the violation
 */
case class Violation(vvr: VehicleViolationRecord,
                     rawLicense: Option[String],
                     corridorData: CorridorData,
                     globalConfig: SystemGlobalConfig,
                     corridorSchedule: CorridorSchedule,
                     pshTmId: String,
                     reportingOfficerCode: String) extends MinimumSeparation.ViolationData {

  def time = vvr.classifiedRecord.speedRecord.exit.getOrElse(vvr.classifiedRecord.speedRecord.entry).eventTimestamp
  def license = vvr.classifiedRecord.speedRecord.entry.license.map(_.value).getOrElse("")
  def measuredSpeed = vvr.classifiedRecord.speedRecord.speed
  def enforcedSpeed = vvr.enforcedSpeed
  def exportLicense = vvr.classifiedRecord.licensePlate
  def separationLicense = {
    //TCM-100 use the ARH result when filtering violation on minimum separation
    val arhLicense = vvr.recognizeResults.find(_.recognizeId == "ARH_entry").map(_.recognize.license.map(_.value).getOrElse(""))
    if (arhLicense.isEmpty) {
      val log = LoggerFactory.getLogger(getClass)
      log.warn("Violation has no arh recognition assuming old version using license %s".format(license))
    }
    arhLicense.getOrElse(license)
  }
}

case class IntradaCheck(vvr: VehicleViolationRecord) extends MinimumSeparation.ViolationData {

  def time = vvr.classifiedRecord.speedRecord.exit.getOrElse(vvr.classifiedRecord.speedRecord.entry).eventTimestamp
  def license = vvr.classifiedRecord.speedRecord.entry.license.map(_.value).getOrElse("")
  def measuredSpeed = vvr.classifiedRecord.speedRecord.speed
  def enforcedSpeed = vvr.enforcedSpeed
  def exportLicense = vvr.classifiedRecord.licensePlate
  def separationLicense = {
    //TCM-100 use the ARH result when filtering violation on minimum separation
    val arhLicense = vvr.recognizeResults.find(_.recognizeId == "ARH_entry").map(_.recognize.license.map(_.value).getOrElse(""))
    if (arhLicense.isEmpty) {
      val log = LoggerFactory.getLogger(getClass)
      log.warn("Violation has no arh recognition assuming old version using %s".format(license))
    }
    arhLicense.getOrElse(license)
  }
}

/**
 * Contains the additional information needed to register violations.
 *
 * @param globalConfig The static, global system configuration data
 * @param corridors The static data for all the corridors in the system
 * @param schedules All schedules and time-sensitive data for the corridors
 */
case class SystemConfigurationData(globalConfig: SystemGlobalConfig,
                                   corridors: Seq[CorridorData],
                                   schedules: Seq[CorridorSchedule],
                                   reportingOfficers: Map[Int, ReportingOfficers],
                                   vehicleClassSpeedLimits: Map[VehicleCode.Value, Int],
                                   speedMargins: SortedSet[SpeedMargin])

case class ViolationDeterminationConfigData(corridorServices: Seq[CorridorService],
                                            schedules: Seq[CorridorScheduleData],
                                            vehicleClassSpeedLimits: Map[VehicleCode.Value, Int],
                                            speedMargins: SortedSet[SpeedMargin])

/**
 * Holds the details of when enforcement was switched on and by which officer.
 */
case class ReportingOfficers private[violations] (switchOnAtTimeByOfficer: Seq[(Long, String)]) {
  def getReportingOfficerForTime(time: Long): Option[String] = mostRecentReportingOfficerForTime(time)

  /**
   * find the officer that switched system to enforce for the first time for this day
   *
   * @param time point of time within the day
   * @return the requested reporting officer
   */
  def getReportingOfficerForDay(time: Long): Option[String] = {
    getReportingOfficersForDay(time) match {
      case Nil     ⇒ mostRecentReportingOfficerForTime(time)
      case default ⇒ default.reverse.headOption
    }
  }

  private def mostRecentReportingOfficerForTime(time: Long): Option[String] =
    switchOnAtTimeByOfficer.find(so ⇒ so._1 < time).map(_._2)

  private def getReportingOfficersForDay(time: Long): Seq[String] = {
    val (dayStart, _) = DayUtility.calculateDayPeriod(time)
    switchOnAtTimeByOfficer.filter(so ⇒ so._1 > dayStart && so._1 < time).map(_._2)
  }
}
object ReportingOfficers {
  def apply(reportingOfficers: Map[Long, String]): ReportingOfficers = {
    ReportingOfficers(reportingOfficers.toSeq.sortBy(ro ⇒ -ro._1))
  }
}

/**
 * Static system configuration information. Mostly needed to enrich basic violation data
 * so that it can be sent to LPTV in 'zaak' files
 *
 * @param countryCodes a Translation table to convert iso country codes to UN codes
 * @param routeId The route id
 * @param mtmRouteId The route id that is part of the name of the 'mtminfo' file
 * @param certificateTypes System measurementMethodTypes including names and (calculated) values
 * @param locationCertificates The System location certificates
 * @param systemName The values for the header lines of 'zaak' files
 * @param minimumViolationTimeSeparation The minimum time allowed between violations for the same license
 * @param serialNumber The system serial number
 */
case class SystemGlobalConfig(
  countryCodes: CountryCodes,
  routeId: String,
  mtmRouteId: String,
  roadPart: String,
  certificateTypes: Seq[MeasurementMethodInstallationType],
  locationCertificates: Seq[LocationCertificate],
  activeSoftwareCertificate: ComponentCertificate,
  activeConfigurationCertificate: ComponentCertificate,
  systemName: String,
  minimumViolationTimeSeparation: Long,
  serialNumber: String,
  treatyCountries: Seq[String],
  pshtmId: String)

/**
 * Static corridor configuration information
 *
 * @param corridorId The Id number for the corridor
 * @param entryGantryId The id of the gantry at the beginning of the corridor
 * @param exitGantryId The id of the gantry at the end of the corridor
 * @param entryLocationName The name of the gantry at the beginning of the corridor
 * @param exitLocationName The name of the gantry at the end of the corridor
 * @param length The length of the corridor
 * @param roadCode The 4 digit code of the road
 * @param compressionFactor Compression factor for camera images for this corridor
 * @param nrDaysKeepViolations How long to keep violations stored for this corridor
 * @param dynaMaxScanInterval How frequently is the dynamic speed indication scanned for changes
 * @param pardonTimeEsa The pardon time used when the dynamic speed indication changes
 * @param locationCode A 5 digit code of the road
 * @param locationLine1 The first line of the description of the corridor location
 * @param locationLine2 An optional second line of the description of the corridor location
 * @param drivingDirectionFrom The direction from which the traffic is coming
 * @param drivingDirectionTo The direction to which the traffic is going
 * @param roadType The type of road. Default Highway
 * @param service The service of the corridor.
 * @param insideTown Whether the road is inside a city/town. Default false
 */
case class CorridorData(corridorId: Int, //== CorridorData(corridorId = corridor.info.corridorId,.....lekker handig als er ook een corridor.Id is.
                        entryGantryId: String,
                        exitGantryId: String,
                        entryLocationName: String,
                        exitLocationName: String,
                        length: Double,
                        roadCode: Int,
                        radarCode: Int,
                        compressionFactor: Int,
                        nrDaysKeepViolations: Int,
                        dynaMaxScanInterval: Int,
                        pardonTimeEsa: Int,
                        locationCode: String,
                        locationLine1: String,
                        locationLine2: Option[String] = None,
                        drivingDirectionFrom: Option[String] = None,
                        drivingDirectionTo: Option[String] = None,
                        roadType: Enumerations.RoadType.Value = Enumerations.RoadType.Highway,
                        service: Service,
                        caseFile: CaseFile = new HHMVS40(),
                        insideTown: Boolean = false)

case class CorridorService(corridorId: Int, service: Option[Service])

case class VehicleSpeed(measuredSpeed: Int,
                        scheduleSpeedLimit: Int,
                        dynamicSpeedLimit: Either[SpeedIndicatorStatus.Value, Option[Int]],
                        vehicleClassSpeedLimit: Option[Int],
                        speedMargins: Map[Int, Int])

case class SpeedIndicator(signIndicator: Boolean, speedIndicatorType: Option[SpeedIndicatorType.Value]) {
  if (signIndicator)
    require(speedIndicatorType.isDefined, "speedIndicatorType must be present when signIndicator is true")
  else
    require(speedIndicatorType.isEmpty, "speedIndicatorType must be empty when signIndicator is false")
}

/**
 * Time based config information for the violation. Indicates the settings that were in effect at the
 * time of the violation.
 *
 * @param corridorId The corridor to which this schedule applies
 * @param pshTmIdFormat A format to generate a code, unique per day and corridor
 * @param startTime The starting time for this schedule
 * @param endTime The end time for this schedule
 * @param textCode Optional text code for violations
 * @param dutyType Optional duty type code
 * @param deploymentCode Optional deployment code
 * @param indicationRoadworks Optional flag if there is a warning for road works on the road
 * @param indicationDanger Optional flag if there is a warning for danger on the road
 * @param indicationActualWork Optional flag if the is actual work on the road taking place
 * @param invertDrivingDirection Optional flag to indicate that the driving direction is reversed
 * @param speedIndicator signIndicator and optional speedIndicatorType of corridor schedule
 */
case class CorridorSchedule(corridorId: Int,
                            enforcedSpeedLimit: Int,
                            indicatedSpeedLimit: Option[Int], // move to schedule
                            pshTmIdFormat: String,
                            startTime: Long,
                            endTime: Long,
                            textCode: Option[Int] = None,
                            dutyType: Option[String] = None,
                            deploymentCode: Option[String] = None,
                            indicationRoadworks: Option[Boolean] = None,
                            indicationDanger: Option[Boolean] = None,
                            indicationActualWork: Option[Boolean] = None,
                            invertDrivingDirection: Option[Boolean] = None,
                            speedIndicator: SpeedIndicator,
                            supportMsgBoard: Boolean = false)

case class TimeBlock(hour: Int, minutes: Int)

case class CorridorScheduleData(corridorId: Int,
                                enforcedSpeedLimit: Int,
                                startTime: TimeBlock,
                                endTime: TimeBlock,
                                historicDateTimeStamp: Option[Long] = None)

case class SpeedMargin(baseSpeed: Int, margin: Int)
object SpeedMargin {
  implicit val speedMarginOrdering = Ordering.by[SpeedMargin, Int](_.baseSpeed)
}

/**
 * A message produced by auditor to be put to the system log
 */
case class AuditorSystemLog(component: String, systemId: String, message: String, corridor: Option[String])