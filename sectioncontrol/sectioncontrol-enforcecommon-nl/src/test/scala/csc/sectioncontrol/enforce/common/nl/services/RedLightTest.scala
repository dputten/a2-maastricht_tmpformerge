/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

package csc.sectioncontrol.enforce.common.nl.services

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.messages.{ VehicleClassifiedRecord, VehicleImageType, VehicleImage, VehicleMetadata }
import csc.sectioncontrol.storage.ServiceType

class RedLightTest extends WordSpec with MustMatchers {

  "Service RedLight" must {
    val defaultImage = VehicleImage(timestamp = 0, offset = 0L, uri = "", imageType = VehicleImageType.License, checksum = "", timeYellow = None, timeRed = None)
    val images = Seq(
      defaultImage.copy(imageType = VehicleImageType.License, timeYellow = Some(10), timeRed = Some(20)),
      defaultImage.copy(imageType = VehicleImageType.MeasureMethod2Speed, timeYellow = Some(50), timeRed = Some(60)),
      defaultImage.copy(imageType = VehicleImageType.Overview, timeYellow = Some(70), timeRed = Some(80)),
      defaultImage.copy(imageType = VehicleImageType.OverviewRedLight, timeYellow = Some(71), timeRed = Some(81)),
      defaultImage.copy(imageType = VehicleImageType.OverviewSpeed, timeYellow = Some(72), timeRed = Some(82)),
      defaultImage.copy(imageType = VehicleImageType.RedLight, timeYellow = Some(90), timeRed = Some(100)))
    val vmd = VehicleMetadata(lane = null, eventId = "", eventTimestamp = 1, eventTimestampStr = None, images = images, NMICertificate = "", applChecksum = "", serialNr = "SN1234")
    val entry = vmd.copy(images = images)
    val exit = Some(vmd.copy(images = images))

    val service = new RedLight(pardonRedTime = 0, minimumYellowTime = 1, factNumber = "1234")

    "return correct serviceType" in {
      service.serviceType must be(ServiceType.RedLight)
    }
    "indicate that a second image is required" in {
      service.requireSecondImage must be(true)
    }
    "give the correct first image that has vehicle image type is correct" in {
      val image = service.getOriginalImage1(entry, exit)
      //.getFirstImage(entry, exit)._1
      image.isDefined must be(true)
      image.get.imageType must be(VehicleImageType.RedLight)
    }
    "give the correct second image that has vehicle image type is correct" in {
      val image = service.getOriginalImage2(entry, exit)
      image.isDefined must be(true)
      image.get.imageType must be(VehicleImageType.Overview)
    }
    "gives the correct timeYellow value from the image that has vehicle image type 'RedLight'" in {
      val image = service.timeYellow(images)
      image.isDefined must be(true)
      image.get must be(images.find(_.imageType == VehicleImageType.RedLight).get.timeYellow.get)
    }
    "gives the correct timeRed value from the image that has vehicle image type 'RedLight'" in {
      val image = service.timeRed(images)
      image.isDefined must be(true)
      image.get must be(images.find(_.imageType == VehicleImageType.RedLight).get.timeRed.get)
    }
    "gives the correct thresholdTime" in {
      val service = new RedLight(pardonRedTime = 100, minimumYellowTime = 20, factNumber = "1234")
      service.thresholdTime must be(service.minimumYellowTime)
    }
    "gives the correct thresholdSpeed" in {
      val service = new RedLight(pardonRedTime = 100, minimumYellowTime = 20, factNumber = "1234")
      service.thresholdSpeed(30) must be(30)
    }
  }
}
