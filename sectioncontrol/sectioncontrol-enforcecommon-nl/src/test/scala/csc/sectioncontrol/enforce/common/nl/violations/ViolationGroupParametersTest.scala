/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.common.nl.violations

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.messages.certificates.{ TypeCertificate, MeasurementMethodType, ComponentCertificate }
import csc.sectioncontrol.enforce.common.nl.messages.HeaderInfo
import csc.sectioncontrol.enforce.register.CountryCodes

/**
 * @author Maarten Hazewinkel
 */
class ViolationGroupParametersTest extends WordSpec with MustMatchers {
  "ViolationGroupParameters" must {
    lazy val corridorInfo = CorridorInfo(corridorId = 1,
      corridorEntryLocation = "Entry",
      corridorExitLocation = "Exit",
      corridorLength = 1578,
      roadCode = 12,
      drivingDirectionFrom = Some("Beginning"),
      drivingDirectionTo = Some("End"),
      locationLine1 = "Here",
      locationLine2 = None,
      locationCode = "12345")
    lazy val base = ViolationGroupParameters(systemId = "HHM1",
      systemName = "HHM1",
      countryCodes = CountryCodes(),
      routeId = "EG33",
      corridorInfo = corridorInfo,
      mtmRouteId = "a002l",
      violationsDate = System.currentTimeMillis,
      radarCode = 2,
      pshTmIdFormat = "[YY][MM][DD]EG33",
      hardwareSoftwareSeals = List(ComponentCertificate("DeelA", "1234"),
        ComponentCertificate("DeelB", "5678")),
      headerInfo = HeaderInfo("Header1", "Header2", new MeasurementMethodType(typeDesignation = "XX123",
        category = "A",
        unitSpeed = "km/h",
        unitRedLight = "s",
        unitLength = "m",
        restrictiveConditions = "",
        displayRange = "20-250 km/h",
        temperatureRange = "temperatuurbereik -10 to 50 graden Celsius",
        permissibleError = "toelatbare fout 3%",
        typeCertificate = new TypeCertificate("eg33-cert", 3,
          List(ComponentCertificate("matcher1", "blah1"),
            ComponentCertificate("vr2", "blah2"))))),
      roadPart = "L",
      pshtmId = "A120")
    "construct normally with good parameters" in {
      base.systemId must be("HHM1")
    }
    "fail requirement test if any argument is null" in {
      evaluating { base.copy(systemId = null) } must produce[IllegalArgumentException]
      evaluating { base.copy(routeId = null) } must produce[IllegalArgumentException]
      evaluating { base.copy(corridorInfo = null) } must produce[IllegalArgumentException]
      evaluating { base.copy(mtmRouteId = null) } must produce[IllegalArgumentException]
      evaluating { base.copy(roadType = null) } must produce[IllegalArgumentException]
      evaluating { base.copy(dutyType = null) } must produce[IllegalArgumentException]
      evaluating { base.copy(deploymentCode = null) } must produce[IllegalArgumentException]
      evaluating { base.copy(pshTmIdFormat = null) } must produce[IllegalArgumentException]
      evaluating { base.copy(hardwareSoftwareSeals = null) } must produce[IllegalArgumentException]
      evaluating { base.copy(headerInfo = null) } must produce[IllegalArgumentException]
    }
    "fail requirement test on systemId (calibration test report contents)" in {
      evaluating { base.copy(systemId = "") } must produce[IllegalArgumentException]
    }
    "fail requirement test on routeId (main export directory name part TTTT)" in {
      evaluating { base.copy(routeId = "ABC") } must produce[IllegalArgumentException]
      evaluating { base.copy(routeId = "ABCDE") } must produce[IllegalArgumentException]
    }
    "fail requirement test on mtmRouteId (mtm export file name)" in {
      evaluating { base.copy(mtmRouteId = "") } must produce[IllegalArgumentException]
      evaluating { base.copy(mtmRouteId = "a02r") } must produce[IllegalArgumentException]
      evaluating { base.copy(mtmRouteId = "0002l") } must produce[IllegalArgumentException]
    }
    "fail requirement test on dutyType (Soort dienst)" in {
      evaluating { base.copy(dutyType = Some("ABCDE")) } must produce[IllegalArgumentException]
    }
    "fail requirement test on deploymentCode (Inzet)" in {
      evaluating { base.copy(deploymentCode = Some("ABC")) } must produce[IllegalArgumentException]
    }
    "fail requirement test on radarCode (Radarcode)" in {
      evaluating { base.copy(radarCode = -1) } must produce[IllegalArgumentException]
      evaluating { base.copy(radarCode = 10) } must produce[IllegalArgumentException]
    }
    "fail requirement test on hardwareSoftwareSeals (Hard- en softwarezegels)" in {
      evaluating { base.copy(hardwareSoftwareSeals = List()) } must produce[IllegalArgumentException]
      val seal: ComponentCertificate = ComponentCertificate("A", "1")
      evaluating { base.copy(hardwareSoftwareSeals = List(seal)) } must produce[IllegalArgumentException]
    }
  }

  "CorridorInfo" must {
    lazy val base = CorridorInfo(corridorId = 1,
      corridorEntryLocation = "Entry",
      corridorExitLocation = "Exit",
      corridorLength = 1578,
      roadCode = 12,
      drivingDirectionFrom = Some("Beginning"),
      drivingDirectionTo = Some("End"),
      locationLine1 = "Here",
      locationLine2 = None,
      locationCode = "12345")
    "construct normally with good parameters" in {
      base.locationLine1 must be("Here")
    }
    "fail requirement test if any argument is null" in {
      evaluating { base.copy(corridorEntryLocation = null) } must produce[IllegalArgumentException]
      evaluating { base.copy(corridorExitLocation = null) } must produce[IllegalArgumentException]
      evaluating { base.copy(drivingDirectionFrom = null) } must produce[IllegalArgumentException]
      evaluating { base.copy(drivingDirectionTo = null) } must produce[IllegalArgumentException]
      evaluating { base.copy(locationLine1 = null) } must produce[IllegalArgumentException]
      evaluating { base.copy(locationLine2 = null) } must produce[IllegalArgumentException]
    }
    "fail requirement test on corridorId" in {
      evaluating { base.copy(corridorId = -1) } must produce[IllegalArgumentException]
      evaluating { base.copy(corridorId = 10000) } must produce[IllegalArgumentException]
    }
    "fail requirement test on corridorEntryLocation (Kmp passagepunt)" in {
      evaluating { base.copy(corridorEntryLocation = "") } must produce[IllegalArgumentException]
      evaluating { base.copy(corridorEntryLocation = "1234567") } must produce[IllegalArgumentException]
    }
    "fail requirement test on corridorExitLocation (Kmp tweede passagepunt)" in {
      evaluating { base.copy(corridorExitLocation = "") } must produce[IllegalArgumentException]
      evaluating { base.copy(corridorExitLocation = "1234567") } must produce[IllegalArgumentException]
    }
    "fail requirement test on corridorLength (Kmp tweede passagepunt)" in {
      evaluating { base.copy(corridorLength = 0) } must produce[IllegalArgumentException]
    }
    "fail requirement test on roadCode (Wegcode)" in {
      evaluating { base.copy(roadCode = -1) } must produce[IllegalArgumentException]
      evaluating { base.copy(roadCode = 10000) } must produce[IllegalArgumentException]
    }
    "fail requirement test on drivingDirectionFrom (Richting van)" in {
      evaluating { base.copy(drivingDirectionFrom = Some("12345678901234567890123456")) } must produce[IllegalArgumentException]
    }
    "fail requirement test on drivingDirectionTo (Richting naar)" in {
      evaluating { base.copy(drivingDirectionTo = Some("12345678901234567890123456")) } must produce[IllegalArgumentException]
    }
    "fail requirement test on locationLine1 (Locatie1)" in {
      evaluating { base.copy(locationLine1 = "") } must produce[IllegalArgumentException]
      evaluating { base.copy(locationLine1 = "1234567890123456789012345678901234567890123456") } must produce[IllegalArgumentException]
    }
    "fail requirement test on locationLine2 (Locatie2)" in {
      evaluating { base.copy(locationLine2 = Some("1234567890123456789012345678901234567890123456")) } must produce[IllegalArgumentException]
    }
    "fail requirement test on locationCode (Pleegplaats)" in {
      evaluating { base.copy(locationCode = "-1") } must produce[IllegalArgumentException]
      evaluating { base.copy(locationCode = "100000") } must produce[IllegalArgumentException]
    }
  }
}
