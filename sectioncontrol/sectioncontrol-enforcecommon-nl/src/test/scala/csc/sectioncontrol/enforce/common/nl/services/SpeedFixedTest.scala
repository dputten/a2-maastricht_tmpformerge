/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

package csc.sectioncontrol.enforce.common.nl.services

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.messages.{ VehicleImageType, VehicleImage, VehicleMetadata }
import csc.sectioncontrol.storage.ServiceType

class SpeedFixedTest extends WordSpec with MustMatchers {

  "Service SpeedFixed" must {
    val defaultImage = VehicleImage(timestamp = 0, offset = 0L, uri = "", imageType = VehicleImageType.License, checksum = "", timeYellow = None, timeRed = None)
    val images = Seq(
      defaultImage.copy(imageType = VehicleImageType.License, timeYellow = Some(10), timeRed = Some(20)),
      defaultImage.copy(imageType = VehicleImageType.MeasureMethod2Speed, timeYellow = Some(50), timeRed = Some(60)),
      defaultImage.copy(imageType = VehicleImageType.Overview, timeYellow = Some(70), timeRed = Some(80)),
      defaultImage.copy(imageType = VehicleImageType.OverviewRedLight, timeYellow = Some(71), timeRed = Some(81)),
      defaultImage.copy(imageType = VehicleImageType.OverviewSpeed, timeYellow = Some(72), timeRed = Some(82)),
      defaultImage.copy(imageType = VehicleImageType.RedLight, timeYellow = Some(90), timeRed = Some(100)))
    val vmd = VehicleMetadata(lane = null, eventId = "", eventTimestamp = 1, eventTimestampStr = None, images = images, NMICertificate = "", applChecksum = "", serialNr = "SN1234")
    val entry = vmd.copy(images = images)
    val exit = Some(vmd.copy(images = images))

    val service = new SpeedFixed()

    "return correct serviceType" in {
      service.serviceType must be(ServiceType.SpeedFixed)
    }
    "indicate that a second image is not required" in {
      service.requireSecondImage must be(false)
    }
    "give the correct first image that has vehicle image type of 'Overview'" in {
      val ref = service.getImage1Ref(entry, exit)
      ref.isDefined must be(true)
      ref.get._1 must be(VehicleImageType.OverviewSpeed)
      ref.get._2 must be(entry)
    }
    "give the correct second image that has vehicle image type of 'MeasureMethod2Speed'" in {
      val ref = service.getImage2Ref(entry, exit)
      ref.isDefined must be(true)
      ref.get._1 must be(VehicleImageType.MeasureMethod2Speed)
      ref.get._2 must be(entry)
    }
  }
}
