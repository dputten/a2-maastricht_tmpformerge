/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.common.nl.violations

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.enforce.Enumerations._
import csc.sectioncontrol.messages.certificates.{ TypeCertificate, MeasurementMethodType, ComponentCertificate }
import csc.sectioncontrol.messages.{ SpeedIndicatorType, IndicatorReason, VehicleCode, ProcessingIndicator }
import csc.sectioncontrol.enforce.nl.casefiles.HHMVS14
import csc.sectioncontrol.enforce.common.nl.messages.{ SpeedIndicationSign, HeaderInfo, ViolationData }
import csc.sectioncontrol.enforce.common.nl.services.{ SpeedFixed, RedLight, TargetGroup, SectionControl }
import csc.sectioncontrol.enforce.register.CountryCodes

/**
 * @author Maarten Hazewinkel
 */
class ViolationDataTest extends WordSpec with MustMatchers {

  "Violation constructor" must {
    lazy val base = ViolationData(
      countryCodes = CountryCodes(),
      processingIndicator = ProcessingIndicator.Automatic,
      reason = IndicatorReason(),
      exportLicensePlate = "XX00XX",
      vehicleClass = Some(VehicleCode.PA),
      measuredSpeed = 101,
      vehicleCountryCode = Some("NL"),
      reportingOfficerCode = "VC2345",
      pshTmId = "000604EG33",
      indicatedSpeed = SpeedIndicationSign(signIndicator = true, indicatorType = Some(SpeedIndicatorType.A3), indicatedSpeed = Some(50)),
      corridorEntryTime = 2001L,
      corridorExitTime = 4001L,
      violationJarSha = "SHA-1",
      images = Seq())
    "construct normally with good parameters" in {
      base.exportLicensePlate must be("XX00XX")
    }
    "fail requirement test if any argument is null" in {
      evaluating { base.copy(processingIndicator = null) } must produce[IllegalArgumentException]
      evaluating { base.copy(exportLicensePlate = null) } must produce[IllegalArgumentException]
      evaluating { base.copy(vehicleClass = null) } must produce[IllegalArgumentException]
      evaluating { base.copy(vehicleCountryCode = null) } must produce[IllegalArgumentException]
      evaluating { base.copy(indicatedSpeed = null) } must produce[IllegalArgumentException]
      evaluating { base.copy(indicationRoadworks = null) } must produce[IllegalArgumentException]
      evaluating { base.copy(indicationDanger = null) } must produce[IllegalArgumentException]
      evaluating { base.copy(indicationActualWork = null) } must produce[IllegalArgumentException]
      evaluating { base.copy(invertDrivingDirection = null) } must produce[IllegalArgumentException]
      evaluating { base.copy(textCode = null) } must produce[IllegalArgumentException]
    }
    "fail requirement test on licensePlate (Kenteken voertuig)" in {
      evaluating {
        base.copy(processingIndicator = ProcessingIndicator.Automatic,
          exportLicensePlate = "")
      } must produce[IllegalArgumentException]
      evaluating {
        base.copy(processingIndicator = ProcessingIndicator.Mobi,
          exportLicensePlate = "")
      } must produce[IllegalArgumentException]
      evaluating {
        base.copy(processingIndicator = ProcessingIndicator.Automatic,
          exportLicensePlate = null)
      } must produce[IllegalArgumentException]
      evaluating {
        base.copy(processingIndicator = ProcessingIndicator.Mobi,
          exportLicensePlate = null)
      } must produce[IllegalArgumentException]
      evaluating { base.copy(exportLicensePlate = "1234567890123") } must produce[IllegalArgumentException]
      evaluating { base.copy(exportLicensePlate = "abcd-def-9012", vehicleCountryCode = Some("DE")) } must produce[IllegalArgumentException]
    }
    "allow empty licensePlate (Kenteken voertuig) for Manual processingIndicator (Indicatie verwerking)" in {
      base.copy(processingIndicator = ProcessingIndicator.Manual, exportLicensePlate = "").exportLicensePlate must be("")
    }
    "fail requirement test on vehicleClass (Voertuigklasse)" in {
      evaluating {
        base.copy(processingIndicator = ProcessingIndicator.Automatic,
          vehicleClass = Some(null))
      } must produce[IllegalArgumentException]
      evaluating {
        base.copy(processingIndicator = ProcessingIndicator.Mobi,
          vehicleClass = Some(null))
      } must produce[IllegalArgumentException]
      evaluating {
        base.copy(processingIndicator = ProcessingIndicator.Automatic,
          vehicleClass = None)
      } must produce[IllegalArgumentException]
      evaluating {
        base.copy(processingIndicator = ProcessingIndicator.Mobi,
          vehicleClass = None)
      } must produce[IllegalArgumentException]
    }
    "allow empty vehicleClass (Voertuigklasse) for Manual processingIndicator (Indicatie verwerking)" in {
      base.copy(processingIndicator = ProcessingIndicator.Manual, vehicleClass = None).vehicleClass must be(None)
    }
    "fail requirement test on measuredSpeed (Gemeten snelheid)" in {
      evaluating { base.copy(measuredSpeed = -1) } must produce[IllegalArgumentException]
      evaluating { base.copy(measuredSpeed = 0) } must produce[IllegalArgumentException]
    }
    "fail requirement test on vehicleCountryCode (Landcode voertuig)" in {
      evaluating {
        base.copy(processingIndicator = ProcessingIndicator.Automatic,
          vehicleCountryCode = Some(null))
      } must produce[IllegalArgumentException]
      evaluating {
        base.copy(processingIndicator = ProcessingIndicator.Mobi,
          vehicleCountryCode = Some(null))
      } must produce[IllegalArgumentException]
      evaluating {
        base.copy(processingIndicator = ProcessingIndicator.Automatic,
          vehicleCountryCode = None)
      } must produce[IllegalArgumentException]
      evaluating {
        base.copy(processingIndicator = ProcessingIndicator.Mobi,
          vehicleCountryCode = None)
      } must produce[IllegalArgumentException]
    }
    "allow empty vehicleCountryCode (Landcode voertuig) for Manual processingIndicator (Indicatie verwerking)" in {
      base.copy(processingIndicator = ProcessingIndicator.Manual, vehicleCountryCode = None).vehicleCountryCode must be(None)
    }
    "fail requirement test on indicationRoadworks (Indicatie werkzaamheden)" in {
      evaluating {
        base.copy(indicationRoadworks = Some(true),
          indicatedSpeed = SpeedIndicationSign(signIndicator = false))
      } must produce[IllegalArgumentException]
      evaluating {
        base.copy(indicationRoadworks = Some(true),
          indicationDanger = None)
      } must produce[IllegalArgumentException]
      evaluating {
        base.copy(indicationRoadworks = Some(true),
          indicationActualWork = None)
      } must produce[IllegalArgumentException]

      evaluating {
        base.copy(indicationRoadworks = Some(false),
          indicationDanger = Some(false))
      } must produce[IllegalArgumentException]
      evaluating {
        base.copy(indicationRoadworks = Some(false),
          indicationDanger = Some(true))
      } must produce[IllegalArgumentException]
      evaluating {
        base.copy(indicationRoadworks = Some(false),
          indicationActualWork = Some(false))
      } must produce[IllegalArgumentException]
      evaluating {
        base.copy(indicationRoadworks = Some(false),
          indicationActualWork = Some(true))
      } must produce[IllegalArgumentException]
    }
    "fail requirement test on textCode (Codetekst)" in {
      evaluating { base.copy(textCode = Some(-1)) } must produce[IllegalArgumentException]
      evaluating { base.copy(textCode = Some(100)) } must produce[IllegalArgumentException]
    }
  }

  "Violation output format" must {
    val corridorInfo = CorridorInfo(corridorId = 1,
      corridorEntryLocation = "Entry",
      corridorExitLocation = "Exit",
      corridorLength = 1578.8,
      roadCode = 12,
      drivingDirectionFrom = Some("Beginning"),
      drivingDirectionTo = Some("End"),
      locationLine1 = "Here",
      locationLine2 = None,
      locationCode = "01234")
    val globalParameters = ViolationGroupParameters(
      countryCodes = CountryCodes(),
      systemId = "HHM1",
      systemName = "HHM1",
      routeId = "EG33",
      corridorInfo = corridorInfo,
      mtmRouteId = "a002l",
      violationsDate = 960148740000L,
      radarCode = 2,
      pshTmIdFormat = "[YY][MM][DD]EG33",
      hardwareSoftwareSeals = List(ComponentCertificate("DeelA", "1234"),
        ComponentCertificate("DeelB", "5678")),
      headerInfo = HeaderInfo("Header1", "Header2", new MeasurementMethodType(typeDesignation = "XX123",
        category = "A",
        unitSpeed = "km/h",
        unitRedLight = "s",
        unitLength = "m",
        restrictiveConditions = "",
        displayRange = "20-250 km/h",
        temperatureRange = "temperatuurbereik -10 to 50 graden Celsius",
        permissibleError = "toelatbare fout 3%",
        typeCertificate = new TypeCertificate("eg33-cert", 3,
          List(ComponentCertificate("matcher1", "blah1"),
            ComponentCertificate("vr2", "blah2"))))),
      roadPart = "L",
      pshtmId = "A120")
    val base = ViolationData(
      countryCodes = CountryCodes(),
      processingIndicator = ProcessingIndicator.Automatic,
      reason = IndicatorReason(),
      exportLicensePlate = "XX00XX",
      vehicleClass = Some(VehicleCode.PA),
      measuredSpeed = 101,
      vehicleCountryCode = Some("NL"),
      reportingOfficerCode = "VC1234",
      pshTmId = "000604EG33AAAAA",
      indicatedSpeed = SpeedIndicationSign(signIndicator = false, indicatorType = None, indicatedSpeed = Some(50)),
      corridorEntryTime = 960148744092L,
      corridorExitTime = 960148793000L,
      violationJarSha = "SHA-1",
      images = Seq())

    "format correct yellow and redtime in SS.M format" in {
      base.formatRedYellowTime(Some(2222L)) must be("02.2")
      base.formatRedYellowTime(Some(222L)) must be("00.2")
      base.formatRedYellowTime(Some(199200L)) must be("99.9")
      base.formatRedYellowTime(Some(9999L)) must be("09.9")
      base.formatRedYellowTime(Some(900L)) must be("00.9")
      base.formatRedYellowTime(Some(400L)) must be("00.4")
      base.formatRedYellowTime(None) must be("00.0")
    }
    "be according to spec" in {
      base.toDataLine("0011", globalParameters, new HHMVS14(), new SectionControl()) must be(
        "A 0011 20000604 2159 XX00XX       PA 101 NL  000 N     Entry 215904.092   Exit 215953.000  1578 0 Here                                                                                        0012 1 N                   VC1234         01234 Beginning                 End                       2               000604EG33 DeelA 1234,DeelB 5678, ")

      base.copy(processingIndicator = ProcessingIndicator.Mobi,
        indicatedSpeed = SpeedIndicationSign(signIndicator = true, indicatorType = Some(SpeedIndicatorType.A3), indicatedSpeed = Some(50)),
        indicationRoadworks = Some(true),
        indicationDanger = Some(false),
        indicationActualWork = Some(true),
        textCode = Some(19) //  indicationTargetGroupViolation = true,
        //factNumber = Some("A1234")
        ).toDataLine("0234", globalParameters, new HHMVS14(), new TargetGroup(factNumber = "A1234")) must be(
          "V 0234 20000604 2159 XX00XX       PA 101 NL  000   A3  Entry 215904.092                         B Here                                                                                        0012 1 N J N J             VC1234         01234 Beginning                 End                       2 19 D A1234    000604EG33 DeelA 1234,DeelB 5678, ")
      //"V 0234 20000604 2159 XX00XX       PA 101 NL  000 J A3  Entry 215904.092                         Z Here                                                                                        0012 1 N J N J             VC1234         01234 Beginning                 End                       2 19 D A1234    000604EG33 DeelA 1234,DeelB 5678, ")
      //       "V 0234 20000604 2159 XX00XX       PA 101 NL  050 J A3  Entry 215904.092   Exit 215953.000  1578 Z Here                                                                                        0012 1 N J N J             VC1234         01234 Beginning                 End                       2 19 D A1234    000604EG33 DeelA 1234,DeelB 5678, ")

      base.copy(processingIndicator = ProcessingIndicator.Mobi,
        indicatedSpeed = SpeedIndicationSign(signIndicator = true, indicatorType = Some(SpeedIndicatorType.A3), indicatedSpeed = Some(50)),
        indicationRoadworks = Some(true),
        indicationDanger = Some(false),
        indicationActualWork = Some(true),
        textCode = Some(19) //  indicationTargetGroupViolation = true,
        //factNumber = Some("A1234")
        ).toDataLine("0235", globalParameters, new HHMVS14(), new RedLight(pardonRedTime = 0, minimumYellowTime = 0, factNumber = "A1234")) must be(
          "V 0235 20000604 2159 XX00XX       PA     NL  000       Entry 215904.092                         B Here                                                                                        0012 1 N J N J   00.0 00.0 VC1234         01234 Beginning                 End                       2 19   A1234    000604EG33 DeelA 1234,DeelB 5678, ")

      base.copy(processingIndicator = ProcessingIndicator.Mobi,
        indicatedSpeed = SpeedIndicationSign(signIndicator = true, indicatorType = Some(SpeedIndicatorType.A3), indicatedSpeed = Some(50)),
        indicationRoadworks = Some(true),
        indicationDanger = Some(false),
        indicationActualWork = Some(true),
        textCode = Some(19)).toDataLine("0236", globalParameters, new HHMVS14(), new SectionControl()) must be(
          "V 0236 20000604 2159 XX00XX       PA 101 NL  050 J A3  Entry 215904.092   Exit 215953.000  1578 B Here                                                                                        0012 1 N J N J             VC1234         01234 Beginning                 End                       2 19            000604EG33 DeelA 1234,DeelB 5678, ")

      base.toDataLine("3756", globalParameters.copy(roadType = RoadType.Provincial,
        dutyType = Some("AB12"),
        deploymentCode = Some("MI")), new HHMVS14(), new SectionControl()) must be(
        "A 3756 20000604 2159 XX00XX       PA 101 NL  000 N     Entry 215904.092   Exit 215953.000  1578 0 Here                                                                                        0012 2 N                   VC1234 AB12 MI 01234 Beginning                 End                       2               000604EG33 DeelA 1234,DeelB 5678, ")
    }

    "fail requirement test on reportingOfficerCode (Verbalistantcode)" in {
      evaluating { base.copy(reportingOfficerCode = null) } must produce[IllegalArgumentException]
    }
    "Report MIL as country code" in {
      base.copy(military = true).toDataLine("0011", globalParameters, new HHMVS14, new SectionControl()) must be(
        "A 0011 20000604 2159 XX00XX       PA 101 MIL 000 N     Entry 215904.092   Exit 215953.000  1578 0 Here                                                                                        0012 1 N                   VC1234         01234 Beginning                 End                       2               000604EG33 DeelA 1234,DeelB 5678, ")
    }
    "Calculate the proper status code" in {
      val manualBase = base.copy(processingIndicator = ProcessingIndicator.Manual)
      manualBase.copy(exportLicensePlate = "").toDataLine("0011", globalParameters, new HHMVS14(), new SectionControl()) must be(
        ". 0011 20000604 2159              PA 101 NL  000 N     Entry 215904.092   Exit 215953.000  1578 7 Here                                                                                        0012 1 N                   VC1234         01234 Beginning                 End                       2               000604EG33 DeelA 1234,DeelB 5678, ")
      manualBase.copy(vehicleClass = None).toDataLine("0011", globalParameters, new HHMVS14(), new SectionControl()) must be(
        ". 0011 20000604 2159 XX00XX          101 NL  000 N     Entry 215904.092   Exit 215953.000  1578 2 Here                                                                                        0012 1 N                   VC1234         01234 Beginning                 End                       2               000604EG33 DeelA 1234,DeelB 5678, ")
      manualBase.copy(exportLicensePlate = "", vehicleClass = None).toDataLine("0011", globalParameters, new HHMVS14(), new SectionControl()) must be(
        ". 0011 20000604 2159                 101 NL  000 N     Entry 215904.092   Exit 215953.000  1578 7 Here                                                                                        0012 1 N                   VC1234         01234 Beginning                 End                       2               000604EG33 DeelA 1234,DeelB 5678, ")
      manualBase.copy(vehicleCountryCode = None).toDataLine("0011", globalParameters, new HHMVS14(), new SectionControl()) must be(
        ". 0011 20000604 2159 XX00XX       PA 101     000 N     Entry 215904.092   Exit 215953.000  1578 7 Here                                                                                        0012 1 N                   VC1234         01234 Beginning                 End                       2               000604EG33 DeelA 1234,DeelB 5678, ")
      manualBase.copy(exportLicensePlate = "", vehicleCountryCode = None).toDataLine("0011", globalParameters, new HHMVS14, new SectionControl) must be(
        ". 0011 20000604 2159              PA 101     000 N     Entry 215904.092   Exit 215953.000  1578 7 Here                                                                                        0012 1 N                   VC1234         01234 Beginning                 End                       2               000604EG33 DeelA 1234,DeelB 5678, ")
      manualBase.copy(vehicleClass = None, vehicleCountryCode = None).toDataLine("0011", globalParameters, new HHMVS14, new SectionControl) must be(
        ". 0011 20000604 2159 XX00XX          101     000 N     Entry 215904.092   Exit 215953.000  1578 7 Here                                                                                        0012 1 N                   VC1234         01234 Beginning                 End                       2               000604EG33 DeelA 1234,DeelB 5678, ")
      manualBase.copy(exportLicensePlate = "", vehicleClass = None, vehicleCountryCode = None).toDataLine("0011", globalParameters, new HHMVS14, new SectionControl) must be(
        ". 0011 20000604 2159                 101     000 N     Entry 215904.092   Exit 215953.000  1578 7 Here                                                                                        0012 1 N                   VC1234         01234 Beginning                 End                       2               000604EG33 DeelA 1234,DeelB 5678, ")
    }

    "produce the correct A1/A3 output" must {
      "produce the correct RedLight output (always 000)" in {
        val redlight = new RedLight(pardonRedTime = 10, minimumYellowTime = 20, factNumber = "A1234")
        val indicatedSpeed = SpeedIndicationSign(signIndicator = true, indicatorType = Some(SpeedIndicatorType.A3), indicatedSpeed = Some(66))

        base.copy(indicatedSpeed = indicatedSpeed)
          .toDataLine("0011", globalParameters, new HHMVS14(), redlight) must be(
            "A 0011 20000604 2159 XX00XX       PA     NL  000       Entry 215904.092                         0 Here                                                                                        0012 1 N         00.0 00.0 VC1234         01234 Beginning                 End                       2      A1234    000604EG33 DeelA 1234,DeelB 5678, ")

        base.copy(indicatedSpeed = indicatedSpeed.copy(signIndicator = false, indicatorType = None, indicatedSpeed = Some(8)))
          .toDataLine("0011", globalParameters, new HHMVS14(), redlight) must be(
            "A 0011 20000604 2159 XX00XX       PA     NL  000       Entry 215904.092                         0 Here                                                                                        0012 1 N         00.0 00.0 VC1234         01234 Beginning                 End                       2      A1234    000604EG33 DeelA 1234,DeelB 5678, ")
      }
      "produce the correct SpeedFixed output" in {
        val speedFixed = new SpeedFixed()
        val indicatedSpeed = SpeedIndicationSign(signIndicator = true, indicatorType = Some(SpeedIndicatorType.A1), indicatedSpeed = Some(133))

        base.copy(indicatedSpeed = indicatedSpeed)
          .toDataLine("0011", globalParameters, new HHMVS14(), speedFixed) must be(
            "A 0011 20000604 2159 XX00XX       PA 101 NL  133 J A1  Entry 215904.092                         0 Here                                                                                        0012 1 N                   VC1234         01234 Beginning                 End                       2               000604EG33 DeelA 1234,DeelB 5678, ")

        base.copy(indicatedSpeed = indicatedSpeed.copy(signIndicator = false, indicatorType = None, indicatedSpeed = Some(8)))
          .toDataLine("0011", globalParameters, new HHMVS14(), speedFixed) must be(
            "A 0011 20000604 2159 XX00XX       PA 101 NL  000 N     Entry 215904.092                         0 Here                                                                                        0012 1 N                   VC1234         01234 Beginning                 End                       2               000604EG33 DeelA 1234,DeelB 5678, ")
      }
    }
  }

  "SpeedIndicationSign constructor" must {
    lazy val base = SpeedIndicationSign(signIndicator = true, indicatorType = Some(SpeedIndicatorType.A1), indicatedSpeed = Some(90))
    "construct normally with good parameters" in {
      base.indicatedSpeed must be(Some(90))
      base.indicatorType must be(Some(SpeedIndicatorType.A1))
    }
    "allow indicatedSpeed value when signIndicator is false" in {
      base.copy(signIndicator = false, indicatorType = None).indicatedSpeed must be(Some(90))
    }
    "fail requirement test if signIndicator is false having an indicatorType" in {
      evaluating { base.copy(signIndicator = false) } must produce[IllegalArgumentException]
    }
    "fail requirement test if signIndicator is true with no indicatorType and indicatedSpeed" in {
      evaluating { base.copy(indicatedSpeed = None) } must produce[IllegalArgumentException]
      evaluating { base.copy(indicatorType = None) } must produce[IllegalArgumentException]
    }
    "fail requirement test if any argument is null" in {
      evaluating { base.copy(indicatorType = null) } must produce[IllegalArgumentException]
    }
    "fail requirement test on indicatedSpeed (Bordsnelheid)" in {
      evaluating { base.copy(indicatedSpeed = Some(-1)) } must produce[IllegalArgumentException]
      evaluating { base.copy(indicatedSpeed = Some(1000)) } must produce[IllegalArgumentException]
    }
  }
}
/*
"A 0011 20000604 2159 XX00XX       PA 101 NL  000 N     Entry 215904.092   Exit 215953.000  1578 0 Here                                                                                        0012 1 N                   VC1234         01234 Beginning                 End                       2               000604EG33 ComponentCertificate(DeelA,1234),ComponentCertificate(DeelB,5678), " was not equal to
"A 0011 20000604 2159 XX00XX       PA 101 NL  000 N     Entry 215904.092   Exit 215953.000  1578 0 Here                                                                                        0012 1 N                   VC1234         01234 Beginning                 End                       2               000604EG33 DeelA 1234,DeelB 5678, "
*/ 