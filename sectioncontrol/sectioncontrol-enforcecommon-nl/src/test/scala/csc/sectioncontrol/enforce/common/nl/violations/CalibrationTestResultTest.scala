/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.common.nl.violations

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import org.apache.commons.io.IOUtils
import csc.sectioncontrol.messages.certificates.{ TypeCertificate, MeasurementMethodType, ComponentCertificate }
import csc.sectioncontrol.enforce.common.nl.messages.{ HeaderInfo, SystemParameters }
import csc.sectioncontrol.enforce.register.CountryCodes
import csc.sectioncontrol.storage.ServiceType

/**
 * @author Maarten Hazewinkel
 */
class CalibrationTestResultTest extends WordSpec with MustMatchers {
  val jpegData = IOUtils.toByteArray(getClass.getResourceAsStream("testimage.jpg"))
  val pngData = IOUtils.toByteArray(getClass.getResourceAsStream("testimage.png"))

  "CalibrationTestResult constructor" must {
    lazy val photo = CalibrationTestPhoto(cameraId = 2,
      time = 2001L,
      image = jpegData)
    lazy val base = CalibrationTestResult(testedOk = true,
      time = 2001L,
      cameraCount = 2,
      reportingOfficerCode = "VC1234",
      softwareSeals = List(ComponentCertificate("A", "1")),
      serialNumbers = List("AB123"),
      parameters = SystemParameters("1", 2, 3, 4, 5, "6", "7", 8, 9, 10),
      serviceType = ServiceType.SectionControl,
      testPhotos = List())
    "construct normally with good parameters" in {
      base.testedOk must be(true)
    }
    "fail requirement test if any argument is null" in {
      evaluating { base.copy(softwareSeals = null) } must produce[IllegalArgumentException]
      evaluating { base.copy(serialNumbers = null) } must produce[IllegalArgumentException]
      evaluating { base.copy(parameters = null) } must produce[IllegalArgumentException]
      evaluating { base.copy(testPhotos = null) } must produce[IllegalArgumentException]
    }
    "fail requirement test on cameraCount" in {
      evaluating { base.copy(cameraCount = -1) } must produce[IllegalArgumentException]
    }
    "fail requirement test on softwareSeals" in {
      evaluating { base.copy(softwareSeals = List()) } must produce[IllegalArgumentException]
    }
    "fail requirement test on serialNumbers" in {
      evaluating { base.copy(serialNumbers = List()) } must produce[IllegalArgumentException]
      evaluating { base.copy(serialNumbers = List("A", "")) } must produce[IllegalArgumentException]
    }
    "fail requirement test on parameters" in {
      evaluating { base.copy(parameters = SystemParameters(null, 2, 3, 4, 5, "6", "7", 8, 9, 10)) } must produce[IllegalArgumentException]
      evaluating { base.copy(parameters = SystemParameters("1", 2, 3, 4, 5, null, "7", 8, 9, 10)) } must produce[IllegalArgumentException]
    }
    "fail requirement test on testPhotos" in {
      evaluating {
        base.copy(testPhotos = List(photo.copy(cameraId = 1),
          photo.copy(cameraId = 2),
          photo.copy(cameraId = 3)))
      } must produce[IllegalArgumentException]
      evaluating {
        base.copy(testPhotos = List(photo.copy(cameraId = 3),
          photo.copy(cameraId = 3)))
      } must produce[IllegalArgumentException]
    }
  }

  "CalibrationTest output format" must {
    "be according to spec" in {
      val corridorInfo = CorridorInfo(corridorId = 1,
        corridorEntryLocation = "Entry",
        corridorExitLocation = "Exit",
        corridorLength = 1578,
        roadCode = 12,
        drivingDirectionFrom = Some("Beginning"),
        drivingDirectionTo = Some("End"),
        locationLine1 = "Here",
        locationLine2 = None,
        locationCode = "12345")
      val globalParameters = ViolationGroupParameters(systemId = "HHM1",
        systemName = "HHM1",
        countryCodes = CountryCodes(),
        routeId = "EG33",
        corridorInfo = corridorInfo,
        mtmRouteId = "a002l",
        violationsDate = 960148740000L,
        radarCode = 2,
        pshTmIdFormat = "[YY][MM][DD][HHHH]",
        //pshTmIdFormat = "[YY][MM][DD]EG33",
        hardwareSoftwareSeals = List(ComponentCertificate("DeelA", "1234"),
          ComponentCertificate("DeelB", "5678")),
        headerInfo = HeaderInfo("Header1", "Header2", new MeasurementMethodType(typeDesignation = "XX123",
          category = "A",
          unitSpeed = "km/h",
          unitRedLight = "s",
          unitLength = "m",
          restrictiveConditions = "",
          displayRange = "20-250 km/h",
          temperatureRange = "temperatuurbereik -10 to 50 graden Celsius",
          permissibleError = "toelatbare fout 3%",
          typeCertificate = new TypeCertificate("eg33-cert", 3,
            List(ComponentCertificate("matcher1", "blah1"),
              ComponentCertificate("vr2", "blah2"))))),
        roadPart = "L",
        pshtmId = "A120")
      val photo = CalibrationTestPhoto(cameraId = 2,
        time = 960148743000L,
        image = IOUtils.toByteArray(getClass.getResourceAsStream("testphoto.jpg")))
      val base = CalibrationTestResult(testedOk = true,
        time = 960148743000L,
        cameraCount = 2,
        reportingOfficerCode = "VC1234",
        softwareSeals = List(ComponentCertificate("A", "1")),
        serialNumbers = List("AB123"),
        parameters = SystemParameters("1", 2, 3, 4, 5, "6", "7", 8, 9, 10),
        serviceType = ServiceType.SectionControl,
        testPhotos = List(photo.copy(cameraId = 2),
          photo.copy(cameraId = 4)))
      base.toDataLine(globalParameters, 960156000000L) must be("1,215903,hhm1,000604A120,2,A 1,AB123,1,2,3,4,5,6,7,8,9,10")
      val variant = base.copy(testedOk = false,
        time = 960148784000L,
        cameraCount = 4,
        softwareSeals = List(ComponentCertificate("A", "1"),
          ComponentCertificate("B", "2")),
        serialNumbers = List("XY987", "AB123"),
        parameters = SystemParameters("1", 2, 3, 4, 5, "6", "7", 8, 9, 10))
      variant.toDataLine(globalParameters, 960156000000L) must be("0,215944,hhm1,000604A120,4,A 1;B 2,XY987;AB123,1,2,3,4,5,6,7,8,9,10")
      val midnight = variant.copy(time = 960156000000L)
      midnight.toDataLine(globalParameters, 960156000000L) must be("0,235959,hhm1,000604A120,4,A 1;B 2,XY987;AB123,1,2,3,4,5,6,7,8,9,10")
      midnight.toDataLine(globalParameters, 960242400000L) must be("0,000000,hhm1,000605A120,4,A 1;B 2,XY987;AB123,1,2,3,4,5,6,7,8,9,10")
    }
  }

  "CalibrationTestPhoto constructor" must {
    lazy val base = CalibrationTestPhoto(cameraId = 2,
      time = 2001L,
      image = jpegData)
    "construct normally with good parameters" in {
      base.cameraId must be(2)
    }
    "fail requirement test if any argument is null" in {
      evaluating { base.copy(image = null) } must produce[IllegalArgumentException]
    }
    "fail requirement test on cameraId" in {
      evaluating { base.copy(cameraId = -1) } must produce[IllegalArgumentException]
    }
  }
}
