package csc.sectioncontrol.enforce.common.nl.violations

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import java.util.Calendar
import csc.sectioncontrol.common.DayUtility
import akka.util.duration._
/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 12/15/14.
 */

class ReportingOfficersTest extends WordSpec with MustMatchers {
  val now = Calendar.getInstance.getTimeInMillis
  val (dayStart, dayEnd) = DayUtility.calculateDayPeriod(now)

  "ReportingOfficers" must {

    "Select most recent reporting officer for a given time" in {
      val ro = ReportingOfficers(Seq(
        (dayStart + 3.hour.toMillis, "Officer3"),
        (dayStart + 2.hour.toMillis, "Officer2"),
        (dayStart + 1.hour.toMillis, "Officer1")))
      ro.getReportingOfficerForTime(dayStart + 2.hours.toMillis + 2.minutes.toMillis) must be(Some("Officer2"))
    }

    "Find reporting officer for the day" in {
      val ro_1 = ReportingOfficers(Seq(
        (dayStart + 3.hour.toMillis, "Officer3"),
        (dayStart + 2.hour.toMillis, "Officer2"),
        (dayStart - 1.hour.toMillis, "Officer1")))
      ro_1.getReportingOfficerForDay(dayStart + 4.hours.toMillis) must be(Some("Officer2"))

      val ro_2 = ReportingOfficers(Seq((dayStart - 1.hour.toMillis, "OfficerYesterday")))
      ro_2.getReportingOfficerForDay(dayStart + 1.hours.toMillis) must be(Some("OfficerYesterday"))
    }

    "Select most recent reporting officer" in {
      val ro = ReportingOfficers(Seq(
        (dayStart + 2.hour.toMillis, "Officer2"),
        (dayStart - 3.hour.toMillis, "OfficerYesterday")))
      ro.getReportingOfficerForTime(dayStart + 1.hours.toMillis) must be(Some("OfficerYesterday"))
    }
  }
}
