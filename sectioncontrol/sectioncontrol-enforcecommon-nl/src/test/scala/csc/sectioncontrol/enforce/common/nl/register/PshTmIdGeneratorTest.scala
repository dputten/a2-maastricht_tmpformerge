/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.common.nl.register

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import java.util.{ Calendar, GregorianCalendar }
import csc.sectioncontrol.enforce.common.nl.register.PshTmIdGenerator._
import csc.sectioncontrol.enforce.common.nl.register.PshTmIdGenerator.TextElement
import csc.sectioncontrol.storage.ServiceType

/**
 *
 * @author Maarten Hazewinkel
 */
class PshTmIdGeneratorTest extends WordSpec with MustMatchers {
  "PshTmIdGenerator.FormatParser" must {
    "fail to parse a invalid format string" in {
      val p = PshTmIdGenerator.FormatParser
      p.parseAll(p.pshTmId, "test").successful must be(false)
    }

    "parse a fixed format string" in {
      val p = PshTmIdGenerator.FormatParser
      p.parseAll(p.pshTmId, "ABCDEF19").get must be(List(TextElement("ABCDEF19")))
    }

    "parse a string with a field" in {
      val p = PshTmIdGenerator.FormatParser
      p.parseAll(p.pshTmId, "ABC[YY]DEF19").get must be(List(TextElement("ABC"), YearElement, TextElement("DEF19")))
    }

    "parse a string with two fields" in {
      val p = PshTmIdGenerator.FormatParser
      p.parseAll(p.pshTmId, "ABC[DD]DEF[LLLL]").get must be(List(TextElement("ABC"), DayElement, TextElement("DEF"), LocationElement))
    }

    "parse four fields without text" in {
      val p = PshTmIdGenerator.FormatParser
      p.parseAll(p.pshTmId, "[MM][VVVV][NNN][T]").get must be(List(MonthElement, ReportingOfficerElement, DayInYearElement, ViolationTypeElement))
    }

    "fail to parse a string with an invalid field" in {
      val p = PshTmIdGenerator.FormatParser
      p.parseAll(p.pshTmId, "ABC[BOGUS]DEF19").successful must be(false)
    }
  }

  "PshTmIdGenerator" must {
    val jun_04_2000 = new GregorianCalendar(2000, Calendar.JUNE, 4).getTimeInMillis

    "produce ParseError for an invalid format" in {
      PshTmIdGenerator("invalid", jun_04_2000, "XY5678", "1234", ServiceType.RedLight, "3215", roadPart = "L") must be("ParseError")
    }

    "produce correct values in date fields" in {
      PshTmIdGenerator("YY[YY]MM[MM]DD[DD]NNN[NNN]", jun_04_2000, "XY5678", "1234", ServiceType.RedLight, "3215", roadPart = "L") must be("YY00MM06DD04NNN156")
    }

    "produce correct values in non-date fields" in {
      PshTmIdGenerator("VVVV[VVVV]LLLL[LLLL]T[T]", jun_04_2000, "XY5678", "1234", ServiceType.SpeedFixed, "3215", roadPart = "L") must be("VVVV5678LLLL1234T1")
      PshTmIdGenerator("VVVV[VVVV]LLLL[LLLL]T[T]", jun_04_2000, "XY5678", "1234", ServiceType.RedLight, "3215", roadPart = "L") must be("VVVV5678LLLL1234T2")
      PshTmIdGenerator("VVVV[VVVV]LLLL[LLLL]T[T]", jun_04_2000, "XY5678", "1234", ServiceType.SectionControl, "3215", roadPart = "L") must be("VVVV5678LLLL1234T3")
      PshTmIdGenerator("VVVV[VVVV]LLLL[LLLL]T[T]", jun_04_2000, "XY5678", "1234", ServiceType.ANPR, "3215", roadPart = "L") must be("VVVV5678LLLL1234T")
      PshTmIdGenerator("VVVV[VVVV]LLLL[LLLL]T[T]HHHH[HHHH]", jun_04_2000, "XY5678", "1234", ServiceType.RedLight, "32156", roadPart = "L") must be("VVVV5678LLLL1234T2HHHH3215")
    }

    "handle short location codes" in {
      PshTmIdGenerator("LLLL[LLLL]", jun_04_2000, "XY5678", "123", ServiceType.RedLight, "3215", roadPart = "L") must be("LLLL 123")
    }

    "handle long location codes" in {
      PshTmIdGenerator("LLLL[LLLL]", jun_04_2000, "XY5678", "12345", ServiceType.RedLight, "3215", roadPart = "L") must be("LLLL2345")
    }
    "handle short HHM codes" in {
      PshTmIdGenerator("HHHH[HHHH]", jun_04_2000, "XY5678", "12345", ServiceType.RedLight, "15", roadPart = "L") must be("HHHH  15")
    }
    "handle long HHM codes" in {
      PshTmIdGenerator("HHHH[HHHH]", jun_04_2000, "XY5678", "12345", ServiceType.RedLight, "32156", roadPart = "L") must be("HHHH3215")
    }
    "handle Type codes" in {
      PshTmIdGenerator("T[T]", jun_04_2000, "XY5678", "12345", ServiceType.SpeedFixed, "3215", roadPart = "L") must be("T1")
      PshTmIdGenerator("T[T]", jun_04_2000, "XY5678", "12345", ServiceType.SpeedFixed, "3215", roadPart = "R") must be("T1")
      PshTmIdGenerator("T[T]", jun_04_2000, "XY5678", "12345", ServiceType.RedLight, "3215", roadPart = "L") must be("T2")
      PshTmIdGenerator("T[T]", jun_04_2000, "XY5678", "12345", ServiceType.RedLight, "3215", roadPart = "R") must be("T2")
      PshTmIdGenerator("T[T]", jun_04_2000, "XY5678", "12345", ServiceType.SectionControl, "3215", roadPart = "L") must be("T3")
      PshTmIdGenerator("T[T]", jun_04_2000, "XY5678", "12345", ServiceType.SectionControl, "3215", roadPart = "R") must be("T4")
    }
    "handle Type codes with long roadparts" in {
      PshTmIdGenerator("T[T]", jun_04_2000, "XY5678", "12345", ServiceType.SectionControl, "3215", roadPart = "Left") must be("T3")
      PshTmIdGenerator("T[T]", jun_04_2000, "XY5678", "12345", ServiceType.SectionControl, "3215", roadPart = "Right") must be("T4")
    }
  }
}
