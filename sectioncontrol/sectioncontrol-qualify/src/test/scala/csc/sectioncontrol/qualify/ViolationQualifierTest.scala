/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.qualify

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

class ViolationQualifierTest extends WordSpec with MustMatchers {

  "ViolationQualifier" must {
    val qualifier = new ViolationQualifier {
      protected def checkSpeed(measuredSpeed: Int, enforcedSpeedLimit: Int, margin: Int): Boolean = {
        measuredSpeed >= enforcedSpeedLimit + margin
      }
    }
    val speedMarginsZero = Map(0 -> 0)
    val speedMarginsEight = Map(0 -> 8)

    "produce a violation when a vehicle exceeds the enforced limit on a schedule" in {
      qualifier.isViolation(measuredSpeed = 125,
        scheduledSpeedLimit = 120,
        dynamicSpeedLimit = Right(None),
        vehicleClassSpeedLimit = Some(130),
        speedLimitMargins = speedMarginsZero,
        enforceDynamic = false) must be(true -> 120)
    }

    "produce no violation if the enforced speed limit is not exceeded" in {
      qualifier.isViolation(measuredSpeed = 113,
        scheduledSpeedLimit = 120,
        dynamicSpeedLimit = Right(None),
        vehicleClassSpeedLimit = Some(130),
        speedLimitMargins = speedMarginsZero,
        enforceDynamic = false) must be(false -> 120)
    }

    "produce a violation when a vehicle exceeds the vehicle class limit" in {
      qualifier.isViolation(measuredSpeed = 113,
        scheduledSpeedLimit = 120,
        dynamicSpeedLimit = Right(None),
        vehicleClassSpeedLimit = Some(80),
        speedLimitMargins = speedMarginsZero,
        enforceDynamic = false) must be(true -> 80)
    }

    "not produce a violation when a vehicle exceeds the dynamic speed limit which differs from schedule speed limit" in {
      qualifier.isViolation(measuredSpeed = 113,
        scheduledSpeedLimit = 120,
        dynamicSpeedLimit = Right(Some(90)),
        vehicleClassSpeedLimit = Some(130),
        speedLimitMargins = speedMarginsZero,
        enforceDynamic = false) must be(false -> 0)
    }

    "produce a violation when a vehicle exceeds the dynamic speed limit which differs from schedule speed limit when enforcing dynamic" in {
      qualifier.isViolation(measuredSpeed = 113,
        scheduledSpeedLimit = 120,
        dynamicSpeedLimit = Right(Some(90)),
        vehicleClassSpeedLimit = Some(130),
        speedLimitMargins = speedMarginsZero,
        enforceDynamic = true) must be(true -> 90)
    }

    "produce a violation when a vehicle exceeds the dynamic speed limit which matches the schedule speed limit" in {
      qualifier.isViolation(measuredSpeed = 123,
        scheduledSpeedLimit = 120,
        dynamicSpeedLimit = Right(Some(120)),
        vehicleClassSpeedLimit = Some(130),
        speedLimitMargins = speedMarginsZero,
        enforceDynamic = false) must be(true -> 120)
    }

    "produce a violation when a vehicle exceeds the schedule speed limit with a blank dynamic speed" in {
      qualifier.isViolation(measuredSpeed = 123,
        scheduledSpeedLimit = 120,
        dynamicSpeedLimit = Right(None),
        vehicleClassSpeedLimit = Some(130),
        speedLimitMargins = speedMarginsZero,
        enforceDynamic = false) must be(true -> 120)
    }

    "produce a violation when a vehicle exceeds the schedule speed limit plus margin" in {
      qualifier.isViolation(measuredSpeed = 130,
        scheduledSpeedLimit = 120,
        dynamicSpeedLimit = Right(None),
        vehicleClassSpeedLimit = Some(130),
        speedLimitMargins = speedMarginsEight,
        enforceDynamic = false) must be(true -> 120)
    }

    "produce no violation when a vehicle exceeds the schedule speed limit by less than the margin" in {
      qualifier.isViolation(measuredSpeed = 125,
        scheduledSpeedLimit = 120,
        dynamicSpeedLimit = Right(None),
        vehicleClassSpeedLimit = Some(130),
        speedLimitMargins = speedMarginsEight,
        enforceDynamic = false) must be(false -> 120)
    }
  }
}
