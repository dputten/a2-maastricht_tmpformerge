/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *  
 */

package csc.sectioncontrol.qualify

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

class RedLightQualifierTest extends WordSpec with MustMatchers {
  case class VehicleImage(timestamp: Long, timeYellow: Option[Long], timeRed: Option[Long])

  "RedLightQualifier" must {
    val qualifier = new RedLightQualifier()
    val fTimestamp = System.currentTimeMillis()
    val sTimestamp = System.currentTimeMillis() + 100
    val vehicleImage = VehicleImage(timestamp = 10l, timeYellow = Some(10l), timeRed = Some(10l))

    "produce no violation when both images are not given" in {
      qualifier.isViolation(
        firstImage = None,
        secondImage = None,
        pardonRedTime = 10l,
        minimumYellowTime = 20l,
        measuredSpeed = 30,
        thresholdSpeed = 40) must be(false -> 0)
    }
    "produce no violation when secondImage image is not given" in {
      qualifier.isViolation(
        firstImage = Some(vehicleImage),
        secondImage = None,
        pardonRedTime = 10l,
        minimumYellowTime = 20l,
        measuredSpeed = 30,
        thresholdSpeed = 40) must be(false -> 0)
    }
    "produce no violation when firstImage image is not given" in {
      qualifier.isViolation(
        firstImage = None,
        secondImage = Some(vehicleImage),
        pardonRedTime = 10l,
        minimumYellowTime = 20l,
        measuredSpeed = 30,
        thresholdSpeed = 40) must be(false -> 0)
    }
    "produce no violation when both images are given but firstImage.timeYellow is None" in {
      qualifier.isViolation(
        firstImage = Some(vehicleImage.copy(timeYellow = None)),
        secondImage = Some(vehicleImage),
        pardonRedTime = 10l,
        minimumYellowTime = 20l,
        measuredSpeed = 30,
        thresholdSpeed = 40) must be(false -> 0)
    }
    "produce no violation when both images are given but firstImage.timeRed is None" in {
      qualifier.isViolation(
        firstImage = Some(vehicleImage.copy(timeRed = None)),
        secondImage = Some(vehicleImage),
        pardonRedTime = 10l,
        minimumYellowTime = 20l,
        measuredSpeed = 30,
        thresholdSpeed = 40) must be(false -> 0)
    }
    "produce no violation when both images are given but secondImage.timeYellow is None" in {
      qualifier.isViolation(
        firstImage = Some(vehicleImage),
        secondImage = Some(vehicleImage.copy(timeYellow = None)),
        pardonRedTime = 10l,
        minimumYellowTime = 20l,
        measuredSpeed = 30,
        thresholdSpeed = 40) must be(false -> 0)
    }
    "produce no violation when both images are given but secondImage.timeRed is None" in {
      qualifier.isViolation(
        firstImage = Some(vehicleImage),
        secondImage = Some(vehicleImage.copy(timeRed = None)),
        pardonRedTime = 10l,
        minimumYellowTime = 20l,
        measuredSpeed = 30,
        thresholdSpeed = 40) must be(false -> 0)
    }
    "produce violation when first.timeRed > pardonRedTime" in {
      //first.timeRed > pardonRedTime
      qualifier.isViolation(
        firstImage = Some(vehicleImage.copy(timeRed = Some(8), timestamp = fTimestamp)),
        secondImage = Some(vehicleImage.copy(timestamp = sTimestamp)),
        pardonRedTime = 5l,
        minimumYellowTime = 10l,
        measuredSpeed = 5,
        thresholdSpeed = 4) must be(true -> 4)

      //first.timeRed <= pardonRedTime
      qualifier.isViolation(
        firstImage = Some(vehicleImage.copy(timeRed = Some(8), timestamp = fTimestamp)),
        secondImage = Some(vehicleImage.copy(timestamp = sTimestamp)),
        pardonRedTime = 9l,
        minimumYellowTime = 10l,
        measuredSpeed = 5,
        thresholdSpeed = 4) must be(false -> 4)
    }
    "produce violation when first.timeYellow >= minimumYellowTime" in {
      //first.timeYellow >= minimumYellowTime
      qualifier.isViolation(
        firstImage = Some(vehicleImage.copy(timeRed = Some(8), timestamp = fTimestamp)),
        secondImage = Some(vehicleImage.copy(timestamp = sTimestamp)),
        pardonRedTime = 5l,
        minimumYellowTime = 10l,
        measuredSpeed = 5,
        thresholdSpeed = 4) must be(true -> 4)

      //first.timeYellow < minimumYellowTime
      qualifier.isViolation(
        firstImage = Some(vehicleImage.copy(timeRed = Some(8), timestamp = fTimestamp)),
        secondImage = Some(vehicleImage.copy(timestamp = sTimestamp)),
        pardonRedTime = 5l,
        minimumYellowTime = 11l,
        measuredSpeed = 5,
        thresholdSpeed = 4) must be(false -> 4)
    }
    "produce violation when measuredSpeed >= thresholdSpeed" in {
      //measuredSpeed >= thresholdSpeed
      qualifier.isViolation(
        firstImage = Some(vehicleImage.copy(timeRed = Some(8), timestamp = fTimestamp)),
        secondImage = Some(vehicleImage.copy(timestamp = sTimestamp)),
        pardonRedTime = 5l,
        minimumYellowTime = 10l,
        measuredSpeed = 5,
        thresholdSpeed = 4) must be(true -> 4)

      //measuredSpeed < thresholdSpeed
      qualifier.isViolation(
        firstImage = Some(vehicleImage.copy(timeRed = Some(8), timestamp = fTimestamp)),
        secondImage = Some(vehicleImage.copy(timestamp = sTimestamp)),
        pardonRedTime = 5l,
        minimumYellowTime = 10l,
        measuredSpeed = 5,
        thresholdSpeed = 6) must be(false -> 6)
    }
  }
}
