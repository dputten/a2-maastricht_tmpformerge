/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

package csc.sectioncontrol.qualify

/**
 * a violation qualifier for corridors that have a service type of SectionControl (trajectcontrole)
 */
class SectionControlQualifier extends ViolationQualifier {
  /**
   * @param measuredSpeed speed that car was driving
   * @param scheduledSpeedLimit max speed allowed
   * @param dynamicSpeedLimit
   * @param vehicleClassSpeedLimit speedlimit of vehicle for it's class (e.g. large truck)
   * @param speedLimitMargins margin of speedlimit
   * @param enforceDynamic inducate is dynamx is used
   * @return returns a tuple of 2 values. The first is a boolean indicating is the vehicle is in violation.
   * The second is the speed that it is enforcing on
   */
  override def isViolation(measuredSpeed: Int, scheduledSpeedLimit: Int, dynamicSpeedLimit: Either[AnyRef, Option[Int]], vehicleClassSpeedLimit: Option[Int], speedLimitMargins: Map[Int, Int], enforceDynamic: Boolean): (Boolean, Int) = {
    super.isViolation(measuredSpeed, scheduledSpeedLimit, dynamicSpeedLimit, vehicleClassSpeedLimit, speedLimitMargins, enforceDynamic)
  }

  protected def checkSpeed(measuredSpeed: Int, enforcedSpeedLimit: Int, margin: Int): Boolean = {
    measuredSpeed >= enforcedSpeedLimit + margin
  }

}
