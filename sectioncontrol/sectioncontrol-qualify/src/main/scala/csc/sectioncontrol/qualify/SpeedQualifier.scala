/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

package csc.sectioncontrol.qualify

/**
 * a violation qualifier for corridors that have a service type of Speed (Snelheid)
 */
class SpeedQualifier extends ViolationQualifier {
  /**
   * @param measuredSpeed speed that vehicle was driving
   * @param scheduledSpeedLimit max speed allowed according to the schedule
   * @param dynamicSpeedLimit
   * @param vehicleClassSpeedLimit speedlimit of vehicle for it's class (e.g. large truck it's 80 km/h)
   * @param speedLimitMargins margin of speedlimit
   * @param enforceDynamic indicate is dynamax is used
   * @return returns a tuple of 2 values. The first is a boolean indicating is the vehicle is in violation.
   * The second is the speed that it is enforcing on
   */
  override def isViolation(measuredSpeed: Int, scheduledSpeedLimit: Int, dynamicSpeedLimit: Either[AnyRef, Option[Int]], vehicleClassSpeedLimit: Option[Int], speedLimitMargins: Map[Int, Int], enforceDynamic: Boolean): (Boolean, Int) = {
    super.isViolation(measuredSpeed, scheduledSpeedLimit, dynamicSpeedLimit, vehicleClassSpeedLimit, speedLimitMargins, enforceDynamic)
  }

  protected def checkSpeed(measuredSpeed: Int, enforcedSpeedLimit: Int, margin: Int): Boolean = {
    measuredSpeed >= enforcedSpeedLimit + margin
  }

}
