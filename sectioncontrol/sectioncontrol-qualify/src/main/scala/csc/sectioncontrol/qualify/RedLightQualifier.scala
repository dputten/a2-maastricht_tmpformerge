/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

package csc.sectioncontrol.qualify

/**
 * a violation qualifier for corridors that have a service type of RedLight (Roodlicht)
 */
class RedLightQualifier {

  /**
   * used to determine if vehicle is a redlight violation
   * @param timestamp of picture taken
   * @param timeYellow duration that yellow was on when vehicle passed through
   * @param timeRed duration that red was on when vehicle passed through
   */
  type VehicleImage = { val timestamp: Long; val timeYellow: Option[Long]; val timeRed: Option[Long] }

  /**
   * generic method that checks if all given options are defined
   * @param options variable argument list of options
   * @return true is all given optionals are defined else false
   */
  private def isDefined(options: Option[_]*): Boolean = !options.exists(_.isEmpty)

  /**
   * floor the microseconds to 1 decimal
   * e.g.15:35:43.721 becomes 15:35:43.700
   * @param timestamp
   * @return
   */
  def floorToOneDecimal(timestamp: Long) = ((timestamp / 100) * 100)

  /**
   *
   * @param firstImage optional info regarding the first redlight image
   * @param secondImage optional info regarding the second redlight image
   * @param pardonRedTime configured minimum redtime the a redlight must be on to be considered as a violation
   * @param minimumYellowTime minimum time that a yellow light must be on while the vehicle when through it
   * @param measuredSpeed speed of vehicle
   * @param thresholdSpeed minimal speed the a vehicle must be driving
   * @return a tuple of 2 values. The first is a boolean indicating is the vehicle is in violation.
   * The second is the speed that it is enforcing on
   */
  def isViolation(firstImage: Option[VehicleImage], secondImage: Option[VehicleImage], pardonRedTime: Long, minimumYellowTime: Long, measuredSpeed: Int, thresholdSpeed: Int): (Boolean, Int) = {

    (firstImage, secondImage) match {
      case (Some(first), Some(second)) if (isDefined(first.timeRed, first.timeYellow, second.timeRed, second.timeYellow)) ⇒
        val inViolation =
          first.timeRed.get > pardonRedTime &&
            first.timeYellow.get >= minimumYellowTime &&
            measuredSpeed >= thresholdSpeed &&
            second.timeRed.get >= first.timeRed.get + (floorToOneDecimal(second.timestamp - first.timestamp) - 100)

        (inViolation, thresholdSpeed)
      case _ ⇒ (false, 0)
    }
  }
}
