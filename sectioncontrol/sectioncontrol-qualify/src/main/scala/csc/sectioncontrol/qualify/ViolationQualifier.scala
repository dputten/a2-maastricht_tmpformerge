/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.qualify

import csc.sectioncontrol.sign.SoftwareChecksum
import collection.immutable.SortedSet

trait ViolationQualifier {
  def isViolation(measuredSpeed: Int,
                  scheduledSpeedLimit: Int,
                  dynamicSpeedLimit: Either[AnyRef, Option[Int]],
                  vehicleClassSpeedLimit: Option[Int],
                  speedLimitMargins: Map[Int, Int],
                  enforceDynamic: Boolean): (Boolean, Int) = {
    // there are never violations when dynamic speed is undetermined or the system is in standby
    if (dynamicSpeedLimit.isLeft) {
      (false, 0)
    } else {
      if (dynamicSpeedLimit.right.get.isDefined) {
        val dynamicSpeedLimitValue = dynamicSpeedLimit.right.get.get

        // if we don't enforce the dynamic speed limit, and a dynamic limit is set and it is different
        // from the scheduled speed limit, no violations are produced.
        if (!enforceDynamic && dynamicSpeedLimitValue != scheduledSpeedLimit) {
          (false, 0)
        } else {
          val roadSpeedLimit = if (enforceDynamic) dynamicSpeedLimitValue else scheduledSpeedLimit
          val vehicleSpeedLimit = vehicleClassSpeedLimit.getOrElse(roadSpeedLimit)

          // enforce on the minimum of vehicle and road settings
          val enforcedSpeedLimit = roadSpeedLimit.min(vehicleSpeedLimit)
          val margin = speedLimitMargin(enforcedSpeedLimit, speedLimitMargins)

          (checkSpeed(measuredSpeed, enforcedSpeedLimit, margin), enforcedSpeedLimit)
        }
      } else {
        val roadSpeedLimit = scheduledSpeedLimit
        val vehicleSpeedLimit = vehicleClassSpeedLimit.getOrElse(roadSpeedLimit)

        // enforce on the minimum of vehicle and road settings
        val enforcedSpeedLimit = roadSpeedLimit.min(vehicleSpeedLimit)
        val margin = speedLimitMargin(enforcedSpeedLimit, speedLimitMargins)

        (checkSpeed(measuredSpeed, enforcedSpeedLimit, margin), enforcedSpeedLimit)
      }
    }
  }

  private def speedLimitMargin(speed: Int, margins: Map[Int, Int]): Int = {
    val indexes = SortedSet(margins.toSeq: _*)
    val nextLargerMargin = indexes.dropWhile(_._1 < speed).headOption.map(_._2).getOrElse(0)
    val nextSmallerMargin = indexes.takeWhile(_._1 <= speed).lastOption.map(_._2).getOrElse(0)
    math.max(nextSmallerMargin, nextLargerMargin)
  }

  protected def checkSpeed(measuredSpeed: Int, enforcedSpeedLimit: Int, margin: Int): Boolean

  lazy val sha = SoftwareChecksum.createHashFromRuntimeClass(this).getOrElse(SoftwareChecksum("", ""))
}
