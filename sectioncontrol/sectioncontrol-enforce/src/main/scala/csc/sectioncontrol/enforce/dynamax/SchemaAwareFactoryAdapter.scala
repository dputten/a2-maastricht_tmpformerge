/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.dynamax

import org.xml.sax.InputSource
import javax.xml.parsers.{ SAXParserFactory, SAXParser }
import xml.{ Elem, TopScope }
import xml.parsing.NoBindingFactoryAdapter
import javax.xml.transform.stream.StreamSource
import javax.xml.XMLConstants
import javax.xml.validation.{ SchemaFactory, Schema }
import java.io.{ InputStream, StringReader, File }
import org.slf4j.LoggerFactory

/**
 * Implementation of a contentHandler Adapter for the SAX parser using a XSD.
 * @param schema the Schema of the XML
 */
class SchemaAwareFactoryAdapter(schema: Schema) extends NoBindingFactoryAdapter {
  private val log = LoggerFactory.getLogger(this.getClass.getName)
  /**
   * Constructor with a XSD path as a String
   * @param xsd the XSD path
   */
  def this(xsd: String) {
    this({
      val sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
      sf.newSchema(new StreamSource(new File(xsd)))
    })
  }

  /**
   * Constructor with a XSD path as a String
   * @param xsd the XSD path
   */
  def this(xsd: InputStream) {
    this({
      val sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
      sf.newSchema(new StreamSource(xsd))
    })
  }

  /**
   * Load the XML and create a Elem
   * @param xml the xm as String to be processed
   */
  def loadXML(xml: String): Elem = {
    loadXML(new InputSource(new StringReader(xml)))
  }

  /**
   * Load the XML and create a Elem
   * @param source the XML as a InputStream
   */
  def loadXML(source: InputSource): Elem = {
    // create parser
    val parser: SAXParser = try {
      val f = SAXParserFactory.newInstance()
      f.setNamespaceAware(true)
      f.setFeature("http://xml.org/sax/features/namespace-prefixes", true)
      f.newSAXParser()
    } catch {
      case e: Exception ⇒
        log.error("error: Unable to instantiate parser")
        throw e
    }

    val xr = parser.getXMLReader()
    val vh = schema.newValidatorHandler()
    vh.setContentHandler(this)
    xr.setContentHandler(vh)

    // parse file
    scopeStack.push(TopScope)
    xr.parse(source)
    scopeStack.pop
    return rootElem.asInstanceOf[Elem]
  }
}

/**
 * Supported Statics to use the SchemaAwareFactoryAdapter
 */
object SchemaAwareFactoryAdapter {
  /**
   * Load a XML with a XSD schema
   * @param xsd the XSD path
   * @param xml the XML
   */
  def loadXml(xsd: String, xml: String): Elem = {
    new SchemaAwareFactoryAdapter(xsd).loadXML(xml)
  }
  /**
   * Load a XML with a XSD schema
   * @param xsd the XSD inputStream
   * @param xml the XML
   */
  def loadXml(xsd: InputStream, xml: String): Elem = {
    new SchemaAwareFactoryAdapter(xsd).loadXML(xml)
  }

}