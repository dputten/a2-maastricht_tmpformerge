/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.dynamax

import org.jboss.netty.util.{ Timeout, HashedWheelTimer, TimerTask }
import java.util.concurrent.TimeUnit
import java.util.Date
import org.xml.sax.SAXParseException
import akka.actor.ActorRef
import csc.akkautils.DirectLogging

/**
 * Implementation of the processing of received XML.
 * @param xsd: The xsd where the received XML is described
 * @param checksumApp: The application checksum of Dynamax
 * @param checksumConfig: The configurationchecksum of Dynamax
 * @param maxIdleTime_mSecs: The maximum Idle time of the receiver.
 * @param recipients list of receivers
 */
class DynamaxXMLProcessor(xsd: String, checksumApp: String, checksumConfig: String, maxIdleTime_mSecs: Long, recipients: Set[ActorRef]) extends DirectLogging {
  //The timer
  val timer = new HashedWheelTimer()
  //The last timeout
  var lastTimeout: Option[Timeout] = None
  //The last received Sequence number
  var lastSequenceNr = -1L
  //this is used to calculate the technical pardonTime when a message is missing
  //This message should be send between last correct received message and the current.
  //So in between we don't know the Dynamax state => this should be the technical pardon Time
  var lastTimeRecvCorrectMessage: Date = new Date
  //Is processor in error state, starts in error (no connection) so the system processes the first received message
  var errorState = true
  //  When no message is received before x seconds after starting sending
  // a undetermined speed limit (SSS.DYN.33)
  resetTimer

  /**
   * Process the received XML.
   * The message or failure will be send further to the recipients when necessary
   */
  def processMessage(xml: String) = {
    //now roundOff to seconds
    val now = new Date(System.currentTimeMillis() / 1000 * 1000)
    createAndValidate(xml) match {
      case Some(hhmUpdate) ⇒ {
        //received correct message
        lastTimeRecvCorrectMessage = now
        //if delayed message skip it
        val oldTimeLimit = new Date(now.getTime - maxIdleTime_mSecs)
        if (hhmUpdate.sendTime.after(oldTimeLimit)) {
          if (errorState) {
            //resolved from error
            errorState = false
            //use now as changed time
            val update = new SectionSignUpdateMessage(hhmUpdate.updateMsg.sectionSign, 0, now)
            val newMsg = new HHMSignUpdateMessage(sequenceNr = hhmUpdate.sequenceNr,
              sendTime = now, repeat = hhmUpdate.repeat,
              checksumMsg = hhmUpdate.checksumMsg,
              checksumApp = hhmUpdate.checksumApp,
              checksumConfig = hhmUpdate.checksumConfig,
              updateMsg = update,
              disturbance = None)
            sendToRecipients(newMsg)
            if (hhmUpdate.disturbance != None) {
              val newDisturbance = new HHMSignUpdateMessage(sequenceNr = hhmUpdate.sequenceNr,
                sendTime = now, repeat = hhmUpdate.repeat,
                checksumMsg = hhmUpdate.checksumMsg,
                checksumApp = hhmUpdate.checksumApp,
                checksumConfig = hhmUpdate.checksumConfig,
                updateMsg = update,
                disturbance = hhmUpdate.disturbance)

              sendToRecipients(newDisturbance)
            }
          } else {
            //just normal operation
            //send only information messages
            if (!hhmUpdate.repeat) {
              sendToRecipients(hhmUpdate)
            }
          }
        }
      }
      case None ⇒ {
        //received incorrect message
        if (!errorState) {
          //detected error
          errorState = true
          //send undetermined message
          //pardon time is the time of the last correct received sequence Nr and now
          //when this message has a correct sequenceNr now and lastTimeRecvCorrectSequenceNr
          // are equal => pardonTime_mSecs = 0
          var pardonTime_mSecs = now.getTime - lastTimeRecvCorrectMessage.getTime
          if (pardonTime_mSecs < 0) {
            //should never happen but just being extra careful
            pardonTime_mSecs = 0
          }
          //SSS.DYN.33 goto UNDETERMINED when Dynamax messages fails
          val update = new SectionSignUpdateMessage(AggregatedSignValue.UNDETERMINED, pardonTime_mSecs, now)
          val failMsg = new HHMSignUpdateMessage(lastSequenceNr, now, false, "", "", "", update, None)
          sendToRecipients(failMsg)
        }
      }
    }
    //reset timeout
    resetTimer()
  }

  /**
   * Parses the XML and creates a HHMSignUpdateMessage and validates the sequenceNumber and the checksums.
   * @param xml: The received XML
   * @return HHMSignUpdateMessage when the validation succeeds and None when it fails
   */
  def createAndValidate(xml: String): Option[HHMSignUpdateMessage] = {
    var forwardMsg: Option[HHMSignUpdateMessage] = None
    try {
      var validationError = false
      val hhmUpdate = SignUpdateMessageConverter.createHHMSignUpdateMessage(xml, Some(xsd))
      //Check the SequenceNumber
      //The sequence number is only correct when it is one higher than the previous received sequence number.
      //After a failure the sequence is correct only when we receive a in order sequence number.
      //So if the expected message is received after a failure, it is still in error until the next expected message is received
      //When receiving negative values it's probably an error, but the algorithm can also handle this
      val expectedSequence = lastSequenceNr + 1
      lastSequenceNr = hhmUpdate.sequenceNr
      if (expectedSequence > 0 && hhmUpdate.sequenceNr != expectedSequence) {
        validationError = true
        log.error("Missing message detected: Sequence expected= %d received=%d".format(expectedSequence, hhmUpdate.sequenceNr))
      }
      //check Dynamax checksums
      if (hhmUpdate.checksumApp != checksumApp) {
        validationError = true
        log.error("Checksum Application Failure: expected= %s received=%s".format(checksumApp, hhmUpdate.checksumApp))
      }
      if (hhmUpdate.checksumConfig != checksumConfig) {
        validationError = true
        log.error("Checksum Configuration Failure: expected= %s received=%s".format(checksumConfig, hhmUpdate.checksumConfig))
      }
      if (!validationError) {
        forwardMsg = Some(hhmUpdate)
      }
    } catch {
      case ex: ChecksumFailureException ⇒ {
        log.error("Checksum Message Failure: %s".format(ex))
      }
      case sax: SAXParseException ⇒ {
        log.error("XML isn't valid: %s".format(sax))
      }
    }
    forwardMsg
  }

  /**
   * Resets the Idle Timer
   * SSS.DYN.32
   */
  def resetTimer() {
    lastTimeout foreach {
      timeout ⇒
        {
          if (!timeout.isExpired && !timeout.isCancelled) {
            timeout.cancel
          }
        }
    }
    val timerTimeout = timer.newTimeout(new TimerTask() {
      def run(timeout: Timeout) {
        IdleDetected()
      }
    }, maxIdleTime_mSecs, TimeUnit.MILLISECONDS)
    lastTimeout = Some(timerTimeout)
  }

  /**
   * stop the Idle Timer
   * Used for testing
   */
  def stopTimer() {
    lastTimeout foreach {
      timeout ⇒
        {
          if (!timeout.isExpired && !timeout.isCancelled) {
            timeout.cancel
          }
          lastTimeout = None
        }
    }
  }

  /**
   * Process the Idle connection event, by sending a  HHMSignUpdateMessage to the recipients
   *  SSS.DYN.32 Idle time
   * SSS.DYN.33 goto UNDETERMINED when Dynamax messages fails
   */
  def IdleDetected() {
    log.warning("Detected Idle dynamax-hhm connection")
    val now = new Date(System.currentTimeMillis() / 1000 * 1000)
    val update = new SectionSignUpdateMessage(AggregatedSignValue.UNDETERMINED, maxIdleTime_mSecs, now)
    val idleMsg = new HHMSignUpdateMessage(sequenceNr = lastSequenceNr + 1, sendTime = now, repeat = false, checksumMsg = "", checksumApp = "", checksumConfig = "", updateMsg = update, disturbance = None)
    sendToRecipients(idleMsg)
    errorState = true
  }
  /**
   * Send the message to all recipients
   */
  private def sendToRecipients(msg: Any) {
    for (actorRef ← recipients) {
      if (actorRef != null) {
        actorRef ! msg
      }
    }
  }
}