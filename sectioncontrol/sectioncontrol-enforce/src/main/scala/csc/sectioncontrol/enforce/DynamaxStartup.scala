/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce

import scala.collection.JavaConversions._

import org.apache.activemq.camel.component.ActiveMQComponent
import org.apache.activemq.broker.BrokerRegistry

import akka.actor.{ Props, ActorRef, ActorSystem }
import akka.camel.CamelExtension

import dynamax.{ EnforceDynamax, HHMDynamaxReceiver }
import csc.curator.utils.Curator

case class ZkDynamaxIface(id: String, uri: String, checksumApp: String, checksumCfg: String, timeoutmSec: Long, maxCacheUse: Long, reportFile: String)
case class ZkDynamaxConfig(xsd: String, activeMQComponentName: String, activeMQBroker: String)

class DynamaxStartup(curator: Curator,
                     system: ActorSystem,
                     zkPathDynamax: String,
                     zkPathSystem: String,
                     baseBackup: Int,
                     maxRetries: Int,
                     getActorSystem: (String) ⇒ Option[ActorSystem],
                     getSpeedLog: (String) ⇒ ActorRef) {

  def start() {
    //read config
    //expect ZkDynamaxConfig to be present.
    val dynCfg = curator.get[ZkDynamaxConfig](zkPathDynamax + "/config").get
    var camelContext = CamelExtension(system).context
    camelContext.addComponent(dynCfg.activeMQComponentName, ActiveMQComponent.activeMQComponent(dynCfg.activeMQBroker))

    //read MQ config
    val listIface = curator.get[List[ZkDynamaxIface]](zkPathDynamax + "/iface").get
    for (iface ← listIface) {
      val enforce = system.actorOf(Props(new EnforceDynamax(curator = curator,
        mqId = iface.id,
        maxCacheUse = iface.maxCacheUse,
        zkPathPrefix = zkPathSystem,
        getActorSystem = getActorSystem,
        getSpeedLog = getSpeedLog)))
      system.actorOf(Props(new HHMDynamaxReceiver(
        uri = iface.uri,
        xsd = dynCfg.xsd,
        checksumApp = iface.checksumApp,
        checksumConfig = iface.checksumCfg,
        timeoutmSec = iface.timeoutmSec,
        recipients = Set(enforce))))
    }
  }
  def stop() {
    //stop brokers
    val brokers = BrokerRegistry.getInstance.getBrokers
    brokers.foreach(pair ⇒ pair._2.stop)
  }

}

