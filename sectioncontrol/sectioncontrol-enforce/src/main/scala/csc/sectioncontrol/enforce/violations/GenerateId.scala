/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

package csc.sectioncontrol.enforce.violations

/**
 * Mix-in trait that defines a nextId method that returns the next Int in the sequence
 * 1..maxInt each time it is called. The first call returns 1.
 *
 * @author Maarten Hazewinkel
 */
trait GenerateId {
  private def from(n: Int): Stream[Int] = Stream.cons(n, from(n + 1))
  private var stream: Stream[Int] = from(0)

  /**
   * Returns the next Int in the sequence 1..maxInt, advancing by 1 for each call
   */
  def nextId: Int = { synchronized { stream = stream.tail; stream.head } }
}
