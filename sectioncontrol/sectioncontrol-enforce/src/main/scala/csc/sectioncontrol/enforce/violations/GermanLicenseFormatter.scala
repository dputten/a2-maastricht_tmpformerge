package csc.sectioncontrol.enforce.violations

import io.Source

object GermanLicenseFormatter {

  def apply(license: String, rawLicense1: Option[String], rawLicense2: Option[String]): String = {
    val raw = getRawLicence(rawLicense1, rawLicense2)
    val tmpLic = raw match {
      case None ⇒ {
        val license1 = license.replaceFirst("([A-Za-zäöüÄÖÜß])([0-9])", "$1-$2")
        germanPrefixes.find(license1.startsWith(_)) match {
          case Some(prefix) ⇒
            license1.substring(0, prefix.length) + "-" + license1.substring(prefix.length)

          case None ⇒
            license1.replace("-", "--")
        }
      }
      case Some(rawLic) ⇒ {
        rawLic.replaceAll("""[\*_\(\)\/ |]""", "-")
      }
    }
    tmpLic.replace("--", "-")
  }

  def getRawLicence(rawLicense1: Option[String], rawLicense2: Option[String]): Option[String] = {
    (rawLicense1, rawLicense2) match {
      case (Some(lic), None) ⇒ Some(lic)
      case (None, Some(lic)) ⇒ Some(lic)
      case (None, None)      ⇒ None
      case (Some(lic1), Some(lic2)) ⇒ {
        if (lic1 == lic2) {
          Some(lic1)
        } else {
          //try to get best guess
          val posArms1 = lic1.indexOf("*")
          val posArms2 = lic2.indexOf("*")
          if (posArms1 > 0) {
            if (posArms2 < 0)
              Some(lic1)
            else {
              //both look correct check prefix
              val prefix1 = germanPrefixes.contains(lic1.substring(0, posArms1))
              val prefix2 = germanPrefixes.contains(lic2.substring(0, posArms2))
              if (!prefix1 && !prefix2) {
                //try to correct
                val prefix1 = germanPrefixes.find(lic1.startsWith(_))
                val prefix2 = germanPrefixes.find(lic2.startsWith(_))
                (prefix1, prefix2) match {
                  case (Some(pre), _) ⇒ Some(lic1.replace(pre, pre + "*"))
                  case (_, Some(pre)) ⇒ Some(lic2.replace(pre, pre + "*"))
                  case _ ⇒ {
                    //both licences doesn't look like a german license
                    Some(lic1)
                  }
                }

              } else if (!prefix1 && prefix2) {
                //both may correct or not correct just pick the first
                //=> pick only the second when prefix2 == true and prefix1 == false
                Some(lic2)
              } else {
                Some(lic1)
              }
            }

          } else if (posArms2 > 0) {
            Some(lic2)
          } else {
            //both doesn't comply to german check prefix
            //try to correct
            val prefix1 = germanPrefixes.find(lic1.startsWith(_))
            val prefix2 = germanPrefixes.find(lic2.startsWith(_))
            (prefix1, prefix2) match {
              case (Some(pre), _) ⇒ Some(lic1.replace(pre, pre + "*"))
              case (_, Some(pre)) ⇒ Some(lic2.replace(pre, pre + "*"))
              case _ ⇒ {
                //both licences doesn't look like a german license
                Some(lic1)
              }
            }
          }
        }
      }
    }
  }

  /**
   * German license has to start with kreis prefix and should be of format
   * XXX-XXX-XXXXX (- are only as indication and not part of the license) where the first XXXX
   * are the kreis part and is 1 to 3 characters.
   * @param license
   * @return
   */
  def validate(license: String): Boolean = {
    """^[A-Z0-9ÄÖÜß]{2,11}$""".r.findFirstIn(license).isDefined
  }

  def validateFormatted(formattedLicense: String): Boolean =
    """^[A-Z0-9ÄÖÜß]{1,3}(-[A-Z0-9ÄÖÜß]{1,3})?-[A-Z0-9ÄÖÜß]{1,5}$""".r.findFirstIn(formattedLicense).isDefined

  val germanPrefixes = {
    val sourceFileName = "/GermanLicensePrefixes.txt"
    val source = Source.fromInputStream(getClass.getResourceAsStream(sourceFileName), "ISO-8859-1")
    source.getLines().toSeq.sorted.reverse
  }
}
