/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.excludelog

import akka.actor.{ ActorLogging, Actor }
import akka.event.LoggingReceive

import csc.curator.utils.Curator

import csc.sectioncontrol.enforce.{ SystemEventData, Cleanup }
import csc.sectioncontrol.storage.ZkState
import csc.sectioncontrol.storagelayer.excludelog.{ Exclusions, SuspendEvent, ExcludeLogLayer }
import csc.sectioncontrol.storagelayer.state.StateService
import csc.sectioncontrol.storagelayer.corridor.CorridorService

/**
 * Log enforcement suspend and resume events to zookeeper, generate an overview of the resulting exclusion
 * periods when no violation should be registered, handle cleanup of stored event.
 *
 * @author Maarten Hazewinkel
 */
class ExcludeLog(val systemId: String, curator: Curator) extends Actor with ActorLogging {
  val storage = new ExcludeLogLayer(curator)

  def receive = LoggingReceive {
    case event: SystemEventData if (event.isSuspendOrResumeEvent) ⇒
      storage.saveEvent(systemId, SuspendEvent(event))

    case GetExclusions(corridorInfoId) ⇒ {
      val corridorMapping = CorridorService.getCorridorIdMapping(curator, systemId, corridorInfoId)
      val stateEvents = corridorMapping.map(mapping ⇒ {

        val states = StateService.getStateLogHistory(systemId, mapping.corridorId, 0L, System.currentTimeMillis(), curator)
        states.map { ss ⇒
          SuspendEvent("SystemState", ss.timestamp, None, 1, isSuspendedState(ss.state), Some(ss.reason))
        }
      })

      sender ! Exclusions(corridorInfoId, storage.readEvents(systemId, corridorInfoId) ++ stateEvents.getOrElse(Seq()))
    }

    case Cleanup(retentionDays) ⇒
      storage.cleanEvents(systemId, retentionDays)
  }
  def isSuspendedState(state: String): Boolean = !ZkState.enforceStates.contains(state)
}

/**
 * Message to request exclusions for specified corridor
 * @param corridorId the corridor Id
 */
case class GetExclusions(corridorId: Int)

