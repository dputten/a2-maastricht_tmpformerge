/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.dynamax

import collection.mutable.{ ListBuffer, HashMap }
import scala.Either

import akka.actor.{ ActorLogging, Actor, ActorRef, ActorSystem }

import csc.config.Path
import csc.curator.utils.Curator

import csc.sectioncontrol.enforce.{ SpeedEventData, SpeedIndicatorStatus }
import csc.sectioncontrol.messages.SystemEvent
import csc.sectioncontrol.storage._

class EnforceDynamax(curator: Curator, mqId: String, maxCacheUse: Long, zkPathPrefix: String, getActorSystem: (String) ⇒ Option[ActorSystem], getSpeedLog: (String) ⇒ ActorRef) extends Actor with ActorLogging {
  val lastUpdate = 0
  val cache = new HashMap[String, ListBuffer[ZkCorridor]]
  val zkPathCorridors = "corridors"

  def receive = {
    case msg: HHMSignUpdateMessage ⇒ {
      updateCorridorCache()
      msg.disturbance match {
        case Some(disturbance) ⇒ processDisturbance(disturbance)
        case None              ⇒ processSpeed(msg.updateMsg, msg.sequenceNr)
      }
    }
  }

  private def updateCorridorCache() {
    //update only when cache is too old
    if (lastUpdate + maxCacheUse < System.currentTimeMillis()) {
      cache.clear()
      val systems = curator.getChildren(Path(zkPathPrefix))
      for (systemPath ← systems) {
        //is dynamax started for this system
        val systemConfigPath = Paths.Systems.getConfigPath(systemPath.nodes.last.name)
        val isDynamax = curator.get[ZkSystem](Path(systemConfigPath)) match {
          case Some(systemConfig) ⇒ systemConfig.isDynamax
          case None ⇒ {
            log.warning("System Config not found %s".format(systemPath))
            false
          }
        }
        if (isDynamax) {
          val path = systemPath / zkPathCorridors
          val corridors = curator.getChildren(path)
          for (corridorPath ← corridors) {
            val configPath = corridorPath / "config"
            val corridorOpt = curator.get[ZkCorridor](configPath)
            corridorOpt match {
              case Some(corridor) ⇒ {
                if (corridor.dynamaxMqId == mqId) {
                  val list = getSystemList(systemPath.nodes.last.name)
                  list += corridor
                }
              }
              case None ⇒ {
                //very strange child is gone
                log.warning("Corridor %s is gone".format(corridorPath))
              }
            }
          }
        }
      }
    }
  }
  private def processDisturbance(disturbance: DisturbanceEvent) {
    //create system event for all systems
    val templateMsg = SystemEvent(componentId = "Dynamax-%s-%s".format(mqId, disturbance.component),
      timestamp = disturbance.time.getTime,
      systemId = "system",
      eventType = "Dynamax-%s-%s".format(disturbance.disturbanceType, disturbance.state),
      userId = "Dynamax",
      reason = Some(disturbance.message))
    for (system ← cache.keySet) {
      val actorSystemOpt = getActorSystem(system)
      actorSystemOpt match {
        case Some(actorSystem) ⇒ actorSystem.eventStream.publish(templateMsg.copy(systemId = system))
        case None              ⇒ log.error("MQId[%s] System %s isn't started. Unable to send systemEvent [%s]".format(mqId, system, templateMsg))
      }
    }
  }

  private def processSpeed(updateMsg: SectionSignUpdateMessage, sequenceNr: Long) {
    for ((system, corridorList) ← cache) {
      for (corridor ← corridorList) {
        //check if speed is an approved speed
        val speed: Either[SpeedIndicatorStatus.Value, Option[Int]] = if (corridor.approvedSpeeds.contains(updateMsg.sectionSign.id)) {
          Right(Some(updateMsg.sectionSign.id))
        } else {
          Left(SpeedIndicatorStatus.Undetermined)
        }
        //Send Speed Update
        val msg = new DynamaxSpeedData(effectiveTimestamp = updateMsg.time.getTime,
          corridorId = corridor.info.corridorId,
          sequenceNumber = sequenceNr,
          speedSetting = speed,
          preChangeExclusionTime = Some(updateMsg.techPardonTime_mSecs))
        //find Actor
        getSpeedLog(system) ! msg
      }
    }
  }

  private def getSystemList(system: String): ListBuffer[ZkCorridor] = {
    cache.get(system) match {
      case Some(list) ⇒ list
      case None ⇒ {
        val list = new ListBuffer[ZkCorridor]()
        cache += system -> list
        list
      }
    }
  }
}

/**
 * Dynamax speed representation
 */
case class DynamaxSpeedData(effectiveTimestamp: Long,
                            corridorId: Int,
                            sequenceNumber: Long,
                            speedSetting: Either[SpeedIndicatorStatus.Value, Option[Int]],
                            override val preChangeExclusionTime: Option[Long]) extends SpeedEventData
