/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.speedlog

import akka.util.duration._
import akka.actor.{ ActorLogging, Actor, ActorRef }
import akka.event.LoggingReceive
import csc.sectioncontrol.storagelayer.corridor.CorridorService
import csc.sectioncontrol.storagelayer.state.StateService
import csc.config.Path
import csc.curator.utils.Curator
import csc.sectioncontrol.enforce._

/**
 * Log speed events to zookeeper, generate an overview of enforced speed settings over time, handle
 * cleanup of stored event.
 *
 * @author Maarten Hazewinkel
 */
class SpeedLog(val systemId: String,
               curator: Curator) extends Actor with ActorLogging {

  import SpeedLog._

  def receive = LoggingReceive {
    case event: SpeedEventData ⇒
      saveEvent(SpeedEvent(event))

    case GetEnforcedSpeeds(corridorId) ⇒
      val corridorMapping = CorridorService.getCorridorIdMapping(curator, systemId).find(_.infoId == corridorId)

      val stateEvents = corridorMapping.map(mapping ⇒ {
        StateService.getStateLogHistory(systemId, mapping.corridorId, 0L, System.currentTimeMillis(), curator)
      })

      sender ! SpeedSettingOverTime(corridorId, readEvents(corridorId), stateEvents.getOrElse(Seq()))

    case GenerateExcludeEvents(from, until, corridorId, defaultPreChangeExclusionTime, postChangeExclusionTime, excludeLog, scheduledSpeeds) ⇒
      log.debug("GenerateExcludeEvents: for " + corridorId)
      handleGenerateExcludeEvents(from, until, corridorId, defaultPreChangeExclusionTime, postChangeExclusionTime, excludeLog, scheduledSpeeds)
      sender ! GeneratedExcludeEvents

    case Cleanup(retentionDays) ⇒
      cleanEvents(retentionDays)
    case msg: AnyRef ⇒ log.warning("received unexpected message " + msg.toString)
  }

  private def handleGenerateExcludeEvents(from: Long, until: Long, corridorId: Int,
                                          defaultPreChangeExclusionTime: Long, postChangeExclusionTime: Long,
                                          excludeLog: ActorRef, scheduledSpeeds: Seq[ScheduledSpeed]) {
    val periodEvents: Seq[SpeedEvent] = getRelevantEventsForPeriod(from, until)

    // Iterate over all MSI changes during the day.
    periodEvents.filter(se ⇒ se.corridorId == corridorId && se.effectiveTimestamp > from && se.effectiveTimestamp < until).
      foreach {
        se ⇒
          val someSchedule = scheduledSpeeds.find(schedule ⇒ schedule.begin <= se.effectiveTimestamp && schedule.end > se.effectiveTimestamp)
          val seqNum = se.sequenceNumber * 4
          val preChangeExclusionTime = se.preChangeExclusionTime.getOrElse(defaultPreChangeExclusionTime)
          val (supportMsgBoard, speedMatches) = someSchedule match {
            case None           ⇒ (false, false)
            case Some(schedule) ⇒ (schedule.supportMsgBoard, se.speedSetting == Some(schedule.speed))
          }
          log.debug(">>> SpeedEvent: " + se)
          if (speedMatches || !supportMsgBoard) {
            // generate exclude periods by send suspend/unsuspend events to excludeLog
            log.debug(">>>Sending technical pardon time")
            excludeLog ! ExcludeEvent(se.effectiveTimestamp - preChangeExclusionTime, seqNum, se.corridorId, true, "Technical pardon time")
            excludeLog ! ExcludeEvent(se.effectiveTimestamp, seqNum + 1, se.corridorId, false, "Technical pardon time")
            se.speedSetting match {
              case Some(_) ⇒
                log.debug(">>>sending ESA pardon time")
                excludeLog ! ExcludeEvent(se.effectiveTimestamp, seqNum + 2, se.corridorId, true, "ESA pardon time")
                excludeLog ! ExcludeEvent(se.effectiveTimestamp + postChangeExclusionTime, seqNum + 3, se.corridorId, false, "ESA pardon time")
              case None ⇒
                ()
            }
          } else {
            // supportMsgBoard is on and speed doesn't match. Send a single suspend event.
            log.debug(">>>Sending suspend event; MSI does not support scheduled speed")
            excludeLog ! ExcludeEvent(se.effectiveTimestamp - preChangeExclusionTime, seqNum, se.corridorId, true,
              "Technical pardon time; MSI does not support scheduled speed")
          }
      }

    val maxEventSeqNum = if (!periodEvents.isEmpty) periodEvents.map(_.sequenceNumber).max else 0
    // Iterate over schedules
    scheduledSpeeds.sortBy(_.begin).zipWithIndex.foreach {
      case (ScheduledSpeed(begin, end, scheduledSpeed, supportMsgBoard), index) ⇒
        log.debug(">>> schedule from: %d / %d, speed: %d".format(begin, end, scheduledSpeed))

        val seqNum = (maxEventSeqNum + index + 1) * 4
        // determine if MSI-setting matches the scheduled speed at start of the schedule
        val speedMatches1 = periodEvents.reverse.find(se ⇒ se.effectiveTimestamp <= begin) match {
          case None ⇒
            log.debug(">>> no speedEvent < begin found")
            false
          case Some(speedEvent) ⇒
            log.debug(">>> speedEvent < begin=" + speedEvent.speedSetting)
            speedEvent.speedSetting == Some(scheduledSpeed)
        }
        // determine if MSI-setting matches the scheduled speed at the end of the schedule
        val speedMatches2 = periodEvents.reverse.find(se ⇒ se.effectiveTimestamp < end) match {
          case None ⇒
            log.debug(">>> no speedEvent < end found")
            false
          case Some(speedEvent) ⇒
            log.debug(">>> speedEvent < end=" + speedEvent.speedSetting)
            speedEvent.speedSetting == Some(scheduledSpeed)
        }

        if (speedMatches1 || !supportMsgBoard) {
          // suspend/unsuspend to generate ESA pardon period
          if (begin > from) {
            excludeLog ! ExcludeEvent(begin, seqNum + 2, corridorId, suspend = true, reason = "ESA pardon time (schedule)")
            excludeLog ! ExcludeEvent(begin + postChangeExclusionTime, seqNum + 3, corridorId, suspend = false, reason = "ESA pardon time (schedule)")
          }
        } else {
          // supportMsgBoard is on and speed does not match. Send a suspend event to excludeLog
          val event = ExcludeEvent(scala.math.max(begin, from), seqNum + 2, corridorId, suspend = true,
            reason = "ESA pardon time (schedule); MSI does not support scheduled speed")
          log.debug(">>> sending begin: " + event)
          excludeLog ! event
        }
        if (speedMatches2 || !supportMsgBoard) {
          // send suspend/unsuspend message to generate Yechnical pardon period
          if (end < until) {
            excludeLog ! ExcludeEvent(end - defaultPreChangeExclusionTime, 1, corridorId, suspend = true, reason = "Technical pardon time (schedule)")
            excludeLog ! ExcludeEvent(end, seqNum + 1, corridorId, suspend = false, reason = "Technical pardon time (schedule)")
          }
        } else {
          val event = ExcludeEvent(end - defaultPreChangeExclusionTime, 1, corridorId, suspend = true,
            reason = "Technical pardon time (schedule); MSI does not support scheduled speed")
          log.debug(">>> sending end: " + event)
          excludeLog ! event
        }
    }

    //reset all at end of the day
    val seqNum = (maxEventSeqNum + scheduledSpeeds.size + 1) * 4
    excludeLog ! ExcludeEvent(until, seqNum, corridorId, suspend = false, reason = "End of day. reset")
  }

  private def saveEvent(event: SpeedEvent) {
    log.debug("Save received speedevent: " + event)
    val storePath = buildCorridorPath(systemId, event.corridorId)
    if (!curator.exists(storePath))
      curator.createEmptyPath(storePath)
    curator.appendEvent(storePath / speedEventPrefix, event)
  }

  private def readEvents(corridorId: Int): Seq[SpeedEvent] = {
    val path = buildCorridorPath(systemId, corridorId)
    val allEvents = if (curator.exists(path)) curator.getChildren(path) else Seq()
    allEvents.
      filter(_.nodes.last.name.startsWith(speedEventPrefix)).
      map(curator.get[SpeedEvent](_)).
      flatten.
      groupBy(_.effectiveTimestamp).
      map {
        case (_, sameTimeEvents) ⇒ sameTimeEvents.sortBy(-_.sequenceNumber).head
      }.
      toSeq
  }

  private def getRelevantEventsForPeriod(from: Long, until: Long): Seq[SpeedEvent] = {
    val basePath = buildSpeedLogPath(systemId)
    val corridorIds = curator.getChildren(basePath).map(_.nodes.last.name.toInt)
    val allEvents = corridorIds.flatMap(readEvents).filter(se ⇒ se.effectiveTimestamp < until)
    val (eventsBefore, eventsAfter) = allEvents.span(se ⇒ se.effectiveTimestamp <= from)
    if (eventsBefore != Nil) eventsBefore.sortBy(_.effectiveTimestamp).last +: eventsAfter else eventsAfter
  }

  private def cleanEvents(retentionDays: Int) {
    val lastDeleteTime = System.currentTimeMillis() - (retentionDays + 1).days.toMillis
    val dataPath = buildSpeedLogPath(systemId)

    if (curator.exists(dataPath)) {
      val oldEvents = curator.getChildren(dataPath).flatMap {
        corridorPath ⇒
          {
            curator.getChildren(corridorPath).flatMap {
              eventPath ⇒
                {
                  if (eventPath.nodes.last.name.startsWith(speedEventPrefix)) {
                    curator.get[SpeedEvent](eventPath).
                      filter(_.effectiveTimestamp < lastDeleteTime).
                      map((eventPath, _))
                  } else {
                    None
                  }
                }
            }
          }
      }
      oldEvents.foreach(eventData ⇒ curator.delete(eventData._1))
    }
  }
}

object SpeedLog {
  val componentName: String = "speedLog"

  private val speedEventPrefix = "speedEvent"

  private val baseZkPath = Path("/ctes/systems")

  private def buildSpeedLogPath(systemId: String): Path = {
    baseZkPath / systemId / componentName
  }

  private def buildCorridorPath(systemId: String, corridorId: Int): Path = {
    buildSpeedLogPath(systemId) / corridorId.toString
  }
}

/**
 * Get a SpeedSettingsOverTime object back that shows what speed settings were in effect at any time.
 */
case class GetEnforcedSpeeds(corridorId: Int)

/**
 * Trigger generation of exclude events from dynamic speed changes for a corridor.
 *
 * @param from The beginning of the period for which to generate events.
 * @param until The end of the period for which to generate events.
 * @param corridorId The corridor for which to generate events.
 * @param preChangeExclusionTime How much time to exclude before speed changes. Technical pardon time.
 * @param postChangeExclusionTime How much time to exclude after speed changes. ESA pardon time.
 * @param excludeLog The actor to which to send the exclude events.
 * @param scheduleSpeeds The speeds from the schedules
 */
case class GenerateExcludeEvents(from: Long,
                                 until: Long,
                                 corridorId: Int,
                                 preChangeExclusionTime: Long,
                                 postChangeExclusionTime: Long,
                                 excludeLog: ActorRef,
                                 scheduleSpeeds: Seq[ScheduledSpeed])

/**
 * Signals completion of the GenerateExcludeEvents request
 */
case object GeneratedExcludeEvents

private[enforce] case class SpeedEvent(effectiveTimestamp: Long,
                                       sequenceNumber: Long,
                                       corridorId: Int,
                                       noSpeedReason: Option[String],
                                       speedSetting: Option[Int],
                                       preChangeExclusionTime: Option[Long]) {
  require(noSpeedReason.isEmpty || speedSetting.isEmpty, "One of 'speedSetting' and 'noSpeedReason' must be empty.")
}

private[enforce] object SpeedEvent {
  def apply(event: SpeedEventData): SpeedEvent = SpeedEvent(event.effectiveTimestamp,
    event.sequenceNumber,
    event.corridorId,
    event.speedSetting.left.toOption.map(_.toString),
    event.speedSetting.right.toOption.flatMap(identity(_)),
    event.preChangeExclusionTime)
}

case class ExcludeEvent(componentId: String,
                        effectiveTimestamp: Long,
                        corridorId: Option[Int],
                        sequenceNumber: Long,
                        isSuspendOrResumeEvent: Boolean,
                        suspend: Boolean,
                        suspensionReason: Option[String]) extends SystemEventData {
}

object ExcludeEvent {
  def apply(timestamp: Long, sequenceNum: Long, corridorId: Int, suspend: Boolean, reason: String): ExcludeEvent =
    ExcludeEvent(SpeedLog.componentName, timestamp, Some(corridorId), sequenceNum, true, suspend, Some(reason))
}

case class ScheduledSpeed(begin: Long,
                          end: Long,
                          speed: Int,
                          supportMsgBoard: Boolean)