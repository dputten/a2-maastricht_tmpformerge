/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce

import akka.actor.{ Actor, ActorRef, ActorSystem, Props }
import csc.sectioncontrol.storage.Decorate.DecorationType
import net.liftweb.json.{ DefaultFormats, Formats }

import csc.json.lift.EnumerationSerializer
import csc.curator.utils.Curator

import csc.akkautils._
import excludelog.ExcludeLog
import speedlog.SpeedLog
import timewatch.WatchTime
import csc.sectioncontrol.storage._
import csc.sectioncontrol.messages._
import csc.sectioncontrol.storagelayer.hbasetables.RowKeyDistributorFactory
import scala.Some

/**
 * Boot the generic enforce actors.
 *
 * The actors are set up in separate actor systems, one for each configured route/system.
 *
 * @author Maarten Hazewinkel
 */
trait EnforceBoot[ConfigType <: EnforceConfig] extends MultiSystemBoot {
  import EnforceBoot._

  private var dynamax: Option[DynamaxStartup] = None
  private var globalPardonDuringDaylightSavingsTimeSwitch: Boolean = false
  private val defaultFormats = DefaultFormats + new EnumerationSerializer(
    VehicleCategory,
    ZkWheelbaseType,
    VehicleCode,
    ZkIndicationType,
    ServiceType,
    ZkCaseFileType,
    EsaProviderType,
    VehicleImageType,
    DayReportVersion,
    SpeedIndicatorType,
    ConfigType, DecorationType, VehicleImageType)

  /**
   * load the configuration from zookeeper
   */
  protected def getEnforceConfig(systemId: String, curator: Curator): Option[ConfigType]

  protected def startEnforceActors(system: ActorSystem, systemId: String, curator: Curator, hbaseZkServers: String, config: ConfigType)

  override protected def formats(defaultFormats: Formats) = this.defaultFormats

  def pardonSwitch(): Boolean

  override def startupActors() {

    globalPardonDuringDaylightSavingsTimeSwitch = pardonSwitch()

    super.startupActors()

    val config = actorSystem.settings.config
    if (config.hasPath(zkServerQuorumConfigPath)) {

      def getCurator: Curator = {
        createCurator(system = actorSystem, formats = defaultFormats)
      }

      def getActorSystem(systemId: String): Option[ActorSystem] = {
        MultiSystemBoot.getActorSystem(systemId)
      }
      def getSpeedLog(systemId: String): ActorRef = {
        EnforceBoot.getSpeedLog(systemId)
      }
      val dyn = new DynamaxStartup(
        curator = getCurator,
        system = actorSystem,
        zkPathDynamax = "/ctes/configuration/dynamax",
        zkPathSystem = "/ctes/systems",
        baseBackup = 100,
        maxRetries = 50,
        getActorSystem = getActorSystem,
        getSpeedLog = getSpeedLog)
      dyn.start()
      dynamax = Some(dyn)
    } else {
      log.error("required key {} not defined in config", zkServerQuorumConfigPath)
    }
  }

  override def shutdown() {
    try dynamax.foreach(_.stop()) finally super.shutdown()
  }

  /**
   * Starts up the ExcludeLog, SpeedLog and WatchTime actors.
   * Also starts up the EventLogger actor.
   */
  def startupSystemActors(system: ActorSystem, systemId: String, curator: Curator, hbaseZkServerQuorum: String) {
    RowKeyDistributorFactory.init(curator)
    actorSystem.actorOf(Props(new Actor {
      protected def receive = {
        case "Start" ⇒
          val enforceConfig = getEnforceConfig(systemId, curator)
          enforceConfig.foreach { config ⇒
            startEventLogger(systemId, curator, system)
            system.actorOf(Props(new ExcludeLog(systemId, curator)), ExcludeLogName)
            system.actorOf(Props(new SpeedLog(systemId, curator)), SpeedLogName)
            system.actorOf(Props(new WatchTime(systemId, config.ntpServer, config.timeZone, getExcludeLog(systemId), curator, config.pardonDuringDSTSwitch.getOrElse(globalPardonDuringDaylightSavingsTimeSwitch))),
              WatchTimeName)
            startEnforceActors(system, systemId, curator, hbaseZkServerQuorum, config)
          }
          context.stop(self)
      }
    }), moduleName + "_" + systemId + "_Boot") ! "Start"
  }
}

object EnforceBoot {
  val ExcludeLogName = "ExcludeLog"
  val SpeedLogName = "SpeedLog"
  val WatchTimeName = "WatchTime"

  /**
   * Find the ExcludeLog actor for the given system.
   * Throws an exception if no actor system can be found for the system id.
   */
  def getExcludeLog(systemId: String): ActorRef =
    MultiSystemBoot.getActorSystem(systemId).get.actorFor("/user/" + ExcludeLogName)

  /**
   * Find the SpeedLog actor for the given system
   * Throws an exception if no actor system can be found for the system id.
   */
  def getSpeedLog(systemId: String): ActorRef =
    MultiSystemBoot.getActorSystem(systemId).get.actorFor("/user/" + SpeedLogName)
}

/**
 * Configuration settings required for the generic enforce actors
 */
trait EnforceConfig {
  def ntpServer: String
  def timeZone: String
  def verifyRecognition: String
  def pardonDuringDSTSwitch: Option[Boolean]
}

