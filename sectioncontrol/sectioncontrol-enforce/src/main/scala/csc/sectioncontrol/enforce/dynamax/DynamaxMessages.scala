/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.dynamax

import java.util.Date
import java.text.SimpleDateFormat

/**
 * The exception that is thrown when a checksum fails
 */
class ChecksumFailureException(expected: String, actual: String) extends RuntimeException {
  override def toString = "Checksum Failure expected=" + expected + " actual=" + actual
  def getExpected() = expected
  def getActual() = actual
}

/**
 * Possible values of a Sign:
 * XXKM means the maximum speed is set to XX
 * BLANK means use the location maximum speed
 * UNDETERMINED means speed control isn't possible
 * SSS.DYN.9
 */
object AggregatedSignValue extends Enumeration {
  type State = Value
  val BLANK = Value(1)
  val UNDETERMINED = Value(2)
  val KM30 = Value(30)
  val KM40 = Value(40)
  val KM50 = Value(50)
  val KM60 = Value(60)
  val KM70 = Value(70)
  val KM80 = Value(80)
  val KM90 = Value(90)
  val KM100 = Value(100)
  val KM110 = Value(110)
  val KM120 = Value(120)
  val KM130 = Value(130)
}

/**
 * Message sent from the Aggregator component to the HHMKoppeling and DagrapportGenerator
 * component when the section Sign has changed
 */
case class SectionSignUpdateMessage(sectionSign: AggregatedSignValue.Value, techPardonTime_mSecs: Long, time: Date) {
  var dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")

  override def toString() = String.format(
    "sectionSign:[%s], techPardonTime:[%s], time:[%s]", sectionSign.toString, techPardonTime_mSecs.toString, dateFormat.format(time))
}

/**
 * Used with DisturbanceEvent to indicate which
 * component send the disturbance the disturbance is.
 */
object DisturbanceComponent extends Enumeration {
  type State = Value
  val RWSKOPPELING = Value(1)
  val DISTURBANCEMONITOR = Value(2)
  val HHM_SENDER = Value(3)
  val HHM_REPORT_SENDER = Value(4)
}
/**
 * Used with DisturbanceEvent to indicate how serious
 * the disturbance is.
 */
object DisturbanceSeverity extends Enumeration {
  type State = Value
  val LOW = Value(1)
  val HIGH = Value(2)
  val CRITICAL = Value(3)
}

/**
 * Used with DisturbanceEvent to indicate what state
 * the disturbance is.
 */
object DisturbanceState extends Enumeration {
  type State = Value
  val DETECTED = Value(0)
  val SOLVED = Value(1)
}

/**
 * Used with DisturbanceEvent to indicate what type
 * the disturbance is.
 */
object DisturbanceType extends Enumeration {
  type State = Value
  val KS_CONNECTION = Value(0)
  val STORAGE_SIZE = Value(1)
  val KS_IDENTIFICATION = Value(2)
  val HHM_CONNECTION = Value(3)
  val HHM_REPORT_CONNECTION = Value(4)

}

/**
 * This message is received by the DisturbanceMonitor actor
 * when a disturbance is detected or solved by another
 * component. Furthermore, it is used to sent a detected
 * disturbance to the DynamaxMessageSender actor (for notifying the HHM).
 * @param instanceId unique actor instance id.
 * @param disturbanceType the type of disturbance
 * @param component What component created the disturbance
 * @param message description of disturbance
 * @param severity The severity of the disturbance
 * @param time when the disturbance happend.
 * @param state indicate what state the disturbance is in.
 */
case class DisturbanceEvent(instanceId: Long,
                            disturbanceType: DisturbanceType.Value,
                            component: DisturbanceComponent.Value,
                            message: String,
                            severity: DisturbanceSeverity.Value,
                            time: Date,
                            state: DisturbanceState.Value) {

  override def toString() = String.format(
    "disturbanceType:[%s], component:[%s], message:[%s], severity:[%s], time:[%s], state:[%s]",
    disturbanceType.toString(),
    component.toString(),
    message,
    severity.toString(),
    time.toString(),
    state.toString())

  /**
   * create the solved event based on the current event
   */
  def createSolvedEvent(solvedTime: Date): DisturbanceEvent = {
    new DisturbanceEvent(
      instanceId = instanceId,
      disturbanceType = disturbanceType,
      component = component,
      message = message,
      severity = severity,
      time = solvedTime,
      state = DisturbanceState.SOLVED)

  }
}

/**
 * Message sent from Dynamax in XML fromat
 * Is created by  DynamaxMessageReceiver and send to TrafficControlerActor
 */
case class HHMSignUpdateMessage(
  sequenceNr: Long,
  sendTime: Date,
  repeat: Boolean,
  checksumMsg: String,
  checksumApp: String,
  checksumConfig: String,
  updateMsg: SectionSignUpdateMessage,
  disturbance: Option[DisturbanceEvent] = None)

