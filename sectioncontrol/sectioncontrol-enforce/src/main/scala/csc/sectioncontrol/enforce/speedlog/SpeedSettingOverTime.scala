/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.speedlog

import collection.immutable.SortedSet
//import csc.sectioncontrol.enforce.{ CaseFileVersionType, CaseFile, SpeedIndicatorStatus }
import csc.sectioncontrol.enforce.{ SpeedIndicatorStatus }
import csc.sectioncontrol.storage.ZkState
import annotation.tailrec

/**
 * Expose a view of the speed settings over time.
 * This is used to determine what the dynamic speed setting for a corridor was at any
 * particular time.
 *
 * The settings are sorted in reverse time order.
 *
 * @author Maarten Hazewinkel
 */
case class SpeedSettingOverTime private (corridorId: Int, settings: SortedSet[(Long, SpeedSetting)]) { //, fileFormat: CaseFile) {
  import SpeedSettingOverTime._

  /**
   * Returns the speed setting in effect at the requested time.
   */
  def apply(time: Long): SpeedSetting = {
    settings.find(_._1 <= time).map(_._2).getOrElse(indeterminateSetting)
  }
}

object SpeedSettingOverTime {
  val indeterminateSetting = SpeedSetting(Left(SpeedIndicatorStatus.Undetermined))

  private val StateStandby = "State-Standby"
  private val StateOk = "State-Ok"

  /*private[enforce] def apply(corridorId: Int, speedEvents: Seq[SpeedEvent], stateLog: Seq[ZkSystemState] = Seq(),
                             fileFormat: CaseFile = CaseFile(CaseFileVersionType.HHMVS14)): SpeedSettingOverTime = {
*/
  private[enforce] def apply(corridorId: Int, speedEvents: Seq[SpeedEvent], stateLog: Seq[ZkState] = Seq()): SpeedSettingOverTime = {
    val enforceStates = Seq(ZkState.enforceOn,
      ZkState.enforceDegraded,
      ZkState.enforceOff)

    val stateEvents = stateLog.map(_ match {
      case ZkState(state, time, _, _) if enforceStates.contains(state) ⇒
        SpeedEvent(time, -1, corridorId, Some(StateOk), None, None)
      case ZkState(_, time, _, _) ⇒
        SpeedEvent(time, -1, corridorId, Some(StateStandby), None, None)
    })

    val allEvents = (speedEvents ++ stateEvents).
      sortBy(se ⇒ (se.effectiveTimestamp, se.sequenceNumber))

    val initialSpeedEvent = SpeedEvent(0, -1, corridorId, Some(SpeedIndicatorStatus.Undetermined.toString), None, None)
    val filteredEvents = reduceByState(allEvents.toList, Nil, false, initialSpeedEvent)
    val timeSettingPairs = filteredEvents.map(event ⇒
      (event.effectiveTimestamp, SpeedSetting(Either.cond(event.noSpeedReason.isEmpty, event.speedSetting, SpeedIndicatorStatus.withName(event.noSpeedReason.get)))))

    //SpeedSettingOverTime(corridorId, SortedSet(timeSettingPairs: _*)(Ordering.by(-_._1)), fileFormat)
    SpeedSettingOverTime(corridorId, SortedSet(timeSettingPairs: _*)(Ordering.by(-_._1)))
  }

  @tailrec
  private def reduceByState(events: List[SpeedEvent],
                            filteredEvents: List[SpeedEvent],
                            isSuspendedState: Boolean,
                            lastSpeedEvent: SpeedEvent): Seq[SpeedEvent] = {
    events match {
      case Nil ⇒ filteredEvents.reverse.toSeq

      case (sb @ SpeedEvent(_, _, _, Some(StateStandby), _, _)) :: moreEvents ⇒
        reduceByState(moreEvents,
          sb.copy(noSpeedReason = Some(SpeedIndicatorStatus.StandBy.toString)) :: filteredEvents,
          true,
          lastSpeedEvent)

      case (ok @ SpeedEvent(_, _, _, Some(StateOk), _, _)) :: moreEvents ⇒
        reduceByState(moreEvents,
          lastSpeedEvent.copy(effectiveTimestamp = ok.effectiveTimestamp) :: filteredEvents,
          false,
          lastSpeedEvent)

      case (se @ SpeedEvent(_, _, _, _, _, _)) :: moreEvents ⇒
        if (isSuspendedState) {
          reduceByState(moreEvents, filteredEvents, isSuspendedState, se)
        } else {
          reduceByState(moreEvents, se :: filteredEvents, isSuspendedState, se)
        }
    }
  }
}

/**
 * An single speed setting
 */
case class SpeedSetting(setting: Either[SpeedIndicatorStatus.Value, Option[Int]])

