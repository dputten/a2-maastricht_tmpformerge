/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce

/**
 * Generic message that can be sent to several actors to trigger stored data cleanup.
 */
case class Cleanup(retentionDays: Int)

/*object CaseFileVersionType extends Enumeration {
  val TCVS33 /* old - IRS TC-VS versie 3.3 */ = Value
  val HHMVS14 /* new - IRS HHM-VS 1.4 */ = Value
  val HHMVS40 /* new - IRS HHM-VS 4.0 */ = Value
}*/

/**
 * Support a switch for file formats
 * @param version default is the old format
 */
//case class CaseFile(version: CaseFileVersionType.Value = CaseFileVersionType.HHMVS40)
