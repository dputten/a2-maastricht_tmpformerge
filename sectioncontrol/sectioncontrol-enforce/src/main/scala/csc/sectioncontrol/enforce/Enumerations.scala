/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce

/**
 * TODO 28032012 RR->MH: Scaladocs please
 * @author Maarten Hazewinkel
 */
object Enumerations {

  object RoadType extends Enumeration {
    val Highway = Value("1")
    val Provincial = Value("2")
    val OutsideTown = Value("3")
    val InsideTown = Value("5")
  }
}
