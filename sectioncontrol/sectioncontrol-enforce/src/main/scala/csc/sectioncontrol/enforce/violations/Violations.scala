/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.violations

import annotation.tailrec
import util.Random

object Violations {
  /**
   * Repeatedly attempt to execute some code with randomized delays between attempts.
   * Useful for code that depends on external factors like filesystems or networks which may exhibit
   * temporary conditions that prevent the executed code from completing successfully.
   */
  @tailrec
  def repeatAttemptWithDelays[T](maxAttempts: Int = 10, averageDelayMs: Int = 100)(code: ⇒ T): Either[Exception, T] = {
    val result: Either[Exception, Option[T]] = try {
      Right(Some(code))
    } catch {
      case e: Exception ⇒
        if (maxAttempts == 1) {
          Left(e)
        } else {
          Thread.sleep(Random.nextInt(averageDelayMs * 2))
          Right(None)
        }
    }
    if (result.isLeft) {
      Left(result.left.get)
    } else if (result.right.get.isDefined) {
      Right(result.right.get.get)
    } else {
      repeatAttemptWithDelays(maxAttempts = maxAttempts - 1, averageDelayMs = averageDelayMs)(code)
    }
  }

}
