/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.timewatch

import java.net.InetAddress
import java.util.{ Date, Calendar, TimeZone, GregorianCalendar }

import org.apache.commons.net.ntp.{ TimeInfo, NtpV3Packet, NTPUDPClient }

import akka.util.duration._
import akka.actor.{ Actor, ActorRef, ActorLogging, Cancellable }

import csc.curator.utils.Curator

import csc.sectioncontrol.enforce.SystemEventData
import csc.sectioncontrol.storage.Paths
import csc.sectioncontrol.messages.{ Interval, TimeChanges }
import csc.sectioncontrol.storagelayer.TimewatchService

/**
 * Actor that watches for irregularities in time and records suspend and resume events to
 * disqualify violations during those times.
 * It watches for both DST changes and for leap seconds.
 * DST changes are calculated locally using the timezone classes in the JVM.
 * Leap seconds cannot be detected locally. For this an NTP server is queried and the
 * appropriate flags in the response are checked.
 *
 * The actor sets up a schedule for itself to be called every minute. This because the NTP
 * documentation states that the leap second bit will be set before 23:59, but does not give
 * any more detailed guarantee.
 *
 * The next leap second is expected on June 30th 2012.
 *
 * @author Maarten Hazewinkel
 */
class WatchTime(val systemId: String,
                val ntpServer: String,
                val localTimeZone: String,
                val excludeLog: ActorRef,
                curator: Curator, pardonDuringDaylightSavingsSwitch: Boolean) extends Actor with ActorLogging {

  private var schedule: Option[Cancellable] = None
  private val ntpClient = new NTPUDPClient()
  private var knownChanges = TimeChanges(None, None)
  private val calUTC = new GregorianCalendar(TimeZone.getTimeZone("UTC"))
  private val calLocal = new GregorianCalendar(TimeZone.getTimeZone(localTimeZone))

  def receive = {
    case CheckForTimeChanges ⇒ {
      val now = System.currentTimeMillis()
      if (isLastDayOfMonth(now)) {
        // leap seconds can only be set on the last day of a month.
        // in practice only the last days of June and December have been used.
        checkNtpForLeapSecond(pardonDuringDaylightSavingsSwitch)
      }
      checkForDSTSwitch(now, pardonDuringDaylightSavingsSwitch)
    }
  }

  private[timewatch] def checkNtpForLeapSecond(send: Boolean = true) {
    getNtpTimeInfo.foreach { timeInfo ⇒
      val suspendInterval = timeInfo.getMessage.getLeapIndicator match {
        case NtpV3Packet.LI_LAST_MINUTE_HAS_59_SECONDS ⇒
          val dayEnd = endOfDay(timeInfo.getMessage.getTransmitTimeStamp.getTime, calUTC)
          Some(Interval(dayEnd - 1000, dayEnd))
        case NtpV3Packet.LI_LAST_MINUTE_HAS_61_SECONDS ⇒
          val dayEnd = endOfDay(timeInfo.getMessage.getTransmitTimeStamp.getTime, calUTC)
          Some(Interval(dayEnd, dayEnd + 1000))
        case _ ⇒
          None
      }
      if (suspendInterval.isDefined && knownChanges.leapSecond != suspendInterval) {
        if (send) {
          sendSuspension(suspendInterval.get, "Leap second")
        }

        knownChanges = knownChanges.copy(leapSecond = suspendInterval)
        TimewatchService.set(curator, systemId, knownChanges)
      }
    }
  }

  private[timewatch] def getNtpTimeInfo: Option[TimeInfo] = {
    ntpClient.open()
    try {
      ntpClient.setSoTimeout(10000)
      Some(ntpClient.getTime(InetAddress.getByName(ntpServer)))
    } catch {
      case e ⇒
        log.warning("Did not receive response from NTP server " + ntpServer)
        None
    } finally {
      ntpClient.close()
    }
  }

  private[timewatch] def checkForDSTSwitch(day: Long, send: Boolean = true) {
    val dayEnd = endOfDay(day, calLocal)
    val dayStart = startOfDay(day, calLocal)

    if (dayEnd - dayStart != 24.hours.toMillis) {
      val timeZone = calLocal.getTimeZone
      val dayStartsInDST = timeZone.inDaylightTime(new Date(dayStart))
      val changeTime = (dayStart to (dayEnd, 60 * 1000)).find(t ⇒ timeZone.inDaylightTime(new Date(t)) != dayStartsInDST)

      val suspendInterval = changeTime.map { changeTime ⇒
        if (dayStartsInDST) {
          Interval(changeTime - timeZone.getDSTSavings, changeTime + timeZone.getDSTSavings)
        } else {
          Interval(changeTime, changeTime)
        }
      }

      if (suspendInterval.isDefined && knownChanges.dstChange != suspendInterval) {
        if (send) {
          sendSuspension(suspendInterval.get, "DST change")
        }

        knownChanges = knownChanges.copy(dstChange = suspendInterval)
        TimewatchService.set(curator, systemId, knownChanges)
      }
    }
  }

  private[timewatch] def sendSuspension(suspendInterval: Interval, reason: String) {
    excludeLog ! TimeChangeEvent(suspendInterval.from, 1, true, reason)
    excludeLog ! TimeChangeEvent(suspendInterval.to, 2, false, reason)
  }

  private def endOfDay(time: Long, cal: Calendar): Long = {
    cal.setTimeInMillis(time)
    cal.set(Calendar.HOUR_OF_DAY, 23)
    cal.set(Calendar.MINUTE, 59)
    cal.set(Calendar.SECOND, 60)
    cal.set(Calendar.MILLISECOND, 0)
    cal.getTimeInMillis
  }

  private def startOfDay(time: Long, cal: Calendar): Long = {
    cal.setTimeInMillis(time)
    cal.set(Calendar.HOUR_OF_DAY, 0)
    cal.set(Calendar.MINUTE, 0)
    cal.set(Calendar.SECOND, 0)
    cal.set(Calendar.MILLISECOND, 0)
    cal.getTimeInMillis
  }

  private[timewatch] def isLastDayOfMonth(time: Long): Boolean = {
    calUTC.setTimeInMillis(time)
    val currentMonth = calUTC.get(Calendar.MONTH)
    calUTC.add(Calendar.DATE, 1)
    val tomorrowMonth = calUTC.get(Calendar.MONTH)
    currentMonth != tomorrowMonth
  }

  override def preStart() {
    super.preStart()
    knownChanges = TimewatchService.get(curator, systemId)
    schedule = Some(context.system.scheduler.schedule(1 second, 1 minute, self, CheckForTimeChanges))
  }

  override def postStop() {
    schedule.foreach(_.cancel())
    schedule = None
    super.postStop()
  }

}

object WatchTime {
  val componentId = "WatchTime"
}

case class TimeChangeEvent(effectiveTimestamp: Long,
                           sequenceNumber: Long,
                           suspend: Boolean,
                           reason: String) extends SystemEventData {
  def componentId = WatchTime.componentId
  def corridorId = None
  def isSuspendOrResumeEvent = true
  def suspensionReason = Some(reason)
}

/**
 * Trigger message for the WatchTime actor.
 */
case object CheckForTimeChanges
