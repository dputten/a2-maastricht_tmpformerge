/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.dynamax

import java.security.MessageDigest
import java.math.BigInteger
import xml.{ Elem, XML }
import java.util.Date
import csc.akkautils.DirectLogging
import java.text.SimpleDateFormat

/**
 * Implementation of the conversion from and to XML Message
 */
object SignUpdateMessageConverter extends DirectLogging {
  //Used format of time in the XML
  var dateFormat = new SimpleDateFormat("yyyy/MM/dd-HH:mm:ss")

  //XML version header
  val xmlversionHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"

  /**
   * Create a XML using update message and optional a disturbance.
   * @param updateMsg: The update message with aggregated Sign information
   * @param disturbance: Option when a disturbance is pressent include it into the XML
   * @param repeat: Indication if it is a repeat of information message
   * @param sequenceNr: The used sequence number
   * @param checksumApp : The checksum of the dynamax application
   * @param checksumConfig: The checksum of the dynamax configuration
   * @param sendTime: The sendTime of the XML message
   */
  def createXML(updateMsg: SectionSignUpdateMessage, disturbance: Option[DisturbanceEvent], repeat: Boolean, sequenceNr: Long, checksumApp: String, checksumConfig: String, sendTime: Date): String = {
    val checkSumString = createChecksum(updateMsg, disturbance, sequenceNr)
    val xml =
      <hhm xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="HHMDisturbanceMessage.xsd">
        <sequenceNr>{ sequenceNr }</sequenceNr>
        <sendtime>{ dateFormat.format(sendTime) }</sendtime>
        {
          if (disturbance.isDefined) {
            <disturbance id={ disturbance.get.instanceId.toString }>
              <severity>{ disturbance.get.severity }</severity>
              <type>{ disturbance.get.disturbanceType }</type>
              <component>{ disturbance.get.component }</component>
              <message>{ disturbance.get.message }</message>
              <datetime>{ dateFormat.format(disturbance.get.time) }</datetime>
              <state>{ disturbance.get.state }</state>
            </disturbance>;
          }
        }<signmessage type={ if (repeat) "repeat" else "info" }>
           <sign>{ updateMsg.sectionSign.toString }</sign>
           <datetime>{ dateFormat.format(updateMsg.time) }</datetime>
           <tech.pardon.time>{ updateMsg.techPardonTime_mSecs }</tech.pardon.time>
         </signmessage>
        <checksum>
          <message>{ checkSumString }</message>
          <application>{ checksumApp }</application>
          <configuration>{ checksumConfig }</configuration>
        </checksum>
      </hhm>;
    xmlversionHeader + xml.toString
  }

  /**
   * Create a HHMSignUpdateMessage from a XML and check the message checksum.
   * When the xsd is given it is also checked against the xsd
   * @param xml: The received XML
   * @param xsd: Optional the XSD path
   * @return  HHMSignUpdateMessage: The converted HHMSignUpdateMessage
   */
  def createHHMSignUpdateMessage(xml: String, xsd: Option[String] = None): HHMSignUpdateMessage = {
    val data = loadXML(xsd, xml)
    var disturbanceEvent: Option[DisturbanceEvent] = None
    val disturbanceEntry = (data \\ "disturbance")
    if (!disturbanceEntry.isEmpty) {
      val id = (disturbanceEntry \\ "@id").text.toLong
      val severity = (disturbanceEntry \\ "severity").text
      val disType = (disturbanceEntry \\ "type").text
      val component = (disturbanceEntry \\ "component").text
      val message = (disturbanceEntry \\ "message").text
      val time = (disturbanceEntry \\ "datetime").text
      val state = (disturbanceEntry \\ "state").text
      disturbanceEvent = Some(new DisturbanceEvent(id,
        DisturbanceType.withName(disType),
        DisturbanceComponent.withName(component),
        message,
        DisturbanceSeverity.withName(severity),
        dateFormat.parse(time),
        DisturbanceState.withName(state)))
    }
    val updateEntry = (data \\ "signmessage")
    val time = (updateEntry \\ "datetime").text
    val pardonTime_mSecs = (updateEntry \\ "tech.pardon.time").text.toLong
    val sign = (updateEntry \\ "sign").text

    var signValue = AggregatedSignValue.withName(sign)
    val update = new SectionSignUpdateMessage(signValue, pardonTime_mSecs, dateFormat.parse(time))
    val repeat = ((updateEntry \\ "@type").text == "repeat")

    val sequenceNr = (data \\ "sequenceNr").text.toLong
    val sentime = (data \\ "sendtime").text

    val checksumEntry = (data \\ "checksum")
    val checkSumMsg = (checksumEntry \\ "message").text
    val checkSumApp = (checksumEntry \\ "application").text
    val checkSumConfig = (checksumEntry \\ "configuration").text

    //test checksum message SSS.DYN.47
    val calculatedChecksum = createChecksum(update, disturbanceEvent, sequenceNr)
    if (calculatedChecksum != checkSumMsg) {
      //checksum isn't correct
      throw new ChecksumFailureException(checkSumMsg, calculatedChecksum)
    }
    new HHMSignUpdateMessage(sequenceNr, dateFormat.parse(sentime), repeat, checkSumMsg, checkSumApp, checkSumConfig, update, disturbanceEvent)
  }

  /**
   * Create a checksum from a updateMessage and a disturbance including a sequenceNr.
   * @param  updateMsg: the update message
   * @param  disturbance: optional the disturbance
   * @param sequenceNr: the sequence number
   */
  def createChecksum(updateMsg: SectionSignUpdateMessage, disturbance: Option[DisturbanceEvent], sequenceNr: Long): String = {
    val fields = updateMsg.toString + disturbance.getOrElse("").toString + sequenceNr
    val checksumAlgorithm = MessageDigest.getInstance("MD5")
    val checkSumBytes = checksumAlgorithm.digest(fields.getBytes)
    new BigInteger(1, checkSumBytes).toString(16)
  }

  /**
   * Load the XML and create a Elem.
   * When a xsd is given used the schema to validate the XML otherwise just load the XML
   * @param xsd: Optional the xsd path
   * @param xml: The received XML
   */
  def loadXML(xsd: Option[String], xml: String): Elem = {
    if (xsd.isDefined && xsd.get.size > 0) {
      val schemaLoc = getClass.getClassLoader.getResource(xsd.get)
      log.info("Schema location=%s".format(schemaLoc.getFile))
      SchemaAwareFactoryAdapter.loadXml(schemaLoc.openStream, xml)
    } else {
      log.info("Schema not used")
      XML.loadString(xml)
    }
  }
}