/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.violations

import java.text.SimpleDateFormat
import java.util.Date

/**
 * Contains 2 implicit conversions used within the package csc.sectioncontrol.enforce.violations.
 * The first conversion adds padLeft and padRight methods to String.
 * The second conversion adds an apply(Date) method to SimpleDateFormat which calls the format method after
 * setting the fixed timezone (from Constants).
 *
 * @author Maarten Hazewinkel
 */
object Implicits {

  /**
   * Helper class to implement padding operations on Strings.
   */
  class PaddingString(value: String) {
    /**
     * Pad the String on the left side with repeated paddingChars until the String is the required size.
     * Strings longer than the requested size are not modified.
     */
    def padLeft(requiredSize: Int, paddingChar: Char): String = {
      if (value.length < requiredSize) {
        padding(requiredSize - value.length, paddingChar) + value
      } else {
        value
      }
    }

    /**
     * Pad the String on the right side with repeated paddingChars until the String is the required size.
     * Strings longer than the requested size are not modified.
     */
    def padRight(requiredSize: Int, paddingChar: Char): String = {
      if (value.length < requiredSize) {
        value + padding(requiredSize - value.length, paddingChar)
      } else {
        value
      }
    }

    private def padding(size: Int, char: Char) = String.valueOf(Array.fill(size)(char))
  }

  /**
   * Implicit conversion to add padLeft and padRight methods to a String.
   */
  implicit def string2PaddingString(value: String) = new PaddingString(value)

  /**
   * Implicit conversion to add an apply(Date) method to a SimpleDateFormat.
   */
  implicit def simpleDateFormat2Function1(df: SimpleDateFormat): (Long) ⇒ String = {
    new ((Long) ⇒ String) {
      def apply(date: Long) = {
        df.setTimeZone(DayUtility.fileExportTimeZone)
        df.format(new Date(date))
      }
    }
  }
}
