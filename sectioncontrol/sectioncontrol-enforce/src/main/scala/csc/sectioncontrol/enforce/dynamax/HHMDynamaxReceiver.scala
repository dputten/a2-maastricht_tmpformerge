/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.dynamax

import akka.actor.{ ActorLogging, Actor, ActorRef }
import akka.camel.{ CamelMessage, Consumer }

/**
 * The Receiver of the Dynamax XML messages to the HHM.
 * The XML messages should be send using a MQ, This is set using the URI. It is possible to change this
 * protocol, because this implementation only require that a XML is send.
 */
class HHMDynamaxReceiver(uri: String, xsd: String, checksumApp: String, checksumConfig: String, timeoutmSec: Long, recipients: Set[ActorRef]) extends Actor with Consumer with ActorLogging {
  def endpointUri = uri //Example: "activemqserver:queue:Dynamax"
  //The processor keeping track of the state
  val processor = new DynamaxXMLProcessor(xsd, checksumApp, checksumConfig, timeoutmSec, recipients)

  /**
   * Receiving a XML message
   */
  override def postStop = {
    processor.stopTimer
  }

  def receive = {
    case msg: CamelMessage ⇒ {
      log.info("Received Message")
      val xml = msg.bodyAs[String]
      log.debug("XML received=" + xml)
      processor.processMessage(xml)
    }
  }
}