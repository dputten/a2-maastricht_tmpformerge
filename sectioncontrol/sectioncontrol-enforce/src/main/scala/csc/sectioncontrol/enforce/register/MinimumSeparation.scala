/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.register

/**
 * Split groups of violation to ensure a minimal time distance between multiple violations with
 * a single license.
 *
 * @author Maarten Hazewinkel
 */
object MinimumSeparation {
  /**
   * Supplies a function to split violations into 2 groups so that the first group contains only
   * violations such that per license no violation is separated by less that the minSep parameter
   * of milliseconds. All other violations wil be in the second (discarded) group.
   */
  def apply[T <: ViolationData](violations: Seq[T], minSep: Long, contextViolations: Seq[T] = Seq[T]()): SplitViolations[T] = {
    // Group violations per license
    violations.groupBy(_.separationLicense).map {
      case (license, vsByLicense) ⇒
        vsByLicense.
          // Within each license, sort violations by speed difference with the allowed speed, and in descending order.
          // For the same speed difference, we sort in ascending by time.
          sortBy(v ⇒ (v.enforcedSpeed - v.measuredSpeed, v.time)).
          // Split the violations by walking the sorted list. Violations that are not within the separation time
          // of already-accepted violations are added to the accepted list. Others are added to the discarded list.
          foldLeft(SplitViolations[T]())(enforceSeparation[T](minSep, contextViolations))
    }.
      // Consolidate (flatten) the per-license results into a single SplitViolations instance
      foldLeft(SplitViolations[T]())((vs1, vs2) ⇒ vs1.copy(ok = vs2.ok ::: vs1.ok,
        discarded = vs2.discarded ::: vs1.discarded))
  }

  private def enforceSeparation[T <: ViolationData](minSep: Long, contextViolations: Seq[T])(vs: SplitViolations[T], v: T): SplitViolations[T] = {
    if (vs.ok.find(vOk ⇒ (vOk.time - v.time).abs < minSep).isDefined
      || contextViolations.find(vContext ⇒ (vContext.time - v.time).abs < minSep && vContext.separationLicense == v.separationLicense).isDefined)
      vs.copy(discarded = v :: vs.discarded)
    else
      vs.copy(ok = v :: vs.ok)
  }

  trait ViolationData {
    def time: Long
    def license: String
    def measuredSpeed: Int
    def enforcedSpeed: Int
    def exportLicense: String
    def separationLicense: String
  }
}

case class SplitViolations[T <: MinimumSeparation.ViolationData](ok: List[T], discarded: List[T])

object SplitViolations {
  def apply[T <: MinimumSeparation.ViolationData](): SplitViolations[T] = SplitViolations[T](Nil, Nil)
}
