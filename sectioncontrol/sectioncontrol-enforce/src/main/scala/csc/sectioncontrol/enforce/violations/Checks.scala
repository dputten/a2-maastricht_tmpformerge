/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.violations

import java.util.{ GregorianCalendar, Calendar }

/**
 * Implements functions that are used to check specific conditions internally in the
 * csc.sectioncontrol.enforce.violations package.
 * Not available outside of the package.
 *
 * @author Maarten Hazewinkel
 */
object Checks {
  val nullValuesErrorMessage = "null values are not allowed"

  private val MarkerJFIF1: Array[Byte] = Array(0xFF.toByte, 0xD8.toByte, 0xFF.toByte, 0xE0.toByte)
  private val MarkerJFIF2: Array[Byte] = Array(0x4A.toByte, 0x46.toByte, 0x49.toByte, 0x46.toByte, 0x00.toByte)

  def allNotNull(values: AnyRef*): Boolean = !values.toSeq.exists(_ == null)

  def isJFIF(image: Array[Byte]) = image.slice(0, 4).sameElements(MarkerJFIF1) && image.slice(6, 11).sameElements(MarkerJFIF2)

  def isSameDate(date1: Long, date2: Long) = {
    def dateParts(date: Long) = {
      val cal = new GregorianCalendar(DayUtility.fileExportTimeZone)
      cal.setTimeInMillis(date)
      (cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH))
    }
    dateParts(date1) == dateParts(date2)
  }
}
