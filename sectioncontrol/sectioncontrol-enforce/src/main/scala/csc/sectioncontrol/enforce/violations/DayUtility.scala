/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.violations

import java.util.{ Calendar, GregorianCalendar }

/**
 * TODO: Should be removed and use csc.sectioncontrol.common
 * Couldn't remove this because this will effect writer sub-project causing the
 * checksum to change
 * @author Maarten Hazewinkel
 */
object DayUtility {
  /**
   * @deprecated use csc.sectioncontrol.common.DayUtility.fileExportTimeZone
   */
  val fileExportTimeZone = java.util.TimeZone.getTimeZone("Europe/Amsterdam")

  /**
   * Calculate the start- and end-points for the provided day. Considers DST issues by using the TimeZone
   * constant from the violations.Constants object.
   *
   * @param date A timestamp within the day of interest
   * @return A pair of longs indicating the start and end times of the day in unix epoch milliseconds
   * @deprecated use csc.sectioncontrol.common.DayUtility.calculateDayPeriod
   */
  def calculateDayPeriod(date: Long): (Long, Long) = {
    val cal = new GregorianCalendar(fileExportTimeZone)
    cal.setTimeInMillis(date)
    cal.set(Calendar.HOUR_OF_DAY, 0)
    cal.set(Calendar.MINUTE, 0)
    cal.set(Calendar.SECOND, 0)
    cal.set(Calendar.MILLISECOND, 0)
    val start = cal.getTimeInMillis
    cal.set(Calendar.HOUR_OF_DAY, 24)
    (start, cal.getTimeInMillis)
  }

  /**
   * Add one day. And take into account the saving daylights jumps
   * @param date the start date
   * @return added one day.
   * @deprecated use csc.sectioncontrol.common.DayUtility.addOneDay
   */
  def addOneDay(date: Long): Long = {
    val cal = new GregorianCalendar(fileExportTimeZone)
    cal.setTimeInMillis(date)
    cal.add(Calendar.DAY_OF_YEAR, 1)
    cal.getTimeInMillis
  }

}
