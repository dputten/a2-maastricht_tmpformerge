/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.violations

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import org.apache.commons.io.IOUtils
import csc.sectioncontrol.enforce.violations.Checks._
import java.util.{ TimeZone, GregorianCalendar }

/**
 * @author Maarten Hazewinkel
 */
class ChecksTest extends WordSpec with MustMatchers {
  "function allNotNull" must {
    "return false if any argument is null" in {
      allNotNull(null) must be(false)
      allNotNull("", "", null, "") must be(false)
    }
    "return true if no arguments are given" in {
      allNotNull() must be(true)
    }
    "return true if all no argument is null" in {
      allNotNull("") must be(true)
      allNotNull("", "", "") must be(true)
    }
  }

  "function isJFIF" must {
    "return true for a jpeg image" in {
      isJFIF(IOUtils.toByteArray(getClass.getResourceAsStream("testimage.jpg"))) must be(true)
      isJFIF(IOUtils.toByteArray(getClass.getResourceAsStream("testphoto.jpg"))) must be(true)
    }
    "return false for anything else" in {
      isJFIF(IOUtils.toByteArray(getClass.getResourceAsStream("testimage.png"))) must be(false)
      isJFIF(Array[Byte]()) must be(false)
    }
  }

  "function isSameDate" must {
    "return true for same dates" in {
      isSameDate(new GregorianCalendar(2012, 2, 24, 0, 0).getTimeInMillis,
        new GregorianCalendar(2012, 2, 24, 23, 59).getTimeInMillis) must be(true)
    }
    "return false for different dates" in {
      isSameDate(new GregorianCalendar(2012, 2, 24, 0, 0).getTimeInMillis,
        new GregorianCalendar(2012, 2, 24, 23, 61).getTimeInMillis) must be(false)
    }
    "adjust for different time zones" in {
      val cal1 = new GregorianCalendar(2012, 2, 24, 0, 0)
      cal1.setTimeZone(TimeZone.getTimeZone("Europe/Amsterdam"))
      val cal2 = new GregorianCalendar(2012, 2, 24, 23, 59)
      cal2.setTimeZone(TimeZone.getTimeZone("Zulu"))
      isSameDate(cal1.getTimeInMillis,
        cal2.getTimeInMillis) must be(false)

      val cal3 = new GregorianCalendar(2012, 2, 25, 0, 0)
      cal1.setTimeZone(TimeZone.getTimeZone("Europe/Amsterdam"))
      isSameDate(cal2.getTimeInMillis,
        cal3.getTimeInMillis) must be(true)
    }
  }
}
