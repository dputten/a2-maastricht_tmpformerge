/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.violations

import csc.sectioncontrol.enforce.violations.Implicits._
import org.scalatest.matchers.MustMatchers
import org.scalatest.WordSpec
import java.text.SimpleDateFormat

/**
 *       //TODO 28032012 RR->MH: please remove
 * @author Maarten Hazewinkel
 */
class ImplicitsTest extends WordSpec with MustMatchers {
  "String extension method padLeft" must {
    "do correct padding" in {
      "yy".padLeft(10, 'x') must be("xxxxxxxxyy")
      "1".padLeft(2, '0') must be("01")
    }
    "not truncate too-long values" in {
      "123456".padLeft(4, ' ') must be("123456")
    }
  }

  "String extension method padRight" must {
    "do correct padding" in {
      "yy".padRight(10, 'x') must be("yyxxxxxxxx")
      "1".padRight(2, '0') must be("10")
    }
    "not truncate too-long values" in {
      "123456".padRight(4, ' ') must be("123456")
    }
  }

  "implicit conversion for SimpleDateFormat" must {
    "work and apply correct Timezone" in {
      val format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z")
      format(960148740000L) must be("2000-06-04 21:59:00 CEST")
      format(1329820825549L) must be("2012-02-21 11:40:25 CET")
    }
  }
}
