/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.excludelog

import org.scalatest.matchers.MustMatchers
import java.util.{ Calendar, GregorianCalendar }
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import akka.util.duration._
import akka.testkit.{ TestProbe, TestActorRef }
import akka.dispatch.Await
import akka.actor.ActorSystem
import akka.util.{ Duration, Timeout }
import akka.pattern.ask
import net.liftweb.json.DefaultFormats
import csc.sectioncontrol.storage._
import csc.config.Path
import csc.sectioncontrol.enforce.{ CuratorHelper, SystemEventData, Cleanup }
import csc.sectioncontrol.storagelayer.excludelog.{ Exclusions, Exclusion }
import csc.curator.CuratorTestServer
import org.apache.curator.retry.RetryUntilElapsed
import csc.curator.utils.Curator
import csc.sectioncontrol.storagelayer.state.StateService
import csc.sectioncontrol.storage.ZkCorridorInfo
import csc.sectioncontrol.storage.ZkUser
import csc.sectioncontrol.storage.ZkCorridor
import csc.sectioncontrol.storagelayer.RedLightConfig
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.messages.{ VehicleImageType, EsaProviderType, VehicleCode }

/**
 *
 * @author Maarten Hazewinkel
 */
class ExcludeLogTest extends WordSpec with MustMatchers with BeforeAndAfterAll with CuratorTestServer {
  val jan_25_2002_1000 = new GregorianCalendar(2002, Calendar.JANUARY, 25, 10, 0).getTimeInMillis
  val jan_25_2002_1100 = new GregorianCalendar(2002, Calendar.JANUARY, 25, 11, 0).getTimeInMillis
  val jan_25_2002_1200 = new GregorianCalendar(2002, Calendar.JANUARY, 25, 12, 0).getTimeInMillis
  val jan_25_2002_1300 = new GregorianCalendar(2002, Calendar.JANUARY, 25, 13, 0).getTimeInMillis
  val jan_25_2002_1500 = new GregorianCalendar(2002, Calendar.JANUARY, 25, 15, 0).getTimeInMillis
  val endOfTime = Long.MaxValue - 1

  implicit val testSystem = ActorSystem("ExcludeLogTest")
  val testDuration: Duration = 2.seconds
  implicit val testTimeout = Timeout(testDuration)

  var testExcludeLog: TestActorRef[ExcludeLog] = _

  val defaultExclusion = Exclusion("SystemState", 0, endOfTime, "All/SystemState:Not started")

  var storage: Curator = _

  private def current = { "current" }
  private def failures = { "failures" }
  private def alerts = { "alerts" }
  private def errors = { "errors" }
  private def control = { "control" }

  override def beforeEach() {
    super.beforeEach()
    val formats = DefaultFormats + new EnumerationSerializer(
      ZkWheelbaseType,
      VehicleCode,
      ZkIndicationType,
      ServiceType,
      ZkCaseFileType,
      EsaProviderType,
      VehicleImageType,
      ConfigType)
    storage = CuratorHelper.create(this, clientScope, new RetryUntilElapsed(5000, 100), formats = formats)
    testExcludeLog = TestActorRef(new ExcludeLog("eg33", storage))

    storage = CuratorHelper.create(this, clientScope, formats = formats)
    storage.createEmptyPath(Path("/destroy"))
    storage.createEmptyPath(Path("/ctes") / "systems" / "a2")
    storage.createEmptyPath(Path("/ctes") / "systems" / "a2" / "corridors" / "corridor1" / control / errors / failures / current)
    storage.createEmptyPath(Path("/ctes") / "systems" / "a2" / "corridors" / "corridor2" / control / errors / failures / current)
    storage.createEmptyPath(Path("/ctes") / "systems" / "a2" / "corridors" / "corridor1" / control / errors / alerts / current)
    storage.createEmptyPath(Path("/ctes") / "systems" / "a2" / "corridors" / "corridor2" / control / errors / alerts / current)
    storage.createEmptyPath(Path("/ctes") / "users")
    storage.createEmptyPath(Path("/ctes") / "configuration")
    storage.put(Path("/ctes") / "users" / "testUser", new ZkUser("testUser", "testReportingOfficer", "", "", "", "", "", "", "", List()))
    storage.put(Path("/ctes") / "users" / "testUser1", new ZkUser("testUser1", "testReportingOfficer", "", "", "", "", "", "", "", List()))

    val config1Path = Path(Paths.Corridors.getConfigPath("eg33", "1"))

    storage.createEmptyPath(config1Path)
    storage.set(config1Path, new ZkCorridor(id = "1",
      name = "first corridor",
      roadType = 0,
      serviceType = ServiceType.RedLight,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = true,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = null,
      info = new ZkCorridorInfo(corridorId = 1,
        locationCode = "1",
        reportId = "TCS A2 Links  39.7 - 54.9",
        reportHtId = "A020011",
        reportPerformance = true,
        locationLine1 = "loc 1"),
      startSectionId = "1",
      endSectionId = "1",
      allSectionIds = Seq("1"),
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 402,
      roadCode = 2,
      dutyType = "KA2",
      deploymentCode = "03",
      codeText = ""), 0)
    val rootPathToServices = Path(Paths.Corridors.getServicesPath("eg33", "1"))
    val pathToService = rootPathToServices / ServiceType.SectionControl.toString
    storage.createEmptyPath(pathToService)
    storage.set(pathToService, new RedLightConfig(factNumber = "1234", pardonRedTime = 1000, minimumYellowTime = 1000), 0)

  }

  override def afterEach() {
    testExcludeLog.stop()
    super.afterEach()
  }

  override def afterAll() {
    testSystem.shutdown()
    super.afterAll()
  }

  "ExcludeLog exclusions" must {
    "be empty when no events are supplied" in {
      //old version failed sometimes
      val probe = TestProbe()
      probe.send(testExcludeLog, GetExclusions(1))
      val excl = probe.expectMsgType[Exclusions](1.second)
      excl.exclusions.toSet must be(Set(defaultExclusion))
    }

    "have a single exclusion period from multiple events" in {
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1200, 1, true, Some("test2"))
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1300, 1, true, Some("test2"))
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1500, 1, false, Some("test2"))

      val result = testExcludeLog ? GetExclusions(1)
      Await.result(result, testDuration).asInstanceOf[Exclusions].exclusions.toSet must be(
        Set(Exclusion("Source1", jan_25_2002_1200, jan_25_2002_1500, "All/Source1:test2"),
          defaultExclusion))
    }

    "combine corridor-specific and general events" in {
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1200, 1, true, Some("test3"))
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1300, 1, true, Some("test3"))
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1500, 1, false, Some("test3"))
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1000, 1, true, Some("test3"), corridorId = Some(1))
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1100, 1, false, Some("test3"), corridorId = Some(1))

      val result = testExcludeLog ? GetExclusions(1)
      Await.result(result, testDuration).asInstanceOf[Exclusions].exclusions.toSet must be(
        Set(Exclusion("Source1", jan_25_2002_1200, jan_25_2002_1500, "All/Source1:test3"),
          Exclusion("Source1", jan_25_2002_1000, jan_25_2002_1100, "1/Source1:test3"),
          defaultExclusion))
    }

    "include data from system state log" in {
      val systemId = "eg33"
      val corridorId = "1"

      StateService.create(systemId, corridorId, ZkState(ZkState.enforceOn, jan_25_2002_1000, "test", "test3st"), storage)

      var versioned = StateService.getVersioned(systemId, corridorId, storage)
      StateService.update(systemId, corridorId, ZkState("EnforceOff", jan_25_2002_1200, "test", "test3st"), versioned.get.version, storage)

      versioned = StateService.getVersioned(systemId, corridorId, storage)
      StateService.update(systemId, corridorId, ZkState("OtherState", jan_25_2002_1300, "test", "test3st"), versioned.get.version, storage)

      versioned = StateService.getVersioned(systemId, corridorId, storage)
      StateService.update(systemId, corridorId, ZkState("EnforceOn", jan_25_2002_1500, "test", "test3st"), versioned.get.version, storage)

      testExcludeLog ! MockEvent("Source1", jan_25_2002_1000, 1, true, Some("test3st"), corridorId = Some(1))
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1100, 1, false, Some("test3st"), corridorId = Some(1))

      val result = testExcludeLog ? GetExclusions(1)
      Await.result(result, testDuration).asInstanceOf[Exclusions].exclusions.toSet must be(
        Set(Exclusion("SystemState", jan_25_2002_1200, jan_25_2002_1500, "All/SystemState:test3st"),
          Exclusion("Source1", jan_25_2002_1000, jan_25_2002_1100, "1/Source1:test3st"),
          Exclusion("SystemState", 0, jan_25_2002_1000, "All/SystemState:Not started")))
    }

    "ignore events from other corridors than requested" in {
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1200, 1, true, Some("test4"), corridorId = Some(2))
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1300, 1, true, Some("test4"), corridorId = Some(2))
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1500, 1, false, Some("test4"), corridorId = Some(2))
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1000, 1, true, Some("test4"), corridorId = Some(1))
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1100, 1, false, Some("test4"), corridorId = Some(1))

      val result = testExcludeLog ? GetExclusions(1)
      Await.result(result, testDuration).asInstanceOf[Exclusions].exclusions.toSet must be(
        Set(Exclusion("Source1", jan_25_2002_1000, jan_25_2002_1100, "1/Source1:test4"),
          defaultExclusion))
    }

    "clean up old events" in {
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1200, 1, true, Some("test5"), corridorId = Some(1))
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1300, 1, true, Some("test5"), corridorId = Some(1))
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1500, 1, false, Some("test5"), corridorId = Some(1))
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1000, 1, true, Some("test5"), corridorId = Some(1))
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1100, 1, false, Some("test5"), corridorId = Some(1))

      testExcludeLog ! Cleanup(10)

      val result = testExcludeLog ? GetExclusions(1)
      Await.result(result, testDuration).asInstanceOf[Exclusions].exclusions.toSet must be(Set(defaultExclusion))
    }

    "retain last suspend during cleanup of old events" in {
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1200, 1, true, Some("test6"), corridorId = Some(1))
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1300, 1, true, Some("test6"), corridorId = Some(1))
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1500, 1, true, Some("test6"), corridorId = Some(1))
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1000, 1, true, Some("test6"), corridorId = Some(1))
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1100, 1, false, Some("test6"), corridorId = Some(1))

      testExcludeLog ! Cleanup(10)

      val result = testExcludeLog ? GetExclusions(1)
      Await.result(result, testDuration).asInstanceOf[Exclusions].exclusions.toSet must be(
        Set(Exclusion("Source1", jan_25_2002_1500, endOfTime, "1/Source1:test6"),
          defaultExclusion))
    }

    "retain recent events during cleanup" in {
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1200, 1, true, Some("test7"), corridorId = Some(1))
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1300, 1, true, Some("test7"), corridorId = Some(1))
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1500, 1, false, Some("test7"), corridorId = Some(1))
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1000, 1, true, Some("test7"), corridorId = Some(1))
      testExcludeLog ! MockEvent("Source1", jan_25_2002_1100, 1, false, Some("test7"), corridorId = Some(1))

      val recentFrom = System.currentTimeMillis() - 60000
      val recentUntil = System.currentTimeMillis() - 1000
      testExcludeLog ! MockEvent("Source1", recentFrom, 1, true, Some("test7"), corridorId = Some(1))
      testExcludeLog ! MockEvent("Source1", recentUntil, 1, false, Some("test7"), corridorId = Some(1))

      testExcludeLog ! Cleanup(10)

      val result = testExcludeLog ? GetExclusions(1)
      val expected = Set(Exclusion("Source1", recentFrom, recentUntil, "1/Source1:test7"),
        defaultExclusion)
      Await.result(result, testDuration).asInstanceOf[Exclusions].exclusions.toSet must be(
        expected)
    }
  }

  private case class MockEvent(componentId: String,
                               effectiveTimestamp: Long,
                               sequenceNumber: Long,
                               suspend: Boolean,
                               suspensionReason: Option[String],
                               corridorId: Option[Int] = None,
                               isSuspendOrResumeEvent: Boolean = true) extends SystemEventData
}
