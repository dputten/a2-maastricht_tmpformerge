/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.speedlog

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import java.util.{ Calendar, GregorianCalendar }
import csc.sectioncontrol.storage.ZkState
import csc.sectioncontrol.enforce.SpeedIndicatorStatus

/**
 *
 * @author Maarten Hazewinkel
 */
class SpeedSettingOverTimeTest extends WordSpec with MustMatchers {
  def jan25HourMillis(hour: Int): Long = new GregorianCalendar(2002, Calendar.JANUARY, 25, hour, 0).getTimeInMillis
  val jan_25_2002_0900 = jan25HourMillis(9)
  val jan_25_2002_1000 = jan25HourMillis(10)
  val jan_25_2002_1100 = jan25HourMillis(11)
  val jan_25_2002_1200 = jan25HourMillis(12)
  val jan_25_2002_1300 = jan25HourMillis(13)
  val jan_25_2002_1400 = jan25HourMillis(14)
  val jan_25_2002_1500 = jan25HourMillis(15)
  val jan_25_2002_2000 = jan25HourMillis(20)

  val speed120 = Right[SpeedIndicatorStatus.Value, Option[Int]](Some(120))
  val speed100 = Right[SpeedIndicatorStatus.Value, Option[Int]](Some(100))
  val speed90 = Right[SpeedIndicatorStatus.Value, Option[Int]](Some(90))
  val speed70 = Right[SpeedIndicatorStatus.Value, Option[Int]](Some(70))
  val speed50 = Right[SpeedIndicatorStatus.Value, Option[Int]](Some(50))
  val speedInd = Left[SpeedIndicatorStatus.Value, Option[Int]](SpeedIndicatorStatus.Undetermined)
  val speedSb = Left[SpeedIndicatorStatus.Value, Option[Int]](SpeedIndicatorStatus.StandBy)

  "SpeedSettingOverTime" must {
    "be empty when no events are supplied" in {
      SpeedSettingOverTime(1, List[SpeedEvent]()).settings must be('empty)
    }

    "have a single record from 1 event" in {
      SpeedSettingOverTime(1, List(MockEvent(jan_25_2002_0900, 1, speed90))).settings.toList must be(List(jan_25_2002_0900 -> SpeedSetting(speed90)))
    }

    "have multiple records sorted in inverse-chronological order" in {
      SpeedSettingOverTime(1, List(MockEvent(jan_25_2002_1200, 1, speed90),
        MockEvent(jan_25_2002_1300, 1, speed70),
        MockEvent(jan_25_2002_1500, 1, speed120))).settings.toList must be(
        List(jan_25_2002_1500 -> SpeedSetting(speed120),
          jan_25_2002_1300 -> SpeedSetting(speed70),
          jan_25_2002_1200 -> SpeedSetting(speed90)))
    }

    "have multiple records sorted in inverse-chronological order from multiple out-of-order events" in {
      SpeedSettingOverTime(1, List(MockEvent(jan_25_2002_1200, 1, speedInd),
        MockEvent(jan_25_2002_1500, 1, speed90),
        MockEvent(jan_25_2002_1300, 1, speed100),
        MockEvent(jan_25_2002_1000, 1, speed50))).settings.toList must be(
        List(jan_25_2002_1500 -> SpeedSetting(speed90),
          jan_25_2002_1300 -> SpeedSetting(speed100),
          jan_25_2002_1200 -> SpeedSetting(speedInd),
          jan_25_2002_1000 -> SpeedSetting(speed50)))
    }

    "deliver most recent speed setting for a time" in {
      val speedSetting = SpeedSettingOverTime(1, List(MockEvent(jan_25_2002_1200, 1, speedInd),
        MockEvent(jan_25_2002_1500, 1, speed90),
        MockEvent(jan_25_2002_1300, 1, speed100),
        MockEvent(jan_25_2002_1000, 1, speed50)))
      speedSetting(jan_25_2002_2000) must be(SpeedSetting(speed90))
      speedSetting(jan_25_2002_1500) must be(SpeedSetting(speed90))
      speedSetting(jan_25_2002_1400) must be(SpeedSetting(speed100))
      speedSetting(jan_25_2002_1300 - 1) must be(SpeedSetting(speedInd))
      speedSetting(jan_25_2002_1100) must be(SpeedSetting(speed50))
    }

    "deliver indeterminate speed for a time before any records" in {
      val speedSetting = SpeedSettingOverTime(1, List(MockEvent(jan_25_2002_1200, 1, speedInd),
        MockEvent(jan_25_2002_1500, 1, speed90),
        MockEvent(jan_25_2002_1300, 1, speed100),
        MockEvent(jan_25_2002_1000, 1, speed50)))
      speedSetting(jan_25_2002_0900) must be(SpeedSetting(speedInd))
    }

    "integrate system state" in {
      SpeedSettingOverTime(1, List(MockEvent(jan_25_2002_1200, 1, speedInd),
        MockEvent(jan_25_2002_2000, 1, speed90),
        MockEvent(jan_25_2002_1300, 1, speed100),
        MockEvent(jan_25_2002_1000, 1, speed50)),
        List(ZkState(ZkState.standBy, jan_25_2002_1400, "test", "test"),
          ZkState(ZkState.enforceOn, jan_25_2002_1500, "test", "test"))).settings.toList must be(
          List(jan_25_2002_2000 -> SpeedSetting(speed90),
            jan_25_2002_1500 -> SpeedSetting(speed100),
            jan_25_2002_1400 -> SpeedSetting(speedSb),
            jan_25_2002_1300 -> SpeedSetting(speed100),
            jan_25_2002_1200 -> SpeedSetting(speedInd),
            jan_25_2002_1000 -> SpeedSetting(speed50)))
    }

    "interpret system states other than EnforceOn and EnforceDegraded and EnforceOff as stand-by (TC-348)" in {
      SpeedSettingOverTime(1, List(MockEvent(jan_25_2002_2000, 1, speed90),
        MockEvent(jan_25_2002_0900, 1, speed90)),
        List(ZkState(ZkState.enforceOn, jan_25_2002_0900, "test", "test"),
          ZkState(ZkState.off, jan_25_2002_1000, "test", "test"),
          ZkState(ZkState.enforceDegraded, jan_25_2002_1100, "test", "test"),
          ZkState(ZkState.failure, jan_25_2002_1200, "test", "test"),
          ZkState(ZkState.enforceOn, jan_25_2002_1300, "test", "test"),
          ZkState(ZkState.enforceOff, jan_25_2002_1400, "test", "test"),
          ZkState(ZkState.standBy, jan_25_2002_1500, "test", "test"),
          ZkState(ZkState.enforceOn, jan_25_2002_2000, "test", "test"))).settings.toList must be(
          List(jan_25_2002_2000 -> SpeedSetting(speed90),
            jan_25_2002_1500 -> SpeedSetting(speedSb),
            jan_25_2002_1400 -> SpeedSetting(speed90),
            jan_25_2002_1300 -> SpeedSetting(speed90),
            jan_25_2002_1200 -> SpeedSetting(speedSb),
            jan_25_2002_1100 -> SpeedSetting(speed90),
            jan_25_2002_1000 -> SpeedSetting(speedSb),
            jan_25_2002_0900 -> SpeedSetting(speed90)))
    }
  }

  private object MockEvent {
    def apply(effectiveTimestamp: Long,
              sequenceNumber: Long,
              speedSetting: Either[SpeedIndicatorStatus.Value, Option[Int]],
              corridorId: Int = 1): SpeedEvent =
      SpeedEvent(effectiveTimestamp, sequenceNumber, corridorId, speedSetting.left.toOption.map(_.toString),
        speedSetting.right.toOption.flatMap(identity(_)), None)
  }

}
