/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.register

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.akkautils.DirectLogging
import csc.sectioncontrol.storage.Paths
import csc.curator.utils.{ CuratorToolsImpl, Curator }
import csc.curator.CuratorTestServer

class CountryCodesTest extends WordSpec with MustMatchers with CuratorTestServer with DirectLogging {

  var curatorTool: Curator = _
  override def beforeEach() {
    super.beforeEach()
    curatorTool = new CuratorToolsImpl(clientScope, log)
  }

  "CountryCodes" must {
    "create list without zookeeper node" in {
      val codes = CountryCodes(curatorTool)
      codes.translateIsoToUN("NL") must be("NL")
      codes.translateIsoToUN("DE") must be("D")
      codes.translateIsoToUN("BE") must be("B")
      codes.translateIsoToUN("BA") must be("BIH")
      codes.translateIsoToUN("FR") must be("F")
    }
    "create list overriding existing records" in {
      var de = CountryCodes.countries.find(_.UN_code == "D").get
      de = de.copy(UN_code = "DDD")
      val path = Paths.Configuration.getCountryCodes
      curatorTool.put(path, List(de))
      val codes = CountryCodes(curatorTool)
      codes.translateIsoToUN("NL") must be("NL")
      codes.translateIsoToUN("DE") must be("DDD")
      codes.translateIsoToUN("BE") must be("B")
      codes.translateIsoToUN("BA") must be("BIH")
      codes.translateIsoToUN("FR") must be("F")
    }
    "create list adding records" in {
      val noneExist = Country(UN_code = "DDD", iso3166_code = "XX", name = "TESTCODE")
      val path = Paths.Configuration.getCountryCodes
      curatorTool.put(path, List(noneExist))
      val codes = CountryCodes(curatorTool)
      codes.translateIsoToUN("NL") must be("NL")
      codes.translateIsoToUN("DE") must be("D")
      codes.translateIsoToUN("XX") must be("DDD")
      codes.translateIsoToUN("BA") must be("BIH")
    }
    "create list mixed records" in {
      var de = CountryCodes.countries.find(_.UN_code == "D").get
      de = de.copy(UN_code = "DDD")
      val noneExist = Country(UN_code = "DDD", iso3166_code = "XX", name = "TESTCODE")
      val path = Paths.Configuration.getCountryCodes
      curatorTool.put(path, List(de, noneExist))
      val codes = CountryCodes(curatorTool)
      codes.translateIsoToUN("NL") must be("NL")
      codes.translateIsoToUN("DE") must be("DDD")
      codes.translateIsoToUN("XX") must be("DDD")
      codes.translateIsoToUN("BA") must be("BIH")
    }
  }

}