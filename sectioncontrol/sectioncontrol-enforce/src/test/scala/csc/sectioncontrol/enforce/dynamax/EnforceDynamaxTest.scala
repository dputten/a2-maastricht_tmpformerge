/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.dynamax

import org.scalatest.matchers.MustMatchers
import csc.akkautils.DirectLogging
import org.apache.curator.retry.RetryUntilElapsed
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import akka.actor.{ Props, ActorRef, ActorSystem }
import akka.testkit.{ TestProbe, TestKit }
import java.util.Date
import net.liftweb.json.DefaultFormats
import csc.sectioncontrol.messages.{ EsaProviderType, SystemEvent }
import akka.util.duration._
import csc.sectioncontrol.storage._
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.enforce.{ CuratorHelper, SpeedIndicatorStatus }
import csc.curator.CuratorTestServer
import csc.curator.utils.Curator

class EnforceDynamaxTest extends TestKit(ActorSystem("EnforceDynamaxTest")) with WordSpec with MustMatchers
  with BeforeAndAfterAll with CuratorTestServer with DirectLogging {

  implicit val formats = DefaultFormats + new EnumerationSerializer(EsaProviderType, ServiceType,
    ConfigType)
  val zkPath = "/ctes/systems/"
  val servers = "localhost:2181"

  val retryPolicy = new RetryUntilElapsed(5000, 100)
  var storage: Curator = _

  override def beforeEach() {
    super.beforeEach()
    storage = CuratorHelper.create(this, clientScope, retryPolicy, formats)
    createCorridors()
  }

  override def afterAll() {
    system.shutdown()
    super.afterAll()
  }

  def createCorridors() {
    val sys = new ZkSystem(id = "A2",
      name = "A2",
      title = "A2",
      mtmRouteId = "a200l",
      violationPrefixId = "",
      location = ZkSystemLocation(
        description = "",
        viewingDirection = Some(""),
        roadNumber = "",
        roadPart = "",
        region = "",
        systemLocation = ""),
      serialNumber = SerialNumber(""),
      orderNumber = 0,
      maxSpeed = 0,
      compressionFactor = 0,
      retentionTimes = ZkSystemRetentionTimes(
        nrDaysKeepTrafficData = 0,
        nrDaysKeepViolations = 0,
        nrDaysRemindCaseFiles = 0),
      pardonTimes = ZkSystemPardonTimes(pardonTimeEsa_secs = 5000, pardonTimeTechnical_secs = 5000),
      esaProviderType = EsaProviderType.Dynamax)
    storage.put(zkPath + "A2/config", sys)

    val sys2 = new ZkSystem(id = "A12",
      name = "A12",
      title = "A12",
      mtmRouteId = "a200l",
      violationPrefixId = "",
      location = ZkSystemLocation(
        description = "",
        region = "",
        viewingDirection = Some(""),
        roadNumber = "",
        roadPart = "",
        systemLocation = ""),
      serialNumber = SerialNumber(""),
      orderNumber = 0,
      maxSpeed = 0,
      compressionFactor = 0,
      retentionTimes = ZkSystemRetentionTimes(
        nrDaysKeepTrafficData = 0,
        nrDaysKeepViolations = 0,
        nrDaysRemindCaseFiles = 0),
      pardonTimes = ZkSystemPardonTimes(pardonTimeEsa_secs = 45000, pardonTimeTechnical_secs = 55000),
      esaProviderType = EsaProviderType.Dynamax)
    storage.put(zkPath + "A12/config", sys2)

    val initData = new ZkCorridor(id = "cor1",
      name = "cor1",
      serviceType = ServiceType.SectionControl,
      roadType = 1,
      isInsideUrbanArea = false,
      dynamaxMqId = "MQ1",
      approvedSpeeds = Seq[Int](80, 100),
      pshtm = new ZkPSHTMIdentification(useYear = true,
        useMonth = true,
        useDay = true,
        useReportingOfficerId = false,
        useNumberOfTheDay = false,
        useLocationCode = false,
        useHHMCode = false,
        useTypeViolation = false,
        useFixedCharacter = false,
        fixedCharacter = "B"),
      info = new ZkCorridorInfo(corridorId = 100,
        locationCode = "1",
        reportId = "TCS A2 Links  39.7 - 54.9",
        reportHtId = "A020011",
        reportPerformance = true,
        locationLine1 = "line",
        locationLine2 = None),
      startSectionId = "20",
      endSectionId = "40",
      allSectionIds = Seq[String]("20", "40"),
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 402,
      roadCode = 2,
      dutyType = "KA2",
      deploymentCode = "03",
      codeText = "")
    storage.put(zkPath + "A2/corridors/cor1/config", initData)

    val cor2 = initData.copy(id = "cor2", name = "cor2", dynamaxMqId = "MQ2")
    storage.put(zkPath + "A2/corridors/cor2/config", cor2)

    val info = new ZkCorridorInfo(corridorId = 200,
      locationCode = "1",
      reportId = "TCS A2 Links  39.7 - 54.9",
      reportHtId = "A020011",
      reportPerformance = true,
      locationLine1 = "line",
      locationLine2 = None)
    val cor3 = initData.copy(id = "cor3", name = "cor3", dynamaxMqId = "MQ2", info = info)
    storage.put(zkPath + "A12/corridors/cor3/config", cor3)
  }

  "The EnforceDynamax" must {

    "should notify speed logger when mqId match" in {
      val probe = TestProbe()
      def getActorSystem(systemId: String): Option[ActorSystem] = {
        Some(system)
      }
      def getSpeedLog(systemId: String): ActorRef = {
        probe.ref
      }
      val enfActorRef = system.actorOf(Props(new EnforceDynamax(curator = storage,
        mqId = "MQ1",
        maxCacheUse = 60000,
        zkPathPrefix = zkPath,
        getActorSystem = getActorSystem,
        getSpeedLog = getSpeedLog)))

      val update = new SectionSignUpdateMessage(sectionSign = AggregatedSignValue.KM80, techPardonTime_mSecs = 60000, time = new Date())
      val msg = new HHMSignUpdateMessage(
        sequenceNr = 100,
        sendTime = update.time,
        repeat = false,
        checksumMsg = "ABC",
        checksumApp = "ABD",
        checksumConfig = "ABE",
        updateMsg = update,
        disturbance = None)
      enfActorRef ! msg
      val recvMsg = new DynamaxSpeedData(effectiveTimestamp = update.time.getTime,
        corridorId = 100,
        sequenceNumber = msg.sequenceNr,
        speedSetting = Right(Some(update.sectionSign.id)),
        preChangeExclusionTime = Some(update.techPardonTime_mSecs))
      probe.expectMsg(10 seconds, recvMsg)
    }
    "should notify speed logger with unapproved speed" in {
      val probe = TestProbe()
      def getActorSystem(systemId: String): Option[ActorSystem] = {
        Some(system)
      }
      def getSpeedLog(systemId: String): ActorRef = {
        probe.ref
      }
      val enfActorRef = system.actorOf(Props(new EnforceDynamax(curator = storage,
        mqId = "MQ1",
        maxCacheUse = 60000,
        zkPathPrefix = zkPath,
        getActorSystem = getActorSystem,
        getSpeedLog = getSpeedLog)))

      val update = new SectionSignUpdateMessage(sectionSign = AggregatedSignValue.KM90, techPardonTime_mSecs = 60000, time = new Date())
      val msg = new HHMSignUpdateMessage(
        sequenceNr = 100,
        sendTime = update.time,
        repeat = false,
        checksumMsg = "ABC",
        checksumApp = "ABD",
        checksumConfig = "ABE",
        updateMsg = update,
        disturbance = None)
      enfActorRef ! msg
      val recvMsg = new DynamaxSpeedData(effectiveTimestamp = update.time.getTime,
        corridorId = 100,
        sequenceNumber = msg.sequenceNr,
        speedSetting = Left(SpeedIndicatorStatus.Undetermined),
        preChangeExclusionTime = Some(update.techPardonTime_mSecs))
      probe.expectMsg(10 seconds, recvMsg)
    }
    "should notify speed logger when mqId match in multiple systems" in {
      val probeA2 = TestProbe()
      val probeA12 = TestProbe()
      def getActorSystem(systemId: String): Option[ActorSystem] = {
        Some(system)
      }
      def getSpeedLog(systemId: String): ActorRef = {
        systemId match {
          case "A12" ⇒ probeA12.ref
          case "A2"  ⇒ probeA2.ref
          case _     ⇒ fail("Unexpected SpeedLog request")
        }
      }
      val enfActorRef = system.actorOf(Props(new EnforceDynamax(curator = storage,
        mqId = "MQ2",
        maxCacheUse = 60000,
        zkPathPrefix = zkPath,
        getActorSystem = getActorSystem,
        getSpeedLog = getSpeedLog)))

      val update = new SectionSignUpdateMessage(sectionSign = AggregatedSignValue.KM80, techPardonTime_mSecs = 60000, time = new Date())
      val msg = new HHMSignUpdateMessage(
        sequenceNr = 100,
        sendTime = update.time,
        repeat = false,
        checksumMsg = "ABC",
        checksumApp = "ABD",
        checksumConfig = "ABE",
        updateMsg = update,
        disturbance = None)
      enfActorRef ! msg
      val recvMsg = new DynamaxSpeedData(effectiveTimestamp = update.time.getTime,
        corridorId = 100,
        sequenceNumber = msg.sequenceNr,
        speedSetting = Right(Some(update.sectionSign.id)),
        preChangeExclusionTime = Some(update.techPardonTime_mSecs))
      val recvMsg2 = new DynamaxSpeedData(effectiveTimestamp = update.time.getTime,
        corridorId = 200,
        sequenceNumber = msg.sequenceNr,
        speedSetting = Right(Some(update.sectionSign.id)),
        preChangeExclusionTime = Some(update.techPardonTime_mSecs))
      probeA2.expectMsg(10 seconds, recvMsg)
      probeA12.expectMsg(10 seconds, recvMsg2)
    }
    "should notify create system event in multiple systems when disturbance received" in {
      val probe = TestProbe()
      system.eventStream.subscribe(probe.ref, classOf[SystemEvent])

      def getActorSystem(systemId: String): Option[ActorSystem] = {
        Some(system)
      }
      def getSpeedLog(systemId: String): ActorRef = {
        fail("Unexpected SpeedLog request")
      }

      val enfActorRef = system.actorOf(Props(new EnforceDynamax(curator = storage,
        mqId = "MQ2",
        maxCacheUse = 60000,
        zkPathPrefix = zkPath,
        getActorSystem = getActorSystem,
        getSpeedLog = getSpeedLog)))

      val update = new SectionSignUpdateMessage(sectionSign = AggregatedSignValue.KM80, techPardonTime_mSecs = 60000, time = new Date())
      val dist = new DisturbanceEvent(instanceId = 12,
        disturbanceType = DisturbanceType.KS_CONNECTION,
        component = DisturbanceComponent.RWSKOPPELING,
        message = "TEST",
        severity = DisturbanceSeverity.CRITICAL,
        time = update.time,
        state = DisturbanceState.DETECTED)
      val msg = new HHMSignUpdateMessage(
        sequenceNr = 100,
        sendTime = update.time,
        repeat = false,
        checksumMsg = "ABC",
        checksumApp = "ABD",
        checksumConfig = "ABE",
        updateMsg = update,
        disturbance = Some(dist))
      enfActorRef ! msg

      val recvMsg = SystemEvent(componentId = "Dynamax-MQ2-%s".format(dist.component),
        timestamp = dist.time.getTime,
        systemId = "A2",
        eventType = "Dynamax-%s-%s".format(dist.disturbanceType, dist.state),
        userId = "Dynamax",
        reason = Some(dist.message))
      val recvMsg2 = SystemEvent(componentId = "Dynamax-MQ2-%s".format(dist.component),
        timestamp = dist.time.getTime,
        systemId = "A12",
        eventType = "Dynamax-%s-%s".format(dist.disturbanceType, dist.state),
        userId = "Dynamax",
        reason = Some(dist.message))
      val recvList = Seq(recvMsg, recvMsg2)
      val recv1 = probe.expectMsgType[SystemEvent](10 seconds)
      recvList must contain(recv1)
      val recv2 = probe.expectMsgType[SystemEvent](10 seconds)
      recvList must contain(recv2)
      recv2 must not(be(recv1))
    }
  }

}