/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.violations

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

/**
 * @author Maarten Hazewinkel
 */
class GenerateIdTest extends WordSpec with MustMatchers {
  "trait GenerateId" must {
    "generate seqential integers from 1" in {
      val idGenerator = new GenerateId {}
      idGenerator.nextId must be(1)
      idGenerator.nextId must be(2)
      idGenerator.nextId must be(3)
      idGenerator.nextId must be(4)
    }
  }
}