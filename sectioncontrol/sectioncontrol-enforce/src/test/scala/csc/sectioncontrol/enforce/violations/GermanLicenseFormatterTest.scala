/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.violations

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

/**
 * @author Maarten Hazewinkel
 */
class GermanLicenseFormatterTest extends WordSpec with MustMatchers {
  "GermanLicenseFormatter.apply" must {
    "format correctly for VY7" in {
      GermanLicenseFormatter("VY7", None, None) must be("V-Y-7")
    }
    "format correctly for HHHF1762" in {
      GermanLicenseFormatter("HHHF1762", None, None) must be("HH-HF-1762")
    }
    "format correctly for HH1762" in {
      GermanLicenseFormatter("HH1762", None, None) must be("HH-1762")
    }
    "format correctly for unknown prefix URH1762" in {
      GermanLicenseFormatter("URH1762", None, None) must be("URH-1762")
    }
    "format correctly for HHHF1762 with only rawLicense1" in {
      GermanLicenseFormatter("HHHF1762", Some("HH*HF_1762"), None) must be("HH-HF-1762")
    }
    "format correctly for HHHF1762 with only rawLicense2" in {
      GermanLicenseFormatter("HHHF1762", None, Some("HH*HF_1762")) must be("HH-HF-1762")
    }
    "format correctly for HHHF1762 with newlines" in {
      GermanLicenseFormatter("HHHF1762", None, Some("HH*HF/1762")) must be("HH-HF-1762")
    }
    "format correctly for HHHF1762 without arms" in {
      GermanLicenseFormatter("HHHF1762", None, Some("HH_HF/1762")) must be("HH-HF-1762")
    }
    "format correctly for HHHF1762 two raws when equal" in {
      GermanLicenseFormatter("HHHF1762", Some("HH*HF_1762"), Some("HH*HF_1762")) must be("HH-HF-1762")
    }
    "format correctly for HHHF1762 two raws when missing arms" in {
      GermanLicenseFormatter("HHHF1762", Some("HHHF_1762"), Some("HH*HF_1762")) must be("HH-HF-1762")
    }
    "format correctly for HHHF1762 two raws when missing arms2" in {
      GermanLicenseFormatter("HHHF1762", Some("HH*HF_1762"), Some("HHHF_1762")) must be("HH-HF-1762")
    }
    "format correctly for HHHF1762 two raws when missing arms3" in {
      GermanLicenseFormatter("HHHF1762", Some("HHHF_1762"), Some("HHHF-1762")) must be("HH-HF-1762")
    }
    "format correctly for HHHF1762 two raws wrong position" in {
      GermanLicenseFormatter("HHHF1762", Some("HHH*F_1762"), Some("HH*HF_1762")) must be("HH-HF-1762")
    }
    "format correctly for HHHF1762 two raws wrong position2" in {
      GermanLicenseFormatter("HHHF1762", Some("HH*HF_1762"), Some("HHH*F_1762")) must be("HH-HF-1762")
    }
    "format correctly for HHHF1762 two raws wrong position3" in {
      GermanLicenseFormatter("HHHF1762", Some("HHHF*1762"), Some("HHH*F_1762")) must be("HH-HF-1762")
    }
    "format incorrect licences with arms" in {
      GermanLicenseFormatter("OOHF1762", Some("OOHF*1762"), Some("OOH*F_1762")) must be("OOHF-1762")
    }
    "format incorrect licences with one arms" in {
      GermanLicenseFormatter("OOHF1762", Some("OOHF_1762"), Some("OOH*F_1762")) must be("OOH-F-1762")
    }
    "format incorrect licences without arms" in {
      GermanLicenseFormatter("OOHF1762", Some("OOHF/1762"), Some("OOH_F_1762")) must be("OOHF-1762")
    }

  }

  "GermanLicenseFormatter.validate" must {
    "accept VYX7123" in {
      GermanLicenseFormatter.validate("VYX7123") must be(true)
    }
    "reject VY 7" in {
      GermanLicenseFormatter.validate("VY 7") must be(false)
    }
    "reject H!1234K" in {
      GermanLicenseFormatter.validate("H!1234K") must be(false)
    }
    "reject AAAOO7141972" in {
      GermanLicenseFormatter.validate("AAAOO7141972") must be(false)
    }
    "accept B-C-12" in {
      GermanLicenseFormatter.validate("BC12") must be(true)
    }
    "accept M-TL-4931" in {
      GermanLicenseFormatter.validate("MTL4931") must be(true)
    }
    "accept BI-ER-1912" in {
      GermanLicenseFormatter.validate("BIER1912") must be(true)
    }
    "accept KLE-S-1929" in {
      GermanLicenseFormatter.validate("KLES1929") must be(true)
    }
    "accept DA-250-U" in {
      GermanLicenseFormatter.validate("DA250U") must be(true)
    }
    "accept EL-03944" in {
      GermanLicenseFormatter.validate("EL03944") must be(true)
    }
    "accept 0-44-22 (corps diplomatique)" in {
      GermanLicenseFormatter.validate("04422") must be(true)
    }
    "accept Y-751-95  (militair kenteken)" in {
      GermanLicenseFormatter.validate("Y75195") must be(true)
    }
  }

  "GermanLicenseFormatter.validateFormatted" must {
    "accept VYX7123" in {
      GermanLicenseFormatter.validateFormatted("VY-X-7123") must be(true)
    }
    "reject VY7" in {
      GermanLicenseFormatter.validateFormatted("VY7") must be(false)
    }
    "reject H1234K" in {
      GermanLicenseFormatter.validateFormatted("H1234K") must be(false)
    }
    "reject EL--03944" in {
      GermanLicenseFormatter.validateFormatted("EL--03944") must be(false)
    }
    "reject DA-1234-U" in {
      GermanLicenseFormatter.validateFormatted("DA-1234-U") must be(false)
    }
    "reject BIER-1234" in {
      GermanLicenseFormatter.validateFormatted("BIER-1234") must be(false)
    }
    "reject BI" in {
      GermanLicenseFormatter.validateFormatted("BI") must be(false)
    }
    "reject BI-" in {
      GermanLicenseFormatter.validateFormatted("BI-") must be(false)
    }
    "reject OO7141972" in {
      GermanLicenseFormatter.validateFormatted("OO7141972") must be(false)
    }
    "accept B-C-12" in {
      GermanLicenseFormatter.validateFormatted("B-C-12") must be(true)
    }
    "accept M-TL-4931" in {
      GermanLicenseFormatter.validateFormatted("M-TL-4931") must be(true)
    }
    "accept BI-ER-1912" in {
      GermanLicenseFormatter.validateFormatted("BI-ER-1912") must be(true)
    }
    "accept KLE-S-1929" in {
      GermanLicenseFormatter.validateFormatted("KLE-S-1929") must be(true)
    }
    "accept DA-250-U" in {
      GermanLicenseFormatter.validateFormatted("DA-250-U") must be(true)
    }
    "accept EL-03944" in {
      GermanLicenseFormatter.validateFormatted("EL-03944") must be(true)
    }
    "accept 0-44-22 (corps diplomatique)" in {
      GermanLicenseFormatter.validateFormatted("0-44-22") must be(true)
    }
    "accept Y-751-95  (militair kenteken)" in {
      GermanLicenseFormatter.validateFormatted("Y-751-95") must be(true)
    }

  }
}
