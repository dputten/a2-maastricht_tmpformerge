/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.violations

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import Violations._

/**
 * @author Maarten Hazewinkel
 */
class PackageObjectTest extends WordSpec with MustMatchers {
  "function repeatAttemptWithDelays" must {
    "repeat 5 times until failing" in {
      repeatAttemptWithDelays(5, 1) {
        throw new Exception("x")
      } must be('left)
    }
    "attempt 3 times until succeeding" in {
      var count = 0;
      repeatAttemptWithDelays(5, 1) {
        count += 1
        if (count < 3) throw new Exception("x")
        "OK"
      } must be(Right("OK"))
      count must be(3)
    }
  }
}
