/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.register

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import akka.util.duration._
import java.util.{ Calendar, GregorianCalendar }

/**
 *
 * @author Maarten Hazewinkel
 */
class MinimumSeparationTest extends WordSpec with MustMatchers {
  def jan25HourMillis(hour: Int): Long = new GregorianCalendar(2002, Calendar.JANUARY, 25, hour, 0).getTimeInMillis
  val jan_25_2002_0900 = jan25HourMillis(9)
  val jan_25_2002_1000 = jan25HourMillis(10)
  val jan_25_2002_1100 = jan25HourMillis(11)
  val jan_25_2002_1200 = jan25HourMillis(12)
  val jan_25_2002_1300 = jan25HourMillis(13)
  val jan_25_2002_1400 = jan25HourMillis(14)
  val jan_25_2002_1500 = jan25HourMillis(15)
  val jan_25_2002_2000 = jan25HourMillis(20)

  val minutes_120 = 120.minutes
  val minutes_30 = 30.minutes

  def jan25time(hour: Int, minute: Int, second: Int = 0) =
    new GregorianCalendar(2002, Calendar.JANUARY, 25, hour, minute, second).getTimeInMillis

  "MinimumSeparation" must {
    "pick the largest of 2 violations" in {
      MinimumSeparation(List(MockViolation(jan_25_2002_0900, "alpha", 140, 120),
        MockViolation(jan_25_2002_1000, "alpha", 130, 100)), minutes_120.toMillis) must
        be(SplitViolations(List(MockViolation(jan_25_2002_1000, "alpha", 130, 100)),
          List(MockViolation(jan_25_2002_0900, "alpha", 140, 120))))
    }

    "pick the largest of 2 sets of violations" in {
      MinimumSeparation(List(MockViolation(jan_25_2002_0900, "alpha", 140, 120),
        MockViolation(jan_25_2002_1000, "alpha", 130, 100),
        MockViolation(jan_25_2002_0900, "beta", 130, 100),
        MockViolation(jan_25_2002_1000, "beta", 110, 100)), minutes_120.toMillis) must
        be(SplitViolations(List(MockViolation(jan_25_2002_1000, "alpha", 130, 100),
          MockViolation(jan_25_2002_0900, "beta", 130, 100)),
          List(MockViolation(jan_25_2002_0900, "alpha", 140, 120),
            MockViolation(jan_25_2002_1000, "beta", 110, 100))))
    }

    "retain 2 violations if sufficiently separated" in {
      MinimumSeparation(List(MockViolation(jan_25_2002_0900, "alpha", 140, 120),
        MockViolation(jan_25_2002_1100, "alpha", 130, 100)), minutes_120.toMillis) must
        be(SplitViolations(List(MockViolation(jan_25_2002_0900, "alpha", 140, 120),
          MockViolation(jan_25_2002_1100, "alpha", 130, 100)),
          List()))

    }

    "retain both larger violations" in {
      MinimumSeparation(List(MockViolation(jan_25_2002_0900, "alpha", 140, 120),
        MockViolation(jan_25_2002_1000, "alpha", 110, 100),
        MockViolation(jan_25_2002_1100, "alpha", 130, 100)), minutes_120.toMillis) must
        be(SplitViolations(List(MockViolation(jan_25_2002_0900, "alpha", 140, 120),
          MockViolation(jan_25_2002_1100, "alpha", 130, 100)),
          List(MockViolation(jan_25_2002_1000, "alpha", 110, 100))))

    }

    "discard both smaller violations" in {
      MinimumSeparation(List(MockViolation(jan_25_2002_0900, "alpha", 140, 120),
        MockViolation(jan_25_2002_1000, "alpha", 150, 100),
        MockViolation(jan_25_2002_1100, "alpha", 130, 100)), minutes_120.toMillis) must
        be(SplitViolations(List(MockViolation(jan_25_2002_1000, "alpha", 150, 100)),
          List(MockViolation(jan_25_2002_0900, "alpha", 140, 120),
            MockViolation(jan_25_2002_1100, "alpha", 130, 100))))

    }
    "Eliminate multiple violations within 30 minutes" in {
      val jan_25_2002_1005 = jan25time(10, 5)
      val jan_25_2002_1025 = jan25time(10, 25)
      val jan_25_2002_1035 = jan25time(10, 35)
      val jan_25_2002_1045 = jan25time(10, 45)
      val jan_25_2002_1110 = jan25time(11, 10)
      val split = MinimumSeparation(List(MockViolation(time = jan_25_2002_1005),
        MockViolation(time = jan_25_2002_1025),
        MockViolation(time = jan_25_2002_1035),
        MockViolation(time = jan_25_2002_1045),
        MockViolation(time = jan_25_2002_1110)), minutes_30.toMillis)
      split.ok must have size (3)
      split.discarded must have size (2)
    }
    "Eliminate violations within 30 minutes of an already-processed violation" in {
      val split = MinimumSeparation(List(MockViolation(time = jan_25_2002_1100, license = "DE34FG"),
        MockViolation(time = jan_25_2002_1100, license = "HI56JK")),
        minutes_120.toMillis,
        List(MockViolation(time = jan_25_2002_1000, license = "HI56JK")))

      split.ok must have size (1)
      split.discarded must have size (1)
    }
  }
}

case class MockViolation(time: Long, license: String = "AB12CD", measuredSpeed: Int = 113, enforcedSpeed: Int = 100) extends MinimumSeparation.ViolationData {
  def exportLicense = license
  def separationLicense = license
}
