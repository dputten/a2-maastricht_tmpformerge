/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.speedlog

import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import java.util.{ Calendar, GregorianCalendar }
import akka.actor.ActorSystem
import akka.testkit.TestActorRef
import akka.dispatch.Await
import akka.util.duration._
import akka.util.{ Duration, Timeout }
import akka.pattern.ask
import csc.sectioncontrol.enforce.excludelog.ExcludeLog
import csc.sectioncontrol.storagelayer.excludelog.{ Exclusion, Exclusions }
import csc.sectioncontrol.storage._
import csc.sectioncontrol.enforce.{ Cleanup, CuratorHelper, SpeedEventData, SpeedIndicatorStatus }
import csc.curator.CuratorTestServer
import csc.curator.utils.Curator
import csc.sectioncontrol.storagelayer.state.StateService
import csc.sectioncontrol.storagelayer.corridor.CorridorService
import csc.sectioncontrol.storage.ZkCorridorInfo
import csc.sectioncontrol.storage.ZkPSHTMIdentification
import csc.sectioncontrol.enforce.Cleanup
import csc.sectioncontrol.enforce.excludelog.GetExclusions
import csc.sectioncontrol.storage.ZkCorridor

/**
 *
 * @author Maarten Hazewinkel
 */
class SpeedLogTest extends WordSpec with MustMatchers with BeforeAndAfterAll with CuratorTestServer {

  private var storage: Curator = _

  def jan25HourMillis(hour: Int): Long = (new GregorianCalendar(2002, Calendar.JANUARY, 25, hour, 0).getTimeInMillis / 1000L) * 1000L

  val jan_25_2002_0000 = jan25HourMillis(00)
  val jan_25_2002_1000 = jan25HourMillis(10)
  val jan_25_2002_1100 = jan25HourMillis(11)
  val jan_25_2002_1200 = jan25HourMillis(12)
  val jan_25_2002_1300 = jan25HourMillis(13)
  val jan_25_2002_1400 = jan25HourMillis(14)
  val jan_25_2002_1500 = jan25HourMillis(15)
  val jan_25_2002_2400 = jan25HourMillis(24)

  /*
  println(">>>>>" +
    "\njan_25_2002_0000: " + jan_25_2002_0000 +
    "\njan_25_2002_1000: " + jan_25_2002_1000 +
    "\njan_25_2002_1100: " + jan_25_2002_1100 +
    "\njan_25_2002_1200: " + jan_25_2002_1200 +
    "\njan_25_2002_1300: " + jan_25_2002_1300 +
    "\njan_25_2002_1400: " + jan_25_2002_1400 +
    "\njan_25_2002_1500: " + jan_25_2002_1500)
  */

  val speed120 = Right[SpeedIndicatorStatus.Value, Option[Int]](Some(120))
  val speed100 = Right[SpeedIndicatorStatus.Value, Option[Int]](Some(100))
  val speed90 = Right[SpeedIndicatorStatus.Value, Option[Int]](Some(90))
  val speed80 = Right[SpeedIndicatorStatus.Value, Option[Int]](Some(80))
  val speed70 = Right[SpeedIndicatorStatus.Value, Option[Int]](Some(70))
  val speed50 = Right[SpeedIndicatorStatus.Value, Option[Int]](Some(50))
  val speedBl = Right[SpeedIndicatorStatus.Value, Option[Int]](None)
  val speedInd = Left[SpeedIndicatorStatus.Value, Option[Int]](SpeedIndicatorStatus.Undetermined)
  val speedSB = Left[SpeedIndicatorStatus.Value, Option[Int]](SpeedIndicatorStatus.StandBy)

  implicit val testSystem = ActorSystem("SpeedLogTest")
  val testDuration: Duration = 2.seconds
  implicit val testTimeout = Timeout(testDuration)

  var testSpeedLog: TestActorRef[SpeedLog] = _

  val systemId = "eg33"

  override def beforeEach() {
    super.beforeEach()
    storage = CuratorHelper.create(this, clientScope)
    testSpeedLog = TestActorRef(new SpeedLog(systemId, storage))

    val corridor = createCorridor()
    CorridorService.createCorridor(storage, systemId, corridor)
    val corridor2 = corridor.copy(id = "S2", info = corridor.info.copy(corridorId = 2))
    CorridorService.createCorridor(storage, systemId, corridor2)

    StateService.create(systemId, "S1", ZkState(ZkState.enforceOn, 1, "test", "test"), storage)
    StateService.create(systemId, "S2", ZkState(ZkState.enforceOn, 1, "test", "test"), storage)
  }

  override def afterEach() {
    testSpeedLog.stop()
    super.afterEach()
  }

  override def afterAll() {
    testSystem.shutdown()
    super.afterAll()
  }

  val defaultElem = 1 -> SpeedSetting(Left(SpeedIndicatorStatus.Undetermined))
  val defaultElem2 = 0 -> SpeedSetting(Left(SpeedIndicatorStatus.StandBy))
  "SpeedLog speedSettings" must {
    "be empty when no events are supplied" in {
      val result = testSpeedLog ? GetEnforcedSpeeds(1)
      Await.result(result, testDuration).asInstanceOf[SpeedSettingOverTime].settings must be(
        Set(defaultElem, defaultElem2))
    }

    "have multiple events" in {
      testSpeedLog ! MockEvent(jan_25_2002_1200, 1, speed120)
      testSpeedLog ! MockEvent(jan_25_2002_1300, 1, speed90)
      testSpeedLog ! MockEvent(jan_25_2002_1500, 1, speed50)

      val result = testSpeedLog ? GetEnforcedSpeeds(1)
      Await.result(result, testDuration).asInstanceOf[SpeedSettingOverTime].settings.toList must be(
        List(jan_25_2002_1500 -> SpeedSetting(speed50),
          jan_25_2002_1300 -> SpeedSetting(speed90),
          jan_25_2002_1200 -> SpeedSetting(speed120),
          defaultElem, defaultElem2))
    }

    "in events from other corridors than requested" in {
      testSpeedLog ! MockEvent(jan_25_2002_1200, 1, speed50, corridorId = 2)
      testSpeedLog ! MockEvent(jan_25_2002_1300, 1, speed90, corridorId = 2)
      testSpeedLog ! MockEvent(jan_25_2002_1500, 1, speed120, corridorId = 2)
      testSpeedLog ! MockEvent(jan_25_2002_1000, 1, speed70, corridorId = 1)
      testSpeedLog ! MockEvent(jan_25_2002_1100, 1, speedInd, corridorId = 1)

      val result = testSpeedLog ? GetEnforcedSpeeds(1)
      Await.result(result, testDuration).asInstanceOf[SpeedSettingOverTime].settings.toList must be(
        List(jan_25_2002_1100 -> SpeedSetting(speedInd),
          jan_25_2002_1000 -> SpeedSetting(speed70),
          defaultElem, defaultElem2))
    }
    "clean up old events" in {
      testSpeedLog ! MockEvent(jan_25_2002_1200, 1, speed100)
      testSpeedLog ! MockEvent(jan_25_2002_1300, 1, speed90)
      testSpeedLog ! MockEvent(jan_25_2002_1500, 1, speed70)
      testSpeedLog ! MockEvent(jan_25_2002_1000, 1, speedSB)
      testSpeedLog ! MockEvent(jan_25_2002_1100, 1, speed100)

      testSpeedLog ! Cleanup(10)

      val result = testSpeedLog ? GetEnforcedSpeeds(1)
      Await.result(result, testDuration).asInstanceOf[SpeedSettingOverTime].settings.toList must be(
        List(defaultElem, defaultElem2))
    }
    "retain recent events during cleanup" in {
      testSpeedLog ! MockEvent(jan_25_2002_1200, 1, speed100)
      testSpeedLog ! MockEvent(jan_25_2002_1300, 1, speed90)
      testSpeedLog ! MockEvent(jan_25_2002_1500, 1, speed70)
      testSpeedLog ! MockEvent(jan_25_2002_1000, 1, speedSB)
      testSpeedLog ! MockEvent(jan_25_2002_1100, 1, speed100)

      val recent1 = System.currentTimeMillis() - 60000
      val recent2 = System.currentTimeMillis() - 1000
      testSpeedLog ! MockEvent(recent1, 1, speed120)
      testSpeedLog ! MockEvent(recent2, 1, speed90)

      testSpeedLog ! Cleanup(10)

      val result = testSpeedLog ? GetEnforcedSpeeds(1)
      Await.result(result, testDuration).asInstanceOf[SpeedSettingOverTime].settings.toList must be(
        List(recent2 -> SpeedSetting(speed90),
          recent1 -> SpeedSetting(speed120),
          defaultElem, defaultElem2))
    }

    "generate exclude events" in {
      testSpeedLog ! MockEvent(jan_25_2002_1000, 1, speed100, 1)
      testSpeedLog ! MockEvent(jan_25_2002_1100, 1, speedInd, 1)
      testSpeedLog ! MockEvent(jan_25_2002_1200, 1, speed100, 1)
      testSpeedLog ! MockEvent(jan_25_2002_1300, 1, speed90, 1)

      testSpeedLog ! MockEvent(jan_25_2002_1100, 1, speed120, 2)
      testSpeedLog ! MockEvent(jan_25_2002_1200, 1, speed100, 2, Some(50))

      val excludeLog = TestActorRef(new ExcludeLog(systemId, storage))

      val completed = testSpeedLog ? GenerateExcludeEvents(jan_25_2002_1000 - 1, jan_25_2002_1500, 1, 100, 200, excludeLog, Seq())
      val completed2 = testSpeedLog ? GenerateExcludeEvents(jan_25_2002_1000 - 1, jan_25_2002_1500, 2, 100, 200, excludeLog, Seq())
      Await.result(completed, testDuration).asInstanceOf[GeneratedExcludeEvents.type] must be(GeneratedExcludeEvents)
      Await.result(completed2, testDuration).asInstanceOf[GeneratedExcludeEvents.type] must be(GeneratedExcludeEvents)

      val result = excludeLog ? GetExclusions(1)
      val exclusions = Await.result(result, testDuration).asInstanceOf[Exclusions]
      exclusions.intersectsInterval(2, jan_25_2002_1000 - 101) must be(false)
      exclusions.intersectsInterval(2, jan_25_2002_1000 - 100) must be(true)
      exclusions.intersectsInterval(jan_25_2002_1000 + 200, jan_25_2002_1000 + 1000) must be(true)
      exclusions.intersectsInterval(jan_25_2002_1000 + 201, jan_25_2002_1000 + 1000) must be(false)
      exclusions.intersectsInterval(jan_25_2002_1000, jan_25_2002_1000) must be(true)

      exclusions.intersectsInterval(jan_25_2002_1000 + 1000, jan_25_2002_1100 - 101) must be(false)
      exclusions.intersectsInterval(jan_25_2002_1000 + 1000, jan_25_2002_1100 - 100) must be(true)
      exclusions.intersectsInterval(jan_25_2002_1100 + 1000, jan_25_2002_1100 + 1001) must be(false)

      exclusions.intersectsInterval(jan_25_2002_1200 - 1000, jan_25_2002_1200 - 999) must be(false)
      exclusions.intersectsInterval(jan_25_2002_1200 - 50, jan_25_2002_1200 - 49) must be(true)
      exclusions.intersectsInterval(jan_25_2002_1200 + 200, jan_25_2002_1200 + 1000) must be(true)
      exclusions.intersectsInterval(jan_25_2002_1200 + 201, jan_25_2002_1200 + 1000) must be(false)

      val result2 = excludeLog ? GetExclusions(2)
      val exclusions2 = Await.result(result2, testDuration).asInstanceOf[Exclusions]
      exclusions2.intersectsInterval(2, jan_25_2002_1000 - 101) must be(false)
      exclusions2.intersectsInterval(2, jan_25_2002_1000 - 100) must be(false)
      exclusions2.intersectsInterval(jan_25_2002_1000 + 200, jan_25_2002_1000 + 1000) must be(false)
      exclusions2.intersectsInterval(jan_25_2002_1000 + 201, jan_25_2002_1000 + 1000) must be(false)
      exclusions2.intersectsInterval(jan_25_2002_1000, jan_25_2002_1000) must be(false)

      exclusions2.intersectsInterval(jan_25_2002_1000 + 1000, jan_25_2002_1100 - 101) must be(false)
      exclusions2.intersectsInterval(jan_25_2002_1000 + 1000, jan_25_2002_1100 - 100) must be(true)
      exclusions2.intersectsInterval(jan_25_2002_1100 + 1000, jan_25_2002_1100 + 1001) must be(false)

      exclusions2.intersectsInterval(jan_25_2002_1200 - 1000, jan_25_2002_1200 - 999) must be(false)
      exclusions2.intersectsInterval(jan_25_2002_1200 - 52, jan_25_2002_1200 - 51) must be(false)
      exclusions2.intersectsInterval(jan_25_2002_1200 - 50, jan_25_2002_1200 - 49) must be(true)
      exclusions2.intersectsInterval(jan_25_2002_1200 + 200, jan_25_2002_1200 + 1000) must be(true)
      exclusions2.intersectsInterval(jan_25_2002_1200 + 201, jan_25_2002_1200 + 1000) must be(false)
    }
    "generate exclude for schedule speed changes (TC-350)" in {
      testSpeedLog ! MockEvent(jan_25_2002_1000, 1, speed90, 1)
      val excludeLog = TestActorRef(new ExcludeLog(systemId, storage))

      val completed = testSpeedLog ? GenerateExcludeEvents(jan_25_2002_1000, jan_25_2002_1500, 1, 100, 200, excludeLog,
        Seq(ScheduledSpeed(jan_25_2002_1000, jan_25_2002_1100, 90, supportMsgBoard = false),
          ScheduledSpeed(jan_25_2002_1100, jan_25_2002_1500, 100, supportMsgBoard = false)))
      Await.result(completed, testDuration).asInstanceOf[GeneratedExcludeEvents.type] must be(GeneratedExcludeEvents)

      val result = excludeLog ? GetExclusions(1)
      val exclusions = Await.result(result, testDuration).asInstanceOf[Exclusions]
      exclusions.intersectsInterval(jan_25_2002_1100 - 1000, jan_25_2002_1100 - 101) must be(false)
      exclusions.intersectsInterval(jan_25_2002_1100 - 1000, jan_25_2002_1100 - 100) must be(true)
      exclusions.intersectsInterval(jan_25_2002_1100 + 200, jan_25_2002_1100 + 1000) must be(true)
      exclusions.intersectsInterval(jan_25_2002_1100 + 201, jan_25_2002_1100 + 1000) must be(false)
    }

    "generate exclude when supportMsgBoard is true and speedEvents do not match scheduled speed" in {
      testSpeedLog ! MockEvent(jan_25_2002_1000, 1, speed50, 1)
      testSpeedLog ! MockEvent(jan_25_2002_1100, 1, speed90, 1)
      testSpeedLog ! MockEvent(jan_25_2002_1300, 1, speed100, 1)
      testSpeedLog ! MockEvent(jan_25_2002_1400, 1, speed90, 1)

      val excludeLog = TestActorRef(new ExcludeLog(systemId, storage))

      val completed = testSpeedLog ? GenerateExcludeEvents(from = jan_25_2002_1000, until = jan_25_2002_1500, corridorId = 1,
        preChangeExclusionTime = 1000, postChangeExclusionTime = 2000, excludeLog = excludeLog,
        scheduleSpeeds = Seq(ScheduledSpeed(jan_25_2002_1000, jan_25_2002_1200, 90, supportMsgBoard = true),
          ScheduledSpeed(jan_25_2002_1200, jan_25_2002_1500, 100, supportMsgBoard = true)))
      Await.result(completed, testDuration).asInstanceOf[GeneratedExcludeEvents.type] must be(GeneratedExcludeEvents)

      val result = excludeLog ? GetExclusions(1)
      val exclusionsForCorridor = Await.result(result, testDuration).asInstanceOf[Exclusions]
      //println(">>> xxxx exclusions: ")
      //exclusionsForCorridor.exclusions.foreach(println)
      //println("<<<")
      exclusionsForCorridor.exclusions.size must be(6)
      exclusionsForCorridor.intersectsInterval(jan_25_2002_1000, jan_25_2002_1100) must be(true)
      exclusionsForCorridor.intersectsInterval(jan_25_2002_1100, jan_25_2002_1100 + 200) must be(true)
      exclusionsForCorridor.intersectsInterval(jan_25_2002_1200 - 1000, jan_25_2002_1300) must be(true)

      exclusionsForCorridor.intersectsInterval(jan_25_2002_1300, jan_25_2002_1300 + 2000) must be(true)
      exclusionsForCorridor.intersectsInterval(jan_25_2002_1400 - 1000, jan_25_2002_1500) must be(true)

    }

    "generate exclude when supportMsgBoard is true and receive a single speedEvent 'BL'" in {
      testSpeedLog ! MockEvent(jan_25_2002_0000, 1, speedBl, 1)

      val excludeLog = TestActorRef(new ExcludeLog(systemId, storage))

      val completed = testSpeedLog ? GenerateExcludeEvents(jan_25_2002_1000, jan_25_2002_1500, 1, 100, 200, excludeLog,
        Seq(ScheduledSpeed(jan_25_2002_1000, jan_25_2002_1200, 90, supportMsgBoard = true),
          ScheduledSpeed(jan_25_2002_1200, jan_25_2002_1500, 100, supportMsgBoard = true)))
      Await.result(completed, testDuration).asInstanceOf[GeneratedExcludeEvents.type] must be(GeneratedExcludeEvents)

      val result = excludeLog ? GetExclusions(1)
      val exclusionsForCorridor = Await.result(result, testDuration).asInstanceOf[Exclusions]

      //println(">>>>>" + exclusionsForCorridor)

      exclusionsForCorridor.exclusions.size must be(2)
      exclusionsForCorridor.intersectsInterval(jan_25_2002_1000 - 1000, jan_25_2002_1500) must be(true)
    }
    "generate exclude when supportMsgBoard is true and receive a single speedEvent '90'" in {
      testSpeedLog ! MockEvent(jan_25_2002_0000, 1, speed90, 1)

      val excludeLog = TestActorRef(new ExcludeLog(systemId, storage))

      val completed = testSpeedLog ? GenerateExcludeEvents(jan_25_2002_0000, jan_25_2002_2400, 1, 100, 200, excludeLog,
        Seq(ScheduledSpeed(jan_25_2002_0000, jan_25_2002_2400, 90, supportMsgBoard = true)))

      Await.result(completed, testDuration).asInstanceOf[GeneratedExcludeEvents.type] must be(GeneratedExcludeEvents)

      val result = excludeLog ? GetExclusions(1)
      val exclusionsForCorridor = Await.result(result, testDuration).asInstanceOf[Exclusions]

      //println(">>>>>" + exclusionsForCorridor)

      exclusionsForCorridor.exclusions.size must be(1) //reset end of day
      exclusionsForCorridor.intersectsInterval(jan_25_2002_0000, jan_25_2002_2400) must be(false)
    }
    "generate exclude when supportMsgBoard is true and receive a multiple speedEvent before day start (TCU-240)" in {
      val hour = 60L * 60L

      testSpeedLog ! MockEvent(jan_25_2002_0000, 1, speed80, 1)
      testSpeedLog ! MockEvent(jan_25_2002_0000 - 6 * hour, 1, speed90, 1)
      testSpeedLog ! MockEvent(jan_25_2002_0000 - 12 * hour, 1, speedBl, 1)
      testSpeedLog ! MockEvent(jan_25_2002_0000 - 24 * hour, 1, speed100, 1)

      val excludeLog = TestActorRef(new ExcludeLog(systemId, storage))

      val completed = testSpeedLog ? GenerateExcludeEvents(jan_25_2002_0000, jan_25_2002_2400, 1, 100, 200, excludeLog,
        Seq(ScheduledSpeed(jan_25_2002_0000, jan_25_2002_2400, 80, supportMsgBoard = true)))
      Await.result(completed, testDuration).asInstanceOf[GeneratedExcludeEvents.type] must be(GeneratedExcludeEvents)

      val result = excludeLog ? GetExclusions(1)
      val exclusionsForCorridor = Await.result(result, testDuration).asInstanceOf[Exclusions]

      exclusionsForCorridor.exclusions.size must be(1)
      exclusionsForCorridor.exclusions.head must be(Exclusion("SystemState", 0, 1, "All/SystemState:Not started"))
    }
  }

  private case class MockEvent(effectiveTimestamp: Long,
                               sequenceNumber: Long,
                               speedSetting: Either[SpeedIndicatorStatus.Value, Option[Int]],
                               corridorId: Int = 1,
                               override val preChangeExclusionTime: Option[Long] = None) extends SpeedEventData

  private def createCorridor(): ZkCorridor = {
    ZkCorridor(id = "S1",
      name = "S1-name",
      roadType = 0,
      serviceType = ServiceType.SectionControl,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = false,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = ZkPSHTMIdentification(useYear = false,
        useMonth = true,
        useDay = false,
        useReportingOfficerId = false,
        useNumberOfTheDay = true,
        useLocationCode = false,
        useHHMCode = false,
        useTypeViolation = false,
        useFixedCharacter = false,
        fixedCharacter = "T"),
      info = ZkCorridorInfo(corridorId = 1,
        locationCode = "2",
        reportId = "",
        reportHtId = "",
        reportPerformance = true,
        locationLine1 = ""),
      startSectionId = "sec1",
      endSectionId = "sec1",
      allSectionIds = Seq("sec1"),
      direction = ZkDirection(directionFrom = "",
        directionTo = ""),
      radarCode = 4,
      roadCode = 0,
      dutyType = "",
      deploymentCode = "",
      codeText = "")
  }
}