/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.timewatch

import org.scalatest.matchers.MustMatchers
import akka.testkit.{ TestKit, TestActorRef }
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import akka.actor.ActorSystem
import csc.curator.CuratorTestServer
import csc.curator.utils.{ CuratorToolsImpl, Curator }
import csc.akkautils.DirectLogging

/**
 *
 * @author Maarten Hazewinkel
 */
class WatchTimePardonSwitchTest extends TestKit(ActorSystem("WatchTimeTest")) with WordSpec with MustMatchers
  with BeforeAndAfterAll with CuratorTestServer with DirectLogging {

  private var storage: Curator = _
  val springDate = 1332633600000L
  private var pardonOn = false

  private class WatchTimeTestable extends WatchTime("eg33", "localhost", "Europe/Amsterdam", testActor, storage, pardonOn) {

    override private[timewatch] def checkForDSTSwitch(day: Long, send: Boolean = true) {
      super.checkForDSTSwitch(springDate, send)
    }
  }

  private var testWatcher: TestActorRef[WatchTimeTestable] = _

  override def beforeEach() {
    super.beforeEach()
    storage = new CuratorToolsImpl(clientScope, log)
  }

  override def afterEach() {
    testWatcher.stop()
    system.stop(testActor)
    super.afterEach()
  }

  override def afterAll() {
    system.shutdown()
    super.afterAll()
  }

  "WatchTime" must {

    "detect a trigger of the sendSuspension method when the pardonDSTSwitch was true" in {
      pardonOn = true
      testWatcher = TestActorRef(new WatchTimeTestable)

      testWatcher ! CheckForTimeChanges

      val sunday25March2012ThreeOClockInTheMorning = 1332637200000l

      expectMsg(TimeChangeEvent(sunday25March2012ThreeOClockInTheMorning, 1, true, "DST change"))
      expectMsg(TimeChangeEvent(sunday25March2012ThreeOClockInTheMorning, 2, false, "DST change"))
    }

    "detect no trigger of the sendSuspension method when the pardonDSTSwitch was false" in {
      pardonOn = false
      testWatcher = TestActorRef(new WatchTimeTestable)

      testWatcher ! CheckForTimeChanges

      expectNoMsg()
    }
  }
}
