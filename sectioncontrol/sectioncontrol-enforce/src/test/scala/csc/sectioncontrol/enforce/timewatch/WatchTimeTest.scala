/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.timewatch

import org.scalatest.matchers.MustMatchers
import akka.testkit.TestActorRef
import csc.config.Path
import org.apache.commons.net.ntp.{ NtpV3Packet, TimeStamp, NtpV3Impl, TimeInfo }
import java.util.{ Calendar, TimeZone, GregorianCalendar, Date }
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import csc.sectioncontrol.enforce.excludelog.ExcludeLog
import akka.actor.{ ActorRef, Props, ActorSystem }
import csc.sectioncontrol.enforce.CuratorHelper
import csc.curator.CuratorTestServer
import csc.curator.utils.{ CuratorToolsImpl, Curator }
import csc.akkautils.DirectLogging
import csc.sectioncontrol.messages.{ TimeChanges, Interval }

/**
 *
 * @author Maarten Hazewinkel
 */
class WatchTimeTest extends WordSpec with MustMatchers with CuratorTestServer
  with BeforeAndAfterAll with DirectLogging {
  var lastSuspendInterval: Option[Interval] = _
  var sendSuspensionCallCount: Int = _
  private var storage: Curator = _

  implicit val testSystem = ActorSystem("WatchTimeTest")
  var excludeLog: ActorRef = _

  private class WatchTimeTestable extends WatchTime("eg33", "localhost", "Europe/Amsterdam", excludeLog, storage, false) {
    override private[timewatch] def sendSuspension(suspendInterval: Interval, reason: String) {
      sendSuspensionCallCount += 1
      lastSuspendInterval = Some(suspendInterval)
    }

    override private[timewatch] def getNtpTimeInfo: Option[TimeInfo] = {
      if (useNtpServer) {
        super.getNtpTimeInfo
      } else {
        substituteNtpTimeInfo
      }
    }
  }

  private class WatchTimeReal(ntpServer: String) extends WatchTime("eg33", ntpServer, "Europe/Amsterdam", excludeLog, storage, false) {
    override private[timewatch] def sendSuspension(suspendInterval: Interval, reason: String) {
      sendSuspensionCallCount += 1
      lastSuspendInterval = Some(suspendInterval)
    }
  }
  var useNtpServer = true
  var substituteNtpTimeInfo: Option[TimeInfo] = None

  private var testWatcher: TestActorRef[WatchTimeTestable] = _
  val statePath = Path("/ctes/systems/eg33/timewatch-state")

  override def beforeEach() {
    super.beforeEach()
    storage = new CuratorToolsImpl(clientScope, log)
    excludeLog = testSystem.actorOf(Props(new ExcludeLog("eg33", storage)))
    testWatcher = TestActorRef(new WatchTimeTestable)
    lastSuspendInterval = None
    sendSuspensionCallCount = 0
    useNtpServer = true
    substituteNtpTimeInfo = None
  }

  override def afterEach() {
    testWatcher.stop()
    testSystem.stop(excludeLog)
    super.afterEach()
  }

  "WatchTime" must {
    "detect no DST switch on regular days" in {
      testWatcher.underlyingActor.checkForDSTSwitch(1332418212957L)
      sendSuspensionCallCount must be(0)
    }

    "detect DST switch on in spring" in {
      testWatcher.underlyingActor.checkForDSTSwitch(1332633600000L)
      sendSuspensionCallCount must be(1)
      lastSuspendInterval must be(Some(Interval(1332637200000L, 1332637200000L)))
      storage.get[TimeChanges](statePath) must be(Some(TimeChanges(None, Some(Interval(1332637200000L, 1332637200000L)))))

      testWatcher.underlyingActor.checkForDSTSwitch(1301223600000L)
      sendSuspensionCallCount must be(2)
      lastSuspendInterval must be(Some(Interval(1301187600000L, 1301187600000L)))
      storage.get[TimeChanges](statePath) must be(Some(TimeChanges(None, Some(Interval(1301187600000L, 1301187600000L)))))
    }

    "detect DST switch off in autumn" in {
      testWatcher.underlyingActor.checkForDSTSwitch(1319929200000L)
      sendSuspensionCallCount must be(1)
      lastSuspendInterval must be(Some(Interval(1319932800000L, 1319940000000L)))
      storage.get[TimeChanges](statePath) must be(Some(TimeChanges(None, Some(Interval(1319932800000L, 1319940000000L)))))

      testWatcher.underlyingActor.checkForDSTSwitch(1288536000000L)
      sendSuspensionCallCount must be(2)
      lastSuspendInterval must be(Some(Interval(1288483200000L, 1288490400000L)))
      storage.get[TimeChanges](statePath) must be(Some(TimeChanges(None, Some(Interval(1288483200000L, 1288490400000L)))))
    }

    "signal DST switch only once for a single day" in {
      testWatcher.underlyingActor.checkForDSTSwitch(1332633600000L)
      sendSuspensionCallCount must be(1)
      lastSuspendInterval must be(Some(Interval(1332637200000L, 1332637200000L)))
      testWatcher.underlyingActor.checkForDSTSwitch(1332673200000L)
      sendSuspensionCallCount must be(1)
    }

    "not signal DST switch if already signalled on a previous run, as stored in zookeeper" in {
      storage.put(statePath, TimeChanges(None, Some(Interval(1319932800000L, 1319940000000L))))
      testWatcher.restart(new Throwable("WatchTimeTest"))
      testWatcher.underlyingActor.checkForDSTSwitch(1319929200000L)
      sendSuspensionCallCount must be(0)
      lastSuspendInterval must be(None)
    }

    "detect end-of-month dates" in {
      testWatcher.underlyingActor.isLastDayOfMonth(1332426218174L) must be(false)
      testWatcher.underlyingActor.isLastDayOfMonth(1011949200000L) must be(false)
      testWatcher.underlyingActor.isLastDayOfMonth(1012381200000L) must be(false)
      testWatcher.underlyingActor.isLastDayOfMonth(1012467600000L) must be(true)
      testWatcher.underlyingActor.isLastDayOfMonth(1330419600000L) must be(false)
      testWatcher.underlyingActor.isLastDayOfMonth(1330506000000L) must be(true)
    }

    "not signal a leap second when request times out" in {
      testWatcher.underlyingActor.checkNtpForLeapSecond()
      sendSuspensionCallCount must be(0)
    }

    "process a real ntp signal when available without throwing an exception" in {
      val realWatcher: TestActorRef[WatchTimeReal] = TestActorRef(new WatchTimeReal("10.109.22.14"))
      val timeInfo = realWatcher.underlyingActor.getNtpTimeInfo
      timeInfo.foreach(ti ⇒ {
        ti.computeDetails()
      })
    }

    "not signal a leap second when it is not set on the ntp response" in {
      val now = new Date
      val ntpRecord = new NtpV3Impl()
      ntpRecord.setTransmitTime(new TimeStamp(now))
      ntpRecord.setLeapIndicator(NtpV3Packet.LI_NO_WARNING)
      substituteNtpTimeInfo = Some(new TimeInfo(ntpRecord, 10))
      useNtpServer = false

      testWatcher.underlyingActor.checkNtpForLeapSecond()
      sendSuspensionCallCount must be(0)
    }

    def getDayEndUTC(time: Long): Long = {
      val cal = new GregorianCalendar(TimeZone.getTimeZone("UTC"))
      cal.setTimeInMillis(time)
      cal.set(Calendar.HOUR_OF_DAY, 0)
      cal.set(Calendar.MINUTE, 0)
      cal.set(Calendar.SECOND, 0)
      cal.set(Calendar.MILLISECOND, 0)
      cal.add(Calendar.DATE, 1)
      cal.getTimeInMillis
    }

    "signal an added leap second when it is set on the ntp response" in {
      val now = new Date
      val endOfDay = getDayEndUTC(now.getTime)
      val ntpRecord = new NtpV3Impl()
      ntpRecord.setTransmitTime(new TimeStamp(now))
      ntpRecord.setLeapIndicator(NtpV3Packet.LI_LAST_MINUTE_HAS_61_SECONDS)
      substituteNtpTimeInfo = Some(new TimeInfo(ntpRecord, 10))
      useNtpServer = false

      testWatcher.underlyingActor.checkNtpForLeapSecond()
      sendSuspensionCallCount must be(1)
      lastSuspendInterval must be(Some(Interval(endOfDay, endOfDay + 1000)))
      storage.get[TimeChanges](statePath) must be(Some(TimeChanges(Some(Interval(endOfDay, endOfDay + 1000)), None)))
    }

    "signal a removed leap second when it is set on the ntp response" in {
      val now = new Date
      val endOfDay = getDayEndUTC(now.getTime)
      val ntpRecord = new NtpV3Impl()
      ntpRecord.setTransmitTime(new TimeStamp(now))
      ntpRecord.setLeapIndicator(NtpV3Packet.LI_LAST_MINUTE_HAS_59_SECONDS)
      substituteNtpTimeInfo = Some(new TimeInfo(ntpRecord, 10))
      useNtpServer = false

      testWatcher.underlyingActor.checkNtpForLeapSecond()
      sendSuspensionCallCount must be(1)
      lastSuspendInterval must be(Some(Interval(endOfDay - 1000, endOfDay)))
      storage.get[TimeChanges](statePath) must be(Some(TimeChanges(Some(Interval(endOfDay - 1000, endOfDay)), None)))
    }
  }

  override protected def afterAll() {
    testSystem.shutdown()
    super.afterAll()
  }
}
