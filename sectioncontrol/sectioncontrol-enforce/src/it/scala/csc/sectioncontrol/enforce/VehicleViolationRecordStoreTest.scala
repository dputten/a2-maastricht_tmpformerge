/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce

import java.util.Date

import csc.hbase.utils.testframework.HBaseTestFramework
import csc.hbase.utils.{JsonHBaseReader, JsonHBaseWriter, NoDistributionOfRowKey}
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.messages._
import csc.sectioncontrol.storage.HBaseTableDefinitions._
import csc.sectioncontrol.storage.{ConfigType, HBaseTableDefinitions}
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.util.Bytes
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

/**
 * @author Maarten Hazewinkel
 */
class VehicleViolationRecordStoreTest extends WordSpec with MustMatchers with HBaseTestFramework {

  var testTable: Option[HTable] = None

  override protected def beforeAll() {
    super.beforeAll()

    implicit def string2ByteArray(s: String): Array[Byte] = Bytes.toBytes(s)

    testTable = Some(testUtil.createTable("VehicleViolationRecordStoreTest".getBytes, vehicleViolationColumnFamily))
  }

  val additionalJsonFormats = new EnumerationSerializer(VehicleCode, ProcessingIndicator,
    ConfigType)

  val td1 = makeVVR("test1", 1000000, "ALPHA1", "NL", 142, 120, VehicleCode.PA, ProcessingIndicator.Automatic)
  val td2 = makeVVR("test2", 2000000, "BETA12", "NL", 142, 120, VehicleCode.PA, ProcessingIndicator.Automatic)
  val td3 = makeVVR("test3", 2000000, "BETA13", "NL", 142, 120, VehicleCode.PA, ProcessingIndicator.Automatic)

  "VehicleViolationRecordStore" must {
    "read nothing from empty table" in {
      HBaseReaderImpl.readRows((0, ""), (Long.MaxValue, "")) must be('empty)
    }

    "write a single record" in {
      HBaseWriterImpl.writeRow(td1, td1)
    }

    "read a single record in range" in {
      HBaseReaderImpl.readRows((0, ""), (Long.MaxValue, "")) must be(List(td1))
    }

    "write multiple records" in {
      HBaseWriterImpl.writeRows(Seq((td2, td2), (td3, td3)))
    }

    "read multiple records in range" in {
      HBaseReaderImpl.readRows((0, ""), (Long.MaxValue, "")).toSet must be(Set(td1, td2, td3))
    }

    "read a single specified record" in {
      HBaseReaderImpl.readRow(2000000, "BETA12") must be(Some(td2))
    }

    "return None when trying to read a record using a key that does not exist" in {
      HBaseReaderImpl.readRow(2000000, "BETAXX") must be(None)
    }
  }

  override protected def afterAll() {
    testTable.foreach(_.close())
    super.afterAll()
  }

  def makeKey(keyValue: vehicleViolationRecordType): Array[Byte] =
    makeKey(keyValue.classifiedRecord.speedRecord.exit.get.eventTimestamp, keyValue.classifiedRecord.speedRecord.exit.get.license.get.value)

  def makeKey(keyValue: (Long, String)): Array[Byte] = {
    Bytes.add(Bytes.toBytes(keyValue._1), Bytes.toBytes(keyValue._2))
  }

  object HBaseReaderImpl extends JsonHBaseReader[(Long, String), vehicleViolationRecordType] {
    val rowKeyDistributer = new NoDistributionOfRowKey()
    val hbaseReaderTable = testTable.get
    val hbaseReaderDataColumnFamily = vehicleViolationColumnFamily
    val hbaseReaderDataColumnName = vehicleViolationColumnName
    def makeReaderKey(keyValue: (Long, String)) = makeKey(keyValue)
    override def jsonReaderFormats = super.jsonReaderFormats + additionalJsonFormats
  }

  object HBaseWriterImpl extends JsonHBaseWriter[vehicleViolationRecordType, vehicleViolationRecordType] {
    val rowKeyDistributer = new NoDistributionOfRowKey()
    val hbaseWriterTable = testTable.get
    val hbaseWriterDataColumnFamily = vehicleViolationColumnFamily
    val hbaseWriterDataColumnName = vehicleViolationColumnName
    def makeWriterKey(keyValue: vehicleViolationRecordType) = makeKey(keyValue)
    override def jsonWriterFormats = super.jsonWriterFormats + additionalJsonFormats
  }

  def makeVVR(id: String, time: Long, license: String, country: String, measuredSpeed: Int, enforcedSpeed: Int,
              vehicleClass: VehicleCode.Value, processingIndicator: ProcessingIndicator.Value, reason: IndicatorReason = IndicatorReason()) =
    VehicleViolationRecord(id,
      new VehicleClassifiedRecord(
        VehicleSpeedRecord(id,
          VehicleMetadata(Lane("1",
            "lane1",
            "gantry1",
            "route1",
            0.0f,
            0.0f),
            id + "-vmd1",
            time - 60000,
            Some(new Date(time - 60000).toString),
            Some(ValueWithConfidence(license, 42)),
            country = Some(ValueWithConfidence(country, 42)),
            images = Seq(),
            NMICertificate = "",
            applChecksum = "",
            serialNr = "SN1234"),
          Some(VehicleMetadata(Lane("1",
            "lane2",
            "gantry2",
            "route2",
            0.0f,
            0.0f),
            id + "-vmd2",
            time,
            Some(new Date(time).toString),
            Some(ValueWithConfidence(license, 42)),
            country = Some(ValueWithConfidence(country, 42)),
            images = Seq(),
            NMICertificate = "",
            applChecksum = "",
            serialNr = "SN1234")),
          2002, measuredSpeed, "SHA1-foo"),
        Some(vehicleClass),
        processingIndicator,
        false,
        false,
        None,
        false,
        reason),
      enforcedSpeed,
      None,
      recognizeResults = List())
}
