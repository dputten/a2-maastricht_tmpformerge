/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.dynamax

import java.io.File
import java.util.Date

import akka.actor._
import akka.camel.{CamelExtension, Oneway}
import akka.testkit.{TestKit, TestProbe}
import akka.util.duration._
import csc.akkautils.DirectLogging
import csc.curator.CuratorTestServer
import csc.curator.utils.Curator
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.enforce.{CuratorHelper, DynamaxStartup, ZkDynamaxConfig, ZkDynamaxIface}
import csc.sectioncontrol.messages.EsaProviderType
import csc.sectioncontrol.storage._
import net.liftweb.json.DefaultFormats
import org.apache.activemq.camel.component.ActiveMQComponent
import org.apache.curator.retry.RetryUntilElapsed
import org.scalatest.matchers.MustMatchers
import org.scalatest.{BeforeAndAfterAll, WordSpec}

class DynamaxStartupTest extends TestKit(ActorSystem("DynamaxStartupTest")) with WordSpec with MustMatchers
  with CuratorTestServer with BeforeAndAfterAll with DirectLogging {

  implicit val formats = DefaultFormats + new EnumerationSerializer(EsaProviderType, ServiceType, ZkCaseFileType,
    ConfigType)
  val zkPath = "/ctes/systems/"

  val retryPolicy = new RetryUntilElapsed(5000, 100)
  val camelContext = CamelExtension(system).context

  private var storage: Curator = _

  override def beforeEach() {
    val mqdir = new File("activemq-data")
    if (mqdir.exists()) {
      mqdir.delete()
    }

    super.beforeEach()
    storage = CuratorHelper.create(this, clientScope, retryPolicy, formats)

    //add corridors
    createCorridors()
    createConfig()

    camelContext.addComponent("activemqclient", ActiveMQComponent.activeMQComponent("tcp://localhost:6000"))

    Thread.sleep(1000)
  }

  override def afterAll() {
    camelContext.stop()
    //stop actors
    system.shutdown()
    //stop zookeeper
    super.afterAll()
  }

  def createConfig() {
    val cfg = new ZkDynamaxConfig(xsd = "dynamax_hhm.xsd", activeMQComponentName = "activemq", activeMQBroker = "vm:(broker:(tcp://localhost:6000)?persistent=false)")
    storage.put("/ctes/configuration/dynamax/config", cfg)
    val iface = List(new ZkDynamaxIface(id = "MQ1", uri = "activemq:queue:Dynamax", checksumApp = "ABC", checksumCfg = "ABC", timeoutmSec = 10000, maxCacheUse = 60000, reportFile = "/app/dynamax/MQ1"))
    storage.put("/ctes/configuration/dynamax/iface", iface)

  }

  def createCorridors() {
    val sys = new ZkSystem(id = "A2",
      name = "A2",
      title = "A2",
      mtmRouteId = "a200l",
      violationPrefixId = "",
      location = ZkSystemLocation(
        description = "",
        region = "",
        viewingDirection = Some(""),
        roadNumber = "",
        roadPart = "",
        systemLocation = ""),
      serialNumber = SerialNumber(""),
      orderNumber = 0,
      maxSpeed = 0,
      compressionFactor = 0,
      retentionTimes = ZkSystemRetentionTimes(
        nrDaysKeepTrafficData = 0,
        nrDaysKeepViolations = 0,
        nrDaysRemindCaseFiles = 0),
      pardonTimes = ZkSystemPardonTimes(pardonTimeEsa_secs = 5000, pardonTimeTechnical_secs = 5000),
      esaProviderType = EsaProviderType.Dynamax)
    storage.put(zkPath + "A2/config", sys)

    val sys2 = new ZkSystem(id = "A12",
      name = "A12",
      title = "A12",
      mtmRouteId = "a200l",
      violationPrefixId = "",
      location = ZkSystemLocation(
        description = "",
        region = "",
        viewingDirection = Some(""),
        roadNumber = "",
        roadPart = "",
        systemLocation = ""),
      serialNumber = SerialNumber(""),
      orderNumber = 0,
      maxSpeed = 0,
      compressionFactor = 0,
      retentionTimes = ZkSystemRetentionTimes(
        nrDaysKeepTrafficData = 0,
        nrDaysKeepViolations = 0,
        nrDaysRemindCaseFiles = 0),
      pardonTimes = ZkSystemPardonTimes(pardonTimeEsa_secs = 45000, pardonTimeTechnical_secs = 55000),
      esaProviderType = EsaProviderType.Dynamax)
    storage.put(zkPath + "A12/config", sys2)

    val initData = new ZkCorridor(id = "cor1",
      name = "cor1",
      roadType = 1,
      serviceType = ServiceType.SectionControl,
      isInsideUrbanArea = false,
      dynamaxMqId = "MQ1",
      approvedSpeeds = Seq[Int](80, 100),
      pshtm = new ZkPSHTMIdentification(useYear = true,
        useMonth = true,
        useDay = true,
        useReportingOfficerId = false,
        useNumberOfTheDay = false,
        useLocationCode = false,
        useHHMCode = false,
        useTypeViolation = false,
        useFixedCharacter = false,
        fixedCharacter = "B"),
      info = new ZkCorridorInfo(
        corridorId = 100,
        locationCode = "1",
        reportId = "TCS A2 Links  39.7 - 54.9",
        reportHtId = "A020011",
        reportPerformance = true,
        locationLine1 = "line",
        locationLine2 = None),
      startSectionId = "20",
      endSectionId = "40",
      allSectionIds = Seq[String]("20", "40"),
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 402,
      roadCode = 2,
      dutyType = "KA2",
      deploymentCode = "03",
      codeText = "")
    storage.put(zkPath + "A2/corridors/cor1/config", initData)

    val cor2 = initData.copy(id = "cor2", name = "cor2", dynamaxMqId = "MQ2")
    storage.put(zkPath + "A2/corridors/cor2/config", cor2)

    val info = new ZkCorridorInfo(corridorId = 200,
      locationCode = "1",
      reportId = "TCS A2 Links  39.7 - 54.9",
      reportHtId = "A020011",
      reportPerformance = true,
      locationLine1 = "line",
      locationLine2 = None)
    val cor3 = initData.copy(id = "cor3", name = "cor3", dynamaxMqId = "MQ2", info = info)
    storage.put(zkPath + "A12/corridors/cor3/config", cor3)
  }

  "The StartupDynamax" must {

    "start the dynamax actors" in {
      val probe = TestProbe()
      def getActorSystem(systemId: String): Option[ActorSystem] = {
        Some(system)
      }
      def getSpeedLog(systemId: String): ActorRef = {
        probe.ref
      }

      val startDyn = new DynamaxStartup(
        curator = storage,
        system = system,
        zkPathDynamax = "/ctes/configuration/dynamax",
        zkPathSystem = "/ctes/systems",
        baseBackup = 100,
        maxRetries = 50,
        getActorSystem = getActorSystem,
        getSpeedLog = getSpeedLog)

      startDyn.start()
      Thread.sleep(5000)
      val prodRef = system.actorOf(Props[MQProducerClient])
      //create XML
      val update = new SectionSignUpdateMessage(sectionSign = AggregatedSignValue.KM80, techPardonTime_mSecs = 60000, time = new Date(System.currentTimeMillis() / 1000 * 1000))
      val xml = SignUpdateMessageConverter.createXML(update, None, false, 1000, "ABC", "ABC", new Date())

      //send XML to MQueue
      prodRef ! xml
      //initial message has preChangeExclusionTime = 0 and effectiveTimestamp = receive time and not send time
      val recvMsg = probe.expectMsgType[DynamaxSpeedData](10 seconds)
      recvMsg.corridorId must be(100)
      recvMsg.sequenceNumber must be(1000)
      recvMsg.speedSetting must be(Right(Some(update.sectionSign.id)))
      recvMsg.preChangeExclusionTime must be(Some(0))

      val xml2 = SignUpdateMessageConverter.createXML(update, None, false, 1001, "ABC", "ABC", new Date())
      prodRef ! xml2

      val recvMsg2 = new DynamaxSpeedData(effectiveTimestamp = update.time.getTime,
        corridorId = 100,
        sequenceNumber = 1001,
        speedSetting = Right(Some(update.sectionSign.id)),
        preChangeExclusionTime = Some(update.techPardonTime_mSecs))
      probe.expectMsg(10 seconds, recvMsg2)
      startDyn.stop()
    }
  }

}

class MQProducerClient extends Actor with Oneway with ActorLogging {
  def endpointUri = "activemqclient:queue:Dynamax"

  //  override protected def receiveBeforeProduce = {
  //    case msg => {
  //      log.info("Producer send Message: " + msg)
  //      msg
  //    }
  //  }
  //
  //  override protected def receiveAfterProduce =  {
  //    case msg => log.info("Producer Message is send " + msg)
  //  }

}
