The intrada.lic file in this directory contains the 'soft' development license
that can be used to develop and test without installing a dongle.

To use it replace the intrada.lic file in src/config with this version.

When using the image-recog-server the license info have to be added in zookeeper.
For each server a license have to be created, because each server will lock the one it uses.
the licences root node is configurable and default is /ctes/dacolianServer/licenses
Under this parent node the different licenses has to be created.
The content of the nodes is a json object containing the company and key
{"companyId":"CSC", "key":"08SFZ-XFPGY-H92S8-T04SG-U820E"}
The node name doesn't mather.
