/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.image.recognizer.server

import net.liftweb.json.DefaultFormats
import csc.config.Path
import csc.image.recognizer.worker.LicenseKeyData
import csc.akkautils.DirectLoggingAdapter
import csc.curator.utils.Curator

/**
 *
 * @author Maarten Hazewinkel
 */
object ImageServerSetup extends App {
  val log = new DirectLoggingAdapter(this.getClass.getName)
  val curator = Curator.createCuratorWithLogger(None, log, extraFormats = DefaultFormats)

  def saveZk[T <: AnyRef](p: String, v: T) {
    curator.put(Path(p), v)
  }

  val ld1 = LicenseKeyData("CSC", "Key1")
  val ld2 = LicenseKeyData("CSC", "Key2")
  val ld3 = LicenseKeyData("CSC", "Key3")
  val ld4 = LicenseKeyData("CSC", "Key4")

  saveZk("/ctes/dacolianServer/licenses/license1", ld1)
  saveZk("/ctes/dacolianServer/licenses/license2", ld2)
  saveZk("/ctes/dacolianServer/licenses/license3", ld3)
  saveZk("/ctes/dacolianServer/licenses/license4", ld4)
}
