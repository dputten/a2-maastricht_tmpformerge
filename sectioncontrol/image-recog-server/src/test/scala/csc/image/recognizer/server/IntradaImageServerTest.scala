/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.image.recognizer.server

import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import akka.event.Logging
import akka.testkit.{ TestBarrier, TestActorRef }
import akka.actor.{ PoisonPill, Props, ActorSystem }
import akka.util.duration._
import csc.curator.utils.{ CuratorToolsImpl, Curator }
import csc.image.recognizer.worker.LicenseKeyData
import csc.curator.CuratorTestServer

/**
 * Test the image recognizer server license reader
 *
 * @author Maarten Hazewinkel
 */
class IntradaImageServerTest extends WordSpec with MustMatchers
  with CuratorTestServer with BeforeAndAfterAll {

  implicit val testSystem = ActorSystem("IntradaImageServerTest")

  val log = Logging(testSystem, this.getClass)
  private var storage: Curator = _

  "IntradaImageServer" must {
    "Not acquire a license when none is stored" in {
      var foundLicense = false

      val lr1 = TestActorRef(new IntradaImageServer(storage, "/ctes/test/license/path", 10.seconds, "setttings.txt", null) {
        override protected def doWorkWithLicense(license: LicenseKeyData) {
          foundLicense = true
        }
        protected def curator = storage
        protected def licensesPath = "/ctes/test/license/path"
        protected def workInterval = 10.seconds
      })
      lr1 ! Tick
      foundLicense must be(false)
      lr1.stop()
    }

    "Acquire a single license when one is stored" in {
      storage.put("/ctes/test/license/path/l1", LicenseKeyData("com", "key"))
      var foundLicense1 = false
      var foundLicense2 = false
      val license1locked = new TestBarrier(2)
      val license2tried = new TestBarrier(2)
      val lr1 = testSystem.actorOf(Props(new IntradaImageServer(storage, "/ctes/test/license/path", 10.seconds, "setttings.txt", null) {
        override protected def doWorkWithLicense(license: LicenseKeyData) {
          foundLicense1 = true
          license1locked.await() // signal first license locked
          license2tried.await() // wait until lr2 tried to use license
        }
        protected def curator = storage
        protected def licensesPath = "/ctes/test/license/path"
        protected def workInterval = 10.seconds
      }), "lr1")
      val lr2 = TestActorRef(new IntradaImageServer(storage, "/ctes/test/license/path", 10.seconds, "setttings.txt", null) {
        override protected def doWorkWithLicense(license: LicenseKeyData) {
          foundLicense2 = true
        }
        protected def curator = storage
        protected def licensesPath = "/ctes/test/license/path"
        protected def workInterval = 10.seconds
      })
      lr1 ! Tick
      license1locked.await() // wait until license is in use
      lr2 ! Tick
      license2tried.await() // signal lr2 tried to use license
      foundLicense1 must be(true)
      foundLicense2 must be(false)
      lr1 ! PoisonPill
      lr2.stop()
    }

    "Acquire multiple licenses when two are stored" in {
      storage.put("/ctes/test/license/path/l1", LicenseKeyData("com", "key"))
      storage.put("/ctes/test/license/path/l2", LicenseKeyData("com", "key"))
      var foundLicense1 = false
      var foundLicense2 = false
      val license1locked = new TestBarrier(2)
      val license2tried = new TestBarrier(2)
      val lr1 = testSystem.actorOf(Props(new IntradaImageServer(storage, "/ctes/test/license/path", 10.seconds, "setttings.txt", null) {
        override protected def doWorkWithLicense(license: LicenseKeyData) {
          foundLicense1 = true
          license1locked.await() // signal first license locked
          license2tried.await() // wait until lr2 tried to use license
        }
        protected def curator = storage
        protected def licensesPath = "/ctes/test/license/path"
        protected def workInterval = 10.seconds
      }), "lr2")
      val lr2 = TestActorRef(new IntradaImageServer(storage, "/ctes/test/license/path", 10.seconds, "setttings.txt", null) {
        override protected def doWorkWithLicense(license: LicenseKeyData) {
          foundLicense2 = true
        }
        protected def curator = storage
        protected def licensesPath = "/ctes/test/license/path"
        protected def workInterval = 10.seconds
      })
      lr1 ! Tick
      license1locked.await() // wait until license is in use
      lr2 ! Tick
      license2tried.await() // signal lr2 tried to use license
      foundLicense1 must be(true)
      foundLicense2 must be(true)
      lr1 ! PoisonPill
      lr2.stop()
      //give the actors time to shutdown to prevent an java.lang.IllegalStateException
      Thread.sleep(100)
    }
  }
  override def beforeEach() {
    super.beforeEach()
    storage = new CuratorToolsImpl(clientScope, log)
  }
  override def afterAll() {
    testSystem.shutdown()
    super.afterAll()
  }
}
