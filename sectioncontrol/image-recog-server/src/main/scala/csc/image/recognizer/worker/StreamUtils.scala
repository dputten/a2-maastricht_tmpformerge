/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.image.recognizer.worker

import java.io.{ PrintStream, BufferedReader }

/**
 *
 * @author Maarten Hazewinkel
 */
object StreamUtils {
  def input2ChunkIterator(input: BufferedReader): Iterator[String] = {
    def linesChunk(in: BufferedReader, chunk: String = ""): String = {
      val l = in.readLine()
      if (l == null || (l.length == 0 && chunk.length != 0)) {
        chunk
      } else {
        val newChunk = if (l.length > 0) chunk + "\n" + l else chunk
        linesChunk(in, newChunk)
      }
    }

    Iterator.continually(linesChunk(input))
  }

  def writeChunk(chunk: String, output: PrintStream) {
    output.println(chunk)
    output.println()
    output.flush()
  }
}
