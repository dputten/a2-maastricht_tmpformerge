/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.image.recognizer.server

import java.util.concurrent.TimeUnit

import akka.actor.{ ActorLogging, Actor, Cancellable }
import akka.util.Duration

import org.apache.curator.framework.recipes.locks.{ InterProcessSemaphoreV2, Lease }

import csc.curator.utils.Curator
import csc.image.recognizer.worker.{ InitIntradaRequest, LicenseKeyData }

/**
 * Acquirers a lock on one of a limited number of available licenses in zookeeper periodically,
 * and allows subclasses to do work with that license.
 *
 * The the actor tries to acquirer a license once every minute. If successful it allows subclasses
 * to do work with that license. Once that work is complete, the license is released back into the
 * pool.
 *
 * @author Maarten Hazewinkel
 */
class IntradaImageServer(curator: Curator,
                         licensesPath: String,
                         workInterval: Duration,
                         settingsFile: String,
                         processor: ProcessImage) extends Actor with ActorLogging {

  protected def receive = {
    case Tick ⇒
      if (lockedLicense.isEmpty) {
        lockedLicense = obtainLicense()
      }
      lockedLicense.foreach {
        case LockedLicense(license, _, _) ⇒
          try {
            stopSchedule()
            doWorkWithLicense(license)
          } finally {
            startSchedule()
            releaseLicense()
            lockedLicense = None
          }
      }
  }

  private var lockedLicense: Option[LockedLicense] = None

  /**
   * Do any required work with the license. To be implemented by concrete subclasses.
   *
   * @param license The license acquired for this work.
   */
  protected def doWorkWithLicense(license: LicenseKeyData) {
    log.info("Intrada Worker companyId: %s, licenseKey: %s".
      format(license.companyId, license.key))
    processor.processQueueWithInit(InitIntradaRequest(license, settingsFile))
  }

  // Obtain the license information from the Queue??
  private def obtainLicense(): Option[LockedLicense] = {
    curator.curator.flatMap { zk ⇒
      val allLicenses = curator.getChildren(licensesPath)
      allLicenses.foldLeft(None: Option[LockedLicense]) { (foundLicense, path) ⇒
        if (foundLicense.isDefined) {
          foundLicense
        } else {
          val lock = new InterProcessSemaphoreV2(zk, path, 1)
          val lease = lock.acquire(100, TimeUnit.MILLISECONDS)
          try {
            if (lease != null) {
              val license = curator.get[LicenseKeyData](path)
              license.flatMap(ld ⇒ Some(LockedLicense(ld, lock, lease)))
            } else {
              None
            }
          } catch {
            case e: Exception ⇒
              log.error(e, "exception while locking {}", path)
              None
          }
        }
      }
    }
  }

  private def releaseLicense() {
    lockedLicense.foreach(ll ⇒ ll.lock.returnLease(ll.lease))
    lockedLicense = None
  }

  private var tickSchedule: Option[Cancellable] = None

  private def startSchedule() {
    tickSchedule = Some(context.system.scheduler.schedule(workInterval, workInterval, self, Tick))
  }

  private def stopSchedule() {
    tickSchedule.foreach(_.cancel())
    tickSchedule = None
  }

  override def preStart() {
    super.preStart()
    startSchedule()
  }

  override def postStop() {
    try {
      stopSchedule()

      if (lockedLicense.isDefined) {
        releaseLicense()
      }
    } finally {
      super.postStop()
    }
  }

  private case class LockedLicense(license: LicenseKeyData, lock: InterProcessSemaphoreV2, lease: Lease)
}

