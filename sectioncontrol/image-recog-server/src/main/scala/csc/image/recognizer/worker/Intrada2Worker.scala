/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.image.recognizer.worker

import java.io.{ InputStreamReader, BufferedReader }
import imagefunctions.intrada.impl.IntradaFunctionsImpl
import net.liftweb.json.{ DefaultFormats, Serialization }
import imagefunctions.LicensePlate
import imagefunctions.intrada.{ INTRADA_CONFIG, IntradaFunctions }
import csc.akkautils.DirectLogging

/**
 * Process image recognition requests in a separate process.
 *
 * @author Maarten Hazewinkel
 */
object Intrada2Worker extends App with DirectLogging {
  implicit val jsonFormats = DefaultFormats

  log.info("Intrada2 Worker started")

  val inputs = StreamUtils.input2ChunkIterator(new BufferedReader(new InputStreamReader(System.in)))

  val initReq = Serialization.read[InitIntrada2Request](inputs.next())

  log.info("Receive an Init request [{}]", initReq)

  val processor = try {
    new Intrada2ImageProcessor(initReq.contractCountries)
  } catch {
    case e: Exception ⇒
      log.error("Intrada2 Worker initializing failed %s".format(e.getMessage))
      StreamUtils.writeChunk(Serialization.write(InitResponse(false)), System.out)
      log.error("Intrada2 Worker initializing failed %s".format(e.getMessage))
      sys.exit(1)
  }

  StreamUtils.writeChunk(Serialization.write(InitResponse(true)), System.out)
  log.info("Intrada2 Worker initialized")

  try {
    inputs.foreach { chunk ⇒
      if (chunk != "") {
        val req = Serialization.read[Intrada2ProcessRequest](chunk)
        log.debug("Intrada2 Worker start processing %s".format(req))
        val result = processor.readImages(req)
        StreamUtils.writeChunk(Serialization.write(result), System.out)
        log.debug("Intrada2 Worker done processing %s".format(result))
      } else {
        log.info("Intrada2 Worker received empty message. => exit")
        processor.shutdown()
        sys.exit(0)
      }
    }
  } catch {
    case t: Throwable ⇒ {
      StreamUtils.writeChunk(t.getMessage, System.out) //make sure the server side unlock from read
      StreamUtils.writeChunk(t.getMessage, System.err) //add error to error stream
      log.error("Intrada2 Worker error: %s".format(t.getMessage))
    }
  }
  log.info("Intrada2 Worker id Done => exit")
  processor.shutdown()
  sys.exit(0)
}

class Intrada2ImageProcessor(contractCountries: String) extends DirectLogging {
  log.info("Init the IntradaCSC library ...")

  private val intrada: IntradaFunctions = IntradaFunctionsImpl.getCSCInstance(contractCountries)

  log.info("IntradaCSC library initialized")

  def readImages(request: Intrada2ProcessRequest): ImageProcessResponse = {
    log.info("Process the recognition request [{}]", request)

    try {
      val licensePlate = intrada.imagesToIntrada2(request.entryFileName,
        request.entryLicense, request.entryCountry, request.entryConfidence,
        request.exitFileName,
        request.exitLicense, request.exitCountry, request.exitConfidence)

      val license = LicensePlateData(license = licensePlate.getPlateNumber,
        fullLicense = licensePlate.getRawPlateNumber,
        licenseConfidence = licensePlate.getPlateConfidence,
        country = licensePlate.getPlateCountryCode,
        countryConfidence = licensePlate.getPlateCountryConfidence)

      ImageProcessResponse(request.requestId, licensePlate = Some(license), errorMessage = None)
    } catch {
      case e: Exception ⇒
        ImageProcessResponse(request.requestId, licensePlate = None, errorMessage = Some(e.toString))
    }
  }

  def shutdown() {
    intrada.shutdown()
  }

  def createConfig(company: String, dacKey: String, settingsFile: String): INTRADA_CONFIG.ByReference = {
    val cfg = new INTRADA_CONFIG.ByReference

    val bytes = (company + "\0").getBytes()
    var i: Int = 0
    bytes.foreach(byte ⇒ {
      cfg.company(i) = byte
      i += 1
    })
    cfg.dacOptions(0) = 83.toByte
    cfg.dacOptions(1) = 0.toByte

    val bytesKey = (dacKey + "\0").getBytes()
    i = 0
    bytesKey.foreach(byte ⇒ {
      cfg.dacKey(i) = byte
      i += 1
    })

    val bytesFile = (settingsFile + "\0").getBytes()
    i = 0
    bytesFile.foreach(byte ⇒ {
      cfg.settingFile(i) = byte
      i += 1
    })
    cfg
  }
}
