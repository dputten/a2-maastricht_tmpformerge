/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.image.recognizer.worker

import akka.event.LoggingAdapter
import java.io.{ File, PrintStream, InputStreamReader, BufferedReader }
import net.liftweb.json.{ Serialization, DefaultFormats }
import io.Source
import akka.dispatch.{ Await, ExecutionContext, Future }
import akka.util.Duration

class IntradaWorkerClient(initRequest: InitIntradaRequest,
                          val timeoutRecognizeProcess: Duration,
                          logAdapter: LoggingAdapter,
                          implicit val ex: ExecutionContext) extends WorkerClient {

  def log = logAdapter

  def createWorkerProcess(): Process = {
    val homeDir = System.getProperty("akka.home")
    val file = new File(homeDir, "/bin/startIntradaWorker")
    log.debug("Start {}", file.getAbsolutePath)
    Runtime.getRuntime.exec(file.getAbsolutePath)
  }

  def getInitRequest: InitImageRequest = {
    initRequest
  }
}

class Intrada2WorkerClient(initRequest: InitIntrada2Request,
                           val timeoutRecognizeProcess: Duration,
                           logAdapter: LoggingAdapter,
                           implicit val ex: ExecutionContext) extends WorkerClient {

  def log = logAdapter

  def createWorkerProcess(): Process = {
    val homeDir = System.getProperty("akka.home")
    val file = new File(homeDir, "/bin/startIntrada2Worker")
    log.debug("Start {}", file.getAbsolutePath)
    Runtime.getRuntime.exec(file.getAbsolutePath)
  }

  def getInitRequest: InitImageRequest = {
    initRequest
  }
}

class ArhWorkerClient(initRequest: InitArhRequest,
                      val timeoutRecognizeProcess: Duration,
                      logAdapter: LoggingAdapter,
                      implicit val ex: ExecutionContext) extends WorkerClient {
  def log = logAdapter

  def createWorkerProcess(): Process = {
    val homeDir = System.getProperty("akka.home")
    val file = new File(homeDir, "/bin/startArhWorker")
    log.debug("Start {}", file.getAbsolutePath)
    Runtime.getRuntime.exec(file.getAbsolutePath)
  }

  def getInitRequest: InitImageRequest = {
    initRequest
  }
}

/**
 *
 * @author Maarten Hazewinkel
 */
abstract class WorkerClient() {
  val timeoutRecognizeProcess: Duration
  implicit val ex: ExecutionContext

  def log: LoggingAdapter

  def getInitRequest: InitImageRequest

  def createWorkerProcess(): Process

  val homeDir = System.getProperty("akka.home")
  val workerProcess = createWorkerProcess()
  val inputStreamReader = new InputStreamReader(workerProcess.getInputStream)
  val bufferedReader = new BufferedReader(inputStreamReader)

  val results = StreamUtils.input2ChunkIterator(bufferedReader)
  val requests = new PrintStream(workerProcess.getOutputStream)

  implicit val jsonFormats = DefaultFormats

  StreamUtils.writeChunk(logOut(Serialization.write(getInitRequest)), requests)
  val (initResponse, error) = try {
    (Serialization.read[InitResponse](logIn(results.next())), "")
  } catch {
    case e: Exception ⇒
      log.error(e, "Worker process initialization failed")
      val errorOut = if (!isWorkerRunning) {
        readErrorStream()
      }
      bufferedReader.close()
      (InitResponse(false), e.getMessage + "\n" + errorOut)
  }

  if (!initResponse.initOk) {
    throw new IllegalStateException("Worker process did not initialize correctly: " + error)
  }

  /*
   Send the image to the recognizer (Intrada/ARH), wait for the answer, process the answer
   */
  def processImage(requestId: String, imageFileName: String, options: Map[String, String]): ImageProcessResponse = {
    log.info("Send ImageProcessRequest with requestId [%s] to the Intrada Recognizer.".format(requestId))
    sendRequest(ImageProcessRequest(imageFileName, requestId, options))
  }

  /*
  Send the image to the INTRADA recognizer (Interface 2), wait for the answer, process the answer
  */
  def processImages(requestId: String,
                    entryFileName: String, entryLicense: String, entryCountry: String, entryConfidence: Int,
                    exitFileName: String, exitLicense: String, exitCountry: String, exitConfidence: Int): ImageProcessResponse = {

    log.info("Send Intrada2ProcessRequest with requestId [%s] to the Intrada Recognizer.".format(requestId))

    val imageRequest = Intrada2ProcessRequest(entryFileName, entryLicense, entryCountry, entryConfidence,
      exitFileName, exitLicense, exitCountry, exitConfidence,
      requestId)
    sendRequest(imageRequest)
  }

  private def oldSendRequest(request: AnyRef): ImageProcessResponse = {
    StreamUtils.writeChunk(logOut(Serialization.write(request)), requests)

    val (imageResponse, error) = try {
      val future = Future {
        Serialization.read[ImageProcessResponse](logIn(results.next()))
      }
      val response = Await.result[ImageProcessResponse](future, timeoutRecognizeProcess)
      (response, "")
    } catch {
      case e: Exception ⇒
        log.error("Reading Recognition result is problem [%s]".format(e.toString))
        val errorOut = if (!isWorkerRunning) {
          readErrorStream()
        }
        (null, e.getMessage + "\n" + errorOut)
    }

    if (imageResponse == null) {
      throw new IllegalStateException("Worker process failed: " + error)
    }

    imageResponse
  }

  private def sendRequest(request: AnyRef): ImageProcessResponse = {

    def doRequest(request: AnyRef): Either[Exception, ImageProcessResponse] = {
      StreamUtils.writeChunk(logOut(Serialization.write(request)), requests)
      val future = Future {
        logIn(results.next())
      }
      try {
        val responseText = Await.result[String](future, timeoutRecognizeProcess)
        try {
          val imageProcessResponse = Serialization.read[ImageProcessResponse](responseText)
          Right(imageProcessResponse)
        } catch {
          case e: Exception ⇒ {
            log.error(e, "Error serializing response '%s'".format(responseText))
            Left(e)
          }
        }
      } catch {
        case e: Exception ⇒ {
          log.error(e, "Error retrieving result from Future")
          Left(e)
        }
      }
    }

    val exceptionOrResponse = doRequest(request)
    exceptionOrResponse.fold({
      (e: Exception) ⇒
        {
          val errorOut = if (!isWorkerRunning) readErrorStream() else ""
          val errorText = e.getMessage + "\n" + errorOut

          throw new IllegalStateException("Worker process failed: " + errorText)
        }
    }, {
      (r: ImageProcessResponse) ⇒ r
    })
  }

  def isWorkerRunning: Boolean = {
    try {
      workerProcess.exitValue()
      false
    } catch {
      case e: IllegalThreadStateException ⇒ true
    }
  }

  def readErrorStream(): String = {
    Source.createBufferedSource(workerProcess.getErrorStream).getLines().foldLeft("")(_ + _)
  }

  def logOut(data: String): String = {
    log.debug("Sending: " + data)
    data
  }

  def logIn(data: String): String = {
    log.debug("Receiving: " + data)
    data
  }

  def stop() {
    if (isWorkerRunning) {
      //empty line should trigger the worker to stop
      StreamUtils.writeChunk(logOut(Serialization.write("")), requests)
      Thread.sleep(1000)
      //if worker isn't stopped destroy process
      if (isWorkerRunning) {
        workerProcess.destroy()
      }
    }
    try {
      requests.close()
    } finally {
      try {
        bufferedReader.close()
        workerProcess.getInputStream.close()
      } finally {
        workerProcess.getErrorStream.close()
      }
    }
  }
}
