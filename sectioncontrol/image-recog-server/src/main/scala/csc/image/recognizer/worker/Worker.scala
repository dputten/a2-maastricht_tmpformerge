/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.image.recognizer.worker

/* Intrada one image recognition interface */
case class ImageProcessRequest(imageFileName: String, requestId: String, options: Map[String, String])

/*
 Special intrada interface for CSC
 */
case class Intrada2ProcessRequest(entryFileName: String, entryLicense: String, entryCountry: String, entryConfidence: Int,
                                  exitFileName: String, exitLicense: String, exitCountry: String, exitConfidence: Int,
                                  requestId: String)

case class ImageProcessResponse(requestId: String, licensePlate: Option[LicensePlateData], errorMessage: Option[String] = None)

trait InitImageRequest

case class InitIntradaRequest(license: LicenseKeyData, settingsFile: String) extends InitImageRequest {
  require(settingsFile.size < 200, "Path to the settingsFile is too long %d (max 200)".format(settingsFile.size))
}

case class InitIntrada2Request(contractCountries: String) extends InitImageRequest

case class InitArhRequest(ARH_nrHandles: Int,
                          ARH_engineModule: String,
                          ARH_engineGroup: String,
                          lastLicense: Boolean) extends InitImageRequest

case class InitResponse(initOk: Boolean)

case class LicenseKeyData(companyId: String, key: String)

case class LicensePlateData(license: String, fullLicense: String, licenseConfidence: Int,
                            country: String, countryConfidence: Int)

object RecognizeOptions {
  val arhModule = "arh.module"
  val arhGroup = "arh.group"
  val arhLowestLicence = "arh.license.lowest"
  val arhCreateLicenseSnippet = "arh.license.snippet"
  val arhPostFixLicenseSnippet = "arh.license.postfix"
  val intradaSettingsFile = "intrada.settings"
  val intradaPostFixLicenseSnippet = "intrada.license.postfix"
  val intradaCreateLicenseSnippet = "intrada.license.snippet"
}

