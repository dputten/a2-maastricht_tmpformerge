/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.image.recognizer.worker

import java.io.{ InputStreamReader, BufferedReader }
import imagefunctions.intrada.impl.IntradaFunctionsImpl
import net.liftweb.json.{ DefaultFormats, Serialization }
import imagefunctions.LicensePlate
import imagefunctions.intrada.{ INTRADA_CONFIG, IntradaFunctions }
import csc.akkautils.DirectLogging

/**
 * Process image recognition requests in a separate process.
 *
 * @author Maarten Hazewinkel
 */
object IntradaWorker extends App with DirectLogging {
  implicit val jsonFormats = DefaultFormats
  log.info("Intrada Worker started")

  val inputStreamReader = new InputStreamReader(System.in)
  val bufferedReader = new BufferedReader(inputStreamReader)
  val inputs: Iterator[String] = StreamUtils.input2ChunkIterator(bufferedReader)

  val initReq = Serialization.read[InitIntradaRequest](inputs.next())
  log.info("Start initializing with %s".format(initReq))
  val processor = try {
    new IntradaImageProcessor(initReq.license.companyId, initReq.license.key)
  } catch {
    case e: Exception ⇒
      StreamUtils.writeChunk(Serialization.write(InitResponse(false)), System.out)
      log.error("Intrada Worker initializing failed %s".format(e.getMessage))
      sys.exit(1)
  }

  StreamUtils.writeChunk(Serialization.write(InitResponse(true)), System.out)
  log.info("Intrada Worker initialized")

  try {
    inputs.foreach { chunk ⇒
      if (chunk != "") {
        val req = Serialization.read[ImageProcessRequest](chunk)
        log.debug("Intrada Worker start processing %s".format(req))
        val result = processor.readImage(req)
        StreamUtils.writeChunk(Serialization.write(result), System.out)
        log.debug("Intrada Worker done processing %s".format(result))
      } else {
        log.info("Intrada Worker received empty message. => exit")
        processor.shutdown()
        sys.exit(0)
      }
    }
  } catch {
    case t: Throwable ⇒ {
      StreamUtils.writeChunk(t.getMessage, System.out) //make sure the server side unlock from read
      StreamUtils.writeChunk(t.getMessage, System.err) //add error to error stream
      log.error("Intrada Worker error: %s".format(t.getMessage))
    }
  } finally {
    bufferedReader.close()
  }
  log.info("Intrada Worker id Done => exit")
  processor.shutdown()
  sys.exit(0)
}

class IntradaImageProcessor(company: String, key: String) {
  private val intrada: IntradaFunctions = IntradaFunctionsImpl.getDefaultInstance(company, key)

  def readImage(request: ImageProcessRequest): ImageProcessResponse = {
    try {
      val options = request.options
      //get the Intrada supported options
      val settingFile = options.get(RecognizeOptions.intradaSettingsFile).getOrElse(null)
      val strOpt = options.get(RecognizeOptions.intradaCreateLicenseSnippet)

      val createSnippet = strOpt.map(_.toLowerCase == "true").getOrElse(false)
      val snippetFile = if (createSnippet) {
        request.imageFileName + options.get(RecognizeOptions.intradaPostFixLicenseSnippet).getOrElse("_plate_intrada.tif")
      } else {
        null
      }

      val licensePlate = intrada.imageToIntrada(request.imageFileName, snippetFile, settingFile)

      val license = LicensePlateData(license = licensePlate.getPlateNumber,
        fullLicense = licensePlate.getRawPlateNumber,
        licenseConfidence = licensePlate.getPlateConfidence,
        country = licensePlate.getPlateCountryCode,
        countryConfidence = licensePlate.getPlateCountryConfidence)

      val error = licensePlate.getPlateStatus match {
        case LicensePlate.PLATESTATUS_PLATE_FOUND        ⇒ None
        case LicensePlate.PLATESTATUS_INVALID_IMAGE_DESC ⇒ Some("Invalid image desc")
        case LicensePlate.PLATESTATUS_POOR_GREY_IMAGE    ⇒ Some("Poor grey image")
        case LicensePlate.PLATESTATUS_NO_PLATE_FOUND     ⇒ Some("No plate found")
      }

      ImageProcessResponse(request.requestId, licensePlate = Some(license), errorMessage = error)
    } catch {
      case e: Exception ⇒
        ImageProcessResponse(request.requestId, licensePlate = None, errorMessage = Some(e.toString))
    }
  }
  def shutdown() {
    intrada.shutdown()
  }

  def createConfig(company: String, dacKey: String, settingsFile: String): INTRADA_CONFIG.ByReference = {
    val cfg = new INTRADA_CONFIG.ByReference

    val bytes = (company + "\0").getBytes()
    var i: Int = 0
    bytes.foreach(byte ⇒ {
      cfg.company(i) = byte
      i += 1
    })
    cfg.dacOptions(0) = 83.toByte
    cfg.dacOptions(1) = 0.toByte

    val bytesKey = (dacKey + "\0").getBytes()
    i = 0
    bytesKey.foreach(byte ⇒ {
      cfg.dacKey(i) = byte
      i += 1
    })

    val bytesFile = (settingsFile + "\0").getBytes()
    i = 0
    bytesFile.foreach(byte ⇒ {
      cfg.settingFile(i) = byte
      i += 1
    })
    cfg
  }
}
