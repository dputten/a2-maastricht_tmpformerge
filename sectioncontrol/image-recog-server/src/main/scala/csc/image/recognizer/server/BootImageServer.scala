/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.image.recognizer.server

import akka.util.duration._
import akka.util.Duration

import akka.actor._
import akka.actor.SupervisorStrategy.Restart

import org.apache.hadoop.conf.Configuration
import org.apache.curator.retry.ExponentialBackoffRetry

import com.typesafe.config.Config

import csc.akkautils._
import csc.curator.utils.Curator
import csc.hbase.utils.HBaseTableKeeper

import csc.image.recognizer.client.RecognizeQueuePaths
import csc.image.recognizer.worker.InitArhRequest

/**
 * Boot the matcher actors.
 *
 * @author Maarten Hazewinkel
 */
class BootImageServer extends GenericBoot {
  def componentName = "Recognize-Server"
  protected def zkSessionTimeout: Int = 500
  protected def retryPolicy = new ExponentialBackoffRetry(zkSessionTimeout, 10)

  var hbaseConfig: Option[Configuration] = None

  /**
   * Launch DacolianServer
   */
  def startupActors() {
    val config = actorSystem.settings.config

    if (verifyRequiredConfigPaths(config)) {
      val zkServerQuorum = config.getString(ImageServer.zkServerQuorumConfigPath)
      val hbaseZkServerQuorum = if (config.hasPath(ImageServer.hbaseZkServerQuorumConfigPath)) {
        config.getString(ImageServer.hbaseZkServerQuorumConfigPath)
      } else {
        zkServerQuorum
      }
      val workQueuePath = config.getString(ImageServer.workQueuePathConfigPath)
      val workInterval = config.getLong(ImageServer.workIntervalConfigPath)
      val recognizeTimeout = config.getLong(ImageServer.recognizeTimeout).seconds

      val curator = Curator.createCurator(caller = this, system = actorSystem, zkServers = zkServerQuorum)

      hbaseConfig = Some(HBaseTableKeeper.createCachedConfig(hbaseZkServerQuorum))
      log.info("image-recog-server config: {}", config)
      log.info("image-recog-server hbaseConfig: {}", hbaseConfig.get)

      /*
      Start Recognition Servers for every Recognizer
       */
      val enableIntrada = config.getBoolean(ImageServer.enableIntrada)

      if (enableIntrada) {
        log.info("Worker for Intrada Interface 1 initialized")
        val licensesPath = config.getString(ImageServer.licensesPathConfigPath)
        val settingsFile = config.getString(ImageServer.settingsFile)

        actorSystem.actorOf(Props(new ImageServer(createIntrada(curator,
          hbaseConfig.get,
          licensesPath,
          workQueuePath,
          workInterval.seconds,
          recognizeTimeout,
          settingsFile))), ImageServer.serverIntrada)
      }

      val enableIntrada2 = config.getBoolean(ImageServer.enableIntrada2)

      if (enableIntrada2) {
        log.info("Worker for Intrada Interface 2 initialized")
        val licensesPath = config.getString(ImageServer.licensesPathConfigPath)

        actorSystem.actorOf(Props(new ImageServer(createIntrada2(curator,
          hbaseConfig.get,
          licensesPath,
          workQueuePath,
          workInterval.seconds,
          recognizeTimeout))), ImageServer.serverIntrada2)
      }

      val enableARH = config.getBoolean(ImageServer.enableArh)

      if (enableARH) {
        log.info("Worker for ARH Interface initialized")
        val module = config.getString(ImageServer.arhModule)
        val group = config.getString(ImageServer.arhGroup)
        val handles = config.getInt(ImageServer.arhHandle)
        val lookLastLic = config.getBoolean(ImageServer.arhLast)

        val init = new InitArhRequest(ARH_nrHandles = handles,
          ARH_engineModule = module,
          ARH_engineGroup = group,
          lastLicense = lookLastLic)
        actorSystem.actorOf(Props(new ImageServer(createArh(curator,
          hbaseConfig.get,
          workQueuePath,
          workInterval.seconds,
          init,
          recognizeTimeout))), ImageServer.serverArh)
      }

      if (!(enableIntrada || enableARH || enableIntrada2)) {
        log.error("At least one recognizer need to be enabled")
      }
    } else {
      log.error("required key(s) not defined in config")
    }
  }

  private def verifyRequiredConfigPaths(config: Config): Boolean = {
    import ImageServer._
    def configHasPath(path: String): Boolean = {
      if (config.hasPath(path)) {
        true
      } else {
        log.info("required key {} not defined in config", path)
        false
      }
    }
    List(zkServerQuorumConfigPath, licensesPathConfigPath, workIntervalConfigPath, workQueuePathConfigPath, enableIntrada, enableArh).
      foldRight(true)((path, allOk) ⇒ allOk && configHasPath(path))
  }

  /**
   * Implemented by subclasses to perform an orderly shutdown of the actors in the
   * actor system.
   */
  def shutdownActors() {
    actorSystem.actorFor(ImageServer.intradaProcessorPath) ! PoisonPill
    actorSystem.actorFor(ImageServer.arhProcessorPath) ! PoisonPill
    hbaseConfig.foreach(_.clear())
  }

  def createIntrada(curator: Curator,
                    hbaseConfig: Configuration,
                    licensesPath: String,
                    workQueuePath: String,
                    workInterval: Duration,
                    recognizeTimeout: Duration,
                    settingsFile: String): Props = {
    val imageProcessor = new ProcessImage(workQueuePath = workQueuePath,
      curator = curator,
      hbaseConfig = hbaseConfig,
      pathExtensionTodo = RecognizeQueuePaths.pathExtensionTodoIntrada,
      recognizeTimeout = recognizeTimeout,
      log = log)
    Props(new IntradaImageServer(curator = curator,
      licensesPath = licensesPath,
      workInterval = workInterval,
      settingsFile = settingsFile,
      processor = imageProcessor))
  }

  def createIntrada2(curator: Curator,
                     hbaseConfig: Configuration,
                     licensesPath: String,
                     workQueuePath: String,
                     workInterval: Duration,
                     recognizeTimeout: Duration): Props = {
    val imageProcessor = new ProcessIntrada2Images(workQueuePath = workQueuePath,
      curator = curator,
      hbaseConfig = hbaseConfig,
      pathExtensionTodo = RecognizeQueuePaths.pathExtensionTodoIntrada2,
      recognizeTimeout = recognizeTimeout,
      log = log)
    Props(new Intrada2ImageServer(curator = curator,
      licensesPath = licensesPath,
      workInterval = workInterval,
      processor = imageProcessor))
  }
  def createArh(curator: Curator,
                hbaseConfig: Configuration,
                workQueuePath: String,
                workInterval: Duration,
                init: InitArhRequest,
                recognizeTimeout: Duration): Props = {
    val imageProcessor = new ProcessImage(workQueuePath = workQueuePath,
      curator = curator,
      hbaseConfig = hbaseConfig,
      pathExtensionTodo = RecognizeQueuePaths.pathExtensionTodoARH,
      recognizeTimeout = recognizeTimeout,
      log = log)
    Props(new ArhImageServer(init = init,
      workInterval = workInterval,
      processor = imageProcessor))

  }

}

/**
 * Dacolian image processing server.
 * Supervises the actual image processor.
 *
 */
class ImageServer(startProcessor: Props) extends Actor with ActorLogging {

  val processor = context.actorOf(startProcessor, "RecognizeProcessor")
  context.watch(processor)

  protected def receive = {
    case Terminated(child) ⇒
      log.info("Processor terminated. Terminating ImageServer.")
      context.stop(self)

    case m ⇒
      log.info("ImageServer received {}", m)
  }

  /**
   * Limited supervision strategy for now.
   * Extend as required once more experience with the server shows like error conditions.
   */
  override val supervisorStrategy = OneForOneStrategy(10, 10.seconds) {
    case e: Exception ⇒
      log.error(e, "Restarting child processor after exception")
      Restart
  }
}

object ImageServer {
  private[server] val zkServerQuorumConfigPath = "imageServer.zkServers"
  private[server] val hbaseZkServerQuorumConfigPath = "imageServer.hbaseZkServers"
  private[server] val workIntervalConfigPath = "imageServer.workIntervalSeconds"
  private[server] val recognizeTimeout = "imageServer.recognizeTimeout"
  private[server] val licensesPathConfigPath = "imageServer.intrada.licensesPath"
  private[server] val settingsFile = "imageServer.intrada.settingsFile"

  private[server] val workQueuePathConfigPath = "imageServer.workQueuePath"
  private[server] val enableIntrada = "imageServer.intrada.enable"

  /* Activation of the specific CSC Intrada interface */
  private[server] val enableIntrada2 = "imageServer.intrada.enable2"

  private[server] val enableArh = "imageServer.arh.enable"
  private[server] val arhModule = "imageServer.arh.module"
  private[server] val arhGroup = "imageServer.arh.group"
  private[server] val arhHandle = "imageServer.arh.handles"
  private[server] val arhLast = "imageServer.arh.lookForLast"

  private[server] val serverIntrada = "IntradaServer"

  private[server] val serverIntrada2 = "IntradaServer2"

  private[server] val serverArh = "ArhServer"

  private[server] val intradaProcessorPath = "/user/" + serverIntrada
  private[server] val intrada2ProcessorPath = "/user/" + serverIntrada2
  private[server] val arhProcessorPath = "/user/" + serverArh
}

object Tick
