/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.image.recognizer.server

import java.io.File
import java.util.concurrent.Executors

import org.apache.commons.io.FileUtils
import org.apache.commons.codec.binary.Hex

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.hbase.client.{ Get, HTable }

import akka.dispatch.ExecutionContext
import akka.event.LoggingAdapter
import akka.util.Duration

import csc.image.recognizer.client.{ RecognizeQueuePaths, ImageResponse, ImageRequest }
import csc.image.recognizer.util.{ QueueWriterImpl, StoreInProcess, QueueReader }
import csc.image.recognizer.worker._
import csc.hbase.utils.HBaseTableKeeper
import csc.sectioncontrol.messages.ValueWithConfidence
import csc.image.recognizer.worker.InitIntradaRequest
import csc.curator.utils.Curator

/**
 * Process image recognition requests from a zookeeper queue.
 *
 * @author Maarten Hazewinkel
 */
class ProcessImage(workQueuePath: String,
                   val curator: Curator,
                   hbaseConfig: Configuration,
                   pathExtensionTodo: String,
                   recognizeTimeout: Duration,
                   log: LoggingAdapter) extends QueueReader[ImageRequest, InitImageRequest]
  with HBaseTableKeeper
  with StoreInProcess[ImageRequest, InitImageRequest] {
  val ec = ExecutionContext.fromExecutorService(Executors.newCachedThreadPool())

  protected val queueReaderPath = workQueuePath + pathExtensionTodo
  protected val inProcessPath = workQueuePath + RecognizeQueuePaths.pathExtensionInProcess

  private lazy val queueWriter = new QueueWriterImpl[ImageResponse](curator.curator, workQueuePath + RecognizeQueuePaths.pathExtensionDone)

  private var worker: Option[WorkerClient] = None

  /**
   * Called for each item retrieved from the queue.
   * Reads the image from HBase, processes it, and writes the results to the 'done' queue in zookeeper.
   *
   * @param imageRequest the image processing request
   */
  protected def processItem(imageRequest: ImageRequest, init: InitImageRequest) {
    val response = readImage(imageRequest) match {
      case Some(imageData) ⇒
        val imageFile = new File(FileUtils.getTempDirectory, makeFilename(imageRequest.requestId) + ".jpg")
        log.info("trying to process {} in file {}", imageRequest, imageFile.getAbsolutePath)
        try {
          FileUtils.writeByteArrayToFile(imageFile, imageData)
          processImage(imageFile, imageRequest.requestId, init, imageRequest.systemId, imageRequest.options)
        } catch {
          case ex: Exception ⇒ {
            log.error(ex, "Failed to process {}: {}", imageRequest, ex.getMessage)
            throw ex
          }
        } finally {
          FileUtils.deleteQuietly(imageFile)
        }
      case None ⇒
        ImageResponse(imageRequest.systemId, imageRequest.requestId, None, None, None, Some("Could not read image data from HBase"))
    }
    log.debug("response is {}", response)

    queueWriter.writeItem(response)
  }

  private def makeFilename(name: String): String = name.replaceAll("[^-_.A-Za-z0-9]", "_")

  private def processImage(imageFile: File, requestId: String, init: InitImageRequest, systemId: String, options: Map[String, String]): ImageResponse = {
    val workerClient = getWorkerClient(init)

    val result = workerClient.processImage(requestId, imageFile.getAbsolutePath, options)
    ImageResponse(systemId,
      requestId,
      result.licensePlate.map(lp ⇒ ValueWithConfidence(lp.license, lp.licenseConfidence)),
      result.licensePlate.map(lp ⇒ lp.fullLicense),
      result.licensePlate.map(lp ⇒ ValueWithConfidence(lp.country, lp.countryConfidence)),
      result.errorMessage)
  }

  /**
   * Load the image data from hbase
   */
  private def readImage(request: ImageRequest): Option[Array[Byte]] = {
    val table = new HTable(hbaseConfig, request.hbaseTable)
    addTable(table)
    try {
      val get = new Get(Hex.decodeHex(request.hbaseRowKeyHex.toCharArray))
      val columnFamily = Bytes.toBytes(request.hbaseColumnFamily)
      val columnQualifier = Bytes.toBytes(request.hbaseColumnQualifier)
      get.addColumn(columnFamily, columnQualifier)
      val result = table.get(get)
      if (!result.isEmpty) {
        Some(result.getValue(columnFamily, columnQualifier))
      } else {
        None
      }
    } finally {
      closeTables()
    }
  }

  private def getWorkerClient(init: InitImageRequest): WorkerClient = {
    worker match {
      case None ⇒ {
        val client = init match {
          case intrada: InitIntradaRequest ⇒ new IntradaWorkerClient(intrada, recognizeTimeout, log, ec)
          case arh: InitArhRequest         ⇒ new ArhWorkerClient(arh, recognizeTimeout, log, ec)
        }
        worker = Some(client)
        client
      }
      case Some(client) ⇒ client
    }
  }

  /**
   * Start processing the images
   *
   * @param init The initialization acquired for this work.
   */
  def processQueueWithInit(init: InitImageRequest) {
    log.debug("processing queue items with initialization {}", init)
    try {
      worker.foreach(client ⇒ {
        log.warning("Worker still exists ")
        client.stop()
        worker = None
      })
      processQueueItems(init)
    } catch {
      case e: Exception ⇒
        log.error(e, "Failure in image processing: {}", e.getMessage)
    } finally {
      worker.foreach(_.stop())
      worker = None
      log.debug("Done processing queue items with initialization {}", init)
    }
  }
}
