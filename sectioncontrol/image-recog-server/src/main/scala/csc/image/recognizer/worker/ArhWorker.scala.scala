/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.image.recognizer.worker

import java.io.{ File, InputStreamReader, BufferedReader }
import net.liftweb.json.{ DefaultFormats, Serialization }
import imagefunctions.LicensePlate
import csc.akkautils.DirectLogging
import imagefunctions.arh.impl.ArhFunctionsImpl
import org.slf4j.LoggerFactory

/**
 * Process image recognition requests in a separate process.
 *
 */
object ArhWorker extends App with DirectLogging {
  implicit val jsonFormats = DefaultFormats
  log.info("ARH Worker started")
  val inputs = StreamUtils.input2ChunkIterator(new BufferedReader(new InputStreamReader(System.in)))

  val initReq = Serialization.read[InitArhRequest](inputs.next())

  val processor = try {
    new ArhImageProcessor(initReq.ARH_nrHandles, initReq.ARH_engineModule, initReq.ARH_engineGroup, initReq.lastLicense)
  } catch {
    case e: Exception ⇒
      StreamUtils.writeChunk(Serialization.write(InitResponse(false)), System.out)
      log.error("ARH Worker initializing failed %s".format(e.getMessage))
      sys.exit(1)
  }
  StreamUtils.writeChunk(Serialization.write(InitResponse(true)), System.out)
  log.info("ARH Worker initialized")

  try {
    inputs.foreach { chunk ⇒
      if (chunk != "") {
        val req = Serialization.read[ImageProcessRequest](chunk)
        log.debug("ARH Worker start processing %s".format(req))
        val result = processor.readImage(req)
        StreamUtils.writeChunk(Serialization.write(result), System.out)
        log.debug("ARH Worker done processing %s".format(result))
      } else {
        log.info("ARH Worker received empty message. => exit")
        processor.shutdown()
        sys.exit(0)
      }
    }
  } catch {
    case t: Throwable ⇒ {
      StreamUtils.writeChunk(t.getMessage, System.out) //make sure the server side unlock from read
      StreamUtils.writeChunk(t.getMessage, System.err) //add error to error stream
      log.error("ARH Worker error: %s".format(t.getMessage))
    }
  }
  log.info("ARH Worker id Done => exit")
  processor.shutdown()
  sys.exit(0)
}

class ArhImageProcessor(ARH_nrHandles: Int,
                        ARH_engineModule: String,
                        ARH_engineGroup: String,
                        lastLicense: Boolean) {
  private val log = LoggerFactory.getLogger(getClass)
  private val arhInstance = ArhFunctionsImpl.getInstance(ARH_nrHandles,
    ARH_engineModule,
    ARH_engineGroup,
    lastLicense, true)

  def readImage(request: ImageProcessRequest): ImageProcessResponse = {
    try {
      //get the ARH supported options
      val options = request.options
      val module = options.get(RecognizeOptions.arhModule).getOrElse(null)
      val group = options.get(RecognizeOptions.arhGroup).getOrElse(null)
      val lastLicence = getBoolean(options, RecognizeOptions.arhLowestLicence, false)
      val createSnippet = getBoolean(options, RecognizeOptions.arhCreateLicenseSnippet, true)
      val snippetFile = if (createSnippet) {
        val imageFile = new File(request.imageFileName)
        imageFile.getAbsolutePath + options.get(RecognizeOptions.arhPostFixLicenseSnippet).getOrElse("_plate_arh.tif")
      } else {
        null
      }

      log.debug("Start recognize " + request.imageFileName)
      val licensePlate = arhInstance.jpgToArh(request.imageFileName, snippetFile, lastLicence, module, group)

      val license = LicensePlateData(license = licensePlate.getPlateNumber,
        fullLicense = licensePlate.getRawPlateNumber,
        licenseConfidence = licensePlate.getPlateConfidence,
        country = licensePlate.getPlateCountryCode,
        countryConfidence = licensePlate.getPlateCountryConfidence)

      log.debug("Recognize result for %s: %s".format(request.imageFileName, license))

      val error = licensePlate.getPlateStatus match {
        case LicensePlate.PLATESTATUS_PLATE_FOUND        ⇒ None
        case LicensePlate.PLATESTATUS_INVALID_IMAGE_DESC ⇒ Some("Invalid image desc")
        case LicensePlate.PLATESTATUS_POOR_GREY_IMAGE    ⇒ Some("Poor grey image")
        case LicensePlate.PLATESTATUS_NO_PLATE_FOUND     ⇒ Some("No plate found")
      }

      ImageProcessResponse(request.requestId, licensePlate = Some(license), errorMessage = error)
    } catch {
      case e: Exception ⇒
        log.error("Recognize Exception", e)
        ImageProcessResponse(request.requestId, licensePlate = None, errorMessage = Some(e.toString))
    }
  }
  def shutdown() {
  }
  private def getBoolean(options: Map[String, String], option: String, default: Boolean): Boolean = {
    val strOpt = options.get(option)
    strOpt.map(_.toLowerCase == "true").getOrElse(default)
  }

}
