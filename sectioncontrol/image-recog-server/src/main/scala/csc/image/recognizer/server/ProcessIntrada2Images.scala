/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.image.recognizer.server

import org.apache.hadoop.hbase.util.Bytes
import org.apache.commons.io.FileUtils
import java.io.File
import org.apache.hadoop.hbase.client.{ Get, HTable }
import org.apache.commons.codec.binary.Hex
import csc.image.recognizer.client.RecognizeQueuePaths
import csc.image.recognizer.util.{ QueueWriterImpl, StoreInProcess, QueueReader }
import csc.image.recognizer.worker._
import csc.hbase.utils.HBaseTableKeeper
import org.apache.hadoop.conf.Configuration
import akka.event.LoggingAdapter
import akka.dispatch.ExecutionContext
import akka.util.Duration
import java.util.concurrent.Executors
import csc.curator.utils.Curator
import csc.sectioncontrol.messages.ValueWithConfidence
import csc.image.recognizer.worker.InitArhRequest
import csc.image.recognizer.client.ImageResponse
import csc.image.recognizer.client.ImagesIntrada2Request
import csc.image.recognizer.worker.InitIntrada2Request

/**
 * Process images recognition requests from a zookeeper queue.
 * Send the recognition request to the Intrada 2 Interface
 *
 * @author Danny van der Putten
 */
class ProcessIntrada2Images(workQueuePath: String,
                            val curator: Curator,
                            hbaseConfig: Configuration,
                            pathExtensionTodo: String,
                            val log: LoggingAdapter,
                            recognizeTimeout: Duration) extends QueueReader[ImagesIntrada2Request, InitImageRequest]
  with HBaseTableKeeper
  with StoreInProcess[ImagesIntrada2Request, InitImageRequest] {
  val ec = ExecutionContext.fromExecutorService(Executors.newCachedThreadPool())

  protected val queueReaderPath = workQueuePath + pathExtensionTodo
  protected val inProcessPath = workQueuePath + RecognizeQueuePaths.pathExtensionInProcess

  private lazy val queueWriter = new QueueWriterImpl[ImageResponse](curator.curator, workQueuePath + RecognizeQueuePaths.pathExtensionDone)

  private var worker: Option[WorkerClient] = None

  /**
   * Called for each item retrieved from the queue.
   * Reads the images from HBase, processes it, and writes the results to the 'done' queue in zookeeper.
   * The Item must be processed or an exception has to be thrown. This will cause the item to be put back on the queue
   *
   * @param imageRequest the image processing request
   */
  protected def processItem(imageRequest: ImagesIntrada2Request, init: InitImageRequest) {

    log.info("Got an image request for ARH license: " + imageRequest.entryARHLic)

    /* Get the entry image from Hbase and write in file */
    val entryImage = readImage(imageRequest, 1) match {
      case Some(imageData) ⇒
        val imageFile = new File(FileUtils.getTempDirectory, makeFilename(imageRequest.requestId) + "_entry.jpg")
        log.debug("Trying to process {} in file {}", imageRequest, imageFile.getAbsolutePath)
        try {
          FileUtils.writeByteArrayToFile(imageFile, imageData)

        } catch {
          case ex: Exception ⇒ {
            log.error("Failed to process {}: {}", imageRequest, ex.getMessage)
            throw ex
          }
        }
        Some(imageFile)

      case None ⇒
        log.warning("Could not find image for request: " + imageRequest)

        None
    }

    /* Get the exit image from Hbase and write in file */
    val exitImage = readImage(imageRequest, 2) match {
      case Some(imageData) ⇒
        val imageFile = new File(FileUtils.getTempDirectory, makeFilename(imageRequest.requestId) + "_exit.jpg")
        log.debug("trying to process {} in file {}", imageRequest, imageFile.getAbsolutePath)
        try {
          FileUtils.writeByteArrayToFile(imageFile, imageData)

        } catch {
          case ex: Exception ⇒ {
            log.error("Failed to process {}: {}", imageRequest, ex.getMessage)
            throw ex
          }
        }
        Some(imageFile)

      case None ⇒
        None

    }

    /*
     Got the two images, send all the Intrada recognizer (interface 2)
     */
    try {
      val response = (entryImage, exitImage) match {
        case (Some(image1), Some(image2)) ⇒ {
          log.info("Send the Images to the intrada2 interface ...")
          val contractCountries = imageRequest.contractCountries.mkString(",")
          val response = processImages(image1, image2, imageRequest, InitIntrada2Request(contractCountries))
          log.info("Recognition response is {}. Put the response in the queue", response)
          response
        }
        case (Some(image1), None) ⇒ {
          val msg = "No exit image. No recognition"
          log.warning(msg + ": Put the error response in the queue")
          ImageResponse(imageRequest.systemId, imageRequest.requestId,
            None, None, None, Some(msg))
        }
        case (None, Some(image2)) ⇒ {
          val msg = "No entry image. No recognition"
          log.warning(msg + ": Put the error response in the queue")
          ImageResponse(imageRequest.systemId, imageRequest.requestId,
            None, None, None, Some(msg))
        }
        case (None, None) ⇒ {
          val msg = "No entry and exit image. No recognition"
          log.warning(msg + ": Put the error response in the queue")
          ImageResponse(imageRequest.systemId, imageRequest.requestId,
            None, None, None, Some(msg))
        }
      }
      queueWriter.writeItem(response)
      log.info("Response written in 'done' queue")
    } catch {
      case e: Exception ⇒ {
        log.error("ProcessImages exception: [%s]".format(e.toString))
        throw e
      }
    } finally {
      entryImage.foreach(image ⇒ {
        log.info("Delete image %s".format(image))
        FileUtils.deleteQuietly(image)
      })

      exitImage.foreach(image ⇒ {
        log.info("Delete image %s".format(image))
        FileUtils.deleteQuietly(image)
      })
    }
  }

  private def makeFilename(name: String): String = name.replaceAll("[^-_.A-Za-z0-9]", "_")

  private def processImages(entryImage: File,
                            exitImage: File,
                            request: ImagesIntrada2Request,
                            init: InitImageRequest): ImageResponse = {
    log.info("Start the Worker client Intrada2")
    val workerClient = getWorkerClient(init)

    val result = workerClient.processImages(request.requestId,
      entryImage.getAbsolutePath, request.entryARHLic, request.entryARHCountry, request.entryARHConfidence,
      exitImage.getAbsolutePath, request.exitARHLic, request.exitARHCountry, request.exitARHConfidence)

    ImageResponse(request.systemId, request.requestId,
      result.licensePlate.map(lp ⇒ ValueWithConfidence(lp.license, lp.licenseConfidence)),
      result.licensePlate.map(lp ⇒ lp.fullLicense),
      result.licensePlate.map(lp ⇒ ValueWithConfidence(lp.country, lp.countryConfidence)),
      result.errorMessage)
  }

  /**
   * Load a specific images data (entry/exit) from hbase
   */
  private def readImage(request: ImagesIntrada2Request, imageNumber: Int): Option[Array[Byte]] = {
    val table = new HTable(hbaseConfig, request.hbaseTable)
    log.debug("TABLE " + table.getName)
    log.info("request: {}", request)
    addTable(table)
    try {
      val get = if (imageNumber == 1)
        new Get(Hex.decodeHex(request.hbaseEntryImageRowKeyHex.toCharArray))
      else
        new Get(Hex.decodeHex(request.hbaseExitImageRowKeyHex.toCharArray))

      val columnFamily = Bytes.toBytes(request.hbaseColumnFamily)

      val columnQualifier = Bytes.toBytes(request.hbaseColumnQualifier)
      get.addColumn(columnFamily, columnQualifier)

      val result = table.get(get)

      if (!result.isEmpty) {
        Some(result.getValue(columnFamily, columnQualifier))
      } else {
        log.warning("No image bytes found for " + request)
        None
      }
    } finally {
      closeTables()
    }
  }

  private def getWorkerClient(init: InitImageRequest): WorkerClient = {
    worker match {
      case None ⇒ {
        val client = init match {
          case intrada: InitIntrada2Request ⇒ new Intrada2WorkerClient(intrada, recognizeTimeout, log, ec)
          case arh: InitArhRequest          ⇒ new ArhWorkerClient(arh, recognizeTimeout, log, ec)
        }
        worker = Some(client)
        client
      }
      case Some(client) ⇒ client
    }
  }

  /**
   * Start processing the images
   *
   */
  def processQueue() {
    log.debug("processing queue items")
    try {
      worker.foreach(client ⇒ {
        log.warning("Worker still exists ")
        client.stop()
        worker = None
      })
      processQueueItems(new InitImageRequest {})
    } catch {
      case e: Exception ⇒
        log.error(e, "Failure in image processing: {}", e.getMessage)
    } finally {
      worker.foreach(_.stop())
      worker = None
      log.debug("Done processing queue items with initialization")
    }
  }
}
