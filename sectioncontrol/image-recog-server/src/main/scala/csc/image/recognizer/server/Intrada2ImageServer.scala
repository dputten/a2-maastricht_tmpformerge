/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.image.recognizer.server

import akka.actor.{ ActorLogging, Actor, Cancellable }
import java.util.concurrent.TimeUnit
import akka.util.Duration
import csc.image.recognizer.worker.{ InitIntrada2Request, LicenseKeyData }
import csc.curator.utils.Curator

/**
 * Acquirers a lock on one of a limited number of available licenses in zookeeper periodically,
 * and allows subclasses to do work with that license.
 *
 * The the actor tries to acquirer a license once every minute. If successful it allows subclasses
 * to do work with that license. Once that work is complete, the license is released back into the
 * pool.
 *
 * Note Danny: Only the contract countries are added. The other actions like getting the licenses, scheduling, etc.
 * are still in place, because I have no idea what will happen when remove this from the code.
 *
 * @author Danny van der Putten
 */
class Intrada2ImageServer(curator: Curator,
                          licensesPath: String,
                          workInterval: Duration,
                          processor: ProcessIntrada2Images) extends Actor with ActorLogging {

  def receive = {
    case Tick ⇒ {
      try {
        stopSchedule()
        processor.processQueue()
      } finally {
        startSchedule()
      }
    }
  }

  private var tickSchedule: Option[Cancellable] = None

  private def startSchedule() {
    tickSchedule = Some(context.system.scheduler.schedule(workInterval, workInterval, self, Tick))
  }

  private def stopSchedule() {
    tickSchedule.foreach(_.cancel())
    tickSchedule = None
  }

  override def preStart() {
    super.preStart()
    startSchedule()
  }

  override def postStop() {
    try {
      stopSchedule()
    } finally {
      super.postStop()
    }
  }
}

