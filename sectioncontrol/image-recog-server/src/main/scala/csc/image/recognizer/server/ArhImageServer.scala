/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.image.recognizer.server

import akka.actor.{ Cancellable, Actor }
import akka.util.Duration
import csc.image.recognizer.worker.InitArhRequest

class ArhImageServer(init: InitArhRequest,
                     workInterval: Duration,
                     processor: ProcessImage) extends Actor {
  def receive = {
    case Tick ⇒ {
      try {
        stopSchedule()
        processor.processQueueWithInit(init)
      } finally {
        startSchedule()
      }
    }
  }
  private var tickSchedule: Option[Cancellable] = None

  private def startSchedule() {
    tickSchedule = Some(context.system.scheduler.schedule(workInterval, workInterval, self, Tick))
  }

  private def stopSchedule() {
    tickSchedule.foreach(_.cancel())
    tickSchedule = None
  }

  override def preStart() {
    super.preStart()
    startSchedule()
  }

  override def postStop() {
    try {
      stopSchedule()
    } finally {
      super.postStop()
    }
  }

}