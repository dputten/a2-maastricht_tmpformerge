/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package sectioncontrol

import sbt._

object CDH5 {
  object V {
    val HBase     = "0.98.1-cdh5.1.2"
    val Hadoop    = "2.3.0-cdh5.1.2"
    val Zookeeper = "3.4.5-cdh5.1.2"  // Zookeeper version included from CDH4 implicitly
    //val Zookeeper = "3.4.6"  // Zookeeper version included from CDH4 implicitly
  }

  val hadoopCommon         = ("org.apache.hadoop"    % "hadoop-common"        % V.Hadoop //classifier "" classifier "tests"
    exclude ("hsqldb", "hsqldb")
    exclude ("net.sf.kosmosfs", "kfs")
    exclude ("org.eclipse.jdt", "core")
    exclude ("net.java.dev.jets3t", "jets3t")
    exclude ("oro", "oro")
    exclude ("jdiff", "jdiff")
    exclude ("org.apache.lucene", "lucene-core")
    exclude ("org.slf4j", "slf4j-api")
    exclude ("org.slf4j", "slf4j-log4j12")
    )

  val hadoopHdfs     = ("org.apache.hadoop"    % "hadoop-hdfs"          % V.Hadoop //classifier "" classifier "tests"
    exclude ("hsqldb", "hsqldb")
    exclude ("net.sf.kosmosfs", "kfs")
    exclude ("org.eclipse.jdt", "core")
    exclude ("net.java.dev.jets3t", "jets3t")
    exclude ("oro", "oro")
    exclude ("jdiff", "jdiff")
    exclude ("org.apache.lucene", "lucene-core")
    exclude ("org.slf4j", "slf4j-api")
    exclude ("org.slf4j", "slf4j-log4j12"))

  val hadoopAuth = ("org.apache.hadoop"    % "hadoop-auth"          % V.Hadoop)
  val hadoopMRClientJobClient  = "org.apache.hadoop"    % "hadoop-mapreduce-client-jobclient"          % V.Hadoop classifier "tests"

  val hbaseClient       = "org.apache.hbase"     % "hbase-client"         % V.HBase //classifier "" classifier "tests" //exclude ("org.apache.zookeeper", "zookeeper")
  val hbaseCommon       = "org.apache.hbase"     % "hbase-common"         % V.HBase //classifier "" classifier "tests" //exclude ("org.apache.zookeeper", "zookeeper")
  val hbaseCompat2      = "org.apache.hbase"     % "hbase-hadoop2-compat" % V.HBase //classifier "" classifier "tests" //exclude ("org.apache.zookeeper", "zookeeper")
  val hbaseCompat       = "org.apache.hbase"     % "hbase-hadoop-compat"  % V.HBase //classifier "" classifier "tests" //exclude ("org.apache.zookeeper", "zookeeper")
  val hbaseServer       = "org.apache.hbase"     % "hbase-server"         % V.HBase //classifier "" classifier "tests" //exclude ("org.apache.zookeeper", "zookeeper")
  val hbasePrefixTree   = "org.apache.hbase"     % "hbase-prefix-tree"    % V.HBase //classifier "" classifier "tests" //exclude ("org.apache.zookeeper", "zookeeper")
  val hbaseThrift       = "org.apache.hbase"     % "hbase-thrift"         % V.HBase //classifier "tests" //exclude ("org.apache.zookeeper", "zookeeper")
  val zookeeper         = "org.apache.zookeeper" % "zookeeper"            % V.Zookeeper //classifier "" classifier "tests"

  val deps = Seq(hadoopCommon, hadoopHdfs, hadoopMRClientJobClient, hbaseClient, hbaseCommon, hbaseCompat2, hbaseCompat, hbaseServer
            /*, hbasePrefixTree, hbaseThrift, zookeeper*/)

  val unmanagedTestLibs = "src/main/lib/cdh5"

  // Test
  /*
  object Test {
    val hadoop            = "org.apache.hadoop"    % "hadoop-common"        % V.Hadoop  % "test"
    val hadoopHdfs        = "org.apache.hadoop"    % "hadoop-hdfs"          % V.Hadoop  % "test"
    val hbaseCommon       = "org.apache.hbase"     % "hbase-common"         % V.HBase   % "test"
    val hbaseCompat2      = "org.apache.hbase"     % "hbase-hadoop2-compat" % V.HBase   % "test"
    val hbaseCompat       = "org.apache.hbase"     % "hbase-hadoop-compat"  % V.HBase   % "test"
    val hbaseTestingUtil  = "org.apache.hbase"     % "hbase-testing-util"   % V.HBase   % "test"
    val hbaseProtocol     = "org.apache.hbase"     % "hbase-protocol"       % V.HBase   % "test"

    val deps = Seq(hadoop, hadoopHdfs, hadoopAuth, hbaseCommon, hbaseCompat2, hbaseCompat, hbaseProtocol, hbaseTestingUtil)
  }
*/
}
