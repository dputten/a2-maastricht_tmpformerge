/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package sectioncontrol

import sbt._

object CDH4 {
  object V {
    val HBase     = "0.92.0-cdh4b1"
    val Hadoop    = "0.23.0-cdh4b1"
//  val Zookeeper = "3.4.1-cdh4b1"  // Zookeeper version included from CDH4 implicitly
  }

  val hbase          = ("org.apache.hbase"     % "hbase"                % V.HBase
    exclude ("org.slf4j", "slf4j-api")
    exclude ("org.slf4j", "slf4j-log4j12"))
  val hadoop         = ("org.apache.hadoop"    % "hadoop-common"        % V.Hadoop
    exclude ("hsqldb", "hsqldb")
    exclude ("net.sf.kosmosfs", "kfs")
    exclude ("org.eclipse.jdt", "core")
    exclude ("net.java.dev.jets3t", "jets3t")
    exclude ("oro", "oro")
    exclude ("jdiff", "jdiff")
    exclude ("org.apache.lucene", "lucene-core")
    exclude ("org.slf4j", "slf4j-api")
    exclude ("org.slf4j", "slf4j-log4j12"))
  val hadoopHdfs     = ("org.apache.hadoop"    % "hadoop-hdfs"          % V.Hadoop
    exclude ("hsqldb", "hsqldb")
    exclude ("net.sf.kosmosfs", "kfs")
    exclude ("org.eclipse.jdt", "core")
    exclude ("net.java.dev.jets3t", "jets3t")
    exclude ("oro", "oro")
    exclude ("jdiff", "jdiff")
    exclude ("org.apache.lucene", "lucene-core")
    exclude ("org.slf4j", "slf4j-api")
    exclude ("org.slf4j", "slf4j-log4j12"))
  val hadoopMRCC     = ("org.apache.hadoop"    % "hadoop-mapreduce-client-common"          % V.Hadoop
    exclude ("hsqldb", "hsqldb")
    exclude ("net.sf.kosmosfs", "kfs")
    exclude ("org.eclipse.jdt", "core")
    exclude ("net.java.dev.jets3t", "jets3t")
    exclude ("oro", "oro")
    exclude ("jdiff", "jdiff")
    exclude ("org.apache.lucene", "lucene-core")
    exclude ("org.slf4j", "slf4j-api")
    exclude ("org.slf4j", "slf4j-log4j12"))

  val deps = Seq(hbase, hadoop, hadoopHdfs, hadoopMRCC)

  val unmanagedTestLibs = "src/main/lib/cdh4"
}
