package sectioncontrol

import sbt._
import Keys._
import java.io.File
import sbt.{PathFinder, Task, TaskKey}
import sbt.Project.Initialize
import sbt.CommandSupport._
import java.io.File



object DistWeb {
  val dist = TaskKey[File]("dist", "Builds a web distribution")
  
  def distTask: Initialize[Task[File]] =
    (target, baseDirectory, state) map { (targetDir, baseDir, st) ⇒
      
      val log = logger(st)
      var tarfile = "sectioncontrol-web.tar.gz"
      (("tar -cpzf " + tarfile +" -C " + targetDir.getParent + " " +  "sectioncontrol-web" ) !)
      log.info("Distribution "+targetDir.getParent +"/"+ tarfile+" created.")
      file(tarfile)
    }
}


