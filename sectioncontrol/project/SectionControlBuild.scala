package sectioncontrol

import sbt._
import sbt.Keys._
import com.typesafe.sbtmultijvm.MultiJvmPlugin
import com.typesafe.sbtmultijvm.MultiJvmPlugin.MultiJvm
import com.typesafe.sbtscalariform.ScalariformPlugin
import com.typesafe.sbtscalariform.ScalariformPlugin.ScalariformKeys
import sbt.Package.ManifestAttributes
import sbtassembly.Plugin._

object SectionControlBuild extends Build {

  lazy val IntegrationTest = config("it") extend(Test)

  lazy val tagTitle = {
    val tag = Git.tag
    if(tag.isEmpty) "" else " GITtag: " + tag
  }
  lazy val gitVers = "GITcommit: " + Git.commitHash + tagTitle
  lazy val buildSettings = Seq(
    organization := "com.csc.traffic",
    version      := "6.1.0.1",
    scalaVersion := "2.9.1"
  )
  lazy val packOpts = Seq(ManifestAttributes(
                            ("GIT-Version", gitVers)
                            )
                      )
  lazy val sectioncontrol = Project(
    id = "sectioncontrol",
    base = file("."),
    settings = parentSettings ++ Publish.versionSettings ++ Seq(
      parallelExecution in GlobalScope := System.getProperty("sectioncontrol.parallelExecution", "false").toBoolean,
      Publish.defaultPublishTo in ThisBuild <<= crossTarget / "repository",
      DistWeb.dist <<= DistWeb.distTask
    ),
    aggregate = Seq(messages, matcher, rdwregistry, measure, sign, certify, classify_nl, enforce, enforce_nl, qualify,
      akka_utils, hbase_utils, hbase_testutils, mtm_nl, notification, reports, violationWriter,enforceCommonNl,
      cleanup, destroy,image_recog_server, image_recog_client, monitor, digitalswitch,ntpCheck, tubes, test_mtm,
      storage, auditor, zipexport, tools, managementIface, amqp_utils, fileTransfer, inputEvents,
      registrationReceiver, decorate, testUtils, validationFilter, imageRetriever,checkImage,snmp)
//      registrationReceiver, decorate, testUtils, validationFilter, imageRetriever, checkImage)
   ).configs( IntegrationTest )
    .settings( Defaults.itSettings : _*)
    .settings( libraryDependencies += Dependency.scalatest )

  lazy val ntpCheck = Project(
    id = "sectioncontrol-ntpcheck",
    base = file("sectioncontrol-ntpcheck"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.ntpCheck
    )
  ) dependsOn(akka_utils, messages)

  lazy val checkImage = Project(
    id = "sectioncontrol-check-image",
    base = file("sectioncontrol-check-image"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.checkImage
    )
  ) dependsOn(akka_utils, messages, enforceCommonNl, image_recog_client)

  lazy val snmp = Project(
    id = "sectioncontrol-snmp",
    base = file("sectioncontrol-snmp"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies  ++= Dependencies.snmp
    )
  ) dependsOn(akka_utils)

  lazy val tubes = Project(
    id = "sectioncontrol-tubes",
    base = file("sectioncontrol-tubes"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies  ++= Dependencies.tubes
    )
  ) dependsOn(akka_utils, messages)

  lazy val digitalswitch = Project(
    id = "sectioncontrol-digitalswitch",
    base = file("sectioncontrol-digitalswitch"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.digitalswitch
    )
  ) dependsOn(akka_utils)

  lazy val reports = Project(
    id = "sectioncontrol-reports-nl",
    base = file("sectioncontrol-reports-nl"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.reports
    )
  ).configs( IntegrationTest )
    .settings( Defaults.itSettings : _*)
    .settings( libraryDependencies += Dependency.scalatest ) dependsOn(messages, storage, certify, testUtils, hbase_utils, hbase_testutils % "test")

  lazy val violationWriter= Project(
    id = "sectioncontrol-violationwriter-nl",
    base = file("sectioncontrol-violationwriter-nl"),
    settings = defaultSettings ++ Publish.versionSettings ++ Seq(
      Publish.defaultPublishTo in ThisBuild <<= crossTarget / "repository",
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.violationWriter
    )
  ) dependsOn(messages, enforceCommonNl)

  lazy val enforceCommonNl= Project(
    id = "sectioncontrol-enforcecommon-nl",
    base = file("sectioncontrol-enforcecommon-nl"),
    settings = defaultSettings ++ Publish.versionSettings ++ Seq(
      Publish.defaultPublishTo in ThisBuild <<= crossTarget / "repository",
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.enforceCommonNl
    )
  ) dependsOn(messages, enforce)

  lazy val notification = Project(
    id = "sectioncontrol-notification",
    base = file("sectioncontrol-notification"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.notification
    )
  ) dependsOn(messages, akka_utils)

  lazy val hbase_utils = Project(
    id = "hbase-utils",
    base = file("hbase-utils"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.hbase_utils
    )
  ) dependsOn(hbase_testutils % "test")

  lazy val hbase_testutils = Project(
    id = "hbase-testutils",
    base = file("hbase-testutils"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      unmanagedBase <<= baseDirectory { base => base / Dependencies.unmanagedTestLibs },
      libraryDependencies ++=Dependencies.hbase_testutils
    )
  ).configs( IntegrationTest )
    .settings( Defaults.itSettings : _*)
    .settings( libraryDependencies += Dependency.scalatest )

  lazy val messages = Project(
    id = "sectioncontrol-messages",
    base = file("sectioncontrol-messages"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.messages
    )
  )

  lazy val matcherDistSettings = Seq(packageOptions := packOpts,
    Dist.outputDirectory <<= target / "matcher",
    Dist.bootClass := "csc.sectioncontrol.matcher.Boot") ++ Dist.distSettings
  lazy val matcher = Project(
    id = "sectioncontrol-matcher",
    base = file("sectioncontrol-matcher"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.matcher
    ) ++ matcherDistSettings
  ).configs( IntegrationTest )
    .settings( Defaults.itSettings : _*)
    .settings( libraryDependencies += Dependency.scalatest ) dependsOn(messages, certify, storage, hbase_utils, hbase_testutils % "test", hbase_testutils % "it", akka_utils)

  lazy val fileTransferDistSettings = Seq(packageOptions := packOpts,
    Dist.outputDirectory <<= target / "fileTransfer",
    Dist.bootClass := "com.csc.fileTransfer.FileTransferBoot") ++ Dist.distSettings
  lazy val fileTransfer = Project(
    id = "fileTransfer",
    base = file("fileTransfer"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.fileTransfer
    ) ++ fileTransferDistSettings
  ) dependsOn(akka_utils, messages)


  lazy val cleanupDistSettings = Seq(packageOptions := packOpts,
    Dist.outputDirectory <<= target / "cleanup",
    Dist.bootClass := "csc.sectioncontrol.cleanup.CleanupBoot") ++ Dist.distSettings
  lazy val cleanup = Project(
    id = "sectioncontrol-cleanup",
    base = file("sectioncontrol-cleanup"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.cleanup
    ) ++ cleanupDistSettings
  ).configs( IntegrationTest )
    .settings( Defaults.itSettings : _*)
    .settings( libraryDependencies += Dependency.scalatest ) dependsOn(messages, storage, hbase_utils, hbase_testutils % "test", hbase_testutils % "it", akka_utils)

  lazy val destroyDistSettings = Seq(packageOptions := packOpts,
    Dist.outputDirectory <<= target / "destroy",
    Dist.bootClass := "csc.sectioncontrol.destroy.DestroyBoot") ++ Dist.distSettings
  lazy val destroy = Project(
    id = "sectioncontrol-destroy",
    base = file("sectioncontrol-destroy"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.destroy
    ) ++ destroyDistSettings
  ) dependsOn(messages, storage, akka_utils, hbase_utils, hbase_testutils % "test")

  lazy val rdwregistry = Project(
    id = "sectioncontrol-rdwregistry",
    base = file("sectioncontrol-rdwregistry"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.rdwregistry
    )
  ).configs( IntegrationTest )
    .settings( Defaults.itSettings : _*)
    .settings( libraryDependencies += Dependency.scalatest ) dependsOn(hbase_utils, hbase_testutils % "test", hbase_testutils % "it")

  lazy val classify_nlDistSettings = Seq(packageOptions := packOpts,
    Dist.outputDirectory <<= target / "classify-nl",
    Dist.bootClass := "csc.sectioncontrol.classify.nl.Boot") ++ Dist.distSettings
  lazy val classify_nl = Project(
    id = "sectioncontrol-classify-nl",
    base = file("sectioncontrol-classify-nl"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.classify_nl
    ) ++ classify_nlDistSettings
  ) dependsOn(messages, storage, rdwregistry, hbase_utils, hbase_testutils % "test", akka_utils, amqp_utils)

  lazy val sign = Project(
    id = "sectioncontrol-sign",
    base = file("sectioncontrol-sign"),
    settings = defaultSettings ++ Publish.versionSettings ++ Seq(
      Publish.defaultPublishTo in ThisBuild <<= crossTarget / "repository",
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.sign
    )
  )

  lazy val certify = Project(
    id = "sectioncontrol-certify",
    base = file("sectioncontrol-certify"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.certify
    )
  ) dependsOn(messages, /*zookeeper_utils,*/ akka_utils)

  lazy val measure = Project(
    id = "sectioncontrol-measure",
    base = file("sectioncontrol-measure"),
    settings = defaultSettings ++ Publish.versionSettings ++ Seq(
      Publish.defaultPublishTo in ThisBuild <<= crossTarget / "repository",
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.measure
    )
  )

  lazy val qualify = Project(
    id = "sectioncontrol-qualify",
    base = file("sectioncontrol-qualify"),
    settings = defaultSettings ++ Publish.versionSettings ++ Seq(
      Publish.defaultPublishTo in ThisBuild <<= crossTarget / "repository",
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.qualify
    )
  )

  lazy val mtm_nl = Project(
    id = "sectioncontrol-mtm-nl",
    base = file("sectioncontrol-mtm-nl"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.mtm_nl
    )
  ) dependsOn(messages, storage, notification)

  lazy val test_mtm = Project(
    id = "test-mtm",
    base = file("test-mtm"),
    settings = defaultSettings ++ assemblySettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.mtm_nl
    )
  ) dependsOn(messages, mtm_nl)

  lazy val enforce = Project(
    id = "sectioncontrol-enforce",
    base = file("sectioncontrol-enforce"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.enforce
    )
  ).configs( IntegrationTest )
    .settings( Defaults.itSettings : _*)
    .settings( libraryDependencies += Dependency.scalatest ) dependsOn(messages, storage, hbase_utils, akka_utils, hbase_testutils % "test", hbase_testutils % "it")

  lazy val enforce_nlDistSettings = Seq(packageOptions := packOpts,
    Dist.outputDirectory <<= target / "enforce-nl",
    Dist.bootClass := "csc.sectioncontrol.enforce.nl.Boot") ++ Dist.distSettings
  lazy val enforce_nl = Project(
    id = "sectioncontrol-enforce-nl",
    base = file("sectioncontrol-enforce-nl"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.enforce_nl
    ) ++ enforce_nlDistSettings
  ).configs( IntegrationTest )
    .settings( Defaults.itSettings : _*)
    .settings( libraryDependencies += Dependency.scalatest ) dependsOn(enforce, enforceCommonNl, violationWriter, messages, checkImage, certify, mtm_nl, tubes, image_recog_client, reports, hbase_utils,
    akka_utils, decorate)

  lazy val monitorDistSettings = Seq(packageOptions := packOpts,
    Dist.outputDirectory <<= target / "monitor",
    Dist.bootClass := "csc.sectioncontrol.monitor.Boot") ++ Dist.distSettings

  lazy val monitor = Project(
    id = "sectioncontrol-monitor",
    base = file("sectioncontrol-monitor"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.monitor
    )  ++ monitorDistSettings
  ).configs( IntegrationTest )
    .settings( Defaults.itSettings : _*)
    .settings( libraryDependencies += Dependency.scalatest ) dependsOn(messages,akka_utils, hbase_utils, digitalswitch, ntpCheck, notification, storage, hbase_testutils % "test", hbase_testutils % "it")

  lazy val akka_utils = Project(
    id = "akka-utils",
    base = file("akka-utils"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.akka_utils
    )
  ) // .dependsOn(zookeeper_utils)

  lazy val image_recog_serverDistSettings = Seq(packageOptions := packOpts,
    Dist.outputDirectory <<= target / "image-recog-server",
    Dist.bootClass := "csc.image.recognizer.server.BootImageServer") ++ Dist.distSettings
  lazy val image_recog_server = Project(
    id = "image-recog-server",
    base = file("image-recog-server"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.image_recog_server
    ) ++ image_recog_serverDistSettings
  ) dependsOn(image_recog_client, akka_utils, hbase_utils, hbase_testutils % "test")

  lazy val image_recog_client = Project(
    id = "image-recog-client",
    base = file("image-recog-client"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.image_recog_client
    )
  ) dependsOn(messages, storage, akka_utils, hbase_utils)

  lazy val toolsDistSettings = Seq(packageOptions := packOpts,
    Dist.outputDirectory <<= target / "tools",
    Dist.bootClass := "csc.sectioncontrol.tools.Boot") ++ Dist.distSettings
  lazy val tools = Project(
    id = "sectioncontrol-tools",
    base = file("sectioncontrol-tools"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.tools
    ) ++ toolsDistSettings
  ).configs( IntegrationTest )
    .settings( Defaults.itSettings : _*)
    .settings( libraryDependencies += Dependency.scalatest ) dependsOn(hbase_utils, hbase_testutils % "test", hbase_testutils % "it", messages, storage, decorate)
  
  lazy val storage = Project(
    id = "sectioncontrol-storage",
    base = file("sectioncontrol-storage"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.storage
    )
  ).configs( IntegrationTest )
    .settings( Defaults.itSettings : _*)
    .settings( libraryDependencies += Dependency.scalatest ) dependsOn(messages, akka_utils, hbase_utils, hbase_testutils % "test", hbase_testutils % "it")

  lazy val auditorDistSettings = Seq(packageOptions := packOpts,
    Dist.outputDirectory <<= target / "auditor",
    Dist.bootClass := "csc.sectioncontrol.auditor.Boot") ++ Dist.distSettings

  lazy val auditor = Project(
    id = "sectioncontrol-auditor",
    base = file("sectioncontrol-auditor"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.auditor
    )  ++ auditorDistSettings
  ) dependsOn(amqp_utils, messages,akka_utils, storage)

  lazy val zipExportDistSettings = Seq(packageOptions := packOpts,
    Dist.outputDirectory <<= target / "zipexport",
    Dist.bootClass := "csc.sectioncontrol.zip.Boot") ++ Dist.distSettings

  lazy val zipexport = Project(
    id = "sectioncontrol-zipexport",
    base = file("sectioncontrol-zipexport"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.zipexport
    ) ++ zipExportDistSettings
  ) dependsOn(messages, storage)

  lazy val managementIfaceDistSettings = Seq(packageOptions := packOpts,
    Dist.outputDirectory <<= target / "managementiface",
    Dist.bootClass := "csc.sectioncontrol.management.Boot") ++ Dist.distSettings

  lazy val managementIface = Project(
    id = "sectioncontrol-managementiface",
    base = file("sectioncontrol-managementiface"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.managementIface
    ) ++ managementIfaceDistSettings
  ).configs( IntegrationTest )
    .settings( Defaults.itSettings : _*)
    .settings( libraryDependencies += Dependency.scalatest ) dependsOn(messages, storage, hbase_testutils % "test", hbase_testutils % "it")


  lazy val inputEventsSettings = Seq(packageOptions := packOpts,
    Dist.outputDirectory <<= target / "inputEvents",
    Dist.bootClass := "csc.input_events.Boot") ++ Dist.distSettings

  lazy val inputEvents = Project(
    id = "sectioncontrol-input-events",
    base = file("sectioncontrol-input-events"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.inputEvents
    ) ++ inputEventsSettings
  ) dependsOn(digitalswitch, messages, akka_utils, snmp, amqp_utils)

  lazy val amqp_utils = Project(
    id = "amqp-utils",
    base = file("amqp-utils"),
    settings = defaultSettings ++ Seq(
      autoCompilerPlugins := true,
      // to fix scaladoc generation
      fullClasspath in doc in Compile <<= fullClasspath in Compile,
      libraryDependencies ++=Dependencies.amqp_utils
    )
  ).dependsOn(akka_utils)

  lazy val registrationReceiverSettings = Seq(packageOptions := packOpts,
    Dist.outputDirectory <<= target / "registrationReceiver",
    Dist.bootClass := "csc.registrationreceiver.Boot") ++ Dist.distSettings

  lazy val registrationReceiver = projectOf(
    "sectioncontrol-registration-receiver",
    Dependencies.registrationReceiver, Some(registrationReceiverSettings),
    amqp_utils, akka_utils, messages, storage, certify, hbase_utils, testUtils, hbase_testutils % "test", hbase_testutils % "it")

  lazy val decorate = projectOf(
    "sectioncontrol-decorate",
    Dependencies.decorate, None,
    akka_utils, messages, storage, certify, testUtils)

  lazy val testUtils = projectOf(
    "sectioncontrol-test",
    Dependencies.testUtils, None,
    akka_utils, messages)

  lazy val validationFilterSettings = Seq(packageOptions := packOpts,
    Dist.outputDirectory <<= target / "validationFilter",
    Dist.bootClass := "csc.validationfilter.Boot") ++ Dist.distSettings


  lazy val validationFilter = projectOf(
    "sectioncontrol-validation-filter",
    Dependencies.validationFilter, Some(validationFilterSettings),
    amqp_utils, akka_utils, messages, storage, checkImage, enforceCommonNl, imageRetriever, testUtils, hbase_utils, hbase_testutils % "test", hbase_testutils % "it")

  lazy val imageRetriever = projectOf(
    "sectioncontrol-imageretriever",
    Dependencies.imageRetriever, None,
    amqp_utils, akka_utils, messages, storage, certify, hbase_utils, hbase_testutils % "test")

  def projectOf(name: String, deps1: Seq[ModuleID], extraSettings:Option[Seq[sbt.Setting[_]]], deps2: ClasspathDep[ProjectReference]*): Project =
    Project(
      id = name,
      base = file(name),
      settings = defaultSettings ++ Seq(
        autoCompilerPlugins := true,
        // to fix scaladoc generation
        fullClasspath in doc in Compile <<= fullClasspath in Compile,
        libraryDependencies ++=deps1
      ) ++ extraSettings.getOrElse(Seq[sbt.Setting[_]]())
    ).configs( IntegrationTest )
      .settings( Defaults.itSettings : _*)
      .settings( libraryDependencies += Dependency.scalatest ).dependsOn(deps2:_*)


  //  lazy val scalaCsv = RootProject(uri("git://github.com/tototoshi/scala-csv.git"))

  // Settings

  override lazy val settings = super.settings ++ buildSettings

  lazy val baseSettings = Defaults.defaultSettings ++ Publish.settings

  lazy val parentSettings = baseSettings ++ Seq(
    publishArtifact in Compile := true
  )

  val excludeTestNames = SettingKey[Seq[String]]("exclude-test-names")
  val excludeTestTags = SettingKey[Seq[String]]("exclude-test-tags")
  val includeTestTags = SettingKey[Seq[String]]("include-test-tags")

  val defaultExcludedTags = Seq("timing")

  lazy val defaultSettings = baseSettings ++ formatSettings ++ Seq(
    resolvers += "CSC Repo" at "http://cscappamd911.nl.emea.csc.com:8082/nexus/content/groups/group_sectioncontrol/",
    // compile options
    scalacOptions ++= Seq("-encoding", "UTF-8", "-deprecation", "-unchecked") ++ (
      if (true || (System getProperty "java.runtime.version" startsWith "1.7")) Seq() else Seq("-optimize")), // -optimize fails with jdk7
    javacOptions  ++= Seq("-Xlint:unchecked", "-Xlint:deprecation"),

    ivyLoggingLevel in ThisBuild := UpdateLogging.Quiet,

    parallelExecution in Test := System.getProperty("sectioncontrol.parallelExecution", "false").toBoolean,

    // for excluding tests by name (or use system property: -Dsectioncontrol.test.names.exclude=TimingSpec)
    excludeTestNames := {
      val exclude = System.getProperty("sectioncontrol.test.names.exclude", "")
      if (exclude.isEmpty) Seq.empty else exclude.split(",").toSeq
    },

    // for excluding tests by tag (or use system property: -Dsectioncontrol.tags.exclude=timing)
    excludeTestTags := {
      val exclude = System.getProperty("sectioncontrol.test.tags.exclude", "")
      if (exclude.isEmpty) defaultExcludedTags else exclude.split(",").toSeq
    },

    // for including tests by tag (or use system property: -Dsectioncontrol.test.tags.include=timing)
    includeTestTags := {
      val include = System.getProperty("sectioncontrol.test.tags.include", "")
      if (include.isEmpty) Seq.empty else include.split(",").toSeq
    },

    // add filters for tests excluded by name
    testOptions in Test <++= excludeTestNames map { _.map(exclude => Tests.Filter(test => !test.contains(exclude))) },

    // add arguments for tests excluded by tag - includes override excludes (opposite to scalatest)
    testOptions in Test <++= (excludeTestTags, includeTestTags) map { (excludes, includes) =>
      val tags = (excludes.toSet -- includes.toSet).toSeq
      if (tags.isEmpty) Seq.empty else Seq(Tests.Argument("-l", tags.mkString(" ")))
    },

    // add arguments for tests included by tag
    testOptions in Test <++= includeTestTags map { tags =>
      if (tags.isEmpty) Seq.empty else Seq(Tests.Argument("-n", tags.mkString(" ")))
    },

    // show full stack traces
    testOptions in Test += Tests.Argument("-oF")

  )

  lazy val formatSettings = ScalariformPlugin.scalariformSettings ++ Seq(
    ScalariformKeys.preferences in Compile := formattingPreferences,
    ScalariformKeys.preferences in Test    := formattingPreferences
  )

  def formattingPreferences = {
    import scalariform.formatter.preferences._
    FormattingPreferences()
    .setPreference(RewriteArrowSymbols, true)
    .setPreference(AlignParameters, true)
    .setPreference(AlignSingleLineCaseStatements, true)
  }

  lazy val multiJvmSettings = MultiJvmPlugin.settings ++ inConfig(MultiJvm)(ScalariformPlugin.scalariformSettings) ++ Seq(
    compileInputs in MultiJvm <<= (compileInputs in MultiJvm) dependsOn (ScalariformKeys.format in MultiJvm),
    ScalariformKeys.preferences in MultiJvm := formattingPreferences
  )

}

// Dependencies

object Dependencies {
  import Dependency._
  val CDH = CDH5
  val violationWriter     = Seq(Test.scalatest)
  val enforceCommonNl     = Seq(scQualify,Test.scalatest)
  val reports             = Seq(scalaarm, javaxTrans, javaMail, akkaActor, liftjson, cscDConfig, cscDLog, Test.scalatest, Test.akkaTestKit, Test.mockJavaMail)
  val notification        = Seq(akkaActor, javaxTrans, javaMail, liftjson, cscDConfig, cscDLog, Test.scalatest, Test.akkaTestKit, Test.mockJavaMail)
  val hbase_utils         = CDH.deps ++ Seq(hbasewd, liftjson, Test.scalatest)
  val hbase_testutils     = CDH.deps ++ Seq(liftjson, commonsIo, scalatest)
  val digitalswitch       = Seq(akkaActor, akkaCamel, mina, camelMina, liftjson, cscDConfig, cscDLog, Test.scalatest, Test.akkaTestKit)
  val messages            = Seq(cscDLog, jodaTime, jodaConv, Test.junit, Test.scalatest)
  val measure             = Seq(scSign, Test.scalatest)
  val sign                = Seq(Test.scalatest)
  val certify             = Seq(scSign, akkaActor, liftjson, cscDConfig, akkaSlf4j, slf4jApi, logback, Test.junit, Test.scalatest, Test.akkaTestKit)
  val matcher             = Seq(scMeasure, akkaActor, liftjson, cscDConfig, akkaSlf4j, slf4jApi, logback, Test.junit, Test.scalatest, Test.akkaTestKit)
  val cleanup             = Seq(akkaActor, liftjson, cscDConfig, akkaSlf4j, slf4jApi, logback, Test.junit, Test.scalatest, Test.akkaTestKit)
  val destroy             = Seq(akkaActor, liftjson, cscDConfig, akkaSlf4j, slf4jApi, logback, Test.junit, Test.scalatest, Test.akkaTestKit)
  val monitor             = Seq(akkaActor, liftjson, cscDConfig, cscDLog,akkaSlf4j, slf4jApi, logback, Test.junit, Test.scalatest, Test.akkaTestKit)
  val rdwregistry         = Seq(hbasewd, liftjson, slf4jApi, Test.junit, Test.scalatest)
  val mtm_nl              = Seq(akkaActor, liftjson, cscDConfig, slf4jApi, Test.junit, Test.scalatest, Test.akkaTestKit,Test.mockJavaMail,javaMail)
  val classify_nl         = Seq(hbasewd, amqpClient, akkaActor, liftjson, cscDConfig, akkaSlf4j, slf4jApi, logback, Test.junit, Test.scalatest, Test.akkaTestKit)
  val enforce             = Seq(akkaActor, liftjson, cscDConfig, commonsNet, akkaCamel, activeMQ, activeMQ_camel, Test.junit, Test.scalatest, Test.akkaTestKit)
  val qualify             = Seq(scSign, Test.scalatest)
  val checkImage          = Seq(Test.scalatest)
  val enforce_nl          = Seq(cscWriter,commonsIo, commonsCodec, akkaActor, akkaCamel, mina, camelMina, stream, liftjson, cscDConfig, akkaSlf4j, slf4jApi, logback, Test.junit, Test.scalatest, Test.akkaTestKit)
  val akka_utils          = Seq(akkaActor, akkaKernel, liftjson, cscDConfig, cscDLogActor, Test.scalatest, Test.akkaTestKit)
  val amqp_utils          = Seq(amqpClient, liftjson, scalaarm, javaxTrans, Test.scalatest, Test.akkaTestKit)
  val image_recog_server  = Seq(dacolian, akkaActor, liftjson, cscDConfig, akkaSlf4j, slf4jApi, logback, Test.junit, Test.scalatest, Test.akkaTestKit)
  val image_recog_client  = Seq(akkaActor, hbasewd, liftjson, cscDConfig, akkaSlf4j, slf4jApi, logback, Test.junit, Test.scalatest, Test.akkaTestKit)
  val ntpCheck            = Seq(akkaActor, Test.junit, Test.scalatest, Test.akkaTestKit)
  val docs                = Seq(Test.scalatest, Test.junit)
  val storage             = Seq(hbasewd, liftjson, cscDConfig,Test.scalatest, Test.junit, Test.akkaTestKit)
  val auditor             = Seq(amqpClient, akkaActor, akkaCamel, akkaSlf4j, slf4jApi, logback, stream, camelMina, mina, Test.junit, Test.scalatest, Test.akkaTestKit)
  val zipexport           = Seq(akkaActor, zip4j, liftjson, akkaSlf4j, slf4jApi, logback, Test.junit, Test.scalatest, Test.akkaTestKit)
  val unmanagedTestLibs   = CDH.unmanagedTestLibs
  val managementIface     = Seq(amqpClient, akkaActor, liftjson, scalaarm, javaxTrans, akkaSlf4j, slf4jApi, logback, Test.junit, Test.scalatest, Test.akkaTestKit)
  val fileTransfer        = Seq(amqpClient, akkaActor, liftjson, akkaSlf4j, slf4jApi, logback, commonsIo, Test.junit, Test.scalatest, Test.akkaTestKit)
  val tools               = Seq(akkaAgent, scSign, slf4jApi, logback, Test.junit, Test.scalatest)
  val tubes               = Seq(akkaActor, liftjson, cscDConfig, slf4jApi, scala_csv, Test.junit, Test.scalatest, Test.akkaTestKit)
  val inputEvents         = Seq(amqpClient, akkaActor, akkaCamel, mina, camelMina, liftjson, Test.junit, Test.scalatest, Test.akkaTestKit)
  val registrationReceiver= Seq(akkaActor, akkaKernel, liftjson, Test.scalatest, Test.akkaTestKit)
  val decorate            = Seq(akkaActor, akkaKernel, liftjson, commonImage, exifExtractor, Test.scalatest, Test.akkaTestKit)
  val testUtils           = Seq(akkaActor, akkaKernel, liftjson, Test.scalatest, Test.akkaTestKit)
  val validationFilter    = Seq(akkaActor, akkaKernel, liftjson, Test.scalatest, Test.akkaTestKit)
  val imageRetriever      = Seq(akkaActor, akkaKernel, liftjson, Test.scalatest, Test.akkaTestKit)
  val snmp                = Seq(snmp4j,akkaActor, akkaKernel, logback, scalaarm, javaxTrans, Test.akkaTestKit, Test.scalatest)
}

object Dependency {

  // Versions
  object V {
    val Camel       = "2.8.0"
    val Scalatest   = "1.7.1"
    val Slf4j       = "1.6.4"
    val cscConfig   = "1.3.1"
    val Akka        = "2.0.1-CAMEL-SNAPSHOT"
    val Logback     = "1.0.0"
    val SCMeasure   = "3.0.0.0"
    val SCQualify   = "3.0.1.0"
    val SCSign      = "2.0.0.7"
    val CSCWriter   = "5.0.0.4"
  }

  // Compile
  val akkaKernel    = "com.typesafe.akka"           % "akka-kernel"            % V.Akka       % "compile" // ApacheV2
  val commonsCodec  = "commons-codec"               % "commons-codec"          % "1.4"        // ApacheV2
  val commonsIo     = "commons-io"                  % "commons-io"             % "2.0.1"      // ApacheV2
  val commonsNet    = "commons-net"                 % "commons-net"            % "3.1"        // ApacheV2
  val slf4jApi      = "org.slf4j"                   % "slf4j-api"              % V.Slf4j      // MIT
  val liftjson      = "net.liftweb"                 %% "lift-json"             % "2.4"        // ApacheV2
  val cscDConfig    = "com.csc.traffic"             %% "dconfig"               % V.cscConfig  exclude ("org.apache.hadoop.zookeeper", "zookeeper")  //exclude ("com.google.guava", "guava")  // CSC
  val cscDLog       = "com.csc.traffic"             %% "dlog"                  % V.cscConfig  exclude ("org.apache.hadoop.zookeeper", "zookeeper")  //exclude ("com.google.guava", "guava")   // CSC
  val cscDLogActor  = "com.csc.traffic"             %% "dlog-actor"            % V.cscConfig  exclude ("org.apache.hadoop.zookeeper", "zookeeper")  //exclude ("com.google.guava", "guava")   // CSC
  val akkaActor     = "com.typesafe.akka"           % "akka-actor"             % V.Akka       // ApacheV2
  val akkaAgent     = "com.typesafe.akka"           % "akka-agent"             % V.Akka       // ApacheV2
  val akkaSlf4j     = "com.typesafe.akka"           % "akka-slf4j"             % V.Akka       exclude("org.slf4j","slf4j-log4j12") // ApacheV2
  val akkaCamel     = "com.typesafe.akka"           % "akka-camel"             % V.Akka       // ApacheV2
  val scalatest     = "org.scalatest"               %% "scalatest"             % V.Scalatest  // ApacheV2
  val javaMail      = "javax.mail"                  % "mail"                   % "1.4"        // ApacheV2
  val scalaarm      = "com.jsuereth"                %% "scala-arm"             % "1.2"        // ApacheV2
  val logback       = "ch.qos.logback"              % "logback-classic"        % V.Logback    // EPL V1.0
  val javaxTrans    = "javax.transaction"           % "jta"                    % "1.0.1B"      % "provided->default"
  val activeMQ      = "org.apache.activemq"         % "activemq-core"          % "5.4.1"      // ApacheV2
  val activeMQ_camel= "org.apache.activemq"         % "activemq-camel"         % "5.4.1"      // ApacheV2
  val dacolian      = "com.csc.traffic"             % "imagefunc"              % "2.3.0"      // CSC
  val camelMina     = "org.apache.camel"            % "camel-mina"             % V.Camel      // ApacheV2
  val mina          = "org.apache.mina"             % "mina-core"              % "1.1.7"      // ApacheV2
  val stream        = "org.apache.camel"            % "camel-stream"           % V.Camel       // ApacheV2
  val scSign        = "com.csc.traffic"             %% "sectioncontrol-sign"   % V.SCSign     // CSC
  val scMeasure     = "com.csc.traffic"             %% "sectioncontrol-measure" % V.SCMeasure // CSC
  val scQualify     = "com.csc.traffic"             %% "sectioncontrol-qualify" % V.SCQualify // CSC
  val cscWriter     = "com.csc.traffic"             %% "sectioncontrol-violationwriter-nl" % V.CSCWriter // CSC
  val zip4j         = "net.lingala.zip4j"           % "zip4j"                   % "1.3.1"
  val jodaTime      = "joda-time"                   % "joda-time"               % "2.2"
  val jodaConv      = "org.joda"                    % "joda-convert"            % "1.3.1"
  val rabbitMq      = "com.rabbitmq"                % "amqp-client"             % "3.2.4"
  val hbasewd       = "com.sematext.hbasewd"        %% "hbasewd"                % "0.2.0-CSC.1"
  val scala_csv     = "com.github.tototoshi"        %% "scala-csv"              % "1.0.0"
  val amqpClient    = "com.github.sstone"           %% "amqp-client"             % "1.2-CSC"
  val exifExtractor = "com.drewnoakes"              % "metadata-extractor"      % "2.7.0-CSC"
  val commonImage   = "org.apache.commons"          % "commons-imaging"         % "1.0.1-CSC"
  val snmp4j        = "org.snmp4j"                  % "snmp4j"                  % "2.3.4"

  object Test {
    val junit       = "junit"                       % "junit"                  % "4.5"        % "test" // Common Public License 1.0
    val scalatest   = "org.scalatest"               %% "scalatest"             % V.Scalatest  % "test" // ApacheV2
    val akkaTestKit = "com.typesafe.akka"           % "akka-testkit"           % V.Akka       % "test" // ApacheV2
    val mockJavaMail= "org.jvnet.mock-javamail"     % "mock-javamail"          % "1.9"        % "test" // ApacheV2
  }
}

