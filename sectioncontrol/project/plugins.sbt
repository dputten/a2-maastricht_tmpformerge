resolvers += "CSC traffic" at "http://cscappamd911.nl.emea.csc.com:8082/nexus/content/groups/group_sectioncontrol/"

addSbtPlugin("com.typesafe.sbtmultijvm" % "sbt-multi-jvm" % "0.1.9")

addSbtPlugin("com.typesafe.sbtscalariform" % "sbtscalariform" % "0.3.1")

addSbtPlugin("com.github.gseitz" % "sbt-release" % "0.4")

addSbtPlugin("com.github.mpeltonen" % "sbt-idea" % "1.1.0")

resolvers += Resolver.url("artifactory", url("http://cscappamd911.nl.emea.csc.com:8082/nexus/content/groups/ivy-release/"))(Resolver.ivyStylePatterns)

addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.8.3")

//addSbtPlugin("com.typesafe.sbteclipse" % "sbteclipse" % "1.5.0")
//addSbtPlugin("com.typesafe.schoir" % "schoir" % "0.1.1")
//addSbtPlugin("me.lessis" % "ls-sbt" % "0.1.1")
