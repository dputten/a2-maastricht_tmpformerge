/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package sectioncontrol

import sbt._

object CDH3 {
  object V {
    val Hadoop       = "0.20.2-cdh3u3"
    val HBase        = "0.90.4-cdh3u3"
//  val Zookeeper    = "3.3.4-cdh3u3"  // Zookeeper version included from CDH3 implicitly
  }

  val hbase          = ("org.apache.hbase"     % "hbase"                % V.HBase
    exclude ("org.slf4j", "slf4j-api")
    exclude ("org.slf4j", "slf4j-log4j12"))

  val hadoopTest     = "org.apache.hadoop"   % "hadoop-test"          % V.Hadoop      intransitive()
//val hbaseTest      = "org.apache.hbase"    % "hbase"                % V.HBase       % "test" classifier "tests"

  val deps = Seq(hbase, hadoopTest)

  val unmanagedTestLibs = "src/main/lib/cdh3"
}
