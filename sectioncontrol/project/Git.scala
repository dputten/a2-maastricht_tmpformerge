package sectioncontrol

import sbt._

object Git {

  private lazy val gitExec = {
    val maybeOsName = sys.props.get("os.name").map(_.toLowerCase)
    val maybeIsWindows = maybeOsName.filter(_.contains("windows"))
    maybeIsWindows.map(_ => "git.exe").getOrElse("git")
  }

  private def cmd(args: Any*): ProcessBuilder = Process(gitExec +: args.map(_.toString))

  def tag: String = {
    try {
      (cmd("describe") !!) trim
    } catch {
      case _ => ""
    }
  }

  def commitHash: String = (cmd("rev-parse", "HEAD") !!) trim

}
