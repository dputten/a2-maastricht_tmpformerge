package sectioncontrol

import sbt._
import sbt.Keys._
import sbt.Project.Initialize
import java.io.File

object Publish {
  final val Snapshot = "-SNAPSHOT"

  val defaultPublishTo = SettingKey[File]("default-publish-to")

  lazy val settings = Seq(
    crossPaths := true,
    pomExtra := sectionControlPomExtra,
    publishTo <<= version { (v:String) => sectionControlPublishTo(v)},
    organizationName := "CSC Computer Sciences Corporation",
    organizationHomepage := Some(url("http://www.csc.com")),
    credentials += Credentials("Sonatype Nexus Repository Manager", "20.32.143.30", "deploy", "deploycsc")
  )

  lazy val versionSettings = Seq(
    commands += stampVersion
  )
  def sectionControlPomExtra = {
    <inceptionYear>2012</inceptionYear>
    <url>http://www.csc.com</url>
  }

  def sectionControlPublishTo(version:String): Option[Resolver] = {
    val property = Option(System.getProperty("csc.publish.repository"))
    val repo = property map { "Sonatype Nexus Repository Manager" at _ }
    repo orElse {
      val nexus = "http://20.32.143.30:8082/nexus/content/repositories/"
      if (version.trim.endsWith("SNAPSHOT"))
          Some("Sonatype Nexus Repository Manager" at nexus + "snapshots")
      else {
        Some("Sonatype Nexus Repository Manager"  at nexus + "releases")
      }
    }
  }

  // timestamped versions

  def stampVersion = Command.command("stamp-version") { state =>
    val extracted = Project.extract(state)
    extracted.append(List(version in ThisBuild ~= stamp), state)
  }

  def stamp(version: String): String = {
    if (version endsWith Snapshot) (version stripSuffix Snapshot) + "-" + timestamp(System.currentTimeMillis)
    else version
  }

  def timestamp(time: Long): String = {
    val format = new java.text.SimpleDateFormat("yyyyMMdd-HHmmss")
    format.format(new java.util.Date(time))
  }
}
