/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.akkautils

import java.util.concurrent.atomic.AtomicReference

import akka.actor.{ Actor, Props, ActorSystem }
import akka.event.Logging

import org.apache.curator.retry.RetryUntilElapsed
import org.apache.curator.framework.{ CuratorFrameworkFactory, CuratorFramework }
import com.typesafe.config.Config

import net.liftweb.json.{ DefaultFormats, Formats }

import csc.dlog.Event
import csc.curator.utils._
import csc.curator.utils.PathAdded
import scala.Some

/**
 * Extension of the GenericBoot trait to startup actors for a Route/System in
 * a separate ActorSystem for each Route/System.
 *
 * @author Maarten Hazewinkel
 */
trait MultiSystemBoot extends GenericBoot {
  import MultiSystemBoot._

  /**
   * Defines the path in the (application/akka) configuration that specifies the
   * Zookeeper ensemble to use. Required to be implemented by the concrete implementation.
   */
  protected def zkServerQuorumConfigPath: String
  protected def hbaseZkServerQuorumConfigPath: String

  /*
  the default retry policy. can be overruled by boot
   */
  protected def retryPolicy = new RetryUntilElapsed(1000 * 60 * 60 * 24 * 3, 5000)

  protected def formats(defaultFormats: Formats): Formats = DefaultFormats

  /**
   * The name for this module. Required to be implemented by the concrete implementation.
   */
  protected def moduleName: String

  private val curatorRef = new AtomicReference[Option[CuratorFramework]](None)
  private val watchNodeRef = new AtomicReference[Option[WatchNode]](None)

  def createCurator(system: ActorSystem, formats: Formats = DefaultFormats): Curator = {
    val logger = Logging.getLogger(system, classOf[CuratorToolsImpl])
    new CuratorToolsImpl(curatorRef.get(), logger, finalFormats = formats) {
      override protected def extendFormats(defaultFormats: Formats) = formats
    }
  }

  /**
   * Start up an EventLogger in the given actor system.
   * This EventLogger will listen for SystemEventType events being published on the
   * actor system event stream and drop them into the appropriate zookeeper queue for
   * consolidation into the central system logs.
   */
  def startEventLogger[SystemEventType <: Event: Manifest](systemId: String, curator: Curator, actorSystem: ActorSystem) {
    try {
      val queuePath = "/ctes/systems/" + systemId + "/events"
      actorSystem.actorOf(Props(EventLogger.createDefaultEventLogger[SystemEventType](curator, queuePath)), "EventLogger_" + systemId)
    } catch {
      //TODO 20120526 RR->MH: not good, why are you catching everything here? I dont think the system should boot if the eventLogger does not work,
      //TODO 20120526 RR->MH: if that would be part always of the boot. crash should be supervised. lets say zookeeper is not there yet at boot,
      //TODO 20120526 RR->MH: btu it is there a minute later, should not be a problem. Supervision and retry would solve this.
      case e ⇒ log.error(e, "eventlogger crashed")
    }
  }

  /**
   * Start a separate ActorSystem for each system found in zookeeper.
   */
  def startupActors() {
    val config = actorSystem.settings.config
    if (config.hasPath(zkServerQuorumConfigPath)) {
      val zkServerQuorum = config.getString(zkServerQuorumConfigPath)
      val hbaseZkServerQuorum = if (config.hasPath(hbaseZkServerQuorumConfigPath)) {
        config.getString(hbaseZkServerQuorumConfigPath)
      } else {
        zkServerQuorum
      }

      // A watchnode also sends an add if it reads in zk the first time
      curatorRef.set(Some(CuratorFrameworkFactory.newClient(zkServerQuorum, retryPolicy)))
      curatorRef.get().foreach {
        c ⇒
          c.start()
          watchNodeRef.set(Some(new WatchNode(c, "/ctes/systems", actorSystem.actorOf(Props(new Actor() {
            private var actorSystems = List[String]()
            def zkServers = zkServerQuorum
            def hbaseZkServers = hbaseZkServerQuorum

            def receive = {
              case PathAdded(systemId, _) ⇒ {
                if (!actorSystems.contains(systemId)) {
                  startupSystemActors(systemId, config, c, hbaseZkServers _)
                  actorSystems = systemId :: actorSystems
                }
              }
              case PathRemoved(systemId, _) ⇒ systemActorSystemsRef.get().get(systemId).foreach(shutdownSystemActors(_, systemId))
            }
          })))))
          watchNodeRef.get().foreach(_.start())
      }
    } else {
      log.error("required key {} not defined in config", zkServerQuorumConfigPath)
    }
  }

  def startupSystemActors(systemId: String, config: Config, curator: CuratorFramework, hbaseZkServers: () ⇒ String) {
    val actorSystem = ActorSystem(moduleName + "-" + systemId, config)
    val storage = createCurator(actorSystem, formats(DefaultFormats))
    var expected = systemActorSystemsRef.get()
    while (!systemActorSystemsRef.compareAndSet(expected, expected + (systemId -> actorSystem))) {
      expected = systemActorSystemsRef.get()
    }
    startupSystemActors(actorSystem, systemId, storage, hbaseZkServers())
  }

  override def shutdown() {
    try super.shutdown() finally shutdownSystems()
  }
  /**
   * Shuts down all system-specific actor systems.
   */
  final def shutdownActors() {
    try watchNodeRef.get().foreach(_.stop()) finally
      try systemActorSystemsRef.get().toSeq.foreach(system ⇒ shutdownSystemActors(system._2, system._1))
      finally curatorRef.get().foreach(_.close())
  }

  final def shutdownSystems() {
    systemActorSystemsRef.get().values.foreach(_.shutdown())
  }
  /**
   * Starts up the actors withing a single system-specific ActorSystem. Needs to be implemented
   * by the concrete implementation.
   */
  def startupSystemActors(system: ActorSystem, systemId: String, curator: Curator, hbaseZkServers: String)

  /**
   * Shut down the actors in a single system-specific ActorSystem. Optional to be implemented by
   * the concrete implementation.
   */
  def shutdownSystemActors(system: ActorSystem, systemId: String) {}
}
//TODO 20120526 RR->MH: why is this in an object, just add to trait. Dont create 'singletons'.
object MultiSystemBoot {
  private val systemActorSystemsRef = new AtomicReference[Map[String, ActorSystem]](Map())

  /**
   * find the ActorSystem for a particular system (route)
   */
  def getActorSystem(systemId: String): Option[ActorSystem] =
    systemActorSystemsRef.get().get(systemId)

}
