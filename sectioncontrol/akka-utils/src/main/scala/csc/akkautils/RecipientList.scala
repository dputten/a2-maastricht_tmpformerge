/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.akkautils

import akka.actor._

/**
 * This base class implements the IntegrationPattern RecipientList.
 */
trait RecipientList {
  val recipients: Seq[ActorRef]

  /**
   * Send the message to all recipients
   */
  def sendToRecipients(msg: Any) {
    for (actorRef ← recipients) {
      actorRef ! msg
    }
  }
}
