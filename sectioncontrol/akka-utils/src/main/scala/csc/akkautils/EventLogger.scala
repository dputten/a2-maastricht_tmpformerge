/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

package csc.akkautils

import org.apache.curator.framework.CuratorFramework
import akka.actor.{ ActorLogging, Actor }
import csc.dlog.{ DynamicPathQueueProducer, SimpleDistributedQueueProducer, QueueProducer, Event }
import csc.json.lift.{ ByteArraySerialization, LiftSerialization }
import net.liftweb.json.DefaultFormats
import csc.curator.utils.Curator

/**
 * An Actor that logs events to the Distributed Queue. Subscribes to `Event` type on the event bus
 * @author Raymond Roestenburg
 */
class EventLogger[T <: Event: Manifest](curator: Curator, qProducerCreator: CuratorFramework ⇒ QueueProducer[T], byteSerialization: ByteArraySerialization) extends Actor with ActorLogging {
  private var clientScope: Option[CuratorFramework] = None
  private var qProducerScope: Option[QueueProducer[T]] = None

  override def preStart() {
    context.system.eventStream.subscribe(self, manifest[T].erasure)
    clientScope = curator.curator
    clientScope match {
      case Some(c) ⇒ {
        qProducerScope = Some(qProducerCreator(c))
        qProducerScope.foreach(_.start)
      }
      case None ⇒ log.error("INIT FAILED CURATOR IS NONE")
    }
  }

  override def postStop() {
    for (scope ← qProducerScope) {
      try scope.close() finally {
        qProducerScope = None
        clientScope = None
      }
    }
  }

  /**
   * Forwards events to distributed queue
   */
  def receive = {
    case event: T ⇒ qProducerScope.foreach(_.put(event))
    case _        ⇒ ()
  }
}

/**
 * Companion object with convenience default event logger
 */
object EventLogger {
  val defaultLiftSerialization = new LiftSerialization {
    implicit def formats = DefaultFormats
  }

  /**
   * Creates a default configured EventLogger Actor, which you need to pass to a Props object in system.actorOf
   * @param curator zk connection
   * @param queuePath path to log to
   * @param byteArraySerialization serialization
   * @tparam T element to log
   * @return the event logger actor
   */
  def createDefaultEventLogger[T <: Event: Manifest](curator: Curator, queuePath: String, byteArraySerialization: ByteArraySerialization = defaultLiftSerialization): Actor = {
    def createQueueProducer(curator: Curator, queuePath: String): CuratorFramework ⇒ QueueProducer[T] = {
      client ⇒ new SimpleDistributedQueueProducer[T](client, queuePath, byteArraySerialization)
    }
    new EventLogger[T](curator, createQueueProducer(curator, queuePath), byteArraySerialization)
  }

  /**
   * Creates a default configured EventLogger Actor supporting a dynamic path, which you need to pass to a Props object in system.actorOf
   * @param curator the zookeeper connection
   * @param queuePath The function to create a queuePath depending on the event
   * @param byteArraySerialization the serialization
   * @tparam T element to log
   * @return the event logger actor
   */
  def createDynamicPathEventLogger[T <: Event: Manifest](curator: Curator, queuePath: (T) ⇒ String, byteArraySerialization: ByteArraySerialization = defaultLiftSerialization): Actor = {
    val createQueueProducer = (client: CuratorFramework) ⇒ new DynamicPathQueueProducer[T](client, queuePath, byteArraySerialization)

    new EventLogger[T](curator, createQueueProducer, byteArraySerialization)

  }
}
