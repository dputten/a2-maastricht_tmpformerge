/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.akkautils

import java.util.{ Date, TimeZone, Calendar, GregorianCalendar }

import akka.actor._
import akka.util.duration._
import akka.actor.SupervisorStrategy.Stop

import csc.config.Path

import csc.curator.utils.{ Curator, Versioned }
import org.apache.zookeeper.KeeperException

/**
 * This actor allows jobs to be scheduled for running either on an interval, or at a specific time
 * of day.
 * The actor will scan the zkPathGlob zookeeper path with wildcards and manage the running of all
 * the job schedules found there.
 *
 * To create the actor, it needs to be created with a concrete implementation of the StartJob trait.
 *
 * The runtime behaviour of this actor is as follows for jobs that have the WaitForCompletion flag
 * set. This flag can be set either in the job metadata in zookeeper, or can be mixed into the RunScheduledJobs
 * actor via the marker trait WaitForCompletion.
 *
 * Every minute the actor sends itself a Tick message. It will then scan it's job path (expanding the
 * wildcards anew for every scan) for jobs that are both enabled, whose nextRun value is in the past,
 * and that are not already running (as indicated by the isRunning flag).
 *
 * The jobs that are found and that satisfy all of the above criteria are started, and their isRunning flag
 * is set to true in zookeeper.
 *
 * When a job completes its run, it reports back to the RunScheduledJobs actor with either a RunComplete or
 * a RunFailed message.
 *
 * For a RunComplete message, the RunScheduledJobs actor will use the values of either runAtMillisInterval
 * or the runAtMillisPastMidnight (only 1 of those fields can be set) to determine the next time the job
 * should be run. This value is set in the nextRun field and the isRunning field is set to false.
 *
 * If a RunComplete message includes the optional completeUntil value, the calculation of the nextRun value
 * is adjusted to not start the job before the completeUntil time has passed.
 *
 * For a RunFailed message, the behaviour depends on the rerun flag in the message.
 * If the rerun flag is set to true, the isRunning flag is set to false in zookeeper, and the nextRun
 * field is left unchanged. This means that the job will be rerun on the next Tick message, so within
 * a minute.
 *
 * If the return flag is set to false, the next run time is calculated based on the runAtMillisInterval or
 * the runAtMillisPastMidnight fields, the nextRun field is set to the calculated value and the isRunning
 * flag is set to false.
 *
 *
 * For jobs that do not have the WaitForCompletion flag set, the behaviour is much simpler. They are still
 * triggered by a Tick message on passing the set nextRun time. However, on start the nextRun value is
 * immediately updated based on the runAtMillisInterval or runAtMillisPastMidnight values. The isRunning flag
 * is not set, and these jobs do not report back upon completion.
 *
 * TODO add monitoring of job actors, deal with unusual termination or excessive runtimes.
 *
 * @param zkPathGlob The path in zookeeper with wildcard elements specified as '*'
 * @param curator The zookeeper connection
 *
 * @author Maarten Hazewinkel
 */
class RunScheduledJobs(zkPathGlob: String,
                       curator: Curator,
                       startJob: (String, Option[RunToken], ActorRef, Curator) ⇒ Unit)
  extends Actor with ActorLogging {

  protected def receive = {
    case Tick ⇒
      log.debug("received Tick")
      sendClockTick()

    case ClockTick(time) ⇒
      log.debug("received ClockTime({}). Checking for due jobs", time)
      checkAndRunJobs(time)

    case RunComplete(zkPath, runToken, None) ⇒
      log.info("job {} completed successfully. Scheduling next run", zkPath)
      scheduleNextRun(zkPath, runToken)

    case RunComplete(zkPath, runToken, Some(completedUntil)) ⇒
      log.info("job {} completed successfully until {} ({}). Scheduling next run", zkPath, new Date(completedUntil), completedUntil)
      scheduleNextRun(zkPath, runToken, completedUntil)

    case RunFailed(zkPath, runToken, rerun) ⇒
      if (rerun) {
        resetJobRun(zkPath, runToken)
        log.info("job {} failed. Scheduling rerun", zkPath)
      } else {
        scheduleNextRun(zkPath, runToken)
        log.info("job {} failed. Scheduling next run", zkPath)
      }
  }

  private var tickSchedule: Option[Cancellable] = None

  override def preStart() {
    super.preStart()
    tickSchedule = Some(context.system.scheduler.schedule(1.second, 1.minute, self, Tick))
    resetAllJobs()
  }

  override def postStop() {
    tickSchedule.foreach(_.cancel())
    tickSchedule = None
    super.postStop()
  }

  protected def sendClockTick() {
    self ! ClockTick(System.currentTimeMillis())
  }

  /**
   * Read all schedules and start up any jobs that are due (and are not blocked by uncompleted previous runs)
   *
   * @param time The clock time to use
   */
  private[akkautils] def checkAndRunJobs(time: Long) {
    val expandedPaths = expandPathGlob(zkPathGlob)
    expandedPaths.foreach(path ⇒ {
      curator.getVersioned[JobSchedule](path.toString()).foreach(jobSchedule ⇒ {

        if (jobSchedule.data.enabled && time >= jobSchedule.data.nextRunTime(time)) {
          if (isWaitForCompletionJob(jobSchedule.data) && jobSchedule.data.isRunning) {
            log.debug("Job {} is already running", path)
          } else {
            if (isWaitForCompletionJob(jobSchedule.data)) {
              val runToken = Some(RunToken(jobSchedule.data.nextRunTime(time), time, jobSchedule.data.timeZone))
              if (updateSchedule(path, jobSchedule.data.copy(isRunning = true, runToken = runToken, waitForCompletion = true), jobSchedule.version)) {
                val jobPath = path.toString()
                log.info("starting job {}", jobPath)
                // TODO rethink failure management and run-state management.
                // See if they can be folded into the job itself rather than in the runner.
                try {
                  context.actorOf(Props(new RunJob(jobPath, runToken, self, curator))) ! StartJob(startJob)
                } catch {
                  case e: Exception ⇒
                    log.error(e, "job {} failed to start", jobPath)
                    self ! RunFailed(jobPath, runToken.get, true)
                }
              }
            } else {
              if (updateSchedule(path, jobSchedule.data.nextRunSchedule(time), jobSchedule.version)) {
                val jobPath = path.toString()
                try {
                  context.actorOf(Props(new RunJob(jobPath, None, self, curator))) ! StartJob(startJob)
                } catch {
                  case e: Exception ⇒
                    log.error(e, "job {} failed to start", jobPath)
                }
              }
            }
          }
        } else if (jobSchedule.data.enabled) {
          log.debug("Job {} is scheduled for {} ({})", path, jobSchedule.data.nextRunTime(time), new Date(jobSchedule.data.nextRunTime(time)).toString)
        }

      })
    })
  }

  /**
   * Reset all enabled schedules to clear the
   */
  private[akkautils] def resetAllJobs() {
    val expandedPaths = expandPathGlob(zkPathGlob)
    expandedPaths.foreach(path ⇒ {
      curator.getVersioned[JobSchedule](path.toString()).foreach(jobSchedule ⇒ {
        if (jobSchedule.data.enabled && jobSchedule.data.isRunning) {
          updateSchedule(path, jobSchedule.data.copy(isRunning = false, runToken = None), jobSchedule.version)
        }
      })
    })
  }

  /**
   * Updates the job schedule to run again at the next scheduled time.
   * Checks that the supplied run token matches the one issued on the last job run.
   *
   * @param zkPath The path for the job schedule
   * @param runToken The token issued to the last (just completed) job run
   */
  private[akkautils] def scheduleNextRun(zkPath: String, runToken: RunToken) {
    val path = Path(zkPath)
    curator.getVersioned[JobSchedule](path.toString()) match {
      case Some(Versioned(jobSchedule, storedVersion)) ⇒
        if (isWaitForCompletionJob(jobSchedule) && jobSchedule.isRunning && Some(runToken) == jobSchedule.runToken) {
          updateSchedule(path, jobSchedule.nextRunSchedule(runToken.startTime), storedVersion)
        }
      case None ⇒
        log.error("Job {} not found. Cannot schedule next run", zkPath)
    }
  }

  /**
   * Updates the job schedule to run again at the next scheduled time after the given completedUntil time.
   * Checks that the supplied run token matches the one issued on the last job run.
   *
   * @param zkPath The path for the job schedule
   * @param runToken The token issued to the last (just completed) job run
   * @param completedUntil The time until which the job has completed all work
   */
  private[akkautils] def scheduleNextRun(zkPath: String, runToken: RunToken, completedUntil: Long) {
    val path = Path(zkPath)
    curator.getVersioned[JobSchedule](path.toString()) match {
      case Some(Versioned(jobSchedule, storedVersion)) ⇒
        if (isWaitForCompletionJob(jobSchedule) && jobSchedule.isRunning && Some(runToken) == jobSchedule.runToken) {
          updateSchedule(path, jobSchedule.copy(nextRun = completedUntil).nextRunSchedule(runToken.startTime), storedVersion)
        }
      case None ⇒
        log.error("Job {} not found. Cannot schedule next run", zkPath)
    }
  }

  /**
   * Updates the job schedule to be re-run at the last scheduled time, which will generally be some time
   * in the past. This will cause the job to be restarted ASAP.
   * Checks that the supplied run token matches the one issued on the last job run.
   *
   * @param zkPath The path for the job schedule
   * @param runToken The token issued to the last (just completed) job run
   */
  private[akkautils] def resetJobRun(zkPath: String, runToken: RunToken) {
    val path = Path(zkPath)
    curator.getVersioned[JobSchedule](path.toString()) match {
      case Some(Versioned(jobSchedule, storedVersion)) ⇒
        if (isWaitForCompletionJob(jobSchedule) && jobSchedule.isRunning && Some(runToken) == jobSchedule.runToken) {
          updateSchedule(path, jobSchedule.copy(isRunning = false, runToken = None), storedVersion)
        }
      case None ⇒
        log.error("Job {} not found. Cannot reset current run", zkPath)
    }
  }

  /**
   * Updates the job schedule in Zookeeper.
   * @return false if the update fails due to non-matching versions. True otherwise.
   */
  private[akkautils] def updateSchedule(path: Path, schedule: JobSchedule, oldVersion: Long, allowedRetries: Int = 10): Boolean = {
    try {
      curator.set(path.toString(), schedule, oldVersion.toInt)
      true
    } catch {
      case e: KeeperException.BadVersionException ⇒
        if (allowedRetries <= 0) {
          log.error(e, "Job {} cannot be saved to Zookeeper. Bad version.", path)
          false
        } else {
          log.info("Job {} failed to be saved to Zookeeper. Bad version. Retrying...")
          val storedVersion = curator.getVersioned[JobSchedule](path.toString()).map(_.version.toLong).getOrElse(0L)
          updateSchedule(path, schedule, storedVersion, allowedRetries - 1)
        }
    }
  }

  /**
   * Expands a Zookeeper path that contains '*' for some nodes into the full list of matching
   * paths in Zookeeper.
   *
   * '*' can only substitute for a full path element, like in "/ctes/systems/*/schedules/job".
   * This will not work: "/ctes/systems/nl*/schedules/job".
   *
   * @param glob The path string with wildcards
   * @return A sequence of matching Path instances
   */
  private[akkautils] def expandPathGlob(glob: String): Seq[Path] = {
    val globPath = Path(glob)

    def expandPath(path: Path): Seq[Path] = {
      path.nodes.indexWhere(_.name == "*") match {
        case -1 ⇒ {
          if (curator.exists(path.toString())) {
            Seq(path)
          } else {
            Seq()
          }
        }
        case 0 ⇒ {
          log.error("Glob wildcard cannot be the first element of the path")
          Seq()
        }
        case i if i == path.nodes.length - 1 ⇒ {
          val prefix = Path(path.nodes.take(i))
          if (curator.exists(prefix.toString())) {
            curator.getChildren(prefix.toString()).map(p ⇒ Path(p))
          } else {
            Seq()
          }
        }
        case i ⇒ {
          val prefix = Path(path.nodes.take(i))
          val postfix = Path(path.nodes.drop(i + 1))
          if (curator.exists(prefix.toString())) {
            curator.getChildren(prefix.toString())
              .flatMap(p ⇒ expandPath(Path(p) / postfix))
          } else {
            Seq()
          }
        }
      }
    }

    expandPath(globPath)
  }

  private def isWaitForCompletionJob(jobSchedule: JobSchedule): Boolean =
    this.isInstanceOf[WaitForCompletion] || jobSchedule.waitForCompletion

  override def supervisorStrategy(): SupervisorStrategy = OneForOneStrategy() {
    case e: Exception ⇒
      log.error(e, "Job crashed.")
      Stop
  }
}

class RunJob(jobPath: String, runToken: Option[RunToken], scheduler: ActorRef, curator: Curator) extends Actor with ActorLogging {
  protected def receive = {
    case StartJob(startJob) ⇒
      startJob(jobPath, runToken, self, curator)

    case CreateJobActor(constructor, startMessage) ⇒
      context.actorOf(Props(constructor())) ! startMessage

    case JobFailed(rerun) ⇒
      runToken.foreach(token ⇒
        scheduler ! RunFailed(jobPath, token, rerun))
      context.stop(self)

    case JobFailed ⇒
      self ! JobFailed()

    case JobComplete(None) ⇒
      runToken.foreach(token ⇒
        scheduler ! RunComplete(jobPath, token, None))
      context.stop(self)

    case JobComplete(Some(completedUntil)) ⇒
      runToken.foreach(token ⇒
        scheduler ! RunComplete(jobPath, token, Some(completedUntil)))
      context.stop(self)

    case JobComplete ⇒
      self ! JobComplete()
  }

  override def supervisorStrategy() = AllForOneStrategy() {
    case e: Exception ⇒
      log.error(e, "Job {} crashed. Rescheduling without rerun.")
      self ! JobFailed(false)
      Stop
  }
}

case class StartJob(startJob: (String, Option[RunToken], ActorRef, Curator) ⇒ Unit)

case class CreateJobActor(constructor: () ⇒ Actor, startMessage: AnyRef)

case class JobFailed(retry: Boolean = false)

case class JobComplete(completedUntil: Option[Long] = None)

/**
 * Non-parameterised clock tick message sent by the Akka scheduler every minute. Causes the
 * actor to send itself a ClockTick message that includes the time.
 */
private[akkautils] case object Tick

/**
 * Clock-tick message with a time parameter. This message actually causes the actor to start
 * running any due jobs.
 *
 * @param time The time
 */
case class ClockTick(time: Long)

/**
 * Defines a schedule for running a job.
 *
 * A schedule requires exactly one of the parameters runAtMillisPastMidnight and runAtMilliesInterval
 * to be defined.
 *
 * @param enabled Whether the job is currently enabled.
 * @param nextRun The next time (epoch milliseconds) the job is to be run
 * @param runAtMillisPastMidnight Whether to run at a fixed number of milliseconds past midnight.
 *                                Used to calculate the next runtime for this schedule.
 * @param runAtMillisInterval Whether to run at a fixed length interval in milliseconds.
 *                            Used to calculate the next runtime for this schedule.
 * @param timeZone The timezone used to calculate when midnight is. Defaults to "UTC"
 * @param waitForCompletion Whether to wait for the job to be completed before starting it again
 * @param isRunning Whether the job is currently running
 * @param runToken The token linked to the current job run
 */
case class JobSchedule(enabled: Boolean,
                       nextRun: Long,
                       runAtMillisPastMidnight: Option[Long],
                       runAtMillisInterval: Option[Long],
                       timeZone: String = "UTC",
                       waitForCompletion: Boolean = false,
                       isRunning: Boolean = false,
                       runToken: Option[RunToken] = None) {
  require(runAtMillisInterval.isDefined ^ runAtMillisPastMidnight.isDefined)

  def nextRunTime(currentTime: Long) = if (nextRun == 0) currentTime else nextRun

  def nextRunSchedule(currentTime: Long): JobSchedule = {
    val nextScheduledRunTime = if (runAtMillisInterval.isDefined) {
      nextRunTime(currentTime) + runAtMillisInterval.get
    } else {
      nextMidnight(currentTime) + runAtMillisPastMidnight.get
    }
    copy(nextRun = nextScheduledRunTime, isRunning = false, runToken = None)
  }

  private def nextMidnight(currentTime: Long): Long = {
    val cal = new GregorianCalendar(TimeZone.getTimeZone(timeZone))
    cal.setTimeInMillis(nextRunTime(currentTime))
    cal.set(Calendar.MINUTE, 0)
    cal.set(Calendar.SECOND, 0)
    cal.set(Calendar.MILLISECOND, 0)
    cal.set(Calendar.HOUR_OF_DAY, 24)
    cal.getTimeInMillis
  }
}

/**
 * A run token issued to jobs that wait for completion before being run again.
 * This token is handed to the job and must be sent back to the RunScheduledJobs instance to
 * complete the job run and allow further runs to be started.
 *
 * @param scheduledTime The timestamp for when the job was scheduled to start.
 * @param startTime The timestamp for when the job was actually started.
 * @param timezone The timezone used for this job.
 */
case class RunToken(scheduledTime: Long, startTime: Long, timezone: String)

/**
 * Signals that a job completed successfully. This is only used for jobs that wait for
 * completion before being run again.
 *
 * @param zkPath The path of the job schedule
 * @param runToken The token issued to the job on the last run
 * @param completedUntil Optional. All work completed up to the indicated time.
 */
private case class RunComplete(zkPath: String, runToken: RunToken, completedUntil: Option[Long])

/**
 * Signals that a job completed in a failed state. This is only used for jobs that wait for
 * completion before being run again.
 *
 * @param zkPath The path of the job schedule
 * @param runToken The token issued to the job on the last run
 * @param rerun Whether to rerun the same instance of the job. If true, the job will be restarted.
 *              If false (the default) the job will be scheduled to run again at the next normally
 *              scheduled run time.
 */
private case class RunFailed(zkPath: String, runToken: RunToken, rerun: Boolean = false)

/**
 * Marker trait to indicate programmatically that a job needs to wait for completion
 * before being run again. This overrides any setting of the waitForCompletion parameter in the
 * job schedule.
 */
trait WaitForCompletion
