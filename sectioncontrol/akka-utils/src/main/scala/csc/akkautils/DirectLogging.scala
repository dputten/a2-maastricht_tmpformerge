/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.akkautils

import akka.event.LoggingAdapter
import org.slf4j.LoggerFactory

/**
 * This trait can be used to do logging in classes like the ActorLogging
 * but without an Actor context
 */
trait DirectLogging {
  @transient
  lazy val log = new DirectLoggingAdapter(this.getClass.getName)
}

/**
 * This class implements the LoggingAdapter interface. This interface is
 * also used in the ActorLogging
 */
class DirectLoggingAdapter(thisClass: String) extends LoggingAdapter {
  private val logSLF4J = LoggerFactory.getLogger(thisClass)

  def isErrorEnabled = logSLF4J.isErrorEnabled

  def isWarningEnabled = logSLF4J.isWarnEnabled

  def isInfoEnabled = logSLF4J.isInfoEnabled

  def isDebugEnabled = logSLF4J.isDebugEnabled

  protected def notifyError(message: String) {
    logSLF4J.error(message)
  }

  protected def notifyError(cause: Throwable, message: String) {
    logSLF4J.error(message, cause)
  }

  protected def notifyWarning(message: String) {
    logSLF4J.warn(message)
  }

  protected def notifyInfo(message: String) {
    logSLF4J.info(message)
  }

  protected def notifyDebug(message: String) {
    logSLF4J.debug(message)
  }

}
