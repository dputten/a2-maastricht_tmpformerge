/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.akkautils

import akka.kernel.Bootable
import java.lang.Boolean._
import com.typesafe.config.{ ConfigFactory, Config }
import java.io.File
import akka.event.{ Logging, LoggingAdapter }
import akka.actor.ActorSystem

/**
 * Generic support class for booting up an actor system with an optional configuration
 * file that augments or overrides the standard Akka application.{conf|json|properties}
 * configuration file.
 *
 * This class read the configuration and uses it to start up an ActorSystem.
 * Subclasses provide the name of the actor system as well as methods to start-up and
 * shut-down the actors that run inside the actor system.
 *
 * The class implements the Akka Bootable trait, allowing it to be started by an Akka
 * microkernel system.
 *
 * Subclasses can also make use of the log instance variable, which is tied to the actor
 * system for logging.
 *
 * @author Maarten Hazewinkel
 */
trait GenericBoot extends Bootable {

  /**
   * Defines the name of the component being booted, and also the name of the actor
   * system that will be started.
   * This is also used as the base of the name of the configuration file to read by
   * appending ".conf" to this name.
   */
  def componentName: String

  /**
   * Implemented by subclasses to start up any required actors in the actor system.
   */
  def startupActors()

  /**
   * Implemented by subclasses to perform an orderly shutdown of the actors in the
   * actor system.
   */
  def shutdownActors()

  //TODO 20120526 RR->MH: dont make public vars.
  /**
   * The actor system. This will always be available since failure to start it will
   * cause the startup method to fail before the subclass is called to execute
   * startupActors().
   */
  var actorSystem: ActorSystem = _
  //TODO 20120526 RR->MH: dont make public vars.
  /**
   * A LoggingAdapter linked to the actorSystem.
   */
  var log: LoggingAdapter = _

  /**
   * Start up an actor system while optionally providing a custom configuration file.
   * The configuration file can be provided in 3 different ways:
   * 1. Use the '-D{componentName}.conf=...' startup parameter to define the system property pointing to the file
   * 2. Put the file '{componentName}.conf' in the classpath at the root level
   * 3. Defne the 'AKKA_HOME' environment variable and put the file '{componentName}.conf' in the 'config' directory under AKKA_HOME
   */
  def startup() {
    val defaultConfig = ConfigFactory.load()
    val config = loadConfiguration(componentName + ".conf") match {
      case Some(customConfig) ⇒ customConfig.withFallback(defaultConfig)
      case None               ⇒ defaultConfig
    }

    actorSystem = ActorSystem(componentName, config)
    log = Logging(actorSystem, this.getClass)

    startupActors()
  }

  /**
   * Shut down the actor system.
   */
  def shutdown() {
    if (actorSystem != null) {
      shutdownActors()
      actorSystem.shutdown()
      actorSystem = null
      log = null
    }
  }

  private val quiet = getBoolean(componentName + ".silentboot")

  private def log(s: String) {
    if (!quiet) println(s)
  }

  protected def loadConfiguration(configFileName: String): Option[Config] = {
    val sysPropConfigFile = System.getProperty(configFileName, "")
    if (sysPropConfigFile != "") {
      log("Loading config from -D%s=%s".format(configFileName, sysPropConfigFile))
      Some(ConfigFactory.parseFile(new File(sysPropConfigFile)))
    } else {
      val resourceConfigFile = getClass.getClassLoader.getResource(configFileName)
      if (resourceConfigFile != null) {
        log("Loading config  resource %s".format(resourceConfigFile.getPath))
        Some(ConfigFactory.parseFile(new File(resourceConfigFile.getPath)))
      } else {
        Option(System.getenv("AKKA_HOME")) flatMap { akkaHome ⇒
          val akkaHomeConfigFile = new File(akkaHome + "/config/" + configFileName)
          if (akkaHomeConfigFile.exists()) {
            Some(akkaHomeConfigFile)
          } else {
            None
          }
        } match {
          case Some(akkaHomeConfigFile) ⇒ {
            log("Loading config from file AKKA_HOME/config/%s [%s]".format(configFileName, akkaHomeConfigFile.getAbsolutePath))
            Some(ConfigFactory.parseFile(akkaHomeConfigFile))
          }
          case None ⇒ {
            log("""No specific configuration file %s found. Using standard Akka configuration.
           |To use specific configuration use one of these three ways:
           |    1. Use the '-D%s=...' startup parameter to define the system property pointing to the file
           |    2. Put the file '%s' in the classpath at the root level
           |    3. Defne the 'AKKA_HOME' environment variable and put the file '%s' in the 'config' directory under AKKA_HOME
            """.stripMargin.format(configFileName, configFileName, configFileName, configFileName))
            None
          }
        }
      }
    }
  }
}
