package csc.sectioncontrol.digitalswitch

/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

import akka.camel.{ CamelMessage ⇒ Message, Consumer }
import csc.config.Path
import csc.curator.utils.{ CuratorActor, Curator }

/**
 * Detects via external udp messages if a switch digital input has triggered an open or off signal and
 * determines what device is one that input signal. Sends a system event with information regarding the
 * device that triggers the signal
 * @param uri the uri endpoint
 * @param systemId id of system
 */
class DetectDigitalInput(uri: String, systemId: String, protected val curator: Curator) extends CuratorActor with Consumer {
  def endpointUri = uri

  val pathToSystem = Path("/ctes") / "systems" / systemId
  var digitalInputs: Seq[DigitalInputConfig] = _

  override def preStart() {
    try {
      super.preStart()
      digitalInputs = getDigitalInputConfig.map(_.configs).getOrElse(List())
      log.debug("retrieved [%s] regular expressions for system [%s]".format(digitalInputs.size, systemId))
    } catch {
      case ex: Exception ⇒ log.error("failed to initialize DetectDigitalInput [%s] for system [%s]: %s".format(pathToSystem.toString(), systemId, ex))
    }
  }

  private def logHeaders(msg: Message) {
    import scala.collection.JavaConversions._
    log.info("Start logging headers")
    msg.getHeaders.foreach {
      case (key, value) ⇒
        log.info("%s:%s".format(key, value.toString))
    }
    log.info("End logging headers")
  }

  def receive = {
    case msg: Message ⇒ {
      //removing line feed
      val txt = msg.bodyAs[String].replaceAll("\\n", "")
      logHeaders(msg)
      log.debug("received message => [" + txt + "]")
      digitalInputs.foreach {
        digitalInput ⇒
          val expression = digitalInput.expression.r
          log.debug("checking with expression => [" + digitalInput.expression + "]")
          txt match {
            case expression(_, prevState, currState) ⇒ {
              log.info("we have a match for message [" + txt + "]")
              log.info("matched with [" + digitalInput.expression + "]")
              val eventType = currState match {
                case digitalInput.openSignal ⇒ digitalInput.openEventType
                case _                       ⇒ digitalInput.closedEventType
              }
              val description = currState match {
                case digitalInput.openSignal ⇒ digitalInput.openDescription
                case _                       ⇒ digitalInput.closedDescription
              }
              val reason = "%s\n%s".format(description, txt)
              val event = SystemEvent(
                componentId = DetectDigitalInput.createComponentName(digitalInput.componentType, msg),
                timestamp = System.currentTimeMillis(),
                systemId = systemId,
                eventType = eventType,
                userId = "system",
                reason = reason)
              if (isEnabled) {
                sendEvent(systemId, event)
              } else {
                log.debug("Digital input is not enabled, will not be sending message")
                log.debug(event.toString)
              }
            }
            case _ ⇒ log.debug("no match found") //disregard message
          }
      }
    }
  }

  /**
   * get configuration for DigitalInput of a specific system
   * @return DigitalInput for a specific system
   */
  private def getDigitalInputConfig: Option[DigitalInput] = {
    curator.get[DigitalInput](pathToSystem / "digital-inputs")
  }

  /**
   * determines if system messages must be send to monitor
   * @return
   */
  private def isEnabled: Boolean = {
    getDigitalInputConfig.map(_.isEnabled).getOrElse(false)
  }

  /**
   * Append an event in storage
   * @param systemId is id of system
   * @param systemEvent to be appended
   */
  private def sendEvent(systemId: String, systemEvent: SystemEvent) {
    val pathToEvents = pathToSystem / "events"

    if (curator.exists(pathToSystem) && !curator.exists(pathToEvents)) {
      curator.createEmptyPath(pathToEvents)
    }

    curator.appendEvent(pathToEvents / "qn-", systemEvent)
  }
}

object DetectDigitalInput {
  val headerRemoteAddress = "CamelMinaRemoteAddress"
  def createComponentName(componentType: String, msg: Message): String = {
    val ipFormat = """xxx.xxx.xxx.xxx"""
    val remoteAddress = """/([0-9.]*):[0-9]*""".r

    try {
      msg.getHeader(headerRemoteAddress) match {
        case remoteAddress(ip) ⇒ {
          componentType.replaceAll(ipFormat, ip)
        }
        case other ⇒ componentType
      }
    } catch {
      case ex: NoSuchElementException ⇒ componentType
    }
  }
}

/**
 * Used by SystemEvent to indicate if one event needs to send multiple system events
 * @param token unique id for the duration of the event that sends multiple system events
 * @param last indicates if the system event is the last message of a sequence of system events from the same event
 */
case class SystemEventToken(token: String, last: Boolean)

/**
 * Represents a system wide event
 * @param componentId id of component that has sent the event
 * @param timestamp when the event was sent
 * @param systemId id of system
 * @param eventType the type of event the system event belongs to
 * @param userId id of user or system
 * @param reason additional information regarding the system event
 * @param token used if one event needs to send multiple system events
 */
case class SystemEvent(componentId: String,
                       timestamp: Long,
                       systemId: String,
                       eventType: String,
                       userId: String,
                       reason: String = "",
                       token: Option[SystemEventToken] = None) extends csc.dlog.Event

/**
 * Contains all digital input configurations of a specific system
 * @param configs sequence of digital input configurations
 */
case class DigitalInput(isEnabled: Boolean = false, configs: Seq[DigitalInputConfig])

/**
 * Represents the configuration of a digital input on a switch
 * @param componentType the type of component that is connected on digital input
 * @param expression regular expression that extracts the received message from the switch
 * @param openSignal represents what an open or off signal is e.g. "On" or "Off" This determines if openEventType or
 * closedEventType is being send from the device that is connected to the digital input of the switch
 * @param openEventType this event represents what type of event is send from the device if the openSignal is
 * equal to the received signal from the switch
 * @param openDescription description when event type is open
 * @param closedEventType this event represents what type of event is send from the device if the openSignal is
 * equal to the received signal from the switch
 * @param closedDescription description when event type is open
 */
case class DigitalInputConfig(componentType: String,
                              expression: String,
                              openSignal: String,
                              openEventType: String,
                              openDescription: String,
                              closedEventType: String,
                              closedDescription: String)
