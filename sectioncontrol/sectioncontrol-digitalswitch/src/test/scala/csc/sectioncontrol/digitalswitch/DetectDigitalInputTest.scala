/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *  
 */

package csc.sectioncontrol.digitalswitch

import org.scalatest.matchers.MustMatchers
import akka.testkit.TestActorRef
import akka.camel.{ CamelMessage ⇒ Message }
import akka.pattern._
import csc.config.Path
import akka.actor.ActorSystem
import akka.util.duration._
import akka.util.Timeout
import org.scalatest.{ WordSpec, BeforeAndAfterAll }
import csc.curator.utils.{ CuratorToolsImpl, Curator }
import csc.curator.CuratorTestServer
import csc.akkautils.DirectLoggingAdapter

class DetectDigitalInputTest extends WordSpec with MustMatchers with BeforeAndAfterAll with CuratorTestServer {
  implicit val testSystem = ActorSystem("DetectDigitalInputTest")
  implicit val timeout = Timeout(5 seconds)

  val systemId = "a2"
  val rootPath = Path("/ctes")
  val pathToSystem = rootPath / "systems" / systemId

  var detector: TestActorRef[DetectDigitalInput] = _
  protected var curator: Curator = _

  val door_switchA = DigitalInputConfig(
    componentType = "192.168.127.253/DI1/Door",
    expression = "<\\d*>\\S* ( \\d|\\d{1,2}) \\d{1,2}:\\d{1,2}:\\d{1,2} 192.168.127.253 INFO:DI 2 transition \\((On|Off) -> (Off|On)\\)",
    openSignal = "On",
    openEventType = "Open",
    openDescription = "it is Open",
    closedDescription = "it is Closed",
    closedEventType = "Closed")

  val temperature_switchA = DigitalInputConfig(
    componentType = "192.168.127.253/DI2/Temperature",
    expression = "<\\d*>\\S* ( \\d|\\d{1,2}) \\d{1,2}:\\d{1,2}:\\d{1,2} 192.168.127.253 INFO:DI 1 transition \\((On|Off) -> (Off|On)\\)",
    openDescription = "it is OutOfRange",
    closedDescription = "it is InRange",
    openSignal = "On",
    openEventType = "OutOfRange",
    closedEventType = "InRange")

  val door_switchB = DigitalInputConfig(
    componentType = "192.168.127.100/DI1/Door",
    expression = "<\\d*>\\S* ( \\d|\\d{1,2}) \\d{1,2}:\\d{1,2}:\\d{1,2} 192.168.127.100 INFO:DI 1 transition \\((On|Off) -> (Off|On)\\)",
    openDescription = "it is Closed",
    closedDescription = "it is Open",
    openSignal = "Off",
    openEventType = "Closed",
    closedEventType = "Open")

  val temperature_switchB = DigitalInputConfig(
    componentType = "192.168.127.100/DI2/Temperature",
    expression = "<\\d*>\\S* ( \\d|\\d{1,2}) \\d{1,2}:\\d{1,2}:\\d{1,2} 192.168.127.100 INFO:DI 2 transition \\((On|Off) -> (Off|On)\\)",
    openDescription = "it is OutOfRange",
    closedDescription = "it is InRange",
    openSignal = "Off",
    openEventType = "OutOfRange",
    closedEventType = "InRange")

  val pathToDi = pathToSystem / "digital-inputs"

  override def beforeEach() {
    super.beforeEach()
    //curator = new CuratorToolsImpl(clientScope, new DirectLoggingAdapter(this.getClass.getName))
    curator = Curator.createCurator(caller = this, system = testSystem, zkServers = zkServers)

    curator.put(pathToDi, DigitalInput(true, List(
      door_switchA,
      temperature_switchA,
      door_switchB,
      temperature_switchB)))

    //curator = Curator.createCurator(caller = this, system = testSystem, zkServers = zkServers)
    detector = TestActorRef(new DetectDigitalInput(
      uri = "mina:udp://localhost:12400?sync=false",
      systemId = systemId,
      curator = curator))
    enableDigitalInput()
  }

  override def afterEach() {
    curator.curator.map(_.close())
    detector.stop()
    super.afterEach()
  }

  override def afterAll() {
    testSystem.shutdown()
    super.afterAll()
  }

  private def enableDigitalInput(enable: Boolean = true) {
    curator.getVersioned[DigitalInput](pathToDi).map {
      di ⇒
        curator.set(pathToDi, di.data.copy(isEnabled = enable), di.version)
    }
  }

  "DetectDigitalInput" must {
    "not send a systemevent when Digital Input is disabled" in {
      enableDigitalInput(false)
      val message = "<172>Jan 01 01:58:39 192.168.127.253 INFO:DI 1 transition (On -> Off)"
      detector ? Message(message, Map())
      val events = getEvents
      events.size must be(0)
    }
    "set hour to '9' instead of '09'" in {
      val message = "<172>Jan 09 01:58:39 192.168.127.253 INFO:DI 2 transition (Off -> On)"
      val time = System.currentTimeMillis()
      detector ? Message(message, Map())
      val events = getEvents
      events.size must be(1)
      checkEvent(events(0), door_switchA, door_switchA.openDescription + "\n" + message, systemId, door_switchA.openEventType, time)
    }
    "determine that temperature exceeded range" in {
      val message = "<172>Jan 01 01:58:39 192.168.127.253 INFO:DI 1 transition (Off -> On)"
      val time = System.currentTimeMillis()
      detector ? Message(message, Map())
      val events = getEvents
      events.size must be(1)
      checkEvent(events(0), temperature_switchA, temperature_switchA.openDescription + "\n" + message, systemId, temperature_switchA.openEventType, time)
    }

    "determine that temperature is within range" in {
      val message = "<172>Jan 01 01:58:39 192.168.127.253 INFO:DI 1 transition (On -> Off)"
      val time = System.currentTimeMillis()
      detector ? Message(message, Map())
      val events = getEvents
      events.size must be(1)
      checkEvent(events(0), temperature_switchA, temperature_switchA.closedDescription + "\n" + message, systemId, temperature_switchA.closedEventType, time)
    }

    "determine that door is open" in {
      val message = "<172>Jan 01 01:58:39 192.168.127.253 INFO:DI 2 transition (Off -> On)"
      val time = System.currentTimeMillis()
      detector ? Message(message, Map())
      val events = getEvents
      events.size must be(1)
      checkEvent(events(0), door_switchA, door_switchA.openDescription + "\n" + message, systemId, door_switchA.openEventType, time)
    }

    "determine that door is closed" in {
      val message = "<172>Jan 01 01:58:39 192.168.127.253 INFO:DI 2 transition (On -> Off)"
      val time = System.currentTimeMillis()
      detector ? Message(message, Map())
      val events = getEvents
      events.size must be(1)
      checkEvent(events(0), door_switchA, door_switchA.closedDescription + "\n" + message, systemId, door_switchA.closedEventType, time)
    }

    "determine that door is open when openSignal is 'Off'" in {
      val message = "<172>Jan 01 01:58:39 192.168.127.100 INFO:DI 1 transition (On -> Off)"
      val time = System.currentTimeMillis()
      detector ? Message(message, Map())
      val events = getEvents
      events.size must be(1)
      checkEvent(events(0), door_switchB, door_switchB.openDescription + "\n" + message, systemId, door_switchB.openEventType, time)
    }

    "determine that temperature is open when openSignal is 'Off'" in {
      val message = "<172>Jan 01 01:58:39 192.168.127.100 INFO:DI 2 transition (On -> Off)"
      val time = System.currentTimeMillis()
      detector ? Message(message, Map())
      val events = getEvents
      events.size must be(1)
      checkEvent(events(0), temperature_switchB, temperature_switchB.openDescription + "\n" + message, systemId, temperature_switchB.openEventType, time)
    }

    "disregard non matching messages" in {
      val message = "Message that must be regarded"
      detector ? Message(message, Map())
      val events = getEvents
      events.size must be(0)
    }
  }

  private def checkEvent(systemEvent: SystemEvent, config: DigitalInputConfig, message: String, systemId: String, eventType: String, currentTimeMillis: Long) {
    systemEvent.componentId must be(config.componentType)
    systemEvent.eventType must be(eventType)
    systemEvent.reason must be(message)
    systemEvent.systemId must be(systemId)
    systemEvent.timestamp must be >= currentTimeMillis
    systemEvent.token must be(None)
    systemEvent.userId must be("system")
  }

  private def clearEvents() {
    curator.deleteRecursive(pathToSystem / "events")
  }

  private def getEvents: Seq[SystemEvent] = {
    val paths = curator.getChildren(pathToSystem / "events")
    paths.foldLeft(List[SystemEvent]()) {
      (list, path) ⇒
        curator.get[SystemEvent](path) match {
          case Some(event) ⇒ event :: list
          case _           ⇒ list
        }
    }
  }
}
