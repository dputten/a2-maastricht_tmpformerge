/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.digitalswitch

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import akka.camel.CamelMessage

class DetectDigitalInputCreateComponentNameTest extends WordSpec with MustMatchers {
  "CreateComponentName" must {
    "replace IP when starting with IP" in {
      val msg = CamelMessage("", Map(DetectDigitalInput.headerRemoteAddress -> "/127.0.0.1:2000"))
      DetectDigitalInput.createComponentName("xxx.xxx.xxx.xxx/DI2/Temperature", msg) must be("127.0.0.1/DI2/Temperature")
    }
    "replace IP when ending with IP" in {
      val msg = CamelMessage("", Map(DetectDigitalInput.headerRemoteAddress -> "/127.0.0.1:2000"))
      DetectDigitalInput.createComponentName("Temperature/xxx.xxx.xxx.xxx", msg) must be("Temperature/127.0.0.1")
    }
    "replace IP when it is in the middle" in {
      val msg = CamelMessage("", Map(DetectDigitalInput.headerRemoteAddress -> "/127.0.0.1:2000"))
      DetectDigitalInput.createComponentName("Input/xxx.xxx.xxx.xxx/DI2/Temperature", msg) must be("Input/127.0.0.1/DI2/Temperature")
    }
    "not replace when IP header is missing" in {
      val msg = CamelMessage("", Map())
      DetectDigitalInput.createComponentName("xxx.xxx.xxx.xxx/DI2/Temperature", msg) must be("xxx.xxx.xxx.xxx/DI2/Temperature")
    }
    "not replace when IP has different format" in {
      val msg = CamelMessage("", Map(DetectDigitalInput.headerRemoteAddress -> "127.0.0.1:2000"))
      DetectDigitalInput.createComponentName("xxx.xxx.xxx.xxx/DI2/Temperature", msg) must be("xxx.xxx.xxx.xxx/DI2/Temperature")
    }
    "not replace when name doesn't contain ip template" in {
      val msg = CamelMessage("", Map(DetectDigitalInput.headerRemoteAddress -> "/127.0.0.1:2000"))
      DetectDigitalInput.createComponentName("xxx.xxx.xxx/DI2/Temperature", msg) must be("xxx.xxx.xxx/DI2/Temperature")
    }
  }

}