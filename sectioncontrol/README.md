# SectionControl
## Building SectionControl

Clone the following repositories for the `JDK` and `SBT` at the same level as the application repositories:

### JDK
```shell
$ git clone ssh://cscappamd911/data/git/blessed/COMMON/java/Linux_x32/jdk1.6.0_31
```

### SBT
```shell
$ git clone ssh://cscappamd911/data/git/blessed/COMMON/sbt/sbt_0.12.3
```

Now modify you local hostfile `/etc/hosts`

```
# /etc/hosts

127.0.0.1 localhost repo1.maven.org download.java.net scala-tools.org repo.typesafe.com
```

Since we don't use the proxy for our command line, `SBT` cannot reach this URLs. If you don't change your hostfile it can take a long time for SBT to discover it cannot reach these servers and times out.

### Create a buildscript (`sbt.sh`)

```shell
# sbt.sh
 
#!/bin/bash

# Disable the proxy for SBT to force lookup in Nexus
export http_proxy=

# set SBT_HOME to right directory (i.e. to the repo you just cloned)
SBT_HOME='/path/to/sbt-repo/sbt_0.12.3'

# You may change/omit the option -Xms256m; it limits the amount of memory Java will use
JAVA_OPTS="-Dsbt.repository.config=$SBT_HOME/repositories_2.9.1_0.11.2 -Dsbt.override.build.repos=true -Dsbt.boot.properties=$SBT_HOME/sbt.boot.properties_2.9.1_0.11.2 -Xms368m -Xmx368m"
export JAVA_OPTS
export JAVA_HOME=/path/to/jdk-repo/jdk1.6.0_31
export PATH=$PATH:${JAVA_HOME}/bin
$SBT_HOME/sbt/bin/sbt -verbose "$@"

```

Now change the permissions your customized `sbt.sh`

```shell
$ chmod 777 sbt.sh
```

Finally you can start a build as follows:

```shell
./sbt.sh clean update dist
```

## Building SectionControl Web
In order to build sectioncontrol-web `SBT` version 0.11.3 should be used. `SBT` 0.11.3 can be used by updating the `sbt.repository.config` and the `sbt.boot.properties` in the `sbt.sh` build script. 

Now start the build from the directory `/sectioncontrol/sectioncontrol-web` and use `../../sbt.sh`

