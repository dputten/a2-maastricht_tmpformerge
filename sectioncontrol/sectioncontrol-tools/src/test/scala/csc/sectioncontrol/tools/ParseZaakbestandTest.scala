package csc.sectioncontrol.tools

import java.io.File
import java.text.SimpleDateFormat
import org.apache.commons.io.FileUtils
import org.scalatest.matchers.MustMatchers
import org.scalatest.WordSpec
import collection.JavaConversions._

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

class ParseZaakbestandTest extends WordSpec with MustMatchers {
  "parser" must {
    "skip header" in {
      val lines = Seq("# Trajectcontrolesysteem CTCS2 A2 Links\n")
      ParseZaakbestand.parseLines(lines).size must be(0)
    }
    "skip footer" in {
      val lines = Seq("ac386abc6029ea8be125b2b4e07c55df\n")
      ParseZaakbestand.parseLines(lines).size must be(0)
    }
    "skip footer without new line" in {
      val lines = Seq("ac386abc6029ea8be125b2b4e07c55df")
      ParseZaakbestand.parseLines(lines).size must be(0)
    }
    "read violation" in {
      val lines = Seq(""". 0526 20131022 1052 BH886        VA 089     100 J A1  54.9L 104840.022  49.9L 105201.804  5014 7 Trajectcontrole A2 links                                                                    0002 1 N KL06 20 02739 Maarssen                  Abcoude                   4 01 EnforceNL 515eed9eb9cc3e488aa376974e2aceda,SectionControl-Matcher 62faab38be85f42e65129a10670304cf,vehicleregistration 8b427d3091c87c0ffe110a521f628ae1,Configuration XTsnJvmN/+nFIBUOGEHbAbZ3yrLwc61cZ1tF+bfFVxI=, \n""")
      val parsedList = ParseZaakbestand.parseLines(lines)
      parsedList.size must be(1)
      parsedList.head must be(new ParsedRegistration("0526", "20131022", "1052", "BH886"))
    }
    "read violation without new line" in {
      val lines = Seq(""". 0526 20131022 1052 BH886        VA 089     100 J A1  54.9L 104840.022  49.9L 105201.804  5014 7 Trajectcontrole A2 links                                                                    0002 1 N KL06 20 02739 Maarssen                  Abcoude                   4 01 EnforceNL 515eed9eb9cc3e488aa376974e2aceda,SectionControl-Matcher 62faab38be85f42e65129a10670304cf,vehicleregistration 8b427d3091c87c0ffe110a521f628ae1,Configuration XTsnJvmN/+nFIBUOGEHbAbZ3yrLwc61cZ1tF+bfFVxI=, """)
      val parsedList = ParseZaakbestand.parseLines(lines)
      parsedList.size must be(1)
      parsedList.head must be(new ParsedRegistration("0526", "20131022", "1052", "BH886"))
    }
    "read a real file" in {
      val lines = FileUtils.readLines(new File("sectioncontrol-tools/src/test/resources/zaakhand.txt"))
      val parsedList = ParseZaakbestand.parseLines(lines)
      parsedList.size must be(153)
      parsedList.contains(new ParsedRegistration("0526", "20131022", "1052", "BH886")) must be(true) //first
      parsedList.contains(new ParsedRegistration("0578", "20131022", "1305", "KA-AE-6018")) must be(true) //somewhere in the middle
      parsedList.contains(new ParsedRegistration("0678", "20131022", "1841", "7052GYS")) must be(true) //last
    }
  }

  val dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss.SSS")
  "epoch" must {
    "calculated correctly 1" in {
      val startDate = dateFormat.parse("2013/10/10 10:20:00.000").getTime
      val reg = new ParsedRegistration(nr = "12", date = "20131010", time = "1020", license = "1234")
      reg.getEpochTime() must be(startDate)
    }
    "calculated correctly 2" in {
      val startDate = dateFormat.parse("2013/10/27 00:00:00.000").getTime
      val reg = new ParsedRegistration(nr = "12", date = "20131027", time = "0000", license = "1234")
      reg.getEpochTime() must be(startDate)
    }
    "calculated correctly 3" in {
      val startDate = dateFormat.parse("2013/01/01 23:59:00.000").getTime
      val reg = new ParsedRegistration(nr = "12", date = "20130101", time = "2359", license = "1234")
      reg.getEpochTime() must be(startDate)
    }
  }
}