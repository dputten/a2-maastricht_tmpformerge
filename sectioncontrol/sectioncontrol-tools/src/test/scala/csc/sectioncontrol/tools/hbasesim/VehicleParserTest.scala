/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.tools.hbasesim

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import org.apache.commons.io.FileUtils
import java.io.File

/**
 * Test the parser.
 */
class VehicleParserTest extends WordSpec with MustMatchers {

  "vehicle parser" must {

    "read comment line" in {
      var parserOutput = VehicleParser.parseScript("#100 KS_VERS_PROT=10 KS_LOCATION=0123456\t " +
        "KS_VERS_HW=3244AD1")
      parserOutput must have size (1)
      val comment = parserOutput.head
      comment match {
        case script: ScriptLine ⇒ script.command must be(SimConstants.COMMENT)
        case _                  ⇒ fail("Unexpected type")
      }
    }
    "read empty comment line" in {
      var parserOutput = VehicleParser.parseScript("#")
      parserOutput must have size (1)
      val comment = parserOutput.head
      comment match {
        case script: ScriptLine ⇒ script.command must be(SimConstants.COMMENT)
        case _                  ⇒ fail("Unexpected type")
      }
    }
    "read configuration line" in {
      var parserOutput = VehicleParser.parseScript("CONFIG-START\nLANE GANTRY=amstelveen\nCONFIG-END")
      parserOutput must have size (1)
      val comment = parserOutput.head
      comment match {
        case config: ConfigLine ⇒ config.options must be(Map("GANTRY" -> "amstelveen"))
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
    }
    "read script line" in {
      var parserOutput = VehicleParser.parseScript("SCRIPT-START\n1000 LANE-REGISTRATION IMAGE=test\nSCRIPT-END\n")
      parserOutput must have size (1)
      val comment = parserOutput.head
      comment match {
        case script: ScriptLine ⇒ {
          script.command must be("LANE-REGISTRATION")
          script.options must be(Map("IMAGE" -> "test"))
        }
        case _ ⇒ fail("Unexpected type")
      }
    }
    "read multiplelines" in {
      var parserOutput = VehicleParser.parseScript("#test script\nCONFIG-START\n#first config\nLANE LANE-ID=100 NAME=lane1 GANTRY=gaasperdam SYSTEM=A9 GPS-LONG=4.961565 GPS-LAT=52.304753\nCONFIG-END\nSCRIPT-START\n1000 LANE-REGISTRATION LANE-ID=100 SPEED=80 LENGTH=4.20 IMAGE=~/data/image.tif\nSCRIPT-END\n#Script done\n")
      parserOutput must have size (4)
      var record = parserOutput(0)
      record match {
        case script: ScriptLine ⇒ script.command must be(SimConstants.COMMENT)
        case _                  ⇒ fail("Unexpected type")
      }
      record = parserOutput(1)
      record match {
        case config: ConfigLine ⇒ {
          config.config must be(SimConstants.CONFIG_LANE)
        }
        case obj: AnyRef ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(2)
      record match {
        case script: ScriptLine ⇒ {
          script.command must be("LANE-REGISTRATION")
        }
        case _ ⇒ fail("Unexpected type")
      }
      record = parserOutput(3)
      record match {
        case script: ScriptLine ⇒ script.command must be(SimConstants.COMMENT)
        case _                  ⇒ fail("Unexpected type")
      }
    }
    "read commands" in {
      var parserOutput = VehicleParser.parseScript("CONFIG-START\nLANE LANE-ID=100 NAME=lane1 GANTRY=gaasperdam SYSTEM=A9 GPS-LONG=4.961565 GPS-LAT=52.304753\nCONFIG-END\nSCRIPT-START\n1000 LANE-REGISTRATION LANE-ID=200 SPEED=100 LICENSE=12XXX3 COUNTRY=NL TIME-YELLOW=200 TIME-RED=100 LICENSE-CONF=85 LENGTH-CONF=90 SPEED-CONF=75 COUNTRY-CONF=80 IMAGE=/home/rbakker/data/image.tif\n1000 WAIT\n1000 WAIT INPUT\nSCRIPT-END\n")
      parserOutput must have size (4)
      var record = parserOutput(0)
      record match {
        case config: ConfigLine ⇒ {
          config.config must be(SimConstants.CONFIG_LANE)
        }
        case obj: AnyRef ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(1)
      record match {
        case script: ScriptLine ⇒ {
          script.command must be(SimConstants.LANE_REGISTRATION)
        }
        case _ ⇒ fail("Unexpected type")
      }
      record = parserOutput(2)
      record match {
        case script: ScriptLine ⇒ {
          script.command must be(SimConstants.WAIT)
          script.options must have size (0)
        }
        case _ ⇒ fail("Unexpected type")
      }
      record = parserOutput(3)
      record match {
        case script: ScriptLine ⇒ {
          script.command must be(SimConstants.WAIT)
          script.options must have size (1)
        }
        case _ ⇒ fail("Unexpected type")
      }
    }
    "read all posibbilities" in {
      val scriptFile = new File(getClass.getClassLoader.getResource("simulate/script.txt").getPath)
      val script = FileUtils.readFileToString(scriptFile)
      val parserOutput = VehicleParser.parseScript(script)
      var record = parserOutput(0)
      record match {
        case script: ScriptLine ⇒ script.command must be(SimConstants.COMMENT)
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(1)
      record match {
        case script: ScriptLine ⇒ script.command must be(SimConstants.COMMENT)
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(2)
      record match {
        case config: ConfigLine ⇒ config.config must be(SimConstants.TIME)
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(3)
      record match {
        case config: ConfigLine ⇒ config.config must be(SimConstants.CHECKSUM)
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(4)
      record match {
        case config: ConfigLine ⇒ config.config must be(SimConstants.DEFAULTS)
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(5)
      record match {
        case config: ConfigLine ⇒ config.config must be(SimConstants.CONFIG_LANE)
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(6)
      record match {
        case config: ConfigLine ⇒ config.config must be(SimConstants.CONFIG_LANE)
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(7)
      record match {
        case config: ConfigLine ⇒ config.config must be(SimConstants.CONFIG_CORRIDOR)
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(8)
      record match {
        case script: ScriptLine ⇒ script.command must be(SimConstants.LANE_REGISTRATION)
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(9)
      record match {
        case script: ScriptLine ⇒ script.command must be(SimConstants.LANE_REGISTRATION)
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(10)
      record match {
        case script: ScriptLine ⇒ script.command must be(SimConstants.REPEAT_START)
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(11)
      record match {
        case script: ScriptLine ⇒ script.command must be(SimConstants.CORRIDOR_REGISTRATION)
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(12)
      record match {
        case script: ScriptLine ⇒ script.command must be(SimConstants.REPEAT_END)
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(13)
      record match {
        case script: ScriptLine ⇒ script.command must be(SimConstants.CALIBRATE)
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(14)
      record match {
        case script: ScriptLine ⇒ script.command must be(SimConstants.PROCESSED_UNTIL)
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
      record = parserOutput(15)
      record match {
        case script: ScriptLine ⇒ script.command must be(SimConstants.WAIT)
        case obj: AnyRef        ⇒ fail("Unexpected type " + obj.getClass.getName)
      }
    }
  }
}