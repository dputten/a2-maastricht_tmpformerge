/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.tools.hbasesim

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import collection.mutable.ListBuffer
import csc.sectioncontrol.messages.{ VehicleImageType, ValueWithConfidence_Float, ValueWithConfidence, Lane }
import java.util.{ Calendar, Date }

/**
 * Test the VehicleSimulator using a mockup for the store.
 */
class VehicleSimulatorTest extends WordSpec with MustMatchers {
  "Simulator" must {
    "create config" in {
      val configList = new ListBuffer[ConfigLine]
      configList += new ConfigLine(config = SimConstants.TIME, options = Map(SimConstants.MODE -> "REALTIME", SimConstants.START -> "10:00:00"))
      configList += new ConfigLine(config = SimConstants.CHECKSUM, options = Map(SimConstants.NMI_CERTIFICATE -> "TP1234", SimConstants.APPL_CHECKSUM -> "0987654321"))
      configList += new ConfigLine(config = SimConstants.DEFAULTS, options = Map(SimConstants.SPEED_CONF -> "30"))
      configList += new ConfigLine(config = SimConstants.DEFAULTS, options = Map(SimConstants.LICENSE_CONF -> "40"))
      configList += new ConfigLine(config = SimConstants.CONFIG_LANE, options = Map(
        SimConstants.LANE_ID -> "100",
        SimConstants.LANE_NAME -> "lane1",
        SimConstants.LANE_GANTRY -> "gaasperdam",
        SimConstants.LANE_SYSTEM -> "A9",
        SimConstants.LANE_GPSLONG -> "4.961565",
        SimConstants.LANE_GPSLAT -> "52.304753"))

      val mock = new StoreRegistrationMock()
      val config = VehicleSimulator.configureSimulator(configList.toList, mock)
      config.timeCfg.realMode must be(true)
      val time = Calendar.getInstance()
      time.set(Calendar.HOUR_OF_DAY, 10)
      time.set(Calendar.MINUTE, 0)
      time.set(Calendar.SECOND, 0)
      time.set(Calendar.MILLISECOND, 0)
      config.timeCfg.startTime must be(time.getTime)
      config.applChecksum must be("0987654321")
      config.nmiCertificate must be("TP1234")
      config.defaults must be(Map(SimConstants.SPEED_CONF -> "30", SimConstants.LICENSE_CONF -> "40"))
      config.entities must be(Map("LANE100" -> new ConfigEntity(lane = new Lane(laneId = "A9-gaasperdam-lane1", name = "lane1", gantry = "gaasperdam", system = "A9", sensorGPS_longitude = 4.961565, sensorGPS_latitude = 52.304753))))
      config.store must be(mock)
    }
    "execute script registration" in {
      val scriptImage = getClass.getClassLoader.getResource("simulate/output.jpg").getPath
      val mock = new StoreRegistrationMock()
      val now = new Date()
      val config = new ConfigGlobal(nmiCertificate = "TP1234",
        applChecksum = "0987654321",
        defaults = Map(),
        entities = Map("LANE100" -> new ConfigEntity(lane = new Lane(laneId = "A9-gaasperdam-lane1", name = "lane1", gantry = "gaasperdam", system = "A9", sensorGPS_longitude = 4.961565, sensorGPS_latitude = 52.304753))),
        store = mock,
        timeCfg = new TimeConfig(false, now))

      val scriptLine = new ScriptLine(delay = 1000, command = SimConstants.LANE_REGISTRATION, options = Map(
        SimConstants.LANE_ID -> "100",
        SimConstants.SPEED -> "100",
        SimConstants.LENGTH -> "4.7",
        SimConstants.LICENSE -> "12XXX3",
        SimConstants.COUNTRY -> "NL",
        SimConstants.TIME_YELLOW -> "200",
        SimConstants.TIME_RED -> "100",
        SimConstants.LICENSE_CONF -> "85",
        SimConstants.LENGTH_CONF -> "90",
        SimConstants.SPEED_CONF -> "75",
        SimConstants.COUNTRY_CONF -> "80",
        SimConstants.IMAGE -> scriptImage))
      val cmd = new RepeatCommandLine(nrExec = 1, startLine = 2, scriptLine = scriptLine, commands = Seq())
      //create container
      val scripts = new RepeatCommandLine(1, 1, new ScriptLine(0, SimConstants.SCRIPT_START, Map()), Seq(cmd))

      val endtime = VehicleSimulator.executeCommand(config = config, command = scripts, curTime = now.getTime)
      endtime must be(now.getTime + scriptLine.delay)
      mock.calibration must have size (0)
      mock.registrations must have size (1)
      val reg = mock.registrations.head.createImageFilledVehicleMetadata()
      reg.eventTimestamp must be(now.getTime + scriptLine.delay)
      reg.applChecksum must be("0987654321")
      reg.NMICertificate must be("TP1234")
      reg.category must be(None)
      reg.country must be(Some(new ValueWithConfidence[String]("NL", 80)))
      reg.speed must be(Some(new ValueWithConfidence_Float(100, 75)))
      reg.length must be(Some(new ValueWithConfidence_Float(4.7F, 90)))
      reg.license must be(Some(new ValueWithConfidence[String]("12XXX3", 85)))
      val redImage = reg.getImage(VehicleImageType.RedLight).get
      redImage.timeYellow must be(Some(200L))
      redImage.timeRed must be(Some(100L))
    }
    "execute script calibration with result OK" in {
      val mock = new StoreRegistrationMock()

      val lane1 = new Lane(laneId = "A9-gaasperdam-lane1", name = "lane1", gantry = "gaasperdam", system = "A9", sensorGPS_longitude = 4.961565, sensorGPS_latitude = 52.304753)
      val lane2 = new Lane(laneId = "A9-gaasperdam-lane2", name = "lane2", gantry = "gaasperdam", system = "A9", sensorGPS_longitude = 4.961565, sensorGPS_latitude = 52.304753)
      val now = new Date()
      val config = new ConfigGlobal(nmiCertificate = "TP1234",
        applChecksum = "0987654321",
        defaults = Map(),
        entities = Map("LANE100" -> new ConfigEntity(lane = lane1),
          "LANE200" -> new ConfigEntity(lane = lane2),
          "CORRIDOR100" -> new ConfigEntity(lane = lane1, secondLane = Some(lane2), length = Some(2000D))),
        store = mock,
        timeCfg = new TimeConfig(false, now))
      val scriptLine = new ScriptLine(delay = 1000, command = SimConstants.CALIBRATE, options = Map(
        SimConstants.CORRIDOR_ID -> "100",
        SimConstants.OFFICER_CODE -> "Blabla",
        SimConstants.RESULT -> "OK"))
      val cmd = new RepeatCommandLine(nrExec = 1, startLine = 2, scriptLine = scriptLine, commands = Seq())
      //create container
      val scripts = new RepeatCommandLine(1, 1, new ScriptLine(0, SimConstants.SCRIPT_START, Map()), Seq(cmd))

      val endtime = VehicleSimulator.executeCommand(config = config, command = scripts, curTime = now.getTime)
      endtime must be(now.getTime + scriptLine.delay)
      mock.registrations must have size (0)
      mock.calibration must have size (1)
      val cal = mock.calibration.head
      cal.corridorId must be("100")
      cal.lanes must have size (2)
      cal.time.getTime must be(now.getTime + scriptLine.delay)
      cal.reportingOfficerCode must be("Blabla")
      cal.success must be(true)
    }
    "execute script calibration with result NOK" in {
      val scriptImage = getClass.getClassLoader.getResource("simulate/output.jpg").getPath
      val mock = new StoreRegistrationMock()

      val lane1 = new Lane(laneId = "A9-gaasperdam-lane1", name = "lane1", gantry = "gaasperdam", system = "A9", sensorGPS_longitude = 4.961565, sensorGPS_latitude = 52.304753)
      val lane2 = new Lane(laneId = "A9-gaasperdam-lane2", name = "lane2", gantry = "gaasperdam", system = "A9", sensorGPS_longitude = 4.961565, sensorGPS_latitude = 52.304753)
      val now = new Date()
      val config = new ConfigGlobal(nmiCertificate = "TP1234",
        applChecksum = "0987654321",
        defaults = Map(),
        entities = Map("LANE100" -> new ConfigEntity(lane = lane1),
          "LANE200" -> new ConfigEntity(lane = lane2),
          "CORRIDOR100" -> new ConfigEntity(lane = lane1, secondLane = Some(lane2), length = Some(2000D))),
        store = mock,
        timeCfg = new TimeConfig(false, now))

      val scriptLine = new ScriptLine(delay = 1000, command = SimConstants.CALIBRATE, options = Map(
        SimConstants.CORRIDOR_ID -> "100",
        SimConstants.OFFICER_CODE -> "Blabla",
        SimConstants.RESULT -> "NOK"))
      val cmd = new RepeatCommandLine(nrExec = 1, startLine = 2, scriptLine = scriptLine, commands = Seq())
      //create container
      val scripts = new RepeatCommandLine(1, 1, new ScriptLine(0, SimConstants.SCRIPT_START, Map()), Seq(cmd))

      val endtime = VehicleSimulator.executeCommand(config = config, command = scripts, curTime = now.getTime)
      endtime must be(now.getTime + scriptLine.delay)
      mock.registrations must have size (0)
      mock.calibration must have size (1)
      val cal = mock.calibration.head
      cal.corridorId must be("100")
      cal.lanes must have size (2)
      cal.time.getTime must be(now.getTime + scriptLine.delay)
      cal.reportingOfficerCode must be("Blabla")
      cal.success must be(false)
    }
    "execute script calibration with 1 lane corridor" in {
      val mock = new StoreRegistrationMock()

      val lane1 = new Lane(laneId = "A9-gaasperdam-lane1", name = "lane1", gantry = "gaasperdam", system = "A9", sensorGPS_longitude = 4.961565, sensorGPS_latitude = 52.304753)
      val lane2 = new Lane(laneId = "A99-gaasperdam-lane2", name = "lane2", gantry = "gaasperdam", system = "A99", sensorGPS_longitude = 4.961565, sensorGPS_latitude = 52.304753)
      val now = new Date()
      val config = new ConfigGlobal(nmiCertificate = "TP1234",
        applChecksum = "0987654321",
        defaults = Map(),
        entities = Map("LANE100" -> new ConfigEntity(lane = lane1),
          "LANE200" -> new ConfigEntity(lane = lane2),
          "CORRIDOR100" -> new ConfigEntity(lane = lane1, secondLane = Some(lane2), length = Some(1000D)),
          "CORRIDOR200" -> new ConfigEntity(lane = lane1, secondLane = None, length = Some(2000D))),
        store = mock,
        timeCfg = new TimeConfig(false, now))

      val scriptLine = new ScriptLine(delay = 1000, command = SimConstants.CALIBRATE, options = Map(
        SimConstants.CORRIDOR_ID -> "200",
        SimConstants.OFFICER_CODE -> "Blabla",
        SimConstants.RESULT -> "OK"))
      val cmd = new RepeatCommandLine(nrExec = 1, startLine = 2, scriptLine = scriptLine, commands = Seq())
      //create container
      val scripts = new RepeatCommandLine(1, 1, new ScriptLine(0, SimConstants.SCRIPT_START, Map()), Seq(cmd))

      val endtime = VehicleSimulator.executeCommand(config = config, command = scripts, curTime = now.getTime)
      endtime must be(now.getTime + scriptLine.delay)
      mock.registrations must have size (0)
      mock.calibration must have size (1)
      val cal = mock.calibration.head
      cal.corridorId must be("200")
      cal.lanes must have size (1)
      cal.lanes.head must be(lane1)
      cal.time.getTime must be(now.getTime + scriptLine.delay)
      cal.reportingOfficerCode must be("Blabla")
      cal.success must be(true)
    }

  }

}

/**
 * The mockup for the registration store
 */
class StoreRegistrationMock extends StoreRegistration {
  case class Calibration(corridorId: String, lanes: Seq[Lane], time: Date, reportingOfficerCode: String, success: Boolean, serialNr: String)
  val registrations = new ListBuffer[ImagesSupport]()
  val calibration = new ListBuffer[Calibration]()

  def init() {
    registrations.clear()
    calibration.clear()
  }

  def close() {}

  def storeRegistration(images: ImagesSupport) {
    registrations += images
  }

  def storeCalibration(corridorId: String, lanes: Seq[Lane], time: Date, reportingOfficerCode: String, success: Boolean, serialNr: String) {
    calibration += new Calibration(corridorId, lanes, time, reportingOfficerCode, success, serialNr)
  }
}

