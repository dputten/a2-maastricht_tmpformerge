#test script
#
CONFIG-START
TIME MODE=REALTIME START=12:00:00
CHECKSUM NMI-CERT=TP668 APPL-CHECKSUM=447dcf9c69d7975feca0b7e28d47120f
DEFAULTS LICENSE-CONF=85 LENGTH-CONF=90 SPEED-CONF=75 COUNTRY-CONF=80
SITE-BUFFER HOSTNAME=127.0.0.1 HOSTPORT=12321

LANE LANE-ID=1 NAME=lane1 GANTRY=gaasperdam SYSTEM=A9 GPS-LONG=4.961565 GPS-LAT=52.304753 IMAG-DEST=/var/tmp/irose/lane1/ CAMERA_ID=A1
LANE LANE-ID=2 NAME=lane1 GANTRY=amstelveen SYSTEM=A9 GPS-LONG=4.961565 GPS-LAT=52.304753 IMAG-DEST=/var/tmp/irose/lane1/ CAMERA_ID=A2
CORRIDOR CORRIDOR-ID=Section1 START-LANE-ID=100 END-LANE-ID=200 CORRIDOR-LENGTH=2345.6

CONFIG-END

SCRIPT-START
1000 LANE-REGISTRATION LANE-ID=1 SPEED=82  LENGTH=4.20 CATEGORY=CAR LICENSE=12XXX3 COUNTRY=NL TIME-YELLOW=2000 TIME-RED=4000 LICENSE-CONF=85 SPEED-CONF=75 COUNTRY-CONF=80 IMAGE=/home/gkruizin/data/image.jpg
1000 LANE-REGISTRATION LANE-ID=2 SPEED=100 LENGTH=4.70 CATEGORY=CAR LICENSE=12XXX3 COUNTRY=NL TIME-YELLOW=5000 TIME-RED=1000 LICENSE-CONF=85 SPEED-CONF=75 COUNTRY-CONF=80 IMAGE=/home/gkruizin/data/image.jpg
REPEAT-START REPEAT-NUMBER=20
1000 CORRIDOR-REGISTRATION CORRIDOR-ID=Section1 LENGTH=5.20 SPEED=100 LICENSE=12XXX3 COUNTRY=NL TIME-YELLOW=200 TIME-RED=100 LICENSE-CONF=85 LENGTH-CONF=90 SPEED-CONF=75 COUNTRY-CONF=80 IMAGE=/home/rbakker/data/image.tif
REPEAT-END
1000 CALIBRATE EXCLUDE-SYSTEM=A9 OFFICER_CODE=bla RESULT=OK
10000 WAIT
SCRIPT-END
