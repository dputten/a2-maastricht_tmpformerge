/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package hbasesim

import java.io.File

import csc.sectioncontrol.messages.{Lane, VehicleMetadata, _}
import csc.sectioncontrol.tools.hbasesim.ImagesSupport
import csc.util.test.FileTestUtils._
import csc.util.test.FuzzyImageMatcher
import org.apache.commons.io.FileUtils
import org.scalatest.matchers.MustMatchers
import org.scalatest.{BeforeAndAfterAll, WordSpec}

/**
 * Test to see if the created images are correct
 */
class ImagesSupportTest extends WordSpec with MustMatchers with BeforeAndAfterAll {

  "ImagesSupport" must {
    "create correct images" in {
      val baseImage = new File(inputDir, "baseImage.jpg").getAbsolutePath
      val vehicleImages = new ImagesSupport(baseImage, vehicleMetadata, 100, Some(20), Some(40))

      val imageNames = Seq("license.jpg", "overview.jpg", "overviewmasked.jpg", "red.jpg",
        "overviewRedLight.jpg", "overviewSpeed.jpg", "measure2speed.jpg")
      val imageByteArrays = Seq(vehicleImages.license, vehicleImages.overview, vehicleImages.overviewMasked,
        vehicleImages.redlight, vehicleImages.overviewRedLight, vehicleImages.overviewSpeed, vehicleImages.measure2Speed)

      imageNames.zip(imageByteArrays).foreach {
        case (imageName, byteArray) ⇒
          val imageFile = new File(outputDir, imageName)
          FileUtils.writeByteArrayToFile(imageFile, byteArray)

          val referenceFile = new File(referenceDir, imageName)
          imagesMustBeAlike(imageFile, referenceFile)
      }
    }
  }

  lazy val vehicleMetadata = VehicleMetadata(
    lane = new Lane(laneId = "lane1-gantry-system", name = "lane1", gantry = "gantry", system = "system", sensorGPS_longitude = 52.4, sensorGPS_latitude = 5.4),
    eventId = "TEST_ID_100",
    eventTimestamp = 20000,
    eventTimestampStr = Some("10/10/2010 10:10:10.100 UTC"),
    license = Some(new ValueWithConfidence[String]("12abc3", 95)),
    rawLicense = None,
    length = Some(new ValueWithConfidence_Float(4.7F, 80)),
    category = None,
    speed = Some(new ValueWithConfidence_Float(80F, 90)),
    country = None,
    images = Seq(),
    NMICertificate = "TP1234",
    applChecksum = "0123456789ABCDEF0123456789ABCDEF",
    serialNr = "SN1234")

  val names = Seq("test1", "test2")
  val baseDir = new File(getClass.getClassLoader.getResource("ImagesSupportTest").getPath)
  val inputDir = new File(baseDir, "input")
  val outputDir = new File(baseDir, "output")
  val referenceDir = new File(baseDir, "reference")

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    deleteDirectory(outputDir) //delete upfront, not after, so we can keep the generated images for failed tests
    outputDir.mkdirs()
  }

  def imagesMustBeAlike(testImageFile: File, refImageFile: File, threshold: Int = 65) =
    FuzzyImageMatcher.check(testImageFile, refImageFile, threshold)

}