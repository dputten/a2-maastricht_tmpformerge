/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.tools.hbasesim

import java.util.Date

import csc.hbase.utils.HBaseTableKeeper
import csc.hbase.utils.testframework.HBaseTestFramework
import csc.sectioncontrol.messages.{Lane, ValueWithConfidence, ValueWithConfidence_Float, VehicleMetadata}
import csc.sectioncontrol.storagelayer.hbasetables.{SelfTestResultTable, VehicleTable}
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

/**
 * Test if the StoreRegistrationHBase stores registrations and calibration correctly in HBase
 */
class StoreRegistrationTest extends WordSpec with MustMatchers with HBaseTestFramework {

  val regTable = "vehicle"
  val calResultTable = "SelftestResult"
  val calImageTable = "SelftestImage"

  var store: Option[StoreRegistrationHBase] = None
  val tableKeeper = new HBaseTableKeeper {}

  override protected def beforeAll() {
    super.beforeAll()

    store = Some(new StoreRegistrationHBase(tableName = regTable, calImageTable = calImageTable, calResultTable = calResultTable, hbaseConfig = testUtil.getConfiguration))
    store.foreach(_.init())
  }

  override protected def afterAll() {
    try {
      tableKeeper.closeTables()
      store.foreach(_.close())
    } finally {
      super.afterAll()
    }
  }

  "StoreRegistration" must {
    "add a registration record" in {
      val scriptFile = getClass.getClassLoader.getResource("simulate/output.jpg").getPath

      val vehicleReg = VehicleMetadata(
        lane = new Lane(laneId = "lane1-gantry-system", name = "lane1", gantry = "gantry", system = "system", sensorGPS_longitude = 52.4, sensorGPS_latitude = 5.4),
        eventId = "TEST_ID_100",
        eventTimestamp = 20000,
        eventTimestampStr = Some("10/10/2010 10:10:10.100 UTC"),
        license = Some(new ValueWithConfidence[String]("12abc3", 95)),
        rawLicense = None,
        length = Some(new ValueWithConfidence_Float(4.7F, 80)),
        category = None,
        speed = Some(new ValueWithConfidence_Float(80F, 90)),
        country = None,
        images = Seq(),
        NMICertificate = "TP1234",
        applChecksum = "0123456789ABCDEF0123456789ABCDEF",
        serialNr = "SN1234")

      val vehicleImages = new ImagesSupport(scriptFile, vehicleReg, 100, Some(20), Some(40))

      store.foreach(_.storeRegistration(vehicleImages))
      //check if registration is in database
      val reader = VehicleTable.makeReader(hbaseConfig, tableKeeper)
      val regList = reader.readRows(("system-gantry", 20000), ("system-gantry", 20001))
      val reg = regList.head
      val expectedReg = vehicleImages.createImageFilledVehicleMetadata()
      reg.applChecksum must be(expectedReg.applChecksum)
      reg.category must be(expectedReg.category)
      reg.country must be(expectedReg.country)
      reg.eventId must be(expectedReg.eventId)
      reg.eventTimestamp must be(expectedReg.eventTimestamp)
      reg.eventTimestampStr must be(expectedReg.eventTimestampStr)

      reg.images.size must be(expectedReg.images.size)
      for (i ← 0 until reg.images.size) {
        // imageType is sometimes a wrong type due to lift de-serialization need to use toString to overcome that bug
        reg.images(i).imageType.toString must be(expectedReg.images(i).imageType.toString)
        reg.images(i).checksum must be(expectedReg.images(i).checksum)
        reg.images(i).offset must be(expectedReg.images(i).offset)
        reg.images(i).timeRed must be(expectedReg.images(i).timeRed)
        reg.images(i).timestamp must be(expectedReg.images(i).timestamp)
        reg.images(i).timeYellow must be(expectedReg.images(i).timeYellow)
        reg.images(i).uri must be(expectedReg.images(i).uri)
      }
      reg.lane must be(expectedReg.lane)
      reg.length must be(expectedReg.length)
      reg.license must be(expectedReg.license)
      reg.NMICertificate must be(expectedReg.NMICertificate)
      reg.rawLicense must be(expectedReg.rawLicense)
      reg.serialNr must be(expectedReg.serialNr)
      reg.speed must be(expectedReg.speed)
      reg.vehicleCategory must be(expectedReg.vehicleCategory)

    }
    "add a calibration record" in {
      val corridorId = "S1"
      val lane = new Lane(laneId = "lane1-gantry-system", name = "lane1", gantry = "gantry", system = "system", sensorGPS_longitude = 52.4, sensorGPS_latitude = 5.4)
      val lane2 = new Lane(laneId = "lane2-gantry-system", name = "lane2", gantry = "gantry", system = "system", sensorGPS_longitude = 52.4, sensorGPS_latitude = 5.4)
      val now = new Date()
      store.foreach(_.storeCalibration(corridorId = corridorId, lanes = Seq(lane, lane2), time = now, reportingOfficerCode = "me", success = true, serialNr = "SN1234"))
      //check if calibration is in database

      val reader = SelfTestResultTable.makeReader(hbaseConfig, tableKeeper, lane.system)
      val result = reader.readRow(corridorId, now.getTime).get
      result.systemId must be(lane.system)
      result.reportingOfficerCode must be("me")
      result.nrCamerasTested must be(2)
      result.time must be(now.getTime)
      result.success must be(true)
      result.errors must be(Seq())
      val imageKeys = result.images
      imageKeys must have size (2)

    }
  }
}

