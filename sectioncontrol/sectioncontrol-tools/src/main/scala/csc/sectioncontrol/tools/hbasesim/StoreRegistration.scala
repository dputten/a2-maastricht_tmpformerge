/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.tools.hbasesim

import csc.sectioncontrol.decorate.Image
import net.liftweb.json.{ DefaultFormats, Serialization }
import org.apache.hadoop.hbase.client.{ RetriesExhaustedWithDetailsException, Put, HBaseAdmin, HTable }
import org.slf4j.LoggerFactory
import org.apache.hadoop.hbase.util.Bytes
import csc.sectioncontrol.storage.HBaseTableDefinitions
import collection.mutable.ListBuffer
import java.util.Date
import java.awt.image.BufferedImage
import org.apache.hadoop.conf.Configuration
import csc.hbase.utils.HBaseAdminTool
import csc.sectioncontrol.storage.SelfTestResult
import csc.sectioncontrol.messages.VehicleMetadata
import csc.sectioncontrol.storage.KeyWithTimeAndId
import scala.Some
import csc.sectioncontrol.messages.Lane
import csc.sectioncontrol.storagelayer.hbasetables.{ RowKeyDistributorFactory, SerializerFormats }

/**
 * Interface used by the Simulation, to be able to test with mockup
 */
trait StoreRegistration {
  def init()
  def close()

  /**
   * Store the vehicleRegisrtation
   * @param images contains the registration and the images
   */
  def storeRegistration(images: ImagesSupport)

  /**
   * Store a calibration
   * @param lanes for which lanes must the calibration contain images
   * @param time the time of the calibration
   * @param reportingOfficerCode the reporting officer code
   * @param success Did the calibration succeed of failed
   */
  def storeCalibration(corridorId: String, lanes: Seq[Lane], time: Date, reportingOfficerCode: String, success: Boolean, serialNr: String)
}

/**
 * Store the registrations and calibrations into HBase.
 *
 * @param tableName The vehicleRegistration table name
 * @param calImageTable The calibration images table name
 * @param calResultTable The calibration result table Name
 * @param hbaseConfig The hbase config
 */
class StoreRegistrationHBase(tableName: String, calImageTable: String, calResultTable: String, hbaseConfig: Configuration) extends StoreRegistration {
  lazy val log = LoggerFactory.getLogger(this.getClass.getName)
  implicit val formats = SerializerFormats.formats
  val rowKeyDistributerVehicle = RowKeyDistributorFactory.getDistributor(tableName)
  val rowKeyDistributerSelfTestResult = RowKeyDistributorFactory.getDistributor(calResultTable)
  val rowKeyDistributerSelfTestImage = RowKeyDistributorFactory.getDistributor(calImageTable)

  var vehicleTable: Option[HTable] = None
  var imageTable: Option[HTable] = None
  var resultTable: Option[HTable] = None

  val columnFamily = HBaseTableDefinitions.vehicleRecordColumnFamily.getBytes
  val attributeOverview = HBaseTableDefinitions.vehicleImageOverviewColumnName.getBytes

  val attributeOverviewSpeed = HBaseTableDefinitions.vehicleImageOverviewSpeedColumnName.getBytes
  val attributeOverviewRedLight = HBaseTableDefinitions.vehicleImageOverviewRedLightColumnName.getBytes

  val attributeOverviewMasked = HBaseTableDefinitions.vehicleImageOverviewMaskedColumnName.getBytes

  val attributeLicense = HBaseTableDefinitions.vehicleImageLicenseColumnName.getBytes
  val attributeRedLight = HBaseTableDefinitions.vehicleImageRedLightColumnName.getBytes
  val attributeMeasure2Speed = HBaseTableDefinitions.vehicleImageMeasureMethod2SpeedColumnName.getBytes
  val attributeMeta = HBaseTableDefinitions.vehicleRecordColumnName.getBytes
  private val attributeResult = HBaseTableDefinitions.selfTestResultColumnName.getBytes()
  private val attributeImage = HBaseTableDefinitions.calibrationImageColumnName.getBytes()

  /**
   * Initialize all the internal Hbase tables
   */
  def init() {
    val adminHBase = new HBaseAdmin(hbaseConfig)

    try {
      if (vehicleTable == None) {
        HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, HBaseTableDefinitions.vehicleRecordColumnFamily)
        vehicleTable = Some(new HTable(hbaseConfig, tableName))
      }
      if (resultTable == None) {
        HBaseAdminTool.ensureTableExists(hbaseConfig, calResultTable, HBaseTableDefinitions.selfTestResultColumnFamily)
        resultTable = Some(new HTable(hbaseConfig, calResultTable))
      }
      if (imageTable == None) {
        HBaseAdminTool.ensureTableExists(hbaseConfig, calImageTable, HBaseTableDefinitions.calibrationImageColumnFamily)
        imageTable = Some(new HTable(hbaseConfig, calImageTable))
      }
    } finally {
      adminHBase.close()
    }
  }

  /**
   * When stopping the actor close the HBase table gracefully
   */
  def close() {
    vehicleTable.foreach { table ⇒
      {
        try {
          table.flushCommits()
          table.close()
        } catch {
          case ex: Exception ⇒ log.error("Failed to flush HBase for table " + tableName)
        } finally {
          vehicleTable = None
        }
      }
    }
    resultTable.foreach { table ⇒
      table.synchronized {
        try {
          table.flushCommits()
          table.close()
        } catch {
          case ex: Exception ⇒ log.error("Failed to flush HBase for table " + calResultTable)
        } finally {
          resultTable = None
        }
      }
    }

    imageTable.foreach { table ⇒
      table.synchronized {
        try {
          table.flushCommits()
          table.close()
        } catch {
          case ex: Exception ⇒ log.error("Failed to flush HBase for table " + calImageTable)
        } finally {
          imageTable = None
        }
      }
    }
  }

  /**
   * Create the row key of the registration
   * @param msg The registration
   * @return the row key as a byte array
   */
  def createKey(msg: VehicleMetadata): Array[Byte] = {
    val orig = Bytes.toBytes("%s-%s-%s-%s".format(msg.lane.system, msg.lane.gantry, msg.eventTimestamp.toString, msg.lane.name))
    rowKeyDistributerVehicle.getDistributedKey(orig)
  }

  /**
   * Store the vehicleRegisrtation
   * @param images contains the registration and the images
   */
  def storeRegistration(images: ImagesSupport) {
    val vehicleReg = images.createImageFilledVehicleMetadata()
    val key = createKey(vehicleReg)
    val put = new Put(key)

    put.add(columnFamily, attributeOverview, images.overview)
    put.add(columnFamily, attributeOverviewMasked, images.overviewMasked)
    put.add(columnFamily, attributeOverviewRedLight, images.overviewRedLight)
    put.add(columnFamily, attributeOverviewSpeed, images.overviewSpeed)

    put.add(columnFamily, attributeLicense, images.license)
    put.add(columnFamily, attributeRedLight, images.redlight)
    put.add(columnFamily, attributeMeasure2Speed, images.measure2Speed)

    val metaData = Serialization.write(vehicleReg)
    put.add(columnFamily, attributeMeta, metaData.getBytes)

    vehicleTable match {
      case Some(table) ⇒ {
        table.put(put)
        table.flushCommits()
      }
      case None ⇒ log.warn("eventid=[%s] failed to register into Hbase".format(vehicleReg.eventId))
    }
    log.info("Inserted " + vehicleReg.toString())
  }

  /**
   * Store a calibration
   * @param lanes for which lanes must the calibration contain images
   * @param time the time of the calibration
   * @param reportingOfficerCode the reporting officer code
   * @param success Did the calibration succeed of failed
   */
  def storeCalibration(corridorId: String, lanes: Seq[Lane], time: Date, reportingOfficerCode: String, success: Boolean, serialNr: String) {
    if (lanes.isEmpty) {
      return
    }
    val systemId = lanes.head.system
    val listImageKeys = new ListBuffer[KeyWithTimeAndId]

    imageTable.foreach(table ⇒ {
      for (lane ← lanes) {
        val key = Bytes.toBytes("%s-%s-%s".format(time.getTime.toString, corridorId, lane.laneId))
        val put = new Put(rowKeyDistributerSelfTestImage.getDistributedKey(key))
        put.add(columnFamily, attributeImage, time.getTime, createCalImage(lane))
        table.put(put)
        listImageKeys += new KeyWithTimeAndId(time.getTime, lane.laneId)
      }
      table.flushCommits()
    })

    //store result
    resultTable.foreach(table ⇒ {
      val key = Bytes.toBytes("%s-%s-%s".format(systemId, corridorId, time.getTime.toString))
      val result = SelfTestResult(
        systemId = systemId,
        corridorId = corridorId,
        reportingOfficerCode = reportingOfficerCode,
        nrCamerasTested = lanes.size,
        time = time.getTime,
        success = success,
        images = listImageKeys.toSeq,
        errors = if (success) Seq() else Seq("Calibration failure simulated"),
        serialNr = serialNr)
      val json = Serialization.write(result)(DefaultFormats)
      val put = new Put(rowKeyDistributerSelfTestResult.getDistributedKey(key))
      put.add(columnFamily, attributeResult, time.getTime, json.getBytes)
      table.put(put)
      table.flushCommits()
      log.info("Inserted " + result.toString())
    })
  }

  /**
   * Create a calibration image for specified lane
   * @param lane Specify the lane
   * @return Array[Byte] a calibration image
   */
  def createCalImage(lane: Lane): Array[Byte] = {
    val bufImage = new BufferedImage(200, 1, BufferedImage.TYPE_BYTE_GRAY)
    val emptyImage = new Image(bufImage)
    val content = new ListBuffer[String]

    content += "Image: Calibration"
    content += "Lane: " + lane.laneId
    val calImage = emptyImage.createImageWithFooter(1, content.toList)
    calImage.getBytes()
  }
}

