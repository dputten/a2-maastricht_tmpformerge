package csc.sectioncontrol.tools

import csc.config.Path
import csc.curator.utils.CuratorToolsImpl
import csc.sectioncontrol.messages.SystemEvent
import csc.sectioncontrol.storagelayer.StorageFactory
import csc.sectioncontrol.tools.EventType.EventType
import csc.sectioncontrol.tools.backup.BackupZookeeper._
import org.apache.curator.retry.ExponentialBackoffRetry

/**
 * Created by carlos on 28.10.15.
 */
object SendSystemEvents {

  type OptionMap = Map[Symbol, String]

  /**
   * Parse the command arguments
   */
  def nextOption(map: OptionMap, list: List[String]): OptionMap = {
    list match {
      case Nil ⇒ map
      case "--servers" :: value :: tail ⇒
        nextOption(map ++ Map('servers -> value), tail)
      case "--eventType" :: value :: tail ⇒
        nextOption(map ++ Map('eventType -> value), tail)
      case "--system" :: value :: tail ⇒
        nextOption(map ++ Map('system -> value), tail)
      case "--corridor" :: value :: tail ⇒
        nextOption(map ++ Map('corridor -> value), tail)
      case "--help" :: tail ⇒
        nextOption(map ++ Map('help -> ""), tail)
      case option :: tail ⇒
        println("Unknown option " + option)
        sys.exit(1)
    }
  }
  /**
   * Prints the usage string to standard out
   */
  def printUsage() {
    println("Usage --eventType <type> --system <systemId>  [--servers] [--corridor]")
    println("\t--eventType <type>: the type of the event (One of " + EventType.allOptions + ")")
    println("\t--servers <zookeeper servers>: the zookeeper servers quorum. default localhost:2181")
    println("\t--corridor <corridorId>: the corridor of the event")
    println("\t--help: show the usage and script usage")
  }

  /**
   * The main tread.
   * Parse arguments and check them for correctness
   */
  def main(args: Array[String]): Unit = {
    val options = nextOption(Map(), args.toList)
    println(options)
    options.get('help) foreach {
      value ⇒
        {
          printUsage
          sys.exit(0)
        }
    }

    var event: SystemEvent = null

    try {
      event = createEvent(options)
    } catch {
      case ex: Exception ⇒
        println(ex.getMessage)
        printUsage
        sys.exit(0)
    }

    val servers = options.get('servers).getOrElse("localhost:2181")
    val frame = StorageFactory.newClient(servers, new ExponentialBackoffRetry(60000, 10))
    frame.foreach(_.start())
    val curator = new CuratorToolsImpl(frame, log)

    val path = Path("/ctes") / "systems" / event.systemId / "events"
    if (!curator.exists(path)) {
      curator.createEmptyPath(path)
    }
    val node = path / "qn-"

    curator.appendEvent(node, event)
    sys.exit(0)
  }

  def createEvent(options: OptionMap): SystemEvent = {
    val systemId = getMandatory(options, 'system)
    val corridorId = options.get('corridor)
    val tp = getMandatory(options, 'eventType)
    val (componentId, eventType) = what(EventType.withName(tp))
    val time = System.currentTimeMillis()
    val userId = "ctes"
    val reason = tp.toString
    SystemEvent(componentId, time, systemId, eventType, userId, reason, None, None, None, corridorId, None, None)
  }

  def getMandatory(options: OptionMap, sym: Symbol): String = {
    options.get(sym) match {
      case Some(value) ⇒ value
      case None        ⇒ throw new IllegalArgumentException(" was not specified")
    }
  }

  def what(tp: EventType): (String, String) = tp match {
    case EventType.Off             ⇒ ("web", "Off")
    case EventType.StandBy         ⇒ ("web", "StandBy")
    case EventType.Maintenance     ⇒ ("web", "Maintenance")
    case EventType.Failure         ⇒ ("web", "Failure")
    case EventType.EnforceOn       ⇒ ("web", "EnforceOn")
    case EventType.EnforceDegraded ⇒ ("web", "EnforceDegraded")
    case EventType.EnforceOff      ⇒ ("web", "EnforceOff")
    case EventType.CameraAlert     ⇒ ("camera", "Alert")
    case EventType.FailureSignOff  ⇒ ("web", "FailureSignOff")
    case EventType.FailureSignOff  ⇒ ("ntp", "Failure")
    case EventType.Open            ⇒ ("web", "Open")
  }

}

object EventType extends Enumeration {

  type EventType = Value
  val Off, StandBy, Maintenance, Failure, EnforceOn, EnforceDegraded, EnforceOff, CameraAlert, NTPFailure, FailureSignOff, Open = Value

  def allOptions = values.toList.map(_.toString).mkString(" ")

}