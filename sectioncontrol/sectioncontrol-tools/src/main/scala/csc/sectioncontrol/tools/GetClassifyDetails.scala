package csc.sectioncontrol.tools

import csc.hbase.utils.HBaseTableKeeper
import csc.sectioncontrol.classify.nl.DecisionResult
import csc.sectioncontrol.messages.VehicleMetadata
import java.io.File
import org.apache.commons.io.FileUtils
import org.slf4j.LoggerFactory
import collection.JavaConversions._
import csc.sectioncontrol.storagelayer.hbasetables.{ VehicleTable, DecisionResultTable }

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

object GetClassifyDetails {

  lazy val log = LoggerFactory.getLogger(this.getClass.getName)
  val systemId = "A2"
  val TIMEPERIOD = 180000
  private var hbaseTables = new HBaseTableKeeper() {}

  type OptionMap = Map[Symbol, String]

  /**
   * Parse the command arguments
   */
  def nextOption(map: OptionMap, list: List[String]): OptionMap = {
    list match {
      case Nil ⇒ map
      case "--input" :: value :: tail ⇒
        nextOption(map ++ Map('input -> value), tail)
      case "--output" :: value :: tail ⇒
        nextOption(map ++ Map('output -> value), tail)
      case "--servers" :: value :: tail ⇒
        nextOption(map ++ Map('servers -> value), tail)
      case "--help" :: tail ⇒
        nextOption(map ++ Map('help -> ""), tail)
      case option :: tail ⇒
        log.error("Unknown option " + option)
        sys.exit(1)
    }
  }
  /**
   * Prints the usage string to standard out
   */
  def printUsage() {
    println("Usage --input <zaakbestand> [--output <outputfile>] [--servers <zookeeper servers>]")
    println("\t--input <zaakbestand>: the zaakbestand")
    println("\t--output <zoutputfile>: the result file")
    println("\t--servers <zookeeper servers>: the zookeeper servers quorum. default localhost:2181")
    println("\t--help: show the usage and script usage")

    log.info("Print the usage")
  }

  /**
   * The main tread.
   * Parse arguments and check them for correctness
   */
  def main(args: Array[String]): Unit = {
    val options = nextOption(Map(), args.toList)
    println(options)
    options.get('help) foreach {
      value ⇒
        {
          printUsage
          sys.exit(0)
        }
    }

    val scriptOpt = options.get('input)
    if (!scriptOpt.isDefined) {
      printUsage
      sys.exit(1)
    }
    val zaakFile = new File(scriptOpt.get)
    log.info("Read file: %s".format(zaakFile.getAbsolutePath))
    val lines = FileUtils.readLines(zaakFile, "ISO-8859-1")
    log.info("Nr of Lines: %d".format(lines.size()))

    val regList = ParseZaakbestand.parseLines(lines)
    log.info("Nr of registrations: %d".format(regList.size))
    val servers = options.get('servers).getOrElse("localhost:2181")
    val hbaseConfig = HBaseTableKeeper.createCachedConfig(servers)

    val reader = DecisionResultTable.createReader(hbaseConfig, hbaseTables)
    val results = regList.flatMap(vio ⇒ {
      val res = getDecisionResult(vio, reader)
      if (res.isEmpty) {
        log.warn("Failed to find decision results for violation " + vio)
      }
      res
    })
    //add registration
    val vrReader = VehicleTable.makeReader(hbaseConfig, hbaseTables)
    val mergedResult = results.map(getRegistration(_, vrReader))

    val outputResult = mergedResult.foldLeft("") {
      case (out, results) ⇒ {
        out + results.violation + " scannedCategory=" + results.registration.flatMap(_.category) + " " + results.decisions + "\n"
      }
    }

    val outputFile = options.get('output).map(new File(_))
    outputFile match {
      case Some(file) ⇒ FileUtils.writeStringToFile(file, outputResult)
      case None       ⇒ println(outputResult)
    }
    log.info("Done written %d lines to %s".format(results.size, outputFile.map(_.getAbsolutePath).getOrElse("StdOut")))
    hbaseTables.closeTables()
  }

  def getDecisionResult(violation: ParsedRegistration, reader: DecisionResultTable.Reader): Option[Result] = {
    val startMinute = violation.getEpochTime()
    val endMinute = startMinute + TIMEPERIOD
    val rows = reader.readRows(startMinute.toString, endMinute.toString)
    val lic = violation.license.replace("-", "")

    val result = rows.find(_.license.replace("-", "") == lic)
    result.map(res ⇒ new Result(violation, res))
  }

  def getRegistration(originalResult: Result, reader: VehicleTable.Reader): Result = {
    val startMinute = originalResult.violation.getEpochTime()
    val endMinute = startMinute + TIMEPERIOD
    val pos = originalResult.decisions.laneId.lastIndexOf("-")
    val exitPrefix = originalResult.decisions.laneId.substring(0, pos + 1)

    val rows = reader.readRows((exitPrefix, startMinute), (exitPrefix, endMinute))
    val lic = originalResult.violation.license.replace("-", "")

    val result = rows.find(_.license.map(_.value.replace("-", "")).getOrElse("") == lic)
    originalResult.copy(registration = result)
  }
}

case class Result(violation: ParsedRegistration, decisions: DecisionResult, registration: Option[VehicleMetadata] = None)

