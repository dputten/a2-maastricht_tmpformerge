/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.tools.hbasesim

import csc.sectioncontrol.decorate.Image
import csc.sectioncontrol.sign.Signing
import csc.sectioncontrol.messages._
import java.io.File
import java.awt.image.BufferedImage
import collection.mutable.ListBuffer
import csc.sectioncontrol.messages.VehicleMetadata
import csc.sectioncontrol.messages.VehicleImage

/**
 * This class creates the VehicleImages needed for the VehicleRegistration. and creates the complete
 * registration.
 *
 * @param image the base image where the three images created from
 * @param registration the registration without the images
 * @param compression the jpg compression for the new created images
 * @param timeYellow the yellow time used in the images
 * @param timeRed the red time, which is the start time of the red light in the images
 */
class ImagesSupport(image: String, registration: VehicleMetadata, compression: Int = 100, timeYellow: Option[Long] = None, timeRed: Option[Long] = None) {

  val baseImage = loadImage(image)
  val overviewMasked = createImage(VehicleImageType.OverviewMasked)
  val overview = createImage(VehicleImageType.Overview)
  val license = createLicense()
  val redlight = createImage(VehicleImageType.RedLight)
  val overviewRedLight = createImage(VehicleImageType.OverviewRedLight)
  val overviewSpeed = createImage(VehicleImageType.OverviewSpeed)
  val measure2Speed = createImage(VehicleImageType.MeasureMethod2Speed)

  /**
   * Load the base image
   * @param image the path to the image
   * @return Image which contain the loaded image
   */
  def loadImage(image: String): Image = {
    val file = new File(image)
    if (file.exists()) {
      new Image(image)
    } else {
      throw new IllegalArgumentException("File does not exists %s".format(file.getAbsolutePath))
    }
  }

  /**
   * Add the registration information to the base image
   * @param imageType The image type which has to be created
   * @return Array[Byte] the new created image
   */
  def createImage(imageType: VehicleImageType.Value): Array[Byte] = {
    val content = new ListBuffer[String]

    content += "Image: " + imageType
    content += "Time: " + registration.eventTimestampStr.getOrElse("--/--/---- --:--:--")
    content += registration.lane.system + " " + registration.lane.gantry + " " + registration.lane.name
    content += "NMI cert: " + registration.NMICertificate
    content += "Appl sign: " + registration.applChecksum
    content += "EventId: " + registration.eventId
    content += "Licence: " + registration.license.map(_.value).getOrElse("Empty")

    getLightTime(timeRed, imageType).foreach(time ⇒ {
      content += "Red-Time: " + time.toString
    })
    timeYellow.foreach(time ⇒ {
      content += "Yellow-Time: " + time
    })

    val decorateImage = baseImage.createImageWithFooter(8, content.toList)

    decorateImage.getBytes(compression = compression)
  }

  /**
   * Create the license image
   * @return Array[Byte] containing the license image
   */
  def createLicense(): Array[Byte] = {
    val bufImage = new BufferedImage(200, 1, BufferedImage.TYPE_BYTE_GRAY)
    val emptyImage = new Image(bufImage)
    val content = new ListBuffer[String]

    content += "Image: " + VehicleImageType.License
    content += "Licence: " + registration.license.map(_.value).getOrElse("Empty")
    val decorateImage = emptyImage.createImageWithFooter(1, content.toList)
    decorateImage.getBytes(compression = compression)
  }

  /**
   * create the VehicleMetadata containing the created images
   * @return VehicleMetadata the registration
   */
  def createImageFilledVehicleMetadata(): VehicleMetadata = {
    val over = new VehicleImage(
      timestamp = getTimestamp(VehicleImageType.Overview),
      offset = 0L,
      uri = VehicleImageType.Overview + "/" + image,
      imageType = VehicleImageType.Overview,
      checksum = Signing.calculateHash(Some(overview)),
      timeYellow = timeYellow,
      timeRed = getLightTime(timeRed, VehicleImageType.Overview))
    val licenseImage = new VehicleImage(
      timestamp = getTimestamp(VehicleImageType.License),
      offset = 0L,
      uri = VehicleImageType.License + "/" + image,
      imageType = VehicleImageType.License,
      checksum = Signing.calculateHash(Some(license)),
      timeYellow = None,
      timeRed = None)
    val imageRedLight = new VehicleImage(
      timestamp = getTimestamp(VehicleImageType.RedLight),
      offset = 0L,
      uri = VehicleImageType.RedLight + "/" + image,
      imageType = VehicleImageType.RedLight,
      checksum = Signing.calculateHash(Some(redlight)),
      timeYellow = timeYellow,
      timeRed = getLightTime(timeRed, VehicleImageType.RedLight))
    val imageOverviewRedLight = new VehicleImage(
      timestamp = getTimestamp(VehicleImageType.OverviewRedLight),
      offset = 0L,
      uri = VehicleImageType.OverviewRedLight + "/" + image,
      imageType = VehicleImageType.OverviewRedLight,
      checksum = Signing.calculateHash(Some(overviewRedLight)),
      timeYellow = timeYellow,
      timeRed = getLightTime(timeRed, VehicleImageType.OverviewRedLight))
    val imageOverviewSpeed = new VehicleImage(
      timestamp = getTimestamp(VehicleImageType.OverviewSpeed),
      offset = 0L,
      uri = VehicleImageType.OverviewSpeed + "/" + image,
      imageType = VehicleImageType.OverviewSpeed,
      checksum = Signing.calculateHash(Some(overviewSpeed)),
      timeYellow = timeYellow,
      timeRed = getLightTime(timeRed, VehicleImageType.OverviewSpeed))
    val imageMeasureMethod2Speed = new VehicleImage(
      timestamp = getTimestamp(VehicleImageType.MeasureMethod2Speed),
      offset = 0L,
      uri = VehicleImageType.MeasureMethod2Speed + "/" + image,
      imageType = VehicleImageType.MeasureMethod2Speed,
      checksum = Signing.calculateHash(Some(measure2Speed)),
      timeYellow = None,
      timeRed = None)

    registration.copy(
      images = Seq(over, licenseImage, imageRedLight, imageOverviewRedLight, imageOverviewSpeed, imageMeasureMethod2Speed))

  }

  /**
   * Calculate the red light time for the different images.
   * When both yellow and red light are 0 no calculation is done (simulate green light)
   * When starting red Light is None return also None for red light (simulate info is not available)
   *
   * @param startTime The redlight start time
   * @param imageType Specify which image is calculated
   * @return the red light value used in the image
   */
  private def getLightTime(startTime: Option[Long], imageType: VehicleImageType.Value): Option[Long] = {
    startTime match {
      case Some(time) ⇒ {
        //when red == 0 calculate red time only when there is a yellow time > 0
        val yellow = timeYellow.getOrElse(0L)
        val red = timeRed.getOrElse(0L)
        if (red == 0 && yellow <= 0) {
          return Some(0)
        }

        val length = registration.length.map(_.value).getOrElse(4.5F)
        val speed = registration.speed.map(_.value).getOrElse(50F)
        imageType match {
          case VehicleImageType.RedLight            ⇒ Some(time)
          case VehicleImageType.Overview            ⇒ Some((time + (length + 1.5F) / speed * 3600).toLong) //length is car length + length of detection lus
          case VehicleImageType.OverviewRedLight    ⇒ Some((time + (length + 1.5F) / speed * 3600).toLong) //length is car length + length of detection lus
          case VehicleImageType.OverviewSpeed       ⇒ Some((time + (length + 1.5F) / speed * 3600).toLong) //length is car length + length of detection lus
          case VehicleImageType.MeasureMethod2Speed ⇒ Some((time + (length + 20F) / speed * 3600).toLong) //length is car length + 20 m
          case other                                ⇒ None
        }
      }
      case None ⇒ None
    }
  }

  /**
   * Calulate the time of the images.
   * @param imageType Specify which image is calculated
   * @return the timestamp of the image
   */
  private def getTimestamp(imageType: VehicleImageType.Value): Long = {
    val startTime = registration.eventTimestamp
    val length = registration.length.map(_.value).getOrElse(4.5F)
    val speed = registration.speed.map(_.value).getOrElse(50F)
    imageType match {
      case VehicleImageType.RedLight            ⇒ startTime
      case VehicleImageType.Overview            ⇒ startTime + ((length + 1.5F) / speed * 3600).toLong //length is car length + length of detection lus
      case VehicleImageType.OverviewRedLight    ⇒ startTime + ((length + 1.5F) / speed * 3600).toLong //length is car length + length of detection lus
      case VehicleImageType.OverviewSpeed       ⇒ startTime + ((length + 1.5F) / speed * 3600).toLong //length is car length + length of detection lus
      case VehicleImageType.MeasureMethod2Speed ⇒ startTime + ((length + 20F) / speed * 3600).toLong //length is car length + 20 m
      case other                                ⇒ startTime
    }
  }

}