/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.tools.hbasesim

import org.apache.commons.io.FileUtils
import java.util.{ TimeZone, Date }
import java.io.{ IOException, InputStreamReader, BufferedReader, File }
import collection.mutable.ListBuffer
import org.slf4j.LoggerFactory
import csc.sectioncontrol.messages._
import java.text.SimpleDateFormat
import org.apache.hadoop.hbase.HBaseConfiguration
import csc.sectioncontrol.storagelayer.StorageFactory
import net.liftweb.json.Formats
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.storage._
import akka.actor.ActorSystem
import org.apache.curator.retry.ExponentialBackoffRetry
import csc.curator.utils.CuratorToolsImpl
import csc.sectioncontrol.messages.ValueWithConfidence_Float
import csc.sectioncontrol.messages.ValueWithConfidence
import csc.sectioncontrol.messages.VehicleMetadata
import csc.sectioncontrol.messages.Lane
import scala.Tuple2

case class TimeConfig(realMode: Boolean, startTime: Date)
case class ConfigEntity(lane: Lane, secondLane: Option[Lane] = None, length: Option[Double] = None)
case class ConfigGlobal(nmiCertificate: String, applChecksum: String, defaults: Map[String, String],
                        entities: Map[String, ConfigEntity], store: StoreRegistration,
                        timeCfg: TimeConfig)

/**
 * This is the object which starts the simulator.
 * It is responseble
 * to read the commandlines
 * to read the scriptfile
 * and execute the commands
 *
 */
object VehicleSimulator {
  lazy val log = LoggerFactory.getLogger(this.getClass.getName)

  type OptionMap = Map[Symbol, String]
  var reader = new BufferedReader(new InputStreamReader(System.in))

  /**
   * Parse the command arguments
   */
  def nextOption(map: OptionMap, list: List[String]): OptionMap = {
    list match {
      case Nil ⇒ map
      case "--script" :: value :: tail ⇒
        nextOption(map ++ Map('script -> value), tail)
      case "--servers" :: value :: tail ⇒
        nextOption(map ++ Map('servers -> value), tail)
      case "--tableVehicle" :: value :: tail ⇒
        nextOption(map ++ Map('table -> value), tail)
      case "--tableCalImage" :: value :: tail ⇒
        nextOption(map ++ Map('calImage -> value), tail)
      case "--tableCalResult" :: value :: tail ⇒
        nextOption(map ++ Map('calResult -> value), tail)
      case "--outputVersion" :: value :: tail ⇒
        nextOption(map ++ Map('outputVersion -> value), tail)
      case "--help" :: tail ⇒
        nextOption(map ++ Map('help -> ""), tail)
      case "--log" :: value :: tail ⇒
        nextOption(map ++ Map('log -> value), tail)
      case option :: tail ⇒
        println("Unknown option " + option)
        sys.exit(1)
    }
  }

  /**
   * Prints the usage string to standard out
   */
  def printUsage() {
    println("Simulator Usage --script <scriptFile> [--servers <zookeeper servers>] [--tableVehicle <tableName>]")
    println("\t--script <scriptFile>: the script file which contains the instructions")
    println("\t--servers <zookeeper servers>: the zookeeper servers quorum. default localhost:2181")
    println("\t--tableVehicle <tableName>: the table where to insert the registrations. default vehicle")
    println("\t--tableCalImage <tableName>: the table where to insert the calibration Images. default SelftestImage")
    println("\t--tableCalResult <tableName>: the table where to insert the calibration Images. default SelftestResult")
    println("\t--outputVersion <version>: the version of the Hbase output format (default latest version value 1,2) ")
    println("\t--help: show the usage and script usage")
  }

  /**
   * Prints the script usage string to standard out
   */
  def printScriptUsage() {
    println("Script Usage: ")
    println("\t# comment")
    println("\tCONFIG CONFIG-START ...configurations... CONFIG-END")
    println("Config Usage: command [argument[=value] ...]")
    println("\tLANE-ID=<script Id> NAME=<lane name> GANTRY=<gantry> SYSTEM=<system> GPS-LONG=<longitude> GPS-LAT=<latitude>")
    println("\t\tLANE-ID=<ref-id> The referention id of this lane")
    println("\t\tNAME=<lane name> The name of the lane")
    println("\t\tGANTRY=<gantry> The gantry name of the lane")
    println("\t\tSYSTEM=<system> The system of the lane")
    println("\t\tGPS-LONG=<longitude> The longitude of the position of the lane")
    println("\t\tGPS-LAT=<latitude> The latitude of the position of the lane")
    println("\tTIME [MODE=<running mode>] [START=<HH:mm:ss>]")
    println("\t\tMODE=<running mode> The running mode: possible values REALTIME,BATCH (optional default BATCH)")
    println("\t\tSTART=<HH:mm:ss>] The start time of the script (optional default current time")
    println("\tCHECKSUM [NMI-CERT=<certificate>] [APPL-CHECKSUM=<checksum>]")
    println("\t\tNMI-CERT=<certificate> The NMI certivicate (optional default TPXXXX)")
    println("\t\tAPPL-CHECKSUM=<checksum> The application checksum (optional default 0123456789ABCDEF0123456789ABCDEF")
    println("\tDEFAULTS agrs=<value> define default which are used in the script when no value is given")
    println("\tCORRIDOR CORRIDOR-ID=<sectionId> START-LANE-ID=<laneId> END-LANE-ID=<laneId> CORRIDOR-LENGTH=<length>")
    println("\t\tCORRIDOR-ID=<scriptId> The referention id of this corridor")
    println("\t\tSTART-LANE-ID=<laneId> The start lane referention id")
    println("\t\tEND-LANE-ID=<laneId> The end lane referention id")
    println("\t\tCORRIDOR-LENGTH=<length> The length of this corridor")

    println("\tSCRIPT SCRIPT-START ...scripts... SCRIPT-END")
    println("Script Usage: delay command [argument[=value] ...]")
    println("\tLANE-REGISTRATION LANE-ID=<laneId> IMAGE=<image> [SPEED=<speed>] [LICENSE=<license>] ")
    println("[COUNTRY=<country>] [TIME-YELLOW=<msec>] [TIME-RED=<msec>] [LICENSE-CONF=<conf>]")
    println("\t\t\t [LENGTH-CONF=<conf>] [SPEED-CONF=<conf>] [COUNTRY-CONF=<conf>]")
    println("\t\tLANE-ID=<ref-id> The referention id of this lane")
    println("\t\tIMAGE=<image> The path to the image")
    println("\t\tSPEED=<speed> The speed of the vehicle")
    println("\t\tSPEED-CONF=<conf> The speed confidence of the vehicle")
    println("\t\tLICENSE=<license> The license of the vehicle")
    println("\t\tLICENSE-CONF=<conf> The license confidence of the vehicle. needed when length is filled")
    println("\t\tLENGTH=<length> The length of the vehicle in meters (optional)")
    println("\t\tLENGTH-CONF=<conf> The length confidence of the vehicle needed when length is filled")
    println("\t\tCOUNTRY=<country> The country of the vehicle two position NL,DE,etc")
    println("\t\tCOUNTRY-CONF=<conf> The country confidence needed when country is filled")
    println("\tCALIBRATE [EXCLUDE-SYSTEM=<list of systems>] OFFICER_CODE=<code> RESULT=<result>")
    println("\t\tEXCLUDE-SYSTEM=<list of systems> list coma separated of systems")
    println("\t\tOFFICER_CODE=<code> The OfficerCode who issued the calibration")
    println("\t\tRESULT=<result> The result of the calibration possible values true,false,OK,NOK")
    println("\tWAIT NOP | INPUT")
    println("\t\tWAIT NOP: Just wait and do nothing")
    println("\t\tWAIT INPUT: Wait until user gives an enter")
  }

  /**
   * The main tread.
   * Parse arguments and check them for correctness
   */
  def main(args: Array[String]): Unit = {
    val options = nextOption(Map(), args.toList)
    println(options)
    options.get('help) foreach {
      value ⇒
        {
          printUsage
          printScriptUsage
          sys.exit(0)
        }
    }

    val scriptOpt = options.get('script)
    if (!scriptOpt.isDefined) {
      printUsage
      sys.exit(1)
    }
    val scriptFile = new File(scriptOpt.get)

    var execLog = options.get('log).getOrElse(scriptFile.getName + ".execlog")
    val servers = options.get('servers).getOrElse("localhost:2181")
    val tableName = options.get('table).getOrElse("vehicle")
    val calImageTable = options.get('calImage).getOrElse("SelftestImage")
    val calResultTable = options.get('calResult).getOrElse("SelftestResult")
    val outputVersion = options.get('outputVersion).getOrElse("")
    val hbaseConfig = HBaseConfiguration.create()
    hbaseConfig.set("hbase.zookeeper.quorum", servers)

    val store = outputVersion match {
      case other ⇒ new StoreRegistrationHBase(tableName, calImageTable, calResultTable, hbaseConfig)
    }
    //
    val curator = StorageFactory.newClient(servers, new ExponentialBackoffRetry(1000, 100))
    curator.foreach(_.start())
    val system = ActorSystem("simulator")
    val zookeeperClient = new CuratorToolsImpl(curator, system.log) {
      override def extendFormats(defaultFormats: Formats): Formats = {
        super.extendFormats(defaultFormats) +
          new EnumerationSerializer(SpeedIndicatorType, ServiceType, ZkCaseFileType, ZkIndicationType, ZkWheelbaseType, VehicleCode, ConfigType)
      }
    }

    //readScriptFile
    try {
      val (configList, scriptList) = readScriptFile(scriptFile.getAbsolutePath)
      val config = configureSimulator(configList, store)

      //create repeat structure
      val (nrRead, command) = createRepeatCommandLine(scriptList, 0)
      if (nrRead != scriptList.size) {
        println("Parsing script file failed: Mismatch between REPEAT-START and REPEAD-END")
        sys.exit(2)
      }
      store.init()
      //process script
      executeCommand(config, command, config.timeCfg.startTime.getTime)
    } catch {
      case e: ParserException ⇒ {
        //log error on line index
        println("Parsing script file failed: " + e.getMessage)
        sys.exit(2)
      }
      case e: IllegalArgumentException ⇒ {
        //log error on line index
        println("Configuration failed: " + e.getMessage)
        sys.exit(3)
      }
    } finally {
      store.close()
      curator.foreach(_.close())
    }
    //stop system
    System.out.println("Stop Simulator")
    reader.close()
    System.out.println("Simulator Stoped")
    sys.exit(0)
  }

  /**
   * Create the configuration from the script file
   * @param configList list of configuration lines from the script file
   * @param store The Registration store which has to be included in the ConfigGlobal
   * @return ConfigGlobal returns the configuration
   */
  def configureSimulator(configList: List[ConfigLine], store: StoreRegistration): ConfigGlobal = {
    var refMap = Map[String, ConfigEntity]()
    var nmi = "TPXXXX"
    var appl = "0123456789ABCDEF0123456789ABCDEF"
    var defaults = Map[String, String]()
    var timeCfg = new TimeConfig(false, new Date())

    for (cfg ← configList) {
      val options = cfg.options
      cfg.config match {
        case SimConstants.TIME ⇒ {
          val mode = options.get(SimConstants.MODE).map(_.toUpperCase == "REALTIME").getOrElse(timeCfg.realMode)
          val startTime = options.get(SimConstants.START).map(str ⇒ {
            val dayStr = new SimpleDateFormat("yyyyMMdd").format(timeCfg.startTime)
            val format = new SimpleDateFormat("yyyyMMdd HH:mm:ss")
            format.parse(dayStr + " " + str)
          }) getOrElse (timeCfg.startTime)
          timeCfg = new TimeConfig(mode, startTime)
        }
        case SimConstants.DEFAULTS ⇒ {
          defaults ++= options
        }
        case SimConstants.CHECKSUM ⇒ {
          nmi = options.get(SimConstants.NMI_CERTIFICATE).getOrElse(nmi)
          appl = options.get(SimConstants.APPL_CHECKSUM).getOrElse(appl)
        }
        case SimConstants.CONFIG_LANE ⇒ {
          val id = getOption(SimConstants.CONFIG_LANE, options, SimConstants.LANE_ID)
          val name = getOption(SimConstants.CONFIG_LANE, options, SimConstants.LANE_NAME)
          val gantry = getOption(SimConstants.CONFIG_LANE, options, SimConstants.LANE_GANTRY)
          val system = getOption(SimConstants.CONFIG_LANE, options, SimConstants.LANE_SYSTEM)
          val long = options.get(SimConstants.LANE_GPSLONG).getOrElse("0").toDouble
          val lat = options.get(SimConstants.LANE_GPSLAT).getOrElse("0").toDouble
          val laneId = "%s-%s-%s".format(system, gantry, name)
          refMap += (SimConstants.CONFIG_LANE + id) -> new ConfigEntity(new Lane(
            laneId = laneId,
            name = name,
            gantry = gantry,
            system = system,
            sensorGPS_longitude = long,
            sensorGPS_latitude = lat))
        }
        case SimConstants.CONFIG_CORRIDOR ⇒ {
          val id = getOption(SimConstants.CONFIG_CORRIDOR, options, SimConstants.CORRIDOR_ID)
          val start = getOption(SimConstants.CONFIG_CORRIDOR, options, SimConstants.START_LANE_ID)
          val end = getOption(SimConstants.CONFIG_CORRIDOR, options, SimConstants.END_LANE_ID)
          val length = getOption(SimConstants.CONFIG_CORRIDOR, options, SimConstants.CORRIDOR_LENGTH).toDouble
          val startLane = refMap.get(SimConstants.CONFIG_LANE + start) match {
            case Some(cfg) ⇒ cfg.lane
            case None      ⇒ throw new IllegalArgumentException("Can't find lane with id %s".format(start))
          }
          val endLane = refMap.get(SimConstants.CONFIG_LANE + end) match {
            case Some(cfg) ⇒ cfg.lane
            case None      ⇒ throw new IllegalArgumentException("Can't find lane with id %s".format(end))
          }
          refMap += (SimConstants.CONFIG_CORRIDOR + id) -> new ConfigEntity(startLane, Some(endLane), Some(length))
        }
      }
    }
    new ConfigGlobal(nmiCertificate = nmi,
      applChecksum = appl,
      defaults = defaults,
      entities = refMap,
      store = store,
      timeCfg = timeCfg)
  }

  /**
   * Get a required option
   * @param command the command which is processed (incuded in exception)
   * @param optionList the list of options
   * @param option The specified option
   * @return the value of the option
   */
  def getOption(command: String, optionList: Map[String, String], option: String): String = {
    val value = optionList.get(option)
    if (value == None) {
      throw new IllegalArgumentException("Missing required argument %s for command %s".format(option, command))
    }
    value.get
  }

  /**
   * Parse the script
   */
  def readScriptFile(fileName: String): (List[ConfigLine], List[ScriptLine]) = {
    val lines = FileUtils.readFileToString(new File(fileName))
    val allList = VehicleParser.parseScript(lines)
    val configList = allList.filter(obj ⇒ obj.isInstanceOf[ConfigLine]).asInstanceOf[List[ConfigLine]]
    val scriptList = allList.filter(obj ⇒ obj.isInstanceOf[ScriptLine]).asInstanceOf[List[ScriptLine]]
    (configList, scriptList)
  }

  /**
   * Execute the script and keep up the delay time independent of the duration of the execution time
   * This is called recursive when it find a repeater block (commands is not empty)
   * @param config all the references to the registration Actors
   * @param command the current repeat command
   * @param curTime the startTime
   * @return the time of the last script line execution
   */
  def executeCommand(config: ConfigGlobal, command: RepeatCommandLine, curTime: Long): Long = {
    //To prevent the simulator to drift from the script file delays the wait time is calculated
    // using a accumulated time. This way the execution time is ignored
    var accTime = curTime

    for (nr ← 0 until command.nrExec) {
      for (commandLine ← command.commands) {
        if (commandLine.commands.isEmpty) {
          //just one command so execute
          accTime += commandLine.scriptLine.delay
          if (config.timeCfg.realMode) {
            val delay = accTime - System.currentTimeMillis
            if (delay > 0) {
              Thread.sleep(delay)
            }
          }
          try {
            accTime = excecuteLine(config, commandLine.scriptLine, new Date(accTime))
          } catch {
            case ex: IllegalArgumentException ⇒ {
              val msg = "Error in script. Command %s scriptLine: %d error %s".format(commandLine.scriptLine.command, commandLine.startLine, ex.getMessage)
              System.out.println(msg)
              log.error(msg)
              sys.exit(-1)
            }
          }
        } else {
          //list fo commands so a repeat block
          accTime = executeCommand(config, commandLine, accTime)
        }
      }
    }
    accTime
  }

  /**
   * Execute one script line
   * It returns the last execution time. For normal execution this is the same as now, but
   * in some actions like waiting for input of a repeating commands,
   * this is adjusted to the last execution time
   *
   * @param config The configuration
   * @param line the script line which has to be executed
   * @param now current time
   * @return time of the last execution time.
   */
  def excecuteLine(config: ConfigGlobal, line: ScriptLine, now: Date): Long = {
    var executeTime = now.getTime //normal actions return the now time, but repeated commands or wait commands will alter this time
    val options = line.options
    line.command match {
      case SimConstants.COMMENT ⇒ {
        //do nothing
      }
      case SimConstants.LANE_REGISTRATION ⇒ {
        log.info("Simulate: laneRegistration")
        val id = getOption(SimConstants.LANE_REGISTRATION, options, SimConstants.LANE_ID)
        val entity = config.entities.get(SimConstants.CONFIG_LANE + id)
        if (entity == None) {
          throw new IllegalArgumentException("Missing Lane %s isn't defined in configuration".format(id))
        }
        val vehicleWithoutImages = createVehicleMetadata(config.nmiCertificate, config.applChecksum, now, entity.get.lane, options, config.defaults)
        val image = getOption(SimConstants.LANE_REGISTRATION, options, SimConstants.IMAGE)
        val timeRed = getOptionWithDefault(options, SimConstants.TIME_RED, config.defaults).map(_.toLong)
        val timeYellow = getOptionWithDefault(options, SimConstants.TIME_YELLOW, config.defaults).map(_.toLong)
        val images = new ImagesSupport(image, vehicleWithoutImages, 100, timeYellow, timeRed)
        //store
        config.store.storeRegistration(images)
      }
      case SimConstants.CORRIDOR_REGISTRATION ⇒ {
        log.info("Simulate: CorridorRegistration")
        val id = getOption(SimConstants.CORRIDOR_REGISTRATION, options, SimConstants.CORRIDOR_ID)
        val speed = getOption(SimConstants.CORRIDOR_REGISTRATION, options, SimConstants.SPEED).toFloat
        config.entities.get(SimConstants.CONFIG_CORRIDOR + id) match {
          case None ⇒ throw new IllegalArgumentException("Missing Corridor %s isn't defined in configuration".format(id))
          case Some(corridor) ⇒ {
            val vehicleWithoutImages = createVehicleMetadata(config.nmiCertificate, config.applChecksum, now, corridor.lane, options, config.defaults)
            val image = getOption(SimConstants.LANE_REGISTRATION, options, SimConstants.IMAGE)
            val timeRed = getOptionWithDefault(options, SimConstants.TIME_RED, config.defaults).map(_.toLong)
            val timeYellow = getOptionWithDefault(options, SimConstants.TIME_YELLOW, config.defaults).map(_.toLong)
            val images = new ImagesSupport(image, vehicleWithoutImages, 100, timeYellow, timeRed)
            //store
            config.store.storeRegistration(images)

            val delay = (corridor.length.getOrElse(1D) / speed * 3600).toLong //delay in milliseconds
            val newTime = new Date(now.getTime + delay)

            val vehicleWithoutImages2 = createVehicleMetadata(config.nmiCertificate, config.applChecksum, newTime, corridor.secondLane.get, options, config.defaults)
            val image2 = getOption(SimConstants.LANE_REGISTRATION, options, SimConstants.IMAGE)
            val images2 = new ImagesSupport(image2, vehicleWithoutImages2, 100, timeYellow, timeRed)
            //store
            config.store.storeRegistration(images2)
          }
        }
      }
      case SimConstants.CALIBRATE ⇒ {
        log.info("Simulate: Calibration")
        val officer = getOptionWithDefault(options, SimConstants.OFFICER_CODE, config.defaults)
        if (officer.isEmpty) {
          throw new IllegalArgumentException("Calibration requested without " + SimConstants.OFFICER_CODE)
        }
        val calResultStr = getOptionWithDefault(options, SimConstants.RESULT, config.defaults)
        val calResult = calResultStr.map(_ match {
          case "TRUE" ⇒ true
          case "OK"   ⇒ true
          case other  ⇒ false
        }).getOrElse(true)

        val serialNr = getOptionWithDefault(options, SimConstants.SERIAL_NR, config.defaults).getOrElse("")

        val corridorId = getOptionWithDefault(options, SimConstants.CORRIDOR_ID, config.defaults)
        if (corridorId.isEmpty) {
          throw new IllegalArgumentException("Calibration requested without " + SimConstants.CORRIDOR_ID)
        }
        val entityName = SimConstants.CONFIG_CORRIDOR + corridorId.get
        val corridor = config.entities.find { case (name, entCfg) ⇒ entityName == name }

        corridor match {
          case Some(cfg) ⇒ {
            val lanes = List(cfg._2.lane) ++ cfg._2.secondLane
            config.store.storeCalibration(corridorId.get, lanes, now, officer.get, calResult, serialNr)
          }
          case None ⇒ throw new IllegalArgumentException("Calibration requested without valid " + SimConstants.CORRIDOR_ID + ": " + corridorId.get)
        }
      }
      case SimConstants.PROCESSED_UNTIL ⇒ {
        log.info("Simulate: processed until")

        val system = getOptionWithDefault(options, SimConstants.LANE_SYSTEM, config.defaults)
        val lane = getOptionWithDefault(options, SimConstants.LANE_ID, config.defaults)
        if (lane.isEmpty && system.isEmpty) {
          throw new IllegalArgumentException("processed requested without " + SimConstants.LANE_ID + " and " + SimConstants.LANE_SYSTEM)
        }
        lane.foreach(laneId ⇒ {
          val entity = config.entities.get(SimConstants.CONFIG_LANE + laneId)
          if (entity == None) {
            throw new IllegalArgumentException("Missing Lane %s isn't defined in configuration".format(laneId))
          }
        })
      }
      case SimConstants.WAIT ⇒ {
        if (options.size == 0) {
          //NOP
        }
        if (options.size > 1) {
          throw new IllegalArgumentException("Only one argument possible for WAIT command")
        }
        options.get(SimConstants.INPUT) foreach { _ ⇒
          {
            log.info("Simulate: wait for input")
            //  prompt the user to enter their name
            System.out.print("Wait for command input: ")
            //  open up standard input
            var inputLine: String = null
            //  readLine() method
            try {
              inputLine = reader.readLine()
            } catch {
              case ex: IOException ⇒ System.out.println("IO error while waiting for input. resuming scriptprocessing");
            }
            System.out.println("Resuming scriptprocessing")
            log.info("Simulate: resume control after wait for input")
            executeTime = System.currentTimeMillis
          }
        }
      }
    }
    executeTime
  }

  /**
   * get a option which supports defaults values
   * @param optionList the options
   * @param option the specified option
   * @param defaults the list with defaults
   * @return the option of the value requested
   */
  def getOptionWithDefault(optionList: Map[String, String], option: String, defaults: Map[String, String]): Option[String] = {
    optionList.get(option).orElse(defaults.get(option))
  }

  /**
   * Create a ValuesWithConfidence based on the value and confidence
   * @param value Option of the value
   * @param conf Option of the confidence
   * @tparam T Value Type
   * @return An option of the requested ValuesWithConfidence
   */
  def createValueWithConfidence[T <: AnyRef](value: Option[T], conf: Option[Int]): Option[ValueWithConfidence[T]] = {
    value.map(content ⇒ new ValueWithConfidence[T](content, conf.getOrElse(0)))
  }

  /**
   * Create a ValuesWithConfidence_Float based on the value and confidence
   * @param value Option of the value
   * @param conf Option of the confidence
   * @return An option of the requested ValuesWithConfidence_Float
   */
  def createValueWithConfidence_Float(value: Option[Float], conf: Option[Int]): Option[ValueWithConfidence_Float] = {
    value.map(content ⇒ new ValueWithConfidence_Float(content, conf.getOrElse(0)))
  }

  /**
   * Translate the script category into the VehicleCategory
   * @param catStr the script category
   * @return the VehicleCategory
   */
  def getCategory(catStr: String): VehicleCategory.Value = {
    catStr.toUpperCase() match {
      case "PERS"          ⇒ VehicleCategory.Pers
      case "CAR"           ⇒ VehicleCategory.Car
      case "MOTOR"         ⇒ VehicleCategory.Motor
      case "VAN"           ⇒ VehicleCategory.Van
      case "CAR_TRAILER"   ⇒ VehicleCategory.CarTrailer
      case "CARTRAILER"    ⇒ VehicleCategory.CarTrailer
      case "BUS"           ⇒ VehicleCategory.Bus
      case "TRUCK_TRAILER" ⇒ VehicleCategory.TruckTrailer
      case "TRUCKTRAILER"  ⇒ VehicleCategory.TruckTrailer
      case "SEMI_TRAILER"  ⇒ VehicleCategory.SemiTrailer
      case "SEMITRAILER"   ⇒ VehicleCategory.SemiTrailer
      case "TRUCK"         ⇒ VehicleCategory.Truck
      case "LARGE_TRUCK"   ⇒ VehicleCategory.Large_Truck
      case "LARGETRUCK"    ⇒ VehicleCategory.Large_Truck
      case other           ⇒ VehicleCategory.Unknown
    }
  }

  /**
   * Create a VehicleMetadata from the options.
   * This doesn't contain the images
   * @param nmiCert The used NMI certificate
   * @param applChecksum The used applicationChecksum
   * @param time The time of the registration
   * @param lane The lane of the registration
   * @param options The options of this registration
   * @param defaults The defaulds which can be used when the options doesn't contain the parameter
   * @return The VehicleMetadata without images
   */
  def createVehicleMetadata(nmiCert: String, applChecksum: String, time: Date, lane: Lane, options: Map[String, String], defaults: Map[String, String]): VehicleMetadata = {
    var dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS z")
    dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))

    val speed = getOptionWithDefault(options, SimConstants.SPEED, defaults).map(_.toFloat)
    val speedConf = getOptionWithDefault(options, SimConstants.SPEED_CONF, defaults).map(_.toInt)
    val length = getOptionWithDefault(options, SimConstants.LENGTH, defaults).map(_.toFloat)
    val lengthConf = getOptionWithDefault(options, SimConstants.LENGTH_CONF, defaults).map(_.toInt)
    val license = getOptionWithDefault(options, SimConstants.LICENSE, defaults)
    val licenseConf = getOptionWithDefault(options, SimConstants.LICENSE_CONF, defaults).map(_.toInt)
    val country = getOptionWithDefault(options, SimConstants.COUNTRY, defaults)
    val countryConf = getOptionWithDefault(options, SimConstants.COUNTRY_CONF, defaults).map(_.toInt)
    val category = getOptionWithDefault(options, SimConstants.CATEGORY, defaults).map(getCategory(_).toString)
    val categoryConf = getOptionWithDefault(options, SimConstants.CATEGORY_CONF, defaults).map(_.toInt)
    val serialNr = getOptionWithDefault(options, SimConstants.SERIAL_NR, defaults).getOrElse("")

    new VehicleMetadata(
      lane = lane,
      eventId = lane.laneId + time.getTime.toString,
      eventTimestamp = time.getTime,
      eventTimestampStr = Some(dateFormat.format(time)), //Only used for testing
      license = createValueWithConfidence[String](license, licenseConf),
      length = createValueWithConfidence_Float(length, lengthConf),
      category = createValueWithConfidence[String](category, categoryConf),
      speed = createValueWithConfidence_Float(speed, speedConf),
      country = createValueWithConfidence[String](country, countryConf),
      images = Seq(),
      NMICertificate = nmiCert,
      applChecksum = applChecksum,
      serialNr = serialNr) //todo red and yellowTime
  }

  /**
   * Create a repeat structure
   * @param scriptLines list of all scriptLines
   * @param startPos start position in the scriptLines list
   * @return Tuple of (next row to read, new created commandLine)
   */
  def createRepeatCommandLine(scriptLines: List[ScriptLine], startPos: Int): Tuple2[Int, RepeatCommandLine] = {
    val list = new ListBuffer[RepeatCommandLine]
    var ptr = if (scriptLines(startPos).command == SimConstants.REPEAT_START) {
      startPos + 1
    } else {
      startPos
    }
    var done = false
    while (!done && ptr < scriptLines.size) {
      val line = scriptLines(ptr)
      line.command match {
        case SimConstants.REPEAT_END ⇒ {
          done = true
          ptr += 1
        }
        case SimConstants.REPEAT_START ⇒ {
          val (pos, command) = createRepeatCommandLine(scriptLines, ptr)
          ptr = pos
          list += command
        }
        case _ ⇒ {
          list += new RepeatCommandLine(1, ptr + 1, line, Seq())
          ptr += 1
        }
      }
    }
    val command = if (scriptLines(startPos).command == SimConstants.REPEAT_START) {
      val scriptLine = scriptLines(startPos)
      val nrRepeat = getOption(SimConstants.REPEAT_START, scriptLine.options, SimConstants.REPEAT_NUMBER).toInt
      new RepeatCommandLine(nrRepeat, startPos + 1, scriptLine, list.toSeq)
    } else {
      new RepeatCommandLine(1, startPos, new ScriptLine(0, SimConstants.SCRIPT_START, Map()), list.toSeq)
    }
    (ptr, command)
  }
}

/**
 * The RepeatCommandLine used to support reapeting blocks
 * @param nrExec number of repeating the commands
 * @param startLine the line number of the script
 * @param scriptLine The content of the cript line
 * @param commands The commands which have to be executed
 */
case class RepeatCommandLine(nrExec: Int, startLine: Int, scriptLine: ScriptLine, commands: Seq[RepeatCommandLine])