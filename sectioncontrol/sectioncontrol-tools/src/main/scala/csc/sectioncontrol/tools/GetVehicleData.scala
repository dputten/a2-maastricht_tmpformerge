/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.tools

import org.slf4j.LoggerFactory
import org.apache.hadoop.hbase.client.{ HBaseAdmin, HTable }
import csc.hbase.utils.HBaseTableKeeper
import java.io.File
import csc.sectioncontrol.storage._
import java.text.SimpleDateFormat
import java.util.Date
import org.apache.hadoop.hbase.{ TableName, HColumnDescriptor, HTableDescriptor }
import csc.sectioncontrol.storagelayer.corridor.CorridorService
import org.apache.curator.framework.CuratorFrameworkFactory
import csc.akkautils.DirectLoggingAdapter
import csc.curator.utils.{ Curator, CuratorToolsImpl }
import org.apache.curator.retry.RetryUntilElapsed
import csc.sectioncontrol.storagelayer.hbasetables.SerializerFormats

object GetVehicleData {
  lazy val log = LoggerFactory.getLogger(this.getClass.getName)
  val systemId = "A2"
  private var hbaseVehicleTable: Option[HTable] = None

  private val timeMargin = 30 * 60 * 1000 //30 minutes
  val dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS")

  type OptionMap = Map[Symbol, String]

  /**
   * Parse the command arguments
   */
  def nextOption(map: OptionMap, list: List[String]): OptionMap = {
    list match {
      case Nil ⇒ map
      case "--restore" :: value :: tail ⇒
        nextOption(map ++ Map('restore -> value), tail)
      case "--gantries" :: value :: tail ⇒
        nextOption(map ++ Map('gantries -> value), tail)
      case "--day" :: value :: tail ⇒
        nextOption(map ++ Map('day -> value), tail)
      case "--backup" :: value :: tail ⇒
        nextOption(map ++ Map('backup -> value), tail)
      case "--servers" :: value :: tail ⇒
        nextOption(map ++ Map('servers -> value), tail)
      case "--zaakRoot" :: value :: tail ⇒
        nextOption(map ++ Map('zaakRoot -> value), tail)
      case "--help" :: tail ⇒
        nextOption(map ++ Map('help -> ""), tail)
      case option :: tail ⇒
        println("Unknown option " + option)
        sys.exit(1)
    }
  }
  /**
   * Prints the usage string to standard out
   */
  def printUsage() {
    println("Usage (--restore <inputDir> || --backup <outputDir>) [--servers <zookeeper servers>] [--zaakRoot <rootDir>] --gantries <gantries>")
    println("\t--restore <inputDir>: the directory of the backup")
    println("\t--backup <zutputfile>: the directory where the backup has to be placed")
    println("\t--zaakRoot <rootDir>: The root directory where the zaakbestanden are placed; default /app/politie")
    println("\t--gantries <gantries>: The gantries to use seperated by a ,; default E1,X1,E2,X2,E3,X3")
    println("\t--servers <zookeeper servers>: the zookeeper servers quorum. default localhost:2181")
    println("\t--help: show the usage and script usage")
  }

  /**
   * The main tread.
   * Parse arguments and check them for correctness
   */
  def main(args: Array[String]): Unit = {
    val options = nextOption(Map(), args.toList)
    println(options)
    options.get('help) foreach {
      value ⇒
        {
          printUsage
          sys.exit(0)
        }
    }

    val restoreOpt = options.get('restore)
    val backupOpt = options.get('backup)
    if (restoreOpt.isEmpty && backupOpt.isEmpty) {
      printUsage
      sys.exit(1)
    }

    val servers = options.get('servers).getOrElse("localhost:2181")
    val hbaseConfig = HBaseTableKeeper.createCachedConfig(servers)
    val tableName = HBaseTableDefinitions.vehicleRecordTableName
    val adminHBase = new HBaseAdmin(hbaseConfig)
    try {
      if (!adminHBase.tableExists(tableName)) {
        val desc = new HTableDescriptor(TableName.valueOf(tableName))
        val meta = new HColumnDescriptor(RestoreVehicle.family)
        desc.addFamily(meta)
        adminHBase.createTable(desc)
      }
    } finally {
      adminHBase.close()
    }

    val table = new HTable(hbaseConfig, tableName)
    hbaseVehicleTable = Some(table)

    if (restoreOpt.isDefined) {
      val timesInserted = RestoreVehicle.restoreTable(table, new File(restoreOpt.getOrElse(".")))
      //create also calibration records
      val calibration = new DummyCalibrate(hbaseConfig)
      calibration.start()
      val curator = createCurator(servers)
      val corridorIds = CorridorService.getCorridorIds(curator, systemId)
      corridorIds.foreach(corridorId ⇒ calibration.createCalibrations(systemId = systemId, corridorId = corridorId, nrCameras = 10, reportingOfficerCode = "XX9999", timesInserted = timesInserted))
      calibration.shutdown()
      curator.curator.foreach(_.close())
    } else if (backupOpt.isDefined) {
      val output = new File(backupOpt.getOrElse("."))
      if (output.exists() && !output.isDirectory) {
        log.error("Specified output directory [%s]exists and isn't a directory".format(output.getAbsolutePath))
        sys.exit(2)
      }
      if (!output.exists()) {
        output.mkdirs()
      }
      val violations = BackupVehicle.getAllViolationsOfDay(options.get('day), options.get('zaakRoot)).sortBy(_.getEpochTime())
      if (violations.isEmpty) {
        log.error("No violations found")
        sys.exit(5)
      }
      val start = violations.head.getEpochTime() - timeMargin
      val end = violations.last.getEpochTime() + timeMargin
      val plateList = violations.map(_.license.replaceAll("-", "")) //Remove - from german licenses

      log.info("Start exporting %d violations from %s to %s".format(violations.size, dateFormat.format(new Date(start)), dateFormat.format(new Date(end))))

      createGantryList(options).foreach(gantry ⇒ {
        BackupVehicle.backupTable(table, systemId, gantry, start, end, plateList, output)
      })

    }
  }

  def createCurator(zkServers: String): Curator = {
    val retryPolicy = new RetryUntilElapsed(1000 * 60 * 60 * 24 * 3, 5000)

    val newClient = CuratorFrameworkFactory.newClient(zkServers, retryPolicy)
    val log = new DirectLoggingAdapter(getClass.getName)
    newClient.start()
    new CuratorToolsImpl(Some(newClient), log, SerializerFormats.formats)
  }

  def createGantryList(options: OptionMap): Seq[String] = {
    val gantries = options.getOrElse('gantries, "E1,X1,E2,X2,E3,X3")
    val gantryList = gantries.split(",")
    log.info("Use gantries %s".format(gantryList.toSeq))
    gantryList
  }
}