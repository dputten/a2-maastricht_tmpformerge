/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.tools.backup

import java.io.File
import csc.sectioncontrol.storagelayer.StorageFactory
import org.apache.curator.retry.ExponentialBackoffRetry
import csc.curator.utils.CuratorToolsImpl
import csc.sectioncontrol.storagelayer.backup.{ Importer, Exporter }
import csc.config.Path
import org.apache.commons.io.FileUtils
import csc.akkautils.DirectLogging

object RestoreZookeeper extends DirectLogging {
  type OptionMap = Map[Symbol, String]

  /**
   * Parse the command arguments
   */
  def nextOption(map: OptionMap, list: List[String]): OptionMap = {
    list match {
      case Nil ⇒ map
      case "--file" :: value :: tail ⇒
        nextOption(map ++ Map('file -> value), tail)
      case "--servers" :: value :: tail ⇒
        nextOption(map ++ Map('servers -> value), tail)
      case "--help" :: tail ⇒
        nextOption(map ++ Map('help -> ""), tail)
      case option :: tail ⇒
        println("Unknown option " + option)
        sys.exit(1)
    }
  }
  /**
   * Prints the usage string to standard out
   */
  def printUsage() {
    println("Usage --file <output file> [--servers]")
    println("\t--file <output file>: the output file")
    println("\t--servers <zookeeper servers>: the zookeeper servers quorum. default localhost:2181")
    println("\t--help: show the usage and script usage")
  }

  /**
   * The main tread.
   * Parse arguments and check them for correctness
   */
  def main(args: Array[String]): Unit = {
    val options = nextOption(Map(), args.toList)
    println(options)
    options.get('help) foreach {
      value ⇒
        {
          printUsage
          sys.exit(0)
        }
    }

    val backupOpt = options.get('file)
    if (backupOpt.isEmpty) {
      printUsage
      sys.exit(1)
    }
    val backupFile = new File(backupOpt.get)
    if (!backupFile.exists()) {
      println("Backup file %s does not exist".format(backupFile.getAbsolutePath))
      sys.exit(2)
    }

    val servers = options.get('servers).getOrElse("localhost:2181")
    val frame = StorageFactory.newClient(servers, new ExponentialBackoffRetry(60000, 10))
    frame.foreach(_.start())
    val curator = new CuratorToolsImpl(frame, log)
    val result = Importer.importConfig(curator, backupFile)
    result.foreach(line ⇒ {
      if (!line.success) {
        println(line.errorMessage.getOrElse("Failed to import [%s]".format(line.path)))
      }
    })
    println("Import done")
  }

}