/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.tools

import util.matching.Regex
import java.text.SimpleDateFormat
import java.io.File
import org.apache.commons.io.FileUtils
import collection.JavaConversions._
import org.slf4j.LoggerFactory

object ParseZaakbestand {
  lazy val log = LoggerFactory.getLogger(this.getClass.getName)
  // Header info starts with '#'
  private val header = new Regex("""#.*\n?""")

  private val checksum = new Regex("""^\S*\n?$""")

  // A violation record starts with ".", 'A', or 'V'
  private val violation = new Regex("""[\.AV] (\d*) (\d{8}) (\d{4}) (\S*).*""")

  def parseFile(file: File): Seq[ParsedRegistration] = {
    log.info("Parse zaakbestand %s".format(file.getAbsolutePath))
    val lines = FileUtils.readLines(file)
    parseLines(lines)
  }

  def parseLines(lines: Seq[String]): Seq[ParsedRegistration] = {
    lines.foldLeft(Seq[ParsedRegistration]()) {
      case (list, line) ⇒ {
        line match {
          case header()                                   ⇒ list
          case checksum()                                 ⇒ list
          case violation(zaaknummer, date, time, license) ⇒ list :+ new ParsedRegistration(zaaknummer, date, time, license)
          case _ ⇒ {
            log.warn("failed to parse line <%s>".format(line))
            list
          }
        }
      }
    }
  }
}

case class ParsedRegistration(nr: String, date: String, time: String, license: String) {
  def getEpochTime(): Long = {
    val dateFormat = new SimpleDateFormat("yyyyMMdd hhmmss.SSS")
    val joinedDate = "%s %s00.000".format(date, time)
    dateFormat.parse(joinedDate).getTime
  }
}
