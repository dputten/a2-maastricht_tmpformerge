/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.tools

import java.io.{ FileFilter, File }
import org.apache.hadoop.hbase.client.{ Put, HTable }
import org.apache.hadoop.hbase.util.Bytes
import org.apache.commons.io.FileUtils
import org.slf4j.LoggerFactory
import csc.sectioncontrol.storage.HBaseTableDefinitions._
import java.util.Date
import util.matching.Regex

object RestoreVehicle {
  lazy val log = LoggerFactory.getLogger(RestoreVehicle.getClass.getName)
  val family = vehicleRecordColumnFamily.getBytes
  val meta = vehicleRecordColumnName.getBytes
  val overviewImage = vehicleImageOverviewColumnName.getBytes()
  val license = vehicleImageLicenseColumnName.getBytes()

  def restoreTable(table: HTable, restoreDir: File): Seq[(Long, String)] = {
    val metaFiles = restoreDir.listFiles(new FileFilter {
      def accept(p1: File) = p1.getName.endsWith("_M")
    })
    log.info("Number of records to import %d".format(metaFiles.size))
    var updated = 0
    var listOfTimes = Seq[(Long, String)]()

    metaFiles.foreach(metaFile ⇒ {
      try {
        val base = metaFile.getName.substring(0, metaFile.getName.length - 2)
        val row = ByteUtil.createByteArray(base)
        val valueMeta = FileUtils.readFileToByteArray(metaFile)
        val imageOverview = FileUtils.readFileToByteArray(new File(metaFile.getParentFile, base + "_O"))
        val imageLicense = FileUtils.readFileToByteArray(new File(metaFile.getParentFile, base + "_L"))

        val newRow = new Put(row)
        newRow.add(family, meta, valueMeta)
        newRow.add(family, overviewImage, imageOverview)
        newRow.add(family, license, imageLicense)
        table.put(newRow)
        listOfTimes = listOfTimes :+ (getTime(valueMeta), getSerialNr(valueMeta))
        updated += 1
      } catch {
        case ex: Exception ⇒ {
          log.error("Failed to import %s: %s".format(metaFile.getAbsolutePath, ex.getMessage))
          ex.printStackTrace()
        }
      }
    })
    table.flushCommits()
    log.info("Number of records imported %d".format(updated))
    listOfTimes
  }

  def getTime(json: Array[Byte]): Long = {
    //"eventTimestamp":1384398823448,
    val eventTimeExp = new Regex(""".*"eventTimestamp":(\d*).*""")
    Bytes.toString(json) match {
      case eventTimeExp(time) ⇒ time.toLong
      case other ⇒ {
        log.info("Failed to find eventTimestamp in " + json)
        0L
      }
    }
  }
  def getSerialNr(json: Array[Byte]): String = {
    //"serialNr":"SN1234",
    val eventTimeExp = new Regex(""".*"serialNr":"(.*)".*""")
    Bytes.toString(json) match {
      case eventTimeExp(nr) ⇒ nr
      case other ⇒ {
        log.info("Failed to find serialNr in " + json)
        ""
      }
    }
  }
}