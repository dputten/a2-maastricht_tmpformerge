/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.tools.hbasesim

import scala.util.parsing.combinator.JavaTokenParsers

/**
 * Representation of one read scriptLine
 * @param delay: the delay time before executing this command
 * @param command the read command
 * @param options A map of options needed for the command to execute
 */
case class ScriptLine(delay: Int, command: String, options: Map[String, String])

/**
 * Representation of a config line
 * @param config type of configuration
 * @param options the configuration options
 */
case class ConfigLine(config: String, options: Map[String, String])

/**
 * Possible errors while parsing the script file
 */
class ParserException(msg: String) extends Exception(msg)

/**
 * All commands and options used in the simulator and script files
 */
object SimConstants {
  val COMMENT = "COMMENT"
  val CONFIG_START = "CONFIG-START"
  val CONFIG_END = "CONFIG-END"
  val CONFIG_LANE = "LANE"
  val LANE_ID = "LANE-ID"
  val LANE_NAME = "NAME"
  val LANE_GANTRY = "GANTRY"
  val LANE_SYSTEM = "SYSTEM"
  val LANE_GPSLONG = "GPS-LONG"
  val LANE_GPSLAT = "GPS-LAT"
  val CHECKSUM = "CHECKSUM"
  val DEFAULTS = "DEFAULTS"
  val PICTURE_DEST = "PICTURE_DEST"

  val TIME = "TIME"

  val MODE = "MODE"
  val START = "START"

  val NMI_CERTIFICATE = "NMI-CERT"
  val APPL_CHECKSUM = "APPL-CHECKSUM"

  val CONFIG_CORRIDOR = "CORRIDOR"
  val CORRIDOR_ID = "CORRIDOR-ID"
  val START_LANE_ID = "START-LANE-ID"
  val END_LANE_ID = "END-LANE-ID"
  val CORRIDOR_LENGTH = "CORRIDOR-LENGTH"

  val SCRIPT_START = "SCRIPT-START"
  val SCRIPT_END = "SCRIPT-END"

  val LANE_REGISTRATION = "LANE-REGISTRATION"
  val CORRIDOR_REGISTRATION = "CORRIDOR-REGISTRATION"
  val CALIBRATE = "CALIBRATE"
  val PROCESSED_UNTIL = "PROCESSED"

  val OFFICER_CODE = "OFFICER_CODE"
  val RESULT = "RESULT"

  val LICENSE = "LICENSE"
  val LICENSE_CONF = "LICENSE-CONF"
  val LANE_ID_1 = "LANE_ID_1"

  val LENGTH = "LENGTH"
  val LENGTH_CONF = "LENGTH-CONF"

  val CATEGORY = "CATEGORY"
  val CATEGORY_CONF = "CATEGORY-CONF"

  val SPEED = "SPEED"
  val SPEED_CONF = "SPEED-CONF"

  val COUNTRY = "COUNTRY"
  val COUNTRY_CONF = "COUNTRY-CONF"

  val TIME_YELLOW = "TIME-YELLOW"
  val TIME_RED = "TIME-RED"
  val IMAGE = "IMAGE"
  val CAMERA_ID = "CAMERA-ID"

  val SERIAL_NR = "SERIAL-NR"

  val WAIT = "WAIT"
  val INPUT = "INPUT"

  val REPEAT_START = "REPEAT-START"
  val REPEAT_END = "REPEAT-END"
  val REPEAT_NUMBER = "REPEAT-NUMBER"

}

/**
 * The parser of the Vehicle Simulate script file
 */
object VehicleParser extends JavaTokenParsers {
  override val whiteSpace = """[ \t]+""".r
  def eol: Parser[Any] = """(\r?\n)+""".r

  /**
   * Parse the script lines
   * @param script: one or more script lines which have to be parsed
   * @return List of all parsed scriptlines
   */
  def parseScript(script: String): List[AnyRef] = parseAll(lines, script) match {
    case Success(result, next) ⇒ result
    case f: Failure            ⇒ throw new ParserException(f.toString)
    case f: Error              ⇒ throw new ParserException(f.toString)
  }
  //create list of scriptlines
  def lines: Parser[List[AnyRef]] = rep(configlines | instructions | rootComment) ^^ { case values ⇒ values.flatten }

  def rootComment: Parser[List[ScriptLine]] = comment ^^ { List(_) }

  def configlines: Parser[List[AnyRef]] = SimConstants.CONFIG_START ~ eol ~ configList ~ SimConstants.CONFIG_END ~ opt(eol) ^^ {
    case start ~ nl ~ values ~ end ~ opt ⇒ List() ++ values
  }

  def configList: Parser[List[ConfigLine]] = rep(config | comment) ^^ { case value ⇒ List() ++ value.filter(_.isInstanceOf[ConfigLine]).asInstanceOf[List[ConfigLine]] }

  def config: Parser[ConfigLine] = configItems ~ opt(cmdArgs) ~ eol ^^ {
    case key ~ Some(values) ~ nl ⇒ new ConfigLine(key, values)
    case key ~ None ~ nl         ⇒ new ConfigLine(key, Map())
  }

  def configItems: Parser[String] = SimConstants.CONFIG_LANE | SimConstants.CONFIG_CORRIDOR | SimConstants.CHECKSUM |
    SimConstants.DEFAULTS | SimConstants.TIME

  def instructions: Parser[List[ScriptLine]] = SimConstants.SCRIPT_START ~ eol ~ rep(instr | comment | repeatIns) ~ SimConstants.SCRIPT_END ~ opt(eol) ^^ {
    case start ~ values ~ end ~ opt ⇒ List() ++ values
  }

  def repeatIns: Parser[ScriptLine] = (SimConstants.REPEAT_START | SimConstants.REPEAT_END) ~ cmdArgs ~ eol ^^ { case command ~ cmdArgs ~ nl ⇒ (new ScriptLine(0, command, cmdArgs)) }
  //parse comment line
  def comment: Parser[ScriptLine] = "#" ~> "[^\n]*".r <~ opt(eol) ^^ { case comm ⇒ { new ScriptLine(0, SimConstants.COMMENT, Map(SimConstants.COMMENT -> comm)) } }

  //parse a instruction line
  def instr: Parser[ScriptLine] = delay ~ command ~ cmdArgs ~ eol ^^ { case delay ~ command ~ cmdArgs ~ nl ⇒ (new ScriptLine(delay, command, cmdArgs)) }
  //supported commands
  def command: Parser[String] = SimConstants.LANE_REGISTRATION | SimConstants.WAIT | SimConstants.CORRIDOR_REGISTRATION | SimConstants.CALIBRATE | SimConstants.PROCESSED_UNTIL
  """[^#]\S+""".r ^^ { case value ⇒ throw new ParserException("Unknown command: " + value) }
  //parse the delay
  def delay: Parser[Int] = wholeNumber ^^ { case wholeNumber ⇒ wholeNumber.toInt }

  //define all possible arguments
  def cmdArgs: Parser[Map[String, String]] = rep(argIntValue | argStringValue | argument | argNumberValue | unknownArg) ^^ { Map() ++ _ }
  //implement unsupported argument
  def unknownArg: Parser[(String, String)] = """[^#\n]\S+""".r ^^ { case value ⇒ throw new ParserException("Unknown argument or missing value: " + value) }
  //parse arguments without a value
  def argument: Parser[(String, String)] = argKey ^^ { case argKey ⇒ (argKey, "") }
  //all supported arguments without a value
  def argKey: Parser[String] = SimConstants.INPUT
  //arguments with Integers
  def argIntValue: Parser[(String, String)] = intKey ~ "=" ~ intValue ^^ { case intKey ~ is ~ intValue ⇒ (intKey, intValue) }
  //all arguments which have a int as value
  def intKey: Parser[String] = SimConstants.COUNTRY_CONF | SimConstants.CATEGORY_CONF | SimConstants.SPEED_CONF | SimConstants.LENGTH_CONF |
    SimConstants.LICENSE_CONF | SimConstants.TIME_RED | SimConstants.TIME_YELLOW | SimConstants.REPEAT_NUMBER
  //parse int value
  def intValue: Parser[String] = wholeNumber ^^ { case wholeNumber ⇒ wholeNumber }
  //parse numberargument
  def argNumberValue: Parser[(String, String)] = numberKey ~ "=" ~ nrValue ^^ { case key ~ is ~ value ⇒ (key, value) }
  def numberKey: Parser[String] = SimConstants.LANE_GPSLONG | SimConstants.LANE_GPSLAT | SimConstants.LENGTH | SimConstants.CORRIDOR_LENGTH | SimConstants.SPEED
  //parse float value
  def nrValue: Parser[String] = decimalNumber ^^ { case nr ⇒ nr }

  //arguments with Strings
  def argStringValue: Parser[(String, String)] = stringKey ~ "=" ~ stringValue ^^ { case stringKey ~ is ~ stringValue ⇒ (stringKey, stringValue) }
  //all arguments which have a String as value
  def stringKey: Parser[String] = SimConstants.IMAGE | SimConstants.NMI_CERTIFICATE | SimConstants.APPL_CHECKSUM | SimConstants.COUNTRY |
    SimConstants.LANE_NAME | SimConstants.LANE_GANTRY | SimConstants.LANE_SYSTEM | SimConstants.LICENSE | SimConstants.LANE_ID | SimConstants.CORRIDOR_ID |
    SimConstants.START_LANE_ID | SimConstants.END_LANE_ID | SimConstants.CATEGORY | SimConstants.RESULT | SimConstants.OFFICER_CODE |
    SimConstants.MODE | SimConstants.START | SimConstants.SERIAL_NR

  //parse stringvalue
  def stringValue: Parser[String] = unquotedString | quotedString
  def unquotedString: Parser[String] = """[^"][\S]*""".r
  def quotedString: Parser[String] = """"[ \S]*"""".r ^^ {
    case str ⇒ {
      str.substring(1, str.length - 1)
    }
  }
}