/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.tools

import java.math.BigInteger

object ByteUtil {
  def createHexString(array: Array[Byte]): String = {
    array.map("%02X" format _).mkString
  }
  def createByteArray(str: String): Array[Byte] = {
    var byteArray = new BigInteger(str, 16).toByteArray();
    //it is possible that the str starts with 00 and then we are missing these bytes
    //so we need to add these
    val neededLength = str.length / 2
    while (byteArray.length < neededLength) {
      byteArray = 0.toByte +: byteArray
    }
    byteArray
  }

}