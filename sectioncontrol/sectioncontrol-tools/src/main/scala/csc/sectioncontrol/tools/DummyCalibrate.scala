/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.tools

import org.apache.hadoop.hbase.client.{ Put, HBaseAdmin, HTable }
import org.apache.hadoop.hbase.{ TableName, HColumnDescriptor, HTableDescriptor }
import org.apache.hadoop.conf.Configuration
import java.util.{ Calendar, Date }
import org.apache.hadoop.hbase.util.Bytes
import collection.mutable.ListBuffer
import csc.sectioncontrol.storage.{ SelfTestResult, KeyWithTimeAndId }
import net.liftweb.json.{ DefaultFormats, Serialization }
import java.text.SimpleDateFormat
import org.slf4j.LoggerFactory

class DummyCalibrate(hbaseConfig: Configuration, tableNameResults: String = "SelftestResult", tableNameImages: String = "SelftestImage") {
  val log = LoggerFactory.getLogger("DummyCalibrate")
  private val columnFamily = "cf".getBytes()
  private val attributeResult = "R".getBytes()
  private val attributeImage = "I".getBytes()

  private var resultTable: Option[HTable] = None
  private var imageTable: Option[HTable] = None

  private val dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS")

  def createCalibrations(systemId: String, corridorId: String, nrCameras: Int, reportingOfficerCode: String, timesInserted: Seq[(Long, String)]) {
    val (beginTime, endTime) = timesInserted.foldLeft(Long.MaxValue, 0L) {
      case ((begin, end), current) ⇒ {
        if (current == 0) {
          (begin, end)
        } else {
          (math.min(begin, current._1), math.max(end, current._1))
        }
      }
    }
    val beginCal = Calendar.getInstance()
    beginCal.setTime(new Date(beginTime))
    val endCal = Calendar.getInstance()
    endCal.setTime(new Date(endTime))
    val startDay = beginCal.get(Calendar.DAY_OF_YEAR)
    val endDay = endCal.get(Calendar.DAY_OF_YEAR)
    beginCal.set(Calendar.HOUR_OF_DAY, 0)
    beginCal.set(Calendar.MINUTE, 0)
    beginCal.set(Calendar.SECOND, 0)
    beginCal.set(Calendar.MILLISECOND, 0)

    val startNr = timesInserted.headOption.map(_._2).getOrElse("NotUsedSerialNr")
    val start = (beginCal.getTime.getTime, startNr)
    val serialMap = timesInserted.map(_._2).distinct
    if (serialMap.size > 1) {
      log.warn("Found multiple serial numbers. This isn't suported yet. registrations (%s) will be pardoned".format(serialMap.tail))
    }
    val serialNr = serialMap.headOption.getOrElse("")

    for (day ← startDay to endDay) {
      beginCal.set(Calendar.DAY_OF_YEAR, day)
      log.info("Create calibration for %s".format(dateFormat.format(beginCal.getTime())))
      store(systemId, corridorId, nrCameras, beginCal.getTime(), reportingOfficerCode, serialNr)
    }
    beginCal.add(Calendar.DAY_OF_YEAR, 1)
    log.info("Create calibration for %s".format(dateFormat.format(beginCal.getTime())))
    store(systemId, corridorId, nrCameras, beginCal.getTime(), reportingOfficerCode, serialNr)
  }

  def start() {
    val adminHBase = new HBaseAdmin(hbaseConfig)
    try {
      if (resultTable == None) {
        if (!adminHBase.tableExists(tableNameResults)) {
          val desc = new HTableDescriptor(TableName.valueOf(tableNameResults))
          val meta = new HColumnDescriptor(columnFamily)
          desc.addFamily(meta)
          adminHBase.createTable(desc)
        }
        resultTable = Some(new HTable(hbaseConfig, tableNameResults))
      }
      if (imageTable == None) {
        if (!adminHBase.tableExists(tableNameImages)) {
          val desc = new HTableDescriptor(TableName.valueOf(tableNameImages))
          val meta = new HColumnDescriptor(columnFamily)
          desc.addFamily(meta)
          adminHBase.createTable(desc)
        }
        imageTable = Some(new HTable(hbaseConfig, tableNameImages))
      }
    } finally {
      adminHBase.close()
    }
  }

  def shutdown() {
    resultTable.foreach { table ⇒
      table.synchronized {
        try {
          table.flushCommits()
          table.close()
        } finally {
          resultTable = None
        }
      }
    }

    imageTable.foreach { table ⇒
      table.synchronized {
        try {
          table.flushCommits()
          table.close()
        } finally {
          imageTable = None
        }
      }
    }
  }

  /**
   * Create the key used for storing the self test result
   * @param systemId the system Id
   * @param time the time of the self test
   * @return the result key
   */
  private def createResultKey(systemId: String, time: Date): Array[Byte] = {
    systemId.getBytes ++ Bytes.toBytes(time.getTime)
  }

  /**
   * dummy image creator.
   * The image must have the right header because enforce process will check for it
   * @return byte array that contains a fake image
   */
  private def createImage: Array[Byte] = {
    //image.slice(0, 4) must comply with MarkerJFIF1
    val MarkerJFIF1: Array[Byte] = Array(0xFF.toByte, 0xD8.toByte, 0xFF.toByte, 0xE0.toByte)

    //image.slice(6, 11) must comply with MarkerJFIF2
    val MarkerJFIF2: Array[Byte] = Array(0x4A.toByte, 0x46.toByte, 0x49.toByte, 0x46.toByte, 0x00.toByte)

    //middle part is filler
    MarkerJFIF1 ++ Array(0xFF.toByte, 0xFF.toByte) ++ MarkerJFIF2
  }

  /**
   * Store a self test result into hbase
   * @param systemId the systemId
   * @param nrCameras the number of cameras tested
   * @param time the time of the self test
   */
  def store(systemId: String, corridorId: String, nrCameras: Int, time: Date, reportingOfficerCode: String, serialNr: String) {
    //store all images
    val listImageKeys = new ListBuffer[KeyWithTimeAndId]

    imageTable.foreach(table ⇒ {
      table.synchronized {
        for (image ← 0 until nrCameras) {
          val key = new KeyWithTimeAndId(time.getTime, "Image" + image)

          val put = new Put(Bytes.toBytes(key.time) ++ key.id.getBytes())
          put.add(columnFamily, attributeImage, time.getTime, createImage)
          table.put(put)
          listImageKeys += key
        }
        table.flushCommits()
      }
    })

    //store result
    resultTable.foreach(table ⇒ {
      val key = createResultKey(systemId, time)
      val result = SelfTestResult(
        systemId = systemId,
        corridorId = corridorId,
        reportingOfficerCode = reportingOfficerCode,
        nrCamerasTested = nrCameras,
        time = time.getTime,
        success = true,
        images = listImageKeys.toSeq,
        errors = Seq(),
        serialNr = serialNr)
      val json = Serialization.write(result)(DefaultFormats)
      table.synchronized {
        val put = new Put(key)
        put.add(columnFamily, attributeResult, time.getTime, json.getBytes)
        table.put(put)
        table.flushCommits()
      }
    })
  }

}