/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.tools

import java.io.File
import org.apache.hadoop.hbase.client.{ Scan, HTable }
import org.apache.commons.io.FileUtils
import org.apache.hadoop.hbase.util.Bytes
import util.matching.Regex
import org.slf4j.LoggerFactory
import csc.sectioncontrol.storage.HBaseTableDefinitions._
import java.math.BigInteger

object BackupVehicle {
  lazy val log = LoggerFactory.getLogger(BackupVehicle.getClass.getName)
  private val family = vehicleRecordColumnFamily.getBytes
  private val meta = vehicleRecordColumnName.getBytes
  private val overviewImage = vehicleImageOverviewColumnName.getBytes()
  private val license = vehicleImageLicenseColumnName.getBytes()

  def getAllViolationsOfDay(dayOpt: Option[String], zaakRoot: Option[String]): Seq[ParsedRegistration] = {
    dayOpt match {
      case None ⇒ {
        log.error("Day isn't filled")
        sys.exit(3)
      }
      case Some(dayStr) ⇒ {
        val rootDir = new File(zaakRoot.getOrElse("/app/politie"))
        if (!rootDir.exists() || !rootDir.isDirectory) {
          log.error("Root of zaakbestanden [%s]doesn't exists".format(rootDir.getAbsolutePath))
          sys.exit(4)
        }
        val allSections = rootDir.listFiles()
        if (allSections == null || allSections.isEmpty) {
          log.error("No sections found in [%s]".format(rootDir.getAbsolutePath))
          sys.exit(5)
        }
        allSections.flatMap(sectionDir ⇒ {
          var violations = Seq[ParsedRegistration]()
          val auto = new File(sectionDir, dayStr + "/auto/files/zaakauto.txt")
          if (auto.exists()) {
            violations = violations ++ ParseZaakbestand.parseFile(auto)
          }
          val hand = new File(sectionDir, dayStr + "/hand/files/zaakhand.txt")
          if (hand.exists()) {
            violations = violations ++ ParseZaakbestand.parseFile(hand)
          }
          val mobi = new File(sectionDir, dayStr + "/mobi/files/zaakmobi.txt")
          if (mobi.exists()) {
            violations = violations ++ ParseZaakbestand.parseFile(mobi)
          }
          violations
        }).toSeq
      }
    }
  }

  def backupTable(table: HTable, systemId: String, gantryId: String, start: Long, end: Long, listPlates: Seq[String], outputDir: File) {
    import collection.JavaConversions._

    val preKey = makePrekey(systemId, gantryId)
    val startKey = makeReaderKey(preKey, start)
    val stopKey = makeReaderKey(preKey, end)

    val scan = new Scan(startKey, stopKey)
    scan.addFamily(family)
    val scanner = table.getScanner(scan)
    var nrExported = 0
    var nrRecords = 0
    try {
      scanner.foreach(r ⇒ {
        nrRecords += 1
        val row = r.getRow
        val valueMeta = r.getValue(family, meta)
        if (needToExport(valueMeta, listPlates)) {
          nrExported += 1
          val base = ByteUtil.createHexString(row)
          FileUtils.writeByteArrayToFile(new File(outputDir, base + "_M"), valueMeta)
          val imageOverview = r.getValue(family, overviewImage)
          FileUtils.writeByteArrayToFile(new File(outputDir, base + "_O"), imageOverview)
          val imageLicense = r.getValue(family, license)
          FileUtils.writeByteArrayToFile(new File(outputDir, base + "_L"), imageLicense)
        }
      })
    } finally {
      scanner.close()
      log.info("Nr of records found    %d for gantry %s".format(nrRecords, gantryId))
      log.info("Nr of records exported %d for gantry %s".format(nrExported, gantryId))
    }
  }

  def needToExport(valueMeta: Array[Byte], listPlates: Seq[String]): Boolean = {
    if (listPlates.isEmpty) {
      true
    } else {
      val license = getLicense(valueMeta)
      listPlates.contains(license)
    }
  }

  def getLicense(json: Array[Byte]): String = {
    //"license":{"value":"80WJLS","confidence":90}
    val licenseExp = new Regex(""".*"license":\{"value":"([^"]*).*""")
    Bytes.toString(json) match {
      case licenseExp(lic) ⇒ lic
      case other ⇒ {
        log.info("Failed to find license in " + json)
        ""
      }
    }
  }

  def makePrekey(systemId: String, gantryId: String): String = {
    systemId + "-" + gantryId + "-"
  }
  def makeReaderKey(keyValue: (String, Long)) = Bytes.toBytes(keyValue._1) ++ Bytes.toBytes(keyValue._2)

}