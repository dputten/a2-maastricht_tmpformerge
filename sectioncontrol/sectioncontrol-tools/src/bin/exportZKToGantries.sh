#! /bin/sh
# export the current central zookeeper
rm /home/ctes/tools/tools/config/ZKexportPRDfull.xml
/home/ctes/tools/tools/bin/backup.sh --node /ctes --file /home/ctes/tools/tools/config/ZKexportPRDfull.xml
# remove zookeeper nodes /ctes/users and /ctes/configuration/reports from the zk export file (no security sensitive data on the gantries)
java -cp /home/ctes/tools/tools/bin/xalan.jar org.apache.xalan.xslt.Process -in /home/ctes/tools/tools/config/ZKexportPRDfull.xml -xsl stylesheet.xslt -out /home/ctes/tools/tools/config/ZKexportPRD.xml

# copy the zookeper export file to the gantries of the cluster
rsync /home/ctes/tools/tools/config/ZKexportPRD.xml ctes@a4le1:/app/
rsync /home/ctes/tools/tools/config/ZKexportPRD.xml ctes@a4lx2:/app/
#rsync /home/ctes/tools/tools/config/ZKexportPRD.xml ctes@a4re3:/app/
#rsync /home/ctes/tools/tools/config/ZKexportPRD.xml ctes@a4rx4:/app/
