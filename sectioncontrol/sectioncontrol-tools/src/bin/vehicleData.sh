#! /bin/sh

TOOLS_HOME="$(cd "$(cd "$(dirname "$0")"; pwd -P)"/..; pwd)"

#
# Get the Java command
#
if [ -z "$JAVACMD" ] ; then
  if [ -n "$JAVA_HOME"  ] ; then
    if [ -x "$JAVA_HOME/jre/sh/java" ] ; then
      # IBM's JDK on AIX uses strange locations for the executables
      JAVACMD="$JAVA_HOME/jre/sh/java"
    else
      JAVACMD="$JAVA_HOME/bin/java"
    fi
  else
    JAVACMD=`which java 2> /dev/null `
    if [ -z "$JAVACMD" ] ; then
        JAVACMD=java
    fi
  fi
fi

if [ ! -x "$JAVACMD" ] ; then
  echo "Error: JAVA_HOME is not defined correctly."
  echo "  We cannot execute $JAVACMD"
  exit 1
fi

TOOLS_CLASSPATH="$TOOLS_HOME/config:$TOOLS_HOME/deploy/*:$TOOLS_HOME/lib/*"
JAVA_OPTS="-Xms512M -Xmx512M -Xss1M -XX:MaxPermSize=128M -XX:+UseParallelGC"

#
# Execute command
#
javaCommandLine="$JAVACMD $JAVA_OPTS -cp "$TOOLS_CLASSPATH" csc.sectioncontrol.tools.GetVehicleData $@"

(cd $TOOLS_HOME/bin; sh -c "$javaCommandLine" || return 1)

