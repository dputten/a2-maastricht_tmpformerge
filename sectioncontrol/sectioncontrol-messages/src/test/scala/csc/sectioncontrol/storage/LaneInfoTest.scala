package csc.sectioncontrol.storage

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.messages.Lane

/**
 * Copyright (C) 2016 CSC. <http://www.csc.com>
 *
 * Created on 6/30/16.
 */

class LaneInfoTest extends WordSpec with MustMatchers {

  val lane1 = Lane("laneId1", "laneName1", "gantry", "A4", 1.0, 1.0, None)
  val lane2 = Lane("laneId2", "laneName2", "gantry", "A4", 2.0, 2.0, None)
  val lane3 = Lane("laneId3", "laneName3", "gantry", "A4", 3.0, 3.0, None)

  "LaneInfo must properly coalesce a sequence of LaneInfo instances" in {
    val laneInfos = Seq(LaneInfo(lane = lane1, count = 1),
      LaneInfo(lane = lane2, count = 2),
      LaneInfo(lane = lane3, count = 3),
      LaneInfo(lane = lane1, count = 1),
      LaneInfo(lane = lane2, count = 2),
      LaneInfo(lane = lane3, count = 3),
      LaneInfo(lane = lane1, count = 1),
      LaneInfo(lane = lane2, count = 2),
      LaneInfo(lane = lane3, count = 3))

    val result = LaneInfo.coalesce(laneInfos)
    result.size must be(3)

    val laneMap: Map[Lane, Seq[LaneInfo]] = result.groupBy(_.lane)
    val lis1 = laneMap(lane1)
    val lis2 = laneMap(lane2)
    val lis3 = laneMap(lane3)

    lis1.size must be(1)
    lis1(0).count must be(3)

    lis2.size must be(1)
    lis2(0).count must be(6)

    lis3.size must be(1)
    lis3(0).count must be(9)
  }
}
