/*
* Copyright (C) 2012 CSC. <http://www.csc.com>
*/

package csc.sectioncontrol.storage

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

class CalibrationStatePathsTest extends WordSpec with MustMatchers {
  "CalibrationState.getDefaultPath" must {
    "return the correct path for calibration-state" in {
      Paths.CalibrationState.getDefaultPath("A4", "R1") must be("/ctes/systems/A4/corridors/R1/control/calibration/trigger")
    }

    "fail when called with incorrect parameter for systemId" in {
      intercept[java.lang.IllegalArgumentException] {
        Paths.CalibrationState.getDefaultPath("", "R1")
      }
    }

    "fail when called with incorrect parameter for corridorId" in {
      intercept[java.lang.IllegalArgumentException] {
        Paths.CalibrationState.getDefaultPath("A4", "")
      }
    }
  }
}

class StatePathsTest extends WordSpec with MustMatchers {
  "State.getDefaultPath" must {
    "return the correct path for corridor state" in {
      Paths.State.getCurrentPath("A4", "R1") must be("/ctes/systems/A4/corridors/R1/control/state/current")
    }

    "fail when called with incorrect parameter for systemId" in {
      intercept[java.lang.IllegalArgumentException] {
        Paths.State.getCurrentPath("", "R1")
      }
    }

    "fail when called with incorrect parameter for corridorId" in {
      intercept[java.lang.IllegalArgumentException] {
        Paths.State.getCurrentPath("A4", "")
      }
    }
  }
}

