package csc.sectioncontrol.storage.decorate

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.storage.Decorate.{ ImageColorConfig, ImageCorrectionConfig }

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 9/17/15.
 */

class ImageCorrectionConfigTest extends WordSpec with MustMatchers {
  "ImageCorrectionConfig" must {
    "reject invalid time formats" in {

      val thrownFrom = intercept[Exception] {
        ImageCorrectionConfig("Id", "wrong time", "wrong time", ImageColorConfig())
      }
      assert(thrownFrom.getMessage === "requirement failed: from must be in format HH:mm:ss")

      val thrownTo = intercept[Exception] {
        ImageCorrectionConfig("Id", "00:00:00", "wrong time", ImageColorConfig())
      }
      assert(thrownTo.getMessage === "requirement failed: to must be in format HH:mm:ss")
    }

    "properly parse time strings to correct value" in {
      val test = ImageCorrectionConfig("Id", "00:00:00", "23:59:59", ImageColorConfig())
      test.fromDate.getTime must be(-3600000)
      test.toDate.getTime must be(82799000)
    }

  }
}
