package csc.sectioncontrol.storage

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

class MessagesTest extends WordSpec with MustMatchers {
  val msi1 = ZkMsiIdentification(1, "1", "some value")
  val msi2 = ZkMsiIdentification(1, "1", "other value")
  val msi3 = ZkMsiIdentification(1, "2", "yet another value")
  val msi4 = ZkMsiIdentification(2, "1", "and yet another value")

  "ZkMsiIdentification" must {

    "correctly compare based on identity" in {
      msi1 == msi1 must be(true)
      msi1 == msi2 must be(true)
      msi2 == msi1 must be(true)
      msi2 == msi2 must be(true)

      msi1 == msi3 must be(false)
      msi1 == msi4 must be(false)
      msi3 == msi1 must be(false)
      msi4 == msi1 must be(false)

      msi2 == msi3 must be(false)
      msi2 == msi4 must be(false)
      msi3 == msi2 must be(false)
      msi4 == msi2 must be(false)

      msi1 equals msi1 must be(true)
      msi1 equals msi2 must be(true)
      msi2 equals msi1 must be(true)
      msi2 equals msi2 must be(true)

      msi1 equals msi3 must be(false)
      msi1 equals msi4 must be(false)
      msi3 equals msi1 must be(false)
      msi4 equals msi1 must be(false)

      msi2 equals msi3 must be(false)
      msi2 equals msi4 must be(false)
      msi3 equals msi2 must be(false)
      msi4 equals msi2 must be(false)
    }

    "correctly behave with Sets" in {
      val s1 = Set(msi1, msi2)
      val s2 = Set(msi1, msi2)
      val s3 = Set(msi2, msi3)
      s1.size must be(1)
      s1.contains(msi2) must be(true)
      s1.contains(msi3) must be(false)

      s1 == s2 must be(true)
      s2 == s1 must be(true)
      s1 == s3 must be(false)
      s2 == s3 must be(false)
    }

    "correctly behave with Lists" in {
      val l = Seq(msi1)
      l.contains(msi2) must be(true)
      l.contains(msi3) must be(false)
    }

  }
}

