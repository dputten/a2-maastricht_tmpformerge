/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.messages

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import java.util.{ Calendar, TimeZone }

class StatsDurationTest extends WordSpec with MustMatchers {

  "invalid StatsDuration" must {

    "fail if key is null" in {
      evaluating {
        StatsDuration(null, "Europe/Amsterdam")
      } must produce[IllegalArgumentException]
    }

    "fail if key is empty" in {
      evaluating {
        StatsDuration(" ", "Europe/Amsterdam")
      } must produce[IllegalArgumentException]
    }

    "fail if key has dashes" in {
      evaluating {
        StatsDuration("2012-04-01", "Europe/Amsterdam")
      } must produce[IllegalArgumentException]
    }

    "fail if key is too much in future" in {
      evaluating {
        StatsDuration("2112-04-01", "Europe/Amsterdam")
      } must produce[IllegalArgumentException]
    }
  }

  val timestamp = 1336387661859L
  "StatsDuration" must {
    val duration = StatsDuration(timestamp, TimeZone.getTimeZone("Europe/Amsterdam"))

    "be formatted properly" in {
      duration.key must be("20120507")
    }

    "calculate tomorrow" in {
      duration.tomorrow.key must be("20120508")
    }
  }

  "StatsDuration with specified timezone" must {
    val duration = StatsDuration(timestamp, TimeZone.getTimeZone("GMT+1:00"))

    "be formatted properly" in {
      duration.key must be("20120507")
    }
  }
  "StatsDuration with specified timezone for the past" must {
    val duration = StatsDuration(timestamp, TimeZone.getTimeZone("GMT+10:00"))

    "be formatted properly" in {
      duration.key must be("20120506")
    }

    "generate readable datum" in {
      duration.datum must be("2012-05-06")
    }
  }

  "StatsDuration for now" must {
    val duration = StatsDuration(System.currentTimeMillis(), TimeZone.getTimeZone("Europe/Amsterdam"))

    "be formatted properly" in {
      duration.key.matches("""\d\d\d\d\d\d\d\d""") must be(true)
    }
  }

  "StatsDuration with text date" must {

    "be formatted properly" in {
      val time = 1338206471683L
      //1338156000000L -> GMT: Sun, 27 May 2012 22:00:00 GMT
      //1338156000000L -> CEST: Sun, 27 May 2012 24:00:00 GMT
      val timeMidnight = 1338156000000L
      val duration = StatsDuration(time, TimeZone.getTimeZone("Europe/Amsterdam"))
      duration.key.matches("20120528") must be(true)
      val diff = time - duration.start
      diff must be(time - timeMidnight)
      //
      val duration2 = StatsDuration("20120528", TimeZone.getTimeZone("Europe/Amsterdam"))
      duration2.key must be("20120528")
      duration2.start must be(timeMidnight)
    }

    "be parsed properly" in {
      val duration = StatsDuration("20120528", "Europe/Amsterdam")
      duration.start must be(1338156000000L)
    }
  }

  "StatsDuration" must {

    "generate today" in {
      val today = StatsDuration.today(TimeZone.getTimeZone("Europe/Amsterdam"))
      val cal = Calendar.getInstance(TimeZone.getDefault)
      cal.clear()
      cal.setTimeInMillis(System.currentTimeMillis())
      val day = cal.get(Calendar.DAY_OF_MONTH)
      today.key.drop(6).toInt must be(day)
    }

    "generate yesterday" in {
      val yesterday = StatsDuration.yesterday(TimeZone.getTimeZone("Europe/Amsterdam"))
      val cal = Calendar.getInstance(TimeZone.getDefault)
      cal.clear()
      cal.setTimeInMillis(System.currentTimeMillis())
      val today = cal.get(Calendar.DAY_OF_MONTH)
      if (today != 1) //same month
        yesterday.key.drop(6).toInt must be(today - 1)
    }
  }
}

