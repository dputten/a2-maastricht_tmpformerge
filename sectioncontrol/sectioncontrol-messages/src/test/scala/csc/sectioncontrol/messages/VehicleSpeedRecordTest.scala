/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.messages

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import java.util.Date

class VehicleSpeedRecordTest extends WordSpec with MustMatchers {
  "VehicleSpeedRecord" must {
    val lane1 = Lane("1001", "lane1", "gantry1", "A2", 3.5f, 7.1f)
    val lane2 = Lane("1002", "lane2", "gantry2", "A2", 3.52f, 7.12f)
    val entry = VehicleMetadata(lane1, "id123", 123L, Some(new Date(123L).toString), Some(ValueWithConfidence("AA00BB")),
      None, Some(ValueWithConfidence_Float(3.5f, 60)), None, None, Some(ValueWithConfidence("NL")), Seq(), "", "", "SN1234")
    val exit = VehicleMetadata(lane2, "id999", 999L, Some(new Date(999L).toString), Some(ValueWithConfidence("AA00BB")),
      None, Some(ValueWithConfidence_Float(3.6f, 80)), None, None, Some(ValueWithConfidence("NL")), Seq(), "", "", "SN1234")
    val record = VehicleSpeedRecord("1001", entry, Some(exit), 2002, 50, "")

    "construct normally with good parameters" in {
      record.length.get must be(ValueWithConfidence_Float(3.6f, 80))
    }
    "construct normally with 0 speed" in {
      val zero = VehicleSpeedRecord("1001", entry, Some(exit), 2002, 0, "")
      zero.speed must be(0)
    }
    "fail requirement test if any argument is null" in {
      evaluating {
        record.copy(id = null)
      } must produce[IllegalArgumentException]
      evaluating {
        record.copy(entry = null)
      } must produce[IllegalArgumentException]
      evaluating {
        record.copy(measurementSHA = null)
      } must produce[IllegalArgumentException]
      evaluating {
        record.copy(speed = -1)
      } must produce[IllegalArgumentException]
    }

    "take no length when nothig is provided" in {
      val entry = VehicleMetadata(lane1, "id123", 123L, Some(new Date(123L).toString), Some(ValueWithConfidence("AA00BB")),
        None, None, None, None, Some(ValueWithConfidence("NL")), Seq(), "", "", "SN1234")
      val exit = VehicleMetadata(lane2, "id999", 999L, Some(new Date(999L).toString), Some(ValueWithConfidence("AA00BB")),
        None, None, None, None, Some(ValueWithConfidence("NL")), Seq(), "", "", "SN1234")
      val record = VehicleSpeedRecord("1001", entry, Some(exit), 2002, 50, "")
      record.length must be(None)
    }
    "take second length with max confidence" in {
      val entry = VehicleMetadata(lane1, "id123", 123L, Some(new Date(123L).toString), Some(ValueWithConfidence("AA00BB")),
        None, Some(ValueWithConfidence_Float(3.5f, 60)), None, None, Some(ValueWithConfidence("NL")), Seq(), "", "", "SN1234")
      val exit = VehicleMetadata(lane2, "id999", 999L, Some(new Date(999L).toString), Some(ValueWithConfidence("AA00BB")),
        None, Some(ValueWithConfidence_Float(3.6f, 90)), None, None, Some(ValueWithConfidence("NL")), Seq(), "", "", "SN1234")
      val record = VehicleSpeedRecord("1001", entry, Some(exit), 2002, 50, "")
      record.length.get must be(ValueWithConfidence_Float(3.6f, 90))
    }
    "take first length with max confidence" in {
      val entry = VehicleMetadata(lane1, "id123", 123L, Some(new Date(123L).toString), Some(ValueWithConfidence("AA00BB")),
        None, Some(ValueWithConfidence_Float(3.5f, 91)), None, None, Some(ValueWithConfidence("NL")), Seq(), "", "", "SN1234")
      val exit = VehicleMetadata(lane2, "id999", 999L, Some(new Date(999L).toString), Some(ValueWithConfidence("AA00BB")),
        None, Some(ValueWithConfidence_Float(3.6f, 90)), None, None, Some(ValueWithConfidence("NL")), Seq(), "", "", "SN1234")
      val record = VehicleSpeedRecord("1001", entry, Some(exit), 2002, 50, "")
      record.length.get must be(ValueWithConfidence_Float(3.5f, 91))
    }

    "take country with max confidence (first)" in {
      val entry = VehicleMetadata(lane1, "id123", 123L, Some(new Date(123L).toString), Some(ValueWithConfidence("AA00BB")),
        None, Some(ValueWithConfidence_Float(3.5f, 91)), None, None, Some(ValueWithConfidence("NL", 90)), Seq(), "", "", "SN1234")
      val exit = VehicleMetadata(lane2, "id999", 999L, Some(new Date(999L).toString), Some(ValueWithConfidence("AA00BB")),
        None, Some(ValueWithConfidence_Float(3.6f, 90)), None, None, Some(ValueWithConfidence("BE", 80)), Seq(), "", "", "SN1234")
      val record = VehicleSpeedRecord("1001", entry, Some(exit), 2002, 50, "")
      record.country.get.value must be("NL")
    }
    "take country with max confidence (second)" in {
      val entry = VehicleMetadata(lane1, "id123", 123L, Some(new Date(123L).toString), Some(ValueWithConfidence("AA00BB")),
        None, Some(ValueWithConfidence_Float(3.5f, 91)), None, None, Some(ValueWithConfidence("NL", 20)), Seq(), "", "", "SN1234")
      val exit = VehicleMetadata(lane2, "id999", 999L, Some(new Date(999L).toString), Some(ValueWithConfidence("AA00BB")),
        None, Some(ValueWithConfidence_Float(3.6f, 90)), None, None, Some(ValueWithConfidence("BE", 80)), Seq(), "", "", "SN1234")
      val record = VehicleSpeedRecord("1001", entry, Some(exit), 2002, 50, "")
      record.country.get.value must be("BE")
    }
    "take country with not None confidence (second)" in {
      val entry = VehicleMetadata(lane1, "id123", 123L, Some(new Date(123L).toString), Some(ValueWithConfidence("AA00BB")),
        None, Some(ValueWithConfidence_Float(3.5f, 91)), None, None, None, Seq(), "", "", "SN1234")
      val exit = VehicleMetadata(lane2, "id999", 999L, Some(new Date(999L).toString), Some(ValueWithConfidence("AA00BB")),
        None, Some(ValueWithConfidence_Float(3.6f, 90)), None, None, Some(ValueWithConfidence("BE", 80)), Seq(), "", "", "SN1234")
      val record = VehicleSpeedRecord("1001", entry, Some(exit), 2002, 50, "")
      record.country.get.value must be("BE")
    }
    "take country with not None confidence (first)" in {
      val entry = VehicleMetadata(lane1, "id123", 123L, Some(new Date(123L).toString), Some(ValueWithConfidence("AA00BB")),
        None, Some(ValueWithConfidence_Float(3.5f, 91)), None, None, Some(ValueWithConfidence("NL", 20)), Seq(), "", "", "SN1234")
      val exit = VehicleMetadata(lane2, "id999", 999L, Some(new Date(999L).toString), Some(ValueWithConfidence("AA00BB")),
        None, Some(ValueWithConfidence_Float(3.6f, 90)), None, None, None, Seq(), "", "", "SN1234")
      val record = VehicleSpeedRecord("1001", entry, Some(exit), 2002, 50, "")
      record.country.get.value must be("NL")
    }
  }
}

