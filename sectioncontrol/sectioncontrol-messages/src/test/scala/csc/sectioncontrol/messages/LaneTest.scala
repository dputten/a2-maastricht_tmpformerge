/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.messages

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

class LaneTest extends WordSpec with MustMatchers {
  "Lane" must {
    val lane = Lane("1000", "lane1", "gantry2", "A2", 3.5f, 7.1f)

    "construct normally with good parameters" in {
      lane.laneId must be("1000")
      lane.name must be("lane1")
      lane.system must be("A2")
      lane.sensorGPS_longitude must be(3.5f)
      lane.sensorGPS_latitude must be(7.1f)
    }
    "fail requirement test if any argument is null" in {
      evaluating {
        lane.copy(name = null)
      } must produce[IllegalArgumentException]
      evaluating {
        lane.copy(gantry = null)
      } must produce[IllegalArgumentException]
      evaluating {
        lane.copy(system = null)
      } must produce[IllegalArgumentException]
    }
    "fail if empty" in {
      evaluating {
        lane.copy(name = "      ")
      } must produce[IllegalArgumentException]
      evaluating {
        lane.copy(gantry = "")
      } must produce[IllegalArgumentException]
      evaluating {
        lane.copy(system = "\t")
      } must produce[IllegalArgumentException]
    }
  }
}

