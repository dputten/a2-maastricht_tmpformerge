package csc.sectioncontrol.messages

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.storage.DailyReportConfigSpeedEntry

/**
 * Copyright (C) 2016 CSC. <http://www.csc.com>
 *
 * Created on 6/28/16.
 */

class DailyReportConfigSpeedEntryTest extends WordSpec with MustMatchers {

  import DailyReportConfigSpeedEntry._

  "DailyReportConfigSpeedEntry companion object" must {
    "not alter a correct list of speed entries" in {
      val input = List(d080, d100, d130, dTotal)
      val output = augmentWithSpeeds(input, Set(80, 100, 130))
      output must be(input)
    }

    "not alter a configuration containing only one speedEntry" in {
      val input = List(d100)
      val output = augmentWithSpeeds(input, Set(100))
      output must be(input)
    }

    "add entries for each speed not configured" in {
      val input = List(d100)
      val expected = List(m080, d100, m130, mTotal)
      val output = augmentWithSpeeds(input, Set(80, 100, 130))
      output must be(expected)
    }

    "add a missing entry for total" in {
      val input = List(d080, d100, d130)
      val expected = input :+ mTotal
      val output = augmentWithSpeeds(input, Set(80, 100, 130))
      output must be(expected)
    }

    "remove total if there's only one entry defining a speed" in {
      val input = List(d080, dTotal)
      val expected = List(d080)
      val result = augmentWithSpeeds(input, Set(80))
      result must be(expected)
    }
  }

  val d080 = DailyReportConfigSpeedEntry("80", "id80", "ht80")
  val d100 = DailyReportConfigSpeedEntry("100", "id100", "ht100")
  val d130 = DailyReportConfigSpeedEntry("130", "id1300", "ht1300")
  val dTotal = DailyReportConfigSpeedEntry("total", "idTotal", "htTotal")

  val missing = DailyReportConfigSpeedEntry("", notConfigured, notConfigured)
  val m080 = missing.copy(speedLimit = "80", ht = "80 kmh missing")
  val m100 = missing.copy(speedLimit = "100", ht = "100 kmh missing")
  val m130 = missing.copy(speedLimit = "130", ht = "130 kmh missing")
  val mTotal = missing.copy(speedLimit = "total", ht = "total missing")
}
