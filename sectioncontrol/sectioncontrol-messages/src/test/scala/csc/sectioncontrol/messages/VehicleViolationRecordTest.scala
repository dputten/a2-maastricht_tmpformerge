package csc.sectioncontrol.messages

import java.util.Date

import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.storage.{ ConfigType, ServiceType, ZkCaseFileType, ZkIndicationType, ZkWheelbaseType }
import net.liftweb.json.{ DefaultFormats, Serialization }
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

/**
 * Created by rbakker on 19-4-17.
 */
class VehicleViolationRecordTest extends WordSpec with MustMatchers {
  implicit val formats = DefaultFormats + new EnumerationSerializer(ProcessingIndicator, VehicleCode, VehicleCategory, ZkWheelbaseType, ZkIndicationType, ServiceType, ZkCaseFileType, EsaProviderType, VehicleImageType, ConfigType, ConfigType, SpeedIndicatorType)

  val messageV600x = """{"id":"A2-1492125108950-822-3","classifiedRecord":{"id":"A2-1492125108950-822-3","speedRecord":{"id":"A2-1492125108950-E2-822-PE2-R1","entry":{"lane":{"la""" +
    """neId":"A2-E2-E2R1","name":"PE2-R1","gantry":"E2","system":"A2","sensorGPS_longitude":52.058163,"sensorGPS_latitude":5.113391,"""" +
    """bpsLaneId":"PW002 254,8 HR R 1 R"},"eventId":"A2-E2-E2R1-605840188092892","eventTimestamp":1492125037722,"eventTimestampStr":"""" +
    """13-04-2017 23:10:37.722 UTC","license":{"value":"76NLT9","confidence":80},"rawLicense":"76_NLT_9","length":{"value":5.19999980""" +
    """9265137,"confidence":95},"category":{"value":"Car","confidence":60},"speed":{"value":140.0,"confidence":95},"country":{"value"""" +
    """:"NL","confidence":80},"images":[{"timestamp":1492125037722,"offset":0,"uri":"/data/images/jpg/A2-E2-E2R1-605840188092892_with""" +
    """Metadata.jpg","imageType":"VehicleImageType$Overview","checksum":"74c67343fa042927fe94f1ad1cc13580"}],"NMICertificate":"TP8881""" +
    """","applChecksum":"79b3c3e0527553ed88996400bad27cba","serialNr":"18","recognizedBy":"ARH","version":1,"licensePlateData":{"lice""" +
    """nse":{"value":"76NLT9","confidence":80},"sourceImageType":"VehicleImageType$Overview","rawLicense":"76_NLT_9","recognizedBy":"""" +
    """ARH","country":{"value":"NL","confidence":80}}},"exit":{"lane":{"laneId":"A2-X5-X5R1","name":"PX5-R1","gantry":"X5","system":"""" +
    """A2","sensorGPS_longitude":52.058163,"sensorGPS_latitude":5.113391,"bpsLaneId":"PW002 257,5 HR R 1 R"},"eventId":"A2-X5-X5R1-60""" +
    """5911429162616","eventTimestamp":1492125108950,"eventTimestampStr":"13-04-2017 23:11:48.950 UTC","license":{"value":"76NLT9","c""" +
    """onfidence":80},"rawLicense":"76_NLT_9","length":{"value":5.199999809265137,"confidence":95},"category":{"value":"Car","confide""" +
    """nce":60},"speed":{"value":140.0,"confidence":95},"country":{"value":"NL","confidence":80},"images":[{"timestamp":1492125108950""" +
    ""","offset":0,"uri":"/data/images/jpg/A2-X5-X5R1-605911429162616_withMetadata.jpg","imageType":"VehicleImageType$Overview","chec""" +
    """ksum":"bd9601c1d822b2740cb5d3f07eb660d2"}],"NMICertificate":"TP8881","applChecksum":"79b3c3e0527553ed88996400bad27cba","serial""" +
    """Nr":"18","recognizedBy":"ARH","version":1,"licensePlateData":{"license":{"value":"76NLT9","confidence":80},"sourceImageType":"""" +
    """VehicleImageType$Overview","rawLicense":"76_NLT_9","recognizedBy":"ARH","country":{"value":"NL","confidence":80}}},"corridorId""" +
    """":822,"speed":138,"measurementSHA":"2267f8180e0459be53ea573bee1eb422"},"code":"VehicleCode$PA","indicator":"ProcessingIndicato""" +
    """r$Automatic","mil":false,"invalidRdwData":false,"manualAccordingToSpec":false,"reason":{"mask":0},"dambord":"P"},"enforcedSpee""" +
    """d":80}"""

  "VehicleViolationRecord" must {
    "decode old messages" in {
      val result = Serialization.read[VehicleViolationRecord](messageV600x)
      //just check some fields
      //New field introduced with version 6.1.x.X
      result.recognizeResults must be(List())
      result.classifiedRecord.speedRecord.entry.licensePlateData.get must be(LicensePlateData(license = Some(ValueWithConfidence("76NLT9", 80)),
        licensePosition = None,
        sourceImageType = Some(VehicleImageType.Overview),
        rawLicense = Some("76_NLT_9"),
        licenseType = None,
        recognizedBy = Some("ARH"),
        country = Some(ValueWithConfidence("NL", 80))))
    }
    "add a recognized result" in {
      val vio = makeVVR()
      val license = "ABCD12"
      val conf = 60
      val rawLic = "AB_CD_12"
      val lic = Some(ValueWithConfidence[String](license, conf))
      val country = Some(ValueWithConfidence[String]("NL", 50))
      val data = LicensePlateData(license = lic,
        rawLicense = Some(rawLic),
        recognizedBy = Some(VehicleMetadata.Recognizer.INTRADA),
        country = country)
      val result = vio.addRecognizeResults(VehicleMetadata.Recognizer.INTRADA, true, data)

      result.copy(recognizeResults = List()) must be(vio)
      result.recognizeResults.size must be(1)
      result.recognizeResults.head.recognizeId must be(VehicleMetadata.Recognizer.INTRADA)
      result.recognizeResults.head.isCurrent must be(true)
      result.recognizeResults.head.recognize must be(data)
    }
    "add a recognized result and reset current flag" in {
      val vio = makeVVR()
      val license = "ABCD12"
      val conf = 60
      val rawLic = "AB_CD_12"
      val lic = Some(ValueWithConfidence[String](license, conf))
      val country = Some(ValueWithConfidence[String]("NL", 50))
      val data = LicensePlateData(license = lic,
        rawLicense = Some(rawLic),
        recognizedBy = Some(VehicleMetadata.Recognizer.INTRADA),
        country = country)
      val list = List(RecognizeRecord("1", true, data),
        RecognizeRecord("2", false, data),
        RecognizeRecord("3", false, data),
        RecognizeRecord("4", true, data))
      val filledVio = vio.copy(recognizeResults = list)
      val result = filledVio.addRecognizeResults(VehicleMetadata.Recognizer.INTRADA, true, data)

      result.copy(recognizeResults = List()) must be(vio)
      result.recognizeResults.size must be(5)
      val recogResult = result.recognizeResults.filter(_.isCurrent)
      recogResult.size must be(1)
      recogResult.head.recognizeId must be(VehicleMetadata.Recognizer.INTRADA)
      recogResult.head.isCurrent must be(true)
      recogResult.head.recognize must be(data)
    }

  }

  def makeVVR(id: String = "id1",
              entryTime: Long = System.currentTimeMillis(),
              exitTime: Long = System.currentTimeMillis() + 300000L,
              countryCode: Option[String] = Some("NL"),
              measuredSpeed: Int = 113,
              vehicleClass: Option[VehicleCode.Value] = Some(VehicleCode.PA),
              licensePlate: Option[String] = Some("AB12CD"),
              processingIndicator: ProcessingIndicator.Value = ProcessingIndicator.Automatic,
              enforcedSpeed: Int = 100,
              reason: IndicatorReason = IndicatorReason()) =
    VehicleViolationRecord(id,
      new VehicleClassifiedRecord(
        VehicleSpeedRecord(id,
          VehicleMetadata(Lane("1",
            "lane1",
            "gantry1",
            "eg33",
            0.0f,
            0.0f),
            id,
            entryTime,
            Some(new Date(entryTime).toString),
            licensePlate.map(lp ⇒ ValueWithConfidence(lp, 42)),
            country = countryCode.map(cc ⇒ ValueWithConfidence(cc, 42)),
            images = Seq(),
            NMICertificate = "",
            applChecksum = "",
            serialNr = "SN1234"),
          Some(VehicleMetadata(Lane("1",
            "lane1",
            "gantry2",
            "eg33",
            0.0f,
            0.0f),
            id,
            exitTime,
            Some(new Date(exitTime).toString),
            licensePlate.map(lp ⇒ ValueWithConfidence(lp, 42)),
            country = countryCode.map(cc ⇒ ValueWithConfidence(cc, 42)),
            images = Seq(),
            NMICertificate = "",
            applChecksum = "",
            serialNr = "SN1234")),
          2002, measuredSpeed, "SHA-1-foo"),
        vehicleClass,
        processingIndicator,
        false,
        false,
        None,
        false,
        reason),
      enforcedSpeed,
      None,
      recognizeResults = List())

}

