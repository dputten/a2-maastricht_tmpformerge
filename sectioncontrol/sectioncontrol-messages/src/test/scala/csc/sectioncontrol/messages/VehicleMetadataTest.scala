/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.messages

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import net.liftweb.json.{ DefaultFormats, Serialization }
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.storage.{ ConfigType, ZkIndicationType, ZkWheelbaseType }

class VehicleMetadataTest extends WordSpec with MustMatchers {
  implicit val formats = DefaultFormats + new EnumerationSerializer(ZkWheelbaseType, VehicleCode, ZkIndicationType, VehicleImageType,
    ConfigType)

  "VehicleMetadataTest" must {
    "encode and decode when full filled" in {

      val vehicleImage = new VehicleImage(timestamp = System.currentTimeMillis(),
        offset = 0, uri = "/dir1", imageType = VehicleImageType.Overview,
        checksum = "dasdasd", timeYellow = Some(100L), timeRed = Some(200L))
      val meta = new VehicleMetadata(
        lane = new Lane(laneId = "A2-gantry-lane1",
          name = "lane1",
          gantry = "gantry",
          system = "A2",
          sensorGPS_longitude = 52D,
          sensorGPS_latitude = 5D),
        eventId = "xx",
        eventTimestamp = System.currentTimeMillis(),
        eventTimestampStr = Some("testTime"), //Only used for testing
        license = Some(new ValueWithConfidence("BB12XF", 80)),
        rawLicense = Some("BB12XF"),
        length = Some(new ValueWithConfidence_Float(4.5F, 70)),
        category = Some(new ValueWithConfidence(VehicleCategory.Car.toString, 60)),
        speed = Some(new ValueWithConfidence_Float(80.5F, 70)),
        country = Some(new ValueWithConfidence("NL", 80)),
        images = Seq(vehicleImage),
        NMICertificate = "PL20",
        applChecksum = "ACBDE1234567890",
        serialNr = "SN1234")

      val json = Serialization.write(meta)
      val result = Serialization.read[VehicleMetadata](json)
      result must be(meta)
      result.vehicleCategory must be(Some(new ValueWithConfidence(VehicleCategory.Car, 60)))
    }
    "encode and decode when minimal filled" in {
      val vehicleImage = new VehicleImage(timestamp = System.currentTimeMillis(),
        offset = 0, uri = "/dir1", imageType = VehicleImageType.Overview,
        checksum = "dasdasd", timeYellow = Some(100L), timeRed = Some(200L))

      val meta = new VehicleMetadata(
        lane = new Lane(laneId = "A2-gantry-lane1",
          name = "lane1",
          gantry = "gantry",
          system = "A2",
          sensorGPS_longitude = 52D,
          sensorGPS_latitude = 5D),
        eventId = "xx",
        eventTimestamp = System.currentTimeMillis(),
        eventTimestampStr = None, //Only used for testing
        license = None,
        rawLicense = None,
        length = None,
        category = None,
        speed = None,
        country = None,
        images = Seq(vehicleImage),
        NMICertificate = "PL20",
        applChecksum = "ACBDE1234567890",
        serialNr = "SN1234")

      val json = Serialization.write(meta)
      val result = Serialization.read[VehicleMetadata](json)
      result must be(meta)
    }
    "unsuported category" in {
      val vehicleImage = new VehicleImage(timestamp = System.currentTimeMillis(),
        offset = 0, uri = "/dir1", imageType = VehicleImageType.Overview,
        checksum = "dasdasd", timeYellow = Some(100L), timeRed = Some(200L))

      val meta = new VehicleMetadata(
        lane = new Lane(laneId = "A2-gantry-lane1",
          name = "lane1",
          gantry = "gantry",
          system = "A2",
          sensorGPS_longitude = 52D,
          sensorGPS_latitude = 5D),
        eventId = "xx",
        eventTimestamp = System.currentTimeMillis(),
        eventTimestampStr = None, //Only used for testing
        license = None,
        rawLicense = None,
        length = None,
        category = Some(new ValueWithConfidence("NoClass", 60)),
        speed = None,
        country = None,
        images = Seq(vehicleImage),
        NMICertificate = "PL20",
        applChecksum = "ACBDE1234567890",
        serialNr = "SN1234")

      meta.vehicleCategory must be(Some(new ValueWithConfidence(VehicleCategory.Unknown, 60)))
    }
  }

}