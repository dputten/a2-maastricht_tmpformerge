package csc.sectioncontrol.common

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import java.text.SimpleDateFormat
import java.util.{ Calendar, GregorianCalendar, Date }

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
class DayUtilityTest extends WordSpec with MustMatchers {
  val dateFormat = new SimpleDateFormat("ddMMyyyy HHmmss.SSS")
  "nl tooling" must {
    "add one day on normal days" in {
      val start = dateFormat.parse("10102012 010000.000").getTime
      val oneDay = DayUtility.addOneDay(start)
      dateFormat.format(new Date(oneDay)) must be("11102012 010000.000")
    }
    "add one day with starting day" in {
      val start = dateFormat.parse("10102012 000000.000").getTime
      val oneDay = DayUtility.addOneDay(start)
      dateFormat.format(new Date(oneDay)) must be("11102012 000000.000")
    }
    "add one day with saving daylight" in {
      val start = dateFormat.parse("28102012 020202.222").getTime
      val oneDay = DayUtility.addOneDay(start)
      dateFormat.format(new Date(oneDay)) must be("29102012 020202.222")
    }
    "add one day with saving daylight with start day" in {
      val start = dateFormat.parse("28102012 000000.000").getTime
      val oneDay = DayUtility.addOneDay(start)
      dateFormat.format(new Date(oneDay)) must be("29102012 000000.000")
    }
    "find correct start and end of day for normal days" in {
      val date = dateFormat.parse("10102012 010000.000").getTime
      val (startDay, endDay) = DayUtility.calculateDayPeriod(date)
      dateFormat.format(new Date(startDay)) must be("10102012 000000.000")
      dateFormat.format(new Date(endDay)) must be("11102012 000000.000")
    }
    "find correct start and end of day when using start day" in {
      val date = dateFormat.parse("10102012 000000.000").getTime
      val (startDay, endDay) = DayUtility.calculateDayPeriod(date)
      dateFormat.format(new Date(startDay)) must be("10102012 000000.000")
      dateFormat.format(new Date(endDay)) must be("11102012 000000.000")
    }
    "find correct start and end of day when with saving daylight" in {
      val date = dateFormat.parse("28102012 010000.000").getTime
      val (startDay, endDay) = DayUtility.calculateDayPeriod(date)
      dateFormat.format(new Date(startDay)) must be("28102012 000000.000")
      dateFormat.format(new Date(endDay)) must be("29102012 000000.000")
    }
    "find correct start and end of day when with saving daylight and start day" in {
      val date = dateFormat.parse("28102012 000000.000").getTime
      val (startDay, endDay) = DayUtility.calculateDayPeriod(date)
      dateFormat.format(new Date(startDay)) must be("28102012 000000.000")
      dateFormat.format(new Date(endDay)) must be("29102012 000000.000")
    }
    "find correct start and end of day when with saving daylight to summer" in {
      val date = dateFormat.parse("25032012 010000.000").getTime
      val (startDay, endDay) = DayUtility.calculateDayPeriod(date)
      dateFormat.format(new Date(startDay)) must be("25032012 000000.000")
      dateFormat.format(new Date(endDay)) must be("26032012 000000.000")
    }
    "find correct start and end of day when with saving daylight and start day to summer" in {
      val date = dateFormat.parse("25032012 000000.000").getTime
      val (startDay, endDay) = DayUtility.calculateDayPeriod(date)
      dateFormat.format(new Date(startDay)) must be("25032012 000000.000")
      dateFormat.format(new Date(endDay)) must be("26032012 000000.000")
    }
  }
  "calculateDayPeriod" must {
    "find start and end times for a normal day" in {
      DayUtility.calculateDayPeriod(new GregorianCalendar(2012, Calendar.FEBRUARY, 28, 0, 0).getTimeInMillis) must
        be((1330383600000L, 1330470000000L))
      DayUtility.calculateDayPeriod(new GregorianCalendar(2012, Calendar.FEBRUARY, 28, 9, 0).getTimeInMillis) must
        be((1330383600000L, 1330470000000L))
    }
    "find start and end times for a DST switch day" in {
      DayUtility.calculateDayPeriod(new GregorianCalendar(2012, Calendar.MARCH, 25, 0, 0).getTimeInMillis) must
        be((1332630000000L, 1332712800000L))
      DayUtility.calculateDayPeriod(new GregorianCalendar(2012, Calendar.MARCH, 25, 9, 0).getTimeInMillis) must
        be((1332630000000L, 1332712800000L))
    }
  }

}