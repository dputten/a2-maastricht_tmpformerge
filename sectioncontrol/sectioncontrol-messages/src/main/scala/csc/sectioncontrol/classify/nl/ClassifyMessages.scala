/*
* Copyright (C) 2012 CSC. <http://www.csc.com>
*/

package csc.sectioncontrol.classify.nl

import csc.sectioncontrol.messages.{ IndicatorReason, ProcessingIndicator, VehicleCode, StatsDuration }

case class ClassifyMessage(stats: StatsDuration, begin: Long, end: Long)

case class ClassifyResult(request: ClassifyMessage, count: Int)

/**
 * Holds the completion status of classification for a system
 *
 * @param lastUpdateTime The last time classification was started up or finished for this system
 * @param completeUntil The time until which classification has been completed successfully for this system
 */
case class ClassificationCompletionStatus(lastUpdateTime: Long, completeUntil: Long)

/**
 * Tracing data for a decision within the classification
 * @param step
 * @param result
 * @param details
 */
case class Decision(step: String, result: String, details: Seq[Decision])

/**
 * All the decisions and results of the classification
 * @param steps
 * @param license
 * @param laneId
 * @param code
 * @param indicator
 * @param reason
 * @param mil
 * @param manualAccordingToSpec
 * @param alternativeClassification
 */
case class DecisionResult(steps: Seq[Decision],
                          license: String,
                          laneId: String,
                          code: Option[VehicleCode.Value],
                          indicator: ProcessingIndicator.Value,
                          reason: IndicatorReason,
                          mil: Boolean,
                          manualAccordingToSpec: Boolean,
                          alternativeClassification: Option[VehicleCode.Value])

