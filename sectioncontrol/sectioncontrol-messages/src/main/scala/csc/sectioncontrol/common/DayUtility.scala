/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.common

import java.util.{ TimeZone, Calendar, GregorianCalendar }
import org.joda.time.{ DateTimeZone, DateTime }
import org.joda.time.tz.DateTimeZoneBuilder

/**
 *
 * @author Maarten Hazewinkel
 */
object DayUtility {
  val fileExportTimeZone = java.util.TimeZone.getTimeZone("Europe/Amsterdam")

  /**
   * Calculate the start- and end-points for the provided day. Considers DST issues by using the TimeZone
   * constant from the violations.Constants object.
   *
   * @param date A timestamp within the day of interest
   * @return A pair of longs indicating the start and end times of the day in unix epoch milliseconds
   */
  def calculateDayPeriod(date: Long): (Long, Long) = {
    val cal = new GregorianCalendar(fileExportTimeZone)
    cal.setTimeInMillis(date)
    cal.set(Calendar.HOUR_OF_DAY, 0)
    cal.set(Calendar.MINUTE, 0)
    cal.set(Calendar.SECOND, 0)
    cal.set(Calendar.MILLISECOND, 0)
    val start = cal.getTimeInMillis
    cal.set(Calendar.HOUR_OF_DAY, 24)
    (start, cal.getTimeInMillis)
  }

  /**
   * Add one day. And take into account the saving daylights jumps
   * @param date the start date
   * @return added one day.
   */
  def addOneDay(date: Long): Long = {
    val time = new DateTime(date)
    time.plusDays(1).getMillis
  }

  /**
   * substract one day. And take into account the saving daylights jumps
   * @param date the start date
   * @return added one day.
   */
  def substractOneDay(date: Long): Long = {
    val time = new DateTime(date)
    time.minusDays(1).getMillis
  }

  def getStartDay(date: Long, timeZone: TimeZone = fileExportTimeZone): Long = {
    val time = new DateTime(date)
    time.withZone(DateTimeZone.forTimeZone(timeZone)).withMillisOfDay(0).getMillis
  }
  def getEndDay(date: Long, timeZone: TimeZone = fileExportTimeZone): Long = {
    val time = new DateTime(date)
    time.withZone(DateTimeZone.forTimeZone(timeZone)).withMillisOfDay(0).plusDays(1).getMillis
  }

}
