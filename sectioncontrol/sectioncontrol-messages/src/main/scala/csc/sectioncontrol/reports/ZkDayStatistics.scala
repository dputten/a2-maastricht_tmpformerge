/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.reports

/**
 * Represents statistics for a day
 * @param id of DayStatistics
 * @param day for specific day
 * @param createDate when report was created
 * @param data compressed binary data that contains xml
 * @param fileName name of file
 */
case class ZkDayStatistics(id: String, day: Long, createDate: Long, data: List[Int], fileName: String)

object ZkDayStatistics {
  def apply(id: String, day: Long, createDate: Long, data: Array[Byte], fileName: String): ZkDayStatistics = {
    val list = data.map(b ⇒ b.toInt).toList
    new ZkDayStatistics(id, day, createDate, list, fileName)
  }
}