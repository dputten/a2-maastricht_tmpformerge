package csc.sectioncontrol.jobs

/**
 * Holds the completion status of classification for a system
 *
 * @param lastUpdateTime The last time classification was started up or finished for this system
 * @param completeUntil The time until which classification has been completed successfully for this system
 */
case class ProcessedRegistrationCompletionStatus(lastUpdateTime: Long, completeUntil: Long)

case class ValidationFilterCompletionStatus(lastUpdateTime: Long, completeUntil: Long)