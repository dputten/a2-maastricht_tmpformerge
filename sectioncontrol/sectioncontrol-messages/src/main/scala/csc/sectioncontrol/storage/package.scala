package csc.sectioncontrol

/**
 * Copyright (C) 2016 CSC. <http://www.csc.com>
 *
 * Created on 6/30/16.
 */

package object storage {
  def eitherStringOrInt(str: String): Either[String, Int] = {
    try {
      val speed = str.toInt
      Right(speed)
    } catch {
      case e: Exception ⇒ Left(str)
    }
  }

  implicit def stringWrapper(value: String) = new StringExtensions(value)

  class StringExtensions(val value: String) {
    def toOptionInt = {
      try {
        Some(value.toInt)
      } catch {
        case e: Exception ⇒ None
      }
    }
  }

}
