package csc.sectioncontrol.storage.Decorate

import java.util.Date

import csc.sectioncontrol.messages.VehicleImageType
import csc.sectioncontrol.messages.VehicleImageType._
import java.text.SimpleDateFormat

case class ColorRGBConfig(R: Int, G: Int, B: Int)
/**
 * Decoration configuration for an image type
 * //@param imageType  Configuration is valid vor this imageType
 * @param decorationType Type of decoration
 * @param imageCorrections
 */
case class DecorationConfig(decorationType: DecorationType.Value,
                            imageCorrections: Option[Seq[String]] = None)

/**
 *
 * @param blackStart
 * @param blackMargin
 * @param whiteStart
 * @param whiteMargin
 */
case class AutoLevelConfig(blackStart: Int, blackMargin: Double, whiteStart: Int, whiteMargin: Double)

/**
 *
 * @param brightness
 * @param contrast
 * @param gamma
 */
case class ImageColorConfig(brightness: Option[Float] = None,
                            contrast: Option[Float] = None,
                            gamma: Option[Double] = None,
                            red: Option[Int] = None,
                            green: Option[Int] = None,
                            blue: Option[Int] = None,
                            autoLevel: Option[AutoLevelConfig] = None)

/**
 *
 * @param from
 * @param to
 * @param colorConfig
 */
case class ImageCorrectionConfig(imageCorrectionId: String, from: String, to: String, colorConfig: ImageColorConfig) {
  import ImageCorrectionConfig._
  require(from.matches(timePattern), "from must be in format HH:mm:ss")
  require(to.matches(timePattern), "to must be in format HH:mm:ss")

  def fromDate = asDate(from)
  def toDate = asDate(to)
}

object ImageCorrectionConfig {
  val timePattern = "[0-2]\\d:[0-5]\\d:[0-5]\\d"
  val timeFormat = new SimpleDateFormat("HH:mm:ss")

  def asDate(timeString: String): Date = {
    timeFormat.parse(timeString)
  }
}

/**
 * The configuration of the decoration step
 * @param decorationsConfigs List of all decoration configurations
 * @param rowHeightPx Row height in pixels
 * @param borderWidthPx The width of the border in pixels
 * @param descentPx Used to position the text (in y direction) within a row. The number of pixels (the baseline) is moved upwards.
 * @param extraLineSpacingPx Extra space between lines in pixels.
 * @param licenseQuality Jpeg quality of license in decoration
 * @param licenseMaxHeightPx Maximum height of license image in decorated image
 * @param licenseMaxWidthPx Maximum width of license image in decorated image
 * @param replaceLicenseWithOriginal Should license in the image be replaced by the version from original image
 * @param histogramMeanUpperLimit If histogram mean < upper limit then replace license
 * @param showLicenseFrame Show a frame around the license in the image
 * @param licenseFrameThickness The thickness of license frame
 * @param licenseFrameColorRGB Color of the license frame
 * @param backgroundColorRGB Background color
 * @param showLogo Show CSC logo in decoration
 * @param fontName Name of the font to be used
 * @param fontColorRGB Color of the font
 * @param imageCorrections TODO
 */
case class DecorateConfig(
  decorationsConfigs: Set[DecorationConfig] = Set(),
  rowHeightPx: Int = 15,
  borderWidthPx: Int = 2,
  descentPx: Int = 2,
  extraLineSpacingPx: Int = 2,
  licenseQuality: Int = 80,
  licenseMaxWidthPx: Int = 200,
  licenseMaxHeightPx: Int = 100,
  replaceLicenseWithOriginal: Boolean = false,
  histogramMeanUpperLimit: Int = 0,
  showLicenseFrame: Boolean = true,
  licenseFrameThickness: Int = 4,
  licenseFrameColorRGB: ColorRGBConfig = ColorRGBConfig.red,
  licenseImageCorrections: Seq[String] = List(),
  backgroundColorRGB: ColorRGBConfig = ColorRGBConfig.black,
  showLogo: Boolean = true,
  fontName: Option[String] = None,
  fontColorRGB: ColorRGBConfig = ColorRGBConfig.white,
  imageCorrections: Seq[ImageCorrectionConfig] = List())

/**
 * Represents the different types of decorations.
 */
object DecorationType extends Enumeration {
  type DecorationType = Value
  val Overview_1, Speed_1, Speed_2, RedLight_1, RedLight_2 = Value

  def getTargetDecorationType(imageType: VehicleImageType): DecorationType = imageType match {
    case VehicleImageType.Overview            ⇒ DecorationType.Overview_1
    case VehicleImageType.OverviewSpeed       ⇒ DecorationType.Speed_1
    case VehicleImageType.MeasureMethod2Speed ⇒ DecorationType.Speed_2
    case VehicleImageType.RedLight            ⇒ DecorationType.RedLight_1
    case VehicleImageType.OverviewRedLight    ⇒ DecorationType.RedLight_2
    case tp @ _                               ⇒ throw new IllegalArgumentException("Not expected " + tp)
  }

}

object ColorRGBConfig {
  val red = ColorRGBConfig(225, 30, 30)
  val white = ColorRGBConfig(255, 255, 255)
  val black = ColorRGBConfig(0, 0, 0)
}
