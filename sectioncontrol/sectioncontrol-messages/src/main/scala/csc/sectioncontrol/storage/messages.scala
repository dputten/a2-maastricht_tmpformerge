/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.storage

import csc.sectioncontrol.messages.{ SpeedIndicatorType, EsaProviderType, VehicleCode }
import java.util.{ Date, Calendar }
import csc.config.Path
import csc.sectioncontrol.storage.Decorate.DecorateConfig

/*
* Is used as an storage object that contains everything of a Corridor
* @param id of corridor
* @param name of corridor
* @param serviceType service type of corridor
* @param pardonTimeEsa
* @param pardonTimeTechnical
* @param pshtm
* @param info
* @param startSectionId
* @param endSectionId
* @param allSectionIds
*/
case class ZkCorridor(id: String,
                      name: String,
                      roadType: Int,
                      serviceType: ServiceType.Value,
                      caseFileType: ZkCaseFileType.Value = ZkCaseFileType.HHMVS40,
                      isInsideUrbanArea: Boolean,
                      dynamaxMqId: String,
                      approvedSpeeds: Seq[Int],
                      pshtm: ZkPSHTMIdentification,
                      info: ZkCorridorInfo,
                      startSectionId: String,
                      endSectionId: String,
                      allSectionIds: Seq[String],
                      direction: ZkDirection,
                      radarCode: Int,
                      roadCode: Int,
                      dutyType: String,
                      deploymentCode: String,
                      codeText: String)

case class ZkDirection(directionFrom: String,
                       directionTo: String)

/*
* Is used as an storage object that contains everything of a Corridor
* @param corridorId of corridor
* @param locationCode
* @param reportId
* @param reportHtId
* @param locationLine1
* @param locationLine2
*/
case class ZkCorridorInfo(corridorId: Int,
                          locationCode: String,
                          reportId: String,
                          reportHtId: String,
                          reportPerformance: Boolean,
                          locationLine1: String,
                          locationLine2: Option[String] = None)

/*
* Is used as an storage object that represents what parts of a PSHTM number must be used
* @param useYear
* @param useMonth
* @param useDay
* @param useReportingOfficerId
* @param useNumberOfTheDay
* @param useLocationCode
* @param useTypeViolation
* @param useFixedCharacter
* @param fixedCharacter
*/
case class ZkPSHTMIdentification(useYear: Boolean,
                                 useMonth: Boolean,
                                 useDay: Boolean,
                                 useReportingOfficerId: Boolean,
                                 useNumberOfTheDay: Boolean,
                                 useLocationCode: Boolean,
                                 useHHMCode: Boolean,
                                 useTypeViolation: Boolean,
                                 useFixedCharacter: Boolean,
                                 fixedCharacter: String,
                                 elementOrderString: Option[String] = None)

/**
 * ZkMsiIdentification uniquely define a Msi in a MTM file
 * @param os_comm_id
 * @param msi_rijstr_pos
 */
case class ZkMsiIdentification(os_comm_id: Int, msi_rijstr_pos: String, omschrijving: String = "") {
  override def equals(other: Any): Boolean = {
    other match {
      case that: ZkMsiIdentification ⇒
        (that canEqual this) &&
          that.os_comm_id == os_comm_id &&
          that.msi_rijstr_pos == msi_rijstr_pos
      case _ ⇒ false
    }
  }

  def canEqual(other: Any): Boolean =
    other.isInstanceOf[ZkMsiIdentification]

  override def hashCode: Int =
    41 * (this.os_comm_id.hashCode() + 41) + this.msi_rijstr_pos.hashCode
}

/*
* Role where an user belongs to
* @param id
* @param systemId
* @param name
* @param length
* @param startGantryId
* @param endGantryId
* @param matrixBoards sequence of matrix board ids
*/
case class ZkSection(id: String,
                     systemId: String,
                     name: String,
                     length: Double,
                     startGantryId: String,
                     endGantryId: String,
                     matrixBoards: Seq[String],
                     msiBlackList: Seq[ZkMsiIdentification])

/*
* Represents a Gantry
* @param id
* @param systemId
* @param name
* @param hectometer
* @param longitude
* @param latitude
*/
case class ZkGantry(id: String,
                    systemId: String,
                    name: String,
                    hectometer: String,
                    longitude: Double,
                    latitude: Double)

/**
 *
 * @param wheelBase
 * @param vehicleMaxSpeed
 * @param marginSpeedLimits
 * @param duplicateTriggerDelta
 * @param inaccuracyMarginForLength
 * @param minimumLengthTrailer
 * @param countryCodeConfidence
 * @param maxLengthMoped
 * @param followDistance The follow distance of vehicleStats in ms. It is an option to support backward compatible
 */
case class ZkPreference(wheelBase: List[ZkWheelBase],
                        vehicleMaxSpeed: List[ZkVehicleMaxSpeed],
                        marginSpeedLimits: List[ZkSpeedLimitMargin],
                        duplicateTriggerDelta: Int = 0,
                        inaccuracyMarginForLength: Int = 0,
                        minimumLengthTrailer: Int = 0,
                        motorVehicleLengthThreshold: Int = 300,
                        shortVehicleLengthThreshold: Int = 500,
                        longVehicleLengthThreshold: Int = 600,
                        countryCodeConfidence: Int = 50,
                        licenseConfidence: Int = 50,
                        maxLengthMoped: Float = 2.0F,
                        followDistance: Option[Long] = None,
                        testLicenseConfidence: Boolean = false,
                        reliableLicenseLimit: Int = 60,
                        guessVehicleCode: Boolean = true)

object ZkPreference {
  val default = ZkPreference(List(ZkWheelBase(ZkWheelbaseType.LBB2M, 2.5),
    ZkWheelBase(ZkWheelbaseType.LBB2M3M, 4.0),
    ZkWheelBase(ZkWheelbaseType.LBB3M4M, 5.0),
    ZkWheelBase(ZkWheelbaseType.LBB4M5M, 6.0),
    ZkWheelBase(ZkWheelbaseType.LBB5M, 7.5),
    ZkWheelBase(ZkWheelbaseType.LBB5M, 7.5),
    ZkWheelBase(ZkWheelbaseType.P2M, 2.0),
    ZkWheelBase(ZkWheelbaseType.P2M3M, 3.5),
    ZkWheelBase(ZkWheelbaseType.P3M4M, 4.5),
    ZkWheelBase(ZkWheelbaseType.P4M5M, 5.5),
    ZkWheelBase(ZkWheelbaseType.P5M, 7.0)),
    List(ZkVehicleMaxSpeed(VehicleCode.AB, 80),
      ZkVehicleMaxSpeed(VehicleCode.AO, 80),
      ZkVehicleMaxSpeed(VehicleCode.AS, 90),
      ZkVehicleMaxSpeed(VehicleCode.AT, 100),
      ZkVehicleMaxSpeed(VehicleCode.CA, 120),
      ZkVehicleMaxSpeed(VehicleCode.CB, 80),
      ZkVehicleMaxSpeed(VehicleCode.CP, 120),
      ZkVehicleMaxSpeed(VehicleCode.MF, 120),
      ZkVehicleMaxSpeed(VehicleCode.MV, 120),
      ZkVehicleMaxSpeed(VehicleCode.PA, 130),
      ZkVehicleMaxSpeed(VehicleCode.BS, 25),
      ZkVehicleMaxSpeed(VehicleCode.BB, 45),
      ZkVehicleMaxSpeed(VehicleCode.BF, 45)),
    List(ZkSpeedLimitMargin(25, 7),
      ZkSpeedLimitMargin(30, 7),
      ZkSpeedLimitMargin(40, 7),
      ZkSpeedLimitMargin(45, 7),
      ZkSpeedLimitMargin(50, 7),
      ZkSpeedLimitMargin(60, 7),
      ZkSpeedLimitMargin(70, 7),
      ZkSpeedLimitMargin(80, 7),
      ZkSpeedLimitMargin(90, 7),
      ZkSpeedLimitMargin(100, 8),
      ZkSpeedLimitMargin(110, 8),
      ZkSpeedLimitMargin(120, 8),
      ZkSpeedLimitMargin(130, 8)))
}

/*
* Part of preference
* @param wheelBaseType
* @param value
*/
case class ZkWheelBase(wheelBaseType: ZkWheelbaseType.Value, value: Double)

/*
* Part of preference
* @param vehicleType
* @param maxSpeed
*/
case class ZkVehicleMaxSpeed(vehicleType: VehicleCode.Value, maxSpeed: Int)

/*
* Indicates a wheelbase type
*/
object ZkWheelbaseType extends Enumeration {
  val P2M, P2M3M, P3M4M, P4M5M, P5M, LBB2M, LBB2M3M, LBB3M4M, LBB4M5M, LBB5M = Value
}

/**
 * part of preferences
 * @param speedLimit
 * @param speedMargin
 */
case class ZkSpeedLimitMargin(speedLimit: Int, speedMargin: Int = 0)

/*
* Is used as an storage object that contains everything of a system
* @param id of system
* @param name of system
* @param mtmRouteId that part of the system that is used to replace 'n999n' when creating case file mtmn999n.txt
* @param violationPrefixId the prefix of the folder name with violations
* @param serialNumber of the system
* @param orderNumber used by day report
* @param maxSpeed
* @param compressionFactor
* @param esaProviderType
* @param minimumViolationTimeSeparation in minutes
*/
case class ZkSystem(id: String,
                    name: String,
                    title: String,
                    location: ZkSystemLocation,
                    violationPrefixId: String,
                    orderNumber: Int,
                    maxSpeed: Int,
                    compressionFactor: Int,
                    retentionTimes: ZkSystemRetentionTimes,
                    pardonTimes: ZkSystemPardonTimes,
                    esaProviderType: EsaProviderType.Value = EsaProviderType.NoProvider,
                    mtmRouteId: String,
                    minimumViolationTimeSeparation: Int = 0, //minutes
                    serialNumber: SerialNumber,
                    pshtmId: Option[String] = None) {

  def isDynamax: Boolean = esaProviderType == EsaProviderType.Dynamax
}

/**
 * PardonTimes related attributes of the system configuration.
 * @param pardonTimeEsa_secs
 * @param pardonTimeTechnical_secs
 */
case class ZkSystemPardonTimes(pardonTimeEsa_secs: Long = 0, pardonTimeTechnical_secs: Long = 0)

/**
 * RetentionTimes related attributes of the system configuration.
 * @param nrDaysKeepTrafficData
 * @param nrDaysKeepViolations
 * @param nrDaysRemindCaseFiles
 */
case class ZkSystemRetentionTimes(nrDaysKeepTrafficData: Int,
                                  nrDaysKeepViolations: Int,
                                  nrDaysRemindCaseFiles: Int)

/**
 * Location related attributes of the system configuration.
 * @param description
 * @param viewingDirection
 * @param region
 * @param roadNumber
 * @param roadPart
 */
case class ZkSystemLocation(description: String,
                            viewingDirection: Option[String],
                            region: String,
                            roadNumber: String,
                            roadPart: String,
                            systemLocation: String)

/**
 * The serial number info
 * @param serialNumber
 * @param serialNumberPrefix
 * @param serialNumberLength
 */
case class SerialNumber(serialNumber: String,
                        serialNumberPrefix: String = "",
                        serialNumberLength: Int = 5)

/*
* Role where an user belongs to
* @param id
* @param indicationRoadworks
* @param indicationDanger
* @param indicationActualWork
* @param invertDrivingDirection
* @param fromPeriodHour
* @param fromPeriodMinute
* @param toPeriodHour
* @param toPeriodMinute
* @param speedLimit
* @param signSpeed
* @param signIndicator
* @param speedIndicatorType
* @param codeText
* @param dutyType
* @param deploymentCode
*/
case class ZkSchedule(id: String,
                      indicationRoadworks: ZkIndicationType.Value,
                      indicationDanger: ZkIndicationType.Value,
                      indicationActualWork: ZkIndicationType.Value,
                      invertDrivingDirection: ZkIndicationType.Value,
                      fromPeriodHour: Int,
                      fromPeriodMinute: Int,
                      toPeriodHour: Int,
                      toPeriodMinute: Int,
                      speedLimit: Int,
                      signSpeed: Int,
                      signIndicator: Boolean,
                      speedIndicatorType: Option[SpeedIndicatorType.Value] = None,
                      supportMsgBoard: Boolean,
                      msgsAllowedBlank: Int = 0) {

  // TODO: remove default value for msgsAllowedBlank

}
object ZkSchedule {
  /**
   * Create a time form the schedule
   * @param start Start time indicating the day
   * @param hour the hour
   * @param minute the minutes
   * @return create the time in millisSeconds
   */
  def getTime(start: Long, hour: Int, minute: Int): Long = {
    val cal = Calendar.getInstance()
    cal.clear()
    cal.setTimeInMillis(start)
    cal.set(Calendar.HOUR_OF_DAY, hour)
    cal.set(Calendar.MINUTE, minute)
    cal.set(Calendar.SECOND, 0)
    cal.set(Calendar.MILLISECOND, 0)
    cal.getTimeInMillis
  }

  def findScheduleForTime(time: Long, schedules: List[ZkSchedule]): Option[ZkSchedule] = {
    schedules.find(schedule ⇒ {
      val startTime = ZkSchedule.getTime(time, schedule.fromPeriodHour, schedule.fromPeriodMinute)
      val endTime = ZkSchedule.getTime(time, schedule.toPeriodHour, schedule.toPeriodMinute)
      time >= startTime && time < endTime
    })
  }
}

//Schedule

object ZkIndicationType extends Enumeration {
  val True, False, None = Value
}

//service type of corridor
object ServiceType extends Enumeration {
  val RedLight, SpeedFixed, SpeedMobile, SectionControl, ANPR, TargetGroup = Value
}

object ZkCaseFileType extends Enumeration {
  val TCVS33 /* old - IRS TC-VS versie 3.3 */ = Value
  val HHMVS14 /* new - IRS HHM-VS 1.4 */ = Value
  val HHMVS40 /* new - IRS HHM-VS 4.0 */ = Value
  val HHMVS20 = Value
}
/*
* State of the system
* @param state
* @param timestamp
* @param userId
* @param reason
*/
case class ZkState(state: String, timestamp: Long, userId: String, reason: String) {
  override def toString: String = "ZkState(%s, %s, %s, %s)".format(state, new Date(timestamp), userId, reason)
}

case class ZkSystemConfigHash(hash: String)

object ZkState {
  // val Off, StandBy, Failure, EnforceOn, EnforceOff, EnforceDegraded = Value
  val off = "Off"
  val Maintenance = "Maintenance"
  val standBy = "StandBy"
  val enforceOn = "EnforceOn"
  val enforceDegraded = "EnforceDegraded"
  val enforceOff = "EnforceOff"
  val failure = "Failure"
  val enforceStates = Seq(enforceOn, enforceDegraded)
}

/*
* An user
* @param id
* @param reportingOfficerId
* @param phone
* @param ipAddress
* @param name
* @param username
* @param email
* @param passwordHash
* @param roleId
* @param sectionIds
*/
case class ZkUser(id: String,
                  reportingOfficerId: String,
                  phone: String,
                  ipAddress: String,
                  name: String,
                  username: String,
                  email: String,
                  passwordHash: String,
                  roleId: String,
                  sectionIds: List[String])

case class ZkRole(id: String, name: String, description: String)

/**
 * The class stored as json object
 * @param systemId  The systemId of the tests
 * @param time  The time of the test
 * @param success The result of the test
 * @param nrCamerasTested The number of cameras tested
 * @param images The list of images
 * @param errors The list of found errors
 */
case class SelfTestResult(systemId: String,
                          corridorId: String,
                          time: Long,
                          success: Boolean,
                          reportingOfficerCode: String,
                          nrCamerasTested: Int,
                          images: Seq[KeyWithTimeAndId],
                          errors: Seq[String],
                          serialNr: String)

/**
 * Enumeration of the direction for selftestRegistration
 */
object Direction extends Enumeration {
  val Incoming = Value(-1, "Incoming")
  val Outgoing = Value(1, "Outgoing")
}

/**
 * Class to contain the self test registration
 *
 * @param systemId SystemId
 * @param time The registrationTime
 * @param image the key to the image
 * @param speed the measured speed
 * @param direction the direction
 * @param length the length
 */
case class SelfTestRegistration(systemId: String,
                                time: Long,
                                image: KeyWithTimeAndId,
                                speed: Float,
                                direction: Direction.Value,
                                length: Float,
                                serialNr: String)

case class ZkAlert(path: String,
                   alertType: String,
                   message: String,
                   timestamp: Long,
                   configType: ConfigType.Value,
                   reductionFactor: Float,
                   confirmed: Boolean = false) extends SystemError {
  /**
   * Detect a similar event to avoid doubles. The properties which are taken into consideration:
   * path, alertType
   */
  def isSimilar(another: ZkAlert): Boolean = {
    path == another.path && alertType == another.alertType
  }
}

case class ValueRange[T](min: T, max: T)

case class ZkClassifyConfidenceIntrada(confidence: Int = 80)

case class ZkClassifyConfidence(minCarLength: Float = 0.1F,
                                maxCarLength: Float = 6F,
                                maxVanLength: Float = 6.4F,
                                minTrailerLength: Float = 5.6F,
                                minLargeTruckLength: Float = 12.2F,
                                carConfidence: Int = 10,
                                trailerConfidence: Int = 10,
                                processCategory: String = "TDC3-TDC1",
                                confidenceLimit: Int = 70)

case class CountryLicenseCharacteristic(country: String, minPlateSize: Int)

case class ZkClassifyCountry(mobiCountries: List[String] = List("BE", "DE", "CH"),
                             similarCountries: List[String] = List("FI", "SE", "LT", "HU"),
                             licenseCharacteristics: List[CountryLicenseCharacteristic] = List())

case class ZkMultiCorridorConfig(classify: MultiCorridorClassifyConfig)

/**
 * Configuration needed for MultiCorridorClassify
 * @param minReliableCar
 * @param minimumViolationTimeSeparation
 */
case class MultiCorridorClassifyConfig(enable: Boolean = false,
                                       minReliableCar: Int = 10,
                                       minimumViolationTimeSeparation: Long = 1800000L)

case class ZkWrongReadDutchLicensePlate(plate: String,
                                        isPlatePersonCar: Boolean = false,
                                        isOtherPlatePersonCar: Boolean = false)

case class ZkCorrectTDC3CategoryConfig(enable: Boolean = true, maxGapTime: Long = 1030L, categoryConfidence: Int = 90)

/**
 * Class containing the PreSelector configuration
 * @param labels comma separated string of labels
 */
case class ZkPreSelector(labels: String)

/**
 * Configuration of the behaviour of the system state.
 *
 * @param enforceDetail is Enforce split into three sub states (EnforceOn,EnforceDegraded, EnforceOffline)
 * @param destroyData needs the deletion of the data, when open door is detected
 * @param startInStandBy needs to start in standby or proceed in last known state
 */
case class ZkSystemDefinition(enforceDetail: Boolean = true, destroyData: Boolean = true, startInStandBy: Boolean = true, acceptFailureInOffMode: Boolean = false)

trait SystemError {
  val path: String
  val message: String
  val timestamp: Long
  val configType: ConfigType.Value
  val confirmed: Boolean

  /**
   * Detect a similar event to avoid doubles. The properties which are taken into consideration:
   * path, message, configType
   */
  def isSimilar(another: SystemError): Boolean = {
    path == another.path && message == another.message && configType == another.configType
  }

}
/*
* Indicates to what configurable item an Alert/Failure belongs to
*/
object ConfigType extends Enumeration {
  val System, Corridor, Section, Gantry, Lane, Other = Value
}

case class ZkFailure(path: String, failureType: String, message: String, timestamp: Long, configType: ConfigType.Value, confirmed: Boolean = false) extends SystemError {
  def isSimilar(another: ZkFailure): Boolean = {
    path == another.path && failureType == another.failureType
  }
}

case class ZkTriggerData(modTime: Long, userId: String, reportingOfficerCode: String, processed: Boolean, reason: String)

case class ZkCalibrationState(on: Boolean)

case class ZkLaneConfig(id: String, name: String, bpsLaneId: Option[String] = None, camera: CameraConfig,
                        radar: RadarConfig, pir: PIRConfig, decorate: Option[DecorateConfig])

/*
Camera Configuration needed for each Lane
@param relayURI path where the data is places for this lane
@param cameraHost The host of the camera
@param cameraPort The ports of the camera
@param maxTriggerRetries The maximum number of retrying to send a camera trigger
@param timeDisconnected The disconnected time when a disconnected systemEvent is send
 */
case class CameraConfig(relayURI: String, cameraHost: String, cameraPort: Int, maxTriggerRetries: Int, timeDisconnected: Long)

/*
* Represents the config of a radar
* @param radarURI
* @param measurementAngle
* @param height
* @param surfaceReflection
*/
case class RadarConfig(radarURI: String, measurementAngle: Double, height: Double, surfaceReflection: Double)

/*
* Represents a Pir
* @param pirHost
* @param pirPort
* @param pirAddress
* @param refreshPeriod
* @param vrHost
* @param vrPort
*/
case class PIRConfig(pirHost: String, pirPort: Int, pirAddress: Short, refreshPeriod: Long, vrHost: String, vrPort: Int)

case class ZkAlertHistory(path: String,
                          alertType: String,
                          message: String,
                          startTime: Long,
                          endTime: Long,
                          configType: String,
                          reductionFactor: Float)

object ZkErrorHistory {

  def getTimeFromNodeName(path: Path): Long = {
    getTimeFromNodeName(path.nodes.last.name)
  }

  /**
   * Format nodeName <time msec (13)><10 pos sequence>
   * @param nodeName
   * @return
   */
  def getTimeFromNodeName(nodeName: String): Long = {
    if (nodeName.length < 13) {
      throw new IllegalArgumentException("NodeName not a AlertHistory name %s".format(nodeName))
    }
    val timePart = nodeName.take(13)
    timePart.toLong
  }
}

case class ZkFailureHistory(path: String,
                            failureType: String,
                            message: String,
                            startTime: Long,
                            endTime: Long,
                            configType: String)

object DayReportVersion extends Enumeration {
  val HHM_V40, HHM_V42, HHM_V50, HHM_V60, HHM_V421 = Value
}

/**
 * Default configuration for sending email to customers
 * @param subject localized message used as the subject for an email
 * @param bodyText Localized message usd as the body text for an email
 * @param useBBC indicate if bbc should be used instead of cc
 * @param to email receivers example separated by ; example 'email@company.com;email2@company2.com
 * @param dateFormat date format used to format date in email subject
 * @param requiresAuthentication indicate if smtp authentication is required
 * @param username used for smtp authentication if requiresAuthentication is true
 * @param password used for smtp authentication if requiresAuthentication is true
 * @param properties additional properties that can be set regarding email
 *                   Copy from 'case class models.storage.DayReportConfig'
 * @param dayReportVersion the version of the report builder used
 * @param dayReportName filename to be used for the day report (excluding extension), using a printf style . It must at least contain a placeholder for the key
 */
case class ZkDayReportConfig(subject: String,
                             bodyText: String = "",
                             useBBC: Boolean = true,
                             to: String,
                             dateFormat: String,
                             requiresAuthentication: Boolean = false,
                             username: Option[String] = None,
                             password: Option[String] = None,
                             properties: Map[String, String],
                             exportDirectory: Option[String] = None,
                             dayReportVersion: DayReportVersion.Value = DayReportVersion.HHM_V40,
                             dayReportName: String = "Dagrapport_%s")

/**
 * Configuration for System Log
 */
case class AuditorConfig(ftpListenerPort: Int = 12421,
                         ntpListenerFile: String = "/var/run/daemonlog-fifo",
                         ntpListenerPattern: String = ".*offset.*",
                         freeSpaceLimit: Int = 80,
                         fileSystems: Option[String] = Some("/"))

/**
 * Configuration of the zip Export
 * @param finalExportTime Minutes after midnight which indicate that the export has to be done
 * @param pollFrequency milliseconds time between check if there is anything to export
 * @param nrWorkers number of zip actors
 * @param splitSize size of the zip files
 * @param workingDir used working directory
 * @param ExportDir the export directory of the zip files
 */
case class ZkExportZipConfig(finalExportTime: Long,
                             pollFrequency: Long,
                             nrWorkers: Int,
                             splitSize: Long,
                             workingDir: String,
                             ExportDir: String,
                             processDate: Option[Long] = None)

case class EnforcementStatus(mtmDataProcessed: Boolean = false,
                             mtmDataProcessingTime: Long = 0,
                             qualificationComplete: Boolean = false,
                             registrationComplete: Boolean = false)

case class ZkCalibration(calibrationSpeed: Float = 250f)

case class ZkVehicleLengthBoundaries(minLength: Float = 0f, maxLength: Float = 0f)

case class ZkVehicleLengthDeviations(minDeviation: Float = 0f, maxDeviation: Float = 0f)

case class ZkVehicleLengthRDWDeviations(minDeviation: Float = 0f, maxDeviation: Float = 0f)

case class ZkVehicleLengthData(vehicleCode: String, boundaries: ZkVehicleLengthBoundaries, deviations: ZkVehicleLengthDeviations, rdwDeviations: ZkVehicleLengthRDWDeviations)

case class ZkVehicleLengthDataConfig(postClassify: Boolean, vehicleLengthData: List[ZkVehicleLengthData])

case class ZkClassifyConfig(defaultVehicleCode: String = "BF", laneOverrule: Map[String, String] = Map())

case class ExportDirectoryForSpeedLimit(speedlimit: Int, exportDirectory: String)
case class ZkCaseFileConfig(header: String, exportDirectoryForSpeedLimits: List[ExportDirectoryForSpeedLimit])
case class IgnoreStatisticsOfLane(systemId: String, gantryId: String, laneId: String)
case class ZkDailyReportConfig(ignoreStatisticsOfLanes: List[IgnoreStatisticsOfLane], speedEntries: List[DailyReportConfigSpeedEntry])

case class DailyReportConfigSpeedEntry(speedLimit: String, id: String, ht: String)

object DailyReportConfigSpeedEntry {
  val notConfigured = "notConfigured"

  def augmentWithSpeeds(speedEntries: List[DailyReportConfigSpeedEntry], scheduleSpeeds: Set[Int]): List[DailyReportConfigSpeedEntry] = {
    val (entriesWithSpeedStrings, otherEntries) = speedEntries.partition { entry ⇒
      eitherStringOrInt(entry.speedLimit).isRight
    }
    val configuredSpeeds = entriesWithSpeedStrings.map(_.speedLimit.toInt).toSet[Int]
    val missingSpeeds = scheduleSpeeds -- configuredSpeeds
    val augmentedSpeedEntries: List[DailyReportConfigSpeedEntry] = {
      missingSpeeds.map { speed ⇒
        DailyReportConfigSpeedEntry(speed.toString, notConfigured, speed.toString + " kmh missing")
      }.toList ++
        entriesWithSpeedStrings.filter { entry ⇒ scheduleSpeeds.contains(entry.speedLimit.toInt) }
    }.sortBy(_.speedLimit.toInt)

    val total = if (augmentedSpeedEntries.size == 1) Nil
    else if (otherEntries.size == 1)
      otherEntries
    else
      List(DailyReportConfigSpeedEntry("total", notConfigured, "total missing"))

    augmentedSpeedEntries ++ total
  }
}

case class ParallelCorridorHHTrajectConfig(speedLimit: String, id: String, ht: String, entryGantryName: String, exitGantryName: String)

object ParallelCorridorHHTrajectConfig {
  val speedLimitTotal = "total"
  val notConfigured = "notConfigured"

  def apply(speedLimit: String, corridorIds: Seq[Int]) = new ParallelCorridorHHTrajectConfig(
    speedLimit = speedLimit,
    id = notConfigured,
    ht = speedLimit + " kmh missing for parrallel corridors " + corridorIds.mkString(", "),
    entryGantryName = notConfigured,
    exitGantryName = notConfigured)
}

case class ParallelCorridorConfig(parallelCorridorIds: List[Int], parallelHHTrajectConfigs: List[ParallelCorridorHHTrajectConfig])
