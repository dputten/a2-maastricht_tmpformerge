/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.storage

import csc.sectioncontrol.messages._
import csc.sectioncontrol.classify.nl.DecisionResult

/**
 * Lock down HBase column names and stored record types.
 * Reading and writing classes should use these values and types to define what to read/write where,
 * which will help ensure that storage type mismatches are minimized.
 *
 * @author Maarten Hazewinkel
 */
object HBaseTableDefinitions {
  import VehicleImageType._

  //def vehicleRecordTableName(systemId: String): String = vehicleRecordTableName
  def vehicleRecordTableName = "vehicle"
  val vehicleRecordColumnFamily = "cf"
  val vehicleRecordColumnName = "M"
  val vehicleRecordLicenseColumnName = "ML"
  type vehicleRecordType = VehicleMetadata

  def processingTableName = "keepalive"
  val processingColumnFamily = "cf"
  val processingComponentColumnName = "C"
  val processingStateColumnName = "S"
  val processingTimeColumnName = "T"
  val processingRecordColumnName = "R"
  type processingRecordType = ProcessingRecord

  //def speedRecordTableName(systemId: String):String = speedRecordTableName
  def speedRecordTableName = "speedRecord"
  val speedRecordColumnFamily = "V"
  val speedRecordColumnName = "SR"
  type speedRecordType = VehicleSpeedRecord

  //def vehicleViolationTableName(systemId: String): String = vehicleViolationTableName
  def vehicleViolationTableName = "violation"
  val vehicleViolationColumnFamily = "V"
  val vehicleViolationColumnName = "VVR"
  type vehicleViolationRecordType = VehicleViolationRecord

  def confirmedViolationTableName = "confirmedViolation"
  val confirmedViolationColumnFamily = "CV"
  val confirmedViolationColumnName = "CVR"

  //def processedViolationTableName(systemId: String): String = processedViolationTableName
  def processedViolationTableName = "processedViolation"
  val processedViolationColumnFamily = "V"
  val processedViolationColumnName = "PVR"
  type processedViolationRecordType = ProcessedViolationRecord

  //def vehicleClassifiedTableName(systemId: String): String = vehicleClassifiedTableName
  def vehicleClassifiedTableName = "vehicleClassified"
  val vehicleClassifiedColumnFamily = "V"
  val vehicleClassifiedColumnName = "VCR"
  type vehicleClassifiedRecordType = VehicleClassifiedRecord

  //def mtmFileTableName(systemId: String): String = mtmFileTableName
  def mtmFileTableName = "mtmFile"
  val mtmFileColumnFamily = "R"
  val mtmFileColumnName = "MTM"
  type mtmFileRecordType = Array[Byte]

  //def vehicleImageTableName(systemId: String) = vehicleImageTableName
  def vehicleImageTableName = vehicleRecordTableName
  val vehicleImageColumnFamily = "cf"
  val vehicleImageOverviewColumnName = "O"
  val vehicleImageOverviewMaskedColumnName = "OM"
  val vehicleImageOverviewRedLightColumnName = "OR"
  val vehicleImageOverviewSpeedColumnName = "OS"
  val vehicleImageLicenseColumnName = "L"
  val vehicleImageRedLightColumnName = "R"
  val vehicleImageMeasureMethod2SpeedColumnName = "2S"
  type vehicleImageRecordType = Array[Byte]

  //def calibrationImageTableName(systemId: String): String = calibrationImageTableName
  def calibrationImageTableName = "SelftestImage"
  val calibrationImageColumnFamily = "cf"
  val calibrationImageColumnName = "I"
  type calibrationImageRecordType = Array[Byte]

  //def calibrationRegistrationTableName(systemId: String): String = calibrationRegistrationTableName
  def calibrationRegistrationTableName = "SelftestRegistration"
  val calibrationRegistrationColumnFamily = "cf"
  val calibrationRegistrationColumnName = "R"
  type calibrationRegistrationRecordType = SelfTestRegistration

  //def selfTestResultTableName(systemId: String): String = selfTestResultTableName
  def selfTestResultTableName = "SelftestResult"
  val selfTestResultColumnFamily = "cf"
  val selfTestResultColumnName = "R"
  type selfTestResultRecordType = SelfTestResult

  //def decisionTableName(systemId: String): String = decisionTableName
  def decisionTableName = "DecisionResult"
  val decisionColumnFamily = "cf"
  val decisionColumnName = "R"
  type decisionRecordType = DecisionResult

  private val columns = Map(
    License -> vehicleImageLicenseColumnName,
    Overview -> vehicleImageOverviewColumnName,
    OverviewMasked -> vehicleImageOverviewMaskedColumnName,
    OverviewRedLight -> vehicleImageOverviewRedLightColumnName,
    OverviewSpeed -> vehicleImageOverviewSpeedColumnName,
    RedLight -> vehicleImageRedLightColumnName,
    MeasureMethod2Speed -> vehicleImageMeasureMethod2SpeedColumnName)

  def toColumnName(imageType: VehicleImageType.Value): Option[String] = toColumnName(imageType.toString)

  def toColumnName(imageType: String): Option[String] = {
    columns
      .find(_._1.toString == imageType)
      .map(s ⇒ Some(s._2))
      .getOrElse(None)
  }

  def serviceStatisticsTableName = "ServiceStatistics"
  val serviceStatisticsColumnFamily = "cf"
  val sectionControlStatisticsColumnName = "SC"
  type sectionControlStatisticsRecordType = SectionControlStatistics
  val redLightStatisticsColumnName = "RL"
  type redLightStatisticsRecordType = RedLightStatistics
  val speedFixedStatisticsColumnName = "SF"
  type speedFixedStatisticsRecordType = SpeedFixedStatistics

}

/**
 * Key class for storing and retrieving objects in the data store
 * @param time The timestamp of the object
 * @param id The id of the object
 */
case class KeyWithTimeAndId(time: Long, id: String)

case class KeyWithTimeIdAndPostFix(time: Long, id: String, postFix: Option[String])
