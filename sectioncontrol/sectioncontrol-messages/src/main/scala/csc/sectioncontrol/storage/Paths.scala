/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.storage

import csc.sectioncontrol.messages.StatsDuration
import java.text.SimpleDateFormat

object Paths {

  implicit def string2pathconcat(s: String) = new AnyRef {
    def /(s2: String) = s + "/" + s2
  }

  private def current = { "current" }
  private def history = { "history" }
  private def state = { "state" }
  private def failures = { "failures" }
  private def alerts = { "alerts" }
  private def errors = { "errors" }
  private def qn_prefix = { "qn-" }
  private def control = { "control" }
  private def lanes = { "lanes" }

  object Corridors {
    /**
     * return the default path where all corridors reside
     * @param systemId is id of system
     * @return Path to corridors
     */
    def getDefaultPath(systemId: String) = Systems.getDefaultPath / systemId / "corridors"

    /**
     * return the root path of given corridorId
     * @param systemId is id of system
     * @param corridorId is id of corridor
     * @return Path to specific corridor
     */
    def getRootPath(systemId: String, corridorId: String) = getDefaultPath(systemId) / corridorId

    /**
     * return the path of the corridor config leaf
     * @param systemId is id of system
     * @param corridorId is id of corridor
     * @return Path to specific corridor config leaf
     */
    def getConfigPath(systemId: String, corridorId: String) = getRootPath(systemId, corridorId) / "config"

    def getDailyReportConfigPath(systemId: String, corridorId: String) = getRootPath(systemId, corridorId) / "dailyreport" / "config"

    /**
     * return the path of the corridors service
     */
    def getServicesPath(systemId: String, corridorId: String) = getRootPath(systemId, corridorId) / "services"

    /**
     * return the path of of the corridors casefile format
     */
    def getCaseFilePath(systemId: String, corridorId: String) = getRootPath(systemId, corridorId) / "caseFiles"

    /**
     * return the path of of the corridors casefile format
     */
    def getCaseFileConfig(systemId: String, corridorId: String) = getCaseFilePath(systemId, corridorId) / "config"

    /**
     * return the path of the list of schedules for the corridor
     */
    def getSchedulesPath(systemId: String, corridorId: String) = getRootPath(systemId, corridorId) / "schedule"

    /**
     * return the path of the list of schedules for the corridor
     */
    def getScheduleHistoryPath(systemId: String, corridorId: String) = getRootPath(systemId, corridorId) / "scheduleHistory"

    /**
     * Return the path where the corridor level jobs are stored
     */
    def getCorridorJobsPath(systemId: String, corridorId: String) = getRootPath(systemId, corridorId) / "jobs"
  }

  object Systems {
    /**
     * return the default path where all systems reside
     */
    def getDefaultPath = "/ctes" / "systems"

    /**
     * return the base path for this system
     */
    def getSystemPath(systemId: String) = getDefaultPath / systemId

    /**
     * return the path where the system config is stored
     */
    def getSystemDefinitionPath(systemId: String) = getDefaultPath / systemId / "definition"

    /**
     * return the path where the system config is stored
     */
    def getConfigPath(systemId: String) = getDefaultPath / systemId / "config"

    /**
     * return the path where the parallelCorridor configuration is stored
     */
    def getParallelCorridorConfigPath(systemId: String) = getSystemPath(systemId) / "dailyreport" / "config"
    /**
     * return the path where the system events is stored
     */
    def getSystemEventPath(systemId: String) = getDefaultPath / systemId / "log-meta"

    def getLogEventsPath(systemId: String) = getDefaultPath / systemId / "log-events"

    def getEventQueuePath(systemId: String) = getDefaultPath / systemId / "events"

    def getLogPath(systemId: String) = getLogEventsPath(systemId) / "qn-"

    //def getSystemOfflineEventsPath(systemId: String) = getDefaultPath / systemId / "offline"
    def getSystemOfflineEventsPath(systemId: String, corridorId: String) = State.getPath(systemId, corridorId) / "offlineEvents"

    def getLogHistoryPath(systemId: String) = getDefaultPath / systemId / "log-history"

    /**
     * Return the path where the system level jobs are stored
     */
    def getSystemJobsPath(systemId: String) = getDefaultPath / systemId / "jobs"

    /**
     * Return the path where the MatchCompletionStatus for a system is stored
     */
    def getPreSelectorCompletionStatusPath(systemId: String) = getSystemJobsPath(systemId) / "preSelectorCompletionStatus"

    /**
     * Return the path where the MatchCompletionStatus for a system is stored
     */
    def getMatchCompletionStatusPath(systemId: String) = getSystemJobsPath(systemId) / "matchCompletionStatus"

    /**
     * Return the path where the ClassificationCompletionStatus for a system is stored
     */
    def getClassificationCompletionStatusPath(systemId: String) = getSystemJobsPath(systemId) / "classificationCompletionStatus"

    /**
     * Return the path where the ClassificationCompletionStatus for a system is stored
     */
    def getProcessedRegistrationsStatusPath(systemId: String) = getSystemJobsPath(systemId) / "processedRegistrationsStatus"

    /**
     * Return the path where the ClassificationCompletionStatus for a system is stored
     */
    def getValidationFilterCompletionStatus(systemId: String) = getSystemJobsPath(systemId) / "validationFilterCompletionStatus"

    /**
     * Return the path of the Enforce Status
     */
    def getEnforceStatusPath(systemId: String, day: Long) = {
      val df = new SimpleDateFormat("yyyy-MM-dd")
      getSystemJobsPath(systemId) / "enforceStatus-nl" / df.format(day)
    }

    /**
     * Get the registerViolationsJob Config path
     */
    def getRegisterViolationsJobConfigPath(systemId: String) = getSystemJobsPath(systemId) + "/registerViolationsJob/config"

    /**
     * Return the path where to store the generated certificates.
     * Children must be instances of ActiveCertificate class
     */
    def getActiveCertificatesPath(systemId: String) = getDefaultPath / systemId / "activeCertificates"

    def getTimewatchStatePath(systemId: String) = getSystemPath(systemId) / "timewatch-state"
    /**
     * Return the path where to look for the provided install certificate.
     * The node must be an instance of TypeCertificate class
     */
    def getInstallCertificatePath(systemId: String) = getDefaultPath / systemId / "certificate" / "measurementMethod"

    /**
     * Return the path where to look for the provided Location certificate
     */
    def getLocationCertificatePath(systemId: String) = getDefaultPath / systemId / "certificate" / "location"

    /**
     * Return the path where to look for the provided ZkFunctionalCorridor.
     * The node must be an instance of ZkFunctionalCorridor class
     */
    // def getFunctionalCorridorPath(systemId: String) = getDefaultPath / systemId / "functionalCorridors"

    /**
     * Return the path where the current configuration hash for a system is stored
     */
    def getConfigHashPath(systemId: String): String = getDefaultPath / systemId / "config-hash"

    /**
     * Return the path to auditor configuration
     */
    def getAuditorPath(systemId: String) = getDefaultPath / systemId / "auditor"

    private def getRuntimeStatisticPath(systemId: String, stats: StatsDuration) = getDefaultPath / systemId / "statistics" / stats.key / "runtimeData"

    /**
     * Return the path where the runtime statistics for each Matcher run is stored
     */
    def getRuntimeMatcherPath(systemId: String, stats: StatsDuration) = getRuntimeStatisticPath(systemId, stats) / "matcher"

    /**
     * Return the path where the runtime statistics for RegisterViolations run is stored (DayViolationsStatistics)
     */
    def getRuntimeViolationsPath(systemId: String, stats: StatsDuration) = getRuntimeStatisticPath(systemId, stats) / "enforce"

    /**
     * Return the path where the generated MTM info is stored
     */
    def getRuntimeMtmInfoPath(systemId: String, stats: StatsDuration) = getRuntimeStatisticPath(systemId, stats) / "mtminfo"

    /**
     * Return the path where the generated Msi info is stored
     */
    def getRuntimeMsiBlanksPath(systemId: String, stats: StatsDuration) = getRuntimeStatisticPath(systemId, stats) / "msiBlanks"

    /**
     * Return the path for the final day report
     */
    def getReportPath(systemId: String, stats: StatsDuration) = getDefaultPath / systemId / "reports" / stats.key

    /**
     * Return the hbase table name for the global systems log
     */
    def getSystemsLogTableName = "GlobalSystemsLog"

    /**
     * Return the path for the case file to support 'zaakbestand versie'
     */
    //def getCaseFilePath(systemId: String) = getDefaultPath / systemId / "case-file"

    def getMultiCorridorConfigPath(systemId: String) = getDefaultPath / systemId / "multiCorridor"

    /**
     * return the root path for all notifications settings for the current system
     */
    def getNotificationsPath(systemId: String) = getDefaultPath / systemId / "notifications"

    /**
     * return path where all notification settings for the current system are stored
     */
    def getNotifierConfigPath(systemId: String, notifierId: String) = getNotificationsPath(systemId) / notifierId / "config"

    //    def getSelfTestPath(systemId: String) = getSystemPath(systemId) / "selftest"
    //    def getSelfTestTriggerPath(systemId: String) = getSelfTestPath(systemId) / "trigger"
    /**
     * return path where the calibration configuration is stored
     */
    def getCalibrationConfigPath(systemId: String) = getDefaultPath / systemId / "selftest" / "config"

    /**
     * return the path where the system config is stored
     */
    def getClassifyConfigPath(systemId: String) = getDefaultPath / systemId / "classify/config"

  }

  object Labels {
    /**
     * return the path where the system config is stored
     */
    def getDefaultPath = "/ctes" / "labels"
  }

  object Preferences {
    /**
     * return the default path where all preferences reside
     */
    def getDefaultPath(systemId: String) = Systems.getDefaultPath / systemId / "preferences"

    def getGlobalConfigPath = Configuration.getRootPath / "preferences"

    /**
     * return the path of the preference leaf
     */
    def getConfigPath(systemId: String) = getDefaultPath(systemId) / "config"
  }

  object Sections {
    /**
     * return de default path where all sections reside
     * @param systemId is id of system
     * @return default Path to Sections
     */
    def getDefaultPath(systemId: String) = Systems.getDefaultPath / systemId / "sections"

    /**
     * return de root path where a section reside
     * @param systemId is id of system
     * @param sectionId is id of section
     * @return Path to Sections
     */
    def getRootPath(systemId: String, sectionId: String) = getDefaultPath(systemId) / sectionId

    /**
     * return the path of the config leaf
     * @param systemId is id of system
     * @param sectionId is id of section
     * @return path of the config leaf
     */
    def getConfigPath(systemId: String, sectionId: String) = getRootPath(systemId, sectionId) / "config"

    def getTubesSettingsPath(systemId: String, sectionId: String) = getRootPath(systemId, sectionId) / "tubesSetting"
  }

  object Gantries {
    /**
     * return de default path where all gantries reside
     * @param systemId is id of system
     * @return default Path to Gantries
     */
    def getDefaultPath(systemId: String) = Systems.getDefaultPath / systemId / "gantries"

    /**
     * return de root path where a gantry reside
     * @param systemId is id of system
     * @param gantryId is id of gantry
     * @return Path to Gantries
     */
    def getRootPath(systemId: String, gantryId: String) = getDefaultPath(systemId) / gantryId

    /**
     * return the path of the config leaf
     * @param systemId is id of system
     * @param gantryId is id of gantry
     * @return path of the config leaf
     */
    def getConfigPath(systemId: String, gantryId: String) = getRootPath(systemId, gantryId) / "config"

    def getLanesPath(systemId: String, gantryId: String) = getRootPath(systemId, gantryId) / "lanes"

    def getLaneRootPath(systemId: String, gantryId: String, laneId: String) =
      getLanesPath(systemId, gantryId) / laneId

    def getLanesConfigPath(systemId: String, gantryId: String, laneId: String) =
      getLanesPath(systemId, gantryId) / laneId / "config"
  }

  object Roles {
    /**
     * Return the root path where roles nodes reside
     */
    def getRootPath = "/ctes" / "roles"

    /**
     * Return the path where a role nodes reside
     */
    def getRolePath(roleId: String) = "/ctes" / "roles" / roleId
  }

  object Users {
    /**
     * Return the root path where user nodes reside
     */
    def getRootPath = "/ctes" / "users"

    /**
     * Return the path where a user nodes reside
     */
    def getUserPath(userId: String) = "/ctes" / "users" / userId
  }

  object Configuration {
    /**
     * Return the path where the ClassificationCompletionStatus for a system is stored
     */
    def getMailServerPath = getRootPath / "mail" / "mailServer"
    /**
     * Return the root path where configuration resides
     */
    def getRootPath = "/ctes" / "configuration"
    /**
     * Return the path where a day report configuraqtion is stored
     */
    def getReportsConfigPath = getRootPath / "reports" / "config"

    def getDynamaxConfigPath = getRootPath / "dynamax" / "config"

    def getDynamaxIfacePath = getRootPath / "dynamax" / "iface"

    def getGlobalEnforceConfig = getRootPath / "enforce" / "enforceConfig-nl"

    def getExportZipConfigPath = getRootPath / "zipexport"

    def getClassifyConfidenceIntradaConfigPath = getRootPath / "classifyConfidenceIntrada"

    def getGlobalSystemsConfig = getRootPath / "jobs" / "systems"

    /**
     * Return the root path for the notification configuration
     */
    def getNotificationRootPath = getRootPath / "notification"

    /**
     * Return the path where the notification state is stored
     */
    def getNotificationStatePath = getNotificationRootPath / "state"

    /**
     * Return the root path for the notification notifiers configurations
     */
    def getNotifiersRootPath = getNotificationRootPath / "notifiers"

    /**
     * Return the path where the notifier config is stored
     */
    def getNotifierConfigPath(notifierId: String) = getNotifiersRootPath / notifierId / "config"

    /**
     * Return the path where the notifier activeEvent settings are stored
     */
    def getNotifierEventsPath(notifierId: String) = getNotifiersRootPath / notifierId / "events"

    def getClassifyConfidence = getRootPath / "classifyConfidence"
    def getClassifyCountry = getRootPath / "classifyCountry"

    def getClassifyRules = getRootPath / "classifyRules"
    def getCorrectTDC3Category = getRootPath / "CorrectTDC3Category"

    def getWrongReadDutchLicensePlatePath = getRootPath / "wrongReadDutchLicensePlate"

    def getCalibration = getRootPath / "calibration"

    def getVehicleLengths = getRootPath / "vehicleLengths"

    def getCertificateMeasurementMethodTypePath(key: String) = getRootPath / "certificate" / "measurementMethodType" / key

    def getCountryCodes = getRootPath / "countries" / "codes"

    def getHBaseTablesRowKeys = getRootPath / "hbaseRowKeys"

    /**
     * return the path where the system config is stored
     */
    def getClassifyConfigPath = getRootPath / "classify/config"
  }

  object PreSelector {
    /**
     * Get the default path where all the preselectors are defined
     * @return the requested path
     */
    def getDefaultPath = "/ctes" / "preselector"

    /**
     * Get the root path of the specified preselector
     * @param preSelectorId the id of the preselector
     * @return the requested path
     */
    def getRootPath(preSelectorId: String) = getDefaultPath / preSelectorId

    /**
     * Get the configuration path of the specified preselector
     * @param preSelectorId the id of the preselector
     * @return the requested path
     */
    def getConfigPath(preSelectorId: String) = getRootPath(preSelectorId) / "config"

  }

  object MaintenanceInterface {
    val maintenance = "maintenance"

    def getModuleCompletionStatusPath(systemId: String, moduleId: String) = Systems.getSystemJobsPath(systemId) / maintenance + "CompletionStatus" / moduleId

    def getConfigPath() = Configuration.getRootPath / maintenance
  }

  object Recognize {
    val recognize = "recognize"

    /**
     * return the global recognize path for this application
     */
    def getDefaultRecognizeOptionsPath() = Configuration.getRootPath / recognize

    /**
     * return the system recognize path for specified system
     */
    def getSystemRecognizeOptionsPath(systemId: String) = Systems.getSystemPath(systemId) / recognize

    /**
     * return the lane recognize path for specified lane
     */
    def getLaneRecognizeOptionsPath(systemId: String, gantryId: String, laneId: String) = Gantries.getLaneRootPath(systemId, gantryId, laneId) / recognize

  }

  object CalibrationState {
    private def calibration = { "calibration" }
    private def control = { "control" }
    private def trigger = { "trigger" }

    def getDefaultPath(systemId: String, corridorId: String) = {
      require(!systemId.isEmpty)
      require(!corridorId.isEmpty)
      Corridors.getRootPath(systemId, corridorId) / control / calibration / trigger
    }
  }

  object State {
    /**
     * return the path where the state is stored
     */
    def getCurrentPath(systemId: String, corridorId: String) = {
      require(!systemId.isEmpty)
      require(!corridorId.isEmpty)
      Corridors.getRootPath(systemId, corridorId) / control / state / current
    }

    def getPath(systemId: String, corridorId: String) = {
      require(!systemId.isEmpty)
      require(!corridorId.isEmpty)
      Corridors.getRootPath(systemId, corridorId) / control / state
    }

    def getHistoryPath(systemId: String, corridorId: String) = {
      require(!systemId.isEmpty)
      require(!corridorId.isEmpty)
      Corridors.getRootPath(systemId, corridorId) / control / state / history
    }

    def getHistoryPrefixPath(systemId: String, corridorId: String) = {
      require(!systemId.isEmpty)
      require(!corridorId.isEmpty)
      getHistoryPath(systemId, corridorId) / qn_prefix
    }
  }

  object Failure {
    def getCurrentPath(systemId: String, corridorId: String) = {
      require(!systemId.isEmpty)
      require(!corridorId.isEmpty)
      Corridors.getRootPath(systemId, corridorId) / control / errors / failures / current
    }

    def getCurrentPrefixPath(systemId: String, corridorId: String) = {
      require(!systemId.isEmpty)
      require(!corridorId.isEmpty)
      getCurrentPath(systemId, corridorId) / qn_prefix
    }

    def getHistoryPath(systemId: String, corridorId: String) = {
      require(!systemId.isEmpty)
      require(!corridorId.isEmpty)
      Corridors.getRootPath(systemId, corridorId) / control / errors / failures / history
    }
  }

  object Alert {
    def getCurrentPath(systemId: String, corridorId: String) = {
      require(!systemId.isEmpty)
      require(!corridorId.isEmpty)
      Corridors.getRootPath(systemId, corridorId) / control / errors / alerts / current
    }

    def getCurrentPrefixPath(systemId: String, corridorId: String) = {
      require(!systemId.isEmpty)
      require(!corridorId.isEmpty)
      getCurrentPath(systemId, corridorId) / qn_prefix
    }

    def getHistoryPath(systemId: String, corridorId: String) = {
      require(!systemId.isEmpty)
      require(!corridorId.isEmpty)
      Corridors.getRootPath(systemId, corridorId) / control / errors / alerts / history
    }
  }

  object Lane {
    def getDefaultPath(systemId: String, gantryId: String) = Gantries.getDefaultPath(systemId) / gantryId / lanes

    def getConfigPath(systemId: String, gantryId: String) = Gantries.getDefaultPath(systemId) / gantryId / lanes
  }
}
