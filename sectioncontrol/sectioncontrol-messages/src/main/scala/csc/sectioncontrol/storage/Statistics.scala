/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storage

import csc.sectioncontrol.messages.{ StatsDuration, Lane }
import csc.sectioncontrol.messages.matcher.MatcherCorridorConfig

/**
 * Statistics for a single lane. It is used by the matcher and the report generator
 */
case class LaneInfo(lane: Lane, count: Int)

object LaneInfo {
  /**
   * Coalesce a Seq[LaneInfo]. Multiple occurrences of the same lane in the input are
   * coalesced into one LaneInfo instance. The result contains a single LaneInfo
   * for each Lane.
   * @param laneInfos any sequence of LaneInfo instances
   * @return a sequence where multiple instances for the same Lane are coalsced
   *         into a single instance
   */
  def coalesce(laneInfos: Seq[LaneInfo]): Seq[LaneInfo] = {
    val laneMap: Map[Lane, Seq[LaneInfo]] = laneInfos.groupBy { laneInfo ⇒ laneInfo.lane }
    val laneTotals = laneMap.map { pair: (Lane, Seq[LaneInfo]) ⇒
      val (lane, laneInfosForLane) = pair
      val totalCount = laneInfosForLane.map(_.count).sum
      lane -> LaneInfo(lane, totalCount)
    }
    laneTotals.values.toSeq
  }
}

/**
 * An indicator to begin matching
 * @param stats day ID
 * @param end  UTC timestamp (in milliseconds) to begin with (exclusive)
 * @param begin UTC timestamp (in milliseconds) to begin with (inclusive). When the value is not provided,
 *              use the last end stored in ZooKeeper.
 */
case class StatisticsPeriod(stats: StatsDuration, end: Long, begin: Long)

sealed trait ServiceStatistics

/**
 * Provide result for matching vehicle registrations on a corridor
 * @param period the message proved as input with begin time always filled
 * @param input amount of processed vehicle registrations on a corridor for the given period
 * @param matched amount of matched runs
 * @param unmatched amount of unmatched vehicle registrations
 * @param doubles amount of double vehicle registrations for the same car (between 2 lanes)
 * @param averageSpeed average speed for all the matched runs
 * @param highestSpeed the highest calculated speed
 * @param laneStats statistics of runs per lane at the exit (sorted by lane ID)
 *                  (Req61: het aantal gedetecteerde voertuigpassages, per rij- en vluchtstrook)
 */
case class SectionControlStatistics(period: StatisticsPeriod,
                                    config: MatcherCorridorConfig,
                                    input: Int,
                                    matched: Int,
                                    unmatched: Int,
                                    doubles: Int,
                                    averageSpeed: Double,
                                    highestSpeed: Int,
                                    laneStats: List[LaneInfo]) extends ServiceStatistics {

}

/**
 * Provide statistics for Red light service
 * @param period the time period of the returned values
 * @param nrRegistrations number of registrations within the defined period
 * @param nrWithRedLight number of registrations with a red light time filled
 * @param averageRedLight the average of the red time of all the registrations with a red light time filled
 * @param highestRedLight the highest red time found
 * @param laneStats the number of registration separated over the lanes
 */
case class RedLightStatistics(period: StatisticsPeriod,
                              nrRegistrations: Int,
                              nrWithRedLight: Int,
                              averageRedLight: Double,
                              highestRedLight: Int,
                              laneStats: List[LaneInfo]) extends ServiceStatistics

/**
 * Provide statistics for fix Speed service
 * @param period the time period of the returnd values
 * @param nrRegistrations number of registrations within the defined period
 * @param nrSpeedMeasurement the number of registrations with a measured speed
 * @param averageSpeed the average speed
 * @param highestSpeed the highest speed
 * @param laneStats the number of registration separated over the lanes
 */
case class SpeedFixedStatistics(period: StatisticsPeriod,
                                nrRegistrations: Int,
                                nrSpeedMeasurement: Int,
                                averageSpeed: Double,
                                highestSpeed: Int,
                                laneStats: List[LaneInfo]) extends ServiceStatistics