/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storage

/**
 * Holds the completion status for all corridors within a system
 */
case class SystemCorridorCompletionStatus(corridors: Map[String, CompletionCorridorStatus])

/**
 * Holds the completion status for a single corridor
 *
 * @param lastUpdateTime The last time process was started up or finished for this corridor
 * @param completeUntil The time until which the process has been completed successfully for this corridor
 */
case class CompletionCorridorStatus(lastUpdateTime: Long, completeUntil: Long)

/**
 * Holds the completion status for all lanes within a system
 * @param lanes All the lanes of a system key [systemId]-[gantryId]-[laneId]
 */
case class SystemLaneCompletionStatus(lanes: Map[String, CompletionLaneStatus])

/**
 * Holds the completion status for a single lane
 *
 * @param lastUpdateTime The last time matching was started up or finished for this lane
 * @param completeUntil The time until which matching has been completed successfully for this corridor
 * @param lastVehicle The time of the last vehicle registration
 */
case class CompletionLaneStatus(systemId: String, gantryId: String, laneId: String,
                                lastUpdateTime: Long, completeUntil: Long, lastVehicle: Long)
