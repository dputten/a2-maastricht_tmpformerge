/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storage

import java.text.SimpleDateFormat
import java.util.Date

case class ZkModuleCompletionStatus(id: String, lastUpdate: Long, lastUpdateStr: String)

object ZkModuleCompletionStatus {
  val dateFormat = new SimpleDateFormat("dd/MM/yyyy'T'HH:mm:ss.SSS z")
  def apply(id: String, lastUpdate: Long): ZkModuleCompletionStatus = ZkModuleCompletionStatus(id, lastUpdate, dateFormat.format(new Date(lastUpdate)))
}

case class ZkMaintenanceModule(id: String, enable: Boolean)

case class ZkRabbitMQConnection(
  host: Option[String] = None,
  name: Option[String] = None,
  port: Option[Int] = None,
  vhost: Option[String] = None,
  user: Option[String] = None,
  password: Option[String] = None,
  exchangeName: Option[String] = None,
  msgKeyTemplate: Option[String] = None)

case class ZkApplicationInfo(id: String, systemType: String, version: String)

case class ZkBlackListEntry(systemId: String, moduleId: String)

case class ZkMaintenanceConfig(applInfo: ZkApplicationInfo, mq: ZkRabbitMQConnection, modules: Seq[ZkMaintenanceModule], frequency: Long, blackList: Seq[ZkBlackListEntry])
