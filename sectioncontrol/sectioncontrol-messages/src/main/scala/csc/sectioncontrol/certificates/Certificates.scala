/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.messages.certificates

/**
 * Location Certificate
 * @param id The Location Certificate
 * @param inspectionDate
 * @param validTime
 */
case class LocationCertificate(id: String, inspectionDate: Long, validTime: Long, components: List[ComponentCertificate])

/**
 * Class to indicate when which measurementType is installed for a given system.
 * @param typeId  The zookeeper key of the referenced MeasurementMethodType
 * @param timeFrom The time from which the MeasurementMethodType is installed on the system
 */
case class MeasurementMethodInstallation(typeId: String, timeFrom: Long)

/**
 * Class to indicate when which measurementType is installed for a given system.
 * @param timeFrom The time from which the MeasurementMethodType is installed on the system
 * @param measurementType The MeasurementMethodType installed on the system
 */
case class MeasurementMethodInstallationType(timeFrom: Long, measurementType: MeasurementMethodType)

/**
 * MeasurementMethodType is defined globally and is referenced by the MeasurementMethodInstallation.
 *
 * @param typeDesignation typeAanduiding
 * @param category  categorie aanduiding example: A
 * @param unitSpeed eenheid snelheid example: km/h
 * @param unitRedLight eenheid roodlicht example s
 * @param unitLength eenheid roodlicht example m
 * @param restrictiveConditions gebruiksbeperkende omstandigheden:
 * @param displayRange aanwijsBereik example: 20-250 km/h
 * @param temperatureRange temperatuur bereik example: temperatuurbereik -10 to 50 graden Celsius
 * @param permissibleError toelatbare fout: example toelatbare fout 3%
 * @param typeCertificate typeCertificaat
 */
case class MeasurementMethodType(typeDesignation: String,
                                 category: String,
                                 unitSpeed: String,
                                 unitRedLight: String,
                                 unitLength: String,
                                 restrictiveConditions: String,
                                 displayRange: String,
                                 temperatureRange: String,
                                 permissibleError: String,
                                 typeCertificate: TypeCertificate)
/**
 * Type certificate as it is provided by an external party.
 * Configuration has a own checksum container because it isn't a component and can change after startup.
 * There for the actual checksum is stored at a different path than the components
 * @param id unique name, for instance TP6830
 * @param revisionNr revision number
 * @param components certificates for components
 */
case class TypeCertificate(id: String, revisionNr: Int, components: List[ComponentCertificate])

/**
 * Checksum for one component
 * @param name Omschrijving van de (gecertificeerde) component. bv Matcher,
 * @param checksum Waarde van de checksum van de component
 */
case class ComponentCertificate(name: String, checksum: String)
/**
 * Component's certificate. It is generated as an SHA checksum from the JAR contents.
 * req 54: provide checksum (softwarezegel)
 */
case class ActiveCertificate(time: Long, //Datum en tijd van controle van de checksum in format
                             componentCertificate: ComponentCertificate)

