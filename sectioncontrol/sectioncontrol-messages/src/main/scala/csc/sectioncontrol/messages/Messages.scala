/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.messages

import java.util.{ Calendar, TimeZone }

import csc.sectioncontrol.storage.Direction

object VehicleCode extends Enumeration {
  val PA, // personenauto
  CA, // bestelauto
  CB, // kampeerauto
  CP, // kampeerauto (licht)
  MF, // motorfiets
  MV, // personenauto
  MH, // motorfiets met aanhanger
  AB, // autobus
  AT, // autobus
  VA, // vrachtauto
  AO, // motorvoertuig
  AS, // motorvoertuig
  BB, // ???
  BF, // ???
  BS // ???
  = Value
}

object ProcessingIndicator extends Enumeration {
  val Automatic, Manual, Mobi = Value
}

object EsaProviderType extends Enumeration {
  val Dynamax, Mtm, NoProvider, TubesProvider = Value
}

object SpeedIndicatorType extends Enumeration {
  val A1, A3 = Value
}

/**
 * Representation of an interval in time.
 *
 * @param from start of the interval
 * @param to end of the interval
 */
case class Interval(from: Long, to: Long)

/**
 * All properties needed for the mailserver
 *
 * @param properties for the mailserver
 * "properties":{"mail.smtp.socketFactory.class":"javax.net.ssl.SSLSocketFactor","mail.smtp.socketFactory.port":"465",
 * "mail.smtp.host":"","mail.smtp.starttls.enable":"false","mail.smtp.auth":"true","mail.from":"noreply@csc.com","mail.smtp.port":"587"}}
 */
case class MailServer(properties: Option[Map[String, String]])

/**
 * Case class used to represent leapseconds or changes of Daylights Savings Time
 * See sectioncontrol.enforce.timewatch.WatchTime.scala
 *
 * @param leapSecond
 * @param dstChange
 */
case class TimeChanges(leapSecond: Option[Interval], dstChange: Option[Interval])

/**
 * Representation of a Lane
 *
 * @param laneId the lane id
 * @param name the lane name
 * @param gantry the gantry id
 * @param system the system id
 * @param sensorGPS_longitude the lane location
 * @param sensorGPS_latitude the lane location
 */
case class Lane(laneId: String,
                name: String,
                gantry: String,
                system: String,
                sensorGPS_longitude: Double,
                sensorGPS_latitude: Double,
                bpsLaneId: Option[String] = None) {
  require(laneId != null)
  require(name != null)
  require(gantry != null)
  require(system != null)
  require(!laneId.trim().isEmpty)
  require(!name.trim().isEmpty)
  require(!gantry.trim().isEmpty)
  require(!system.trim().isEmpty)
}

/**
 * All value which are used with a confidence
 */
case class ValueWithConfidence[T <: AnyRef](value: T, confidence: Int = 0)

/**
 * Float value which are used with a confidence.
 * Separate from the generic ValueWithConfidence because lift-json does not deserialize
 * that correctly for AnyVal types.
 */
// TODO Find better serializer to remove this hack.
case class ValueWithConfidence_Float(value: Float, confidence: Int = 0)
case class ValueWithConfidence_VehicleCategory(value: VehicleCategory.Value, confidence: Int = 0)

object VehicleCategory extends Enumeration {
  val Pers = Value(1)
  val Large_Truck = Value(4)

  val Unknown = Value(6)
  val Motor = Value(10)
  val Car = Value(7)
  val Van = Value(11)
  val CarTrailer = Value(2)
  val Bus = Value(5)
  val Truck = Value(3)
  val TruckTrailer = Value(8)
  val SemiTrailer = Value(9)
}

/**
 * The different types of images
 */
object VehicleImageType extends Enumeration {
  type VehicleImageType = Value
  val Overview, OverviewMasked, OverviewRedLight, OverviewSpeed, License, RedLight, MeasureMethod2Speed = Value
}

/**
 *  collection of file formats supported by this application
 */
object ImageFormat extends Enumeration {
  type ImageFormat = Value
  val TIF_BAYER, TIF_RGB, TIF_GRAY, JPG_RGB, JPG_GRAY = Value
}

/**
 * Class represents an Image
 *
 * @param timestamp The creation time of the image
 * @param offset The offset of the image to sync with VRI
 * @param uri URI of name of the image
 * @param imageType Type of image
 * @param checksum The checksum of the image
 * @param timeYellow The yellow time of the image
 * @param timeRed The red time of the image
 * @param imageVersion version of the image
 */
case class VehicleImage(timestamp: Long, offset: Long, uri: String, imageType: VehicleImageType.Value,
                        checksum: String, timeYellow: Option[Long] = None, timeRed: Option[Long] = None,
                        imageVersion: Option[ImageVersion.Value] = None)

/**
 * Class used to store the VehicleRegistration metadata as JSON
 *
 * @param lane The lane Information
 * @param eventId The EventId
 * @param eventTimestamp  The time of registration
 * @param license the found license
 * @param length the found length
 * @param category the found category
 * @param speed the found speed
 * @param country the found country
 * @param images List of images
 * @param NMICertificate The NMI certificate at the time of the registration
 * @param applChecksum The application checksum at the time of the registration
 * @param serialNr The serial number of the measurement unit
 * @param licenseType the type of the license found by the recognizer
 * @param recognizedBy the recognizer who has supplied the results
 * @param version indicates the version of the structure, related to one value of MetadataVersion
 * @param licensePlateData will contain all the license plate fields, for version > 1
 * @param direction direction
 */
case class VehicleMetadata(
  lane: Lane,
  eventId: String,
  eventTimestamp: Long,
  eventTimestampStr: Option[String], //Only used for testing
  license: Option[ValueWithConfidence[String]] = None,
  rawLicense: Option[String] = None,
  length: Option[ValueWithConfidence_Float] = None,
  category: Option[ValueWithConfidence[String]] = None,
  speed: Option[ValueWithConfidence_Float] = None,
  country: Option[ValueWithConfidence[String]] = None,
  images: Seq[VehicleImage] = Seq(),
  NMICertificate: String,
  applChecksum: String,
  serialNr: String,
  licenseType: Option[String] = None,
  recognizedBy: Option[String] = None,
  version: Option[Int] = None,
  licensePlateData: Option[LicensePlateData] = None,
  direction: Option[Direction.Value] = None,
  interval1Ms: Option[Long] = None,
  interval2Ms: Option[Long] = None,
  jsonMessage: Option[String] = None) {

  val vehicleCategory = category.map(cat ⇒ {
    val vc = {
      try {
        VehicleCategory.withName(cat.value)
      } catch {
        case ex: Exception ⇒ VehicleCategory.Unknown
      }
    }
    new ValueWithConfidence[VehicleCategory.Value](
      vc,
      cat.confidence)
  })

  def getImage(imageType: VehicleImageType.Value): Option[VehicleImage] = {
    images.find(_.imageType.toString == imageType.toString)
  }

}
object VehicleMetadata {
  object Recognizer {
    val ARH = "ARH"
    val INTRADA = "INTRADA"
    val ICR_PLATEFINDER = "ICR"
  }

  object LicenseType {
    val PLATE_TYPE_UNKNOWN = "Unknown"
    val PLATE_TYPE_NORMAL = "Normal"
    val PLATE_TYPE_SQUARE = "Square"
    val PLATE_TYPE_SMALL = "Small"
  }
}

case class LicensePlateData(license: Option[ValueWithConfidence[String]] = None,
                            licensePosition: Option[LicensePosition] = None, //license position (absolute)
                            sourceImageType: Option[VehicleImageType.Value] = None,
                            rawLicense: Option[String] = None,
                            licenseType: Option[String] = None,
                            recognizedBy: Option[String] = None,
                            country: Option[ValueWithConfidence[String]] = None)

object MetadataVersion extends Enumeration {
  type MetadataVersion = Value
  val withLicensePlateData = Value(1)

}

/**
 * The different versions of an image
 */
object ImageVersion extends Enumeration {
  type ImageVersion = Value
  val Original, Decorated, ForDecoration, Snippet = Value

  def getImageVersionForString(string: String): Option[ImageVersion.Value] = {
    ImageVersion.values.find(value ⇒ value.toString.toUpperCase == string.toUpperCase.trim)
  }
}

/**
 * Position of a license plate in an image
 *
 * @param TLx Top left x coordinate
 * @param TLy          y
 * @param TRx Top right x coordinate
 * @param TRy           y
 * @param BLx Bottom left x coordinate
 * @param BLy             y
 * @param BRx Bottom right x coordinate
 * @param BRy              y
 */
case class LicensePosition(TLx: Int, TLy: Int, TRx: Int, TRy: Int, BLx: Int, BLy: Int, BRx: Int, BRy: Int)

/**
 * Two vehicle registrations matched on a corridor
 *
 * @param id ID of the entry
 * @param entry entry vehicle registration
 * @param exit  exit vehicle registration
 * @param corridorId  corridorId of the corridor
 * @param speed measured or detected speed
 * @param measurementSHA SHA of the JAR file when the speed was assigned
 */
case class VehicleSpeedRecord(id: String,
                              entry: VehicleMetadata,
                              exit: Option[VehicleMetadata],
                              corridorId: Int,
                              speed: Int,
                              measurementSHA: String) {
  require(id != null, "id is null")
  require(entry != null, "id is entry")
  require(exit != null, "id is exit")
  require(!id.trim.isEmpty, "id is empty")
  require(speed >= 0, "speed is negative")
  require(measurementSHA != null, "measurementSHA is null")

  def time: Long = if (exit.isDefined) exit.get.eventTimestamp else entry.eventTimestamp

  /**
   * Find the length with max confidence value (from entry, exit and externally provided values)
   */
  def length: Option[ValueWithConfidence_Float] = {
    val exitLength: Option[ValueWithConfidence_Float] = exit.flatMap(_.length)
    val all = List(exitLength, entry.length)
    val max = all.foldLeft[Option[ValueWithConfidence_Float]](None) {
      case (Some(accu), Some(len)) ⇒
        if (len.confidence > accu.confidence) Some(len) else Some(accu)
      case (None, len)  ⇒ len
      case (accu, None) ⇒ accu
    }
    max
  }

  /**
   * Get the Category which only exist on the exit
   */
  def category: Option[VehicleCategory.Value] = {
    exit.flatMap(_.vehicleCategory.map(_.value))
  }

  // Get the country code with the highest confidence level
  def country: Option[ValueWithConfidence[String]] = {
    (entry.country, exit.flatMap(_.country)) match {
      case (None, None)           ⇒ None
      case (Some(cc), None)       ⇒ Some(cc)
      case (None, Some(cc))       ⇒ Some(cc)
      case (Some(cc1), Some(cc2)) ⇒ if (cc1.confidence > cc2.confidence) Some(cc1) else Some(cc2)
    }
  }
}

/**
 * Vehicle registration which cannot be matched with another
 *
 * @param id the same ID as the one from vehicle registration
 * @param entry vehicle registration
 * @param corridorId corridor ID which was was used to to match
 * @param statsDuration the day to match
 * @param matcherRunEnd the end timestamp of the matcher job run
 */
case class VehicleUnmatchedRecord(id: String, entry: VehicleMetadata, corridorId: Int,
                                  statsDuration: StatsDuration, matcherRunEnd: Long)

/**
 * Case class for containing the reason why the classification has a indicator manual
 *
 * @param mask the mask of all possible errors
 */
case class IndicatorReason(mask: Int = 0) {
  private val LICENSE = 1
  private val CLASSIFICATION = 2
  private val COUNTRY = 4

  def isUnreliableLicense(): Boolean = {
    (mask & LICENSE) != 0
  }
  def isUnreliableClassification(): Boolean = {
    (mask & CLASSIFICATION) != 0
  }
  def isUnreliableCountry(): Boolean = {
    (mask & COUNTRY) != 0
  }

  /**
   * when the license is unreliable than country and classification
   * can't be sure either
   *
   * @return
   */
  def setUnreliableLicense(): IndicatorReason = {
    IndicatorReason(mask | LICENSE | CLASSIFICATION | COUNTRY)
  }
  def setUnreliableClassification(): IndicatorReason = {
    IndicatorReason(mask | CLASSIFICATION)
  }

  /**
   * when the country is unreliable than licence? and classification
   * can't be sure either
   *
   * @return
   */
  def setUnreliableCountry(): IndicatorReason = {
    IndicatorReason(mask = mask | LICENSE | CLASSIFICATION | COUNTRY)
  }
}

/**
 * Classified vehicle
 */
case class VehicleClassifiedRecord(id: String, speedRecord: VehicleSpeedRecord, code: Option[VehicleCode.Value],
                                   indicator: ProcessingIndicator.Value, mil: Boolean, invalidRdwData: Boolean,
                                   validLicensePlate: Option[String], manualAccordingToSpec: Boolean,
                                   reason: IndicatorReason, alternativeClassification: Option[VehicleCode.Value],
                                   dambord: Option[String]) {
  require(id != null, "id is null")
  require(speedRecord != null, "speedRecord is null")
  require(code != null, "code is null")
  require(indicator != null, "indicator is null")
  // manualAccordingToSpec can only be applied to Manual
  require(if (manualAccordingToSpec) indicator == ProcessingIndicator.Manual else true, "manualAccordingToSpec and not Manual indiactor")

  def time: Long = speedRecord.time

  def this(speedRecord: VehicleSpeedRecord,
           code: Option[VehicleCode.Value],
           indicator: ProcessingIndicator.Value,
           mil: Boolean,
           invalidRdwData: Boolean,
           validLicensePlate: Option[String] = None,
           manualAccordingToSpec: Boolean = false,
           reason: IndicatorReason,
           alternativeClassification: Option[VehicleCode.Value] = None,
           dambord: Option[String] = None) = this(speedRecord.id, speedRecord, code, indicator,
    mil, invalidRdwData, validLicensePlate, manualAccordingToSpec, reason, alternativeClassification, dambord)

  /**
   * Get a valid license plate (truncated if it exceeds a limit)
   */
  def licensePlate = validLicensePlate match {
    case None ⇒ speedRecord.entry.license match {
      case Some(confidenceValue) ⇒ confidenceValue.value
      case None                  ⇒ ""
    }
    case Some(v) ⇒ v
  }
}

/**
 * class containg the recognize results of the violation
 *
 * @param recognizeId     unique identifier
 * @param isCurrent is this result the current/final result
 * @param recognize the recognize results
 */
case class RecognizeRecord(recognizeId: String, isCurrent: Boolean, recognize: LicensePlateData)

/**
 * Violation record.
 *
 * @param id
 * @param classifiedRecord
 * @param enforcedSpeed
 */
case class VehicleViolationRecord(id: String,
                                  classifiedRecord: VehicleClassifiedRecord,
                                  enforcedSpeed: Int,
                                  dynamicSpeedLimit: Option[Int],
                                  recognizeResults: List[RecognizeRecord]) {
  require(id != null, "id must be non-null")
  require(classifiedRecord != null, "classifiedRecord must be non-null")
  require(enforcedSpeed > 0, "enforcedSpeed must be greater than 0")

  def addRecognizeResults(recognizeId: String, isCurrent: Boolean, recognize: LicensePlateData): VehicleViolationRecord = {
    val currentList = this.recognizeResults
    val newRecord = RecognizeRecord(recognizeId, isCurrent, recognize)
    val udatedList = if (isCurrent) {
      //reset current
      currentList.map(rec ⇒ rec.copy(isCurrent = false)) :+ newRecord
    } else {
      currentList :+ newRecord
    }
    this.copy(recognizeResults = udatedList)
  }
}

/**
 * Processed Violation record.
 */
case class ProcessedViolationRecord(id: String,
                                    violationRecord: VehicleViolationRecord,
                                    registered: Boolean,
                                    pardoned: Boolean,
                                    pardonReason: Option[String],
                                    violationJarSha: String) {
  require(id != null, "id must be non-null")
  require(violationRecord != null, "violationRecord must be non-null")
  require(pardonReason != null, "pardonReason must be non-null")
  require(violationJarSha != null, "violationJarSha must be non-null")
}

/**
 * Class used in validation-filter
 * WARNING: This is only used in validation-filter. in Enforce the first step is to strip all the data back to the
 * vehicleViolationRecord.classifiedRecord.
 * When information is also needed in the violation in enforce it has to be copied into the new violation. (for example:
 * vehicleViolationRecord.recognizeResult)
 * This is done in csc.sectioncontrol.enforce.nl.qualify.ClassifiedRecordsRetriever.extendViolationsWithConfirmedViolationInfo
 * @param timestamp
 * @param vehicleViolationRecord
 */
case class ViolationCandidateRecord(timestamp: Long, vehicleViolationRecord: VehicleViolationRecord) {

  def entryLicense = {
    val entry = vehicleViolationRecord.classifiedRecord.speedRecord.entry
    entry.license.map(_.value).getOrElse("")
  }

  def systemId = vehicleViolationRecord.classifiedRecord.speedRecord.entry.lane.system

}

/**
 * The key to identify a single day
 */
case class StatsDuration(key: String, timezoneID: String) {
  require(timezoneID != null, "timezoneID must be non-null")
  require(!timezoneID.trim.isEmpty, "timezoneID must be non-empty")
  require(key != null, "key must be non-null")
  require(!key.trim.isEmpty, "key must be non-empty")
  require(key.matches("20\\d{6}"), "The key must be in the format JJJJMMDD")
  private val timezone = TimeZone.getTimeZone(timezoneID)

  /**
   * Readable datum in format JJJJ-MM-DD (for instance 2012-04-30)
   */
  lazy val datum: String = {
    "%s-%s-%s".format(key.take(4), key.slice(4, 6), key.drop(6))
  }

  /**
   * UTC timestamp of the midnight 00:00:00 for the day
   */
  lazy val start: Long = {
    val cal = Calendar.getInstance(timezone)
    cal.clear()
    val year: Int = key.take(4).toInt
    cal.set(Calendar.YEAR, year)
    val month: Int = key.slice(4, 6).toInt - 1 //starts with 0
    cal.set(Calendar.MONTH, month)
    val day: Int = key.drop(6).toInt
    cal.set(Calendar.DAY_OF_MONTH, day)
    cal.set(Calendar.HOUR_OF_DAY, 0)
    cal.set(Calendar.MINUTE, 0)
    cal.set(Calendar.SECOND, 0)
    cal.set(Calendar.MILLISECOND, 0)
    cal.getTimeInMillis
  }

  lazy val tomorrow: StatsDuration = {
    val cal = Calendar.getInstance(timezone)
    cal.clear()
    cal.setTimeInMillis(start)
    cal.set(Calendar.HOUR_OF_DAY, 24) // this is tomorrow
    cal.set(Calendar.MINUTE, 0)
    cal.set(Calendar.SECOND, 0)
    cal.set(Calendar.MILLISECOND, 0)
    StatsDuration(StatsDuration.format.format(cal.getTimeInMillis), timezoneID)
  }

  /**
   * Returns true if the timestamp falls within the day limits.
   * Midnight 00:00:00 is inclusive, midnight 24:00:00 is exclusive (because it belongs already to the next day)
   */
  def isWithin(timestamp: Long): Boolean = {
    timestamp >= start && timestamp < tomorrow.start
  }
}

object StatsDuration {
  val format = new java.text.SimpleDateFormat("yyyyMMdd")

  def apply(timestamp: Long, timezone: TimeZone): StatsDuration = {
    val cal = Calendar.getInstance(timezone)
    cal.clear()
    cal.setTimeInMillis(timestamp)
    cal.set(Calendar.HOUR_OF_DAY, 0)
    cal.set(Calendar.MINUTE, 0)
    cal.set(Calendar.SECOND, 0)
    cal.set(Calendar.MILLISECOND, 0)
    StatsDuration(format.format(cal.getTimeInMillis), timezone.getID)
  }

  def apply(key: String, timezone: TimeZone): StatsDuration = {
    StatsDuration(key, timezone.getID)
  }

  /**
   * for testing only
   */
  def local(key: String): StatsDuration = StatsDuration(key, TimeZone.getDefault.getID)

  def today(timezone: TimeZone, time: Long = System.currentTimeMillis()): StatsDuration = {
    apply(time, timezone)
  }

  def yesterday(timezone: TimeZone, time: Long = System.currentTimeMillis()): StatsDuration = {
    val day = today(timezone, time)
    apply(day.start - 1, timezone)
  }
}

