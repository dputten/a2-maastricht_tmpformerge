/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.messages

import csc.dlog.Event

/*
 * used by SystemEvent to indicate if a request for an action required one or more requests/events
 * @param token
 * @param last
 */
case class SystemEventToken(token: String, last: Boolean)

/**
 * Used to request the backend for an action
 *
 * @param componentId The component initiating the event
 * @param timestamp The time of the event
 * @param systemId The system id for the event
 * @param corridorId The corridor id for the event when it refers to a corridor
 * @param path a path when necessary indicating path object of the Event
 * @param eventType The type of event
 * @param userId The user generating the event, or 'system' if the event is initiated by the system without
 *               direct user action
 * @param reason The reason for the event
 * @param token The event token
 * @param startTimestamp The optional startTimestamp of an event (used for events in the passed. timestamp will then contain endTimestamp)
 */
case class SystemEvent(componentId: String,
                       timestamp: Long,
                       systemId: String,
                       eventType: String,
                       userId: String,
                       reason: String,
                       token: Option[SystemEventToken],
                       startTimestamp: Option[Long],
                       path: Option[String],
                       corridorId: Option[String],
                       gantryId: Option[String],
                       laneId: Option[String]) extends Event
object SystemEvent {
  def apply(componentId: String,
            timestamp: Long,
            systemId: String,
            eventType: String,
            userId: String,
            reason: Option[String] = None,
            token: Option[SystemEventToken] = None,
            startTimestamp: Option[Long] = None,
            path: Option[String] = None,
            corridorId: Option[String] = None,
            gantryId: Option[String] = None,
            laneId: Option[String] = None) =
    new SystemEvent(componentId, timestamp, systemId, eventType, userId, reason.getOrElse(""), token, startTimestamp, path, corridorId, gantryId, laneId)

  def apply(componentId: String,
            timestamp: Long,
            systemId: String,
            eventType: String,
            userId: String,
            reason: Option[String]) =
    new SystemEvent(componentId, timestamp, systemId, eventType, userId, reason.getOrElse(""), None, None, None, None, None, None)

  def apply(componentId: String,
            timestamp: Long,
            systemId: String,
            eventType: String,
            userId: String) =
    new SystemEvent(componentId, timestamp, systemId, eventType, userId, "", None, None, None, None, None, None)

  def apply(componentId: String,
            timestamp: Long,
            systemId: String,
            eventType: String,
            userId: String,
            corridorId: Option[String],
            gantryId: Option[String],
            laneId: Option[String]) =
    new SystemEvent(componentId, timestamp, systemId, eventType, userId, "", None, None, None, corridorId, gantryId, laneId)

  def apply(componentId: String,
            timestamp: Long,
            systemId: String,
            eventType: String,
            userId: String,
            reason: Option[String],
            corridorId: Option[String],
            gantryId: Option[String],
            laneId: Option[String]) =
    new SystemEvent(componentId, timestamp, systemId, eventType, userId, reason.getOrElse(""), None, None, None, corridorId, gantryId, laneId)
}
