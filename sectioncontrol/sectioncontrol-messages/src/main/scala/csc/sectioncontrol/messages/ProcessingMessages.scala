package csc.sectioncontrol.messages

/**
 * A processing history entry.
 *
 * @param time the time of the state change
 * @param component the component on which the state has changed
 * @param state the new state
 * @param notes notes, remarks
 */
case class ProcessingHistoryEntry(time: Long, component: String, state: String, notes: String)

/**
 * Reference to a record in a table. The name of the table and a key should unambiguously identify a record.
 * @param tableName the name of the table
 * @param key the key of the record
 */
case class ProcessingRecordReference(tableName: String, key: String)

/**
 * A message processing record.
 *
 * @param component the processing component
 * @param state the current state of the processing
 * @param time the time of the state last changed
 * @param history a history of state changes
 * @param references references to records being involved in the processing
 */
//case class ProcessingRecord(component: String, state: String, time: Long, vehicleRegistrationTime: Long, history: Seq[ProcessingHistoryEntry],
//                            references: Seq[ProcessingRecordReference] = Seq())
case class ProcessingRecord(component: String, state: String, time: Long, history: Seq[ProcessingHistoryEntry],
                            references: Seq[ProcessingRecordReference] = Seq())
/**
 * A factory for special processing record.
 *
 * Note, it's potentially dangerous to extend a case class. Therefore the factory method.
 *
 */
object GantryTimeProcessingRecord {
  val ComponentName = "GantryProcessedTime"
  val State = "KEEPALIVE"
  // Todo:JE->remove first create
  def create(lane: Lane, time: Long): ProcessingRecord = ProcessingRecord(ComponentName, State, time,
    Seq(ProcessingHistoryEntry(time, ComponentName, State, lane.laneId)))
  //  def create(lane: Lane, time: Long, vehicleRegistrationTime: Long): ProcessingRecord = ProcessingRecord(ComponentName, State, time, vehicleRegistrationTime,
  //    Seq(ProcessingHistoryEntry(time, ComponentName, State, lane.laneId)))
}

trait ProcessingState {
  val READY = "READY"
  val FINISHED = "FINISHED"
}

object BasicProcessingState extends ProcessingState