/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.messages.matcher

import csc.sectioncontrol.messages.{ Lane, StatsDuration }

/**
 * Corridor data for matcher
 * @param systemId unique system ID
 * @param id unique corridor ID for the system
 * @param entry  entry gantry ID for entry
 * @param exit  gantry ID for exit
 * @param distance distance between entry and exit in meters
 * @param deltaForDoubleShoot max time between two shoots which can be recognised as one auto between two lanes
 * @param followDistance The timeout (in msec) used in deciding if the matcher
 */
case class MatcherCorridorConfig(systemId: String, zkCorridorId: String, id: Int,
                                 entry: String, exit: String, distance: Int,
                                 deltaForDoubleShoot: Int, followDistance: Long)

/**
 * An indicator to begin matching
 * @param stats day ID
 * @param end  UTC timestamp (in milliseconds) to begin with (exclusive)
 * @param begin UTC timestamp (in milliseconds) to begin with (inclusive). When the value is not provided,
 *              use the last end stored in ZooKeeper.
 */
case class MatchingMessage(stats: StatsDuration, end: Long, begin: Option[Long] = None)

case object GetLastTimesMessage

case class SetLastTimesMessage(message: MatchingMessage)

case class SetNextTimesMessage(message: MatchingMessage)

case object GetLeftoverMessage

case object CleanLeftoverMessage

