package csc.sectioncontrol.fileTransfer

import java.io.File

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 11/24/14.
 */

case class SendFile(file: File)
