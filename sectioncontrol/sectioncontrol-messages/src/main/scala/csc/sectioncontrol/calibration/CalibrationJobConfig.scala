/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.calibration

/**
 *
 * @param sessionTimeout Max time to wait to complete a calibration
 * @param iRoseTimeOut Max time of ssh connection to iRose
 * @param iRoseNextRetry Time to wait before retrying sending calibration command to iRose after failure
 * @param iRoseMessage Message used to check the calibration command has successfully executed
 * @param matcherTimeout Max time to match the input with the output form the iRose
 * @param matcherNextRetry Time to wait before retrying to match the input with the output from the iRose
 * @param matchInterval Time interval within an output can be matched with an input from the iRose
 * @param waitingBeforeResend the max time to wait before a new calibration request is send for a specific lane
 * @param simulationMode Optional value used to calibrate a system in simulation mode. No communication to irose is established
 */
case class CalibrationJobConfig(sessionTimeout: Long,
                                iRoseTimeOut: Long,
                                iRoseNextRetry: Long,
                                iRoseMessage: String,
                                iRoseSpeedMargin: Deviation,
                                iRoseLengthMargin: Deviation,
                                matcherTimeout: Long,
                                matcherNextRetry: Long,
                                matchInterval: Long,
                                waitingBeforeResend: Long,
                                simulationMode: Option[CalibrationSimulationMode] = None)

/**
 * Used to calibrate a system in simulation mode. No communication to irose is established
 * @param simulate to simulate a calibration
 * @param waitTime time to wait before a calibration is done
 */
case class CalibrationSimulationMode(simulate: Boolean, waitTime: Long)

/**
 * Deviation class with a lower and upper limit
 * @param lower lower bound
 * @param upper upper bound
 */
case class Deviation(lower: Int, upper: Int)