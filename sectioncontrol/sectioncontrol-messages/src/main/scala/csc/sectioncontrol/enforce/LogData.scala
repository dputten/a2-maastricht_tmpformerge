/*
* Copyright (C) 2012 CSC. <http://www.csc.com>
*/
package csc.sectioncontrol.enforce

/**
 * Defines requirements on the data we need from the system event bus to be able to construct
 * the ExcludeLog
 *
 * @author Maarten Hazewinkel
 */
trait SystemEventData {
  /**
   * Id of the event source
   */
  def componentId: String

  /**
   * Effective time for the event. May differ from when it was actually recorded.
   * For instance, a failed calibration test has an effective time of the previous
   * calibration test to account for the invalidity of violations since the previous test.
   */
  def effectiveTimestamp: Long

  /**
   * Set if the event applies only for a single Corridor. If set to None, the event applies
   * to all Corridors for this System.
   */
  def corridorId: Option[Int]

  /**
   * sequenceNumber is generated per event sourceId. This is used if multiple events
   * from a single sourceId have the same effectiveTimestamp. In that case the
   * sequenceNumber provides the relative order.
   */
  def sequenceNumber: Long

  /**
   * Is this event a suspend or resume event?
   */
  def isSuspendOrResumeEvent: Boolean

  /**
   * Should enforcement remain suspended after this event?
   */
  def suspend: Boolean

  /**
   * Reason for the suspension of enforcement. Required if suspend==true
   */
  def suspensionReason: Option[String]
}

/**
 * Defines requirements on the data we need from the system event bus to be able to construct
 * the SpeedLog
 *
 * @author Maarten Hazewinkel
 */
trait SpeedEventData {
  /**
   * Effective time for the event. May differ from when it was actually recorded.
   * For instance, a failed calibration test has an effective time of the previous
   * calibration test to account for the invalidity of violations since the previous test.
   */
  def effectiveTimestamp: Long

  /**
   * sequenceNumber is generated per event sourceId. This is used if multiple events
   * for a single systemId+corridorId have the same effectiveTimestamp. In that case the
   * sequenceNumber provides the relative order.
   */
  def sequenceNumber: Long

  /**
   * The corridor to which this speed event applies.
   */
  def corridorId: Int

  /**
   * The new speed setting. Either a status code (which prevents enforcement) or a value which
   * is the speed to be used for enforcement.
   */
  def speedSetting: Either[SpeedIndicatorStatus.Value, Option[Int]]

  /**
   * The time preceeding the event that needs to be excluded from enforcement in Milliseconds
   * Used by DynaMax link.
   */
  def preChangeExclusionTime: Option[Long] = None
}

object SpeedIndicatorStatus extends Enumeration {
  val StandBy, Undetermined = Value
}

