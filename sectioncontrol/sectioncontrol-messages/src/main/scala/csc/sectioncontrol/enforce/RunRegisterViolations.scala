/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.nl.run

case class RegisterViolationsJobConfig(exportDirectoryPath: String)

