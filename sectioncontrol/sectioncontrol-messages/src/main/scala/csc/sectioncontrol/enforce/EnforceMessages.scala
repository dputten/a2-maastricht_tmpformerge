package csc.sectioncontrol.enforce

import csc.sectioncontrol.messages.StatsDuration

/**
 * Statistics for the exported set of violations
 *
 * These values are for the following fields in the daily report:
 * - overtredingenType/auto
 * - overtredingenType/hand
 * - overtredingenType/mobi
 * - overtredingenType/mobi-niet-verwerken
 * - overtredingenType/MTM-pardon
 * - overtredingenType/Dubbele-overtredingen-pardon
 * - overtredingenType/Overig-pardon
 *
 * @param corridorId corridor ID
 * @param auto Number of violations in Auto
 * @param manual Number of violation in Manual
 * @param mobi Number of violations in Mobi
 * @param mobiDoNotProcess Number of violations in Mobi with status code 'A'-'Z'
 * @param mtmPardon Number of pardons due to MTM filtering
 * @param doublePardon Number of pardons due to filtering for double violations
 * @param otherPardon Number of pardons due to other reasons
 * @param manualAccordingToSpec Number of manual violations according to specification
 */
case class ExportViolationsStatistics(corridorId: Int,
                                      speedLimit: Int,
                                      auto: Int,
                                      manual: Int,
                                      mobi: Int,
                                      mobiDoNotProcess: Int,
                                      mtmPardon: Int,
                                      doublePardon: Int,
                                      otherPardon: Int,
                                      manualAccordingToSpec: Int) {

  def total: Int = auto + manual + mobi

  /**
   * Add an instance to self
   *
   * @param b the instance to add
   * @return new instance containing the of self and b
   */
  def +(that: ExportViolationsStatistics): ExportViolationsStatistics = {
    val newCorridorId = if (corridorId == that.corridorId) corridorId else -1
    val newSpeedLimit = speedLimit max that.speedLimit

    ExportViolationsStatistics(newCorridorId, newSpeedLimit, auto + that.auto, manual + that.manual,
      mobi + that.mobi, mobiDoNotProcess + that.mobiDoNotProcess, mtmPardon + that.mtmPardon,
      doublePardon + that.doublePardon, otherPardon + that.otherPardon,
      manualAccordingToSpec + that.manualAccordingToSpec)
  }
}

object ExportViolationsStatistics {
  def apply() = new ExportViolationsStatistics(-1, 0, 0, 0, 0, 0, 0, 0, 0, 0)

  def sum(stats: Seq[ExportViolationsStatistics]): ExportViolationsStatistics = {
    stats.reduce((s1, s2) ⇒ s1 + s2)
  }
}

/**
 * Statistics for all the corridors for the day
 *
 * @param systemId system ID
 * @param stats day report ID
 * @param corridorData  corridor statistics
 */
case class DayViolationsStatistics(systemId: String, stats: StatsDuration, corridorData: Seq[ExportViolationsStatistics])

case class MtmSpeedSetting(time: Long, location: String, setting: String)

case class MtmSpeedSettings(speedSettings: List[MtmSpeedSetting], statsKey: StatsDuration)

case class PossibleDefectMSI(os_comm_id: Int, msi_rijstr_pos: String, os_locatie: String)

case class MsiBlankEvent(expectedTimeFrom: Long, msiBlanks: List[PossibleDefectMSI], statsKey: StatsDuration)
