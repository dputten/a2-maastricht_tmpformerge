/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol

case class Source(systemId: String, corridorId: Option[String] = None, gantryId: Option[String] = None, laneId: Option[String] = None)
case class ErrorRecord(errorType: String, detail: String)
case class ProcessError(source: Source, errors: Seq[ErrorRecord], processMsg: AnyRef)