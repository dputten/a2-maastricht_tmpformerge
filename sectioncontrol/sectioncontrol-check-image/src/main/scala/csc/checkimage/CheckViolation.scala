/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.checkimage

import akka.actor.{ Actor, ActorLogging, ActorRef }
import akka.event.LoggingReceive
import csc.akkautils.DirectLogging
import csc.sectioncontrol.enforce.common.nl.violations.Violation
import csc.sectioncontrol.enforce.violations.GermanLicenseFormatter
import csc.sectioncontrol.messages.{ ProcessingIndicator, ValueWithConfidence, VehicleMetadata }

/**
 * Request to check a violation
 * @param reference a reference
 * @param violation the violation to check
 */
case class ViolationImageCheckRequest(reference: String, violation: Violation)

/**
 * The response of a ViolationImageCheckRequest
 * @param reference the reference given in the request
 * @param violation the requested violation
 * @param result the result of the check
 */
case class ViolationImageCheckResponse(reference: String, violation: Violation, result: ViolationCheckResult)

/**
 * The data to keep track of the requests
 * @param request the received request
 * @param applicant who made the request
 * @param startTime the time the request was received
 * @param result the result of the request
 */
case class ImageCheckRequestData(request: ViolationImageCheckRequest, applicant: ActorRef, startTime: Long, result: ViolationCheckResult)

/**
 * The result of the violation check
 * @param entry the result of the check of the entry
 * @param exit the result of the check of the exit
 */
case class ViolationCheckResult(entry: Option[ImageCheckResult], exit: Option[ImageCheckResult]) extends DirectLogging {
  /**
   * The joined result of the violation check
   */
  val summaryCheck = createSummary()

  /**
   * Join the two results into one
   * @return joined result
   */
  def createSummary(): ImageCheckResult = {
    // Summarize both results
    // If entry doesn't exits than there is an error. should never happen

    // Get the error indication of the entry Image (license, country, no-read)
    val entryResult = entry.map(_.getErrors()).getOrElse(ImageCheckResult.errorCheck)

    //it is possible that the exit doesn't exits
    // Get the error indication of the exit Image (license, country, no-read)
    val exitResult = exit.map(_.getErrors()).getOrElse(0)

    // Join the error results together.
    val summarize = new ImageCheckResult(entryResult | exitResult)

    if (!summarize.hasLicenseCheckFailed()) {
      // No problems with the license. Check license if entry == exit
      val licEntry = entry.flatMap(_.getRecognitionResults().headOption).flatMap(_.license).map(_.value)
      val licExit = exit.flatMap(_.getRecognitionResults().headOption).flatMap(_.license).map(_.value)

      if (licExit.isDefined && licEntry.getOrElse("") != licExit.getOrElse("")) {
        log.info("Entry license (%s) not equal exit license (%s)".format(licEntry, licExit))
        summarize.setErrorLicenseCheck(true)
      }
    }

    if (!summarize.hasCountryCheckFailed()) {
      //check country if entry == exit
      val countryEntry = entry.flatMap(_.getRecognitionResults().headOption).flatMap(_.country).map(_.value)
      val countryExit = exit.flatMap(_.getRecognitionResults().headOption).flatMap(_.country).map(_.value)
      if (countryEntry.isDefined && countryExit.isDefined && (countryEntry.getOrElse("") != countryExit.getOrElse(""))) {
        summarize.setErrorCountryCheck(true)
      }
    }

    //check format OK
    if (!summarize.hasLicenseCheckFailed()) {
      val countryEntry = entry.flatMap(_.getRecognitionResults().headOption).flatMap(_.country).map(_.value)
      val countryExit = exit.flatMap(_.getRecognitionResults().headOption).flatMap(_.country).map(_.value)

      val originalCountry = countryEntry.orElse(countryExit)
      val licEntry = entry.flatMap(_.getRecognitionResults().headOption).flatMap(_.license).map(_.value)
      if (originalCountry == Some("DE") && !GermanLicenseFormatter.validate(licEntry.getOrElse(""))) {
        summarize.setErrorLicenseCheck(true)
        summarize.setErrorLicenseFormatCheck(true)
      }
    }

    summarize
  }

  def createSummaryWithIntrada2(recognizerCountries: Seq[String],
                                autoThresholdLevel: Int,
                                mobiThresholdLevel: Int): ImageCheckResult = {
    // Log each recognition result
    // Get the entry Recognition results
    log.info(" ==== Start Summarize Recognition results Intrada2 ====")

    var entryRecognitionResults = entry.map(_.getRecognitionResults())
    log.info("EntryResults: %s".format(entryRecognitionResults.toString()))

    // Get the Exit Recognition results
    val exitRecognitionResults = exit.map(_.getRecognitionResults())
    log.info("ExitResults: %s".format(exitRecognitionResults.toString()))

    // Get the ARH result for verification with the Intrada result.
    val entryARH: Option[RecognitionResult] = entryRecognitionResults.flatMap(_.find(_.recognizerId == "ARH_ENTRY"))
    val exitARH: Option[RecognitionResult] = exitRecognitionResults.flatMap(_.find(_.recognizerId == "ARH_EXIT"))

    // Get the final Intrada result
    val entryINTRADA: Option[RecognitionResult] = entryRecognitionResults.flatMap(_.find(_.recognizerId == "INTRADA_INTERFACE_2"))
    val exitINTRADA: Option[RecognitionResult] = exitRecognitionResults.flatMap(_.find(_.recognizerId == "INTRADA_INTERFACE_2"))

    // The ARH licenses are equal (otherwise no match). Get only the entry one
    val (entryARHLicense, entryARHLicConf) = entryARH match {
      case Some(RecognitionResult(_, Some(ValueWithConfidence(value, confidence)), _, _, _)) ⇒
        (value, confidence)

      case _ ⇒ ("", 0)
    }

    val (exitARHLicense, exitARHLicConf) = exitARH match {
      case Some(RecognitionResult(_, Some(ValueWithConfidence(value, confidence)), _, _, _)) ⇒
        (value, confidence)

      case _ ⇒ ("", 0)
    }

    val (entryARHCountry, entryARHCountryConf) = entryARH match {
      case Some(RecognitionResult(_, _, _, Some(ValueWithConfidence(value, confidence)), _)) ⇒
        (value, confidence)

      case _ ⇒ ("", 0)
    }

    val (exitARHCountry, exitARHCountryConf) = exitARH match {
      case Some(RecognitionResult(_, _, _, Some(ValueWithConfidence(value, confidence)), _)) ⇒
        (value, confidence)

      case _ ⇒ ("", 0)
    }

    // The Intrada licenses are equal too. The same result was put in entry en exit.
    val (intradaLicense, intradaConfidence) = entryINTRADA match {
      case Some(RecognitionResult(_, Some(ValueWithConfidence(value, confidence)), _, _, _)) ⇒
        (value, confidence)

      case _ ⇒ ("", 0)
    }

    // Get the final country code
    val (intradaCountry, intradaCountryConf) = entryINTRADA match {
      case Some(RecognitionResult(_, _, _, Some(ValueWithConfidence(value, confidence)), _)) ⇒
        (value, confidence)

      case _ ⇒ ("", 0)
    }

    log.info("EntryARH1   : %s (%d) %s (%d)".format(entryARHLicense, entryARHLicConf, entryARHCountry, entryARHCountryConf))
    log.info("ExitARH1    : %s (%d) %s (%d)".format(exitARHLicense, exitARHLicConf, exitARHCountry, exitARHCountryConf))
    log.info("Intrada     : %s (%d) %s (%d)".format(intradaLicense, intradaConfidence, intradaCountry, intradaCountryConf))

    // Join the error results together.
    val summarize = new ImageCheckResult(0)

    // Intrada confidence level 1-10 are special
    if (intradaConfidence <= 10) {
      intradaConfidence match {
        case 1 ⇒
          log.info("Intrada reads two different licenses. Possible read error ARH. ErrorMatchingCheck")
          summarize.setErrorMatchingCheck(true)

        case _ ⇒
          log.info("Intrada returns a not defined confidence level (%d). Remove the violation.".format(intradaConfidence))
          summarize.setErrorMatchingCheck(true)
      }
    } else {
      /*
       The intrada license must be the same as the ARH license, otherwise this is not a violation.
       For time being this violation is sent to manual processing.
       May be in future the violation has to be processed again by classify and 'detect violation'
       to figure out if this is still a violation.
       */
      if (!intradaLicense.equals(entryARHLicense)) {
        log.info("Intrada (%s) not equal ARH (%s). Send the violation to the manual processing".format(intradaLicense, entryARHLicense))
        summarize.setErrorLicenseCheck(true)
        summarize.setErrorCountryCheck(true)
      } else {
        /*
          Is the confidence level high enough for the thresholds
         */
        if (recognizerCountries.contains(intradaCountry)) {
          if (intradaConfidence < autoThresholdLevel) {
            log.info("Contract Country - Confidence level %d is less than AutoThresHold %d. Send to Manual Processing".format(intradaConfidence, autoThresholdLevel))
            summarize.setErrorLicenseCheck(true)
            summarize.setErrorCountryCheck(true)
          }
        } else {
          if (intradaConfidence < mobiThresholdLevel) {
            log.info("Mobi Country - Confidence level %d is less than MobiThresHold %d. Send to Manual Processing".format(intradaConfidence, mobiThresholdLevel))
            summarize.setErrorLicenseCheck(true)
            summarize.setErrorCountryCheck(true)
          }
        }

        if (!summarize.hasErrors()) {
          /*
           Everything looks fine, but there can be an issue with the country code.
           In classify and during creating the ViolationData the Violation Country code will be the country
           code that have the highest ARH CountryCodeConfidenceLevel! On the basis of this country code classify decides the matching-pair
           gets the Processing Indication automatic (contract country) or mobi (non contract country [all other countries]).
           The country code with the highest ARH confidence level is also put in the final violation file (zaakbestand)

           When the Intrada Country code is not equal to the ARH Country code with highest confidence level, there could be an issue.
           A contract country could end up in the mobi processing or even worse a non contract country ends up in automatic!

           */

          // Get the country code with the highest confidence level
          val winningCountryCode = if (entryARHCountryConf > exitARHCountryConf)
            entryARHCountry
          else
            exitARHCountry

          // Get the country code with the highest confidence level
          val losingCountryCode = if (entryARHCountryConf < exitARHCountryConf)
            entryARHCountry
          else
            exitARHCountry

          if (intradaCountry.equals(winningCountryCode)) {
            // No worries
            log.info("Intrada has the same results as ARH.")
            summarize.clearErrors()
          } else {
            if (intradaCountry.equals(losingCountryCode)) {
              // Was it a contract country?
              if (recognizerCountries.contains(winningCountryCode)) {
                // Yes. Still contract country?
                if (recognizerCountries.contains(intradaCountry)) {
                  // No worries about Processing Indication. Only change the final country code
                  log.info("Change Country code from (%s) to (%s) because of Intrada.".format(winningCountryCode, losingCountryCode))

                  summarize.clearErrors()
                  summarize.setCountryChange(true)
                } else {
                  // No contract country anymore. Change final country code and processing indication
                  log.info("Change Country code from (%s) to (%s) because of Intrada. Also the processing indication change to Mobi.".format(winningCountryCode, losingCountryCode))
                  summarize.clearErrors()
                  summarize.setCountryChange(true)
                  summarize.setChangeToMobi(true)
                }
              } else {
                // Original no contract country (a mobi country). Is it now?
                if (recognizerCountries.contains(intradaCountry)) {
                  // Becomes a contract country. Change the final country code and processing indication
                  log.info("Change Country code from (%s) to (%s) because of Intrada. Also the processing indication change to Auto.".format(winningCountryCode, losingCountryCode))
                  summarize.clearErrors()
                  summarize.setCountryChange(true)
                  summarize.setChangeToAuto(true)
                } else {
                  // Still a non contract country. Change only final country code
                  log.info("Change Country code from (%s) to (%s) because of Intrada.".format(winningCountryCode, losingCountryCode))

                  summarize.clearErrors()
                  summarize.setCountryChange(true)
                }

              }
            } else {
              log.info("Intrada country (%s) not equal to ARH Highest (%s) and ARH lowest (%s)".format(intradaCountry, winningCountryCode, losingCountryCode))
              summarize.setErrorCountryCheck(true)
            }
          }
        }
      }
    }

    //check format OK
    if (!summarize.hasLicenseCheckFailed()) {
      if (intradaCountry == "DE" && !GermanLicenseFormatter.validate(intradaLicense)) {
        log.info("German license not formatted well [%s]. LicenseError".format(entryARHLicense))
        summarize.setErrorLicenseCheck(true)
      }
    }

    log.info(" ==== End Summarize Recognition results Intrada2 ====")

    // Return value
    summarize
  }
  /**
   * Join the two results into one
   * @return joined result
   */
  def createSummaryWithIntrada(recognizerCountries: Seq[String],
                               minLowestLicConfLevel: Int,
                               minLicConfLevelRecognizer2: Int): ImageCheckResult = {
    // Log each recognition result
    // Get the entry Recognition results

    log.info(" ==== Start Summarize Recognition results ====")
    var entryRecognitionResults = entry.map(_.getRecognitionResults())
    log.debug("EntryResults: %s".format(entryRecognitionResults.toString()))

    // Get the Exit Recognition results
    val exitRecognitionResults = exit.map(_.getRecognitionResults())
    log.debug("ExitResults: %s".format(exitRecognitionResults.toString()))

    val entryARH1: Option[RecognitionResult] = entryRecognitionResults.flatMap(_.find(_.recognizerId == "ARH1"))
    val exitARH1: Option[RecognitionResult] = exitRecognitionResults.flatMap(_.find(_.recognizerId == "ARH1"))
    val entryINTRADA: Option[RecognitionResult] = entryRecognitionResults.flatMap(_.find(_.recognizerId == "INTRADA"))
    val exitINTRADA: Option[RecognitionResult] = exitRecognitionResults.flatMap(_.find(_.recognizerId == "INTRADA"))

    log.debug("EntryLicARH1: %s".format(entryARH1.toString()))
    log.debug("ExitLicARH1: %s".format(exitARH1.toString()))
    log.debug("entryINTRADA: %s".format(entryINTRADA.toString()))
    log.debug("exitINTRADA: %s".format(exitINTRADA.toString()))

    val (entryARH1License, entryARH1LicConf) = entryARH1 match {
      case Some(RecognitionResult(_, Some(ValueWithConfidence(value, confidence)), _, _, _)) ⇒
        (value, confidence)

      case _ ⇒ ("", 0)
    }

    val (exitARH1License, exitARH1LicConf) = exitARH1 match {
      case Some(RecognitionResult(_, Some(ValueWithConfidence(value, confidence)), _, _, _)) ⇒
        (value, confidence)

      case _ ⇒ ("", 0)
    }

    val (entryIntradaLicense, entryIntradaLicConf) = entryINTRADA match {
      case Some(RecognitionResult(_, Some(ValueWithConfidence(value, confidence)), _, _, _)) ⇒
        (value, confidence)

      case _ ⇒ ("", 0)
    }

    val (exitIntradaLicense, exitIntradaLicConf) = exitINTRADA match {
      case Some(RecognitionResult(_, Some(ValueWithConfidence(value, confidence)), _, _, _)) ⇒
        (value, confidence)

      case _ ⇒ ("", 0)
    }

    val (entryARH1Country, entryARH1CountryConf) = entryARH1 match {
      case Some(RecognitionResult(_, _, _, Some(ValueWithConfidence(value, confidence)), _)) ⇒
        (value, confidence)

      case _ ⇒ ("", 0)
    }

    val (exitARH1Country, exitARH1CountryConf) = exitARH1 match {
      case Some(RecognitionResult(_, _, _, Some(ValueWithConfidence(value, confidence)), _)) ⇒
        (value, confidence)

      case _ ⇒ ("", 0)
    }

    val (entryIntradaCountry, entryIntradaCountryConf) = entryINTRADA match {
      case Some(RecognitionResult(_, _, _, Some(ValueWithConfidence(value, confidence)), _)) ⇒
        (value, confidence)

      case _ ⇒ ("", 0)
    }

    val (exitIntradaCountry, exitIntradaCountryConf) = exitINTRADA match {
      case Some(RecognitionResult(_, _, _, Some(ValueWithConfidence(value, confidence)), _)) ⇒
        (value, confidence)

      case _ ⇒ ("", 0)
    }

    log.info("EntryARH1   : %s (%d) %s (%d)".format(entryARH1License, entryARH1LicConf, entryARH1Country, entryARH1CountryConf))
    log.info("ExitARH1    : %s (%d) %s (%d)".format(exitARH1License, exitARH1LicConf, exitARH1Country, exitARH1CountryConf))
    log.info("EntryIntrada: %s (%d) %s (%d)".format(entryIntradaLicense, entryIntradaLicConf, entryIntradaCountry, entryIntradaCountryConf))
    log.info("ExitIntrada : %s (%d) %s (%d)".format(exitIntradaLicense, exitIntradaLicConf, exitIntradaCountry, exitIntradaCountryConf))

    val minIntradaConfLevel = minLicConfLevelRecognizer2
    val minARHConfLevel = minLowestLicConfLevel

    // Join the error results together.
    val summarize = new ImageCheckResult(0)

    if ((!entryIntradaLicense.isEmpty() && !exitIntradaLicense.isEmpty()) && (entryIntradaLicense == exitIntradaLicense)) {
      /*
       Intrada reads the same license on entry and exit.
       Is it the same license ARH read?
       */
      log.info("Intrada license are the same")
      if (entryIntradaLicense == entryARH1License) {
        /*
         Looks 4 same license plates.
         Verify the country code to be sure
         */
        val threeOrFourCountries = threeOrFourCountriesTheSame(entryARH1Country, exitARH1Country, entryIntradaCountry, exitIntradaCountry)

        if (!threeOrFourCountries) {
          log.info("All four license plates the same, but Countries not. CountryError")
          summarize.setErrorCountryCheck(true)
          summarize.setErrorLicenseCheck(false)
        } else {
          log.info("All four license and countries the same. Not changing Processing Indication")
          summarize.clearErrors()
        }

      } else {
        // Not the same. Processing Indication becomes Manual
        log.info("ARH not equal Intrada. LicenseError")
        summarize.setErrorLicenseCheck(true)
      }

    } else {
      // Intrada results not the same. Are there two different vehicles in this violation?
      log.info("Intrada license are NOT the same. Or 'no plate found'")

      // Is Intrada good enough

      if (entryIntradaLicConf >= minIntradaConfLevel) {
        log.info("Entry Intrada confidence level >= %d".format(minIntradaConfLevel))

        if (exitIntradaLicConf >= minIntradaConfLevel) {
          /*
           At this point Intrada has read both license good, but they are different.
           It's possible that one of these license is equal to the ARH result, but because
           Intrada is sure about it's reading this can indicate an ARH read error on the entry
              are exit image. This is not a correct match and not a violation.
           */
          log.info("Entry Intrada confidence level >= %d".format(minIntradaConfLevel))
          log.info("Intrada different licenses with confLevel >= %d. Possible read error ARH. ErrorMatchingCheck".format(minIntradaConfLevel))
          summarize.setErrorMatchingCheck(true)
        } else {
          log.info("Exit Intrada confidence level < %d".format(minIntradaConfLevel))

          // Intrada only sure about the entry license. Is this the same one as ARH
          if (entryIntradaLicense == entryARH1License) {
            log.info("Entry Intrada license equals Entry ARH license")
            if ((entryARH1LicConf > minARHConfLevel) && (exitARH1LicConf > minARHConfLevel)) {
              log.info("Entry/Exit ARH license confidence above %d".format(minARHConfLevel))
              val threeOrFourCountries = threeOrFourCountriesTheSame(entryARH1Country, exitARH1Country, entryIntradaCountry, exitIntradaCountry)

              if (!threeOrFourCountries) {
                log.info("ARH and entryIntrada license plates the same, but Countries not. CountryError")
                summarize.setErrorCountryCheck(true)
                summarize.setErrorLicenseCheck(false)
              } else {
                log.info("ARH and entryIntrada license and countries the same. Not changing Processing Indication.")
                summarize.clearErrors()
              }
            } else {
              log.info("ARH equal entry Intrada, but lowest ARH confidence level not above %d. LicenseError".format(minARHConfLevel))
              summarize.setErrorLicenseCheck(true)
            }
          } else {
            //
            log.info("Entry Intrada licence with conflevel >= %d not equal to ARH license. LicenseError".format(minIntradaConfLevel))
            summarize.setErrorLicenseCheck(true)
          }
        }
      } else {
        // entryIntrada license confidence not high enough. Try with Intrada exit License
        log.info("Entry Intrada license confidence not above %d".format(minIntradaConfLevel))

        if (exitIntradaLicConf >= minIntradaConfLevel) {
          log.info("Exit Intrada confidence >= %d".format(minIntradaConfLevel))

          if (exitIntradaLicense == entryARH1License) {
            log.info("Exit Intrada license equals Entry ARH license")
            if ((entryARH1LicConf > minARHConfLevel) && (exitARH1LicConf > minARHConfLevel)) {
              log.info("Entry/Exit ARH license confidence above %d".format(minARHConfLevel))
              val threeOrFourCountries = threeOrFourCountriesTheSame(entryARH1Country, exitARH1Country, entryIntradaCountry, exitIntradaCountry)

              if (!threeOrFourCountries) {
                log.info("ARH and exitIntrada license plates the same, but Countries not. CountryError")
                summarize.setErrorCountryCheck(true)
                summarize.setErrorLicenseCheck(false)
              } else {
                log.info("ARH and exitIntrada license and countries the same. Not changing Processing Indication.")
                summarize.clearErrors()
              }
            } else {
              log.info("ARH equal exit Intrada, but lowest ARH confidence level not above %d. LicenseError.".format(minARHConfLevel))
              summarize.setErrorLicenseCheck(true)
            }
          } else {
            //
            log.info("Exit Intrada licence with conflevel >= %d not equal to ARH license. LicenseError".format(minIntradaConfLevel))
            summarize.setErrorLicenseCheck(true)
          }
        } else {
          /*
           Intrada reads two different license plates. Both confidence are not above a minimum.
           See if one of the Intrada license is the same as the ARH license.
           */
          log.info("Exit Intrada License confidence not above %d".format(minIntradaConfLevel))

          if (entryIntradaLicense == entryARH1License) {
            log.info("Entry Intrada license equals Entry ARH license")

            if ((entryIntradaLicConf > minARHConfLevel) && (entryARH1LicConf > minARHConfLevel) && (exitARH1LicConf > minARHConfLevel)) {
              log.info("ARH license confidence and entry Intrada confidence  above %d. LicenseError".format(minARHConfLevel))

              val threeOrFourCountries = threeOrFourCountriesTheSame(entryARH1Country, exitARH1Country, entryIntradaCountry, exitIntradaCountry)

              if (!threeOrFourCountries) {
                log.info("ARH and entryIntrada license plates the same, but Countries not. CountryError")
                summarize.setErrorCountryCheck(true)
                summarize.setErrorLicenseCheck(false)
              } else {
                log.info("ARH and entryIntrada license and countries the same. Not changing Processing Indication.")
                summarize.clearErrors()
              }
            } else {
              log.info("ARH license confidence OR entry Intrada confidence NOT above %d. LicenseError".format(minARHConfLevel))
              summarize.setErrorLicenseCheck(true)
            }
          } else {
            if (exitIntradaLicense == entryARH1License) {
              log.info("Exit Intrada license equals Entry ARH license")

              if ((exitIntradaLicConf > minARHConfLevel) && (entryARH1LicConf > minARHConfLevel) && (exitARH1LicConf > minARHConfLevel)) {
                log.info("ARH license confidence and exit Intrada confidence  above %d. LicenseError".format(minARHConfLevel))

                val threeOrFourCountries = threeOrFourCountriesTheSame(entryARH1Country, exitARH1Country, entryIntradaCountry, exitIntradaCountry)

                if (!threeOrFourCountries) {
                  log.info("ARH and exitIntrada license plates the same, but Countries not. CountryError")
                  summarize.setErrorCountryCheck(true)
                  summarize.setErrorLicenseCheck(false)
                } else {
                  log.info("ARH and exitIntrada license and countries the same. Not changing Processing Indication.")
                  summarize.clearErrors()
                }
              } else {
                log.info("ARH license confidence or entry Intrada confidence not above %d. LicenseError".format(minARHConfLevel))
                summarize.setErrorLicenseCheck(true)
              }
            } else {
              /*
               None of the Intrada license is equal to ARH license. If ARH Country is a none-contract country
               trust ARH and do nothing
               */
              recognizerCountries.contains(entryARH1Country) match {
                case true ⇒ {
                  log.info("ARH Country [%s] is a contract country. LicenseError".format(entryARH1Country))
                  summarize.setErrorLicenseCheck(true)
                }
                case false ⇒ {
                  log.info("ARH Country [%s] is mobi-country. clearErrors".format(entryARH1Country))
                  summarize.clearErrors()
                }
              }
            }
          }

        }
      }
    }

    //check format OK
    if (!summarize.hasLicenseCheckFailed()) {
      if (entryARH1Country == "DE" && !GermanLicenseFormatter.validate(entryARH1License)) {
        log.info("German license not formatted well [%s]. LicenseError".format(entryARH1License))
        summarize.setErrorLicenseCheck(true)
      }
    }

    log.info(" ==== End Summarize Recognition results ====")

    // Return value
    summarize
  }

  def threeOrFourCountriesTheSame(entryARH1Country: String, exitARH1Country: String,
                                  entryIntradaCountry: String, exitIntradaCountry: String): Boolean = {
    if (entryARH1Country == exitARH1Country) {
      // ARH Country the same. Are Intrada Country the same
      if (entryIntradaCountry == exitIntradaCountry) {
        // Is ARH Country equal Intrada Country
        if (entryARH1Country == entryIntradaCountry) {
          // All four the same
          log.info("All four Countries are the same")
          true
        } else {
          log.info("ARH and Intrada has different Countries")
          false
        }
      } else {
        // Intrada countries not equal. Is one of them equal to ARH
        if (entryIntradaCountry == entryARH1Country) {
          log.info("ARH and entryIntrada countries the same")
          true
        } else {
          if (exitIntradaCountry == entryARH1Country) {
            log.info("ARH and exitIntrada countries the same")
            true
          } else {
            log.info("ARH and Intrada countries not the same")
            false
          }
        }
      }
    } else {
      // ARH Countries not the same. maximum 3 countries the same
      if (entryIntradaCountry == exitIntradaCountry) {
        // Intrada the same. One of the ARH matching?
        if (entryIntradaCountry == entryARH1Country) {
          log.info("Intrada and entryARH countries the same")
          true
        } else {
          if (entryIntradaCountry == exitARH1Country) {
            log.info("Intrada and exitARH countries the same")
            true
          } else {
            log.info("ARH and Intrada countries not the same")
            false
          }
        }
      } else {
        // Too many Country indications
        log.info("ARH and Intrada countries not the same")
        false
      }
    }
  }

  override def toString: String = {
    "Entry=%s Exit=%s summery=%s".format(entry, exit, summaryCheck)
  }
}

/**
 * The actor which checks the violation images using the registrationCheck
 * @param registrationCheck reference to the registrationCheck
 * @param confidenceLevel level used to decide if the registration has to be checked
 */
class CheckViolation(registrationCheck: ActorRef,
                     intradaInterfaceToUse: Int,
                     confidenceLevel: Int) extends Actor with ActorLogging {
  /**
   * Postfix used to create reference when checking an entry
   */
  private val ENTRY_POSTFIX = "|entry"
  /**
   * Postfix used to create reference when checking an exit
   */
  private val EXIT_POSTFIX = "|exit"
  /**
   * List of requests
   */
  private var violations = Map[String, ImageCheckRequestData]()

  /**
   * process received messages
   * @return
   */
  def receive = LoggingReceive {
    case request: ViolationImageCheckRequest ⇒ {
      // Create key with the Violation.license and Violation.time (exit time)
      val key = createKey(request.violation)

      // Send this specific Violation to the recognizer
      val result = sendToRecognizer(key, request.violation)

      // Are all recognition request done for this Violation?
      if (isReady(request.violation, result)) {
        //send reply
        sender ! new ViolationImageCheckResponse(request.reference, request.violation, result: ViolationCheckResult)
      } else {
        violations += key -> new ImageCheckRequestData(request, sender, System.currentTimeMillis(), result)
      }

    }

    case response: RegistrationImageCheckResponse ⇒ {
      // Generate the response key for searching in the Violation request.
      val key = createKey(response.reference)

      // Search for this Violation request
      val requestData = violations.get(key)

      //process recognition
      requestData.foreach(data ⇒ {
        // Update the Violation data with the ImageResult response from entry/exit
        val updatedData = updateRequestData(data, response)

        // Verify if all the responses on the requests for this violation are received
        if (isReady(data.request.violation, updatedData.result)) {
          // All responses received. Send the Violation with recognition results back to sender
          data.applicant ! new ViolationImageCheckResponse(data.request.reference, data.request.violation, updatedData.result)

          // Remove this violation request from list
          violations -= key
        } else {
          // There are still outstanding requests. Update this violation request with the received entry/exit response.
          violations += key -> updatedData
        }
      })
    }

    case response: ImagesCheckResponseViolation ⇒ {
      // Generate the response key for searching in the Violation request.
      val key = createKey(response.reference)

      // Search for this Violation request
      val requestData = violations.get(key)

      //process recognition
      requestData.foreach(data ⇒ {
        log.info("Update the result for key {}", key)
        // Update the Violation data with the ImageResult response from entry/exit
        val updatedData = updateRequestDataViolation(data, response)

        // All response for this Violation are received. There is only one response (Intrada Interface 2)
        log.info("All responses received for key {}", key)

        // All responses received. Send the Violation with recognition results back to sender
        data.applicant ! new ViolationImageCheckResponse(data.request.reference, data.request.violation, updatedData.result)

        // Remove this violation request from list
        violations -= key

      })
    }
  }

  /**
   * Update the RequestData after receiving a RegistrationImageCheckResponse
   * @param data the current data
   * @param response the received response
   * @return the new Request Data
   */
  private def updateRequestData(data: ImageCheckRequestData, response: RegistrationImageCheckResponse): ImageCheckRequestData = {
    if (response.reference.endsWith(ENTRY_POSTFIX)) {
      val entryResult = data.result.copy(entry = Some(response.result))
      data.copy(result = entryResult)
    } else if (response.reference.endsWith(EXIT_POSTFIX)) {
      val exitResult = data.result.copy(exit = Some(response.result))
      data.copy(result = exitResult)
    } else {
      data
    }
  }

  /**
   * Update the RequestData after receiving a RegistrationImageCheckResponse
   * @param data the current data
   * @param response the received response
   * @return the new Request Data
   */
  private def updateRequestDataViolation(data: ImageCheckRequestData,
                                         response: ImagesCheckResponseViolation): ImageCheckRequestData = {
    val newResult = data.result.copy(entry = Some(response.result), exit = Some(response.result))
    data.copy(result = newResult)
  }
  /**
   * Create a key based on a violation
   * @param violation the violation
   * @return key
   */
  private def createKey(violation: Violation): String = {
    "%s%d".format(violation.license, violation.time)
  }

  /**
   * Create a key based on a received reference
   * @param reference the received reference
   * @return the key
   */
  private def createKey(reference: String): String = {
    val pos = reference.indexOf("|")
    if (pos < 0) {
      reference
    } else {
      reference.substring(0, pos)
    }
  }

  /**
   * Check if we have received all outstanding request
   * @param violation the violation
   * @param result the result
   * @return true were done false we are still waiting for responses
   */
  private def isReady(violation: Violation, result: ViolationCheckResult): Boolean = {
    result.entry.isDefined && (violation.vvr.classifiedRecord.speedRecord.exit.isEmpty || result.exit.isDefined)
  }

  /**
   * Send requests to the registrationCheck and create the current result
   * @param key the used key
   * @param violation the violation
   * @return the current result
   */
  private def sendToRecognizer(key: String, violation: Violation): ViolationCheckResult = {
    // Get the current license (ARH result)
    val overrideLicense = violation.vvr.classifiedRecord.validLicensePlate

    // If there is a license get the entry data
    val entry = if (overrideLicense.isDefined) {
      violation.vvr.classifiedRecord.speedRecord.entry.copy(license = Some(ValueWithConfidence(overrideLicense.get, 0)))
    } else {
      violation.vvr.classifiedRecord.speedRecord.entry
    }

    // If there is a license get the exit data
    val exit = if (overrideLicense.isDefined) {
      violation.vvr.classifiedRecord.speedRecord.exit.map(_.copy(license = Some(ValueWithConfidence(overrideLicense.get, 0))))
    } else {
      violation.vvr.classifiedRecord.speedRecord.exit
    }

    // Get the Processing Indicator (Auto, Manual, Mobi)
    val processInd = violation.vvr.classifiedRecord.indicator

    // Create the empty ViolationCheckresult object (init)
    var result = new ViolationCheckResult(None, None)

    intradaInterfaceToUse match {
      case CheckViolation.INTRADA_IFACE_COMPARE ⇒ {
        /*
         For the new Intrada interface there are the following parameters necessary:
         - Entry Image
         - Entry ARH License
         - Entry ARH Country
         - Entry ARH Confidence
         - Exit Image
         - Exit ARH License
         - Exit ARH Country
         - Exit ARH Confidence

         They are send in ONE message.
          */

        /* Always ask for recognition */
        exit match {
          case Some(exitRegistration) ⇒
            registrationCheck ! ImagesCheckRequestViolation(key, entry, exitRegistration)

          case None ⇒
            val message = "No recognition done, because of invalid exit registration"

            val entryResult = new ImageCheckResult()
            entryResult.addRecognitionResult(RecognitionResult(RecognitionResult.ARH_ENTRY, entry))
            entryResult.addRecognitionResult(RecognitionResult(RecognitionResult.INTRADA2, message))
            result = result.copy(entry = Some(entryResult))
        }
      }

      case CheckViolation.INTRADA_IFACE_NORMAL ⇒ {

        // 	Is it necessary to send the entry Image for re-recognition?
        val skipMsg = mustRecognize(processInd, entry)

        if (skipMsg.isEmpty) {
          // The entry must recognized
          registrationCheck ! RegistrationImageCheckRequest(key + ENTRY_POSTFIX, entry)
        } else {
          // Not necessary to do recognition on entry
          val entryResult = new ImageCheckResult()
          entryResult.addRecognitionResult(RecognitionResult(RecognitionResult.ARH1, entry))
          entryResult.addRecognitionResult(RecognitionResult(RecognitionResult.INTRADA, skipMsg.getOrElse("")))
          result = result.copy(entry = Some(entryResult))
        }

        // For each exit (more than one) do (may be ) recognition
        exit.foreach {
          vmd ⇒
            {
              // 	Is it necessary to send the entry Image for re-recognition?
              val skipMsg = mustRecognize(processInd, vmd)

              if (skipMsg.isEmpty) {
                // The exit must recognized
                registrationCheck ! RegistrationImageCheckRequest(key + EXIT_POSTFIX, vmd)
              } else {
                val exitResult = new ImageCheckResult()
                exitResult.addRecognitionResult(RecognitionResult(RecognitionResult.ARH1, vmd))
                exitResult.addRecognitionResult(RecognitionResult(RecognitionResult.INTRADA, skipMsg.getOrElse("")))
                result = result.copy(exit = Some(exitResult))
              }
            }
        }
      }
    }

    result
  }
  /**
   * Do we need to check the registration
   * Skip Dacolian check when in Automatic and confidence is higher than "confidenceLevel"
   * @param processInd
   * @param reg
   * @return
   */
  private def mustRecognize(processInd: ProcessingIndicator.Value, reg: VehicleMetadata): Option[String] = {

    val confidenceFound = reg.license.map(_.confidence).getOrElse(0)
    if (processInd == ProcessingIndicator.Automatic && confidenceFound > confidenceLevel) {
      return Some("Skip: Auto confidence %d > %s".format(confidenceFound, confidenceLevel))
    }
    return None
  }
}

object CheckViolation {
  val INTRADA_IFACE_NORMAL = 1
  val INTRADA_IFACE_COMPARE = 2
}