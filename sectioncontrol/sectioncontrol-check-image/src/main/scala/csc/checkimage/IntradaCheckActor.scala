/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.checkimage

import akka.actor.{ Actor, ActorLogging, ActorRef }
import akka.event.LoggingReceive
import csc.akkautils.DirectLogging
import csc.sectioncontrol.enforce.common.nl.violations.{ IntradaCheck }
import csc.sectioncontrol.enforce.violations.GermanLicenseFormatter
import csc.sectioncontrol.messages.{ ProcessingIndicator, ValueWithConfidence, VehicleMetadata }

case class IntradaImageCheckRequest(reference: String, violation: IntradaCheck)

/**
 * The response of a ViolationImageCheckRequest
 * @param reference the reference given in the request
 * @param violation the requested violation
 * @param result the result of the check
 */
case class IntradaImageCheckResponse(reference: String, violation: IntradaCheck, result: IntradaCheckResult)

/**
 * The data to keep track of the requests
 * @param request the received request
 * @param applicant who made the request
 * @param startTime the time the request was received
 * @param result the result of the request
 */
case class IntradaCheckRequestData(request: IntradaImageCheckRequest, applicant: ActorRef, startTime: Long, result: IntradaCheckResult)

/**
 * The result of the violation check
 * @param entry the result of the check of the entry
 * @param exit the result of the check of the exit
 */
case class IntradaCheckResult(entry: Option[ImageCheckResult], exit: Option[ImageCheckResult]) extends DirectLogging {

  def createSummaryWithIntrada2(recognizerCountries: Seq[String],
                                autoThresholdLevel: Int,
                                mobiThresholdLevel: Int): (ImageCheckResult, Option[ValueWithConfidence[String]], Option[ValueWithConfidence[String]]) = {
    // Log each recognition result
    // Get the entry Recognition results
    log.info(" ==== Start Summarize Recognition results Intrada2 ====")

    var entryRecognitionResults = entry.map(_.getRecognitionResults())
    log.info("EntryResults: %s".format(entryRecognitionResults.toString()))

    // Get the Exit Recognition results
    val exitRecognitionResults = exit.map(_.getRecognitionResults())
    log.info("ExitResults: %s".format(exitRecognitionResults.toString()))

    // Get the ARH result for verification with the Intrada result.
    val entryARH: Option[RecognitionResult] = entryRecognitionResults.flatMap(_.find(_.recognizerId == "ARH_ENTRY"))
    val exitARH: Option[RecognitionResult] = exitRecognitionResults.flatMap(_.find(_.recognizerId == "ARH_EXIT"))

    // Get the final Intrada result
    val entryINTRADA: Option[RecognitionResult] = entryRecognitionResults.flatMap(_.find(_.recognizerId == "INTRADA_INTERFACE_2"))

    // The ARH licenses are equal (otherwise no match). Get only the entry one
    val (entryARHLicense, entryARHLicConf) = entryARH match {
      case Some(RecognitionResult(_, Some(ValueWithConfidence(value, confidence)), _, _, _)) ⇒
        (value, confidence)

      case _ ⇒ ("", 0)
    }

    val (exitARHLicense, exitARHLicConf) = exitARH match {
      case Some(RecognitionResult(_, Some(ValueWithConfidence(value, confidence)), _, _, _)) ⇒
        (value, confidence)

      case _ ⇒ ("", 0)
    }

    val (entryARHCountry, entryARHCountryConf) = entryARH match {
      case Some(RecognitionResult(_, _, _, Some(ValueWithConfidence(value, confidence)), _)) ⇒
        (value, confidence)

      case _ ⇒ ("", 0)
    }

    val (exitARHCountry, exitARHCountryConf) = exitARH match {
      case Some(RecognitionResult(_, _, _, Some(ValueWithConfidence(value, confidence)), _)) ⇒
        (value, confidence)

      case _ ⇒ ("", 0)
    }

    // The Intrada licenses are equal too. The same result was put in entry en exit.
    val (intradaLicense, intradaConfidence) = entryINTRADA match {
      case Some(RecognitionResult(_, Some(ValueWithConfidence(value, confidence)), _, _, _)) ⇒
        (value, confidence)

      case _ ⇒ ("", 0)
    }

    // Get the final country code
    val (intradaCountry, intradaCountryConf) = entryINTRADA match {
      case Some(RecognitionResult(_, _, _, Some(ValueWithConfidence(value, confidence)), _)) ⇒
        (value, confidence)

      case _ ⇒ ("", 0)
    }

    log.info("EntryARH1   : %s (%d) %s (%d)".format(entryARHLicense, entryARHLicConf, entryARHCountry, entryARHCountryConf))
    log.info("ExitARH1    : %s (%d) %s (%d)".format(exitARHLicense, exitARHLicConf, exitARHCountry, exitARHCountryConf))
    log.info("Intrada     : %s (%d) %s (%d)".format(intradaLicense, intradaConfidence, intradaCountry, intradaCountryConf))

    // Join the error results together.
    val summarize = new ImageCheckResult(0)

    // Intrada confidence level 1-10 are special
    if (intradaConfidence <= 10) {
      intradaConfidence match {
        case 1 ⇒
          log.info("Intrada reads two different licenses. Possible read error ARH. ErrorMatchingCheck")
          summarize.setErrorMatchingCheck(true)

        case _ ⇒
          log.info("Intrada returns a not defined confidence level (%d). Remove the violation.".format(intradaConfidence))
          summarize.setErrorMatchingCheck(true)
      }
    } else if (intradaConfidence > 10 && (intradaLicense == null || intradaLicense.trim.isEmpty)) {
      // Intrada recognizes empty license plate (""), but with confidence > 10, apparently this can happen (See TCU-156)
      log.info("Intrada returns an empty license plate with confidence > 10. Remove the violation.")
      summarize.setErrorMatchingCheck(true)
    } else {
      /*
       The intrada license must be the same as the ARH license, otherwise this is not a violation.
       For time being this violation is sent to manual processing.
       May be in future the violation has to be processed again by classify and 'detect violation'
       to figure out if this is still a violation.
       */
      if (!intradaLicense.equals(entryARHLicense)) {
        log.info("Intrada (%s) not equal ARH (%s). Send the violation to the manual processing".format(intradaLicense, entryARHLicense))
        summarize.setErrorLicenseCheck(true)
        summarize.setErrorCountryCheck(true)
      } else {
        /*
          Is the confidence level high enough for the thresholds
         */
        if (recognizerCountries.contains(intradaCountry)) {
          if (intradaConfidence < autoThresholdLevel) {
            log.info("Contract Country - Confidence level %d is less than AutoThresHold %d. Send to Manual Processing".format(intradaConfidence, autoThresholdLevel))
            summarize.setErrorLicenseCheck(true)
            summarize.setErrorCountryCheck(true)
          }
        } else {
          if (intradaConfidence < mobiThresholdLevel) {
            log.info("Mobi Country - Confidence level %d is less than MobiThresHold %d. Send to Manual Processing".format(intradaConfidence, mobiThresholdLevel))
            summarize.setErrorLicenseCheck(true)
            summarize.setErrorCountryCheck(true)
          }
        }

        if (!summarize.hasErrors()) {
          /*
           Everything looks fine, but there can be an issue with the country code.
           In classify and during creating the ViolationData the Violation Country code will be the country
           code that have the highest ARH CountryCodeConfidenceLevel! On the basis of this country code classify decides the matching-pair
           gets the Processing Indication automatic (contract country) or mobi (non contract country [all other countries]).
           The country code with the highest ARH confidence level is also put in the final violation file (zaakbestand)

           When the Intrada Country code is not equal to the ARH Country code with highest confidence level, there could be an issue.
           A contract country could end up in the mobi processing or even worse a non contract country ends up in automatic!

           */

          // Get the country code with the highest confidence level
          val winningCountryCode = if (entryARHCountryConf > exitARHCountryConf)
            entryARHCountry
          else
            exitARHCountry

          // Get the country code with the highest confidence level
          val losingCountryCode = if (entryARHCountryConf < exitARHCountryConf)
            entryARHCountry
          else
            exitARHCountry

          if (intradaCountry.equals(winningCountryCode)) {
            // No worries
            log.info("Intrada has the same results as ARH.")
            summarize.clearErrors()
          } else {
            if (intradaCountry.equals(losingCountryCode)) {
              // Was it a contract country?
              if (recognizerCountries.contains(winningCountryCode)) {
                // Yes. Still contract country?
                if (recognizerCountries.contains(intradaCountry)) {
                  // No worries about Processing Indication. Only change the final country code
                  log.info("Change Country code from (%s) to (%s) because of Intrada.".format(winningCountryCode, losingCountryCode))

                  summarize.clearErrors()
                  summarize.setCountryChange(true)
                } else {
                  // No contract country anymore. Change final country code and processing indication
                  log.info("Change Country code from (%s) to (%s) because of Intrada. Also the processing indication change to Mobi.".format(winningCountryCode, losingCountryCode))
                  summarize.clearErrors()
                  summarize.setCountryChange(true)
                  summarize.setChangeToMobi(true)
                }
              } else {
                // Original no contract country (a mobi country). Is it now?
                if (recognizerCountries.contains(intradaCountry)) {
                  // Becomes a contract country. Change the final country code and processing indication
                  log.info("Change Country code from (%s) to (%s) because of Intrada. Also the processing indication change to Auto.".format(winningCountryCode, losingCountryCode))
                  summarize.clearErrors()
                  summarize.setCountryChange(true)
                  summarize.setChangeToAuto(true)
                } else {
                  // Still a non contract country. Change only final country code
                  log.info("Change Country code from (%s) to (%s) because of Intrada.".format(winningCountryCode, losingCountryCode))

                  summarize.clearErrors()
                  summarize.setCountryChange(true)
                }

              }
            } else {
              log.info("Intrada country (%s) not equal to ARH Highest (%s) and ARH lowest (%s)".format(intradaCountry, winningCountryCode, losingCountryCode))
              summarize.setErrorCountryCheck(true)
            }
          }
        }
      }
    }

    //check format OK
    if (!summarize.hasLicenseCheckFailed()) {
      if (intradaCountry == "DE" && !GermanLicenseFormatter.validate(intradaLicense)) {
        log.info("German license not formatted well [%s]. LicenseError".format(entryARHLicense))
        summarize.setErrorLicenseCheck(true)
      }
    }

    log.info(" ==== End Summarize Recognition results Intrada2 ====")

    // Return value
    (summarize, entryINTRADA.map(_.license).getOrElse(None), entryINTRADA.map(_.country).getOrElse(None))
  }

  def threeOrFourCountriesTheSame(entryARH1Country: String, exitARH1Country: String,
                                  entryIntradaCountry: String, exitIntradaCountry: String): Boolean = {
    if (entryARH1Country == exitARH1Country) {
      // ARH Country the same. Are Intrada Country the same
      if (entryIntradaCountry == exitIntradaCountry) {
        // Is ARH Country equal Intrada Country
        if (entryARH1Country == entryIntradaCountry) {
          // All four the same
          log.info("All four Countries are the same")
          true
        } else {
          log.info("ARH and Intrada has different Countries")
          false
        }
      } else {
        // Intrada countries not equal. Is one of them equal to ARH
        if (entryIntradaCountry == entryARH1Country) {
          log.info("ARH and entryIntrada countries the same")
          true
        } else {
          if (exitIntradaCountry == entryARH1Country) {
            log.info("ARH and exitIntrada countries the same")
            true
          } else {
            log.info("ARH and Intrada countries not the same")
            false
          }
        }
      }
    } else {
      // ARH Countries not the same. maximum 3 countries the same
      if (entryIntradaCountry == exitIntradaCountry) {
        // Intrada the same. One of the ARH matching?
        if (entryIntradaCountry == entryARH1Country) {
          log.info("Intrada and entryARH countries the same")
          true
        } else {
          if (entryIntradaCountry == exitARH1Country) {
            log.info("Intrada and exitARH countries the same")
            true
          } else {
            log.info("ARH and Intrada countries not the same")
            false
          }
        }
      } else {
        // Too many Country indications
        log.info("ARH and Intrada countries not the same")
        false
      }
    }
  }
}

/**
 * The actor which checks the violation images using the registrationCheck
 * @param registrationCheck reference to the registrationCheck
 * @param confidenceLevel level used to decide if the registration has to be checked
 */
class IntradaCheckActor(registrationCheck: ActorRef,
                        intradaInterfaceToUse: Int,
                        confidenceLevel: Int) extends Actor with ActorLogging {
  /**
   * Postfix used to create reference when checking an entry
   */
  private val ENTRY_POSTFIX = "|entry"
  /**
   * Postfix used to create reference when checking an exit
   */
  private val EXIT_POSTFIX = "|exit"
  /**
   * List of requests
   */
  private var violations = Map[String, IntradaCheckRequestData]()

  /**
   * process received messages
   * @return
   */
  def receive = LoggingReceive {
    case request: IntradaImageCheckRequest ⇒ {
      val key = createKey(request.violation)
      log.info("IntradaImageCheckRequest with key {}", key)
      log.info("Intrada check images entry: " + request.violation.vvr.classifiedRecord.speedRecord.entry.images)
      request.violation.vvr.classifiedRecord.speedRecord.exit.foreach(exit ⇒ log.info("Intrada check images exit: " + exit.images))

      val result = sendToRecognizer(key, request.violation)
      violations += key -> new IntradaCheckRequestData(request, sender, System.currentTimeMillis(), result)
    }

    case response: RegistrationImageCheckResponse ⇒ {
      // Generate the response key for searching in the Violation request.
      val key = createKey(response.reference)
      log.info("RegistrationImageCheckResponse with key {}", key)

      // Search for this Violation request
      val requestData = violations.get(key)

      //process recognition
      requestData.foreach(data ⇒ {
        // Update the Violation data with the ImageResult response from entry/exit
        val updatedData = updateRequestData(data, response)

        // Verify if all the responses on the requests for this violation are received
        if (isReady(data.request.violation, updatedData.result)) {
          // All responses received. Send the Violation with recognition results back to sender

          log.info("Sending response via registrationImagecheckResponse with results: " + updatedData.result)

          data.applicant ! new IntradaImageCheckResponse(data.request.reference, data.request.violation, updatedData.result)

          // Remove this violation request from list
          violations -= key
        } else {
          // There are still outstanding requests. Update this violation request with the received entry/exit response.
          violations += key -> updatedData
        }
      })
    }

    case response: ImagesCheckResponseViolation ⇒ {
      // Generate the response key for searching in the Violation request.

      val key = createKey(response.reference)
      log.info("ImagesCheckResponseViolation with key {}", key)

      // Search for this Violation request
      val requestData = violations.get(key)

      //process recognition
      requestData.foreach(data ⇒ {
        log.info("Update the result for key {}", key)
        // Update the Violation data with the ImageResult response from entry/exit
        val updatedData = updateRequestDataViolation(data, response)

        // All response for this Violation are received. There is only one response (Intrada Interface 2)
        log.info("All responses received for key {}", key)

        // All responses received. Send the Violation with recognition results back to sender

        log.info("Sending response via ImagesCheckResponseViolation with results: " + updatedData.result)

        data.applicant ! IntradaImageCheckResponse(data.request.reference, data.request.violation, updatedData.result)

        // Remove this violation request from list
        violations -= key

      })
    }
  }

  /**
   * Update the RequestData after receiving a RegistrationImageCheckResponse
   * @param data the current data
   * @param response the received response
   * @return the new Request Data
   */
  private def updateRequestData(data: IntradaCheckRequestData, response: RegistrationImageCheckResponse): IntradaCheckRequestData = {
    if (response.reference.endsWith(ENTRY_POSTFIX)) {
      val entryResult = data.result.copy(entry = Some(response.result))
      data.copy(result = entryResult)
    } else if (response.reference.endsWith(EXIT_POSTFIX)) {
      val exitResult = data.result.copy(exit = Some(response.result))
      data.copy(result = exitResult)
    } else {
      data
    }
  }

  /**
   * Update the RequestData after receiving a RegistrationImageCheckResponse
   * @param data the current data
   * @param response the received response
   * @return the new Request Data
   */
  private def updateRequestDataViolation(data: IntradaCheckRequestData,
                                         response: ImagesCheckResponseViolation): IntradaCheckRequestData = {
    val newResult = data.result.copy(entry = Some(response.result), exit = Some(response.result))
    data.copy(result = newResult)
  }

  /**
   * Create a key based on a violation
   * @param violation the violation
   * @return key
   */
  private def createKey(violation: IntradaCheck): String = {
    "%s%d".format(violation.license, violation.time)
  }

  /**
   * Create a key based on a received reference
   * @param reference the received reference
   * @return the key
   */
  private def createKey(reference: String): String = {
    val pos = reference.indexOf("|")
    if (pos < 0) {
      reference
    } else {
      reference.substring(0, pos)
    }
  }

  /**
   * Check if we have received all outstanding request
   * @param violation the violation
   * @param result the result
   * @return true were done false we are still waiting for responses
   */
  private def isReady(violation: IntradaCheck, result: IntradaCheckResult): Boolean = {
    result.entry.isDefined && (violation.vvr.classifiedRecord.speedRecord.exit.isEmpty || result.exit.isDefined)
  }

  /**
   * Send requests to the registrationCheck and create the current result
   * @param key the used key
   * @param violation the violation
   * @return the current result
   */
  private def sendToRecognizer(key: String, violation: IntradaCheck): IntradaCheckResult = {
    // Get the current license (ARH result)
    val overrideLicense = violation.vvr.classifiedRecord.validLicensePlate

    // If there is a license get the entry data
    val entry = if (overrideLicense.isDefined) {
      violation.vvr.classifiedRecord.speedRecord.entry.copy(license = Some(ValueWithConfidence(overrideLicense.get, 0)))
    } else {
      violation.vvr.classifiedRecord.speedRecord.entry
    }

    // If there is a license get the exit data
    val exit = if (overrideLicense.isDefined) {
      violation.vvr.classifiedRecord.speedRecord.exit.map(_.copy(license = Some(ValueWithConfidence(overrideLicense.get, 0))))
    } else {
      violation.vvr.classifiedRecord.speedRecord.exit
    }

    // Get the Processing Indicator (Auto, Manual, Mobi)
    val processInd = violation.vvr.classifiedRecord.indicator

    // Create the empty ViolationCheckresult object (init)
    var result = new IntradaCheckResult(None, None)

    intradaInterfaceToUse match {
      case CheckViolation.INTRADA_IFACE_COMPARE ⇒ {
        /*
         For the new Intrada interface there are the following parameters necessary:
         - Entry Image
         - Entry ARH License
         - Entry ARH Country
         - Entry ARH Confidence
         - Exit Image
         - Exit ARH License
         - Exit ARH Country
         - Exit ARH Confidence

         They are send in ONE message.
          */

        /* Always ask for recognition */
        exit match {
          case Some(exitRegistration) ⇒
            registrationCheck ! ImagesCheckRequestViolation(key, entry, exitRegistration)

          case None ⇒
            val message = "No recognition done, because of invalid exit registration"

            val entryResult = new ImageCheckResult()
            entryResult.addRecognitionResult(RecognitionResult(RecognitionResult.ARH_ENTRY, entry))
            entryResult.addRecognitionResult(RecognitionResult(RecognitionResult.INTRADA2, message))
            result = result.copy(entry = Some(entryResult))
        }
      }

      case CheckViolation.INTRADA_IFACE_NORMAL ⇒ {

        // 	Is it necessary to send the entry Image for re-recognition?
        val skipMsg = mustRecognize(processInd, entry)

        if (skipMsg.isEmpty) {
          // The entry must recognized
          registrationCheck ! RegistrationImageCheckRequest(key + ENTRY_POSTFIX, entry)
        } else {
          // Not necessary to do recognition on entry
          val entryResult = new ImageCheckResult()
          entryResult.addRecognitionResult(RecognitionResult(RecognitionResult.ARH1, entry))
          entryResult.addRecognitionResult(RecognitionResult(RecognitionResult.INTRADA, skipMsg.getOrElse("")))
          result = result.copy(entry = Some(entryResult))
        }

        // For each exit (more than one) do (may be ) recognition
        exit.foreach {
          vmd ⇒
            {
              // 	Is it necessary to send the entry Image for re-recognition?
              val skipMsg = mustRecognize(processInd, vmd)

              if (skipMsg.isEmpty) {
                // The exit must recognized
                registrationCheck ! RegistrationImageCheckRequest(key + EXIT_POSTFIX, vmd)
              } else {
                val exitResult = new ImageCheckResult()
                exitResult.addRecognitionResult(RecognitionResult(RecognitionResult.ARH1, vmd))
                exitResult.addRecognitionResult(RecognitionResult(RecognitionResult.INTRADA, skipMsg.getOrElse("")))
                result = result.copy(exit = Some(exitResult))
              }
            }
        }
      }
    }

    result
  }

  /**
   * Do we need to check the registration
   * Skip Dacolian check when in Automatic and confidence is higher than "confidenceLevel"
   * @param processInd
   * @param reg
   * @return
   */
  private def mustRecognize(processInd: ProcessingIndicator.Value, reg: VehicleMetadata): Option[String] = {

    val confidenceFound = reg.license.map(_.confidence).getOrElse(0)
    if (processInd == ProcessingIndicator.Automatic && confidenceFound > confidenceLevel) {
      return Some("Skip: Auto confidence %d > %s".format(confidenceFound, confidenceLevel))
    }
    return None
  }
}
