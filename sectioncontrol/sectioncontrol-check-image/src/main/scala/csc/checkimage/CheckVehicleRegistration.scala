/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.checkimage

import akka.actor.{ Actor, ActorLogging, ActorRef }
import akka.event.LoggingReceive
import akka.util.Duration
import akka.util.duration._
import csc.image.recognizer.client.{ ImageRecognitionRequest, ImageRecognitionResult, Recognizer, ImagesRecognitionRequestForViolation }
import csc.sectioncontrol.messages.{ ValueWithConfidence, VehicleMetadata }
import csc.sectioncontrol.storage.KeyWithTimeAndId

// LoggerFactory or DirectLogging

/**
 * Possible States of CheckVehicleRegistration
 */
object RegistrationImageState extends Enumeration {
  val WaitForDacolian, WaitForARH = Value
}

/**
 * The supported request to check a registration
 * @param reference a reference
 * @param registration the registration to check
 */
case class RegistrationImageCheckRequest(reference: String, registration: VehicleMetadata)

/**
 * The supported request to check a violation
 * @param reference a reference
 * @param entryRegistration the entry registration to check
 * @param exitRegistration the exit registration to check
 */
case class ImagesCheckRequestViolation(reference: String, entryRegistration: VehicleMetadata, exitRegistration: VehicleMetadata)

/**
 * The response to the to RegistrationImageCheckRequest
 * @param reference the reference as given in the request
 * @param registration the registration to check
 * @param result the result of the check
 */
case class RegistrationImageCheckResponse(reference: String, registration: VehicleMetadata, result: ImageCheckResult)

/**
 * The response to the to ImagesCheckRequestViolation
 * @param reference the reference as given in the request
 * @param registration the registration to check
 * @param result the result of the check
 */
case class ImagesCheckResponseViolation(reference: String, registration: VehicleMetadata, result: ImageCheckResult)

/**
 * Object to keep track of the different requests
 * @param request The received request
 * @param applicant The sender of the request
 * @param startTime The time the request is received
 * @param state the current start of processing the request
 * @param result the result of the request
 */
case class RegistrationImageState(request: RegistrationImageCheckRequest,
                                  applicant: ActorRef,
                                  startTime: Long,
                                  state: RegistrationImageState.Value,
                                  result: ImageCheckResult)

/**
 * Object to keep track of the different violation image check requests
 * @param request The received request
 * @param applicant The sender of the request
 * @param startTime The time the request is received
 * @param state the current start of processing the request
 * @param result the result of the request
 */
case class ViolationImagesState(request: ImagesCheckRequestViolation,
                                applicant: ActorRef,
                                startTime: Long,
                                state: RegistrationImageState.Value,
                                result: ImageCheckResult)

/**
 * The result of a recognizer
 * @param recognizerId Id of the Recognizer
 * @param license The found license
 * @param rawLicense the found raw license including special characters
 * @param country the found country
 * @param errorMsg the error message when recognition failed
 */
case class RecognitionResult(recognizerId: String,
                             license: Option[ValueWithConfidence[String]] = None,
                             rawLicense: Option[String] = None,
                             country: Option[ValueWithConfidence[String]] = None,
                             errorMsg: Option[String] = None)

/**
 * Support class to create the RecognitionResult
 */
object RecognitionResult {
  val ARH1 = "ARH1"
  val ARH2 = "ARH2"
  val INTRADA = "INTRADA"
  val INTRADA2 = "INTRADA_INTERFACE_2"
  val ARH_ENTRY = "ARH_ENTRY"
  val ARH_EXIT = "ARH_EXIT"

  def apply(id: String, vehicle: VehicleMetadata): RecognitionResult = {
    new RecognitionResult(id, vehicle.license, vehicle.rawLicense, vehicle.country, None)
  }
  def apply(id: String, error: String): RecognitionResult = {
    new RecognitionResult(id, None, None, None, Some(error))
  }
}

/**
 * Definition of errors possible in the ImageCheckResult
 */
object ImageCheckResult {
  val licenseCheck = 1
  val countryCheck = 2
  val errorCheck = 4
  val licenseFormatCheck = 8
  val changeCountry = 16
  val changeToMobi = 32
  val changeToAuto = 64
  val errorMatchingCheck = 128
  val all = licenseCheck | countryCheck | errorCheck | licenseFormatCheck | errorMatchingCheck | changeCountry | changeToMobi | changeToAuto
}

/**
 * Class to keep track of the results and the errors of the image Check
 * @param err Mask containing the error
 */
class ImageCheckResult(err: Int = 0) {
  import ImageCheckResult._
  private var errors = err
  private var recognitionResults = Seq[RecognitionResult]()

  /**
   * Clear all errors
   */
  def clearErrors() {
    errors = 0
  }
  /**
   * Set or remove LicenseCheck error
   * @param error true there is an error false there is not an error
   */
  def setErrorLicenseCheck(error: Boolean) {
    if (error)
      errors = errors | licenseCheck
    else
      errors = errors & (all ^ licenseCheck)
  }
  /**
   * Set or remove CountryCheck error
   * @param error true there is an error false there is not an error
   */
  def setErrorCountryCheck(error: Boolean) {
    if (error)
      errors = errors | countryCheck
    else
      errors = errors & (all ^ countryCheck)
  }
  /**
   * Set or remove general check error
   * @param error true there is an error false there is not an error
   */
  def setErrorCheck(error: Boolean) {
    if (error)
      errors = errors | errorCheck
    else
      errors = errors & (all ^ errorCheck)
  }
  /**
   * Set or remove license format check error
   * @param error true there is an error false there is not an error
   */
  def setErrorLicenseFormatCheck(error: Boolean) {
    if (error)
      errors = errors | licenseFormatCheck
    else
      errors = errors & (all ^ licenseFormatCheck)
  }

  /**
   * Set or remove matching check error
   * @param error true there is an error false there is not an error
   */
  def setErrorMatchingCheck(error: Boolean) {
    if (error)
      errors = errors | errorMatchingCheck
    else
      errors = errors & (all ^ errorMatchingCheck)
  }

  /**
   * Set or remove the Change to other country
   * @param error true there is an error false there is not an error
   */
  def setCountryChange(error: Boolean) {
    if (error)
      errors = errors | changeCountry
    else
      errors = errors & (all ^ changeCountry)
  }

  /**
   * Set or remove the change to Processing Indication Mobi
   * @param error true there is an error false there is not an error
   */
  def setChangeToMobi(error: Boolean) {
    if (error)
      errors = errors | changeToMobi
    else
      errors = errors & (all ^ changeToMobi)
  }

  /**
   * Set or remove the change to Processing Indication Auto
   * @param error true there is an error false there is not an error
   */
  def setChangeToAuto(error: Boolean) {
    if (error)
      errors = errors | changeToAuto
    else
      errors = errors & (all ^ changeToAuto)
  }

  /**
   * Are there any errors
   * @return true problems are found, false everything is OK
   */
  def hasErrors(): Boolean = {
    errors != 0
  }

  /**
   * has the license check failed
   * @return true problems are found, false license is OK
   */
  def hasLicenseCheckFailed(): Boolean = {
    (errors & licenseCheck) != 0
  }
  /**
   * has the country check failed
   * @return true problems are found, false country is OK
   */
  def hasCountryCheckFailed(): Boolean = {
    (errors & countryCheck) != 0
  }
  /**
   * has the check failed
   * @return true problems are found, false no check problems
   */
  def hasErrorCheckFailed(): Boolean = {
    (errors & errorCheck) != 0
  }

  /**
   * has the license check failed
   * @return true problems are found, false license is OK
   */
  def hasLicenseFormatCheckFailed(): Boolean = {
    (errors & licenseFormatCheck) != 0
  }

  /**
   * has the check on the correct matching of images failed
   * @return true problems are found, false no check problems
   */
  def hasErrorMatchingCheckFailed(): Boolean = {
    (errors & errorMatchingCheck) != 0
  }

  /**
   * has the check on the correct matching of images failed
   * @return true problems are found, false no check problems
   */
  def hasCountryChange(): Boolean = {
    (errors & changeCountry) != 0
  }

  /**
   * has the check on the correct matching of images failed
   * @return true problems are found, false no check problems
   */
  def hasChangeToMobi(): Boolean = {
    (errors & changeToMobi) != 0
  }

  /**
   * has the check on the correct matching of images failed
   * @return true problems are found, false no check problems
   */
  def hasChangeToAuto(): Boolean = {
    (errors & changeToAuto) != 0
  }

  /**
   * Add a RecognitionResult to the list
   * @param result new recognize result
   */
  def addRecognitionResult(result: RecognitionResult) {
    recognitionResults :+= result
  }

  /**
   * get all the RecognitionResult
   * @return list of recognition results
   */
  def getRecognitionResults(): Seq[RecognitionResult] = {
    recognitionResults
  }

  /**
   * Get the error mask
   * @return 0 no errors else one or more errors found
   */
  def getErrors(): Int = {
    errors
  }

  override def toString: String = {
    "errors=%d recognitionResult %s".format(errors, recognitionResults)
  }
}

/**
 * Keepalive object used to check if there are old waiting reuqests
 */
object KeepAlive

/**
 * Check the license and country found in the image of a Registration
 * @param recognizeImage Reference to actor that recognize the license and country on a image
 * @param timeoutImageResponse Timeout within the complete check has to be done. keep in mind that when there are a lot of images it can take hours
 * @param recognizerCountries list of supported countries
 * @param recognizerCountryMinConfidence minimum country confidence before the result will be used
 */
class CheckVehicleRegistration(recognizeImage: ActorRef,
                               timeoutImageResponse: Duration,
                               recognizerCountries: Seq[String],
                               recognizerCountryMinConfidence: Int,
                               recognizeWithARHAgain: Boolean) extends Actor with ActorLogging {
  /**
   * List of all processing requests
   */
  private var requests = Map[KeyWithTimeAndId, RegistrationImageState]()

  private var violationRequests = Map[KeyWithTimeAndId, ViolationImagesState]()
  /**
   * Timer to check for old requests
   */
  private val timeoutSchedule = Some(context.system.scheduler.schedule(1.hour, 1.hour, self, KeepAlive))

  /**
   * Method called when stopping the actor
   */
  override def postStop() {
    timeoutSchedule.foreach(_.cancel())
    super.postStop()
  }

  /**
   * Receiving messages
   * @return
   */
  def receive = LoggingReceive {
    case request: RegistrationImageCheckRequest ⇒ {
      // Get the Registration Metadata; could be the entry or exit
      val registration = request.registration

      // Create an empty ImageCheckResult object. Within this object the Dacolian/ARH2 Recognition result is stored.
      val checkResult = new ImageCheckResult()

      // Add the previous ARH recognition results to the 'total' result
      checkResult.addRecognitionResult(RecognitionResult(RecognitionResult.ARH1, request.registration))

      // Add the image check request for this image (entry/exit) to the hashmap. This hashmap is used to verify if all
      // image requests are returned.
      requests += KeyWithTimeAndId(registration.eventTimestamp, registration.eventId) ->
        new RegistrationImageState(
          request,
          sender,
          System.currentTimeMillis(),
          RegistrationImageState.WaitForDacolian,
          checkResult)

      log.info("Send Registration [{}] to Dacolian LicenseReader", registration)

      recognizeImage ! ImageRecognitionRequest(Recognizer.INTRADA, registration)
    }

    case request: ImagesCheckRequestViolation ⇒ {
      // Get the entry and exit Registration Metadata
      val entryRegistration = request.entryRegistration

      val exitRegistration = request.exitRegistration

      // Create an empty ImageCheckResult object. Within this object the Intrada Recognition result is stored.
      val checkResult = new ImageCheckResult()

      // Add the previous ARH recognition results to the 'total' result
      checkResult.addRecognitionResult(RecognitionResult(RecognitionResult.ARH_ENTRY, entryRegistration))
      checkResult.addRecognitionResult(RecognitionResult(RecognitionResult.ARH_EXIT, exitRegistration))

      // Add the image check request for the images of this violation image (entry and exit) to the hashmap.
      // This hashmap is used to verify if all image requests are returned.
      violationRequests += KeyWithTimeAndId(exitRegistration.eventTimestamp, exitRegistration.eventId) ->
        new ViolationImagesState(
          request,
          sender,
          System.currentTimeMillis(),
          RegistrationImageState.WaitForDacolian,
          checkResult)

      log.info("Send Registration [{}] to Intrada LicenseReader Interface2", exitRegistration)

      recognizeImage ! ImagesRecognitionRequestForViolation(Recognizer.INTRADA_INTERFACE_2,
        entryRegistration,
        exitRegistration)
    }

    case result: ImageRecognitionResult ⇒ {
      val inputVmd = result.inputVehicleMetadata
      val key = KeyWithTimeAndId(inputVmd.eventTimestamp, inputVmd.eventId)

      result.recognizer match {
        case Recognizer.INTRADA_INTERFACE_2 ⇒ {
          log.info("Processing a Intrada2 response")
          val requestData = violationRequests.get(key)

          // Process the Images Recognition
          requestData.foreach(request ⇒ {
            (result.recognizer, request.state) match {
              case (Recognizer.INTRADA_INTERFACE_2, RegistrationImageState.WaitForDacolian) ⇒ {
                // Add the Intrada recognition result to the other results
                val resultCheck = request.result

                result.recognitionResult match {
                  case Left(message) ⇒
                    //both results are empty so result is only correct when country isn't supported
                    resultCheck.addRecognitionResult(RecognitionResult(RecognitionResult.INTRADA2, message))
                  case Right(checkedMetadata) ⇒
                    resultCheck.addRecognitionResult(RecognitionResult(RecognitionResult.INTRADA2, checkedMetadata))
                }

                // Delete the Intrada2 request from the queue
                violationRequests -= key

                //send response back to the requester. (Actor with class CheckViolation)
                request.applicant ! new ImagesCheckResponseViolation(request.request.reference,
                  request.request.exitRegistration,
                  resultCheck)

              }

              case (Recognizer.INTRADA_INTERFACE_2, _) ⇒ {
                log.warning("Unexpected INTRADA_2 result %s: for %s".format(result, request))
              }
            }
          })
        }

        case _ ⇒ {
          val requestData = requests.get(key)

          //process recognition
          requestData.foreach(request ⇒ {
            (result.recognizer, request.state) match {
              case (Recognizer.INTRADA, RegistrationImageState.WaitForDacolian) ⇒ {
                val resultCheck = processIntradaRecognition(request, result.recognitionResult)

                // Is the switch for second ARH recognition active (Config Zookeeper)
                if (recognizeWithARHAgain) {
                  // OK the switch is ON, but is it really necessary
                  if (needSecondStep(resultCheck)) {
                    log.info("Send Registration [{}] to ARH LicenseReader", request.request.registration)

                    recognizeImage ! ImageRecognitionRequest(Recognizer.ARH, request.request.registration)

                    // Replace the Dacolian request with the new ARH request
                    requests += key -> request.copy(state = RegistrationImageState.WaitForARH, result = resultCheck)
                  } else {
                    // Delete the Dacolian request from the queue
                    requests -= key

                    //send response back to the requester. (Actor with class CheckViolation)
                    request.applicant ! new RegistrationImageCheckResponse(request.request.reference, request.request.registration, resultCheck)
                  }
                } else {
                  // Delete the Dacolian request from the queue
                  requests -= key

                  //send response back to the requester. (Actor with class CheckViolation)
                  request.applicant ! new RegistrationImageCheckResponse(request.request.reference,
                    request.request.registration,
                    resultCheck)
                }
              }

              case (Recognizer.ARH, RegistrationImageState.WaitForARH) ⇒ {
                val resultCheck = ProcessARHRecognition(request, result.recognitionResult)
                requests -= key
                //send response
                request.applicant ! new RegistrationImageCheckResponse(request.request.reference, request.request.registration, resultCheck)
              }

              case (Recognizer.ARH, _) ⇒ {
                log.warning("Unexpected ARH result %s: for %s".format(result, request))
              }

              case (Recognizer.INTRADA, _) ⇒ {
                log.warning("Unexpected INTRADA result %s: for %s".format(result, request))
              }
            }
          })
        }
      }
    }

    case KeepAlive ⇒ {
      //cleanup registrations which are older than a day
      val oldMsec = System.currentTimeMillis() - timeoutImageResponse.toMillis

      val (oldRequests, rest) = requests.partition { case (key, value) ⇒ value.startTime < oldMsec }
      requests = rest
      val result = new ImageCheckResult()
      result.setErrorCheck(true)

      oldRequests.values.foreach(req ⇒ {
        log.error("Failed to check registration image: Timeout for recognition request reference=%s registration=%s".format(req.request.reference, req.request.registration))
        req.applicant ! RegistrationImageCheckResponse(req.request.reference, req.request.registration, result)
      })
    }
  }

  /**
   * Process the INTRADA recognize results
   * @param requestData the stored request data
   * @param recognitionResult the recognition result
   * @return the result of the check with INTRADA
   */
  private def processIntradaRecognition(requestData: RegistrationImageState,
                                        recognitionResult: Either[String, VehicleMetadata]): ImageCheckResult = {
    val originalCountry = requestData.request.registration.country.map(_.value)
    val isCountrySupported = recognizerCountries.contains(originalCountry.getOrElse("")) || recognizerCountries.contains("ALL")
    val originalLicense = requestData.request.registration.license.map(_.value).getOrElse("")
    val checkResult = requestData.result

    recognitionResult match {
      case Left(message) ⇒
        //both results are empty so result is only correct when country isn't supported
        checkResult.addRecognitionResult(RecognitionResult(RecognitionResult.INTRADA, message))

        log.info("Recognition error for second recognition of [{}] picture of [{}]: [{}]",
          requestData.request.reference,
          requestData.request,
          message)

        if (isCountrySupported) {
          log.info("[%s] supported Country - Set LicenseCheck AND CountryCheck error".format(originalCountry.getOrElse("")))

          //license is Not OK
          checkResult.setErrorLicenseCheck(true)
          //country is Not OK
          checkResult.setErrorCountryCheck(true)
        } else {
          log.info("[%s] is not contract country  - Do not set Check errors".format(originalCountry.getOrElse("")))
        }

      case Right(checkedMetadata) ⇒
        checkResult.addRecognitionResult(RecognitionResult(RecognitionResult.INTRADA, checkedMetadata))

        log.info("[{}] - Intrada License [{}] Country [{}]", checkedMetadata.eventId, checkedMetadata.license, checkedMetadata.country)

        val checkedLicense = checkedMetadata.license.map(_.value.filter(_.isLetterOrDigit)).getOrElse("")

        val checkedCountry = checkedMetadata.country.map(_.value)
        val canBeUsed = canSecondRecognitionBeUsed(originalCountry, recognitionResult)

        val countryCheck = !(isCountrySupported || canBeUsed) || (checkedCountry == originalCountry)
        val licenseCheck = !(isCountrySupported || canBeUsed) || (checkedLicense == originalLicense)

        if (!licenseCheck) {
          log.info("Different license for second recognition of [{}] picture of [{}]: Intrada License [{}]",
            requestData.request.reference,
            requestData.request,
            checkedLicense)
        }

        if (!countryCheck) {
          log.info("Different country for second recognition of [{}] picture of [{}]: Intrada Country [{}]",
            requestData.request.reference,
            requestData.request,
            checkedCountry)
        }

        checkResult.setErrorLicenseCheck(!licenseCheck)
        checkResult.setErrorCountryCheck(!countryCheck)
    }
    checkResult
  }

  /**
   * Is the result of the recognizer reliable
   * @param originalCountry the country found by the first recognizer
   * @param checkedResult the result of the recognizer
   * @return true result is reliable false result isn't reliable
   */
  private def canSecondRecognitionBeUsed(originalCountry: Option[String], checkedResult: Either[String, VehicleMetadata]): Boolean = {
    val isCountrySupported = recognizerCountries.contains(originalCountry.getOrElse("")) || recognizerCountries.contains("ALL")
    checkedResult match {
      case Right(checkedMetadata) ⇒ {
        if (isCountrySupported) {
          true
        } else {
          val checkedCountryConfidence = checkedMetadata.country.map(_.confidence).getOrElse(0)
          (checkedCountryConfidence >= recognizerCountryMinConfidence)
        }
      }
      case other ⇒ false
    }
  }

  /**
   * Do we need another ARH step to give a reliable result
   * Rule 1: ARH1=BE And INTRADA<>BE And ARH2=BE and nr characters lic = 7 => Country = BE
   * Rule 2: ARH1=DE And ARH2=DE Country-Conf-ARH2 >= 20 lic-ARH1=lic-ARH2 => Country = DE
   * Rule 3: ARH1=FR And INTRADA<>NL and ARH2=FR lic-ARH1=lic-ARH2 => Country FR
   * Rule 4: ARH1=NL And ARH2=NL lic-ARH1=lic-ARH2 => Country NL
   * @param result
   * @return
   */
  private def needSecondStep(result: ImageCheckResult): Boolean = {
    if (result.getErrors() == 0) {
      //no errors found => second check not necessary
      return false
    }
    val arh = result.getRecognitionResults().find(_.recognizerId == RecognitionResult.ARH1)
    val arhCountry = arh.flatMap(_.country).map(_.value).getOrElse("")
    arhCountry match {
      case "BE" ⇒ { //Rule 1
        val intrada = result.getRecognitionResults().find(_.recognizerId == RecognitionResult.INTRADA)
        val intradaCountry = intrada.flatMap(_.country).map(_.value).getOrElse("")
        intradaCountry != "BE"
      }
      case "DE" ⇒ true //Rule 2
      case "FR" ⇒ { //Rule 3
        val intrada = result.getRecognitionResults().find(_.recognizerId == RecognitionResult.INTRADA)
        val intradaCountry = intrada.flatMap(_.country).map(_.value).getOrElse("")
        intradaCountry != "NL"
      }
      case "NL"  ⇒ true //Rule 4
      case other ⇒ false
    }
  }

  /**
   * Process the second ARH result
   *
   * Rule 1: ARH1=BE And INTRADA<>BE And ARH2=BE and lic-ARH1=lic-ARH2 and nr characters lic = 7 => Country = BE
   * Rule 2: ARH1=DE And ARH2=DE Country-Conf-ARH2 >= 20 lic-ARH1=lic-ARH2 => Country = DE
   * Rule 3: ARH1=FR And INTRADA<>NL and ARH2=FR lic-ARH1=lic-ARH2 => Country FR
   * Rule 4: ARH1=NL And ARH2=NL lic-ARH1=lic-ARH2 => Country NL
   * @param requestData the stored request data
   * @param recognitionResult the recognition result
   * @return the result of the check with ARH
   */
  private def ProcessARHRecognition(requestData: RegistrationImageState,
                                    recognitionResult: Either[String, VehicleMetadata]): ImageCheckResult = {
    val result = requestData.result

    recognitionResult match {
      case Left(message) ⇒ {
        //ARH2 failed so we can't correct result
        result.addRecognitionResult(RecognitionResult(RecognitionResult.ARH2, message))

        log.info("Recognition error for second ARH recognition of [{}] picture of [{}]: [{}]",
          requestData.request.reference,
          requestData.request,
          message)
      }
      case Right(checkedMetadata) ⇒ {
        val arh2 = RecognitionResult(RecognitionResult.ARH2, checkedMetadata)
        result.addRecognitionResult(arh2)

        log.info("[{}] - ARH2 License [{}] Country [{}]",
          checkedMetadata.eventId, checkedMetadata.license, checkedMetadata.country)

        val arh = result.getRecognitionResults().find(_.recognizerId == RecognitionResult.ARH1)
        val arhCountry = arh.flatMap(_.country).map(_.value).getOrElse("")
        arhCountry match {
          case "BE" ⇒ { //Rule 1
            val intrada = result.getRecognitionResults().find(_.recognizerId == RecognitionResult.INTRADA)
            val intradaCountry = intrada.flatMap(_.country).map(_.value).getOrElse("")
            if (intradaCountry != "BE") {
              val lic1 = arh.flatMap(_.license).map(_.value).getOrElse("")
              val lic2 = arh2.license.map(_.value).getOrElse("")
              val country2 = arh2.country.map(_.value).getOrElse("")
              if (country2 == "BE" && lic1 == lic2 && lic1.size == 7) {
                //correct errors
                log.info("Intrada Country not BE (%s) ARH2 Country is BE and ARH1 %s == %s ARH2 and License has 7 chars. Clear all errors.".
                  format(intradaCountry, lic1, lic2))
                result.clearErrors()
              }
            }
          }
          case "DE" ⇒ { //Rule 2
            val lic1 = arh.flatMap(_.license).map(_.value).getOrElse("")
            val lic2 = arh2.license.map(_.value).getOrElse("")
            val country2 = arh2.country.map(_.value).getOrElse("")
            val conf = arh2.country.map(_.confidence).getOrElse(0)
            if (country2 == "DE" && lic1 == lic2 && conf >= 20) {
              log.info("Intrada Country is DE and ARH1 %s == %s ARH2 and ARH2 License confidence %d >= 20. Clear all errors.".
                format(lic1, lic2, conf))
              //correct errors
              result.clearErrors()
            }
          }
          case "FR" ⇒ { //Rule 3
            val intrada = result.getRecognitionResults().find(_.recognizerId == RecognitionResult.INTRADA)
            val intradaCountry = intrada.flatMap(_.country).map(_.value).getOrElse("")
            if (intradaCountry != "NL") {
              val lic1 = arh.flatMap(_.license).map(_.value).getOrElse("")
              val lic2 = arh2.license.map(_.value).getOrElse("")
              val country2 = arh2.country.map(_.value).getOrElse("")
              if (country2 == "FR" && lic1 == lic2) {
                //correct errors
                result.clearErrors()
              }
            }
          }
          case "NL" ⇒ { //Rule 4
            val lic1 = arh.flatMap(_.license).map(_.value).getOrElse("")
            val lic2 = arh2.license.map(_.value).getOrElse("")
            val country2 = arh2.country.map(_.value).getOrElse("")
            if (country2 == "NL" && lic1 == lic2) {
              //correct errors
              result.clearErrors()
            }
          }
          case other ⇒ //do nothing
        }
      }
    }
    result
  }
}
