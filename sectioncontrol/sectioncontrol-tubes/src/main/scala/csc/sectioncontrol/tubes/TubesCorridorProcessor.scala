package csc.sectioncontrol.tubes

import csc.sectioncontrol.enforce.{ SpeedEventData, SpeedIndicatorStatus }
import scala.annotation.tailrec
import csc.sectioncontrol.messages.Interval

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 11/4/14.
 */

/**
 * Process the parseResult per corridor
 * @param setting           TubesSetting containing chartCodes and messagePatterns
 * @param parseResult       result from parsing the tubes file
 * @param expectedTimeFrom  start of timeframe
 * @param expectedTimeUntil end of timeframe
 */
class TubesCorridorProcessor(setting: TubesSetting,
                             parseResult: TubesParseResult,
                             expectedTimeFrom: Long,
                             expectedTimeUntil: Long) {

  lazy val startLeftUndetermined = SpeedDataImpl(expectedTimeFrom, setting.corridorId, 0, Left(SpeedIndicatorStatus.Undetermined))
  lazy val startRightNone = SpeedDataImpl(expectedTimeFrom, setting.corridorId, 0, Right(None))
  lazy val endRightNone = SpeedDataImpl(expectedTimeUntil, setting.corridorId, 0, Right(None))

  var dstPeriod: Option[Interval] = None

  def getSpeedData: List[SpeedEventData] = {
    val dcfStatus = parseResult.dcf77Status.getOrElse(DCF77State.NOK)
    // If dcsStatus Not OK, speeds cannot be determined
    if (dcfStatus == DCF77State.NOK)
      List(startLeftUndetermined, endRightNone)
    else {
      // Only process status from correct location and events from correct location
      // and in given timeframe
      val statusList: List[TubesEvent] = parseResult.statusList.filter(isCorrectLocation)
      val events = parseResult.eventList.filter(isCorrectLocation).filter(isCorrectTime)
      val accu = {
        if (statusList.isEmpty)
          List(startRightNone)
        else
          List(startLeftUndetermined)
      }
      processEvents(statusList, events, accu)
    }
  }

  /**
   * process events in order to accumulate a List[SpeedEventData]
   * @param statusList
   * @param events
   * @param accu
   * @return
   */
  @tailrec
  private def processEvents(statusList: List[TubesEvent], events: List[TubesEvent], accu: List[SpeedEventData]): List[SpeedEventData] = {
    events match {
      case Nil ⇒
        accu
      case head :: tail ⇒
        val (newStatusList, newAccu) = processEvent(statusList, head, accu)
        processEvents(newStatusList, tail, newAccu)
    }
  }

  /**
   * Process a single event from the list
   * @param statusList  initial status for this corridor
   * @param event       the event to process
   * @param accu        until now accumulated SpeedEventData
   * @return            accumulated SpeedEventData after processing the event
   */
  private def processEvent(statusList: List[TubesEvent], event: TubesEvent, accu: List[SpeedEventData]): ( /*status:*/ List[TubesEvent], /*accu*/ List[SpeedEventData]) = {
    event.eventType match {
      case TubesEventType.IN  ⇒ processInEvent(statusList, event, accu)
      case TubesEventType.ACK ⇒ processAckEvent(statusList, event, accu)
      case TubesEventType.OUT ⇒ processOutEvent(statusList, event, accu)
    }
  }

  /**
   *
   * @param statusList current state of events
   * @param event      event being processes
   * @param accu       current accu with SpeedData elements
   * @return          (new statuslist , new accu)
   */
  private def processInEvent(statusList: List[TubesEvent], event: TubesEvent, accu: List[SpeedEventData]): ( /*status:*/ List[TubesEvent], /*accu*/ List[SpeedEventData]) = {
    statusList match {
      case Nil ⇒
        val newAccu = accu :+ SpeedDataImpl(event.timestamp.getTime, setting.corridorId, 0, Left(SpeedIndicatorStatus.Undetermined))
        (List(event), newAccu)
      case _ ⇒ {
        if (statusList.count(_.matchesEvent(event)) == 0) {
          /*
           * if there's no matching even on the statusList, push current event
           * onto the statusList
           */
          (statusList :+ event, accu)
        } else {
          /*
           * statusList already contains an entry with the chartCode and
           * message of this event. This situation should never occur. For
           * practical reasons it is ignored
           *
           */
          (statusList, accu)
        }
      }
    }
  }

  /**
   * process an event of type ACK.
   * @param statusList  current status
   * @param event       event to be processed
   * @param accu        accumulated list of SpeedDataEvents
   * @return            (statusList, speedEventDataList) after processing the ACK
   */
  private def processAckEvent(statusList: List[TubesEvent], event: TubesEvent, accu: List[SpeedEventData]): ( /*status:*/ List[TubesEvent], /*accu*/ List[SpeedEventData]) = {
    if (statusList.count(_.matchesEvent(event)) == 0)
      // If no matching event in statusList, process as event of type IN
      processInEvent(statusList, event.copy(eventType = TubesEventType.IN), accu)
    else
      // ACK with corresponding event already on the statusList. Ignore the ACK
      (statusList, accu)
  }

  /**
   * Process a single event of type OUT
   * @param statusList  current status
   * @param event       the OUT event to be processed
   * @param accu        accumulated SpeedEventData
   * @return            (newStatusList, newAccu)
   */
  private def processOutEvent(statusList: List[TubesEvent], event: TubesEvent, accu: List[SpeedEventData]): ( /*status:*/ List[TubesEvent], /*accu*/ List[SpeedEventData]) = {
    statusList.count(_.matchesEvent(event)) match {
      case 1 ⇒ {
        val newStatusList = statusList.filterNot(_.matchesEvent(event))
        val newAccu =
          if (newStatusList.isEmpty)
            accu :+ SpeedDataImpl(event.timestamp.getTime, setting.corridorId, 0, Right(None))
          else
            accu
        (newStatusList, newAccu)
      }
      case _ ⇒ (statusList, accu) // Should never occur
    }
  }

  private def isCorrectLocation(event: TubesEvent): Boolean = {
    event.matchesSetting(setting)
  }

  private def isCorrectTime(event: TubesEvent): Boolean = {
    val timestamp = event.timestamp.getTime
    timestamp >= expectedTimeFrom && timestamp < expectedTimeUntil
  }
}

private[tubes] case class SpeedDataImpl(effectiveTimestamp: Long,
                                        corridorId: Int,
                                        sequenceNumber: Long,
                                        speedSetting: Either[SpeedIndicatorStatus.Value, Option[Int]]) extends SpeedEventData
