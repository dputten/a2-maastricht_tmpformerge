package csc.sectioncontrol.tubes

import java.io.File
import java.text.SimpleDateFormat
import akka.actor.{ ActorRef, ActorLogging, Actor }
import akka.event.LoggingReceive
import csc.sectioncontrol.enforce.SpeedEventData
import csc.akkautils.RunToken
import csc.sectioncontrol.messages.{ TimeChanges, Interval }
import csc.sectioncontrol.storage.Paths
import java.util.Date
import csc.curator.utils.{ CuratorActor, Curator }

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 11/4/14.
 */

class ProcessTubesData(systemId: String,
                       protected val curator: Curator,
                       fmt: SimpleDateFormat) extends CuratorActor {

  protected def receive = LoggingReceive {
    case source: TubesSource ⇒ {
      log.info("Received " + source)
      sender ! processTubesSource(source)
    }
  }

  protected def processTubesSource(source: TubesSource): Either[TubesProcessingFailed, TubesProcessingSucceeded] = {
    val eitherResult = TubesParser.parse(new File(source.filename), fmt)
    eitherResult.fold((ex) ⇒ {
      log.error(ex, "Error parsing TuBes file %s".format(source.filename))
      Left(TubesProcessingFailed(source.filename, source.runner, ex))
    }, (parseResult) ⇒ {
      log.info("Parsing {} succeeded, Result: {}", source.filename, parseResult)
      // Adjust timestamps for Daylight Savings Time interval.
      val newParseResult = getDstInterval match {
        case None ⇒ {
          val timesAsLong = parseResult.eventList.map { evt ⇒ evt.timestamp.getTime }
          log.info("ParseResult eventTimes: {}", timesAsLong)
          parseResult
        }
        case Some(interval) ⇒ {
          val oldTimes = parseResult.eventList.map { evt ⇒ evt.timestamp.getTime }
          val newEvents = parseResult.eventList.map { evt ⇒ evt.adjustToDstInterval(interval) }
          val newTimes = newEvents.map { evt ⇒ evt.timestamp.getTime }
          log.info("Adjusted timestamps to daylight savings interval: {}", oldTimes.zip(newTimes))
          parseResult.copy(eventList = newEvents)
        }
      }
      try {
        source.settings.foreach((setting: TubesSetting) ⇒ {
          log.info("Creating TubesCorridorProcessor for {}", setting)
          val processor = new TubesCorridorProcessor(setting, newParseResult, source.expectedTimeFrom, source.expectedTimeUntil)
          val speedEvents = processor.getSpeedData
          log.info("Calculated speedEvents: {}", speedEvents)
          speedEvents.foreach((speedEvent: SpeedEventData) ⇒ source.speedLog ! speedEvent)
        })
        Right(TubesProcessingSucceeded(source.filename, source.runner))
      } catch {
        case ex: Exception ⇒
          Left(TubesProcessingFailed(source.filename, source.runner, ex))
      }
    })
  }

  /**
   * retrieve the Daylight Savings Interval from ZooKeeper
   * @return the optional daylight savings interval.
   */
  private def getDstInterval: Option[Interval] = {
    val maybeTimeChange: Option[TimeChanges] = curator.get[TimeChanges](Paths.Systems.getTimewatchStatePath(systemId))
    maybeTimeChange match {
      case None             ⇒ None
      case Some(timeChange) ⇒ timeChange.dstChange
    }
  }
}

/**
 *
 * @param corridorId  Identification of corridor
 * @param chartCodes chartCodes referring to this corridor
 * @param messagePatterns messagePattern referring to this corridor in case
 *                       the chartCode is empty
 */
case class TubesSetting(corridorId: Int, chartCodes: List[String], messagePatterns: List[String])

case class TubesSource(filename: String,
                       runToken: RunToken,
                       runner: ActorRef,
                       speedLog: ActorRef,
                       settings: List[TubesSetting],
                       expectedTimeFrom: Long,
                       expectedTimeUntil: Long) {
  require(filename != null)
  require(!filename.trim().isEmpty)
  require(speedLog != null)
  require(settings != null)
  require(!settings.isEmpty)
}

case class TubesProcessingSucceeded(filename: String, runner: ActorRef)
case class TubesProcessingFailed(filename: String, runner: ActorRef, exception: Exception)