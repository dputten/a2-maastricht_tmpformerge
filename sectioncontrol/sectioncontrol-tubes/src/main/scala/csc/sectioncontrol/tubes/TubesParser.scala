package csc.sectioncontrol.tubes

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 10/28/14.
 */
import com.github.tototoshi.csv._

import java.io.File
import java.util.{ Locale, TimeZone, Date }
import java.text.SimpleDateFormat
import csc.akkautils.DirectLogging
import csc.sectioncontrol.tubes.DCF77State.DCF77State
import csc.sectioncontrol.messages.Interval

object TubesEventType extends Enumeration {
  type TubesEventType = Value
  val IN, ACK, OUT = Value
}

object DCF77State extends Enumeration {
  type DCF77State = Value
  val OK, NOK = Value
}

class TubesProcessingException(message: String, cause: Throwable)
  extends Exception(message) {
  if (cause != null)
    initCause(cause)

  def this(message: String) = this(message, null)
}

class TubesParseException(message: String, line: Int) extends TubesProcessingException("Line %d: ".format(line) + message)

case class TubesEvent(timestamp: Date, eventType: TubesEventType.Value, chartCode: String, message: String) {

  def this(dateStr: String, eventTypeStr: String, chartCode: String, message: String, fmt: SimpleDateFormat) =
    this(TubesDateFormat.parse(dateStr.trim, fmt), TubesEventType.withName(eventTypeStr.trim), chartCode.trim, message.trim)

  def this(s: Seq[String], fmt: SimpleDateFormat) = this(s.apply(0).trim, s.apply(2).trim, s.apply(5).trim, s.apply(9).trim, fmt)

  def matchesEvent(that: TubesEvent): Boolean = {
    that.chartCode == chartCode && that.message == message
  }

  def matchesSetting(setting: TubesSetting): Boolean = {
    setting.chartCodes.contains(chartCode) ||
      setting.messagePatterns.foldLeft[Boolean](false)((result: Boolean, pattern: String) ⇒ result | message.matches(pattern))
  }

  /**
   * Check if this event occurred during Daylight Savings Interval.
   * If so then adjust the timestamp. IN and ACK events move to the start of
   * the interval, OUT events move to the end of the interval
   * @param i the daylight savings interval
   * @return new event, possibly with an adjusted timestamp.
   */
  def adjustToDstInterval(i: Interval): TubesEvent = {
    val evtTime = timestamp.getTime
    if (evtTime >= i.from && evtTime <= i.to) {
      if (TubesEventType.OUT == eventType) {
        this.copy(timestamp = new Date(i.to))
      } else {
        this.copy(timestamp = new Date(i.from))
      }
    } else {
      this
    }
  }
}

object TubesEvent {
  def apply(dateStr: String, eventTypeStr: String, chartCode: String, message: String, fmt: SimpleDateFormat): TubesEvent = {
    new TubesEvent(dateStr, eventTypeStr, chartCode, message, fmt)
  }

  def apply(strings: Seq[String], fmt: SimpleDateFormat): TubesEvent = {
    val expectedFields = 12
    val actualFields = strings.length
    if (actualFields != 12)
      throw new TubesProcessingException("Invalid number of fields for TubesEvent. Expected: %d, actual: %d".format(expectedFields, actualFields))
    new TubesEvent(strings, fmt)
  }
}

case class TubesParseResult(timestampStatus: Option[Date] = None, statusList: List[TubesEvent] = Nil,
                            timeStampEvents: Option[Date] = None, eventList: List[TubesEvent] = Nil,
                            timeStampDCF77: Option[Date] = None, dcf77Status: Option[DCF77State] = None)

object TubesDateFormat {
  val fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
  fmt.setTimeZone(TimeZone.getTimeZone("Europe/Amsterdam"))

  def parse(s: String, theFormat: SimpleDateFormat = TubesDateFormat.fmt) = theFormat.parse(s)
  def format(d: Date, theFormat: SimpleDateFormat = TubesDateFormat.fmt) = theFormat.format(d)
}

object TubesTimestampParser {
  val fmt = new SimpleDateFormat("MMM dd HH:mm:ss yyyy", Locale.US)
  fmt.setTimeZone(TimeZone.getTimeZone("Europe/Amsterdam"))

  def parse(prefix: String, line: String, format: SimpleDateFormat = TubesTimestampParser.fmt): Date = {
    // <prefix>Mon Sep 01 00:06:00 2014
    val patternString = prefix + "... (.*)"
    val pattern = patternString.r
    val pattern(timeString) = line
    fmt.parse(timeString)
  }
}

class TubesParser(fmt: SimpleDateFormat) extends DirectLogging {
  type CSV = List[List[String]]

  private var parseResult = TubesParseResult()
  private var lineNr = 0

  implicit object MyFormat extends DefaultCSVFormat {
    override val quoteChar: Char = '\''
  }

  protected def getLine(source: CSV): (List[String], CSV) = {
    source match {
      case Nil ⇒ throw new TubesParseException("Unexpected End Of Input", lineNr)
      case List("") :: tail ⇒
        lineNr = lineNr + 1
        getLine(tail)
      case head :: tail ⇒
        lineNr = lineNr + 1
        (head, tail)
    }
  }

  protected def verifyChecksum(text: String, soll: Int): Boolean = {
    val patternString = """Checksum: ([\d]+)"""
    val pattern = patternString.r
    if (text.matches(patternString)) {
      val pattern(numberString) = text
      val ist = numberString.toInt
      if (ist != soll)
        throw new TubesParseException("Invalid checksum. Soll=%d, Ist=%d".format(soll, ist), lineNr)
    } else
      throw new TubesParseException("Checksum message expected.", lineNr)
    true
  }

  protected def parseDCFStatus(source: CSV): (DCF77State, CSV) = {
    val regexString = "Radio clock state: (OK|NOK)"
    val regex = regexString.r
    val (line, rest) = getLine(source)
    val text = line.apply(0)
    if (text.matches(regexString)) {
      val regex(stateString) = text
      (DCF77State.withName(stateString), rest)
    } else
      throw new TubesParseException("Radio clock state expected but found '%s'".format(text), lineNr)
  }

  protected def parseEvents(source: CSV): (List[TubesEvent], CSV) = {

    def doParseEvents(lines: CSV, accu: List[TubesEvent] = Nil, checksum: Int = 0): (List[TubesEvent], Int, CSV) = {
      lines match {
        case head :: tail if head.length == 12 ⇒
          try {
            val newAccu = TubesEvent(head, fmt) :: accu
            val newChecksum = checksum + head.apply(1).trim.toInt
            lineNr = lineNr + 1
            doParseEvents(tail, newAccu, newChecksum)
          } catch {
            case e: TubesProcessingException ⇒ throw new TubesParseException(e.getMessage, lineNr)
          }
        case Nil ⇒
          throw new TubesParseException("Unexpected End Of Input.", lineNr)
        case linesLeft: CSV ⇒ (accu, checksum, linesLeft)
      }
    }
    val (events, checksum, source1) = doParseEvents(source)
    val (checksumList, source2) = getLine(source1)
    verifyChecksum(checksumList.apply(0), checksum)
    (events.reverse, source2)

  }

  protected def parseTimeStamp(source: CSV, prefix: String): (Date, CSV) = {
    val (line, rest) = getLine(source)
    val timeStamp = TubesTimestampParser.parse(prefix, line.apply(0), fmt)
    (timeStamp, rest)
  }

  protected def parseTubes(source: CSV) = {
    // Get the timestamp for the statuslist
    val (timestamp1, source1) = parseTimeStamp(source, "Actuele status tijdstip: ")
    parseResult = parseResult.copy(timestampStatus = Some(timestamp1))

    // Skip headerline for status and parse status
    val (_, source2) = getLine(source1)
    val (status, source3) = parseEvents(source2)
    parseResult = parseResult.copy(statusList = status)

    // Get the timestamp for the eventlist
    val (timestamp2, source4) = parseTimeStamp(source3, "Archief afgelopen 24 uur tijdstip: ")
    parseResult = parseResult.copy(timeStampEvents = Some(timestamp2))

    // Skip headerline for events and parse events
    val (_, source5) = getLine(source4)
    val (events, source6) = parseEvents(source5)
    parseResult = parseResult.copy(eventList = events)

    val (timestamp3, source7) = parseTimeStamp(source6, "Status DCF klok tijdstip: ")
    parseResult = parseResult.copy(timeStampDCF77 = Some(timestamp3))

    val (dcf77Status, _) = parseDCFStatus(source7)
    parseResult = parseResult.copy(dcf77Status = Some(dcf77Status))

  }

  protected def parseCSV(f: File): List[List[String]] = {
    val reader = CSVReader.open(f)
    val result = try {
      reader.all()
    } catch {
      case e: Exception ⇒ {
        println("*** oops *** ")
        Nil
      }
    } finally {
      reader.close()
    }
    result.map(line ⇒
      line.size match {
        case 0 ⇒ line
        case 1 ⇒
          List(line.apply(0).trim)
        case _ ⇒ line
      })
  }

  def parse(f: File): Either[Exception, TubesParseResult] = {
    parseResult = TubesParseResult()
    val source: List[List[String]] = try {
      parseCSV(f)
    } catch {
      case e: Exception ⇒
        Nil
    }
    lineNr = 1
    try {
      parseTubes(source)
      Right(parseResult)
    } catch {
      case tpEx: TubesParseException ⇒
        Left(tpEx)
      case ex: Exception ⇒
        Left(new TubesParseException(ex.getMessage, lineNr))
    }
  }
}

object TubesParser {
  def parse(f: File, fmt: SimpleDateFormat = TubesDateFormat.fmt): Either[Exception, TubesParseResult] = {
    new TubesParser(fmt).parse(f)
  }
}