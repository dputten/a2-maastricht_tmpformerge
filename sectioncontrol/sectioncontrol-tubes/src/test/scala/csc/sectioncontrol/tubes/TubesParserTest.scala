package csc.sectioncontrol.tubes

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import java.net.URL
import java.io.File
import csc.akkautils.DirectLogging

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 10/28/14.
 */

class TubesParserTest extends WordSpec with MustMatchers with DirectLogging {
  type CSV = List[List[String]]

  val fileNames = Seq("test0.log", "test1.log", "test2.log",
    "test3.log", "test4.log", "test5.log", "test6.log")
  val files = fileNames.map((fname) ⇒ getClass.getClassLoader.getResource(fname)).
    map((u: URL) ⇒ new File(u.getPath))
  val parser = new TubesParser(TubesDateFormat.fmt)

  "parser" must {
    "parse correct CSV file" in {
      val tubesResult = TubesParser.parse(files(0))
      tubesResult.isLeft must be(true)
      tubesResult.fold(
        (ex) ⇒ {
          ex.getMessage must be("Line 15: Unparseable date: \"2015-03-07 01:39:00,000\"")
        },
        (parseResult) ⇒ { "oops" })
    }
    "parse correct file with status and events" in {
      val eitherParseResult = TubesParser.parse(files(1))
      val statusTimes = List(1406574159741L, 1406574159754L, 1406574159816L, 1406574159817L, 1406574159818L)
      val eventTimes = List(1409536910164L, 1409536910164L, 1409537879472l, 1409537879472l, 1409538115575l,
        1409538115575l, 1409538992179l, 1409538992180l, 1409584835244l, 1409584835244l, 1409585369254l, 1409585369254l)

      eitherParseResult.isRight must be(true)
      val parseResult = eitherParseResult.right.get
      parseResult.statusList.size must be(5)
      parseResult.eventList.size must be(12)
      parseResult.dcf77Status must be(Some(DCF77State.OK))
      parseResult.timestampStatus.get.getTime must be(1409522760000L)
      parseResult.timeStampEvents.get.getTime must be(1409609161000L)
      parseResult.timeStampDCF77.get.getTime must be(1409609167000L)
      parseResult.statusList.map(_.timestamp.getTime) must be(statusTimes)
      parseResult.eventList.map(_.timestamp.getTime) must be(eventTimes)
    }
    "parse correct file with status and no events" in {
      val eitherParseResult = TubesParser.parse(files(2))
      eitherParseResult.isRight must be(true)
      val parseResult = eitherParseResult.right.get
      parseResult.statusList.size must be(4)
      parseResult.eventList.size must be(0)
      parseResult.dcf77Status must be(Some(DCF77State.NOK))
      parseResult.timestampStatus.get.getTime must be(1410041160000L)
      parseResult.timeStampEvents.get.getTime must be(1410127561000L)
      parseResult.timeStampDCF77.get.getTime must be(1410127564000L)
    }
    "parse correct file with no status and no events" in {
      val eitherParseResult = TubesParser.parse(files(3))
      eitherParseResult.isRight must be(true)
      val parseResult = eitherParseResult.right.get
      parseResult.statusList.size must be(0)
      parseResult.eventList.size must be(0)
      parseResult.dcf77Status must be(Some(DCF77State.OK))
      parseResult.timestampStatus.get.getTime must be(1410041160000L)
      parseResult.timeStampEvents.get.getTime must be(1410127561000L)
      parseResult.timeStampDCF77.get.getTime must be(1410127564000L)
    }
    "detect checksum error" in {
      val eitherParseResult = TubesParser.parse(files(4))
      eitherParseResult.isLeft must be(true)
      eitherParseResult.left.get.getMessage must be("Line 11: Invalid checksum. Soll=1296679, Ist=1296678")
    }

    "correctly handle parser exceptions" in {
      val eitherParseResult = TubesParser.parse(files(5))
      eitherParseResult.isLeft must be(true)
      eitherParseResult.left.get.getMessage must be("Line 6: Checksum message expected.")
    }

    "correctly handle empty files" in {
      val eitherParseResult = TubesParser.parse(files(6))
      eitherParseResult.isLeft must be(true)
      eitherParseResult.left.get.getMessage must be("Line 1: Unexpected End Of Input")
    }
  }
}
