/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.tubes

import java.io.File
import java.net.URL

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.messages.StatsDuration
import csc.sectioncontrol.enforce.SpeedIndicatorStatus
import java.util.Date

class TubesCorridorProcessorTest extends WordSpec with MustMatchers {
  val stats = StatsDuration("20141201", "Europe/Amsterdam")
  val startTime = stats.start
  val endTime = stats.tomorrow.start
  val fmt = TubesDateFormat.fmt

  val eventTimeStr = "2014-12-01 04:01:50.164"
  val eventTime = fmt.parse(eventTimeStr).getTime

  val tubesFile = new File(getClass.getClassLoader.getResource("test1.log").getPath)
  val parser = new TubesParser(TubesDateFormat.fmt)

  "TubesCorridorProcessor getSpeedData" must {
    "correctly parse file" in {
      val setting = TubesSetting(corridorId = 1, chartCodes = List("code1", "code2"), messagePatterns = List(".*[Oo]ost.*"))
      true must be(true)
    }
    "include events with chartCodes included in settings" in {
      val setting = TubesSetting(corridorId = 1, chartCodes = List("code1", "code2"), messagePatterns = List(".*[Oo]ost.*"))
      val events = new TubesParseResult(dcf77Status = Some(DCF77State.OK), statusList = List(), eventList = List(
        TubesEvent(eventTimeStr, "IN", "code1", "message", fmt)))
      val processor = new TubesCorridorProcessor(setting = setting,
        parseResult = events,
        expectedTimeFrom = startTime,
        expectedTimeUntil = endTime)
      val resultSpeeds = processor.getSpeedData
      resultSpeeds.size must be(2)
      val speed = resultSpeeds(0)
      speed.speedSetting must be(Right(None))
      speed.effectiveTimestamp must be(startTime)
      val speed2 = resultSpeeds(1)
      speed2.speedSetting must be(Left(SpeedIndicatorStatus.Undetermined))
      speed2.effectiveTimestamp must be(eventTime)
    }
    "ignore events with chartCodes not included in settings" in {
      val setting = TubesSetting(corridorId = 1, chartCodes = List("code1", "code2"), messagePatterns = List(".*[Oo]ost.*"))
      val events = new TubesParseResult(dcf77Status = Some(DCF77State.OK), statusList = List(), eventList = List(
        TubesEvent(eventTimeStr, "IN", "code", "message", fmt)))
      val processor = new TubesCorridorProcessor(setting = setting,
        parseResult = events,
        expectedTimeFrom = startTime,
        expectedTimeUntil = endTime)
      val resultSpeeds = processor.getSpeedData
      resultSpeeds.size must be(1)
      val speed = resultSpeeds(0)
      speed.speedSetting must be(Right(None))
      speed.effectiveTimestamp must be(startTime)
    }
    "include start events with chartCodes included in settings" in {
      val setting = TubesSetting(corridorId = 1, chartCodes = List("code1", "code2"), messagePatterns = List(".*[Oo]ost.*"))
      val events = new TubesParseResult(dcf77Status = Some(DCF77State.OK), statusList = List(
        TubesEvent("2014-11-30 04:01:50.164", "IN", "code1", "message", fmt)), eventList = List())
      val processor = new TubesCorridorProcessor(setting = setting,
        parseResult = events,
        expectedTimeFrom = startTime,
        expectedTimeUntil = endTime)
      val resultSpeeds = processor.getSpeedData
      resultSpeeds.size must be(1)
      val speed = resultSpeeds(0)
      speed.speedSetting must be(Left(SpeedIndicatorStatus.Undetermined))
    }
    "ignore start events with chartCodes not included in settings" in {
      val setting = TubesSetting(corridorId = 1, chartCodes = List("code1", "code2"), messagePatterns = List(".*[Oo]ost.*"))
      val events = new TubesParseResult(dcf77Status = Some(DCF77State.OK), statusList = List(
        TubesEvent("2014-11-30 04:01:50.164", "IN", "code", "message", fmt)), eventList = List())
      val processor = new TubesCorridorProcessor(setting = setting,
        parseResult = events,
        expectedTimeFrom = startTime,
        expectedTimeUntil = endTime)
      val resultSpeeds = processor.getSpeedData
      resultSpeeds.size must be(1)
      val speed = resultSpeeds(0)
      speed.speedSetting must be(Right(None))
    }

    "include events with messagePattern included in settings" in {
      val setting = TubesSetting(corridorId = 1, chartCodes = List("code1", "code2"), messagePatterns = List(".*[Oo]ost.*"))
      val events = new TubesParseResult(dcf77Status = Some(DCF77State.OK), statusList = List(), eventList = List(
        TubesEvent(eventTimeStr, "IN", "code", "message oost", fmt)))
      val processor = new TubesCorridorProcessor(setting = setting,
        parseResult = events,
        expectedTimeFrom = startTime,
        expectedTimeUntil = endTime)
      val resultSpeeds = processor.getSpeedData
      resultSpeeds.size must be(2)
      val speed = resultSpeeds(0)
      speed.speedSetting must be(Right(None))
      speed.effectiveTimestamp must be(startTime)
      val speed2 = resultSpeeds(1)
      speed2.speedSetting must be(Left(SpeedIndicatorStatus.Undetermined))
      speed2.effectiveTimestamp must be(eventTime)
    }
    "ignore events with messagePattern not included in settings" in {
      val setting = TubesSetting(corridorId = 1, chartCodes = List("code1", "code2"), messagePatterns = List(".*[Oo]ost.*"))
      val events = new TubesParseResult(dcf77Status = Some(DCF77State.OK), statusList = List(), eventList = List(
        TubesEvent(eventTimeStr, "IN", "code", "message ost", fmt)))
      val processor = new TubesCorridorProcessor(setting = setting,
        parseResult = events,
        expectedTimeFrom = startTime,
        expectedTimeUntil = endTime)
      val resultSpeeds = processor.getSpeedData
      resultSpeeds.size must be(1)
      val speed = resultSpeeds(0)
      speed.speedSetting must be(Right(None))
      speed.effectiveTimestamp must be(startTime)
    }
    "include start events with messagePattern included in settings" in {
      val setting = TubesSetting(corridorId = 1, chartCodes = List("code1", "code2"), messagePatterns = List(".*[Oo]ost.*"))
      val events = new TubesParseResult(dcf77Status = Some(DCF77State.OK), statusList = List(
        TubesEvent("2014-11-30 04:01:50.164", "IN", "code1", "messaOostge", fmt)), eventList = List())
      val processor = new TubesCorridorProcessor(setting = setting,
        parseResult = events,
        expectedTimeFrom = startTime,
        expectedTimeUntil = endTime)
      val resultSpeeds = processor.getSpeedData
      resultSpeeds.size must be(1)
      val speed = resultSpeeds(0)
      speed.speedSetting must be(Left(SpeedIndicatorStatus.Undetermined))
    }
    "ignore start events with messagePattern not included in settings" in {
      val setting = TubesSetting(corridorId = 1, chartCodes = List("code1", "code2"), messagePatterns = List(".*[Oo]ost.*"))
      val events = new TubesParseResult(dcf77Status = Some(DCF77State.OK), statusList = List(
        TubesEvent("2014-11-30 04:01:50.164", "IN", "code", "ost message", fmt)), eventList = List())
      val processor = new TubesCorridorProcessor(setting = setting,
        parseResult = events,
        expectedTimeFrom = startTime,
        expectedTimeUntil = endTime)
      val resultSpeeds = processor.getSpeedData
      resultSpeeds.size must be(1)
      val speed = resultSpeeds(0)
      speed.speedSetting must be(Right(None))
    }
  }

}