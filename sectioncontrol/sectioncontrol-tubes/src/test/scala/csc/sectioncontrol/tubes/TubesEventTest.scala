package csc.sectioncontrol.tubes

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import java.text.{ SimpleDateFormat, ParseException }
import java.util.TimeZone
import csc.sectioncontrol.messages.Interval

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 10/28/14.
 */

class TubesEventTest extends WordSpec with MustMatchers {

  val fmt = TubesDateFormat.fmt

  "tubesEvent" must {

    "properly de-serialize TubesEventType variants IN, ACK and OUT" in {
      val tIn = TubesEvent("2014-09-01 04:01:50.164", "IN", "don't care", "don't care", fmt)
      val tAck = TubesEvent("2014-09-01 04:01:50.164", "ACK", "don't care", "don't care", fmt)
      val tOut = TubesEvent("2014-09-01 04:01:50.164", "OUT", "don't care", "don't care", fmt)
      tIn.eventType must be(TubesEventType.IN)
      tAck.eventType must be(TubesEventType.ACK)
      tOut.eventType must be(TubesEventType.OUT)
    }

    "throw an exception when constructed with bogus value for TubesEventType" in {
      evaluating {
        TubesEvent("2014-09-01 04:01:50.164", "Bogus value", "=140+YC016/E9", "Taak afsluiten actief", fmt)
      } must produce[NoSuchElementException]
    }

    "be properly constructed from 4 String arguments" in {
      val t = TubesEvent("2014-09-01 04:01:50.164", "IN", "=140+YC016/E9", "Taak afsluiten actief", fmt)
      TubesDateFormat.format(t.timestamp) must be("2014-09-01 04:01:50.164")
      t.timestamp.getTime must be(1409536910164L)
      t.eventType must be(TubesEventType.IN)
      t.chartCode must be("=140+YC016/E9")
      t.message must be("Taak afsluiten actief")
    }

    "be properly constructed from Seq[String]" in {
      val s = Seq("2014-09-01 17:29:29.254", "x", "OUT", "x", "x", "=140+YC016/E9", "x", "x",
        "x", "Taak snelheidsmaatregel actief", "x", "x")

      val t = TubesEvent(s, fmt)
      TubesDateFormat.format(t.timestamp) must be("2014-09-01 17:29:29.254")
      t.eventType must be(TubesEventType.OUT)
      t.chartCode must be("=140+YC016/E9")
      t.message must be("Taak snelheidsmaatregel actief")

    }

    "throw an error if the timestamp string has the wrong format" in {
      //evaluating { s.charAt(-1) } must produce [IndexOutOfBoundsException]
      evaluating {
        TubesEvent("2014-09.01 04:01:50.164", "IN", "=140+YC016/E9", "Taak afsluiten actief", fmt)
      } must produce[ParseException]
    }

    "properly adjust timestamps of events to daylight savings interval" in {
      val inEvent = TubesEvent(Seq("2014-09-01 17:29:29.254", "x", "IN", "x", "x", "=140+YC016/E9", "x", "x",
        "x", "Taak snelheidsmaatregel actief", "x", "x"), fmt)
      val ackEvent = TubesEvent(Seq("2014-09-01 17:30:29.254", "x", "ACK", "x", "x", "=140+YC016/E9", "x", "x",
        "x", "Taak snelheidsmaatregel actief", "x", "x"), fmt)
      val outEvent = TubesEvent(Seq("2014-09-01 17:32:29.254", "x", "OUT", "x", "x", "=140+YC016/E9", "x", "x",
        "x", "Taak snelheidsmaatregel actief", "x", "x"), fmt)

      val interval: Interval = Interval(inEvent.timestamp.getTime - 1000, outEvent.timestamp.getTime + 1000)

      val newInEvent = inEvent.adjustToDstInterval(interval)
      newInEvent.timestamp.getTime must be(interval.from)
      val newAckEvent = ackEvent.adjustToDstInterval(interval)
      newAckEvent.timestamp.getTime must be(interval.from)
      val newOutEvent = outEvent.adjustToDstInterval(interval)
      newOutEvent.timestamp.getTime must be(interval.to)

    }
  }

  "not adjust timestamps of events outside daylight savings interval" in {
    val event = TubesEvent(Seq("2014-09-01 17:29:29.254", "x", "IN", "x", "x", "=140+YC016/E9", "x", "x",
      "x", "Taak snelheidsmaatregel actief", "x", "x"), fmt)
    val originalTime = event.timestamp.getTime
    val interval: Interval = Interval(event.timestamp.getTime - 2000, event.timestamp.getTime - 1000)
    val newEvent = event.adjustToDstInterval(interval)
    newEvent.timestamp.getTime must be(originalTime)
  }
}
