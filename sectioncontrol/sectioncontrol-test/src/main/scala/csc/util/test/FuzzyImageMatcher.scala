package csc.util.test

import java.io.File

import csc.util.test.FileTestUtils._
import org.scalatest.matchers.{ MatchResult, Matcher }
import org.scalatest.matchers.MustMatchers._

/**
 * Created by carlos on 28.07.15.
 */
class FuzzyImageMatcher(refImage: File, threshold: Int) extends Matcher[File] {

  override def apply(testImage: File): MatchResult = {
    val rmsd = rmsdImages(testImage, refImage)
    //println("File <" + testImage.getName + ">, rmsd=" + rmsd)
    val valid = rmsd < threshold
    valid match {
      case true  ⇒ MatchResult(true, "", "")
      case false ⇒ MatchResult(false, getErrorMessage(testImage, rmsd), "")
    }
  }

  private def getErrorMessage(testImage: File, deviation: Double): String =
    "image %s does not match reference image %s. Deviation is %s, bigger than maximum %s".format(
      testImage.getName, refImage.getName, deviation.ceil.toInt, threshold)

}

object FuzzyImageMatcher {
  def check(testImageFile: File, refImageFile: File, threshold: Int): Unit = {
    val beValid = new FuzzyImageMatcher(refImageFile, threshold)
    testImageFile must beValid
  }
}
