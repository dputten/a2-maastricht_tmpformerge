/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.util.test

import java.io.File

/**
 * Support class used to get test resources
 */
//TODO move to some test-util CSC common project (copied from cts_sensor/test-util)
object Resources {
  def getResourcePath(resource: String): String = {
    getClass.getClassLoader.getResource(resource).getPath
  }
  def getResourceFile(resource: String): File = {
    new File(getResourcePath(resource))
  }
  def getResourceDirPath(): File = {
    val file = getResourceFile("resourcedir.txt")
    file.getParentFile
  }
}