package csc.util.test

import akka.actor.{ Actor, ActorLogging, ActorRef, Props }
import csc.util.test.MockActor.MessageMapper

/**
 * Mock actor based on a MessageMapper function.
 * If some collector ActorRef is specified, that actor will be sent all the messages we receive.
 *
 * TODO move to module sectioncontrol-test after merge of TCA15/TCA16/TCA17
 *
 * Created by carlos on 14.08.15.
 */
abstract class MockActor(collector: Option[ActorRef]) extends Actor with ActorLogging {

  def mapper: MessageMapper

  override protected def receive: Receive = {
    case Some(msg) ⇒ handle(msg)
    case msg       ⇒ handle(msg)
  }

  def handle(msg: Any): Any = {
    val client = sender
    collector foreach (_ ! msg)
    mapper.lift(msg) match {
      case Some(response) ⇒ {
        client ! response
      }
      case None ⇒ {
        println("Mock mapper did not recognize " + msg)
        log.warning("Dont know how to reply to {}", msg)
      }
    }
  }

}

class MockActorImpl(val mapper: MessageMapper, collector: Option[ActorRef]) extends MockActor(collector)

object MockActor {

  type MessageMapper = PartialFunction[Any, Any]

  def props(mapper: MessageMapper, collector: Option[ActorRef] = None): Props =
    Props(new MockActorImpl(mapper, collector))
}
