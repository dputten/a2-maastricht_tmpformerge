/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.decorate

import csc.config.Path
import csc.curator.utils.Curator
import csc.sectioncontrol.storage.Decorate._

/**
 * The service to get the Decoration configuration for a lane.
 * And it is possible to use defaults for each lane.
 * The defaults possible are:
 *  Hardcoded
 *  defined globally
 *  defined for a system
 *  All the defaults are merged together with the lane definitions into one
 *  Decorate Configuration.
 *
 */
object DecorationConfigService {

  /**
   *  All paths used in this service
   */
  object Paths {
    val decoration = "decoration"

    /**
     * return the decoration path for this application
     */
    def getDefaultDecorationPath() = Path("/ctes") / "configuration" / decoration

    /**
     * return the base path for this system
     */
    def getSystemPath(systemId: String) = Path("/ctes") / "systems" / systemId

    /**
     * return the decoration path for specified system
     */
    def getSystemDecorationPath(systemId: String) = getSystemPath(systemId) / decoration

    /**
     * return the decoration path for specified lane
     */
    def getLaneDecorationPath(systemId: String, gantryId: String, laneId: String) =
      getSystemPath(systemId) / "gantries" / gantryId / "lanes" / laneId / decoration

  }

  def getDecoration(curator: Curator, systemId: String, gantryId: String, laneId: String): DecorateConfig = {
    val configPaths = Seq(Paths.getDefaultDecorationPath(),
      Paths.getSystemDecorationPath(systemId),
      Paths.getLaneDecorationPath(systemId, gantryId, laneId))
    curator.get[DecorateConfig](configPaths, getHardCodedDefault()).get
  }

  /**
   * create the hard coded default
   * @return ZkDecorateConfig default
   */
  private[decorate] def getHardCodedDefault(): DecorateConfig = {
    new DecorateConfig(
      decorationsConfigs = Set(new DecorationConfig(
        decorationType = DecorationType.Overview_1,
        imageCorrections = None),
        new DecorationConfig(
          decorationType = DecorationType.RedLight_1,
          imageCorrections = None),
        new DecorationConfig(
          decorationType = DecorationType.RedLight_2,
          imageCorrections = None),
        new DecorationConfig(
          decorationType = DecorationType.Speed_1,
          imageCorrections = None),
        new DecorationConfig(
          decorationType = DecorationType.Speed_2,
          imageCorrections = None)),
      rowHeightPx = 15,
      borderWidthPx = 2,
      descentPx = 2,
      extraLineSpacingPx = 2,
      licenseQuality = 80,
      licenseMaxWidthPx = 200,
      licenseMaxHeightPx = 100,
      replaceLicenseWithOriginal = false,
      histogramMeanUpperLimit = 0,
      showLicenseFrame = true,
      licenseFrameThickness = 4,
      licenseFrameColorRGB = ColorRGBConfig(225, 30, 30),
      licenseImageCorrections = List(),
      backgroundColorRGB = ColorRGBConfig(0, 0, 0),
      showLogo = true,
      fontName = None,
      fontColorRGB = ColorRGBConfig(255, 255, 255),
      imageCorrections = List())
  }

}

