package csc.sectioncontrol.decorate

import java.io._
import java.util.UUID

import csc.akkautils.DirectLogging
import csc.sectioncontrol.messages.VehicleMetadata
import net.liftweb.json.{ JsonAST, JsonParser, Printer }

/**
 * Created by carlos on 06.08.15.
 * Responsible for all the EXIF tags manipulation on images, either reading or writing
 */
object ImageMetadataHandler extends DirectLogging {

  def addMetadata(msg: VehicleMetadata, source: Array[Byte], target: Array[Byte]): Option[Array[Byte]] =
    readMetadata(target) match {
      case Nil ⇒ readMetadata(source) match {
        case Nil       ⇒ None
        case extracted ⇒ writeMetadata(target, extracted ++ getComment(msg))
      }
      case _ ⇒ Some(target) //target image already has metadata
    }

  private def getComment(msg: VehicleMetadata) =
    msg.jsonMessage.map(jm ⇒ ReadWriteExifUtil.createCommentTag(prettyJson(jm)))

  def readMetadata(data: Array[Byte]): Seq[ExifTagInfo] = {
    val in = new ByteArrayInputStream(data)
    try {
      //reads the tags, filtered
      ReadWriteExifUtil.readExif(in).filter(ExifTagInfo.nonBaseFilter)
    } finally {
      in.close()
    }
  }

  def writeMetadata(data: Array[Byte], metadata: Seq[ExifTagInfo]): Option[Array[Byte]] = {
    Some(ReadWriteExifUtil.writeExif(data, metadata))
  }

  def getFileContents(file: File): Option[Array[Byte]] = {
    val input = new FileInputStream(file)

    try {
      val fos = new ByteArrayOutputStream //(65535)
      val bis = new BufferedInputStream(input)
      val buf = new Array[Byte](1024)
      Stream.continually(bis.read(buf)).takeWhile(_ != -1).foreach(fos.write(buf, 0, _))
      Some(fos.toByteArray)
    } catch {
      case ex: Exception ⇒
        log.warning("Error getting bytes from file %s", file.getAbsolutePath)
        None
    } finally {
      input.close()
    }
  }

  private def newTempFile = File.createTempFile("ImageMetadataHandler-" + UUID.randomUUID().toString, ".jpg")

  private def writeToTempFile(data: Array[Byte]): Option[File] = {
    val file = newTempFile
    val out = new FileOutputStream(file)

    try {
      out.write(data)
      Some(file)
    } catch {
      case _ ⇒
        file.delete() //delete the temp file
        None
    } finally {
      out.flush()
      out.close()
    }
  }

  private def prettyJson(json: String): String = try {
    val ast = JsonParser.parse(json)
    Printer.pretty(JsonAST.render(ast))
  } catch {
    case e: Exception ⇒
      log.warning("Error whilst making a pretty print version of a json text. [%s]".format(e.getMessage))
      log.warning("JSON=<{}>", json)
      json
  }
}
