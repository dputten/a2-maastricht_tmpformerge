package csc.sectioncontrol.decorate

import java.io.ByteArrayInputStream

import csc.curator.utils.Curator
import csc.sectioncontrol.messages.{ Lane, VehicleImage, VehicleMetadata }
import csc.sectioncontrol.sign.certificate.CertificateService
import csc.sectioncontrol.storage.Decorate.DecorateConfig
import csc.sectioncontrol.storage.Decorate.DecorationType.DecorationType
import csc.sectioncontrol.storagelayer.{ CorridorConfig, SystemConfiguration }
import org.slf4j.LoggerFactory

/**
 * Created by carlos on 07.07.15.
 */
class DecorateService(curator: ⇒ Curator, systemId: String) {

  private val log = LoggerFactory.getLogger(this.getClass.getName)
  private var currentCorridor: Option[CorridorConfig] = None; //caching the last CorridorConfig used
  private val systemCfg = getSystemConfig()

  /**
   *
   * @param msg
   * @param decorationType
   * @param sourceImage
   * @param corridorId corridor id (only used in RedLight and Speed related decorations)
   * @param jpegQuality
   * @return decorated image, if able to produce it
   */
  def getDecoratedImage(msg: VehicleMetadata,
                        decorationType: DecorationType,
                        sourceImage: ImageAndContents,
                        corridorId: Option[String],
                        jpegQuality: Int): Option[Array[Byte]] = sourceImage match {

    case ImageAndContents(Some(image), Some(data)) ⇒ {
      updateCorridor(corridorId) //updates currentCorridor
      val view = createDecoratorView(msg, image, decorationType)
      val decorator = getDecorator(msg.lane, decorationType, jpegQuality)
      val decorated = decorator.decorate(sourceImage, msg, view) //decorates
      decorated.map(addMetadata(msg, sourceImage.contents.get, _)) //adds metadata
    }
    case _ ⇒ None
  }

  protected def addMetadata(msg: VehicleMetadata, source: Array[Byte], target: Array[Byte]): Array[Byte] = {
    val enriched = ImageMetadataHandler.addMetadata(msg, source, target)
    enriched match {
      case Some(image) ⇒ image
      case None ⇒ {
        log.info("Could not add metadata to image, vehicle ", msg.eventId)
        target //returns the unmodified target
      }
    }
  }

  protected def createDecoratorView(msg: VehicleMetadata,
                                    image: VehicleImage,
                                    decorationType: DecorationType): DecorationView = {
    val viewContext = createViewContext(msg, image)
    DecorationView.getView(decorationType, viewContext)
  }

  protected def getDecorator(lane: Lane, decorationType: DecorationType, jpegQuality: Int): ImageDecorator =
    new ImageDecorator(decorationType, getDecorateConfig(lane), jpegQuality)

  protected def getDecorateConfig(lane: Lane): DecorateConfig =
    DecorationConfigService.getDecoration(curator, lane.system, lane.gantry, lane.laneId)

  protected def getSystemConfig(): SystemConfiguration = new SystemConfiguration(curator)

  protected def certificateService: CertificateService = CertificateService

  protected def getCorridorConfig(corridorId: String): Option[CorridorConfig] =
    systemCfg.getCorridor(corridorId, systemId)

  protected def createViewContext(msg: VehicleMetadata, image: VehicleImage): MutableContext = {
    val ctx = new MutableContext
    ctx.curator = curator
    ctx.systemId = systemId
    ctx.corridor = currentCorridor
    ctx.certService = certificateService
    ctx.msg = msg
    ctx.image = image
    ctx
  }

  private def updateCorridor(corridorId: Option[String]): Unit =
    corridorId match {
      case None ⇒ currentCorridor = None //no corridor id specified
      case Some(id) ⇒ currentCorridor match {
        case Some(corridor) if (corridor.id != corridorId) ⇒ getCorridorConfig(id) //update corridor
        case None ⇒ currentCorridor = getCorridorConfig(id) //update corridor
        case _ ⇒ // nothing todo
      }
    }
}

case class ImageAndContents(img: Option[VehicleImage] = None, contents: Option[Array[Byte]] = None) {
  def isComplete: Boolean = img.isDefined && contents.isDefined

  def asImage: Option[Image] = {
    contents.map(c ⇒ new Image(new ByteArrayInputStream(c)))
  }
}

object ImageAndContents {
  val Empty = ImageAndContents()
}

