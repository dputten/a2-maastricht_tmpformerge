/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.decorate

import java.awt.font.{ FontRenderContext, TextLayout }
import java.awt.image.BufferedImage
import java.awt.{ Color, Font, RenderingHints }

import csc.akkautils.DirectLogging

/**
 * A builder of images that contain strings in columns.
 * @param imageType the type of the image
 * @param rowHeightPx the height of a row in pixels. The font is scaled to this height
 * @param borderWidthPx The width of the border in pixels
 * @param descentPx Used to position the text (in y direction) within a row. The number of pixels (the baseline) is moved upwards.
 * @param extraLineSpacingPx Extra space between lines in pixels.
 * @param backgroundColor the background color
 * @param font the font to be used (optional)
 * @param fontColor the font color
 */
class ColumnedStringsImageBuilder(
  val imageType: Int = BufferedImage.TYPE_INT_RGB,
  val rowHeightPx: Int = 15,
  val borderWidthPx: Int = 2,
  val descentPx: Int = 2,
  val extraLineSpacingPx: Int = 2,
  val backgroundColor: Color = Color.BLACK,
  val font: Option[Font] = None,
  val fontColor: Color = Color.WHITE) extends DirectLogging {

  import scala.math._

  /**
   * Represents a text, positioned at (row, column)
   * @param textLayout a TextLayout containing the text
   * @param row the row in which the text is located
   * @param column the column where the text starts
   */
  case class PositionedText(textLayout: TextLayout, row: Int, column: Int)

  val fontRenderContext = new FontRenderContext(null, true, true)
  val fontForRowHeight = createFont(font.getOrElse(getDefaultFont), rowHeightPx)

  /**
   * Create an image which contains the given text in columns.
   * @param content A list of strings that the new image should contain
   * @param width the width of the created images
   * @param numberOfColumns the number of columns the image should contain
   * @return The new image
   */
  def createImage(content: List[String], width: Int, numberOfColumns: Int): BufferedImage = {
    val availableWidth = width - (2 * borderWidthPx)
    require(availableWidth > 0, "Width is too small")
    require(!content.isEmpty, "No content to create")

    val columnSize: Int = availableWidth / numberOfColumns

    val positionedContent = positionStrings(content, columnSize, numberOfColumns)

    val numberOfRows = positionedContent.last.row + 1 // Row numbering starts with 0.
    val height = numberOfRows * (rowHeightPx + extraLineSpacingPx) - extraLineSpacingPx

    val imageHeight = height + (2 * borderWidthPx)
    val newImage = new BufferedImage(width, imageHeight, imageType)
    val newGraphic = newImage.createGraphics()
    newGraphic.setBackground(backgroundColor)
    newGraphic.clearRect(0, 0, width, imageHeight)
    // Draw all texts
    newGraphic.setColor(fontColor)
    newGraphic.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
    positionedContent.foreach(positionedText ⇒ {
      val x = positionedText.column * columnSize + borderWidthPx
      val y = (positionedText.row + 1) * rowHeightPx + (positionedText.row * extraLineSpacingPx) + borderWidthPx - 2
      positionedText.textLayout.draw(newGraphic, x, y)
    })
    newImage
  }

  def getDefaultFont: Font = {
    new Font(Font.SANS_SERIF, Font.PLAIN, 12)
  }

  def createFont(baseFont: Font, rowHeight: Int): Font = {
    val baseSize = 20.0f
    val refFont = baseFont.deriveFont(baseSize)
    val refText = new TextLayout("WyXhj!'?g", refFont, fontRenderContext)
    val refHeight = refText.getAscent + refText.getDescent + refText.getLeading
    val fontSize = (rowHeight.toFloat / refHeight) * baseSize

    log.debug("Calculated font size for given row height (%d) is %1.2f".format(rowHeight, fontSize))

    baseFont.deriveFont(fontSize)
  }

  /**
   * Given the constrains (determined by constructor), position the given text in columns.
   * @param content A list of strings that will be positioned (in the order they appear in this list)
   * @param columnSize the width of a column
   * @param numberOfColumns the number of columns the image should contain
   * @return A list of PositionedText objects.
   */
  def positionStrings(content: List[String], columnSize: Int, numberOfColumns: Int): List[PositionedText] = {
    var currentRow = 0
    var currentColumn = 0

    content.map(text ⇒ {
      val aText = text.trim match {
        case ""     ⇒ " " // One space
        case string ⇒ text
      }
      // Create TextLayout for aText and determine its width
      val textLayout = new TextLayout(aText, fontForRowHeight, fontRenderContext)
      val textLayoutForLength = new TextLayout(aText.replace(' ', '-'), fontForRowHeight, fontRenderContext)
      val textWidth = textLayoutForLength.getBounds.getWidth

      // Check if text fits on remainder of current row
      val columnsNeededForText = ceil(textWidth / columnSize.toDouble).toInt.min(numberOfColumns).max(1)

      val fitsOnRemainderOfCurrentRow = (currentColumn + columnsNeededForText <= numberOfColumns)

      if (!fitsOnRemainderOfCurrentRow) { // text goes to next row
        currentRow += 1
        currentColumn = 0
      }
      val positionOfText = new PositionedText(textLayout = textLayout, row = currentRow, column = currentColumn)
      // Move to next free column
      currentColumn += columnsNeededForText
      if (currentColumn >= numberOfColumns) {
        currentRow += 1
        currentColumn = 0
      }

      positionOfText
    })
  }
}
