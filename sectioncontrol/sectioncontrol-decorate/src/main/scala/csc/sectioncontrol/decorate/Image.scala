/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.decorate

import java.awt.image.{ BufferedImage, RescaleOp }
import java.awt.{ List ⇒ _, _ }
import java.io.{ ByteArrayOutputStream, File, InputStream }
import javax.imageio.stream.ImageOutputStream
import javax.imageio.{ IIOImage, ImageIO, ImageWriteParam }

import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer

/**
 * Class represent the Image.
 * @param image The image
 */
case class Image(image: BufferedImage) {
  require(image != null, "Missing image")

  /**
   * Normal used constructor
   * @param fileName the file path of the image
   */
  def this(fileName: String) {
    this(ImageIO.read(new File(fileName)))
  }

  /**
   * Public constructor
   * @param input the input stream of the image
   */
  def this(input: InputStream) {
    this(ImageIO.read(input))
  }

  def width: Int = image.getWidth

  def height: Int = image.getHeight

  /**
   * Write this image to byte array
   * @param compression The compression of the file must be between 0 and 100
   *                    0 is interpreted as "high compression is important",
   *                    while a setting of 100 is interpreted as "high image quality is important"
   * @param imageType the type of the image
   */
  def getBytes(compression: Int = 100, imageType: String = "jpg"): Array[Byte] = {
    val stream = new ByteArrayOutputStream()
    val ios = ImageIO.createImageOutputStream(stream)
    writeImageToStream(ios, compression, imageType)
    stream.toByteArray
  }

  /**
   * Write this image to file
   * @param fileName  The name of the new imagefile
   * @param compression The compression of the file must be between 0 and 100,
   *                    0 is interpreted as "high compression is important",
   *                    while a setting of 100 is interpreted as "high image quality is important"
   * @param imageType the type of the image
   */
  def writeImage(fileName: String, compression: Int = 100, imageType: String = "jpg") {
    val ios = ImageIO.createImageOutputStream(new File(fileName))
    writeImageToStream(ios, compression, imageType)
  }

  /**
   * Write this image to io stream
   * @param ios  The name of the new imagefile
   * @param compression The compression of the file must be between 0 and 100
   *                    0 is interpreted as "high compression is important",
   *                    while a setting of 100 is interpreted as "high image quality is important"
   * @param imageType the type of the image
   */
  private def writeImageToStream(ios: ImageOutputStream, compression: Int, imageType: String) {
    require(compression <= 100, "compression shopuld be between 0 and 100")
    require(compression >= 0, "compression shopuld be between 0 and 100")
    // Find a jpeg writer
    val writer = {
      val writerList = ImageIO.getImageWritersByFormatName(imageType).asScala
      if (writerList.isEmpty) {
        throw new IllegalArgumentException("Couldn't find writer for imageType %s".format(imageType))
      } else {
        writerList.next()
      }
    }

    try {
      writer.setOutput(ios);

      // Set the compression quality
      val iwparam = writer.getDefaultWriteParam()
      iwparam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
      iwparam.setCompressionQuality(compression.toFloat / 100);
      // Write the image
      writer.write(null, new IIOImage(image, null, null), iwparam);
    } finally {
      // Cleanup
      try {
        ios.flush();
      } finally {
        try {
          writer.dispose();
        } finally {
          ios.close();
        }
      }
    }
  }

  /**
   * create a new Image with a footer attached to the current image
   * The footer is horizontal divided into a number of columns. This to make the different
   * content items vertical aligned when there are multiple lines needed to add all the contents.
   * When a content text is larger than a column it takes as many columns as needed
   * TODO: what if content needs more than a line contains
   * @param nrColumns number of columns, the footer is divided to
   * @param content List of text which have to be added into the footer
   * @return Image with the footer attached
   */
  def createImageWithFooter(nrColumns: Int, content: List[String]): Image = {
    val columnSize = image.getWidth() / nrColumns
    //calculate nr of columns needed
    val graphic = image.createGraphics()
    val renderCtx = graphic.getFontRenderContext
    val font = graphic.getFont
    val nrColumnList = new ListBuffer[Int]()
    var totalColumns = 0
    for (text ← content) {
      val dim = font.getStringBounds(text, renderCtx)
      val nr = (dim.getWidth / columnSize).toInt + 1
      //check row full 
      nrColumnList += nr
      val nrEmptyColInRow = columnSize - totalColumns % columnSize
      if (nr > nrEmptyColInRow) {
        //row overflow start on next row
        totalColumns += nr + nrEmptyColInRow
      } else {
        totalColumns += nr
      }
    }
    val round = if (totalColumns % nrColumns > 0) 1 else 0
    val nrRows = (totalColumns / nrColumns).toInt + round

    val dim = font.getStringBounds("text", renderCtx)
    val rowHeight = dim.getHeight.toInt
    val footerHeight = (rowHeight * nrRows).toInt + 3 //keep a little border under the last line
    //create new file
    val newImage = new BufferedImage(image.getWidth, image.getHeight + footerHeight, image.getType)
    //copy original image to new image
    val newGraphic = newImage.createGraphics()
    newGraphic.drawImage(image,
      0, 0, image.getWidth, image.getHeight,
      0, 0, image.getWidth, image.getHeight,
      null);
    //add Extra content
    var ypos = image.getHeight + rowHeight
    var xpos = 0
    for (index ← 0 until content.size) {
      val text = content(index)
      val neededNrCol = nrColumnList(index)
      //Does content fit in this row 
      var endPos = xpos + neededNrCol * columnSize
      if (endPos > image.getWidth) {
        //new line
        xpos = 0
        ypos += rowHeight
        endPos = neededNrCol * columnSize
      }
      newGraphic.drawString(text, xpos, ypos)
      //update position
      xpos = endPos
    }
    //done creating new file
    new Image(newImage)
  }

  /**
   * Create a new image, composed of this image and the addedImage. The addedImage will be added to the right of this image.
   *    newImage.width = thisImage.width + addedImage.width
   *    newImage.height = max(thisImage.height, addedImage.height)
   * @param addedImage Image to be added to the right of this image
   * @return The new image
   */
  def addRight(addedImage: Image): Image = {
    val newImageWidth = image.getWidth + addedImage.image.getWidth
    val newImageHeight = image.getHeight max addedImage.image.getHeight

    val newImage = new BufferedImage(newImageWidth, newImageHeight, image.getType)
    val newGraphic = newImage.createGraphics()
    // Add this image on the left side of the new image
    newGraphic.drawImage(image,
      0, 0, image.getWidth - 1, image.getHeight - 1,
      0, 0, image.getWidth - 1, image.getHeight - 1,
      null)
    // Add adeddImage on the right side of the new image
    newGraphic.drawImage(addedImage.image,
      image.getWidth, 0, newImageWidth - 1, addedImage.image.getHeight - 1,
      0, 0, addedImage.image.getWidth - 1, addedImage.image.getHeight - 1,
      null)
    newGraphic.dispose()

    new Image(newImage)
  }

  /**
   * Create a new image, composed of this image and the addedImage. The addedImage will be added to the bottom of this image.
   *    newImage.width = max(thisImage.width, addedImage.width)
   *    newImage.height = thisImage.height + addedImage.height
   * @param addedImage Image to be added to the bottom of this image
   * @return The new image
   */
  def addBottom(addedImage: Image): Image = {
    val newImageWidth = image.getWidth max addedImage.image.getWidth
    val newImageHeight = image.getHeight + addedImage.image.getHeight

    val newImage = new BufferedImage(newImageWidth, newImageHeight, image.getType)
    val newGraphic = newImage.createGraphics()
    // Add this image on the left side of the new image
    newGraphic.drawImage(image,
      0, 0, image.getWidth - 1, image.getHeight - 1,
      0, 0, image.getWidth - 1, image.getHeight - 1,
      null)
    // Add adeddImage on the right side of the new image
    newGraphic.drawImage(addedImage.image,
      0, image.getHeight, addedImage.image.getWidth - 1, newImageHeight - 1,
      0, 0, addedImage.image.getWidth - 1, addedImage.image.getHeight - 1,
      null)
    newGraphic.dispose()

    new Image(newImage)
  }

  /**
   * Draw a rectangle on this image. The rectangle size and location is determined by TL (topLeft) and BR (bottom right).
   * The rectangle is drawn on the outside, using given color and thickness.
   * @param TL Top left corner of the cropping rectangle.
   * @param BR Bottom right corner of the cropping rectangle.
   * @param color Color to be used for the rectangle.
   * @param thickness Thickness to be used for the rectangle.
   * @return The new image
   */
  def drawOuterRectangle(TL: Point, BR: Point, color: Color = Color.GREEN, thickness: Int = 2): Image = {
    val TR = new Point(BR.getX.toInt, TL.getY.toInt)
    val BL = new Point(TL.getX.toInt, BR.getY.toInt)

    drawOuterQuadrangle(TL = TL, TR = TR, BR = BR, BL = BL, color = color, thickness = thickness)
  }

  /**
   * Draw a quadrangle on this image.
   * The quadrangle size and location is determined by TL (top left), TR (top right), BR (bottom right) and BL (bottom left).
   * The quadrangle is drawn on the outside, using given color and thickness.
   * @param TL Top left corner of the cropping rectangle.
   * @param TR Top right corner of the cropping rectangle.
   * @param BR Bottom right corner of the cropping rectangle.
   * @param BL Bottom left corner of the cropping rectangle.
   * @param color Color to be used for the rectangle.
   * @param thickness Thickness to be used for the rectangle.
   * @return The new image
   */
  def drawOuterQuadrangle(TL: Point, TR: Point, BR: Point, BL: Point, color: Color = Color.GREEN, thickness: Int = 2): Image = {
    val delta = (thickness / 2)

    val vertices = Seq(
      new Point(TL.getX.toInt - delta, TL.getY.toInt - delta),
      new Point(TR.getX.toInt + delta, TR.getY.toInt - delta),
      new Point(BR.getX.toInt + delta, BR.getY.toInt + delta),
      new Point(BL.getX.toInt - delta, BL.getY.toInt + delta))

    val newImage = new BufferedImage(image.getWidth, image.getHeight, image.getType)
    val newGraphic = newImage.createGraphics()
    newGraphic.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
    // Copy this image to the new image
    newGraphic.drawImage(image,
      0, 0, image.getWidth - 1, image.getHeight - 1,
      0, 0, image.getWidth - 1, image.getHeight - 1,
      null)

    newGraphic.setColor(color)
    val stroke = new BasicStroke(thickness)
    newGraphic.setStroke(stroke)

    (0 until vertices.size).foreach { idx ⇒
      val toIdx = (idx + 1) % vertices.size
      newGraphic.drawLine(vertices(idx).getX.toInt, vertices(idx).getY.toInt, vertices(toIdx).getX.toInt, vertices(toIdx).getY.toInt)
    }

    newGraphic.dispose()

    new Image(newImage)
  }

  /**
   * apply an autolevel algorithm. It add light to a image. The amount of light depends on the the current image.
   * @param blackStart the value where black should start from between 0-255
   * @param blackMargin the max percentage of black required
   * @param whiteStart the value where white should start from between 0-255
   * @param whiteMargin the max percentage of white required
   * @return a new image with an autolevel correction applied
   */
  def applyAutoLevel(blackStart: Int, blackMargin: Double, whiteStart: Int, whiteMargin: Double): Image = {
    val (newImage, black, white) = ImageCorrections.applyDynamicAutoLevel(image, blackStart, blackMargin, whiteStart, whiteMargin)
    new Image(newImage)
  }

  /**
   * add gamma to a picture
   * @param correction the amount of gamma added
   * @return a new image with a gamma correction applied
   */
  def applyGammaCorrection(correction: Double): Image = new Image(ImageCorrections.applyGammaCorrection(image, correction))

  /**
   * add RGB to a picture
   * @param red the amount of red to add
   * @param green the green of red to add
   * @param blue the blue of red to add
   * @return a new image with a RGB correction applied
   */
  def applyRGBCorrection(red: Int, green: Int, blue: Int): Image = new Image(ImageCorrections.applyRGBCorrection(image, red, green, blue))

  /**
   * add brightness and contrast to a picture
   * @param brightness the amount of brightness to add
   * @param contrast the amount of contrast to add
   * @return a new image with a brightness/contrast correction applied
   */
  def applyBrightnessContrastCorrection(brightness: Float, contrast: Float): Image = {
    val op = new RescaleOp(brightness, contrast, null)
    new Image(op.filter(image, null))
  }

  /**
   * Create a new image, being this image overlayed with the overlayedImage in one of the corners.
   * @param overlayedImage Image to be overlayed on top of this image
   * @param corner Corner where the image will be overlayed.
   * @return The new image
   */
  def overlay(overlayedImage: Image, corner: Image.Corner.Value): Image = {
    val topLeft = corner match {
      case Image.Corner.TopLeft     ⇒ new Point(0, 0)
      case Image.Corner.TopRight    ⇒ new Point(image.getWidth - overlayedImage.image.getWidth - 1, 0)
      case Image.Corner.BottomLeft  ⇒ new Point(0, image.getHeight - overlayedImage.image.getHeight - 1)
      case Image.Corner.BottomRight ⇒ new Point(image.getWidth - overlayedImage.image.getWidth - 1, image.getHeight - overlayedImage.image.getHeight - 1)
    }

    overlay(overlayedImage, topLeft)
  }

  /**
   * Create a new image, being this image overlayed with the overlayedImage (position determined by given topLeft).
   * Given clipping polygon is applied. (= only overlayedImage inside polygon will be visible.)
   * @param overlayedImage Image to be overlayed on top of this image
   * @param topLeft Position of the top left corner of the overlayedImage.
   * @param clippingPolygonVertices Vertices of the clipping polygon.
   * @return The new image
   */
  def overlayWithClippingPolygon(overlayedImage: Image, topLeft: Point, clippingPolygonVertices: Seq[Point], useAntiAliasing: Boolean = true): Image = {

    val xValues = clippingPolygonVertices.map(_.x)
    val yValues = clippingPolygonVertices.map(_.y)

    val clippingPolygon = new Polygon(xValues.toArray, yValues.toArray, clippingPolygonVertices.size)

    val newImage = new BufferedImage(image.getWidth, image.getHeight, image.getType)
    val newGraphic = newImage.createGraphics()
    // Copy this image to the new image
    newGraphic.drawImage(image,
      0, 0, image.getWidth - 1, image.getHeight - 1,
      0, 0, image.getWidth - 1, image.getHeight - 1,
      null)
    if (useAntiAliasing) newGraphic.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
    // Add overlayedImage at position topLeft
    newGraphic.setClip(clippingPolygon)
    newGraphic.drawImage(overlayedImage.image, topLeft.getX.toInt, topLeft.getY.toInt, null)
    newGraphic.dispose()

    new Image(newImage)
  }

  /**
   * Apply a polygon mask to this image.
   * The result will be the this image, with the area outside the polygon colored black.
   * There is an additional option to crop the resulting image so it just contains the polygon.
   * @param vertices All vertices of the masking polygon in drawing order. (Please note: also side between last en first vertex.)
   * @param doCrop Indicates whether the resulting image should be cropped.
   * @return The masked image
   */
  def applyPolygonMask(vertices: Seq[Point], doCrop: Boolean = false): Image = {
    require(vertices.size >= 3, "Polygon mask must have at least 3 vertices")

    val xValues = vertices.map(_.x)
    val yValues = vertices.map(_.y)

    def doApplyPolygonMask: BufferedImage = {
      val clippingPolygon = new Polygon(xValues.toArray, yValues.toArray, vertices.size)

      val newImage = new BufferedImage(image.getWidth, image.getHeight, image.getType)
      val newGraphic = newImage.createGraphics()
      newGraphic.setClip(clippingPolygon)
      newGraphic.drawImage(image,
        0, 0, image.getWidth - 1, image.getHeight - 1,
        0, 0, image.getWidth - 1, image.getHeight - 1,
        null)
      newGraphic.dispose()
      newImage
    }

    doCrop match {
      case true ⇒
        val topLeft = new Point(xValues.min, yValues.min)
        val bottomRight = new Point(xValues.max, yValues.max)
        new Image(doApplyPolygonMask).crop(topLeft, bottomRight)
      case false ⇒
        new Image(doApplyPolygonMask)
    }
  }

  /**
   * Crop the image. Cropping rectangle is determined by topLeft and bottomRight.
   * The result may have an extra border.
   * @param topLeft Top left corner of the cropping rectangle.
   * @param bottomRight Bottom right corner of the cropping rectangle.
   * @return The cropped image
   */
  def crop(topLeft: Point, bottomRight: Point): Image = {
    require(topLeft.x < bottomRight.x, "Crop not possible: topLeft is not to the left of bottomRight")
    require(topLeft.y < bottomRight.y, "Crop not possible: topLeft is not above bottomRight")
    val visibleTopLeft = new Point(topLeft.x.max(0), topLeft.y.max(0))
    val visibleBottomRight = new Point(bottomRight.x.min(image.getWidth - 1), bottomRight.y.min(image.getHeight - 1))

    val newImageWidth = visibleBottomRight.x - visibleTopLeft.x
    val newImageHeight = visibleBottomRight.y - visibleTopLeft.y

    val newImage = new BufferedImage(newImageWidth, newImageHeight, image.getType)
    val newGraphic = newImage.createGraphics()
    newGraphic.drawImage(image,
      0, 0, newImageWidth, newImageHeight,
      visibleTopLeft.x, visibleTopLeft.y, visibleBottomRight.x, visibleBottomRight.y,
      null)

    newGraphic.dispose()

    new Image(newImage)
  }

  def scale(newDimension: Dimension): Image = {
    require(newDimension.height > 0, "Need some positive target height")
    require(newDimension.width > 0, "Need some positive target width")
    val resizedImage = new BufferedImage(newDimension.width, newDimension.height, image.getType)
    val newGraphic = resizedImage.createGraphics()
    newGraphic.drawImage(image, 0, 0, newDimension.width, newDimension.height, null)

    //improved quality, copied from http://www.mkyong.com/java/how-to-resize-an-image-in-java/
    newGraphic.setComposite(AlphaComposite.Src);
    newGraphic.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
      RenderingHints.VALUE_INTERPOLATION_BILINEAR);
    newGraphic.setRenderingHint(RenderingHints.KEY_RENDERING,
      RenderingHints.VALUE_RENDER_QUALITY);
    newGraphic.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
      RenderingHints.VALUE_ANTIALIAS_ON);

    new Image(resizedImage)
  }

  /**
   * Create a new image, being this image overlayed with the overlayedImage. The position of the topLeft corner of the overlayedImage is
   * determined by the topLeft parameter. All parts of the overlayedImage outside this image will be removed.
   * @param overlayedImage Image to be overlayed on top of this image
   * @param topLeft Position of the topLeft corner of the overlayedImage in this image.
   * @return The new image
   */
  def overlay(overlayedImage: Image, topLeft: Point): Image = {
    val newImage = new BufferedImage(image.getWidth, image.getHeight, image.getType)
    val newGraphic = newImage.createGraphics()
    // Copy this image to the new image
    newGraphic.drawImage(image,
      0, 0, image.getWidth - 1, image.getHeight - 1,
      0, 0, image.getWidth - 1, image.getHeight - 1,
      null)
    // Add overlayedImage at position topLeft
    newGraphic.drawImage(overlayedImage.image, topLeft.getX.toInt, topLeft.getY.toInt, null)
    newGraphic.dispose()

    new Image(newImage)
  }

}

object Image {

  object Corner extends Enumeration {
    val TopLeft, TopRight, BottomLeft, BottomRight = Value
  }

  /**
   * Create an Image object, with given width and number of columns, containing the given content placed in columns.
   * The height of the image is determined by: content size, content item lengths, font height.
   * @param content A list of strings that the new image should contain
   * @param width the width of the created images
   * @param numberOfColumns the number of columns the image should contain
   * @param imageType the type of the image
   * @param rowHeightPx the height of a row in pixels. The font is scaled to this height
   * @param borderWidthPx The width of the border in pixels
   * @param descentPx Used to position the text (in y direction) within a row. The number of pixels (the baseline) is moved upwards.
   * @param extraLineSpacingPx Extra space between lines in pixels.
   * @param backgroundColor the background color
   * @param font the font to be used (optional)
   * @param fontColor the font color
   * @return The new image
   */
  def createColumnedStringsImage(
    content: List[String],
    width: Int,
    numberOfColumns: Int,
    imageType: Int = BufferedImage.TYPE_INT_RGB,
    rowHeightPx: Int = 15,
    borderWidthPx: Int = 2,
    descentPx: Int = 2,
    extraLineSpacingPx: Int = 2,
    backgroundColor: Color = Color.BLACK,
    font: Option[Font] = None,
    fontColor: Color = Color.WHITE): Image = {
    val builder = new ColumnedStringsImageBuilder(imageType = imageType,
      backgroundColor = backgroundColor,
      rowHeightPx = rowHeightPx,
      borderWidthPx = borderWidthPx,
      descentPx = descentPx,
      extraLineSpacingPx = extraLineSpacingPx,
      font = font,
      fontColor = fontColor)
    new Image(builder.createImage(content, width, numberOfColumns))
  }
}
