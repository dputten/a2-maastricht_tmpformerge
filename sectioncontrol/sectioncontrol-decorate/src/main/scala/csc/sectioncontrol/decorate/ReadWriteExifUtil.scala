package csc.sectioncontrol.decorate

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
//http://commons.apache.org/proper/commons-imaging/apidocs/src-html/org/apache/commons/imaging/formats/tiff/constants/TiffTagConstants.html#line.270
//http://commons.apache.org/proper/commons-imaging/apidocs/org/apache/commons/imaging/ImagingConstants.html#PARAM_KEY_READ_THUMBNAILS
//https://issues.apache.org/jira/browse/IMAGING-50
//http://www.media.mit.edu/pia/Research/deepview/exif.html

import com.drew.imaging.ImageMetadataReader
import com.drew.metadata.Metadata
import com.drew.metadata.jpeg.JpegComponent
import java.io._
import org.apache.commons.imaging.common.RationalNumber
import org.apache.commons.imaging.formats.jpeg.exif.ExifRewriter
import org.apache.commons.imaging.formats.tiff.constants.{ ExifTagConstants, TiffDirectoryType }
import org.apache.commons.imaging.formats.tiff.taginfos._
import org.apache.commons.imaging.formats.tiff.write.TiffOutputSet
import org.apache.commons.imaging.util.IoUtils
import scala.collection.JavaConversions._
import org.slf4j.LoggerFactory
import java.text.SimpleDateFormat
import java.util.TimeZone
import jpg.JpegCommentWriter

/**
 * Jpg Exif information support
 */
object ReadWriteExifUtil {
  val simpleDateFormat: SimpleDateFormat = {
    val datePatternExif = "yyyy:MM:dd HH:mm:ss"
    val timeZone = "UTC"
    val tmp = new SimpleDateFormat(datePatternExif)
    tmp.setTimeZone(TimeZone.getTimeZone(timeZone))
    tmp
  }

  def createCommentTag(comment: String): ExifTagInfo = {
    ExifTagInfo(ExifTagInfo.directoryJpegComment, "Jpeg Comment", 0, ExifValueString(comment))
  }

  /**
   * Derived property:
   * Return the dateTimeOriginal and the subSecTimeOriginal as a combined long time
   */
  def dateTimeOriginalWithSubSecTimeOriginalUtc(info: Seq[ExifTagInfo]): Option[Long] = {
    for (
      tagDateTime ← info.find(exif ⇒
        exif.tagType == ExifTagConstants.EXIF_TAG_DATE_TIME_ORIGINAL.tag &&
          exif.directory == ExifTagInfo.directoryExif_SubIFD);
      dateTimeOriginal ← {
        tagDateTime.value match {
          case ExifValueString(str) ⇒ Some(removeBeginAndEndQuotes(str))
          case _                    ⇒ None
        }
      };
      tagSubSecTime ← info.find(exif ⇒
        exif.tagType == ExifTagConstants.EXIF_TAG_SUB_SEC_TIME_ORIGINAL.tag &&
          exif.directory == ExifTagInfo.directoryExif_SubIFD);
      subSecTimeOriginal ← {
        tagSubSecTime.value match {
          case ExifValueIntegers(array) ⇒ Some(array(0))
          case ExifValueString(str)     ⇒ Some(str.toInt)
          case _                        ⇒ None
        }
      }
    ) yield {
      val date = simpleDateFormat.parse(dateTimeOriginal)
      //make sure the millis are zero
      val dateTime = (date.getTime / 1000) * 1000
      dateTime + subSecTimeOriginal.toLong
    }
  }

  /**
   * value from the exif contain enclosing quotes. Remove these if present.
   * @param value string
   */
  private def removeBeginAndEndQuotes(value: String): String = {
    if (value.startsWith("'") && value.endsWith("'")) {
      value.substring(1, value.length - 1)
    } else {
      value
    }
  }

  /**
   * Read all the exif information of a jpg image and create a list of ExifTagInfo
   * representing the exif information.
   * @param fileNameSrcMeta The jpg image
   * @return list of ExifTagInfo representing the exif information.
   */
  def readExif(fileNameSrcMeta: File): Seq[ExifTagInfo] = {
    val metadata = ImageMetadataReader.readMetadata(fileNameSrcMeta)
    readExif(metadata)
  }

  def readExif(input: InputStream): Seq[ExifTagInfo] = {
    val metadata = ImageMetadataReader.readMetadata(input)
    readExif(metadata)
  }

  private def readExif(metadata: Metadata): Seq[ExifTagInfo] = {
    metadata.getDirectories.flatMap(directory ⇒ {
      directory.getTags.flatMap(tag ⇒ {
        val value = directory.getObject(tag.getTagType) match {
          case rad: com.drew.lang.Rational ⇒ Seq(ExifTagInfo(
            directory = directory.getName.toUpperCase,
            tagName = tag.getTagName,
            tagType = tag.getTagType,
            value = ExifValueRational(Seq(Rational(rad.getNumerator.toInt, rad.getDenominator.toInt)))))
          case int: Integer ⇒ Seq(ExifTagInfo(
            directory = directory.getName.toUpperCase,
            tagName = tag.getTagName,
            tagType = tag.getTagType,
            value = ExifValueIntegers(Seq(int))))
          case ar: Array[Int] ⇒ Seq(ExifTagInfo(
            directory = directory.getName.toUpperCase,
            tagName = tag.getTagName,
            tagType = tag.getTagType,
            value = ExifValueIntegers(ar.toSeq)))
          case long: java.lang.Long ⇒ Seq(ExifTagInfo(
            directory = directory.getName.toUpperCase,
            tagName = tag.getTagName,
            tagType = tag.getTagType,
            value = ExifValueLongs(Seq(long))))
          case ar: Array[Long] ⇒ Seq(ExifTagInfo(
            directory = directory.getName.toUpperCase,
            tagName = tag.getTagName,
            tagType = tag.getTagType,
            value = ExifValueLongs(ar.toSeq)))
          case sh: java.lang.Short ⇒ Seq(ExifTagInfo(
            directory = directory.getName.toUpperCase,
            tagName = tag.getTagName,
            tagType = tag.getTagType,
            value = ExifValueShorts(Seq(sh))))
          case ar: Array[Short] ⇒ Seq(ExifTagInfo(
            directory = directory.getName.toUpperCase,
            tagName = tag.getTagName,
            tagType = tag.getTagType,
            value = ExifValueShorts(ar.toSeq)))
          case ar: Array[Byte] ⇒ Seq(ExifTagInfo(
            directory = directory.getName.toUpperCase,
            tagName = tag.getTagName,
            tagType = tag.getTagType,
            value = ExifValueBytes(ar.toSeq)))
          case jpg: JpegComponent ⇒ Seq(ExifTagInfo(
            directory = directory.getName.toUpperCase,
            tagName = tag.getTagName,
            tagType = tag.getTagType,
            value = ExifValueString(tag.getDescription)))
          case str: String ⇒ Seq(ExifTagInfo(
            directory = directory.getName.toUpperCase,
            tagName = tag.getTagName,
            tagType = tag.getTagType,
            value = ExifValueString(str)))
          case o: AnyRef ⇒ {
            val log = LoggerFactory.getLogger(ReadWriteExifUtil.getClass)
            log.warn("Tag: %s: Not suported value type %s".format(tag.getTagName, o.getClass))
            Seq()
          }
        }
        value
      })
    })
  }.toSeq

  /**
   * To join an image and exif information into a new jpg image.
   * NOTE: the framework deletes some tagTypes and will be lost.
   *       for example TIFF_TAG_STRIP_OFFSETS (0x111 273)
   * NOTE2: The framework also changes byte arrays to integer arrays
   *
   * @param source The image contents to use
   * @param exifInf The exif information
   * @return byte array with EXIF enriched image
   */
  def writeExif(source: Array[Byte], exifInf: Seq[ExifTagInfo]): Array[Byte] = {

    val outputSet = new TiffOutputSet()
    val exifDirectory = outputSet.getOrCreateExifDirectory
    val rootDirectory = outputSet.getOrCreateRootDirectory

    exifInf.foreach(exif ⇒ {
      exif.directory match {
        case ExifTagInfo.directoryJpeg        ⇒ //skip writing
        case ExifTagInfo.directoryJfif        ⇒ //skip writing
        case ExifTagInfo.directoryJpegComment ⇒ //skip writing
        case _ ⇒ {
          val (directory, dirType) = exif.directory match {
            case ExifTagInfo.directoryExif_IFD0 ⇒ (rootDirectory, TiffDirectoryType.TIFF_DIRECTORY_IFD3)
            case _                              ⇒ (exifDirectory, TiffDirectoryType.TIFF_DIRECTORY_IFD3)
          }
          exif.value match {
            case ExifValueString(str) ⇒ {
              val info = new TagInfoAscii(exif.tagName, exif.tagType, str.length + 1, dirType)
              directory.add(info, str)
            }
            case ExifValueBytes(byteArray) ⇒ {
              val info = new TagInfoByte(exif.tagName, exif.tagType, byteArray.length, dirType)
              directory.add(info, byteArray: _*)
            }
            case ExifValueIntegers(intArray) ⇒ {
              val info = new TagInfoLong(exif.tagName, exif.tagType, intArray.length, dirType)
              directory.add(info, intArray: _*)
            }
            case ExifValueLongs(longArray) ⇒ {
              val info = new TagInfoLong(exif.tagName, exif.tagType, longArray.length, dirType)
              directory.add(info, longArray.map(_.toInt): _*)
            }
            case ExifValueShorts(shortArray) ⇒ {
              val info = new TagInfoShort(exif.tagName, exif.tagType, shortArray.length, dirType)
              directory.add(info, shortArray: _*)
            }
            case ExifValueRational(radArray) ⇒ {
              val info = new TagInfoRational(exif.tagName, exif.tagType, radArray.length, dirType)
              val value = radArray.map(rad ⇒
                new RationalNumber(rad.numerator, rad.denominator))
              directory.add(info, value: _*)
            }
          }
        }
      }
    })

    val comment = getComment(exifInf)
    val src = writeComment(comment, source)
    val os = new ByteArrayOutputStream()

    try {
      new ExifRewriter().updateExifMetadataLossless(src, os, outputSet)
      os.toByteArray
    } finally {
      IoUtils.closeQuietly(false, os)
    }
  }

  def writeComment(comment: Option[String], data: Array[Byte]): Array[Byte] = comment.flatMap(comment ⇒ {
    val bos = new ByteArrayOutputStream()
    try {
      val jcw = new JpegCommentWriter(bos, new ByteArrayInputStream(data))
      jcw.write(comment)
      jcw.close
      Some(bos.toByteArray)
    } catch {
      case t: Throwable ⇒ {
        val log = LoggerFactory.getLogger(ReadWriteExifUtil.getClass)
        log.error("Could not add comment to jpg file", t)
        None
      }
    }
  }).getOrElse(data)

  def getComment(exifInf: Seq[ExifTagInfo]): Option[String] = {
    val commentTag = exifInf.find(tag ⇒ tag.directory == ExifTagInfo.directoryJpegComment &&
      tag.tagType == 0)
    commentTag.flatMap(_.value match {
      case ExifValueString(str) ⇒ Some(str)
      case _                    ⇒ None
    })
  }

  def getFileContents(file: File): Array[Byte] = {
    val input = new FileInputStream(file)

    try {
      val fos = new ByteArrayOutputStream //(65535)
      val bis = new BufferedInputStream(input)
      val buf = new Array[Byte](1024)
      Stream.continually(bis.read(buf)).takeWhile(_ != -1).foreach(fos.write(buf, 0, _))
      fos.toByteArray
    } finally {
      input.close()
    }
  }
}