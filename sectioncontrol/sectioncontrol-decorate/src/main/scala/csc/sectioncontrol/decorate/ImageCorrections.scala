package csc.sectioncontrol.decorate

import java.awt.Color
import java.awt.image.BufferedImage

/**
 * used to apply color/gamma and autolevel histogram corrections to a given image
 */
object ImageCorrections {

  /**
   *
   * @param channel
   * @param percentage
   * @param pixels
   */
  case class ChannelResult(channel: Int = 0, percentage: Double = 0.0d, pixels: Long = 0l)

  /**
   *  apply rgb correction to a image
   * @param original picture
   * @param red the amount of red added (-255-255)
   * @param green the amount of green added (-255-255)
   * @param blue the amount of green added (-255-255)
   * @return
   */
  def applyRGBCorrection(original: BufferedImage, red: Int, green: Int, blue: Int): BufferedImage = {
    require(red >= -255 && red <= 255, "red must be between -255 and 255")
    require(green >= -255 && green <= 255, "green must be between -255 and 255")
    require(blue >= -255 && blue <= 255, "blue must be between -255 and 255")

    val image = new BufferedImage(original.getWidth, original.getHeight, original.getType)

    imagePixels(original) {
      case (x: Int, y: Int, rgb: Color) ⇒
        val newColor = new Color(color(rgb.getRed + red), color(rgb.getGreen + green), color(rgb.getBlue + blue))
        image.setRGB(x, y, newColor.getRGB)
    }

    image
  }

  /**
   *
   * @param original
   * @param correction
   * @return
   */
  def applyGammaCorrection(original: BufferedImage, correction: Double): BufferedImage = {
    require(correction != 0, "gamma must not be 0")

    val gamma = 1 / correction
    lazy val gammaLookUpTable = (0 until 256).map(i ⇒ (255 * (math.pow(i.toDouble / 255, gamma))).toInt).toArray
    val image = new BufferedImage(original.getWidth, original.getHeight, original.getType)

    imagePixels(original) {
      case (x: Int, y: Int, rgb: Color) ⇒
        val newPixel = colorToRGB(rgb.getAlpha, gammaLookUpTable(rgb.getRed), gammaLookUpTable(rgb.getGreen), gammaLookUpTable(rgb.getBlue))
        image.setRGB(x, y, newPixel)
    }

    image
  }

  /**
   *
   * @param original
   * @param blackStart
   * @param blackMargin
   * @param whiteStart
   * @param whiteMargin
   * @return
   */
  def applyDynamicAutoLevel(original: BufferedImage, blackStart: Int, blackMargin: Double, whiteStart: Int, whiteMargin: Double): (BufferedImage, Int, Int) = {
    require(blackStart >= 0 && blackStart <= 255, "black start value must be between 0 and 255")
    require(blackMargin >= 0 && blackMargin <= 100, "black margin value must be between 0 and 100 percent")
    require(whiteStart >= 0 && whiteStart <= 255, "white start value must be between 0 and 255")
    require(whiteMargin >= 0 && whiteMargin <= 100, "white margin value must be between 0 and 100 percent")

    val (newBlackMargin, newWhiteMargin) = determineDynamicMargin(original, blackStart, blackMargin, whiteStart, whiteMargin)
    (applyAutoLevel(original, newBlackMargin, newWhiteMargin), newBlackMargin, newWhiteMargin)
  }

  /**
   *
   * @param original
   * @param blackMargin
   * @param whiteMargin
   * @return
   */
  def applyAutoLevel(original: BufferedImage, blackMargin: Int, whiteMargin: Int): BufferedImage = {
    require(blackMargin >= 0 && blackMargin <= 255, "black start value must be between 0 and 255")
    require(whiteMargin >= 0 && whiteMargin <= 255, "white start value must be between 0 and 255")
    require(whiteMargin > blackMargin, "white start value must be higher than black start value")
    require(whiteMargin != blackMargin, "white start value must not be the same as black start value")

    val image: BufferedImage = new BufferedImage(original.getWidth, original.getHeight, original.getType)

    def newChannelValue(channel: Int, left: Int, right: Int) = {
      if (channel < left) 0
      else if (channel > right) 255
      else (channel - left) * 255 / (right - left)
    }

    imagePixels(original) {
      case (x: Int, y: Int, rgb: Color) ⇒
        val red = newChannelValue(rgb.getRed, blackMargin, whiteMargin)
        val green = newChannelValue(rgb.getGreen, blackMargin, whiteMargin)
        val blue = newChannelValue(rgb.getBlue, blackMargin, whiteMargin)
        val color = new Color(red, green, blue)
        image.setRGB(x, y, color.getRGB)
    }

    image
  }

  private def determineDynamicMargin(original: BufferedImage, blackStart: Int, blackMargin: Double, whiteStart: Int, whiteMargin: Double): (Int, Int) = {
    val minimalSpreadBetweenChannels = 10
    val blackSkips = (0 until blackStart).toList
    val whiteSkips = (255 until whiteStart by -1).toList
    val channelsToSkip = (blackSkips ::: whiteSkips).distinct

    val histogram = totalHistogram(original)
    val totalImagePixels = calculateTotalPixels(histogram, channelsToSkip)
    val percentage = 100d / totalImagePixels

    val calculateMarginFinal = calculateMargin(original, percentage, histogram)_
    val finalBlack = calculateMarginFinal((blackStart until histogram.length), blackStart, blackMargin)
    val finalWhite = calculateMarginFinal((whiteStart to (finalBlack.channel + minimalSpreadBetweenChannels) by -1), whiteStart, whiteMargin)

    if (finalWhite.channel <= finalBlack.channel) (finalBlack.channel, finalBlack.channel + minimalSpreadBetweenChannels)
    else (finalBlack.channel, finalWhite.channel)
  }

  private def calculateMargin(original: BufferedImage, percentage: Double, histogram: Seq[Long])(range: Range, startChannel: Int, margin: Double): ChannelResult = {
    val (result, _) = range.foldLeft(ChannelResult(channel = startChannel), false) {
      case ((channelResult, done), currentChannel) ⇒
        if (!done) {
          val currentTotalPixels = channelResult.pixels + histogram(currentChannel)
          val currentTotalPercentage = percentage * currentTotalPixels

          if (currentTotalPercentage <= margin) (ChannelResult(channel = currentChannel, percentage = currentTotalPercentage, pixels = currentTotalPixels), false)
          else (channelResult, true)
        } else {
          (channelResult, true)
        }
    }

    result
  }

  private def calculateTotalPixels(histogram: Seq[Long], skip: List[Int]): Long = {
    histogram.zipWithIndex.filterNot(a ⇒ skip.exists(_ == a._2)).map(_._1).sum
  }

  private def histogram(original: BufferedImage): (Seq[Int], Seq[Int], Seq[Int]) = {
    val reds = Array.fill[Int](256)(0)
    val greens = Array.fill[Int](256)(0)
    val blues = Array.fill[Int](256)(0)

    imagePixels(original) {
      case (x: Int, y: Int, rgb: Color) ⇒
        reds(rgb.getRed) += 1
        greens(rgb.getGreen) += 1
        blues(rgb.getBlue) += 1
    }

    (reds, greens, blues)
  }

  def totalHistogram(image: BufferedImage): Seq[Long] = {
    val (reds, greens, blues) = histogram(image)
    (reds, greens, blues).zipped.map(_.toLong + _.toLong + _.toLong).toList
  }

  def calculateHistogramMean(image: BufferedImage): Double = {
    val (totalWeight, totalPixels) = totalHistogram(image).zipWithIndex.foldLeft((0L, 0L)) {
      case ((tw, tp), (channelCount, channel)) ⇒
        (tw + (channel * channelCount), tp + channelCount)
    }
    totalWeight.toDouble / totalPixels
  }

  private def colorToRGB(alpha: Int, red: Int, green: Int, blue: Int): Int = {
    var newPixel = 0
    newPixel += alpha
    newPixel = newPixel << 8
    newPixel += red
    newPixel = newPixel << 8
    newPixel += green
    newPixel = newPixel << 8
    newPixel += blue

    newPixel
  }

  private def color(rgb: Int) = {
    if (rgb > 255) 255
    else if (rgb < 0) 0
    else rgb
  }

  private def imagePixels(image: BufferedImage)(f: (Int, Int, java.awt.Color) ⇒ Unit) {
    for (x ← 0 until image.getWidth; y ← 0 until image.getHeight) {
      f(x, y, new Color(image.getRGB(x, y)))
    }
  }
}
