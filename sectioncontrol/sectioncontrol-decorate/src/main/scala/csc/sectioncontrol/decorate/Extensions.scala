package csc.sectioncontrol.decorate

//copyright CSC 2010

import java.io._
import java.util.{ Date, TimeZone }

import csc.akkautils.DirectLogging
import org.apache.commons.compress.archivers.{ ArchiveEntry, ArchiveStreamFactory }
import org.apache.commons.compress.utils.IOUtils

import scala.collection.mutable.ListBuffer
import java.text.SimpleDateFormat

/**
 * Object holder for different object extensions.
 */
//TODO: move to CSC common project (copied from cts_sensor/baseregistration)
object Extensions {
  implicit def files(file: File) = new FileExtensions(file)
  implicit def strings(string: String) = new StringExtensions(string)
  implicit def dates(date: Date) = new DateExtensions(date)

  //http://polyglot-window.blogspot.com/2009/03/arm-blocks-in-scala-revisited.html
  def using[T <: { def close() }](resource: T)(block: T ⇒ Unit) {
    try {
      block(resource)
    } finally {
      if (resource != null) resource.close()
    }
  }
}

/**
 * Supported utility functions for Date Objects
 */
class DateExtensions(date: Date) {
  /**
   * Create formatted date using given format and timezone
   * @param format: format of the representation of the date
   * @param timeZone: Timezone as String
   */
  def toString(format: String, timeZone: String = "CET") = {
    var dateFormat = new SimpleDateFormat(format)
    dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone))

    dateFormat.format(date)
  }
}

/**
 * String extensions
 */
class StringExtensions(string: String) {

  /**
   * Create a date object using a string formated as specified
   * @param format the string format
   * @param timeZone the timezone of the string representation
   */
  def toDate(format: String, timeZone: String = "CET"): Date = {
    var dateFormat = new SimpleDateFormat(format)
    dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone))

    dateFormat.parse(string)
  }
}

/**
 * File extensions methods
 */
class FileExtensions(file: File) extends DirectLogging {
  /**
   *   ArchiveEntry
   * unpack a compressed file
   */
  def unpack(pathToStore: String): Boolean = {
    var folder = new File(pathToStore)
    if (!folder.exists) {
      folder.mkdir
    }

    import csc.sectioncontrol.decorate.Extensions._

    var files = new ListBuffer[ArchiveEntry]()

    try {
      using(new BufferedInputStream(new FileInputStream(file))) { input ⇒
        val archive = new ArchiveStreamFactory().createArchiveInputStream(input)
        var entry = archive.getNextEntry
        while (entry != null) {
          using(new FileOutputStream(new File(pathToStore, entry.getName))) { output ⇒
            IOUtils.copy(archive, output)
          }
          entry = archive.getNextEntry
        }
      }
      return true
    } catch {
      case e: Exception ⇒
        log.error("failed unpacking [%s] => %s".format(file.getAbsolutePath, e.getStackTraceString))
        return false
    }
  }

  /**
   * retrieve extension of given file
   */
  def getExtension: Option[String] = {
    val name = file.getName
    var begin = name.lastIndexOf(".")

    if (begin == -1)
      return None
    else
      return Some(name.substring(begin + 1, name.length))
  }
}
