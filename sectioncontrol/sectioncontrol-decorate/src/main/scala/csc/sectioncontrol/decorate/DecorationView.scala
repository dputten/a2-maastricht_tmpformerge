package csc.sectioncontrol.decorate

import java.util.{ Date, Locale }

import csc.curator.utils.Curator
import csc.sectioncontrol.decorate.Extensions._
import csc.sectioncontrol.messages.{ ValueWithConfidence_Float, VehicleImage, VehicleMetadata }
import csc.sectioncontrol.sign.certificate.CertificateService
import csc.sectioncontrol.storage.Decorate.DecorationType
import csc.sectioncontrol.storage.Decorate.DecorationType.DecorationType
import csc.sectioncontrol.storage.Direction
import csc.sectioncontrol.storagelayer.CorridorConfig

/**
 * Created by carlos on 23.07.15.
 * Represents the visible difference between decorated images, based on DecorationType
 */
trait DecorationView {
  /**
   * @return whether this decoration contains a license image
   */
  def includeLicense: Boolean

  /**
   * @return whether the number of columns this decoration uses
   */
  def columnCount: Int

  /**
   * @return text that should be visible in the decorated image
   */
  def content: List[String]

}

case class IRoseSystemData(interval: Int, loopDistance: Int)

case class LaneSeqConfig(seqNumber: Option[Int] = None, seqName: Option[String] = None)

/**
 * Represents the context containing all data that might be used to produce the view result.
 */
trait ViewContext {

  def curator: Curator
  def corridor: Option[CorridorConfig]
  def certService: CertificateService
  def laneSeqConfig: LaneSeqConfig
  def laneData: Option[IRoseSystemData]

  def msg: VehicleMetadata
  def image: VehicleImage
  def systemId: String
}

/**
 * Base class for DecorationView with ViewContext, receiving a delegate context.
 * All the string content defs used by more than one DecorationType will be declared here.
 * @param ctx delegate ViewContext
 */
abstract class BaseDecorationView(ctx: ViewContext) extends DecorationView with ViewContext {

  val datePattern = "dd-MM-yyyy HH:mm:ss.SSS"
  val datePattern2 = "dd-MM-yyyy HH:mm:ss z"

  override def curator: Curator = ctx.curator
  override def msg: VehicleMetadata = ctx.msg
  override def systemId: String = ctx.systemId
  override def image: VehicleImage = ctx.image
  override def corridor: Option[CorridorConfig] = ctx.corridor
  override def certService: CertificateService = ctx.certService
  override def laneSeqConfig: LaneSeqConfig = ctx.laneSeqConfig
  override def laneData: Option[IRoseSystemData] = ctx.laneData

  def getImageTimeStamp(timezone: String): String = {
    val dateString = new Date(image.timestamp).toString(datePattern2, timezone)
    "Datum: " + dateString
  }

  def laneDescriptionEntry: String = {
    "Locatie van het passagepunt: " + msg.lane.bpsLaneId.getOrElse(msg.lane.system + " " + msg.lane.gantry + " " + msg.lane.name)
  }

  def nmiCertificateEntry: String = {
    "NMi Type certificaat: " + msg.NMICertificate
  }

  def appChecksumEntry: String = {
    "SW: " + activeSoftwareSeal
  }

  def appConfigurationSealEntry: String = {
    "CF: " + activeConfigurationSeal
  }

  private def activeConfigurationSeal: String =
    certService.getActiveConfigurationCertificate(curator, systemId, msg.eventTimestamp).checksum

  private def activeSoftwareSeal: String =
    certService.getActiveSoftwareCertificate(curator, systemId, msg.eventTimestamp).checksum

  def eventIdEntry: String = {
    "EventId: " + msg.eventId
  }

  private def getLocationLinesFromConfig(config: Option[CorridorConfig]): (String, String) = config match {
    case Some(_config) ⇒
      _config.info.locationLine2 match {
        case Some(line) ⇒ (_config.info.locationLine1, " / " + line)
        case None       ⇒ (_config.info.locationLine1, "")
      }

    case None ⇒
      ("Onbekend", "")
  }

  def locationDescriptionRedLightEntry = {
    val (locationLine1, locationLine2) = getLocationLinesFromConfig(corridor)
    "Locatie: %s %s%s".format(systemId, locationLine1, locationLine2).padTo(105, ' ')
  }

  def locationDescriptionSpeedFixedEntry = {
    val (locationLine1, locationLine2) = getLocationLinesFromConfig(corridor)
    "Locatie: %s %s%s".format(systemId, locationLine1, locationLine2).padTo(105, ' ')
  }

  def typeAprovalNumberEntry: String = {
    "Typegoedkeuringsnummer: " + typeCertificate(image.timestamp)
  }

  private def typeCertificate(time: Long): String = {
    val cert = certService.getCurrentCertificate(curator, systemId, image.timestamp)
    cert.map(_.measurementType.typeCertificate.id).getOrElse("")
  }

  def serialNumberEntry: String = {
    val serialNumber = msg.serialNr
    "Serienummer: " + serialNumber
  }

  def categoryEntry: String = {
    val cert = certService.getCurrentCertificate(curator, systemId, image.timestamp)
    "Categorie: %s".format(cert.map(_.measurementType.category).getOrElse("A"))
  }

  def speedEntry: String = {
    val speed = msg.speed.getOrElse(new ValueWithConfidence_Float(0.0f, 100))
    val cert = certService.getCurrentCertificate(curator, systemId, image.timestamp)
    val unit = cert.map(_.measurementType.unitSpeed).getOrElse("km/h")
    "Snelheid: %d %s".format(speed.value.floor.toInt, unit)
  }

  def laneNumberEntry: String = {
    val seqNumber = laneSeqConfig.seqNumber.getOrElse(0)
    val seqName = laneSeqConfig.seqName.getOrElse("Onbekend")
    "Rijstrook: %d %s".format(seqNumber, seqName)
  }

  def directionEntry: String = {
    def getDirectionCode(directionO: Option[Direction.Value]): String = directionO match {
      case Some(direction) ⇒ direction match {
        case Direction.Incoming ⇒ "Tegemoetkomend"
        case Direction.Outgoing ⇒ "Afgaand"
      }
      case None ⇒ "?"
    }

    "Rijrichting: %s".format(getDirectionCode(msg.direction))
  }

  def redTimeEntry: String = {
    val redTime = image.timeRed.getOrElse(0L) / 1000f
    val cert = certService.getCurrentCertificate(curator, systemId, image.timestamp)
    val unit = cert.map(_.measurementType.unitRedLight).getOrElse("s")
    "Roodtijd: %2.1f %s".formatLocal(Locale.GERMAN, redTime, unit)
  }

  def yellowTimeEntry: String = {
    val yellowTime = image.timeYellow.getOrElse(0L) / 1000f
    val cert = certService.getCurrentCertificate(curator, systemId, image.timestamp)
    val unit = cert.map(_.measurementType.unitRedLight).getOrElse("s")
    "Geeltijd: %2.1f %s".formatLocal(Locale.GERMAN, yellowTime, unit)
  }

  def intervalFromSystemDataEntry: String = {
    val intervalText = getLaneData match {
      case Some(systemData) ⇒
        "%d mm".format(systemData.interval)
      case None ⇒
        "onbekend"
    }
    "Ingesteld interval: %s".format(intervalText)
  }

  def interval0000Entry: String = intervalEntry(Some(0L))

  def interval1FromMessageEntry: String = {
    intervalEntry(msg.interval1Ms)
  }

  def interval2FromMessageEntry: String = {
    intervalEntry(msg.interval2Ms)
  }

  private def intervalEntry(intervalO: Option[Long]): String = {
    val intervalText = intervalO match {
      case Some(interval) ⇒
        val f = interval / 1000f
        "%1.3f s".formatLocal(Locale.GERMAN, f)
      case None ⇒
        "onbekend"
    }
    "Intervaltijd: %s".format(intervalText)
  }

  def loopDistanceMmEntry: String = {
    val loopDistanceText = laneData match {
      case Some(systemData) ⇒
        "%d mm".format(systemData.loopDistance)
      case None ⇒
        "onbekend"
    }
    "Lusafstand: %s".format(loopDistanceText)
  }

  def loopDistanceCmEntry: String = {
    val loopDistanceText = laneData match {
      case Some(systemData) ⇒
        val loopDistanceCm = (systemData.loopDistance / 10f).round
        "%d cm".format(loopDistanceCm)
      case None ⇒
        "onbekend"
    }
    "Lusafstand: %s".format(loopDistanceText)
  }

  def violationTypeEntry(violationText: String): String = {
    "Overtredingstype: %s".format(violationText)
  }

  def photoNumberEntry(value: String) = "Fotonummer: %s".format(value)

  val emptyEntry: String = " "

  private def getLaneData: Option[IRoseSystemData] = {
    //TODO csilva: move IRoseSystemData to zk (to be done in other story)
    val id = msg.lane.laneId
    //TODO csilva: originally VehicleRegistrationConfig.Some(iRose).get("lane%s".format(laneId))
    val laneId = "lane%s".format(id)
    None
  }
}

class Overview1View(ctx: ViewContext) extends BaseDecorationView(ctx) {

  override def getImageTimeStamp(timezone: String): String = {
    val dateString = new Date(image.timestamp).toString(datePattern, timezone)
    "Datum en tijd van de passage: " + dateString
  }

  override def content: List[String] = List(
    getImageTimeStamp("Europe/Amsterdam"),
    laneDescriptionEntry,
    nmiCertificateEntry,
    appChecksumEntry,
    appConfigurationSealEntry,
    eventIdEntry)

  override def includeLicense: Boolean = false
  override def columnCount: Int = 1
}

class RedLight1View(ctx: ViewContext) extends BaseDecorationView(ctx) {
  override def content: List[String] =
    List(
      getImageTimeStamp("Europe/Amsterdam"),
      locationDescriptionRedLightEntry,
      categoryEntry,
      laneNumberEntry,
      directionEntry,
      speedEntry,
      typeAprovalNumberEntry,
      serialNumberEntry,
      photoNumberEntry("A"),
      interval0000Entry,
      redTimeEntry,
      yellowTimeEntry,
      loopDistanceCmEntry)

  override def includeLicense: Boolean = false
  override def columnCount: Int = 3
}

class RedLight2View(ctx: ViewContext) extends BaseDecorationView(ctx) {
  override def content: List[String] =
    List(
      getImageTimeStamp("Europe/Amsterdam"),
      locationDescriptionRedLightEntry,
      categoryEntry,
      laneNumberEntry,
      directionEntry,
      speedEntry,
      typeAprovalNumberEntry,
      serialNumberEntry,
      photoNumberEntry("B"),
      interval1FromMessageEntry,
      redTimeEntry,
      yellowTimeEntry,
      loopDistanceCmEntry)

  override def includeLicense: Boolean = true
  override def columnCount: Int = 3
}

class Speed1View(ctx: ViewContext) extends BaseDecorationView(ctx) {
  override def content: List[String] =
    List(
      getImageTimeStamp("Europe/Amsterdam"),
      locationDescriptionSpeedFixedEntry,
      categoryEntry,
      laneNumberEntry,
      directionEntry,
      speedEntry,
      typeAprovalNumberEntry,
      serialNumberEntry,
      photoNumberEntry("A"),
      interval0000Entry,
      loopDistanceCmEntry)

  override def includeLicense: Boolean = true
  override def columnCount: Int = 3
}

class Speed2View(ctx: ViewContext) extends BaseDecorationView(ctx) {
  override def content: List[String] = {
    List(
      getImageTimeStamp("Europe/Amsterdam"),
      locationDescriptionSpeedFixedEntry,
      categoryEntry,
      laneNumberEntry,
      directionEntry,
      speedEntry,
      typeAprovalNumberEntry,
      serialNumberEntry,
      photoNumberEntry("B"),
      interval2FromMessageEntry,
      loopDistanceCmEntry)
  }

  override def includeLicense: Boolean = false
  override def columnCount: Int = 3
}

/**
 * Mutable impl of ViewContext. Allows the same object to be kept and reused when needed
 */
class MutableContext extends ViewContext {

  var msg: VehicleMetadata = null
  var image: VehicleImage = null
  var curator: Curator = null
  var systemId: String = null
  var corridor: Option[CorridorConfig] = None
  var certService: CertificateService = CertificateService
  var laneSeqConfig: LaneSeqConfig = LaneSeqConfig(None, None) //TODO csilva populate this properly
  var laneData: Option[IRoseSystemData] = None //TODO csilva populate this properly, see method getLaneData

}

object DecorationView {

  /**
   * @param decorationType
   * @param ctx
   * @return DecorationView for the corresponding DecorationType and based on the given ViewContext
   */
  def getView(decorationType: DecorationType, ctx: ViewContext): DecorationView = decorationType match {
    case DecorationType.Overview_1 ⇒ new Overview1View(ctx)
    case DecorationType.RedLight_1 ⇒ new RedLight1View(ctx)
    case DecorationType.RedLight_2 ⇒ new RedLight2View(ctx)
    case DecorationType.Speed_1    ⇒ new Speed1View(ctx)
    case DecorationType.Speed_2    ⇒ new Speed2View(ctx)
  }

}