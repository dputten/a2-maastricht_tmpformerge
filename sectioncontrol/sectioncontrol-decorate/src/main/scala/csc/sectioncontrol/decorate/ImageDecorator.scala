/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.decorate

import java.awt.{ Color, Dimension, Font, Point }
import java.util.{ Calendar, Date }

import csc.akkautils.DirectLogging
import csc.sectioncontrol.decorate.Image.Corner
import csc.sectioncontrol.messages.VehicleMetadata
import csc.sectioncontrol.storage.Decorate.{ DecorationConfig, ImageColorConfig, DecorateConfig }
import csc.sectioncontrol.storage.Decorate.DecorationType._

/**
 * Class for decorating images.
 */
class ImageDecorator(decorationType: DecorationType,
                     config: ⇒ DecorateConfig,
                     jpegQuality: Int) extends DirectLogging {
  val logoImageResourcePath = Some("csc_logo_black.jpg")
  val decorationConfig = getDecorationConfig(config, decorationType)

  def backgroundColor = new Color(config.backgroundColorRGB.R, config.backgroundColorRGB.G, config.backgroundColorRGB.B)

  def font = config.fontName match {
    case Some(name) ⇒ new Font(name, Font.PLAIN, 12)
    case None       ⇒ new Font(Font.SANS_SERIF, Font.PLAIN, 12)
  }

  def fontColor = new Color(config.fontColorRGB.R, config.fontColorRGB.G, config.fontColorRGB.B)

  val logoImage: Option[Image] = for (
    path ← logoImageResourcePath;
    image ← loadImage(path)
  ) yield image

  def loadImage(path: String): Option[Image] = try {
    Some(new Image(getClass.getClassLoader.getResourceAsStream(path)))
  } catch {
    case e: Exception ⇒
      log.error("Could not load logo image [%s] from resource".format(logoImageResourcePath))
      None
  }

  /**
   * Apply image corrections the a given RegistrationImage. This can be gamma, contract and brightness corrections
   * @param baseImage the image that will be corrected
   * @return a new Image with the applied color corrections
   */
  def applyImageCorrections(baseImage: Image, timestamp: Long, imageCorrectionIds: Option[Seq[String]]): Image = {
    /**
     * creates a new Date object that contains the year, month and day from one date and
     * hour, minute, seconds, and mills from the other
     * @param date Date whereby year, month and day will be used
     * @param time Date whereby hour, minute, seconds, and mills will be used
     * @return returns new date that is a combination of two dates
     */
    def combineDates(date: Date, time: Date): Date = {
      val dateCal = Calendar.getInstance()
      val timeCal = Calendar.getInstance()

      dateCal.setTimeInMillis(date.getTime)
      timeCal.setTimeInMillis(time.getTime)

      val dateTimeCal = Calendar.getInstance()
      dateTimeCal.clear()

      //user the year, month and day from 'date' parameter
      dateTimeCal.set(Calendar.YEAR, dateCal.get(Calendar.YEAR))
      dateTimeCal.set(Calendar.MONTH, dateCal.get(Calendar.MONTH))
      dateTimeCal.set(Calendar.DATE, dateCal.get(Calendar.DATE))

      //user hour, minute, seconds, and mills from 'time' parameter
      dateTimeCal.set(Calendar.HOUR_OF_DAY, timeCal.get(Calendar.HOUR_OF_DAY))
      dateTimeCal.set(Calendar.MINUTE, timeCal.get(Calendar.MINUTE))
      dateTimeCal.set(Calendar.SECOND, timeCal.get(Calendar.SECOND))
      dateTimeCal.set(Calendar.MILLISECOND, timeCal.get(Calendar.MILLISECOND))

      // returns new date that is the combination of two dates
      dateTimeCal.getTime
    }

    val imageCorrectionsToBeApplied = imageCorrectionIds match {
      case Some(ids) ⇒
        config.imageCorrections.filter(ic ⇒ ids.contains(ic.imageCorrectionId))

      case None ⇒
        Seq()
    }
    imageCorrectionsToBeApplied.foldLeft(baseImage) {
      case (image, correction) ⇒
        val imageDate = new Date(timestamp)
        val from = combineDates(imageDate, correction.fromDate)
        val to = combineDates(imageDate, correction.toDate)
        val config = correction.colorConfig

        log.debug("from=[%s], to=[%s], imageDate=[%s]".format(from, to, imageDate))

        //apply image corrections only within the given time fame
        if (imageDate.getTime >= from.getTime && imageDate.getTime <= to.getTime) {

          log.debug("applying image corrections, gamma:[%s], brightness:[%s], contrast:[%s], red:[%s], green:[%s], blue:[%s], autoLevel:[%s]".format(
            config.gamma, config.brightness, config.contrast, config.red, config.green, config.blue, config.autoLevel))

          //apply auto level histogram correction
          val autoLevelCorrected = config match {
            case ImageColorConfig(_, _, _, _, _, _, Some(autoLevel)) ⇒ image.applyAutoLevel(
              blackStart = autoLevel.blackStart,
              blackMargin = autoLevel.blackMargin,
              whiteStart = autoLevel.whiteStart,
              whiteMargin = autoLevel.whiteMargin)
            case _ ⇒ image
          }

          //apply gamma correction
          val gammaCorrected = config match {
            case ImageColorConfig(_, _, Some(gamma), _, _, _, _) ⇒ autoLevelCorrected.applyGammaCorrection(gamma)
            case _ ⇒ autoLevelCorrected
          }

          //apply color correction
          val colorCorrected = config match {
            case ImageColorConfig(_, _, _, Some(red), Some(green), Some(blue), _) ⇒ gammaCorrected.applyRGBCorrection(red, green, blue)
            case ImageColorConfig(_, _, _, Some(red), _, _, _) ⇒ gammaCorrected.applyRGBCorrection(red, 0, 0)
            case ImageColorConfig(_, _, _, _, Some(green), _, _) ⇒ gammaCorrected.applyRGBCorrection(0, green, 0)
            case ImageColorConfig(_, _, _, _, _, Some(blue), _) ⇒ gammaCorrected.applyRGBCorrection(0, 0, blue)
            case _ ⇒ gammaCorrected
          }

          //apply brightness/contrast corrections
          //brightness 1.0f and contrast 0.0f are default settings that does not change the image
          config match {
            case ImageColorConfig(Some(brightness), Some(contrast), _, _, _, _, _) ⇒ colorCorrected.applyBrightnessContrastCorrection(brightness, contrast)
            case ImageColorConfig(Some(brightness), _, _, _, _, _, _) ⇒ colorCorrected.applyBrightnessContrastCorrection(brightness, 0.0f)
            case ImageColorConfig(_, Some(contrast), _, _, _, _, _) ⇒ colorCorrected.applyBrightnessContrastCorrection(1.0f, contrast)
            case _ ⇒ colorCorrected
          }
        } else {
          //no time frame found for the given image. Returning original image.
          image
        }
    }
  }

  def licenseNeedsReplacement(baseImage: Image, histogramMeanUpperLimit: Int, replaceLicenseWithOriginal: Boolean): Boolean = {
    // Tijdelijke logging zodat een goede histogramMeanUpperLimit bepaald kan worden
    //println("The histogram mean is %d".format(ImageCorrections.calculateHistogramMean(baseImage.image).toInt))
    replaceLicenseWithOriginal && (ImageCorrections.calculateHistogramMean(baseImage.image).toInt <= histogramMeanUpperLimit)
  }

  /**
   * Decorate baseImage, keeping the exif tags, based on message content and the DecorationView.
   * @param sourceImage Image to be decorated.
   * @param message The corresponding message.
   * @param view DecorationView providing the content strings and layout information
   * @return The decorated image contents
   */
  def decorate(sourceImage: ImageAndContents, message: VehicleMetadata, view: DecorationView): Option[Array[Byte]] = {

    if (!sourceImage.isComplete) return None //cannot continue

    val contentStrings: List[String] = view.content
    val imageCorrectionIds: Option[Seq[String]] = decorationConfig.imageCorrections

    val timestamp: Long = sourceImage.img.get.timestamp

    val vehicleImageBase = applyImageCorrections(sourceImage.asImage.get, timestamp, imageCorrectionIds)

    val vehicleImage = view.includeLicense match {
      case true ⇒ {
        createCorrectedLicenseSnippet(message, sourceImage.asImage.get, timestamp) match {
          case Some(snippet) ⇒ {
            // Replace license plate
            val licenseNeedsReplacing = licenseNeedsReplacement(sourceImage.asImage.get, config.histogramMeanUpperLimit,
              config.replaceLicenseWithOriginal)
            addLicenseEnhancements(vehicleImageBase, snippet, message, licenseNeedsReplacing, timestamp)
          }
          case None ⇒ vehicleImageBase
        }
      }
      case false ⇒ vehicleImageBase
    }

    val decorationImage: Image = if (config.showLogo && logoImage.isDefined) {
      val textImageWidth = vehicleImage.width - logoImage.get.width
      val stringImage = createStringsImage(contentStrings, textImageWidth, view.columnCount, config.rowHeightPx)
      stringImage.addRight(logoImage.get)
    } else {
      // Decoration consists of columned text.
      createStringsImage(contentStrings, vehicleImage.width, view.columnCount, config.rowHeightPx)
    }

    val result = vehicleImage.addBottom(decorationImage)

    Some(result.getBytes(compression = jpegQuality))
  }

  def createCorrectedLicenseSnippet(msg: VehicleMetadata,
                                    sourceImage: Image,
                                    timestamp: Long): Option[Image] = getLicensePosition(msg) match {
    case Some(vertices) ⇒ {
      val initial = sourceImage.applyPolygonMask(vertices, true)
      val corrected = applyImageCorrections(initial, timestamp, Some(config.licenseImageCorrections))
      Some(corrected)
    }
    case None ⇒ None
  }

  // License enhancements are:
  // - replace license with a version from the original image.
  // - draw a border around the license.
  // - place a license image (ForDecoration) in the bottom right corner.
  def addLicenseEnhancements(vehicleImageBase: Image,
                             licenseSnippet: Image,
                             message: VehicleMetadata,
                             licenseNeedsReplacing: Boolean,
                             timestamp: Long //,
                             //imageCache: ImageCache
                             ): Image = {

    val calcLicenseVerticesInOriginal = getLicensePosition(message)

    if (calcLicenseVerticesInOriginal.isEmpty)
      log.warning("Message does not contain a licensePosition.")

    val imageStep1: Image = calcLicenseVerticesInOriginal match {
      case Some(licenseVerticesInOriginal) ⇒
        val licenseBoundingBoxTL = new Point(
          licenseVerticesInOriginal.map(_.getX.toInt).min,
          licenseVerticesInOriginal.map(_.getY.toInt).min)

        // Replace license plate
        val imageStep1a: Image = licenseNeedsReplacing match {
          case true ⇒ vehicleImageBase.overlayWithClippingPolygon(
            overlayedImage = licenseSnippet,
            topLeft = licenseBoundingBoxTL,
            clippingPolygonVertices = licenseVerticesInOriginal)
          case false ⇒
            vehicleImageBase
        }

        // Add frame around license plate
        val imageStep1b = config.showLicenseFrame match {
          case true ⇒
            val thickness = config.licenseFrameThickness max 1
            val color = new Color(config.licenseFrameColorRGB.R, config.licenseFrameColorRGB.G, config.licenseFrameColorRGB.B)
            imageStep1a.drawOuterQuadrangle(licenseVerticesInOriginal(0), licenseVerticesInOriginal(1),
              licenseVerticesInOriginal(2), licenseVerticesInOriginal(3), color, thickness)
          case false ⇒
            imageStep1a
        }
        imageStep1b

      case None ⇒
        vehicleImageBase
    }

    // Add license image in corner
    val licenseInCorner = getScaledLicense(licenseSnippet)
    imageStep1.overlay(licenseInCorner, Corner.BottomRight)
  }

  def getLicensePosition(metadata: VehicleMetadata): Option[Seq[Point]] = metadata.licensePlateData match {
    case Some(lpd) ⇒ lpd.licensePosition match {
      case Some(licensePosition) ⇒ {
        val vertices = Seq(
          new Point(licensePosition.TLx, licensePosition.TLy),
          new Point(licensePosition.TRx, licensePosition.TRy),
          new Point(licensePosition.BRx, licensePosition.BRy),
          new Point(licensePosition.BLx, licensePosition.BLy))
        Some(vertices)
      }
      case None ⇒ None
    }
    case None ⇒ None
  }

  def getDecorationConfig(config: DecorateConfig, decorationType: DecorationType): DecorationConfig = {
    val decConfigO = config.decorationsConfigs.find(_.decorationType == decorationType)
    decConfigO.getOrElse(new DecorationConfig(decorationType = decorationType))
  }

  def createStringsImage(content: List[String], width: Int, numberOfColumns: Int, rowHeightPx: Int): Image = {
    Image.createColumnedStringsImage(
      content = content,
      width = width,
      numberOfColumns = numberOfColumns,
      rowHeightPx = rowHeightPx,
      borderWidthPx = config.borderWidthPx,
      descentPx = config.borderWidthPx,
      extraLineSpacingPx = config.borderWidthPx,
      backgroundColor = backgroundColor,
      font = Some(font),
      fontColor = fontColor)
  }

  private def getScaledLicense(source: Image): Image = {
    val sourceDim = new Dimension(source.width, source.height)
    val targetDim = getScaledOutputDimensions(sourceDim)
    source.scale(targetDim)
  }

  /**
   * Calculates the dimensions of the output scaled image.
   * The output image is a scaled version of the input image, that best fits the available area.
   * @param inputDimensions source image dimensions
   * @return the dimensions of the output image
   */
  private def getScaledOutputDimensions(inputDimensions: Dimension): Dimension = {
    val maxWidthPx: Int = config.licenseMaxWidthPx
    val maxHeightPx: Int = config.licenseMaxHeightPx
    val widthFactor = maxWidthPx.toFloat / inputDimensions.width.toFloat
    val heightFactor = maxHeightPx.toFloat / inputDimensions.height.toFloat
    val resizeFactor = widthFactor min heightFactor

    new Dimension((inputDimensions.width.toFloat * resizeFactor).toInt, (inputDimensions.height.toFloat * resizeFactor).toInt)
  }

}
