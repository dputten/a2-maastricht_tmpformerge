package jpg;// Jpeg.java
// $Id: Jpeg.java,v 1.2 1999/10/11 09:40:15 ylafon Exp $
// (c) COPYRIGHT MIT, INRIA and Keio, 1999.
// Please first read the full copyright statement in file COPYRIGHT.html



/**
 * @version $Revision: 1.2 $
 * @author  Benoit Mahe (bmahe@w3.org)
 */
public interface Jpeg {

    /* Start Of Frame N */
    public int M_SOF0  = 0xC0;

    /* N indicates which compression process */
    public int M_SOF1  = 0xC1;

    /* Only SOF0-SOF2 are now in common use */
    public int M_SOF2  = 0xC2;
    public int M_SOF3  = 0xC3;

    /* NB: codes C4 and CC are NOT SOF markers */
    public int M_SOF5  = 0xC5;
    public int M_SOF6  = 0xC6;
    public int M_SOF7  = 0xC7;
    public int M_SOF9  = 0xC9;
    public int M_SOF10 = 0xCA;
    public int M_SOF11 = 0xCB;
    public int M_SOF13 = 0xCD;
    public int M_SOF14 = 0xCE;
    public int M_SOF15 = 0xCF;

    /* Start Of Image (beginning of datastream) */
    public int M_SOI   = 0xD8;

    /* End Of Image (end of datastream) */
    public int M_EOI   = 0xD9;

    /* Start Of Scan (begins compressed data) */
    public int M_SOS   = 0xDA;

    /* Application-specific marker, type N */
    public int M_APP0  = 0xE0;

    /* (we don't bother to list all 16 APPn's) */
    public int M_APP1  = 0xE1;
    public int M_APP9  = 0xE9;
    public int M_APP12 = 0xEC;
    public int M_COM   = 0xFE;
    /* The maximal comment length */
    public int M_MAX_COM_LENGTH = 65500;

}








