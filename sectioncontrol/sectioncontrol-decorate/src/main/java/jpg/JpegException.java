package jpg;// JpegException.java
// $Id: JpegException.java,v 1.1 1999/09/21 12:20:24 bmahe Exp $
// (c) COPYRIGHT MIT, INRIA and Keio, 1999.
// Please first read the full copyright statement in file COPYRIGHT.html


/**
 * @version $Revision: 1.1 $
 * @author  Benoit Mahe (bmahe@w3.org)
 */
public class JpegException extends Exception {

    public JpegException(String msg) {
	super(msg);
    }

}