package csc.sectioncontrol.decorate

import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers
import csc.curator.CuratorTestServer
import csc.akkautils.DirectLoggingAdapter
import csc.curator.utils.{ Curator, CuratorToolsImpl }
import akka.testkit.TestActorRef
import csc.sectioncontrol.storage.Decorate._
import net.liftweb.json.{ DefaultFormats, Formats }
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.messages._
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storage.Decorate.DecorateConfig

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 9/17/15.
 */

class DecorationConfigServiceTest extends WordSpec with MustMatchers with CuratorTestServer with BeforeAndAfterAll {
  val log = new DirectLoggingAdapter(this.getClass.getName)
  private var curator: Curator = _
  private val formats = DefaultFormats + new EnumerationSerializer(
    VehicleCategory,
    ZkWheelbaseType,
    VehicleCode,
    ZkIndicationType,
    ServiceType,
    ZkCaseFileType,
    EsaProviderType,
    VehicleImageType,
    DayReportVersion,
    SpeedIndicatorType,
    ConfigType,
    DecorationType,
    VehicleImageType)

  case class RowHeightPix(rowHeightPix: Int)
  "DecorationConfigService" must {
    "properly combine DecorationConfig items from zookeeper" in {
      val defaultDecoration = DecorationConfigService.getHardCodedDefault().copy(rowHeightPx = 1)
      curator.put[DecorateConfig](DecorationConfigService.Paths.getDefaultDecorationPath(), defaultDecoration)

      val actualDefault1 = DecorationConfigService.getDecoration(curator, "system", "gantry", "lane")
      actualDefault1.rowHeightPx must be(1)

      val systemDecoration = defaultDecoration.copy(rowHeightPx = 2)
      curator.put[DecorateConfig](DecorationConfigService.Paths.getSystemDecorationPath("system"), systemDecoration)

      val actualDefault2 = DecorationConfigService.getDecoration(curator, "system", "gantry", "lane")
      actualDefault2.rowHeightPx must be(2)

      val laneDecoration = systemDecoration.copy(rowHeightPx = 3)
      curator.put[DecorateConfig](DecorationConfigService.Paths.getLaneDecorationPath("system", "gantry", "lane"), laneDecoration)

      val actualDefault3 = DecorationConfigService.getDecoration(curator, "system", "gantry", "lane")
      actualDefault3.rowHeightPx must be(3)

    }
  }

  override def beforeEach() {
    super.beforeEach()
    curator = new CuratorToolsImpl(clientScope, log, finalFormats = formats) {
      override protected def extendFormats(defaultFormats: Formats) = formats
    }
  }

  override def afterAll() {
    super.afterAll()
  }

}
