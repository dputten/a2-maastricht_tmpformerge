/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.decorate

import java.io.File
import java.text.SimpleDateFormat

import csc.akkautils.DirectLogging
import csc.sectioncontrol.messages._
import csc.sectioncontrol.storage.Decorate._
import csc.util.test.FileTestUtils._
import csc.util.test.{ FileTestUtils, FuzzyImageMatcher, ObjectBuilder, Resources }
import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }

/**
 * Test the ImageDecorator class
 */
class ImageDecoratorTest
  extends WordSpec
  with MustMatchers
  with DirectLogging
  with BeforeAndAfterEach
  with BeforeAndAfterAll
  with DecorateTestData {

  import csc.sectioncontrol.decorate.ImageDecoratorTest._

  override protected def beforeAll(): Unit = {
    super.beforeAll()

    testBaseDir.delete()
    testInputDir.mkdirs()
    testOutputDir.mkdirs()
    clearDirectory(testInputDir)
    clearDirectory(testOutputDir)
  }

  //i think we have tests that are redundant/useless, as they just test combinations of inputs (which are orthogonal)
  //i only leave them here because now its cheap to create new ones
  val cases: List[DecorationCase] = List(
    DecorationCase("overview1_logo.jpg", overviewImage1),
    DecorationCase("overview1_noLogo.jpg", overviewImage1),
    DecorationCase("speed1_landscape_logo.jpg", overviewImage1, corrections = correctionSeq1),
    DecorationCase("speed1_portrait_logo.jpg", overviewImage1, corrections = correctionSeq1),
    DecorationCase("speed1_noLicense_logo.jpg", overviewImage1, corrections = correctionSeq1),
    DecorationCase("speed1_landscape_noLogo.jpg", overviewImage1),
    DecorationCase("speed1_portrait_noLogo.jpg", overviewImage1),
    DecorationCase("speed1_noLicense_noLogo.jpg", overviewImage1),
    DecorationCase("speed1_landscape_font_logo.jpg", overviewImage1),
    DecorationCase("speed1_portrait_font_logo.jpg", overviewImage1),
    DecorationCase("speed1_noLicense_font_logo.jpg", overviewImage1),
    DecorationCase("speed1_landscape_replace_frame_logo.jpg", overviewImage2, corrections = correctionSeq2),
    DecorationCase("speed1_landscape_noreplace_frame_logo.jpg", overviewImage2, corrections = correctionSeq2),
    DecorationCase("speed1_landscape_replace_noframe_logo.jpg", overviewImage2, corrections = correctionSeq2))

  "ImageDecorator" when {
    for (c ← cases) {
      c.getConditions must {
        c.getGoal in {
          c.executeAndVerify()
        }
      }
    }
  }
}

object ImageDecoratorTest extends ObjectBuilder {

  val resourceBaseDir = new File(Resources.getResourceDirPath().getAbsolutePath)
  val imageDecoratorResourceDir = new File(resourceBaseDir, "unit/imageDecorator")
  val imageDecoratorInputResourceDir = new File(imageDecoratorResourceDir, "input")
  val imageDecoratorOutputResourceDir = new File(imageDecoratorResourceDir, "output")
  val testBaseDir = new File(resourceBaseDir, "out/ImageDecoratorTest")

  val testInputDir = new File(testBaseDir, "input")
  val testOutputDir = new File(testBaseDir, "output")

  val overview1Content = List(
    "01-01-1970 01:00:00.100",
    "route gantry1 lane1",
    "NMI cert: cert XXXX",
    "Appl sign: 2451aaa5799a967ace9fced0d6edf7c132137ca198045759ae50b771a41f8fad",
    "EventId: A2-44.5MH-Lane1",
    "Lus afstand: 1502 mm",
    "Ingesteld interval: 4002 mm")

  val speed1Content = List(
    "16-07-2013 07:53:16.000",
    "Rijstrook: 100",
    "Snelheid: 133 km/h",
    "Typegoedkeuringsnummer: TP4321",
    "Lus afstand: 1502 mm",
    " ",
    "Locatie: Max 25 tekens is tot hier",
    " ",
    "Serienummer: 123Serial",
    "Ingesteld interval: 4002 mm",
    "Overtredingstype: Snelheid",
    "Rijrichting: A",
    " ",
    "Categorie: A")

  val imageCorrection1 = ImageCorrectionConfig(
    imageCorrectionId = "correction_1",
    from = "00:00:00",
    to = "23:59:59",
    colorConfig = ImageColorConfig(
      autoLevel = Some(AutoLevelConfig(
        blackStart = 1,
        blackMargin = 0.5,
        whiteStart = 254,
        whiteMargin = 2.0))))

  val imageCorrection2 = ImageCorrectionConfig(
    imageCorrectionId = "correction_2",
    from = "00:00:00",
    to = "23:59:59",
    colorConfig = ImageColorConfig(red = Some(50)))

  val correctionSeq1 = Seq(imageCorrection1)
  val correctionSeq2 = Seq(imageCorrection1, imageCorrection2)

  val rowHeightPx1 = 24
  val rowHeightPx2 = 30

  val baseDecorateConfig = DecorateConfig(
    rowHeightPx = rowHeightPx1,
    licenseQuality = 80,
    licenseMaxWidthPx = 400,
    licenseMaxHeightPx = 400,
    showLogo = false)

  val imageMeta = create[VehicleImage].copy(
    imageType = VehicleImageType.Overview,
    imageVersion = Some(ImageVersion.Original),
    timestamp = 1374504235,
    timeYellow = Some(23),
    timeRed = Some(123))

  val overviewImage1: ImageAndContents = {
    val imageContents = getFileContents("image.jpg", imageDecoratorInputResourceDir)
    ImageAndContents(Some(imageMeta), Some(imageContents))
  }

  val overviewImage2: ImageAndContents = {
    val imageContents = getFileContents("test2_overview.jpg", imageDecoratorInputResourceDir)
    ImageAndContents(Some(imageMeta), Some(imageContents))
  }

  val baseMessage = create[VehicleMetadata].copy(
    eventId = "A2-44.5MH-Lane1",
    lane = new Lane("100", "lane1", "gantry1", "route", 0, 0, bpsLaneId = Some("PW062 28.45 1 HR L R 2")),
    NMICertificate = "cert XXXX",
    applChecksum = "2451aaa5799a967ace9fced0d6edf7c132137ca198045759ae50b771a41f8fad",
    eventTimestamp = 100)

  val jpegQuality = 70

  val overview1View = new DummyDecorationView(1, false, ImageDecoratorTest.overview1Content)

  val speed1View = new DummyDecorationView(3, true, ImageDecoratorTest.speed1Content)

  val image1LandscapePosition = LicensePosition(
    TLx = 1840, TLy = 915, TRx = 1976, TRy = 919,
    BLx = 1838, BLy = 945, BRx = 1973, BRy = 950)

  val image1PortraitPosition = LicensePosition(
    TLx = 2268, TLy = 645, TRx = 2335, TRy = 634,
    BLx = 2276, BLy = 707, BRx = 2346, BRy = 694)

  val image2LandscapePosition = LicensePosition(
    TLx = 1845, TLy = 1155, TRx = 1980, TRy = 1163,
    BLx = 1843, BLy = 1181, BRx = 1977, BRy = 1191)

  val noLicense = "noLicense"
  val landscape = "landscape"
  val portrait = "portrait"
  val licenses = Seq(landscape, portrait, noLicense)

}

/**
 * Represents a decoration test case. The filename should determine most of the case conditions, for example:
 * -decoration type (tokens speed1 and overview1)
 * -license type (tokens landscape, portrait and noLicense)
 * -show logo (token _logo)
 * -bigger font (token _font)
 * -replace licensePlate (token _replace)
 * -frame around licensePlate (token _frame)
 *  The licensePosition in image is derived from the pair source image / license type
 * The test case describes itself (method getConditions) and the expected result (method getGoal)
 * The goal will contain the filename, so we can easily correlate a failure with a case entry
 *
 * @param filename filename to generate and match. Should contain tokens that trigger some case conditions
 * @param source source image (metadata and contents)
 * @param corrections corrections to be applied, default Nil
 */
case class DecorationCase(filename: String,
                          source: ImageAndContents,
                          corrections: Seq[ImageCorrectionConfig] = Nil) extends MustMatchers {

  import csc.sectioncontrol.decorate.ImageDecoratorTest._

  private val license: String = {
    val found = licenses.find(filename.contains(_))
    found match {
      case Some(value) ⇒ value
      case None        ⇒ noLicense
    }
  }

  private val licensePosition = source match {
    case img if (img == overviewImage1) ⇒ license match {
      case value if (value == landscape) ⇒ Some(image1LandscapePosition)
      case value if (value == portrait)  ⇒ Some(image1PortraitPosition)
      case _                             ⇒ None
    }
    case img if (img == overviewImage2) ⇒ license match {
      case value if (value == landscape) ⇒ Some(image2LandscapePosition)
      case _                             ⇒ None
    }
    case _ ⇒ None
  }

  private val decorationType = if (filename.contains("overview1")) DecorationType.Overview_1 else DecorationType.Speed_1
  private val logo: Boolean = filename.contains("_logo")
  private val rowHeightPx: Int = if (filename.contains("_font")) rowHeightPx2 else rowHeightPx1
  private val replace = filename.contains("_replace")
  private val frame = filename.contains("_frame")

  private val decorateConfig = {
    var config = baseDecorateConfig.copy(
      showLogo = logo,
      rowHeightPx = rowHeightPx,
      replaceLicenseWithOriginal = replace,
      showLicenseFrame = frame)

    if (replace) {
      config = config.copy(histogramMeanUpperLimit = 255) //making sure its replaced
    }

    if (frame) {
      config = config.copy(
        licenseFrameThickness = 8,
        licenseFrameColorRGB = ColorRGBConfig(60, 188, 60))
    }

    //fix correction ids in decoration configs (if any exist)
    val correctionIds = corrections.map(_.imageCorrectionId)

    if (correctionIds.size > 0) {
      val dc = new DecorationConfig(decorationType, Some(correctionIds))
      config = config.copy(decorationsConfigs = Set(dc), imageCorrections = corrections)
    }

    config
  }

  val decorator = new ImageDecorator(decorationType, decorateConfig, jpegQuality)

  val view = decorationType match {
    case DecorationType.Overview_1 ⇒ overview1View
    case DecorationType.Speed_1    ⇒ speed1View
    case _                         ⇒ null //cannot throw here, will blow up later
  }

  def executeAndVerify(): Unit = {
    val message = createMessage()
    val result = decorator.decorate(source, message, view)
    writeImageAndCompare(filename, result)
  }

  def writeImageAndCompare(filename: String, image: Option[Array[Byte]]): Unit = {

    image.isDefined must be(true)

    val actualFile = new File(testOutputDir, filename)
    val expectedFile = new File(imageDecoratorOutputResourceDir, filename)
    FileTestUtils.writeFile(actualFile, image.get)

    FuzzyImageMatcher.check(actualFile, expectedFile, 16)
  }

  def createMessage(): VehicleMetadata = {
    var result = baseMessage

    val lpd = LicensePlateData(licensePosition = licensePosition)
    result = result.copy(licensePlateData = Some(lpd))

    result
  }

  def getConditions: String = {

    val sb = new StringBuilder("configured for (")
    sb.append(decorationType.toString).append(", ")
    sb.append(license match {
      case value if (value == noLicense) ⇒ "no"
      case value                         ⇒ value
    })

    sb.append(" license, ")
    sb.append(if (logo) "with" else "no")
    sb.append(" logo")
    if (rowHeightPx != rowHeightPx1) sb.append(", font changed")

    if (corrections.size > 0) sb.append(", corrections")

    sb.append(")").toString()
  }

  def getGoal: String = "generate file %s, matching the expected".format(filename)

}
