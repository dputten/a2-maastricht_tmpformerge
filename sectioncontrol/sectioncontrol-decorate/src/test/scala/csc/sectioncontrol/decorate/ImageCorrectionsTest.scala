package csc.sectioncontrol.decorate

import java.awt.Color
import java.awt.image.BufferedImage
import java.io.File

import csc.akkautils.DirectLogging
import csc.util.test.Resources
import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }
import csc.util.test.FileTestUtils._

class ImageCorrectionsTest extends WordSpec with MustMatchers with DirectLogging
  with BeforeAndAfterEach with BeforeAndAfterAll {

  val baseResourceDir = Resources.getResourceDirPath().getAbsolutePath
  val imageTestResourceDir = new File(baseResourceDir, "unit/image-test")
  val outputDir = new File(baseResourceDir, "out/image-test")
  if (!outputDir.exists()) {
    outputDir.mkdirs()
  } else {
    clearDirectory(outputDir)
  }
  val dark_image_file = new File(imageTestResourceDir, "ref_with_quadrangle.jpg")
  val black_image_file = new File(imageTestResourceDir, "black.jpg")
  val white_image_file = new File(imageTestResourceDir, "white.jpg")
  val gradient_image_file = new File(imageTestResourceDir, "gradient.jpg")
  val polygon_mask_source_image_file = new File(imageTestResourceDir, "polygon_mask_source.jpg")

  override def afterAll(configMap: Map[String, Any]) {
    deleteDirectory(outputDir)
  }

  override def afterEach {
    clearDirectory(outputDir)
  }

  def imagePixels(image: BufferedImage)(f: (Int, Int, java.awt.Color) ⇒ Unit) {
    for (x ← 0 until image.getWidth; y ← 0 until image.getHeight) {
      f(x, y, new Color(image.getRGB(x, y)))
    }
  }

  def color(rgb: Int) = {
    if (rgb > 255) 255
    else if (rgb < 0) 0
    else rgb
  }
  "with an image it" must {
    "be able to change the RGB values" must {
      val red = 10
      val green = 2
      val blue = 3
      val originalImage = (new Image(dark_image_file.getAbsolutePath)).image
      val correctedImage = ImageCorrections.applyRGBCorrection(original = originalImage, red = red, green = green, blue = blue)

      imagePixels(originalImage) {
        case (x: Int, y: Int, rgb: Color) ⇒
          val newColor = new Color(correctedImage.getRGB(x, y))
          newColor.getRed must be(color(rgb.getRed + red))
          newColor.getGreen must be(color(rgb.getGreen + green))
          newColor.getBlue must be(color(rgb.getBlue + blue))
      }
    }
    "be able to change the gamma" must {
      val gamma = 2.0d
      val originalImage = (new Image(dark_image_file.getAbsolutePath)).image
      val correctedImage = ImageCorrections.applyGammaCorrection(original = originalImage, correction = gamma)

      imagePixels(originalImage) {
        case (x: Int, y: Int, rgb: Color) ⇒
          val newColor = new Color(correctedImage.getRGB(x, y))
          rgb.getRed must be <= newColor.getRed
          rgb.getGreen must be <= newColor.getGreen
          rgb.getBlue must be <= newColor.getBlue
      }
    }
  }

  "calculateHistogramMean(black image)" must {
    "return 0" in {
      val originalImage = (new Image(black_image_file.getAbsolutePath)).image
      val calculatedMean = ImageCorrections.calculateHistogramMean(originalImage)

      calculatedMean must be(0.0)
    }
  }

  "calculateHistogramMean(white image)" must {
    "return 255" in {
      val originalImage = (new Image(white_image_file.getAbsolutePath)).image
      val calculatedMean = ImageCorrections.calculateHistogramMean(originalImage)

      calculatedMean must be(255.0)
    }
  }

  "calculateHistogramMean(gradient image)" must {
    "return 116.2" in {
      val originalImage = (new Image(gradient_image_file.getAbsolutePath)).image
      val calculatedMean = ImageCorrections.calculateHistogramMean(originalImage)

      calculatedMean must be(116.2 plusOrMinus 0.05)
    }
  }

  "calculateHistogramMean(polygon_mask_source image)" must {
    "return 142.3" in {
      val originalImage = (new Image(polygon_mask_source_image_file.getAbsolutePath)).image
      val calculatedMean = ImageCorrections.calculateHistogramMean(originalImage)

      calculatedMean must be(142.3 plusOrMinus 0.05)
    }
  }

}
