package csc.sectioncontrol.decorate

import java.io.File

import csc.akkautils.DirectLogging
import csc.sectioncontrol.messages.{ ValueWithConfidence, LicensePlateData, LicensePosition, Lane }
import csc.sectioncontrol.sign.certificate.CertificateService
import csc.sectioncontrol.storage.Decorate.{ DecorationType, DecorateConfig }
import csc.sectioncontrol.storagelayer.SystemConfiguration
import csc.util.test.{ FuzzyImageMatcher, FileTestUtils, Resources }
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }
import org.scalatest.matchers.MustMatchers
import csc.util.test.FileTestUtils._

/**
 * Created by carlos on 06.08.15.
 */
class DecorateServiceTest
  extends WordSpec
  with MustMatchers
  with DirectLogging
  with BeforeAndAfterEach
  with BeforeAndAfterAll
  with DecorateTestData {

  import DecorateServiceTest._

  override protected def beforeAll(): Unit = {
    super.beforeAll()

    deleteDirectory(testBaseDir) //delete upfront, not after, so we can keep the generated images for failed tests
    testInputDir.mkdirs()
    testOutputDir.mkdirs()
  }

  "DecorateService" when {

    val source = createWithMetadata() //creates an overview image with metadata
    val overview: ImageAndContents = getImage(source)

    "called getDecoratedImage() given a source image with metadata" must {
      "return a decorated image, with metadata matching exactly the source" in {

        val service = new TestDecorateService
        val image = service.getDecoratedImage(msg, DecorationType.Speed_1, overview, None, 80)

        image.isDefined must be(true)

        val resultFile = new File(testOutputDir, "decoratedWithMetadata.jpg")
        FileTestUtils.writeFile(resultFile, image.get)

        val expected = new File(imageResourceDir, "expected.jpg")
        FuzzyImageMatcher.check(resultFile, expected, 20)

        val metadata = ReadWriteExifUtil.readExif(resultFile).filter(ExifTagInfo.nonBaseFilter)
        metadata.size must be(exifTagInfo.size + 1) //metadata added to the source + comment

        val remainder = metadata.toSet -- exifTagInfo //remove all matching tags
        remainder.size must be(1)

        val last = remainder.toList(0)
        last.directory must be(ExifTagInfo.directoryJpegComment)
        val matchComment = last.value match {
          case ExifValueString(str) ⇒ str == jsonMessage
          case _                    ⇒ false
        }
        matchComment must be(true)
      }
    }
  }

  def createWithMetadata(): File = {
    val source = new File(imageResourceDir, "image.jpg")
    val target = new File(testOutputDir, "imageWithMetadata.jpg")
    target.getParentFile.mkdirs()

    val data = ReadWriteExifUtil.writeExif(getFileContents(source), exifTagInfo)
    writeFile(target, data)
    target
  }

  class TestDecorateService extends DecorateService(null, systemId) {
    override protected def getSystemConfig(): SystemConfiguration = null //not used
    override protected def getDecorateConfig(lane: Lane): DecorateConfig = decorateConfig
    override protected def certificateService: CertificateService = dummyCertificateService
  }
}

object DecorateServiceTest extends DecorateTestData {

  val resourceBaseDir = new File(Resources.getResourceDirPath().getAbsolutePath)
  val imageResourceDir = new File(resourceBaseDir, "unit/decorateService")
  val imageInputResourceDir = new File(imageResourceDir, "input")
  val imageOutputResourceDir = new File(imageResourceDir, "output")
  val testBaseDir = new File(resourceBaseDir, "out/decorateService")

  val testInputDir = new File(testBaseDir, "input")
  val testOutputDir = new File(testBaseDir, "output")

  val licensePosition = LicensePosition(
    TLx = 1840, TLy = 915, TRx = 1976, TRy = 919,
    BLx = 1838, BLy = 945, BRx = 1973, BRy = 950)

  val jsonMessage =

    """{
      |  "name":"foo"
      |}""".stripMargin

  val licensePlateData = LicensePlateData(
    license = Some(ValueWithConfidence("84-SH-HF", 100)),
    licensePosition = Some(licensePosition))

  val msg = message.copy(
    jsonMessage = Some(jsonMessage),
    licensePlateData = Some(licensePlateData),
    version = Some(1)) //message with some json

  import ExifTagInfo._

  //we cannot include 'Exif Version' in the list of tags to be compared, as Array[Byte] will be read as Array[Int]
  val exifTagInfo = Seq(
    //ExifTagInfo(directoryExif_SubIFD, "Exif Version", 36864, ExifValueBytes(Array[Byte](48, 50, 50, 48))),
    ExifTagInfo(directoryExif_SubIFD, "Date/Time Original", 36867, ExifValueString("2014:08:06 08:37:38")),
    ExifTagInfo(directoryExif_SubIFD, "Sub-Sec Time", 37520, ExifValueString("935")),
    ExifTagInfo(directoryExif_SubIFD, "Sub-Sec Time Original", 37521, ExifValueString("935")),
    ExifTagInfo(directoryExif_SubIFD, "Unique Image ID", 42016, ExifValueString("MAC=00 0C DF 85 00 4C IP=192.168.2.20 UserInfo=3238-new-camera-uit-3255")),
    ExifTagInfo(directoryExif_SubIFD, "Body Serial Number", 42033, ExifValueString("SN00000077")),
    ExifTagInfo(directoryExif_IFD0, "Software", 305, ExifValueString("APP=140402A0; FPGA=140716A0; FPGAINI=140402A0; OS=130621A0")),
    ExifTagInfo(directoryExif_IFD0, "Date/Time", 306, ExifValueString("2014:08:06 08:37:38")),
    ExifTagInfo(directoryExif_IFD0, "Copyright", 33432, ExifValueString("Copyright (C) JAI Inc.")))

}
