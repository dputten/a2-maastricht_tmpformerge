package csc.sectioncontrol.decorate

import java.io.File

import csc.curator.utils.Curator
import csc.sectioncontrol.messages._
import csc.sectioncontrol.messages.certificates._
import csc.sectioncontrol.sign.certificate.CertificateService
import csc.sectioncontrol.storage.Decorate.{ ColorRGBConfig, DecorateConfig }
import csc.sectioncontrol.storage.{ Direction, ZkCorridorInfo }
import csc.sectioncontrol.storagelayer.{ CorridorConfig, RedLightConfig, SectionControlConfig, SpeedFixedConfig }
import csc.util.test.FileTestUtils._
import csc.util.test.ObjectBuilder

trait DecorateTestData extends ObjectBuilder {

  lazy val activeCertificate = ComponentCertificate("Softwarezegel", "d41d8cd98f00b204e9800998ecf8427e")
  lazy val componentCertificate = ComponentCertificate("Configuratiezegel", "ABCDEF1234567890")
  lazy val activeJarCertificate = ActiveCertificate(0, componentCertificate)

  lazy val currentCertificate = new MeasurementMethodInstallationType(0L,
    new MeasurementMethodType(typeDesignation = "",
      category = "A",
      unitSpeed = "km/h",
      unitRedLight = "s",
      unitLength = "m",
      restrictiveConditions = "",
      displayRange = "20-250 km/h",
      temperatureRange = "temperatuurbereik -10 to 50 graden Celsius",
      permissibleError = "toelatbare fout 3%",
      typeCertificate = new TypeCertificate(cert, 0, List())))

  lazy val corridorInfo = ZkCorridorInfo(
    corridorId = 1,
    locationCode = "00003",
    reportId = "",
    reportHtId = "",
    reportPerformance = true,
    locationLine1 = "LocationLine 1",
    locationLine2 = Some("LocationLine 2"))

  lazy val sectionControlConfig = create[SectionControlConfig]
  lazy val sectionControlCorridor = using(sectionControlConfig).create[CorridorConfig]

  lazy val redLightConfig = create[RedLightConfig]
  lazy val redLightCorridor = using(redLightConfig).create[CorridorConfig].copy(info = corridorInfo)

  lazy val speedConfig = create[SpeedFixedConfig]
  lazy val speedCorridor = using(speedConfig).create[CorridorConfig].copy(info = corridorInfo)

  lazy val cert = "dummy Certificate"
  lazy val eventId = "A2-44.5MH-Lane1"

  lazy val systemId = "3214"
  lazy val laneSeqConfig = LaneSeqConfig(Some(1), Some("XY"))
  lazy val laneData = IRoseSystemData(0, 1500)

  lazy val lane = Lane("3214-1-RD", "lane1", "gantry1", "route", 0, 0, Some("PW062 28.45 1 HR L R 2"))
  lazy val image = create[VehicleImage].copy(
    timestamp = 1,
    timeYellow = Some(12342L),
    timeRed = Some(333L))

  lazy val message = create[VehicleMetadata].copy(
    lane = lane,
    eventId = eventId,
    NMICertificate = cert,
    speed = Some(new ValueWithConfidence_Float(133.34f, 88)),
    serialNr = "SNP12345678",
    direction = Some(Direction.Outgoing),
    interval1Ms = Some(1000L),
    interval2Ms = Some(1234L))

  lazy val imageMeta = create[VehicleImage].copy(
    imageType = VehicleImageType.Overview,
    imageVersion = Some(ImageVersion.Original),
    timestamp = 1374504235,
    timeYellow = Some(23),
    timeRed = Some(123))

  lazy val certificateService = new CertificateService {
    override def getActiveSoftwareCertificate(curator: Curator, systemId: String, time: Long): ComponentCertificate =
      activeCertificate

    override def getCurrentCertificate(curator: Curator, systemId: String, time: Long): Option[MeasurementMethodInstallationType] =
      Some(currentCertificate)

    override def getActiveConfigurationCertificate(curator: Curator, systemId: String, time: Long): ComponentCertificate = componentCertificate
    override def saveActiveCertificate(curator: Curator, systemId: String, certificate: ActiveCertificate): Unit = {}
  }

  lazy val decorateConfig = create[DecorateConfig].copy(
    rowHeightPx = 24,
    licenseQuality = 80,
    licenseMaxWidthPx = 400,
    licenseMaxHeightPx = 400,
    showLicenseFrame = true,
    showLogo = true,
    licenseFrameThickness = 5,
    licenseFrameColorRGB = ColorRGBConfig.red,
    fontColorRGB = ColorRGBConfig.white)

  lazy val dummyCertificateService = new CertificateService {
    override def getActiveConfigurationCertificate(curator: Curator, systemId: String, time: Long): ComponentCertificate =
      componentCertificate

    override def getActiveSoftwareCertificate(curator: Curator, systemId: String, time: Long): ComponentCertificate =
      activeCertificate

    override def getCurrentCertificate(curator: Curator, systemId: String, time: Long): Option[MeasurementMethodInstallationType] =
      Some(currentCertificate)

    override def saveActiveCertificate(curator: Curator, systemId: String, certificate: ActiveCertificate): Unit = {}

  }

  def createViewContext(corridor: CorridorConfig,
                        msg: VehicleMetadata,
                        image: VehicleImage): ViewContext = {

    val ctx = new MutableContext
    ctx.systemId = systemId
    ctx.corridor = Some(corridor)
    ctx.curator = null //curator is not really needed as the certificateService is dummy
    ctx.msg = msg
    ctx.image = image
    ctx.certService = certificateService
    ctx.laneSeqConfig = laneSeqConfig
    ctx.laneData = Some(laneData)

    ctx
  }

  def getImage(file: File, meta: VehicleImage = imageMeta): ImageAndContents =
    ImageAndContents(Some(meta), Some(getFileContents(file)))

}

class DummyDecorationView(val columnCount: Int, val includeLicense: Boolean, val content: List[String]) extends DecorationView
