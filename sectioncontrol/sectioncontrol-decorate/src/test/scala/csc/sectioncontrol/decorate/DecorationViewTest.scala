package csc.sectioncontrol.decorate

import csc.akkautils.DirectLogging
import csc.sectioncontrol.storage.Decorate.DecorationType
import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }

/**
 * Created by carlos on 28.07.15.
 */
class DecorationViewTest
  extends WordSpec
  with MustMatchers
  with DirectLogging
  with BeforeAndAfterEach
  with BeforeAndAfterAll
  with DecorateTestData {

  lazy val overview1Content = List(
    "Datum en tijd van de passage: 01-01-1970 01:00:00.001",
    "Locatie van het passagepunt: PW062 28.45 1 HR L R 2",
    "NMi Type certificaat: dummy Certificate",
    "SW: d41d8cd98f00b204e9800998ecf8427e",
    "CF: ABCDEF1234567890",
    "EventId: A2-44.5MH-Lane1")

  lazy val redLight1Content = List(
    "Datum: 01-01-1970 01:00:00 CET",
    "Locatie: 3214 LocationLine 1 / LocationLine 2                                                            ",
    "Categorie: A",
    "Rijstrook: 1 XY",
    "Rijrichting: Afgaand",
    "Snelheid: 133 km/h",
    "Typegoedkeuringsnummer: dummy Certificate",
    "Serienummer: SNP12345678",
    "Fotonummer: A",
    "Intervaltijd: 0,000 s",
    "Roodtijd: 0,3 s",
    "Geeltijd: 12,3 s",
    "Lusafstand: 150 cm")

  lazy val redLight2Content = List(
    "Datum: 01-01-1970 01:00:00 CET",
    "Locatie: 3214 LocationLine 1 / LocationLine 2                                                            ",
    "Categorie: A",
    "Rijstrook: 1 XY",
    "Rijrichting: Afgaand",
    "Snelheid: 133 km/h",
    "Typegoedkeuringsnummer: dummy Certificate",
    "Serienummer: SNP12345678",
    "Fotonummer: B",
    "Intervaltijd: 1,000 s",
    "Roodtijd: 0,3 s",
    "Geeltijd: 12,3 s",
    "Lusafstand: 150 cm")

  lazy val speed1Content = List(
    "Datum: 01-01-1970 01:00:00 CET",
    "Locatie: 3214 LocationLine 1 / LocationLine 2                                                            ",
    "Categorie: A",
    "Rijstrook: 1 XY",
    "Rijrichting: Afgaand",
    "Snelheid: 133 km/h",
    "Typegoedkeuringsnummer: dummy Certificate",
    "Serienummer: SNP12345678",
    "Fotonummer: A",
    "Intervaltijd: 0,000 s",
    "Lusafstand: 150 cm")

  lazy val speed2Content = List(
    "Datum: 01-01-1970 01:00:00 CET",
    "Locatie: 3214 LocationLine 1 / LocationLine 2                                                            ",
    "Categorie: A",
    "Rijstrook: 1 XY",
    "Rijrichting: Afgaand",
    "Snelheid: 133 km/h",
    "Typegoedkeuringsnummer: dummy Certificate",
    "Serienummer: SNP12345678",
    "Fotonummer: B",
    "Intervaltijd: 1,234 s",
    "Lusafstand: 150 cm")

  "Overview1View" when {

    "given the proper ViewContext" must {

      "return the correct decoration lines" in {
        val ctx = createViewContext(sectionControlCorridor, message, image)
        val view = DecorationView.getView(DecorationType.Overview_1, ctx)

        view.columnCount must be(1)
        view.includeLicense must be(false)
        view.content must be(overview1Content)
      }
    }
  }

  "RedLight1View" when {

    "given the proper ViewContext" must {

      "return the correct decoration lines" in {
        val ctx = createViewContext(redLightCorridor, message, image)
        val view = DecorationView.getView(DecorationType.RedLight_1, ctx)

        view.columnCount must be(3)
        view.includeLicense must be(false)
        view.content must be(redLight1Content)
      }
    }
  }

  "RedLight2View" when {

    "given the proper ViewContext" must {

      "return the correct decoration lines" in {
        val ctx = createViewContext(redLightCorridor, message, image)
        val view = DecorationView.getView(DecorationType.RedLight_2, ctx)

        view.columnCount must be(3)
        view.includeLicense must be(true)

        view.content must be(redLight2Content)
      }
    }
  }

  "Speed1View" when {

    "given the proper ViewContext" must {

      "return the correct decoration lines" in {
        val ctx = createViewContext(speedCorridor, message, image)
        val view = DecorationView.getView(DecorationType.Speed_1, ctx)

        view.columnCount must be(3)
        view.includeLicense must be(true)
        view.content must be(speed1Content)
      }
    }
  }

  "Speed2View" when {

    "given the proper ViewContext" must {

      "return the correct decoration lines" in {
        val ctx = createViewContext(speedCorridor, message, image)
        val view = DecorationView.getView(DecorationType.Speed_2, ctx)

        view.columnCount must be(3)
        view.includeLicense must be(false)
        view.content must be(speed2Content)
      }
    }
  }

}
