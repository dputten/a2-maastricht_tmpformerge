/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.decorate

import java.awt.{ Color, Point }
import java.io.File

import csc.akkautils.DirectLogging
import csc.sectioncontrol.decorate.Image.Corner
import csc.util.test.{ FuzzyImageMatcher, Resources }
import org.apache.commons.io.FileUtils
import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }

/**
 * Test the Image class.
 */
class ImageTest extends WordSpec with MustMatchers with DirectLogging
  with BeforeAndAfterEach with BeforeAndAfterAll {

  import csc.util.test.FileTestUtils._

  val baseResourceDir = Resources.getResourceDirPath().getAbsolutePath
  val imageTestResourceDir = new File(baseResourceDir, "unit/image-test")
  val outputDir = new File(baseResourceDir, "out/image-test")
  if (!outputDir.exists()) {
    outputDir.mkdirs()
  } else {
    clearDirectory(outputDir)
  }

  val large_image_file = new File(imageTestResourceDir, "large.jpg")
  val small_image_file = new File(imageTestResourceDir, "small.jpg")
  val mask_source_image_file = new File(imageTestResourceDir, "polygon_mask_source.jpg")

  override def afterAll(configMap: Map[String, Any]) {
    deleteDirectory(outputDir)
  }

  override def afterEach {
    clearDirectory(outputDir)
  }

  "A large Image" when {
    "a small Image is added to the right" must {
      "return an image of size [width = large.width + small.width, height = large.height]" in {
        val largeImage = new Image(large_image_file.getAbsolutePath)
        val smallImage = new Image(small_image_file.getAbsolutePath)

        val testImage = largeImage.addRight(smallImage)
        val testImageUrl = outputDir + "/test.jpg"
        testImage.writeImage(testImageUrl)
        val testImageFile = new File(testImageUrl)
        val refImageFile = new File(imageTestResourceDir, "ref_result_large_R_small.jpg")
        imagesMustBeAlike(testImageFile, refImageFile)
      }
    }
    "a small Image is added to the bottom" must {
      "return an image of size [width = large.width, height = large.height + small.height]" in {
        val largeImage = new Image(large_image_file.getAbsolutePath)
        val smallImage = new Image(small_image_file.getAbsolutePath)

        val testImage = largeImage.addBottom(smallImage)
        val testImageUrl = outputDir + "/test.jpg"
        testImage.writeImage(testImageUrl)
        val testImageFile = new File(testImageUrl)
        val refImageFile = new File(imageTestResourceDir, "ref_result_large_B_small.jpg")
        imagesMustBeAlike(testImageFile, refImageFile)
      }
    }
  }

  "A small Image" when {
    "a large Image is added to the right" must {
      "return an image of size [width = large.width + small.width, height = large.height]" in {
        val largeImage = new Image(large_image_file.getAbsolutePath)
        val smallImage = new Image(small_image_file.getAbsolutePath)

        val testImage = smallImage.addRight(largeImage)
        val testImageUrl = outputDir + "/test.jpg"
        testImage.writeImage(testImageUrl)
        val testImageFile = new File(testImageUrl)
        val refImageFile = new File(imageTestResourceDir, "ref_result_small_R_large.jpg")
        imagesMustBeAlike(testImageFile, refImageFile)
      }
    }
    "a large Image is added to the bottom" must {
      "return an image of size [width = large.width, height = large.height + small.height]" in {
        val largeImage = new Image(large_image_file.getAbsolutePath)
        val smallImage = new Image(small_image_file.getAbsolutePath)

        val testImage = smallImage.addBottom(largeImage)
        val testImageUrl = outputDir + "/test.jpg"
        testImage.writeImage(testImageUrl)
        val testImageFile = new File(testImageUrl)
        val refImageFile = new File(imageTestResourceDir, "ref_result_small_B_large.jpg")
        imagesMustBeAlike(testImageFile, refImageFile)
      }
    }
  }

  val maskVertices = List(
    new Point(200, 600),
    new Point(300, 250),
    new Point(700, 150),
    new Point(1000, 400),
    new Point(750, 600),
    new Point(900, 800),
    new Point(500, 900))

  "An Image" when {
    "a polygon mask is applied (vertices clockwise), without cropping" must {
      "return an image of the same size where the area outside the polygon is black" in {
        val sourceImage = new Image(mask_source_image_file.getAbsolutePath)

        val testImage = sourceImage.applyPolygonMask(maskVertices)
        val testImageUrl = outputDir + "/test.jpg"
        testImage.writeImage(testImageUrl)
        val testImageFile = new File(testImageUrl)
        val refImageFile = new File(imageTestResourceDir, "ref_polygon_mask_without_crop.jpg")
        imagesMustBeAlike(testImageFile, refImageFile)
      }
    }

    "a polygon mask is applied (vertices counterclockwise), without cropping" must {
      "return an image of the same size where the area outside the polygon is black" in {
        val sourceImage = new Image(mask_source_image_file.getAbsolutePath)

        val testImage = sourceImage.applyPolygonMask(maskVertices.reverse)
        val testImageUrl = outputDir + "/test.jpg"
        testImage.writeImage(testImageUrl)
        val testImageFile = new File(testImageUrl)
        val refImageFile = new File(imageTestResourceDir, "ref_polygon_mask_without_crop.jpg")
        imagesMustBeAlike(testImageFile, refImageFile)
      }
    }

    "a polygon mask is applied (vertices clockwise), with cropping" must {
      "return an image just fitting the polygon, where the area outside the polygon is black" in {
        val sourceImage = new Image(mask_source_image_file.getAbsolutePath)

        val testImage = sourceImage.applyPolygonMask(maskVertices, true)
        val testImageUrl = outputDir + "/test.jpg"
        testImage.writeImage(testImageUrl)
        val testImageFile = new File(testImageUrl)
        val refImageFile = new File(imageTestResourceDir, "ref_polygon_mask_with_crop.jpg")
        imagesMustBeAlike(testImageFile, refImageFile)
      }
    }

    "a polygon mask is applied (vertices counterclockwise), with cropping" must {
      "return an image just fitting the polygon, where the area outside the polygon is black" in {
        val sourceImage = new Image(mask_source_image_file.getAbsolutePath)

        val testImage = sourceImage.applyPolygonMask(maskVertices.reverse, true)
        val testImageUrl = outputDir + "/test.jpg"
        testImage.writeImage(testImageUrl)
        val testImageFile = new File(testImageUrl)
        val refImageFile = new File(imageTestResourceDir, "ref_polygon_mask_with_crop.jpg")
        imagesMustBeAlike(testImageFile, refImageFile)
      }
    }

    "a polygon mask is applied, passing only 2 vertices" must {
      "throw an excception" in {
        val sourceImage = new Image(mask_source_image_file.getAbsolutePath)

        val exception = intercept[Exception] {
          sourceImage.applyPolygonMask(List(new Point(100, 100), new Point(200, 200)), true)
        }
        exception.getMessage must be("requirement failed: Polygon mask must have at least 3 vertices")
      }
    }

    "cropped - passing topLeft en bottomRight corners" must {
      "return an image containing the area between topLeft and bottomRight" in {
        val sourceImage = new Image(mask_source_image_file.getAbsolutePath)

        val testImage = sourceImage.crop(new Point(400, 400), new Point(700, 700))
        val testImageUrl = outputDir + "/test.jpg"
        testImage.writeImage(testImageUrl)
        val testImageFile = new File(testImageUrl)
        val refImageFile = new File(imageTestResourceDir, "ref_crop.jpg")
        imagesMustBeAlike(testImageFile, refImageFile)
      }
    }

    "cropped - where topLeft is to the right of bottomRight" must {
      "throw an exception" in {
        val sourceImage = new Image(mask_source_image_file.getAbsolutePath)

        val exception = intercept[Exception] {
          sourceImage.crop(new Point(200, 400), new Point(100, 700))
        }
        exception.getMessage must be("requirement failed: Crop not possible: topLeft is not to the left of bottomRight")
      }
    }

    "cropped - where topLeft is below bottomRight" must {
      "throw an exception" in {
        val sourceImage = new Image(mask_source_image_file.getAbsolutePath)

        val exception = intercept[Exception] {
          sourceImage.crop(new Point(100, 400), new Point(200, 200))
        }
        exception.getMessage must be("requirement failed: Crop not possible: topLeft is not above bottomRight")
      }
    }

    "overlayed with a smaller image (fitting completely)" must {
      "return an image containing the smaller image at the given location" in {
        val sourceImage = new Image(large_image_file.getAbsolutePath)
        val overlayImage = new Image(small_image_file.getAbsolutePath)

        val testImage = sourceImage.overlay(overlayImage, new Point(70, 50))
        val testImageUrl = outputDir + "/test.jpg"
        testImage.writeImage(testImageUrl)
        val testImageFile = new File(testImageUrl)
        val refImageFile = new File(imageTestResourceDir, "ref_large_overlayed_with_small_fitting.jpg")
        imagesMustBeAlike(testImageFile, refImageFile)
      }
    }

    "overlayed with a smaller image (sticking out right)" must {
      "return an image containing the smaller image at the given location" in {
        val sourceImage = new Image(large_image_file.getAbsolutePath)
        val overlayImage = new Image(small_image_file.getAbsolutePath)

        val testImage = sourceImage.overlay(overlayImage, new Point(380, 70))
        val testImageUrl = outputDir + "/test.jpg"
        testImage.writeImage(testImageUrl)
        val testImageFile = new File(testImageUrl)
        val refImageFile = new File(imageTestResourceDir, "ref_large_overlayed_with_small_sticking_out_right.jpg")
        imagesMustBeAlike(testImageFile, refImageFile)
      }
    }

    "overlayed with a smaller image (sticking out left)" must {
      "return an image containing the smaller image at the given location" in {
        val sourceImage = new Image(large_image_file.getAbsolutePath)
        val overlayImage = new Image(small_image_file.getAbsolutePath)

        val testImage = sourceImage.overlay(overlayImage, new Point(-100, 70))
        val testImageUrl = outputDir + "/test.jpg"
        testImage.writeImage(testImageUrl)
        val testImageFile = new File(testImageUrl)
        val refImageFile = new File(imageTestResourceDir, "ref_large_overlayed_with_small_sticking_out_left.jpg")
        imagesMustBeAlike(testImageFile, refImageFile)
      }
    }

    "overlayed with a smaller image (top left corner)" must {
      "return an image containing the smaller image in the top left corner" in {
        val sourceImage = new Image(large_image_file.getAbsolutePath)
        val overlayImage = new Image(small_image_file.getAbsolutePath)

        val testImage = sourceImage.overlay(overlayImage, Corner.TopLeft)
        val testImageUrl = outputDir + "/test.jpg"
        testImage.writeImage(testImageUrl)
        val testImageFile = new File(testImageUrl)
        val refImageFile = new File(imageTestResourceDir, "ref_large_overlayed_top_left_corner.jpg")
        imagesMustBeAlike(testImageFile, refImageFile)
      }
    }

    "overlayed with a smaller image (top right corner)" must {
      "return an image containing the smaller image in the top right corner" in {
        val sourceImage = new Image(large_image_file.getAbsolutePath)
        val overlayImage = new Image(small_image_file.getAbsolutePath)

        val testImage = sourceImage.overlay(overlayImage, Corner.TopRight)
        val testImageUrl = outputDir + "/test.jpg"
        testImage.writeImage(testImageUrl)
        val testImageFile = new File(testImageUrl)
        val refImageFile = new File(imageTestResourceDir, "ref_large_overlayed_top_right_corner.jpg")
        imagesMustBeAlike(testImageFile, refImageFile)
      }
    }

    "overlayed with a smaller image (bottom left corner)" must {
      "return an image containing the smaller image in the bottom left corner" in {
        val sourceImage = new Image(large_image_file.getAbsolutePath)
        val overlayImage = new Image(small_image_file.getAbsolutePath)

        val testImage = sourceImage.overlay(overlayImage, Corner.BottomLeft)
        val testImageUrl = outputDir + "/test.jpg"
        testImage.writeImage(testImageUrl)
        val testImageFile = new File(testImageUrl)
        val refImageFile = new File(imageTestResourceDir, "ref_large_overlayed_bottom_left_corner.jpg")
        imagesMustBeAlike(testImageFile, refImageFile)
      }
    }

    "overlayed with a smaller image (bottom right corner)" must {
      "return an image containing the smaller image in the bottom right corner" in {
        val sourceImage = new Image(large_image_file.getAbsolutePath)
        val overlayImage = new Image(small_image_file.getAbsolutePath)

        val testImage = sourceImage.overlay(overlayImage, Corner.BottomRight)
        val testImageUrl = outputDir + "/test.jpg"
        testImage.writeImage(testImageUrl)
        val testImageFile = new File(testImageUrl)
        val refImageFile = new File(imageTestResourceDir, "ref_large_overlayed_bottom_right_corner.jpg")
        imagesMustBeAlike(testImageFile, refImageFile)
      }
    }

    "overlayed with a smaller image with a clipping polygon (inside the small image)" must {
      "return an image containing the clipped smaller image at the given location" in {
        val sourceImage = new Image(large_image_file.getAbsolutePath)
        val overlayImage = new Image(small_image_file.getAbsolutePath)

        val clippingPolygonVertices = Seq(new Point(50, 10), new Point(150, 90), new Point(70, 110), new Point(10, 50))

        val testImage = sourceImage.overlayWithClippingPolygon(overlayImage, new Point(5, 5), clippingPolygonVertices = clippingPolygonVertices)
        val testImageUrl = outputDir + "/test.jpg"
        testImage.writeImage(testImageUrl)
        val testImageFile = new File(testImageUrl)
        val refImageFile = new File(imageTestResourceDir, "ref_large_overlayed_with_small_and_clipping_1.jpg")
        imagesMustBeAlike(testImageFile, refImageFile)
      }
    }

    "overlayed with a smaller image with a clipping polygon (partially inside the small image)" must {
      "return an image containing the partially clipped smaller image at the given location" in {
        val sourceImage = new Image(large_image_file.getAbsolutePath)
        val overlayImage = new Image(small_image_file.getAbsolutePath)

        val clippingPolygonVertices = Seq(new Point(50, 50), new Point(150, 130), new Point(70, 150), new Point(10, 90))

        val testImage = sourceImage.overlayWithClippingPolygon(overlayImage, new Point(5, 5), clippingPolygonVertices = clippingPolygonVertices)
        val testImageUrl = outputDir + "/test.jpg"
        testImage.writeImage(testImageUrl)
        val testImageFile = new File(testImageUrl)
        val refImageFile = new File(imageTestResourceDir, "ref_large_overlayed_with_small_and_clipping_2.jpg")
        imagesMustBeAlike(testImageFile, refImageFile)
      }
    }

    "drawOuterRectangle is called (rectangle inside image)" must {
      "return an image containing the rectangle with given color and thickness" in {
        val sourceImage = new Image(large_image_file.getAbsolutePath)

        val testImage = sourceImage.drawOuterRectangle(TL = new Point(50, 50), BR = new Point(150, 150), color = new Color(123, 45, 223), thickness = 5)
        val testImageUrl = outputDir + "/test.jpg"
        testImage.writeImage(testImageUrl)
        val testImageFile = new File(testImageUrl)
        val refImageFile = new File(imageTestResourceDir, "ref_with_rectangle.jpg")
        imagesMustBeAlike(testImageFile, refImageFile)
      }
    }

    "drawOuterRectangle is called (rectangle partially inside image)" must {
      "return an image containing the rectangle with given color and thickness" in {
        val sourceImage = new Image(large_image_file.getAbsolutePath)

        val testImage = sourceImage.drawOuterRectangle(TL = new Point(50, 50), BR = new Point(350, 350)) // Default color and thickness
        val testImageUrl = outputDir + "/test.jpg"
        testImage.writeImage(testImageUrl)
        val testImageFile = new File(testImageUrl)
        val refImageFile = new File(imageTestResourceDir, "ref_with_rectangle_part.jpg")
        imagesMustBeAlike(testImageFile, refImageFile)
      }
    }

    "drawOuterQuadrangle is called (quadrangle inside image)" must {
      "return an image containing the quadrangle with given color and thickness" in {
        val sourceImage = new Image(large_image_file.getAbsolutePath)

        val testImage = sourceImage.drawOuterQuadrangle(TL = new Point(50, 50), TR = new Point(350, 170), BR = new Point(200, 260), BL = new Point(40, 110), color = new Color(123, 45, 223), thickness = 5)
        val testImageUrl = outputDir + "/test.jpg"
        testImage.writeImage(testImageUrl)
        val testImageFile = new File(testImageUrl)
        val refImageFile = new File(imageTestResourceDir, "ref_with_quadrangle.jpg")
        imagesMustBeAlike(testImageFile, refImageFile)
      }
    }

    "drawOuterQuadrangle is called (quadrangle partially inside image)" must {
      "return an image containing the quadrangle with given color and thickness" in {
        val sourceImage = new Image(large_image_file.getAbsolutePath)

        val testImage = sourceImage.drawOuterQuadrangle(TL = new Point(50, 50), TR = new Point(450, 170), BR = new Point(200, 460), BL = new Point(40, 110)) // Default color and thickness
        val testImageUrl = outputDir + "/test.jpg"
        testImage.writeImage(testImageUrl)
        val testImageFile = new File(testImageUrl)
        val refImageFile = new File(imageTestResourceDir, "ref_with_quadrangle_part.jpg")
        imagesMustBeAlike(testImageFile, refImageFile)
      }
    }

  }

  def imagesMustBeTheSame(testImageFile: File, refImageFile: File) = {
    val testFile = FileUtils.readFileToByteArray(testImageFile)
    val refFile = FileUtils.readFileToByteArray(refImageFile)

    testFile must be(refFile)
  }

  def imagesMustBeAlike(testImageFile: File, refImageFile: File, threshold: Int = 20) = {
    val rmsd = rmsdImages(testImageFile, refImageFile)

    FuzzyImageMatcher.check(testImageFile, refImageFile, threshold)
  }
}
