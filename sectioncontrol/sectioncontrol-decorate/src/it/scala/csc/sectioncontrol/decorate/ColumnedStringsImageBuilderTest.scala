/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.decorate

import java.awt.image.BufferedImage
import java.awt.{Color, Font}
import java.io.File
import javax.imageio.{IIOImage, ImageIO, ImageWriteParam}

import csc.akkautils.DirectLogging
import csc.util.test.FileTestUtils._
import csc.util.test.{FuzzyImageMatcher, Resources}
import org.scalatest.matchers.MustMatchers
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, WordSpec}

import scala.collection.JavaConverters._

/**
 * Test the ColumnedStringsImageBuilder
 */
class ColumnedStringsImageBuilderTest extends WordSpec with MustMatchers with DirectLogging
  with BeforeAndAfterEach with BeforeAndAfterAll {

  var imageCounter = 0
  val baseResourceDir = Resources.getResourceDirPath().getAbsolutePath
  val decorateResourceDir = new File(baseResourceDir, "unit/decorate")
  val inputDir = new File(baseResourceDir, "out/ColumnedStringsImageBuilderTest")

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    deleteDirectory(inputDir) //delete upfront, not after, so we can keep the generated images for failed tests
    inputDir.mkdirs()
    imageCounter = 0
  }

  private def newTestImage(): String = {
    val value = imageCounter + 1
    imageCounter = value
    inputDir.getAbsolutePath + "/test-image" + value + ".jpg"
  }

  private def testImage(builder: ColumnedStringsImageBuilder, content: List[String], expectedImage: String): Unit = {
    val url = newTestImage()
    val testImage = builder.createImage(content = content, width = 200, numberOfColumns = 4)
    writeBufferedImage(testImage, url)
    val testImageFile = new File(url)
    val refImageFile = new File(decorateResourceDir, expectedImage)
    imagesMustBeAlike(testImageFile, refImageFile)
    testImageFile.delete() //deletes only if successful
  }

  "The ColumnedStringsImageBuilder" when {
    "passed 4 1-column strings and [width = 200, numberOfColumns = 4]" must {
      val builder = new ColumnedStringsImageBuilder(imageType = BufferedImage.TYPE_INT_RGB)
      "create an image with one row" in {
        val content = List("One", "Two", "Three", "Four")
        testImage(builder, content, "ref_image_4x1_column_strings.jpg")
      }
    }
    "passed 3 1-column and 1 2-column strings and [width = 200, numberOfColumns = 4]" must {
      val builder = new ColumnedStringsImageBuilder(imageType = BufferedImage.TYPE_INT_RGB)
      "create an image with 2 rows" in {
        val content = List("One", "Two longer", "Three", "Four")
        testImage(builder, content, "ref_image_3x1_1x2_column_strings.jpg")
      }
    }
    "passed 4 2-column strings and [width = 200, numberOfColumns = 4]" must {
      val builder = new ColumnedStringsImageBuilder(imageType = BufferedImage.TYPE_INT_RGB)
      "create an image with 2 rows" in {
        val content = List("One longer", "Two longer", "Three longer", "Four longer")
        testImage(builder, content, "ref_image_4x2_column_strings.jpg")
      }
    }
    "passed 4 4-column strings and [width = 200, numberOfColumns = 4]" must {
      val builder = new ColumnedStringsImageBuilder(imageType = BufferedImage.TYPE_INT_RGB)
      "create an image with 2 rows" in {
        val content = List("This is a four column string", "This is a four column string",
          "This is a four column string", "This is a four column string")
        testImage(builder, content, "ref_image_4x4_column_strings.jpg")
      }
    }
    "passed 1 5-column strings and [width = 200, numberOfColumns = 4]" must {
      val builder = new ColumnedStringsImageBuilder(imageType = BufferedImage.TYPE_INT_RGB)
      "create an image with 1 row, which contains only part of the string" in {
        val content = List("This is a very nice five column string")
        testImage(builder, content, "ref_image_1x5_column_string.jpg")
      }
    }
    "passed 4 empty strings and [width = 200, numberOfColumns = 4]" must {
      val builder = new ColumnedStringsImageBuilder(imageType = BufferedImage.TYPE_INT_RGB)
      "create an image with 1 empty row" in {
        val content = List("", "", "", "")
        testImage(builder, content, "ref_image_4x0_strings.jpg")
      }
    }
    "passed 5 empty strings and [width = 200, numberOfColumns = 4]" must {
      val builder = new ColumnedStringsImageBuilder(imageType = BufferedImage.TYPE_INT_RGB)
      "create an image with 2 empty rows" in {
        val content = List("", "", "", "", "")
        testImage(builder, content, "ref_image_5x0_strings.jpg")
      }
    }
  }

  "The ColumnedStringsImageBuilder, configured [rowHeightPx = 20]" when {
    "passed 5 strings and [width = 200, numberOfColumns = 4]" must {
      val builder = new ColumnedStringsImageBuilder(imageType = BufferedImage.TYPE_INT_RGB, rowHeightPx = 20)
      "create an image with a larger font" in {
        val content = List("One", "TwoLong", "Three", "Four", "Five")
        testImage(builder, content, "ref_image_5_strings_row20.jpg")
      }
    }
  }

  "The ColumnedStringsImageBuilder, configured [backgroundColor = Blue, fontColor = Cyan]" when {
    "passed 5 strings and [width = 200, numberOfColumns = 4]" must {
      val builder = new ColumnedStringsImageBuilder(imageType = BufferedImage.TYPE_INT_RGB, backgroundColor = Color.BLUE, fontColor = Color.CYAN)
      "create an image using given colors" in {
        val content = List("One", "TwoLong", "Three", "Four", "Five")
        testImage(builder, content, "ref_image_5_strings_bluecyan.jpg")
      }
    }
  }

  "The ColumnedStringsImageBuilder, configured [borderWidthPx = 30, descentPx = 3, extraLineSpacingPx = 15]" when {
    "passed 5 strings and [width = 200, numberOfColumns = 4]" must {
      val builder = new ColumnedStringsImageBuilder(borderWidthPx = 30, descentPx = 3, extraLineSpacingPx = 15)
      "create an image using given settings" in {
        val content = List("One", "TwoLong", "Three", "Four", "Five")
        testImage(builder, content, "ref_image_5_strings_settings.jpg")
      }
    }
  }

  "The ColumnedStringsImageBuilder, configured [font = (Serif-Bold-14)]" when {
    "passed 4 1-column strings and [width = 200, numberOfColumns = 4]" must {
      val SerifBold14Font = new Font(Font.SERIF, Font.BOLD, 14)
      val builder = new ColumnedStringsImageBuilder(imageType = BufferedImage.TYPE_INT_RGB, font = Some(SerifBold14Font))
      "create an image with one row" in {
        val content = List("One", "Two", "Three", "Four")
        testImage(builder, content, "ref_image_4x1_column_strings_serif.jpg")
      }
    }
  }

  "The ColumnedStringsImageBuilder, configured [ue]" when {
    "passed 4 1-column strings and [width = 200, numberOfColumns = 4]" must {
      val SerifBold14Font = new Font(Font.SERIF, Font.BOLD, 14)
      val builder = new ColumnedStringsImageBuilder(imageType = BufferedImage.TYPE_INT_RGB, font = Some(SerifBold14Font))
      "create an image with one row" in {
        val content = List("     ", "Two        ", "Three                                                                                                   ", "Four          ")
        testImage(builder, content, "ref_image_spaces.jpg")
      }
    }
  }

  def imagesMustBeAlike(testImageFile: File, refImageFile: File, threshold: Int = 65) =
    FuzzyImageMatcher.check(testImageFile, refImageFile, threshold)

  def writeBufferedImage(image: BufferedImage, url: String) = {
    val imageType = "jpg"
    val writer = {
      val writerList = ImageIO.getImageWritersByFormatName(imageType).asScala
      if (writerList.isEmpty) {
        throw new IllegalArgumentException("Couldn't find writer for imageType %s".format(imageType))
      } else {
        writerList.next()
      }
    }

    // Prepare output file
    val ios = ImageIO.createImageOutputStream(new File(url));
    try {
      writer.setOutput(ios);

      // Set the compression quality
      val iwparam = writer.getDefaultWriteParam()
      iwparam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
      iwparam.setCompressionQuality(100.toFloat / 100);
      // Write the image
      writer.write(null, new IIOImage(image, null, null), iwparam);
    } finally {
      // Cleanup
      try {
        ios.flush();
      } finally {
        try {
          writer.dispose();
        } finally {
          ios.close();
        }
      }
    }
  }
}
