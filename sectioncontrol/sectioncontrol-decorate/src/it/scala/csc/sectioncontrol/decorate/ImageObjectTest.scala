/**
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.decorate

import java.awt.Font
import java.awt.image.BufferedImage
import java.io.File

import csc.akkautils.DirectLogging
import csc.util.test.{FuzzyImageMatcher, Resources}
import org.scalatest.matchers.MustMatchers
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, WordSpec}

/**
 * Test the Image object.
 */
class ImageObjectTest extends WordSpec with MustMatchers with DirectLogging
  with BeforeAndAfterEach with BeforeAndAfterAll {

  import csc.util.test.FileTestUtils._

  val baseResourceDir = Resources.getResourceDirPath().getAbsolutePath
  val imageTestResourceDir = new File(baseResourceDir, "unit/image-test")
  val outputDir = new File(baseResourceDir, "out/ImageObjectTest")

  override protected def beforeAll(): Unit = {
    super.beforeAll()

    deleteDirectory(outputDir)
    if (!outputDir.exists()) {
      outputDir.mkdirs()
    } else {
      clearDirectory(outputDir)
    }
  }

  "The Image.createColumnedStringsImage method" when {
    "called with (content, width, numberOfColumns, imageType, font)" must {
      "create a corresponding Image object" in {
        val font = new Font(Font.SERIF, Font.PLAIN, 16)
        val content = List("This", "is", "a test", "of", "the", "createColumnedStringsImage", "method", "of", "the", "Image", "object")
        val testImage = Image.createColumnedStringsImage(
          content = content,
          width = 400,
          numberOfColumns = 10,
          imageType = BufferedImage.TYPE_INT_RGB,
          rowHeightPx = 15,
          font = Some(font))
        val test_image_url = outputDir + "/test.jpg"
        testImage.writeImage(test_image_url)
        val testImageFile = new File(test_image_url)
        val refImageFile = new File(imageTestResourceDir, "ref_columned_strings.jpg")

        FuzzyImageMatcher.check(testImageFile, refImageFile, 15)
      }
    }
  }

}