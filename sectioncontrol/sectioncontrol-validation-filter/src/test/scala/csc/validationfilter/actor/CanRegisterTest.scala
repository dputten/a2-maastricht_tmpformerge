package csc.validationfilter.actor

import akka.actor.{ ActorRef, Props, Actor, ActorSystem }
import akka.testkit.{ ImplicitSender, TestKit }
import akka.util.duration._
import csc.curator.CuratorTestServer
import csc.curator.utils.Curator
import csc.validationfilter.actor.messages.{ SystemsMessage, ViolationCandidateMessage, ValidateAbleVehicleMessage }
import csc.validationfilter.hbasestub.HBasePersistentStub
import csc.validationfilter.{ CuratorFill, CuratorHelper, Dictionary }
import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }

class CanRegisterTest extends TestKit(ActorSystem("validationSystem")) with WordSpec with MustMatchers with BeforeAndAfterAll with BeforeAndAfterEach with CuratorTestServer with ImplicitSender {

  case class TestSystems(sytems: List[String])

  object TestActorProvider extends SystemAwareActorProvider {
    def props(system: String) = Props(new TestActor)
  }

  class TestActor extends Actor with CanRegister {

    val provider: SystemAwareActorProvider = TestActorProvider

    var originalSender: ActorRef = _

    def receive = super.process orElse {
      case TestSystems(systems) ⇒
        originalSender = sender

        self ! SystemsMessage(systems)

      case ProcessingSystemsDone ⇒
        originalSender ! registeredActors.size
    }
  }

  override protected def afterAll(): Unit = {
    system.shutdown()
  }

  "TestActor that has CanRegister trait" must {
    "correctly register new actor for each system" in {
      val testActor = system.actorOf(TestActorProvider.props("don't care"))
      testActor ! TestSystems(List("system 1", "system 2"))
      expectMsg(2)

      //some second fetch with same systems, shouldn't change anything
      testActor ! TestSystems(List("system 1", "system 2"))
      expectMsg(2)

      // added one extra
      testActor ! TestSystems(List("system 1", "system 2", "system 3"))
      expectMsg(3)

      //removed system 2
      testActor ! TestSystems(List("system 1", "system 3"))
      expectMsg(2)
    }
  }
}