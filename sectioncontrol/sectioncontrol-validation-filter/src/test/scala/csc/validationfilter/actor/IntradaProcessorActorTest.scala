package csc.validationfilter.actor

import akka.actor.{ ActorRef, ActorSelection, ActorSystem, Props }
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import csc.checkimage.{ ImageCheckResult, IntradaCheckResult, RecognitionResult }
import csc.curator.utils.Curator
import csc.sectioncontrol.messages.{ VehicleClassifiedRecord, VehicleViolationRecord, _ }
import csc.validationfilter.Dictionary
import csc.validationfilter.actor.messages._
import csc.validationfilter.configuration.EnforceNLConfig
import csc.validationfilter.domain.ViolationVerificationResult
import org.scalatest.WordSpec
import org.scalatest.matchers.ShouldMatchers

/**
 * Created by rbakker on 20-4-17.
 */
class IntradaProcessorActorTest extends TestKit(ActorSystem("IntradaProcessorActorTest")) with WordSpec with ShouldMatchers {
  val systemId = "A2"
  val lic = "ABCD12"
  val conf = 60
  val rawLic = "AB_CD_12"
  val imageResponse = ViolationAndIntradaResult(
    record = createViolationCandidateRecord(lic, conf, rawLic),
    intradaCheckResult = createCheckResult(lic, conf, rawLic))

  val enforceCfg = ConfigAndCuratorMessage(system = systemId, curator = null, config = EnforceNLConfig(ntpServer = "local",
    timeZone = "Europe/Amsterdam",
    verifyRecognition = "Overview",
    recognizerCountries = Some(Seq("NL", "DE", "FR", "BE"))))

  "IntradaProcessorActor" must {
    "create a correct response" in {
      val controler = TestProbe()
      val zookeeper = TestProbe()

      val actor = TestActorRef(Props(new IntradaProcessorActor(validationStepsController = controler.ref, input = imageResponse) {
        override def configActor = system.actorSelection("/system/" + zookeeper.ref.path.name)

      }))
      zookeeper.expectMsg(GetEnforceConfig(systemId))
      zookeeper.send(actor, enforceCfg)
      val expectedMsg = ImageProcessorResultMessage(ViolationVerificationResult.OK,
        LicensePlateData(license = Some(ValueWithConfidence[String](lic, conf)),
          sourceImageType = Some(VehicleImageType.Overview),
          rawLicense = None,
          recognizedBy = Some("SUMMARY"),
          country = Some(ValueWithConfidence[String]("NL", 50))),
        Some(LicensePlateData(license = Some(ValueWithConfidence[String](lic, conf)),
          sourceImageType = Some(VehicleImageType.Overview),
          rawLicense = Some(rawLic),
          recognizedBy = Some(VehicleMetadata.Recognizer.INTRADA),
          country = Some(ValueWithConfidence[String]("NL", 50)))))
      controler.expectMsg(expectedMsg)
    }
  }

  import ValidationStepsControllerTest._
  def createViolationCandidateRecord(license: String, confidence: Int, rawLicense: String): ViolationCandidateRecord = {
    val speedRecord = create[VehicleSpeedRecord].copy(id = ViolationVerificationResult.ReClassify.toString)

    val lane = speedRecord.entry.lane.copy(system = systemId)
    val entryData = speedRecord.entry.copy(lane = lane, license = Some(ValueWithConfidence[String](license, confidence)), rawLicense = Some(rawLic),
      country = Some(ValueWithConfidence[String]("NL", confidence)), licensePlateData = Some(LicensePlateData()))

    val exitData = speedRecord.entry.copy(lane = lane, license = Some(ValueWithConfidence[String](license, confidence)), rawLicense = Some(rawLic),
      country = Some(ValueWithConfidence[String]("NL", confidence)), licensePlateData = Some(LicensePlateData()))

    val speedRecordToUse = speedRecord.copy(entry = entryData, exit = Some(exitData))

    val vehicleClassifiedRecord = create[VehicleClassifiedRecord].copy(id = ViolationVerificationResult.ReClassify.toString, speedRecord = speedRecordToUse)

    val vehicleViolationRecord = VehicleViolationRecord("id", vehicleClassifiedRecord, 100, None, List())
    ViolationCandidateRecord(1000, vehicleViolationRecord)
  }

  def createCheckResult(license: String, confidence: Int, rawLicense: String): IntradaCheckResult = {
    val entry = new ImageCheckResult(0)
    val lic = Some(ValueWithConfidence(license, confidence))
    val country = Some(ValueWithConfidence[String]("NL", 50))
    val data = LicensePlateData(license = lic,
      rawLicense = Some(rawLic),
      recognizedBy = Some(VehicleMetadata.Recognizer.INTRADA),
      country = country)
    val vehicle = create[VehicleMetadata].copy(license = lic, country = country, licensePlateData = Some(data), recognizedBy = Some(VehicleMetadata.Recognizer.INTRADA), rawLicense = Some(rawLic))

    entry.addRecognitionResult(RecognitionResult(id = RecognitionResult.ARH_ENTRY, vehicle = vehicle))
    entry.addRecognitionResult(RecognitionResult(id = RecognitionResult.ARH_EXIT, vehicle = vehicle))
    entry.addRecognitionResult(RecognitionResult(id = RecognitionResult.INTRADA2, vehicle = vehicle))
    IntradaCheckResult(entry = Some(entry), exit = Some(entry))
  }
}
