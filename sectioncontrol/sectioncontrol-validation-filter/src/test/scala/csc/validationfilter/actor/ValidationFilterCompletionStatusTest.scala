package csc.validationfilter.actor

import akka.actor.{ ActorRef, ActorSystem }
import akka.testkit.{ ImplicitSender, TestKit }
import csc.curator.CuratorTestServer
import csc.curator.utils.Curator
import csc.sectioncontrol.storagelayer.jobs.{ ValidationFilterCompletionStatusService, ProcessedRegistrationCompletionStatusService }
import csc.validationfilter.actor.messages._
import csc.validationfilter.hbasestub.HBasePersistentStub
import csc.validationfilter.{ CuratorFill, CuratorHelper, Dictionary }
import org.joda.time.DateTime
import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }
import akka.util.duration._

class ValidationFilterCompletionStatusTest extends TestKit(ActorSystem("validationSystem")) with WordSpec with MustMatchers with BeforeAndAfterAll with BeforeAndAfterEach with CuratorTestServer with ImplicitSender {

  var curator: Curator = _
  var validationFilterCompletionStatusService: ValidationFilterCompletionStatusService = _

  var accessActor: ActorRef = _

  override def beforeEach: Unit = {
    super.beforeEach()

    try { system.stop(accessActor) }
    catch {
      case e: Exception ⇒ ///
    }

    curator = CuratorHelper.create(this, clientScope)
    CuratorFill.createConfig(curator, HBasePersistentStub.SYSTEM, HBasePersistentStub.CORRIDOR)
    validationFilterCompletionStatusService = new ValidationFilterCompletionStatusService(curator)

    accessActor = system.actorOf(ZookeeperAccessActor.props(curator), Dictionary.ZOOKEEPER_ACTOR)

  }

  override def afterEach() {
    // curator.curator.foreach(c ⇒ c.close())
    super.afterEach()
  }

  override protected def afterAll(): Unit = {
    system.shutdown()
  }

  "ValidationFiltercompletionStatus test " must {

    "yield a timestamp in initial call to service which is at least more than 23 hours ago" in {

      val twentythreeHoursAgo = new DateTime(System.currentTimeMillis()).minusHours(23).getMillis

      accessActor ! GetFinishedJobsMessage(HBasePersistentStub.SYSTEM)

      receiveWhile(1500 millis) {
        case msg: CompletionStatusMessage ⇒
          val initialTimeIsLongerAgo = msg.validationFilterCompletion.timestamp < twentythreeHoursAgo
          initialTimeIsLongerAgo must be(true)
      }
    }

    "process a ValidationFilterStatusMessage that creates a ValidationCompletionStatusNode in zookeeper" in {

      validationFilterCompletionStatusService.exists(HBasePersistentStub.SYSTEM) must be(false)

      accessActor ! ValidationFilterStatusMessage(123l, HBasePersistentStub.SYSTEM)

      accessActor ! GetFinishedJobsMessage(HBasePersistentStub.SYSTEM)

      receiveWhile(1500 millis) {
        case msg: CompletionStatusMessage ⇒
          msg.validationFilterCompletion.timestamp must be(123l)
      }

    }

    "handle create, exists and update logics correctly" in {

      validationFilterCompletionStatusService.exists(HBasePersistentStub.SYSTEM) must be(false)

      accessActor ! ValidationFilterStatusMessage(123l, HBasePersistentStub.SYSTEM)

      Thread.sleep(2000)
      validationFilterCompletionStatusService.get(HBasePersistentStub.SYSTEM).get.completeUntil must be(123l)

      validationFilterCompletionStatusService.exists(HBasePersistentStub.SYSTEM) must be(true)

      accessActor ! ValidationFilterStatusMessage(456l, HBasePersistentStub.SYSTEM)

      Thread.sleep(2000)

      validationFilterCompletionStatusService.get(HBasePersistentStub.SYSTEM).get.completeUntil must be(456l)

    }

  }

}