package csc.validationfilter.actor

import akka.actor.{ ActorRef, Props, ActorSystem }
import akka.testkit.{ TestKit }
import csc.validationfilter.actor.messages.{ Fetch, ValidateAbleVehicleMessage }

import csc.validationfilter.hbasestub.{ HBasePersistentStub, HBasePersistentStubService }
import csc.validationfilter.service.{ HBasePersistentServiceImpl, PersistentService }
import csc.validationfilter.{ TestUtils, TestCurator, Dictionary }
import org.scalatest.matchers.{ ShouldMatchers, MustMatchers }
import org.scalatest.{ BeforeAndAfterEach, BeforeAndAfterAll, WordSpec }
import akka.util.duration._

class ClassifiedRecordReaderActorTest() extends TestKit(ActorSystem("test-validation")) with WordSpec with ShouldMatchers with BeforeAndAfterAll with BeforeAndAfterEach {

  class TestClassifiedRecordReaderActor(sysId: String, testActor: ActorRef) extends ClassifiedRecordReaderActor(sysId) {
    override val persistentService: PersistentService = new HBasePersistentServiceImpl(sysId) with HBasePersistentStubService
    override def validationStepsController = testActor

  }

  override protected def beforeAll(): Unit = {
    val curator = new TestCurator
    system.actorOf(ZookeeperAccessActor.props(curator), Dictionary.ZOOKEEPER_ACTOR)
  }

  override protected def afterAll(): Unit = {
    system.shutdown()
  }

  "ClassifiedRecordReader Actor" must {
    "fetch vehicle records from hbase" in {

      val classifiedReaderActor = system.actorOf(Props(new TestClassifiedRecordReaderActor("test-system", testActor)))
      val processedUntil = HBasePersistentStub.classifiedRecordsFromStorage.filter(_.time < TestUtils.processedUntilTime)

      var messages = List[ValidateAbleVehicleMessage]()

      receiveWhile(500 millis) {
        case msg: ValidateAbleVehicleMessage ⇒ messages = msg :: messages
      }
      messages.length should be(processedUntil.size)

      // do a second run, the testcurator is taking care of a new lastupdate timestamp
      val newStartTime = processedUntil.last.time
      val remainder = HBasePersistentStub.classifiedRecordsFromStorage.filter(_.time >= newStartTime).take(2)
      messages = Nil
      classifiedReaderActor ! Fetch

      receiveWhile(1500 millis) {
        case msg: ValidateAbleVehicleMessage ⇒ messages = msg :: messages
      }

      messages.length should be(remainder.size)
    }
  }
}