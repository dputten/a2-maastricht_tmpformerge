package csc.validationfilter.actor

import akka.actor.{ ActorRef, ActorSystem }
import akka.testkit.{ ImplicitSender, TestKit }
import csc.curator.CuratorTestServer
import csc.curator.utils.Curator
import csc.sectioncontrol.storagelayer.jobs.ProcessedRegistrationCompletionStatusService
import csc.validationfilter.hbasestub.HBasePersistentStub
import csc.validationfilter.{ CuratorFill, CuratorHelper, Dictionary }
import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }
import scala.collection.immutable.SortedSet
import csc.validationfilter.actor.messages.KeepAliveMessage
import csc.validationfilter.actor.messages.WakeupMessage
import csc.validationfilter.actor.messages.ProcessedRegistrationMessage

class ProcessRegistrationActorTest extends TestKit(ActorSystem("validationSystem")) with WordSpec with MustMatchers with BeforeAndAfterAll with BeforeAndAfterEach with CuratorTestServer with ImplicitSender {

  var curator: Curator = _
  var processRegistrationStatusService: ProcessedRegistrationCompletionStatusService = _

  var accessActor: ActorRef = _

  override def beforeEach(): Unit = {
    super.beforeEach()

    try { system.stop(accessActor) }
    catch {
      case e: Exception ⇒ ///
    }

    curator = CuratorHelper.create(this, clientScope)
    CuratorFill.createConfig(curator, HBasePersistentStub.SYSTEM, HBasePersistentStub.CORRIDOR)
    processRegistrationStatusService = new ProcessedRegistrationCompletionStatusService(curator)

    accessActor = system.actorOf(ZookeeperAccessActor.props(curator), Dictionary.ZOOKEEPER_ACTOR)

  }

  override def afterEach() {
    super.afterEach()
  }

  override protected def afterAll(): Unit = {
    system.shutdown()
  }

  "ProcessRegistrationActor Actor" must {

    "process multiple processRegistrationMessages skipping the incompleted resulting in a status in zookeeper" in {

      processRegistrationStatusService.exists(HBasePersistentStub.SYSTEM) must be(false)

      val processed = List(
        ProcessedRegistrationMessage(id = "1", eventTimestamp = 1, beginProcessing = System.currentTimeMillis(), endProcessing = 0, HBasePersistentStub.SYSTEM),
        ProcessedRegistrationMessage(id = "1", eventTimestamp = 1, beginProcessing = System.currentTimeMillis(), endProcessing = 10, HBasePersistentStub.SYSTEM),
        ProcessedRegistrationMessage(id = "2", eventTimestamp = 2, beginProcessing = System.currentTimeMillis(), endProcessing = 0, HBasePersistentStub.SYSTEM),
        ProcessedRegistrationMessage(id = "2", eventTimestamp = 2, beginProcessing = System.currentTimeMillis(), endProcessing = 10, HBasePersistentStub.SYSTEM),
        ProcessedRegistrationMessage(id = "3", eventTimestamp = 3, beginProcessing = System.currentTimeMillis(), endProcessing = 0, HBasePersistentStub.SYSTEM),
        ProcessedRegistrationMessage(id = "4", eventTimestamp = 4, beginProcessing = System.currentTimeMillis(), endProcessing = 0, HBasePersistentStub.SYSTEM),
        ProcessedRegistrationMessage(id = "4", eventTimestamp = 4, beginProcessing = System.currentTimeMillis(), endProcessing = 10, HBasePersistentStub.SYSTEM))

      val processRegistrationActor = system.actorOf(ProcessedRegistrationsActor.props(HBasePersistentStub.SYSTEM))
      processed foreach (processRegistrationActor ! _)
      processRegistrationActor ! WakeupMessage(HBasePersistentStub.SYSTEM)

      Thread.sleep(2000)
      processRegistrationStatusService.get(HBasePersistentStub.SYSTEM).get.completeUntil must be(2)
    }

    "process a processRegistrationMessage resulting in a status in zookeeper" in {

      processRegistrationStatusService.exists(HBasePersistentStub.SYSTEM) must be(false)

      val processed = List(
        ProcessedRegistrationMessage(id = "12345", eventTimestamp = 1, beginProcessing = System.currentTimeMillis(), endProcessing = 0, HBasePersistentStub.SYSTEM),
        ProcessedRegistrationMessage(id = "12345", eventTimestamp = 1, beginProcessing = System.currentTimeMillis(), endProcessing = 12, HBasePersistentStub.SYSTEM))

      val processRegistrationActor = system.actorOf(ProcessedRegistrationsActor.props(HBasePersistentStub.SYSTEM))
      processed foreach (processRegistrationActor ! _)
      processRegistrationActor ! WakeupMessage(HBasePersistentStub.SYSTEM)

      Thread.sleep(2000)

      processRegistrationStatusService.exists(HBasePersistentStub.SYSTEM) must be(true)

    }

    "process a processRegistrationMessage without endtime resulting in no status in zookeeper" in {

      processRegistrationStatusService.exists(HBasePersistentStub.SYSTEM) must be(false)

      val processed = List(ProcessedRegistrationMessage(id = "12345", eventTimestamp = 1, beginProcessing = System.currentTimeMillis(), endProcessing = 0, HBasePersistentStub.SYSTEM))

      val processRegistrationActor = system.actorOf(ProcessedRegistrationsActor.props(HBasePersistentStub.SYSTEM))
      processed foreach (processRegistrationActor ! _)
      processRegistrationActor ! WakeupMessage(HBasePersistentStub.SYSTEM)

      Thread.sleep(2000)

      processRegistrationStatusService.exists(HBasePersistentStub.SYSTEM) must be(false)

    }

    "process multiple processRegistrationMessages skipping the expired timeouts resulting in a status in zookeeper" in {

      processRegistrationStatusService.exists(HBasePersistentStub.SYSTEM) must be(false)

      val processed = List(
        ProcessedRegistrationMessage(id = "1", eventTimestamp = 1, beginProcessing = System.currentTimeMillis(), endProcessing = 0, HBasePersistentStub.SYSTEM),
        ProcessedRegistrationMessage(id = "1", eventTimestamp = 1, beginProcessing = System.currentTimeMillis(), endProcessing = 10, HBasePersistentStub.SYSTEM),
        ProcessedRegistrationMessage(id = "2", eventTimestamp = 2, beginProcessing = System.currentTimeMillis() - 5 * 1000, endProcessing = 0, HBasePersistentStub.SYSTEM),
        ProcessedRegistrationMessage(id = "3", eventTimestamp = 3, beginProcessing = System.currentTimeMillis(), endProcessing = 0, HBasePersistentStub.SYSTEM),
        ProcessedRegistrationMessage(id = "3", eventTimestamp = 3, beginProcessing = System.currentTimeMillis(), endProcessing = 10, HBasePersistentStub.SYSTEM))

      val processRegistrationActor = system.actorOf(ProcessedRegistrationsActor.props(HBasePersistentStub.SYSTEM))
      processed foreach (processRegistrationActor ! _)
      processRegistrationActor ! WakeupMessage(HBasePersistentStub.SYSTEM)

      Thread.sleep(2000)
      processRegistrationStatusService.get(HBasePersistentStub.SYSTEM).get.completeUntil must be(3)

    }

    "process multiple processRegistrationMessages not skipping the unexpired timeouts resulting in a status in zookeeper" in {

      processRegistrationStatusService.exists(HBasePersistentStub.SYSTEM) must be(false)

      val processed = List(
        ProcessedRegistrationMessage(id = "1", eventTimestamp = 1, beginProcessing = System.currentTimeMillis(), endProcessing = 0, HBasePersistentStub.SYSTEM),
        ProcessedRegistrationMessage(id = "1", eventTimestamp = 1, beginProcessing = System.currentTimeMillis(), endProcessing = 10, HBasePersistentStub.SYSTEM),
        ProcessedRegistrationMessage(id = "2", eventTimestamp = 2, beginProcessing = System.currentTimeMillis() - 48 * 100, endProcessing = 0, HBasePersistentStub.SYSTEM),
        ProcessedRegistrationMessage(id = "3", eventTimestamp = 3, beginProcessing = System.currentTimeMillis(), endProcessing = 0, HBasePersistentStub.SYSTEM),
        ProcessedRegistrationMessage(id = "3", eventTimestamp = 3, beginProcessing = System.currentTimeMillis(), endProcessing = 10, HBasePersistentStub.SYSTEM))

      val processRegistrationActor = system.actorOf(ProcessedRegistrationsActor.props(HBasePersistentStub.SYSTEM))
      processed foreach (processRegistrationActor ! _)
      processRegistrationActor ! WakeupMessage(HBasePersistentStub.SYSTEM)

      Thread.sleep(2000)
      processRegistrationStatusService.get(HBasePersistentStub.SYSTEM).get.completeUntil must be(1)

    }
    "process multiple processRegistrationMessages that updates status in zookeeper" in {

      processRegistrationStatusService.exists(HBasePersistentStub.SYSTEM) must be(false)

      val processed = List(
        ProcessedRegistrationMessage(id = "1", eventTimestamp = 1, beginProcessing = System.currentTimeMillis(), endProcessing = 0, HBasePersistentStub.SYSTEM),
        ProcessedRegistrationMessage(id = "1", eventTimestamp = 1, beginProcessing = System.currentTimeMillis(), endProcessing = 10, HBasePersistentStub.SYSTEM))

      val processRegistrationActor = system.actorOf(ProcessedRegistrationsActor.props(HBasePersistentStub.SYSTEM))
      processed foreach (processRegistrationActor ! _)
      processRegistrationActor ! WakeupMessage(HBasePersistentStub.SYSTEM)

      Thread.sleep(2000)
      processRegistrationStatusService.get(HBasePersistentStub.SYSTEM).get.completeUntil must be(1)

      processRegistrationStatusService.exists(HBasePersistentStub.SYSTEM) must be(true)

      List(
        ProcessedRegistrationMessage(id = "3", eventTimestamp = 25, beginProcessing = System.currentTimeMillis(), endProcessing = 0, HBasePersistentStub.SYSTEM),
        ProcessedRegistrationMessage(id = "3", eventTimestamp = 25, beginProcessing = System.currentTimeMillis(), endProcessing = 10, HBasePersistentStub.SYSTEM))
        .foreach(processRegistrationActor ! _)

      processRegistrationActor ! WakeupMessage(HBasePersistentStub.SYSTEM)

      Thread.sleep(2000)
      processRegistrationStatusService.get(HBasePersistentStub.SYSTEM).get.completeUntil must be(25)

    }

    "process keepAlive message when work in progress must not update zookeeper" in {

      processRegistrationStatusService.exists(HBasePersistentStub.SYSTEM) must be(false)

      val processed = List(
        ProcessedRegistrationMessage(id = "1", eventTimestamp = 1, beginProcessing = System.currentTimeMillis(), endProcessing = 0, HBasePersistentStub.SYSTEM),
        ProcessedRegistrationMessage(id = "1", eventTimestamp = 1, beginProcessing = System.currentTimeMillis(), endProcessing = 10, HBasePersistentStub.SYSTEM))

      val processRegistrationActor = system.actorOf(ProcessedRegistrationsActor.props(HBasePersistentStub.SYSTEM))
      processed foreach (processRegistrationActor ! _)

      processRegistrationActor ! KeepAliveMessage(1234l, HBasePersistentStub.SYSTEM)

      Thread.sleep(2000)
      processRegistrationStatusService.exists(HBasePersistentStub.SYSTEM) must be(false)

    }

    "process keepAlive message when NO work in progress update zookeeper" in {

      processRegistrationStatusService.exists(HBasePersistentStub.SYSTEM) must be(false)

      val processRegistrationActor = system.actorOf(ProcessedRegistrationsActor.props(HBasePersistentStub.SYSTEM))

      val thisIsTheTimeStored = 1234567L

      processRegistrationActor ! KeepAliveMessage(thisIsTheTimeStored, HBasePersistentStub.SYSTEM)

      Thread.sleep(2000)
      processRegistrationStatusService.get(HBasePersistentStub.SYSTEM).get.completeUntil must be(thisIsTheTimeStored)

    }

  }

  // timeout= now - beginProcessing >= (timeoutInSeconds * 1000)
  // timeout= 10000L - beginProcessing >= (1 * 1000)
  val nowTest = 10000L
  val timeoutInSeconds = 1
  def timedOutNotCompleted(eventTimestamp: Long) = ProcessedRegistrationMessage(id = "timed out, not completed", eventTimestamp = eventTimestamp, beginProcessing = 9000, endProcessing = 0, HBasePersistentStub.SYSTEM)
  def timedOutCompleted(eventTimestamp: Long) = ProcessedRegistrationMessage(id = "timed out, completed", eventTimestamp = eventTimestamp, beginProcessing = 9000, endProcessing = 10000, HBasePersistentStub.SYSTEM)
  def notTimedOutNotCompleted(eventTimestamp: Long) = ProcessedRegistrationMessage(id = "not timed out, not completed ", eventTimestamp = eventTimestamp, beginProcessing = 10000, endProcessing = 0, HBasePersistentStub.SYSTEM)
  def notTimedOutCompleted(eventTimestamp: Long) = ProcessedRegistrationMessage(id = "not timed out, completed ", eventTimestamp = eventTimestamp, beginProcessing = 10000, endProcessing = 10000, HBasePersistentStub.SYSTEM)

  "ProcessedRegistrationsActor.getCompletedUntilTime" must {

    // T!C - TC - T!C | C !T!C
    "return the eventTimestamp of the first completed messages direct after the timeout" in {
      val processedRegistrationMessages = SortedSet[ProcessedRegistrationMessage](
        timedOutNotCompleted(0), timedOutCompleted(1), timedOutNotCompleted(2), notTimedOutCompleted(3), notTimedOutNotCompleted(4))

      val completedUntil = ProcessedRegistrationsActor.getCompletedUntilTime(processedRegistrationMessages, nowTest, timeoutInSeconds)
      completedUntil.isEmpty must be(false)
      completedUntil.get must be(3)
    }

    // T!C - TC - T!C | !TC !TC
    "return the eventTimestamp of the last message in a successive sequence of completed messages direct after the timeout" in {
      val processedRegistrationMessages = SortedSet[ProcessedRegistrationMessage](
        timedOutNotCompleted(0), timedOutCompleted(1), timedOutNotCompleted(2), notTimedOutCompleted(3), notTimedOutCompleted(4))

      val completedUntil = ProcessedRegistrationsActor.getCompletedUntilTime(processedRegistrationMessages, nowTest, timeoutInSeconds)
      completedUntil.isEmpty must be(false)
      completedUntil.get must be(4)
    }

    // T!C - TC - T!C | !T!C !T!C
    "return the eventTimestamp of the last timed-out message when the first messages direct after the timeout is not completed" in {
      val processedRegistrationMessages = SortedSet[ProcessedRegistrationMessage](
        timedOutNotCompleted(0), timedOutCompleted(1), timedOutNotCompleted(2), notTimedOutNotCompleted(3), notTimedOutNotCompleted(4))

      val completedUntil = ProcessedRegistrationsActor.getCompletedUntilTime(processedRegistrationMessages, nowTest, timeoutInSeconds)
      completedUntil.isEmpty must be(false)
      completedUntil.get must be(2)
    }

    // T!C - TC - T!C | !T!C !TC
    "return the eventTimestamp of the last timed-out message when the first messages direct after the timeout is not completed, but the second is completed" in {
      val processedRegistrationMessages = SortedSet[ProcessedRegistrationMessage](
        timedOutNotCompleted(0), timedOutCompleted(1), timedOutNotCompleted(2), notTimedOutNotCompleted(3), notTimedOutCompleted(4))

      val completedUntil = ProcessedRegistrationsActor.getCompletedUntilTime(processedRegistrationMessages, nowTest, timeoutInSeconds)
      println(completedUntil)
      completedUntil.isEmpty must be(false)
      completedUntil.get must be(2)
    }

    // T!C - TC - T!C |
    "return the eventTimestamp of the last timed-out message when there are no messages after the timeout" in {
      val processedRegistrationMessages = SortedSet[ProcessedRegistrationMessage](
        timedOutNotCompleted(0), timedOutCompleted(1), timedOutNotCompleted(2))

      val completedUntil = ProcessedRegistrationsActor.getCompletedUntilTime(processedRegistrationMessages, nowTest, timeoutInSeconds)
      println(completedUntil)
      completedUntil.isEmpty must be(false)
      completedUntil.get must be(2)
    }
  }

  "ProcessedRegistrationsActor.discardCompletedMessagesUntilLastProcessed" must {
    "filter nothing out when there are no timed-out messages and all are not completed." in {
      val processedRegistrationMessages = SortedSet[ProcessedRegistrationMessage](
        notTimedOutNotCompleted(0), notTimedOutNotCompleted(1), notTimedOutNotCompleted(2), notTimedOutNotCompleted(3), notTimedOutNotCompleted(4))

      val result = ProcessedRegistrationsActor.discardCompletedMessagesUntilLastProcessed(processedRegistrationMessages, nowTest, timeoutInSeconds)
      result.size must be(5)
    }

    "filter out the first successive sequences of completed messages after the timeout when there are no timed-out messages" in {
      val processedRegistrationMessages = SortedSet[ProcessedRegistrationMessage](
        notTimedOutCompleted(0), notTimedOutCompleted(1), notTimedOutNotCompleted(2), notTimedOutCompleted(3), notTimedOutNotCompleted(4))

      val result = ProcessedRegistrationsActor.discardCompletedMessagesUntilLastProcessed(processedRegistrationMessages, nowTest, timeoutInSeconds)
      result.size must be(3)
    }

    "filter out the timed-out messages and the first completed message (no successive sequence) after the timeout" in {
      val processedRegistrationMessages = SortedSet[ProcessedRegistrationMessage](
        timedOutNotCompleted(0), timedOutCompleted(1), timedOutNotCompleted(2), notTimedOutCompleted(3), notTimedOutNotCompleted(4))

      val result = ProcessedRegistrationsActor.discardCompletedMessagesUntilLastProcessed(processedRegistrationMessages, nowTest, timeoutInSeconds)
      result.size must be(1)
    }

    "filter out timed-out messages and the completed" in {
      val processedRegistrationMessages = SortedSet[ProcessedRegistrationMessage](
        notTimedOutCompleted(0), timedOutNotCompleted(1), timedOutCompleted(2), notTimedOutCompleted(3), notTimedOutCompleted(4))

      val result = ProcessedRegistrationsActor.discardCompletedMessagesUntilLastProcessed(processedRegistrationMessages, 10000L, 1)
      result.size must be(0)
    }
  }

  "ProcessedRegistrationsActor" must {
    "processedRegistrationMessage SortedSet should be sorted on eventTimestamp through the compare override" in {
      import csc.validationfilter.actor.messages.ProcessedRegistrationMessage
      val processedRegistrationMessages = SortedSet[ProcessedRegistrationMessage](
        ProcessedRegistrationMessage(id = "1", eventTimestamp = 3, beginProcessing = 9000, endProcessing = 10000, HBasePersistentStub.SYSTEM),
        ProcessedRegistrationMessage(id = "1", eventTimestamp = 1, beginProcessing = 9000, endProcessing = 10000, HBasePersistentStub.SYSTEM),
        ProcessedRegistrationMessage(id = "2", eventTimestamp = 2, beginProcessing = 9000, endProcessing = 10000, HBasePersistentStub.SYSTEM),
        ProcessedRegistrationMessage(id = "3", eventTimestamp = 4, beginProcessing = 9000, endProcessing = 10000, HBasePersistentStub.SYSTEM),
        ProcessedRegistrationMessage(id = "3", eventTimestamp = 0, beginProcessing = 9000, endProcessing = 10000, HBasePersistentStub.SYSTEM))

      for (a ← 0 to 4) {
        processedRegistrationMessages.toList(a).eventTimestamp must be(a)
      }
    }
  }
}