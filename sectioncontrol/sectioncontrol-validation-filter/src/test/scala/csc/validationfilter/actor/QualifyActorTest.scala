package csc.validationfilter.actor

import akka.actor.{ Props, ActorSystem }
import akka.testkit.{ ImplicitSender, TestKit }
import csc.curator.CuratorTestServer
import csc.curator.utils.Curator
import csc.validationfilter.qualify.actor.QualifyActor
import csc.validationfilter.{ CuratorHelper, CuratorFill, Dictionary }
import csc.validationfilter.actor.messages.{ ViolationCandidateMessage, ValidateAbleVehicleMessage }
import csc.validationfilter.hbasestub.HBasePersistentStub
import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterEach, BeforeAndAfterAll, WordSpec }
import akka.util.duration._

class QualifyActorTest extends TestKit(ActorSystem("validationSystem")) with WordSpec with MustMatchers with BeforeAndAfterAll with BeforeAndAfterEach with CuratorTestServer with ImplicitSender {

  var curator: Curator = _

  override def beforeEach(): Unit = {
    super.beforeEach()
    curator = CuratorHelper.create(this, clientScope)
    CuratorFill.createConfig(curator, HBasePersistentStub.SYSTEM, HBasePersistentStub.CORRIDOR)

    system.actorOf(ZookeeperAccessActor.props(curator), Dictionary.ZOOKEEPER_ACTOR)
  }

  override def afterEach() {
    curator.curator.foreach(c ⇒ c.close())
    super.afterEach()
  }

  override protected def afterAll(): Unit = {
    system.shutdown()
  }

  val someRegistration = HBasePersistentStub.classifiedRecordsFromStorage.head

  "Validation Actor" must {
    "process a validateableVehicleMessage" in {
      val qualifyActor = system.actorOf(QualifyActor.props)
      qualifyActor ! ValidateAbleVehicleMessage(someRegistration)

      expectMsgClass(3 second, classOf[ViolationCandidateMessage])
    }
  }

}