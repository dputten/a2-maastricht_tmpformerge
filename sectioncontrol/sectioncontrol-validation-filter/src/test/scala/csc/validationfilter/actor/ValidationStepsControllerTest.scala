package csc.validationfilter.actor

import akka.actor._
import akka.testkit.{ ImplicitSender, TestActorRef, TestKit }
import akka.util.duration._
import csc.curator.CuratorTestServer
import csc.sectioncontrol.messages._
import csc.util.test.MockActor.MessageMapper
import csc.util.test.{ MockActor, ObjectBuilder }
import csc.validationfilter._
import csc.validationfilter.actor.messages.{ GetActor, ImageProcessorResultMessage, ProcessedRegistrationMessage, ReclassifiedMessage, RegisteredActorMessage, ValidateAbleVehicleMessage, ValidationFinished, ViolationCandidateImageStatusMessage, ViolationCandidateMessage }
import csc.validationfilter.domain.ViolationVerificationResult
import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }

class ValidationStepsControllerSpeedRecordTest
  extends TestKit(ActorSystem("ValidationStepsControllerSpeedRecordTest"))
  with WordSpec
  with MustMatchers
  with BeforeAndAfterAll
  with BeforeAndAfterEach
  with CuratorTestServer
  with ImplicitSender {

  import ValidationStepsControllerTest._

  def createImageProcessorResultMessage(license: String, confidence: Int): ImageProcessorResultMessage = {
    ImageProcessorResultMessage(ViolationVerificationResult.ReClassify,
      LicensePlateData(license = Some(ValueWithConfidence[String](license, confidence)),
        recognizedBy = Some("SUMMARY"),
        country = Some(ValueWithConfidence[String]("NL", 50))),
      Some(LicensePlateData(license = Some(ValueWithConfidence[String](license, confidence)),
        recognizedBy = Some(VehicleMetadata.Recognizer.INTRADA),
        country = Some(ValueWithConfidence[String]("NL", 50)))))
  }

  def createViolationCandidateRecord(license: String, confidence: Int, rawLicense: String): ViolationCandidateRecord = {
    val speedRecord = create[VehicleSpeedRecord].copy(id = ViolationVerificationResult.ReClassify.toString)

    val entryData = speedRecord.entry.copy(license = Some(ValueWithConfidence[String](license, confidence)), rawLicense = Some(rawLicense),
      country = Some(ValueWithConfidence[String]("BE", confidence)), licensePlateData = Some(LicensePlateData()))

    val exitData = speedRecord.entry.copy(license = Some(ValueWithConfidence[String](license, confidence)), rawLicense = Some(rawLicense),
      country = Some(ValueWithConfidence[String]("BE", confidence)), licensePlateData = Some(LicensePlateData()))

    val speedRecordToUse = speedRecord.copy(entry = entryData, exit = Some(exitData))

    val vehicleClassifiedRecord = create[VehicleClassifiedRecord].copy(id = ViolationVerificationResult.ReClassify.toString, speedRecord = speedRecordToUse)

    val vehicleViolationRecord = VehicleViolationRecord("id", vehicleClassifiedRecord, 100, None, List())
    ViolationCandidateRecord(1000, vehicleViolationRecord)
  }

  "ValidationStepsControllerActor.speedRecord" must {

    "set the license, confidence, country and rawlicense, recognizedBy to the Intrada result when Arh and Intrada return a different result." in {
      val actor = TestActorRef(new ValidationStepsControllerActor)

      val validationStepsControllerActor = actor.underlyingActor

      val intradaImageProcessorResultMessage = createImageProcessorResultMessage("AAAAAA", 50)
      val vehicleSpeedRecord = validationStepsControllerActor.speedRecord(intradaImageProcessorResultMessage, createViolationCandidateRecord("BBBBBB", 100, "BB_BB_BB"))

      vehicleSpeedRecord.entry.license.get.value must be("AAAAAA")
      vehicleSpeedRecord.entry.license.get.confidence must be(50)
      vehicleSpeedRecord.entry.rawLicense.get must be("AAAAAA")
      vehicleSpeedRecord.entry.recognizedBy must be(Some(VehicleMetadata.Recognizer.INTRADA))
      vehicleSpeedRecord.entry.country must be(Some(ValueWithConfidence[String]("NL", 50)))

      vehicleSpeedRecord.exit.get.license.get.value must be("AAAAAA")
      vehicleSpeedRecord.exit.get.license.get.confidence must be(50)
      vehicleSpeedRecord.exit.get.rawLicense.get must be("AAAAAA")
      vehicleSpeedRecord.exit.get.recognizedBy must be(Some(VehicleMetadata.Recognizer.INTRADA))
      vehicleSpeedRecord.exit.get.country must be(Some(ValueWithConfidence[String]("NL", 50)))

      vehicleSpeedRecord.entry.licensePlateData.get.license.get.value must be("AAAAAA")
      vehicleSpeedRecord.entry.licensePlateData.get.license.get.confidence must be(50)
      vehicleSpeedRecord.entry.licensePlateData.get.rawLicense.get must be("AAAAAA")
      vehicleSpeedRecord.entry.licensePlateData.get.recognizedBy.get must be(VehicleMetadata.Recognizer.INTRADA)
      vehicleSpeedRecord.entry.licensePlateData.get.country must be(Some(ValueWithConfidence[String]("NL", 50)))

      vehicleSpeedRecord.exit.get.licensePlateData.get.license.get.value must be("AAAAAA")
      vehicleSpeedRecord.exit.get.licensePlateData.get.license.get.confidence must be(50)
      vehicleSpeedRecord.exit.get.licensePlateData.get.rawLicense.get must be("AAAAAA")
      vehicleSpeedRecord.exit.get.licensePlateData.get.recognizedBy.get must be(VehicleMetadata.Recognizer.INTRADA)
      vehicleSpeedRecord.exit.get.licensePlateData.get.country must be(Some(ValueWithConfidence[String]("NL", 50)))
    }

    "set the license, confidence, country, recognizedBy to the Intrada result and the rawlicense to Arh when they return the same result." in {
      val actor = TestActorRef(new ValidationStepsControllerActor)
      val validationStepsControllerActor = actor.underlyingActor

      val intradaImageProcessorResultMessage = createImageProcessorResultMessage("AAAAAA", 50)
      val vehicleSpeedRecord = validationStepsControllerActor.speedRecord(intradaImageProcessorResultMessage,
        createViolationCandidateRecord("AAAAAA", 100, "AA_AA_AA"))

      vehicleSpeedRecord.entry.license.get.value must be("AAAAAA")
      vehicleSpeedRecord.entry.license.get.confidence must be(50)
      vehicleSpeedRecord.entry.rawLicense.get must be("AA_AA_AA")
      vehicleSpeedRecord.entry.recognizedBy must be(Some(VehicleMetadata.Recognizer.INTRADA))
      vehicleSpeedRecord.entry.country must be(Some(ValueWithConfidence[String]("NL", 50)))

      vehicleSpeedRecord.exit.get.license.get.value must be("AAAAAA")
      vehicleSpeedRecord.exit.get.license.get.confidence must be(50)
      vehicleSpeedRecord.exit.get.rawLicense.get must be("AA_AA_AA")
      vehicleSpeedRecord.exit.get.recognizedBy must be(Some(VehicleMetadata.Recognizer.INTRADA))
      vehicleSpeedRecord.exit.get.country must be(Some(ValueWithConfidence[String]("NL", 50)))

      vehicleSpeedRecord.entry.licensePlateData.get.license.get.value must be("AAAAAA")
      vehicleSpeedRecord.entry.licensePlateData.get.license.get.confidence must be(50)
      vehicleSpeedRecord.entry.licensePlateData.get.rawLicense.get must be("AA_AA_AA")
      vehicleSpeedRecord.entry.licensePlateData.get.recognizedBy.get must be(VehicleMetadata.Recognizer.INTRADA)
      vehicleSpeedRecord.entry.licensePlateData.get.country must be(Some(ValueWithConfidence[String]("NL", 50)))

      vehicleSpeedRecord.exit.get.licensePlateData.get.license.get.value must be("AAAAAA")
      vehicleSpeedRecord.exit.get.licensePlateData.get.license.get.confidence must be(50)
      vehicleSpeedRecord.exit.get.licensePlateData.get.rawLicense.get must be("AA_AA_AA")
      vehicleSpeedRecord.exit.get.licensePlateData.get.recognizedBy.get must be(VehicleMetadata.Recognizer.INTRADA)
      vehicleSpeedRecord.exit.get.licensePlateData.get.country must be(Some(ValueWithConfidence[String]("NL", 50)))
    }
  }

}

class ValidationStepsControllerTest
  extends TestKit(ActorSystem("validationSystem"))
  with WordSpec
  with MustMatchers
  with BeforeAndAfterAll
  with BeforeAndAfterEach
  with CuratorTestServer
  with ImplicitSender {

  import ValidationStepsControllerTest._

  var imageChekersActor: ActorRef = _
  var reclassifyActor: ActorRef = _

  val licData = LicensePlateData(license = Some(ValueWithConfidence[String]("AAAAAA", 60)),
    recognizedBy = Some(VehicleMetadata.Recognizer.INTRADA),
    country = Some(ValueWithConfidence[String]("NL", 50)))

  class StubImageCheckersActor extends MockActor(None) {
    override def mapper: MessageMapper = {
      case GetActor(_) ⇒ RegisteredActorMessage(Some(self))
      case ViolationCandidateMessage(record) ⇒
        ImageProcessorResultMessage(verificationResultOf(record.vehicleViolationRecord.id), licData.copy(recognizedBy = Some("SUMMARY")), Some(licData))
    }
  }

  class ValidationStepsTestActor extends ValidationStepsControllerActor {
    override val retrieveImagesActor = context.actorOf(MockActor.props(imageRetriever))
    override val qualifierActor = context.actorOf(MockActor.props(qualifier))
    override val updateStateWriterActor: ActorRef = context.actorOf(MockActor.props(candidateWriter, Some(testSelf)))
  }

  override def beforeEach(): Unit = {
    super.beforeEach()
    system.eventStream.subscribe(self, classOf[ProcessedRegistrationMessage])
    imageChekersActor = system.actorOf(Props(new StubImageCheckersActor), Dictionary.IMAGE_CHECKERS_ACTOR)
  }

  override def afterEach() {
    super.afterEach()
    imageChekersActor ! PoisonPill //must be done here, as the PoisionPill is never sent in NoImages case
    system.eventStream.unsubscribe(self)
  }

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    system.actorOf(MockActor.props(reclassifer), Dictionary.RECLASSIFY_ACTOR)
  }

  override protected def afterAll(): Unit = {
    system.shutdown()
  }

  "ValidationStepsControllerActor" must {

    "process successfully a passage without image" in {
      runAndVerify(NoImages)
    }

    "process successfully a normal passage with status OK" in {
      runAndVerify(OK)
    }

    "process successfully a passage with status Invalid" in {
      runAndVerify(Invalid)
    }

    "process successfully a passage wich needs reclassification" in {
      runAndVerify(ReClassify)
    }
  }

  def testSelf: ActorRef = self

  def runAndVerify(testCase: TestCase): Unit = {

    val validationStepsController = system.actorOf(Props(new ValidationStepsTestActor))

    val sr = create[VehicleSpeedRecord].copy(id = testCase.id)
    val entryData = sr.entry.copy(license = Some(ValueWithConfidence[String]("AA11AA", 90)), rawLicense = Some("AA_11_AA"))
    val exitData = sr.entry.copy(license = Some(ValueWithConfidence[String]("AA11AA", 90)), rawLicense = Some("AA_11_AA"))
    val speedRecordToUse = sr.copy(entry = entryData, exit = Some(exitData))

    val vehicleClassifiedRecord = create[VehicleClassifiedRecord].copy(id = testCase.id, speedRecord = speedRecordToUse)
    //val vehicleClassifiedRecord = create[VehicleClassifiedRecord].copy(id = testCase.id, speedRecord = sr)

    validationStepsController ! ValidateAbleVehicleMessage(vehicleClassifiedRecord)

    //check registration start
    val startRegistration = expectMsgClass(1 second, classOf[ProcessedRegistrationMessage])
    startRegistration.id must be(vehicleClassifiedRecord.id)

    if (testCase != NoImages) {

      if (testCase != Invalid) {
        //check registration 'saved'
        val message = expectMsgClass(1 second, classOf[ViolationCandidateMessage])
        val candidate = message.record
        candidate.vehicleViolationRecord.id must be(vehicleClassifiedRecord.id)

        if (testCase == ReClassify) {
          //check reclassify
          candidate.vehicleViolationRecord.classifiedRecord.validLicensePlate must be(Some(reclassifiedLicense))
        }
        val recognizeResults = candidate.vehicleViolationRecord.recognizeResults
        recognizeResults.size must be(2)
        recognizeResults must contain(RecognizeRecord("SUMMARY", true, licData.copy(recognizedBy = Some("SUMMARY"))))
        recognizeResults must contain(RecognizeRecord("INTRADA", false, licData))
      }

      //check registration end
      val endRegistration = expectMsgClass(1 second, classOf[ProcessedRegistrationMessage])
      endRegistration.id must be(vehicleClassifiedRecord.id)
      endRegistration.beginProcessing must be(startRegistration.beginProcessing)
    }
  }

}

object ValidationStepsControllerTest extends ObjectBuilder {

  val reclassifiedLicense = "reclassified"

  def currentTime: Long = System.currentTimeMillis()

  def verificationResultOf(id: String) = ViolationVerificationResult.withName(id) //will fail (and should fail) for NoImages

  //mock actor behaviour functions

  val qualifier: PartialFunction[Any, Any] = {
    case msg: ValidateAbleVehicleMessage ⇒
      val sr = create[VehicleSpeedRecord].copy(id = msg.vehicleClassifiedRecord.id)
      val vr = create[VehicleViolationRecord].copy(id = msg.vehicleClassifiedRecord.id, classifiedRecord = msg.vehicleClassifiedRecord)
      ViolationCandidateMessage(ViolationCandidateRecord(currentTime, vr))
  }

  val imageRetriever: PartialFunction[Any, Any] = {
    case ViolationCandidateMessage(record) ⇒ ViolationCandidateImageStatusMessage(record.vehicleViolationRecord.id != NoImages.id)
  }

  val candidateWriter: PartialFunction[Any, Any] = {
    case msg: ViolationCandidateMessage ⇒ ValidationFinished(true)
  }

  val reclassifer: PartialFunction[Any, Any] = {
    //fake reclassification: just set the validLicensePlate (to later verify)
    case msg: VehicleSpeedRecord ⇒ ReclassifiedMessage(
      create[VehicleClassifiedRecord].copy(id = msg.id + "updated-by-reclassify", speedRecord = msg, validLicensePlate = Some(reclassifiedLicense)))

  }

  //test cases. Id matches ViolationVerificationResult value (except for 'NoImages')

  abstract class TestCase(val id: String)
  case object NoImages extends TestCase("NoImages")
  case object OK extends TestCase(ViolationVerificationResult.OK.toString)
  case object Invalid extends TestCase(ViolationVerificationResult.Invalid.toString)
  case object ReClassify extends TestCase(ViolationVerificationResult.ReClassify.toString)

}
