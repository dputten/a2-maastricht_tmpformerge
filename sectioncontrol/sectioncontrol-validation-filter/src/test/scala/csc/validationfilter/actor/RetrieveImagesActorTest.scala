package csc.validationfilter.actor

import akka.actor.{ ActorRef, ActorSystem }
import akka.testkit.{ TestKit, TestProbe }
import csc.imageretriever.{ GetImageRequest, GetImageResponse }
import csc.sectioncontrol.messages._
import csc.util.test.{ MockActor, ObjectBuilder }
import csc.validationfilter.Dictionary
import csc.validationfilter.actor.messages.{ ViolationCandidateImageStatusMessage, ViolationCandidateMessage }
import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }

/**
 * Created by carlos on 14.08.15.
 */
class RetrieveImagesActorTest
  extends TestKit(ActorSystem("validationSystem"))
  with WordSpec
  with MustMatchers
  with BeforeAndAfterAll
  with BeforeAndAfterEach {

  import csc.validationfilter.actor.RetrieveImagesActorTest._

  var actor: ActorRef = _
  val probe: TestProbe = new TestProbe(system)

  "RetrieveImagesActor" must {

    "return positive image status when both images exist" in {
      runAndCheck(bothImagesRecord, true)
    }

    "return negative image status when no exit vehicle exists" in {
      runAndCheck(noExitVehicleRecord, false)
    }

    "return negative image status when exit image is not found" in {
      runAndCheck(noExitImageRecord, false)
    }

    "return negative image status when entry image is not found" in {
      runAndCheck(noEntryImageRecord, false)
    }

    "return negative image status when none of the images is found" in {
      runAndCheck(noImagesRecord, false)
    }
  }

  def runAndCheck(rec: ViolationCandidateRecord, expected: Boolean): Unit = {
    probe.send(actor, ViolationCandidateMessage(rec))
    val response = probe.expectMsgClass(classOf[ViolationCandidateImageStatusMessage])
    response.hasImages must be(expected)
  }

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    system.actorOf(MockActor.props(imageRetrieverBehavior), Dictionary.IMAGE_RETRIEVER_ACTOR)
  }

  override def beforeEach(): Unit = {
    actor = system.actorOf(RetrieveImagesActor.props)
  }

  override def afterAll(): Unit = {
    system.shutdown()
    super.afterAll()
  }
}

object RetrieveImagesActorTest extends ObjectBuilder {

  lazy val missingImageUri = "missing"
  lazy val entryLane = Lane("entry", "entry", "entry", "system", 0, 0)
  lazy val exitLane = Lane("exit", "exit", "exit", "system", 0, 0)
  lazy val imageContents: Array[Byte] = "".getBytes

  lazy val bothImagesRecord = createViolationRecord(true, true, true)
  lazy val noExitVehicleRecord = createViolationRecord(false, false, true)
  lazy val noExitImageRecord = createViolationRecord(true, false, true)
  lazy val noEntryImageRecord = createViolationRecord(true, true, false)
  lazy val noImagesRecord = createViolationRecord(true, false, false)

  def createViolationRecord(exitVehicle: Boolean, exitImage: Boolean, entryImage: Boolean): ViolationCandidateRecord = {

    val exit: Option[VehicleMetadata] = if (exitVehicle) {
      Some(create[VehicleMetadata].copy(lane = exitLane, images = Seq(createImage(exitImage))))
    } else None

    val entry = create[VehicleMetadata].copy(lane = entryLane, images = Seq(createImage(entryImage)))

    val speedRecord = create[VehicleSpeedRecord].copy(entry = entry, exit = exit)
    val classifiedRecord = create[VehicleClassifiedRecord].copy(speedRecord = speedRecord)
    val vehicleViolation = create[VehicleViolationRecord].copy(classifiedRecord = classifiedRecord)
    create[ViolationCandidateRecord].copy(vehicleViolationRecord = vehicleViolation)
  }

  val imageRetrieverBehavior: PartialFunction[Any, Any] = {
    case msg: GetImageRequest ⇒ GetImageResponse(msg.refId, msg.key, getContents(msg.key.uri), None)
  }

  def getContents(uri: String): Option[Array[Byte]] = if (uri == missingImageUri) None else Some(imageContents)

  def createImage(existing: Boolean) = create[VehicleImage].copy(
    imageType = VehicleImageType.Overview,
    uri = if (existing) "some" else missingImageUri)

}
