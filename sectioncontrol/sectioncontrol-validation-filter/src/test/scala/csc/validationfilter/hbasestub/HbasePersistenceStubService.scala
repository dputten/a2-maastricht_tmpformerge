package csc.validationfilter.hbasestub

import java.util.Date

import csc.hbase.utils.HBaseTableKeeper
import csc.sectioncontrol.messages._
import csc.validationfilter.TestUtils
import csc.validationfilter.service.PersistentService

trait HBasePersistentStubService extends PersistentService {

  def violationWriter = new HBaseViolationWriter

  class HBaseViolationWriter extends ViolationWriter with HBaseTableKeeper {

    lazy val violationWriter = null

    def store(record: ViolationCandidateRecord) = {
      println("writing violationrecord: " + record)
    }
  }

  def classifiedRecordReader(s: String) = new HBaseClassifiedRecordReader(s)

  class HBaseClassifiedRecordReader(s: String) extends ClassifiedRecordReader with HBaseTableKeeper {

    lazy val classifiedRecordsReader = null

    def read(from: Long, to: Long): Seq[VehicleClassifiedRecord] = HBasePersistentStub.classifiedRecords(from, to)

  }

}

object HBasePersistentStub {

  val SYSTEM = "test-system"
  val CORRIDOR = 2002

  val numberOfStubbedInstances = 30
  val thirtyMinutesAfterStartTime = TestUtils.processedUntilTime + (30 * 60 * 1000)
  val fiveMinutes = 5 * 60 * 1000

  private val lane1 = Lane("1001", "lane1", "gantry1", SYSTEM, 3.5f, 7.1f)
  private val lane2 = Lane("1002", "lane2", "gantry2", SYSTEM, 3.52f, 7.12f)

  private def entry = VehicleMetadata(lane1, "id123", 123L, Some(new Date(123L).toString), Some(ValueWithConfidence("AA00BB")),
    None, Some(ValueWithConfidence_Float(3.5f, 60)), None, None, Some(ValueWithConfidence("NL")), Seq(), "", "", "SN1234")
  private def exit(exitTime: Long) = VehicleMetadata(lane2, "id999", exitTime, Some(new Date(exitTime).toString), Some(ValueWithConfidence("AA00BB")),
    None, Some(ValueWithConfidence_Float(3.6f, 80)), None, None, Some(ValueWithConfidence("NL")), Seq(), "", "", "SN1234")
  private def record(exitTime: Long) = VehicleSpeedRecord("1001", entry, Some(exit(exitTime)), CORRIDOR, 50, "")

  private def vehicleClassifiedRecord(exitTime: Long) = VehicleClassifiedRecord("1001", record(exitTime), None, ProcessingIndicator.Automatic, true, false, Some("76-SV-PP"), false, IndicatorReason(), None, None)

  val classifiedRecordsFromStorage = {
    List.tabulate(numberOfStubbedInstances)(idx ⇒ idx + thirtyMinutesAfterStartTime - idx * fiveMinutes).map(vehicleClassifiedRecord).sortBy(_.time)
  }

  def classifiedRecords(from: Long, to: Long) = classifiedRecordsFromStorage.filter(vcr ⇒ vcr.time >= from && vcr.time < to)

}

