package csc.validationfilter

import csc.config.Path
import csc.curator.utils.{ Versioned, Curator }
import csc.json.lift.LiftSerialization
import csc.sectioncontrol.classify.nl.ClassificationCompletionStatus
import csc.sectioncontrol.jobs.ValidationFilterCompletionStatus
import csc.validationfilter.configuration.EnforceNLConfig
import org.apache.curator.framework.CuratorFramework
import org.apache.zookeeper.data.Stat
import org.joda.time.DateTime

object TestUtils {

  val processedUntilTime = new DateTime(System.currentTimeMillis()).minusHours(1).getMillis
}

class TestCurator extends Curator {

  var numOfCalls = 0

  var timeStamp = new DateTime(System.currentTimeMillis()).minusDays(1).getMillis

  /**
   * implemented curator mock methods
   */
  override def getVersioned[T <: AnyRef](path: String)(implicit evidence$9: Manifest[T]): Option[Versioned[T]] = {

    /*
       this small routine simulates the fact that a subsequent call to the classifiedreader actor will yield a new ClassificationCompletionStatus
       (in a normal world where the classification is an ongoing process)
     */
    val startTime = TestUtils.processedUntilTime + (numOfCalls * 10 * 60 * 1000)
    numOfCalls = numOfCalls + 1

    Some(Versioned[T](ClassificationCompletionStatus(0l, startTime).asInstanceOf[T], 0))
  }
  override def get[T <: AnyRef](path: Path)(implicit evidence$5: Manifest[T]): Option[T] = {

    Some(ValidationFilterCompletionStatus(System.currentTimeMillis(), timeStamp).asInstanceOf[T])
  }

  override def exists(path: String): Boolean = false

  override def getChildNames(parent: String): Seq[String] = List("test-system")

  override def put[T <: AnyRef](path: String, value: T, ephemeral: Boolean): Unit = {
    timeStamp = value.asInstanceOf[ValidationFilterCompletionStatus].completeUntil
    // empty implementation mimicking storage
  }
  /**
   * ######################################
   * ##### NOT IMPLEMENTED! ################
   * #####################################
   */

  override def put[T <: AnyRef](path: Path, value: T): Unit = throw new NoSuchMethodException("unsupported")

  override def get[T <: AnyRef](path: String)(implicit evidence$4: Manifest[T]): Option[T] = throw new NoSuchMethodException("unsupported")

  override def curator: Option[CuratorFramework] = throw new NoSuchMethodException("unsupported")

  override def set[T <: AnyRef](path: String, value: T, storedVersion: Int): Unit = throw new NoSuchMethodException("unsupported")

  override def set[T <: AnyRef](path: Path, value: T, storedVersion: Int): Unit = throw new NoSuchMethodException("unsupported")

  override def get[T <: AnyRef](pathList: Seq[Path])(implicit evidence$6: Manifest[T]): Option[T] = throw new NoSuchMethodException("unsupported")

  override def get[T <: AnyRef](pathList: Seq[Path], default: T)(implicit evidence$7: Manifest[T]): Option[T] = throw new NoSuchMethodException("unsupported")

  override def get[T <: AnyRef](pathList: Seq[Path], defaults: Map[String, Any])(implicit evidence$8: Manifest[T]): Option[T] = throw new NoSuchMethodException("unsupported")

  override def appendEvent[T <: AnyRef](path: String, value: T): Unit = throw new NoSuchMethodException("unsupported")

  override def appendEvent[T <: AnyRef](path: String, value: T, retries: Int): Unit = throw new NoSuchMethodException("unsupported")

  override def appendEvent[T <: AnyRef](path: Path, value: T): Unit = throw new NoSuchMethodException("unsupported")

  override def appendEvent[T <: AnyRef](path: Path, value: T, retries: Int): Unit = throw new NoSuchMethodException("unsupported")

  override def serialization: LiftSerialization = throw new NoSuchMethodException("unsupported")

  override def delete(path: String): Unit = throw new NoSuchMethodException("unsupported")

  override def delete(path: Path): Unit = throw new NoSuchMethodException("unsupported")

  override def getVersioned[T <: AnyRef](path: Path)(implicit evidence$10: Manifest[T]): Option[Versioned[T]] = throw new NoSuchMethodException("unsupported")

  override def getVersioned[T <: AnyRef](pathList: Seq[Path], default: T)(implicit evidence$11: Manifest[T]): Option[Versioned[T]] = throw new NoSuchMethodException("unsupported")

  override def getChildNames(parent: Path): Seq[String] = throw new NoSuchMethodException("unsupported")

  override def createEmptyPath(path: String): Unit = throw new NoSuchMethodException("unsupported")

  override def createEmptyPath(path: Path): Unit = throw new NoSuchMethodException("unsupported")

  override def safeDelete(path: String): Unit = throw new NoSuchMethodException("unsupported")

  override def safeDelete(path: Path): Unit = throw new NoSuchMethodException("unsupported")

  override def getChildren(parent: String): Seq[String] = throw new NoSuchMethodException("unsupported")

  override def getChildren(parent: Path): Seq[Path] = throw new NoSuchMethodException("unsupported")

  override def deleteRecursive(path: String): Unit = throw new NoSuchMethodException("unsupported")

  override def deleteRecursive(path: Path): Unit = throw new NoSuchMethodException("unsupported")

  override def exists(path: Path): Boolean = throw new NoSuchMethodException("unsupported")

  override def stat(path: String): Option[Stat] = throw new NoSuchMethodException("unsupported")

  override def stat(path: Path): Option[Stat] = throw new NoSuchMethodException("unsupported")
}
