package csc.validationfilter

import csc.config.Path
import csc.curator.utils.Curator
import csc.sectioncontrol.enforce.common.nl.services.SectionControl
import csc.sectioncontrol.messages.EsaProviderType
import csc.sectioncontrol.storage._
import csc.validationfilter.configuration.EnforceNLConfig

object CuratorFill {

  def createConfig(curator: Curator, systemId: String, corridorId: Int) {
    val sys = ZkSystem(id = systemId,
      name = systemId,
      title = "test",
      location = ZkSystemLocation(description = "",
        viewingDirection = None,
        region = "",
        roadNumber = "",
        roadPart = "",
        systemLocation = ""),
      violationPrefixId = "n062",
      orderNumber = 1,
      maxSpeed = 100,
      compressionFactor = 90,
      retentionTimes = ZkSystemRetentionTimes(nrDaysKeepTrafficData = 5,
        nrDaysKeepViolations = 5,
        nrDaysRemindCaseFiles = 2),
      pardonTimes = ZkSystemPardonTimes(),
      esaProviderType = EsaProviderType.NoProvider,
      mtmRouteId = "",
      minimumViolationTimeSeparation = 0,
      serialNumber = SerialNumber("SN1234"))
    curator.put(Paths.Systems.getConfigPath(systemId), sys)
    val corridor = ZkCorridor(id = corridorId.toString,
      name = corridorId.toString,
      roadType = 1,
      serviceType = ServiceType.SectionControl,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = false,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = ZkPSHTMIdentification(useYear = false,
        useMonth = false,
        useDay = false,
        useReportingOfficerId = false,
        useNumberOfTheDay = false,
        useLocationCode = false,
        useHHMCode = false,
        useTypeViolation = false,
        useFixedCharacter = false,
        fixedCharacter = "A",
        elementOrderString = None),
      info = ZkCorridorInfo(corridorId = corridorId,
        locationCode = "10",
        reportId = "",
        reportHtId = "",
        reportPerformance = true,
        locationLine1 = "",
        locationLine2 = None),
      startSectionId = "S1",
      endSectionId = "S1",
      allSectionIds = Seq("S1"),
      direction = ZkDirection(directionFrom = "",
        directionTo = ""),
      radarCode = 9,
      roadCode = 0,
      dutyType = "K0",
      deploymentCode = "0",
      codeText = "")
    curator.put(Paths.Corridors.getConfigPath(systemId, corridorId.toString), corridor)
    val section = ZkSection(id = "S1",
      systemId = systemId,
      name = "S1",
      length = 8000,
      startGantryId = "E1",
      endGantryId = "X1",
      matrixBoards = Nil,
      msiBlackList = Nil)
    curator.put(Paths.Sections.getConfigPath(systemId, corridor.startSectionId), section)

    val rootPathToServices = Path(Paths.Corridors.getServicesPath(systemId, corridorId.toString))
    val pathToService = rootPathToServices / ServiceType.SectionControl.toString
    curator.put(pathToService, SectionControl())

    val schedule = createSchedule("aaa", 1, 2, 22, 10, 17)
    val schedulePath = Paths.Corridors.getSchedulesPath(systemId, corridorId.toString)
    curator.put(schedulePath, List(schedule))

    val enforceNLConfig = EnforceNLConfig("ntpserver", "timezone", "Overview", recognizerCountries = Some(Seq("NL", "DE", "FR", "BE")), recognizerCountryMinConfidence = Some(70))
    val enforceConfigPath = Path(Paths.Systems.getSystemJobsPath(systemId)) / "enforceConfig-nl"
    curator.put(enforceConfigPath, enforceNLConfig)

  }

  def createSchedule(id: String,
                     fromPeriodHour: Int,
                     fromPeriodMinute: Int,
                     toPeriodHour: Int,
                     toPeriodMinute: Int,
                     speedLimit: Int): ZkSchedule = {
    ZkSchedule(id = id,
      indicationRoadworks = ZkIndicationType.False,
      indicationDanger = ZkIndicationType.False,
      indicationActualWork = ZkIndicationType.False,
      invertDrivingDirection = ZkIndicationType.False,
      fromPeriodHour = fromPeriodHour,
      fromPeriodMinute = fromPeriodMinute,
      toPeriodHour = toPeriodHour,
      toPeriodMinute = toPeriodMinute,
      speedLimit = speedLimit,
      signSpeed = 100,
      signIndicator = false,
      speedIndicatorType = None,
      supportMsgBoard = true)
  }

}
