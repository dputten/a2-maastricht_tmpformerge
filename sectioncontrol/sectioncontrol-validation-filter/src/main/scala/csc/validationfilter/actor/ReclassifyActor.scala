package csc.validationfilter.actor

import akka.actor._
import akka.util.FiniteDuration
import akka.util.duration._
import com.github.sstone.amqp.Amqp.ChannelParameters
import com.github.sstone.amqp.{ Amqp, RabbitMQConnection }
import com.typesafe.config.Config
import csc.amqp.AmqpSerializers.Deserializer
import csc.amqp.{ GenericConsumer, PublisherActor }
import csc.sectioncontrol.messages.{ VehicleClassifiedRecord, VehicleSpeedRecord }
import csc.validationfilter.Boot
import csc.validationfilter.actor.ReclassifyConfig.{ AmqpConfig, AmqpServicesConfig }
import csc.validationfilter.actor.messages.ReclassifiedMessage

class ReclassifyActor(producer: ActorRef) extends Actor with ActorLogging {

  val state: scala.collection.mutable.Map[String, ActorRef] = scala.collection.mutable.Map.empty

  override protected def receive: Receive = {
    case msg: VehicleSpeedRecord ⇒ handleRequest(msg)
    case msg: VehicleClassifiedRecord ⇒
      log.info("Received reclassification result for speedrecord with id " + msg.speedRecord.id)
      state.get(msg.speedRecord.id) match {
        case Some(client) ⇒
          client ! ReclassifiedMessage(msg)
          state.remove(msg.speedRecord.id)
        case None ⇒ log.warning("Could not find client for speedrecord with id {}", msg.speedRecord.id)
      }
  }

  def handleRequest(req: VehicleSpeedRecord): Unit = {
    val client = sender
    producer ! req
    state.put(req.id, client)
  }

  case class State(clientMap: Map[String, ActorRef]) {
    def add(id: String, client: ActorRef): State = this.copy(clientMap = clientMap.updated(id, client))
    def remove(id: String): State = this.copy(clientMap = clientMap - id)
  }
}

object ReclassifyActor {

  def apply(actorSystem: ActorSystem, config: ReclassifyConfig, name: String): ActorRef = {

    val amqpConfig = config.amqpConnection
    val connection = RabbitMQConnection.create(amqpConfig.actorName, amqpConfig.amqpUri, amqpConfig.reconnectDelay)(actorSystem)

    val producer = connection.createChannelOwner()
    Amqp.waitForConnection(actorSystem, producer).await()

    val prod = actorSystem.actorOf(Props(new ReclassifyActorProducer(producer, config.amqpServices.producerEndpoint)))
    val actor = actorSystem.actorOf(Props(new ReclassifyActor(prod)), name)
    val listener = actorSystem.actorOf(Props(new ReclassifyActorConsumer(actor)))

    val consumer = connection.createSimpleConsumer(config.amqpServices.consumerQueue, listener, Some(ChannelParameters(100)), false)
    Amqp.waitForConnection(actorSystem, consumer).await()

    actor
  }

}

class ReclassifyActorProducer(val producer: ActorRef, val producerEndpoint: String) extends PublisherActor {

  override val ApplicationId: String = "ReclassifyActorProducer"
  override implicit val formats = Boot.jsonFormats
  override def producerRouteKey(msg: AnyRef): Option[String] = msg match {
    case msg: VehicleSpeedRecord ⇒ Some(msg.entry.lane.system + "." + msg.corridorId + "." + "VehicleSpeedRecord")
    case _                       ⇒ None
  }
}

class ReclassifyActorConsumer(handler: ActorRef) extends GenericConsumer(handler) {

  override val ApplicationId: String = "ReclassifyActorConsumer"
  override implicit val formats = Boot.jsonFormats

  override def deserializers: Map[String, Deserializer] = Map(
    "VehicleClassifiedRecord" -> toMessage[VehicleClassifiedRecord])

}

case class ReclassifyConfig(amqpConnection: AmqpConfig, amqpServices: AmqpServicesConfig)

object ReclassifyConfig {

  val rootKey = "reclassify"
  val amqpKey = "amqp-connection"
  val amqpServicesKey = "amqp-services"

  class AmqpConfig(val actorName: String, val amqpUri: String, val reconnectDelay: FiniteDuration) {
    def this(config: Config) = this(
      config.getString("actor-name"),
      config.getString("amqp-uri"),
      config.getMilliseconds("reconnect-delay").toLong.millis)
  }

  class AmqpServicesConfig(val consumerQueue: String, val producerEndpoint: String, val serversCount: Int) {
    def this(config: Config) = this(
      config.getString("consumer-queue"),
      config.getString("producer-endpoint"),
      config.getInt("servers-count"))
  }

  def apply(config: Config): ReclassifyConfig = {
    val root = config.getConfig(rootKey)
    val amqpConnection = new AmqpConfig(root.getConfig(amqpKey))
    val amqpServices = new AmqpServicesConfig(root.getConfig(amqpServicesKey))
    new ReclassifyConfig(amqpConnection, amqpServices)
  }
}
