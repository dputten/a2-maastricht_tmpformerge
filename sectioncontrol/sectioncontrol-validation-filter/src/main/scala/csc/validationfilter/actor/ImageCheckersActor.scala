package csc.validationfilter.actor

import akka.actor.{ ActorLogging, ActorRef, Props, Actor }
import csc.checkimage.{ IntradaCheckActor, CheckVehicleRegistration }
import csc.curator.utils.Curator
import csc.image.recognizer.client.{ RecognizeImage, RecognitionType }
import csc.validationfilter.actor.messages._
import csc.validationfilter.configuration.EnforceNLConfig
import csc.validationfilter.Dictionary

import akka.util.duration._

object ImageCheckersActor {

  def props(curator: Curator) = Props(new ImageCheckersActor(curator))

}

class ImageCheckersActor(curator: Curator) extends Actor with CanRegister with ActorLogging {

  val provider = ImageCheckActorProvider
  val configActor = context.system.actorSelection("/user/" + Dictionary.ZOOKEEPER_ACTOR)
  val validationFilterConfig = context.system.settings.config.getConfig("validation-filter")
  val scanInterval = validationFilterConfig.getInt("systems.scanInterVal")
  val waitTime = validationFilterConfig.getInt("systems.scanStartWaitDuration")

  context.system.scheduler.schedule(waitTime seconds, scanInterval seconds, self, GetSystems)

  override protected def receive: Receive = {
    case GetSystems ⇒
      log.info("Getting systems from Zookeeper...")
      configActor ! GetSystems
    case SystemsMessage(systems) ⇒

      log.info("Got the following systems: " + systems.mkString(","))

      val removedSystems = registeredSystemNames.diff(systems)
      log.info("Removing these unused systems: " + removedSystems.mkString(","))
      removedSystems foreach unregister

      val unregisteredSystems = systems.filterNot(isRegistered)
      unregisteredSystems.foreach(systemId ⇒ configActor ! GetEnforceConfig(systemId))

    case ConfigAndCuratorMessage(systemId, curator, config) ⇒

      log.info("RecognizeImage/IntradaCheckActor/CheckVehicleRegistration config: {}", config)
      val recognizeImage = recognizeImageActor(systemId, curator, config)
      val checkRegistrationImage = checkRegistrationImageActor(recognizeImage, config, systemId)
      val intradaChecker = intradaCheckActor(checkRegistrationImage, config, systemId)
      log.info("Registering %s with actor %s".format(systemId, intradaChecker.path.name))

      register(systemId, context.actorOf(provider.props(intradaChecker)))

    case GetActor(systemId) ⇒ sender ! RegisteredActorMessage(actor(systemId))

  }

  def verifyRecognitionType(config: EnforceNLConfig) = RecognitionType.withName(config.verifyRecognition)

  def recognizeImageActor(systemId: String, curator: Curator, config: EnforceNLConfig): ActorRef = context.actorOf(Props(new RecognizeImage(verifyRecognitionType(config), "/ctes/dacolianServer/workQueue", systemId, curator, validationFilterConfig.getString("hbaseServers"))))

  def checkRegistrationImageActor(recognizeImage: ActorRef, config: EnforceNLConfig, systemId: String): ActorRef = context.actorOf(Props(new CheckVehicleRegistration(recognizeImage,
    1.day,
    config.recognizerCountries.getOrElse(throw new UnsupportedOperationException("No recognizer countries found in config")),
    config.recognizerCountryMinConfidence.getOrElse(throw new UnsupportedOperationException("No minimal country confidence found in config")),
    config.recognizeWithARHAgain)), "checkRegistrationImage-" + systemId)

  def intradaCheckActor(checkRegistrationImage: ActorRef, config: EnforceNLConfig, systemId: String): ActorRef = context.actorOf(Props(
    new IntradaCheckActor(checkRegistrationImage, config.intradaInterfaceToUse, config.minLowestLicConfLevel)), "intradaCheckActor-" + systemId)
}
