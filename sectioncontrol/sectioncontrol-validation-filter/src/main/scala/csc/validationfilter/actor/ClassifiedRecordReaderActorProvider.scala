package csc.validationfilter.actor

import java.util.Date

import akka.actor.{ ActorLogging, Actor, Props }
import csc.validationfilter.Dictionary
import csc.validationfilter.actor.messages._
import csc.validationfilter.service._
import akka.util.duration._

object ClassifiedRecordReaderActorProvider extends SystemAwareActorProvider {
  override def props(system: String): Props = Props(new ClassifiedRecordReaderActor(system))
}

class ClassifiedRecordReaderActor(systemId: String) extends Actor with ActorLogging {

  log.info("Instantiated classifiedRecordReaderActor for system " + systemId)

  val persistentService: PersistentService = new HBasePersistentServiceImpl(systemId) with HBasePersistentService

  val configActor = context.system.actorSelection("/user/" + Dictionary.ZOOKEEPER_ACTOR)

  val fetchInterval = context.system.settings.config.getConfig("validation-filter").getInt("classifiedRecordReader.fetchInterval")

  /**
   * this must stay a def, as for each vehicleRecord we want an instance of the validationStepsController
   * @return
   */
  def validationStepsController = context.actorOf(ValidationStepsControllerActor.props)

  self ! Fetch

  override protected def receive: Receive = {
    case Fetch ⇒
      log.info("Asking completionstatus message for " + systemId)
      configActor ! GetFinishedJobsMessage(systemId)
      context.become(process())
  }

  def process(): Receive = {
    case msg: CompletionStatusMessage ⇒

      log.info("Got a " + msg.classificationCompletion + " completionstatus message for " + systemId)

      msg.classificationCompletion.foreach { completionStatus ⇒
        val startSearchingFrom = msg.validationFilterCompletion.timestamp
        val classifiedVehicleRecords = persistentService.read(startSearchingFrom, completionStatus.completeUntil)
        log.info("Searching classified records table for start {}/{} and end {}/{}", startSearchingFrom, new Date(startSearchingFrom), completionStatus.completeUntil, new Date(completionStatus.completeUntil))
        if (!classifiedVehicleRecords.isEmpty) {
          val nextStartSearchingTime = classifiedVehicleRecords.maxBy(_.time).time + 1 // plus one millisecond
          configActor ! ValidationFilterStatusMessage(nextStartSearchingTime, systemId)
        }

        log.info("completionstatus " + classifiedVehicleRecords.size + " classified records for " + systemId)
        classifiedVehicleRecords foreach (validationStepsController ! ValidateAbleVehicleMessage(_))

        if (classifiedVehicleRecords.isEmpty) {
          context.system.eventStream.publish(KeepAliveMessage(completionStatus.completeUntil, systemId))
        }
      }

      context.system.scheduler.scheduleOnce(fetchInterval minutes, self, Fetch)
      context.become(receive)
  }
}
