package csc.validationfilter.actor

import akka.actor.{ ActorLogging, Props, Actor }
import akka.util.duration._
import csc.validationfilter.Dictionary
import csc.validationfilter.actor.messages.GetSystems

object StartReadersActor {
  def props = Props(new StartReadersActor())
}

class StartReadersActor extends Actor with CanRegister with ActorLogging {

  val provider = ClassifiedRecordReaderActorProvider
  val configActor = context.system.actorSelection("/user/" + Dictionary.ZOOKEEPER_ACTOR)
  val validationFilterConfig = context.system.settings.config.getConfig("validation-filter")
  val scanInterval = validationFilterConfig.getInt("systems.scanInterVal")
  val waitTime = validationFilterConfig.getInt("systems.scanStartWaitDuration")

  context.system.scheduler.schedule(waitTime seconds, scanInterval seconds, self, GetSystems)

  override protected def receive: Receive = super.process orElse {
    case GetSystems ⇒ configActor ! GetSystems
    case ProcessingSystemsDone ⇒ if (log.isDebugEnabled) {
      log.debug("Currently the following systems are registered: " + registeredSystemNames.mkString(","))
    }
  }
}

