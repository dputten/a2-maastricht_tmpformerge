package csc.validationfilter.actor

import akka.actor._
import csc.sectioncontrol.messages.{ VehicleMetadata, ValueWithConfidence, VehicleSpeedRecord, ViolationCandidateRecord }
import csc.validationfilter.Dictionary
import csc.validationfilter.actor.messages._
import csc.validationfilter.domain.ViolationVerificationResult
import csc.validationfilter.qualify.actor.QualifyActor

object ValidationStepsControllerActor {
  def props = Props(new ValidationStepsControllerActor)
}

/**
 * this actor is the core actor for the validation process.
 * For each batch (series of vehicle passages), an instance of this class will be created. So for a configured amount of
 * records, the class will handle the process.
 *
 * ### Step 1
 * listen to validatableVehicleMessage, forward the message to a new instance of a qualifying actor
 * ### Step 2
 * Step 1 results in a PendingViolationMessage, we kill the corresponding qualifying actor and try to fetch the corresponding images of
 * the violation
 * ### Step 3
 * Do a second check with respect to image recognition (Intrada)
 * ### Step 4
 * Check the response of the image check and .... *
 *
 */

class ValidationStepsControllerActor extends Actor with ActorLogging {

  log.info("ValidationStepsController started...")

  val imageCheckersActor = context.system.actorSelection("/user/" + Dictionary.IMAGE_CHECKERS_ACTOR)

  val reclassifyActor = context.system.actorSelection("/user/" + Dictionary.RECLASSIFY_ACTOR)

  override def receive = {
    case msg: ValidateAbleVehicleMessage ⇒
      log.info("Received a validate-able vehicle message, now start qualifying...")
      qualifierActor ! msg
    case msg: ViolationCandidateMessage ⇒
      val registration = convertToRegistration(msg.record, 0)
      send(registration) //notifies processing start
      log.info("License: {}. Qualifying done, retrieving images...", msg.record.entryLicense)
      retrieveImagesActor ! msg
      context.become(receiveWithIdAndCandidate(registration.id, msg.record))
    case ReceiveTimeout ⇒
      log.error("Couldn't process further due to Received Timeout from.  " + sender.path)
      context.stop(self)
    case ValidationFinished(false) ⇒
      log.info("ValidationStepsController work done, no violation detected.")
      context.stop(self)

  }

  def receiveWithIdAndCandidate(id: String, candidate: ViolationCandidateRecord): Receive = {
    case ViolationCandidateImageStatusMessage(hasImages) ⇒
      if (hasImages) {
        log.info("License: {}. Image results available, preparing Intrada check...", candidate.entryLicense)
        imageCheckersActor ! GetActor(candidate.systemId)
      } else {
        log.info("License: {}. Images are not available ", candidate.entryLicense)
        self ! ValidationFinished(success = false)
      }

    case RegisteredActorMessage(imageCheckActorOption) ⇒
      log.info("License: {}. Intrada: found a imageCheckActor, trying to get check the images and process the results", candidate.entryLicense)

      imageCheckActorOption.foreach(_ ! ViolationCandidateMessage(candidate))

    case msg: ImageProcessorResultMessage ⇒
      log.info("License: {}. Image check and processing finished. Now handling ViolationVerificationResult status.", candidate.entryLicense)
      sender ! PoisonPill // we kill the IntradaProcessorActor as it is not a child of this actor

      val violationRecord = candidate.vehicleViolationRecord.addRecognizeResults(msg.summary.recognizedBy.getOrElse("Unknown"), true, msg.summary)
      val violationRecordIntrada = msg.intrada.map(intrada ⇒ violationRecord.addRecognizeResults(intrada.recognizedBy.getOrElse("Unknown"), false, intrada))

      val corrected = candidate.copy(vehicleViolationRecord = violationRecordIntrada.getOrElse(violationRecord))

      msg.violationVerificationResult match {
        case ViolationVerificationResult.OK ⇒
          log.info("License: {} OK!", candidate.entryLicense)
          updateStateWriterActor ! ViolationCandidateMessage(corrected)

        // When VerificationResult.Invalid sends success=true  is this correct????
        // Yes this is correct. Success signals that the process has succesful enden.
        case ViolationVerificationResult.Invalid ⇒
          log.info("License: {}. Registration with invalid.", candidate.entryLicense)
          self ! ValidationFinished(success = true)
        case ViolationVerificationResult.ReClassify ⇒
          log.info("License: {}. Reclassify violation", candidate.entryLicense)
          val changedSpeedRecord = speedRecord(msg, candidate)
          reclassifyActor ! changedSpeedRecord //send to reclassify
      }
      //update the current state
      context.become(receiveWithIdAndCandidate(id, corrected))

    case ReclassifiedMessage(record) ⇒
      val violationRecord = candidate.vehicleViolationRecord.copy(classifiedRecord = record)
      val corrected = candidate.copy(vehicleViolationRecord = violationRecord)
      log.info("License: {}. Got a reclassified response (License might deviate!).", corrected.entryLicense)
      updateStateWriterActor ! ViolationCandidateMessage(corrected)
      context.become(receiveWithIdAndCandidate(id, corrected))

    case ValidationFinished(success) ⇒
      log.info("License: {}. ValidationStepsController work done, calling stop(self)", candidate.entryLicense)
      if (success) {
        val registration = convertToRegistration(candidate, now)
        send(registration.copy(id = id)) //notifies processing end
      }
      context.stop(self)
  }

  /**
   * The actor that performs the actual qualifying
   */
  val qualifierActor = context.actorOf(QualifyActor.props)

  val updateStateWriterActor = context.actorOf(UpdateStateWriterActor.props)

  val retrieveImagesActor = context.actorOf(RetrieveImagesActor.props)

  def send(msg: ProcessedRegistrationMessage) = {
    log.info("Publishing message to eventstream: " + msg)
    context.system.eventStream.publish(msg)
  }

  def convertToRegistration(record: ViolationCandidateRecord, end: Long): ProcessedRegistrationMessage =
    ProcessedRegistrationMessage(
      record.vehicleViolationRecord.classifiedRecord.id,
      record.vehicleViolationRecord.classifiedRecord.time,
      record.timestamp, //start processing time
      end,
      record.systemId)

  protected def now: Long = System.currentTimeMillis()

  def getLicense(license: Option[ValueWithConfidence[String]]): String = license.map(_.value).getOrElse("")

  def getLicenseToUseForRaw(msg: ImageProcessorResultMessage, record: ViolationCandidateRecord): Option[String] = {
    val arhEntryLicense = getLicense(record.vehicleViolationRecord.classifiedRecord.speedRecord.entry.license)
    val intradaLicense = getLicense(msg.summary.license)
    val licenseToUseForRaw =
      if (arhEntryLicense == intradaLicense) {
        // This is the normal case so only debug logging.
        log.debug("Arh and Intrada return the same license:")
        log.debug("Arh will be used for entry.rawLicense:" + record.vehicleViolationRecord.classifiedRecord.speedRecord.entry.rawLicense.getOrElse(""))
        log.debug("Arh will be used for exit.rawLicense:" + record.vehicleViolationRecord.classifiedRecord.speedRecord.exit.get.rawLicense.getOrElse(""))
        record.vehicleViolationRecord.classifiedRecord.speedRecord.entry.rawLicense
      } else {
        // This should be a more exceptional case so info logging.
        log.info("Arh and Intrada return a different license. Intrada will be used for entry and exit.")
        log.info("Intrada will be used for entry.rawLicense:" + intradaLicense)
        log.info("Intrada will be used for exit.rawLicense:" + intradaLicense)
        Some(intradaLicense)
      }

    licenseToUseForRaw
  }

  def createNewEntry(msg: ImageProcessorResultMessage, record: ViolationCandidateRecord, licenseToUseForRaw: Option[String]): VehicleMetadata = {
    val entryLicensePlateData = record.vehicleViolationRecord.classifiedRecord.speedRecord.entry.licensePlateData match {
      case Some(eld) ⇒ Some(eld.copy(license = msg.summary.license, country = msg.summary.country, recognizedBy = Some(VehicleMetadata.Recognizer.INTRADA), rawLicense = licenseToUseForRaw))
      case _         ⇒ None
    }
    log.debug("entryLicensePlateData:" + entryLicensePlateData)
    record.vehicleViolationRecord.classifiedRecord.speedRecord.entry.copy(license = msg.summary.license, country = msg.summary.country, rawLicense =
      licenseToUseForRaw, recognizedBy = Some(VehicleMetadata.Recognizer.INTRADA), licensePlateData = entryLicensePlateData)
  }

  def createNewExit(msg: ImageProcessorResultMessage, record: ViolationCandidateRecord, licenseToUseForRaw: Option[String]): Option[VehicleMetadata] = {
    val exitLicensePlateData = record.vehicleViolationRecord.classifiedRecord.speedRecord.exit match {
      case Some(exit) ⇒ exit.licensePlateData match {
        case Some(eld) ⇒ Some(eld.copy(license = msg.summary.license, country = msg.summary.country, recognizedBy = Some(VehicleMetadata.Recognizer.INTRADA), rawLicense = licenseToUseForRaw))
        case _         ⇒ None
      }
      case _ ⇒ None
    }
    log.debug("exitLicensePlateData:" + exitLicensePlateData)

    record.vehicleViolationRecord.classifiedRecord.speedRecord.exit.map(_.copy(license = msg.summary.license, country = msg.summary.country,
      rawLicense = licenseToUseForRaw, recognizedBy = Some(VehicleMetadata.Recognizer.INTRADA), licensePlateData = exitLicensePlateData))
  }

  /**
   * Changes the speedrecord. Takes the returned license and or country in case of a re-classify status and updates the current
   * speedrecord with those new values as input for the re-classification
   * @param record
   * @param msg
   */
  def speedRecord(msg: ImageProcessorResultMessage, record: ViolationCandidateRecord): VehicleSpeedRecord = {

    try {
      log.debug("record.vehicleViolationRecord.classifiedRecord.speedRecord.exit: " +
        record.vehicleViolationRecord.classifiedRecord.speedRecord.exit.get)
      log.debug("msg.license: " + msg.summary.license)
    } catch {
      case e: Exception ⇒ log.error("No license found!")
    }

    val licenseToUseForRaw = getLicenseToUseForRaw(msg, record)
    val newEntry = createNewEntry(msg, record, licenseToUseForRaw)
    val newExit = createNewExit(msg, record, licenseToUseForRaw)

    log.debug("newEntry:" + newEntry)
    log.debug("newExit:" + newExit)

    record.vehicleViolationRecord.classifiedRecord.speedRecord.copy(entry = newEntry, exit = newExit)
  }
}
