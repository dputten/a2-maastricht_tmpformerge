package csc.validationfilter.actor

import akka.actor.{ Actor, ActorLogging, ActorRef, Props }
import csc.checkimage._
import csc.sectioncontrol.enforce.common.nl.violations.IntradaCheck
import csc.sectioncontrol.messages.{ VehicleViolationRecord, ViolationCandidateRecord }
import csc.validationfilter.actor.messages.{ ValidationFinished, ViolationCandidateMessage, ViolationAndIntradaResult }

object ImageCheckActorProvider extends SystemAwareActorProvider {
  override def props(system: String): Props = Props()
  def props(actorRef: ActorRef): Props = Props(new ImageCheckActor(actorRef))
}

/**
 * For each system an ImageCheckActor is created, the ImageCheckersActor (plural!) is the one responsible for creating these instances.
 * So in other actors by means of the ImageCheckersActor a handle to a ImageCheckActor can be retrieved, based on the system.
 * (as the image check process uses the system internally). So when we are in this actor all we know is that a request for some system to check an image
 * is done. In order to be able to give the response to the calling actor, this actor stores the sender for a given VVR (vehicle violation record)
 *
 * Afterwards, the reference is removed again.
 *
 */

class ImageCheckActor(intradaCheckActor: ActorRef) extends Actor with ActorLogging {

  case class PendingViolationAndActor(record: ViolationCandidateRecord, actorRef: ActorRef)

  var pendingViolationAndActorsByKey = scala.collection.mutable.Map[String, PendingViolationAndActor]()

  override def receive = {
    case msg: ViolationCandidateMessage ⇒

      val vvr = msg.record.vehicleViolationRecord
      val speedRecord = vvr.classifiedRecord.speedRecord
      val key = createKey(vvr)
      pendingViolationAndActorsByKey += (key -> PendingViolationAndActor(msg.record, sender))

      speedRecord.exit match {
        case Some(exit) ⇒ intradaCheckActor ! IntradaImageCheckRequest(key, IntradaCheck(vvr))
        case None ⇒
          log.error("No exit entry defined for vvr record with ID: " + vvr.id)
          sender ! ValidationFinished(false)
      }

    case msg: IntradaImageCheckResponse ⇒

      log.info("Got IntradaImageCheckResponse with ref " + msg.reference)

      pendingViolationAndActorsByKey.get(msg.reference) match {
        case Some(pendingViolationAndActor) ⇒
          /*
           fire and forget. When we are here we got a response from the IntradaCheckActor .now we get the handle of the original validationStepsController actor back
           and pass the pendingViolationRecord and the intrada check result to a intradaProcessor(Actor).
           In the intradaprocessor, results are send back to the validationstepscontroller, and THERE the instance of the used intradaProcessorActor (which is created HERE)
           is killed.
           */

          intradaProcessorActor(pendingViolationAndActor.actorRef, ViolationAndIntradaResult(pendingViolationAndActor.record, msg.result))
          pendingViolationAndActorsByKey -= msg.reference
        case None ⇒ log.error("Somehow the calling actor ref (from PendingViolationAndActor local state) has disappeared, " +
          "unable to process further. Current reference = " + msg.reference + "Validation-filter module probably needs restart.")

      }
  }

  private def createKey(vvr: VehicleViolationRecord): String = {
    def time = vvr.classifiedRecord.speedRecord.exit.getOrElse(vvr.classifiedRecord.speedRecord.entry).eventTimestamp
    def license = vvr.classifiedRecord.speedRecord.entry.license.map(_.value).getOrElse("")
    "%s%d".format(license, time)
  }

  def intradaProcessorActor(sendTo: ActorRef, input: ViolationAndIntradaResult) = context.actorOf(IntradaProcessorActor.props(sendTo, input))
}
