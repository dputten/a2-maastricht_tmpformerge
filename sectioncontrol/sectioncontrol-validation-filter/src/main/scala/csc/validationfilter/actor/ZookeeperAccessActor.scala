package csc.validationfilter.actor

import java.util.Date

import akka.actor.{ ActorLogging, Props, Actor }
import csc.config.Path
import csc.curator.utils.{ Versioned, Curator }
import csc.sectioncontrol.classify.nl.ClassificationCompletionStatus
import csc.sectioncontrol.jobs.{ ValidationFilterCompletionStatus, ProcessedRegistrationCompletionStatus }
import csc.sectioncontrol.storage.{ Paths }
import csc.sectioncontrol.storagelayer.jobs.{ ValidationFilterCompletionStatusService, ProcessedRegistrationCompletionStatusService }
import csc.validationfilter.{ ConfigHelper, Dictionary }
import csc.validationfilter.actor.messages._
import csc.validationfilter.configuration.{ EnforceNLConfig, LoadSystemConfiguration }
import org.joda.time.DateTime

object ZookeeperAccessActor {
  def props(curator: Curator) = Props(new ZookeeperAccessActor(curator))
}

class ZookeeperAccessActor(curator: Curator) extends Actor with ActorLogging {

  override protected def receive: Receive = {
    case GetSystems                                         ⇒ sender ! SystemsMessage(curator.getChildNames(Paths.Systems.getDefaultPath).toList)
    case GetConfigMessage(system)                           ⇒ sender ! ConfigMessage(LoadSystemConfiguration(system, Dictionary.timeZone, curator))
    case GetFinishedJobsMessage(system)                     ⇒ sender ! CompletionStatusMessage(classificationCompletionStatus(system), ValidationFilterStatusMessage(getValidationFilterStatus(system).completeUntil, system))
    case GetEnforceConfig(system)                           ⇒ sender ! ConfigAndCuratorMessage(system, curator, enforceConfig(system))
    case GetCurator                                         ⇒ sender ! CuratorMessage(curator)
    case ValidationFilterStatusMessage(timestamp, system)   ⇒ updateValidationFilterCompletionStatus(timestamp, system)
    case UpdateProcessRegistrationStatus(timestamp, system) ⇒ updateProcessRegistrationStatus(timestamp, system)
  }

  def classificationCompletionStatus(system: String): Option[ClassificationCompletionStatus] = {
    curator.getVersioned[ClassificationCompletionStatus](Paths.Systems.getClassificationCompletionStatusPath(system)) match {
      case Some(Versioned(completionStatus, _)) ⇒ Some(completionStatus)
      case None                                 ⇒ None
    }
  }

  def enforceConfig(systemId: String): EnforceNLConfig = {
    val path = Path(Paths.Systems.getSystemJobsPath(systemId)) / "enforceConfig-nl"
    curator.get[EnforceNLConfig](Seq(ConfigHelper.globalEnforceConfigPath, path), ConfigHelper.defaultConfig).get //' cause of the default config we can call get here
  }

  def processRegistrationStatusService() = new ProcessedRegistrationCompletionStatusService(curator)

  def validationFilterStatusService() = new ValidationFilterCompletionStatusService(curator)

  def getValidationFilterStatus(system: String): ValidationFilterCompletionStatus = {

    def aDayAgo() = new DateTime(System.currentTimeMillis()).minusDays(1).getMillis
    validationFilterStatusService().get(system).getOrElse(ValidationFilterCompletionStatus(System.currentTimeMillis(), aDayAgo()))
  }

  def updateValidationFilterCompletionStatus(timestamp: Long, systemId: String) = {
    val validationFilterCompletionStatusService = validationFilterStatusService()
    val newStatus = ValidationFilterCompletionStatus(System.currentTimeMillis(), timestamp)
    validationFilterCompletionStatusService.exists(systemId) match {
      case true ⇒
        val savedStatus = validationFilterCompletionStatusService.getVersioned(systemId)
        if (timestamp > savedStatus.get.data.completeUntil) {
          log.info("Updated timestamp in zookeeper for validationFilterStatus with " + new Date(timestamp))
          validationFilterCompletionStatusService.update(systemId, newStatus, savedStatus.get.version)
        }
      case false ⇒
        validationFilterCompletionStatusService.create(systemId, newStatus)
    }
  }

  def updateProcessRegistrationStatus(lastProcessedTimestamp: Long, systemId: String) {
    val processedRegistrationCompletionStatusService = processRegistrationStatusService()
    val newStatus = ProcessedRegistrationCompletionStatus(System.currentTimeMillis(), lastProcessedTimestamp)
    processedRegistrationCompletionStatusService.exists(systemId) match {
      case true ⇒
        val savedStatus = processedRegistrationCompletionStatusService.getVersioned(systemId)
        log.info("Determining if we need to update processedRegistrationCompletionStatus node in zookeeper...")
        if (lastProcessedTimestamp > savedStatus.get.data.completeUntil) {
          // Should always be the case
          log.info("Updating processedRegistrationCompletionStatus node in zookeeper with " + new Date(lastProcessedTimestamp))
          processedRegistrationCompletionStatusService.update(systemId, newStatus, savedStatus.get.version)
        } else {
          log.info("Got update timestamp earlier than current, not saving time " + new Date(lastProcessedTimestamp))
        }
      case false ⇒
        log.info("Creating processedRegistrationCompletionStatus node in zookeeper")
        processedRegistrationCompletionStatusService.create(systemId, newStatus)
    }
  }

}
