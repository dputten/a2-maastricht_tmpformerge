package csc.validationfilter.actor.messages

import csc.akkautils.DirectLogging

case class WakeupMessage(systemId: String)

case class KeepAliveMessage(completedUntil: Long, systemId: String)

case class ProcessedRegistrationTimeFrameMessage(endTime: Long, systemId: String)

case class ProcessedRegistrationMessage(id: String, eventTimestamp: Long, beginProcessing: Long, endProcessing: Long, systemId: String) extends DirectLogging {
  def isCompleted = endProcessing > 0

  def hasTimedOut(timeoutInSeconds: Long, pointInTime: Long) = pointInTime - beginProcessing >= (timeoutInSeconds * 1000)

}

object ProcessedRegistrationMessage {
  implicit val registrationOrdering = new Ordering[ProcessedRegistrationMessage] {
    override def compare(r1: ProcessedRegistrationMessage, r2: ProcessedRegistrationMessage): Int =
      if (r1.eventTimestamp < r2.eventTimestamp) -1 else if (r1.eventTimestamp > r2.eventTimestamp) 1 else 0
  }
}
