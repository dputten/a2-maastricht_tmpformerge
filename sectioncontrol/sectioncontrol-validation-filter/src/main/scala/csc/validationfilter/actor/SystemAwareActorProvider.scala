package csc.validationfilter.actor

import akka.actor.Actor.Receive
import akka.actor._
import csc.validationfilter.actor.messages.SystemsMessage

case object ProcessingSystemsDone

trait SystemAwareActorProvider {

  def props(system: String): Props
}

trait CanRegister extends Actor with ActorLogging {

  val provider: SystemAwareActorProvider

  case class RegisteredActor(system: String, actor: ActorRef)

  var registeredActors = scala.collection.mutable.MutableList[RegisteredActor]()

  def isRegistered(system: String): Boolean = registeredActors.exists(_.system == system)

  def register(system: String)(implicit context: ActorContext): Unit = {
    if (!isRegistered(system)) registeredActors = registeredActors :+ RegisteredActor(system, context.actorOf(provider.props(system)))
  }

  def register(system: String, actor: ActorRef): Unit = {
    if (!isRegistered(system)) registeredActors = registeredActors :+ RegisteredActor(system, actor)
  }

  def unregister(system: String): Unit = {
    registeredActors = registeredActors.filterNot(_.system == system)
  }

  def registeredSystemNames = registeredActors.map(_.system).toList

  def actor(system: String) = registeredActors.find(_.system == system).map(_.actor)

  def process(implicit context: ActorContext): Receive = {
    case msg: SystemsMessage ⇒

      val removedSystems = registeredSystemNames.diff(msg.systems)
      removedSystems foreach unregister

      msg.systems foreach register

      sender ! ProcessingSystemsDone
  }

}