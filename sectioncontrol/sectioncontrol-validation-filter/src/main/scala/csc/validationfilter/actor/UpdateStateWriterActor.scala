package csc.validationfilter.actor

import akka.actor.{ ActorLogging, Actor, Props }

import csc.validationfilter.actor.messages.{ ValidationFinished, ViolationCandidateMessage }
import csc.validationfilter.service.{ HBasePersistentService, HBasePersistentServiceImpl, PersistentService }

object UpdateStateWriterActor {
  def props = Props(new UpdateStateWriterActor())
  val SYSTEM_AGNOSTIC = "System_agnostic" //meaning the persistentservice cannot use the Reader capabilities! (Actually it can but it won't find any..)
}

class UpdateStateWriterActor extends Actor with ActorLogging {

  val persistentService: PersistentService = new HBasePersistentServiceImpl(UpdateStateWriterActor.SYSTEM_AGNOSTIC) with HBasePersistentService

  override protected def receive: Receive = {
    case ViolationCandidateMessage(record) ⇒
      persistentService.store(record)
      log.info("License: {}. Stored confirmedViolation record", record.entryLicense)
      sender ! ValidationFinished(true)
  }
}
