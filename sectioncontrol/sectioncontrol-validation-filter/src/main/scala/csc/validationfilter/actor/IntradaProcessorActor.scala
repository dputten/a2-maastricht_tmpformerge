package csc.validationfilter.actor

import akka.actor.{ Actor, ActorLogging, ActorRef, Props }
import csc.akkautils.DirectLogging
import csc.checkimage.{ ImageCheckResult, IntradaCheckResult, RecognitionResult }
import csc.sectioncontrol.messages.{ LicensePlateData, ValueWithConfidence, VehicleImageType, VehicleMetadata }
import csc.validationfilter.Dictionary
import csc.validationfilter.actor.messages.{ ConfigAndCuratorMessage, GetEnforceConfig, ImageProcessorResultMessage, ViolationAndIntradaResult }
import csc.validationfilter.domain.ViolationVerificationResult

object IntradaProcessorActor {
  def props(sendTo: ActorRef, input: ViolationAndIntradaResult) = Props(new IntradaProcessorActor(sendTo, input))
}

class IntradaProcessorActor(validationStepsController: ActorRef, input: ViolationAndIntradaResult) extends Actor with ActorLogging {

  def configActor = context.system.actorSelection("/user/" + Dictionary.ZOOKEEPER_ACTOR)

  val systemId = input.record.vehicleViolationRecord.classifiedRecord.speedRecord.entry.lane.system

  configActor ! GetEnforceConfig(systemId)

  override def receive = {
    case ConfigAndCuratorMessage(_, _, enforceConfig) ⇒
      enforceConfig.recognizerCountries match {
        case None ⇒ log.error("No recognizer countries defined for system " + systemId)
        case Some(countries) ⇒

          val (violationVerificationResult, license, country) = getViolationVerificationResult(input.intradaCheckResult, countries, enforceConfig.autoThresholdLevel, enforceConfig.mobiThresholdLevel, input.record.entryLicense)

          val sum = createSumarizedLicensePlateData(enforceConfig.verifyRecognition, license, country)
          val intrada = createIntradaLicensePlateData(input.intradaCheckResult, enforceConfig.verifyRecognition)
          validationStepsController ! ImageProcessorResultMessage(finalViolationVerificationResult(violationVerificationResult), sum, intrada)
      }
  }

  def createSumarizedLicensePlateData(sourceImageType: String,
                                      license: Option[ValueWithConfidence[String]],
                                      country: Option[ValueWithConfidence[String]]): LicensePlateData = {
    val imageType = try {
      Some(VehicleImageType.withName(sourceImageType))
    } catch { case t: Throwable ⇒ None }

    LicensePlateData(license = license,
      licensePosition = None,
      sourceImageType = imageType,
      rawLicense = None,
      licenseType = None,
      recognizedBy = Some("SUMMARY"),
      country = country)
  }
  def createIntradaLicensePlateData(intradaCheckResult: IntradaCheckResult, sourceImageType: String): Option[LicensePlateData] = {
    var entryRecognitionResults = intradaCheckResult.entry.map(_.getRecognitionResults())
    val entryINTRADA: Option[RecognitionResult] = entryRecognitionResults.flatMap(_.find(_.recognizerId == "INTRADA_INTERFACE_2"))
    entryINTRADA.map(intrada ⇒ {
      val imageType = try {
        Some(VehicleImageType.withName(sourceImageType))
      } catch {
        case t: Throwable ⇒ None
      }
      LicensePlateData(license = intrada.license,
        licensePosition = None,
        sourceImageType = imageType,
        rawLicense = intrada.rawLicense,
        licenseType = None,
        recognizedBy = Some(VehicleMetadata.Recognizer.INTRADA),
        country = intrada.country)
    })
  }

  def getViolationVerificationResult(intradaCheckResult: IntradaCheckResult, countries: Seq[String], autoTreshHoldLevel: Int, mobiTreshHoldLevel: Int, entryLicense: String): (ViolationVerificationResult.Value, Option[ValueWithConfidence[String]], Option[ValueWithConfidence[String]]) = {
    val (imageCheckResult, license, country) = intradaCheckResult.createSummaryWithIntrada2(countries, autoTreshHoldLevel, mobiTreshHoldLevel)
    (ViolationStatus.determine(imageCheckResult, entryLicense), license, country)
  }

  def finalViolationVerificationResult(violationVerificationResult: ViolationVerificationResult.Value) = violationVerificationResult match {
    case ViolationVerificationResult.OK      ⇒ violationVerificationResult
    case ViolationVerificationResult.Invalid ⇒ violationVerificationResult
    case _                                   ⇒ ViolationVerificationResult.ReClassify // if we're not ok, or totally invalid, we might need some re-classification
  }
}

object ViolationStatus extends DirectLogging {

  def determine(result: ImageCheckResult, entryLicense: String) = {

    if (result.hasErrors()) {

      if (result.hasErrorCheckFailed()) {
        log.info("Manual Indication: license=%s reason=License Check license failed %s".format(entryLicense, result))
        ViolationVerificationResult.ManualLicence
      } else if (result.hasLicenseFormatCheckFailed()) {
        log.debug("Manual Indication: license=%s reason=License format incorrect %s".format(entryLicense, result))
        ViolationVerificationResult.ManualLicenceFormat
      } else if (result.hasLicenseCheckFailed()) {
        log.info("Manual Indication: license=%s reason=License Check different license found %s".format(entryLicense, result))
        ViolationVerificationResult.ManualLicence
      } else if (result.hasCountryCheckFailed()) {
        log.info("Manual Indication: license=%s reason=Country Check invalid country %s".format(entryLicense, result))
        ViolationVerificationResult.ManualCountry
      } else if (result.hasErrorMatchingCheckFailed()) {
        log.info("Invalid violation indication: license=%s reason=Intrada sure its other license".format(entryLicense))
        ViolationVerificationResult.Invalid
      } else if (result.hasCountryChange()) {
        log.info("Country Change indication for license=%s".format(entryLicense))

        if (result.hasChangeToMobi()) {
          log.info("Change to mobi indication for license=%s".format(entryLicense))
          ViolationVerificationResult.ChangeToMobiAndCountry
        } else if (result.hasChangeToAuto()) {
          log.info("Change to auto indication for license=%s".format(entryLicense))
          ViolationVerificationResult.ChangeToAutoAndCountry
        } else {
          ViolationVerificationResult.ChangeCountry
        }
      } else {
        log.info("There are no specific error situations")
        ViolationVerificationResult.OK
      }

    } else {
      ViolationVerificationResult.OK
    }
  }
}

