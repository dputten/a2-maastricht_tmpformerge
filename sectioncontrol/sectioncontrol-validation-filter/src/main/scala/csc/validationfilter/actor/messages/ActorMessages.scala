package csc.validationfilter.actor.messages

import akka.actor.ActorRef
import csc.checkimage.IntradaCheckResult
import csc.curator.utils.Curator
import csc.sectioncontrol.classify.nl.ClassificationCompletionStatus
import csc.sectioncontrol.enforce.common.nl.violations.ViolationDeterminationConfigData
import csc.sectioncontrol.messages.{ LicensePlateData, ValueWithConfidence, VehicleClassifiedRecord, ViolationCandidateRecord }
import csc.validationfilter.configuration.EnforceNLConfig
import csc.validationfilter.domain.ViolationVerificationResult

case class ValidateAbleVehicleMessage(vehicleClassifiedRecord: VehicleClassifiedRecord)
case class ViolationCandidateMessage(record: ViolationCandidateRecord)
case class ViolationCandidateImageStatusMessage(hasImages: Boolean)

case class GetConfigMessage(systemId: String)
case class ConfigMessage(config: ViolationDeterminationConfigData)

case object GetSystems
case class SystemsMessage(systems: List[String])

case class GetFinishedJobsMessage(systemId: String)
case class CompletionStatusMessage(classificationCompletion: Option[ClassificationCompletionStatus], validationFilterCompletion: ValidationFilterStatusMessage)

case class GetEnforceConfig(system: String)
case class ConfigAndCuratorMessage(system: String, curator: Curator, config: EnforceNLConfig)

case object GetCurator
case class CuratorMessage(curator: Curator)

case class UpdateProcessRegistrationStatus(timestamp: Long, systemId: String)

case class ValidationFilterStatusMessage(timestamp: Long, systemId: String)

case class GetActor(systemId: String)
case class RegisteredActorMessage(actor: Option[ActorRef])

case class ViolationAndIntradaResult(record: ViolationCandidateRecord, intradaCheckResult: IntradaCheckResult)

case class ImageProcessorResultMessage(violationVerificationResult: ViolationVerificationResult.Value, summary: LicensePlateData, intrada: Option[LicensePlateData])

case class ReclassifiedMessage(record: VehicleClassifiedRecord)

case class ValidationFinished(success: Boolean)

case object SetReaders

case object Fetch

