package csc.validationfilter.actor

import akka.actor.{ ActorLogging, Props, ActorRef, Actor }
import csc.imageretriever.{ GetImageResponse, GetImageRequest, ImageKey }
import csc.sectioncontrol.messages.{ ViolationCandidateRecord, VehicleImageType, VehicleMetadata }
import csc.validationfilter.Dictionary
import csc.validationfilter.actor.messages.{ ViolationCandidateMessage, ViolationCandidateImageStatusMessage }
import java.util.UUID
/**
 * Actor that receives a PendingViolationMessage, checks/obtains the required images, and sends back a
 * PendingViolationImageStatusMessage.
 * This actor deals with a single request, and will stop after the work is done
 *
 * Created by carlos on 13.08.15.
 */
class RetrieveImagesActor extends Actor with ActorLogging {

  val imageRetrieverActor = context.system.actorSelection("/user/" + Dictionary.IMAGE_RETRIEVER_ACTOR)

  override protected def receive: Receive = {
    case ViolationCandidateMessage(record) ⇒ handleRequest(record)
  }

  def handleRequest(record: ViolationCandidateRecord): Unit = {

    val client = sender
    val rec = record.vehicleViolationRecord.classifiedRecord.speedRecord
    val entries: List[VehicleMetadata] = List(rec.entry) ++ rec.exit //gets all the VehicleMetadata entries

    val state = State(client, Nil, entries.size)
    val requests = for (e ← entries; r ← createImageRequest(e)) yield r //for each, create an image request
    log.info("images to retrieve: ({}) {}", requests.size, requests)

    requests foreach (imageRetrieverActor ! _) //send all the requests

    context.become(imageResponseReceive(state)) //now waiting for image responses
  }

  def imageResponseReceive(state: State): Receive = {
    case msg: GetImageResponse ⇒ handleImageResponse(state, msg)
    case x                     ⇒ log.info("Illegal answer retrieved: " + x)
  }

  def handleImageResponse(state: State, msg: GetImageResponse): Unit = {
    val responses = state.responses :+ msg //adds the response to the list

    if (responses.size == state.expectedResponseCount) { //we have all the expected responses
      val bothImagesAvailable = responses.size == 2 && responses.forall(_.contents.isDefined)
      state.client ! ViolationCandidateImageStatusMessage(bothImagesAvailable)
      context.stop(self) //work is done
    } else {
      context.become(imageResponseReceive(state.copy(responses = responses))) //we wait for more
    }
  }

  def createImageRequest(md: VehicleMetadata): Option[GetImageRequest] = {
    val imageType = VehicleImageType.Overview
    md.getImage(imageType) map { image ⇒
      val imageKey = ImageKey(image.uri, image.timestamp, imageType, md.lane.system, md.lane.gantry, md.lane.name)
      GetImageRequest(UUID.randomUUID().toString, imageKey)
    }
  }

  /**
   * Contains all the state of this Actor
   * @param client actor to return the final response to
   * @param responses the image responses we got so far
   * @param expectedResponseCount the amount of image responses we are waiting for
   */
  case class State(client: ActorRef,
                   responses: List[GetImageResponse],
                   expectedResponseCount: Int)

}

object RetrieveImagesActor {
  def props: Props = Props(new RetrieveImagesActor())
}