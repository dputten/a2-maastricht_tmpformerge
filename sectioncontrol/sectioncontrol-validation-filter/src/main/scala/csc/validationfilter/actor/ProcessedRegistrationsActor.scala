package csc.validationfilter.actor

import akka.actor.{ Props, Actor, ActorLogging }
import csc.validationfilter.{ ProcessedRegistrationCompletionConfiguration, Dictionary }
import csc.validationfilter.actor.messages.{ KeepAliveMessage, UpdateProcessRegistrationStatus, ProcessedRegistrationMessage, WakeupMessage }
import akka.util.duration._

import scala.collection.immutable.SortedSet
import csc.akkautils.DirectLogging

object ProcessedRegistrationsActor extends DirectLogging {

  def props(systemId: String) = Props(new ProcessedRegistrationsActor(systemId))

  def discardCompletedMessagesUntilLastProcessed(processedRegistrationMessages: SortedSet[ProcessedRegistrationMessage], now: Long, timeoutInSeconds: Long): SortedSet[ProcessedRegistrationMessage] = {

    def timedOut(msg: ProcessedRegistrationMessage): Boolean = {
      val timedOut = msg.hasTimedOut(timeoutInSeconds, now)
      if (timedOut) log.info("Dropped {} due to timeout", msg)
      timedOut
    }

    processedRegistrationMessages
      .filterNot(processedRegistrationMessage ⇒ timedOut(processedRegistrationMessage))
      .dropWhile(_.isCompleted)
  }

  def isTheFirstMessageAfterTheTimeOut_Completed(processedRegistrationMessages: SortedSet[ProcessedRegistrationMessage],
                                                 now: Long, timeoutInSeconds: Long): Boolean = {
    processedRegistrationMessages
      .filterNot(_.hasTimedOut(timeoutInSeconds, now))
      .toList
      .headOption match {
        case Some(processedRegistrationMessage) ⇒ processedRegistrationMessage.isCompleted
        case None                               ⇒ false
      }
  }

  def getCompletedUntilTime(processedRegistrationMessages: SortedSet[ProcessedRegistrationMessage],
                            now: Long, timeoutInSeconds: Long): Option[Long] = {

    val lastMessage = processedRegistrationMessages.filterNot(_.hasTimedOut(timeoutInSeconds, now)).isEmpty match {
      case true ⇒ processedRegistrationMessages.lastOption
      case false ⇒
        if (isTheFirstMessageAfterTheTimeOut_Completed(processedRegistrationMessages, now, timeoutInSeconds))
          processedRegistrationMessages
            .filterNot(_.hasTimedOut(timeoutInSeconds, now))
            .toList
            .takeWhile(_.isCompleted)
            .lastOption
        else
          processedRegistrationMessages.filter(_.hasTimedOut(timeoutInSeconds, now)).toList.lastOption
    }

    lastMessage match {
      case Some(processedRegistrationMessage) ⇒ Some(processedRegistrationMessage.eventTimestamp)
      case _                                  ⇒ None
    }
  }
}

class ProcessedRegistrationsActor(systemId: String) extends Actor with ActorLogging {

  val storageActor = context.system.actorSelection("/user/" + Dictionary.ZOOKEEPER_ACTOR)

  val config = {
    val conf = context.system.settings.config.getConfig("validation-filter.processedRegistrationCompletion")
    ProcessedRegistrationCompletionConfiguration(conf)
  }
  log.debug("Timeout = %d seconds".format(config.timeoutInSeconds))

  /**
   * Registrations ordered by event time, i.e. first vehicle to pass the exit, first vehicle in the list
   */
  var processedRegistrationMessages = SortedSet.empty[ProcessedRegistrationMessage]

  override def preStart() {
    super.preStart()
    context.system.eventStream.subscribe(self, classOf[ProcessedRegistrationMessage])
    context.system.eventStream.subscribe(self, classOf[KeepAliveMessage])
    context.system.scheduler.schedule(config.initialDelay minute, config.frequency minute, self, WakeupMessage(systemId))
  }

  // if we get a keepalive message,but there are still registrations in the list, we wait for those  and do not store the
  // keep alive, otherwise keepalive will be used
  def canStoreKeepAliveTimestamp = processedRegistrationMessages.isEmpty

  /**
   * Handle the following message types
   *
   * - ProcessedRegistrationMessage: Message that indicates the start or completion of pre-qualification
   * - KeepAlive message: checking how far we got until now in the whole chain
   * - WakeupMessage: This actor works batch oriented. The WakeupMessage triggers this actor to check if
   * pre-qualify has progressed since we last checked and we can update the completed until status
   */
  def receive = {
    case KeepAliveMessage(completedUntil, forSystem) if systemId == forSystem ⇒
      if (canStoreKeepAliveTimestamp) {
        log.info("Storing keep alive timestamp, no work in progress currently")
        saveProcessedRegistrationCompletedUntil(completedUntil)
      } else {
        log.info("Work in progress: not storing keepalive")
      }

    case msg: ProcessedRegistrationMessage if msg.systemId == systemId ⇒
      log.info("Received processedRegistrationMessage with id {} for system {}", msg.id, systemId)

      msg.isCompleted match {
        case true ⇒ updateMessageWithCompletionTime(msg)
        case false if processedRegistrationMessages.exists(p ⇒ p.id == msg.id) ⇒ log.error("Message with id {} already completed. Ignoring.", msg.id)
        case false ⇒ processedRegistrationMessages += msg
      }

      log.debug("Pending messages: " + processedRegistrationMessages)

    case msg: WakeupMessage if msg.systemId == systemId ⇒
      log.info("Received WakeupMessage for system {}", systemId)
      val now = System.currentTimeMillis()

      ProcessedRegistrationsActor.getCompletedUntilTime(processedRegistrationMessages, now, config.timeoutInSeconds) match {
        case Some(lastProcessedTime) ⇒ saveProcessedRegistrationCompletedUntil(lastProcessedTime)
        case None                    ⇒
      }

      processedRegistrationMessages = ProcessedRegistrationsActor.discardCompletedMessagesUntilLastProcessed(
        processedRegistrationMessages, now, config.timeoutInSeconds)

    case _ ⇒ log.debug("Received an unknown message")
  }

  private def updateMessageWithCompletionTime(msg: ProcessedRegistrationMessage) =
    processedRegistrationMessages.find(_.id == msg.id) match {
      case Some(oldMsg) ⇒ processedRegistrationMessages = processedRegistrationMessages.filterNot(_.id == oldMsg.id) + msg
      case None         ⇒ log.error("processedRegistrationMessage to handle not found in the list. Id:" + msg.id)
    }

  private def saveProcessedRegistrationCompletedUntil(lastProcessedTimestamp: Long) = storageActor ! UpdateProcessRegistrationStatus(lastProcessedTimestamp, systemId)
}

