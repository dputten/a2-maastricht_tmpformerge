package csc.validationfilter.qualify

import csc.akkautils.DirectLogging
import csc.sectioncontrol.enforce.common.nl.services.Service
import csc.sectioncontrol.enforce.common.nl.violations._
import csc.sectioncontrol.messages.{ RecognizeRecord, VehicleClassifiedRecord, VehicleCode, VehicleViolationRecord }
import org.joda.time.DateTime

/**
 * Helper case class for holding necessary corridor schedule-data and the service
 *
 * @param service
 * @param corridorSchedule
 */
case class QualifyData(service: Option[Service], corridorSchedule: Option[CorridorScheduleData])

class Qualifier extends DirectLogging {

  /**
   * Finds the schedule in the list of schedules where the vehicle (passage) registrationtime is within the start-(inclusive)
   * and endtime(exclusive) of some schedule
   *
   * @param registrationTime
   * @param schedules
   * @return some schedule, or none...
   */
  private def findSchedule(registrationTime: Long, schedules: Seq[CorridorScheduleData]) = {

    val registrationMillisInDay = new DateTime(registrationTime).getMillisOfDay

    def millisOfBlock(tb: TimeBlock) = tb.hour * 60 * 60 * 1000 + tb.minutes * 60 * 1000

    schedules.find { schedule ⇒
      val afterStartTime = registrationMillisInDay >= millisOfBlock(schedule.startTime)
      val beforeEndTime = registrationMillisInDay < millisOfBlock(schedule.endTime)
      afterStartTime && beforeEndTime
    }
  }

  private def sameDay(ts1: Long, ts2: Long) = new DateTime(ts1).getDayOfYear == new DateTime(ts2).getDayOfYear

  /**
   * Helper method to retrieve the necessary data that enables the process to actually make a decision whether it is a violation or not.
   *
   * @param vcr the record at hand
   * @param config the input config data
   * @return QualifyData instance
   */
  private def qualifyDataFor(vcr: VehicleClassifiedRecord, config: ViolationDeterminationConfigData): QualifyData = {

    case class ScheduleList(list: List[CorridorScheduleData])

    val time = vcr.speedRecord.time
    val corridorId = vcr.speedRecord.corridorId

    val schedulesForCorridor = config.schedules.filter(_.corridorId == corridorId)

    val schedule = ScheduleList(schedulesForCorridor.sortBy(_.historicDateTimeStamp).filter(_.historicDateTimeStamp.map(ts ⇒ sameDay(time, ts)).getOrElse(false)).toList) match {
      case ScheduleList(historicSchedules) if (!historicSchedules.isEmpty) ⇒
        findSchedule(time, historicSchedules)
      case _ ⇒
        findSchedule(time, schedulesForCorridor.filter(_.historicDateTimeStamp == None))
    }

    val corridorService = config.corridorServices.find(_.corridorId == corridorId).get

    QualifyData(corridorService.service, schedule)
  }

  /**
   * First from the config data some subset of necessary info is extracted in order to be able to determine whether this vehicle passage
   * violates something (redlight, speed). It finds the service dynamically based on the config and determines the actual allowed speeds at registration time.
   *
   * @param vcr the record at hand
   * @param config the input config data
   * @return a violation or not
   */
  private[qualify] def makeViolation(vcr: VehicleClassifiedRecord, config: ViolationDeterminationConfigData): Option[VehicleViolationRecord] = {

    val qualifyData = qualifyDataFor(vcr, config)

    if (qualifyData.service.isDefined && qualifyData.corridorSchedule.isDefined) {

      val service = qualifyData.service.get
      val measuredSpeed = vcr.speedRecord.speed
      val scheduleSpeedLimit = qualifyData.corridorSchedule.get.enforcedSpeedLimit

      qualifyData.corridorSchedule.foreach(x ⇒ log.info("CorridorScheduleData" + x))

      val dynamicSpeedLimit = Right(None)
      val vehicleClassSpeedLimit = getVehicleClassSpeedLimit(vcr, config.vehicleClassSpeedLimits)
      val speedMargins = config.speedMargins.map(sm ⇒ (sm.baseSpeed -> sm.margin)).toMap

      val vehicleSpeed = VehicleSpeed(
        measuredSpeed = measuredSpeed,
        scheduleSpeedLimit = scheduleSpeedLimit,
        dynamicSpeedLimit = dynamicSpeedLimit,
        vehicleClassSpeedLimit = vehicleClassSpeedLimit,
        speedMargins = speedMargins)

      val (isViolation, enforcedSpeed) = service.isViolation(vcr, vehicleSpeed)

      log.info("Vehicle speed from config " + vehicleSpeed)
      log.info("enforced speed from config " + enforcedSpeed)
      log.info(" measured speed " + vcr.speedRecord.speed)

      val message = "%s is %s in violation (measuredSpeed=%s, enforcedSpeed=%s)"
      val logMessage = message.format(vcr.licensePlate, if (isViolation) "" else "not", measuredSpeed, enforcedSpeed)
      log.debug(logMessage)

      if (isViolation) {
        val entryRecog = vcr.speedRecord.entry.licensePlateData.map(lic ⇒ RecognizeRecord(recognizeId = lic.recognizedBy.getOrElse("Unknown") + "_entry", isCurrent = true, recognize = lic))
        val exitRecog = vcr.speedRecord.exit.flatMap(_.licensePlateData.map(lic ⇒ RecognizeRecord(recognizeId = lic.recognizedBy.getOrElse("Unknown") + "_exit", isCurrent = false, recognize = lic)))
        val recogList = List() ++ entryRecog ++ exitRecog
        Some(VehicleViolationRecord(vcr.id, vcr, enforcedSpeed, dynamicSpeedLimit.right.toOption.flatMap(identity), recogList))
      } else {
        None
      }
    } else {
      None
    }

  }

  private def getVehicleClassSpeedLimit(vcr: VehicleClassifiedRecord, vehicleClassSpeedLimits: Map[VehicleCode.Value, Int]): Option[Int] = {
    val classification = vcr.code.orElse(vcr.alternativeClassification)
    classification.flatMap(code ⇒ vehicleClassSpeedLimits.get(code))
  }
}
