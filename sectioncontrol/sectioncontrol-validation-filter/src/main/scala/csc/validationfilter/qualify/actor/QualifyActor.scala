package csc.validationfilter.qualify.actor

import akka.actor.{ ReceiveTimeout, Actor, ActorRef, Props }
import akka.util.Duration
import csc.sectioncontrol.messages.ViolationCandidateRecord
import csc.validationfilter.{ ValidationFilterConfig, Dictionary }
import csc.validationfilter.actor.messages._
import akka.util.duration._

object QualifyActor {
  def props = Props(new QualifyActor())
}

class QualifyActor extends Actor {

  import csc.validationfilter.qualify.Qualifier
  val validationFilterConfig = context.system.settings.config.getConfig("validation-filter")
  val timeout = validationFilterConfig.getInt("qualify-timeout")

  val configActor = context.system.actorSelection("/user/" + Dictionary.ZOOKEEPER_ACTOR)

  val qualifier = new Qualifier()

  override protected def receive: Receive = {
    case msg: ValidateAbleVehicleMessage ⇒
      configActor ! GetConfigMessage(msg.vehicleClassifiedRecord.speedRecord.entry.lane.system)
      context.setReceiveTimeout(timeout millisecond)
      context.become(processWithConfig(msg, sender))
  }

  def processWithConfig(validatebleVehicleMessage: ValidateAbleVehicleMessage, respondToActor: ActorRef): Receive = {
    case msg: ConfigMessage ⇒
      qualifier.makeViolation(validatebleVehicleMessage.vehicleClassifiedRecord, msg.config) match {
        case Some(vehicleViolationRecord) ⇒ respondToActor ! ViolationCandidateMessage(ViolationCandidateRecord(now, vehicleViolationRecord))
        case None                         ⇒ respondToActor ! ValidationFinished(success = false)
      }
    case ReceiveTimeout ⇒
      respondToActor ! ReceiveTimeout
  }

  def now = System.currentTimeMillis()
}

