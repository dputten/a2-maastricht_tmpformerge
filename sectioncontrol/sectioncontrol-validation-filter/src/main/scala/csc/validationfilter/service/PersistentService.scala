package csc.validationfilter.service

import csc.sectioncontrol.messages.{ ViolationCandidateRecord, VehicleClassifiedRecord }

trait PersistentService {
  def violationWriter: ViolationWriter
  def classifiedRecordReader(s: String): ClassifiedRecordReader

  def read(from: Long, to: Long): Seq[VehicleClassifiedRecord]
  def store(record: ViolationCandidateRecord)

  trait ViolationWriter {
    def store(record: ViolationCandidateRecord)
  }

  trait ClassifiedRecordReader {
    def read(from: Long, to: Long): Seq[VehicleClassifiedRecord]
  }

}

