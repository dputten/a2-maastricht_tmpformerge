package csc.validationfilter.service

import csc.hbase.utils.HBaseTableKeeper
import csc.sectioncontrol.messages.{ VehicleClassifiedRecord, ViolationCandidateRecord }
import csc.sectioncontrol.storagelayer.hbasetables.{ ConfirmedViolationRecordTable, VehicleClassifiedTable }
import csc.validationfilter.ValidationFilterConfig

object HBasePersistency {

  private val hbaseServers = ValidationFilterConfig.config.getString("hbaseServers")
  val hbaseConfig = HBaseTableKeeper.createCachedConfig(hbaseServers)

}

class HBasePersistentServiceImpl(system: String) {
  this: PersistentService ⇒

  private val reader = classifiedRecordReader(system)
  private val writer = violationWriter

  def read(from: Long, to: Long): Seq[VehicleClassifiedRecord] = reader.read(from, to)
  def store(record: ViolationCandidateRecord) = writer.store(record)
}

trait HBasePersistentService extends PersistentService {

  def violationWriter = new HBaseViolationWriter

  class HBaseViolationWriter extends ViolationWriter with HBaseTableKeeper {

    lazy val violationWriter = ConfirmedViolationRecordTable.makeWriter(HBasePersistency.hbaseConfig, this)

    def store(record: ViolationCandidateRecord) = violationWriter.writeRow(record.vehicleViolationRecord.classifiedRecord, record)
  }

  def classifiedRecordReader(s: String) = new HBaseClassifiedRecordReader(s)

  class HBaseClassifiedRecordReader(s: String) extends ClassifiedRecordReader with HBaseTableKeeper {

    lazy val classifiedRecordsReader = VehicleClassifiedTable.makeReader(HBasePersistency.hbaseConfig, this, s)

    def read(from: Long, to: Long): Seq[VehicleClassifiedRecord] = classifiedRecordsReader.readRows(from, to)
  }

}