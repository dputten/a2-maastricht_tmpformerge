package csc.validationfilter.configuration

import csc.sectioncontrol.enforce.EnforceConfig

case class DriftEvent(driftMsecPerHour: Int, eventType: String)
case class RegistrationTimeUnreliableConfig(driftEvents: Seq[DriftEvent])

case class EnforceNLConfig(ntpServer: String,
                           timeZone: String,
                           verifyRecognition: String,
                           maxMtmFileDelay: Option[Long] = None,
                           maxViolationExportDelay: Option[Long] = None,
                           maxDailyReportDelay: Option[Long] = None,
                           dataRetentionDays: Option[Int] = None,
                           runtimeOffset: Option[Long] = None,
                           timeoutImageResponse: Option[Int] = None,
                           confidenceLevel: Option[Int] = None,
                           recognizerCountries: Option[Seq[String]] = None,
                           recognizerCountryMinConfidence: Option[Int] = None,
                           maxCertifiedSpeed: Int = 300,
                           pardonDuringDSTSwitch: Option[Boolean] = None,
                           recognizeWithARHAgain: Boolean = false,
                           intradaInterfaceToUse: Int = 2,
                           minLowestLicConfLevel: Int = 50,
                           minLicConfLevelRecognizer2: Int = 70,
                           autoThresholdLevel: Int = 55,
                           mobiThresholdLevel: Int = 12,
                           pardonRegistrationTime: RegistrationTimeUnreliableConfig = RegistrationTimeUnreliableConfig(Seq())) extends EnforceConfig

