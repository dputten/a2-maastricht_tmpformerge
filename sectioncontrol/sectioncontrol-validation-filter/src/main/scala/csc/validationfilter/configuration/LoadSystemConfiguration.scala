package csc.validationfilter.configuration

import java.util.{ Calendar, GregorianCalendar, TimeZone }

import csc.config.Path
import csc.curator.utils.Curator
import csc.sectioncontrol.enforce.common.nl.services._
import csc.sectioncontrol.enforce.common.nl.violations._
import csc.sectioncontrol.messages.{ VehicleCode, SpeedIndicatorType }
import csc.sectioncontrol.storage._

import scala.collection.SortedSet

object LoadSystemConfiguration {

  def apply(systemId: String, timezone: TimeZone, curator: Curator): ViolationDeterminationConfigData = {
    val corridorIds = getCorridorIds(systemId, curator)
    val corridorServices = (corridorIds.map { id ⇒ corridorService(systemId, id, curator) })
    val schedules = corridorIds.flatMap(corridorId ⇒ getCorridorScheduleData(systemId, corridorId, timezone, curator))
    val vehicleMaxSpeed = getVehicleMaxSpeeds(systemId, curator)
    val speedMargins = getSpeedMargins(systemId, curator)

    ViolationDeterminationConfigData(
      corridorServices,
      schedules,
      vehicleMaxSpeed,
      speedMargins)
  }

  /**
   * retrieve the service that belongs to a corridor
   * @param corridor that is stored
   * @param systemId id of system
   * @param curator zookeeper storage
   * @return optional Service
   */
  private def getService(corridor: ZkCorridor, systemId: String, curator: Curator): Option[Service] = {

    val rootPathToServices = Path(Paths.Corridors.getServicesPath(systemId, corridor.id))

    //e.g. systems/../corridors/../services/RedLight
    val pathToService = (rootPathToServices / corridor.serviceType.toString).toString()

    val result = corridor.serviceType match {
      case ServiceType.RedLight       ⇒ curator.get[RedLight](pathToService)
      case ServiceType.SpeedFixed     ⇒ curator.get[SpeedFixed](pathToService)
      case ServiceType.SpeedMobile    ⇒ curator.get[SpeedMobile](pathToService)
      case ServiceType.SectionControl ⇒ curator.get[SectionControl](pathToService)
      case ServiceType.ANPR           ⇒ curator.get[ANPR](pathToService)
      case ServiceType.TargetGroup    ⇒ curator.get[TargetGroup](pathToService)
      case _                          ⇒ None
    }
    result
  }

  private def getCorridorIds(systemId: String, curator: Curator): Seq[String] = {
    val corridorPaths = curator.getChildren(Paths.Corridors.getDefaultPath(systemId))
    corridorPaths.map { _.split('/').last }
  }

  /**
   * Due to the fact that now a corridor can have multiple services (redlight, sectioncontrol, etc) we have to extract it to know which one is configured.
   * We use get in the method below, it should never occur that the service is NOT configured. If this is the case, we let it crash.
   * This should be refactored into some case where corridors are corridors and thus sectioncontrol aware and not something that handles redlight violations etc
   */
  private def corridorService(systemId: String, corridorId: String, curator: Curator): CorridorService = {
    curator.get[ZkCorridor](Paths.Corridors.getConfigPath(systemId, corridorId)).map(corridor ⇒
      CorridorService(corridor.info.corridorId, getService(corridor, systemId, curator))).get
  }

  def getCorridorSchedules(systemId: String, corridorId: String, dayStart: Long, dayEnd: Long, timezone: TimeZone, curator: Curator): List[CorridorSchedule] = {
    val currentSchedulesPath = Paths.Corridors.getSchedulesPath(systemId, corridorId)
    val currentSchedules = loadCorridorSchedules(systemId, corridorId, dayStart, dayEnd, currentSchedulesPath, timezone, curator)

    val historicScheduleSaves = curator.getChildNames(Paths.Corridors.getScheduleHistoryPath(systemId, corridorId)).
      map(_.toLong).
      filter(t ⇒ t > dayStart && t < dayEnd).
      sorted.
      toList
    val historicIntervals = (dayStart :: historicScheduleSaves).zip(historicScheduleSaves)
    val historicSchedules = historicIntervals.map { interval ⇒
      val historicPath = Paths.Corridors.getScheduleHistoryPath(systemId, corridorId) + "/" + interval._2.toString
      loadCorridorSchedules(systemId, corridorId, interval._1, interval._2, historicPath, timezone, curator)
    }
    updateCurrentSchedules(currentSchedules, historicScheduleSaves.lastOption.getOrElse(0L)) ++ historicSchedules.flatten
  }

  def getCorridorScheduleData(systemId: String, corridorId: String, timezone: TimeZone, curator: Curator): List[CorridorScheduleData] = {
    currentCorridorScheduleData(systemId, corridorId, timezone, curator) ::: historicCorridorScheduleData(systemId, corridorId, timezone, curator)
  }

  private def currentCorridorScheduleData(systemId: String, corridorId: String, timezone: TimeZone, curator: Curator): List[CorridorScheduleData] = {
    val currentSchedulesPath = Paths.Corridors.getSchedulesPath(systemId, corridorId)
    loadCorridorScheduleData(systemId, corridorId, currentSchedulesPath, timezone, curator)
  }

  private def historicCorridorScheduleData(systemId: String, corridorId: String, timezone: TimeZone, curator: Curator): List[CorridorScheduleData] = {
    val historicScheduleSaves = curator.getChildNames(Paths.Corridors.getScheduleHistoryPath(systemId, corridorId)).toList
    historicScheduleSaves.flatMap { historyId ⇒
      val historicPath = Paths.Corridors.getScheduleHistoryPath(systemId, corridorId) + "/" + historyId
      loadCorridorScheduleData(systemId, corridorId, historicPath, timezone, curator).map(schedule ⇒ schedule.copy(historicDateTimeStamp = Some(historyId.toLong)))
    }
  }

  private def updateCurrentSchedules(current: List[CorridorSchedule], endTimeLastHistoric: Long): List[CorridorSchedule] = {
    current.map(schedule ⇒ {
      if (endTimeLastHistoric > schedule.startTime) {
        schedule.copy(startTime = endTimeLastHistoric)
      } else {
        schedule
      }
    })
  }

  def loadCorridorScheduleData(systemId: String, corridorId: String, schedulesZkPath: String, timezone: TimeZone, curator: Curator): List[CorridorScheduleData] = {

    val corridorSchedulesOption = for {
      corridor ← curator.get[ZkCorridor](Paths.Corridors.getConfigPath(systemId, corridorId))
      schedules ← curator.get[List[ZkSchedule]](schedulesZkPath)
    } yield schedules.map(schedule ⇒ CorridorScheduleData(corridor.info.corridorId,
      schedule.speedLimit,
      TimeBlock(schedule.fromPeriodHour, schedule.fromPeriodMinute),
      TimeBlock(schedule.toPeriodHour, schedule.toPeriodMinute)))

    corridorSchedulesOption.getOrElse(List())
  }

  def loadCorridorSchedules(systemId: String, corridorId: String, timeStart: Long, timeEnd: Long, schedulesZkPath: String, timezone: TimeZone, curator: Curator): List[CorridorSchedule] = {
    implicit def convertIndication(indication: ZkIndicationType.Value): Option[Boolean] = {
      indication match {
        case ZkIndicationType.True  ⇒ Some(true)
        case ZkIndicationType.False ⇒ Some(false)
        case ZkIndicationType.None  ⇒ None
      }
    }

    val corridorSchedulesOption = for {
      corridor ← curator.get[ZkCorridor](Paths.Corridors.getConfigPath(systemId, corridorId))
      schedules ← curator.get[List[ZkSchedule]](schedulesZkPath)
    } yield schedules.map(schedule ⇒ CorridorSchedule(corridor.info.corridorId,
      schedule.speedLimit,
      if (schedule.signSpeed >= 0) Some(schedule.signSpeed) else None,
      getPshTmIdFormat(systemId, corridorId, curator),
      calculateDayTime(timeStart, schedule.fromPeriodHour, schedule.fromPeriodMinute, timezone),
      calculateDayTime(timeStart, schedule.toPeriodHour, schedule.toPeriodMinute, timezone),
      indicationRoadworks = schedule.indicationRoadworks,
      indicationActualWork = schedule.indicationActualWork,
      indicationDanger = schedule.indicationDanger,
      supportMsgBoard = schedule.supportMsgBoard,
      textCode = parseStringToInt(corridor.codeText),
      dutyType = if (corridor.dutyType.trim.length == 0) None else Option(corridor.dutyType),
      deploymentCode = if (corridor.deploymentCode.trim.length == 0) None else Option(corridor.deploymentCode),
      invertDrivingDirection = schedule.invertDrivingDirection,
      speedIndicator = SpeedIndicator(
        signIndicator = schedule.signIndicator,
        speedIndicatorType = schedule.speedIndicatorType.map(a ⇒ SpeedIndicatorType.withName(a.toString)))))

    val corridorSchedules = corridorSchedulesOption.map(schedules ⇒ schedules.flatMap(schedule ⇒
      if (schedule.startTime >= timeStart && schedule.endTime <= timeEnd) {
        List(schedule)
      } else if (schedule.startTime >= timeEnd || schedule.endTime <= timeStart) {
        List()
      } else {
        List(schedule.copy(startTime = math.max(schedule.startTime, timeStart),
          endTime = math.min(schedule.endTime, timeEnd)))
      }))

    corridorSchedules.getOrElse(List())
  }

  private def parseStringToInt(v: String): Option[Int] = {
    if (v.length > 0) {
      try {
        Some(v.toInt)
      } catch {
        case e: Exception ⇒ None
      }
    } else {
      None
    }
  }
  //
  private def getPshTmIdFormat(systemId: String, corridorId: String, curator: Curator): String = {
    val defaultElementOrder = "YMDNVLHTX"

    val pshTmIdFormat = for {
      corridor ← curator.get[ZkCorridor](Paths.Corridors.getConfigPath(systemId, corridorId))
    } yield {
      val pshTmIdSpec = corridor.pshtm

      val elementOrderString = corridor.pshtm.elementOrderString.getOrElse(defaultElementOrder)

      def elementString(elementChar: Char): String = elementChar match {
        case 'Y' ⇒ if (pshTmIdSpec.useYear) "[YY]" else ""
        case 'M' ⇒ if (pshTmIdSpec.useMonth) "[MM]" else ""
        case 'D' ⇒ if (pshTmIdSpec.useDay) "[DD]" else ""
        case 'N' ⇒ if (pshTmIdSpec.useNumberOfTheDay) "[NNN]" else ""
        case 'V' ⇒ if (pshTmIdSpec.useReportingOfficerId) "[VVVV]" else ""
        case 'L' ⇒ if (pshTmIdSpec.useLocationCode) "[LLLL]" else ""
        case 'H' ⇒ if (pshTmIdSpec.useHHMCode) "[HHHH]" else ""
        case 'T' ⇒ if (pshTmIdSpec.useTypeViolation) "[T]" else ""
        case 'X' ⇒ if (pshTmIdSpec.useFixedCharacter) pshTmIdSpec.fixedCharacter else ""
      }

      elementOrderString.foldLeft("")((acc, cur) ⇒ acc + elementString(cur))
    }
    require(pshTmIdFormat.isDefined, "Could not load PSH-TM-ID format for systemId " + systemId + ", corridorId " + corridorId)
    pshTmIdFormat.get
  }

  def getVehicleMaxSpeeds(systemId: String, curator: Curator): Map[VehicleCode.Value, Int] = {
    val vehicleMaxSpeeds = for {
      prefs ← curator.get[ZkPreference](Seq(Path(Paths.Preferences.getGlobalConfigPath), Path(Paths.Preferences.getConfigPath(systemId))), ZkPreference.default)
    } yield prefs.vehicleMaxSpeed.map(vms ⇒ vms.vehicleType -> vms.maxSpeed).toMap

    require(vehicleMaxSpeeds.isDefined, "Could not load vehicle max speeds for systemId " + systemId)
    vehicleMaxSpeeds.get
  }

  def getSpeedMargins(systemId: String, curator: Curator): SortedSet[SpeedMargin] = {
    val speedMargins = for {
      prefs ← curator.get[ZkPreference](Seq(Path(Paths.Preferences.getGlobalConfigPath), Path(Paths.Preferences.getConfigPath(systemId))), ZkPreference.default)
    } yield prefs.marginSpeedLimits.map(msl ⇒ SpeedMargin(msl.speedLimit, msl.speedMargin))

    require(speedMargins.isDefined, "Could not load speed margins for systemId " + systemId)
    SortedSet(speedMargins.get: _*)
  }
  private def calculateDayTime(dayStart: Long, hour: Int, minute: Int, timezone: TimeZone): Long = {
    val cal = new GregorianCalendar(timezone)
    cal.setTimeInMillis(dayStart)
    cal.set(Calendar.HOUR_OF_DAY, hour)
    cal.set(Calendar.MINUTE, minute)
    cal.getTimeInMillis
  }

}
