package csc.validationfilter.domain

object ViolationVerificationResult extends Enumeration {
  type ViolationVerificationResult = Value
  val OK, ManualLicence, ManualCountry, Invalid, ManualLicenceFormat, ChangeToMobiAndCountry, ChangeToAutoAndCountry, ChangeCountry, ReClassify = Value
}

