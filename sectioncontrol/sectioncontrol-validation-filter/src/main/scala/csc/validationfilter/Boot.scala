/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */
package csc.validationfilter

import akka.actor.{ ActorRef, ActorSystem, Props }
import akka.kernel.Bootable
import akka.util.duration._
import csc.akkautils.DirectLogging
import csc.curator.utils.{ Curator, CuratorToolsImpl }
import csc.imageretriever.ImageRetrieverConfig
import csc.imageretriever.ImageRetrieverConfig.{ AmqpConfig, AmqpServicesConfig, HBaseConfig }
import csc.imageretriever.actor.RetrieveImageActor
import csc.sectioncontrol.storage.Paths
import csc.sectioncontrol.storagelayer.StorageFactory
import csc.sectioncontrol.storagelayer.hbasetables.{ RowKeyDistributorFactory, SerializerFormats }
import csc.sectioncontrol.storagelayer.jobs.ProcessedRegistrationCompletionStatusService
import csc.validationfilter.actor.messages.{ ProcessedRegistrationTimeFrameMessage, WakeupMessage }
import csc.validationfilter.actor.{ ImageCheckersActor, StartReadersActor, ZookeeperAccessActor, _ }
import org.apache.curator.retry.ExponentialBackoffRetry

class Boot extends Bootable with DirectLogging {
  implicit val actorSystem = ActorSystem("ValidationFilter")
  var startProcessedRegistrationsActors: Seq[ActorRef] = _

  def startup() = {
    log.info("Starting validation filter")
    val curator = startCurator()

    // for each system start an startProcessedRegistrationsActor. Messages are handled by system

    val systems = curator.getChildNames(Paths.Systems.getDefaultPath)
    log.info("preparing registration processors for " + systems)

    startProcessedRegistrationsActors = systems.map(systemId ⇒ startProcessedRegistrationsActor(systemId))

    actorSystem.actorOf(ZookeeperAccessActor.props(curator), Dictionary.ZOOKEEPER_ACTOR)
    actorSystem.actorOf(ImageCheckersActor.props(curator), Dictionary.IMAGE_CHECKERS_ACTOR)
    actorSystem.actorOf(imageRetrieverProps, Dictionary.IMAGE_RETRIEVER_ACTOR)
    actorSystem.actorOf(StartReadersActor.props)
    ReclassifyActor(actorSystem, ReclassifyConfig(actorSystem.settings.config.getConfig("validation-filter")), Dictionary.RECLASSIFY_ACTOR)
  }

  def shutdown() = {
    log.info("Shutting down validation filter")
    startProcessedRegistrationsActors.foreach(a ⇒ actorSystem.stop(a))
  }

  protected def startCurator(): Curator = {
    val retryPolicy = new ExponentialBackoffRetry(ZookeeperConfig.config.getInt("session-timeout"), ZookeeperConfig.config.getInt("retries"))
    val curatorFramework = StorageFactory.newClient(ZookeeperConfig.config.getString("servers"), retryPolicy)
    curatorFramework.foreach(_.start())
    val zookeeperClient = new CuratorToolsImpl(curatorFramework, log, Boot.jsonFormats)
    RowKeyDistributorFactory.init(zookeeperClient)
    zookeeperClient
  }

  def imageRetrieverProps: Props = {
    val validationFilterConfig = actorSystem.settings.config.getConfig("validation-filter") //ValidationFilterConfig.config
    val imageRetrieverConfig = validationFilterConfig.getConfig(ImageRetrieverConfig.rootKey)
    val amqp = new AmqpConfig(imageRetrieverConfig.getConfig(ImageRetrieverConfig.amqpKey))
    val amqpServices = new AmqpServicesConfig(imageRetrieverConfig.getConfig(ImageRetrieverConfig.amqpServicesKey))
    val hbase = new HBaseConfig(validationFilterConfig.getString("hbaseServers")) //hbase servers taken from validation filter config
    val config = ImageRetrieverConfig(amqp, amqpServices, hbase)
    log.info("RetrieveImageActor config: {}", config)
    RetrieveImageActor.props(config)
  }

  protected def startProcessedRegistrationsActor(systemId: String): ActorRef = actorSystem.actorOf(ProcessedRegistrationsActor.props(systemId), "ProcessedRegistrationsActor-" + systemId)

  def getSystems(curator: Curator): Seq[String] = curator.getChildNames(Paths.Systems.getDefaultPath)

}

object Boot {
  val jsonFormats = SerializerFormats.formats
}
