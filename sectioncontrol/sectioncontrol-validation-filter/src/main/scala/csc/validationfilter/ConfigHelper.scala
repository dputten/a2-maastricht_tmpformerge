package csc.validationfilter

import csc.config.Path
import csc.sectioncontrol.storage.Paths

/**
 * this class is here to be able to have these sorts of configs (when zookeeper doesn't have value
 * this should be refactored / removed later when we don't depend on zookeeper anymore
 */

object ConfigHelper {

  val globalEnforceConfigPath = Path(Paths.Configuration.getGlobalEnforceConfig)

  val defaultConfig = Map(
    "recognizeWithARHAgain" -> false,
    "intradaInterfaceToUse" -> 2,
    "minLowestLicConfLevel" -> 50,
    "minLicConfLevelRecognizer2" -> 70,
    "autoThresholdLevel" -> 55,
    "mobiThresholdLevel" -> 12,
    "pardonRegistrationTime" -> Map("driftEvents" -> "[]"))

}
