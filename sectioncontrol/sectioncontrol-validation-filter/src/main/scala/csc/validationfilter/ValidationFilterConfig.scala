package csc.validationfilter

import com.typesafe.config.{ Config, ConfigFactory }

/*
TODO: This does not load test/resources/application.conf. Use actorsystem.settings.config instead
nb: This object is only used in HBasePersistenceService
 */
object ValidationFilterConfig {

  val config = ConfigFactory.load().getConfig("validation-filter")
}

object ZookeeperConfig {

  val config = ConfigFactory.load().getConfig("zookeeper")

}

case class ProcessedRegistrationCompletionConfiguration(initialDelay: Int, frequency: Int, timeoutInSeconds: Int)
object ProcessedRegistrationCompletionConfiguration {
  def apply(config: Config) = {
    new ProcessedRegistrationCompletionConfiguration(
      config.getInt("wakeup.initialDelay"),
      config.getInt("wakeup.frequency"),
      config.getInt("timeoutInSeconds"))
  }
}