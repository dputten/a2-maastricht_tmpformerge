package csc.validationfilter

import java.util.TimeZone

object Dictionary {

  val timeZone = TimeZone.getTimeZone("Europe/Amsterdam")

  val ZOOKEEPER_ACTOR = "zookeeperAccessActor"

  val IMAGE_CHECKERS_ACTOR = "imageCheckersActor"

  val IMAGE_RETRIEVER_ACTOR = "imageRetrieverActor"

  val RECLASSIFY_ACTOR = "reclassifyActor"

}
