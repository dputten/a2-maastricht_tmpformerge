package csc.validationfilter.violations

import csc.hbase.utils.HBaseTableKeeper
import csc.hbase.utils.testframework.HBaseTestFramework
import csc.sectioncontrol.storage.HBaseTableDefinitions._
import csc.sectioncontrol.storagelayer.hbasetables.ConfirmedViolationRecordTable
import org.apache.hadoop.hbase.client.HTable
import org.scalatest.matchers.MustMatchers
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, WordSpec}

class ViolationsTest extends WordSpec
  with MustMatchers
  with BeforeAndAfterAll
  with BeforeAndAfterEach with HBaseTestFramework {

  import csc.validationfilter.actor.RetrieveImagesActorTest
  val tableKeeper = new HBaseTableKeeper {}

  var table: HTable = _

  val violationRecords = (1 to 100).map { dummy ⇒
    val record = RetrieveImagesActorTest.createViolationRecord(true, true, true)
    (record.vehicleViolationRecord.classifiedRecord, record)
  }.toSeq

  override def beforeAll(): Unit = {
    super.beforeAll()
    table = testUtil.createTable(confirmedViolationTableName.getBytes, confirmedViolationColumnFamily.getBytes)
  }

  "The violations test" must {

    "store violation records" in {

      val writer = ConfirmedViolationRecordTable.makeWriter(testUtil.getConfiguration, tableKeeper)
      writer.writeRows(violationRecords)

      testUtil.countRows(table) must be(100)
    }

  }

  override protected def afterAll() {
    table.close()
    super.afterAll()
  }
}

