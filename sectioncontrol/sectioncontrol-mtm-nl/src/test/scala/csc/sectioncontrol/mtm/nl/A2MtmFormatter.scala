/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.mtm.nl

import util.matching.Regex

/**
 * Format old format for A20 to new format for A2
 */
object A2MtmFormatter extends App {
  val input = getClass.getResourceAsStream("/20120209-MTM2-A2.txt")
  val lines = scala.io.Source.fromInputStream(input, "utf-8").getLines()
  val MsiEntry = new Regex("""\|(\d+)(\s+)\|""")
  val newLines = lines.map(line ⇒ {
    if (line.startsWith("|msi|")) {
      //process MSI data
      val beginning = line.substring(0, 77)
      val end = line.substring(77)
      val newEnd = end match {
        case MsiEntry(speed, spaces) ⇒ {
          "|/%s/%s|".format(speed, spaces.substring(2))
        }
        case _ ⇒ end
      }
      beginning + newEnd
    } else line
  })
  newLines.foreach(line ⇒ println(line))
}

