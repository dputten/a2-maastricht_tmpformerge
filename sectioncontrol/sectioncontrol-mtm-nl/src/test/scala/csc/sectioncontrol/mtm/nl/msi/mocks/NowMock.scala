package csc.sectioncontrol.mtm.nl

class NowMock() extends TimeNow {
  var _now: Long = _
  override def getNow: Long = _now
  override def setNow(now: Long) = {
    _now = now
  }
}
