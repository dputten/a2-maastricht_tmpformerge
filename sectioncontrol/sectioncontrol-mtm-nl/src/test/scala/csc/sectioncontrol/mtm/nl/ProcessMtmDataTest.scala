package csc.sectioncontrol.mtm.nl

import java.io.File

import akka.actor.{ Actor, ActorSystem, Props }
import akka.dispatch.Await
import akka.pattern.ask
import akka.testkit.TestActorRef
import akka.util.Timeout
import akka.util.duration._
import csc.akkautils.DirectLoggingAdapter
import csc.curator.CuratorTestServer
import csc.curator.utils.{ Curator, CuratorToolsImpl }
import csc.sectioncontrol.enforce.{ SpeedEventData, SpeedIndicatorStatus }
import csc.sectioncontrol.messages.{ Interval, StatsDuration, SystemEvent }
import csc.sectioncontrol.storage.ZkMsiIdentification
import csc.sectioncontrol.storagelayer.excludelog.{ ExcludeLogLayer, SuspendEvent }
import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfter, BeforeAndAfterAll, WordSpec }

class ProcessMtmDataTest extends WordSpec with MustMatchers with CuratorTestServer with BeforeAndAfter with BeforeAndAfterAll {
  implicit val testSystem = ActorSystem("ProcessMtmDataTest")
  var systemEvents: List[SystemEvent] = _
  var counter: Int = _
  var speedEvents: List[SpeedEventData] = _
  var curator: Curator = _
  val log = new DirectLoggingAdapter(this.getClass.getName)

  before {
    systemEvents = Nil
    counter = 0
    speedEvents = Nil
  }

  def getStatsDuration() = Some(StatsDuration("20120528", "Europe/Amsterdam"))

  def getMsi(corridorConfigs: List[CorridorConfig]): Msi = {
    val timeNowMock = new NowMock
    timeNowMock.setNow(1)

    new Msi(corridorConfigs, null, Nil,
      "testSystemId", new SystemServiceMock, new MailServerServiceMock(), new UserServiceMock(), timeNowMock, "ProcessMtmData")
  }

  "ProcessMtmData" must {

    "send a system event if no data file found" in {
      testSystem.eventStream.subscribe(listener, classOf[SystemEvent])
      val actorRef = testSystem.actorOf(Props(new ProcessMtmData(curator, "A2", getMsi(List(new CorridorConfig(25, List(383, 387), Nil))))))
      val dummyRef = TestActorRef(new CountingActor)
      implicit val timeout = Timeout(5 seconds)
      val future = actorRef ? MtmSource("bla-bla", dummyRef, List(new CorridorConfig(25, List(383, 387), Nil)),
        1328569200000L, 1328655600000L, None, getStatsDuration)
      val getResult = Await.result(future, timeout.duration).asInstanceOf[(Int, Int)]
      getResult must be((0, 0))
      counter must be(0)
      val head :: Nil = systemEvents
      head.reason.contains("No such file or directory")
      head.componentId must be("ProcessMtmData")
    }

    "send a few system events with invalid station" in {
      testSystem.eventStream.subscribe(listener, classOf[SystemEvent])
      val configs = List(new CorridorConfig(25, List(1089, 383, 387, 1325, 388, 389, 390, 391, 392, 393, 1587, 395), Nil))

      val actorRef = testSystem.actorOf(Props(new ProcessMtmData(curator, "A2", getMsi(configs))))
      val dummyRef = TestActorRef(new CountingActor)
      val path = getClass.getClassLoader.getResource("20120209-MTM2-A2.txt").getPath
      val name = new File(path)
      name.exists() must be(true)
      implicit val timeout: Timeout = Timeout(30 seconds)
      val future = actorRef ? MtmSource(name.getAbsolutePath, dummyRef, configs, 1328569200000L, 1328655600000L, None, getStatsDuration)
      val getResult = Await.result(future, timeout.duration).asInstanceOf[(Int, Int)]

      getResult must be((14, 1))
      systemEvents.size must be(0)
      counter must be(9)
    }

    "save suspend-events in excludelog if MSI-changes during dst-change" in {
      testSystem.eventStream.subscribe(listener, classOf[SystemEvent])
      val configs = List(new CorridorConfig(25, List(383, 387, 1325, 388, 389, 390, 391, 392, 393, 1587, 395), Nil))

      val actorRef = testSystem.actorOf(Props(new ProcessMtmData(curator, "A2", getMsi(configs))))
      val dummyRef = TestActorRef(new CountingActor)
      val name = new File(getClass.getClassLoader.getResource("20141026-MTM2-dst-check.txt").getPath)
      name.exists() must be(true)
      implicit val timeout: Timeout = Timeout(20 seconds)
      val future = actorRef ? MtmSource(name.getAbsolutePath, dummyRef, configs, 1414274400000L, 1414364400000L,
        Some(Interval(1414281600000L, 1414288800000L)), getStatsDuration)
      val getResult = Await.result(future, timeout.duration).asInstanceOf[(Int, Int)]
      val excludeLogStore = new ExcludeLogLayer(curator)
      val events = excludeLogStore.readEvents("A2", 25)
      val expected = List(
        SuspendEvent("ProcessMtmData", 1414281600000L, Some(25), 1, true, Some("DST change")),
        SuspendEvent("ProcessMtmData", 1414288800000L, Some(25), 2, false, Some("DST change")))
      events must be(expected)
      getResult must be((14, 39))
      systemEvents.size must be(0)
      counter must be(47)
    }

    "not save suspend-events in excludelog if no MSI-changes during dst-change" in {
      testSystem.eventStream.subscribe(listener, classOf[SystemEvent])
      val configs = List(new CorridorConfig(25, List(383, 387, 1325, 388, 389, 390, 391, 392, 393, 1587, 395), Nil))

      val actorRef = testSystem.actorOf(Props(new ProcessMtmData(curator, "A2", getMsi(configs))))
      val dummyRef = TestActorRef(new CountingActor)
      val path = getClass.getClassLoader.getResource("20141026-MTM2-no-dst-check.txt").getPath
      val name = new File(path)
      name.exists() must be(true)
      implicit val timeout: Timeout = Timeout(20 seconds)
      val future = actorRef ? MtmSource(name.getAbsolutePath, dummyRef, configs, 1414274400000L, 1414364400000L,
        Some(Interval(1414281600000L, 1414288800000L)), getStatsDuration)
      val getResult = Await.result(future, timeout.duration).asInstanceOf[(Int, Int)]
      val excludeLogStore = new ExcludeLogLayer(curator)
      val events = excludeLogStore.readEvents("A2", 25)

      events must be(Nil)
      getResult must be((14, 37))
      systemEvents.size must be(0)
      counter must be(45)
    }

    "integrate os and top status in speed event stream" in {
      testSystem.eventStream.subscribe(listener, classOf[SystemEvent])
      val configs = List(new CorridorConfig(25, List(989), Nil))

      val actorRef = testSystem.actorOf(Props(new ProcessMtmData(curator, "A2", getMsi(configs))))
      val dummyRef = TestActorRef(new CountingActor)
      val path = getClass.getClassLoader.getResource("mtmruw-os-top-integration.txt").getPath
      val name = new File(path)
      name.exists() must be(true)
      implicit val timeout: Timeout = Timeout(20 seconds)
      val future = actorRef ? MtmSource(name.getAbsolutePath, dummyRef, configs, 1341439200000L, 1341439200000L + 1.day.toMillis,
        None, getStatsDuration)
      val getResult = Await.result(future, timeout.duration).asInstanceOf[(Int, Int)]
      getResult must be((10, 4))
      systemEvents.size must be(0)
      counter must be(11)
      val test = speedEvents.reverse.zip(List(
        SpeedDataImpl(1341439200000L, 25, 11, Right(Some(100))),
        SpeedDataImpl(1341493200000L, 25, 12, Left(SpeedIndicatorStatus.Undetermined)),
        SpeedDataImpl(1341493500000L, 25, 13, Right(Some(100))),
        SpeedDataImpl(1341493800000L, 25, 15, Left(SpeedIndicatorStatus.Undetermined)),
        SpeedDataImpl(1341494100000L, 25, 15, Right(Some(120))),
        SpeedDataImpl(1341494400000L, 25, 15, Left(SpeedIndicatorStatus.Undetermined)),
        SpeedDataImpl(1341494700000L, 25, 15, Right(Some(120))),
        SpeedDataImpl(1341495000000L, 25, 15, Left(SpeedIndicatorStatus.Undetermined)),
        SpeedDataImpl(1341495300000L, 25, 15, Right(Some(120))),
        SpeedDataImpl(1341495600000L, 25, 15, Left(SpeedIndicatorStatus.Undetermined)),
        SpeedDataImpl(1341495900000L, 25, 15, Right(Some(120)))))

      test.foreach {
        case (se1, se2) ⇒
          se1.effectiveTimestamp must be(se2.effectiveTimestamp)
          se1.corridorId must be(se2.corridorId)
          se1.speedSetting must be(se2.speedSetting)
      }
    }

    "send SystemEvents for excluded MSIs" in {
      testSystem.eventStream.subscribe(listener, classOf[SystemEvent])
      val configs = List(new CorridorConfig(25, List(383, 387, 1325, 388, 389, 390, 391, 392, 393, 1587, 395),
        List(ZkMsiIdentification(383, "1"))))
      val actorRef = testSystem.actorOf(Props(new ProcessMtmData(curator, "A2", getMsi(configs))))
      val dummyRef = TestActorRef(new CountingActor)
      val path = getClass.getClassLoader.getResource("20120209-MTM2-A2.txt").getPath
      val name = new File(path)
      name.exists() must be(true)

      implicit val timeout: Timeout = Timeout(20 seconds)
      val future = actorRef ? MtmSource(name.getAbsolutePath, dummyRef, configs, 1328569200000L, 1328655600000L, None, getStatsDuration)
      val getResult = Await.result(future, timeout.duration).asInstanceOf[(Int, Int)]
      getResult must be((14, 1))
      systemEvents.size must be(1)
      val event = systemEvents.head
      event.componentId must be("ProcessMtmData")
      event.eventType must be("INFO")
      event.userId must be("System")
      event.reason must be("Uitgesloten signaalgever: os_comm_id: 383, rijstrook: 1")
      counter must be(9)
    }

  }

  override def beforeEach() {
    super.beforeEach()
    curator = new CuratorToolsImpl(clientScope, log)
  }

  override protected def afterAll() {
    testSystem.shutdown()
    super.afterAll()
  }

  class CountingActor extends Actor {
    def receive = {
      case ev: SpeedEventData ⇒
        counter = counter + 1
        speedEvents = ev :: speedEvents
    }
  }

  val listener = testSystem.actorOf(Props(new Actor {
    def receive = {
      case d: SystemEvent ⇒ systemEvents = d :: systemEvents
    }
  }))
}
