package csc.sectioncontrol.mtm.nl

import csc.sectioncontrol.storagelayer.mail.MailServerService
import csc.sectioncontrol.messages.MailServer
import csc.curator.utils.Versioned

class MailServerServiceMock() extends MailServerService {
  def get(): Option[MailServer] = Some(MailServerFactory.createMailServer)
  override def exists(): Unit = ()
  override def delete(): Unit = ()
  override def update(mailServer: MailServer, version: Int): Unit = ()
  override def getVersioned(): Option[Versioned[MailServer]] = null
  override def create(mailServer: MailServer): Unit = ()
}
