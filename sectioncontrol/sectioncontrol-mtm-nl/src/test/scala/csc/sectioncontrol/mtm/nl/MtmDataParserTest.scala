/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.mtm.nl

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import akka.util.duration._
import csc.sectioncontrol.storage.ZkMsiIdentification

class MtmDataParserTest extends WordSpec with MustMatchers {

  "TopData" must {
    "parse TOP new data" in {
      val entry = TopData("|top|2003/11/25 08:41:33|1068912754| 10| 5| 08|", 1068850800000L, 1068850800000L + 1.day.toMillis)
      entry.tijdstip_s must be("1068912754")
      entry.topMode must be(10)
      entry.clockStatus must be(5)
      entry.fepMode must be(8)
    }
    "parse TOP old data" in {
      val entry = TopData("|top|2003/11/25 08:41:33|1328569200|      41|                 1|      2222|", 1328569200000L, 1328655600000L)
      entry.timestamp must be(1328569200000L)
      entry.topMode must be(41)
      entry.clockStatus must be(1)
      entry.fepMode must be(2222)
    }
    "parse TOP negative data" in {
      val entry = TopData("|top|2003/11/25 08:41:33|1328610346|      -1|                 -1|      -1|", 1328569200000L, 1328655600000L)
      entry.tijdstip_s must be("1328610346")
      entry.topMode must be(-1)
      entry.clockStatus must be(-1)
      entry.fepMode must be(-1)
    }
    "parse empty TOP data with valid timestamp (req 3)" in {
      val entry = TopData("|top|2003/11/25 08:41:33|1328610346|      |                 |      |", 1328569200000L, 1328655600000L)
      entry.tijdstip_s must be("1328610346")
      entry.topMode must be(-1)
      entry.clockStatus must be(-1)
      entry.fepMode must be(-1)
    }
    "fail to parse TOP data without timestamp (req 3)" in {
      val exception = intercept[MtmFormatException] {
        TopData("|top|2003/11/25 08:41:33| |      |                 |      |", 1328569200000L, 1328655600000L)
      }
      exception.message must be("Onverwacht TOP dataformat.")
      exception.line must be("|top|2003/11/25 08:41:33| |      |                 |      |")
    }
    "fail to parse TOP data with incorrect timestamp" in {
      val exception = intercept[MtmFormatException] {
        TopData("|top|2003/11/25 08:41:33|1328610346|      |                 |      |", 0L, 0L + 1.day.toMillis)
      }
      exception.message must be("TOP data voor verkeerde dag.")
      exception.line must be("|top|2003/11/25 08:41:33|1328610346|      |                 |      |")
    }
  }

  "MsiData" must {
    "parse MSI blank data" in {
      val entry = MsiData("|msi|2003/11/25 11:13:29|1068912754| 836|A13 L 7.200| 1|BL |", 1068850800000L, 1068850800000L + 1.day.toMillis)
      entry.tijdstip_s must be("1068912754")
      entry.os_comm_id must be(836)
      entry.msi_rijstr_pos must be("1")
      entry.msi_stand must be("BL")
      entry.getSpeed must be(None)
    }
    "parse @ (end of all restirctions) data" in {
      val entry = MsiData("|msi|2003/11/25 11:13:29|1068912754| 836|A13 L 7.200| 1|*@* |", 1068850800000L, 1068850800000L + 1.day.toMillis)
      entry.tijdstip_s must be("1068912754")
      entry.os_comm_id must be(836)
      entry.msi_rijstr_pos must be("1")
      entry.msi_stand must be("*@*")
      entry.getSpeed must be(None)
    }
    "parse MSI new data with speed" in {
      val entry = MsiData("|msi|2003/11/25 11:13:29|1068912754| 836|A13 L 7.200| 1 | *80* | ", 1068850800000L, 1068850800000L + 1.day.toMillis)
      entry.tijdstip_s must be("1068912754")
      entry.timestamp must be(1068912754000L)
      entry.os_comm_id must be(836)
      entry.msi_rijstr_pos must be("1")
      entry.msi_stand must be("*80*")
      entry.getSpeed must be(Some(80))
    }
    "parse MSI in old format with speed" in {
      val entry = MsiData("|msi|2003/11/25 11:13:29|1068912754| 836|A13 L 7.200| 1 | 80F | ", 1068850800000L, 1068850800000L + 1.day.toMillis)
      entry.tijdstip_s must be("1068912754")
      entry.timestamp must be(1068912754000L)
      entry.os_comm_id must be(836)
      entry.msi_rijstr_pos must be("1")
      entry.msi_stand must be("80F")
      entry.getSpeed must be(Some(80))
    }
    "parse MSI new data with preferred speed" in {
      val entry = MsiData("|msi|2003/11/25 11:13:29|1068912754| 836|A13 L 7.200| 1| 100|", 1068850800000L, 1068850800000L + 1.day.toMillis)
      entry.tijdstip_s must be("1068912754")
      entry.timestamp must be(1068912754000L)
      entry.os_comm_id must be(836)
      entry.msi_rijstr_pos must be("1")
      entry.msi_stand must be("100")
      entry.getSpeed must be(Some(100))
    }
    "parse MSI new data with preferred speed - no leading space for speed" in {
      val entry = MsiData("|msi|2003/11/25 11:13:29|1068912754| 836|A13 L 7.200| 1|100   |", 1068850800000L, 1068850800000L + 1.day.toMillis)
      entry.tijdstip_s must be("1068912754")
      entry.timestamp must be(1068912754000L)
      entry.os_comm_id must be(836)
      entry.msi_rijstr_pos must be("1")
      entry.msi_stand must be("100")
      entry.getSpeed must be(Some(100))
    }
    "parse MSI new data with end max speed" in {
      val entry = MsiData("|msi|2003/11/25 11:13:29|1068912754| 836|A13 L 7.200| 1| /100/|", 1068850800000L, 1068850800000L + 1.day.toMillis)
      entry.timestamp must be(1068912754000L)
      entry.os_comm_id must be(836)
      entry.getSpeed must be(None)
    }
    "parse MSI new data with max speed" in {
      val entry = MsiData("|msi|2003/11/25 11:13:29|1068912754| 839|A13 L 7.200| 1| (70)|", 1068850800000L, 1068850800000L + 1.day.toMillis)
      entry.timestamp must be(1068912754000L)
      entry.os_comm_id must be(839)
      entry.getSpeed must be(Some(70))
    }
    "parse empty MSI new data (req 3)" in {
      val entry = MsiData("|msi|2003/11/25 11:13:29|1068912754| 839|| | |", 1068850800000L, 1068850800000L + 1.day.toMillis)
      entry.timestamp must be(1068912754000L)
      entry.os_comm_id must be(839)
      entry.getSpeed must be(None)
    }
    "fail to parse MSI data without location (req 3)" in {
      val exception = intercept[MtmFormatException] {
        MsiData("|msi|2003/11/25 11:13:29|1068912754| || | |", 1068850800000L, 1068850800000L + 1.day.toMillis)
      }
      exception.message must be("Onverwacht MSI dataformat.")
      exception.line must be("|msi|2003/11/25 11:13:29|1068912754| || | |")
    }
    "fail to parse MSI data with incorrect timestamp" in {
      val exception = intercept[MtmFormatException] {
        MsiData("|msi|2003/11/25 11:13:29|1068912754| 839|| | |", 0L, 0L + 1.day.toMillis)
      }
      exception.message must be("MSI data voor verkeerde dag.")
      exception.line must be("|msi|2003/11/25 11:13:29|1068912754| 839|| | |")
    }
  }

  "OsData" must {
    "parse OS new data" in {
      val entry = OsData("|os|2003/11/25 13:24:53|1068912754|152|A13 L 8.400|nee1|nee2| on-line| geen4| ja5| ja6| ja7| ja8| ja9|onbekend10|", 1068850800000L, 1068850800000L + 1.day.toMillis)
      entry.tijdstip_s must be("1068912754")
      entry.timestamp must be(1068912754000L)
      entry.os_comm_id must be(152)
      entry.alt_msi_stand_ind must be("nee1")
      entry.fatale_fout_in_os_ind must be("nee2")
      entry.os_comm_stat must be("geen4")
      entry.btrw_msi_stand must be("ja5")
      entry.btrw_msi_status must be("ja6")
      entry.btrw_msi_lampstatus must be("ja7")
      entry.btrw_detstatus must be("ja8")
      entry.btrw_os_status must be("ja9")
      entry.os_comm_kwaliteit must be("onbekend10")
    }
    "parse OS empty data" in {
      val entry = OsData("|os|2003/11/25 13:24:53|1068912754|152|A13 L 8.400||| || | | | | ||", 1068850800000L, 1068850800000L + 1.day.toMillis)
      entry.tijdstip_s must be("1068912754")
      entry.os_comm_id must be(152)
      entry.alt_msi_stand_ind must be("")
      entry.fatale_fout_in_os_ind must be("")
      entry.os_comm_stat must be("")
      entry.btrw_msi_stand must be("")
      entry.btrw_msi_status must be("")
      entry.btrw_msi_lampstatus must be("")
      entry.btrw_detstatus must be("")
      entry.btrw_os_status must be("")
      entry.os_comm_kwaliteit must be("")
    }
    "fail to parse OS data without location (req 3)" in {
      val exception = intercept[MtmFormatException] {
        OsData("|os|2003/11/25 13:24:53|1068912754| |A13 L 8.400||| || | | | | ||", 1068850800000L, 1068850800000L + 1.day.toMillis)
      }
      exception.message must be("Onverwacht OS dataformat.")
      exception.line must be("|os|2003/11/25 13:24:53|1068912754| |A13 L 8.400||| || | | | | ||")
    }
    "fail to parse OS data with incorrect timestamp" in {
      val exception = intercept[MtmFormatException] {
        OsData("|os|2003/11/25 13:24:53|1068912754|152|A13 L 8.400||| || | | | | ||", 0L, 0L + 1.day.toMillis)
      }
      exception.message must be("OS data voor verkeerde dag.")
      exception.line must be("|os|2003/11/25 13:24:53|1068912754|152|A13 L 8.400||| || | | | | ||")
    }
  }

  "MtmDataParser" must {
    "parse nothing for no TOP data" in {
      val parser = getParser("no-TOP-data.txt", 1068850800000L)
      val result = parser.getTopData
      result must be(Nil)
    }
    "parse TOP data" in {
      val parser = getParser("TOP-format.txt", 1328569200000L)
      val result = parser.getTopData
      result.size must be(27)
      val (good, bad) = result.partition(_.isOperational)
      good.size must be(5)
      bad.size must be(22)
    }
    "fail for wrong date TOP content" in {
      val exception = intercept[MtmFormatException] {
        getParser("TOP-format.txt", 0L)
      }
      exception.message must be("TOP data voor verkeerde dag.")
      exception.line must be("|top|2003/11/25 08:41:33|1328569200| 10| 5| 08|")
    }
    "parse MSI data" in {
      val parser = getParser("msi-format.txt", 1068850800000L)
      val result = parser.getMsiData
      result.size must be(7)
      parser.getTopData.size must be(1)
    }
    "fail for wrong date MSI content" in {
      val exception = intercept[MtmFormatException] {
        getParser("msi-format.txt", 0L)
      }
      exception.message must be("MSI data voor verkeerde dag.")
      exception.line must be("|msi|2003/11/25 11:13:29|1068912754| 836|A13 L 7.200| 1|BL |")
    }
    "parse OS data" in {
      val parser = getParser("os-format.txt", 1068850800000L)
      val result = parser.getOsData
      result.size must be(6)
    }
    "fail for wrong date OS content" in {
      val exception = intercept[MtmFormatException] {
        getParser("os-format.txt", 0L)
      }
      exception.message must be("OS data voor verkeerde dag.")
      exception.line must be("|os|2003/11/25 13:24:53|1068912754|152|A13 L 8.400|nee|nee| local| geen| ja| ja| ja| ja| ja|onbekend|")
    }
    "parse all the data" in {
      val parser = getParser("all-from-spec.txt", 1068850800000L)
      parser.getMsiData.size must be(5)
      parser.getOsData.size must be(6)
      parser.getTopData.size must be(4)
    }
    "parse example data" in {
      val parser = getParser("A02L0397.txt", 1336860000000L)
      val msiData = parser.getMsiData
      msiData.size must be(363 - 38 + 1) //see line numbers
      val map: Map[Long, List[MsiData]] = msiData.groupBy[Long](msiEvent ⇒ msiEvent.timestamp)
      parser.getOsData.size must be(551 - 403 + 1)
      parser.getTopData.size must be(1)
    }
  }

  private def getParser(name: String, date: Long): MtmDataParser = {
    val input = getClass.getResourceAsStream("/" + name)
    val parser = new MtmDataParser(input, date, date + 1.day.toMillis)
    input.close()
    parser
  }
}

