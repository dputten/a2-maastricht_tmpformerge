package csc.sectioncontrol.mtm.nl

import csc.sectioncontrol.storagelayer.system.SystemService
import csc.sectioncontrol.storage.ZkSystem
import TestClassFactories.ZkSystemFactory
import csc.curator.utils.Versioned

class SystemServiceMock() extends SystemService {
  def get(systemId: String): Option[ZkSystem] = {
    Some(ZkSystemFactory.create)
  }
  override def exists(systemId: String): Unit = ()
  override def delete(systemId: String): Unit = ()
  override def update(systemId: String, mailServer: ZkSystem, version: Int): Unit = ()
  override def getVersioned(systemId: String): Option[Versioned[ZkSystem]] = null
}
