package csc.sectioncontrol.mtm.nl.msi

import akka.actor.{ Actor, ActorSystem, Props }
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import akka.util.duration._
import csc.akkautils.DirectLogging
import csc.sectioncontrol.enforce.{ MsiBlankEvent, PossibleDefectMSI }
import csc.sectioncontrol.messages.StatsDuration
import csc.sectioncontrol.mtm.nl._
import csc.sectioncontrol.notification.email.{ EmailData, EmailSender, MailServerData }
import csc.sectioncontrol.storage.{ ZkIndicationType, ZkMsiIdentification, ZkSchedule }
import org.jvnet.mock_javamail.Mailbox
import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, WordSpec }

/**
 * Helper object for the corridorconfigs containing the matrixIds which are equal to the os_comm_id in the MTM file for the msi rows.
 *
 * @return
 */
object CorridorConfigs {
  val schedulesOff = List(ZkSchedule("Msgsupport off", ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.False, 0, 0, 23, 59, 100, 100, false, None, false, 1))
  val schedules = List(ZkSchedule("Msgsupport on", ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.False, 0, 0, 23, 59, 100, 100, false, None, true, 1))
  val schedulesStartLater = List(ZkSchedule("Msgsupport off 08:00", ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.False, 8, 0, 23, 59, 100, 100, false, None, true, 1))
  val twoSchedules = List(
    ZkSchedule("Msgsupport on", ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.False, 0, 0, 0, 30, 100, 100, false, None, true, 1),
    ZkSchedule("Msgsupport off 08:00", ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.False, 8, 0, 23, 59, 100, 100, false, None, true, 1))

  def getCorridorConfig_1 = new CorridorConfig(1, List(836), Nil, schedules)
  def getCorridorConfig_1_with_blacklist = new CorridorConfig(1, List(836), List(ZkMsiIdentification(836, "2", "description")), schedules)
  def getCorridorConfig_2 = new CorridorConfig(2, List(837), Nil, schedules)
  def getCorridorConfig_3_parallel = new CorridorConfig(2, List(836), Nil, schedules)
}

/**
 * Because Msi is created from within an actor and it needs actor context for sending system events therefore a mock is created.
 */
class MockActor(msiFile: String) extends Actor {
  def receive = {
    case MockMessage ⇒
      val input = getClass.getResourceAsStream(msiFile)
      val mtmDataParser = new MtmDataParser(input, 1328569200000L, 1328569200000L + 1.day.toMillis)
      input.close()

      val timeNowMock = new NowMock
      timeNowMock.setNow(1)

      val msi = new Msi(List(CorridorConfigs.getCorridorConfig_1, CorridorConfigs.getCorridorConfig_2), mtmDataParser, Nil,
        "testSystemId", new SystemServiceMock, new MailServerServiceMock(), new UserServiceMock(), timeNowMock, "ProcessMtmData")

      msi.processBlankMessageSignalIndicators(context, 1328569200000L, Some(StatsDuration("20120528", "Europe/Amsterdam")))
    case _ ⇒
  }
}

class MsiTest extends TestKit(ActorSystem("MsiTest")) with WordSpec with MustMatchers with EmailSender with EmailTestUtils with BeforeAndAfterAll with DirectLogging {
  val entryNoBL_836_1_80 = MsiData("|msi|2012/02/07 00:00:00|1328569200| 836|A13 L 7.200| 1 | *80* | ", 1328569200000L, 1328569200000L + 1.day.toMillis)
  val entryNoBL_836_1_100 = MsiData("|msi|2012/02/07 00:00:00|1328569200| 836|A13 L 7.200| 1 | *100* | ", 1328569200000L, 1328569200000L + 1.day.toMillis)
  val entryBL_836_1_BL = MsiData("|msi|2012/02/07 00:00:00|1328569200| 836|A13 L 7.200| 1|BL |", 1328569200000L, 1328569200000L + 1.day.toMillis)
  val entryBL_836_1_BL_Later = MsiData("|msi|2012/02/07 01:00:00|1328572800| 836|A13 L 7.200| 1|BL |", 1328569200000L, 1328569200000L + 1.day.toMillis)
  val entryBL_836_2_BL = MsiData("|msi|2012/02/07 00:00:00|1328569200| 836|A13 L 7.300| 2|BL |", 1328569200000L, 1328569200000L + 1.day.toMillis)

  val entryNoBL_837_1_80 = MsiData("|msi|2012/02/07 00:00:00|1328569200| 837|A13 L 7.300| 1 | *80* | ", 1328569200000L, 1328569200000L + 1.day.toMillis)
  val entryNoBL_837_1_100 = MsiData("|msi|2012/02/07 00:00:00|1328569200| 837|A13 L 7.300| 1 | *100* | ", 1328569200000L, 1328569200000L + 1.day.toMillis)
  val entryBL_837_1_BL = MsiData("|msi|2012/02/07 00:00:00|1328569200| 837|A13 L 7.300| 1|BL |", 1328569200000L, 1328569200000L + 1.day.toMillis)

  def userServiceMock = new UserServiceMock

  val timeNowMock = new NowMock
  timeNowMock.setNow(1)

  override def afterAll() {
    system.shutdown()
  }

  "msi.processBlankMessageSignalIndicators" must {
    "produce no msi-event with in the complete integration cycle without blanks" in {
      val probe = TestProbe()
      system.eventStream.subscribe(probe.ref, classOf[MsiBlankEvent])
      val mockActor = TestActorRef(Props(new MockActor("/20120209-MTM2-A2-NO-blanks-complete.txt")))

      mockActor ! MockMessage

      probe.expectNoMsg()
      system.eventStream.unsubscribe(probe.ref)
    }

    "produce a msi-event with two messages in the complete integration cycle" in {
      val probe = TestProbe()
      system.eventStream.subscribe(probe.ref, classOf[MsiBlankEvent])
      val mockActor = TestActorRef(Props(new MockActor("/20120209-MTM2-A2-blanks-complete.txt")))

      mockActor ! MockMessage

      val msiBlanks = List(PossibleDefectMSI(836, "1", " A20 R  28.100 "), PossibleDefectMSI(837, "2", " A20 R  28.100 "))

      val expectedMsg = MsiBlankEvent(1328569200000L, msiBlanks, StatsDuration("20120528", "Europe/Amsterdam"))

      probe.expectMsg(5 seconds, expectedMsg)
      system.eventStream.unsubscribe(probe.ref)
    }
  }

  "send the mail correctly" in {
    val msiData = List[MsiData](entryBL_836_1_BL, entryBL_837_1_BL)
    val emailAddresses = List("user1@test.nl", "user2@test.nl")
    val msi = new Msi(List(CorridorConfigs.getCorridorConfig_1, CorridorConfigs.getCorridorConfig_2), null, Nil, "",
      new SystemServiceMock, new MailServerServiceMock(), userServiceMock, timeNowMock, "ProcessMtmData")

    Mailbox.clearAll()

    msi.createAndSendMail(1328569200000L, msi.getDefectMSIs(msiData))

    val emailData = new EmailData(
      subject = "Defecte signaalgever(s) testTitle",
      bodyText =
        "In het MTMruw bestand van 07-02-2012 tonen de volgende matrix signaalgevers van het traject testTitle de beeldstand BL:\n\nOs_comm_id: 836\nOs_locatie: A13 L 7.200\nMsi_rijstrook_pos: 1\nMSI_stand: BL\n\nOs_comm_id: 837\nOs_locatie: A13 L 7.300\nMsi_rijstrook_pos: 1\nMSI_stand: BL\n",
      cc = None,
      to = emailAddresses,
      bcc = None,
      from = Some("noreply@csc.com"))
    val inbox = Mailbox.get("user1@test.nl")
    val email = getEmail(inbox.get(0))
    try {
      checkEmail(email, emailData)
    } catch { case t: Throwable ⇒ t.printStackTrace() }
  }

  "msi.getToAddresses" must {
    "return the correct addresses" in {
      println("return the correct addresses")

      val msi = new Msi(List(CorridorConfigs.getCorridorConfig_1, CorridorConfigs.getCorridorConfig_2), null, Nil, "",
        new SystemServiceMock, new MailServerServiceMock(), userServiceMock, timeNowMock, "ProcessMtmData")
      val emailAddresses = List("user1@test.nl", "user2@test.nl")
      msi.getToAddresses must be(emailAddresses)
    }
  }

  "msi.createEmailData" must {
    "create EmailData correctly" in {
      println("create EmailData correctly")
      val msiData = List[MsiData](entryBL_836_1_BL, entryBL_837_1_BL)
      val msi = new Msi(List(CorridorConfigs.getCorridorConfig_1, CorridorConfigs.getCorridorConfig_2), null, Nil, "",
        new SystemServiceMock, new MailServerServiceMock(), userServiceMock, timeNowMock, "ProcessMtmData")
      val emailAddresses = List("aaaaa@aaaaa.nl", "bbbbb@bbbbb.nl")

      val result = msi.createEmailData(1328569200000L, msi.getDefectMSIs(msiData), emailAddresses)
      result.bcc must be(None)
      result.bodyText must be("In het MTMruw bestand van 07-02-2012 " +
        "tonen de volgende matrix signaalgevers van het traject testTitle de beeldstand BL:\n\n" +
        "Os_comm_id: 836\nOs_locatie: A13 L 7.200\nMsi_rijstrook_pos: 1\nMSI_stand: BL\n\n" +
        "Os_comm_id: 837\nOs_locatie: A13 L 7.300\nMsi_rijstrook_pos: 1\nMSI_stand: BL\n")
      result.cc must be(None)
      result.from must be(None)
      result.subject must be("Defecte signaalgever(s) testTitle")
      result.to must be(emailAddresses)
    }
  }

  "msi.createMailServerData" must {
    "create MailServerData correctly" in {
      println("create MailServerData correctly")
      val msi = new Msi(List(CorridorConfigs.getCorridorConfig_1), null, Nil, "", null, new MailServerServiceMock(), userServiceMock, timeNowMock, "ProcessMtmData")
      msi.createMailServerData must be(MailServerData("", properties = Some(Map("mail.smtp.socketFactory.class" -> "javax.net.ssl.SSLSocketFactor",
        "mail.smtp.socketFactory.port" -> "465",
        "mail.smtp.host" -> "localhost",
        "mail.smtp.starttls.enable" -> "false",
        "mail.smtp.auth" -> "true",
        "mail.from" -> "noreply@csc.com",
        "mail.smtp.port" -> "587"))))
    }
  }

  "msi.getmailServerProperties" must {
    "get the properties correctly" in {
      println("get the properties correctly")
      val msi = new Msi(List(CorridorConfigs.getCorridorConfig_1), null, Nil, "", null, new MailServerServiceMock(), userServiceMock, timeNowMock, "ProcessMtmData")
      msi.getmailServerProperties must be(Some(Map("mail.smtp.socketFactory.class" -> "javax.net.ssl.SSLSocketFactor",
        "mail.smtp.socketFactory.port" -> "465",
        "mail.smtp.host" -> "localhost",
        "mail.smtp.starttls.enable" -> "false",
        "mail.smtp.auth" -> "true",
        "mail.from" -> "noreply@csc.com",
        "mail.smtp.port" -> "587")))
    }
  }

  "msi.createSubject" must {
    "create the subject correctly" in {
      println("create the subject correctly")
      val msi = new Msi(List(CorridorConfigs.getCorridorConfig_1), null, Nil, "", new SystemServiceMock, null, userServiceMock, timeNowMock, "ProcessMtmData")
      msi.createSubject must be("Defecte signaalgever(s) testTitle")
    }
  }

  "msi.createBody" must {
    "create the body correctly" in {
      println("create the body correctly")
      val msiData = List[MsiData](entryBL_836_1_BL, entryBL_837_1_BL)
      val msi = new Msi(List(CorridorConfigs.getCorridorConfig_1, CorridorConfigs.getCorridorConfig_2), null, Nil, "", null, null, userServiceMock, timeNowMock, "ProcessMtmData")
      val blanks = msi.getDefectMSIs(msiData)

      blanks.length must be(2)

      msi.createBody("07-02-2012", "A13 L Overschie", blanks) must be("In het MTMruw bestand van 07-02-2012 " +
        "tonen de volgende matrix signaalgevers van het traject A13 L Overschie de beeldstand BL:\n\n" +
        "Os_comm_id: 836\nOs_locatie: A13 L 7.200\nMsi_rijstrook_pos: 1\nMSI_stand: BL\n\n" +
        "Os_comm_id: 837\nOs_locatie: A13 L 7.300\nMsi_rijstrook_pos: 1\nMSI_stand: BL\n")
    }
  }

  "msi.formatDate" must {
    "format the date correctly" in {
      println("format the date correctly")
      val msi = new Msi(List(CorridorConfigs.getCorridorConfig_1), null, Nil, "", null, null, userServiceMock, timeNowMock, "ProcessMtmData")
      msi.formatDate(1328569200000L) must be("07-02-2012")
    }
  }

  "msi.getDefectMSIs" must {
    "return nothing when there are no msi rows available" in {
      val msiData = List[MsiData]()
      val msi = new Msi(List(CorridorConfigs.getCorridorConfig_1), null, Nil, "", null, null, userServiceMock, timeNowMock, "ProcessMtmData")
      val result = msi.getDefectMSIs(msiData)

      result.length must be(0)
    }
    "return nothing when there are no schedules available" in {
      val msiData = List[MsiData](entryBL_836_2_BL)
      val configCorridor = CorridorConfigs.getCorridorConfig_1.copy(schedules = Nil)
      val msi = new Msi(List(configCorridor), null, Nil, "", null, null, userServiceMock, timeNowMock, "ProcessMtmData")
      val result = msi.getDefectMSIs(msiData)
      result.length must be(0)
    }
    "return nothing when there are no msg supported schedules available" in {
      val msiData = List[MsiData](entryBL_836_1_BL)
      val configCorridor = CorridorConfigs.getCorridorConfig_1.copy(schedules = CorridorConfigs.schedulesOff)
      val msi = new Msi(List(configCorridor), null, Nil, "", null, null, userServiceMock, timeNowMock, "ProcessMtmData")
      val result = msi.getDefectMSIs(msiData)
      result.length must be(0)
    }

    // simulate two msi rows with os_comm_id's belonging to corridor-1
    // - one row blank
    // - one row not blank
    "return nothing when schedule speed and blank is showing" in {
      val msiData = List[MsiData](entryNoBL_836_1_100, entryBL_836_1_BL_Later)
      val msi = new Msi(List(CorridorConfigs.getCorridorConfig_1), null, Nil, "", null, null, userServiceMock, timeNowMock, "ProcessMtmData")
      val result = msi.getDefectMSIs(msiData)
      result.length must be(0)
    }
    // simulate one msi row with os_comm_id belonging to corridor-2
    // - one row blank
    "return one defect for one msi row (with state BL) belonging to one lane in one corridor" in {
      val msiData = List[MsiData](entryBL_836_1_BL)
      val msi = new Msi(List(CorridorConfigs.getCorridorConfig_1), null, Nil, "", null, null, userServiceMock, timeNowMock, "ProcessMtmData")
      val result = msi.getDefectMSIs(msiData)

      result.length must be(1)
      result.head.msi_rijstr_pos must be("1")
      result.head.os_comm_id must be(836)
      result.head.os_locatie must be("A13 L 7.200")
    }

    "return one blank when schedule is started after correct speed" in {
      val msiData = List[MsiData](entryNoBL_836_1_100, entryBL_836_1_BL_Later)
      val configCorridor = CorridorConfigs.getCorridorConfig_1.copy(schedules = CorridorConfigs.schedulesStartLater)

      val msi = new Msi(List(configCorridor), null, Nil, "", null, null, userServiceMock, timeNowMock, "ProcessMtmData")
      val result = msi.getDefectMSIs(msiData)
      result.length must be(1)
      result.head.msi_rijstr_pos must be("1")
      result.head.os_comm_id must be(836)
      result.head.os_locatie must be("A13 L 7.200")
    }

    // simulate two msi rows with os_comm_id's belonging to corridor-1
    // - two rows blank
    "return one blank for two msi rows (with state BL) belonging to one lane in one corridor" in {
      val msiData = List[MsiData](entryBL_836_1_BL, entryBL_836_1_BL_Later)
      val msi = new Msi(List(CorridorConfigs.getCorridorConfig_1), null, Nil, "", null, null, userServiceMock, timeNowMock, "ProcessMtmData")
      val result = msi.getDefectMSIs(msiData)

      result.length must be(1)
      result.head.msi_rijstr_pos must be("1")
      result.head.os_comm_id must be(836)
      result.head.os_locatie must be("A13 L 7.200")
    }
    "return one blank for two msi rows (with state BL) belonging to one lane in two parrallel corridors" in {
      val msiData = List[MsiData](entryBL_836_1_BL, entryBL_836_1_BL_Later)
      val msi = new Msi(List(CorridorConfigs.getCorridorConfig_1, CorridorConfigs.getCorridorConfig_3_parallel), null, Nil, "", null, null, userServiceMock, timeNowMock, "ProcessMtmData")
      val result = msi.getDefectMSIs(msiData)

      result.length must be(1)
      result.head.msi_rijstr_pos must be("1")
      result.head.os_comm_id must be(836)
      result.head.os_locatie must be("A13 L 7.200")
    }

    "return one blank for two msi rows (with state BL) belonging to one lane in two schedules" in {
      val msiData = List[MsiData](entryBL_836_1_BL, entryBL_836_1_BL_Later)
      val configCorridor = CorridorConfigs.getCorridorConfig_1.copy(schedules = CorridorConfigs.twoSchedules)
      val msi = new Msi(List(CorridorConfigs.getCorridorConfig_1, CorridorConfigs.getCorridorConfig_3_parallel), null, Nil, "", null, null, userServiceMock, timeNowMock, "ProcessMtmData")
      val result = msi.getDefectMSIs(msiData)

      result.length must be(1)
      result.head.msi_rijstr_pos must be("1")
      result.head.os_comm_id must be(836)
      result.head.os_locatie must be("A13 L 7.200")
    }

    "return one blank for two msi rows (with state BL) belonging to one lane in two schedules one partly blank" in {
      val msiData = List[MsiData](entryNoBL_836_1_80, entryBL_836_1_BL_Later)
      val configCorridor = CorridorConfigs.getCorridorConfig_1.copy(schedules = CorridorConfigs.twoSchedules)
      val msi = new Msi(List(CorridorConfigs.getCorridorConfig_1, CorridorConfigs.getCorridorConfig_3_parallel), null, Nil, "", null, null, userServiceMock, timeNowMock, "ProcessMtmData")
      val result = msi.getDefectMSIs(msiData)

      result.length must be(1)
      result.head.msi_rijstr_pos must be("1")
      result.head.os_comm_id must be(836)
      result.head.os_locatie must be("A13 L 7.200")
    }

    "return one blank for two msi rows (with state BL) belonging to one lane in one schedule and other schedule is OK" in {
      val msiData = List[MsiData](entryNoBL_836_1_100, entryBL_836_1_BL_Later)
      val configCorridor = CorridorConfigs.getCorridorConfig_1.copy(schedules = CorridorConfigs.twoSchedules)
      val msi = new Msi(List(configCorridor), null, Nil, "", null, null, userServiceMock, timeNowMock, "ProcessMtmData")
      val result = msi.getDefectMSIs(msiData)
      result.length must be(1)
      result.head.msi_rijstr_pos must be("1")
      result.head.os_comm_id must be(836)
      result.head.os_locatie must be("A13 L 7.200")
    }
    "return nothing for two msi rows (with state BL) belonging to one lane in one schedule and other schedule is OK" in {
      val msiData = List[MsiData](entryNoBL_836_1_100)
      val configCorridor = CorridorConfigs.getCorridorConfig_1.copy(schedules = CorridorConfigs.twoSchedules)
      val msi = new Msi(List(CorridorConfigs.getCorridorConfig_1, CorridorConfigs.getCorridorConfig_3_parallel), null, Nil, "", null, null, userServiceMock, timeNowMock, "ProcessMtmData")
      val result = msi.getDefectMSIs(msiData)

      result.length must be(0)
    }

    "return two blanks for two msi rows (with state BL) belonging to two lanes in one corridor" in {
      val msiData = List[MsiData](entryBL_836_1_BL, entryBL_836_2_BL)
      val msi = new Msi(List(CorridorConfigs.getCorridorConfig_1), null, Nil, "", null, null, userServiceMock, timeNowMock, "ProcessMtmData")
      val result = msi.getDefectMSIs(msiData)
      println(result)
      result.size must be(2)
      val cor_836_1 = result.find(mu ⇒ mu.os_comm_id == 836 && mu.msi_rijstr_pos == "1").get
      cor_836_1.msi_rijstr_pos must be("1")
      cor_836_1.os_comm_id must be(836)
      cor_836_1.os_locatie must be("A13 L 7.200")

      val cor_836_2 = result.find(mu ⇒ mu.os_comm_id == 836 && mu.msi_rijstr_pos == "2").get
      cor_836_2.msi_rijstr_pos must be("2")
      cor_836_2.os_comm_id must be(836)
      cor_836_2.os_locatie must be("A13 L 7.300")
    }

    // simulate two msi rows with os_comm_id's belonging to corridor-1 and corridor-2
    // - one rows blank
    "return one blank for two msi rows (one state BL) for two corridors with their own lane" in {
      val msiData = List[MsiData](entryNoBL_836_1_80, entryBL_837_1_BL)
      val msi = new Msi(List(CorridorConfigs.getCorridorConfig_1, CorridorConfigs.getCorridorConfig_2), null, Nil, "", null, null, userServiceMock, timeNowMock, "ProcessMtmData")
      val result = msi.getDefectMSIs(msiData)

      result.length must be(1)
      result.head.msi_rijstr_pos must be("1")
      result.head.os_comm_id must be(837)
      result.head.os_locatie must be("A13 L 7.300")
    }

    // simulate two msi rows with os_comm_id's belonging to corridor-1 and corridor-2
    // - two rows blank
    "return two blanks for two msi rows (with state BL) belonging to two lanes in two corridor" in {
      val msiData = List[MsiData](entryBL_836_1_BL, entryBL_837_1_BL)
      val msi = new Msi(List(CorridorConfigs.getCorridorConfig_1, CorridorConfigs.getCorridorConfig_2), null, Nil, "", null, null, userServiceMock, timeNowMock, "ProcessMtmData")
      val result = msi.getDefectMSIs(msiData)

      result.length must be(2)
      val cor_836 = result.find(mu ⇒ mu.os_comm_id == 836).head
      cor_836.msi_rijstr_pos must be("1")
      cor_836.os_comm_id must be(836)
      cor_836.os_locatie must be("A13 L 7.200")

      val cor_837 = result.find(mu ⇒ mu.os_comm_id == 837).head
      cor_837.msi_rijstr_pos must be("1")
      cor_837.os_comm_id must be(837)
      cor_837.os_locatie must be("A13 L 7.300")
    }

    // simulate four msi rows with os_comm_id's belonging to corridor-1 and corridor-2
    // - two rows blank
    // - two rows other
    "return no blanks for four msi rows (with two state BL) belonging to two lanes in two corridor" in {
      val msiData = List[MsiData](entryBL_836_1_BL, entryNoBL_836_1_100, entryBL_837_1_BL, entryNoBL_837_1_100)
      val msi = new Msi(List(CorridorConfigs.getCorridorConfig_1, CorridorConfigs.getCorridorConfig_2), null, Nil, "", null, null, userServiceMock, timeNowMock, "ProcessMtmData")
      val result = msi.getDefectMSIs(msiData)

      result.length must be(0)
    }
    "return 2 blanks for four msi rows (with two state BL) and not equal to schedule speed" in {
      val msiData = List[MsiData](entryBL_836_1_BL, entryNoBL_836_1_80, entryBL_837_1_BL, entryNoBL_837_1_80)
      val msi = new Msi(List(CorridorConfigs.getCorridorConfig_1, CorridorConfigs.getCorridorConfig_2), null, Nil, "", null, null, userServiceMock, timeNowMock, "ProcessMtmData")
      val result = msi.getDefectMSIs(msiData)

      result.length must be(2)
      val cor_836 = result.find(mu ⇒ mu.os_comm_id == 836).head
      cor_836.msi_rijstr_pos must be("1")
      cor_836.os_comm_id must be(836)
      cor_836.os_locatie must be("A13 L 7.200")

      val cor_837 = result.find(mu ⇒ mu.os_comm_id == 837).head
      cor_837.msi_rijstr_pos must be("1")
      cor_837.os_comm_id must be(837)
      cor_837.os_locatie must be("A13 L 7.300")
    }

    // load two msi rows with os_comm_id's belonging to corridor-1 and corridor-2
    // - one row blank -> blacklisted lane2
    // - one row other
    "return no blanks for two loaded msi rows (with one state BL) belonging to two lanes in two corridor but one blacklisted lane" in {
      val input = getClass.getResourceAsStream("/20120209-MTM2-A2-blanks-blacklist.txt")
      val mtmDataParser = new MtmDataParser(input, 1328569200000L, 1328569200000L + 1.day.toMillis)
      input.close()

      val msi = new Msi(List(CorridorConfigs.getCorridorConfig_1_with_blacklist), mtmDataParser, Nil, "", null, null, userServiceMock, timeNowMock, "ProcessMtmData")
      val result = msi.getDefectMSIs()

      result.length must be(0)

    }

    // load three msi rows with os_comm_id's belonging to corridor-1 and corridor-2
    // - one row blank -> blacklisted lane2 -> 836
    // - one row blank -> belongs to excluded os_comm_id -> 837
    // - one row other -> 836
    "return no blanks for three loaded msi rows (with two state BL) belonging to two lanes in two corridor but one blacklisted lane and one excluded os_comm_id" in {
      val input = getClass.getResourceAsStream(
        "/20120209-MTM2-A2-blanks-blacklist-exclusions.txt")
      val mtmDataParser = new MtmDataParser(input, 1328569200000L, 1328569200000L + 1.day.toMillis)
      input.close()

      val msi = new Msi(List(CorridorConfigs.getCorridorConfig_1_with_blacklist, CorridorConfigs.getCorridorConfig_2), mtmDataParser, List(837), "", null, null, userServiceMock, timeNowMock, "ProcessMtmData")
      val result = msi.getDefectMSIs()

      result.length must be(0)

    }

    // load two msi rows with os_comm_id's belonging to corridor-1
    // - one row blank
    // - one row other
    "return one blank for two loaded msi rows (with one state BL) belonging to two lanes in one corridor" in {
      println("return one blank for two loaded msi rows (with one state BL) belonging to two lanes in one corridor")
      val input = getClass.getResourceAsStream("/20120209-MTM2-A2-blanks.txt")
      val mtmDataParser = new MtmDataParser(input, 1328569200000L, 1328569200000L + 1.day.toMillis)
      input.close()

      val msi = new Msi(List(CorridorConfigs.getCorridorConfig_1), mtmDataParser, Nil, "", new SystemServiceMock, new MailServerServiceMock, userServiceMock, timeNowMock, "ProcessMtmData")
      val result = msi.getDefectMSIs()

      result.length must be(1)
    }
  }
}
