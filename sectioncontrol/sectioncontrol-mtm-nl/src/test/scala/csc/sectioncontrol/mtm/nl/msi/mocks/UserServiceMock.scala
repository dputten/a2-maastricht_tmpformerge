package csc.sectioncontrol.mtm.nl

import csc.sectioncontrol.storagelayer.user.UserService
import csc.sectioncontrol.storage.ZkUser
import csc.curator.utils.Versioned

class UserServiceMock extends UserService {
  def getByRole(roleName: String): Seq[Option[ZkUser]] = {
    val user1 = new ZkUser("1", "", "", "", "", "", "user1@test.nl", "", "1", Nil)
    val user2 = new ZkUser("2", "", "", "", "", "", "user2@test.nl", "", "1", Nil)

    Seq(Some(user1), Some(user2))
  }

  override def getAll(): Seq[Option[ZkUser]] = null
  override def exists(userId: String): Unit = ()
  override def delete(userId: String): Unit = ()
  override def update(userId: String, user: ZkUser, version: Int): Unit = ()
  override def getVersioned(userId: String): Option[Versioned[ZkUser]] = null
  override def get(userId: String): Option[ZkUser] = null
  override def create(user: ZkUser): Unit = ()
}
