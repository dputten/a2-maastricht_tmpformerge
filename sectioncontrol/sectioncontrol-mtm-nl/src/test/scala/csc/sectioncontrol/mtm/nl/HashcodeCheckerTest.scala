/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.mtm.nl

import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers
import akka.testkit.TestActorRef
import akka.actor.ActorSystem

class HashcodeCheckerTest extends WordSpec with MustMatchers {

  "HashcodeChecker" must {

    "report wrong hashcode" in {
      val input = getClass.getResourceAsStream("/all-from-spec.txt")
      val (valid, _) = ProcessMtmData.isHashcodeValid(input)
      input.close()
      valid must be(false)
    }

    "report proper hashcode" in {
      val input = getClass.getResourceAsStream("/20120209-MTM2-A20-bestand.txt")
      val (valid, _) = ProcessMtmData.isHashcodeValid(input)
      input.close()
      valid must be(true)
    }
  }
}

