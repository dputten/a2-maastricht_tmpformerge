/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.mtm.nl

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.enforce.SpeedIndicatorStatus
import akka.util.duration._
import csc.sectioncontrol.storage.{ ZkIndicationType, ZkSchedule, ZkMsiIdentification }

class MtmProcessorTest extends WordSpec with MustMatchers {

  "OS data" must {

    "report as bad" in {
      val osData = OsData("|os|2012/02/07 07:21:33|1328595693|       383| A20 R  28.100 |nee|nee|on-line|  goed| ja| ja| ja| ja| ja|  slecht|", 1328569200000L, 1328655600000L)
      osData.isOperational must be(false)
    }
    "report as good" in {
      val osData = OsData("|os|2012/02/07 07:21:33|1328595693|       383| A20 R  28.100 |nee|nee|on-line|  goed| ja| ja| ja| ja| ja|  goed|", 1328569200000L, 1328655600000L)
      osData.isOperational must be(true)
    }
  }
  "MtmProcessor with spec data" must {

    "fold no TOP System Events" in {
      val input = getClass.getResourceAsStream("/no-TOP-data.txt")
      val parser = new MtmDataParser(input, 1328569200000L, 1328655600000L)
      input.close()
      val processor = new MtmCorridorProcessor(getA20config(1), parser)
      val result = processor.getTopSystemEvents
      result must be(Nil)
    }
    "fold TOP System Events" in {
      val input = getClass.getResourceAsStream("/all-from-spec.txt")
      val parser = new MtmDataParser(input, 1068850800000L, 1068850800000L + 86400000L)
      input.close()
      val processor = new MtmCorridorProcessor(getA20config(2), parser)
      val result = processor.getTopSystemEvents
      result.size must be(1)
    }
    "fold OS data" in {
      val processor = getProcessor("all-from-spec.txt", new CorridorConfig(3, List(152, 153), Nil), 1068850800000L)
      val result = processor.getOsSystemEvents
      result.size must be(2)
      val dataToCheck = result.map(data ⇒ (data.effectiveTimestamp, data.suspend))
      val d1 :: d2 :: Nil = dataToCheck
      d1 must be((1068912754000L, true))
      d2 must be((1068914554000L, false))
    }

    "read init MSI data" in {
      val processor = getProcessor("all-from-spec.txt", new CorridorConfig(4, List(835, 836), Nil), 1068850800000L)
      val result = processor.getMsiEvents
      result.size must be(1)
      val event :: Nil = result
      event.effectiveTimestamp must be(1068912754000L)
      event.corridorId must be(4)
      event.speedSetting must be(Right(None))
    }
  }

  "MtmProcessor with example data" must {

    "fold TOP System Events" in {
      val processor = getProcessor("20120209-MTM2-A2.txt", getA20config(5), 1328569200000L)
      val result = processor.getTopSystemEvents
      result.size must be(7)
      val dataToCheck = result.map(data ⇒ (data.effectiveTimestamp, data.suspend))
      val d1 :: d2 :: _ = dataToCheck
      d1 must be((1328569200000L, false))
      d2 must be((1328610323000L, true))
      dataToCheck.last must be((1328652885000L, false))
    }
    "fold OS data" in {
      val processor = getProcessor("20120209-MTM2-A2.txt", getA20config(6), 1328569200000L)
      val result = processor.getOsSystemEvents
      result.size must be(7)
      val dataToCheck = result.map(data ⇒ (data.effectiveTimestamp, data.suspend))
      val d1 :: d2 :: _ = dataToCheck
      d1 must be((1328569200000L, false))
      d2 must be((1328595693000L, true))
      dataToCheck.last must be((1328653072000L, false))
    }
    "read example MSI data" in {
      val input = getClass.getResourceAsStream("/20120209-MTM2-A2.txt")
      val parser = new MtmDataParser(input, 1328569200000L, 1328655600000L)
      input.close()
      val processor = new MtmCorridorProcessor(getA20config(7), parser, List(388))
      val result = processor.getMsiEvents
      result.size must be(69)
      val dataToCheck = result.map(data ⇒ (data.effectiveTimestamp, data.speedSetting))
      val d1 :: d2 :: d3 :: d4 :: _ = dataToCheck
      d1 must be((1328569200000L, Right(Some(80))))
      d2 must be((1328592023000L, Left(SpeedIndicatorStatus.Undetermined)))
      d3 must be((1328592096000L, Right(Some(80))))
      d4 must be((1328592546000L, Left(SpeedIndicatorStatus.Undetermined)))
      dataToCheck.last must be((1328634822000L, Right(Some(80))))
    }

    "read example MSI data and blacklist given MSIs" in {
      val input = getClass.getResourceAsStream("/20120209-MTM2-A2-blacklist.txt")
      val parser = new MtmDataParser(input, 1328569200000L, 1328655600000L)
      input.close()
      val processor = new MtmCorridorProcessor(getA20config_alt(8), parser, List(388))
      val result = processor.getMsiEvents
      result.size must be(71)
      val dataToCheck = result.map(data ⇒ (data.effectiveTimestamp, data.speedSetting))
      val d1 :: d2 :: d3 :: d4 :: _ = dataToCheck
      d1 must be((1328569200000L, Right(Some(80))))
      d2 must be((1328592024000L, Left(SpeedIndicatorStatus.Undetermined)))
      d3 must be((1328592088000L, Right(Some(80))))
      d4 must be((1328592546000L, Left(SpeedIndicatorStatus.Undetermined)))
      dataToCheck.last must be((1328634822000L, Right(Some(80))))
    }

    "fail to read example OS data for location with incomplete initial data" in {
      val processor = getProcessor("MTM2-A2-incomplete-OS-init.txt", new CorridorConfig(9, List(383, 387, 388, 1325), Nil), 1328569200000L)
      val exception = intercept[MtmFormatException] {
        processor.getOsSystemEvents
      }
      exception.message must be("Onbekende OS data. de locatie is niet gedefinieerd in de begin status (388).")
      exception.line must be("|os|2012/02/07 22:20:45|1328649645|       388| A20 R  28.800 |nee|nee|  local|  goed| ja| ja| ja| ja| ja|    goed|")
    }
    "fail to read example MSI data for location with incomplete initial data" in {
      val processor = getProcessor("20120209-MTM2-A2-invalid-location.txt", new CorridorConfig(10, List(383, 387, 1325), Nil), 1328569200000L)
      val exception = intercept[MtmFormatException] {
        processor.getMsiEvents
      }
      exception.message must be("Onbekende MSI data. Rijstrook 1 is op portaal 1325 is niet gedefinieerd in de begin status.")
      exception.line must be("|msi|2012/02/07 06:21:28|1328592088|      1325| A20 R  28.570 |             1|/80/     |")
    }
  }

  "MtmProcessor with real A2 data" must {
    val r1 = 989 to 997
    val r2 = 1176 to 1189
    val list = r1.toList ++ r2.toList ++ List(1019, 1020, 1021, 1028)
    list.size must be(27)
    val processor = getProcessor("A02L0397.txt", new CorridorConfig(11, list, Nil), 1336860000000L)

    "find exactly one TOP system event" in {
      val result = processor.getTopSystemEvents
      result.size must be(1)
      val dataToCheck = result.map(data ⇒ (data.effectiveTimestamp, data.suspend))
      val d1 :: Nil = dataToCheck
      d1 must be((1336860000000L, false))
    }

    "find three OS system events" in {
      val result = processor.getOsSystemEvents
      result.size must be(3)
    }

    "find speed events" in {
      val expected: Seq[Either[SpeedIndicatorStatus.Value, Option[Int]]] = Seq(
        Right(None), Right(Some(90)), Left(SpeedIndicatorStatus.Undetermined),
        Right(Some(90)), Right(None), Left(SpeedIndicatorStatus.Undetermined),
        Right(Some(90)), Right(None), Left(SpeedIndicatorStatus.Undetermined),
        Right(Some(90)), Right(None), Right(Some(90)),
        Left(SpeedIndicatorStatus.Undetermined), Right(Some(90)), Right(None))
      val result = processor.getMsiEvents
      result.size must be(15)
      val speedSettings = result.map(_.speedSetting)
      speedSettings must be(expected)
    }
  }

  "MtmProcessor with mixed (but empty) initial state" must {
    val r1 = 989 to 997
    val processor = getProcessor("A02L0397-mixed-initial-state.txt", new CorridorConfig(12, r1.toList, Nil), 1336860000000L)

    "find exactly one speed event - empty (no dynamic speed enforcement)" in {
      val result = processor.getMsiEvents
      result.size must be(1)
      val event :: Nil = result
      event.speedSetting must be(Right(None))
    }
  }

  "MtmProcessor with mixed initial state" must {
    val r1 = 989 to 997
    val processor = getProcessor("A02L0397-inconsistent-initial-state.txt", new CorridorConfig(13, r1.toList, Nil), 1336860000000L)

    "find exactly one speed event - inconsistent" in {
      val result = processor.getMsiEvents
      result.size must be(1)
      val event :: Nil = result
      event.speedSetting must be(Left(SpeedIndicatorStatus.Undetermined))
    }
  }

  "MtmProcessor with 3 states" must {

    "find 3 states - empty, 100, empty" in {
      val processor = getProcessor("empty-100-empty-state.txt", new CorridorConfig(14, List(989, 990), Nil), 1336860000000L)
      val result = processor.getMsiEvents
      result.size must be(3)
      val first :: second :: third :: Nil = result
      first.speedSetting must be(Right(None))
      second.speedSetting must be(Right(Some(100)))
      third.speedSetting must be(Right(None))
    }

    "find 3 states - empty, inconsistent , 100" in {
      val processor = getProcessor("empty-inconsistent-100-state.txt", new CorridorConfig(15, List(989, 990), Nil), 1336860000000L)
      val result = processor.getMsiEvents
      result.size must be(3)
      val first :: second :: third :: Nil = result
      first.speedSetting must be(Right(None))
      second.speedSetting must be(Left(SpeedIndicatorStatus.Undetermined))
      third.speedSetting must be(Right(Some(100)))
    }
  }

  "MtmProcessor with 7 states" must {
    // Old-style processor, without schedules
    val processor = getProcessor("empty-inconsistent-speed-state.txt", new CorridorConfig(16, List(989, 990), Nil), 1336860000000L)

    // New-style processor with schedule. First schedule (00:00:00 thru 00:05:00) allows for 1 blank msg.
    // second schedule (00:06:00 thru 23:59:00) does not allow for blanks.
    val processor2 = getProcessor("empty-inconsistent-speed-state.txt", new CorridorConfig(17, List(989, 990), Nil,
      List(ZkSchedule("AllDay", ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.False, 00, 00, 00, 05, 100, 100, false, None, true, 1),
        ZkSchedule("AllDay", ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.False, 00, 06, 23, 59, 100, 100, false, None, true, 0))), 1336860000000L)

    "find 6 states - empty, 100, inconsistent, 100, 90, empty" in {
      val result = processor.getMsiEvents
      result.size must be(6)
      val first :: second :: third :: forth :: fifth :: sixth :: Nil = result
      first.speedSetting must be(Right(None))
      second.speedSetting must be(Right(Some(100)))
      third.speedSetting must be(Left(SpeedIndicatorStatus.Undetermined))
      forth.speedSetting must be(Right(Some(100)))
      fifth.speedSetting must be(Right(Some(90)))
      sixth.speedSetting must be(Right(None))
    }

    "find 5 states - inconsistent, 100, inconsistent, 90, inconsistent" in {
      val result = processor2.getMsiEvents
      result.size must be(5)
      val first :: second :: third :: forth :: fifth :: Nil = result

      first.effectiveTimestamp must be(1336860000000L)
      first.speedSetting must be(Left(SpeedIndicatorStatus.Undetermined))

      second.effectiveTimestamp must be(1336860010000L)
      second.speedSetting must be(Right(Some(100)))

      third.effectiveTimestamp must be(1336860060000L)
      third.speedSetting must be(Left(SpeedIndicatorStatus.Undetermined))

      forth.effectiveTimestamp must be(1336860180000L)
      forth.speedSetting must be(Right(Some(90)))

      fifth.effectiveTimestamp must be(1336860240000L)
      fifth.speedSetting must be(Left(SpeedIndicatorStatus.Undetermined))
    }
  }
  "TCM-99: MtmProcessor with multiple schedules" must {
    "support joining schedules with stable MSG with too many blanks" in {
      val processor = getProcessor("stable-blankand100-state.txt", new CorridorConfig(17, List(989, 990), Nil,
        List(ZkSchedule("Msgsupport off", ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.False, 0, 0, 0, 5, 100, 100, false, None, false, 1),
          ZkSchedule("MsgSupport on", ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.False, 0, 6, 23, 59, 100, 100, false, None, true, 1))), 1336860000000L)

      val result = processor.getMsiEvents
      result.size must be(2)
      val first :: second :: Nil = result
      first.effectiveTimestamp must be(1336860000000L)
      first.speedSetting must be(Right(Some(100)))
      second.effectiveTimestamp must be(1336860000000L + 6 * 60 * 1000)
      second.speedSetting must be(Left(SpeedIndicatorStatus.Undetermined))
    }
    "support joining schedules with stable MSG and acceptable blanks resulting in one speed" in {
      val processor = getProcessor("stable-blankand100-state.txt", new CorridorConfig(17, List(989, 990), Nil,
        List(ZkSchedule("Msgsupport off", ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.False, 00, 00, 00, 05, 100, 100, false, None, false, 1),
          ZkSchedule("MsgSupport on", ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.False, 00, 06, 23, 59, 100, 100, false, None, true, 2))), 1336860000000L)

      val result = processor.getMsiEvents
      result.size must be(1)
      val first = result.head
      first.effectiveTimestamp must be(1336860000000L)
      first.speedSetting must be(Right(Some(100)))
    }
    "support gaps in schedule with stable MSG with too many blanks" in {
      try {
        val processor = getProcessor("stable-blankand100-state.txt", new CorridorConfig(17, List(989, 990), Nil,
          List(ZkSchedule("Msgsupport off", ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.False, 0, 0, 0, 5, 100, 100, false, None, false, 1),
            ZkSchedule("MsgSupport on", ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.False, 1, 6, 23, 59, 100, 100, false, None, true, 1))), 1336860000000L)

        val result = processor.getMsiEvents
        result.size must be(2) //? or should there be a record for when there is no schedule
        val first :: second :: Nil = result
        first.effectiveTimestamp must be(1336860000000L)
        first.speedSetting must be(Right(Some(100)))
        second.effectiveTimestamp must be(1336860000000L + 3600000 + 6 * 60 * 1000)
        second.speedSetting must be(Left(SpeedIndicatorStatus.Undetermined))
      } catch {
        case t: Throwable ⇒ t.printStackTrace()
      }
    }
    "support gaps in schedule with 3 MSG states when schedule change within MSG speed" in {
      val processor = getProcessor("3-stateChanges.txt", new CorridorConfig(17, List(989, 990), Nil,
        List(ZkSchedule("Msgsupport off", ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.False, 1, 6, 10, 0, 100, 100, false, None, false, 1),
          ZkSchedule("MsgSupport on", ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.False, 13, 0, 23, 59, 100, 100, false, None, true, 1))), 1336860000000L)

      val result = processor.getMsiEvents
      result.size must be(3)
      val first :: second :: third :: Nil = result
      first.effectiveTimestamp must be(1336860000000L)
      first.speedSetting must be(Right(Some(90)))
      second.effectiveTimestamp must be(1336860000000L + 3600000)
      second.speedSetting must be(Right(Some(100)))
      third.effectiveTimestamp must be(1336860000000L + 12 * 3600000)
      third.speedSetting must be(Right(Some(120)))
    }
    "support gaps in schedule with 3 MSG states when MSG change within schedule" in {
      val processor = getProcessor("3-stateChanges.txt", new CorridorConfig(17, List(989, 990), Nil,
        List(ZkSchedule("Msgsupport off", ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.False, 1, 6, 10, 0, 100, 100, false, None, false, 1),
          ZkSchedule("MsgSupport on", ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.None, ZkIndicationType.False, 11, 0, 23, 59, 100, 100, false, None, true, 1))), 1336860000000L)

      val result = processor.getMsiEvents
      result.size must be(4)
      val first :: second :: third :: fourth :: Nil = result
      first.effectiveTimestamp must be(1336860000000L)
      first.speedSetting must be(Right(Some(90)))
      second.effectiveTimestamp must be(1336860000000L + 3600000)
      second.speedSetting must be(Right(Some(100)))
      third.effectiveTimestamp must be(1336860000000L + 11 * 3600000)
      third.speedSetting must be(Left(SpeedIndicatorStatus.Undetermined))
      fourth.effectiveTimestamp must be(1336860000000L + 12 * 3600000)
      fourth.speedSetting must be(Right(Some(120)))
    }
  }

  private def getProcessor(name: String, config: CorridorConfig, expectedDate: Long): MtmCorridorProcessor = {
    val input = getClass.getResourceAsStream("/" + name)
    val parser = new MtmDataParser(input, expectedDate, expectedDate + 1.day.toMillis)
    input.close()
    new MtmCorridorProcessor(config, parser)
  }

  //do not include A20b data
  def getA20config(id: Int) = new CorridorConfig(id, List(383, 387, 1325, 388, 389, 390, 391, 392, 393, 1587, 395), Nil)
  def getA20config_alt(id: Int) = new CorridorConfig(id, List(383, 387, 1325, 388, 389, 390, 391, 392, 393, 1587, 395),
    List(ZkMsiIdentification(1325, "3", "description"), ZkMsiIdentification(1325, "4", "otherDecription")))
}