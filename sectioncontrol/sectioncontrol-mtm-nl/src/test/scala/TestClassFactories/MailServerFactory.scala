package csc.sectioncontrol.mtm.nl

import csc.sectioncontrol.messages.MailServer

object MailServerFactory {
  def createMailServer = MailServer(Some(Map(
    "mail.smtp.socketFactory.class" -> "javax.net.ssl.SSLSocketFactor",
    "mail.smtp.socketFactory.port" -> "465",
    "mail.smtp.host" -> "localhost",
    "mail.smtp.starttls.enable" -> "false",
    "mail.smtp.auth" -> "true",
    "mail.from" -> "noreply@csc.com",
    "mail.smtp.port" -> "587")))
  def createMailServerForUpdate = MailServer(Some(Map("mail.smtp.socketFactory.class" -> "javax.net.ssl.SSLSocketFactor",
    "mail.smtp.socketFactory.port" -> "465",
    "mail.smtp.host" -> "localhost",
    "mail.smtp.starttls.enable" -> "false",
    "mail.smtp.auth" -> "true",
    "mail.from" -> "noreply@csc.com",
    "mail.smtp.port" -> "666")))
}
