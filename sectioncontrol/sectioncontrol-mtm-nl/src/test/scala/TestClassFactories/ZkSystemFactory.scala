package TestClassFactories

import csc.sectioncontrol.messages.EsaProviderType
import csc.sectioncontrol.storage.{ SerialNumber, ZkSystem, ZkSystemLocation, ZkSystemRetentionTimes, _ }

object ZkSystemFactory {
  def create = new ZkSystem(
    id = "testSystemId",
    name = "testName",
    title = "testTitle",
    mtmRouteId = "testMtmRouteId",
    violationPrefixId = "A123",
    location = ZkSystemLocation(
      description = "testDescription",
      viewingDirection = Some(""),
      region = "testRegion",
      roadNumber = "testRoadNumber",
      roadPart = "testRoadPart",
      systemLocation = "Amsterdam"),
    serialNumber = SerialNumber("testSerialNumber"),
    orderNumber = 13,
    maxSpeed = 15,
    compressionFactor = 17,
    retentionTimes = ZkSystemRetentionTimes(
      nrDaysKeepTrafficData = 18,
      nrDaysKeepViolations = 19,
      nrDaysRemindCaseFiles = 3),
    pardonTimes = ZkSystemPardonTimes(pardonTimeEsa_secs = 200, pardonTimeTechnical_secs = 201),
    esaProviderType = EsaProviderType.NoProvider,
    minimumViolationTimeSeparation = 0)
}
