package csc.sectioncontrol.mtm.nl

import csc.akkautils.DirectLogging
import csc.sectioncontrol.messages.{ StatsDuration, SystemEvent }
import csc.sectioncontrol.notification.email.{ EmailData, EmailSender, MailServerData }
import csc.sectioncontrol.storagelayer.mail.MailServerService
import csc.sectioncontrol.storagelayer.system.SystemService
import csc.sectioncontrol.storagelayer.user.UserService
import java.text.SimpleDateFormat
import java.util.{ Calendar, TimeZone }

import csc.sectioncontrol.enforce.{ MsiBlankEvent, PossibleDefectMSI }
import csc.sectioncontrol.storage.ZkSchedule

/**
 * Class responsible for returning all lanes who's msi status has been blank all day
 */
class Msi(corridorConfigs: List[CorridorConfig],
          mtmDataParser: MtmDataParser,
          exclusions: List[Int] = Nil,
          systemId: String,
          systemService: SystemService,
          mailServerService: MailServerService,
          userService: UserService,
          timeNow: TimeNow,
          componentId: String) extends DirectLogging with EmailSender {

  var _mtmDataParser = mtmDataParser

  /**
   * A defect MSG is specified during a schedule period when:
   * - the "supportMSG" is of that scheduleperiode is "on" AND
   * - the MSGs
   *    - is part of the schedule corridor AND
   *    - is not excluded AND
   *    - showed BL(ank) during (a part of) the scheduleperiod AND
   *    - did not showed the schedulespeed during that scheduleperiod.
   *
   * @param allMsiData
   * @return
   */
  def getDefectMSIs(allMsiData: List[MsiData] = getRelevantMsiData): List[PossibleDefectMSI] = {
    log.debug("All MSI rows: " + allMsiData)

    val sortedMSI = allMsiData.groupBy(_.os_comm_id)
    //loop over corridors
    val defects = corridorConfigs.flatMap(corridor ⇒ {
      //loop over schedules
      corridor.schedules.flatMap(schedule ⇒ {
        if (schedule.supportMsgBoard) {
          //need to check msi's
          //select msi
          val blacklist = corridor.msiBlackList.map(_.os_comm_id)
          val processMSI = corridor.matrixIds.filterNot(id ⇒ blacklist.contains(id))
          //loop over MSI's
          processMSI.flatMap(comm_id ⇒ {
            sortedMSI.get(comm_id) match {
              case Some(msiChanges) ⇒ {
                val lanes = msiChanges.groupBy(_.msi_rijstr_pos)
                lanes.flatMap {
                  case (rijstr_pos, list) if (isMSIDefect(schedule, list)) ⇒ {
                    val msiKey = list.head.getKey
                    Seq(PossibleDefectMSI(msiKey.os_comm_id, msiKey.msi_rijstr_pos, msiKey.os_locatie))
                  }
                  case other ⇒ Seq()
                }
              }
              case other ⇒ Seq()
            }
          })
        } else {
          Seq()
        }
      })
    })
    val possibleDefects = defects.distinct.sortBy(_.os_comm_id)
    log.info("All MSI's that have been blank all day: " + possibleDefects)
    possibleDefects
  }

  /**
   * checks is the MSG showed during the schedule period
   *     BL(ank) during (a part of) the scheduleperiod AND
   *     did not showed the schedulespeed
   *
   * @param schedule the current schedule
   * @param msiChanges all the changes for specified MSI
   * @return true there was a blank and no schedule speed during schedule, false otherwise
   */
  private def isMSIDefect(schedule: ZkSchedule, msiChanges: List[MsiData]): Boolean = {
    if (msiChanges.isEmpty)
      false

    val minute = 60000
    val dayTime = msiChanges.head.timestamp
    val startSchedule = ZkSchedule.getTime(dayTime, schedule.fromPeriodHour, schedule.fromPeriodMinute)
    val endSchedule = ZkSchedule.getTime(dayTime, schedule.toPeriodHour, schedule.toPeriodMinute) + minute
    //select msi data in this schedule (including the one before schedule start when not equal)
    val sorted = msiChanges.sortBy(_.timestamp)
    val msiWithinSchedule = sorted.filter(msi ⇒ msi.timestamp >= startSchedule && msi.timestamp < endSchedule)
    //do we need the msi before start shedule
    val timeList = msiWithinSchedule.map(_.timestamp)
    val msiBeforeSchedule = if (timeList.contains(startSchedule)) {
      //contains a msi at start schedule => don need the msi before the schedule
      None
    } else {
      //get the msi before schedule start
      sorted.reverse.find(_.timestamp < startSchedule)
    }
    val allRelevantMsiData = msiBeforeSchedule ++ msiWithinSchedule

    //do the check has blanks and not schedule Speed
    val scheduleSpeed = schedule.signSpeed
    val msiWithScheduleSpeed = allRelevantMsiData.filter(_.getSpeed.exists(_ == scheduleSpeed))
    val msiWithblanks = allRelevantMsiData.filter(_.isBlank)
    //only true when there are blanks but not ScheduleSpeeds
    msiWithScheduleSpeed.isEmpty && msiWithblanks.nonEmpty
  }

  /**
   * Filter all the MSI's not relevant because
   * - they are excluded
   * - are blackListed
   * - do not belong to these sections
   *
   * @return
   */
  private def getRelevantMsiData: List[MsiData] = {
    val relevantMsiData = corridorConfigs.flatMap(cc ⇒
      _mtmDataParser.getMsiData.filter(data ⇒ {
        !exclusions.contains(data.os_comm_id) &&
          !cc.msiBlackList.contains(data.id) &&
          cc.matrixIds.contains(data.os_comm_id)
      }))

    log.debug("relevantMsiData: " + relevantMsiData)
    relevantMsiData
  }

  /**
   * Send system events for each all day blank msi.
   * This is for the day report
   *
   * @return
   */
  def sendEventForReport(expectedTimeFrom: Long, blanks: List[PossibleDefectMSI], configs: List[CorridorConfig], componentId: String, systemId: String, context: akka.actor.ActorContext, statsKey: Option[StatsDuration]) {
    context.system.eventStream.publish(MsiBlankEvent(expectedTimeFrom, blanks, statsKey.get))
  }

  /**
   * Send system events for each all day blank msi.
   * This is for the day report
   *
   * @return
   */
  def sendMailFailureEventForReport(expectedTimeFrom: Long, blanks: List[PossibleDefectMSI], configs: List[CorridorConfig], componentId: String, systemId: String, context: akka.actor.ActorContext, statsKey: Option[StatsDuration]) {
    context.system.eventStream.publish(SystemEvent(componentId, System.currentTimeMillis(),
      systemId, "Email Msi", "system", Some("Versturen van email voor defecte signaalgever(s) is mislukt.")))
  }

  private val maxTries = 3

  /**
   * Send mail for all day blanks msi.
   *
   * @return
   */
  private def sendMail(emailData: EmailData, mailServerData: MailServerData): Boolean = {
    def sendMailTry(tryNumber: Int): Boolean = {
      if (tryNumber > maxTries)
        false
      else {
        sendEmail(emailData, mailServerData) match {
          case Right(_) ⇒ true
          case Left(exception) ⇒
            log.warning("Sending email for defect (blank all day) MSI's failed for system %s: %s".format(systemId, exception.getMessage))

            sendMailTry(tryNumber + 1)
        }
      }
    }
    sendMailTry(1)
  }

  /**
   * Entry point for handling all day blank msi.
   * - get the blanks
   * - send the mail to users with the role->tactisch beheer
   * - send the system events for reporting
   *
   * @return
   */
  def processBlankMessageSignalIndicators(context: akka.actor.ActorContext, expectedTimeFrom: Long, statsKey: Option[StatsDuration]) = {
    val msiBlanks = getDefectMSIs()
    if (!msiBlanks.isEmpty) {
      if (!createAndSendMail(expectedTimeFrom, msiBlanks)) {
        sendMailFailureEventForReport(expectedTimeFrom, msiBlanks, corridorConfigs, componentId, systemId, context, statsKey)
      }
      sendEventForReport(expectedTimeFrom, msiBlanks, corridorConfigs, componentId, systemId, context, statsKey)
    }
  }

  def createAndSendMail(expectedTimeFrom: Long, blanks: List[PossibleDefectMSI]): Boolean = {
    createSubject

    val emailData = createEmailData(expectedTimeFrom, blanks, getToAddresses)
    val mailServerData = createMailServerData

    log.debug("create Msi mail with following EmailData:" + emailData)
    log.debug("create Msi mail with following MailServerData:" + mailServerData)

    sendMail(emailData, mailServerData)
  }

  def getToAddresses: Seq[String] = {
    for {
      user ← userService.getByRole("Tactisch beheer")
      email ← user.map(_.email)
    } yield email
  }

  def createEmailData(expectedTimeFrom: Long, blanks: List[PossibleDefectMSI], emailAddresses: Seq[String]): EmailData =
    EmailData(createSubject, createBody(formatDate(expectedTimeFrom), getSystemTitle, blanks), emailAddresses, None, None, None)

  def createMailServerData: MailServerData = MailServerData("", properties = getmailServerProperties)

  def getmailServerProperties: Option[Map[String, String]] = {
    mailServerService.get() match {
      case Some(mailServer) ⇒ mailServer.properties
      case None             ⇒ None
    }
  }

  def createSubject: String = "Defecte signaalgever(s) %s".format(getSystemTitle)

  def getSystemTitle: String = systemService.get(systemId).map(_.title).getOrElse("")

  def createBody(formattedDate: String, systemTitle: String, blanks: List[PossibleDefectMSI]): String = {
    val newLine = "\n"
    var body = ("In het MTMruw bestand van %s tonen de volgende matrix signaalgevers van " +
      "het traject %s de beeldstand BL:").format(formattedDate, systemTitle) + newLine + newLine

    blanks.foreach(blank ⇒ {
      body = body + "Os_comm_id: " + blank.os_comm_id + newLine
      body = body + "Os_locatie: " + blank.os_locatie + newLine
      body = body + "Msi_rijstrook_pos: " + blank.msi_rijstr_pos + newLine
      body = body + "MSI_stand: BL" + newLine + newLine
    })
    body.stripLineEnd
  }

  /**
   * format a given long values as a date
   *
   * @param time to format
   * @param format optional date format
   * @return string representation of a given date
   */
  def formatDate(time: Long, format: String = "dd-MM-yyyy"): String = {
    val cal = Calendar.getInstance(TimeZone.getTimeZone("CET"))
    val sdf = new SimpleDateFormat(format)
    cal.setTimeInMillis(time)
    sdf.format(cal.getTime)
  }
}

trait TimeNow {
  def getNow: Long

  def setNow(now: Long)
}

class SystemNow() extends TimeNow {
  override def getNow: Long = System.currentTimeMillis()

  override def setNow(now: Long): Unit = ()
}