package csc.sectioncontrol.mtm.nl

import java.io.{ FileInputStream, InputStream }
import java.security.MessageDigest

import akka.actor.{ Actor, ActorLogging, ActorRef }
import akka.util.duration._
import csc.curator.utils.Curator
import csc.sectioncontrol.enforce.{ SpeedEventData, SpeedIndicatorStatus, SystemEventData }
import csc.sectioncontrol.messages.{ Interval, StatsDuration, SystemEvent }
import csc.sectioncontrol.storage.{ ZkCorridor, ZkMsiIdentification, ZkSection, _ }
import csc.sectioncontrol.storagelayer.excludelog.{ ExcludeLogLayer, SuspendEvent }
import csc.sectioncontrol.storagelayer.mail.MailServerServiceSmtp
import csc.sectioncontrol.storagelayer.roles.RoleServiceZooKeeper
import csc.sectioncontrol.storagelayer.system.SystemServiceZookeeper
import csc.sectioncontrol.storagelayer.user.UserServiceZooKeeper

import scala.annotation.tailrec

/**
 * Parse MTM data and send corresponding messages
 */
class ProcessMtmData(curator: Curator, systemId: String, msi: Msi) extends Actor with ActorLogging {

  def this(curator: Curator, systemId: String) {
    this(curator, systemId, null)
  }

  import ProcessMtmData._

  def receive = {
    case MtmSource(filename, speedLog, configs, expectedTimeFrom, expectedTimeUntil, dstChange, day) ⇒
      try {
        val checkInput = new FileInputStream(filename)
        val (isValid, someReason) = try {
          isHashcodeValid(checkInput)
        } finally {
          checkInput.close()
        }

        if (isValid) {
          val input = new FileInputStream(filename)
          try {
            val mtmDataParser = new MtmDataParser(input, expectedTimeFrom, expectedTimeUntil)

            processAllBlankMsi(mtmDataParser, expectedTimeFrom, configs, day)

            val total: (Int, Int) = configs.foldLeft[(Int, Int)]((0, 0)) {
              case ((systemCount, speedCount), config: CorridorConfig) ⇒
                val processor = new MtmCorridorProcessor(config, mtmDataParser, /*exclusions = */ Nil)
                val topEvents = processor.getTopSystemEvents
                val topFailIntervals = makeFailIntervals(topEvents)
                val osEvents = processor.getOsSystemEvents
                val osFailIntervals = makeFailIntervals(osEvents)

                // construct MSI data
                val msiEvents = processor.getMsiEvents.sortBy(ev ⇒ (ev.effectiveTimestamp, ev.sequenceNumber))
                val filteredEvents: List[SpeedEventData] = mergeFailIntervals(mergeFailIntervals(msiEvents, topFailIntervals), osFailIntervals).filter(_.effectiveTimestamp < expectedTimeUntil)

                val systemEvents = config.msiBlackList.map(msi ⇒ {
                  val msg = "Uitgesloten signaalgever: " + {
                    val descr = msi.omschrijving.trim
                    if (descr.length > 0) {
                      "%s os_comm_id: %d, rijstrook: %s".format(descr, msi.os_comm_id, msi.msi_rijstr_pos)
                    } else {
                      "os_comm_id: %d, rijstrook: %s".format(msi.os_comm_id, msi.msi_rijstr_pos)
                    }
                  }

                  SystemEvent(componentId = componentId, timestamp = System.currentTimeMillis(), systemId = systemId,
                    userId = "System", eventType = "INFO", reason = Some(msg), token = None, startTimestamp = None, path = None,
                    corridorId = Some(config.corridorInfoId.toString), gantryId = None, laneId = None)
                })

                systemEvents.foreach(context.system.eventStream.publish(_))

                /*
                TC Spec H3 req 50a: Wanneer er tijdens het dubbele uur bij de overgang van zomer- naar wintertijd
                beeldstanden voorkomen is het toegestaan om de overtredingen in dit tijdvenster uit te filteren
                 */

                dstChange.foreach(interval ⇒ {
                  // did the dstChange occur withing this timeframe ?
                  if (interval.from >= expectedTimeFrom && interval.to <= expectedTimeUntil &&
                    // and did summer time change to winter time ? (see WatchTime.scala)
                    interval.from != interval.to) {
                    log.info("daylight savings time changed from: <{}> to <{}>", interval.from, interval.to)

                    if (filteredEvents.exists(speedEvent ⇒
                      speedEvent.effectiveTimestamp >= interval.from && speedEvent.effectiveTimestamp <= interval.to)) {
                      log.info("MSI changes during dst-change, saving suspendEvents for " + interval)
                      saveSuspension(curator, Some(config.corridorInfoId), interval, "DST change")
                    }
                  }
                })

                // send MSI data to speedLog
                filteredEvents.foreach(event ⇒ speedLog ! event)
                (systemCount + topEvents.size + osEvents.size, speedCount + msiEvents.size)
            }
            sender ! total
          } finally {
            input.close()
          }
        } else {
          someReason.foreach(reason ⇒ {
            log.error(reason)
          })
          val error = "HASHCODE is fout tijdens lezen van MTMRuw bestand: " + filename
          log.error(error)
          context.system.eventStream.publish(SystemEvent(componentId, System.currentTimeMillis(), systemId, "MtmChecksumError", "system", Some(error)))
          sender ! (0, 0)
        }
      } catch {
        // TODO Handle exceptions through death and supervision instead of handling locally.
        case e @ MtmFormatException(message, line) ⇒
          //TODO 20120519 RR->AS: use supervision and let it crash
          log.error(e, "Invalid format while parsing MTMRuw file: " + filename)
          context.system.eventStream.publish(SystemEvent(componentId, System.currentTimeMillis(), systemId, "MtmFormatError", "system", Some(message + "; " + line)))
          sender ! (0, 0)
        case e: Exception ⇒
          //TODO 20120519 RR->AS: use supervision and let it crash
          log.error(e, "Exception while parsing MTMRuw file: " + filename)
          context.system.eventStream.publish(SystemEvent(componentId, System.currentTimeMillis(), systemId, "MtmParseError", "system", Some(e.getMessage)))
          sender ! (0, 0)
      }

  }

  /**
   * Take care of handling the all day blanks msi
   * - msi is a constructor parameter only for the unit tests. If it's not set create it
   * - give expectedTimeFrom as the day to look as it is the start for the mtm processing
   * @return
   */
  // TODO RDJ -> PV Refactor, should be a better solution than setting constructor parameters only for tests
  def processAllBlankMsi(mtmDataParser: MtmDataParser, expectedTimeFrom: Long, configs: List[CorridorConfig], day: Option[StatsDuration]) {
    if (msi != null) {
      msi._mtmDataParser = mtmDataParser
      msi.processBlankMessageSignalIndicators(context, expectedTimeFrom, day)
    } else {
      val msi = new Msi(configs, mtmDataParser, exclusions = Nil, systemId,
        new SystemServiceZookeeper(curator),
        new MailServerServiceSmtp(curator),
        new UserServiceZooKeeper(curator, new RoleServiceZooKeeper(curator)),
        new SystemNow(),
        componentId)
      msi.processBlankMessageSignalIndicators(context, expectedTimeFrom, day)
    }
  }

  private[nl] def saveSuspension(curator: Curator, someCorridorId: Option[Int], suspendInterval: Interval, reason: String): Unit = {
    val excludeLogStore = new ExcludeLogLayer(curator)
    excludeLogStore.saveEvent(systemId,
      SuspendEvent(ProcessMtmData.componentId, suspendInterval.from, someCorridorId, 1L, /*suspend = */ true, Some(reason)))
    excludeLogStore.saveEvent(systemId,
      SuspendEvent(ProcessMtmData.componentId, suspendInterval.to, someCorridorId, 2L, /* suspend = */ false, Some(reason)))
  }

  private[nl] def makeFailIntervals(events: List[SystemEventData]): List[(Long, Long)] = {
    @tailrec
    def extractIntervals(intervals: List[(Long, Long)], eventDataList: List[SystemEventData]): List[(Long, Long)] = {
      eventDataList match {
        case Nil ⇒
          intervals
        case sysEvent1 :: moreEvents if !sysEvent1.suspend ⇒
          extractIntervals(intervals, moreEvents)
        case sysEvent1 :: sysEvent2 :: moreEvents if sysEvent2.suspend ⇒ /* && sysEvent1.suspend == true */
          extractIntervals(intervals, sysEvent1 :: moreEvents)
        case sysEvent1 :: sysEvent2 :: moreEvents ⇒ /* sysEvent1.suspend == true && sysEvent2.suspend == false */
          extractIntervals((sysEvent1.effectiveTimestamp, sysEvent2.effectiveTimestamp) :: intervals, moreEvents)
        case sysEventLast :: Nil ⇒ /* sysEventLast.suspend == true */
          (sysEventLast.effectiveTimestamp, sysEventLast.effectiveTimestamp + 1.day.toMillis) :: intervals
      }
    }

    extractIntervals(List[(Long, Long)](), events.sortBy(ev ⇒ (ev.effectiveTimestamp, ev.sequenceNumber)))
  }

  @tailrec
  private[nl] final def mergeFailIntervals(speedEvents: List[SpeedEventData], failIntervals: List[(Long, Long)]): List[SpeedEventData] = {
    if (failIntervals.isEmpty || speedEvents.isEmpty) {
      speedEvents
    } else {
      val interval = failIntervals.head
      val (beforeEvents, afterStartEvents) = speedEvents.span(ev ⇒ ev.effectiveTimestamp < interval._1)
      val (duringEvents, afterEvents) = afterStartEvents.span(ev ⇒ ev.effectiveTimestamp <= interval._2)
      val restoreSpeedEvent = if (duringEvents.isEmpty) {
        if (beforeEvents.isEmpty) {
          speedEvents.sortBy(ev ⇒ (ev.effectiveTimestamp, ev.sequenceNumber)).head
        } else {
          beforeEvents.sortBy(ev ⇒ (ev.effectiveTimestamp, ev.sequenceNumber)).last
        }
      } else {
        duringEvents.sortBy(ev ⇒ (ev.effectiveTimestamp, ev.sequenceNumber)).last
      }
      val suspendEvent = SpeedDataImpl(interval._1, restoreSpeedEvent.corridorId, restoreSpeedEvent.sequenceNumber, Left(SpeedIndicatorStatus.Undetermined))
      val resumeEvent = SpeedDataImpl(interval._2, restoreSpeedEvent.corridorId, restoreSpeedEvent.sequenceNumber, restoreSpeedEvent.speedSetting)
      val newEvents = beforeEvents ++ List(suspendEvent, resumeEvent) ++ afterEvents
      mergeFailIntervals(newEvents, failIntervals.tail)
    }
  }
}

object ProcessMtmData {
  val componentId = "ProcessMtmData"
  val OS_componentId = "ProcessMtmData_OS"
  val TOP_componentId = "ProcessMtmData_TOP"
  val MARK = "HASHCODE|"
  val CharSet = "US-ASCII"

  def md5SumString(bytes: Array[Byte]): String = {
    val md5 = MessageDigest.getInstance("MD5")
    md5.reset()
    md5.update(bytes)

    md5.digest().map(0xFF & _).map {
      "%02x".format(_)
    }.foldLeft("") {
      _ + _
    }
  }

  /**
   * Check the data
   */
  def isHashcodeValid(input: InputStream): (Boolean, Option[String]) = {
    val data = Stream.continually(input.read).takeWhile(-1 !=).map(_.toByte).toArray
    val str = new String(data, CharSet)
    // fail if non ASCII character is present
    val unicode = new String(data, "UTF-8").split("\n").toList.zipWithIndex
    unicode.foreach {
      case (line, index) ⇒
        val allAscii = line.forall(ch ⇒ ch.toInt < 255)
        if (!allAscii) throw new MtmFormatException("Unicode karakter gevonden in MTM bestand (%s)".format(line), line)
    }
    val hashIndex = str.lastIndexOf(MARK)
    if (hashIndex == -1) {
      (false, Some("No HASHCODE found in MTM file."))
    } else {
      val (content, end) = data.splitAt(hashIndex + MARK.size)
      val hash = md5SumString(content)
      val hashValue = new String(end, CharSet).trim
      val isSame = hashValue == hash
      if (isSame)
        (isSame, Some("MTM file HASHCODE is valid."))
      else
        (isSame, Some("MTM file HASHCODE is invalid. Soll '%s', but Ist '%s'.".format(hashValue, hash)))
    }
  }

}

/**
 * Yet another Corridor configuration
 * @param corridorInfoId corridor ID
 * @param matrixIds list of station ID (os_comm_id - de unieke identificatie van het onderstation
 *                  in het MTM-systeem)
 */
case class CorridorConfig(corridorInfoId: Int, matrixIds: List[Int], msiBlackList: List[ZkMsiIdentification], schedules: List[ZkSchedule] = Nil) {
  require(matrixIds != null)
  require(!matrixIds.isEmpty)
}

object CorridorConfig {
  def getCorridorConfigs(curator: Curator, systemId: String): List[CorridorConfig] = {
    getCorridorIds(curator, systemId).flatMap {
      corridorId ⇒
        val corridor = curator.get[ZkCorridor](Paths.Corridors.getConfigPath(systemId, corridorId))
        val schedules = {
          val path = Paths.Corridors.getSchedulesPath(systemId, corridorId)
          curator.get[List[ZkSchedule]](path).getOrElse(Nil)
        }
        val sections: Seq[ZkSection] = corridor.map(_.allSectionIds).getOrElse(Seq()).flatMap {
          sectionId ⇒ curator.get[ZkSection](Paths.Sections.getConfigPath(systemId, sectionId))
        }
        val matrixIds = sections.flatMap {
          section ⇒ section.matrixBoards.map(_.toInt)
        }.toSet.toList
        val msiBlackList = sections.flatMap {
          section ⇒ section.msiBlackList
        }.toSet.toList

        if (!matrixIds.isEmpty) corridor.map(c ⇒
          CorridorConfig(c.info.corridorId, matrixIds, msiBlackList, schedules))
        else {
          None
        }
    }.toList
  }

  def getCorridorIds(curator: Curator, systemId: String): Seq[String] = {
    val corridorPaths = curator.getChildren(Paths.Corridors.getDefaultPath(systemId))
    corridorPaths.map {
      _.split('/').last
    }
  }
}

/**
 * Information for MTM data file parsing
 * @param filename absolute filename of the incoming data file
 * @param speedLog  actor to send SpeedEventData events
 * @param configs corridor configuration to use
 */
case class MtmSource(filename: String,
                     speedLog: ActorRef,
                     configs: List[CorridorConfig],
                     expectedTimeFrom: Long,
                     expectedTimeUntil: Long,
                     dstChange: Option[Interval],
                     statsKey: Option[StatsDuration]) {

  require(filename != null)
  require(!filename.trim().isEmpty)
  require(speedLog != null)
  require(configs != null)
  require(!configs.isEmpty)
}

/**
 * req 7: Het HHM dient een alarm te genereren indien het de snelheidslimiet
 * onbepaald heeft gemaakt doordat het bestand mtmruw.txt foutieve of
 * niet interpreteerbare gegevens bevat.
 * @param message  error description
 * @param line invalid line in the MTM data file
 */
case class MtmFormatException(message: String, line: String) extends RuntimeException(message + "\nregel: " + line)

case class TimeChangeEvent(someCorridorId: Option[Int], effectiveTimestamp: Long,
                           sequenceNumber: Long,
                           suspend: Boolean,
                           reason: String) extends SystemEventData {
  def componentId = ProcessMtmData.componentId

  def corridorId = someCorridorId

  def isSuspendOrResumeEvent = true

  def suspensionReason = Some(reason)
}
