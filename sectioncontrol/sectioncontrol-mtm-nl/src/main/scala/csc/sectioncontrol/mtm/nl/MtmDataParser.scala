package csc.sectioncontrol.mtm.nl

import java.io.InputStream

import csc.sectioncontrol.mtm.nl.MtmDataParser.MsiBlankEntry
import csc.sectioncontrol.storage.ZkMsiIdentification
import org.slf4j.LoggerFactory

import scala.util.matching.Regex

/**
 * Parse MTM file according to the specification:
 * IRS TC – ESA
 * Specificatie ESA-koppeling
 * Versie 2.0,    juli 2011
 * equirement SSS.TC.27 says:
 * Indien er zich op het traject een (dynamische) externe snelheidsaanduiding bevindt is de te hanteren snelheidslimiet:
 *
 * 1 gelijk aan de geldende snelheidslimiet indien zeker is dat er geen andere snelheidsaanduiding op het traject en het inleidende wegvak wordt getoond;
 *
 * 2 gelijk aan de geldende snelheidslimiet indien zeker is dat alle snelheidsaanduidingen op het gehele traject en het inleidende wegvak een consistente snelheidslimiet tonen die bovendien overeenkomt met de door de politie ingestelde snelheidslimieten (zie [req 63a]) voor dat tijdstip);
 *
 * 3 onbepaald, in alle overige gevallen.
 *
 * The 'geldende snelheidslimiet' here refers to the speedlimit that is applicable to the vehicle if there were no speedsigns from MTM (i.e. the speedlimit based on a "vast bord" alongside the road or the max speedlimit of the vehicle based on the vehicle category. Which ever of the 2 speedlimits is lower).
 *
 * Now in order to aggregate the speedsign from MTM let's follow the requirement point for point:
 * Situation 1 is applicable 'when it is certain that no (MTM) speedsign is shown on the traject'). In order to check in the code if we are dealing with this this situation we need to know which MTM signs are considered speedsigns. I think the following is the case:
 *
 * X*, X: Kruis, al of niet met attentie				no speedsign
 * PR*, PR, *PL*, PL: Pijl links of rechts, al of niet met attentie	no speedsign
 * OB *, OB: Overruling blank, resp. met en zonder attentie		no speedsign
 * BL*, BL: Uit, al of niet met attentie				no speedsign
 * GP*, GP: Vallende pijl, met en zonder attentie			no speedsign
 * @*, @: Einde alle restricties, al of niet met attentie		no speedsign
 * De volgende beeldstanden hebben betrekking op advies- en maximumsnelheden waarbij XXX de snelheid representeert. De beschikbare snelheden zijn: 30, 40, 50, 60, 70, 80, 90, 100, 110, 120
 * /XXX/: Einde maximumsnelheid XXX				no speedsign
 * (XXX): Maximumsnelheid XXX					speedsign
 * XXX *: Adviessnelheid XXX met attentie				speedsign
 * XXX: Adviessnelheid XXX						speedsign
 *
 * When no speedsign is shown on the traject the aggregated sign is the BLANK sign.
 *
 * Situation 2 is applicable 'when all speedsigns shown on the traject are consistent and equal to the configured speedlimit'  The 'equal tot the configured speedlimit' is not relevant for the aggregation of the speedlimits. So for aggregation we only have to check if all speedsigns shown on the traject are consistent (i.e. show the same speed). If this is the case the aggregated sign is the speed.
 *
 * Situation 3 is in all other situations. In order to implement this in code I think we should check the situations one-by-one. First check situation 1, then 2 and if 1 or 2 is not the case then we are in situation 3. In this situation the aggregated sign is 'onbepaald' (undetermined).
 *
 *
 * @param input source containing MTMdata to be parsed
 * @param expectedTimeFrom  start of current time frame
 * @param expectedTimeUntil end of current time frame
 *
 */
class MtmDataParser(input: InputStream, expectedTimeFrom: Long, expectedTimeUntil: Long) {
  require(input != null, "MTM data must be provided.")
  val log = LoggerFactory.getLogger(classOf[MtmDataParser])

  val source = scala.io.Source.fromInputStream(input, "UTF-8")
  //ignore comments
  val lines = source.getLines().filterNot(line ⇒ line.startsWith("#")).toList
  val msiData = createList("|MSI|", line ⇒ MsiData(line, expectedTimeFrom, expectedTimeUntil))
  val osData = createList("|OS|", line ⇒ OsData(line, expectedTimeFrom, expectedTimeUntil))
  val topData = createList("|TOP|", line ⇒ TopData(line, expectedTimeFrom, expectedTimeUntil))

  def getMsiData: List[MsiData] = msiData

  def getOsData: List[OsData] = osData

  def getTopData: List[TopData] = topData

  private def createList[T](start: String, f: String ⇒ T): List[T] = {
    val content = lines.filter(line ⇒ line.startsWith(start) || line.startsWith(start.toLowerCase))
    content.map(f)
  }
}

/**
 * req 3 : In aanvulling op [req 1] moet een HHM per locatie de geldende
 * snelheidslimiet onbepaald maken gedurende de periodes waarin
 * mtmruw.txt lege velden bevat.
 */
object MtmDataParser {
  val TopEntry = new Regex("""(\d+?)\|(-?\d+?)\|(-?\d+?)\|(-?\d+?)\|""")
  //1068912754| 10| 5| 08|
  //For req 3
  val TopEmptyEntry = new Regex("""(\d+?)\|.*""")
  //1068912754|  |  |  |
  val MsiEntry1 = new Regex("""(\d+?)\|(\d+?)\|(.+?)\|(-?\d+?)\|(.+?)\|""")
  // 1068912754| 836|A13 L 7.200| 1|BL |
  val MsiEntry = new Regex("""(\d+?)\|(\d+?)\|.*?\|(-?\d+?)\|(.+?)\|""")
  // 1068912754| 836|A13 L 7.200| 1|BL |
  //For req 3
  val MsiEmptyEntry = new Regex("""(\d+?)\|(\d+?)\|.*""")
  // 1068912754| 836|| | |
  //Einde maximumsnelheid, Maximumsnelheid
  val MsiSpeedEntry = new Regex("""[\(\*]?(\d+?)[\)\*F]?""") // 100, (100), *100*, 100F

  val MsiBlankEntry = new Regex("""[\*]?(OB|BL|@|E)[\*F]?""")
  val MsiBps = new Regex("""\d+?\|\d+?\|(.*?)\|-?\d+?\|.+?\|""") // 1068912754| 836|A13 L 7.200| 1|BL |

  //1068913067|153|A13 L 9.000|nee|nee|on-line| goed| ja| ja| ja| ja| ja|onbekend|
  val OsEntry = new Regex("""(\d+?)\|(\d+?)\|.+?\|(\w*?)\|(\w*?)\|\D*?\|(\w*?)\|(\w*?)\|(\w*?)\|(\w*?)\|(\w*?)\|(\w*?)\|(\w*?)\|""")
  //For req 3
  val OsEmptyEntry = new Regex("""(\d+?)\|(\d+?)\|.*""")

  def validateTimestamp(timestamp: Long, expectedTimeFrom: Long, expectedTimeUntil: Long): Boolean =
    timestamp >= expectedTimeFrom && timestamp <= expectedTimeUntil
}

/**
 * OS data
 */
case class OsData(source: String, tijdstip_s: String,
                  os_comm_id: Int,
                  alt_msi_stand_ind: String,
                  fatale_fout_in_os_ind: String,
                  os_comm_stat: String,
                  btrw_msi_stand: String,
                  btrw_msi_status: String,
                  btrw_msi_lampstatus: String,
                  btrw_detstatus: String,
                  btrw_os_status: String,
                  os_comm_kwaliteit: String,
                  isMidnight: Boolean) {
  /**
   * req 2
   * In aanvulling op [req 1] moet een HHM per locatie de geldende
   * snelheidslimiet onbepaald maken in de periodes dat het bestand
   * mtmruw.txt aangeeft:
   * • dat van een onderstation (OS) op het traject
   * o de alternatieve MSI-stand gelijk is aan 'ja';
   * o het optreden van een fatale fout gelijk is aan 'ja';
   * o de status van de communicatieverbinding tussen
   * TOP/FEP en een onderstation ongelijk is aan 'goed';
   * o de betrouwbaarheidsindicatie van de getoonde stand van
   * de MSI’s ongelijk is aan 'ja';
   * o de betrouwbaarheidsindicatie van de gemelde MSIstatussen
   * ongelijk is aan 'ja';
   * o de betrouwbaarheidsindicatie van de gemelde MSIlampstatussen
   * ongelijk is aan 'ja';
   * o de betrouwbaarheidsindicatie van de OS-statussen
   * ongelijk is aan 'ja';
   * o de kwaliteit van de communicatie tussen TOP/FEP en een
   * onderstation ongelijk is aan 'goed';
   */
  val isUndefined: Boolean = {
    alt_msi_stand_ind == "ja" || fatale_fout_in_os_ind == "ja" ||
      os_comm_stat != "goed" || btrw_msi_stand != "ja" ||
      btrw_msi_status != "ja" || btrw_msi_lampstatus != "ja" ||
      btrw_os_status != "ja" || os_comm_kwaliteit != "goed"
  }
  val isOperational: Boolean = !isUndefined
  val timestamp = tijdstip_s.toLong * 1000
}

object OsData {
  def apply(data: String, expectedTimeFrom: Long, expectedTimeUntil: Long): OsData = {
    val extract = data.substring(24).replaceAll("""\s""", "")
    val osData = extract match {
      case MtmDataParser.OsEntry(tijdstip_s, os_comm_id, alt_msi_stand_ind,
        fatale_fout_in_os_ind, os_comm_stat, btrw_msi_stand,
        btrw_msi_status, btrw_msi_lampstatus, btrw_detstatus, btrw_os_status, os_comm_kwaliteit) ⇒
        val midnight = data.contains("00:00:00")
        new OsData(data, tijdstip_s, os_comm_id.toInt, alt_msi_stand_ind,
          fatale_fout_in_os_ind, os_comm_stat, btrw_msi_stand,
          btrw_msi_status, btrw_msi_lampstatus, btrw_detstatus, btrw_os_status, os_comm_kwaliteit, midnight)
      case MtmDataParser.OsEmptyEntry(tijdstip_s, os_comm_id) ⇒
        new OsData(data, tijdstip_s, os_comm_id.toInt, "", "", "", "", "", "", "", "", "", false)
      case _ ⇒ throw new MtmFormatException("Onverwacht OS dataformat.", data)
    }
    if (!MtmDataParser.validateTimestamp(osData.timestamp, expectedTimeFrom, expectedTimeUntil)) {
      throw new MtmFormatException("OS data voor verkeerde dag.", data)
    }
    osData
  }
}

case class MsiKey(os_comm_id: Int, msi_rijstr_pos: String, os_locatie: String)

/**
 * MSI data
 */
case class MsiData(source: String, tijdstip_s: String, os_comm_id: Int, os_locatie: String, msi_rijstr_pos: String,
                   msi_stand: String, isMidnight: Boolean) {

  def getKey: MsiKey = MsiKey(os_comm_id, msi_rijstr_pos, os_locatie)

  def getSpeed: Option[Int] = {
    msi_stand match {
      case MtmDataParser.MsiSpeedEntry(speed) ⇒ Some(speed.toInt)
      case _                                  ⇒ None
    }
  }

  def getBps: String = {
    val relevant = source.substring(25)
    val fields = relevant.split('|').toList
    fields match {
      case _ :: _ :: bps :: _ ⇒ bps.trim.replaceAll("  ", " ")
      case other              ⇒ ""
    }
  }

  def isBlank: Boolean = msi_stand.matches(MsiBlankEntry.toString)

  val id = ZkMsiIdentification(os_comm_id, msi_rijstr_pos, getBps)

  val isOperational = getSpeed.isDefined

  val timestamp = tijdstip_s.toLong * 1000
}

object MsiData {
  def apply(data: String, expectedTimeFrom: Long, expectedTimeUntil: Long): MsiData = {
    val extract = data.substring(25).replaceAll("""\s""", "")
    val msiData = extract match {
      case MtmDataParser.MsiEntry(timestamp, osCommId, lanePos, msiStand) ⇒
        val midnight = data.contains("00:00:00")
        val os_locatie = data.split("""\|""")(5) // the extract contains no blanks
        new MsiData(data, timestamp, osCommId.toInt, os_locatie, lanePos, msiStand, midnight)
      case MtmDataParser.MsiEmptyEntry(timestamp, osCommId) ⇒
        new MsiData(data, timestamp, osCommId.toInt, "", "1", "XXX", false)
      case _ ⇒ throw new MtmFormatException("Onverwacht MSI dataformat.", data)
    }
    if (!MtmDataParser.validateTimestamp(msiData.timestamp, expectedTimeFrom, expectedTimeUntil)) {
      throw new MtmFormatException("MSI data voor verkeerde dag.", data)
    }
    msiData
  }
}

/**
 * Transaction Oriented Processor and Front End Processor data
 */
case class TopData(source: String, tijdstip_s: String, topMode: Int, clockStatus: Int, fepMode: Int) {
  /**
   * req 2
   * In aanvulling op [req 1] moet een HHM per locatie de geldende
   * snelheidslimiet onbepaald maken in de periodes dat het bestand
   * mtmruw.txt aangeeft:
   * • of dat van de TOP of FEP’s
   * o de TOP-mode ongelijk is aan 'operationeel';
   * o de DCF-klokstatus ongelijk is aan 'goed';
   * o de FEP-mode ongelijk is aan 'operationeel'.
   */
  def isOperational: Boolean = {
    topMode == 41 && clockStatus == 1 && fepMode == 41
  }

  def timestamp = tijdstip_s.toLong * 1000
}

object TopData {
  def apply(data: String, expectedTimeFrom: Long, expectedTimeUntil: Long): TopData = {
    val extract = data.substring(25).replaceAll("""\s""", "")
    val topData = extract match {
      case MtmDataParser.TopEntry(tijdstip_s, topMode, clockMode, fepMode) ⇒
        new TopData(data, tijdstip_s, topMode.toInt, clockMode.toInt, fepMode.toInt)
      case MtmDataParser.TopEmptyEntry(tijdstip_s) ⇒
        new TopData(data, tijdstip_s, -1, -1, -1)
      case _ ⇒
        throw new MtmFormatException("Onverwacht TOP dataformat.", data)
    }
    if (!MtmDataParser.validateTimestamp(topData.timestamp, expectedTimeFrom, expectedTimeUntil)) {
      throw new MtmFormatException("TOP data voor verkeerde dag.", data)
    }
    topData
  }
}

