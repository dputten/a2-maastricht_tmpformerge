/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.mtm.nl

import csc.sectioncontrol.enforce.{ SpeedIndicatorStatus, SpeedEventData, SystemEventData }
import scala.Left
import csc.sectioncontrol.storage.ZkSchedule
import csc.akkautils.DirectLogging
import csc.curator.utils.Curator
import java.util.{ TimeZone, Calendar }
import java.text.SimpleDateFormat
import csc.sectioncontrol.messages.SystemEvent
import csc.sectioncontrol.storagelayer.mail.MailServerServiceSmtp
import csc.sectioncontrol.notification.email.{ EmailSender, EmailData, MailServerData }
import csc.sectioncontrol.storagelayer.user.UserServiceZooKeeper
import csc.sectioncontrol.storagelayer.system.SystemServiceZookeeper

/**
 * Process MTM data for one corridor
 * @param config corridor information
 * @param parser parser of incoming data
 * @param exclusions stations to be excluded from the result because they report invalid data
 */
private[nl] class MtmCorridorProcessor(config: CorridorConfig,
                                       parser: MtmDataParser,
                                       exclusions: List[Int] = Nil) extends DirectLogging {

  type OsState = Map[Int, OsData] //maps portal ID to data
  type MsiState = Map[Int, Map[String, MsiData]] //maps portal ID to (lane -> data) os_comm_id -> (msi_rijstr_pos -> msiData)

  private var counter: Int = 0

  private def count: Int = {
    counter = counter + 1
    counter
  }

  /**
   * Leave only those events which lead to state change
   */
  def getTopSystemEvents: List[SystemEventData] = {
    val events: List[TopData] = parser.getTopData
    val (_, result) = events.foldLeft[(Option[Boolean], List[SystemEventData])](None, Nil) {
      case ((state, foldedEvents), event) ⇒ (state, event.isOperational) match {
        //include all while without state
        case (None, newState) ⇒
          (Option(newState), createTopSystemData(event) :: foldedEvents)
        //no state change - not included
        case (Some(oldState), newState) if oldState == newState ⇒
          (Option(newState), foldedEvents)
        // state changed - included
        case (Some(oldState), newState) ⇒
          (Option(newState), createTopSystemData(event) :: foldedEvents)
      }
    }
    result.reverse
  }

  def getOsSystemEvents: List[SystemEventData] = {
    //filter data for one corridor
    val osData = parser.getOsData.filter(data ⇒ {
      !exclusions.contains(data.os_comm_id) && config.matrixIds.contains(data.os_comm_id)
    })
    osData match {
      case Nil ⇒ Nil //no OS data for this corridor
      case head :: _ ⇒ {
        val (initOsState, dayChanges) = readInitialOsState(osData)
        val initError: Option[String] = getOsError(initOsState)
        val firstEvent = SystemDataImpl(componentId = ProcessMtmData.OS_componentId, effectiveTimestamp = head.timestamp,
          corridorId = Some(config.corridorInfoId), sequenceNumber = count, isSuspendOrResumeEvent = true,
          suspend = initError.isDefined, suspensionReason = initError)

        val foldStart = (initOsState, initError, List(firstEvent))
        val (_, _, result) = dayChanges.foldLeft[(OsState, Option[String], List[SystemEventData])](foldStart) {
          case ((previousState, previousError, foldedEvents), event) ⇒
            if (!previousState.contains(event.os_comm_id)) {
              throw new MtmFormatException("Onbekende OS data. de locatie is niet gedefinieerd in de begin status (%s).".format(event.os_comm_id), event.source)
            }
            val newState = previousState + (event.os_comm_id -> event)
            val newError: Option[String] = getOsError(newState)
            (previousError, newError) match {
              //no state change - not included
              case (first, second) if first.isDefined == second.isDefined ⇒
                (newState, previousError, foldedEvents)
              // state changed - included
              case (first, second) ⇒
                (newState, newError, createOsSystemData(event, newError) :: foldedEvents)
            }
        }
        result.reverse
      }
    }
  }

  /**
   *
   * @return transformation of all relevant msi-events from the MTM system into
   *         a List[SpeedEventData]
   */

  def getMsiEvents: List[SpeedEventData] = {

    val msiData = getRelevantMsiDataForCorridor
    msiData match {
      case Nil ⇒ Nil //no MSI data provided
      case head :: _ ⇒
        // split in initial events (at 00:00:00) and events during the day
        val (midnightData, msiEvents) = msiData.span(msi ⇒ msi.isMidnight)
        // transform midnightData into msiState
        val initialMsiState = createMsiState(midnightData)

        // Group events by time to generate state-over-time
        val eventsByTime = msiEvents.groupBy(_.timestamp)
        //get any time of the processing day
        val dayTime = msiData.map(_.timestamp).headOption
        val scheduleTimes = dayTime.map(time ⇒ getScheduleTimes(time)).getOrElse(Seq())
        val eventTimes: List[Long] = (eventsByTime.keys.toList ++ scheduleTimes).sorted

        // foldState contains the accumulator for SpeedEventData and
        // the (over time evolving) state of all msi's
        val foldState: (List[SpeedEventData], MsiState) =
          (List(SpeedDataImpl(head.timestamp, config.corridorInfoId, count, makeSpeedEvent(initialMsiState, head.timestamp))), initialMsiState)

        val (allSpeedEvents, _ /* finalState */ ) = eventTimes.foldLeft(foldState) {
          case ((speedEvents, msiState), timestamp) ⇒
            val newState = nextMsiState(msiState, getEventsByTime(eventsByTime, timestamp))
            val speedSetting = makeSpeedEvent(newState, timestamp)

            // if speedsetting changed, record it
            if (speedSetting != speedEvents.head.speedSetting) {
              log.info("SpeedSetting for corridor {} changed at {} from {} to {}", config.corridorInfoId, timestamp,
                speedEvents.head.speedSetting, speedSetting)
              (SpeedDataImpl(timestamp, config.corridorInfoId, count, speedSetting) :: speedEvents, newState)
            } else {
              log.info("SpeedSetting for corridor {} unchanged ({}) at {}", config.corridorInfoId, speedSetting, timestamp)
              (speedEvents, newState)
            }
        }
        // we had it backwards...
        allSpeedEvents.reverse
    }
  }

  //=============================================================
  //===== private stuff
  //=============================================================

  private def getEventsByTime(eventsByTime: Map[Long, List[MsiData]], timestamp: Long): List[MsiData] = {
    val list = eventsByTime.toList.sortBy(_._1).reverse
    val found = list.find {
      case (time, list) ⇒ {
        timestamp >= time
      }
    }
    found.map(_._2).getOrElse(List())
  }

  private def getScheduleTimes(dayStart: Long): List[Long] = {
    val minute = 60000L
    val schedules = config.schedules
    val startAndEndTimes = schedules.flatMap(schedule ⇒ {
      val start = if (schedule.fromPeriodHour != 0 || schedule.fromPeriodMinute != 0)
        Some(ZkSchedule.getTime(dayStart, schedule.fromPeriodHour, schedule.fromPeriodMinute))
      else
        None
      val end = if (schedule.toPeriodHour != 23 || schedule.toPeriodMinute != 59)
        Some(ZkSchedule.getTime(dayStart, schedule.toPeriodHour, schedule.toPeriodMinute) + minute)
      else
        None
      start ++ end
    })
    startAndEndTimes.distinct
  }

  /**
   * Filter all the MSI's not relevant because
   * - they are excluded
   * - are blackListed
   * - do not belong to these sections
   * @return
   */
  private def getRelevantMsiDataForCorridor = {
    parser.getMsiData.filter(data ⇒ {
      !exclusions.contains(data.os_comm_id) &&
        !config.msiBlackList.contains(data.id) &&
        config.matrixIds.contains(data.os_comm_id)
    })
  }

  /**
   *
   * @param state map representing the state of all MSG's
   * @param timestamp Point in time for the given state
   * @return the speedSetting according to the state and the schedule
   */
  private def makeSpeedEvent(state: MsiState, timestamp: Long): Either[SpeedIndicatorStatus.Value, Option[Int]] = {
    val speedSetting = getSpeedSetting(state)
    val someSchedule = ZkSchedule.findScheduleForTime(timestamp, config.schedules)

    (someSchedule, speedSetting) match {
      case (Some(schedule), Right(_)) if schedule.supportMsgBoard ⇒ {
        val blanksInState = countBlanksInState(state)
        if (blanksInState > schedule.msgsAllowedBlank) {
          log.info("SpeedSetting for corridor {} at {} set to undetermined because blank MSI's ({}) exceed blanks allowed ({})",
            config.corridorInfoId, timestamp, blanksInState, schedule.msgsAllowedBlank)
          Left(SpeedIndicatorStatus.Undetermined)
        } else {
          speedSetting
        }
      }
      case _ ⇒ speedSetting
    }
  }

  private def emptyMsiState: MsiState = {
    config.matrixIds.map(id ⇒ { id -> Map.empty[String, MsiData] }).toMap
  }

  private def createMsiState(events: List[MsiData]): MsiState = {
    events.foldLeft(emptyMsiState) {
      case (state, event) ⇒ {
        val portalMap: Map[String, MsiData] = state(event.os_comm_id)
        val newPortalMap = portalMap + (event.msi_rijstr_pos -> event)
        state + (event.os_comm_id -> newPortalMap)
      }
    }
  }

  /**
   * Given an initial state of the msi's and a list of msi events, construct the
   * new state of the msi's
   * @param msiState  current state of all msi's
   * @param events events to apply to the current msiState to obtain the new msiState
   * @return new state after applying events
   */
  private def nextMsiState(msiState: MsiState, events: List[MsiData]): MsiState = {
    events.foldLeft(msiState) {
      case (state, event) ⇒
        state.get(event.os_comm_id) match {
          case None ⇒
            val msg = "Onbekende MSI data. Locatie %d is niet gedefinieerd in de begin status.".format(event.os_comm_id)
            throw new MtmFormatException(msg, event.source)

          case Some(locationMap) ⇒ locationMap.get(event.msi_rijstr_pos) match {
            case None ⇒
              val msg = "Onbekende MSI data. Rijstrook %s is op portaal %d is niet gedefinieerd in de begin status.".format(event.msi_rijstr_pos, event.os_comm_id)
              throw new MtmFormatException(msg, event.source)
            case Some(previousMsi) ⇒
              val newLocationMap = locationMap + (event.msi_rijstr_pos -> event)
              state + (event.os_comm_id -> newLocationMap)
          }
        }
    }
  }

  /**
   * Given the state of the msi's, calculate the speedSetting.
   * - If no MSI shows a speed, the speed setting becomes Right(None)
   * - If the msi's that show a speed all show the same speed, the speedSetting becomes Right(Some(givenSpeed))
   * - If the msi's show different speeds, the speedSetting becomes Left(SpeedIndicatorStatus.Undetermined)
   * @param state the given state
   * @return speedSetting calculated from the msiState
   */
  private def getSpeedSetting(state: MsiState): Either[SpeedIndicatorStatus.Value, Option[Int]] = {
    val msiValues: List[MsiData] = state.values.flatMap(portalMap ⇒ portalMap.values).toList
    // retrieve a list of speeds shown on the msi's
    val speedList = msiValues.foldLeft(List[String]()) {
      case (speeds, event) ⇒
        event.msi_stand match {
          case MtmDataParser.MsiSpeedEntry(speed) ⇒ speed :: speeds
          case other                              ⇒ speeds
        }
    }
    val distinctSpeeds = speedList.distinct
    log.info("Snelheidsbeeld: {}", distinctSpeeds)
    distinctSpeeds.size match {
      case 0     ⇒ Right(None) // Consistent, no msi shows a speed
      case 1     ⇒ Right(Some(speedList.head.toInt)) // Consistent. All non-blank MSI's show same speed
      case other ⇒ Left(SpeedIndicatorStatus.Undetermined) // Inconsistent speed on msi's
    }
  }

  /**
   * Given the msiState, count the number of msi's that are blank
   * @param state the state of all msi's
   * @return number of blank msi's
   */
  private def countBlanksInState(state: MsiState) = {
    val msiValues = state.values.flatMap(portalMap ⇒ portalMap.values).toList
    msiValues.count(msiEvent ⇒ {
      msiEvent.msi_stand match {
        case MtmDataParser.MsiBlankEntry(v) ⇒ true
        case _                              ⇒ false
      }
    })
  }

  private def createTopSystemData(event: TopData): SystemEventData = {
    SystemDataImpl(ProcessMtmData.TOP_componentId, event.timestamp, Some(config.corridorInfoId), count, true, !event.isOperational, Some(event.source))
  }

  private def createOsSystemData(event: OsData, reason: Option[String]): SystemEventData = {
    SystemDataImpl(ProcessMtmData.OS_componentId, event.timestamp, Some(config.corridorInfoId), count, true, !event.isOperational, reason)
  }

  /**
   * Read the data for midnight and create initial state .
   * @param data the MTM data
   * @return   the initial state at midnight and the rest od the data
   */
  private def readInitialOsState(data: List[OsData]): (OsState, List[OsData]) = {
    val (initialData, rest) = data.span(os ⇒ os.isMidnight)
    val state = initialData.map(event ⇒ (event.os_comm_id, event)).toMap
    (state, rest)
  }

  private def getOsError(state: OsState): Option[String] = {
    if (state.values.isEmpty)
      Some("No initial state defined at midnight.")
    else {
      val error = state.values.find(v ⇒ !v.isOperational)
      error.map(_.source)
    }
  }
}

/**
 * OS and TOP/FEP data representation
 */
private case class SystemDataImpl(componentId: String,
                                  effectiveTimestamp: Long,
                                  corridorId: Option[Int],
                                  sequenceNumber: Long,
                                  isSuspendOrResumeEvent: Boolean,
                                  suspend: Boolean,
                                  suspensionReason: Option[String]) extends SystemEventData

/**
 * MSI data representation
 */
private[nl] case class SpeedDataImpl(effectiveTimestamp: Long,
                                     corridorId: Int,
                                     sequenceNumber: Long,
                                     speedSetting: Either[SpeedIndicatorStatus.Value, Option[Int]]) extends SpeedEventData

