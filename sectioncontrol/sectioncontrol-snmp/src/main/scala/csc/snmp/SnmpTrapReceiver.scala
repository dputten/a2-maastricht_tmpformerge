package csc.snmp

import org.snmp4j._
import org.snmp4j.mp.MPv1
import org.snmp4j.smi._
import akka.actor.ActorRef
import csc.akkautils.DirectLogging
import org.snmp4j.transport.DefaultUdpTransportMapping
import java.util
import scala.collection.JavaConversions._

case class TrapMessage(text: String, agentAddress: Option[String], timestamp: Long)

// http://www.tcpipguide.com/free/t_SNMPVersion1SNMPv1MessageFormat-3.htm
class SnmpTrapReceiver(snmpConfig: SnmpConfig, listener: ActorRef, timeNow: TimeNow) extends CommandResponder with DirectLogging {
  private var snmp: Snmp = null
  private var transport: TransportMapping[_ <: Address] = null

  def start(): Unit = {
    init()
    snmp.addCommandResponder(this)
  }

  def stop(): Unit = {
    snmp.close()
    transport.close()
  }

  protected def init(): Unit = {
    transport = new DefaultUdpTransportMapping(new UdpAddress(snmpConfig.ipAddress + "/" + snmpConfig.port), true)
    try {
      snmp = new Snmp(transport)
    } catch {
      case t: Throwable ⇒
        transport.close()
        throw t
    }
    snmp.getMessageDispatcher.addMessageProcessingModel(new MPv1())
    snmp.listen()
    log.info("Listening for snmp-traps on {}:{}", snmpConfig.ipAddress, snmpConfig.port)
  }

  // Implemented from CommandResponder
  // This is where the event comes in.
  def processPdu(event: CommandResponderEvent): Unit = {
    this.synchronized {
      val resultValues = extractPdu(event)
      resultValues.map { (t: (String, String)) ⇒
        listener ! TrapMessage(t._1, Option(t._2), timeNow.getNow)
        log.info("Sent TrapMessage to listener.")
      }
    }
  }

  private def extractPdu(event: CommandResponderEvent): Option[(String, String)] = {
    val pDUv1: PDUv1 = event.getPDU.asInstanceOf[PDUv1]
    log.info("------------------------------------------------------------")
    log.info("getSpecificTrap:{}", pDUv1.getSpecificTrap)
    log.info("getAgentAddress: {}", pDUv1.getAgentAddress)
    log.info("getVariableBindings:{}", pDUv1.getVariableBindings)
    log.info("getTimestamp:{}", pDUv1.getTimestamp)
    log.info("getPeerAddress:{}", event.getPeerAddress)
    log.info("------------------------------------------------------------")

    val varBinds: List[VariableBinding] = event.getPDU.getVariableBindings.toList
    val someBinding = varBinds.find { (aBinding: VariableBinding) ⇒ aBinding.getOid.toString.startsWith(snmpConfig.enterpriseId) }

    someBinding match {
      case None ⇒
        log.error("Variablebinding with id: {} not found in snmp trap.", snmpConfig.enterpriseId)
        None
      case Some(aBinding) ⇒
        val bindingValue = aBinding.toValueString
        val bindingId = aBinding.getOid.toString
        val result = "<%d> %s SpecificTrap:%s VariableBindings:[%s = %s]".
          format(pDUv1.getTimestamp, pDUv1.getAgentAddress, pDUv1.getSpecificTrap, bindingId, bindingValue)

        Some((result, pDUv1.getAgentAddress.toString))
    }
  }

  /*
  private def extractPduOld(event: CommandResponderEvent): (String, String) = {
    val pDUv1: PDUv1 = event.getPDU.asInstanceOf[PDUv1]
    log.info("------------------------------------------------------------")
    log.info("getSpecificTrap:{}", pDUv1.getSpecificTrap)
    log.info("getAgentAddress: {}", pDUv1.getAgentAddress)
    log.info("getVariableBindings:{}", pDUv1.getVariableBindings)
    log.info("getTimestamp:{}", pDUv1.getTimestamp)
    log.info("getPeerAddress:{}", event.getPeerAddress)
    log.info("------------------------------------------------------------")

    var bindingId = ""
    var bindingValue = ""
    val varBinds: util.Vector[_ <: VariableBinding] = event.getPDU.getVariableBindings

    if (varBinds != null && !varBinds.isEmpty) {
      val varIter = varBinds.iterator()
      while (varIter.hasNext) {
        val variableBinding = varIter.next()
        if (variableBinding.getOid.toString.startsWith(snmpConfig.enterpriseId)) {
          bindingValue = variableBinding.toValueString
          bindingId = variableBinding.getOid.toString
        }

        log.info("OID :{}", variableBinding.getOid)
        log.info("Variablebinding: {}", variableBinding)
      }
    }

    if (bindingValue == "") {
      log.error("Variablebinding with id: {} not found in snmp trap.", snmpConfig.enterpriseId)
      throw new Exception("Variablebinding with id: %s not found in snmp trap.".format(snmpConfig.enterpriseId))
    }

    val result = "<%d> %s SpecificTrap:%s VariableBindings:[%s = %s]".format(pDUv1.getTimestamp, pDUv1.getAgentAddress, pDUv1.getSpecificTrap,
      bindingId, bindingValue)
    (result, pDUv1.getAgentAddress.toString)
  }
  */

}
