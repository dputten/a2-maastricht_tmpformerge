package csc.snmp

trait TimeNow {
  def getNow: Long

  def setNow(now: Long)
}

class SystemNow() extends TimeNow {
  override def getNow: Long = System.currentTimeMillis()

  override def setNow(now: Long): Unit = ()
}
