package csc.snmp

//Trap
//Asynchronous notification from agent to manager. SNMP traps enable an agent to notify the management station of significant events by way of an unsolicited SNMP message.
//Includes current sysUpTime value, an OID identifying the type of trap and optional variable bindings.
//Destination addressing for traps is determined in an application-specific manner typically through trap configuration variables in the MIB.
//The format of the trap message was changed in SNMPv2 and the PDU was renamed SNMPv2-Trap. While in classic communication the client always actively requests information from the server,
//SNMP allows the additional use of so-called "traps". These are data packages that are sent from the SNMP client to the server without being explicitly requested.
//http://lists.agentpp.org/pipermail/snmp4j/2009-March/003375.html

import org.snmp4j._
import org.snmp4j.mp.SnmpConstants
import org.snmp4j.smi._
import org.snmp4j.transport.DefaultUdpTransportMapping
import org.snmp4j.event.ResponseEvent
import csc.akkautils.DirectLogging
import resource._

class SnmpTrapGenerator(snmpConfig: SnmpConfig) extends DirectLogging {

  def send() = {
    managed(new DefaultUdpTransportMapping()).acquireAndGet {
      (transport: TransportMapping[_ <: Address]) ⇒
        managed(new Snmp(transport)).acquireAndGet {
          (snmp: Snmp) ⇒
            transport.listen()
            log.info("Sending ProtocolDataUnit(PDU).")
            val somePDU = getProtocolDataUnit
            somePDU.foreach((pdu: PDU) ⇒ {
              val someResponseEvent: Option[ResponseEvent] = Option(snmp.send(pdu, getTargetAddress))
              log.info("ProtocolDataUnit(PDU) is sent.")
              someResponseEvent.foreach((r: ResponseEvent) ⇒ {
                log.info("GOT responseEvent.getError" + r.getError)
                log.info("GOT responseEvent.getResponse.getRequestID" + r.getResponse.getRequestID)
                log.info("GOT responseEvent.getError" + r.getError)
              })
            })
        }
    }
  }

  def getProtocolDataUnit: Option[PDU] = {
    val pdu = new PDUv1()
    pdu.setType(PDU.V1TRAP)
    pdu.setSpecificTrap(2)
    pdu.setAgentAddress(new IpAddress(snmpConfig.ipAddress))
    pdu.setTimestamp(1000)

    try {
      val variableBinding = new VariableBinding()
      variableBinding.setOid(new OID(snmpConfig.enterpriseId + ".20.2")) //set the key
      variableBinding.setVariable(new Integer32(1)) // set the value
      pdu.add(variableBinding)
      log.info("Created PDU in SnmpTrapGenerator: " + pdu)
      Some(pdu)
    } catch {
      case e: Exception ⇒
        log.error(e, "Error obtaining protocol data unit")
        None
    }
  }

  def getTargetAddress = {
    val targetAddress = new UdpAddress(snmpConfig.ipAddress + "/" + snmpConfig.port)
    val target = new CommunityTarget()
    target.setCommunity(new OctetString("public"))
    target.setAddress(targetAddress)
    target.setVersion(SnmpConstants.version1)
    target.setTimeout(500L)
    target.setRetries(3)
    target
  }
}
