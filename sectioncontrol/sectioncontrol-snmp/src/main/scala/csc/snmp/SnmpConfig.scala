package csc.snmp

import com.typesafe.config.Config

case class SnmpConfig(ipAddress: String, port: Int, enterpriseId: String)

object SnmpConfig {
  def apply(config: Config): SnmpConfig =
    SnmpConfig.apply(
      config.getString("snmpReceiver.ipAddress"),
      config.getInt("snmpReceiver.port"),
      config.getString("snmpReceiver.enterpriseId"))
}
