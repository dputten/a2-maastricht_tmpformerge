package unit.snmp

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.snmp.{ SnmpConfig, SnmpTrapReceiver }
import com.typesafe.config.ConfigFactory

/*
The spike is used with the real testboard.
 */
class spike extends WordSpec with MustMatchers {
  "SnmpTrapReceiver" must {
    "be created and receive a message sent by the sender" in {
      val config = ConfigFactory.load("spike.conf")
      val snmpConfig = SnmpConfig(config)

      val now = new NowMock
      now.setNow(12345)

      val snmpTrapReceiver = new SnmpTrapReceiver(snmpConfig, null, now)
      snmpTrapReceiver.start()

      Thread.sleep(15000)

      snmpTrapReceiver.stop()
    }
  }
}
