package unit.snmp

import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers
import csc.snmp._
import com.typesafe.config.ConfigFactory
import akka.testkit.{ TestKit, TestProbe }
import akka.util.duration._
import akka.actor.ActorSystem
import csc.snmp.TrapMessage
import csc.snmp.SnmpConfig

class SnmpConfigTest extends WordSpec with MustMatchers {
  "SnmpConfig" must {
    "read the config" in {
      val config = ConfigFactory.load("test.conf")
      val snmpConfig = SnmpConfig(config)
      snmpConfig.ipAddress must be("0.0.0.0")
      snmpConfig.port must be(1620)
      snmpConfig.enterpriseId must be("1.3.6.1.4.1.8691.10.1210")
    }
  }
}

class SnmpTrapReceiverTest extends TestKit(ActorSystem("SnmpTrapReceiver")) with WordSpec with MustMatchers with BeforeAndAfterAll {
  override def afterAll(): Unit = {
    system.shutdown()
    super.afterAll()
  }

  "SnmpTrapReceiver" must {
    "Receive a snmptrap and send a trapmessage to the listener" in {
      val config = ConfigFactory.load("test.conf")
      val snmpConfig = SnmpConfig(config)

      val probe = new TestProbe(system)
      val now = new NowMock
      now.setNow(12345)

      val snmpTrapReceiver = new SnmpTrapReceiver(snmpConfig, probe.ref, now)
      snmpTrapReceiver.start()

      val snmpTrapGenerator = new SnmpTrapGenerator(snmpConfig)
      snmpTrapGenerator.send()

      val response = probe.expectMsgClass(2 seconds, classOf[TrapMessage])
      response must be(TrapMessage("<1000> 0.0.0.0 SpecificTrap:2 VariableBindings:[1.3.6.1.4.1.8691.10.1210.20.2 = 1]", Some("0.0.0.0"), 12345))

      snmpTrapReceiver.stop()
      system.stop(probe.ref)
    }

    "Throw an exception in the receive when the oid could not be found in the variablebindings" in {
      evaluating {
        val config = ConfigFactory.load("error.conf")
        val snmpConfig = SnmpConfig(config)

        val probe = new TestProbe(system)
        val now = new NowMock
        now.setNow(12345)

        val snmpTrapReceiver = new SnmpTrapReceiver(snmpConfig, probe.ref, now)
        snmpTrapReceiver.start()

        val snmpTrapGenerator = new SnmpTrapGenerator(snmpConfig)
        snmpTrapGenerator.send()
        snmpTrapReceiver.stop()

        val response = probe.expectMsgClass(2 seconds, classOf[TrapMessage])
        response must be(TrapMessage("<1000> 0.0.0.0 SpecificTrap:2 VariableBindings:[1.3.6.1.4.1.8691.10.1210.20.2 = 1]", Some("0.0.0.0"), 12345))
      } must produce[Exception]
    }
  }
}

class NowMock() extends TimeNow {
  var _now: Long = _
  override def getNow: Long = _now
  override def setNow(now: Long) = {
    _now = now
  }
}

