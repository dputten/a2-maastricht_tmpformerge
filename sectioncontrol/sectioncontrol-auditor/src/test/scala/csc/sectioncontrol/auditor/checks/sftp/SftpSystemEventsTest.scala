/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.auditor.checks.sftp

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.akkautils.DirectLogging
import csc.sectioncontrol.storage.Paths
import java.util.Date
import csc.curator.CuratorTestServer
import csc.curator.utils.CuratorToolsImpl

class SftpSystemEventsTest extends WordSpec with MustMatchers with CuratorTestServer with DirectLogging {

  override def beforeEach() {
    super.beforeEach()
    //simulate systems
    val tools = new CuratorToolsImpl(clientScope, log)
    tools.createEmptyPath(Paths.Systems.getSystemPath("A2"))
    tools.createEmptyPath(Paths.Systems.getSystemPath("A9"))
    tools.createEmptyPath(Paths.Systems.getSystemPath("A10"))
  }

  "SFTPSystemEvents" must {
    "create 3 system events" in {
      val tools = new CuratorToolsImpl(clientScope, log)
      val sys = new FTPSystemEvents("/test,/data", tools)
      val action = new SftpAction(actionType = SftpActionTypes.Upload, time = new Date(), user = "me", detail = "/data/testFile.zip")
      val events = sys.createSystemEvents(action)
      events.size must be(3)
      val first = events(0)
      first.componentId must be("sftp")
      first.eventType must be(action.actionType.toString)
      first.systemId must (equal("A2") or equal("A9") or equal("A10"))
      first.timestamp must be(action.time.getTime)
      first.userId must be("system")
      val second = events(1)
      second.componentId must be("sftp")
      second.eventType must be(action.actionType.toString)
      first.systemId must (equal("A2") or equal("A9") or equal("A10"))
      second.timestamp must be(action.time.getTime)
      second.userId must be("system")
      val last = events(2)
      last.componentId must be("sftp")
      last.eventType must be(action.actionType.toString)
      first.systemId must (equal("A2") or equal("A9") or equal("A10"))
      last.timestamp must be(action.time.getTime)
      last.userId must be("system")
    }
    "filter system events" in {
      val tools = new CuratorToolsImpl(clientScope, log)
      val sys = new FTPSystemEvents("/test,/data", tools)
      val action = new SftpAction(actionType = SftpActionTypes.Upload, time = new Date(), user = "me", detail = "/zip/testFile.zip")
      val events = sys.createSystemEvents(action)
      events.size must be(0)
    }
    "skip dir test for login" in {
      val tools = new CuratorToolsImpl(clientScope, log)
      val sys = new FTPSystemEvents("/test,/data", tools)
      val action = new SftpAction(actionType = SftpActionTypes.Login, time = new Date(), user = "me", detail = "123")
      val events = sys.createSystemEvents(action)
      events.size must be(3)
      val first = events(0)
      first.componentId must be("sftp")
      first.eventType must be(action.actionType.toString)
      first.systemId must (equal("A2") or equal("A9") or equal("A10"))
      first.timestamp must be(action.time.getTime)
      first.userId must be("system")
      val second = events(1)
      second.componentId must be("sftp")
      second.eventType must be(action.actionType.toString)
      first.systemId must (equal("A2") or equal("A9") or equal("A10"))
      second.timestamp must be(action.time.getTime)
      second.userId must be("system")
      val last = events(2)
      last.componentId must be("sftp")
      last.eventType must be(action.actionType.toString)
      first.systemId must (equal("A2") or equal("A9") or equal("A10"))
      last.timestamp must be(action.time.getTime)
      last.userId must be("system")
    }
  }
}