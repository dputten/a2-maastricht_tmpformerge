/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */
package csc.sectioncontrol.auditor.checks.exportfiles

import java.io.File
import org.scalatest.{ BeforeAndAfterEach, WordSpec }
import org.scalatest.matchers.MustMatchers
import csc.akkautils.DirectLogging
import csc.sectioncontrol.messages.StatsDuration
import java.util.TimeZone
import org.apache.commons.io.FileUtils
import csc.sectioncontrol.common.DayUtility

class ExportDayZipTest extends WordSpec with MustMatchers with DirectLogging with BeforeAndAfterEach {

  val rootDir = (new File(getClass().getClassLoader().getResource("application.conf").getPath())).getParentFile()
  val testDir = new File(rootDir, "testZip")
  val timeZone = TimeZone.getTimeZone("Europe/Amsterdam")
  val stats = StatsDuration("20130720", DayUtility.fileExportTimeZone)

  "CheckOldFiles" must {
    "delete old files" in {
      testDir.mkdirs()
      //createfiles
      val zip20130717 = new File(testDir, "32150001_20130717.zip")
      zip20130717.createNewFile()
      val zip20130718 = new File(testDir, "32150001_20130718.zip")
      zip20130718.createNewFile()
      val zip20130719 = new File(testDir, "32150001_20130719.zip")
      zip20130719.createNewFile()
      val zip20130720 = new File(testDir, "32150001_20130720.zip")
      zip20130720.createNewFile()
      //run check
      val events = ExportDayZip.checkOldFiles(testDir, ("3215", 3, 2), stats, "sys1", log)
      //check Files
      zip20130717.exists() must be(false)
      zip20130718.exists() must be(true)
      zip20130719.exists() must be(true)
      zip20130720.exists() must be(true)
      events.size must be(3)

      events.exists(_.reason == "1 overtredingbestanden van 3215 zijn niet opgehaald: %s".format(zip20130719.getName)) must be(true)
      events.exists(_.reason == "1 overtredingbestanden van 3215 ouder dan 2 dagen gesignaleerd: %s".format(zip20130718.getName)) must be(true)
      events.exists(_.reason == "Verwijderde overtredings bestanden %s (zijn ouder dan 3 dagen)".format(zip20130717.getCanonicalPath)) must be(true)
    }
    "delete old files with splitted zipfiles" in {
      testDir.mkdirs()
      //createfiles
      val zip20130717 = new File(testDir, "32150001_20130717.zip")
      zip20130717.createNewFile()
      val zip20130717_2 = new File(testDir, "32150001_20130717.z01")
      zip20130717_2.createNewFile()
      val zip20130718 = new File(testDir, "32150001_20130718.zip")
      zip20130718.createNewFile()
      val zip20130718_2 = new File(testDir, "32150001_20130718.z01")
      zip20130718_2.createNewFile()
      val zip20130719 = new File(testDir, "32150001_20130719.zip")
      zip20130719.createNewFile()
      val zip20130720 = new File(testDir, "32150001_20130720.zip")
      zip20130720.createNewFile()
      //run check
      val events = ExportDayZip.checkOldFiles(testDir, ("3215", 3, 2), stats, "sys1", log)
      //check Files
      zip20130717.exists() must be(false)
      zip20130717_2.exists() must be(false)
      zip20130718.exists() must be(true)
      zip20130718_2.exists() must be(true)
      zip20130719.exists() must be(true)
      zip20130720.exists() must be(true)
      events.size must be(3)

      events.exists(_.reason == "1 overtredingbestanden van 3215 zijn niet opgehaald: %s".format(zip20130719.getName)) must be(true)
      events.exists(_.reason == "1 overtredingbestanden van 3215 ouder dan 2 dagen gesignaleerd: %s".format(zip20130718.getName)) must be(true)
      events.exists(_.reason == "Verwijderde overtredings bestanden %s (zijn ouder dan 3 dagen)".format(zip20130717.getCanonicalPath)) must be(true)
    }
    "delete old files with extention" in {
      testDir.mkdirs()
      //createfiles
      val zip20130717 = new File(testDir, "32150001_20130717.1.zip")
      zip20130717.createNewFile()
      val zip20130718 = new File(testDir, "32150001_20130718.1.zip")
      zip20130718.createNewFile()
      val zip20130719 = new File(testDir, "32150001_20130719.1.zip")
      zip20130719.createNewFile()
      val zip20130720 = new File(testDir, "32150001_20130720.1.zip")
      zip20130720.createNewFile()
      //run check
      val events = ExportDayZip.checkOldFiles(testDir, ("3215", 3, 2), stats, "sys1", log)
      //check Files
      zip20130717.exists() must be(false)
      zip20130718.exists() must be(true)
      zip20130719.exists() must be(true)
      zip20130720.exists() must be(true)
      events.size must be(3)

      events.exists(_.reason == "1 overtredingbestanden van 3215 zijn niet opgehaald: %s".format(zip20130719.getName)) must be(true)
      events.exists(_.reason == "1 overtredingbestanden van 3215 ouder dan 2 dagen gesignaleerd: %s".format(zip20130718.getName)) must be(true)
      events.exists(_.reason == "Verwijderde overtredings bestanden %s (zijn ouder dan 3 dagen)".format(zip20130717.getCanonicalPath)) must be(true)
    }
    "delete old files double date with extention" in {
      testDir.mkdirs()
      //createfiles
      val zip20130717 = new File(testDir, "32150001_20130717.1.zip")
      zip20130717.createNewFile()
      val zip20130717_2 = new File(testDir, "32150001_20130717.2.zip")
      zip20130717_2.createNewFile()
      val zip20130718 = new File(testDir, "32150001_20130718.1.zip")
      zip20130718.createNewFile()
      val zip20130719 = new File(testDir, "32150001_20130719.1.zip")
      zip20130719.createNewFile()
      val zip20130720 = new File(testDir, "32150001_20130720.1.zip")
      zip20130720.createNewFile()
      //run check
      val events = ExportDayZip.checkOldFiles(testDir, ("3215", 3, 2), stats, "sys1", log)
      //check Files
      zip20130717.exists() must be(false)
      zip20130717_2.exists() must be(false)
      zip20130718.exists() must be(true)
      zip20130719.exists() must be(true)
      zip20130720.exists() must be(true)
      events.size must be(4)

      events.exists(_.reason == "1 overtredingbestanden van 3215 zijn niet opgehaald: %s".format(zip20130719.getName)) must be(true)
      events.exists(_.reason == "1 overtredingbestanden van 3215 ouder dan 2 dagen gesignaleerd: %s".format(zip20130718.getName)) must be(true)
      events.exists(_.reason == "Verwijderde overtredings bestanden %s (zijn ouder dan 3 dagen)".format(zip20130717.getCanonicalPath)) must be(true)
      events.exists(_.reason == "Verwijderde overtredings bestanden %s (zijn ouder dan 3 dagen)".format(zip20130717_2.getCanonicalPath)) must be(true)
    }
  }

  override protected def afterEach() {
    super.afterEach()
    FileUtils.deleteQuietly(testDir)
  }
}