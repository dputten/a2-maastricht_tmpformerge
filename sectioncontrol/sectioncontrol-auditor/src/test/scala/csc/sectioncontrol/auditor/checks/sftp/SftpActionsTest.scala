/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.auditor.checks.sftp

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import java.text.SimpleDateFormat
import java.util.Calendar

class SftpActionsTest extends WordSpec with MustMatchers {
  val timeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
  val year = Calendar.getInstance().get(Calendar.YEAR)

  "Parser" must {
    "parse login" in {
      val line = """Apr  9 11:32:15 puppet sftp-server[17030]: session opened for local user root from [20.32.124.196]"""
      val action = SftpActions.parseLine(line)
      action.isDefined must be(true)
      action.get.actionType must be(SftpActionTypes.Login)
      action.get.user must be("root")
      action.get.detail must be("17030")
      action.get.time must be(timeFormat.parse("9/4/%4d 11:32:15".format(year)))
    }
    "parse logout" in {
      val line = """Apr 10 10:54:46 puppet sftp-server[4348]: session closed for local user root from [20.32.124.216]"""
      val action = SftpActions.parseLine(line)
      action.isDefined must be(true)
      action.get.actionType must be(SftpActionTypes.LogOut)
      action.get.user must be("root")
      action.get.detail must be("4348")
      action.get.time must be(timeFormat.parse("10/4/%4d 10:54:46".format(year)))
    }
    "parse read" in {
      val line = """Apr 10 10:36:13 puppet sftp-server[1385]: open "/home/rvermazeren/test/allstatus.sh" flags READ mode 0666"""
      val action = SftpActions.parseLine(line)
      action.isDefined must be(true)
      action.get.actionType must be(SftpActionTypes.Download)
      action.get.user must be("")
      action.get.detail must be("/home/rvermazeren/test/allstatus.sh")
      action.get.time must be(timeFormat.parse("10/4/%4d 10:36:13".format(year)))
    }
    "parse write" in {
      val line = """Apr  9 10:41:31 puppet sftp-server[16337]: open "/home/rbakker2/test.txt" flags WRITE,CREATE,TRUNCATE,EXCL mode 0666"""
      val action = SftpActions.parseLine(line)
      action.isDefined must be(true)
      action.get.actionType must be(SftpActionTypes.Upload)
      action.get.user must be("")
      action.get.detail must be("/home/rbakker2/test.txt")
      action.get.time must be(timeFormat.parse("9/4/%4d 10:41:31".format(year)))
    }
    "parse delete" in {
      val line = """Apr  9 10:36:53 puppet sftp-server[16193]: remove name "/home/rbakker2/test.txt""""
      val action = SftpActions.parseLine(line)
      action.isDefined must be(true)
      action.get.actionType must be(SftpActionTypes.Delete)
      action.get.user must be("")
      action.get.detail must be("/home/rbakker2/test.txt")
      action.get.time must be(timeFormat.parse("9/4/%4d 10:36:53".format(year)))
    }
    "find user" in {
      SftpActions.parseLine("""Apr  9 10:47:04 puppet sftp-server[16450]: session opened for local user rbakker2 from [20.32.124.151]""").isDefined must be(true)
      val action = SftpActions.parseLine("""Apr  9 11:00:36 puppet sftp-server[16450]: remove name "/home/rbakker2/test.txt"""")
      action.get.actionType must be(SftpActionTypes.Delete)
      action.get.user must be("rbakker2")
      action.get.detail must be("/home/rbakker2/test.txt")
      action.get.time must be(timeFormat.parse("9/4/%4d 11:00:36".format(year)))
      SftpActions.parseLine("""Apr  9 11:00:39 puppet sftp-server[16450]: session closed for local user rbakker2 from [20.32.124.151]""").isDefined must be(true)
      val action2 = SftpActions.parseLine("""Apr  9 11:00:36 puppet sftp-server[16450]: remove name "/home/rbakker2/test.txt"""")
      action2.get.actionType must be(SftpActionTypes.Delete)
      action2.get.user must be("")
      action2.get.detail must be("/home/rbakker2/test.txt")
      action2.get.time must be(timeFormat.parse("9/4/%4d 11:00:36".format(year)))
    }
  }
}