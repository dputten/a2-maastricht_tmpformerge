/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.auditor.output_events

import akka.actor.{ ActorSystem, UnhandledMessage }
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import com.github.sstone.amqp.Amqp.{ Ok, Publish }
import csc.amqp.JsonSerializers
import csc.sectioncontrol.messages.SystemEvent
import net.liftweb.json.{ DefaultFormats, Formats }
import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterEach, WordSpec }

class SystemEventPublisherTest extends TestKit(ActorSystem("TestSystem"))
  with WordSpec with MustMatchers with BeforeAndAfterEach with JsonSerializers {

  override implicit val formats: Formats = DefaultFormats
  implicit val testSystem = ActorSystem("SystemEventPublisherTest")

  var sut: TestActorRef[SystemEventPublisher] = _
  var producer = TestProbe()
  val amqpConfig = AmqpConfig(endpoint = "endpoint", routingKey = "routingKey")

  override protected def beforeEach() {
    sut = TestActorRef(new SystemEventPublisher(amqpConfig.endpoint, amqpConfig.routingKey, producer.ref))
  }

  override protected def afterEach() {
    sut.stop()
  }

  "A SystemEventPublisher" must {
    "forward a system event received on the event stream to the queue" in {
      val systemEvent = SystemEvent(componentId = "Auditor", timestamp = 1L, systemId = "A4", eventType = "OpenDoor", userId = "testuser")

      testSystem.eventStream.publish(systemEvent)

      producer.expectMsgPF() {
        case Publish(amqpConfig.endpoint, amqpConfig.routingKey, _, _, _, _) ⇒
          // Send a reply to satisfy the ask (?) in PublisherActor and exit gracefully
          producer.send(producer.sender, Ok)
      }
    }

    "ignore events other than SystemEvent" in {
      testSystem.eventStream.publish("Should be ignored")
      producer.expectNoMsg()
    }
  }

}

case class AmqpConfig(endpoint: String, routingKey: String)