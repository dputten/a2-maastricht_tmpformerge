/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.auditor.checks.sftp

import akka.testkit.{ TestProbe, TestKit }
import akka.actor.{ Props, ActorSystem }
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers
import csc.akkautils.DirectLogging
import csc.sectioncontrol.storage.Paths
import csc.sectioncontrol.auditor.checks.SFTPAuditor
import java.io.File
import csc.sectioncontrol.messages.SystemEvent
import akka.util.duration._
import akka.camel.CamelExtension
import csc.curator.CuratorTestServer
import csc.curator.utils.CuratorToolsImpl

class SftpAuditorTest extends TestKit(ActorSystem("SftpAuditorTest")) with WordSpec with MustMatchers
  with BeforeAndAfterAll with CuratorTestServer with DirectLogging {

  override def beforeEach() {
    super.beforeEach()
    //simulate systems
    val tools = new CuratorToolsImpl(clientScope, log)
    tools.createEmptyPath(Paths.Systems.getSystemPath("A2"))
  }

  override def afterAll() {
    system.shutdown()
    super.afterAll()
  }

  "SftpAuditor" must {
    "must pickup messages" in {
      val resource = getClass.getClassLoader().getResource("sftp.log")
      resource must not be (null)
      val file = new File(resource.getFile)
      val probe = TestProbe()
      system.eventStream.subscribe(probe.ref, classOf[SystemEvent])
      val tools = new CuratorToolsImpl(clientScope, log)
      val uri = "stream:file?fileName=%s".format(file.getAbsolutePath)
      val actor = system.actorOf(Props(new SFTPAuditor(uri, "/home/rbakker2/", tools)))
      CamelExtension.get(system).awaitActivation(actor, 3 seconds)
      val events = probe.receiveN(4)
      val systemEvents = events.map { case m: SystemEvent ⇒ m }
      systemEvents(0).componentId must be("sftp")
      systemEvents(0).eventType must be(SftpActionTypes.Login.toString)
      systemEvents(0).systemId must be("A2")
      systemEvents(0).userId must be("system")
      systemEvents(1).componentId must be("sftp")
      systemEvents(1).eventType must be(SftpActionTypes.Upload.toString)
      systemEvents(1).systemId must be("A2")
      systemEvents(1).userId must be("system")
      systemEvents(2).componentId must be("sftp")
      systemEvents(2).eventType must be(SftpActionTypes.Delete.toString)
      systemEvents(2).systemId must be("A2")
      systemEvents(2).userId must be("system")
      systemEvents(3).componentId must be("sftp")
      systemEvents(3).eventType must be(SftpActionTypes.LogOut.toString)
      systemEvents(3).systemId must be("A2")
      systemEvents(3).userId must be("system")

      probe.expectNoMsg(1 second)
      system.eventStream.unsubscribe(probe.ref)
      system.stop(actor)
    }
  }
}