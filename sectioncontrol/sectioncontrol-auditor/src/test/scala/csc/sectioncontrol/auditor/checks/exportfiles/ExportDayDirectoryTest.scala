/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */
package csc.sectioncontrol.auditor.checks.exportfiles

import java.io.File
import org.scalatest.{ BeforeAndAfterEach, WordSpec }
import org.scalatest.matchers.MustMatchers
import csc.akkautils.DirectLogging
import csc.sectioncontrol.messages.StatsDuration
import java.util.TimeZone
import org.apache.commons.io.FileUtils

class ExportDayDirectoryTest extends WordSpec with MustMatchers with DirectLogging with BeforeAndAfterEach {

  val rootDir = (new File(getClass().getClassLoader().getResource("application.conf").getPath())).getParentFile()
  val testDir = new File(rootDir, "testDir")
  val timeZone = TimeZone.getTimeZone("Europe/Amsterdam")
  val stats = StatsDuration("20130720", timeZone)

  "CheckOldFiles" must {
    "delete old files" in {
      //createfiles
      val dir20130717 = new File(testDir, "20130717")
      dir20130717.mkdirs()
      val dir20130718 = new File(testDir, "20130718")
      dir20130718.mkdirs()
      val dir20130719 = new File(testDir, "20130719")
      dir20130719.mkdirs()
      val dir20130720 = new File(testDir, "20130720")
      dir20130720.mkdirs()
      //run check
      val events = ExportDayDirectory.checkOldFiles(rootDir, (testDir.getName, 3, 2), stats, "sys1", log)
      //check Files
      dir20130717.exists() must be(false)
      dir20130718.exists() must be(true)
      dir20130719.exists() must be(true)
      dir20130720.exists() must be(true)
      events.size must be(3)

      events.exists(_.reason == "1 overtredingbestanden van %s zijn niet opgehaald: 20130719".format(testDir.getName)) must be(true)
      events.exists(_.reason == "1 overtredingbestanden van %s ouder dan 2 dagen gesignaleerd: 20130718".format(testDir.getName)) must be(true)
      events.exists(_.reason == "Verwijderde overtredings bestanden %s (zijn ouder dan 3 dagen)".format(dir20130717.getCanonicalPath)) must be(true)
    }
    "delete old files with extention" in {
      //createfiles
      val dir20130717 = new File(testDir, "20130717.1")
      dir20130717.mkdirs()
      val dir20130718 = new File(testDir, "20130718.1")
      dir20130718.mkdirs()
      val dir20130719 = new File(testDir, "20130719.1")
      dir20130719.mkdirs()
      val dir20130720 = new File(testDir, "20130720.1")
      dir20130720.mkdirs()
      //run check
      val events = ExportDayDirectory.checkOldFiles(rootDir, (testDir.getName, 3, 2), stats, "sys1", log)
      //check Files
      dir20130717.exists() must be(false)
      dir20130718.exists() must be(true)
      dir20130719.exists() must be(true)
      dir20130720.exists() must be(true)
      events.size must be(3)

      events.exists(_.reason == "1 overtredingbestanden van %s zijn niet opgehaald: 20130719.1".format(testDir.getName)) must be(true)
      events.exists(_.reason == "1 overtredingbestanden van %s ouder dan 2 dagen gesignaleerd: 20130718.1".format(testDir.getName)) must be(true)
      events.exists(_.reason == "Verwijderde overtredings bestanden %s (zijn ouder dan 3 dagen)".format(dir20130717.getCanonicalPath)) must be(true)
    }
    "delete old files double date with extention" in {
      //createfiles
      val dir20130717 = new File(testDir, "20130717.1")
      dir20130717.mkdirs()
      val dir20130717_2 = new File(testDir, "20130717.2")
      dir20130717_2.mkdirs()
      val dir20130718 = new File(testDir, "20130718.1")
      dir20130718.mkdirs()
      val dir20130719 = new File(testDir, "20130719.1")
      dir20130719.mkdirs()
      val dir20130720 = new File(testDir, "20130720.1")
      dir20130720.mkdirs()
      //run check
      val events = ExportDayDirectory.checkOldFiles(rootDir, (testDir.getName, 3, 2), stats, "sys1", log)
      //check Files
      dir20130717.exists() must be(false)
      dir20130717_2.exists() must be(false)
      dir20130718.exists() must be(true)
      dir20130719.exists() must be(true)
      dir20130720.exists() must be(true)
      events.size must be(4)

      events.exists(_.reason == "1 overtredingbestanden van %s zijn niet opgehaald: 20130719.1".format(testDir.getName)) must be(true)
      events.exists(_.reason == "1 overtredingbestanden van %s ouder dan 2 dagen gesignaleerd: 20130718.1".format(testDir.getName)) must be(true)
      events.exists(_.reason == "Verwijderde overtredings bestanden %s (zijn ouder dan 3 dagen)".format(dir20130717.getCanonicalPath)) must be(true)
      events.exists(_.reason == "Verwijderde overtredings bestanden %s (zijn ouder dan 3 dagen)".format(dir20130717_2.getCanonicalPath)) must be(true)
    }
  }

  override protected def afterEach() {
    super.afterEach()
    FileUtils.deleteQuietly(testDir)
  }
}