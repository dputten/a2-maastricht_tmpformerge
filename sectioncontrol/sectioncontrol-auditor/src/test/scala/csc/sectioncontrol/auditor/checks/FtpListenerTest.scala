/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.auditor.checks

import listeners.FtpActionsListener
import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import java.net.Socket
import java.io.PrintWriter
import akka.actor.ActorSystem
import akka.testkit.{ TestProbe, TestActorRef }
import akka.util.Timeout
import akka.util.duration._
import csc.curator.utils.{ CuratorToolsImpl, Curator }
import csc.akkautils.DirectLogging
import csc.sectioncontrol.messages.{ EsaProviderType, SystemEvent }
import csc.config.Path
import akka.camel.CamelExtension
import csc.curator.CuratorTestServer
import csc.sectioncontrol.storage._
import java.text.SimpleDateFormat
import csc.sectioncontrol.storagelayer.{ SectionControlConfig, CorridorConfig }
import csc.sectioncontrol.storage.ZkCorridorInfo
import csc.sectioncontrol.storage.SerialNumber
import csc.sectioncontrol.enforce.nl.run.RegisterViolationsJobConfig
import csc.sectioncontrol.storage.ZkPSHTMIdentification
import csc.sectioncontrol.storage.ZkSystem

class FtpListenerTest extends WordSpec with MustMatchers with BeforeAndAfterAll with DirectLogging with CuratorTestServer {
  implicit val testSystem = ActorSystem("FtpListenerTest")
  var testActor: TestActorRef[FtpActionsListener] = _

  implicit val timeout = Timeout(5 seconds)

  override def beforeEach() {
    super.beforeEach()
    testActor = TestActorRef(new FtpActionsListener(12421, new CuratorMock))
    CamelExtension.get(testSystem).awaitActivation(testActor, 10 seconds)
  }

  override def afterEach() {
    testActor.stop()
    super.afterEach()
  }

  "FtpActionsListener" must {

    "detect TCP connection" in {
      val probe = TestProbe()
      testSystem.eventStream.subscribe(probe.ref, classOf[SystemEvent])
      val radar = new Socket("localhost", 12421)
      val out = new PrintWriter(radar.getOutputStream, true)
      try {
        out.println("Uploaded /folder/20120730/a020001/auto.txt")
      } finally {
        try {
          out.close()
        } finally {
          radar.close()
        }
      }
      val msg = probe.expectMsgType[SystemEvent](1 second)
      msg.reason must be("Uploaded /folder/20120730/a020001/auto.txt")
    }
    "insert correct corridorId" in {
      //fill zookeeper
      val zkStore = new CuratorToolsImpl(clientScope, log)
      createZookeeperData(zkStore)
      //do test
      val connect = new Socket("localhost", 12421)
      val out = new PrintWriter(connect.getOutputStream, true)
      try {
        out.println("Uploaded /root/a0020011/20120730/a020001/auto.txt")
      } finally {
        try {
          out.close()
        } finally {
          connect.close()
        }
      }
      Thread.sleep(1000)

      //check created events
      val pathToEvents = Path(Paths.Systems.getLogEventsPath("eg33"))
      val events = zkStore.getChildren(pathToEvents)
      events.foreach(path ⇒ {
        val systemEvent = zkStore.get[SystemEvent](path)
        val event = systemEvent.get
        event.corridorId must be(Some("E1X1"))
        event.systemId must be("eg33")
        event.componentId must be("ftp")
      })
    }
  }

  val dateFormat = new SimpleDateFormat("yyyyMMdd")

  def createZookeeperData(zkStore: Curator) {

    val folderConfigPath = Path(Paths.Systems.getSystemJobsPath("eg33") + "/registerViolationsJob/config")
    val cfg = new RegisterViolationsJobConfig(exportDirectoryPath = "/folder/export")
    zkStore.put(folderConfigPath, cfg)
    //add corridor info
    val systemPath = Path(Paths.Systems.getConfigPath("eg33"))
    val system = new ZkSystem(id = "eg33",
      name = "eg33",
      title = "eg33",
      violationPrefixId = "a002",
      location = ZkSystemLocation(
        description = "",
        region = "testRegion",
        roadNumber = "1",
        roadPart = "Links",
        viewingDirection = None,
        systemLocation = "Amsterdam"),
      orderNumber = 0,
      maxSpeed = 130,
      compressionFactor = 20,
      retentionTimes = ZkSystemRetentionTimes(
        nrDaysKeepTrafficData = 28,
        nrDaysKeepViolations = 4,
        nrDaysRemindCaseFiles = 5),
      pardonTimes = ZkSystemPardonTimes(pardonTimeEsa_secs = 45000, pardonTimeTechnical_secs = 55000),
      mtmRouteId = "",
      esaProviderType = EsaProviderType.NoProvider,
      minimumViolationTimeSeparation = 1, /*minutes*/
      serialNumber = SerialNumber("sn1234"))
    zkStore.createEmptyPath(systemPath)
    zkStore.set(systemPath, system, 0)

    val corridorPath1 = Path(Paths.Corridors.getConfigPath("eg33", "E1X1"))
    val cor1 = new CorridorConfig(id = "E1X1",
      name = "E1X1",
      roadType = 1,
      serviceType = ServiceType.SectionControl,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = false,
      dynamaxMqId = "",
      approvedSpeeds = Seq[Int](80, 100),
      pshtm = new ZkPSHTMIdentification(useYear = true,
        useMonth = true,
        useDay = true,
        useReportingOfficerId = false,
        useNumberOfTheDay = false,
        useLocationCode = false,
        useHHMCode = false,
        useTypeViolation = false,
        useFixedCharacter = false,
        fixedCharacter = "B"),
      info = new ZkCorridorInfo(corridorId = 11,
        locationCode = "1",
        reportId = "TCS A2 Links  39.7 - 54.9",
        reportHtId = "A020011",
        reportPerformance = true,
        locationLine1 = "line",
        locationLine2 = None),
      startSectionId = "20",
      endSectionId = "40",
      allSectionIds = Seq[String]("20", "40"),
      serviceConfig = new SectionControlConfig())
    zkStore.createEmptyPath(corridorPath1)
    zkStore.set(corridorPath1, cor1, 0)
  }

  override protected def afterAll() {
    testSystem.shutdown()
    super.afterAll()
  }
}

class CuratorMock extends Curator {
  def curator = null

  def serialization = null

  /**
   * Utility method to get the stats for a zookeeper item
   */
  def stat(path: String) = null

  def stat(path: Path) = null

  /**
   * Utility method to read values from zookeeper
   */
  def get[T <: AnyRef: Manifest](path: String) = None

  def get[T <: AnyRef: Manifest](path: Path) = None

  def get[T <: AnyRef: Manifest](pathList: Seq[Path]) = None

  def get[T <: AnyRef: Manifest](pathList: Seq[Path], default: T) = None

  def get[T <: AnyRef: Manifest](pathList: Seq[Path], defaults: Map[String, Any]): Option[T] = None

  /**
   * Utility method to read values from zookeeper
   */
  def getVersioned[T <: AnyRef: Manifest](path: String) = None

  def getVersioned[T <: AnyRef: Manifest](path: Path) = None

  def getVersioned[T <: AnyRef: Manifest](pathList: Seq[Path], default: T) = None

  /**
   * Utility method to read child nodes from zookeeper. Returns the full path of each child node.
   */
  def getChildren(parent: String) = null

  def getChildren(parent: Path) = null

  /**
   * Utility method to read child nodes from zookeeper. Returns the local name of each child node.
   */
  def getChildNames(parent: String) = Seq("A2")

  def getChildNames(parent: Path) = Seq("A2")

  def createEmptyPath(path: String) {}

  def createEmptyPath(path: Path) {}

  /**
   * Utility method to insert new values into zookeeper
   */
  def put[T <: AnyRef](path: String, value: T, ephemeral: Boolean) {}

  def put[T <: AnyRef](path: Path, value: T) {}

  /**
   * Utility method to create/write events to zookeeper
   */
  def appendEvent[T <: AnyRef](path: String, value: T) {}

  def appendEvent[T <: AnyRef](path: Path, value: T) {}

  def appendEvent[T <: AnyRef](path: String, value: T, retries: Int) {}

  def appendEvent[T <: AnyRef](path: Path, value: T, retries: Int) {}

  /**
   * Utility method to overwrite values in zookeeper
   */
  def set[T <: AnyRef](path: String, value: T, storedVersion: Int) {}

  def set[T <: AnyRef](path: Path, value: T, storedVersion: Int) {}

  /**
   * Utility method to check the existence of nodes in zookeeper
   */
  def exists(path: String) = false

  def exists(path: Path) = false

  /**
   * Utility method to delete values in zookeeper
   */
  def delete(path: String) {}

  def delete(path: Path) {}

  /**
   * Utility method to delete values in zookeeper recursively
   */
  def deleteRecursive(path: String) {}

  def deleteRecursive(path: Path) {}

  /**
   * Utility method to delete values in zookeeper recursively. Does not fail if the path does not exist.
   */
  def safeDelete(path: String) {}

  def safeDelete(path: Path) {}
}