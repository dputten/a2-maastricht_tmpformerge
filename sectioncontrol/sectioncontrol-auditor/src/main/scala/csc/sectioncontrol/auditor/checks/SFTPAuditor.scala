/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.auditor.checks

import akka.actor.{ ActorLogging, Actor }
import akka.camel.{ CamelMessage, Consumer }

import csc.curator.utils.Curator

import sftp.{ FTPSystemEvents, SftpActions }

class SFTPAuditor(uri: String, baseDirs: String, curator: Curator) extends Actor with ActorLogging with Consumer {

  def endpointUri = uri //Example: "stream:file?fileName=/server/logs/server.log&scanStream=true"
  val systemEventsCreator = new FTPSystemEvents(baseDirs, curator)

  override def preStart() = {
    super.preStart()
    log.info("SFTPAuditor started, endpointUri=%s".format(endpointUri))
  }

  def receive = {
    case msg: CamelMessage ⇒ {
      val record = msg.bodyAs[String]
      val actionOpt = SftpActions.parseLine(record)
      if (actionOpt == None) {
        log.debug("Unknown message received: " + record)
      } else {
        val systemEvents = systemEventsCreator.createSystemEvents(actionOpt.get)
        val eventStream = context.system.eventStream
        systemEvents.foreach(event ⇒ eventStream.publish(event))
      }
    }
  }

}