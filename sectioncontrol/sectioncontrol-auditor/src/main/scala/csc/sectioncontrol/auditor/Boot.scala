package csc.sectioncontrol.auditor

import akka.actor.Props
import akka.util.duration._
import com.typesafe.config.Config
import csc.akkautils.{ EventLogger, GenericBoot }
import csc.amqp.AmqpConnectionConfig
import csc.curator.utils.xml.{ CuratorToolsImplXml, XmlReader }
import csc.curator.utils.{ Curator, CuratorToolsImpl }
import csc.json.lift.EnumerationSerializer
import csc.rabbitmq.RabbitMQ._
import csc.sectioncontrol.auditor.checks.exportfiles.ExportFilesAuditor
import csc.sectioncontrol.auditor.checks.listeners.{ FtpActionsListener, TimeSyncListener }
import csc.sectioncontrol.auditor.checks.{ DiscAuditor, SFTPAuditor }
import csc.sectioncontrol.auditor.input_events.{ SystemEventConsumer, SystemEventListener }
import csc.sectioncontrol.auditor.output_events.SystemEventPublisher
import csc.sectioncontrol.messages._
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storagelayer.StorageFactory
import csc.sectioncontrol.storagelayer.hbasetables.RowKeyDistributorFactory
import net.liftweb.json.{ DefaultFormats, Formats }
import org.apache.curator.framework.CuratorFramework
import org.apache.curator.retry.ExponentialBackoffRetry

class Boot extends GenericBoot {

  override def componentName = "Sectioncontrol-Auditor"

  // TODO RDJ Why is this used as a class variable?
  var curator: Option[CuratorFramework] = None

  /**
   * FIXME
   * Needed because of some problem with enum serialization
   */
  val enumerationFormats = new EnumerationSerializer(
    ZkWheelbaseType,
    VehicleCode,
    ZkIndicationType,
    ServiceType,
    ZkCaseFileType,
    EsaProviderType,
    VehicleImageType,
    SpeedIndicatorType,
    ConfigType)

  override def startupActors() {
    val configuration = actorSystem.settings.config
    val curator = initCurator(configuration)

    // Start auditors
    startSftpAuditor(configuration, curator)
    startFtpAuditor(configuration, curator)
    startTimeSyncAuditor(configuration, curator)
    startDiscAuditor(configuration, curator)
    startExportFilesAuditor(configuration, curator)
    startSystemEventListener(configuration, curator)
    startSystemEventPublisher(configuration, curator)
  }

  override def shutdownActors() {
    curator.foreach(zk ⇒ zk.close())
    curator = None
  }

  private def initCurator(configuration: Config): Curator = {
    configuration.getString("curator.implementation") match {
      case "xml"       ⇒ getXmlCurator(configuration.getString("curator.xml-file-location"))
      case "zookeeper" ⇒ getZookeeperCurator(configuration)
      case invalid ⇒
        log.error("Invalid configuration for curator implementation. Valid are { xml | zookeeper }. Got {}", invalid)
        log.error("Falling back to zookeeper configuration")
        getZookeeperCurator(configuration)
    }
  }

  private def getZookeeperCurator(configuration: Config): Curator = {
    log.info("Using Zookeeper implementation for curator.")
    val zkServerQuorum = configuration.getString("sectioncontrol.auditor.zookeeper.zkServers")
    val zkSessionTimeout = configuration.getInt("sectioncontrol.auditor.zookeeper.sessionTimeout")
    val zkRetries = configuration.getInt("sectioncontrol.auditor.zookeeper.retries")
    curator = StorageFactory.newClient(zkServerQuorum, new ExponentialBackoffRetry(zkSessionTimeout, zkRetries))
    curator.foreach(_.start())
    val zookeeperClient = new CuratorToolsImpl(curator, log) {
      override def extendFormats(defaultFormats: Formats): Formats = {
        super.extendFormats(defaultFormats) + enumerationFormats
      }
    }
    RowKeyDistributorFactory.init(zookeeperClient)
    log.info("Zookeeper curator initialized.")
    zookeeperClient
  }

  private def getXmlCurator(xmlFileLocation: String): Curator = {
    log.info("Using XML implementation for curator.")
    val xmlClient = new CuratorToolsImplXml(new XmlReader(xmlFileLocation, log), log, DefaultFormats + enumerationFormats)
    log.info("XML curator initialized.")
    xmlClient
  }

  private def startSftpAuditor(configuration: Config, curator: Curator) = {
    if (configuration.getBoolean("sectioncontrol.auditor.sftp.enabled")) {
      log.info("Starting SFTP auditor...")
      val uri = configuration.getString("sectioncontrol.auditor.sftp.uri")
      val base = configuration.getString("sectioncontrol.auditor.sftp.basePath")
      actorSystem.actorOf(Props(new SFTPAuditor(uri, base, curator)), "SFTPAuditor")
    }
  }

  private def startFtpAuditor(configuration: Config, curator: Curator) = {
    if (configuration.getBoolean("sectioncontrol.auditor.ftp.enabled")) {
      log.info("Starting FTP auditor...")
      val port = configuration.getInt("sectioncontrol.auditor.ftp.port")
      actorSystem.actorOf(Props(new FtpActionsListener(port, curator)), "ftpListener")
    }
  }

  private def startTimeSyncAuditor(configuration: Config, curator: Curator) = {
    if (configuration.getBoolean("sectioncontrol.auditor.timesync.enabled")) {
      log.info("Starting TimeSync auditor...")
      val logFile = configuration.getString("sectioncontrol.auditor.timesync.logfile")
      val pattern = configuration.getString("sectioncontrol.auditor.timesync.pattern")
      actorSystem.actorOf(Props(new TimeSyncListener(logFile, pattern, curator)), "ntpListener")
    }
  }

  private def startDiscAuditor(configuration: Config, curator: Curator) = {
    if (configuration.getBoolean("sectioncontrol.auditor.disc.enabled")) {
      log.info("Starting Disc auditor...")
      val limit = configuration.getInt("sectioncontrol.auditor.disc.freeSpaceLimit")
      val file = configuration.getString("sectioncontrol.auditor.disc.fileSystems")
      val duration = configuration.getInt("sectioncontrol.auditor.disc.pollMinutes")
      val disc = actorSystem.actorOf(Props(new DiscAuditor(file, limit, curator)), "DiscAuditor")
      actorSystem.scheduler.schedule(1 minute, duration minutes, disc, Wakeup)
    }
  }

  private def startExportFilesAuditor(configuration: Config, curator: Curator) = {
    if (configuration.getBoolean("sectioncontrol.auditor.export.enabled")) {
      log.info("Starting Export Files auditor...")
      val duration = configuration.getInt("sectioncontrol.auditor.export.pollMinutes")
      val export = actorSystem.actorOf(Props(new ExportFilesAuditor(curator)), "ExportFilesAuditor")
      actorSystem.scheduler.schedule(1 minute, duration minutes, export, Wakeup)
    }
  }

  // Used by startConsumer and startProducer. RabbitMQ needs an actor system
  implicit lazy val system = actorSystem

  private def startSystemEventListener(configuration: Config, curator: Curator) = {
    if (configuration.getBoolean("sectioncontrol.auditor.systemevent.listener.enabled")) {
      log.info("Starting System Event listener...")

      val queueName = configuration.getString("sectioncontrol.auditor.systemevent.listener.queueName")
      val amqpConfig = AmqpConnectionConfig("sectioncontrol.auditor.systemevent.amqpConnection", configuration)
      val systemEventListener = actorSystem.actorOf(Props(new SystemEventListener()))
      val systemEventConsumer = actorSystem.actorOf(Props(new SystemEventConsumer(systemEventListener)))
      startConsumer(amqpConfig, queueName, systemEventConsumer)
    }
  }

  private def startSystemEventPublisher(configuration: Config, curator: Curator) = {
    if (configuration.getBoolean("sectioncontrol.auditor.systemevent.publisher.enabled")) {
      log.info("Starting System Event publisher...")

      configuration.getString("sectioncontrol.auditor.systemevent.publisher.method") match {
        case "amqp" ⇒
          log.info("Using RabbitMQ as implementation")
          val exchange = configuration.getString("sectioncontrol.auditor.systemevent.publisher.exchange")
          // TODO DECIDE Could also be hardcoded or based on systemId from configuration
          val routingKey = configuration.getString("sectioncontrol.auditor.systemevent.publisher.routing-key")
          val amqpConfig = AmqpConnectionConfig("sectioncontrol.auditor.systemevent.amqpConnection", configuration)
          val producer = startProducer(amqpConfig)
          actorSystem.actorOf(Props(new SystemEventPublisher(exchange, routingKey, producer)))

        case "zookeeper" ⇒
          log.info("Using Zookeeper as implementation")
          val translateToQueuePath = (ev: SystemEvent) ⇒ Paths.Systems.getEventQueuePath(ev.systemId)
          actorSystem.actorOf(Props(EventLogger.createDynamicPathEventLogger[SystemEvent](curator, translateToQueuePath)), "EventLogger_" + actorSystem.name)
      }
    }
  }

}

object Wakeup
