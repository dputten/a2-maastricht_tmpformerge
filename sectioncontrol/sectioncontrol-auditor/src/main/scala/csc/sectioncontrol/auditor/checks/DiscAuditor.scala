/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.auditor.checks

import java.io.File
import akka.actor.Actor

import csc.akkautils.DirectLogging

import csc.curator.utils.Curator

import csc.sectioncontrol.messages.SystemEvent
import csc.sectioncontrol.storage.Paths
import csc.sectioncontrol.auditor.Wakeup

class DiscAuditor(fileSystemsList: String, freeSpaceLimit: Int, curator: Curator) extends Actor with DirectLogging {
  val fileSystems: List[String] = fileSystemsList.split(",").map(_.trim).toList

  def receive = {
    case Wakeup ⇒ check()
  }

  def check() {
    fileSystems.foreach(fileSystem ⇒ {
      val file = new java.io.File(fileSystem)
      if (file.exists) {
        val spaces = getSpaces(file)
        if (spaces._3 > freeSpaceLimit) {
          val message = "Het overschrijden van de limiet van %d%% van de maximale opslagcapaciteit (%d%%) in %s".format(freeSpaceLimit, spaces._3, fileSystem)
          log.info(message)
          getSystemIds().foreach(systemId ⇒ {
            logSystemEvent(component = "opslagcapaciteit", systemId = systemId, message = message)
          })
        } else {
          log.debug("Disk space checked on %s -> %s.".format(fileSystem, spaces))
        }
      } else {
        log.warning("Unknown file system %s.".format(fileSystem))
      }
    })
  }

  private def getSpaces(file: File): (Long, Long, Int) = {
    val totalSpace = file.getTotalSpace //total disk space in bytes.
    val usableSpace = file.getUsableSpace ///unallocated / free disk space in bytes.
    val ratio = 1 - usableSpace.toFloat / totalSpace.toFloat
    (totalSpace, usableSpace, (ratio * 100).round)
  }
  private def logSystemEvent(component: String, systemId: String, message: String) {
    val alarmEvent = SystemEvent(component, System.currentTimeMillis(), systemId, "DiskSpace", "system",
      reason = Some(message))
    context.system.eventStream.publish(alarmEvent)
  }

  private def getSystemIds(): Seq[String] = {
    curator.getChildNames(Paths.Systems.getDefaultPath)
  }

}