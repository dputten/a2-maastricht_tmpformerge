/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.auditor.checks.sftp

import csc.curator.utils.Curator
import csc.sectioncontrol.messages.SystemEvent
import csc.sectioncontrol.storage.Paths
import csc.sectioncontrol.auditor.i18n.Messages

/**
 * Listener used when SFTP connection is used.
 * @param baseDirs
 * @param curator
 */
class FTPSystemEvents(baseDirs: String, curator: Curator) {
  val baseDirList = baseDirs.split(",").map(_.trim).toList
  //TODO: Rewrite to use the AuditorListener and map events to corridors.
  //This isn't done yet because this protocol isn't used yet.
  def createSystemEvents(action: SftpAction): Seq[SystemEvent] = {
    //get all systems
    val systems = curator.getChildNames(Paths.Systems.getDefaultPath)
    //filter files
    val filteredSystems = if (action.actionType == SftpActionTypes.Download ||
      action.actionType == SftpActionTypes.Upload ||
      action.actionType == SftpActionTypes.Delete) {
      val filePartOfDirs = baseDirList.foldLeft(false) {
        (isPart, dir) ⇒ action.detail.startsWith(dir)
      }
      //RB: possible to add a check to select only the systems the file belongs to
      //if file is part of baseDir return all systems otherwise create empty list
      if (filePartOfDirs) {
        systems
      } else {
        Seq()
      }
    } else {
      systems
    }
    filteredSystems.map(systemId ⇒ {
      val reason = action.actionType match {
        case SftpActionTypes.Login    ⇒ Messages("csc.sectioncontrol.auditor.reason.Login").format(action.user)
        case SftpActionTypes.LogOut   ⇒ Messages("csc.sectioncontrol.auditor.reason.LogOut").format(action.user)
        case SftpActionTypes.Upload   ⇒ Messages("csc.sectioncontrol.auditor.reason.Upload").format(action.detail, action.user)
        case SftpActionTypes.Download ⇒ Messages("csc.sectioncontrol.auditor.reason.Download").format(action.detail, action.user)
        case SftpActionTypes.Delete   ⇒ Messages("csc.sectioncontrol.auditor.reason.Delete").format(action.detail, action.user)
      }
      SystemEvent(componentId = "sftp",
        timestamp = action.time.getTime,
        systemId = systemId,
        eventType = action.actionType.toString,
        userId = "system",
        reason = Some(reason))

    })
  }
}