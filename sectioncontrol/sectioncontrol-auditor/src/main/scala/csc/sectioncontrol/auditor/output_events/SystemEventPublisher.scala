package csc.sectioncontrol.auditor.output_events

import akka.actor.{ ActorLogging, ActorRef }
import akka.event.LoggingReceive
import csc.amqp.{ AmqpSerializers, PublisherActor }
import csc.sectioncontrol.messages.SystemEvent
import net.liftweb.json.{ DefaultFormats, Formats }

/**
 * Forwards SystemEvents received on the Event Bus to an AMQP compliant queue
 *
 * @param producerEndpoint    The endpoint / exchange to send the messages to
 * @param routingKey  The routing key used by AMQP to route the message to the correct queue(s)
 * @param producer    The actor that communicates with the AMQP message broker
 */
class SystemEventPublisher(val producerEndpoint: String, routingKey: String, val producer: ActorRef)
  extends PublisherActor with ActorLogging {

  val ApplicationId: String = "SystemEventPublisher"

  // No enums in SystemEvent so DefaultFormats should cover it
  override implicit val formats: Formats = DefaultFormats

  override def preStart() {
    context.system.eventStream.subscribe(self, classOf[SystemEvent])
    super.preStart()
  }

  override def postStop() {
    context.system.eventStream.unsubscribe(self)
    super.postStop()
  }

  def producerRouteKey(msg: AnyRef): Option[String] = {
    Some(routingKey)
  }
}
