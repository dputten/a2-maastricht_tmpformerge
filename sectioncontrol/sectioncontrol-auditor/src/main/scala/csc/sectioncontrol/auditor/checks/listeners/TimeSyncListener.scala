/*
* Copyright (C) 2012 CSC. <http://www.csc.com>
*/
package csc.sectioncontrol.auditor.checks.listeners

import csc.curator.utils.Curator

/**
 * Responsible to log any received ntp message
 */
class TimeSyncListener(filename: String, pattern: String, curator: Curator)
  extends AuditorListener(curator) {

  //"stream:file?fileName=/var/run/daemonlog-fifo&scanStream=true"
  def endpointUri = "stream:file?fileName=%s&scanStream=true".format(filename)

  def filter = pattern

  def componentName = "ntp"

  def getMessage(line: String) = "Het aanpassen van de interne klok (%s)".format(line)
  def getCorridor(line: String) = getSystemIds().map(id ⇒ (id, None)).toMap

}
