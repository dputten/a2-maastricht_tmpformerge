/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.auditor.checks.exportfiles

import akka.actor.ActorLogging
import csc.config.Path
import csc.curator.utils.Curator

import csc.sectioncontrol.messages.StatsDuration
import csc.sectioncontrol.storage.{ ZkSystem, Paths }
import csc.sectioncontrol.enforce.nl.run.RegisterViolationsJobConfig

class SystemExportDirAuditor(curator: Curator) extends SystemExportFilesAuditor with ActorLogging {

  def dailyMaintenance(day: StatsDuration, systemId: String) {
    // read configuration
    val folderConfigPath = Path(Paths.Systems.getSystemJobsPath(systemId) + "/registerViolationsJob/config")
    val folder: Option[java.io.File] = curator.get[RegisterViolationsJobConfig](folderConfigPath) match {
      case None ⇒
        log.error("Could not find configuration for registerViolationsJob under %s".format(folderConfigPath))
        None
      case Some(conf) ⇒
        val folder = new java.io.File(conf.exportDirectoryPath)
        if (folder.exists) Some(folder) else None
    }
    val systemPath = Paths.Systems.getConfigPath(systemId)
    val violationsConf: Option[(String, Int, Int)] = curator.get[ZkSystem](Path(systemPath)) match {
      case Some(system) ⇒ Some(system.violationPrefixId, system.retentionTimes.nrDaysKeepViolations, system.retentionTimes.nrDaysRemindCaseFiles)
      case None ⇒
        log.error("Could not find configuration for system under %s".format(systemPath))
        None
    }
    //
    //check old folders for zaakbestanden
    (folder, violationsConf) match {
      case (Some(f), Some(conf)) ⇒ {
        val events = ExportDayDirectory.checkOldFiles(f, conf, day, systemId, log)
        events.foreach(sendEvent(_))
      }
      case any ⇒ log.error("Unexpected configuration for daily check: %s".format(any))
    }
  }

}