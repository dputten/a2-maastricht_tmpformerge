/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.auditor.checks.exportfiles

import org.joda.time.DateTime

import akka.actor.{ Props, ActorRef, Actor, ActorLogging }

import csc.curator.utils.Curator

import csc.sectioncontrol.auditor.Wakeup
import csc.sectioncontrol.storage.{ Paths, ZkExportZipConfig }
import csc.sectioncontrol.messages.StatsDuration
import csc.sectioncontrol.common.DayUtility

class ExportFilesAuditor(curator: Curator) extends Actor with ActorLogging {

  private var systemActor: ActorRef = _
  private var lastDay = 0

  override def preStart() {
    super.preStart()
    val config = getConfig()
    log.info("ExportFilesAuditor config: {}", config)

    systemActor = if (config.isEmpty) {
      log.info("Started ExportFilesAuditor with Export Directories")
      context.actorOf(Props(new SystemExportDirAuditor(curator)), "SystemExportDirAuditor")
    } else {
      log.info("Started ExportFilesAuditor with Export zip files")
      context.actorOf(Props(new SystemExportZipAuditor(curator)), "SystemExportZipAuditor")
    }
  }

  def receive = {
    case Wakeup ⇒ {
      val now = new DateTime()
      val dayNr = now.getDayOfYear
      //test need to execute?
      if (dayNr != lastDay) {
        lastDay = dayNr
        //find all systems
        val systems = curator.getChildNames(Paths.Systems.getDefaultPath)
        //make requests for all systems
        val day = StatsDuration(now.getMillis, DayUtility.fileExportTimeZone)
        systems.foreach(sys ⇒ {
          systemActor ! new ExportFilesCheck(day, sys)
        })
      }
    }
  }

  def getConfig(): Option[ZkExportZipConfig] = {
    val path = Paths.Configuration.getExportZipConfigPath
    curator.get[ZkExportZipConfig](path)
  }

}