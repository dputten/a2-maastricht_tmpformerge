/*
* Copyright (C) 2012 CSC. <http://www.csc.com>
*/
package csc.sectioncontrol.auditor.checks.listeners

import util.matching.Regex
import akka.actor.{ ActorLogging, Actor }
import akka.camel.{ CamelMessage, Consumer }

import csc.curator.utils.Curator

import csc.sectioncontrol.messages.SystemEvent
import csc.sectioncontrol.storage.Paths

abstract class AuditorListener(curator: Curator) extends Actor with ActorLogging with Consumer {

  def endpointUri: String //Example: "stream:file?filename=/server/logs/server.log&scanStream=true"

  def filter: String //Example: ".*"

  override def preStart() = {
    super.preStart()
    log.info("AuditorListener started, endpointUri=%s filter=%s, componentName=%s",
      endpointUri, filter, componentName)
  }

  val filterRegex = new Regex(filter)

  def componentName: String

  def getMessage(line: String): String
  def getCorridor(line: String): Map[String, Option[String]]

  def receive = {
    case msg: CamelMessage ⇒ {
      val record = msg.bodyAs[String]
      record match {
        case filterRegex() ⇒ processLogRecord(record)
        case _             ⇒ log.debug("AuditorListener message filtered out: %s", msg)
      }
    }
    //receive an unknown message
    case msg: Any ⇒ {
      log.error("Unknown message received %s", msg)
    }
  }

  private def processLogRecord(record: String) {
    log.info("Auditor message: %s", record)

    val corridorMapping = getCorridor(record)
    //if there is a mapping use only those mappings
    val filtered = corridorMapping.filter(_._2.isDefined)
    val systemCorridor = if (filtered.isEmpty) {
      corridorMapping
    } else {
      filtered
    }
    systemCorridor.foreach {
      case (systemId, corridor) ⇒ {
        //TODO AS -> filter out FTP action for one system based on the filename prefix for events which can't be mapped to a corridor
        val alarmEvent = SystemEvent(
          componentId = componentName,
          timestamp = System.currentTimeMillis(),
          systemId = systemId,
          eventType = "Info",
          userId = "system",
          reason = Some(getMessage(record)),
          corridorId = corridor)
        context.system.eventStream.publish(alarmEvent)
      }
    }
  }

  def getSystemIds(): Seq[String] = {
    curator.getChildNames(Paths.Systems.getDefaultPath)
  }
}
