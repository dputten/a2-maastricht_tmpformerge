/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.auditor.checks.exportfiles

import csc.sectioncontrol.messages.{ SystemEvent, StatsDuration }
import akka.event.LoggingAdapter
import java.util.{ Calendar, Date }
import collection.mutable.ListBuffer
import org.apache.commons.io.FileUtils
import java.io.File
import java.text.SimpleDateFormat

/**
 * Object to support the deletion and signaling of old ExportFiles
 * Is removed from EnforceEventListener to be able test it separately.
 */
object ExportDayDirectory {

  /**
   * Check the existing export files. And remove the files which are too old
   *
   * @param filePath The root path
   * @param conf tuple(<enforce Directory> <nrdays to keep export files> <nrDays to signal>)
   * @param day Current day
   * @param systemId the system Id
   * @param log the log adapter to be able to log
   * @return list of SystemEvents
   */
  def checkOldFiles(filePath: java.io.File, conf: (String, Int, Int), day: StatsDuration, systemId: String, log: LoggingAdapter): Seq[SystemEvent] = {
    val folders = filePath.listFiles.filter(_.isDirectory).filter(_.getName.startsWith(conf._1))
    val oldFolders = folders.flatMap(_.listFiles).map(file ⇒ (getDay(file), file)).filter(_._1.getTime < day.start)
    val now = new Date(day.start)
    val threshHoldRemove = getDay(now, conf._2)
    val (toRemindOrSignal, toRemove) = oldFolders.partition(_._1.after(threshHoldRemove))
    val threshHoldSignal = getDay(now, conf._3)
    val (toRemind, toSignal) = toRemindOrSignal.partition(_._1.after(threshHoldSignal))

    val events = new ListBuffer[SystemEvent]()
    if (toRemind.isEmpty)
      log.debug("No old zaakbestanden of %s detected to create a reminder on %s".format(conf._1, day.datum))
    else {
      val names = toRemind.map(_._2.getName()).mkString(":")
      val message = "%d overtredingbestanden van %s zijn niet opgehaald: %s".format(toRemind.size, conf._1, names)
      log.info(message)
      val event = SystemEvent("overtredingbestanden", System.currentTimeMillis(), systemId, "Info", "system", reason = Some(message))
      events += event
    }
    if (toSignal.isEmpty)
      log.debug("No old zaakbestanden of %s detected to signal %s".format(conf._1, day.datum))
    else {
      val names = toSignal.map(_._2.getName()).mkString(":")
      val message = "%d overtredingbestanden van %s ouder dan %s dagen gesignaleerd: %s".format(toSignal.size, conf._1, conf._3, names)
      log.info(message)
      val event = SystemEvent("signaaltijd", System.currentTimeMillis(), systemId, "Info", "system", reason = Some(message))
      events += event
    }
    toRemove.foreach {
      case (time, file) ⇒ {
        FileUtils.deleteDirectory(file)
        val message = "Verwijderde overtredings bestanden %s (zijn ouder dan %d dagen)".format(file.getCanonicalPath, conf._2)
        val event = SystemEvent("bewaartijd", System.currentTimeMillis(), systemId, "Info", "system", reason = Some(message))
        events += event
        log.debug("Old zaakbestand deleted: " + file.getCanonicalPath)
      }
    }
    events
  }

  /**
   * Get the day before nrDays
   * @param date  Start day
   * @param nrDays days before the start day
   * @return start of requested day
   */
  private def getDay(date: Date, nrDays: Int): Date = {
    val cal = Calendar.getInstance()
    cal.setTime(date)
    cal.add(Calendar.DAY_OF_YEAR, -nrDays)
    cal.getTime
  }

  /**
   * Get the day from the file
   * @param file
   * @return
   */
  private def getDay(file: File): Date = {
    val name = file.getName
    if (name.length < 8) {
      return new Date(file.lastModified())
    } else {
      val format = new SimpleDateFormat("yyyyMMdd")
      val date = if (name.length > 8) {
        name.substring(0, 8)
      } else {
        name
      }
      try {
        val parsedDate = format.parse(date)
        val cal = Calendar.getInstance()
        cal.setTime(parsedDate)
        cal.set(Calendar.HOUR_OF_DAY, 0)
        cal.set(Calendar.MINUTE, 0)
        cal.set(Calendar.SECOND, 0)
        cal.set(Calendar.MILLISECOND, 0)
        cal.getTime
      } catch {
        case other ⇒ return new Date(file.lastModified())
      }
    }
  }

}