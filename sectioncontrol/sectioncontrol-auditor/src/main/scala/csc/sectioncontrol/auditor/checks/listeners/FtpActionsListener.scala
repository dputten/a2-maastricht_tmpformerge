/*
* Copyright (C) 2012 CSC. <http://www.csc.com>
*/
package csc.sectioncontrol.auditor.checks.listeners

import csc.curator.utils.Curator
import csc.config.Path

import csc.sectioncontrol.storage.{ ZkCorridor, ZkSystem, Paths }

/**
 * Responsible to log user FTP actions
 */
class FtpActionsListener(portNumber: Int, curator: Curator)
  extends AuditorListener(curator) {

  //"mina:tcp://localhost:12421?textline=true&sync=false"
  def endpointUri = "mina:tcp://localhost:%d?textline=true&sync=false".format(portNumber)

  def filter = ".*"

  def componentName = "ftp"

  def getMessage(line: String) = line

  /**
   * try to map the line to a corridor
   * @param line of format "<action> <path file>"
   * @return corridor of None
   */
  def getCorridor(line: String): Map[String, Option[String]] = {
    val parts = line.split(" ")
    //should be two parts and we need the last part
    if (parts.size == 2) {
      getCorridorMapping(parts.last)
    } else {
      //unexpected format return None for all systems
      getSystemIds().map(id ⇒ (id, None)).toMap
    }
  }

  private def getCorridorMapping(filePath: String): Map[String, Option[String]] = {
    val systems = getSystemIds()
    systems.map(systemId ⇒ {
      val systemPath = Paths.Systems.getConfigPath(systemId)
      curator.get[ZkSystem](Path(systemPath)) match {
        case Some(zkSystem) ⇒ {
          val zkCorridors: List[ZkCorridor] = curator.getChildren(Path(Paths.Corridors.getDefaultPath(systemId))).flatMap(path ⇒ {
            val configPath = Path(Paths.Corridors.getConfigPath(systemId, path.nodes.last.name))
            curator.get[ZkCorridor](configPath)
          }).toList
          val corCfg = zkCorridors.find(cfg ⇒ {
            val ht = "%s%04d".format(zkSystem.violationPrefixId, cfg.info.corridorId)
            filePath.contains(ht)
          })
          (systemId, corCfg.map(_.id))
        }
        case None ⇒ {
          log.error("Could not find configuration for system under %s".format(systemPath))
          (systemId, None)
        }
      }
    }).toMap
  }

}
