/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

package csc.sectioncontrol.auditor.i18n

/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

import java.text.MessageFormat

object Messages {
  val messages = Map(
    "csc.sectioncontrol.auditor.reason.Login" -> "Login door gebruiker %s",
    "csc.sectioncontrol.auditor.reason.Logout" -> "logout van gebruiker %s",
    "csc.sectioncontrol.auditor.reason.Upload" -> "Upload bestand %s door gebruiker %s",
    "csc.sectioncontrol.auditor.reason.Download" -> "Download bestand %s door gebruiker %s",
    "csc.sectioncontrol.auditor.reason.Delete" -> "Delete bestand %s door gebruiker %s")
  /**
   * Translates a message.
   *
   * Uses `java.text.MessageFormat` internally to format the message.
   *
   * @param key the message key
   * @param args the message arguments
   * @return the formatted message or a default rendering if the key wasn’t defined
   */
  def apply(key: String, args: Any*): String = {
    translate(key, args).getOrElse(noMatch(key, args))
  }

  /**
   * Translates a message.
   *
   * Uses `java.text.MessageFormat` internally to format the message.
   *
   * @param key the message key
   * @param args the message arguments
   * @return the formatted message, if this key was defined
   */
  private def translate(key: String, args: Seq[Any]): Option[String] = {
    if (messages.contains(key)) {
      Some(new MessageFormat(messages(key)).format(args.map(_.asInstanceOf[java.lang.Object]).toArray))
    } else {
      None
    }
  }

  private def noMatch(key: String, args: Seq[Any]) = key
}
