package csc.sectioncontrol.auditor.input_events

import akka.actor.{ Actor, ActorLogging, ActorRef }
import akka.event.LoggingReceive
import csc.amqp.AmqpSerializers._
import csc.amqp.GenericConsumer
import csc.sectioncontrol.messages.SystemEvent
import net.liftweb.json.DefaultFormats

class SystemEventConsumer(handler: ActorRef) extends GenericConsumer(handler) {

  override val ApplicationId: String = "SystemEventConsumer"
  override implicit val formats = DefaultFormats
  override def deserializers: Map[String, Deserializer] = Map("SystemEvent" -> toMessage[SystemEvent])

}

class SystemEventListener extends Actor with ActorLogging {

  override protected def receive = LoggingReceive {
    case event: SystemEvent ⇒ context.system.eventStream.publish(event)
    case default            ⇒ log.warning("Unexpected message in {}: {}", this.getClass.getName, default)
  }

}
