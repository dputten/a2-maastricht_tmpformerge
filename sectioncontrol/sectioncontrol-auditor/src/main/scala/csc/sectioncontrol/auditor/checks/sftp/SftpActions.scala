/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.auditor.checks.sftp

import java.util.{ Calendar, Date }
import util.matching.Regex
import java.text.SimpleDateFormat

object SftpActionTypes extends Enumeration {
  val Login, LogOut, Upload, Download, Delete = Value
}

case class SftpAction(actionType: SftpActionTypes.Value, time: Date, user: String, detail: String)

object SftpActions {
  var userProcess = Map[String, String]()

  val loginReg = new Regex("""(\S+)\s+(\d+)\s+(\d{2}:\d{2}:\d{2})\s+\S+\s+sftp-server\[(\d+)\]: session opened for local user (\S+) from.*""")
  val logoutReg = new Regex("""(\S+)\s+(\d+)\s+(\d{2}:\d{2}:\d{2})\s+\S+\s+sftp-server\[(\d+)\]: session closed for local user (\S+) from.*""")
  val readReg = new Regex("""(\S+)\s+(\d+)\s+(\d{2}:\d{2}:\d{2})\s+\S+\s+sftp-server\[(\d+)\]: open "([^"]*)" flags READ mode .*""")
  val writeReg = new Regex("""(\S+)\s+(\d+)\s+(\d{2}:\d{2}:\d{2})\s+\S+\s+sftp-server\[(\d+)\]: open "([^"]*)" flags (\S+) mode .*""")
  val deleteReg = new Regex("""(\S+)\s+(\d+)\s+(\d{2}:\d{2}:\d{2})\s+\S+\s+sftp-server\[(\d+)\]: remove name "([^"]*)".*""")
  /**
   * Parse lines
   * Login
   * Apr  9 11:32:15 puppet sftp-server[17030]: session opened for local user root from [20.32.124.196]
   * Logout
   * Apr 10 10:54:46 puppet sftp-server[4348]: session closed for local user root from [20.32.124.216]
   * Read
   * Apr 10 10:36:13 puppet sftp-server[1385]: open "/home/rvermazeren/test/allstatus.sh" flags READ mode 0666
   * Write
   * Apr  9 10:41:31 puppet sftp-server[16337]: open "/home/rbakker2/test.txt" flags WRITE,CREATE,TRUNCATE,EXCL mode 0666
   * Delete
   * Apr  9 10:36:53 puppet sftp-server[16193]: remove name "/home/rbakker2/test.txt"
   *
   * @param line the received log line
   * @return
   */
  def parseLine(line: String): Option[SftpAction] = {
    line match {
      case loginReg(month, day, time, processId, user) ⇒ {
        val dateTime = createTime(month, day, time)
        userProcess += processId -> user
        Some(new SftpAction(actionType = SftpActionTypes.Login, time = dateTime, user = user, detail = processId))
      }
      case logoutReg(month, day, time, processId, user) ⇒ {
        val dateTime = createTime(month, day, time)
        userProcess -= processId
        Some(new SftpAction(actionType = SftpActionTypes.LogOut, time = dateTime, user = user, detail = processId))
      }
      case readReg(month, day, time, processId, file) ⇒ {
        val dateTime = createTime(month, day, time)
        val user = userProcess.get(processId).getOrElse("")
        Some(new SftpAction(actionType = SftpActionTypes.Download, time = dateTime, user = user, detail = file))
      }
      case writeReg(month, day, time, processId, file, flags) ⇒ {
        if (flags.contains("WRITE")) {
          val dateTime = createTime(month, day, time)
          val user = userProcess.get(processId).getOrElse("")
          Some(new SftpAction(actionType = SftpActionTypes.Upload, time = dateTime, user = user, detail = file))
        } else {
          None
        }
      }
      case deleteReg(month, day, time, processId, file) ⇒ {
        val dateTime = createTime(month, day, time)
        val user = userProcess.get(processId).getOrElse("")
        Some(new SftpAction(actionType = SftpActionTypes.Delete, time = dateTime, user = user, detail = file))
      }
      case _ ⇒ None
    }
  }

  private def createTime(month: String, day: String, time: String): Date = {
    val year = Calendar.getInstance().get(Calendar.YEAR)
    val timeFormat = new SimpleDateFormat("yyyy MMM dd HH:mm:ss")
    val formattedDate = "%d %s %s %s".format(year, month, day, time)
    timeFormat.parse(formattedDate)
  }
}