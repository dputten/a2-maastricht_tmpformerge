/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.auditor.checks.exportfiles

import akka.actor.Actor
import csc.sectioncontrol.messages.{ SystemEvent, StatsDuration }

case class ExportFilesCheck(day: StatsDuration, systemId: String)

abstract class SystemExportFilesAuditor extends Actor {

  def receive = {
    case msg: ExportFilesCheck ⇒ dailyMaintenance(day = msg.day, systemId = msg.systemId)
  }

  def dailyMaintenance(day: StatsDuration, systemId: String)

  def sendEvent(systemEvent: SystemEvent) {
    context.system.eventStream.publish(systemEvent)
  }

}