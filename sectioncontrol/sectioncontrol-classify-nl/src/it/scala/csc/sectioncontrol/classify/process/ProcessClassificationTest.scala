/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.classify.process

import akka.actor.{Actor, ActorSystem}
import akka.testkit.TestActorRef
import csc.hbase.utils.HBaseTableKeeper
import csc.hbase.utils.testframework.HBaseTestFramework
import csc.sectioncontrol.classify.nl.process.ProcessClassification
import csc.sectioncontrol.classify.nl.{ClassifierConfig, ClassifyResult, _}
import csc.sectioncontrol.messages._
import csc.sectioncontrol.rdwregistry.RdwData
import csc.sectioncontrol.storage.ZkClassifyConfidence
import csc.sectioncontrol.storagelayer.hbasetables.{SpeedRecordTable, VehicleClassifiedTable}
import org.apache.hadoop.hbase.util.Bytes
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

class ProcessClassificationTest extends WordSpec with MustMatchers with HBaseTestFramework {

  val tableKeeper = new HBaseTableKeeper {}

  override protected def beforeAll() {
    super.beforeAll()
    implicit def string2ByteArray(s: String): Array[Byte] = Bytes.toBytes(s)
  }

  implicit val testSystem = ActorSystem("ProcessClassificationTest")

  "Classifier actor" must {
    val checker = TestActorRef(new ResultChecker(testSystem))
    testSystem.eventStream.subscribe(checker, classOf[ClassifyResult])

    "process a message" in {

      val inputGenerator = new SpeedRecordGenerator(testUtil.getConfiguration, tableKeeper)
      inputGenerator.addSpeedRecords()
      //
      val reader = SpeedRecordTable.makeReader(testUtil.getConfiguration, tableKeeper, inputGenerator.systemId)
      val incoming = reader.readRows(inputGenerator.startTime, System.currentTimeMillis())
      incoming.size must be(2)
      val config = new ClassifierConfig(0.4f, 0.5f, List("DE", "BE", "CH"), classifyConfidence = ZkClassifyConfidence(confidenceLimit = 50))
      val rdwRegistry = new MockRdwRegistry(RdwData("KW9999", 25, 3, 2500, 3.0f, 2.0f, 1500, 150, 160, "LF"))
      val writer = VehicleClassifiedTable.makeWriter(testUtil.getConfiguration, tableKeeper)
      val classifierRef = TestActorRef(new ProcessClassification(rdwRegistry, config, reader, writer, new MockClassifyDecisionWriter()))
      val statDuration = StatsDuration.local("20120101")
      val classifyMessage = ClassifyMessage(statDuration, inputGenerator.startTime, System.currentTimeMillis())
      classifierRef ! classifyMessage
      val result = checker.underlyingActor.result.get
      result must be(ClassifyResult(classifyMessage, 2))
      //check
      val rereader = VehicleClassifiedTable.makeReader(testUtil.getConfiguration, tableKeeper, inputGenerator.systemId)
      val result2 = rereader.readRows(0L, System.currentTimeMillis())
      result2.size must be(2)
      val first :: second :: Nil = result2.toList
      first.indicator must be(ProcessingIndicator.Automatic)
      second.indicator must be(ProcessingIndicator.Manual)
      second.code.get must be(VehicleCode.AS)
    }
  }

  override protected def afterAll() {
    tableKeeper.closeTables()
    testSystem.shutdown()
    super.afterAll()
  }
}

private class ResultChecker(system: ActorSystem) extends Actor {
  var r: Option[ClassifyResult] = None

  def result = r

  def receive = {
    case res: ClassifyResult ⇒
      r = Some(res)

    case _ ⇒
      throw new RuntimeException()
  }
}

