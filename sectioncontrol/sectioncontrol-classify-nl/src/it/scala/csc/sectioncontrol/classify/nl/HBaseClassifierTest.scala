/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.classify.nl

import csc.hbase.utils.HBaseTableKeeper
import csc.hbase.utils.testframework.HBaseTestFramework
import csc.sectioncontrol.rdwregistry.RdwData
import csc.sectioncontrol.storagelayer.hbasetables.{SpeedRecordTable, VehicleClassifiedTable}
import org.apache.hadoop.hbase.util.Bytes
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

class HBaseClassifierTest extends WordSpec with MustMatchers with HBaseTestFramework {

  val tableKeeper = new HBaseTableKeeper {}

  override protected
  def beforeAll() {
    super.beforeAll()
    implicit def string2ByteArray(s: String): Array[Byte] = Bytes.toBytes(s)
  }

  "Classifier" must {

    "properly serialize and deserialize" in {
      val inputGenerator = new SpeedRecordGenerator(testUtil.getConfiguration, tableKeeper)
      inputGenerator.addSpeedRecords()

      val reader = SpeedRecordTable.makeReader(testUtil.getConfiguration, tableKeeper, inputGenerator.systemId)
      val incoming = reader.readRows(0, System.currentTimeMillis())
      incoming.size must be(2)
      val config = new ClassifierConfig(0.4f, 0.5f, List("DE", "BE", "CH"))
      val rdwRegistry = new MockRdwRegistry(RdwData("KW9999", 25, 3, 2500, 3.0f, 2.0f, 1500, 150, 160, "LF"))
      val classifier: SpeedRecordClassifier = new RdwMatchClassifier(rdwRegistry, config, new MockClassifyDecisionWriter())
      val classifiedList = incoming.map(vehicle ⇒ classifier.classify(vehicle)).toList
      //write
      val listWithKeys = classifiedList.map(record ⇒ (record, record))
      val writer = VehicleClassifiedTable.makeWriter(testUtil.getConfiguration, tableKeeper)
      writer.writeRows(listWithKeys)
      //check
      val rereader = VehicleClassifiedTable.makeReader(testUtil.getConfiguration, tableKeeper, inputGenerator.systemId)
      val result = rereader.readRows(0L, System.currentTimeMillis())
      result.size must be(2)
    }
  }

  override protected def afterAll() {
    try {
      tableKeeper.closeTables()
    } finally {
      super.afterAll()
    }
  }
}

