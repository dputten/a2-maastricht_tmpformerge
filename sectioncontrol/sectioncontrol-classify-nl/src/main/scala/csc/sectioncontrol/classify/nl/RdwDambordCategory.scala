package csc.sectioncontrol.classify.nl

/**
 * All the RDW categories found both in the specification and in the provided MS Excel file
 */
object RdwDambordCategory {
  sealed trait Category { def name: String; def info: String; def soortKentekens: List[KentekenReeks] }
  case object MIL extends Category { val name = "MIL"; val info = "Military"; val soortKentekens = List(KentekenReeks("16", Nil), KentekenReeks("20", List("0"))) }
  case object M extends Category { val name = "M"; val info = "Motor KB"; val soortKentekens = List(KentekenReeks("3", List("1", "2"))) }
  case object P extends Category { val name = "P"; val info = "Personenauto KB"; val soortKentekens = List(KentekenReeks("1", List("1", "2", "5"))) }
  case object LB extends Category { val name = "LB"; val info = "Lichte bedrijfsauto"; val soortKentekens = List(KentekenReeks("2", List("3"))) }
  case object ZB extends Category { val name = "ZB"; val info = "Zware bedrijfsauto"; val soortKentekens = List(KentekenReeks("2", List("4"))) }
  case object B extends Category { val name = "B"; val info = "Bedrijfsauto KB"; val soortKentekens = List(KentekenReeks("2", List("1", "2"))) }
  case object A extends Category { val name = "A"; val info = "Aanhangwagen KB"; val soortKentekens = List(KentekenReeks("4", List("1"))) }
  case object O extends Category { val name = "O"; val info = "Oplegger KB"; val soortKentekens = List(KentekenReeks("5", List("1"))) }
  case object C extends Category { val name = "C"; val info = "Bromfiets"; val soortKentekens = List(KentekenReeks("17", List("1"))) }

  case object ONG extends Category { val name = "ONG"; val info = "Ongeldig"; val soortKentekens = List(KentekenReeks("9", List("1")), KentekenReeks("14", Nil), KentekenReeks("15", List("1"))) }
  case object PD extends Category { val name = "PD"; val info = "Personenauto KB; < 1973"; val soortKentekens = Nil } // werd al niet gebruikt
  case object BD extends Category { val name = "BD"; val info = "Bedrijfsauto KB; < 1973"; val soortKentekens = Nil } // werd al niet gebruikt
  case object BKA extends Category { val name = "BKA"; val info = "Handelaars KB Ahw/Opl"; val soortKentekens = List(KentekenReeks("7", List("1"))) }
  case object MD extends Category { val name = "MD"; val info = "Motor KB; < 1973"; val soortKentekens = Nil } // werd al niet gebruikt
  case object BK extends Category { val name = "BK"; val info = "Bijz. KB Buitenlanders"; val soortKentekens = List(KentekenReeks("10", List("1"))) }
  case object BKH extends Category { val name = "BKH"; val info = "Handelaars KB P, B en M"; val soortKentekens = List(KentekenReeks("6", List("1")), KentekenReeks("19", List("1"))) }
  case object BKC extends Category { val name = "BKC"; val info = "Bijz. kent corps diplomatiek"; val soortKentekens = List(KentekenReeks("11", List("0"))) }
  case object BKG extends Category { val name = "BKG"; val info = "Bijz. KB Grensverkeer"; val soortKentekens = List(KentekenReeks("12", List("1"))) }
  case object BKK extends Category { val name = "BKK"; val info = "Bijz. KB Koninklijk Huis"; val soortKentekens = List(KentekenReeks("13", List("1"))) }
  case object BRH extends Category { val name = "BRH"; val info = "Kenteken erkend bedrijf (handelaren)"; val soortKentekens = Nil } // werd al WEL gebruikt
  case object CD extends Category { val name = "CD"; val info = "Hoge militairen/bijzondere diplomatiek vertegenwoordiging"; val soortKentekens = Nil } // werd al niet gebruikt
  case object CDJ extends Category { val name = "CDJ"; val info = "Internationaal gerechtshof"; val soortKentekens = Nil } // werd al niet gebruikt
  case object PE extends Category { val name = "PE"; val info = "Personenauto 73 tm 77"; val soortKentekens = Nil } // werd al niet gebruikt
  case object Z extends Category { val name = "Z"; val info = "Vrtg niet conform eisen WvW"; val soortKentekens = Nil } // werd al niet gebruikt
  case object ZZ extends Category { val name = "ZZ"; val info = "Vrtg niet conform eisen WvW"; val soortKentekens = List(KentekenReeks("8", List("1"))) }

  private val allCategories: List[Category] = List(MIL, M, P, LB, ZB, B, A, O, C, ONG, BKA, BK, BKH, BKC, BKG, BKK, ZZ)

  case class KentekenReeks(soortKentekenSerie: String, onderverdelingSoortKentekenSerie: List[String])

  case class Other(name: String) extends Category {
    require(name != null)
    require(!name.trim().isEmpty)
    val info = "Unknown"
    val soortKentekens = Nil
  }

  def parse(soortKentekenSerie: String, onderverdelingSoortKentekenSerie: String): Category = {
    val result = allCategories.
      filter(category ⇒ {
        category.soortKentekens.exists(kentekenReeks ⇒ kentekenReeks.soortKentekenSerie == soortKentekenSerie &&
          kentekenReeks.onderverdelingSoortKentekenSerie.contains(onderverdelingSoortKentekenSerie))
      })

    result.length match {
      case 0 ⇒ ONG // Something came in but we do not know it so it must be 'ONG'ELDIG
      case 1 ⇒ result.head
      case _ ⇒ throw new Exception("Multiple result found. Should not be possible.")
    }
  }

  def parse(str: String): Category = str match {
    case "M"   ⇒ M
    case "P"   ⇒ P
    case "LB"  ⇒ LB
    case "ZB"  ⇒ ZB
    case "B"   ⇒ B
    case "A"   ⇒ A
    case "O"   ⇒ O
    case "C"   ⇒ C

    case "ONG" ⇒ ONG
    case "PD"  ⇒ PD
    case "BD"  ⇒ BD
    case "BKA" ⇒ BKA
    case "MD"  ⇒ MD
    case "BK"  ⇒ BK
    case "BKH" ⇒ BKH
    case "BKC" ⇒ BKC
    case "BKG" ⇒ BKG
    case "BKK" ⇒ BKK
    case "BRH" ⇒ BRH
    case "CD"  ⇒ CD
    case "CDJ" ⇒ CDJ
    case "PE"  ⇒ PE
    case "Z"   ⇒ Z
    case "ZZ"  ⇒ ZZ
    case _     ⇒ Other(str)
  }
}
