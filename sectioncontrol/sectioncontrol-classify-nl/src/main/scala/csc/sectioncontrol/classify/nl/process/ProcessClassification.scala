/*
* Copyright (C) 2012 CSC. <http://www.csc.com>
*/
package csc.sectioncontrol.classify.nl.process

import akka.actor.{ ActorLogging, Actor }
import csc.sectioncontrol.rdwregistry.RdwRegistry
import csc.sectioncontrol.storagelayer.hbasetables.{ SpeedRecordTable, VehicleClassifiedTable, DecisionResultTable }
import csc.sectioncontrol.classify.nl.{ ClassifyResult, ClassifyMessage, RdwMatchClassifier, ClassifierConfig }
import csc.sectioncontrol.classify.nl.preprocess.PreProcessTDC3Category

class ProcessClassification(rdwRegistry: RdwRegistry, config: ClassifierConfig,
                            speedReader: SpeedRecordTable.Reader,
                            resultWriter: VehicleClassifiedTable.Writer,
                            decisionWriter: DecisionResultTable.Writer) extends Actor with ActorLogging {
  val classifier = new RdwMatchClassifier(rdwRegistry, config, decisionWriter)

  val preprocessor = new PreProcessTDC3Category(rdwRegistry, config, log)

  def receive = {
    case message @ ClassifyMessage(_, begin, end) ⇒ {
      //pre-processing needs a margin because it needs the registration before and after to
      // be able to correct the TDC3 category
      val marginStart = begin - preprocessor.getStartMargin()
      val marginEnd = end + preprocessor.getEndMargin()

      val incomingData = speedReader.readRows(marginStart, marginEnd)
      val preProcessedData = preprocessor.preProcess(incomingData)
      //remove the extra records before processing the data
      val correctedPeriod = preProcessedData.filter(rec ⇒ rec.time >= begin && rec.time <= end)
      //do the actual classify
      val outcome = correctedPeriod.map(record ⇒ classifier.classify(record))

      //TODO get a handle to the ClassifiedRecordBatchProcessorActor and send 'outcome' wrapped in a ValidationBatchMessage
      //TODO then remove the writeRows line

      //write all the results
      resultWriter.writeRows(outcome, record ⇒ (record, Some(record.time)))
      //reply that we are done
      val result = ClassifyResult(message, outcome.size)
      context.system.eventStream.publish(result)
      sender ! result
    }
  }
}

