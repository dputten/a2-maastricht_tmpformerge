/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.sectioncontrol.classify.nl.amqp

import akka.actor.{ ActorRef, ActorLogging, Actor }
import csc.amqp.AmqpSerializers
import com.github.sstone.amqp.Amqp.{ Ok, Ack, Delivery }
import csc.sectioncontrol.messages.VehicleSpeedRecord
import csc.sectioncontrol.storagelayer.hbasetables.SerializerFormats

/**
 * Actor for handling incoming messages. Types of messages which are handled:
 * - [[csc.sectioncontrol.messages.VehicleSpeedRecord]]
 *
 */
class ClassifyConsumer(classifierActor: ActorRef) extends Actor
  with ActorLogging with AmqpSerializers {

  override implicit val formats = SerializerFormats.formats
  override val ApplicationId = "Classifier"

  object VehicleSpeedRecordExtractor extends MessageExtractor[VehicleSpeedRecord]

  override protected def receive: Receive = {
    case delivery @ VehicleSpeedRecordExtractor(message) ⇒ {
      classifierActor ! message
      sender ! Ack(delivery.envelope.getDeliveryTag)
    }
    case delivery: Delivery ⇒ {
      log.error("Rejecting message with deliveryTag {} - cannot determine message type", delivery.envelope.getDeliveryTag)
      sender ! Ack(delivery.envelope.getDeliveryTag)
    }
    case ok: Ok ⇒ log.debug("Ok from AMQP")
    case unhandled: AnyRef ⇒ {
      log.warning("Unhandled message: {}", unhandled)
    }
  }

}

