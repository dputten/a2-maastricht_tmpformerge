/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.classify.nl.trace

import csc.sectioncontrol.messages.{ VehicleSpeedRecord, IndicatorReason, ProcessingIndicator, VehicleCode }
import csc.sectioncontrol.classify.nl.{ DecisionResult, Decision, Classification }
import csc.akkautils.DirectLogging

class DecisionTree extends DirectLogging {
  var decisions = Seq[Decision]()

  def addDecision(step: String, result: String, details: Seq[Decision] = Seq()) {
    decisions = decisions :+ new Decision(step, result, details)
  }

  def addDecision(decision: Decision) {
    decisions = decisions :+ decision
  }

  def addDecisionWithLog(step: String, result: String, details: Seq[Decision] = Seq()) {
    decisions = decisions :+ new Decision(step, result, details)

    val detailsStr = details.map(d ⇒ "%s: %s".format(d.step, d.result)).mkString("|")
    log.info("%s: %s details: %s".format(step, result, detailsStr))
  }

  def addDecisionWithLog(decision: Decision) {
    decisions = decisions :+ decision

    val details = decision.details.map(d ⇒ "%s: %s".format(d.step, d.result)).mkString("|")
    log.info("%s: %s details: %s".format(decision.step, decision.result, details))
  }

  def createResult(classification: Classification, record: VehicleSpeedRecord): DecisionResult = {
    val license = record.exit.getOrElse(record.entry).license.map(_.value).getOrElse("NONE")
    val laneId = record.exit.getOrElse(record.entry).lane.laneId
    new DecisionResult(steps = decisions,
      license = license,
      laneId = laneId,
      code = classification.code,
      indicator = classification.indicator,
      reason = classification.reason,
      mil = classification.mil,
      manualAccordingToSpec = classification.manualAccordingToSpec,
      alternativeClassification = classification.alternativeClassification)
  }

}