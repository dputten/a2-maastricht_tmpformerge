/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.classify.nl.config

import csc.sectioncontrol.classify.nl.ClassifierConfig
import csc.curator.utils.Curator
import akka.util.Duration

class ConfigurationCache(curator: Curator, dirtyDuration: Duration) {
  case class cacheRecord(systemId: String, time: Long, config: ClassifierConfig)
  var cache = Map[String, cacheRecord]()

  def clear() {
    cache = Map[String, cacheRecord]()
  }

  def getConfiguration(systemId: String): Option[ClassifierConfig] = {
    cleanup(System.currentTimeMillis())
    val record = cache.get(systemId).map(_.config)
    record.orElse(ClassifierConfigBuilder.createConfig(curator, systemId))
  }

  def cleanup(now: Long) {
    cache = cache.filter {
      case (systemId, record) ⇒ {
        record.time + dirtyDuration.toMillis > now
      }
    }
  }
}