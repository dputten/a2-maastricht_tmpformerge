/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.classify.nl.amqp

import akka.actor.{ Props, ActorRef, ActorLogging, Actor }
import com.github.sstone.amqp.{ Amqp, RabbitMQConnection }
import csc.sectioncontrol.classify.nl.process.ProcessSingleClassification
import org.apache.hadoop.conf.Configuration
import csc.hbase.utils.{ HBaseAdminTool, HBaseTableKeeper }
import csc.sectioncontrol.storagelayer.hbasetables.{ DecisionResultTable, VehicleClassifiedTable, SpeedRecordTable }
import csc.sectioncontrol.rdwregistry.{ HBaseRdwRegistry, RdwRegistry }
import csc.curator.utils.Curator
import org.apache.hadoop.hbase.client.HTable
import csc.sectioncontrol.classify.nl.config.{ AMQPClassifyConfig, ConfigurationCache }
import akka.util.Duration
import com.github.sstone.amqp.Amqp.ChannelParameters

class AMQPClassifierIface(curator: Curator, mqConnection: RabbitMQConnection, hbaseConfig: Configuration, config: AMQPClassifyConfig) extends Actor with ActorLogging {
  val tableKeeper = new HBaseTableKeeper() {}

  log.info("AMQPClassifierIface hbaseConfig: {}", hbaseConfig)
  log.info("AMQPClassifierIface config: {}", config)

  override def preStart() {
    super.preStart()
    val prod = mqConnection.createChannelOwner()
    log.debug("AMQPClassifierIface wait for channel")
    Amqp.waitForConnection(context.system, prod).await()
    val producer = context.actorOf(Props(new ClassifyProducer(prod, config.producerEndpoint)))

    val classifiedRecordWriter = VehicleClassifiedTable.makeWriter(hbaseConfig, tableKeeper)
    val registry: RdwRegistry = makeHBaseRdwRegistry()
    val classifiedDecision = DecisionResultTable.makeWriter(hbaseConfig, tableKeeper)
    val cache = new ConfigurationCache(curator, config.dirtyDuration)

    val classifier = context.actorOf(Props(new ProcessSingleClassification(
      configCache = cache,
      rdwRegistry = registry,
      resultWriter = classifiedRecordWriter,
      decisionWriter = classifiedDecision,
      producer = producer)))

    val classifyConsumer = context.actorOf(Props(new ClassifyConsumer(classifier)))

    val consumer = mqConnection.createSimpleConsumer(config.consumerQueue, classifyConsumer, Some(ChannelParameters(5)), false)
    log.debug("AMQPClassifierIface wait for queue consumer")
    Amqp.waitForConnection(context.system, consumer).await()
    log.info("AMQPClassifierIface is started")
  }

  private def makeHBaseRdwRegistry(): RdwRegistry = {
    HBaseAdminTool.ensureTableExists(hbaseConfig, HBaseRdwRegistry.TABLE, HBaseRdwRegistry.FAMILY)
    val table = new HTable(hbaseConfig, HBaseRdwRegistry.TABLE)
    tableKeeper.addTable(table)
    new HBaseRdwRegistry(table)
  }

  override def postStop() {
    tableKeeper.closeTables()
    super.postStop()
    log.info("AMQPClassifierIface is stopped")
  }

  override def preRestart(reason: Throwable, message: Option[Any]) {
    log.error(reason, "AMPQClassifierIface restated while processing %s".format(message))
    super.preRestart(reason, message)
  }

  def receive: Receive = {
    case msg: AnyRef ⇒ {
      log.warning("Received an unexpected message with type: " + msg.getClass.getName)
    }
  }

}