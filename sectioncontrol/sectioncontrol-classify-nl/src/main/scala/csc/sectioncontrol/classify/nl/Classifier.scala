/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.classify.nl

import csc.sectioncontrol.messages._
import csc.sectioncontrol.storage.{ ZkClassifyConfidenceIntrada, ZkCorrectTDC3CategoryConfig, ZkClassifyConfidence, ZkWrongReadDutchLicensePlate }

/**
 * Classify a vehicle according to the Dutch classification rules (Voertuigclassificatie)
 */
trait SpeedRecordClassifier {
  /**
   * Classify a vehicle according to the Dutch classification rules (Voertuigclassificatie)
   * {{{
   * Voor alle kentekens:
   * • voertuigklasse bepalen via gemeten voertuigkenmerken (zoals
   * voertuiglengte);
   * Voor Nederlandse kentekens:
   * • voertuigklasse bepalen via de syntax van het kenteken, waarbij gebruik
   * gemaakt wordt van het overzicht uitgereikte kentekenseries (‘RDW
   * dambord’);
   * • voertuigklasse bepalen via gegevens uit het RDW-kentekenregister
   * volgens het stroomschema in Bijlage A.
   * }}}
   */
  def classify(passage: VehicleSpeedRecord): VehicleClassifiedRecord
}

/**
 * Int is the upper limit, Float is the factor to be multiplied with the wheel base value. If length is larger then
 * the biggest value then the last value will be used.
 *
 * Req 41: (source: e-mail on 27-03-2012)
 * de constante is een factor waarmee de wielbasis vermeningvuldigd moet worden om tot de voertuig lengte te komen.
 * Bij een kleine auto zal de factor dicht tegen de 1 liggen omdat de voertuiglengte niet veel afwijkt van
 * de wielbasis (bijv een Smart). Bij langere voertuigen (grotere wielbasis) zal de factor enerzijds verder
 * van de 1 afliggen omdat de voertuiglengte absoluut gezien meer afwijking heeft tov de wielbasis,
 * anderzijds wordt deze weer verminderd doordat de procentuele afwijking wat minder zal zijn.
 */
case class WheelBaseInfo(p: Seq[(Int, Float)] = List((2, 1.2f), (3, 1.15f), (4, 1.1f), (5, 1.05f)),
                         lb: Seq[(Int, Float)] = List((2, 1.2f), (3, 1.15f), (4, 1.1f), (5, 1.05f)))

//Preferences -> ClassifierConfig conversion is done in Boot
case class ClassifierConfig(
  margin1: Float, //marge1 = marge onnauwkeurigheid lengtebepaling
  margin2: Float, //marge2 = minimum lengte aanhanger
  mobiCountries: List[String] = List("BE", "DE", "CH"),
  countryCodeConfidence: Int = 50, // below this threshold the country is considered to be undefined
  wheelBaseInfo: WheelBaseInfo = WheelBaseInfo(),
  classifyConfidence: ZkClassifyConfidence = ZkClassifyConfidence(),
  similarLicenseMobiCountries: List[String] = List("FI", "SE", "LT", "HU"), // list of license plates which are similar to the plates of the mobi countries
  correctTDC3: ZkCorrectTDC3CategoryConfig = ZkCorrectTDC3CategoryConfig(),
  wrongReadLicense: List[ZkWrongReadDutchLicensePlate] = List(), // List with Dutch plates which are read wrong all the time
  classifyConfidenceIntrada: Option[ZkClassifyConfidenceIntrada] = Some(ZkClassifyConfidenceIntrada()))

