/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.classify.nl.preprocess

import csc.sectioncontrol.rdwregistry.{ RdwData, RdwRegistry }
import csc.sectioncontrol.messages.{ ValueWithConfidence, VehicleCategory, VehicleSpeedRecord }
import akka.event.LoggingAdapter
import csc.sectioncontrol.classify.nl.{ RdwDambordCategory, LicensePlateNL, ClassifierConfig }

/**
 * Correct the Category of the TDC3.
 * Rule:
 * when TDC3 = CarTrailer, SemiTrailer, TruckTrailer
 * and land = NL
 * and 	dambord = P of LB
 * and 	RDW = OK
 * and 	TDC3 first passage = (empty)
 * and 	gap time < 1,03 seconds
 * and 	license <> license previous passage
 * than TDC3 Category of both registrations become Car
 *
 * @param rdwRegistry
 * @param config
 */
class PreProcessTDC3Category(rdwRegistry: RdwRegistry, config: ClassifierConfig, log: LoggingAdapter) {
  import PreProcessTDC3Category._
  val maxGapTime = config.correctTDC3.maxGapTime
  val categoryConfidence = config.correctTDC3.categoryConfidence

  def getStartMargin(): Long = {
    maxGapTime
  }
  def getEndMargin(): Long = {
    maxGapTime
  }

  /**
   * Do the pre-processing for all the speed records
   * @param speedRecords
   * @return
   */
  def preProcess(speedRecords: Seq[VehicleSpeedRecord]): Seq[VehicleSpeedRecord] = {
    if (!config.correctTDC3.enable) {
      return speedRecords
    }
    val exitCheck = new CheckExitCategory()
    val exitCorrected = preProcessGantry(exitCheck, speedRecords)

    val entryCheck = new CheckEntryCategory()
    preProcessGantry(entryCheck, exitCorrected)
  }

  def preProcessGantry(check: CheckGantryCategory, speedRecords: Seq[VehicleSpeedRecord]): Seq[VehicleSpeedRecord] = {
    val lanes = check.groupListBy(speedRecords)
    val processed = lanes.flatMap {
      case (laneId, speedRecords) ⇒ {
        if (laneId == UNKNOWN) {
          speedRecords
        } else {
          //Do pre-process
          preProcessLane(check, speedRecords)
        }
      }
    }
    processed.toSeq
  }

  /**
   * Process the lanes
   * @param speedRecords
   * @return
   */
  def preProcessLane(check: CheckGantryCategory, speedRecords: Seq[VehicleSpeedRecord]): Seq[VehicleSpeedRecord] = {
    //sort list
    val sortedRecords = speedRecords.sortBy(check.getTime(_))
    //look at pairs of records

    if (sortedRecords.size <= 1) {
      //there is no pair => no correction
      sortedRecords
    } else {
      val first = sortedRecords.head
      val (last, processed) = sortedRecords.tail.foldLeft(first, Seq[VehicleSpeedRecord]()) {
        case ((previous, list), current) ⇒ {
          if (doesRuleApply(check, previous, current)) {
            //split the None - trailer into car - car
            log.info("Corrected %s Category to Car for registration %s and %s".format(check.categoryType, previous, current))
            (check.updateCategoryToCar(current, categoryConfidence), list :+ check.updateCategoryToCar(previous, categoryConfidence))
          } else {
            (current, list :+ previous)
          }
        }
      }
      processed :+ last
    }
  }

  /**
   * check
   * when TDC3 = CarTrailer, SemiTrailer, TruckTrailer
   * and land = NL
   * and 	dambord = P of LB
   * and 	RDW = OK
   * and 	TDC3 first passage = (empty)
   * and 	gap time < 1,03 seconds
   * and 	license <> license previous passage
   * @param firstRecord
   * @param secondRecord
   * @return true when rule apply otherwise false
   */
  def doesRuleApply(check: CheckGantryCategory, firstRecord: VehicleSpeedRecord, secondRecord: VehicleSpeedRecord): Boolean = {
    var ruleApply = false

    val categoryFirst = check.getCategory(firstRecord)
    val licenseFirst = firstRecord.entry.license.map(_.value)

    if (check.isVehicleMetaDataDefined(firstRecord) && categoryFirst.isEmpty && licenseFirst.isDefined) {
      //check the next record
      //check gap
      val gap = check.getTime(secondRecord) - check.getTime(firstRecord)
      if (gap >= 0 && gap < maxGapTime) {
        //check TDC3
        val categorySecond = check.getCategory(secondRecord).map(_.value)
        val category = categorySecond.getOrElse(UNKNOWN)
        if (categorySecond.isDefined &&
          (category == VehicleCategory.CarTrailer.toString ||
            category == VehicleCategory.SemiTrailer.toString ||
            category == VehicleCategory.TruckTrailer.toString)) {
          //check license
          val licenseSecond = secondRecord.entry.license.map(_.value)
          if (licenseSecond.isDefined && licenseFirst.isDefined &&
            licenseSecond.getOrElse(UNKNOWN) != licenseFirst.getOrElse(UNKNOWN)) {
            // check land = NL
            val countryExit = secondRecord.exit.flatMap(_.country).map(_.value)
            val country = secondRecord.entry.country.map(_.value).orElse(countryExit)
            if (country.exists(_ == "NL")) {
              // check dambord = P of LB
              val license = licenseSecond.getOrElse(UNKNOWN)
              val licenseNL = LicensePlateNL(license)
              val plateCat = licenseNL.rdwCategory

              if (plateCat.isDefined && (plateCat.exists(_ == RdwDambordCategory.P) || plateCat.exists(_ == RdwDambordCategory.LB))) {
                // check RDW = OK
                if (getRDWData(license).isDefined) {
                  //all check are done and passed.
                  ruleApply = true
                }
              }
            }
          }
        }
      }
    }
    ruleApply
  }

  /**
   * Get the RDWData
   * @param licensePlate
   * @return
   */
  private def getRDWData(licensePlate: String): Option[RdwData] = {
    rdwRegistry.getData(licensePlate) match {
      case Some(data) ⇒ {
        //is data empty
        if (data.deviceCode == 0 ||
          data.maxMass == 0 ||
          data.eegCategory == "00") {
          None
        } else {
          Some(data)
        }
      }
      case None ⇒ None
    }
  }

}

object PreProcessTDC3Category {
  val UNKNOWN = "unknown"
}

trait CheckGantryCategory {
  def categoryType: String
  def groupListBy(speedRecords: Seq[VehicleSpeedRecord]): Map[String, Seq[VehicleSpeedRecord]]
  def getTime(speedRecord: VehicleSpeedRecord): Long
  def updateCategoryToCar(speedRecord: VehicleSpeedRecord, categoryConfidence: Int): VehicleSpeedRecord
  def getCategory(speedRecord: VehicleSpeedRecord): Option[ValueWithConfidence[String]]
  def isVehicleMetaDataDefined(speedRecord: VehicleSpeedRecord): Boolean
}

class CheckExitCategory extends CheckGantryCategory {
  val categoryType = "Exit"
  def groupListBy(speedRecords: Seq[VehicleSpeedRecord]): Map[String, Seq[VehicleSpeedRecord]] = {
    speedRecords.groupBy(_.exit.map(_.lane.laneId).getOrElse(PreProcessTDC3Category.UNKNOWN))
  }
  def sortByTime(speedRecords: Seq[VehicleSpeedRecord]): Seq[VehicleSpeedRecord] = {
    speedRecords.sortBy(_.time)
  }
  def getTime(speedRecord: VehicleSpeedRecord): Long = {
    speedRecord.time
  }
  def updateCategoryToCar(speedRecord: VehicleSpeedRecord, categoryConfidence: Int): VehicleSpeedRecord = {
    speedRecord.exit match {
      case Some(exit) ⇒ {
        val newExit = exit.copy(category = Some(ValueWithConfidence[String](VehicleCategory.Car.toString, categoryConfidence)))
        speedRecord.copy(exit = Some(newExit))
      }
      case None ⇒ speedRecord
    }
  }
  def getCategory(speedRecord: VehicleSpeedRecord): Option[ValueWithConfidence[String]] = {
    speedRecord.exit.flatMap(_.category)
  }
  def isVehicleMetaDataDefined(speedRecord: VehicleSpeedRecord): Boolean = {
    speedRecord.exit.isDefined
  }
}

class CheckEntryCategory extends CheckGantryCategory {
  val categoryType = "Entry"
  def groupListBy(speedRecords: Seq[VehicleSpeedRecord]): Map[String, Seq[VehicleSpeedRecord]] = {
    speedRecords.groupBy(_.entry.lane.laneId)
  }
  def sortByTime(speedRecords: Seq[VehicleSpeedRecord]): Seq[VehicleSpeedRecord] = {
    speedRecords.sortBy(_.entry.eventTimestamp)
  }
  def getTime(speedRecord: VehicleSpeedRecord): Long = {
    speedRecord.entry.eventTimestamp
  }

  def updateCategoryToCar(speedRecord: VehicleSpeedRecord, categoryConfidence: Int): VehicleSpeedRecord = {
    val newEntry = speedRecord.entry.copy(category = Some(ValueWithConfidence[String](VehicleCategory.Car.toString, categoryConfidence)))
    speedRecord.copy(entry = newEntry)
  }
  def getCategory(speedRecord: VehicleSpeedRecord): Option[ValueWithConfidence[String]] = {
    speedRecord.entry.category
  }
  def isVehicleMetaDataDefined(speedRecord: VehicleSpeedRecord): Boolean = {
    true
  }
}

