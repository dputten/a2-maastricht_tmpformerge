package csc.sectioncontrol.classify.nl
import java.io.InputStream

/**
 * Provides kentekenseries ('RDW dambord') for one dambord table
 */
case class Dambord(data: Map[Char, Map[Char, RdwDambordCategory.Category]]) {
  /**
   * Returns a RDW category
   * @param chars - first char is the row, second char is the column
   * @return kentekenserie from the dambord or None if not found
   */
  def getCategory(chars: String): Option[RdwDambordCategory.Category] = {
    require(chars.length == 2)
    data.get(chars(0)) match {
      case None      ⇒ None
      case Some(row) ⇒ row.get(chars(1))
    }
  }
}

/**
 * Parse dambord definition from CSV file
 */
object DamBordParser {

  /**
   * Parse one line from the CSV file
   */
  def parseLine(line: String, column: List[Char]): (Char, Map[Char, RdwDambordCategory.Category]) = {
    val firstLetter :: data = line.split(";").toList
    val categories = data.map(_.trim).filterNot(_.isEmpty()).map(RdwDambordCategory.parse(_))
    if (categories.size != column.size) throw new RuntimeException("Invalid data: %s, for header:".format(categories, column))
    val zipped = column zip categories
    (firstLetter(0), zipped.toMap)
  }

  /**
   * Parse the whole input as one table
   * @param input - Dambord definition
   * @return Dambord table
   */
  def parse(input: InputStream): Dambord = {
    val source = scala.io.Source.fromInputStream(input)
    //drop first line with the comment
    val _ :: secondLetterHeader :: lines = source.mkString.split("\n").toList
    //drop first ';' separator, drop the empty values at the end, make chars
    val columnHeader: List[Char] = secondLetterHeader.split(";").toList.tail.map(_.trim()).takeWhile(!_.isEmpty).map(_(0))
    val valueLines = lines.filter(_.head.isLetter).map(_.trim) // filter out lines which start with ';'
    val rows = valueLines.map(parseLine(_, columnHeader))
    Dambord(rows.toMap)
  }
}

