/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.classify.nl.category

import csc.sectioncontrol.messages.VehicleSpeedRecord
import csc.sectioncontrol.storage.ZkClassifyConfidence
import csc.sectioncontrol.classify.nl.Decision

object ProcessJoinedExit extends ProcessCategory {

  def getScannedCategory(config: ZkClassifyConfidence, record: VehicleSpeedRecord): CategoryResult = {
    implicit var decisions = Seq[Decision]()

    val length = ProcessExitTDC3Length.getScannedCategory(config, record)
    decisions = addDecision("ScannedCategory", "EntryLength", length.decisions)

    //both checks need to be done
    val exitCat = ProcessExitTDC3.getScannedCategory(config, record)
    decisions = addDecision("ScannedCategory", "ExitConfidence", exitCat.decisions)
    //merge both results into one => result is reliable when both results are reliable
    //None only when both are None
    val reliable = (length.isReliable, exitCat.isReliable) match {
      case (Some(true), Some(true)) ⇒ Some(true)
      case (Some(false), _)         ⇒ Some(false)
      case (_, Some(false))         ⇒ Some(false)
      case other                    ⇒ None
    }
    decisions = addDecision("ScannedCategory", "reliableLength=%s reliableConfidence=%s".format(length.isReliable.toString, exitCat.isReliable.toString))
    CategoryResult(length.category, reliable, decisions)
  }
}