/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.classify.nl.amqp

import akka.actor.ActorRef
import csc.amqp.PublisherActor
import csc.sectioncontrol.messages.VehicleClassifiedRecord
import csc.sectioncontrol.storagelayer.hbasetables.SerializerFormats

class ClassifyProducer(val producer: ActorRef, val producerEndpoint: String) extends PublisherActor {
  val ApplicationId = "calibration"
  implicit val formats = SerializerFormats.formats

  def producerRouteKey(msg: AnyRef): Option[String] = {
    msg match {
      case msg: VehicleClassifiedRecord ⇒ {
        Some("%s.%d.VehicleClassifiedRecord".format(msg.speedRecord.entry.lane.system, msg.speedRecord.corridorId))
      }
      case other ⇒ Some(msg.getClass.getName)
    }
  }

}