/*
* Copyright (C) 2012 CSC. <http://www.csc.com>
*/
package csc.sectioncontrol.classify.nl.process

import akka.actor.{ ActorRef, ActorLogging, Actor }
import csc.sectioncontrol.rdwregistry.RdwRegistry
import csc.sectioncontrol.storagelayer.hbasetables.{ VehicleClassifiedTable, DecisionResultTable }
import csc.sectioncontrol.classify.nl.RdwMatchClassifier
import csc.sectioncontrol.messages.VehicleSpeedRecord
import csc.sectioncontrol.classify.nl.config.ConfigurationCache
import csc.sectioncontrol.{ ErrorRecord, Source, ProcessError }

class ProcessSingleClassification(configCache: ConfigurationCache,
                                  rdwRegistry: RdwRegistry,
                                  resultWriter: VehicleClassifiedTable.Writer,
                                  decisionWriter: DecisionResultTable.Writer,
                                  producer: ActorRef) extends Actor with ActorLogging {

  def receive = {
    case record: VehicleSpeedRecord ⇒ {
      //do the actual classify
      configCache.getConfiguration(record.entry.lane.system) match {
        case Some(config) ⇒ {
          val classifier = new RdwMatchClassifier(rdwRegistry, config, decisionWriter)
          val outcome = classifier.classify(record)
          //write the results
          resultWriter.writeRow(outcome, outcome)
          //reply that we are done
          producer ! outcome
        }
        case None ⇒ {
          log.error("Failed to get the configuration for systemid=%s".format(record.entry.lane.system))
          producer ! ProcessError(
            source = Source(record.entry.lane.system),
            errors = Seq(ErrorRecord("Retrieve-config", "failed to get configuration")),
            processMsg = record)
        }
      }

    }
  }

}

