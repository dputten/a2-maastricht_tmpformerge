/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.classify.nl

import csc.config.Path
import csc.curator.utils.{ Versioned, CuratorActor, Curator }

import csc.sectioncontrol.storage.Paths

/**
 * Manages status information on the completion of classification jobs.
 *
 * @author Maarten Hazewinkel
 */
class ManageClassificationStatus(protected val curator: Curator) extends CuratorActor {
  protected def receive = {
    case ClassificationStarted(systemId, startedAt) ⇒
      updateSystemStatus(systemId, _.copy(lastUpdateTime = startedAt))

    case ClassificationFinished(systemId, finishedAt, completedUntil) ⇒
      updateSystemStatus(systemId, _.copy(lastUpdateTime = finishedAt, completeUntil = completedUntil))

    case GetClassificationCompleteUntil(systemId) ⇒
      sender ! ClassificationIsCompleteUntil(loadStatusAndVersion(getPath(systemId))._1.completeUntil, systemId)
  }

  private[nl] def updateSystemStatus(systemId: String,
                                     statusUpdate: (ClassificationCompletionStatus ⇒ ClassificationCompletionStatus)) {
    val path = getPath(systemId)
    val (status, version) = loadStatusAndVersion(path)
    val updatedStatus = statusUpdate(status)
    // A failure in storing the new value causes the (default) supervisor strategy to restart this
    // actor, and the status update will then be lost. This is not a problem. The classifier might redo some
    // work that is already done, but should not fail in any way.
    curator.set(path, updatedStatus, version.toInt)
  }

  private[nl] def loadStatusAndVersion(path: Path): (ClassificationCompletionStatus, Long) = {
    curator.getVersioned[ClassificationCompletionStatus](path) match {
      case Some(Versioned(s, v)) ⇒ (s, v)
      case None ⇒
        val status = ClassificationCompletionStatus(0, 0)
        curator.put(path, status)
        loadStatusAndVersion(path)
    }
  }

  private def getPath(systemId: String): Path = {
    Path(Paths.Systems.getClassificationCompletionStatusPath(systemId))
  }
}

/**
 * Signal that classification has started for the indicated system
 */
case class ClassificationStarted(systemId: String, startedAt: Long)

/**
 * Signal that classification has successfully completed for the indicated system
 */
case class ClassificationFinished(systemId: String, finishedAt: Long, completedUntil: Long)

/**
 * Request the time until which classification has been completed for the system
 */
case class GetClassificationCompleteUntil(systemId: String)

/**
 * Response indicating up to which time classification has been completed for the system.
 */
case class ClassificationIsCompleteUntil(time: Long, systemId: String)
