/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.classify.nl.category

import csc.sectioncontrol.messages.{ VehicleCategory, VehicleSpeedRecord }
import csc.sectioncontrol.storage.ZkClassifyConfidence
import csc.sectioncontrol.classify.nl.trace.DecisionTree
import csc.sectioncontrol.classify.nl.Decision

object ProcessExitTDC3 extends ProcessCategory {
  /**
   * Determine the scanned vehicle category based on both passage points and the
   * calculated average speed.
   * @param record The speed record
   * @return A pair of the scanned category and an indication of the reliability of the value
   */
  def getScannedCategory(config: ZkClassifyConfidence, record: VehicleSpeedRecord): CategoryResult = {
    implicit var decisions = Seq[Decision]()
    val category = record.exit.flatMap(_.vehicleCategory)

    if (category.isEmpty || category.get.value.equals(VehicleCategory.Unknown)) {
      decisions = addDecision("ScannedCategoryExit", "Exit category=%s".format(if (category.isEmpty) "None" else "Unknown"))

      CategoryResult(VehicleCategory.Unknown, None, decisions)
    } else {
      val cat = category.get.value
      if (record.speed > 200 && !isPersonCar(cat)) {
        decisions = addDecision("ScannedCategoryExit", "SpeedCheck=Speed %d too high for category".format(record.speed))
        CategoryResult(cat, Some(false), decisions)
      } else {
        val confidence = category.get.confidence
        val limit = config.confidenceLimit
        val categoryReliable = confidence >= limit
        decisions = addDecision("ScannedCategoryExit", "ConfidenceCheck=%s confidence %d  limit %d".format(categoryReliable.toString, confidence, limit))

        val reliable = cat match {
          case VehicleCategory.Truck        ⇒ record.speed <= 130
          case VehicleCategory.Large_Truck  ⇒ record.speed <= 130
          case VehicleCategory.TruckTrailer ⇒ record.speed <= 130
          case VehicleCategory.SemiTrailer  ⇒ record.speed <= 130
          case other                        ⇒ true
        }
        decisions = addDecision("ScannedCategoryExit", "SpeedCheck=%s Speed %d  limit 130 for %s category".format(reliable.toString, record.speed, cat.toString))
        decisions = addDecision("ScannedCategoryExit", "isReliable=" + (reliable && categoryReliable).toString)
        CategoryResult(cat, Some(reliable && categoryReliable), decisions)
      }
    }
  }

}