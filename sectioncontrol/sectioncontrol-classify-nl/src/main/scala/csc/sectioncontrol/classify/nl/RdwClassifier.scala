/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.classify.nl

import category.ScannedCategory
import csc.sectioncontrol.messages._
import csc.sectioncontrol.rdwregistry.RdwRegistry
import csc.sectioncontrol.rdwregistry.RdwData
import csc.akkautils.DirectLogging
import trace.DecisionTree
import csc.sectioncontrol.messages.IndicatorReason
import csc.sectioncontrol.messages.VehicleClassifiedRecord
import csc.sectioncontrol.messages.ValueWithConfidence
import csc.sectioncontrol.messages.VehicleMetadata
import csc.sectioncontrol.messages.VehicleSpeedRecord
import csc.sectioncontrol.storagelayer.hbasetables.{ VehicleClassifiedTable, DecisionResultTable }
import csc.sectioncontrol.classify.nl.RdwDambordCategory.Category

/**
 * Result of the classification to be put to the message
 */
private[nl] case class Classification(code: Option[VehicleCode.Value],
                                      indicator: ProcessingIndicator.Value,
                                      reason: IndicatorReason,
                                      rdwSizeCategory: Option[VehicleCategory.Value] = None,
                                      mil: Boolean = false,
                                      invalidRdwData: Boolean = false,
                                      modifiedLicensePlate: Option[String] = None,
                                      manualAccordingToSpec: Boolean = false,
                                      alternativeClassification: Option[VehicleCode.Value] = None,
                                      dambord: Option[String])

private[nl] class RdwMatchClassifier(rdwRegistry: RdwRegistry, config: ClassifierConfig, decisionWriter: DecisionResultTable.Writer) extends SpeedRecordClassifier with DirectLogging {
  val maxLicensePlateLength = 12
  val rdwRequired = List(RdwDambordCategory.LB, RdwDambordCategory.ZB, RdwDambordCategory.B, RdwDambordCategory.O, RdwDambordCategory.A, RdwDambordCategory.C)
  val accordingToSpecRdwDambordCategory = List(RdwDambordCategory.BK, RdwDambordCategory.BKC, RdwDambordCategory.BKG, RdwDambordCategory.BKH, RdwDambordCategory.BRH, RdwDambordCategory.ZZ)

  def classifyRecord(record: VehicleSpeedRecord): Classification = {
    val decisions = new DecisionTree()
    val vregistration = record.exit.getOrElse(record.entry)
    val scannedCategory = new ScannedCategory(record, config.classifyConfidence)
    scannedCategory.getDecisions().foreach(dec ⇒ decisions.addDecisionWithLog(dec))

    val classification = canBeClassified(record) match {
      case Some(Right((license))) ⇒
        decisions.addDecisionWithLog("Classify Record", "Record can Be Classified license=" + license)
        try {
          //req 38, req 52
          val country: Option[ValueWithConfidence[String]] = record.country

          if (country.exists(_.value == "NL")) { //is the country NL ?
            decisions.addDecisionWithLog("Classify Record", "Country is NL")
            classifyNL(license, vregistration, scannedCategory, decisions)
          } else {
            classifyForeigner(country, record, scannedCategory, decisions)
          }
        } catch {
          case e: Exception ⇒
            decisions.addDecisionWithLog("Classify Record",
              "License %s Manual Indication - Exception [%s]".format(record.entry.license.map(_.value).getOrElse("Unknown"), e.getMessage))
            Classification(code = None, indicator = ProcessingIndicator.Manual, reason = createIndicatorReason(record, true), dambord = None)
        }
      case Some(Left(modified)) ⇒
        decisions.addDecisionWithLog("Classify Record", "Record can Be Classified modified license=" + modified)
        Classification(code = None, indicator = ProcessingIndicator.Manual, reason = IndicatorReason().setUnreliableLicense(),
          rdwSizeCategory = None, mil = false, invalidRdwData = false, modifiedLicensePlate = Some(modified), dambord = None)
      case None ⇒
        decisions.addDecisionWithLog("Classify Record", "Record can not Be Classified - License not found")
        Classification(code = None, indicator = ProcessingIndicator.Manual, reason = IndicatorReason().setUnreliableLicense(),
          rdwSizeCategory = None, mil = false, invalidRdwData = false, modifiedLicensePlate = None, dambord = None)
    }

    //final classification check
    val finalClassification = decideFinalClassification(classification, scannedCategory, record, decisions)
    val decisionResult = decisions.createResult(finalClassification, record)
    decisionWriter.writeRow(record.time.toString + record.id.toString, decisionResult)
    finalClassification
  }

  def classifyNL(license: String, vregistration: VehicleMetadata, scannedCategory: ScannedCategory, decisions: DecisionTree): Classification = {

    // Make it a Dutch plate
    val licensePlate = LicensePlateNL(license)

    // Get the RDW Dambord Category
    val category: Option[RdwDambordCategory.Category] = licensePlate.rdwCategory

    // Is it necessary to look into the RDW DB
    val requestRdwData: Boolean = category.exists(rdwRequired.contains(_))

    // Get the rdw data if necessary
    val rdwData = getRDWData(requestRdwData, licensePlate.plate)

    (rdwData, requestRdwData) match {
      case (Some(rdw), true) if rdw.hasEmptyFields ⇒
        decisions.addDecisionWithLog("Classify NL",
          "Empty fields in RDWData for %s with RDWCategory %s".format(license, category.map(_.toString).getOrElse("Unknown")))
        classifyManual(decisions, category, scannedCategory, true, true, Some(licensePlate)) //empty RDW data, but it is required, req 42
      case (None, true) ⇒
        decisions.addDecisionWithLog("Classify NL",
          "No RDWData for %s with RDWCategory %s".format(license, category.map(_.toString).getOrElse("Unknown")))
        classifyManual(decisions, category, scannedCategory, true, false, Some(licensePlate)) //no RDW data, but it is required, req 42
      case (_, false) ⇒ category match {
        case Some(RdwDambordCategory.MIL) ⇒
          decisions.addDecisionWithLog("Classify NL", "Dambord gives MIL " + license)
          classifyMil(scannedCategory, vregistration, licensePlate, decisions)
        case Some(RdwDambordCategory.M) ⇒
          decisions.addDecisionWithLog("Classify NL", "Dambord gives M (Motor) license=" + license)
          Classification(code = Some(VehicleCode.MF), indicator = ProcessingIndicator.Automatic, reason = IndicatorReason(), dambord = Some(RdwDambordCategory.M.name))
        case Some(RdwDambordCategory.P) ⇒
          decisions.addDecisionWithLog("Classify NL", "Dambord gives P (PersonCar) license=" + license)
          classifyP(scannedCategory, licensePlate, decisions)
        case Some(dam) if accordingToSpecRdwDambordCategory.contains(dam) ⇒
          classifyAccordingToSpec(dam, scannedCategory, licensePlate, decisions)
        case _ ⇒
          decisions.addDecisionWithLog("Classify NL", "Dambord - Unexpected category with no RDW data required for %s with RDWCategory %s".format(license, category.map(_.toString).getOrElse("Unknown")))
          classifyManual(decisions, category, scannedCategory, false, false, Some(licensePlate))
      }
      case (Some(rdw), _) ⇒ category match {
        case Some(RdwDambordCategory.LB) ⇒
          decisions.addDecisionWithLog("Classify NL", "Dambord gives LB (Light Commercial Vehicle) license=" + license)
          classifyLB(rdw, scannedCategory, licensePlate, decisions)
        case Some(RdwDambordCategory.ZB) ⇒
          decisions.addDecisionWithLog("Classify NL", "Dambord gives ZB (Heavy Commercial Vehicle) license=" + license)
          classifyZB(rdw, scannedCategory, decisions)
        case Some(RdwDambordCategory.B) ⇒
          decisions.addDecisionWithLog("Classify NL", "Dambord gives B (Commercial vehicle) license=" + license)
          classifyB(rdw, scannedCategory, licensePlate, decisions)
        case Some(RdwDambordCategory.O) ⇒
          decisions.addDecisionWithLog("Classify NL", "Dambord gives O (Truck-Trailer) license=" + license)
          classifyOA(rdw, vregistration, decisions, RdwDambordCategory.O)
        case Some(RdwDambordCategory.A) ⇒
          decisions.addDecisionWithLog("Classify NL", "Dambord gives A (Trailer) license=" + license)
          classifyOA(rdw, vregistration, decisions, RdwDambordCategory.A)
        case Some(RdwDambordCategory.C) ⇒
          decisions.addDecisionWithLog("Classify NL", "Dambord gives C (Moped) license=" + license)
          classifyC(rdw, vregistration, decisions) //this is against req 40
        case _ ⇒
          decisions.addDecisionWithLog("Classify NL", "Dambord - Unexpected category with RDW data required for %s with RDWCategory %s".format(license, category.map(_.toString).getOrElse("Unknown")))
          classifyManual(decisions, category, scannedCategory, false, true, Some(licensePlate))
      }
    }
  }

  def classifyForeigner(country: Option[ValueWithConfidence[String]], record: VehicleSpeedRecord, scannedCategory: ScannedCategory, decisions: DecisionTree): Classification = {
    val licensePlate = record.entry.license.map(_.value).getOrElse("Unknown")
    isFromSelectedCountries(country, decisions) match {
      case Some(false) ⇒
        val res = vehicleCodeSwitch(step = "ClassifyNoTreatyCountry",
          licensePlate = licensePlate,
          category = scannedCategory, decisions = decisions,
          unknown = ClassifyAttributes(Some(VehicleCode.MV), ProcessingIndicator.Mobi, false, IndicatorReason().setUnreliableClassification(), manualIndicationMessage = Some("Notreatry country and Category is unknown")),
          person = ClassifyAttributes(Some(VehicleCode.MV), ProcessingIndicator.Mobi, false, IndicatorReason()),
          bus = ClassifyAttributes(Some(VehicleCode.AT), ProcessingIndicator.Mobi, false, IndicatorReason()),
          other = ClassifyAttributes(Some(VehicleCode.VA), ProcessingIndicator.Mobi, false, IndicatorReason()))

        decisions.addDecisionWithLog("ClassifyForeigner",
          "License=%s VehicleCode=%s Processing=%s Country=%s reason=No Treaty country".format(licensePlate,
            res.vehicleCode.getOrElse("None"), res.indication, country))
        res.manualIndicationMessage.foreach(msg ⇒ log.info("classifyManual: license=%s %s".format(licensePlate, msg)))
        Classification(res.vehicleCode, res.indication, res.reason, dambord = None) //req 40
      case Some(true) ⇒
        val res = vehicleCodeSwitch(step = "ClassifyTreatryCountry",
          licensePlate = licensePlate,
          category = scannedCategory, decisions = decisions,
          unknown = ClassifyAttributes(Some(VehicleCode.MV), ProcessingIndicator.Manual, true, IndicatorReason().setUnreliableClassification(), manualIndicationMessage = Some("AccordingSpecs: Treatry country and Category is unknown")),
          person = ClassifyAttributes(Some(VehicleCode.MV), ProcessingIndicator.Automatic, false, IndicatorReason()),
          bus = ClassifyAttributes(Some(VehicleCode.AT), ProcessingIndicator.Manual, true, IndicatorReason().setUnreliableClassification(), manualIndicationMessage = Some("AccordingSpecs: Treatry country and Bus")),
          other = ClassifyAttributes(Some(VehicleCode.VA), ProcessingIndicator.Manual, true, IndicatorReason().setUnreliableClassification(), manualIndicationMessage = Some("AccordingSpecs: Treatry country and VA")))

        decisions.addDecisionWithLog("ClassifyForeigner",
          "License=%s VehicleCode=%s Processing=%s Country=%s reason=Treaty country".format(licensePlate,
            res.vehicleCode.getOrElse("None"), res.indication, country))

        res.manualIndicationMessage.foreach(msg ⇒ log.info("classifyManual: license=%s %s".format(licensePlate, msg)))

        val classify = Classification(res.vehicleCode, res.indication, res.reason, None, false, false, None, res.accordingToSpecs, dambord = None)
        updateForReliableFlag("ClassifyForeigner", classify, scannedCategory, decisions)
      case _ ⇒
        val msg = country match {
          case Some(c) ⇒
            "country confidence is to low %d country=%s".format(c.confidence, c.value)
          case None ⇒
            "No country"
        }
        val res = vehicleCodeSwitch(step = "ClassifyCountryUnreliable",
          licensePlate = licensePlate,
          category = scannedCategory, decisions = decisions,
          unknown = ClassifyAttributes(Some(VehicleCode.MV), ProcessingIndicator.Manual, false, IndicatorReason().setUnreliableCountry(), manualIndicationMessage = Some(msg + " and Category is unknown")),
          person = ClassifyAttributes(Some(VehicleCode.MV), ProcessingIndicator.Manual, false, IndicatorReason().setUnreliableCountry(), manualIndicationMessage = Some(msg + " and Category is personcar")),
          bus = ClassifyAttributes(Some(VehicleCode.AT), ProcessingIndicator.Manual, false, IndicatorReason().setUnreliableCountry(), manualIndicationMessage = Some(msg + " and Bus")),
          other = ClassifyAttributes(Some(VehicleCode.VA), ProcessingIndicator.Manual, false, IndicatorReason().setUnreliableCountry(), manualIndicationMessage = Some(msg + " and VA")))

        decisions.addDecisionWithLog("ClassifyForeigner",
          "License=%s VehicleCode=%s Processing=%s Country=%s reason=%s".format(licensePlate,
            res.vehicleCode.getOrElse("None"), res.indication, country, msg))

        res.manualIndicationMessage.foreach(resMsg ⇒ log.info("classifyManual: license=%s %s".format(licensePlate, resMsg)))

        Classification(code = res.vehicleCode, indicator = res.indication, reason = res.reason, dambord = None)
    }
  }

  /**
   *
   * @param classification
   * @return
   */
  def decideFinalClassification(classification: Classification,
                                scannedCategory: ScannedCategory,
                                record: VehicleSpeedRecord,
                                decisions: DecisionTree): Classification = {
    /*
     * There are problems with the license readers. This causes a lot of invalid violations;
     * wrong license plate indication by the picture in the final 'police record'.
     * People complain, police have to say sorry. Client really don't like that and on the other way
     * it costs CSC malus.
     *
     * When known 'invalid' license plates are passing and speeding they must be sent to
     * the manual processing.
     */
    val (licenseToManual, newCategoryCode) = getLicense(record) match {
      case Some(license) ⇒ licenseOnWrongReadlist(license, scannedCategory.isPersonCar(), decisions)
      case _             ⇒ (false, None)
    }

    //final indicator; Automatic, Manual or Mobi
    val indicator = if (licenseToManual) {
      decisions.addDecisionWithLog("FinalClassification", "Force to manual because of wrong read license plate: " + getLicense(record))
      ProcessingIndicator.Manual
    } else {
      decisions.addDecisionWithLog("FinalClassification", "Use original ProcessingIndicator: " + classification.indicator)
      classification.indicator
    }

    //final indicator reason
    val indicatorReason = if (indicator == ProcessingIndicator.Manual && classification.reason.mask == 0) {
      /*
       * The indication can not be manual and the reason for manual is none (0).
       * When the indication is manual the reason must be set. Set it to all reasons (7)
       */
      decisions.addDecisionWithLog("FinalClassification", "ProcessingIndicator switched to manual. Set reason to 7. ")
      IndicatorReason(7)
    } else {
      decisions.addDecisionWithLog("FinalClassification", "Use original reason: " + classification.reason)
      classification.reason
    }

    /*
     * final classification;
     *  change only the data from the original that is 'finalized' in this method
     */
    val finalClassification = if (licenseToManual && classification.code != newCategoryCode) {
      decisions.addDecisionWithLog("FinalClassification", "Use another CategoryCode because of wrong read licenseplate: " + getLicense(record))
      classification.copy(reason = indicatorReason, indicator = indicator, code = newCategoryCode)
    } else {
      classification.copy(reason = indicatorReason, indicator = indicator)
    }

    log.debug("record.entry.license.get.confidence:" + record.entry.license.get.confidence)
    log.debug("config.classifyConfidenceIntrada.get.confidence:" + config.classifyConfidenceIntrada.get.confidence)
    log.debug("record.entry.recognizedBy.get.toLowerCase:" + record.entry.recognizedBy.getOrElse("No recognizedBy").toLowerCase)

    val finalClassificationWithIntradaConfidenceCheck = if (record.entry.license.get.confidence < config.classifyConfidenceIntrada.get.confidence &&
      record.entry.recognizedBy.get.toLowerCase == "intrada") {
      decisions.addDecisionWithLog("LicenseConfidence", "Set to Manual because of license: %s and config.classifyConfidenceIntrada:%d".format(record.entry.license.get, record.entry.license.get.confidence))
      finalClassification.copy(reason = IndicatorReason(7), indicator = ProcessingIndicator.Manual)
    } else {
      decisions.addDecisionWithLog("LicenseConfidence", "Normal flow. RecognizedBy = %s and license.confidence = %d".format(record.entry.recognizedBy.getOrElse("No recognizedBy").toLowerCase, config.classifyConfidenceIntrada.get.confidence))
      finalClassification
    }

    log.debug("Before final classification reached")
    logClassification(classification, record)
    log.debug("End off classification reached. ProcessingIndicator or ManualReason can be changed")
    logClassification(finalClassificationWithIntradaConfidenceCheck, record)

    finalClassificationWithIntradaConfidenceCheck
  }

  private def getRDWData(requestRdwData: Boolean, licensePlate: String): Option[RdwData] = {
    if (requestRdwData) {
      rdwRegistry.getData(licensePlate) match {
        case Some(data) ⇒
          if (data.hasEmptyFields) {
            log.warning("Found empty fields in RDW record for %s: %s".format(licensePlate, data))
          }
          Some(data)
        case None ⇒ None
      }
    } else None
  }

  /**
   * retrieve license
   * @param record speedrecord
   * @return
   */
  private def getLicense(record: VehicleSpeedRecord): Option[String] = record.exit.getOrElse(record.entry).license.map(_.value)

  /**
   * log a classification
   * @param classification
   * @param record
   */
  private def logClassification(classification: Classification, record: VehicleSpeedRecord) {
    log.debug("%s Indication: license=%s code=%s, alternativeCode=%s, reason=%s, entryLane=%s exitLane=%s".format(
      classification.indicator.toString,
      record.entry.license.map(_.value).getOrElse("Unknown"),
      classification.code.toString,
      classification.alternativeClassification.toString,
      classification.reason.mask.toString,
      record.entry.lane.laneId,
      record.exit.map(_.lane.laneId).getOrElse("empty")))
  }

  /**
   * Some(Right) -> to be classified
   * Some(Left) -> wrong license plate
   * None -> cannot be classified
   */
  private def canBeClassified(record: VehicleSpeedRecord): Option[Either[String, String]] = {
    val vr = record.exit.getOrElse(record.entry)
    vr.license match {
      case (Some(license)) if license.value.size > maxLicensePlateLength ⇒
        Some(Left(license.value.take(maxLicensePlateLength)))
      case (Some(license)) ⇒
        Some(Right(license.value))
      case _ ⇒
        None
    }
  }

  /**
   * check the country code is one of the values defined for 'Mobi'.
   * Return None if the confidence level is too low.
   */
  private def isFromSelectedCountries(countryValue: Option[ValueWithConfidence[String]], decisions: DecisionTree): Option[Boolean] = {
    countryValue match {
      case Some(ValueWithConfidence(country, confidence)) ⇒
        // Is it a contract country (D, CH, B, ...)
        val isMobiCountry = config.mobiCountries.exists(_ == country)

        // Is it a country which have a simulair layout as the Dutch or contract country
        val isSimilar = config.similarLicenseMobiCountries.exists(_ == country)

        val details = Seq(new Decision("FromSelectedCountries", "This is a 'ContractCountry': " + isMobiCountry.toString, Seq()),
          new Decision("FromSelectedCountries", "This is a 'SimilarCountry': " + isSimilar.toString, Seq()))

        if (!isSimilar && !isMobiCountry) {
          decisions.addDecisionWithLog("FromSelectedCountries",
            "This country is Mobi and not Simular: " + country, details)
          Some(false) //mobi => don't use the confidence
        } else {
          val allDetails = details :+ new Decision("FromSelectedCountries",
            "Country Confidence: found %d > limit %d".format(confidence, config.countryCodeConfidence), Seq())

          if (confidence > config.countryCodeConfidence) {
            if (isMobiCountry) {
              decisions.addDecisionWithLog("FromSelectedCountries", "Contract country: " + country, allDetails)
            } else {
              decisions.addDecisionWithLog("FromSelectedCountries", "Simular country: " + country, allDetails)
            }
            Some(isMobiCountry) //auto or manual
          } else {
            val msg = "Country Confidence is to low country=%s conf=%d".format(country, confidence)
            decisions.addDecisionWithLog("FromSelectedCountries", msg, allDetails)
            None //hand => low confidence
          }
        }
      case _ ⇒
        decisions.addDecisionWithLog("FromSelectedCountries", "No country found")
        None
    }
  }

  private def classifyMil(category: ScannedCategory, vregistration: VehicleMetadata, licensePlate: LicensePlateNL, decisions: DecisionTree): Classification = {
    /**
     * The VehicleCode is taken from Voertuigclassificatie version 4.4.
     */
    val isCar = category.isPersonCar(decisions)

    val code = if (isCar || category.getCategory() == VehicleCategory.Unknown) {
      decisions.addDecisionWithLog("Classify MIL", "It is a Personcar or Unknown. VehicleCode = CA")
      VehicleCode.CA
    } else {
      decisions.addDecisionWithLog("Classify MIL", "It is NOT a Personcar. VehicleCode = VA")
      VehicleCode.VA
    }

    val (indicator, reason) = if (category.isReliable().getOrElse(false)) {
      decisions.addDecisionWithLog("Classify MIL", "Category is reliable. Automatic processing")
      (ProcessingIndicator.Automatic, IndicatorReason())
    } else {
      decisions.addDecisionWithLog("Classify MIL", "Category is NOT reliable. Manual processing")
      (ProcessingIndicator.Manual, IndicatorReason().setUnreliableClassification())
    }

    Classification(code = Some(code), indicator = indicator, reason = reason, rdwSizeCategory = None, mil = true,
      invalidRdwData = false, dambord = Some(RdwDambordCategory.MIL.name))
  }

  private def classifyP(category: ScannedCategory, licensePlate: LicensePlateNL, decisions: DecisionTree): Classification = {

    val classify = if (category.isPersonCarForPClassification(decisions)) {
      getRDWData(true, licensePlate.plate) match {
        case None ⇒
          decisions.addDecisionWithLog("Classify P", "Manual Indication, reason=No RDW data available")
          log.info("Classify P: Manual Indication: license=%s reason=No RDW data".format(licensePlate.plate))
          Classification(code = None,
            indicator = ProcessingIndicator.Manual,
            reason = IndicatorReason().setUnreliableClassification(),
            rdwSizeCategory = None,
            mil = false,
            invalidRdwData = true,
            modifiedLicensePlate = None,
            manualAccordingToSpec = false,
            alternativeClassification = Some(VehicleCode.PA),
            dambord = Some(RdwDambordCategory.P.name)) //no RDW data
        case Some(rdwData) if rdwData.hasEmptyFields ⇒
          decisions.addDecisionWithLog("Classify P", "Manual Indication, reason=Empty fields in RDW data")
          log.info("Classify P: Manual Indication: license=%s reason=Empty fields in RDW data".format(licensePlate.plate))
          Classification(code = None,
            indicator = ProcessingIndicator.Manual,
            reason = IndicatorReason().setUnreliableClassification(),
            rdwSizeCategory = None,
            mil = false,
            invalidRdwData = true,
            modifiedLicensePlate = None,
            manualAccordingToSpec = true,
            alternativeClassification = Some(VehicleCode.PA),
            dambord = Some(RdwDambordCategory.P.name)) //Empty fields in RDW data
        case Some(rdwData) ⇒
          detectCamper(rdwData, VehicleCode.PA, licensePlate, decisions, RdwDambordCategory.P)
      }
    } else if (category.getCategory().equals(VehicleCategory.CarTrailer)) {
      //AS auto
      decisions.addDecisionWithLog("Classify P",
        "license=%s VehicleCode=AS Processing=Manual reason=Scanned Category is CarTrailer".format(licensePlate.plate))
      //Classification(Some(VehicleCode.AS), ProcessingIndicator.Automatic, IndicatorReason(), true)
      //TODO: temporary fix to make sure we classify cars as AS wrongly: put it to manual
      Classification(code = Some(VehicleCode.AS), indicator = ProcessingIndicator.Manual, reason = IndicatorReason().setUnreliableClassification(), dambord = Some(RdwDambordCategory.P.name))
    } else if (category.getCategory().equals(VehicleCategory.Unknown)) {
      decisions.addDecisionWithLog("Classify P",
        "License=%s VehicleCode=PA Processing=Manual reason=Scanned Category probably false or unknown".format(licensePlate.plate))
      Classification(code = Some(VehicleCode.PA), indicator = ProcessingIndicator.Manual, reason = IndicatorReason().setUnreliableClassification(), dambord = Some(RdwDambordCategory.P.name))
    } else {
      //hand
      decisions.addDecisionWithLog("Classify P",
        "license=%s VehicleCode=AS Processing=Manual reason=Probably a Trailer, ScannedCategory is %s".format(licensePlate.plate, category.getCategory().toString))
      Classification(code = Some(VehicleCode.AS), indicator = ProcessingIndicator.Manual, reason = IndicatorReason().setUnreliableClassification(), dambord = Some(RdwDambordCategory.P.name))
    }

    updateForReliableFlag("Classify P", classify, category, decisions)
  }

  private def classifyAccordingToSpec(dambord: Category, category: ScannedCategory, licensePlate: LicensePlateNL, decisions: DecisionTree): Classification = {
    decisions.addDecisionWithLog("Classify AccordingSpecs", "Manual Indication, reason=Special Dambord code %s".format(dambord))
    val res = vehicleCodeSwitch(step = "ClassifyAccordingSpecs",
      licensePlate = licensePlate.plate,
      category = category, decisions = decisions,
      unknown = ClassifyAttributes(Some(VehicleCode.PA), ProcessingIndicator.Manual, true, IndicatorReason().setUnreliableClassification(), manualIndicationMessage = Some("AccordingSpecs: Category is unknown")),
      person = ClassifyAttributes(Some(VehicleCode.PA), ProcessingIndicator.Manual, true, IndicatorReason().setUnreliableClassification(), manualIndicationMessage = Some("AccordingSpecs: Category is personcar")),
      bus = ClassifyAttributes(Some(VehicleCode.AT), ProcessingIndicator.Manual, true, IndicatorReason().setUnreliableClassification(), manualIndicationMessage = Some("AccordingSpecs:  and Bus")),
      other = ClassifyAttributes(Some(VehicleCode.VA), ProcessingIndicator.Manual, true, IndicatorReason().setUnreliableClassification(), manualIndicationMessage = Some("AccordingSpecs:  and VA")))

    res.manualIndicationMessage.foreach(msg ⇒ log.info("classifyManual: license=%s %s".format(licensePlate, msg)))
    Classification(res.vehicleCode, res.indication, res.reason, None, false, false, None, res.accordingToSpecs, dambord = None)
  }

  private def classifyLB(rdwData: RdwData, category: ScannedCategory, licensePlate: LicensePlateNL, decisions: DecisionTree): Classification = {
    val step = "Classify LB"
    var msg: String = ""

    if (isBus(rdwData)) {
      msg = "License=%s TypeVehicle=Bus, Rdw deviceCode=" + rdwData.deviceCode
      decisions.addDecisionWithLog(step, msg)

      val code = if (isDiffMax100(rdwData)) {
        decisions.addDecisionWithLog("Classify LB", "diffMaxSpeed=" + rdwData.diffMaxSpeed + " VehicleCode=AT")
        VehicleCode.AT
      } else {
        decisions.addDecisionWithLog("Classify LB", "diffMaxSpeed=" + rdwData.diffMaxSpeed + " VehicleCode=AB")
        VehicleCode.AB
      }
      Classification(code = Some(code), indicator = ProcessingIndicator.Automatic, reason = IndicatorReason(), dambord = Some(RdwDambordCategory.LB.name))
    } else if (category.isPersonCar(decisions) || category.getCategory().equals(VehicleCategory.Truck)) {
      decisions.addDecisionWithLog("Classify LB", "TypeVehicle=No trailer, category=" + category.getCategory().toString)
      val classify = detectCamper(rdwData, VehicleCode.CA, licensePlate, decisions, RdwDambordCategory.LB)
      updateForReliableFlag("Classify LB", classify, category, decisions)
    } else if (category.getCategory().equals(VehicleCategory.CarTrailer)) {
      //AS auto
      decisions.addDecisionWithLog("Classify LB",
        "license=%s  VehicleCode=AS, Processing=Manual Category=%s".format(licensePlate.plate, category.getCategory().toString))

      //val classify = Classification(Some(VehicleCode.AS), ProcessingIndicator.Automatic, IndicatorReason(), true)
      //updateForReliableFlag("Classify LB", classify, category, decisions)
      //TODO: temporary fix to make sure we classify cars as AS wrongly: put it to manual
      Classification(code = Some(VehicleCode.AS), indicator = ProcessingIndicator.Manual, reason = IndicatorReason().setUnreliableClassification(), dambord = Some(RdwDambordCategory.LB.name))
    } else if (category.getCategory().equals(VehicleCategory.Unknown)) {
      decisions.addDecisionWithLog("Classify LB", "License=%s VehicleCode=CA Processing=Manual reason=Category UnKnown".format(licensePlate.plate))
      Classification(code = Some(VehicleCode.CA), indicator = ProcessingIndicator.Manual, reason = IndicatorReason().setUnreliableClassification(), dambord = Some(RdwDambordCategory.P.name))
    } else {
      //hand
      decisions.addDecisionWithLog("Classify LB", "TypeVehicle=Possible Trailer, category=" + category.getCategory().toString +
        ", VehicleCode=AS, Processing=Manual")
      log.info("ClassifyLP: license=%s, Processing=Manual, reason=IncorrectCategory %s".format(licensePlate.plate, category.toString))
      Classification(code = Some(VehicleCode.AS), indicator = ProcessingIndicator.Manual, reason = IndicatorReason().setUnreliableClassification(), dambord = Some(RdwDambordCategory.LB.name))
    }
  }

  private def classifyZB(rdwData: RdwData, category: ScannedCategory, decisions: DecisionTree): Classification = {
    isBus(rdwData) match {
      case true ⇒
        decisions.addDecisionWithLog("Type vehicle", "Bus deviceCode=" + rdwData.deviceCode)
        val code = if (isDiffMax100(rdwData)) {
          decisions.addDecisionWithLog("isMaxSpeed100", "true diffMaxSpeed=" + rdwData.diffMaxSpeed)
          VehicleCode.AT
        } else {
          decisions.addDecisionWithLog("isMaxSpeed100", "false diffMaxSpeed=" + rdwData.diffMaxSpeed)
          VehicleCode.AB
        }
        Classification(code = Some(code), indicator = ProcessingIndicator.Automatic, reason = IndicatorReason(), dambord = Some(RdwDambordCategory.ZB.name))
      case false ⇒
        decisions.addDecisionWithLog("Type vehicle", "No bus, deviceCode=" + rdwData.deviceCode)
        val (someCode, processing) = if (category.getCategory().equals(VehicleCategory.Car) ||
          category.getCategory().equals(VehicleCategory.Truck) || category.getCategory().equals(VehicleCategory.Bus)) {
          if (isCamper(rdwData)) {
            decisions.addDecisionWithLog("Type vehicle", "Camper deviceCode=" + rdwData.deviceCode)
            (Some(VehicleCode.CB), ProcessingIndicator.Automatic)
          } else {
            decisions.addDecisionWithLog("Type vehicle", "Truck deviceCode=" + rdwData.deviceCode)
            (Some(VehicleCode.VA), ProcessingIndicator.Automatic)
          }
        } else if (category.getCategory().equals(VehicleCategory.CarTrailer) || category.getCategory().equals(VehicleCategory.TruckTrailer)) {
          decisions.addDecisionWithLog("Type vehicle", "CarTrailer/TruckTrailer deviceCode=" + rdwData.deviceCode)
          (Some(VehicleCode.AO), ProcessingIndicator.Manual)
        } else if (category.getCategory().equals(VehicleCategory.Unknown)) {
          decisions.addDecisionWithLog("Type vehicle", "Unknown deviceCode=" + rdwData.deviceCode)
          (Some(VehicleCode.VA), ProcessingIndicator.Manual)
        } else {
          (None, ProcessingIndicator.Manual)
        }
        val alternativeClass = someCode match {
          case None ⇒ Some(VehicleCode.VA)
          case _    ⇒ None
        }
        Classification(code = someCode, indicator = processing, reason = IndicatorReason(), alternativeClassification = alternativeClass, dambord = Some(RdwDambordCategory.ZB.name))
    }
  }

  private def classifyB(rdwData: RdwData, category: ScannedCategory, licensePlate: LicensePlateNL, decisions: DecisionTree) = {
    val invalidRdwData = rdwData.maxMass == 0
    if (invalidRdwData) {
      decisions.addDecisionWithLog("Classify B", "Invalid RDW data " + rdwData)
      log.info("Manual Indication: license=%s reason=No RDW data".format(licensePlate.plate))
      classifyManual(decisions, Some(RdwDambordCategory.B), category, true, true, Some(licensePlate))
    } else {
      if (isLight(rdwData)) {
        decisions.addDecisionWithLog("Classify B", "Light - limit=3500 maxMass=" + rdwData.maxMass)
        classifyLB(rdwData, category, licensePlate, decisions)
      } else {
        decisions.addDecisionWithLog("Classify B", "Heavy - limit=3500 maxMass=" + rdwData.maxMass)
        decisions.addDecisionWithLog("Classify B", "Invalid RDW data " + rdwData)
        classifyZB(rdwData, category, decisions)
      }
    }
  }

  private def classifyOA(rdwData: RdwData, vregistration: VehicleMetadata, decisions: DecisionTree, dambord: RdwDambordCategory.Category): Classification = {
    val code = if (isLight(rdwData)) {
      decisions.addDecisionWithLog("Classify OA", "Light - maxMass=" + rdwData.maxMass + " Code become AS")
      VehicleCode.AS
    } else {
      decisions.addDecisionWithLog("Classify OA", "Heavy - maxMass=" + rdwData.maxMass + "Code become AO ")
      VehicleCode.AO
    }
    Classification(code = Some(code), indicator = ProcessingIndicator.Automatic, reason = IndicatorReason(), dambord = Some(dambord.name))
  }

  private def classifyC(rdwData: RdwData, vregistration: VehicleMetadata, decisions: DecisionTree): Classification = {
    val code: VehicleCode.Value = if (isMaxSpeed25(rdwData)) {
      decisions.addDecisionWithLog("MaxSpeed", "25 maxSpeed=" + rdwData.maxSpeed)
      VehicleCode.BS
    } else if (isL6(rdwData)) {
      decisions.addDecisionWithLog("MaxSpeed", "Category L6 maxSpeed=" + rdwData.maxSpeed)
      VehicleCode.BB
    } else {
      decisions.addDecisionWithLog("MaxSpeed", "Category %s maxSpeed=%s".format(rdwData.eegCategory, rdwData.maxSpeed))
      VehicleCode.BF
    }
    Classification(code = Some(code), indicator = ProcessingIndicator.Manual, reason = IndicatorReason().setUnreliableClassification(),
      manualAccordingToSpec = true, dambord = Some(RdwDambordCategory.C.name))
  }

  private def detectCamper(rdwData: RdwData, defaultCode: VehicleCode.Value, licensePlate: LicensePlateNL, decisions: DecisionTree, dambord: RdwDambordCategory.Category): Classification = {
    val code = if (isCamper(rdwData)) {
      decisions.addDecisionWithLog("Detect Camper", "Vehicle is a camper deviceCode=" + rdwData.deviceCode + " VehicleCode=CP")
      VehicleCode.CP
    } else {
      decisions.addDecisionWithLog("Detect Camper", "Vehicle is NOT a camper deviceCode=" + rdwData.deviceCode + " VehicleCode=" + defaultCode)
      defaultCode
    }
    Classification(code = Some(code), indicator = ProcessingIndicator.Automatic, reason = IndicatorReason(), dambord = Some(dambord.name))
  }

  private def updateForReliableFlag(step: String, classify: Classification, category: ScannedCategory, decisions: DecisionTree): Classification = {
    if (classify.indicator == ProcessingIndicator.Automatic && !category.isReliable().getOrElse(false)) {
      //classify would be automatic but scaned category is unreliable
      decisions.addDecisionWithLog(step, "Category is NOT reliable. Manual processing")
      classify.copy(indicator = ProcessingIndicator.Manual, reason = IndicatorReason().setUnreliableClassification())
    } else {
      classify
    }
  }

  /**
   * check by 6 meters between PA and VA for manual processing
   */
  private def classifyManual(decisions: DecisionTree, dambord: Option[RdwDambordCategory.Category], category: ScannedCategory, invalidRdwData: Boolean,
                             manualAccordingToSpec: Boolean, licensePlate: Option[LicensePlateNL], reason: IndicatorReason = IndicatorReason().setUnreliableClassification()): Classification = {
    decisions.addDecisionWithLog("ClassifyManual", "Category " + dambord.map(_.toString).getOrElse("NONE"))

    val code = dambord match {
      case Some(RdwDambordCategory.MIL) ⇒ if (category.isPersonCar(decisions)) Some(VehicleCode.CA) else Some(VehicleCode.VA)
      case Some(RdwDambordCategory.M)   ⇒ Some(VehicleCode.MV)
      case Some(RdwDambordCategory.P)   ⇒ if (category.isPersonCar(decisions)) Some(VehicleCode.PA) else Some(VehicleCode.AS)
      case Some(RdwDambordCategory.LB) ⇒
        val cat = category.getCategory()
        if (category.isPersonCar(decisions) || cat == VehicleCategory.Truck)
          Some(VehicleCode.CA)
        else
          Some(VehicleCode.AS)
      case Some(RdwDambordCategory.ZB) ⇒ Some(VehicleCode.VA)
      case Some(RdwDambordCategory.B) ⇒
        if (category.isPersonCar(decisions))
          Some(VehicleCode.CA)
        else if (category.getCategory() == VehicleCategory.CarTrailer) {
          decisions.addDecisionWithLog("ClassifyManual", "Trailer Category is CarTrailer")
          Some(VehicleCode.AS)
        } else {
          decisions.addDecisionWithLog("ClassifyManual", "No Trailer Category " + category.getCategory().toString)
          Some(VehicleCode.VA)
        }
      case Some(RdwDambordCategory.O) ⇒ Some(VehicleCode.AO)
      case Some(RdwDambordCategory.A) ⇒ Some(VehicleCode.AO)
      case Some(RdwDambordCategory.C) ⇒ None //probably auto-ambulance
      case _ ⇒
        val res = vehicleCodeSwitch(step = "ClassifyManual",
          licensePlate = licensePlate.map(_.plate).getOrElse("Unknown"),
          category = category, decisions = decisions,
          unknown = ClassifyAttributes(Some(VehicleCode.PA), ProcessingIndicator.Manual, false, reason),
          person = ClassifyAttributes(Some(VehicleCode.PA), ProcessingIndicator.Manual, false, reason),
          bus = ClassifyAttributes(Some(VehicleCode.AT), ProcessingIndicator.Manual, false, reason),
          other = ClassifyAttributes(Some(VehicleCode.VA), ProcessingIndicator.Manual, false, reason))
        res.vehicleCode
    }

    val altCode = if (code.isDefined) None else Some(VehicleCode.VA)
    //Req 42 invalid RDW => code must be undetermined
    val (finalCode, finaleAltCode) = if (invalidRdwData) {
      (None, code.orElse(altCode))
    } else {
      (code, altCode)
    }
    log.info("classifyManual: license=%s category=%s".format(licensePlate.map(_.plate).getOrElse("Unknown"), code))
    Classification(code = finalCode, indicator = ProcessingIndicator.Manual, reason = reason, rdwSizeCategory = None, mil = false,
      invalidRdwData = invalidRdwData, modifiedLicensePlate = None, manualAccordingToSpec = manualAccordingToSpec,
      alternativeClassification = finaleAltCode, dambord = dambord.map(_.name))
  }

  // Waarom zitten deze eigenschappen niet op RdwData? Dit zijn derived properties. Allemaal afgeleide info van RdwData
  // waar komen de magic ints vandaan? Constanten zijn beter.
  private def isCamper(rdwData: RdwData): Boolean =
    rdwData.deviceCode == 68 || rdwData.deviceCode == 25

  private def isBus(rdwData: RdwData): Boolean = rdwData.deviceCode == 1

  private def isDiffMax100(rdwData: RdwData): Boolean = rdwData.diffMaxSpeed == 100

  private def isMaxSpeed25(rdwData: RdwData): Boolean = rdwData.maxSpeed <= 25

  private def isL6(rdwData: RdwData): Boolean = rdwData.eegCategory == "L6"

  private def createIndicatorReason(record: VehicleSpeedRecord, classification: Boolean): IndicatorReason = {
    val license = record.entry.license.orElse(record.exit.flatMap(_.license))
    val country = record.country
    createIndicatorReason(license, country, classification)
  }
  private def createIndicatorReason(license: Option[ValueWithConfidence[String]], country: Option[ValueWithConfidence[String]], classification: Boolean): IndicatorReason = {
    var reason = IndicatorReason()
    if (license.isEmpty) {
      reason = reason.setUnreliableLicense()
    }
    if (country.isEmpty) {
      reason = reason.setUnreliableCountry()
    }
    if (classification) {
      reason = reason.setUnreliableClassification()
    }
    reason
  }
  /**
   * The mass is less or equal to 3500 kg
   */
  private def isLight(rdwData: RdwData): Boolean = rdwData.maxMass <= 3500

  def classify(speedRecord: VehicleSpeedRecord): VehicleClassifiedRecord = {
    log.info("==== Start classify record ====")
    val Classification(vehicleCode, indicator, reason, _, mil, invalidRdwData, modified, manualAccordingToSpec, alterClass, dambord) = classifyRecord(speedRecord)
    log.info("==== Done classify record ====")
    new VehicleClassifiedRecord(VehicleClassifiedTable.makeKey(speedRecord), speedRecord, vehicleCode, indicator, mil, invalidRdwData, modified, manualAccordingToSpec, reason, alterClass, dambord)
  }

  def licenseOnWrongReadlist(licensePlate: String, isPersonCar: Boolean, decisions: DecisionTree): (Boolean, Option[VehicleCode.Value]) = {

    // Is the license plate in the wrongReadList?
    val plate = config.wrongReadLicense.find(wrongPlate ⇒ wrongPlate.plate == licensePlate)

    plate match {
      case Some(x) ⇒
        decisions.addDecisionWithLog("licenseInWrongReadList", "License %s in wrongReadList".format(licensePlate))

        if (isPersonCar) {
          if (x.isPlatePersonCar) {
            // Car of wrongread license plates is parsing
            decisions.addDecisionWithLog("licenseInWrongReadList", "Vehicle wrong read license passed: set to PA")
            (true, Some(VehicleCode.PA))
          } else {
            if (x.isOtherPlatePersonCar) {
              // The other car is passing
              decisions.addDecisionWithLog("licenseInWrongReadList", "Other vehicle passed: set to PA")
              (true, Some(VehicleCode.PA))
            } else (true, Some(VehicleCode.PA))
          }
        } else {
          if (!x.isPlatePersonCar) {
            // Car of the wrongread licenseplate is parsing
            decisions.addDecisionWithLog("licenseInWrongReadList", "Vehicle wrong read license passed: set to VA")
            (true, Some(VehicleCode.VA))
          } else {
            if (!x.isOtherPlatePersonCar) {
              decisions.addDecisionWithLog("licenseInWrongReadList", "Other vehicle passed: set to VA")
              (true, Some(VehicleCode.VA))
            } else (true, Some(VehicleCode.VA))
          }
        }
      case None ⇒
        log.debug("License %s NOT in wrongReadList".format(licensePlate))
        (false, None)
    }
  }

  case class ClassifyAttributes(vehicleCode: Option[VehicleCode.Value],
                                indication: ProcessingIndicator.Value,
                                accordingToSpecs: Boolean,
                                reason: IndicatorReason,
                                manualIndicationMessage: Option[String] = None)

  def vehicleCodeSwitch(step: String, licensePlate: String, category: ScannedCategory, decisions: DecisionTree, unknown: ClassifyAttributes, person: ClassifyAttributes, bus: ClassifyAttributes, other: ClassifyAttributes): ClassifyAttributes = {

    val result = if (category.getCategory().equals(VehicleCategory.Unknown)) {
      decisions.addDecisionWithLog(step, "category is unknown: set to " + unknown)
      unknown
    } else if (category.isPersonCar(decisions)) {
      decisions.addDecisionWithLog(step, "category is person car: set to " + person)
      person
    } else if (category.getCategory().equals(VehicleCategory.Bus)) {
      decisions.addDecisionWithLog(step, "category is a bus: set to " + bus)
      bus
    } else {
      decisions.addDecisionWithLog(step, "category is " + category.getCategory() + " : set to " + other)
      other
    }
    if (result.indication == ProcessingIndicator.Automatic && !category.isReliable().getOrElse(false)) {
      log.info("classifyManual: license=%s category is unreliable".format(licensePlate))
      decisions.addDecisionWithLog(step, "category is unreliable: set to manual")
      result.copy(indication = ProcessingIndicator.Manual, reason = IndicatorReason().setUnreliableClassification())
    } else {
      result
    }
  }
}

