/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.classify.nl.config

import csc.sectioncontrol.storage._
import csc.config.Path
import csc.curator.utils.Curator
import collection.immutable.List
import csc.sectioncontrol.classify.nl.ClassifierConfig
import csc.sectioncontrol.storage.ZkClassifyConfidence
import csc.sectioncontrol.classify.nl.WheelBaseInfo
import csc.sectioncontrol.storage.ZkWheelBase
import csc.sectioncontrol.storage.ZkClassifyCountry
import com.typesafe.config.Config
import akka.util.Duration
import java.util.concurrent.TimeUnit
import csc.sectioncontrol.storagelayer.classify.ClassifyIntradaConfidenceServiceZooKeeper

case class AmqpConfiguration(actorName: String, amqpUri: String, reconnectDelay: Duration)
case class AMQPClassifyConfig(producerEndpoint: String, consumerQueue: String, dirtyDuration: Duration)

object ClassifierConfigBuilder {
  private val defaultCountry = ZkClassifyCountry()
  private val rootConfig = "amqp"

  def createAmqpConfig(config: Config): AmqpConfiguration = {
    val actorName = config.getString(rootConfig + ".connection.actor-name")
    val amqpUri = config.getString(rootConfig + ".connection.amqp-uri")
    val reconnectDelay = Duration.create(config.getMilliseconds(rootConfig + ".connection.reconnect-delay"), TimeUnit.MILLISECONDS)
    new AmqpConfiguration(actorName, amqpUri, reconnectDelay)
  }
  def createAmqpClassifyConfig(config: Config): Option[AMQPClassifyConfig] = {
    val enable = config.getString(rootConfig + ".classify.enable").trim.toUpperCase
    if (enable == "ON" || enable == "TRUE") {
      val consumerQueue = config.getString(rootConfig + ".classify.consumer-queue")
      val producerEndpoint = config.getString(rootConfig + ".classify.producer-endpoint")
      val dirty = Duration.create(config.getMilliseconds(rootConfig + ".classify.cacheDirty"), TimeUnit.MILLISECONDS)
      Some(new AMQPClassifyConfig(producerEndpoint, consumerQueue, dirty))
    } else {
      None
    }
  }

  def createConfig(curator: Curator, systemId: String): Option[ClassifierConfig] = {
    for {
      confidence ← curator.get[ZkClassifyConfidence](Paths.Configuration.getClassifyConfidence)
      preferences ← curator.get[ZkPreference](Seq(Path(Paths.Preferences.getGlobalConfigPath), Path(Paths.Preferences.getConfigPath(systemId))), ZkPreference.default)
      country ← curator.get[ZkClassifyCountry](Seq(Path(Paths.Configuration.getClassifyCountry)), defaultCountry)
    } yield ClassifierConfig(preferences.inaccuracyMarginForLength / 100.0f,
      preferences.minimumLengthTrailer / 100.0f,
      countryCodeConfidence = preferences.countryCodeConfidence,
      wheelBaseInfo = buildWheelBaseInfo(preferences.wheelBase),
      classifyConfidence = confidence,
      mobiCountries = country.mobiCountries,
      similarLicenseMobiCountries = country.similarCountries,
      correctTDC3 = retrieveOrCreateDefaultCorrectTDC3Category(curator),
      wrongReadLicense = retrieveWrongReadLicense(curator),
      classifyConfidenceIntrada = getClassifyConfidenceIntrada(curator))
  }

  def getClassifyConfidenceIntrada(curator: Curator): Option[ZkClassifyConfidenceIntrada] = {
    val classifyIntradaConfidenceServiceZooKeeper = new ClassifyIntradaConfidenceServiceZooKeeper(curator)
    classifyIntradaConfidenceServiceZooKeeper.get()
  }

  private def buildWheelBaseInfo(wheelBase: List[ZkWheelBase]): WheelBaseInfo = {
    import ZkWheelbaseType._
    val wbMap = wheelBase.map { case ZkWheelBase(key, value) ⇒ key -> value.toFloat }.toMap

    def wbValue(key: ZkWheelbaseType.Value): Float =
      wbMap.get(key).flatMap(f ⇒ if (f == 0.0f) None else Some(f)).getOrElse(1.0f)

    WheelBaseInfo(p = Seq(2 -> wbValue(P2M),
      3 -> wbValue(P2M3M),
      4 -> wbValue(P3M4M),
      5 -> wbValue(P4M5M),
      999 -> wbValue(P5M)),
      lb = Seq(2 -> wbValue(LBB2M),
        3 -> wbValue(LBB2M3M),
        4 -> wbValue(LBB3M4M),
        5 -> wbValue(LBB4M5M),
        999 -> wbValue(LBB5M)))
  }

  private def retrieveOrCreateDefaultCorrectTDC3Category(curator: Curator): ZkCorrectTDC3CategoryConfig = {
    val path = Paths.Configuration.getCorrectTDC3Category
    curator.get[ZkCorrectTDC3CategoryConfig](path) getOrElse ZkCorrectTDC3CategoryConfig()
  }

  private def retrieveWrongReadLicense(curator: Curator): List[ZkWrongReadDutchLicensePlate] = {
    val path = Paths.Configuration.getWrongReadDutchLicensePlatePath
    curator.get[List[ZkWrongReadDutchLicensePlate]](path) getOrElse List()
  }

}