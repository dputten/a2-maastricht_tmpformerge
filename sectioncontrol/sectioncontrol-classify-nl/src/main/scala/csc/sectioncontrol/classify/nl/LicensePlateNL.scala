/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.classify.nl

import java.io.{ InputStreamReader, IOException }
import net.liftweb.json.{ DefaultFormats, Serialization }
import org.slf4j.LoggerFactory
import csc.akkautils.DirectLogging

/**
 * Dutch license plate
 */
case class LicensePlateNL(_plate: String) extends DirectLogging {
  // public vars so they can be set from the unit tests otherwise the automatic rule for the choosing the right file will be done
  // and dambord or kentekenreeks will be used for both tests
  // see getUseDambord
  var sideCodeFileName = "/sidecodes.json"
  var useDambord: Option[Boolean] = None

  val plate = if (_plate == null) "" else _plate.toUpperCase

  /**
   * Returns the side code this license plate belongs to
   */
  lazy val sideCode: Option[LicensePlateNL.SideCode] = LicensePlateNL.SideCode(plate, sideCodeFileName)

  /**
   * Returns 2 characters code (row, column) for the Dambord table
   */
  lazy val significantPlateCharacters: Option[String] = sideCode match {
    case None ⇒ None
    case Some(_sideCode) ⇒
      Some(plate.charAt(_sideCode.firstCharacter).toString + plate.charAt(_sideCode.secondCharacter).toString)
  }

  /**
   * Returns RDW category of the vehicle with this license plate
   */
  lazy val rdwCategory: Option[RdwDambordCategory.Category] = {
    getUseDambord

    useDambord match {
      case Some(true) ⇒
        (sideCode, significantPlateCharacters) match {
          case (Some(_sideCode), Some(_significantPlateCharacters)) ⇒
            if (LicensePlateNL.isMilitary(_sideCode, _significantPlateCharacters, plate))
              Some(RdwDambordCategory.MIL)
            else {
              val category = _sideCode.getDambord.getCategory(_significantPlateCharacters)
              if (category.isEmpty) {
                val log = LoggerFactory.getLogger(getClass)
                log.warn("Code combination %s isn't defined in sitecode %d (%s)".format(_significantPlateCharacters, _sideCode.code, _sideCode.dambordFile))
              }
              category
            }
          case _ ⇒
            None
        }
      case Some(false) ⇒
        (sideCode, significantPlateCharacters) match {
          case (Some(_sideCode), Some(_significantPlateCharacters)) ⇒
            _sideCode.getKentekenserie.getCategory(_sideCode.code.toString, _significantPlateCharacters)
          case _ ⇒ Some(RdwDambordCategory.ONG)
        }
      case _ ⇒
        None
    }
  }

  def getUseDambord {
    useDambord match {
      case Some(true)  ⇒ log.debug("use dambord")
      case Some(false) ⇒ log.debug("use kentekenreeks")
      case _           ⇒ setUseDambord()
    }
  }

  /**
   * The sidecode file is deserialised and contains two attributes for the category files
   * One for the dambord and one for the kentekenreeks.
   * The rule is that when kentekenreeks is filled it is the category determination that is used
   * otherwise the dambord is used.
   * When both are not filled there is a problem and an exception is thrown
   */
  def setUseDambord() {
    if (!sideCode.isEmpty) {
      if (sideCode.get.kentekenserieFile.length > 0) {
        useDambord = Some(false)
      } else if (sideCode.get.dambordFile.length > 0 && sideCode.get.kentekenserieFile.length == 0)
        useDambord = Some(true)
      else
        throw new Exception("No file found for Rdw category determination.")
    }
  }
}
object LicensePlateNL {

  val log = LoggerFactory.getLogger(getClass)

  /**
   * A sitecode definition
   * @param code  The number of the code
   * @param firstCharacter position of the first character used in the Dambord
   * @param secondCharacter position of the second character used in the Dambord
   * @param mask  The mask of the code, defining the positions of the letters and digits
   * @param dambordFile The file (classpath style) containing the dambord of the code
   * @param milCodes The defence codes within this site code
   */
  case class SideCode(code: Int, firstCharacter: Int, secondCharacter: Int, mask: String, dambordFile: String, kentekenserieFile: String, milCodes: Seq[String]) {
    /**
     * The dambord definition read from the dambordFile using the resource API
     */
    lazy val kentekenserie = {
      try {
        val file = getClass.getResourceAsStream(kentekenserieFile)
        val lines = FileToLinesReader.read(file)
        KentekenserieParser.parse(lines)
      } catch {
        case t: Throwable ⇒
          log.error("Could not initialize kentekenreeks sidecode for file file %s".format(kentekenserieFile), t)
          throw t
      }
    }

    def getKentekenserie: Kentekenserie = {
      kentekenserie
    }

    /**
     * The dambord definition read from the dambordFile using the resource API
     */
    lazy val dambord = {
      try {
        DamBordParser.parse(getClass.getResourceAsStream(dambordFile))
      } catch {
        case t: Throwable ⇒
          log.error("Could not initialize Dambord sidecode %d with file %s".format(code, dambordFile), t)
          throw t
      }
    }

    def getDambord: Dambord = {
      dambord
    }

    /**
     * Is the plate part of this site-code
     * @param data the license plate
     * @return true if it is part of this code otherwise false
     */
    private def matches(data: String): Boolean = {
      if (mask.length == data.length) {
        mask.zip(data).forall { case (ch1, ch2) ⇒ (ch1.isDigit && ch2.isDigit) || (ch1.isLetter && ch2.isLetter) }
      } else {
        false
      }
    }
  }

  object SideCode {
    /**
     * Find the correct site-code
     * @param plate the license plate
     * @return The site- code when found otherwise None
     */
    def apply(plate: String, sideCodeFileName: String): Option[SideCode] = {
      if (plate == null || plate.trim().isEmpty) {
        None
      } else {
        all(sideCodeFileName).find(code ⇒ code.matches(plate.trim().toUpperCase))
      }
    }
  }

  /**
   * Containing all the supported site-codes read from the file sideCodeFileName("/sidecodes.json")
   * When there are problems with the files an IOException is thrown
   */
  def all(sideCodeFileNameNew: String): List[SideCode] = {
    val sideCodesFile = getClass.getResource(sideCodeFileNameNew)
    if (sideCodesFile == null) {
      throw new IOException("File %s not found".format(sideCodeFileNameNew))
    }
    log.info("Reading Dambord sidecodes from file %s".format(sideCodesFile.toString))
    try {
      //can't use sideCodesFile, because it is possible that the resource is placed in the jar file
      //need to use getResourceAsStream
      val stream = getClass.getResourceAsStream(sideCodeFileNameNew)
      if (stream == null) {
        //this is very strange because the getResource can find the resource
        log.error("Could not get input stream for %s".format(sideCodeFileNameNew))
        throw new IOException("File %s not be opened".format(sideCodeFileNameNew))
      }
      val fileContent = new InputStreamReader(stream)
      implicit val formats = DefaultFormats
      Serialization.read[List[SideCode]](fileContent)
    } catch {
      case t: Throwable ⇒
        log.error("Could not initialize Dambord sidecodes", t)
        throw t
    }
  }

  /**
   * Test is the plate is a defence vehicle
   * @param sideCode the side code
   * @param _significantPlateCharacters the first and second character
   * @param licensePlate the complete plate
   * @return true when it is an defence plate otherwise false
   */
  def isMilitary(sideCode: LicensePlateNL.SideCode, _significantPlateCharacters: String, licensePlate: String): Boolean = {
    val plate = licensePlate.trim().toUpperCase
    val list = sideCode.milCodes
    // [^A-Z] leaves only the plate's characters
    list.contains(_significantPlateCharacters) || list.contains(plate.replaceAll("[^A-Z]", ""))
  }
}