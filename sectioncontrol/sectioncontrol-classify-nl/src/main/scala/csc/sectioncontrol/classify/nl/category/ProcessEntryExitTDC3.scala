/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.classify.nl.category

import csc.sectioncontrol.messages.{ ValueWithConfidence, VehicleCategory, VehicleSpeedRecord }
import csc.sectioncontrol.storage.ZkClassifyConfidence
import csc.sectioncontrol.classify.nl.{ ClassifyResult, Decision }

object ProcessEntryExitTDC3 extends ProcessCategory {
  val limit = 10

  private def isPartOfCategoryType(category: VehicleCategory.Value, confidence: Int, reliable: Option[Boolean], cat1: Option[ValueWithConfidence[VehicleCategory.Value]], cat2: Option[ValueWithConfidence[VehicleCategory.Value]]): Option[CategoryResult] = {
    cat1 match {
      case Some(cat) if (cat.value == category && cat.confidence >= confidence) ⇒ Some(CategoryResult(category, reliable, Seq(
        Decision("TDC3 category", "category=%s mimConfidence=%d reliable=%s".format(category.toString, confidence, reliable.toString), Seq(
          Decision("TDC3 Cat1", "category=%s mimConfidence=%d".format(cat1.map(_.value.toString).getOrElse(""), cat1.map(_.confidence).getOrElse(0)), Seq()),
          Decision("TDC3 Cat2", "category=%s mimConfidence=%d".format(cat2.map(_.value.toString).getOrElse(""), cat2.map(_.confidence).getOrElse(0)), Seq()))))))
      case other ⇒
        cat2 match {
          case Some(cat) if (cat.value == category && cat.confidence >= confidence) ⇒ Some(CategoryResult(category, reliable, Seq(
            Decision("TDC3 category", "category=%s mimConfidence=%d reliable=%s".format(category.toString, confidence, reliable.toString), Seq(
              Decision("TDC3 Cat1", "category=%s mimConfidence=%d".format(cat1.map(_.value.toString).getOrElse(""), cat1.map(_.confidence).getOrElse(0)), Seq()),
              Decision("TDC3 Cat2", "category=%s mimConfidence=%d".format(cat2.map(_.value.toString).getOrElse(""), cat2.map(_.confidence).getOrElse(0)), Seq()))))))
          case other ⇒ None
        }
    }
  }

  private val selectRules = Seq(
    (cat1: Option[ValueWithConfidence[VehicleCategory.Value]], cat2: Option[ValueWithConfidence[VehicleCategory.Value]]) ⇒ {
      //is equal
      val cat1Val = cat1.map(_.value)
      val cat2Val = cat2.map(_.value)
      (cat1Val, cat2Val) match {
        case (Some(val1), Some(val2)) if val1 == val2 ⇒ Some(CategoryResult(val1, Some(true), Seq(Decision("TDC3 category", "Categories are equal. Category=%s reliable=true".format(val1.toString), Seq()))))
        case (Some(val1), None) ⇒ {
          val reliable = cat1.map(_.confidence).getOrElse(0) >= limit
          Some(CategoryResult(val1, Some(reliable), Seq(Decision("TDC3 category", "Only Entry Category is found. Category=%s reliable=%s".format(val1.toString, reliable), Seq()))))
        }
        case (None, Some(val1)) ⇒ {
          val reliable = cat2.map(_.confidence).getOrElse(0) >= limit
          Some(CategoryResult(val1, Some(reliable), Seq(Decision("TDC3 category", "Only Exit Category is found. Category=%s reliable=%s".format(val1.toString, reliable), Seq()))))
        }
        case other ⇒ None
      }
    },
    (cat1: Option[ValueWithConfidence[VehicleCategory.Value]], cat2: Option[ValueWithConfidence[VehicleCategory.Value]]) ⇒ {
      isPartOfCategoryType(VehicleCategory.Car, limit, Some(true), cat1, cat2)
    },
    (cat1: Option[ValueWithConfidence[VehicleCategory.Value]], cat2: Option[ValueWithConfidence[VehicleCategory.Value]]) ⇒ {
      isPartOfCategoryType(VehicleCategory.Motor, limit, Some(true), cat1, cat2)
    },
    (cat1: Option[ValueWithConfidence[VehicleCategory.Value]], cat2: Option[ValueWithConfidence[VehicleCategory.Value]]) ⇒ {
      isPartOfCategoryType(VehicleCategory.Van, limit, Some(true), cat1, cat2)
    },
    (cat1: Option[ValueWithConfidence[VehicleCategory.Value]], cat2: Option[ValueWithConfidence[VehicleCategory.Value]]) ⇒ {
      isPartOfCategoryType(VehicleCategory.Car, 0, Some(false), cat1, cat2)
    },
    (cat1: Option[ValueWithConfidence[VehicleCategory.Value]], cat2: Option[ValueWithConfidence[VehicleCategory.Value]]) ⇒ {
      isPartOfCategoryType(VehicleCategory.Motor, 0, Some(false), cat1, cat2)
    },
    (cat1: Option[ValueWithConfidence[VehicleCategory.Value]], cat2: Option[ValueWithConfidence[VehicleCategory.Value]]) ⇒ {
      isPartOfCategoryType(VehicleCategory.Van, 0, Some(false), cat1, cat2)
    },
    (cat1: Option[ValueWithConfidence[VehicleCategory.Value]], cat2: Option[ValueWithConfidence[VehicleCategory.Value]]) ⇒ {
      isPartOfCategoryType(VehicleCategory.CarTrailer, limit, Some(true), cat1, cat2)
    },
    (cat1: Option[ValueWithConfidence[VehicleCategory.Value]], cat2: Option[ValueWithConfidence[VehicleCategory.Value]]) ⇒ {
      isPartOfCategoryType(VehicleCategory.Bus, limit, Some(true), cat1, cat2)
    },
    (cat1: Option[ValueWithConfidence[VehicleCategory.Value]], cat2: Option[ValueWithConfidence[VehicleCategory.Value]]) ⇒ {
      isPartOfCategoryType(VehicleCategory.Truck, limit, Some(true), cat1, cat2)
    },
    (cat1: Option[ValueWithConfidence[VehicleCategory.Value]], cat2: Option[ValueWithConfidence[VehicleCategory.Value]]) ⇒ {
      isPartOfCategoryType(VehicleCategory.TruckTrailer, limit, Some(true), cat1, cat2)
    },
    (cat1: Option[ValueWithConfidence[VehicleCategory.Value]], cat2: Option[ValueWithConfidence[VehicleCategory.Value]]) ⇒ {
      isPartOfCategoryType(VehicleCategory.SemiTrailer, limit, Some(true), cat1, cat2)
    },
    (cat1: Option[ValueWithConfidence[VehicleCategory.Value]], cat2: Option[ValueWithConfidence[VehicleCategory.Value]]) ⇒ {
      isPartOfCategoryType(VehicleCategory.CarTrailer, 0, Some(false), cat1, cat2)
    },
    (cat1: Option[ValueWithConfidence[VehicleCategory.Value]], cat2: Option[ValueWithConfidence[VehicleCategory.Value]]) ⇒ {
      isPartOfCategoryType(VehicleCategory.Bus, 0, Some(false), cat1, cat2)
    },
    (cat1: Option[ValueWithConfidence[VehicleCategory.Value]], cat2: Option[ValueWithConfidence[VehicleCategory.Value]]) ⇒ {
      isPartOfCategoryType(VehicleCategory.Truck, 0, Some(false), cat1, cat2)
    },
    (cat1: Option[ValueWithConfidence[VehicleCategory.Value]], cat2: Option[ValueWithConfidence[VehicleCategory.Value]]) ⇒ {
      isPartOfCategoryType(VehicleCategory.TruckTrailer, 0, Some(false), cat1, cat2)
    },
    (cat1: Option[ValueWithConfidence[VehicleCategory.Value]], cat2: Option[ValueWithConfidence[VehicleCategory.Value]]) ⇒ {
      isPartOfCategoryType(VehicleCategory.SemiTrailer, 0, Some(false), cat1, cat2)
    })

  /**
   * Determine the scanned vehicle category based on both passage points and the
   * calculated average speed.
   * @param record The speed record
   * @return A pair of the scanned category and an indication of the reliability of the value
   */
  def getScannedCategory(config: ZkClassifyConfidence, record: VehicleSpeedRecord): CategoryResult = {
    implicit var decisions = Seq[Decision]()
    val category1 = record.entry.vehicleCategory
    val category2 = record.exit.flatMap(_.vehicleCategory)

    selectRules.foreach(func ⇒ func(category1, category2))
    val listOfResults = selectRules.flatMap(func ⇒ func(category1, category2))

    listOfResults.headOption.getOrElse(CategoryResult(VehicleCategory.Unknown, None, Seq(Decision("TDC3 category", "No category match. Category Entry=%s Category Exit=%s".format(category1.toString, category2.toString), Seq()))))
  }

}