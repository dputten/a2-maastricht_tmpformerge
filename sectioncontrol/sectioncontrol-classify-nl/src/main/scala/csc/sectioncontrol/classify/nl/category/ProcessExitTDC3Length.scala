/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.classify.nl.category

import csc.sectioncontrol.messages.{ VehicleCategory, VehicleSpeedRecord }
import csc.sectioncontrol.classify.nl.trace.DecisionTree
import csc.sectioncontrol.storage.ZkClassifyConfidence
import csc.sectioncontrol.classify.nl.Decision

object ProcessExitTDC3Length extends ProcessCategory {
  /**
   * Determine the scanned vehicle category based on both passage points and the
   * calculated average speed.
   * @param record The speed record
   * @return A pair of the scanned category and an indication of the reliability of the value
   */
  def getScannedCategory(config: ZkClassifyConfidence, record: VehicleSpeedRecord): CategoryResult = {
    implicit var decisions = Seq[Decision]()
    val category = record.exit.flatMap(_.vehicleCategory)

    if (category.isEmpty || category.get.value.equals(VehicleCategory.Unknown)) {
      decisions = addDecision("Exit category", if (category.isEmpty) "None" else "Unknown")
      CategoryResult(VehicleCategory.Unknown, None, decisions)
    } else {
      val cat = category.get.value
      decisions = addDecision("Exit category", cat.toString)
      if (record.speed > 200 && !isPersonCar(cat)) {
        decisions = addDecision("SpeedCheck", "Speed %d too high for category".format(record.speed))
        CategoryResult(cat, Some(false), decisions)
      } else {
        val X1 = config.minCarLength
        val X2 = config.maxCarLength
        val X3 = config.maxVanLength
        val X4 = config.minTrailerLength

        cat match {
          case VehicleCategory.Car | VehicleCategory.Pers ⇒
            if (record.entry.length.isEmpty) {
              decisions = addDecision("LengthCheck", "skipped length= None")
              CategoryResult(cat, Some(true), decisions)
            } else {
              val length = record.entry.length.map(_.value).getOrElse(0F)
              if (X1 <= length && length < X2) {
                decisions = addDecision("LengthCheck", "true %.2f <= %.2f < %.2f".format(X1, length, X2))
                CategoryResult(cat, Some(true), decisions)
              } else {
                decisions = addDecision("LengthCheck", "false %.2f <= %.2f < %.2f".format(X1, length, X2))
                val categoryConfidence = category.get.confidence
                if (categoryConfidence < config.carConfidence) {
                  decisions = addDecision("CategoryConfCheck", "false conf=%d < %d".format(categoryConfidence, config.carConfidence))
                  CategoryResult(cat, Some(false), decisions)
                } else {
                  decisions = addDecision("CategoryConfCheck", "true conf=%d < %d".format(categoryConfidence, config.carConfidence))
                  CategoryResult(cat, Some(true), decisions)
                }
              }
            }
          case VehicleCategory.Van ⇒ {
            if (record.entry.length.isEmpty) {
              decisions = addDecision("LengthCheck", "skipped length= None")
              CategoryResult(cat, Some(true), decisions)
            } else {
              val length = record.entry.length.map(_.value).getOrElse(0F)
              if (X1 <= length && length < X3) {
                decisions = addDecision("LengthCheck", "true %.2f <= %.2f < %.2f".format(X1, length, X3))
                CategoryResult(cat, Some(true), decisions)
              } else {
                decisions = addDecision("LengthCheck", "false %.2f <= %.2f < %.2f".format(X1, length, X3))
                val categoryConfidence = category.get.confidence
                if (categoryConfidence < config.carConfidence) {
                  decisions = addDecision("CategoryConfCheck", "false conf=%d < %d".format(categoryConfidence, config.carConfidence))
                  CategoryResult(cat, Some(false), decisions)
                } else {
                  decisions = addDecision("CategoryConfCheck", "true conf=%d < %d".format(categoryConfidence, config.carConfidence))
                  CategoryResult(cat, Some(true), decisions)
                }
              }
            }
          }
          case VehicleCategory.CarTrailer ⇒
            if (record.entry.length.isEmpty) {
              decisions = addDecision("LengthCheck", "skipped length= None")
              CategoryResult(cat, Some(true), decisions)
            } else {
              val length = record.entry.length.map(_.value).getOrElse(0F)
              if (X4 <= length) {
                decisions = addDecision("LengthCheck", "true %.2f <= %.2f".format(X4, length))
                val categoryConfidence = category.get.confidence
                if (categoryConfidence >= config.trailerConfidence) {
                  decisions = addDecision("CategoryConfCheck", "true conf=%d < %d".format(categoryConfidence, config.trailerConfidence))
                  CategoryResult(cat, Some(true), decisions)
                } else {
                  decisions = addDecision("CategoryConfCheck", "false conf=%d < %d".format(categoryConfidence, config.trailerConfidence))
                  CategoryResult(cat, Some(false), decisions)
                }
              } else {
                decisions = addDecision("LengthCheck", "false %.2f <= %.2f".format(X4, length))
                CategoryResult(cat, Some(false), decisions)
              }
            }
          case VehicleCategory.Motor ⇒
            if (record.entry.length.isEmpty) {
              decisions = addDecision("LengthCheck", "skipped length= None")
              CategoryResult(cat, Some(true), decisions)
            } else {
              val length = record.entry.length.map(_.value).getOrElse(0F)
              if (X1 <= length && length < X2) {
                decisions = addDecision("LengthCheck", "true %.2f <= %.2f < %.2f".format(X1, length, X2))
                CategoryResult(cat, Some(true), decisions)
              } else {
                decisions = addDecision("LengthCheck", "false %.2f <= %.2f < %.2f".format(X1, length, X2))
                CategoryResult(cat, Some(false), decisions)
              }
            }
          case VehicleCategory.Truck ⇒ {
            if (record.speed > 130) {
              decisions = addDecision("SpeedCheck", "Speed %d too high (>130) for Truck category".format(record.speed))
              CategoryResult(cat, Some(false), decisions)
            } else {
              decisions = addDecision("SpeedCheck", "Speed %d OK for Truck category".format(record.speed))
              CategoryResult(cat, Some(true), decisions)
            }
          }
          case VehicleCategory.Large_Truck ⇒ {
            if (record.speed > 130) {
              decisions = addDecision("SpeedCheck", "Speed %d too high (>130) for Large Truck category".format(record.speed))
              CategoryResult(cat, Some(false), decisions)
            } else {
              decisions = addDecision("SpeedCheck", "Speed %d OK for Large Truck category".format(record.speed))
              CategoryResult(cat, Some(true), decisions)
            }
          }
          case VehicleCategory.TruckTrailer ⇒ {
            if (record.speed > 130) {
              decisions = addDecision("SpeedCheck", "Speed %d too high (>130) for TruckTrailer category".format(record.speed))
              CategoryResult(cat, Some(false), decisions)
            } else {
              decisions = addDecision("SpeedCheck", "Speed %d OK for TruckTrailer category".format(record.speed))
              CategoryResult(cat, Some(true), decisions)
            }
          }
          case VehicleCategory.SemiTrailer ⇒ {
            if (record.speed > 130) {
              decisions = addDecision("SpeedCheck", "Speed %d too high (>130) for TruckTrailer category".format(record.speed))
              CategoryResult(cat, Some(false), decisions)
            } else {
              decisions = addDecision("SpeedCheck", "Speed %d OK for TruckTrailer category".format(record.speed))
              CategoryResult(cat, Some(true), decisions)
            }
          }
          case other ⇒ CategoryResult(cat, Some(true), decisions)
        }
      }
    }
  }

  /**
   * Modified check if it is a person car.
   * Including the check TDC3 is empty and
   * 	TDC1 measurement is < 6 (inclusive TDC1 none)
   * @return
   */
  def isPersonCarForPClassification(config: ZkClassifyConfidence, decisions: DecisionTree, record: VehicleSpeedRecord): Boolean = {
    decisions.addDecision("isPersonCarForPClassification", "The length is reliable")
    val category = record.exit.flatMap(_.vehicleCategory)

    if (category.isEmpty || category.get.value.equals(VehicleCategory.Unknown)) {
      decisions.addDecision("isPersonCarForPClassification", "No measured category (TDC-3) is available [" + category.toString + "]")
      //TDC3 is empty
      val length = record.entry.length.map(_.value).getOrElse(0F)
      val X2 = config.maxCarLength
      val result = length < X2 //only personCar when size is below maxCarLength

      decisions.addDecision("isPersonCarForPClassification", "Category correction: %s length=%.2f maxCarLength=%.2f".format(result.toString, length, X2))
      result
    } else {
      //no changes when TDC3 isn't empty
      decisions.addDecision("isPersonCarForPClassification", "Measured category (TDC-3) is available [" + category.map(_.value).getOrElse("Unknown") + "]")
      isPersonCar(category.map(_.value).getOrElse(VehicleCategory.Unknown))
    }
  }
}