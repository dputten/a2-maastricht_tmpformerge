/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.classify.nl

import amqp.AMQPClassifierIface
import config.ClassifierConfigBuilder
import process.ProcessClassification

import java.util.{ TimeZone, Calendar, GregorianCalendar }

import akka.actor._
import akka.actor.SupervisorStrategy.Stop
import akka.util.duration._

import net.liftweb.json.Formats

import org.apache.curator.retry.RetryUntilElapsed
import org.apache.hadoop.hbase.client.HTable

import csc.akkautils._
import csc.curator.utils.{ Curator, CuratorActor }
import csc.hbase.utils._

import csc.sectioncontrol.messages._
import csc.sectioncontrol.rdwregistry.{ HBaseRdwRegistry, RdwRegistry }
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storagelayer.hbasetables._
import csc.sectioncontrol.storage.SystemCorridorCompletionStatus
import csc.akkautils.JobFailed
import csc.akkautils.JobComplete
import csc.akkautils.CreateJobActor
import akka.actor.AllForOneStrategy
import akka.actor.Terminated
import csc.akkautils.RunToken
import com.github.sstone.amqp.RabbitMQConnection
import org.apache.hadoop.conf.Configuration
import com.typesafe.config.Config

/**
 * Boot the classifier actors.
 *
 * @author Maarten Hazewinkel
 */
class Boot extends MultiSystemBoot {
  import Boot._

  def componentName = "SectionControl-Classify-NL"

  protected def moduleName = componentName
  override protected def retryPolicy = new RetryUntilElapsed(1000 * 60 * 60 * 24 * 3, 5000)
  override protected def formats(defaultFormats: Formats) = SerializerFormats.formats

  /**
   * Start a separate ActorSystem for each system found in zookeeper.
   */
  override def startupActors() {
    super.startupActors()
    val config = actorSystem.settings.config
    ClassifierConfigBuilder.createAmqpClassifyConfig(config).foreach(classifyCfg ⇒ {
      log.info("Creating classify MQ interface")
      val amqpConnectionCfg = ClassifierConfigBuilder.createAmqpConfig(config)

      val mqConnection = RabbitMQConnection.create(amqpConnectionCfg.actorName, amqpConnectionCfg.amqpUri, amqpConnectionCfg.reconnectDelay)(actorSystem)
      log.info("Amqp connection created")

      val curator = createCurator(system = actorSystem, formats = SerializerFormats.formats)
      actorSystem.actorOf(Props(new AMQPClassifierIface(
        curator = curator,
        mqConnection = mqConnection, hbaseConfig = hbaseConfig, config = classifyCfg)))
      log.info("classify MQ interface started")
    })
  }

  /**
   * Startup the job scheduler that will kick of the classifiers according to their schedules in zookeeper.
   *
   * The path where the scheduler looks for jobs is:
   * "/ctes/systems/_/jobs/classifyJobNL"
   *
   * For the future, to distribute classifiers across multiple servers, the job path can be made a parameter or
   * a setting in zookeeper that relates it to the current server name.
   * That will allow fine grained control over where things run.
   *
   * RR: we could have a mapping of system to server in zookeeper, all servers read this mapping,
   * see for which systems it must run what, or try to run as part of a group.
   * through leader selection it runs per system an actor system
   * the global system takes care of stopping, starting actor systems
   */
  def startupSystemActors(system: ActorSystem, systemId: String, curator: Curator, hbaseZkServers: String) {
    val configuration = actorSystem.settings.config

    RowKeyDistributorFactory.init(curator)
    startEventLogger[SystemEvent](systemId, curator, system)

    // The RunScheduledJobs actor is supervised by the actor system guardian
    system.actorOf(Props(new RunScheduledJobs(Paths.Systems.getSystemJobsPath(systemId) + "/classifyJob-nl",
      curator,
      startClassifierJob) with WaitForCompletion),
      "classifier-nl_JobScheduler")

    system.actorOf(Props(new ManageClassificationStatus(curator)), manageClassifyStatusName)

  }

  /**
   * Configuration should only started once
   */
  private lazy val hbaseConfig = {
    val config = actorSystem.settings.config
    val hbaseZkServerQuorum = if (config.hasPath(hbaseZkServerQuorumConfigPath)) {
      config.getString(hbaseZkServerQuorumConfigPath)
    } else {
      config.getString(zkServerQuorumConfigPath)
    }
    HBaseTableKeeper.createCachedConfig(hbaseZkServerQuorum)
  }

  protected def zkServerQuorumConfigPath = "sectioncontrol.zkServers"
  protected def hbaseZkServerQuorumConfigPath = "sectioncontrol.hbaseZkServers"

  protected def startClassifierJob(zkJobPath: String, runToken: Option[RunToken], runner: ActorRef, curator: Curator) {
    runner ! CreateJobActor(() ⇒ new ClassifierJob(curator, hbaseConfig, runToken, zkJobPath, getSystemId(zkJobPath)), StartClassifier)
  }
}

object Boot {
  val manageClassifyStatusName = "classifyCompletionManager"
  val manageClassifyStatusPath = "/user/" + manageClassifyStatusName

  private def getSystemId(jobPath: String) = {
    val index = Paths.Systems.getDefaultPath.split('/').length
    jobPath.split('/')(index)
  }
}

/**
 * Specific details for starting up the classifier jobs.
 * Classifier jobs always wait for completion before starting again by extending WaitForCompletionJob.
 */
class ClassifierJob(protected val curator: Curator, hbaseConfig: ⇒ Configuration, runToken: Option[RunToken], zkJobPath: String, systemId: String)
  extends CuratorActor with HBaseTableKeeper {

  var runner: ActorRef = _

  protected def receive = {
    case StartClassifier ⇒
      createClassifierForSystem() match {
        case Some(classifierActor) ⇒
          val config = loadClassifierJobConfig()
          log.info("ClassifierJob config: {}", config)
          runner = sender
          runClassifier(classifierActor, config, runToken.get)
        case None ⇒
          log.error("could not create classifier for job {}", zkJobPath)
          sender ! JobFailed
      }
  }

  override def postStop() {
    try {
      closeTables()
    } finally {
      super.postStop()
    }
  }

  /**
   * Actually run the classification job. Creates a temporary actor that monitors the classifier and handles both
   * completion and failure.
   */
  private def runClassifier(classifier: ActorRef, config: ClassifierJobConfig, runToken: RunToken) {
    val name = classifier.path.name

    // This actor lives for the duration of executing a single classification run.
    // It monitors but does not supervise the classifier actor.
    // It is supervised by the RunScheduledJobs actor for the classifier jobs.
    context.actorOf(Props(new Actor {
      protected def receive = {
        case "start" ⇒
          classifyCompletion ! GetClassificationCompleteUntil(systemId)

        case ClassificationIsCompleteUntil(completeTime, sysId) ⇒
          context.watch(classifier)
          val matcherStatus = curator.get[SystemCorridorCompletionStatus](Paths.Systems.getMatchCompletionStatusPath(systemId)).
            getOrElse(SystemCorridorCompletionStatus(Map()))
          val matcherEndtimes = matcherStatus.corridors.filter { corridorStatus ⇒
            curator.exists(Paths.Corridors.getRootPath(systemId, corridorStatus._1))
          }.values.
            filter(_.lastUpdateTime >= runToken.scheduledTime - config.matcherLookbackWindow).
            map(_.completeUntil)
          val matchersEndtime = if (matcherEndtimes.isEmpty) 0 else matcherEndtimes.min

          val (dayStart, dayEnd) = {
            val (runDayStart, runDayEnd) = calculateDayPeriod(runToken.scheduledTime, runToken.timezone)
            if (completeTime < runDayStart) {
              calculateDayPeriod(if (completeTime != 0) completeTime else runToken.startTime - 24.hours.toMillis, runToken.timezone)
            } else {
              (runDayStart, runDayEnd)
            }
          }

          val classifyFrom = math.max(dayStart, completeTime)
          val classifyUntil = math.min(dayEnd, matchersEndtime)
          val classificationMessage = ClassifyMessage(StatsDuration(classifyFrom, TimeZone.getTimeZone(runToken.timezone)),
            classifyFrom, classifyUntil)
          log.info("starting classification for {} with message {}", name, classificationMessage)
          classifier ! classificationMessage
          classifyCompletion ! ClassificationStarted(systemId, runToken.startTime)

        case cr @ ClassifyResult(ClassifyMessage(_, beginTime, endTime), _) ⇒
          log.info("completed classification for {} until {}. Result = {}", name, endTime, cr)
          classifyCompletion ! ClassificationFinished(systemId, System.currentTimeMillis(), endTime)
          runner ! JobComplete(Some(endTime))

        case Terminated(`classifier`) ⇒
          log.info("classifier actor {} died", name)
          runner ! JobFailed
      }
    }), name + "_runner") ! "start"
  }

  override def supervisorStrategy() = AllForOneStrategy() {
    case e: Exception ⇒
      log.error(e, "Classifier instance failed. Scheduling rerun for: {}", zkJobPath)
      runner ! JobFailed(true)
      Stop
  }

  //TODO FlitsCloud: Improve date-time handling across the system. Joda time? Scala time?
  private def calculateDayPeriod(time: Long, timezone: String): (Long, Long) = {
    val cal = new GregorianCalendar(TimeZone.getTimeZone(timezone))
    cal.setTimeInMillis(time)
    cal.set(Calendar.HOUR_OF_DAY, 0)
    cal.set(Calendar.MINUTE, 0)
    cal.set(Calendar.SECOND, 0)
    cal.set(Calendar.MILLISECOND, 0)
    val start = cal.getTimeInMillis
    cal.set(Calendar.HOUR_OF_DAY, 24)
    (start, cal.getTimeInMillis)
  }

  /**
   * Load the classifier config from zookeeper, or fall back to a default config.
   */
  private def loadClassifierJobConfig(): ClassifierJobConfig = {
    curator.get[ClassifierJobConfig](zkJobPath + "/config") match {
      case Some(mc) ⇒ mc
      case None     ⇒ ClassifierJobConfig.default
    }
  }

  val defaultCountry = ZkClassifyCountry()

  /**
   * Load configuration data from zookeeper and instantiate the classifier actor.
   */
  private def createClassifierForSystem(): Option[ActorRef] = {
    ClassifierConfigBuilder.createConfig(curator, systemId).map(config ⇒ {
      context.actorOf(Props(makeClassifier(config)), "classifier_" + systemId)
    })
  }

  private def retrieveOrCreateDefaultCorrectTDC3Category: ZkCorrectTDC3CategoryConfig = {
    val path = Paths.Configuration.getCorrectTDC3Category
    curator.get[ZkCorrectTDC3CategoryConfig](path) getOrElse ZkCorrectTDC3CategoryConfig()
  }

  private def retrieveWrongReadLicense: List[ZkWrongReadDutchLicensePlate] = {
    val path = Paths.Configuration.getWrongReadDutchLicensePlatePath
    curator.get[List[ZkWrongReadDutchLicensePlate]](path) getOrElse List()
  }

  /**
   * build the WheelBaseInfo object from the stored preferences
   */
  private def buildWheelBaseInfo(wheelBase: List[ZkWheelBase]): WheelBaseInfo = {
    import ZkWheelbaseType._
    val wbMap = wheelBase.map { case ZkWheelBase(key, value) ⇒ key -> value.toFloat }.toMap

    def wbValue(key: ZkWheelbaseType.Value): Float =
      wbMap.get(key).flatMap(f ⇒ if (f == 0.0f) None else Some(f)).getOrElse(1.0f)

    WheelBaseInfo(p = Seq(2 -> wbValue(P2M),
      3 -> wbValue(P2M3M),
      4 -> wbValue(P3M4M),
      5 -> wbValue(P4M5M),
      999 -> wbValue(P5M)),
      lb = Seq(2 -> wbValue(LBB2M),
        3 -> wbValue(LBB2M3M),
        4 -> wbValue(LBB3M4M),
        5 -> wbValue(LBB4M5M),
        999 -> wbValue(LBB5M)))
  }

  /**
   *
   * Connect to all the hbase tables needed by the matcher and create the matcher instance.
   */
  private def makeClassifier(corridorConfig: ClassifierConfig): ProcessClassification = {
    val speedRecordReader = SpeedRecordTable.makeReader(hbaseConfig, this, systemId)
    val classifiedRecordWriter = VehicleClassifiedTable.makeWriter(hbaseConfig, this)
    val registry: RdwRegistry = makeHBaseRdwRegistry()
    val classifiedDecision = DecisionResultTable.makeWriter(hbaseConfig, this)
    log.info("ProcessClassification config: {}", corridorConfig)

    new ProcessClassification(registry, corridorConfig, speedRecordReader, classifiedRecordWriter, classifiedDecision)
  }

  private def makeHBaseRdwRegistry(): RdwRegistry = {
    HBaseAdminTool.ensureTableExists(hbaseConfig, HBaseRdwRegistry.TABLE, HBaseRdwRegistry.FAMILY)
    val table = new HTable(hbaseConfig, HBaseRdwRegistry.TABLE)
    addTable(table)
    new HBaseRdwRegistry(table)
  }

  private lazy val classifyCompletion = context.actorFor(Boot.manageClassifyStatusPath)
}

case object StartClassifier

/**
 * Configuration of a matcher job
 */
case class ClassifierJobConfig(matcherLookbackWindow: Long,
                               rdwTableName: String)

object ClassifierJobConfig {
  /**
   * Default matcher configuration if no specific config is stored in zookeeper.
   */
  val default = ClassifierJobConfig(12.hours.toMillis, HBaseRdwRegistry.TABLE)
}
