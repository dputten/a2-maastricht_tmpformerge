/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.classify.nl.category

import csc.sectioncontrol.storage.ZkClassifyConfidence
import csc.sectioncontrol.messages._
import csc.sectioncontrol.classify.nl.Decision
import csc.sectioncontrol.classify.nl.trace.DecisionTree

/**
 * Class responsible for the scanned category of the PIR measurements and its reliability
 * @param record the record
 * @param config the configuration
 */
class ScannedCategory(record: VehicleSpeedRecord, config: ZkClassifyConfidence) extends ProcessCategory {

  private val categoryResult = getScannedCategory(record)
  private val decisions = categoryResult.decisions

  def getDecisions(): Seq[Decision] = {
    decisions
  }
  /**
   * Is the category a personcar
   * @return
   */
  def isPersonCar(): Boolean = isPersonCar(categoryResult.category)

  def isPersonCar(decision: DecisionTree): Boolean = {
    val result = isPersonCar(categoryResult.category)
    val msg = "%s category=%s".format(result, categoryResult.category.toString)
    decision.addDecision("isPersonCar", msg)
    result
  }

  /**
   * Is the category a LargeTruck
   * @return
   */
  def isLargeTruck(): Boolean = isLargeTruck(categoryResult.category)

  /**
   * is the confidence of the exit category reliable
   * @return
   */
  def isConfidenceExitReliable(): Boolean = {
    val limit = config.confidenceLimit

    val confidence = for {
      exit ← record.exit
      category ← exit.category
    } yield (category.confidence)

    confidence.getOrElse(0) >= limit
    //record.entry.category.map(_.confidence).getOrElse(0) >= limit
  }

  /**
   * The overridden equals method.
   * Is equal when compared with ScannedCategory and scannedCategory, scanReliable are equal
   * Or when compared with VehicleCategory and scannedCategory is equal to the VehicleCategory
   * @param p1
   * @return
   */
  override def equals(p1: Any) = {
    p1 match {
      case scannedCat: ScannedCategory ⇒ (getCategory().equals(scannedCat.getCategory())) && (isReliable() == scannedCat.isReliable())
      case cat: VehicleCategory.Value  ⇒ getCategory().equals(cat)
      case other                       ⇒ super.equals(p1)
    }
  }

  /**
   * Is the scanned category reliable
   * @return
   */
  def isReliable(): Option[Boolean] = categoryResult.isReliable

  /**
   * get the scanned category
   * @return
   */
  def getCategory() = categoryResult.category

  /**
   * is the scanned category the same as the rdwCategory
   * @param rdwSizeCategory
   * @return
   */
  def isRDWSameAsScanned(rdwSizeCategory: Option[VehicleCategory.Value]): Boolean = {
    if (rdwSizeCategory.isEmpty) {
      false
    } else {
      val rdw = rdwSizeCategory.get
      if (isPersonCar(categoryResult.category) && isPersonCar(rdw)) {
        true
      } else if (isLargeTruck(categoryResult.category) && isLargeTruck(rdw)) {
        true
      } else {
        false
      }
    }
  }

  /**
   * Modified check if it is a person car.
   * @return boolean indicating if it is a person car
   */
  def isPersonCarForPClassification(decisions: DecisionTree): Boolean = {
    config.processCategory match {
      case "TDC3-TDC1" ⇒ ProcessExitTDC3Length.isPersonCarForPClassification(config, decisions, record)
      case other ⇒ {
        decisions.addDecision("isPersonCarForPClassification", "The length is NOT reliable")
        //no changes when the reliableLength is turned off
        isPersonCar(decisions)
      }
    }
  }

  private def getScannedCategory(record: VehicleSpeedRecord): CategoryResult = {
    config.processCategory match {
      case "TDC3-TDC1"   ⇒ ProcessExitTDC3Length.getScannedCategory(config, record)
      case "TDC3"        ⇒ ProcessExitTDC3.getScannedCategory(config, record)
      case "TDC3-JOINED" ⇒ ProcessJoinedExit.getScannedCategory(config, record)
      case "TDC3-TDC3"   ⇒ ProcessEntryExitTDC3.getScannedCategory(config, record)
      case other         ⇒ ProcessExitTDC3.getScannedCategory(config, record)
    }
  }

}