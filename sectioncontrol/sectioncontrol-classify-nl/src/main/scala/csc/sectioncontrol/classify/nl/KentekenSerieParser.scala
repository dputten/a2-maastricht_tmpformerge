package csc.sectioncontrol.classify.nl
import java.io.InputStream
import csc.akkautils.DirectLogging

case class KentekenserieRegel(sideCode: String, kentekenSerie1_2: String, soortKentekenSerie: String, onderverdelingSoortKentekenSerie: String)

case class Kentekenserie(kentekenserieRegels: List[KentekenserieRegel]) extends DirectLogging {

  /**
   * Returns a RDW category
   *
   * 1) Find the KentekenserieRegel with the significant characters from the plate.
   * 2) With the properties soortKentekenSerie and onderverdelingSoortKentekenSerie find the correct category within the RdwDambordCategory
   * @param significantCharacters - the significant characters of the plate
   */
  def getCategory(sideCode: String, significantCharacters: String): Option[RdwDambordCategory.Category] = {
    require(significantCharacters.length == 2)

    val kentekenserieRegel = kentekenserieRegels.find(kr ⇒ kr.sideCode == sideCode && kr.kentekenSerie1_2 == significantCharacters)
    log.debug("kentekenserieRegel in getCategory:" + kentekenserieRegel.toString)

    kentekenserieRegel match {
      case Some(_kentekenserieRegel) ⇒
        Some(RdwDambordCategory.parse(_kentekenserieRegel.soortKentekenSerie, _kentekenserieRegel.onderverdelingSoortKentekenSerie))
      case _ ⇒ Some(RdwDambordCategory.ONG)
    }
  }
}

object FileToLinesReader extends DirectLogging {

  /**
   * Parse the whole input as one table
   *
   * @param input - the kentekenreeks file as a stream
   * @return a list of all rows from the file
   */
  def read(input: InputStream): List[String] = {
    require(input != null, "Input cannot be null for FileToLinesReader.")
    var lines: List[String] = Nil
    try {
      val bufferedSource = scala.io.Source.fromInputStream(input)
      lines = bufferedSource.getLines().toList
      bufferedSource.close()
    } catch {
      case e: Throwable ⇒
        log.error(e.getMessage)
        throw new Exception("FileToLinesReader: file could not be read.")
    }
    lines
  }
}

object KentekenserieParser {

  /**
   * Parse the lines into a list of Kentekenserie objects
   * @param lines - lines from the file
   * @return Dambord table
   */
  def parse(lines: List[String]): Kentekenserie =
    {
      Kentekenserie(lines.map(line ⇒ {
        val lineItems = line.split(";")
        KentekenserieRegel(lineItems(0), lineItems(1), lineItems(4), lineItems(5))
      }))
    }
}

