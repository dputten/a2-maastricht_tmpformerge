/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.classify.nl.category

import csc.sectioncontrol.messages.VehicleCategory
import csc.sectioncontrol.classify.nl.Decision

case class CategoryResult(category: VehicleCategory.Value, isReliable: Option[Boolean], decisions: Seq[Decision])

trait ProcessCategory {

  def addDecision(step: String, result: String, details: Seq[Decision] = Seq())(implicit decisions: Seq[Decision]): Seq[Decision] = {
    decisions :+ new Decision(step, result, details)
  }

  /**
   * There are several scannedCategory (TDC-3) classes that is the
   * same as a person car
   *
   * @param category
   * @return
   */
  def isPersonCar(category: VehicleCategory.Value): Boolean =
    category.equals(VehicleCategory.Pers) ||
      category.equals(VehicleCategory.Car) ||
      category.equals(VehicleCategory.Van) ||
      category.equals(VehicleCategory.Motor)

  /**
   * There are several scannedCategory (TDC-3) classes that is the
   * same as a Large truck.
   *
   * @param category
   * @return
   */
  def isLargeTruck(category: VehicleCategory.Value): Boolean =
    category.equals(VehicleCategory.TruckTrailer) ||
      category.equals(VehicleCategory.SemiTrailer) ||
      category.equals(VehicleCategory.Large_Truck)

}