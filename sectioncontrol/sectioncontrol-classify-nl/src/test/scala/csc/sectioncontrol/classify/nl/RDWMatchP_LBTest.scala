/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.classify.nl

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.rdwregistry.RdwData
import csc.sectioncontrol.messages.{ VehicleCode, VehicleCategory, ProcessingIndicator }
import csc.sectioncontrol.storage.ZkClassifyConfidence

class RDWMatchP_LBTest extends WordSpec with MustMatchers {
  val writer = new MockClassifyDecisionWriter()
  val rdw = RdwData("GZ123C", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
    wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
  val config = ClassifierConfig(0.4f, 0.5f)
  val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)

  "foreigner treaty countries Car" must {
    "classified to MV when Car>0" in {
      val record = CreateMatches.createTDC3_10_5(country = Option("DE"))
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.MV)
      result.indicator must be(ProcessingIndicator.Automatic)
    }
    "classified to MV when Car=0 length none" in {
      val record = CreateMatches.createTDC3_0_TDC1_0_8(country = Option("DE"))
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.MV)
      result.indicator must be(ProcessingIndicator.Automatic)
    }
    "classified to MV when Car=0 length < 6" in {
      val record = CreateMatches.createTDC3_0_TDC1_OK_7(country = Option("DE"))
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.MV)
      result.indicator must be(ProcessingIndicator.Automatic)
    }
    "classified to MV hand when Car=0 length > 6" in {
      val record = CreateMatches.createTDC3_0_TDC1_fail_6(country = Option("DE"))
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.MV)
      result.indicator must be(ProcessingIndicator.Manual)
      result.manualAccordingToSpec must be(false)
    }
    "classified to MV when empty" in {
      val record = CreateMatches.createTDC3Empty_2(country = Option("DE"))
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.MV)
      result.indicator must be(ProcessingIndicator.Manual)
    }
  }
  "Dutch P Car" must {
    "classified to PA when Car>0" in {
      val record = CreateMatches.createTDC3_10_5()
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.PA)
      result.indicator must be(ProcessingIndicator.Automatic)
    }
    "classified to PA when Car=0 length none" in {
      val record = CreateMatches.createTDC3_0_TDC1_0_8()
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.PA)
      result.indicator must be(ProcessingIndicator.Automatic)
    }
    "classified to PS when Car=0 length < 6" in {
      val record = CreateMatches.createTDC3_0_TDC1_OK_7()
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.PA)
      result.indicator must be(ProcessingIndicator.Automatic)
    }
    "classified to PA hand when Car=0 length > 6" in {
      val record = CreateMatches.createTDC3_0_TDC1_fail_6()
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.PA)
      result.indicator must be(ProcessingIndicator.Manual)
    }
    "classified to PA when empty" in {
      val record = CreateMatches.createTDC3Empty_2()
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.PA)
      result.indicator must be(ProcessingIndicator.Manual)
    }
  }
  "Dutch CA Car" must {
    "classified to CA when Car>0" in {
      val record = CreateMatches.createTDC3_10_5(plate = CreateMatches.PLATE_LB)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.CA)
      result.indicator must be(ProcessingIndicator.Automatic)
    }
    "classified to CA when Car=0 length none" in {
      val record = CreateMatches.createTDC3_0_TDC1_0_8(plate = CreateMatches.PLATE_LB)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.CA)
      result.indicator must be(ProcessingIndicator.Automatic)
    }
    "classified to CA when Car=0 length < 6" in {
      val record = CreateMatches.createTDC3_0_TDC1_OK_7(plate = CreateMatches.PLATE_LB)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.CA)
      result.indicator must be(ProcessingIndicator.Automatic)
    }
    "classified to CA hand when Car=0 length > 6" in {
      val record = CreateMatches.createTDC3_0_TDC1_fail_6(plate = CreateMatches.PLATE_LB)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.CA)
      result.indicator must be(ProcessingIndicator.Manual)
    }
    "classified to CA when empty" in {
      val record = CreateMatches.createTDC3Empty_2(plate = CreateMatches.PLATE_LB)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.CA)
      result.indicator must be(ProcessingIndicator.Manual)
    }
  }

  "foreigner treaty countries VAN" must {
    "classified to MV when Van>0" in {
      val record = CreateMatches.createTDC3_10_5(country = Option("DE"), category = VehicleCategory.Van)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.MV)
      result.indicator must be(ProcessingIndicator.Automatic)
    }
    "classified to MV when Van=0 length none" in {
      val record = CreateMatches.createTDC3_0_TDC1_0_8(country = Option("DE"), category = VehicleCategory.Van)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.MV)
      result.indicator must be(ProcessingIndicator.Automatic)
      result.manualAccordingToSpec must be(false)
    }
    "classified to MV when Van=0 length < 6" in {
      val record = CreateMatches.createTDC3_0_TDC1_OK_7(country = Option("DE"), category = VehicleCategory.Van)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.MV)
      result.indicator must be(ProcessingIndicator.Automatic)
    }
    "classified to MV hand when Van=0 length > 6" in {
      val record = CreateMatches.createTDC3_0_TDC1_fail_6(country = Option("DE"), category = VehicleCategory.Van)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.MV)
      result.indicator must be(ProcessingIndicator.Manual)
      result.manualAccordingToSpec must be(false)
    }
  }
  "Dutch P Van" must {
    "classified to PA when Van>0" in {
      val record = CreateMatches.createTDC3_10_5(category = VehicleCategory.Van)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.PA)
      result.indicator must be(ProcessingIndicator.Automatic)
    }
    "classified to PA when Van=0 length none" in {
      val record = CreateMatches.createTDC3_0_TDC1_0_8(category = VehicleCategory.Van)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.PA)
      result.indicator must be(ProcessingIndicator.Automatic)
    }
    "classified to PS when Van=0 length < 6" in {
      val record = CreateMatches.createTDC3_0_TDC1_OK_7(category = VehicleCategory.Van)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.PA)
      result.indicator must be(ProcessingIndicator.Automatic)
    }
    "classified to PA hand when Van=0 length > 6" in {
      val record = CreateMatches.createTDC3_0_TDC1_fail_6(category = VehicleCategory.Van)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.PA)
      result.indicator must be(ProcessingIndicator.Manual)
    }
  }
  "Dutch CA Van" must {
    "classified to PA when Van>0" in {
      val record = CreateMatches.createTDC3_10_5(plate = CreateMatches.PLATE_LB, category = VehicleCategory.Van)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.CA)
      result.indicator must be(ProcessingIndicator.Automatic)
    }
    "classified to PA when Van=0 length none" in {
      val record = CreateMatches.createTDC3_0_TDC1_0_8(plate = CreateMatches.PLATE_LB, category = VehicleCategory.Van)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.CA)
      result.indicator must be(ProcessingIndicator.Automatic)
    }
    "classified to PS when Van=0 length < 6" in {
      val record = CreateMatches.createTDC3_0_TDC1_OK_7(plate = CreateMatches.PLATE_LB, category = VehicleCategory.Van)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.CA)
      result.indicator must be(ProcessingIndicator.Automatic)
    }
    "classified to PA hand when Van=0 length > 6" in {
      val record = CreateMatches.createTDC3_0_TDC1_fail_6(plate = CreateMatches.PLATE_LB, category = VehicleCategory.Van)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.CA)
      result.indicator must be(ProcessingIndicator.Manual)
    }
  }

  "foreigner treaty countries CarTrailer" must {
    "classified to VA when CarTrailer>0" in {
      val record = CreateMatches.createTDC3_10_5(country = Option("DE"), category = VehicleCategory.CarTrailer)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.VA)
      result.indicator must be(ProcessingIndicator.Manual)
      result.manualAccordingToSpec must be(true)
    }
    "classified to MV when CarTrailer=0 length none" in {
      val record = CreateMatches.createTDC3_0_TDC1_0_8(country = Option("DE"), category = VehicleCategory.CarTrailer)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.VA)
      result.indicator must be(ProcessingIndicator.Manual)
      result.manualAccordingToSpec must be(true)
    }
    "classified to MV when CarTrailer=0 length < 6" in {
      val record = CreateMatches.createTDC3_0_TDC1_OK_7(country = Option("DE"), category = VehicleCategory.CarTrailer)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.VA)
      result.indicator must be(ProcessingIndicator.Manual)
      result.manualAccordingToSpec must be(true)
    }
    "classified to MV hand when CarTrailer=0 length > 6" in {
      val record = CreateMatches.createTDC3_0_TDC1_fail_6(country = Option("DE"), category = VehicleCategory.CarTrailer)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.VA)
      result.indicator must be(ProcessingIndicator.Manual)
      result.manualAccordingToSpec must be(true)
    }
  }
  "Dutch P CarTrailer" must {
    "classified to PA when CarTrailer>0" in {
      val record = CreateMatches.createTDC3_10_5(category = VehicleCategory.CarTrailer)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.AS)
      result.indicator must be(ProcessingIndicator.Manual)
      result.manualAccordingToSpec must be(false)
    }
    "classified to PA when CarTrailer=0 length none" in {
      val record = CreateMatches.createTDC3_0_TDC1_0_8(category = VehicleCategory.CarTrailer)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.AS)
      result.indicator must be(ProcessingIndicator.Manual)
      result.manualAccordingToSpec must be(false)
    }
    "classified to PS when CarTrailer=0 length < 6" in {
      val record = CreateMatches.createTDC3_0_TDC1_OK_7(category = VehicleCategory.CarTrailer)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.AS)
      result.indicator must be(ProcessingIndicator.Manual)
      result.manualAccordingToSpec must be(false)
    }
    "classified to PA hand when CarTrailer=0 length > 6" in {
      val record = CreateMatches.createTDC3_0_TDC1_fail_6(category = VehicleCategory.CarTrailer)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.AS)
      result.indicator must be(ProcessingIndicator.Manual)
      result.manualAccordingToSpec must be(false)
    }
  }
  "Dutch CA CarTrailer" must {
    "classified to PA when CarTrailer>0" in {
      val record = CreateMatches.createTDC3_10_5(plate = CreateMatches.PLATE_LB, category = VehicleCategory.CarTrailer)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.AS)
      result.indicator must be(ProcessingIndicator.Manual)
      result.manualAccordingToSpec must be(false)
    }
    "classified to PA when CarTrailer=0 length none" in {
      val record = CreateMatches.createTDC3_0_TDC1_0_8(plate = CreateMatches.PLATE_LB, category = VehicleCategory.CarTrailer)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.AS)
      result.indicator must be(ProcessingIndicator.Manual)
      result.manualAccordingToSpec must be(false)
    }
    "classified to PS when CarTrailer=0 length < 6" in {
      val record = CreateMatches.createTDC3_0_TDC1_OK_7(plate = CreateMatches.PLATE_LB, category = VehicleCategory.CarTrailer)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.AS)
      result.indicator must be(ProcessingIndicator.Manual)
      result.manualAccordingToSpec must be(false)
    }
    "classified to PA hand when CarTrailer=0 length > 6" in {
      val record = CreateMatches.createTDC3_0_TDC1_fail_6(plate = CreateMatches.PLATE_LB, category = VehicleCategory.CarTrailer)
      val result = classifier.classify(record)
      result.code.get must be(VehicleCode.AS)
      result.indicator must be(ProcessingIndicator.Manual)
      result.manualAccordingToSpec must be(false)
    }
  }
}