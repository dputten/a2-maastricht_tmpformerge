/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.classify.nl.preprocess

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.akkautils.DirectLogging
import csc.sectioncontrol.classify.nl.{ MockRdwRegistry, ClassifierConfig }
import csc.sectioncontrol.rdwregistry.RdwData
import csc.sectioncontrol.messages.{ ValueWithConfidence, VehicleCategory }
import akka.util.duration._

class TDC3ProcesLaneTest extends WordSpec with MustMatchers with DirectLogging {
  import CreateTestData._

  val config = ClassifierConfig(0.4f, 0.5f)
  val rdw = new MockRdwRegistry(RdwData("GZ123C", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
    wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1"))
  val processor = new PreProcessTDC3Category(rdw, config, log)

  val startTime = 1380585600000L

  "preProcessLane for exit" must {
    val exitCheck = new CheckExitCategory
    "return emptry list when 0 records" in {
      val updateList = processor.preProcessLane(exitCheck, Seq())
      updateList.size must be(0)
    }
    "return same list when 1 record" in {
      val entry1 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit1 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("SJ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord1 = createSpeedRecord(entry1, exit1)

      val updateList = processor.preProcessLane(exitCheck, Seq(speedRecord1))
      updateList.size must be(1)
      updateList.head must be(speedRecord1)
    }
    "return same list when 2 records" in {
      val entry1 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit1 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("SJ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord1 = createSpeedRecord(entry1, exit1)
      val entry2 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("GZ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit2 = createVehicle(laneId = "lane1", timestamp = startTime + 1031, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = None, country = Some("NL"))
      val speedRecord2 = createSpeedRecord(entry2, exit2)

      val updateList = processor.preProcessLane(exitCheck, Seq(speedRecord1, speedRecord2))
      updateList.size must be(2)
      updateList.head must be(speedRecord1)
      updateList.last must be(speedRecord2)
    }
    "return same list when 2 records not in order" in {
      val entry1 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit1 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("SJ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord1 = createSpeedRecord(entry1, exit1)
      val entry2 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("GZ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit2 = createVehicle(laneId = "lane1", timestamp = startTime + 1031, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = None, country = Some("NL"))
      val speedRecord2 = createSpeedRecord(entry2, exit2)

      val updateList = processor.preProcessLane(exitCheck, Seq(speedRecord2, speedRecord1))
      updateList.size must be(2)
      updateList.head must be(speedRecord1)
      updateList.last must be(speedRecord2)
    }
    "return updated records when criteria apply with 2 records" in {
      val entry1 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit1 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("SJ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord1 = createSpeedRecord(entry1, exit1)
      val entry2 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("GZ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit2 = createVehicle(laneId = "lane1", timestamp = startTime + 1000, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = None, country = Some("NL"))
      val speedRecord2 = createSpeedRecord(entry2, exit2)

      val updateList = processor.preProcessLane(exitCheck, Seq(speedRecord1, speedRecord2))
      updateList.size must be(2)
      val expectRecord1 = createSpeedRecord(entry1, exit1.copy(category = Some(new ValueWithConfidence[String]("Car", 90))))
      updateList.head must be(expectRecord1)
      val expectRecord2 = createSpeedRecord(entry2, exit2.copy(category = Some(new ValueWithConfidence[String]("Car", 90))))
      updateList.last must be(expectRecord2)
    }
    "return updated records when criteria apply with 4 records" in {
      val startTimeEntry = startTime - 5.minutes.toMillis
      val entry0 = createVehicle(laneId = "lane1", timestamp = startTimeEntry, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit0 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("SJ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord0 = createSpeedRecord(entry0, exit0)
      val gap1 = 2000
      val entry1 = createVehicle(laneId = "lane1", timestamp = startTimeEntry + gap1, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit1 = createVehicle(laneId = "lane1", timestamp = startTime + gap1, license = Some("SJ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord1 = createSpeedRecord(entry1, exit1)
      val gap2 = gap1 + 1000
      val entry2 = createVehicle(laneId = "lane1", timestamp = startTimeEntry + gap2, license = Some("GZ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit2 = createVehicle(laneId = "lane1", timestamp = startTime + gap2, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = None, country = Some("NL"))
      val speedRecord2 = createSpeedRecord(entry2, exit2)
      val gap3 = gap2 + 2000
      val entry3 = createVehicle(laneId = "lane1", timestamp = startTimeEntry + gap3, license = Some("GZ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit3 = createVehicle(laneId = "lane1", timestamp = startTime + gap3, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = None, country = Some("NL"))
      val speedRecord3 = createSpeedRecord(entry3, exit3)

      val updateList = processor.preProcessLane(exitCheck, Seq(speedRecord0, speedRecord1, speedRecord2, speedRecord3))
      updateList.size must be(4)
      updateList(0) must be(speedRecord0)
      val expectRecord1 = createSpeedRecord(entry1, exit1.copy(category = Some(new ValueWithConfidence[String]("Car", 90))))
      updateList(1) must be(expectRecord1)
      val expectRecord2 = createSpeedRecord(entry2, exit2.copy(category = Some(new ValueWithConfidence[String]("Car", 90))))
      updateList(2) must be(expectRecord2)
      updateList(3) must be(speedRecord3)
    }
  }
  "preProcessLane for entry" must {
    val entryCheck = new CheckEntryCategory
    "return emptry list when 0 records" in {
      val updateList = processor.preProcessLane(entryCheck, Seq())
      updateList.size must be(0)
    }
    "return same list when 1 record" in {
      val entry1 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit1 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("SJ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord1 = createSpeedRecord(entry1, exit1)

      val updateList = processor.preProcessLane(entryCheck, Seq(speedRecord1))
      updateList.size must be(1)
      updateList.head must be(speedRecord1)
    }
    "return same list when 2 records" in {
      val entry1 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit1 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("SJ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord1 = createSpeedRecord(entry1, exit1)
      val entry2 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis + 1031, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = Some(4.5F), country = Some("NL"))
      val exit2 = createVehicle(laneId = "lane1", timestamp = startTime + 1031, license = Some("GZ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord2 = createSpeedRecord(entry2, exit2)

      val updateList = processor.preProcessLane(entryCheck, Seq(speedRecord1, speedRecord2))
      updateList.size must be(2)
      updateList.head must be(speedRecord1)
      updateList.last must be(speedRecord2)
    }
    "return same list when 2 records not in order" in {
      val entry1 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit1 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("SJ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord1 = createSpeedRecord(entry1, exit1)
      val entry2 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis + 1031, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = Some(4.5F), country = Some("NL"))
      val exit2 = createVehicle(laneId = "lane1", timestamp = startTime + 1031, license = Some("GZ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord2 = createSpeedRecord(entry2, exit2)

      val updateList = processor.preProcessLane(entryCheck, Seq(speedRecord2, speedRecord1))
      updateList.size must be(2)
      updateList.head must be(speedRecord1)
      updateList.last must be(speedRecord2)
    }
    "return updated records when criteria apply with 2 records" in {
      val entry1 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit1 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("SJ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord1 = createSpeedRecord(entry1, exit1)
      val entry2 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = Some(4.5F), country = Some("NL"))
      val exit2 = createVehicle(laneId = "lane1", timestamp = startTime + 1000, license = Some("GZ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord2 = createSpeedRecord(entry2, exit2)

      val updateList = processor.preProcessLane(entryCheck, Seq(speedRecord1, speedRecord2))
      updateList.size must be(2)
      val expectRecord1 = createSpeedRecord(entry1.copy(category = Some(new ValueWithConfidence[String]("Car", 90))), exit1)
      updateList.head must be(expectRecord1)
      val expectRecord2 = createSpeedRecord(entry2.copy(category = Some(new ValueWithConfidence[String]("Car", 90))), exit2)
      updateList.last must be(expectRecord2)
    }
    "return updated records when criteria apply with 4 records" in {
      val startTimeEntry = startTime - 5.minutes.toMillis
      val entry0 = createVehicle(laneId = "lane1", timestamp = startTimeEntry, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit0 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("SJ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord0 = createSpeedRecord(entry0, exit0)
      val gap1 = 2000
      val entry1 = createVehicle(laneId = "lane1", timestamp = startTimeEntry + gap1, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit1 = createVehicle(laneId = "lane1", timestamp = startTime + gap1, license = Some("SJ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord1 = createSpeedRecord(entry1, exit1)
      val gap2 = gap1 + 1000
      val entry2 = createVehicle(laneId = "lane1", timestamp = startTimeEntry + gap2, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = Some(4.5F), country = Some("NL"))
      val exit2 = createVehicle(laneId = "lane1", timestamp = startTime + gap2, license = Some("GZ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord2 = createSpeedRecord(entry2, exit2)
      val gap3 = gap2 + 2000
      val entry3 = createVehicle(laneId = "lane1", timestamp = startTimeEntry + gap3, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = Some(4.5F), country = Some("NL"))
      val exit3 = createVehicle(laneId = "lane1", timestamp = startTime + gap3, license = Some("GZ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord3 = createSpeedRecord(entry3, exit3)

      val updateList = processor.preProcessLane(entryCheck, Seq(speedRecord0, speedRecord1, speedRecord2, speedRecord3))
      updateList.size must be(4)
      updateList(0) must be(speedRecord0)
      val expectRecord1 = createSpeedRecord(entry1.copy(category = Some(new ValueWithConfidence[String]("Car", 90))), exit1)
      updateList(1) must be(expectRecord1)
      val expectRecord2 = createSpeedRecord(entry2.copy(category = Some(new ValueWithConfidence[String]("Car", 90))), exit2)
      updateList(2) must be(expectRecord2)
      updateList(3) must be(speedRecord3)
    }
  }

}