/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.classify.nl

import csc.sectioncontrol.messages._
import csc.sectioncontrol.rdwregistry.RdwData
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import java.util.Date
import csc.sectioncontrol.messages.VehicleSpeedRecord
import csc.sectioncontrol.messages.ValueWithConfidence
import csc.sectioncontrol.messages.Lane
import csc.sectioncontrol.messages.VehicleMetadata
import scala.Some

class RDWDataTest extends WordSpec with MustMatchers {
  val writer = new MockClassifyDecisionWriter()

  "RDWData checks" must {
    val config = ClassifierConfig(0.4f, 0.5f)

    "detect correctly filled data in manual" in {
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(RdwData("KW9999", 25, 3, 2500, 3.0f, 2.0f, 1500, 150, 160, "LF")), config, writer)
      val passage = classifier.classify(getMatch("SJ123C", category = VehicleCategory.Unknown, length = Some(4.5F)))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(2)
      passage.code must be(Some(VehicleCode.PA))
      passage.alternativeClassification must be(None)
      passage.validLicensePlate must be(None)
      passage.licensePlate must be("SJ123C")
    }
    "detect correctly filled data in auto" in {
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(RdwData("KW9999", 25, 3, 2500, 3.0f, 2.0f, 1500, 150, 160, "LF")), config, writer)
      val passage = classifier.classify(getMatch("SJ123C", category = VehicleCategory.Car, length = Some(4.5F)))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.code must be(Some(VehicleCode.PA))
      passage.alternativeClassification must be(None)
      passage.validLicensePlate must be(None)
      passage.licensePlate must be("SJ123C")
    }
    "detect empty deviceCode" in {
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(RdwData("KW9999", 25, 0, 2500, 3.0f, 2.0f, 1500, 150, 160, "LF")), config, writer)
      val passage = classifier.classify(getMatch("SJ123C", category = VehicleCategory.Unknown, length = Some(4.5F)))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(2)
      passage.code must be(None)
      passage.alternativeClassification must be(Some(VehicleCode.PA))
      passage.validLicensePlate must be(None)
      passage.licensePlate must be("SJ123C")
    }
    "detect invalid deviceCode" in {
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(RdwData("KW9999", 25, 92, 2500, 3.0f, 2.0f, 1500, 150, 160, "LF")), config, writer)
      val passage = classifier.classify(getMatch("SJ123C", category = VehicleCategory.Unknown, length = Some(4.5F)))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(2)
      passage.code must be(None)
      passage.alternativeClassification must be(Some(VehicleCode.PA))
      passage.validLicensePlate must be(None)
      passage.licensePlate must be("SJ123C")
    }
    "detect empty maxMass" in {
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(RdwData("KW9999", 25, 3, 0, 3.0f, 2.0f, 1500, 150, 160, "LF")), config, writer)
      val passage = classifier.classify(getMatch("SJ123C", category = VehicleCategory.Unknown, length = Some(4.5F)))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(2)
      passage.code must be(None)
      passage.alternativeClassification must be(Some(VehicleCode.PA))
      passage.validLicensePlate must be(None)
      passage.licensePlate must be("SJ123C")
    }
    "detect empty eegCategory" in {
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(RdwData("KW9999", 25, 3, 2500, 3.0f, 2.0f, 1500, 150, 160, "  ")), config, writer)
      val passage = classifier.classify(getMatch("SJ123C", category = VehicleCategory.Unknown, length = Some(4.5F)))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(2)
      passage.code must be(None)
      passage.alternativeClassification must be(Some(VehicleCode.PA))
      passage.validLicensePlate must be(None)
      passage.licensePlate must be("SJ123C")
    }
    "detect not filled eegCategory" in {
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(RdwData("KW9999", 25, 3, 2500, 3.0f, 2.0f, 1500, 150, 160, "00")), config, writer)
      val passage = classifier.classify(getMatch("SJ123C", category = VehicleCategory.Unknown, length = Some(4.5F)))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(2)
      passage.code must be(None)
      passage.alternativeClassification must be(Some(VehicleCode.PA))
      passage.validLicensePlate must be(None)
      passage.licensePlate must be("SJ123C")
    }
    "detect illogical data wheelBase" in {
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(RdwData("KW9999", 25, 3, 2500, 0.99f, 2.0f, 1500, 150, 160, "00")), config, writer)
      val passage = classifier.classify(getMatch("SJ123C", category = VehicleCategory.Unknown, length = Some(4.5F)))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(2)
      passage.code must be(None)
      passage.alternativeClassification must be(Some(VehicleCode.PA))
      passage.validLicensePlate must be(None)
      passage.licensePlate must be("SJ123C")
    }
    "detect illogical data length > width" in {
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(RdwData("KW9999", 25, 3, 2500, 3.00f, 4.0f, 4500, 150, 160, "00")), config, writer)
      val passage = classifier.classify(getMatch("SJ123C", category = VehicleCategory.Unknown, length = Some(4.5F)))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(2)
      passage.code must be(None)
      passage.alternativeClassification must be(Some(VehicleCode.PA))
      passage.validLicensePlate must be(None)
      passage.licensePlate must be("SJ123C")
    }
  }

  def getMatch(plate: String, category: VehicleCategory.Value = VehicleCategory.Car, length: Option[Float] = None, country: Option[String] = Some("NL")): VehicleSpeedRecord = {
    val first = vregis("id1", 1000L, "g1", plate, None, length, country)
    val second = vregis("id2", 2000L, "g2", plate, Some(category), None, country)
    VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")
  }
  def vregis(id: String, timestamp: Long, gantry: String, license: String, category: Option[VehicleCategory.Value], length: Option[Float], country: Option[String]): VehicleMetadata = {
    val lane = Lane("23L", "foo", gantry, "route1", 25f, 37f)
    val lic = Some(ValueWithConfidence(license, 90))
    val cou = country match {
      case None    ⇒ None
      case Some(c) ⇒ Some(ValueWithConfidence(c, 95))
    }
    VehicleMetadata(lane, id, timestamp, Some(new Date(timestamp).toString), lic, None, length.map(new ValueWithConfidence_Float(_, 60)), category.map(cat ⇒ new ValueWithConfidence[String](cat.toString(), 90)), None, cou, Seq(), "", "", "")
  }

}