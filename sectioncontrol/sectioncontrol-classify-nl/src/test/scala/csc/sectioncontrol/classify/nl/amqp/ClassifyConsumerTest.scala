/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.classify.nl.amqp

import akka.testkit.{ TestActorRef, TestProbe, TestKit }
import akka.actor.{ Props, ActorSystem }
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers
import com.github.sstone.amqp.Amqp.{ Ack, Delivery }
import com.rabbitmq.client.{ AMQP, Envelope }
import akka.util.duration._
import csc.sectioncontrol.messages.VehicleSpeedRecord

class ClassifyConsumerTest extends TestKit(ActorSystem("ClassifyConsumerTest")) with WordSpec with MustMatchers with BeforeAndAfterAll {
  override def afterAll() {
    super.afterAll()
    system.shutdown()
  }
  "Consumer" must {
    "transfer Delivery to VehicleSpeedRecord" in {
      val output = TestProbe()
      val sender = TestProbe()
      val consumer = TestActorRef(Props(new ClassifyConsumer(output.ref)))
      val properties = new AMQP.BasicProperties.Builder()
      properties.contentType("application/json")
      properties.`type`("VehicleSpeedRecord")

      val deliveryMsg = Delivery(
        consumerTag = "tag",
        envelope = new Envelope(10L, true, "exc", "routingkey"),
        properties = properties.build(),
        body = """{"id":"A2-S1-0000-R1","entry":{"lane":{"laneId":"A2-E1-R1","name":"R1","gantry":"E1","system":"A2","sensorGPS_longitude":52.4,"sensorGPS_latitude":5.4},"eventId":"A2-E1-R1-00001","eventTimestamp":1438671100000,"license":{"value":"1234AB","confidence":80},"rawLicense":"1234AB","length":4.3,"category":{"value":"CAR","confidence":60},"speed":{"value":125,"confidence":50},"country":{"value":"NL","confidence":60},"images":[{"timestamp":1438671100000,"offset":0,"uri":"overview","imageType":"VehicleImageType$Overview","checksum":"ABCDEF"}],"NMICertificate":"PD1234","applChecksum":"1234ABC","serialNr":"SN1234","recognizedBy":"ARH"},"exit":{"lane":{"laneId":"A2-X1-R1","name":"R1","gantry":"X1","system":"A2","sensorGPS_longitude":52.4,"sensorGPS_latitude":5.4},"eventId":"A2-X1-R1-00001","eventTimestamp":1438671100000,"license":{"value":"1234AB","confidence":80},"rawLicense":"1234AB","length":4.3,"category":{"value":"CAR","confidence":60},"speed":{"value":125,"confidence":50},"country":{"value":"NL","confidence":60},"images":[{"timestamp":1438671100000,"offset":0,"uri":"overview","imageType":"VehicleImageType$Overview","checksum":"ABCDEF"}],"NMICertificate":"PD1234","applChecksum":"1234ABC","serialNr":"SN1234","recognizedBy":"ARH"},"corridorId":11,"speed":120,"measurementSHA":"1234ABCDFE"}""".getBytes)
      sender.send(consumer, deliveryMsg)
      sender.expectMsg(Ack(deliveryMsg.envelope.getDeliveryTag))
      val request = output.expectMsgType[VehicleSpeedRecord](10.millis)
      request.id must be("A2-S1-0000-R1")
    }
    "skip Delivery when failure in VehicleSpeedRecord" in {
      val output = TestProbe()
      val sender = TestProbe()
      val consumer = TestActorRef(Props(new ClassifyConsumer(output.ref)))
      val properties = new AMQP.BasicProperties.Builder()
      properties.contentType("application/json")
      properties.`type`("VehicleSpeedRecord")

      val deliveryMsg = Delivery(
        consumerTag = "tag",
        envelope = new Envelope(10L, true, "exc", "routingkey"),
        properties = properties.build(),
        body = """{"id":"A2-S1-0000-R1","eventId":"A2-E1-R1-00001","eventTimestamp":1438671100000,"license":{"value":"1234AB","confidence":80},"rawLicense":"1234AB","length":4.3,"category":{"value":"CAR","confidence":60},"speed":{"value":125,"confidence":50},"country":{"value":"NL","confidence":60},"images":[{"timestamp":1438671100000,"offset":0,"uri":"overview","imageType":"VehicleImageType$Overview","checksum":"ABCDEF"}],"NMICertificate":"PD1234","applChecksum":"1234ABC","serialNr":"SN1234","recognizedBy":"ARH"},"exit":{"lane":{"laneId":"A2-X1-R1","name":"R1","gantry":"X1","system":"A2","sensorGPS_longitude":52.4,"sensorGPS_latitude":5.4},"eventId":"A2-X1-R1-00001","eventTimestamp":1438671100000,"license":{"value":"1234AB","confidence":80},"rawLicense":"1234AB","length":4.3,"category":{"value":"CAR","confidence":60},"speed":{"value":125,"confidence":50},"country":{"value":"NL","confidence":60},"images":[{"timestamp":1438671100000,"offset":0,"uri":"overview","imageType":"VehicleImageType$Overview","checksum":"ABCDEF"}],"NMICertificate":"PD1234","applChecksum":"1234ABC","serialNr":"SN1234","recognizedBy":"ARH"},"corridorId":11,"speed":120,"measurementSHA":"1234ABCDFE"}""".getBytes)
      sender.send(consumer, deliveryMsg)
      sender.expectMsg(Ack(deliveryMsg.envelope.getDeliveryTag))
      output.expectNoMsg(10.millis)
    }
    "skip Delivery when unknown message" in {
      val output = TestProbe()
      val sender = TestProbe()
      val consumer = TestActorRef(Props(new ClassifyConsumer(output.ref)))
      val properties = new AMQP.BasicProperties.Builder()
      properties.contentType("application/json")
      properties.`type`("SystemEvent")

      val deliveryMsg = Delivery(
        consumerTag = "tag",
        envelope = new Envelope(10L, true, "exc", "routingkey"),
        properties = properties.build(),
        body = """{"id":"A2-S1-0000-R1","eventId":"A2-E1-R1-00001","eventTimestamp":1438671100000,"license":{"value":"1234AB","confidence":80},"rawLicense":"1234AB","length":4.3,"category":{"value":"CAR","confidence":60},"speed":{"value":125,"confidence":50},"country":{"value":"NL","confidence":60},"images":[{"timestamp":1438671100000,"offset":0,"uri":"overview","imageType":"VehicleImageType$Overview","checksum":"ABCDEF"}],"NMICertificate":"PD1234","applChecksum":"1234ABC","serialNr":"SN1234","recognizedBy":"ARH"},"exit":{"lane":{"laneId":"A2-X1-R1","name":"R1","gantry":"X1","system":"A2","sensorGPS_longitude":52.4,"sensorGPS_latitude":5.4},"eventId":"A2-X1-R1-00001","eventTimestamp":1438671100000,"license":{"value":"1234AB","confidence":80},"rawLicense":"1234AB","length":4.3,"category":{"value":"CAR","confidence":60},"speed":{"value":125,"confidence":50},"country":{"value":"NL","confidence":60},"images":[{"timestamp":1438671100000,"offset":0,"uri":"overview","imageType":"VehicleImageType$Overview","checksum":"ABCDEF"}],"NMICertificate":"PD1234","applChecksum":"1234ABC","serialNr":"SN1234","recognizedBy":"ARH"},"corridorId":11,"speed":120,"measurementSHA":"1234ABCDFE"}""".getBytes)
      sender.send(consumer, deliveryMsg)
      sender.expectMsg(Ack(deliveryMsg.envelope.getDeliveryTag))
      output.expectNoMsg(10.millis)
    }
  }
}
