/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.classify.nl.preprocess

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.classify.nl._
import csc.sectioncontrol.rdwregistry.RdwData
import csc.akkautils.DirectLogging
import csc.sectioncontrol.messages._
import akka.util.duration._
import csc.sectioncontrol.classify.nl.ClassifierConfig

class TDC3DoesRuleApplyTest extends WordSpec with MustMatchers with DirectLogging {
  import CreateTestData._
  val config = ClassifierConfig(0.4f, 0.5f)
  val rdw = new MockRdwRegistry(RdwData("GZ123C", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
    wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1"))
  val processor = new PreProcessTDC3Category(rdw, config, log)

  val startTime = 1380585600000L

  "DoesRuleApply for exit" must {
    val exitCheck = new CheckExitCategory
    "return true when all criteria's apply" in {
      val entry1 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit1 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("SJ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord1 = createSpeedRecord(entry1, exit1)
      val entry2 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("GZ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit2 = createVehicle(laneId = "lane1", timestamp = startTime + 1000, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = None, country = Some("NL"))
      val speedRecord2 = createSpeedRecord(entry2, exit2)
      processor.doesRuleApply(exitCheck, speedRecord1, speedRecord2) must be(true)
    }
    "return false when gap check failes" in {
      val entry1 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit1 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("SJ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord1 = createSpeedRecord(entry1, exit1)
      val entry2 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("GZ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit2 = createVehicle(laneId = "lane1", timestamp = startTime + 1031, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = None, country = Some("NL"))
      val speedRecord2 = createSpeedRecord(entry2, exit2)
      processor.doesRuleApply(exitCheck, speedRecord1, speedRecord2) must be(false)
    }
    "return false when second category check failes" in {
      val entry1 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit1 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("SJ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord1 = createSpeedRecord(entry1, exit1)
      val entry2 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("GZ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit2 = createVehicle(laneId = "lane1", timestamp = startTime + 1000, license = Some("GZ123C"), category = Some(VehicleCategory.Car), length = None, country = Some("NL"))
      val speedRecord2 = createSpeedRecord(entry2, exit2)
      processor.doesRuleApply(exitCheck, speedRecord1, speedRecord2) must be(false)
    }
    "return false when Country check failes" in {
      val entry1 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit1 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("SJ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord1 = createSpeedRecord(entry1, exit1)
      val entry2 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("GZ123C"), category = None, length = Some(4.5F), country = Some("DE"))
      val exit2 = createVehicle(laneId = "lane1", timestamp = startTime + 1000, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = None, country = Some("DE"))
      val speedRecord2 = createSpeedRecord(entry2, exit2)
      processor.doesRuleApply(exitCheck, speedRecord1, speedRecord2) must be(false)
    }
    "return false when dambord check failes" in {
      val localRdw = new MockRdwRegistry(RdwData("MB12CD", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1"))
      val localProcessor = new PreProcessTDC3Category(localRdw, config, log)

      val entry1 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit1 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("SJ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord1 = createSpeedRecord(entry1, exit1)
      val entry2 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("MB12CD"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit2 = createVehicle(laneId = "lane1", timestamp = startTime + 1000, license = Some("MB12CD"), category = Some(VehicleCategory.CarTrailer), length = None, country = Some("NL"))
      val speedRecord2 = createSpeedRecord(entry2, exit2)
      localProcessor.doesRuleApply(exitCheck, speedRecord1, speedRecord2) must be(false)
    }
    "return false when RDW check failes" in {
      val localProcessor = new PreProcessTDC3Category(new NoRdwRegistry(), config, log)

      val entry1 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit1 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("SJ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord1 = createSpeedRecord(entry1, exit1)
      val entry2 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("GZ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit2 = createVehicle(laneId = "lane1", timestamp = startTime + 1000, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = None, country = Some("NL"))
      val speedRecord2 = createSpeedRecord(entry2, exit2)
      localProcessor.doesRuleApply(exitCheck, speedRecord1, speedRecord2) must be(false)
    }
    "return false when first category check failes" in {
      val entry1 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit1 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("SJ123C"), category = Some(VehicleCategory.Car), length = None, country = Some("NL"))
      val speedRecord1 = createSpeedRecord(entry1, exit1)
      val entry2 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("GZ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit2 = createVehicle(laneId = "lane1", timestamp = startTime + 1000, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = None, country = Some("NL"))
      val speedRecord2 = createSpeedRecord(entry2, exit2)
      processor.doesRuleApply(exitCheck, speedRecord1, speedRecord2) must be(false)
    }
    "return false when license are equal" in {
      val entry1 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("GZ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit1 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("GZ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord1 = createSpeedRecord(entry1, exit1)
      val entry2 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("GZ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit2 = createVehicle(laneId = "lane1", timestamp = startTime + 1000, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = None, country = Some("NL"))
      val speedRecord2 = createSpeedRecord(entry2, exit2)
      processor.doesRuleApply(exitCheck, speedRecord1, speedRecord2) must be(false)
    }
  }
  "DoesRuleApply for entry" must {
    val entryCheck = new CheckEntryCategory
    "return true when all criteria's apply" in {
      val exit1 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val entry1 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("SJ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord1 = createSpeedRecord(entry1, exit1)
      val exit2 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("GZ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val entry2 = createVehicle(laneId = "lane1", timestamp = startTime + 1000, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = None, country = Some("NL"))
      val speedRecord2 = createSpeedRecord(entry2, exit2)
      processor.doesRuleApply(entryCheck, speedRecord1, speedRecord2) must be(true)
    }
    "return false when gap check failes" in {
      val exit1 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val entry1 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("SJ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord1 = createSpeedRecord(entry1, exit1)
      val exit2 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("GZ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val entry2 = createVehicle(laneId = "lane1", timestamp = startTime + 1031, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = None, country = Some("NL"))
      val speedRecord2 = createSpeedRecord(entry2, exit2)
      processor.doesRuleApply(entryCheck, speedRecord1, speedRecord2) must be(false)
    }
    "return false when second category check failes" in {
      val exit1 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val entry1 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("SJ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord1 = createSpeedRecord(entry1, exit1)
      val exit2 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("GZ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val entry2 = createVehicle(laneId = "lane1", timestamp = startTime + 1000, license = Some("GZ123C"), category = Some(VehicleCategory.Car), length = None, country = Some("NL"))
      val speedRecord2 = createSpeedRecord(entry2, exit2)
      processor.doesRuleApply(entryCheck, speedRecord1, speedRecord2) must be(false)
    }
    "return false when Country check failes" in {
      val exit1 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val entry1 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("SJ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord1 = createSpeedRecord(entry1, exit1)
      val exit2 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("GZ123C"), category = None, length = Some(4.5F), country = Some("DE"))
      val entry2 = createVehicle(laneId = "lane1", timestamp = startTime + 1000, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = None, country = Some("DE"))
      val speedRecord2 = createSpeedRecord(entry2, exit2)
      processor.doesRuleApply(entryCheck, speedRecord1, speedRecord2) must be(false)
    }
    "return false when dambord check failes" in {
      val localRdw = new MockRdwRegistry(RdwData("MB12CD", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1"))
      val localProcessor = new PreProcessTDC3Category(localRdw, config, log)

      val exit1 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val entry1 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("SJ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord1 = createSpeedRecord(entry1, exit1)
      val exit2 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("MB12CD"), category = None, length = Some(4.5F), country = Some("NL"))
      val entry2 = createVehicle(laneId = "lane1", timestamp = startTime + 1000, license = Some("MB12CD"), category = Some(VehicleCategory.CarTrailer), length = None, country = Some("NL"))
      val speedRecord2 = createSpeedRecord(entry2, exit2)
      localProcessor.doesRuleApply(entryCheck, speedRecord1, speedRecord2) must be(false)
    }
    "return false when RDW check failes" in {
      val localProcessor = new PreProcessTDC3Category(new NoRdwRegistry(), config, log)

      val exit1 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val entry1 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("SJ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord1 = createSpeedRecord(entry1, exit1)
      val exit2 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("GZ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val entry2 = createVehicle(laneId = "lane1", timestamp = startTime + 1000, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = None, country = Some("NL"))
      val speedRecord2 = createSpeedRecord(entry2, exit2)
      localProcessor.doesRuleApply(entryCheck, speedRecord1, speedRecord2) must be(false)
    }
    "return false when first category check failes" in {
      val exit1 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val entry1 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("SJ123C"), category = Some(VehicleCategory.Car), length = None, country = Some("NL"))
      val speedRecord1 = createSpeedRecord(entry1, exit1)
      val exit2 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("GZ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val entry2 = createVehicle(laneId = "lane1", timestamp = startTime + 1000, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = None, country = Some("NL"))
      val speedRecord2 = createSpeedRecord(entry2, exit2)
      processor.doesRuleApply(entryCheck, speedRecord1, speedRecord2) must be(false)
    }
    "return false when license are equal" in {
      val exit1 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("GZ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val entry1 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("GZ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord1 = createSpeedRecord(entry1, exit1)
      val exit2 = createVehicle(laneId = "lane1", timestamp = startTime - 5.minutes.toMillis, license = Some("GZ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val entry2 = createVehicle(laneId = "lane1", timestamp = startTime + 1000, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = None, country = Some("NL"))
      val speedRecord2 = createSpeedRecord(entry2, exit2)
      processor.doesRuleApply(entryCheck, speedRecord1, speedRecord2) must be(false)
    }
  }

}