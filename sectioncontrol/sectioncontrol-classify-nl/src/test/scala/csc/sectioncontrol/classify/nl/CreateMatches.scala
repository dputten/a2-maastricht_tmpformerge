/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.classify.nl

import csc.sectioncontrol.messages._
import java.util.Date
import csc.sectioncontrol.messages.VehicleSpeedRecord
import csc.sectioncontrol.messages.ValueWithConfidence
import csc.sectioncontrol.messages.VehicleClassifiedRecord
import csc.sectioncontrol.messages.Lane
import csc.sectioncontrol.messages.VehicleMetadata

object CreateMatches {
  val PLATE_MIL = "KW9999"
  val PLATE_M = "MB12CD"
  val PLATE_P = "GZ123C"
  val PLATE_LB = "1VBC23"
  val PLATE_B = "BB12CD"
  val PLATE_ZB = "12BZC3"
  val PLATE_A = "12WN34"
  val PLATE_O = "OL1234"
  val PLATE_C = "12FZC3"
  val PLATE_UNKOWN = "20SDBJ"
  val PLATE_WRONG = "AABBCC"

  def createTDC3NoCar_1(plate: String = PLATE_P, country: Option[String] = Some("NL")): VehicleSpeedRecord = {
    getMatch(plate, VehicleCategory.Truck, Some(7F), country)
  }
  def createTDC3Empty_2(plate: String = PLATE_P, country: Option[String] = Some("NL")): VehicleSpeedRecord = {
    getMatch(plate, VehicleCategory.Unknown, Some(7F), country)
  }
  def createTDC3_70_4(plate: String = PLATE_P, country: Option[String] = Some("NL"), category: VehicleCategory.Value = VehicleCategory.Car): VehicleSpeedRecord = {
    val first = vreg("id1", 1000L, "g1", plate, None, Some(7F), country)
    val second = VehicleMetadata(
      createLane("g2"),
      "id2",
      2000L,
      Some(new Date(2000L).toString),
      first.license,
      None, None,
      Some(new ValueWithConfidence[String](category.toString(), 70)),
      None, first.country, Seq(), "", "", "")

    VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")
  }
  def createTDC3_10_5(plate: String = PLATE_P, country: Option[String] = Some("NL"), category: VehicleCategory.Value = VehicleCategory.Car): VehicleSpeedRecord = {
    val first = vreg("id1", 1000L, "g1", plate, None, Some(7F), country)
    val second = VehicleMetadata(
      createLane("g2"),
      "id2",
      2000L,
      Some(new Date(2000L).toString),
      first.license,
      None, None,
      Some(new ValueWithConfidence[String](category.toString(), 10)),
      None, first.country, Seq(), "", "", "")

    VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")
  }
  def createTDC3_0_TDC1_fail_6(plate: String = PLATE_P, country: Option[String] = Some("NL"), category: VehicleCategory.Value = VehicleCategory.Car): VehicleSpeedRecord = {
    val first = vreg("id1", 1000L, "g1", plate, None, Some(7F), country)
    val second = VehicleMetadata(
      createLane("g2"),
      "id2",
      2000L,
      Some(new Date(2000L).toString),
      first.license,
      None, None,
      Some(new ValueWithConfidence[String](category.toString(), 0)),
      None, first.country, Seq(), "", "", "")

    VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")
  }
  def createTDC3_0_TDC1_OK_7(plate: String = PLATE_P, country: Option[String] = Some("NL"), category: VehicleCategory.Value = VehicleCategory.Car): VehicleSpeedRecord = {
    val first = vreg("id1", 1000L, "g1", plate, None, Some(4.6F), country)
    val second = VehicleMetadata(
      createLane("g2"),
      "id2",
      2000L,
      Some(new Date(2000L).toString),
      first.license,
      None, None,
      Some(new ValueWithConfidence[String](category.toString(), 0)),
      None, first.country, Seq(), "", "", "")

    VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")
  }

  def createTDC3_0_TDC1_0_8(plate: String = PLATE_P, country: Option[String] = Some("NL"), category: VehicleCategory.Value = VehicleCategory.Car): VehicleSpeedRecord = {
    val first = vreg("id1", 1000L, "g1", plate, None, None, country)
    val second = VehicleMetadata(
      createLane("g2"),
      "id2",
      2000L,
      Some(new Date(2000L).toString),
      first.license,
      None, None,
      Some(new ValueWithConfidence[String](category.toString(), 0)),
      None, first.country, Seq(), "", "", "")

    VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")
  }

  def getMatch(plate: String, category: VehicleCategory.Value = VehicleCategory.Car, length: Option[Float] = None, country: Option[String] = Some("NL")): VehicleSpeedRecord = {
    val first = vreg("id1", 1000L, "g1", plate, None, length, country)
    val second = vreg("id2", 2000L, "g2", plate, Some(category), None, country)
    VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")
  }

  def getCountry(passage: VehicleClassifiedRecord): String = {
    passage.speedRecord.exit.get.country.get.value
  }

  def vreg(id: String, timestamp: Long, gantry: String, license: String, category: Option[VehicleCategory.Value], length: Option[Float], country: Option[String]): VehicleMetadata = {
    vregis(id, timestamp, gantry, license, category, length, country)
  }

  def vregis(id: String, timestamp: Long, gantry: String, license: String, category: Option[VehicleCategory.Value], length: Option[Float], country: Option[String]): VehicleMetadata = {
    val lic = Some(ValueWithConfidence(license, 90))
    val cou = country match {
      case None    ⇒ None
      case Some(c) ⇒ Some(ValueWithConfidence(c, 95))
    }
    VehicleMetadata(createLane(gantry), id, timestamp, Some(new Date(timestamp).toString), lic, None, length.map(new ValueWithConfidence_Float(_, 60)), category.map(cat ⇒ new ValueWithConfidence[String](cat.toString(), 90)), None, cou, Seq(), "", "", "")
  }

  private def createLane(gantry: String) = Lane("23L", "foo", gantry, "route1", 25f, 37f)

}