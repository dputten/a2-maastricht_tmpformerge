package csc.sectioncontrol.classify.nl.licensePlate.dambord

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import java.io.InputStream
import csc.akkautils.DirectLogging
import csc.sectioncontrol.classify.nl.{ DamBordParser, RdwDambordCategory }

class DambordTest extends WordSpec with MustMatchers with DirectLogging {
  "LicensePlate" must {
    val allCategories = parseAll()
    val result = allCategories.filter(cat ⇒ {
      val category = RdwDambordCategory.parse(cat)
      category match {
        case RdwDambordCategory.Other(error) ⇒ true
        case _                               ⇒ false
      }
    })
    result.foreach(c ⇒ log.error("Unexpected RDW category: " + c))

    "report no unexpected RDW categories" in {
      result must be('empty)
    }
  }

  def parseAll(): Set[String] = {
    val definitions = getInputs.map(input ⇒ {
      val names = parse(input)
      names
    })
    definitions.reduce[Set[String]] { case (op1, op2) ⇒ op1 ++ op2 }
  }

  def getInputs: List[InputStream] = {
    (1 to 14).map(code ⇒ {
      val name = "/Dambord%s.csv".format(code)
      getClass.getResourceAsStream(name)
    }).toList
  }

  def parse(input: InputStream): Set[String] = {
    val source = scala.io.Source.fromInputStream(input)
    val _ :: secondLetterHeader :: lines = source.mkString.split("\n").toList //drop first line
    val columnHeader: List[String] = secondLetterHeader.split(";").toList.tail.map(_.trim()).takeWhile(!_.isEmpty) //drop first ';' separator
    val valueLines = lines.filter(_.head.isLetter) // filter out lines which start with ';'
    val rows = valueLines.map(line ⇒ {
      val (_, data) = DamBordParser.parseLine(line, columnHeader.map(_(0)))
      data.values.map(_.name).toSet
    })
    val folded = rows.reduce[Set[String]] { case (op1, op2) ⇒ op1 ++ op2 }
    folded
  }
}
