/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.classify.nl

import net.liftweb.json.DefaultFormats
import org.apache.hadoop.hbase.client._
import csc.sectioncontrol.messages._
import org.apache.hadoop.hbase.util.Bytes
import csc.json.lift.EnumerationSerializer
import csc.hbase.utils.{ HBaseTableKeeper, JsonHBaseWriter }
import java.util.Date
import csc.sectioncontrol.storagelayer.hbasetables.SpeedRecordTable
import org.apache.hadoop.conf.Configuration

/**
 * Generate speed records
 */
class SpeedRecordGenerator(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper) {

  val systemId = "route1"
  val writer = SpeedRecordTable.makeWriter(hbaseConfig, tableKeeper)

  val startTime = System.currentTimeMillis() - 1000 * 3600 * 24 * 3 //3 days ago
  val endTime = System.currentTimeMillis() - 1000 * 3600 * 24 * 2 // 2 days ago

  def addSpeedRecords() {
    val vehicles = List(createVR(startTime + 1, "entry1", "12AB34", Some(4.5f), Some(VehicleCategory.Pers), Some("NL")),
      createVR(startTime + 60 * 10 * 1000, "exit1", "12AB34", Some(4.5f), Some(VehicleCategory.Pers), Some("NL")),
      createVR(startTime + 3, "entry1", "12AB45", None, Some(VehicleCategory.CarTrailer), Some("NL")), //no length detected at the entry point
      createVR(startTime + 3 + 60 * 10 * 1000, "exit1", "12AB45", None, Some(VehicleCategory.CarTrailer), Some("NL")))
    val grouped = vehicles.grouped(2).map {
      case entry :: exit :: Nil ⇒ {
        val rec = VehicleSpeedRecord(exit.eventId, entry, Some(exit), 2002, 90, "SHA-1 data")
        (rec, rec)
      }
      case error ⇒ throw new IllegalArgumentException("Unexpected data.")
    }
    writer.writeRows(grouped.toSeq)
  }

  private def createVR(timestamp: Long, gantry: String, license: String, length: Option[Float], category: Option[VehicleCategory.Value], country: Option[String]): VehicleMetadata = {
    val lane = Lane("23L", "foo", gantry, systemId, 25f, 37f)
    val lic = Some(ValueWithConfidence(license, 90))
    val len = length match {
      case None    ⇒ None
      case Some(l) ⇒ Some(ValueWithConfidence_Float(l, 90))
    }
    val cou = country match {
      case None    ⇒ None
      case Some(c) ⇒ Some(ValueWithConfidence(c, 90))
    }
    VehicleMetadata(lane, timestamp.toString + "-" + license, timestamp, Some(new Date(timestamp).toString), lic, None, len, category.map(cat ⇒ new ValueWithConfidence[String](cat.toString(), 60)), None, cou, Seq(), "", "", "SN1234")
  }
}

