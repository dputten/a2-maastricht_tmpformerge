/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.classify.nl

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.rdwregistry.RdwData
import csc.sectioncontrol.messages._
import csc.sectioncontrol.storage.ZkClassifyConfidence
import csc.sectioncontrol.messages.VehicleSpeedRecord
import java.util.Date

class RDWMatchClassifierConfTest extends WordSpec with MustMatchers {
  val writer = new MockClassifyDecisionWriter()

  "MatchClassifier" must {
    val config = ClassifierConfig(0.4f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
      processCategory = "TDC3"))
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(RdwData("KW9999", 25, 3, 2500, 3.0f, 2.0f, 1500, 150, 160, "LF")), config, writer)

    "detect MIL auto" in {
      val passage = classifier.classify(getMatch("KW9999", length = Some(4.5F)))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CA)
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.mil must be(true)
      passage.validLicensePlate must be(None)
      passage.manualAccordingToSpec must be(false)
    }

    "detect long MIL auto" in {
      val passage = classifier.classify(getMatch("KW9911", VehicleCategory.Truck, 90, Some(8F)))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.VA)
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.mil must be(true)
      passage.validLicensePlate must be(None)
      passage.manualAccordingToSpec must be(false)
    }

    "detect motorbyke" in {
      val passage = classifier.classify(getMatch("MB12CD"))
      getCountry(passage) must be("NL")
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.mil must be(false)
      passage.validLicensePlate must be(None)
      passage.manualAccordingToSpec must be(false)
    }

    "detect Italian auto" in {
      val passage = classifier.classify(getMatch("1234567890", country = Some("IT")))
      getCountry(passage) must be("IT")
      passage.indicator must be(ProcessingIndicator.Mobi)
      passage.code.get must be(VehicleCode.MV)
      passage.validLicensePlate must be(None)
      passage.manualAccordingToSpec must be(false)
    }

    "detect German personal auto" in {
      val passage = classifier.classify(getMatch("DE123456", length = Some(4.5F), country = Some("DE")))
      getCountry(passage) must be("DE")
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.code.get must be(VehicleCode.MV)
      passage.validLicensePlate must be(None)
      passage.manualAccordingToSpec must be(false)
    }

    "detect Belgium truck" in {
      val passage = classifier.classify(getMatch("BE123456", country = Some("BE"), category = VehicleCategory.Large_Truck))
      getCountry(passage) must be("BE")
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.code.get must be(VehicleCode.VA)
      passage.validLicensePlate must be(None)
      passage.manualAccordingToSpec must be(true)
    }

    "detect personal auto" in {
      val passage = classifier.classify(getMatch("SJ123C", length = Some(4.5F)))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.code.get must be(VehicleCode.PA)
      passage.validLicensePlate must be(None)
      passage.licensePlate must be("SJ123C")
    }

    "detect too long license plate" in {
      val passage = classifier.classify(getMatch("1234567890123456789"))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.code must be(None)
      passage.validLicensePlate must be(Some("123456789012"))
      passage.licensePlate must be("123456789012")
    }
  }

  "MatchClassifier P" must {
    val rdw = RdwData("GZ123C", vehicleClass = 25, deviceCode = 68, maxMass = 2500,
      wheelBase = 2.0f, length = 6.5f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1e")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.4f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
      processCategory = "TDC3")), decisionWriter = writer)

    "detect camper auto" in {
      val passage = classifier.classify(getMatch("GZ123C", category = VehicleCategory.Van, length = Some(6.3F)))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.invalidRdwData must be(false)
      passage.code.get must be(VehicleCode.CP)
    }
  }

  "MatchClassifier P" must {
    val rdw = RdwData("GZ123C", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
      wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
      processCategory = "TDC3")), decisionWriter = writer)

    "detect long personal auto" in {
      val passage = classifier.classify(getMatch("GZ123C", category = VehicleCategory.Car, length = Some(5.9F)))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.code.get must be(VehicleCode.PA)
    }
  }

  //BOVEN IS GOED

  "MatchClassifier P" must {
    val rdw = RdwData("GZ123C", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
      wheelBase = 2.0f, length = 5.3f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
      processCategory = "TDC3")), decisionWriter = writer)

    "detect long personal auto with a trailer" in {
      val passage = classifier.classify(getMatch("GZ123C", category = VehicleCategory.CarTrailer, length = Some(5.9F)))
      //todo: tempory fix all AS with license of pulling vehicle must be manual
      //passage.indicator must be(ProcessingIndicator.Automatic)
      //passage.reason.mask must be(0)
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(2)

      passage.code.get must be(VehicleCode.AS)
    }
  }

  "MatchClassifier P" must {

    "detect long personal auto with a trailer and confidence is low" in {
      val rdw = RdwData("GZ123C", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 5.3f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.8f, classifyConfidence = new ZkClassifyConfidence(
        processCategory = "TDC3")), decisionWriter = writer)
      val passage = classifier.classify(getMatch("GZ123C", category = VehicleCategory.CarTrailer, confidence = 50, length = Some(5.5F)))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.code.get must be(VehicleCode.AS)
    }
  }

  "MatchClassifier LB, no bus, no camper" must {
    val rdw = RdwData("1VBC23", vehicleClass = 20, deviceCode = 10, maxMass = 2500,
      wheelBase = 2.0f, length = 10.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
      processCategory = "TDC3")), decisionWriter = writer)

    "detect long LB with a trailer" in {
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.CarTrailer, length = Some(6F)))
      //todo: tempory fix all AS with license off pulling vehicle must be manual
      //passage.indicator must be(ProcessingIndicator.Automatic)
      //passage.reason.mask must be(0)
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(2)

      passage.code.get must be(VehicleCode.AS)
    }
    "detect long LB with a trailer with low confidence" in {
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.TruckTrailer, confidence = 50))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.code.get must be(VehicleCode.AS)
    }
    "detect camper LB without a trailer" in {
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Truck))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.code.get must be(VehicleCode.CA)
    }
  }

  "MatchClassifier LB, no bus, new camper (after 1998)" must {
    val rdw = RdwData("1VBC23", vehicleClass = 20, deviceCode = 25, maxMass = 2500,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
      processCategory = "TDC3")), decisionWriter = writer)

    "detect camper" in {
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.code.get must be(VehicleCode.CP)
    }
  }

  "MatchClassifier LB, no bus, old camper (before 1998)" must {
    val rdw = RdwData("1VBC23", vehicleClass = 20, deviceCode = 68, maxMass = 2500,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
      processCategory = "TDC3")), decisionWriter = writer)

    "detect camper" in {
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.code.get must be(VehicleCode.CP)
    }
  }

  "MatchClassifier LB, caravan is no camper after 1998" must {
    val rdw = RdwData("1VBC23", vehicleClass = 20, deviceCode = 69, maxMass = 2500,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
      processCategory = "TDC3")), decisionWriter = writer)

    "detect caravan (no camper)" in {
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.code.get must be(VehicleCode.CA)
    }
  }

  "MatchClassifier normal Bus" must {
    val rdw = RdwData("1VBC23", vehicleClass = 20, deviceCode = 1, maxMass = 2500,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
      processCategory = "TDC3")), decisionWriter = writer)

    "detect bus" in {
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.code.get must be(VehicleCode.AB)
    }
  }

  "MatchClassifier tempo 100 Bus" must {
    val rdw = RdwData("1VBC23", vehicleClass = 20, deviceCode = 1, maxMass = 2500,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 100, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
      processCategory = "TDC3")), decisionWriter = writer)

    "detect bus tempo 100" in {
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.code.get must be(VehicleCode.AT)
    }
  }

  "MatchClassifier ZB, bus, no tempo100" must {
    val rdw = RdwData("12BZC3", vehicleClass = 20, deviceCode = 1, maxMass = 2500,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
      processCategory = "TDC3")), decisionWriter = writer)

    "detect AB" in {
      val passage = classifier.classify(getMatch("12BZC3", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.code.get must be(VehicleCode.AB)
    }
  }

  "MatchClassifier ZB, bus, tempo100" must {
    val rdw = RdwData("12BZC3", vehicleClass = 20, deviceCode = 1, maxMass = 2500,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 100, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
      processCategory = "TDC3")), decisionWriter = writer)

    "detect AB tempo100 AT" in {
      val passage = classifier.classify(getMatch("12BZC3", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.code.get must be(VehicleCode.AT)
    }
  }

  "MatchClassifier ZB, no bus" must {
    val rdw = RdwData("12BZC3", vehicleClass = 20, deviceCode = 10, maxMass = 2500,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 0, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
      processCategory = "TDC3")), decisionWriter = writer)

    "detect no camper" in {
      val passage = classifier.classify(getMatch("12BZC3", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.code.get must be(VehicleCode.VA)
    }
  }

  "MatchClassifier ZB, no bus, camper" must {
    val rdw = RdwData("12BZC3", vehicleClass = 20, deviceCode = 68, maxMass = 2500,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 0, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
      processCategory = "TDC3")), decisionWriter = writer)

    "detect camper" in {
      val passage = classifier.classify(getMatch("12BZC3", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.code.get must be(VehicleCode.CB)
    }
  }

  "MatchClassifier B heavy" must {
    val rdw = RdwData("BB12CD", vehicleClass = 20, deviceCode = 68, maxMass = 3501,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 0, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
      processCategory = "TDC3")), decisionWriter = writer)

    "detect heavy" in {
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.code.get must be(VehicleCode.CB)
    }
  }

  "MatchClassifier B light" must {
    val rdw = RdwData("BB12CD", vehicleClass = 20, deviceCode = 1, maxMass = 3500,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
      processCategory = "TDC3")), decisionWriter = writer)

    "detect light" in {
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.invalidRdwData must be(false)
      passage.code.get must be(VehicleCode.AB)
    }
  }

  "MatchClassifier A light" must {
    val rdw = RdwData("12WN34", vehicleClass = 20, deviceCode = 1, maxMass = 1300,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
      processCategory = "TDC3")), decisionWriter = writer)

    "detect AS" in {
      val passage = classifier.classify(getMatch("12WN34", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.code.get must be(VehicleCode.AS)
    }
  }

  "MatchClassifier A heavy" must {
    val rdw = RdwData("12WN34", vehicleClass = 20, deviceCode = 1, maxMass = 4300,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
      processCategory = "TDC3")), decisionWriter = writer)

    "detect AS" in {
      val passage = classifier.classify(getMatch("12WN34", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.code.get must be(VehicleCode.AO)
    }
  }

  "MatchClassifier O light" must {
    val rdw = RdwData("OL1234", vehicleClass = 20, deviceCode = 1, maxMass = 1300,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
      processCategory = "TDC3")), decisionWriter = writer)

    "detect AS" in {
      val passage = classifier.classify(getMatch("OL1234", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.code.get must be(VehicleCode.AS)
    }
  }

  "MatchClassifier O heavy" must {
    val rdw = RdwData("OL1234", vehicleClass = 20, deviceCode = 1, maxMass = 4300,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
      processCategory = "TDC3")), decisionWriter = writer)

    "detect AS" in {
      val passage = classifier.classify(getMatch("OL1234", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.code.get must be(VehicleCode.AO)
    }
  }

  "MatchClassifier C" must {
    val rdw = RdwData("12FZC3", vehicleClass = 20, deviceCode = 10, maxMass = 300,
      wheelBase = 1.2f, length = 1.4f, width = 100, maxSpeed = 25, diffMaxSpeed = 25, eegCategory = "L6")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
      processCategory = "TDC3")), decisionWriter = writer)

    "detect BS for speed = 25" in {
      val passage = classifier.classify(getMatch("12FZC3", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.code.get must be(VehicleCode.BS)
    }
  }

  "MatchClassifier C" must {
    val rdw = RdwData("12FZC3", vehicleClass = 20, deviceCode = 10, maxMass = 300,
      wheelBase = 1.2f, length = 1.4f, width = 100, maxSpeed = 45, diffMaxSpeed = 45, eegCategory = "L6")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
      processCategory = "TDC3")), decisionWriter = writer)

    "detect BB for L6 category" in {
      val passage = classifier.classify(getMatch("12FZC3", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.code.get must be(VehicleCode.BB)
    }
  }

  "MatchClassifier C" must {
    val rdw = RdwData("12FZC3", vehicleClass = 20, deviceCode = 10, maxMass = 300,
      wheelBase = 1.2f, length = 1.4f, width = 100, maxSpeed = 45, diffMaxSpeed = 45, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
      processCategory = "TDC3")), decisionWriter = writer)

    "detect BF" in {
      val passage = classifier.classify(getMatch("12FZC3", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.code.get must be(VehicleCode.BF)
    }
  }

  "MatchClassifier with high country confidence level" must {
    val rdw = RdwData("12FZC3", vehicleClass = 20, deviceCode = 10, maxMass = 300,
      wheelBase = 0.2f, length = 0.4f, width = 100, maxSpeed = 45, diffMaxSpeed = 45, eegCategory = "L6")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.5f, countryCodeConfidence = 99, classifyConfidence = new ZkClassifyConfidence(
      processCategory = "TDC3")), decisionWriter = writer)

    "detect manual instead of mobi" in {
      val passage = classifier.classify(getMatch("12FZC3", category = VehicleCategory.Car, country = Some("BE")))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.code.get must be(VehicleCode.MV)
    }
  }

  "MatchClassifier B with invalid RDW data (no mass)" must {
    val rdw = RdwData("BB12CD", vehicleClass = 20, deviceCode = 1, maxMass = 0,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
      processCategory = "TDC3")), decisionWriter = writer)

    "detect manual and report RDW error about mass" in {
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.invalidRdwData must be(true)
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.CA)
      passage.manualAccordingToSpec must be(true)
    }
  }

  "MatchClassifier ZB, without RDW data" must {
    val classifier = new RdwMatchClassifier(new NoRdwRegistry(), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
      processCategory = "TDC3")), decisionWriter = writer)

    "report manual and error because RDW data is required" in {
      val passage = classifier.classify(getMatch("12BZC3", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.VA)
      passage.manualAccordingToSpec must be(false)
    }
  }

  "MatchClassifier ZB" must {
    val classifier = new RdwMatchClassifier(new NoRdwRegistry(), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
      processCategory = "TDC3")), decisionWriter = writer)

    "not be classified when RDW doesn't exists" in {
      val first = vreg("id1", 1000L, "g1", "12BZC3", None, Some(0F), None)
      val second = vreg("id2", 2000L, "g2", "12BZC3", Some(VehicleCategory.Truck, 50), None, Some("NL"))
      val passage = classifier.classify(VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, ""))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.code must be(None)
      passage.alternativeClassification must be(Some(VehicleCode.VA))
      passage.manualAccordingToSpec must be(false)
    }

    "not be classified when empty fields in RDW data " in {
      val first = vreg("id1", 1000L, "g1", "12BZC3", None, Some(0F), None)
      val second = vreg("id2", 2000L, "g2", "12BZC3", Some(VehicleCategory.Unknown, 90), None, Some("NL"))
      val rdw = RdwData("12BZC3", vehicleClass = 20, deviceCode = 10, maxMass = 0,
        wheelBase = 1.2f, length = 1.4f, width = 100, maxSpeed = 25, diffMaxSpeed = 25, eegCategory = "L6")

      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
        processCategory = "TDC3")), decisionWriter = writer)
      val passage = classifier.classify(VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, ""))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.code must be(None)
      passage.alternativeClassification must be(Some(VehicleCode.VA))
      passage.manualAccordingToSpec must be(true)
    }
  }

  "MatchClassifier, without country code" must {
    val classifier = new RdwMatchClassifier(new NoRdwRegistry(), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
      processCategory = "TDC3")), decisionWriter = writer)
    val first = vreg("id1", 1000L, "g1", "12BZC3", None, Some(4.5F), None)
    val second = vreg("id2", 2000L, "g2", "12BZC3", Some(VehicleCategory.Car, 90), None, None)
    val passage = classifier.classify(VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, ""))
    "not be classified" in {
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.code.get must be(VehicleCode.MV)
      passage.manualAccordingToSpec must be(false)
    }
  }

  "MatchClassifier MIL, without measured length" must {

    "Car not be classified if confidence is high and category filled" in {
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
        processCategory = "TDC3")), decisionWriter = writer)
      val first = vreg("id1", 1000L, "g1", "KW9999", None, Some(0F), Some("NL"))
      val second = vreg("id2", 2000L, "g2", "KW9999", Some(VehicleCategory.Car, 70), None, Some("NL"))
      val passage = classifier.classify(VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, ""))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.code must be(Some(VehicleCode.CA))
      passage.mil must be(true) //it is not classified
      passage.manualAccordingToSpec must be(false)
    }
    "Car not be classified if confidence is low and category filled" in {
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
        processCategory = "TDC3")), decisionWriter = writer)
      val first = vreg("id1", 1000L, "g1", "KW9999", None, Some(0F), Some("NL"))
      val second = vreg("id2", 2000L, "g2", "KW9999", Some(VehicleCategory.Car, 50), None, Some("NL"))
      val passage = classifier.classify(VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, ""))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.code must be(Some(VehicleCode.CA))
      passage.mil must be(true) //it is not classified
      passage.manualAccordingToSpec must be(false)
    }
    "CA when classified if category unknown" in {
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), ClassifierConfig(0.3f, 0.5f, classifyConfidence = new ZkClassifyConfidence(
        processCategory = "TDC3")), decisionWriter = writer)
      val first = vreg("id1", 1000L, "g1", "KW9999", None, Some(0F), Some("NL"))
      val second = vreg("id2", 2000L, "g2", "KW9999", Some(VehicleCategory.Unknown, 90), None, Some("NL"))
      val passage = classifier.classify(VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, ""))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.code must be(Some(VehicleCode.CA))
      passage.mil must be(true) //it is not classified
      passage.manualAccordingToSpec must be(false)
    }
  }

  def getMatch(plate: String, category: VehicleCategory.Value = VehicleCategory.Car, confidence: Int = 90, length: Option[Float] = None, country: Option[String] = Some("NL")): VehicleSpeedRecord = {
    val first = vreg("id1", 1000L, "g1", plate, None, length, country)
    val second = vreg("id2", 2000L, "g2", plate, Some(category, confidence), None, country)
    VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")
  }

  def getCountry(passage: VehicleClassifiedRecord): String = {
    passage.speedRecord.exit.get.country.get.value
  }

  def vreg(id: String, timestamp: Long, gantry: String, license: String, category: Option[(VehicleCategory.Value, Int)], length: Option[Float], country: Option[String]): VehicleMetadata = {
    val lane = Lane("23L", "foo", gantry, "route1", 25f, 37f)
    val lic = Some(ValueWithConfidence(license, 90))
    val cou = country match {
      case None    ⇒ None
      case Some(c) ⇒ Some(ValueWithConfidence(c, 95))
    }
    val cat = category.map { case (c, conf) ⇒ new ValueWithConfidence[String](c.toString(), conf) }
    VehicleMetadata(lane, id, timestamp, Some(new Date(timestamp).toString), lic, None, length.map(new ValueWithConfidence_Float(_, 60)), cat, None, cou, Seq(), "", "", "SN1234")
  }

}