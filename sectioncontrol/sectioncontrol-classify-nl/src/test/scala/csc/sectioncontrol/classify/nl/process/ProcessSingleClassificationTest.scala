/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.classify.nl.process

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import akka.testkit.{ TestProbe, TestActorRef, TestKit }
import akka.actor.{ Props, ActorSystem }
import csc.sectioncontrol.classify.nl.config.ConfigurationCache
import csc.sectioncontrol.rdwregistry.RdwData
import csc.sectioncontrol.storagelayer.hbasetables.{ DecisionResultTable, VehicleClassifiedTable }
import akka.util.Duration
import akka.util.duration._
import csc.sectioncontrol.classify.nl.MockRdwRegistry
import csc.sectioncontrol.messages._
import csc.sectioncontrol.classify.nl.ClassifierConfig
import csc.sectioncontrol.classify.nl.DecisionResult
import csc.sectioncontrol.messages.VehicleSpeedRecord
import csc.sectioncontrol.messages.VehicleClassifiedRecord
import csc.sectioncontrol.messages.Lane

class ProcessSingleClassificationTest extends TestKit(ActorSystem("ProcessSingleClassificationTest")) with WordSpec with MustMatchers {
  "ProcessSingleClassification" must {
    "classify record" in {
      val producer = TestProbe()
      val rdwRegistry = new MockRdwRegistry(RdwData("KW9999", 25, 3, 2500, 3.0f, 2.0f, 1500, 150, 160, "LF"))
      val classifyWriter = new ClassifyWriterMock()
      val process = TestActorRef(Props(new ProcessSingleClassification(
        configCache = new ConfigurationCacheMock(1.minute),
        rdwRegistry = rdwRegistry,
        resultWriter = classifyWriter,
        decisionWriter = new DecisionWriter,
        producer = producer.ref)))
      val speedRecord = createSpeedRecord()
      process ! speedRecord
      val response = producer.expectMsgType[VehicleClassifiedRecord](10.millis)
      response.speedRecord must be(speedRecord)

      val records = classifyWriter.getWrittenRecords
      records.size must be(1)
      records.head must be(response)

      response.indicator must be(ProcessingIndicator.Automatic)
      response.code must be(Some(VehicleCode.CA))
    }
  }

  def createSpeedRecord(): VehicleSpeedRecord = {
    val entry = VehicleMetadata(
      lane = Lane(
        laneId = "A2-E1-R1",
        name = "R1",
        gantry = "E1",
        system = "A2",
        sensorGPS_longitude = 52.4,
        sensorGPS_latitude = 5.4),
      eventId = "A2-E1-R1-00001",
      eventTimestamp = 1438671100000L,
      eventTimestampStr = None,
      license = Some(ValueWithConfidence("1234AB", 80)),
      rawLicense = Some("1234AB"),
      length = Some(ValueWithConfidence_Float(4.3F, 20)),
      category = Some(ValueWithConfidence("Car", 80)),
      speed = Some(ValueWithConfidence_Float(125F, 50)),
      country = Some(ValueWithConfidence("NL", 60)),
      images = Seq(
        VehicleImage(timestamp = 1438671100000L,
          offset = 0,
          uri = "overview",
          imageType = VehicleImageType.Overview,
          checksum = "ABCDEF")),
      NMICertificate = "PD1234",
      applChecksum = "1234ABC",
      serialNr = "SN1234",
      recognizedBy = Some("ARH"))
    val exit = entry.copy(lane = entry.lane.copy(laneId = "A2-X1-R1", gantry = "X1"), eventId = "A2-X1-R1-00001")

    VehicleSpeedRecord(id = "A2-S1-0000-R1",
      entry = entry,
      exit = Some(exit),
      corridorId = 11,
      speed = 12,
      measurementSHA = "1234ABCDFE")
  }

}

class ConfigurationCacheMock(dirtyDuration: Duration) extends ConfigurationCache(null, dirtyDuration) {
  override def getConfiguration(systemId: String) = {
    cleanup(System.currentTimeMillis())
    val record = cache.get(systemId).map(_.config)
    record.orElse(createConfig(systemId))
  }
  def createConfig(systemId: String): Option[ClassifierConfig] = {
    Some(ClassifierConfig(1.0f, 2.0f))
  }
}

class ClassifyWriterMock extends VehicleClassifiedTable.Writer {
  private var written = Seq[VehicleClassifiedRecord]()
  def getWrittenRecords: Seq[VehicleClassifiedRecord] = {
    written
  }
  /**
   * Writes a single row.
   */
  def writeRow(key: VehicleClassifiedRecord, value: VehicleClassifiedRecord, timestamp: Option[Long]) {
    written = written :+ value
  }

  /**
   * Writes a series of rows.
   */
  def writeRows(keysAndValues: Seq[(VehicleClassifiedRecord, VehicleClassifiedRecord)]) {
    written = written ++ keysAndValues.map(_._2)
  }

  /**
   * Writes a series of rows with explicit timestamps.
   */
  def writeRowsWithTimestamp(keysAndValues: Seq[(VehicleClassifiedRecord, VehicleClassifiedRecord, Long)]) {
    written = written ++ keysAndValues.map(_._2)

  }

  /**
   * Writes a series of rows taking the key from the instance. If timestamp is None then
   * current time is used.
   */
  def writeRows(values: Seq[VehicleClassifiedRecord], getKey: (VehicleClassifiedRecord) ⇒ (VehicleClassifiedRecord, Option[Long])) {
    written = written ++ values
  }
}

class DecisionWriter extends DecisionResultTable.Writer {
  /**
   * Writes a single row.
   */
  def writeRow(key: String, value: DecisionResult, timestamp: Option[Long]) {}

  /**
   * Writes a series of rows.
   */
  def writeRows(keysAndValues: Seq[(String, DecisionResult)]) {}

  /**
   * Writes a series of rows with explicit timestamps.
   */
  def writeRowsWithTimestamp(keysAndValues: Seq[(String, DecisionResult, Long)]) {}

  /**
   * Writes a series of rows taking the key from the instance. If timestamp is None then
   * current time is used.
   */
  def writeRows(values: Seq[DecisionResult], getKey: (DecisionResult) ⇒ (String, Option[Long])) {}
}