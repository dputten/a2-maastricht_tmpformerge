/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.classify.nl.preprocess

import csc.sectioncontrol.messages._
import java.util.Date
import csc.sectioncontrol.messages.VehicleSpeedRecord
import csc.sectioncontrol.messages.ValueWithConfidence
import csc.sectioncontrol.messages.Lane
import csc.sectioncontrol.messages.VehicleMetadata

object CreateTestData {
  def createSpeedRecord(entry: VehicleMetadata): VehicleSpeedRecord = {
    VehicleSpeedRecord(entry.eventId, entry, None, 2002, 100, "")
  }

  def createSpeedRecord(entry: VehicleMetadata, exit: VehicleMetadata): VehicleSpeedRecord = {
    VehicleSpeedRecord(exit.eventId, entry, Some(exit), 2002, 100, "")
  }

  def createVehicle(laneId: String, timestamp: Long, license: Option[String], category: Option[VehicleCategory.Value], length: Option[Float], country: Option[String]): VehicleMetadata = {
    val lane = Lane(laneId, "foo", "gantry", "route1", 25f, 37f)
    val lic = license.map(l ⇒ ValueWithConfidence(l, 90))
    val cou = country.map(c ⇒ ValueWithConfidence(c, 95))
    val cat = category.map(c ⇒ new ValueWithConfidence[String](c.toString(), 90))

    VehicleMetadata(lane, laneId + timestamp, timestamp, Some(new Date(timestamp).toString), lic, None, length.map(new ValueWithConfidence_Float(_, 60)), cat, None, cou, Seq(), "", "", "")
  }

}