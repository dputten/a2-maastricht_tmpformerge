/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.classify.nl

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.messages._
import csc.sectioncontrol.rdwregistry.RdwData
import csc.sectioncontrol.rdwregistry.RdwRegistry
import java.util.Date
import csc.sectioncontrol.storage.ZkClassifyConfidence

class RDWMatchClassifierTest extends WordSpec with MustMatchers {
  val writer = new MockClassifyDecisionWriter()

  "MatchClassifier" must {
    val config = ClassifierConfig(0.4f, 0.5f, mobiCountries = List("DE", "BE", "CH"))
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(RdwData("KW9999", 25, 3, 2500, 3.0f, 2.0f, 1500, 150, 160, "LF")), config, writer)

    "detect MIL auto" in {
      val passage = classifier.classify(getMatch("KW9999", length = Some(4.5F)))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CA)
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.mil must be(true)
      passage.validLicensePlate must be(None)
      passage.manualAccordingToSpec must be(false)
    }

    "detect long MIL auto" in {
      val passage = classifier.classify(getMatch("KW9911", VehicleCategory.Truck, Some(8F)))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.VA)
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.mil must be(true)
      passage.validLicensePlate must be(None)
      passage.manualAccordingToSpec must be(false)
    }

    "detect motorbyke" in {
      val passage = classifier.classify(getMatch("MB12CD"))
      getCountry(passage) must be("NL")
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.mil must be(false)
      passage.validLicensePlate must be(None)
      passage.manualAccordingToSpec must be(false)
    }

    "detect Italian auto" in {
      val passage = classifier.classify(getMatch("1234567890", country = Some("IT")))
      getCountry(passage) must be("IT")
      passage.indicator must be(ProcessingIndicator.Mobi)
      passage.reason.mask must be(0)
      passage.code.get must be(VehicleCode.MV)
      passage.validLicensePlate must be(None)
      passage.manualAccordingToSpec must be(false)
    }

    "detect German personal auto" in {
      val passage = classifier.classify(getMatch("DE123456", length = Some(4.5F), country = Some("DE")))
      getCountry(passage) must be("DE")
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.code.get must be(VehicleCode.MV)
      passage.validLicensePlate must be(None)
      passage.manualAccordingToSpec must be(false)
    }

    "detect Belgium truck" in {
      val passage = classifier.classify(getMatch("BE123456", country = Some("BE"), category = VehicleCategory.Large_Truck))
      getCountry(passage) must be("BE")
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(2)
      passage.code.get must be(VehicleCode.VA)
      passage.validLicensePlate must be(None)
      passage.manualAccordingToSpec must be(true)
    }

    "detect personal auto" in {
      val passage = classifier.classify(getMatch("SJ123C", length = Some(4.5F)))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.code must be(Some(VehicleCode.PA))
      passage.alternativeClassification must be(None)
      passage.validLicensePlate must be(None)
      passage.licensePlate must be("SJ123C")
    }

    "detect too long license plate" in {
      val passage = classifier.classify(getMatch("1234567890123456789"))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(7)
      passage.code must be(None)
      passage.validLicensePlate must be(Some("123456789012"))
      passage.licensePlate must be("123456789012")
    }
  }

  "MatchClassifier P" must {
    val rdw = RdwData("GZ123C", vehicleClass = 25, deviceCode = 68, maxMass = 2500,
      wheelBase = 2.0f, length = 6.5f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1e")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f, mobiCountries = List("DE", "BE", "CH")), writer)

    "detect camper auto" in {
      val passage = classifier.classify(getMatch("GZ123C", category = VehicleCategory.Van, length = Some(6.3F)))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.invalidRdwData must be(false)
      passage.code.get must be(VehicleCode.CP)
    }
  }

  "MatchClassifier P" must {
    val rdw = RdwData("GZ123C", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
      wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.3f, margin2 = 0.5f, mobiCountries = List("DE", "BE", "CH")), writer)

    "detect long personal auto" in {
      val passage = classifier.classify(getMatch("GZ123C", category = VehicleCategory.Car, length = Some(5.9F)))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.code must be(Some(VehicleCode.PA))
      passage.alternativeClassification must be(None)
    }
  }

  "MatchClassifier P" must {
    val rdw = RdwData("GZ123C", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
      wheelBase = 2.0f, length = 5.3f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.3f, margin2 = 0.5f, mobiCountries = List("DE", "BE", "CH")), writer)

    "detect long personal auto with a trailer" in {
      val passage = classifier.classify(getMatch("GZ123C", category = VehicleCategory.CarTrailer, length = Some(5.9F)))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(2)
      passage.code must be(Some(VehicleCode.AS))
    }
  }

  "MatchClassifier P" must {
    "detect long personal auto with a trailer and length is smaller than minimum" in {
      val rdw = RdwData("GZ123C", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 5.3f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.8f, List("DE", "BE", "CH")), writer)
      val passage = classifier.classify(getMatch("GZ123C", category = VehicleCategory.CarTrailer, length = Some(5.5F)))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(2)
      passage.code.get must be(VehicleCode.AS)
    }
  }

  "MatchClassifier LB, no bus, no camper" must {
    val rdw = RdwData("1VBC23", vehicleClass = 20, deviceCode = 10, maxMass = 2500,
      wheelBase = 2.0f, length = 10.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.3f, margin2 = 0.5f, mobiCountries = List("DE", "BE", "CH")), writer)

    "detect long LB with a trailer" in {
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.CarTrailer, length = Some(6F)))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(2)
      passage.code must be(Some(VehicleCode.AS))
      passage.alternativeClassification must be(None)
    }
    "detect long LB with a trailer with incorrect Pir category" in {
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.TruckTrailer))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(2)
      passage.code.get must be(VehicleCode.AS)
    }
    "detect camper LB without a trailer" in {
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Truck))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.code.get must be(VehicleCode.CA)
    }
  }

  "MatchClassifier LB, no bus, new camper (after 1998)" must {
    val rdw = RdwData("1VBC23", vehicleClass = 20, deviceCode = 25, maxMass = 2500,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.3f, margin2 = 0.5f, mobiCountries = List("DE", "BE", "CH")), writer)

    "detect camper" in {
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.code.get must be(VehicleCode.CP)
    }
  }

  "MatchClassifier LB, no bus, old camper (before 1998)" must {
    val rdw = RdwData("1VBC23", vehicleClass = 20, deviceCode = 68, maxMass = 2500,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.3f, margin2 = 0.5f, mobiCountries = List("DE", "BE", "CH")), writer)

    "detect camper" in {
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.code.get must be(VehicleCode.CP)
    }
  }

  "MatchClassifier LB, caravan is no camper after 1998" must {
    val rdw = RdwData("1VBC23", vehicleClass = 20, deviceCode = 69, maxMass = 2500,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.3f, margin2 = 0.5f, mobiCountries = List("DE", "BE", "CH")), writer)

    "detect caravan (no camper)" in {
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.code.get must be(VehicleCode.CA)
    }
  }

  "MatchClassifier normal Bus" must {
    val rdw = RdwData("1VBC23", vehicleClass = 20, deviceCode = 1, maxMass = 2500,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.3f, margin2 = 0.5f, mobiCountries = List("DE", "BE", "CH")), writer)

    "detect bus" in {
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.code.get must be(VehicleCode.AB)
    }
  }

  "MatchClassifier tempo 100 Bus" must {
    val rdw = RdwData("1VBC23", vehicleClass = 20, deviceCode = 1, maxMass = 2500,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 100, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.3f, margin2 = 0.5f, mobiCountries = List("DE", "BE", "CH")), writer)

    "detect bus tempo 100" in {
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.code.get must be(VehicleCode.AT)
    }
  }

  "MatchClassifier ZB, bus, no tempo100" must {
    val rdw = RdwData("12BZC3", vehicleClass = 20, deviceCode = 1, maxMass = 2500,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.3f, margin2 = 0.5f, mobiCountries = List("DE", "BE", "CH")), writer)

    "detect AB" in {
      val passage = classifier.classify(getMatch("12BZC3", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.code.get must be(VehicleCode.AB)
    }
  }

  "MatchClassifier ZB, bus, tempo100" must {
    val rdw = RdwData("12BZC3", vehicleClass = 20, deviceCode = 1, maxMass = 2500,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 100, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.3f, margin2 = 0.5f, mobiCountries = List("DE", "BE", "CH")), writer)

    "detect AB tempo100 AT" in {
      val passage = classifier.classify(getMatch("12BZC3", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.code.get must be(VehicleCode.AT)
    }
  }

  "MatchClassifier ZB, no bus" must {
    val rdw = RdwData("12BZC3", vehicleClass = 20, deviceCode = 10, maxMass = 2500,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 0, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.3f, margin2 = 0.5f, mobiCountries = List("DE", "BE", "CH")), writer)

    "detect no camper" in {
      val passage = classifier.classify(getMatch("12BZC3", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.code.get must be(VehicleCode.VA)
    }
  }

  "MatchClassifier ZB, no bus, camper" must {
    val rdw = RdwData("12BZC3", vehicleClass = 20, deviceCode = 68, maxMass = 2500,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 0, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.5f), writer)

    "detect camper" in {
      val passage = classifier.classify(getMatch("12BZC3", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.code.get must be(VehicleCode.CB)
    }
  }

  "MatchClassifier ZB, no bus, cartrailer or trucktrailer " must {
    val rdw = RdwData("12BZC3", vehicleClass = 20, deviceCode = 68, maxMass = 2500,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 0, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.5f), writer)

    "detect cartrailer" in {
      val passage = classifier.classify(getMatch("12BZC3", category = VehicleCategory.CarTrailer))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(7)
      passage.code.get must be(VehicleCode.AO)
    }

    "detect trucktrailer" in {
      val passage: VehicleClassifiedRecord = classifier.classify(getMatch("12BZC3", category = VehicleCategory.TruckTrailer))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(7)
      passage.code.get must be(VehicleCode.AO)
    }

    "detect VA if TDC3 category == Unknown" in {
      val passage: VehicleClassifiedRecord = classifier.classify(getMatch("12BZC3", category = VehicleCategory.Unknown))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(7)
      passage.code.get must be(VehicleCode.VA)
    }

    "detect unknown" in {
      val passage: VehicleClassifiedRecord = classifier.classify(getMatch("12BZC3", category = VehicleCategory.Pers))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(7)
      passage.code must be(None)
      passage.alternativeClassification must be(Some(VehicleCode.VA))
    }
  }

  "MatchClassifier B heavy" must {
    val rdw = RdwData("BB12CD", vehicleClass = 20, deviceCode = 68, maxMass = 3501,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 0, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.3f, margin2 = 0.5f, mobiCountries = List("DE", "BE", "CH")), writer)

    "detect heavy" in {
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.code.get must be(VehicleCode.CB)
    }
  }

  "MatchClassifier B light" must {
    val rdw = RdwData("BB12CD", vehicleClass = 20, deviceCode = 1, maxMass = 3500,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.3f, margin2 = 0.5f, mobiCountries = List("DE", "BE", "CH")), writer)

    "detect light" in {
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.invalidRdwData must be(false)
      passage.code.get must be(VehicleCode.AB)
    }
  }

  "MatchClassifier A light" must {
    val rdw = RdwData("12WN34", vehicleClass = 20, deviceCode = 1, maxMass = 1300,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.3f, margin2 = 0.5f, mobiCountries = List("DE", "BE", "CH")), writer)

    "detect AS" in {
      val passage = classifier.classify(getMatch("12WN34", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.code.get must be(VehicleCode.AS)
    }
  }

  "MatchClassifier A heavy" must {
    val rdw = RdwData("12WN34", vehicleClass = 20, deviceCode = 1, maxMass = 4300,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.3f, margin2 = 0.5f, mobiCountries = List("DE", "BE", "CH")), writer)

    "detect AS" in {
      val passage = classifier.classify(getMatch("12WN34", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.code.get must be(VehicleCode.AO)
    }
  }

  "MatchClassifier O light" must {
    val rdw = RdwData("OL1234", vehicleClass = 20, deviceCode = 1, maxMass = 1300,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.3f, margin2 = 0.5f, mobiCountries = List("DE", "BE", "CH")), writer)

    "detect AS" in {
      val passage = classifier.classify(getMatch("OL1234", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.code.get must be(VehicleCode.AS)
    }
  }

  "MatchClassifier O heavy" must {
    val rdw = RdwData("OL1234", vehicleClass = 20, deviceCode = 1, maxMass = 4300,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.3f, margin2 = 0.5f, mobiCountries = List("DE", "BE", "CH")), writer)

    "detect AS" in {
      val passage = classifier.classify(getMatch("OL1234", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.code.get must be(VehicleCode.AO)
    }
  }

  "MatchClassifier C" must {
    val rdw = RdwData("12FZC3", vehicleClass = 20, deviceCode = 10, maxMass = 300,
      wheelBase = 1.2f, length = 1.4f, width = 100, maxSpeed = 25, diffMaxSpeed = 25, eegCategory = "L6")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.3f, margin2 = 0.5f, mobiCountries = List("DE", "BE", "CH")), writer)

    "detect BS for speed = 25" in {
      val passage = classifier.classify(getMatch("12FZC3", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(2)
      passage.code.get must be(VehicleCode.BS)
    }
  }

  "MatchClassifier C" must {
    val rdw = RdwData("12FZC3", vehicleClass = 20, deviceCode = 10, maxMass = 300,
      wheelBase = 1.2f, length = 1.4f, width = 100, maxSpeed = 45, diffMaxSpeed = 45, eegCategory = "L6")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.3f, margin2 = 0.5f, mobiCountries = List("DE", "BE", "CH")), writer)

    "detect BB for L6 category" in {
      val passage = classifier.classify(getMatch("12FZC3", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(2)
      passage.code.get must be(VehicleCode.BB)
    }
  }

  "MatchClassifier C" must {
    val rdw = RdwData("12FZC3", vehicleClass = 20, deviceCode = 10, maxMass = 300,
      wheelBase = 1.2f, length = 1.4f, width = 100, maxSpeed = 45, diffMaxSpeed = 45, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.3f, margin2 = 0.5f, mobiCountries = List("DE", "BE", "CH")), writer)

    "detect BF" in {
      val passage = classifier.classify(getMatch("12FZC3", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(2)
      passage.code.get must be(VehicleCode.BF)
    }
  }

  "MatchClassifier with high country confidence level" must {
    val rdw = RdwData("12FZC3", vehicleClass = 20, deviceCode = 10, maxMass = 300,
      wheelBase = 1.2f, length = 1.4f, width = 100, maxSpeed = 45, diffMaxSpeed = 45, eegCategory = "L6")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.5f, countryCodeConfidence = 99, mobiCountries = List("DE", "BE", "CH")), writer)

    "detect manual instead of mobi for mobi countries" in {
      val passage = classifier.classify(getMatch("12FZC3", category = VehicleCategory.Car, country = Some("BE")))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(7)
      passage.code.get must be(VehicleCode.MV)
    }
    "detect manual for similar country plates" in {
      val passage = classifier.classify(getMatch("12FZC3", category = VehicleCategory.Car, country = Some("SE")))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(7)
      passage.code.get must be(VehicleCode.MV)
    }
    "detect mobi for other country plates" in {
      val passage = classifier.classify(getMatch("12FZC3", category = VehicleCategory.Car, country = Some("FR")))
      passage.indicator must be(ProcessingIndicator.Mobi)
      passage.reason.mask must be(0)
      passage.code.get must be(VehicleCode.MV)
    }
  }

  "MatchClassifier B with invalid RDW data (no mass)" must {
    val rdw = RdwData("BB12CD", vehicleClass = 20, deviceCode = 1, maxMass = 0,
      wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(0.3f, 0.5f, mobiCountries = List("DE", "BE", "CH")), writer)

    "detect manual and report RDW error about mass" in {
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(2)
      passage.invalidRdwData must be(true)
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.CA)
      passage.manualAccordingToSpec must be(true)
    }
  }

  "MatchClassifier ZB, without RDW data" must {

    val classifier = new RdwMatchClassifier(new NoRdwRegistry(), ClassifierConfig(0.3f, 0.5f), writer)

    "report manual and error because RDW data is required" in {
      val passage = classifier.classify(getMatch("12BZC3", category = VehicleCategory.Car))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(2)
      passage.invalidRdwData must be(true)
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.VA)
      passage.manualAccordingToSpec must be(false)
    }
  }

  "MatchClassifier, without country code" must {
    val classifier = new RdwMatchClassifier(new NoRdwRegistry(), ClassifierConfig(0.3f, 0.5f), writer)
    val first = vreg("id1", 1000L, "g1", "12BZC3", None, Some(4.5F), None)
    val second = vreg("id2", 2000L, "g2", "12BZC3", Some(VehicleCategory.Car), None, None)
    val passage = classifier.classify(VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, ""))
    "not be classified" in {
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(7)
      passage.code.get must be(VehicleCode.MV)
      passage.manualAccordingToSpec must be(false)
    }
  }

  "MatchClassifier MIL, without measured length" must {

    "Pers can be classified if length is 0-0 and category filled" in {

      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), ClassifierConfig(margin1 = 0.3f, margin2 = 20.5f), writer)
      val first = vreg("id1", 1000L, "g1", "KW9999", None, Some(0F), Some("NL"))
      val second = vreg("id2", 2000L, "g2", "KW9999", Some(VehicleCategory.Car), None, Some("NL"))
      val passage = classifier.classify(VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, ""))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.code must be(Some(VehicleCode.CA))
      passage.mil must be(true)
      passage.manualAccordingToSpec must be(false)
    }
    "Truck not be classified if length is 0-0 and category filled" in {
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), ClassifierConfig(0.3f, 0.5f), writer)
      val first = vreg("id1", 1000L, "g1", "KW9999", None, Some(0F), Some("NL"))
      val second = vreg("id2", 2000L, "g2", "KW9999", Some(VehicleCategory.Truck), None, Some("NL"))
      val passage = classifier.classify(VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, ""))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.code must be(Some(VehicleCode.VA))
      passage.mil must be(true)
      passage.manualAccordingToSpec must be(false)
    }

    "not be classified if category is empty" in {
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), ClassifierConfig(0.3f, 0.5f), writer)
      val first = vreg("id1", 1000L, "g1", "KW9999", None, Some(0F), Some("NL"))
      val second = vreg("id2", 2000L, "g2", "KW9999", None, None, Some("NL"))
      val passage = classifier.classify(VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, ""))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(2)
      passage.code must be(Some(VehicleCode.CA))
      passage.mil must be(true)
      passage.manualAccordingToSpec must be(false)
    }
  }

  "MatchClassifier P, without measured length" must {
    val rdw = RdwData("GZ123C", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
      wheelBase = 2.0f, length = 11.6f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.3f, margin2 = 20.8f), writer)

    "Pers not be classified if length is 0-0 and category filled" in {
      val first = vreg("id1", 1000L, "g1", "GZ123C", None, Some(0F), Some("NL"))
      val second = vreg("id2", 2000L, "g2", "GZ123C", Some(VehicleCategory.Car), None, Some("NL"))
      val passage = classifier.classify(VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, ""))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.code must be(Some(VehicleCode.PA))
      passage.alternativeClassification must be(None)
      passage.mil must be(false) //it is not classified
      passage.manualAccordingToSpec must be(false)
    }
    "be classified even when length is 0-0" in {
      val first = vreg("id1", 1000L, "g1", "GZ123C", None, Some(0F), Some("NL"))
      val second = vreg("id2", 2000L, "g2", "GZ123C", Some(VehicleCategory.Car), None, Some("NL"))
      val passage = classifier.classify(VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, ""))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.code must be(Some(VehicleCode.PA))
      passage.alternativeClassification must be(None)
      passage.mil must be(false) //it is not classified
      passage.manualAccordingToSpec must be(false)
    }

    "be classified even when length is None" in {
      val first = vregis("id1", 1000L, "g1", "GZ123C", None, None, Some("NL"))
      val second = vregis("id2", 2000L, "g2", "GZ123C", Some(VehicleCategory.Car), None, Some("NL"))
      val passage = classifier.classify(VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, ""))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.code must be(Some(VehicleCode.PA))
      passage.alternativeClassification must be(None)
      passage.mil must be(false) //it is not classified
      passage.manualAccordingToSpec must be(false)
    }
  }

  "Classify Car with Rules" when {
    val rdw = RdwData("GZ123C", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
      wheelBase = 2.0f, length = 11.6f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")

    val classify = getClassify(rdwRegistry = new MockRdwRegistry(rdw), classifyConfidence = productionClassifyConfidence)
    val entryLength = ValueWithConfidence_Float(3F, 0)
    val exitCategory = ValueWithConfidence("Car", 90)
    def createEntry(length: Option[ValueWithConfidence_Float] = None) = vregisWithConfidence(id = "id1", timestamp = 1000L, gantry = "g1", license = "GZ123C", category = None, length = length, country = Some(ValueWithConfidence("NL", 100)))
    def createExit(category: Option[ValueWithConfidence[String]] = None) = vregisWithConfidence(id = "id2", timestamp = 2000L, gantry = "g2", license = "GZ123C", category = category, length = None, country = Some(ValueWithConfidence("NL", 100)))

    "length is None and" when {
      val entry = createEntry(length = None)
      "exit category confidence >= 90" in {
        (90 to 100).foreach {
          confidence ⇒
            val exit = createExit(category = Some(exitCategory.copy(confidence = confidence)))
            val passage = classify.classify(VehicleSpeedRecord(exit.eventId, entry, Some(exit), 2002, 100, ""))
            passage.indicator must be(ProcessingIndicator.Automatic)
            passage.code must be(Some(VehicleCode.PA))
            passage.alternativeClassification must be(None)
        }
      }

      "exit category confidence between 50-80" in {
        (50 to 80).foreach {
          confidence ⇒
            val exit = createExit(category = Some(exitCategory.copy(confidence = confidence)))
            val passage = classify.classify(VehicleSpeedRecord(exit.eventId, entry, Some(exit), 2002, 100, ""))

            passage.indicator must be(ProcessingIndicator.Automatic)
            passage.code must be(Some(VehicleCode.PA))
            passage.alternativeClassification must be(None)
        }
      }
      "exit category confidence between 10-40" in {
        (10 to 40).foreach {
          confidence ⇒
            val exit = createExit(category = Some(exitCategory.copy(confidence = confidence)))
            val passage = classify.classify(VehicleSpeedRecord(exit.eventId, entry, Some(exit), 2002, 100, ""))
            passage.indicator must be(ProcessingIndicator.Automatic)
            passage.code must be(Some(VehicleCode.PA))
            passage.alternativeClassification must be(None)
            passage.reason.mask must be(0)
        }
      }
      "exit category confidence is 0" in {
        (0 to 0).foreach {
          confidence ⇒
            val exit = createExit(category = Some(exitCategory.copy(confidence = confidence)))
            val passage = classify.classify(VehicleSpeedRecord(exit.eventId, entry, Some(exit), 2002, 100, ""))
            passage.indicator must be(ProcessingIndicator.Automatic)
            passage.code must be(Some(VehicleCode.PA))
            passage.alternativeClassification must be(None)
            passage.reason.mask must be(0)
        }
      }
    }
    "length is between 0 - 6 and" when {
      val entry = createEntry(length = Some(entryLength.copy(value = 5.0F)))
      "exit category confidence >= 90" in {
        (90 to 100).foreach {
          confidence ⇒
            val exit = createExit(category = Some(exitCategory.copy(confidence = confidence)))
            val passage = classify.classify(VehicleSpeedRecord(exit.eventId, entry, Some(exit), 2002, 100, ""))
            passage.indicator must be(ProcessingIndicator.Automatic)
            passage.code must be(Some(VehicleCode.PA))
            passage.alternativeClassification must be(None)
            passage.reason.mask must be(0)
        }
      }
      "exit category confidence between 50-89" in {
        (50 to 89).foreach {
          confidence ⇒
            val exit = createExit(category = Some(exitCategory.copy(confidence = confidence)))
            val passage = classify.classify(VehicleSpeedRecord(exit.eventId, entry, Some(exit), 2002, 100, ""))
            passage.indicator must be(ProcessingIndicator.Automatic)
            passage.code must be(Some(VehicleCode.PA))
            passage.alternativeClassification must be(None)
            passage.reason.mask must be(0)
        }
      }
      "exit category confidence between 10-40" in {
        (1 to 49).foreach {
          confidence ⇒
            val exit = createExit(category = Some(exitCategory.copy(confidence = confidence)))
            val passage = classify.classify(VehicleSpeedRecord(exit.eventId, entry, Some(exit), 2002, 100, ""))
            passage.indicator must be(ProcessingIndicator.Automatic)
            passage.code must be(Some(VehicleCode.PA))
            passage.alternativeClassification must be(None)
            passage.reason.mask must be(0)
        }
      }
    }
    "length is > 6 and" when {
      val entry = createEntry(length = Some(entryLength.copy(value = 6.1F)))
      "exit category confidence >= 90" in {
        (90 to 100).foreach {
          confidence ⇒
            val exit = createExit(category = Some(exitCategory.copy(confidence = confidence)))
            val passage = classify.classify(VehicleSpeedRecord(exit.eventId, entry, Some(exit), 2002, 100, ""))
            passage.indicator must be(ProcessingIndicator.Automatic)
            passage.code must be(Some(VehicleCode.PA))
            passage.alternativeClassification must be(None)
        }
      }
      "exit category confidence between 10-99" in {
        (10 to 99).foreach {
          confidence ⇒
            val exit = createExit(category = Some(exitCategory.copy(confidence = confidence)))
            val passage = classify.classify(VehicleSpeedRecord(exit.eventId, entry, Some(exit), 2002, 100, ""))
            passage.indicator must be(ProcessingIndicator.Automatic)
            passage.code must be(Some(VehicleCode.PA))
            passage.alternativeClassification must be(None)
            passage.reason.mask must be(0)
        }
      }
      "exit category confidence between 1-9" in {
        (1 to 9).foreach {
          confidence ⇒
            val exit = createExit(category = Some(exitCategory.copy(confidence = confidence)))
            val passage = classify.classify(VehicleSpeedRecord(exit.eventId, entry, Some(exit), 2002, 100, ""))
            passage.indicator must be(ProcessingIndicator.Manual)
            passage.code must be(Some(VehicleCode.PA))
            passage.alternativeClassification must be(None)
            passage.reason.mask must be(2)
        }
      }
      "exit category confidence is 0" in {
        (0 to 0).foreach {
          confidence ⇒
            val exit = createExit(category = Some(exitCategory.copy(confidence = confidence)))
            val passage = classify.classify(VehicleSpeedRecord(exit.eventId, entry, Some(exit), 2002, 100, ""))
            passage.indicator must be(ProcessingIndicator.Manual)
            passage.code must be(Some(VehicleCode.PA))
            passage.reason.mask must be(2)
        }
      }
    }
  }

  "Classify 'CarTrailer' with Rules" when {
    val rdw = RdwData("1VBC23", vehicleClass = 20, deviceCode = 10, maxMass = 2500,
      wheelBase = 2.0f, length = 10.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")

    val classify = getClassify(rdwRegistry = new MockRdwRegistry(rdw), classifyConfidence = productionClassifyConfidence)
    val entryLength = ValueWithConfidence_Float(3F, 0)
    val exitCategory = ValueWithConfidence("CarTrailer", 90)
    def createEntry(length: Option[ValueWithConfidence_Float] = None) = vregisWithConfidence(id = "id1", timestamp = 1000L, gantry = "g1", license = "1VBC23", category = None, length = length, country = Some(ValueWithConfidence("NL", 100)))
    def createExit(category: Option[ValueWithConfidence[String]] = None) = vregisWithConfidence(id = "id2", timestamp = 2000L, gantry = "g2", license = "1VBC23", category = category, length = None, country = Some(ValueWithConfidence("NL", 100)))

    "length is None and" when {
      val entry = createEntry(length = None)
      "exit category confidence between 0-49" in {
        (0 to 49).foreach {
          confidence ⇒
            val exit = createExit(category = Some(exitCategory.copy(confidence = confidence)))
            val passage = classify.classify(VehicleSpeedRecord(exit.eventId, entry, Some(exit), 2002, 100, ""))
            passage.indicator must be(ProcessingIndicator.Manual)
            passage.code must be(Some(VehicleCode.AS))
            passage.alternativeClassification must be(None)
        }
      }
      "exit category confidence between 50-100" in {
        (50 to 100).foreach {
          confidence ⇒
            val exit = createExit(category = Some(exitCategory.copy(confidence = confidence)))
            val passage = classify.classify(VehicleSpeedRecord(exit.eventId, entry, Some(exit), 2002, 100, ""))
            passage.indicator must be(ProcessingIndicator.Manual)
            passage.code must be(Some(VehicleCode.AS))
            passage.alternativeClassification must be(None)
        }
      }
    }
    "length is between 0 - 5.6 and" when {
      val entry = createEntry(length = Some(entryLength.copy(value = 5.6F)))
      "exit category confidence between 0-89" in {
        (0 to 89).foreach {
          confidence ⇒
            val exit = createExit(category = Some(exitCategory.copy(confidence = confidence)))
            val passage = classify.classify(VehicleSpeedRecord(exit.eventId, entry, Some(exit), 2002, 100, ""))
            passage.indicator must be(ProcessingIndicator.Manual)
            passage.code must be(Some(VehicleCode.AS))
            passage.alternativeClassification must be(None)
        }
      }
      "exit category confidence >= 90" in {
        (90 to 100).foreach {
          confidence ⇒
            val exit = createExit(category = Some(exitCategory.copy(confidence = confidence)))
            val passage = classify.classify(VehicleSpeedRecord(exit.eventId, entry, Some(exit), 2002, 100, ""))
            passage.indicator must be(ProcessingIndicator.Manual)
            passage.code must be(Some(VehicleCode.AS))
            passage.alternativeClassification must be(None)
        }
      }
    }
    "length is > 6 and" when {
      val entry = createEntry(length = Some(entryLength.copy(value = 6.1F)))
      "exit category confidence 0-100" in {
        (0 to 100).foreach {
          confidence ⇒
            val exit = createExit(category = Some(exitCategory.copy(confidence = confidence)))
            val passage = classify.classify(VehicleSpeedRecord(exit.eventId, entry, Some(exit), 2002, 100, ""))
            passage.indicator must be(ProcessingIndicator.Manual)
            passage.code must be(Some(VehicleCode.AS))
            passage.alternativeClassification must be(None)
        }
      }
    }
  }

  "extended unittest with specific license plates" must {
    "detect 80WJLS" in {
      val rdw = RdwData("80WJLS", vehicleClass = 6, deviceCode = 26, maxMass = 30000,
        wheelBase = 6.81f, length = 11.96f, width = 255, maxSpeed = 0, diffMaxSpeed = 0, eegCategory = "04")

      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.3f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("80WJLS", length = Some(15f), category = VehicleCategory.CarTrailer))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.code must be(Some(VehicleCode.AO))
      passage.alternativeClassification must be(None)
      passage.reason.mask must be(0)

    }
    "detect 62WJJZ" in {
      val rdw = RdwData("62WJJZ", vehicleClass = 6, deviceCode = 69, maxMass = 2000,
        wheelBase = 4.63f, length = 8.02f, width = 255, maxSpeed = 0, diffMaxSpeed = 0, eegCategory = "02")

      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.3f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("62WJJZ", length = Some(15f), category = VehicleCategory.CarTrailer))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.code must be(Some(VehicleCode.AS))
      passage.alternativeClassification must be(None)
      passage.reason.mask must be(0)

    }
  }

  def getMatch(plate: String, category: VehicleCategory.Value = VehicleCategory.Car, length: Option[Float] = None, country: Option[String] = Some("NL")): VehicleSpeedRecord = {
    val first = vreg("id1", 1000L, "g1", plate, None, length, country)
    val second = vreg("id2", 2000L, "g2", plate, Some(category), None, country)
    VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")
  }

  def getCountry(passage: VehicleClassifiedRecord): String = {
    passage.speedRecord.exit.get.country.get.value
  }

  "Float 0" must {
    "be compared to 0" in {
      val fl1 = 5 / 3 - 5 / 3
      fl1 must be(0.0f)
    }
    "be equal to 0" in {
      val fl1 = 5 / 3 - 5 / 3
      fl1 == 0 must be(true)
    }
    "be matched to 0" in {
      val fl1 = 5 / 3 - 5 / 3
      val result = fl1 match {
        case 0 ⇒ true
        case _ ⇒ false
      }
      result must be(true)
    }
  }

  def vreg(id: String, timestamp: Long, gantry: String, license: String, category: Option[VehicleCategory.Value], length: Option[Float], country: Option[String]): VehicleMetadata = {
    vregis(id, timestamp, gantry, license, category, length, country)
  }

  def vregis(id: String, timestamp: Long, gantry: String, license: String, category: Option[VehicleCategory.Value], length: Option[Float], country: Option[String]): VehicleMetadata = {
    val lane = Lane("23L", "foo", gantry, "route1", 25f, 37f)
    val lic = Some(ValueWithConfidence(license, 90))
    val cou = country match {
      case None    ⇒ None
      case Some(c) ⇒ Some(ValueWithConfidence(c, 95))
    }
    VehicleMetadata(lane, id, timestamp, Some(new Date(timestamp).toString), lic, None, length.map(new ValueWithConfidence_Float(_, 60)), category.map(cat ⇒ new ValueWithConfidence[String](cat.toString(), 90)), None, cou, Seq(), "", "", "SN1234")
  }

  def productionClassifyConfidence: ZkClassifyConfidence = {
    ZkClassifyConfidence(
      minCarLength = 0.1F,
      maxCarLength = 6F,
      maxVanLength = 6.4F,
      minTrailerLength = 5.6F,
      minLargeTruckLength = 12.2F,
      carConfidence = 10,
      trailerConfidence = 10,
      processCategory = "TDC3-TDC1",
      confidenceLimit = 0)
  }

  def getClassify(rdwRegistry: RdwRegistry = new NoRdwRegistry(), classifyConfidence: ZkClassifyConfidence) = {
    new RdwMatchClassifier(rdwRegistry, ClassifierConfig(margin1 = 0.3f, margin2 = 0.5f, classifyConfidence = classifyConfidence), writer)
  }

  def vregisWithConfidence(id: String, timestamp: Long, gantry: String, license: String, category: Option[ValueWithConfidence[String]], length: Option[ValueWithConfidence_Float], country: Option[ValueWithConfidence[String]]): VehicleMetadata = {
    VehicleMetadata(
      lane = Lane("23L", "foo", gantry, "route1", 25f, 37f),
      eventId = id,
      eventTimestamp = timestamp,
      eventTimestampStr = Some(new Date(timestamp).toString),
      license = Some(ValueWithConfidence(license, 90)),
      rawLicense = None,
      length = length,
      category = category,
      speed = None,
      country = country,
      images = Seq(),
      NMICertificate = "",
      applChecksum = "",
      serialNr = "SN1234")
  }
}
