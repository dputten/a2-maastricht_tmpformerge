/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.classify.nl.category

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.messages._
import java.util.Date
import csc.sectioncontrol.messages.VehicleSpeedRecord
import csc.sectioncontrol.messages.ValueWithConfidence
import csc.sectioncontrol.storage.ZkClassifyConfidence
import csc.sectioncontrol.messages.Lane
import csc.sectioncontrol.messages.VehicleMetadata

class ProcessEntryExitTDC3Test extends WordSpec with MustMatchers {
  val config = ZkClassifyConfidence()
  "ProcessCategory" must {
    "return equal Car" in {
      val first = vregEntry(Some(VehicleCategory.Car), 0)
      val second = vregExit(Some(VehicleCategory.Car), 0)
      val record = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val result = ProcessEntryExitTDC3.getScannedCategory(config, record)
      result.category must be(VehicleCategory.Car)
      result.isReliable must be(Some(true))
    }
    "return equal CarTrailer" in {
      val first = vregEntry(Some(VehicleCategory.CarTrailer), 50)
      val second = vregExit(Some(VehicleCategory.CarTrailer), 0)
      val record = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val result = ProcessEntryExitTDC3.getScannedCategory(config, record)
      result.category must be(VehicleCategory.CarTrailer)
      result.isReliable must be(Some(true))
    }
    "return entry: Exit missing entry with high conf" in {
      val first = vregEntry(Some(VehicleCategory.CarTrailer), 50)
      val second = vregExit(None, 0)
      val record = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val result = ProcessEntryExitTDC3.getScannedCategory(config, record)
      result.category must be(VehicleCategory.CarTrailer)
      result.isReliable must be(Some(true))
    }
    "return entry: Exit missing entry with low conf" in {
      val first = vregEntry(Some(VehicleCategory.CarTrailer), 0)
      val second = vregExit(None, 0)
      val record = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val result = ProcessEntryExitTDC3.getScannedCategory(config, record)
      result.category must be(VehicleCategory.CarTrailer)
      result.isReliable must be(Some(false))
    }
    "return exit: Entry missing exit with high conf" in {
      val first = vregEntry(None, 0)
      val second = vregExit(Some(VehicleCategory.Car), 50)
      val record = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val result = ProcessEntryExitTDC3.getScannedCategory(config, record)
      result.category must be(VehicleCategory.Car)
      result.isReliable must be(Some(true))
    }
    "return exit: Entry missing entry with low conf" in {
      val first = vregEntry(None, 0)
      val second = vregExit(Some(VehicleCategory.Car), 0)
      val record = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val result = ProcessEntryExitTDC3.getScannedCategory(config, record)
      result.category must be(VehicleCategory.Car)
      result.isReliable must be(Some(false))
    }
    "return unknown: Entry and exit missing" in {
      val first = vregEntry(None, 0)
      val second = vregExit(None, 0)
      val record = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val result = ProcessEntryExitTDC3.getScannedCategory(config, record)
      result.category must be(VehicleCategory.Unknown)
      result.isReliable must be(None)
    }
    "return result car Entry Car conf high Exit CarTrailer conf high" in {
      val first = vregEntry(Some(VehicleCategory.Car), 70)
      val second = vregExit(Some(VehicleCategory.CarTrailer), 70)
      val record = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val result = ProcessEntryExitTDC3.getScannedCategory(config, record)
      result.category must be(VehicleCategory.Car)
      result.isReliable must be(Some(true))
    }
    "return result car Entry Car conf low Exit CarTrailer conf high" in {
      val first = vregEntry(Some(VehicleCategory.Car), 0)
      val second = vregExit(Some(VehicleCategory.CarTrailer), 70)
      val record = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val result = ProcessEntryExitTDC3.getScannedCategory(config, record)
      result.category must be(VehicleCategory.Car)
      result.isReliable must be(Some(false))
    }
    "return result Van Entry Truck conf high Exit Van conf high" in {
      val first = vregEntry(Some(VehicleCategory.Truck), 70)
      val second = vregExit(Some(VehicleCategory.Van), 70)
      val record = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val result = ProcessEntryExitTDC3.getScannedCategory(config, record)
      result.category must be(VehicleCategory.Van)
      result.isReliable must be(Some(true))
    }
    "return result Van Entry Truck conf high Exit Van conf low" in {
      val first = vregEntry(Some(VehicleCategory.Truck), 70)
      val second = vregExit(Some(VehicleCategory.Van), 0)
      val record = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val result = ProcessEntryExitTDC3.getScannedCategory(config, record)
      result.category must be(VehicleCategory.Van)
      result.isReliable must be(Some(false))
    }
    "return result CarTrailer Entry Truck conf high Exit CarTrailer conf high" in {
      val first = vregEntry(Some(VehicleCategory.Truck), 70)
      val second = vregExit(Some(VehicleCategory.CarTrailer), 70)
      val record = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val result = ProcessEntryExitTDC3.getScannedCategory(config, record)
      result.category must be(VehicleCategory.CarTrailer)
      result.isReliable must be(Some(true))
    }
    "return result truck Entry Truck conf high Exit CarTrailer conf low" in {
      val first = vregEntry(Some(VehicleCategory.Truck), 70)
      val second = vregExit(Some(VehicleCategory.CarTrailer), 0)
      val record = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val result = ProcessEntryExitTDC3.getScannedCategory(config, record)
      result.category must be(VehicleCategory.Truck)
      result.isReliable must be(Some(true))
    }
    "return result CarTrailer Entry Truck conf low Exit CarTrailer conf low" in {
      val first = vregEntry(Some(VehicleCategory.Truck), 0)
      val second = vregExit(Some(VehicleCategory.CarTrailer), 0)
      val record = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val result = ProcessEntryExitTDC3.getScannedCategory(config, record)
      result.category must be(VehicleCategory.CarTrailer)
      result.isReliable must be(Some(false))
    }
  }

  def vregEntry(cat: Option[VehicleCategory.Value], confidence: Int): VehicleMetadata = {
    val lane = Lane("23L", "foo", "E1", "route1", 25f, 37f)
    val lic = Some(ValueWithConfidence("12XYZ3", 90))
    val cou = Some(ValueWithConfidence("NL", 95))
    val length = Some(new ValueWithConfidence_Float(5.2F, 60))
    val category = cat.map(catVal ⇒ new ValueWithConfidence[String](catVal.toString(), confidence))
    val now = System.currentTimeMillis()
    VehicleMetadata(lane, "ENTRY", now, Some(new Date(now).toString), lic, None, length, category, None, cou, Seq(), "", "", "SN1234")
  }

  def vregExit(cat: Option[VehicleCategory.Value], confidence: Int): VehicleMetadata = {
    val lane = Lane("23L", "foo", "X1", "route1", 25f, 37f)
    val lic = Some(ValueWithConfidence("12XYZ3", 90))
    val cou = Some(ValueWithConfidence("NL", 95))
    val length = Some(new ValueWithConfidence_Float(5.2F, 60))
    val category = cat.map(catVal ⇒ new ValueWithConfidence[String](catVal.toString(), confidence))
    val now = System.currentTimeMillis() + 300000
    VehicleMetadata(lane, "Exit", now, Some(new Date(now).toString), lic, None, length, category, None, cou, Seq(), "", "", "SN1234")
  }
}