/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.classify.nl.amqp

import akka.testkit.{ TestProbe, TestKit }
import akka.actor.{ Props, ActorSystem }
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.messages._
import akka.util.duration._
import csc.sectioncontrol.messages.ValueWithConfidence
import csc.sectioncontrol.messages.ValueWithConfidence_Float
import csc.sectioncontrol.messages.VehicleSpeedRecord
import csc.sectioncontrol.messages.VehicleImage
import csc.sectioncontrol.messages.Lane
import com.github.sstone.amqp.Amqp.{ Delivery, Ok, Publish }
import csc.sectioncontrol.storagelayer.hbasetables.SerializerFormats
import csc.amqp.AmqpSerializers
import csc.akkautils.DirectLoggingAdapter

class ClassifyProducerTest extends TestKit(ActorSystem("ClassifyProducerTest")) with AmqpSerializers with WordSpec with MustMatchers with BeforeAndAfterAll {
  implicit val formats = SerializerFormats.formats
  val ApplicationId = "test"
  val log = new DirectLoggingAdapter(this.getClass.getName)

  override def afterAll() {
    super.afterAll()
    system.shutdown()
  }

  "ClassifyProducer" must {
    "create Publish message" in {
      val output = TestProbe()
      val producerEndpoint = "exchange"
      val prod = system.actorOf(Props(new ClassifyProducer(output.ref, producerEndpoint)))
      val record = createRecord()
      prod ! record

      val msg = output.expectMsgType[Publish](1.hour) //.second)
      output.send(output.sender, Ok)
      msg.exchange must be(producerEndpoint)
      msg.key must be("A2.11.VehicleClassifiedRecord")
      msg.properties.get.getType must be("VehicleClassifiedRecord")

      val builder = buildProperties(manifest[VehicleClassifiedRecord].erasure.getSimpleName)
      val delivery = Delivery("consumerTag", null, builder.build(), msg.body)
      val result = toMessage[VehicleClassifiedRecord](delivery)
      result.isRight must be(true)
      result.right.get must be(record)
    }
  }

  def createRecord(): VehicleClassifiedRecord = {
    val entryLane = Lane(
      laneId = "A2-E1-R1",
      name = "R1",
      gantry = "E1",
      system = "A2",
      sensorGPS_longitude = 52.4,
      sensorGPS_latitude = 5.4)

    val entryImages = Seq(
      VehicleImage(timestamp = 1438671100000L,
        offset = 0,
        uri = "overview",
        imageType = VehicleImageType.Overview,
        checksum = "ABCDEF"))

    val entryData: VehicleMetadata = new VehicleMetadata(
      lane = entryLane,
      eventId = "A2-E1-R1-00001",
      eventTimestamp = 1438671100000L,
      eventTimestampStr = None,
      license = Some(ValueWithConfidence("1234AB", 80)),
      rawLicense = Some("1234AB"),
      length = Some(ValueWithConfidence_Float(4.3F, 20)),
      category = Some(ValueWithConfidence("Car", 80)),
      speed = Some(ValueWithConfidence_Float(125F, 50)),
      country = Some(ValueWithConfidence("NL", 60)),
      images = entryImages,
      NMICertificate = "PD1234",
      applChecksum = "1234ABC",
      serialNr = "SN1234",
      recognizedBy = Some("ARH"))

    val exitData = VehicleMetadata(
      lane = Lane(
        laneId = "A2-X1-R1",
        name = "R1",
        gantry = "X1",
        system = "A2",
        sensorGPS_longitude = 52.4,
        sensorGPS_latitude = 5.4),
      eventId = "A2-X1-R1-00001",
      eventTimestamp = 1438671100000L,
      eventTimestampStr = None,
      license = Some(ValueWithConfidence("1234AB", 80)),
      rawLicense = Some("1234AB"),
      length = Some(ValueWithConfidence_Float(4.3F, 20)),
      category = Some(ValueWithConfidence("Car", 80)),
      speed = Some(ValueWithConfidence_Float(125F, 50)),
      country = Some(ValueWithConfidence("NL", 60)),
      images = Seq(
        VehicleImage(timestamp = 1438671100000L,
          offset = 0,
          uri = "overview",
          imageType = VehicleImageType.Overview,
          checksum = "ABCDEF")),
      NMICertificate = "PD1234",
      applChecksum = "1234ABC",
      serialNr = "SN1234",
      recognizedBy = Some("ARH"))

    val speed = VehicleSpeedRecord(id = "A2-S1-0000-R1",
      entry = entryData,
      exit = Some(exitData),
      corridorId = 11,
      speed = 12,
      measurementSHA = "1234ABCDFE")

    VehicleClassifiedRecord(id = speed.id,
      speedRecord = speed,
      code = Some(VehicleCode.PA),
      indicator = ProcessingIndicator.Automatic,
      mil = false,
      invalidRdwData = false,
      validLicensePlate = entryData.license.map(_.value),
      manualAccordingToSpec = false,
      reason = IndicatorReason(),
      alternativeClassification = None,
      dambord = Some("P"))
  }

}