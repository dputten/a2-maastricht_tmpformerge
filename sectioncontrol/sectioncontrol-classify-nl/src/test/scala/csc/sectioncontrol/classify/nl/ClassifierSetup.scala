/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.classify.nl

import csc.config.Path
import csc.curator.utils.Curator
import csc.akkautils.{ DirectLoggingAdapter, JobSchedule }
import csc.sectioncontrol.storage._
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.messages.VehicleCode
import net.liftweb.json.DefaultFormats

/**
 * Utility class to load a basic zookeeper config for testing the Matcher boot.
 * Also loads the necessary configuration for vehicle-registration to run.
 *
 * @author Maarten Hazewinkel
 */
object ClassifierSetup extends App {
  val log = new DirectLoggingAdapter(this.getClass.getName)
  val curator = Curator.createCuratorWithLogger(None, log, extraFormats = DefaultFormats + new EnumerationSerializer(ZkWheelbaseType,
    ConfigType))

  def saveZk[T <: AnyRef](p: String, v: T) {
    curator.put(Path(p), v)
  }

  val systemId = "A2"

  val p = ZkPreference(List(ZkWheelBase(ZkWheelbaseType.LBB2M, 2.5),
    ZkWheelBase(ZkWheelbaseType.LBB2M3M, 4.0),
    ZkWheelBase(ZkWheelbaseType.LBB3M4M, 5.0),
    ZkWheelBase(ZkWheelbaseType.LBB4M5M, 6.0),
    ZkWheelBase(ZkWheelbaseType.LBB5M, 7.5),
    ZkWheelBase(ZkWheelbaseType.LBB5M, 7.5),
    ZkWheelBase(ZkWheelbaseType.P2M, 2.0),
    ZkWheelBase(ZkWheelbaseType.P2M3M, 3.5),
    ZkWheelBase(ZkWheelbaseType.P3M4M, 4.5),
    ZkWheelBase(ZkWheelbaseType.P4M5M, 5.5),
    ZkWheelBase(ZkWheelbaseType.P5M, 7.0)),
    List(ZkVehicleMaxSpeed(VehicleCode.AB, 80),
      ZkVehicleMaxSpeed(VehicleCode.AO, 80),
      ZkVehicleMaxSpeed(VehicleCode.AS, 90),
      ZkVehicleMaxSpeed(VehicleCode.AT, 100),
      ZkVehicleMaxSpeed(VehicleCode.CA, 120),
      ZkVehicleMaxSpeed(VehicleCode.CB, 80),
      ZkVehicleMaxSpeed(VehicleCode.CP, 120),
      ZkVehicleMaxSpeed(VehicleCode.MF, 120),
      ZkVehicleMaxSpeed(VehicleCode.MV, 120),
      ZkVehicleMaxSpeed(VehicleCode.PA, 130),
      ZkVehicleMaxSpeed(VehicleCode.VA, 80)),
    List(ZkSpeedLimitMargin(30, 7),
      ZkSpeedLimitMargin(40, 7),
      ZkSpeedLimitMargin(50, 7),
      ZkSpeedLimitMargin(60, 7),
      ZkSpeedLimitMargin(70, 7),
      ZkSpeedLimitMargin(80, 7),
      ZkSpeedLimitMargin(90, 7),
      ZkSpeedLimitMargin(100, 8),
      ZkSpeedLimitMargin(110, 8),
      ZkSpeedLimitMargin(120, 8),
      ZkSpeedLimitMargin(130, 8)),
    1000, 40, 195, 50, reliableLicenseLimit = 80)
  saveZk(Paths.Preferences.getConfigPath(systemId), p)

  val j = JobSchedule(true, 0, None, runAtMillisInterval = Some(300000L), timeZone = "Europe/Amsterdam")
  saveZk(Paths.Systems.getSystemJobsPath(systemId) + "/classifyJob-nl", j)

  saveZk(Paths.Configuration.getClassifyCountry, ZkClassifyCountry())
}

