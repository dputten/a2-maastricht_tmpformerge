package csc.sectioncontrol.classify.nl

/**
 * Created by dputten on 4/8/14.
 */

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.messages._
import csc.sectioncontrol.rdwregistry.RdwData
import csc.akkautils.DirectLogging
import java.util.Date
import csc.sectioncontrol.storage.ZkWrongReadDutchLicensePlate
import csc.sectioncontrol.storagelayer.hbasetables.DecisionResultTable

class RdwMatchWrongLicense extends WordSpec with MustMatchers with DirectLogging {
  val writer = new MockClassifyDecisionLogWriter()

  val wrongLicense = List(ZkWrongReadDutchLicensePlate("AA11BB", true, true),
    ZkWrongReadDutchLicensePlate("11DBBD", true, true),
    ZkWrongReadDutchLicensePlate("11DBBT", true, false),
    ZkWrongReadDutchLicensePlate("OD04SL", false, false),
    ZkWrongReadDutchLicensePlate("63WBNS", false, true),
    ZkWrongReadDutchLicensePlate("VV9999", false, false))

  "Classifier good read license" must {
    val config = ClassifierConfig(0.4f, 0.5f,
      wrongReadLicense = wrongLicense)

    val classifier = new RdwMatchClassifier(new MockRdwRegistry(RdwData("GZ123C", 25, 3, 2500, 3.0f, 2.0f, 1500, 150, 160, "LF")), config, writer)

    "Good read license plate" in {
      // Some passage
      val passage = classifier.classify(getMatch("GZ123C", length = Some(4.5F)))

      // The country of the match must be NL
      getCountry(passage) must be("NL")

      //The code must be PA
      passage.code must be(Some(VehicleCode.PA))

      // Match goes to automatic
      passage.indicator must be(ProcessingIndicator.Automatic)

      // Automatic ==> reason is 0
      passage.reason.mask must be(0)

      // This is not a MIL vehicle
      passage.mil must be(false)

      //
      passage.validLicensePlate must be(None)

      // The vehicle doesn't go to manual as according specs
      passage.manualAccordingToSpec must be(false)
    }
  }

  "Classifier wrong read license with Car,Car" must {
    val config = ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f,
      wrongReadLicense = wrongLicense)

    val rdwDataRow = RdwData("11DBBD",
      vehicleClass = 1,
      deviceCode = 44,
      maxMass = 1450,
      wheelBase = 239,
      length = 381,
      width = 249,
      maxSpeed = 0,
      diffMaxSpeed = 0,
      dublicateCode = 0,
      eegCategory = "M1")

    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdwDataRow), config, writer)

    "Orig Car parsing" in {
      // Some passage
      val passage = classifier.classify(getMatch("11DBBD", category = VehicleCategory.Car, length = Some(3.8F)))

      // The country of the match must be NL
      getCountry(passage) must be("NL")

      //The code must be VA
      passage.code must be(Some(VehicleCode.PA))

      // Match goes to automatic
      passage.indicator must be(ProcessingIndicator.Manual)

      // Automatic ==> reason is 0
      passage.reason.mask must be(7)

      // KW9999 is a MIL vehicle
      passage.mil must be(false)

      //
      passage.validLicensePlate must be(None)

      // The vehicle doesn't go to manual as according specs
      passage.manualAccordingToSpec must be(false)
    }

    "Other PersonCar parsing" in {
      // Some passage
      val passage = classifier.classify(getMatch("11DBBD", category = VehicleCategory.Car, length = Some(4.3F)))

      // The country of the match must be NL
      getCountry(passage) must be("NL")

      //The code must be PA
      passage.code must be(Some(VehicleCode.PA))

      // Match goes to automatic
      passage.indicator must be(ProcessingIndicator.Manual)

      // Automatic ==> reason is 0
      passage.reason.mask must be(7)

      // KW9999 is a MIL vehicle
      passage.mil must be(false)

      //
      passage.validLicensePlate must be(None)

      // The vehicle doesn't go to manual as according specs
      passage.manualAccordingToSpec must be(false)
    }
  }

  "Classifier wrong read license with Car,TruckTrailer" must {
    val config = ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f,
      wrongReadLicense = wrongLicense)

    val rdwDataRow = RdwData("11DBBT",
      vehicleClass = 1,
      deviceCode = 44,
      maxMass = 1320,
      wheelBase = 237,
      length = 376,
      width = 0,
      maxSpeed = 0,
      diffMaxSpeed = 0,
      dublicateCode = 0,
      eegCategory = "M1")

    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdwDataRow), config, writer)

    "Orig Car parsing" in {
      // Some passage
      val passage = classifier.classify(getMatch("11DBBT", category = VehicleCategory.Car, length = Some(3.8F)))

      // The country of the match must be NL
      getCountry(passage) must be("NL")

      //The code must be PA
      passage.code must be(Some(VehicleCode.PA))

      // Match goes to automatic
      passage.indicator must be(ProcessingIndicator.Manual)

      // Automatic ==> reason is 0
      passage.reason.mask must not be (0)

      // KW9999 is a MIL vehicle
      passage.mil must be(false)

      //
      passage.validLicensePlate must be(None)

      // The vehicle doesn't go to manual as according specs
      passage.manualAccordingToSpec must be(false)
    }

    "Other TruckTrailer parsing" in {
      // Some passage
      val passage = classifier.classify(getMatch("11DBBT", category = VehicleCategory.TruckTrailer, length = Some(18.3F)))

      // The country of the match must be NL
      getCountry(passage) must be("NL")

      //The code must be VA
      passage.code must be(Some(VehicleCode.VA))

      // Match goes to automatic
      passage.indicator must be(ProcessingIndicator.Manual)

      // Automatic ==> reason is 0
      passage.reason.mask must not be (0)

      // KW9999 is a MIL vehicle
      passage.mil must be(false)

      //
      passage.validLicensePlate must be(None)

      // The vehicle doesn't go to manual as according specs
      passage.manualAccordingToSpec must be(false)
    }
  }

  "Classifier wrong read license with TruckTrailer,Car" must {
    val config = ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f,
      wrongReadLicense = wrongLicense)

    val rdwDataRow = RdwData("63WBNS",
      vehicleClass = 7,
      deviceCode = 26,
      maxMass = 45000,
      wheelBase = 930,
      length = 1280,
      width = 249,
      maxSpeed = 0,
      diffMaxSpeed = 0,
      dublicateCode = 1,
      eegCategory = "O4")

    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdwDataRow), config, writer)

    "Orig TruckTrailer parsing" in {
      // Some passage
      val passage = classifier.classify(getMatch("63WBNS", category = VehicleCategory.TruckTrailer, length = Some(18F)))

      // The country of the match must be NL
      getCountry(passage) must be("NL")

      //The code must be VA
      passage.code must be(Some(VehicleCode.VA))

      // Match goes to automatic
      passage.indicator must be(ProcessingIndicator.Manual)

      // Automatic ==> reason is 0
      passage.reason.mask must be(7)

      // KW9999 is a MIL vehicle
      passage.mil must be(false)

      //
      passage.validLicensePlate must be(None)

      // The vehicle doesn't go to manual as according specs
      passage.manualAccordingToSpec must be(false)
    }

    "Other PersonCar parsing" in {
      // Some passage
      val passage = classifier.classify(getMatch("63WBNS", category = VehicleCategory.Car, length = Some(4.3F)))

      // The country of the match must be NL
      getCountry(passage) must be("NL")

      //The code must be PA
      passage.code must be(Some(VehicleCode.PA))

      // Match goes to automatic
      passage.indicator must be(ProcessingIndicator.Manual)

      // Automatic ==> reason is 0
      passage.reason.mask must be(7)

      // KW9999 is a MIL vehicle
      passage.mil must be(false)

      //
      passage.validLicensePlate must be(None)

      // The vehicle doesn't go to manual as according specs
      passage.manualAccordingToSpec must be(false)
    }
  }

  "Classifier wrong read license with TruckTrailer,TruckTrailer" must {
    val config = ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f,
      wrongReadLicense = wrongLicense)

    val rdwDataRow = RdwData("OD04SL",
      vehicleClass = 7,
      deviceCode = 26,
      maxMass = 45000,
      wheelBase = 899,
      length = 1260,
      width = 249,
      maxSpeed = 0,
      diffMaxSpeed = 0,
      dublicateCode = 1,
      eegCategory = "O4")

    val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdwDataRow), config, writer)

    "Orig TruckTrailer parsing" in {
      // Some passage
      val passage = classifier.classify(getMatch("OD04SL", category = VehicleCategory.TruckTrailer, length = Some(18F)))

      // The country of the match must be NL
      getCountry(passage) must be("NL")

      //The code must be VA
      passage.code must be(Some(VehicleCode.VA))

      // Match goes to automatic
      passage.indicator must be(ProcessingIndicator.Manual)

      // Automatic ==> reason is 0
      passage.reason.mask must be(7)

      // KW9999 is a MIL vehicle
      passage.mil must be(false)

      //
      passage.validLicensePlate must be(None)

      // The vehicle doesn't go to manual as according specs
      passage.manualAccordingToSpec must be(false)
    }

    "Other TruckTrailer parsing" in {
      // Some passage
      val passage = classifier.classify(getMatch("OD04SL", category = VehicleCategory.TruckTrailer, length = Some(24.3F)))

      // The country of the match must be NL
      getCountry(passage) must be("NL")

      //The code must be VA
      passage.code must be(Some(VehicleCode.VA))

      // Match goes to automatic
      passage.indicator must be(ProcessingIndicator.Manual)

      // Automatic ==> reason is 0
      passage.reason.mask must be(7)

      // KW9999 is a MIL vehicle
      passage.mil must be(false)

      //
      passage.validLicensePlate must be(None)

      // The vehicle doesn't go to manual as according specs
      passage.manualAccordingToSpec must be(false)
    }
  }

  def getMatch(plate: String,
               category: VehicleCategory.Value = VehicleCategory.Car,
               length: Option[Float] = None,
               country: Option[String] = Some("NL")): VehicleSpeedRecord = {
    val first = vreg("id1", 1000L, "g1", plate, None, length, country)
    val second = vreg("id2", 2000L, "g2", plate, Some(category), None, country)
    VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")
  }

  def getCountry(passage: VehicleClassifiedRecord): String = {
    passage.speedRecord.exit.get.country.get.value
  }

  def vreg(id: String, timestamp: Long, gantry: String, license: String, category: Option[VehicleCategory.Value], length: Option[Float], country: Option[String]): VehicleMetadata = {
    vregis(id, timestamp, gantry, license, category, length, country)
  }

  def vregis(id: String, timestamp: Long, gantry: String, license: String, category: Option[VehicleCategory.Value], length: Option[Float], country: Option[String]): VehicleMetadata = {
    val lane = Lane("23L", "foo", gantry, "route1", 25f, 37f)
    val lic = Some(ValueWithConfidence(license, 90))
    val cou = country match {
      case None    ⇒ None
      case Some(c) ⇒ Some(ValueWithConfidence(c, 95))
    }
    VehicleMetadata(lane, id, timestamp, Some(new Date(timestamp).toString), lic, None, length.map(new ValueWithConfidence_Float(_, 60)), category.map(cat ⇒ new ValueWithConfidence[String](cat.toString(), 90)), None, cou, Seq(), "", "", "")
  }
}

private[nl] class MockClassifyDecisionLogWriter extends DecisionResultTable.Writer with DirectLogging {
  /**
   * Writes a single row.
   */
  def writeRow(key: String, value: DecisionResult, timestamp: Option[Long]) {
    log.info("Key [%s]".format(key))
    log.info("decision for license: [%s]".format(value.license))
    log.info("For this license there where [%d] steps".format(value.steps.size))

    val it = value.steps.iterator

    while (it.hasNext) {
      val a = it.next()
      log.info("Step [%s], result [%s]".format(a.step, a.result))

      if (a.details.nonEmpty) {
        val it2 = a.details.iterator

        while (it2.hasNext) {
          val b = it2.next()
          log.info("Details for this step: [%s] [%s]".format(b.step, b.result))
        }
      }
    }

  }

  /**
   * Writes a series of rows.
   */
  def writeRows(keysAndValues: Seq[(String, DecisionResult)]) {}

  /**
   * Writes a series of rows with explicit timestamps.
   */
  def writeRowsWithTimestamp(keysAndValues: Seq[(String, DecisionResult, Long)]) {}

  /**
   * Writes a series of rows taking the key from the instance. If timestamp is None then
   * current time is used.
   */
  def writeRows(values: Seq[DecisionResult], getKey: (DecisionResult) ⇒ (String, Option[Long])) {}
}