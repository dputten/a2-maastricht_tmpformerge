package csc.sectioncontrol.classify.nl.licensePlate

import csc.sectioncontrol.classify.nl.RdwDambordCategory.Category
import csc.sectioncontrol.classify.nl.{ RdwDambordCategory, LicensePlateNL }
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

class KentekenSerieLicensePlateNLTest extends LicensePlateNLTest {
  override def useDambord = false
  override def ongeldigIndicator: Option[Category] = Some(RdwDambordCategory.ONG)

  "Classification algorithm" must {
    /**
     * Korps diplomatiek is added to the kentekenreeks file so now the category can be determined..
     * Same for koninklijk huis. But these are filtered out because of the length-4 of the license plate.
     */
    "classify license plates with first characters CD as Bijz kent korps diplomatiek for mask AB1234 (side code 1)" in {
      getLicense("CD5681", useDambord).rdwCategory must be(Some(RdwDambordCategory.BKC))
      getLicense("CD7931", useDambord).rdwCategory must be(Some(RdwDambordCategory.BKC))
    }
  }
}

class DambordLicensePlateNLTest extends LicensePlateNLTest {
  override def useDambord = true
  override def ongeldigIndicator: Option[Category] = None

  "Classification algorithm" must {
    /**
     * This is a strange one. Korps diplomatiek is filtered out by the RDW. Same for koninklijk huis.
     * It is known that the CD licenses should be BKC but because they are not in the dambord1.csv no category
     * can be found.
     */
    "classify license plates with first characters CD (Bijz kent korps diplomatiek) as not found mask AB1234 (side code 1)" in {
      getLicense("CD5681", useDambord).rdwCategory must be(None)
      getLicense("CD7931", useDambord).rdwCategory must be(None)
    }
  }
}

abstract class LicensePlateNLTest extends WordSpec with MustMatchers {

  /**
   * For backward compatibility.
   *   true  => use dambord (old situation)
   *   false => use kentekenserie (new situation)
   */
  def useDambord: Boolean

  /**
   * In the new situation (kentekenreeks) all the license plates that resolved to 'ongeldig' ('ONG') have been removed form the
   * list. The old implementation (dambord) had some 'ongeldig' mappings and returned 'None' for every value not mapped.
   * In the kentekenreeks implementation everything that is not mapped is 'ongeldig'.
   */
  def ongeldigIndicator: Option[Category]

  /**
   * Helper method to wrap the license plate string in a proper object.
   *
   * @param plate the license plate to wrap
   * @param useDambord true for dambord (old), false for kentekenreeks (new)
   * @return the plate wrapped in a LicensePlateNL object
   */
  def getLicense(plate: String, useDambord: Boolean): LicensePlateNL = {
    val result = LicensePlateNL(plate)
    result.useDambord = Some(useDambord)
    if (useDambord)
      result.sideCodeFileName = "/sidecodes.json"
    else
      result.sideCodeFileName = "/sidecodesWithKentekenreeks.json"

    result
  }

  "Classification algorithm" must {

    "classify Bedrijfsauto KB (category B) for mask AB1234 (side code 1)" in {
      val plate1 = getLicense("AB1234", useDambord)
      plate1.plate must be("AB1234")
      plate1.sideCode.get.code must be(1)
      plate1.rdwCategory must be(Some(RdwDambordCategory.B))
    }

    "classify Bedrijfsauto KB (category B) for mask 1234AB (side code 2)" in {
      val plate1 = getLicense("1234AB", useDambord)
      plate1.sideCode.get.code must be(2)
      plate1.rdwCategory must be(Some(RdwDambordCategory.B))
    }

    "classify Bedrijfsauto KB (category B) for mask 12AB34 (side code 3)" in {
      val plate1 = getLicense("12AB34", useDambord)
      plate1.sideCode.get.code must be(3)
      plate1.rdwCategory must be(Some(RdwDambordCategory.B))
    }

    "classify Personenauto KB (category P) for mask AB12CD (side code 4)" in {
      val plate1 = getLicense("DB01BB", useDambord)
      plate1.sideCode.get.code must be(4)
      plate1.rdwCategory must be(Some(RdwDambordCategory.P))
    }

    "classify Personenauto KB (category P) for mask ABCD12 (side code 5)" in {
      val plate1 = getLicense("DBBB01", useDambord)
      plate1.sideCode.get.code must be(5)
      plate1.rdwCategory must be(Some(RdwDambordCategory.P))
    }

    "classify Personenauto KB (category P) for mask 12ABCD (side code 6)" in {
      val plate1 = getLicense("01DBBB", useDambord)
      plate1.sideCode.get.code must be(6)
      plate1.rdwCategory must be(Some(RdwDambordCategory.P))
    }

    "classify Bromfiets (category C) for mask 12ABC3 (side code 7)" in {
      val plate1 = getLicense("01DBB1", useDambord)
      plate1.sideCode.get.code must be(7)
      plate1.rdwCategory must be(Some(RdwDambordCategory.C))
    }

    "classify Personenauto KB (category P) for mask 1ABC23 (side code 8)" in {
      val plate1 = getLicense("0KKX01", useDambord)
      plate1.sideCode.get.code must be(8)
      plate1.rdwCategory must be(Some(RdwDambordCategory.P))
    }

    "classify Bromfiets (category C) for mask AB123C (side code 9)" in {
      val plate1 = getLicense("DB001B", useDambord)
      plate1.sideCode.get.code must be(9)
      plate1.rdwCategory must be(Some(RdwDambordCategory.C))
    }

    "classify Personenauto KB (category P) for mask A123BC (side code 10)" in {
      val plate1 = getLicense("G123GA", useDambord)
      plate1.sideCode.get.code must be(10)
      plate1.rdwCategory must be(Some(RdwDambordCategory.P))
    }

    "classify Bromfiets (category C) for mask ABC12D (side code 11)" in {
      val plate1 = getLicense("FVA99A", useDambord)
      plate1.sideCode.get.code must be(11)
      plate1.rdwCategory must be(Some(RdwDambordCategory.C))
    }

    "classify Motor KB (category M) for mask A12BCD (side code 12)" in {
      val plate1 = getLicense("M99DAA", useDambord)
      plate1.sideCode.get.code must be(12)
      plate1.rdwCategory must be(Some(RdwDambordCategory.M))
    }

    "classify Aanhangwagen KB (category A) for mask 1AB234 (side code 13)" in {
      val plate1 = getLicense("9WX999", useDambord)
      plate1.sideCode.get.code must be(13)
      plate1.rdwCategory must be(Some(RdwDambordCategory.A))
    }

    "classify Personenauto KB (category P) for mask 123AB4 (side code 14)" in {
      val plate1 = getLicense("999JP9", useDambord)
      plate1.sideCode.get.code must be(14)
      plate1.rdwCategory must be(Some(RdwDambordCategory.P))
    }

    case class LicensePlateMapping(plate: String, sideCode: Int, category: Option[Category])

    "map license plates to correct side code and category" in {
      val plates = List(
        LicensePlateMapping("B123BC", 10, Some(RdwDambordCategory.ZB)),
        LicensePlateMapping("K123BZ", 10, Some(RdwDambordCategory.P)),
        LicensePlateMapping("AB1234", 1, Some(RdwDambordCategory.B)),
        LicensePlateMapping("BBB12B", 11, Some(RdwDambordCategory.ZB)),
        LicensePlateMapping("SSS66B", 11, Some(RdwDambordCategory.ONG)),
        LicensePlateMapping("ZZZ00Z", 11, Some(RdwDambordCategory.P)),
        LicensePlateMapping("MMX00Z", 11, ongeldigIndicator),
        LicensePlateMapping("B12BBB", 12, Some(RdwDambordCategory.ZB)),
        LicensePlateMapping("V66VVV", 12, Some(RdwDambordCategory.LB)),
        LicensePlateMapping("F99BBB", 12, Some(RdwDambordCategory.C)),
        LicensePlateMapping("M99MBB", 12, ongeldigIndicator),
        LicensePlateMapping("9BX999", 13, Some(RdwDambordCategory.ZB)),
        LicensePlateMapping("0DK123", 13, Some(RdwDambordCategory.C)),
        LicensePlateMapping("8SD543", 13, Some(RdwDambordCategory.ONG)),
        LicensePlateMapping("8MM121", 13, ongeldigIndicator),
        LicensePlateMapping("123BF6", 14, Some(RdwDambordCategory.ZB)),
        LicensePlateMapping("666MX6", 14, Some(RdwDambordCategory.M)),
        LicensePlateMapping("098SS3", 14, Some(RdwDambordCategory.ONG)),
        LicensePlateMapping("666MM1", 14, ongeldigIndicator))

      plates.foreach { plateMapping ⇒
        val result = getLicense(plateMapping.plate, useDambord)
        result.sideCode.get.code must be(plateMapping.sideCode)
        result.rdwCategory must be(plateMapping.category)
      }
    }

    "yield an invalid (ongeldig) category for license plates that do not map to a valid side code (1-14)" in {
      val unknownPlates = List(null, "      ", "123456", "AA64", "K2650", "AB12345")

      unknownPlates.foreach { plate ⇒
        val result = getLicense(plate, useDambord)
        result.sideCode must be(None)
        result.rdwCategory must be(ongeldigIndicator)
      }
    }

    "yield a side code but an invalid (ongeldig) category for license plates that do map to a side code but are still unknown" in {
      val unknownPlates = List("cc1234", "II1234")

      unknownPlates.foreach { plate ⇒
        val result = getLicense(plate, useDambord)
        result.sideCode.get.code must be(1)
        result.rdwCategory must be(ongeldigIndicator)
      }
    }

    "classify license plates with first characters K<X> as military for mask AB1234 (side code 1)" in {
      val validMilitarySignificantCharacters =
        List("KA", "KL", "KM", "KN", "KO", "KP", "KR", "KS", "KT", "KU", "KV", "KW", "KX", "KY", "KZ", "LM")

      validMilitarySignificantCharacters.foreach { chars ⇒
        getLicense(chars + "0001", useDambord).rdwCategory must be(Some(RdwDambordCategory.MIL))
      }
    }

    "classify license plates with first characters K<X> as military for mask 1234AB (side code 2)" in {
      val validMilitarySignificantCharacters =
        List("KA", "KM", "KZ", "KU", "LM")

      validMilitarySignificantCharacters.foreach { chars ⇒
        getLicense("0001" + chars, useDambord).rdwCategory must be(Some(RdwDambordCategory.MIL))
      }
    }

    "classify license plates with first characters K<X> as military for mask 12AB34 (side code 3)" in {
      val validMilitarySignificantCharacters =
        List("KA", "KL", "KM", "KP", "KU", "KZ", "LM")

      validMilitarySignificantCharacters.foreach { chars ⇒
        getLicense("00" + chars + "01", useDambord).rdwCategory must be(Some(RdwDambordCategory.MIL))
      }
    }

    "classify license plates with first characters DM as military for mask 12ABC3 (side code 7)" in {
      getLicense("D001MA", useDambord).rdwCategory must be(Some(RdwDambordCategory.MIL))
      getLicense("D001MB", useDambord).rdwCategory must be(Some(RdwDambordCategory.MIL))
    }

    "classify license plates with first characters DM as military for mask AB123C (side code 9)" in {
      getLicense("DM001A", useDambord).rdwCategory must be(Some(RdwDambordCategory.MIL))
      getLicense("DM001B", useDambord).rdwCategory must be(Some(RdwDambordCategory.MIL))
    }

    "classify license plates with first characters DM as military for mask A123BC (side code 10)" in {
      getLicense("00DMA1", useDambord).rdwCategory must be(Some(RdwDambordCategory.MIL))
      getLicense("00DMB1", useDambord).rdwCategory must be(Some(RdwDambordCategory.MIL))
    }

    "classify license plates in lower case" in {
      val validMilitarySignificantCharacters =
        List("KA", "KL", "KM", "KN", "KO", "KP", "KR", "KS", "KT", "KU", "KV", "KW", "KX", "KY", "KZ", "LM")

      validMilitarySignificantCharacters.map(_.toLowerCase).foreach { chars ⇒
        getLicense(chars + "0001", useDambord).rdwCategory must be(Some(RdwDambordCategory.MIL))
      }
    }

  }
}