/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.classify.nl.preprocess

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.akkautils.DirectLogging
import csc.sectioncontrol.classify.nl.{ MockRdwRegistry, ClassifierConfig }
import csc.sectioncontrol.rdwregistry.RdwData
import csc.sectioncontrol.messages.{ ValueWithConfidence, VehicleCategory }
import akka.util.duration._
import csc.sectioncontrol.storage.ZkCorrectTDC3CategoryConfig

class TDC3PreProcessTest extends WordSpec with MustMatchers with DirectLogging {
  import CreateTestData._
  val config = ClassifierConfig(0.4f, 0.5f)
  val rdw = new MockRdwRegistry(RdwData("GZ123C", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
    wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1"))
  val processor = new PreProcessTDC3Category(rdw, config, log)

  val startTime = 1380585600000L

  "preProcess" must {
    "group by lane and process data exit" in {
      val startTimeEntry = startTime - 5.minutes.toMillis
      val entry11 = createVehicle(laneId = "lane1", timestamp = startTimeEntry, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit11 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("SJ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord11 = createSpeedRecord(entry11, exit11)
      val gap = 1000
      val entry12 = createVehicle(laneId = "lane1", timestamp = startTimeEntry + gap, license = Some("GZ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit12 = createVehicle(laneId = "lane1", timestamp = startTime + gap, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = None, country = Some("NL"))
      val speedRecord12 = createSpeedRecord(entry12, exit12)

      val entry21 = createVehicle(laneId = "lane2", timestamp = startTimeEntry, license = Some("SJ124D"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit21 = createVehicle(laneId = "lane2", timestamp = startTime + gap, license = Some("SJ124D"), category = None, length = None, country = Some("NL"))
      val speedRecord21 = createSpeedRecord(entry21, exit21)
      val entry22 = createVehicle(laneId = "lane1", timestamp = startTimeEntry + gap, license = Some("GZ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit22 = createVehicle(laneId = "lane1", timestamp = startTime + 2 * gap, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = None, country = Some("NL"))
      val speedRecord22 = createSpeedRecord(entry22, exit22)

      val updateList = processor.preProcess(Seq(speedRecord11, speedRecord12, speedRecord21, speedRecord22))
      updateList.size must be(4)
      updateList(0) must be(speedRecord21)

      val expectRecord11 = createSpeedRecord(entry11, exit11.copy(category = Some(new ValueWithConfidence[String]("Car", 90))))
      updateList(1) must be(expectRecord11)
      val expectRecord12 = createSpeedRecord(entry12, exit12.copy(category = Some(new ValueWithConfidence[String]("Car", 90))))
      updateList(2) must be(expectRecord12)
      updateList(3) must be(speedRecord22)
    }
    "group by lane and process data entry" in {
      val startTimeEntry = startTime - 5.minutes.toMillis
      val entry11 = createVehicle(laneId = "lane1", timestamp = startTimeEntry, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit11 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("SJ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord11 = createSpeedRecord(entry11, exit11)
      val gap = 1000
      val entry12 = createVehicle(laneId = "lane1", timestamp = startTimeEntry + gap, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = Some(4.5F), country = Some("NL"))
      val exit12 = createVehicle(laneId = "lane1", timestamp = startTime + gap, license = Some("GZ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord12 = createSpeedRecord(entry12, exit12)

      val entry21 = createVehicle(laneId = "lane2", timestamp = startTimeEntry, license = Some("SJ124D"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit21 = createVehicle(laneId = "lane2", timestamp = startTime + gap, license = Some("SJ124D"), category = None, length = None, country = Some("NL"))
      val speedRecord21 = createSpeedRecord(entry21, exit21)
      val entry22 = createVehicle(laneId = "lane1", timestamp = startTimeEntry + gap, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = Some(4.5F), country = Some("NL"))
      val exit22 = createVehicle(laneId = "lane1", timestamp = startTime + 2 * gap, license = Some("GZ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord22 = createSpeedRecord(entry22, exit22)

      val updateList = processor.preProcess(Seq(speedRecord11, speedRecord12, speedRecord21, speedRecord22))
      updateList.size must be(4)
      updateList(0) must be(speedRecord21)

      val expectRecord11 = createSpeedRecord(entry11.copy(category = Some(new ValueWithConfidence[String]("Car", 90))), exit11)
      updateList(1) must be(expectRecord11)
      val expectRecord12 = createSpeedRecord(entry12.copy(category = Some(new ValueWithConfidence[String]("Car", 90))), exit12)
      updateList(2) must be(expectRecord12)
      updateList(3) must be(speedRecord22)
    }
    "group by lane and process data entry and exit" in {
      val startTimeEntry = startTime - 5.minutes.toMillis
      val entry11 = createVehicle(laneId = "lane1", timestamp = startTimeEntry, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit11 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("SJ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord11 = createSpeedRecord(entry11, exit11)
      val gap = 1000
      val entry12 = createVehicle(laneId = "lane1", timestamp = startTimeEntry + gap, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = Some(4.5F), country = Some("NL"))
      val exit12 = createVehicle(laneId = "lane1", timestamp = startTime + gap, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = None, country = Some("NL"))
      val speedRecord12 = createSpeedRecord(entry12, exit12)

      val entry21 = createVehicle(laneId = "lane2", timestamp = startTimeEntry, license = Some("SJ124D"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit21 = createVehicle(laneId = "lane2", timestamp = startTime + gap, license = Some("SJ124D"), category = None, length = None, country = Some("NL"))
      val speedRecord21 = createSpeedRecord(entry21, exit21)
      val entry22 = createVehicle(laneId = "lane1", timestamp = startTimeEntry + gap, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = Some(4.5F), country = Some("NL"))
      val exit22 = createVehicle(laneId = "lane1", timestamp = startTime + 2 * gap, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = None, country = Some("NL"))
      val speedRecord22 = createSpeedRecord(entry22, exit22)

      val updateList = processor.preProcess(Seq(speedRecord11, speedRecord12, speedRecord21, speedRecord22))
      updateList.size must be(4)
      updateList(0) must be(speedRecord21)

      val expectRecord11 = createSpeedRecord(entry11.copy(category = Some(new ValueWithConfidence[String]("Car", 90))), exit11.copy(category = Some(new ValueWithConfidence[String]("Car", 90))))
      updateList(1) must be(expectRecord11)
      val expectRecord12 = createSpeedRecord(entry12.copy(category = Some(new ValueWithConfidence[String]("Car", 90))), exit12.copy(category = Some(new ValueWithConfidence[String]("Car", 90))))
      updateList(2) must be(expectRecord12)
      updateList(3) must be(speedRecord22)
    }
    "not process when disabled" in {
      val localConfig = ClassifierConfig(0.4f, 0.5f, correctTDC3 = ZkCorrectTDC3CategoryConfig(enable = false))
      val localProcessor = new PreProcessTDC3Category(rdw, localConfig, log)

      val startTimeEntry = startTime - 5.minutes.toMillis
      val entry11 = createVehicle(laneId = "lane1", timestamp = startTimeEntry, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit11 = createVehicle(laneId = "lane1", timestamp = startTime, license = Some("SJ123C"), category = None, length = None, country = Some("NL"))
      val speedRecord11 = createSpeedRecord(entry11, exit11)
      val gap = 1000
      val entry12 = createVehicle(laneId = "lane1", timestamp = startTimeEntry + gap, license = Some("GZ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit12 = createVehicle(laneId = "lane1", timestamp = startTime + gap, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = None, country = Some("NL"))
      val speedRecord12 = createSpeedRecord(entry12, exit12)

      val entry21 = createVehicle(laneId = "lane2", timestamp = startTimeEntry, license = Some("SJ124D"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit21 = createVehicle(laneId = "lane2", timestamp = startTime + gap, license = Some("SJ124D"), category = None, length = None, country = Some("NL"))
      val speedRecord21 = createSpeedRecord(entry21, exit21)
      val entry22 = createVehicle(laneId = "lane1", timestamp = startTimeEntry + gap, license = Some("GZ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val exit22 = createVehicle(laneId = "lane1", timestamp = startTime + 2 * gap, license = Some("GZ123C"), category = Some(VehicleCategory.CarTrailer), length = None, country = Some("NL"))
      val speedRecord22 = createSpeedRecord(entry22, exit22)

      val updateList = localProcessor.preProcess(Seq(speedRecord11, speedRecord12, speedRecord21, speedRecord22))
      updateList.size must be(4)

      updateList(0) must be(speedRecord11)
      updateList(1) must be(speedRecord12)
      updateList(2) must be(speedRecord21)
      updateList(3) must be(speedRecord22)
    }
    "handle data without exit" in {
      val startTimeEntry = startTime - 5.minutes.toMillis
      val entry1 = createVehicle(laneId = "lane1", timestamp = startTimeEntry, license = Some("SJ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val speedRecord1 = createSpeedRecord(entry1)
      val gap = 1000
      val entry2 = createVehicle(laneId = "lane1", timestamp = startTimeEntry + gap, license = Some("GZ123C"), category = None, length = Some(4.5F), country = Some("NL"))
      val speedRecord2 = createSpeedRecord(entry2)

      val updateList = processor.preProcess(Seq(speedRecord1, speedRecord2))
      updateList.size must be(2)
      updateList(0) must be(speedRecord1)
      updateList(1) must be(speedRecord2)
    }
    "handle empty list" in {
      val updateList = processor.preProcess(Seq())
      updateList.size must be(0)
    }
  }

}