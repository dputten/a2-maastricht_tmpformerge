package csc.sectioncontrol.classify.nl.licensePlate.kentekenserie

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.akkautils.DirectLogging
import csc.sectioncontrol.classify.nl.{ KentekenserieParser, FileToLinesReader }

class KentekenserieTest extends WordSpec with MustMatchers with DirectLogging {

  "FileToLinesReader.read" must {
    "read the file and return a list with the correct number of lines" in {
      val name = "/Kentserie RDW versie 20150424.csv"
      val file = getClass.getResourceAsStream(name)
      val lines = FileToLinesReader.read(file)

      lines.length must be(8389)
    }
    "read the file and return a requirement exception if the file could not be found" in {
      val name = "/xxx.xxx"
      val file = getClass.getResourceAsStream(name)
      val thrown = evaluating { FileToLinesReader.read(file) } must produce[Exception]
      thrown.getMessage must equal("requirement failed: Input cannot be null for FileToLinesReader.")
    }
  }

  "KentekenserieParser.read" must {
    "parse the list and return a list with the correct number of objects" in {
      val name = "/Kentserie RDW versie 20150424.csv"
      val file = getClass.getResourceAsStream(name)
      val lines = FileToLinesReader.read(file)
      lines.length must be(8389)

      val kentekenserie = KentekenserieParser.parse(lines)
      kentekenserie.kentekenserieRegels.length must be(8389)
    }
  }
}

