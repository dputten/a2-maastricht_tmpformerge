/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.classify.nl

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.messages._
import csc.sectioncontrol.rdwregistry.RdwData
import csc.sectioncontrol.rdwregistry.RdwRegistry
import java.util.Date
import csc.sectioncontrol.storage.ZkClassifyConfidence
import csc.sectioncontrol.storagelayer.hbasetables.DecisionResultTable

class RDWFlowClassifyCodeTest extends WordSpec with MustMatchers {
  val writer = new MockClassifyDecisionWriter()

  "MatchClassifier" must {
    val config = ClassifierConfig(0.4f, 0.5f)
    val classifier = new RdwMatchClassifier(new MockRdwRegistry(RdwData("KW9999", 25, 3, 2500, 3.0f, 2.0f, 1500, 150, 160, "LF")), config, writer)

    "detect MIL auto" in {
      val passage = classifier.classify(getMatch("KW9999", length = Some(4.5F)))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CA)
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.mil must be(true)
      passage.validLicensePlate must be(None)
      passage.manualAccordingToSpec must be(false)
    }

    "detect long MIL auto" in {
      val passage = classifier.classify(getMatch("KW9911", VehicleCategory.Truck, Some(8F)))

      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.VA)
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.mil must be(true)
      passage.validLicensePlate must be(None)
      passage.manualAccordingToSpec must be(false)
    }

    "detect motorbyke" in {
      val passage = classifier.classify(getMatch("MB12CD"))
      getCountry(passage) must be("NL")
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.mil must be(false)
      passage.validLicensePlate must be(None)
      passage.manualAccordingToSpec must be(false)
    }

    "detect Italian auto" in {
      val passage = classifier.classify(getMatch("1234567890", country = Some("IT")))
      getCountry(passage) must be("IT")
      passage.indicator must be(ProcessingIndicator.Mobi)
      passage.reason.mask must be(0)
      passage.code.get must be(VehicleCode.MV)
      passage.validLicensePlate must be(None)
      passage.manualAccordingToSpec must be(false)
    }

    "detect German personal auto" in {
      val passage = classifier.classify(getMatch("DE123456", length = Some(4.5F), country = Some("DE")))
      getCountry(passage) must be("DE")
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.code.get must be(VehicleCode.MV)
      passage.validLicensePlate must be(None)
      passage.manualAccordingToSpec must be(false)
    }

    "detect Belgium truck" in {
      val passage = classifier.classify(getMatch("BE123456", country = Some("BE"), category = VehicleCategory.Large_Truck))
      getCountry(passage) must be("BE")
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(2)
      passage.code.get must be(VehicleCode.VA)
      passage.validLicensePlate must be(None)
      passage.manualAccordingToSpec must be(true)
    }

    "detect personal auto" in {
      val passage = classifier.classify(getMatch("SJ123C", length = Some(4.5F)))
      passage.indicator must be(ProcessingIndicator.Automatic)
      passage.reason.mask must be(0)
      passage.code must be(Some(VehicleCode.PA))
      passage.alternativeClassification must be(None)
      passage.validLicensePlate must be(None)
      passage.licensePlate must be("SJ123C")
    }

    "detect too long license plate" in {
      val passage = classifier.classify(getMatch("1234567890123456789"))
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.reason.mask must be(7)
      passage.code must be(None)
      passage.validLicensePlate must be(Some("123456789012"))
      passage.licensePlate must be("123456789012")
    }
  }

  "foreigner treaty countries" must {
    val config = ClassifierConfig(0.4f, 0.5f)
    val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
    "be classified when TDC3=Car" in {
      val passage = classifier.classify(getMatch("DE123456", category = VehicleCategory.Car, length = Some(4.5F), country = Some("DE")))
      getCountry(passage) must be("DE")
      passage.code.get must be(VehicleCode.MV)
    }
    "be classified when TDC3=Van" in {
      val passage = classifier.classify(getMatch("DE123456", category = VehicleCategory.Van, length = Some(4.5F), country = Some("DE")))
      getCountry(passage) must be("DE")
      passage.code.get must be(VehicleCode.MV)
    }
    "be classified when TDC3=Unknown" in {
      val passage = classifier.classify(getMatch("DE123456", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("DE")))
      getCountry(passage) must be("DE")
      passage.code.get must be(VehicleCode.MV)
      passage.indicator must be(ProcessingIndicator.Manual)
    }
    "be classified when TDC3=CarTrailer" in {
      val passage = classifier.classify(getMatch("DE123456", category = VehicleCategory.CarTrailer, length = Some(4.5F), country = Some("DE")))
      getCountry(passage) must be("DE")
      passage.code.get must be(VehicleCode.VA)
    }
    "be classified when TDC3=Bus" in {
      val passage = classifier.classify(getMatch("DE123456", category = VehicleCategory.Bus, length = Some(4.5F), country = Some("DE")))
      getCountry(passage) must be("DE")
      passage.code.get must be(VehicleCode.AT)
    }
    "be classified when TDC3=Motor" in {
      val passage = classifier.classify(getMatch("DE123456", category = VehicleCategory.Motor, length = Some(4.5F), country = Some("DE")))
      getCountry(passage) must be("DE")
      passage.code.get must be(VehicleCode.MV)
    }
    "be classified when TDC3=Truck" in {
      val passage = classifier.classify(getMatch("DE123456", category = VehicleCategory.Truck, length = Some(4.5F), country = Some("DE")))
      getCountry(passage) must be("DE")
      passage.code.get must be(VehicleCode.VA)
    }
    "be classified when TDC3=TruckTrailer" in {
      val passage = classifier.classify(getMatch("DE123456", category = VehicleCategory.TruckTrailer, length = Some(4.5F), country = Some("DE")))
      getCountry(passage) must be("DE")
      passage.code.get must be(VehicleCode.VA)
    }
    "be classified when TDC3=SemiTrailer" in {
      val passage = classifier.classify(getMatch("DE123456", category = VehicleCategory.SemiTrailer, length = Some(4.5F), country = Some("DE")))
      getCountry(passage) must be("DE")
      passage.code.get must be(VehicleCode.VA)
    }
    "be classified when TDC3=None" in {
      val first = vreg("id1", 1000L, "g1", "DE123456", None, Some(4.3F), Some("DE"))
      val second = vreg("id2", 2000L, "g2", "DE123456", None, None, Some("DE"))
      val rec = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val passage = classifier.classify(rec)
      getCountry(passage) must be("DE")
      passage.code.get must be(VehicleCode.MV)
      passage.indicator must be(ProcessingIndicator.Manual)
    }
  }
  "other foreigners" must {
    val config = ClassifierConfig(0.4f, 0.5f)
    val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
    "be classified when TDC3=Car" in {
      val passage = classifier.classify(getMatch("1234567890", category = VehicleCategory.Car, length = Some(4.5F), country = Some("IT")))
      getCountry(passage) must be("IT")
      passage.code.get must be(VehicleCode.MV)
      passage.indicator must be(ProcessingIndicator.Mobi)
    }
    "be classified when TDC3=Van" in {
      val passage = classifier.classify(getMatch("1234567890", category = VehicleCategory.Van, length = Some(4.5F), country = Some("IT")))
      getCountry(passage) must be("IT")
      passage.code.get must be(VehicleCode.MV)
      passage.indicator must be(ProcessingIndicator.Mobi)
    }
    "be classified when TDC3=Unknown" in {
      val passage = classifier.classify(getMatch("1234567890", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("IT")))
      getCountry(passage) must be("IT")
      passage.code.get must be(VehicleCode.MV)
      passage.indicator must be(ProcessingIndicator.Mobi)
    }
    "be classified when TDC3=CarTrailer" in {
      val passage = classifier.classify(getMatch("1234567890", category = VehicleCategory.CarTrailer, length = Some(4.5F), country = Some("IT")))
      getCountry(passage) must be("IT")
      passage.code.get must be(VehicleCode.VA)
      passage.indicator must be(ProcessingIndicator.Mobi)
    }
    "be classified when TDC3=Bus" in {
      val passage = classifier.classify(getMatch("1234567890", category = VehicleCategory.Bus, length = Some(4.5F), country = Some("IT")))
      getCountry(passage) must be("IT")
      passage.code.get must be(VehicleCode.AT)
      passage.indicator must be(ProcessingIndicator.Mobi)
    }
    "be classified when TDC3=Motor" in {
      val passage = classifier.classify(getMatch("1234567890", category = VehicleCategory.Motor, length = Some(4.5F), country = Some("IT")))
      getCountry(passage) must be("IT")
      passage.code.get must be(VehicleCode.MV)
      passage.indicator must be(ProcessingIndicator.Mobi)
    }
    "be classified when TDC3=Truck" in {
      val passage = classifier.classify(getMatch("1234567890", category = VehicleCategory.Truck, length = Some(4.5F), country = Some("IT")))
      getCountry(passage) must be("IT")
      passage.code.get must be(VehicleCode.VA)
      passage.indicator must be(ProcessingIndicator.Mobi)
    }
    "be classified when TDC3=TruckTrailer" in {
      val passage = classifier.classify(getMatch("1234567890", category = VehicleCategory.TruckTrailer, length = Some(4.5F), country = Some("IT")))
      getCountry(passage) must be("IT")
      passage.code.get must be(VehicleCode.VA)
      passage.indicator must be(ProcessingIndicator.Mobi)
    }
    "be classified when TDC3=SemiTrailer" in {
      val passage = classifier.classify(getMatch("1234567890", category = VehicleCategory.SemiTrailer, length = Some(4.5F), country = Some("IT")))
      getCountry(passage) must be("IT")
      passage.code.get must be(VehicleCode.VA)
      passage.indicator must be(ProcessingIndicator.Mobi)
    }
    "be classified when TDC3=None" in {
      val first = vreg("id1", 1000L, "g1", "1234567890", None, Some(4.3F), Some("IT"))
      val second = vreg("id2", 2000L, "g2", "1234567890", None, None, Some("IT"))
      val rec = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val passage = classifier.classify(rec)
      getCountry(passage) must be("IT")
      passage.code.get must be(VehicleCode.MV)
      passage.indicator must be(ProcessingIndicator.Mobi)
    }
  }
  "Dutch MIL License" must {
    val config = ClassifierConfig(0.4f, 0.5f)
    val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
    "be classified when TDC3=Car" in {
      val passage = classifier.classify(getMatch("KW9999", category = VehicleCategory.Car, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CA)
    }
    "be classified when TDC=Car and RDW=filled" in {
      val rdw = RdwData("KW9999", vehicleClass = 25, deviceCode = 68, maxMass = 2500,
        wheelBase = 2.0f, length = 6.5f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1e")
      val localClassifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = localClassifier.classify(getMatch("KW9999", category = VehicleCategory.Car, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CA)
    }
    "be classified when TDC3=Van" in {
      val passage = classifier.classify(getMatch("KW9999", category = VehicleCategory.Van, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CA)
    }
    "be classified when TDC3=Unknown" in {
      val passage = classifier.classify(getMatch("KW9999", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CA)
      passage.indicator must be(ProcessingIndicator.Manual)
    }
    "be classified when TDC3=CarTrailer" in {
      val passage = classifier.classify(getMatch("KW9999", category = VehicleCategory.CarTrailer, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.VA)
    }
    "be classified when TDC3=Bus" in {
      val passage = classifier.classify(getMatch("KW9999", category = VehicleCategory.Bus, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.VA)
    }
    "be classified when TDC3=Motor" in {
      val passage = classifier.classify(getMatch("KW9999", category = VehicleCategory.Motor, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CA)
    }
    "be classified when TDC3=Truck" in {
      val passage = classifier.classify(getMatch("KW9999", category = VehicleCategory.Truck, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.VA)
    }
    "be classified when TDC3=TruckTrailer" in {
      val passage = classifier.classify(getMatch("KW9999", category = VehicleCategory.TruckTrailer, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.VA)
    }
    "be classified when TDC3=SemiTrailer" in {
      val passage = classifier.classify(getMatch("KW9999", category = VehicleCategory.SemiTrailer, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.VA)
    }
    "be classified when TDC3=None" in {
      val first = vreg("id1", 1000L, "g1", "KW9999", None, Some(4.3F), Some("NL"))
      val second = vreg("id2", 2000L, "g2", "KW9999", None, None, Some("NL"))
      val rec = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val passage = classifier.classify(rec)
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CA)
      passage.indicator must be(ProcessingIndicator.Manual)
    }
  }
  "Dutch M License" must {
    val config = ClassifierConfig(0.4f, 0.5f)
    val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
    "be classified when TDC3=Car" in {
      val passage = classifier.classify(getMatch("MB12CD", category = VehicleCategory.Car, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.MF)
    }
    "be classified when TDC=Car and RDW=filled" in {
      val rdw = RdwData("MB12CD", vehicleClass = 25, deviceCode = 68, maxMass = 2500,
        wheelBase = 2.0f, length = 6.5f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1e")
      val localClassifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = localClassifier.classify(getMatch("MB12CD", category = VehicleCategory.Car, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.MF)
    }
    "be classified when TDC3=Van" in {
      val passage = classifier.classify(getMatch("MB12CD", category = VehicleCategory.Van, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.MF)
    }
    "be classified when TDC3=Unknown" in {
      val passage = classifier.classify(getMatch("MB12CD", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.MF)
    }
    "be classified when TDC3=CarTrailer" in {
      val passage = classifier.classify(getMatch("MB12CD", category = VehicleCategory.CarTrailer, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.MF)
    }
    "be classified when TDC3=Bus" in {
      val passage = classifier.classify(getMatch("MB12CD", category = VehicleCategory.Bus, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.MF)
    }
    "be classified when TDC3=Motor" in {
      val passage = classifier.classify(getMatch("MB12CD", category = VehicleCategory.Motor, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.MF)
    }
    "be classified when TDC3=Truck" in {
      val passage = classifier.classify(getMatch("MB12CD", category = VehicleCategory.Truck, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.MF)
    }
    "be classified when TDC3=TruckTrailer" in {
      val passage = classifier.classify(getMatch("MB12CD", category = VehicleCategory.TruckTrailer, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.MF)
    }
    "be classified when TDC3=SemiTrailer" in {
      val passage = classifier.classify(getMatch("MB12CD", category = VehicleCategory.SemiTrailer, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.MF)
    }
    "be classified when TDC3=None" in {
      val first = vreg("id1", 1000L, "g1", "MB12CD", None, Some(4.3F), Some("NL"))
      val second = vreg("id2", 2000L, "g2", "MB12CD", None, None, Some("NL"))
      val rec = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val passage = classifier.classify(rec)
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.MF)
    }

  }

  "Dutch P License" must {
    "be classified when TDC3=Car RDW=None" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("GZ123C", category = VehicleCategory.Car, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.PA)
    }
    "be classified when TDC3=Car RDW=Camper" in {
      val rdw = RdwData("GZ123C", vehicleClass = 25, deviceCode = 68, maxMass = 2500,
        wheelBase = 2.0f, length = 6.5f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1e")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("GZ123C", category = VehicleCategory.Car, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CP)
    }
    "be classified when TDC3=Car RDW=Not a Camper" in {
      val rdw = RdwData("GZ123C", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = classifier.classify(getMatch("GZ123C", category = VehicleCategory.Car, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.PA)
    }
    "be classified when TDC3=Van RDW=None" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("GZ123C", category = VehicleCategory.Van, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.PA)
    }
    "be classified when TDC3=Van RDW=Camper" in {
      val rdw = RdwData("GZ123C", vehicleClass = 25, deviceCode = 68, maxMass = 2500,
        wheelBase = 2.0f, length = 6.5f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1e")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("GZ123C", category = VehicleCategory.Van, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CP)
    }
    "be classified when TDC3=Van RDW=Not a Camper" in {
      val rdw = RdwData("GZ123C", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = classifier.classify(getMatch("GZ123C", category = VehicleCategory.Van, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.PA)
    }
    "be classified when TDC3=Unknown and has length of a car" in {
      val rdw = RdwData("GZ123C", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = classifier.classify(getMatch("GZ123C", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.PA)
    }
    "be classified when TDC3=Unknown and has length longer than a car" in {
      val rdw = RdwData("GZ123C", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = classifier.classify(getMatch("GZ123C", category = VehicleCategory.Unknown, length = Some(6), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.PA)
      passage.indicator must be(ProcessingIndicator.Manual)
    }
    "be classified when TDC3=CarTrailer" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("GZ123C", category = VehicleCategory.CarTrailer, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AS)
    }
    "be classified when TDC=CarTrailer and RDW=filled" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val rdw = RdwData("GZ123C", vehicleClass = 25, deviceCode = 68, maxMass = 2500,
        wheelBase = 2.0f, length = 6.5f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1e")
      val localClassifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = localClassifier.classify(getMatch("GZ123C", category = VehicleCategory.CarTrailer, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AS)
    }
    "be classified when TDC3=Bus" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("GZ123C", category = VehicleCategory.Bus, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AS)
    }
    "be classified when TDC=Bus and RDW=filled" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val rdw = RdwData("GZ123C", vehicleClass = 25, deviceCode = 68, maxMass = 2500,
        wheelBase = 2.0f, length = 6.5f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1e")
      val localClassifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = localClassifier.classify(getMatch("GZ123C", category = VehicleCategory.Bus, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AS)
    }
    "be classified when TDC3=Motor RDW=None" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("GZ123C", category = VehicleCategory.Motor, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.PA)
    }
    "be classified when TDC3=Motor RDW=Camper" in {
      val rdw = RdwData("GZ123C", vehicleClass = 25, deviceCode = 68, maxMass = 2500,
        wheelBase = 2.0f, length = 6.5f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1e")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("GZ123C", category = VehicleCategory.Motor, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CP)
    }
    "be classified when TDC3=Motor RDW=Not a Camper" in {
      val rdw = RdwData("GZ123C", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = classifier.classify(getMatch("GZ123C", category = VehicleCategory.Motor, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.PA)
    }
    "be classified when TDC3=Truck" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("GZ123C", category = VehicleCategory.Truck, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AS)
    }
    "be classified when TDC=Truck and RDW=filled" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val rdw = RdwData("GZ123C", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val localClassifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = localClassifier.classify(getMatch("GZ123C", category = VehicleCategory.Truck, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AS)
    }

    "be classified when TDC3=TruckTrailer" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("GZ123C", category = VehicleCategory.TruckTrailer, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AS)
    }
    "be classified when TDC=TruckTrailer and RDW=filled" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val rdw = RdwData("GZ123C", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val localClassifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = localClassifier.classify(getMatch("GZ123C", category = VehicleCategory.TruckTrailer, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AS)
    }
    "be classified when TDC3=SemiTrailer" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("GZ123C", category = VehicleCategory.SemiTrailer, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AS)
    }
    "be classified when TDC=SemiTrailer and RDW=filled" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val rdw = RdwData("GZ123C", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val localClassifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = localClassifier.classify(getMatch("GZ123C", category = VehicleCategory.SemiTrailer, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AS)
    }
    "be classified when TDC3=None" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)

      val first = vreg("id1", 1000L, "g1", "GZ123C", None, Some(6.3F), Some("NL"))
      val second = vreg("id2", 2000L, "g2", "GZ123C", None, None, Some("NL"))
      val rec = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val passage = classifier.classify(rec)
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.PA)
      passage.indicator must be(ProcessingIndicator.Manual)
    }
    "be classified when TDC=None and RDW=filled" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val rdw = RdwData("GZ123C", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val first = vreg("id1", 1000L, "g1", "GZ123C", None, Some(6.3F), Some("NL"))
      val second = vreg("id2", 2000L, "g2", "GZ123C", None, None, Some("NL"))
      val rec = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val passage = classifier.classify(rec)
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.PA)
      passage.indicator must be(ProcessingIndicator.Manual)
    }
  }
  "Dutch LB License" must {
    "be classified when TDC3=Unknown RDW=BUS" in {
      val rdw = RdwData("1VBC23", vehicleClass = 20, deviceCode = 1, maxMass = 2500,
        wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AB)
    }
    "be classified when TDC3=Unknown RDW=Tempo BUS" in {
      val rdw = RdwData("1VBC23", vehicleClass = 20, deviceCode = 1, maxMass = 2500,
        wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 100, eegCategory = "L1")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AT)
    }

    "be classified when TDC3=Car RDW=None" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Car, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.CA)
      passage.manualAccordingToSpec must be(false)
      passage.invalidRdwData must be(true)

    }
    "be classified when TDC3=Car RDW=Camper" in {
      val rdw = RdwData("1VBC23", vehicleClass = 25, deviceCode = 68, maxMass = 2500,
        wheelBase = 2.0f, length = 6.5f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1e")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Car, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CP)
    }
    "be classified when TDC3=Car RDW=Not a Camper" in {
      val rdw = RdwData("1VBC23", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Car, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CA)
    }
    "be classified when TDC3=Van RDW=None" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Van, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.CA)
      passage.manualAccordingToSpec must be(false)
      passage.invalidRdwData must be(true)
    }
    "be classified when TDC3=Van RDW=Camper" in {
      val rdw = RdwData("1VBC23", vehicleClass = 25, deviceCode = 68, maxMass = 2500,
        wheelBase = 2.0f, length = 6.5f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1e")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Van, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CP)
    }
    "be classified when TDC3=Van RDW=Not a Camper" in {
      val rdw = RdwData("1VBC23", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Van, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CA)
    }
    "be classified when TDC3=Unknown" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.AS)
      passage.manualAccordingToSpec must be(false)
      passage.invalidRdwData must be(true)
    }
    "be classified when TDC3=Unknown RDW=filled" in {
      val rdw = RdwData("1VBC23", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CA)
      passage.indicator must be(ProcessingIndicator.Manual)
    }
    "be classified when TDC3=None" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)

      val first = vreg("id1", 1000L, "g1", "1VBC23", None, Some(4.3F), Some("NL"))
      val second = vreg("id2", 2000L, "g2", "1VBC23", None, None, Some("NL"))
      val rec = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val passage = classifier.classify(rec)
      getCountry(passage) must be("NL")
      passage.indicator must be(ProcessingIndicator.Manual)
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.AS)
      passage.manualAccordingToSpec must be(false)
      passage.invalidRdwData must be(true)

    }
    "be classified when TDC3=None RDW=filled" in {
      val rdw = RdwData("1VBC23", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)

      val first = vreg("id1", 1000L, "g1", "1VBC23", None, Some(4.3F), Some("NL"))
      val second = vreg("id2", 2000L, "g2", "1VBC23", None, None, Some("NL"))
      val rec = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val passage = classifier.classify(rec)
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CA)
      passage.indicator must be(ProcessingIndicator.Manual)

    }
    "be classified when TDC3=CarTrailer" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.CarTrailer, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.AS)
      passage.manualAccordingToSpec must be(false)
      passage.invalidRdwData must be(true)
    }
    "be classified when TDC3=CarTrailer RDW=Filled" in {
      val rdw = RdwData("1VBC23", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.CarTrailer, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AS)
    }
    "be classified when TDC3=Bus" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Bus, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.AS)
      passage.manualAccordingToSpec must be(false)
      passage.invalidRdwData must be(true)

    }
    "be classified when TDC3=Bus RDW=Filled" in {
      val rdw = RdwData("1VBC23", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Bus, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AS)
    }
    "be classified when TDC3=Motor RDW=None" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Motor, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.CA)
      passage.manualAccordingToSpec must be(false)
      passage.invalidRdwData must be(true)

    }
    "be classified when TDC3=Motor RDW=Camper" in {
      val rdw = RdwData("1VBC23", vehicleClass = 25, deviceCode = 68, maxMass = 2500,
        wheelBase = 2.0f, length = 6.5f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1e")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Motor, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CP)
    }
    "be classified when TDC3=Motor RDW=Not a Camper" in {
      val rdw = RdwData("1VBC23", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Motor, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CA)
    }
    "be classified when TDC3=Truck RDW=None" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Truck, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.CA)
      passage.manualAccordingToSpec must be(false)
      passage.invalidRdwData must be(true)

    }
    "be classified when TDC3=Truck RDW=Camper" in {
      val rdw = RdwData("1VBC23", vehicleClass = 25, deviceCode = 68, maxMass = 2500,
        wheelBase = 2.0f, length = 6.5f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1e")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Truck, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CP)
    }
    "be classified when TDC3=Truck RDW=Not a Camper" in {
      val rdw = RdwData("1VBC23", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.Truck, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CA)
    }
    "be classified when TDC3=TruckTrailer" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.TruckTrailer, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.AS)
      passage.manualAccordingToSpec must be(false)
      passage.invalidRdwData must be(true)

    }
    "be classified when TDC3=TruckTrailer RDW=Filled" in {
      val rdw = RdwData("1VBC23", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.TruckTrailer, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AS)
    }
    "be classified when TDC3=SemiTrailer" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.SemiTrailer, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.AS)
      passage.manualAccordingToSpec must be(false)
      passage.invalidRdwData must be(true)
    }
    "be classified when TDC3=SemiTrailer RDW=Filled" in {
      val rdw = RdwData("1VBC23", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = classifier.classify(getMatch("1VBC23", category = VehicleCategory.SemiTrailer, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AS)
    }

  }
  "Dutch B License" must {
    "be classified when TDC3=Unknown RDW=BUS" in {
      val rdw = RdwData("BB12CD", vehicleClass = 20, deviceCode = 1, maxMass = 2500,
        wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AB)
    }
    "be classified when TDC3=Unknown RDW=Tempo BUS" in {
      val rdw = RdwData("BB12CD", vehicleClass = 20, deviceCode = 1, maxMass = 2500,
        wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 100, eegCategory = "L1")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AT)
    }

    "be classified when TDC3=Car RDW=None" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.Car, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.CA)
      passage.manualAccordingToSpec must be(false)
      passage.invalidRdwData must be(true)

    }
    "be classified when TDC3=Car RDW=Camper" in {
      val rdw = RdwData("BB12CD", vehicleClass = 25, deviceCode = 68, maxMass = 2500,
        wheelBase = 2.0f, length = 6.5f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1e")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.Car, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CP)
    }
    "be classified when TDC3=Car RDW=Not a Camper" in {
      val rdw = RdwData("BB12CD", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.Car, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CA)
    }
    "be classified when TDC3=Van RDW=None" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.Van, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.CA)
      passage.manualAccordingToSpec must be(false)
      passage.invalidRdwData must be(true)

    }
    "be classified when TDC3=Van RDW=Camper" in {
      val rdw = RdwData("BB12CD", vehicleClass = 25, deviceCode = 68, maxMass = 2500,
        wheelBase = 2.0f, length = 6.5f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1e")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.Van, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CP)
    }
    "be classified when TDC3=Van RDW=Not a Camper" in {
      val rdw = RdwData("BB12CD", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.Van, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CA)
    }
    "be classified when TDC3=Unknown" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.VA)
      passage.manualAccordingToSpec must be(false)
      passage.invalidRdwData must be(true)

    }
    "be classified when TDC3=Unknown RDW=Filled" in {
      val rdw = RdwData("BB12CD", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CA)
      passage.indicator must be(ProcessingIndicator.Manual)
    }
    "be classified when TDC3=None" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)

      val first = vreg("id1", 1000L, "g1", "BB12CD", None, Some(4.3F), Some("NL"))
      val second = vreg("id2", 2000L, "g2", "BB12CD", None, None, Some("NL"))
      val rec = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val passage = classifier.classify(rec)
      getCountry(passage) must be("NL")
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.VA)
      passage.manualAccordingToSpec must be(false)
      passage.invalidRdwData must be(true)

    }
    "be classified when TDC3=None RDW=Filled" in {
      val rdw = RdwData("BB12CD", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)

      val first = vreg("id1", 1000L, "g1", "BB12CD", None, Some(4.3F), Some("NL"))
      val second = vreg("id2", 2000L, "g2", "BB12CD", None, None, Some("NL"))
      val rec = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val passage = classifier.classify(rec)
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CA)
      passage.indicator must be(ProcessingIndicator.Manual)
    }

    "be classified when TDC3=CarTrailer" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.CarTrailer, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.AS)
      passage.manualAccordingToSpec must be(false)
      passage.invalidRdwData must be(true)

    }
    "be classified when TDC3=CarTrailer RDW=Filled" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val rdw = RdwData("BB12CD", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.CarTrailer, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AS)
    }
    "be classified when TDC3=Bus" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.Bus, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.VA)
      passage.manualAccordingToSpec must be(false)
      passage.invalidRdwData must be(true)

    }
    "be classified when TDC3=Bus RDW=Filled" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val rdw = RdwData("BB12CD", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.Bus, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AS)
    }
    "be classified when TDC3=Motor RDW=None" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.Motor, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.CA)
      passage.manualAccordingToSpec must be(false)
      passage.invalidRdwData must be(true)

    }
    "be classified when TDC3=Motor RDW=Camper" in {
      val rdw = RdwData("1VBC23", vehicleClass = 25, deviceCode = 68, maxMass = 2500,
        wheelBase = 2.0f, length = 6.5f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1e")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.Motor, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CP)
    }
    "be classified when TDC3=Motor RDW=Not a Camper" in {
      val rdw = RdwData("BB12CD", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.Motor, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CA)
    }
    "be classified when TDC3=Truck RDW=None" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.Truck, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.VA)
      passage.manualAccordingToSpec must be(false)
      passage.invalidRdwData must be(true)

    }
    "be classified when TDC3=Truck RDW=Camper" in {
      val rdw = RdwData("BB12CD", vehicleClass = 25, deviceCode = 68, maxMass = 2500,
        wheelBase = 2.0f, length = 6.5f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1e")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.Truck, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CP)
    }
    "be classified when TDC3=Truck RDW=Not a Camper" in {
      val rdw = RdwData("BB12CD", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.Truck, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.CA)
    }
    "be classified when TDC3=TruckTrailer" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.TruckTrailer, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.VA)
      passage.manualAccordingToSpec must be(false)
      passage.invalidRdwData must be(true)
    }
    "be classified when TDC3=TruckTrailer RDW=Filled" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val rdw = RdwData("BB12CD", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.TruckTrailer, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AS)
    }
    "be classified when TDC3=SemiTrailer" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.SemiTrailer, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.VA)
      passage.manualAccordingToSpec must be(false)
      passage.invalidRdwData must be(true)

    }
    "be classified when TDC3=SemiTrailer RDW=Filled" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val rdw = RdwData("BB12CD", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.SemiTrailer, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AS)
    }
    "be classified when TDC3=Unknown RDW=Heavy Bus" in {
      val rdw = RdwData("BB12CD", vehicleClass = 20, deviceCode = 1, maxMass = 3501,
        wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AB)
    }
    "be classified when TDC3=None RDW=Heavy Bus" in {
      val rdw = RdwData("BB12CD", vehicleClass = 20, deviceCode = 1, maxMass = 3501,
        wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)

      val first = vreg("id1", 1000L, "g1", "BB12CD", None, Some(4.3F), Some("NL"))
      val second = vreg("id2", 2000L, "g2", "BB12CD", None, None, Some("NL"))
      val rec = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val passage = classifier.classify(rec)
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AB)
    }

    "be classified when TDC3=Unknown RDW=Heavy Tempo BUS" in {
      val rdw = RdwData("BB12CD", vehicleClass = 20, deviceCode = 1, maxMass = 3501,
        wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 100, eegCategory = "L1")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AT)
    }
    "be classified when TDC3=None RDW=Heavy Tempo Bus" in {
      val rdw = RdwData("BB12CD", vehicleClass = 20, deviceCode = 1, maxMass = 3501,
        wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 100, eegCategory = "L1")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)

      val first = vreg("id1", 1000L, "g1", "BB12CD", None, Some(4.3F), Some("NL"))
      val second = vreg("id2", 2000L, "g2", "BB12CD", None, None, Some("NL"))
      val rec = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val passage = classifier.classify(rec)
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AT)
    }

    "be classified when TDC3=Unknown RDW=Heavy Camper" in {
      // NB: since maxMass > 3500 this will be classified as ZB => VehicleCode.VA
      val rdw = RdwData("BB12CD", vehicleClass = 25, deviceCode = 68, maxMass = 3501,
        wheelBase = 2.0f, length = 6.5f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1e")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.VA)
    }
    "be classified when TDC3=Unknown RDW=Heavy Not a Camper" in {
      val rdw = RdwData("BB12CD", vehicleClass = 20, deviceCode = 20, maxMass = 3501,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = classifier.classify(getMatch("BB12CD", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.VA)
    }
  }

  "Dutch ZB License" must {
    for ((description, dms, expected) ← Seq(("Tempo Bus", 100, VehicleCode.AT), ("Bus", 160, VehicleCode.AB))) {
      "be classified when TDC3=Unknown RDW=%s".format(description) in {
        val rdw = RdwData("12BZC3", vehicleClass = 20, deviceCode = 1, maxMass = 2500,
          wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = dms, eegCategory = "L1")
        val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
        val passage = classifier.classify(getMatch("12BZC3", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
        getCountry(passage) must be("NL")
        passage.code must be(Some(expected))
      }
    }

    for (tdc3_category ← Seq(VehicleCategory.Car, VehicleCategory.Truck, VehicleCategory.Bus)) {
      val rdw = RdwData("12BZC3", vehicleClass = 20, deviceCode = 25 /* camper */ , maxMass = 2500,
        wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)

      "be classified CB when TDC3=%s RDW=Camper".format(tdc3_category) in {
        val passage = classifier.classify(getMatch("12BZC3", category = tdc3_category, length = Some(4.5F), country = Some("NL")))
        getCountry(passage) must be("NL")
        passage.code must be(Some(VehicleCode.CB))
        passage.alternativeClassification must be(None)
        passage.manualAccordingToSpec must be(false)
        passage.invalidRdwData must be(false)
      }
    }

    for (tdc3_category ← Seq(VehicleCategory.Car, VehicleCategory.Truck, VehicleCategory.Bus)) {
      val rdw = RdwData("12BZC3", vehicleClass = 20, deviceCode = 2, maxMass = 2500,
        wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)

      "be classified VA when TDC3=%s RDW!=Camper".format(tdc3_category) in {
        val passage = classifier.classify(getMatch("12BZC3", category = tdc3_category, length = Some(4.5F), country = Some("NL")))
        getCountry(passage) must be("NL")
        passage.code must be(Some(VehicleCode.VA))
        passage.alternativeClassification must be(None)
        passage.manualAccordingToSpec must be(false)
        passage.invalidRdwData must be(false)
      }
    }

    for (category ← Seq(VehicleCategory.CarTrailer, VehicleCategory.TruckTrailer)) {
      val rdw = RdwData("12BZC3", vehicleClass = 20, deviceCode = 2, maxMass = 2500,
        wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)

      "be classified AO when TDC3=%s RDW!=Bus".format(category) in {
        val passage = classifier.classify(getMatch("12BZC3", category = category, length = Some(4.5F), country = Some("NL")))
        getCountry(passage) must be("NL")
        passage.code must be(Some(VehicleCode.AO))
        passage.alternativeClassification must be(None)
        passage.manualAccordingToSpec must be(false)
        passage.invalidRdwData must be(false)
      }
    }

    "be classified VA when TDC3=Unknown RDW!=Bus" in {
      val rdw = RdwData("12BZC3", vehicleClass = 20, deviceCode = 2, maxMass = 2500,
        wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = classifier.classify(getMatch("12BZC3", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code must be(Some(VehicleCode.VA))
      passage.alternativeClassification must be(None)
      passage.manualAccordingToSpec must be(false)
      passage.invalidRdwData must be(false)
    }

    "be classified alternativeClassification VA when TDC3=Other RDW!=Bus" in {
      val rdw = RdwData("12BZC3", vehicleClass = 20, deviceCode = 2, maxMass = 2500,
        wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = classifier.classify(getMatch("12BZC3", category = VehicleCategory.Motor, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code must be(None)
      passage.alternativeClassification must be(Some(VehicleCode.VA))
      passage.manualAccordingToSpec must be(false)
      passage.invalidRdwData must be(false)
    }

    "be classified when TDC3=Unknown RDW=None" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("12BZC3", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.VA)
      passage.manualAccordingToSpec must be(false)
      passage.invalidRdwData must be(true)

    }
    "be classified when TDC3=Unknown RDW=Camper" in {
      val rdw = RdwData("BB12CD", vehicleClass = 25, deviceCode = 68, maxMass = 3501,
        wheelBase = 2.0f, length = 6.5f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1e")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("12BZC3", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.VA)
    }
    "be classified when TDC3=Unknown RDW=Not a Camper" in {
      val rdw = RdwData("12BZC3", vehicleClass = 20, deviceCode = 20, maxMass = 3501,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val passage = classifier.classify(getMatch("12BZC3", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.VA)
    }
    "be classified when TDC3=None RDW=Not a Camper" in {
      val rdw = RdwData("12BZC3", vehicleClass = 20, deviceCode = 20, maxMass = 3501,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val first = vreg("id1", 1000L, "g1", "BB12CD", None, Some(4.3F), Some("NL"))
      val second = vreg("id2", 2000L, "g2", "BB12CD", None, None, Some("NL"))
      val rec = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val passage = classifier.classify(rec)
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.VA)
    }

  }

  "Dutch A License" must {
    "be classified when TDC3=Unknown RDW=Ligth" in {
      val rdw = RdwData("12WN34", vehicleClass = 20, deviceCode = 1, maxMass = 1300,
        wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")

      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("12WN34", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AS)
    }
    "be classified when TDC3=None RDW=Ligth" in {
      val rdw = RdwData("12WN34", vehicleClass = 20, deviceCode = 1, maxMass = 1300,
        wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val first = vreg("id1", 1000L, "g1", "12WN34", None, Some(4.3F), Some("NL"))
      val second = vreg("id2", 2000L, "g2", "12WN34", None, None, Some("NL"))
      val rec = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val passage = classifier.classify(rec)
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AS)
    }

    "be classified when TDC3=Unknown RDW=Heavy" in {
      val rdw = RdwData("12WN34", vehicleClass = 20, deviceCode = 1, maxMass = 4300,
        wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("12WN34", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AO)
    }
    "be classified when TDC3=Unknown RDW=None" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("12WN34", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.AO)
      passage.manualAccordingToSpec must be(false)
      passage.invalidRdwData must be(true)

    }
  }
  "Dutch O License" must {
    "be classified when TDC3=Unknown RDW=Ligth" in {
      val rdw = RdwData("OL1234", vehicleClass = 20, deviceCode = 1, maxMass = 1300,
        wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")

      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("OL1234", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AS)
    }
    "be classified when TDC3=Unknown RDW=Heavy" in {
      val rdw = RdwData("OL1234", vehicleClass = 20, deviceCode = 1, maxMass = 4300,
        wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("OL1234", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AO)
    }
    "be classified when TDC3=None RDW=Heavy" in {
      val rdw = RdwData("OL1234", vehicleClass = 20, deviceCode = 1, maxMass = 4300,
        wheelBase = 2.0f, length = 4.0f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), config, writer)
      val first = vreg("id1", 1000L, "g1", "OL1234", None, Some(4.3F), Some("NL"))
      val second = vreg("id2", 2000L, "g2", "OL1234", None, None, Some("NL"))
      val rec = VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")

      val passage = classifier.classify(rec)
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.AO)
    }

    "be classified when TDC3=Unknown RDW=None" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("OL1234", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.AO)
      passage.manualAccordingToSpec must be(false)
      passage.invalidRdwData must be(true)

    }
  }
  "Dutch C License" must {
    "be classified when TDC3=Unknown RDW=speed 25" in {
      val rdw = RdwData("12FZC3", vehicleClass = 20, deviceCode = 10, maxMass = 300,
        wheelBase = 1.2f, length = 1.4f, width = 100, maxSpeed = 25, diffMaxSpeed = 25, eegCategory = "L6")

      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("12FZC3", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.BS)
    }
    "be classified when TDC3=Unknown RDW=Speed 45 L6" in {
      val rdw = RdwData("12FZC3", vehicleClass = 20, deviceCode = 10, maxMass = 300,
        wheelBase = 1.2f, length = 1.4f, width = 100, maxSpeed = 45, diffMaxSpeed = 45, eegCategory = "L6")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("12FZC3", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.BB)
    }
    "be classified when TDC3=Unknown RDW=Speed 45 L1" in {
      val rdw = RdwData("12FZC3", vehicleClass = 20, deviceCode = 10, maxMass = 300,
        wheelBase = 1.2f, length = 1.4f, width = 100, maxSpeed = 45, diffMaxSpeed = 45, eegCategory = "L1")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("12FZC3", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.BF)
    }
    "be classified when TDC3=Unknown RDW=None" in {
      val config = ClassifierConfig(0.4f, 0.5f)
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), config, writer)
      val passage = classifier.classify(getMatch("12FZC3", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code must be(None)
      passage.alternativeClassification.get must be(VehicleCode.VA)
    }
  }
  "Dutch Unknown License" must {
    "be classified when TDC3=Unknown dambord=None RDW=Pers" in {
      val rdw = RdwData("AABBCC", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")

      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("AABBCC", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.PA)
    }
    "be classified when TDC3=Unknown dambord=None RDW=None" in {
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("AABBCC", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.PA)
    }
    "be classified when TDC3=Unknown dambord=Unknown RDW=None" in {
      val classifier = new RdwMatchClassifier(new NoRdwRegistry(), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("20SDBJ", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.PA)
    }
    "be classified when TDC3=Unknown dambord=Unknown RDW=Pers" in {
      val rdw = RdwData("20SDBJ", vehicleClass = 20, deviceCode = 20, maxMass = 2500,
        wheelBase = 2.0f, length = 14.9f, width = 1000, maxSpeed = 150, diffMaxSpeed = 160, eegCategory = "L1")
      val classifier = new RdwMatchClassifier(new MockRdwRegistry(rdw), ClassifierConfig(margin1 = 0.4f, margin2 = 0.5f), writer)
      val passage = classifier.classify(getMatch("20SDBJ", category = VehicleCategory.Unknown, length = Some(4.5F), country = Some("NL")))
      getCountry(passage) must be("NL")
      passage.code.get must be(VehicleCode.PA)
    }
  }

  def getMatch(plate: String, category: VehicleCategory.Value = VehicleCategory.Car, length: Option[Float] = None, country: Option[String] = Some("NL")): VehicleSpeedRecord = {
    val first = vreg("id1", 1000L, "g1", plate, None, length, country)
    val second = vreg("id2", 2000L, "g2", plate, Some(category), None, country)
    VehicleSpeedRecord(second.eventId, first, Some(second), 2002, 100, "")
  }

  def getCountry(passage: VehicleClassifiedRecord): String = {
    passage.speedRecord.exit.get.country.get.value
  }

  def vreg(id: String, timestamp: Long, gantry: String, license: String, category: Option[VehicleCategory.Value], length: Option[Float], country: Option[String]): VehicleMetadata = {
    vregis(id, timestamp, gantry, license, category, length, country)
  }

  def vregis(id: String, timestamp: Long, gantry: String, license: String, category: Option[VehicleCategory.Value], length: Option[Float], country: Option[String]): VehicleMetadata = {
    val lane = Lane("23L", "foo", gantry, "route1", 25f, 37f)
    val lic = Some(ValueWithConfidence(license, 90))
    val cou = country match {
      case None    ⇒ None
      case Some(c) ⇒ Some(ValueWithConfidence(c, 95))
    }
    VehicleMetadata(lane, id, timestamp, Some(new Date(timestamp).toString), lic, None, length.map(new ValueWithConfidence_Float(_, 60)), category.map(cat ⇒ new ValueWithConfidence[String](cat.toString(), 90)), None, cou, Seq(), "", "", "")
  }

  def productionClassifyConfidence: ZkClassifyConfidence = {
    ZkClassifyConfidence(
      minCarLength = 0.1F,
      maxCarLength = 6F,
      maxVanLength = 6.4F,
      minTrailerLength = 5.6F,
      minLargeTruckLength = 12.2F,
      carConfidence = 10,
      trailerConfidence = 10,
      processCategory = "TDC3-TDC1",
      confidenceLimit = 0)
  }

  def getClassify(rdwRegistry: RdwRegistry = new NoRdwRegistry(), classifyConfidence: ZkClassifyConfidence) = {
    new RdwMatchClassifier(rdwRegistry, ClassifierConfig(margin1 = 0.3f, margin2 = 0.5f, classifyConfidence = classifyConfidence), writer)
  }

  def vregisWithConfidence(id: String, timestamp: Long, gantry: String, license: String, category: Option[ValueWithConfidence[String]], length: Option[ValueWithConfidence_Float], country: Option[ValueWithConfidence[String]]): VehicleMetadata = {
    VehicleMetadata(
      lane = Lane("23L", "foo", gantry, "route1", 25f, 37f),
      eventId = id,
      eventTimestamp = timestamp,
      eventTimestampStr = Some(new Date(timestamp).toString),
      license = Some(ValueWithConfidence(license, 90)),
      rawLicense = None,
      length = length,
      category = category,
      speed = None,
      country = country,
      images = Seq(),
      NMICertificate = "",
      applChecksum = "",
      serialNr = "SN1234")
  }
}

private[nl] class MockRdwRegistry(data: RdwData) extends RdwRegistry {
  def getData(plate: String): Option[RdwData] = Some(data.copy(plate = plate))
}

private[nl] class NoRdwRegistry extends RdwRegistry {
  def getData(plate: String): Option[RdwData] = None
}

private[nl] class MockClassifyDecisionWriter extends DecisionResultTable.Writer {
  /**
   * Writes a single row.
   */
  def writeRow(key: String, value: DecisionResult, timestamp: Option[Long]) {}

  /**
   * Writes a series of rows.
   */
  def writeRows(keysAndValues: Seq[(String, DecisionResult)]) {}

  /**
   * Writes a series of rows with explicit timestamps.
   */
  def writeRowsWithTimestamp(keysAndValues: Seq[(String, DecisionResult, Long)]) {}

  /**
   * Writes a series of rows taking the key from the instance. If timestamp is None then
   * current time is used.
   */
  def writeRows(values: Seq[DecisionResult], getKey: (DecisionResult) ⇒ (String, Option[Long])) {}
}
