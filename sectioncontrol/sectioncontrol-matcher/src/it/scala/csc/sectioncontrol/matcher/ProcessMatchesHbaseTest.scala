/*
* Copyright (C) 2012 CSC. <http://www.csc.com>
*/
package csc.sectioncontrol.matcher

import akka.actor.ActorSystem
import akka.dispatch.Await
import akka.pattern.ask
import akka.testkit.TestActorRef
import akka.util.Timeout
import akka.util.duration._
import csc.curator.CuratorTestServer
import csc.curator.utils.Curator
import csc.hbase.utils.HBaseTableKeeper
import csc.hbase.utils.testframework.HBaseTestFramework
import csc.sectioncontrol.messages.{StatsDuration, VehicleMetadata}
import csc.sectioncontrol.messages.matcher.{MatcherCorridorConfig, MatchingMessage, _}
import csc.sectioncontrol.storage.{SectionControlStatistics, StatisticsPeriod}
import csc.sectioncontrol.storagelayer.hbasetables.{MatcherLeftOverTable, SpeedRecordTable, UnmatchedVehicleTable, VehicleTable}
import org.scalatest.matchers.MustMatchers
import org.scalatest.{BeforeAndAfterEach, WordSpec}

class ProcessMatchesHbaseTest extends WordSpec with MustMatchers with HBaseTestFramework with CuratorTestServer with BeforeAndAfterEach {

  implicit val testSystem = ActorSystem("MatchingTest")
  val checker = TestActorRef(new ResultChecker(testSystem))

  private var curator: Curator = _
  val tableKeeper = new HBaseTableKeeper {}

  "MatchingProcessor actor" must {
    "process a message" in {
      val config = MatcherCorridorConfig("A2", "1", 1, "g1", "g2", 2500, 200, 3600000)
      val incoming = VehicleTable.makeReader(testUtil.getConfiguration, tableKeeper)
      val outgoingMatched = SpeedRecordTable.makeWriter(testUtil.getConfiguration, tableKeeper)
      val outgoingUnmatched = UnmatchedVehicleTable.makeWriter(testUtil.getConfiguration, tableKeeper)
      val leftover = MatcherLeftOverTable.makeStore(testUtil.getConfiguration, tableKeeper, config.zkCorridorId)
      val actorRef = TestActorRef(new Matcher("eg33", curator,
        config, incoming, outgoingMatched, outgoingUnmatched, leftover))
      implicit val timeout = Timeout(5 seconds)
      val message = MatchingMessage(StatsDuration.local("20000921"), Long.MaxValue, Some(0))
      actorRef ! message
      val result = checker.underlyingActor.result.get
      val statPeriod = StatisticsPeriod(stats = message.stats, end = message.end, begin = message.begin.getOrElse(0))
      result must be(SectionControlStatistics(statPeriod, config, 0, 0, 0, 0, 0, 0, Nil))
      //
      val getFuture = actorRef ? GetLeftoverMessage
      val getResult = Await.result(getFuture, timeout.duration).asInstanceOf[Seq[VehicleMetadata]]
      getResult must be(Nil)
      //
      val cleanFuture = actorRef ? CleanLeftoverMessage
      val cleanResult = Await.result(cleanFuture, timeout.duration).asInstanceOf[Int]
      cleanResult must be(0)
    }
  }

  override def beforeEach() {
    super.beforeEach()
    curator = CuratorHelper.create(this, clientScope)
  }

  override def afterEach() {
    super.afterEach()
  }

  override protected def beforeAll() {
    super.beforeAll()
    testSystem.eventStream.subscribe(checker, classOf[SectionControlStatistics])
  }

  override protected def afterAll() {
    tableKeeper.closeTables()
    checker.stop()
    testSystem.shutdown()
    super.afterAll()
  }
}
