/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.matcher

import akka.actor.{Actor, ActorSystem}
import akka.dispatch.Await
import akka.pattern.ask
import akka.testkit.TestActorRef
import akka.util.Timeout
import akka.util.duration._
import csc.curator.CuratorTestServer
import csc.curator.utils.Curator
import csc.hbase.utils.HBaseStore
import csc.sectioncontrol.messages.matcher._
import csc.sectioncontrol.messages.{StatsDuration, VehicleMetadata, VehicleSpeedRecord, VehicleUnmatchedRecord}
import csc.sectioncontrol.storage.{LaneInfo, SectionControlStatistics, StatisticsPeriod}
import csc.sectioncontrol.storagelayer.hbasetables.{SpeedRecordTable, UnmatchedVehicleTable, VehicleTable}
import org.apache.hadoop.hbase.util.Bytes
import org.scalatest.matchers.MustMatchers
import org.scalatest.{BeforeAndAfterAll, WordSpec}

import scala.collection.mutable.ListBuffer

class ProcessMatchesNoDataTest extends WordSpec with MustMatchers with BeforeAndAfterAll with CuratorTestServer {
  implicit val testSystem = ActorSystem("MatchingTest")
  var testActor: TestActorRef[Matcher] = _
  val checker = TestActorRef(new ResultChecker(testSystem))
  testSystem.eventStream.subscribe(checker, classOf[SectionControlStatistics])

  implicit def string2ByteArray(s: String): Array[Byte] = Bytes.toBytes(s)

  implicit val timeout = Timeout(5 seconds)
  val period = StatsDuration.local("20121230")
  val config = MatcherCorridorConfig("A2", "1", 1, "g1", "g2", 2500, 200, 3600000)
  private var curator: Curator = _

  override def beforeEach() {
    super.beforeEach()
    val incoming = new MockVehicleMetadataReader(Nil)
    val outgoingMatched = new MockSpeedWriter()
    val outgoingUnmatched = new MockUnmatchedWriter()
    val leftover = new MockLeftoverStore(new ListBuffer)
    curator = CuratorHelper.create(this, clientScope)
    testActor = TestActorRef(new Matcher("eg33", curator,
      config, incoming, outgoingMatched, outgoingUnmatched, leftover))
  }

  override def afterEach() {
    testActor.stop()
    super.afterEach()
  }

  override protected def afterAll() {
    checker.stop()
    testSystem.shutdown()
    super.afterAll()
  }

  "MatchingProcessor with no data" must {

    "do nothing for empty data" in {
      val message = MatchingMessage(period, 5555555555L, Some(0))
      val statPeriod = StatisticsPeriod(stats = message.stats, end = message.end, begin = message.begin.getOrElse(0))
      val getFuture = testActor ? message
      val getResult = Await.result(getFuture, timeout.duration).asInstanceOf[SectionControlStatistics]
      getResult must be(SectionControlStatistics(statPeriod, config, 0, 0, 0, 0, 0, 0, Nil))
      val result = checker.underlyingActor.result.get
      result must be(SectionControlStatistics(statPeriod, config, 0, 0, 0, 0, 0, 0, Nil))
      //check times from Zookeeper
      val timesFuture = testActor ? GetLastTimesMessage
      val timesResult = Await.result(timesFuture, timeout.duration).asInstanceOf[Option[MatchingMessage]]
      timesResult.get must be(message)
    }
    "do nothing for empty data if no start date is provided" in {
      val message = MatchingMessage(period, 5555555555L)
      val getFuture = testActor ? message
      val getResult = Await.result(getFuture, timeout.duration).asInstanceOf[SectionControlStatistics]
      val messageWithTime = StatisticsPeriod(stats = message.stats, end = message.end, begin = (5555555555L - 3600L * 1000L * 24L))
      getResult must be(SectionControlStatistics(messageWithTime, config, 0, 0, 0, 0, 0, 0, Nil))
      val result = checker.underlyingActor.result.get
      result must be(SectionControlStatistics(messageWithTime, config, 0, 0, 0, 0, 0, 0, Nil))
      //check times from Zookeeper
      val timesFuture = testActor ? GetLastTimesMessage
      val timesResult = Await.result(timesFuture, timeout.duration).asInstanceOf[Option[MatchingMessage]]
      timesResult.get must be(MatchingMessage(period, 5555555555L, Some(5555555555L - 3600L * 1000L * 24L)))
    }
    "have no leftovers" in {
      val getFuture = testActor ? GetLeftoverMessage
      val getResult = Await.result(getFuture, timeout.duration).asInstanceOf[Seq[VehicleMetadata]]
      getResult must be(Nil)
    }
    "clear leftovers without errors" in {
      val cleanFuture = testActor ? CleanLeftoverMessage
      val cleanResult = Await.result(cleanFuture, timeout.duration).asInstanceOf[Int]
      cleanResult must be(0)
    }
    "must manage times when no begin provided" in {
      val timesFuture = testActor ? GetLastTimesMessage
      val timesResult = Await.result(timesFuture, timeout.duration).asInstanceOf[Option[MatchingMessage]]
      timesResult must be(None)
      //send time
      val timesFuture2 = testActor ? SetLastTimesMessage(MatchingMessage(period, 111L))
      val timesResult2 = Await.result(timesFuture2, timeout.duration).asInstanceOf[Option[MatchingMessage]]
      timesResult2 must be(None)
      //
      val timesFuture3 = testActor ? GetLastTimesMessage
      val timesResult3 = Await.result(timesFuture3, timeout.duration).asInstanceOf[Option[MatchingMessage]]
      timesResult3.get must be(MatchingMessage(period, 111L, None))
      //send next time
      val timesFuture4 = testActor ? SetLastTimesMessage(MatchingMessage(period, 222L))
      val timesResult4 = Await.result(timesFuture4, timeout.duration).asInstanceOf[Option[MatchingMessage]]
      timesResult4.get must be(MatchingMessage(period, 111L, None))
      //
      val timesFuture5 = testActor ? GetLastTimesMessage
      val timesResult5 = Await.result(timesFuture5, timeout.duration).asInstanceOf[Option[MatchingMessage]]
      timesResult5.get must be(MatchingMessage(period, 222L, None))
    }
  }
}

class ProcessMatchesOnlyLeftOversTest extends WordSpec with MustMatchers with BeforeAndAfterAll with CuratorTestServer {
  implicit val testSystem = ActorSystem("MatchingTest")
  var testActor: TestActorRef[Matcher] = _
  val outgoingUnmatched = new MockUnmatchedWriter()
  val checker = TestActorRef(new ResultChecker(testSystem))
  testSystem.eventStream.subscribe(checker, classOf[SectionControlStatistics])
  val vr: VehicleMetadata = VRBox("id1", 1000L, "g1", "AA00AA", Some(3.5f), Some("NL"))

  implicit def string2ByteArray(s: String): Array[Byte] = Bytes.toBytes(s)

  implicit val timeout = Timeout(5 seconds)
  val period = StatsDuration.local("20120102")
  val config = MatcherCorridorConfig("eg33", "1", 1, "g1", "g2", 2500, 200, 3600000)
  private var curator: Curator = _

  override def beforeEach() {
    super.beforeEach()
    val incoming = new MockVehicleMetadataReader(Nil)
    val outgoingMatched = new MockSpeedWriter()
    val leftovers = new ListBuffer[VehicleMetadata]
    leftovers.append(vr)
    val leftover = new MockLeftoverStore(leftovers)
    curator = CuratorHelper.create(this, clientScope)
    testActor = TestActorRef(new Matcher("eg33", curator,
      config, incoming, outgoingMatched, outgoingUnmatched, leftover))
  }

  override def afterEach() {
    testActor.stop()
    super.afterEach()
  }

  override protected def afterAll() {
    checker.stop()
    testSystem.shutdown()
    super.afterAll()
  }

  "MatchingProcessor with only leftovers" must {

    "report 1 as unmatched" in {
      val message = MatchingMessage(period, Long.MaxValue)
      val statPeriod = StatisticsPeriod(stats = message.stats, end = message.end, begin = Long.MaxValue - 1000L * 3600L * 24L)

      testActor ! message
      val result = checker.underlyingActor.result.get
      val countMap = result.laneStats
      countMap.size must be(1)
      result must be(SectionControlStatistics(statPeriod, config, 0, 0, 1, 0, 0, 0, countMap))
      val unmatchedVehicle :: Nil = outgoingUnmatched.data.toList
      val toCompare = VehicleUnmatchedRecord(vr.eventId, vr, 1, period, 9223372036854775807L)
      unmatchedVehicle must be(toCompare)
      //   have no leftovers
      val getFuture = testActor ? GetLeftoverMessage
      val getResult = Await.result(getFuture, timeout.duration).asInstanceOf[Seq[VehicleMetadata]]
      getResult must be(Nil)
      //check times from Zookeeper
      val timesFuture = testActor ? GetLastTimesMessage
      val timesResult = Await.result(timesFuture, timeout.duration).asInstanceOf[Option[MatchingMessage]]
      timesResult.get must be(MatchingMessage(period, Long.MaxValue, Some(Long.MaxValue - 1000L * 3600L * 24L)))
    }
  }
}

class ProcessMatchesTest extends WordSpec with MustMatchers with BeforeAndAfterAll with CuratorTestServer {
  implicit val testSystem = ActorSystem("MatchingTest")
  var testActor: TestActorRef[Matcher] = _
  val checker = TestActorRef(new ResultChecker(testSystem))
  testSystem.eventStream.subscribe(checker, classOf[SectionControlStatistics])
  var outgoingMatched: MockSpeedWriter = _
  var outgoingUnmatched: MockUnmatchedWriter = _
  var leftover: MockLeftoverStore = _

  implicit def string2ByteArray(s: String): Array[Byte] = Bytes.toBytes(s)

  implicit val timeout = Timeout(5 seconds)
  val period = StatsDuration.local("20110626")
  //val config = MatcherCorridorConfig("route1", "1", 1, "g1", "g2", 40, 200)
  val config = MatcherCorridorConfig("eg33", "1", 1, "g1", "g2", 40, 200, 3600000)
  private var curator: Curator = _

  override def beforeEach() {
    super.beforeEach()
    val incomingData = List(VRBox("25L", "id1", 2000L, "g2", "AA00AA", Some(3.5f), Some("NL")), //matched with leftover
      VRBox("id24", 1501L, "g1", "AA0022", Some(3.6f), Some("NL")),
      VRBox("36L", "id2", 2001L, "g2", "AA0022", Some(3.5f), Some("NL")),
      VRBox("id2666", 2001L, "g2", "AA00WW", Some(3.5f), Some("NL")), //unmatched
      VRBox("id3", 2011L, "g1", "AA00QQ", Some(3.6f), Some("NL"))) //nest leftover
    val incoming = new MockVehicleMetadataReader(incomingData)
    outgoingMatched = new MockSpeedWriter()
    outgoingUnmatched = new MockUnmatchedWriter()
    val leftovers = new ListBuffer[VehicleMetadata]
    val vr: VehicleMetadata = VRBox("id777", 1000L, "g1", "AA00AA", Some(3.5f), Some("NL"))
    leftovers.append(vr)
    leftover = new MockLeftoverStore(leftovers)
    curator = CuratorHelper.create(this, clientScope)
    testActor = TestActorRef(new Matcher("eg33", curator,
      config, incoming, outgoingMatched, outgoingUnmatched, leftover))
  }

  override def afterEach() {
    testActor.stop()
    super.afterEach()
  }

  override protected def afterAll() {
    checker.stop()
    testSystem.shutdown()
    super.afterAll()
  }

  "MatchingProcessor with all data" must {

    "report proper statistics" in {
      val timesFuture = testActor ? SetLastTimesMessage(MatchingMessage(period, 5L))
      Await.result(timesFuture, timeout.duration)
      val message = MatchingMessage(period, 2020L)
      testActor ! message
      val result = checker.underlyingActor.result.get
      val laneCountList: List[LaneInfo] = result.laneStats
      laneCountList.size must be(4)
      val totalCount = laneCountList.foldLeft[Int](0) {
        case (accu, laneInfo) ⇒ accu + laneInfo.count
      }
      totalCount must be(5)
      val statPeriod = StatisticsPeriod(stats = message.stats, end = message.end, begin = 5L)
      result must be(SectionControlStatistics(statPeriod, config, 5, 2, 1, 0, 216, 288, laneCountList))
      outgoingMatched.data.size must be(2)
      outgoingUnmatched.data.size must be(1)
      leftover.data.size must be(1)
      //
      val getFuture = testActor ? GetLeftoverMessage
      val getResult = Await.result(getFuture, timeout.duration).asInstanceOf[Seq[VehicleMetadata]]
      val left :: Nil = getResult
      left must be(VRBox("id3", 2011L, "g1", "AA00QQ", Some(3.6f), Some("NL")))
      //check times from Zookeeper
      val timesFuture2 = testActor ? GetLastTimesMessage
      val timesResult2 = Await.result(timesFuture2, timeout.duration).asInstanceOf[Option[MatchingMessage]]
      timesResult2.get must be(MatchingMessage(period, 2020L, Some(5L)))
    }
  }
}

class MockVehicleMetadataReader(val storage: Seq[VehicleMetadata]) extends VehicleTable.Reader(null, null) {

  override def readRow(key: (String, Long))(implicit mt: Manifest[VehicleMetadata]) = throw new UnsupportedOperationException()

  override def readRows(from: (String, Long), to: (String, Long))(implicit mt: Manifest[VehicleMetadata]) =
    storage.filter(vmd ⇒ from._1 == vmd.lane.system + "-" + vmd.lane.gantry)

}

class MockSpeedWriter extends SpeedRecordTable.Writer {
  val data: ListBuffer[VehicleSpeedRecord] = new ListBuffer

  def writeRow(key: VehicleSpeedRecord, value: VehicleSpeedRecord, ts: Option[Long]) {
    data += value
  }

  def writeRows(keysAndValues: Seq[(VehicleSpeedRecord, VehicleSpeedRecord)]) {
    throw new UnsupportedOperationException()
  }

  def writeRows(values: Seq[VehicleSpeedRecord], getKey: VehicleSpeedRecord ⇒ (VehicleSpeedRecord, Option[Long])) {
    data ++= values
  }

  def writeRowsWithTimestamp(keysAndValues: Seq[(VehicleSpeedRecord, VehicleSpeedRecord, Long)]) {
    throw new UnsupportedOperationException()
  }
}

class MockLeftoverStore(val data: ListBuffer[VehicleMetadata]) extends HBaseStore[VehicleMetadata] {
  def readAllRows(implicit mt: Manifest[VehicleMetadata]) = data.toSeq

  def writeRows(values: Seq[VehicleMetadata]) {
    data ++= values
  }

  def deleteRows(values: Seq[VehicleMetadata]) {
    data --= values
  }
}

class MockUnmatchedWriter extends UnmatchedVehicleTable.Writer {
  val data: ListBuffer[VehicleUnmatchedRecord] = new ListBuffer

  def writeRow(key: VehicleUnmatchedRecord, value: VehicleUnmatchedRecord, ts: Option[Long]) {
    data += value
  }

  def writeRows(keysAndValues: Seq[(VehicleUnmatchedRecord, VehicleUnmatchedRecord)]) {
    throw new UnsupportedOperationException()
  }

  def writeRows(values: Seq[VehicleUnmatchedRecord], getKey: VehicleUnmatchedRecord ⇒ (VehicleUnmatchedRecord, Option[Long])) {
    data ++= values
  }

  def writeRowsWithTimestamp(keysAndValues: Seq[(VehicleUnmatchedRecord, VehicleUnmatchedRecord, Long)]) {
    throw new UnsupportedOperationException()
  }
}

class ResultChecker(system: ActorSystem) extends Actor {
  var r: Option[SectionControlStatistics] = None

  def result = r

  def receive = {
    case res: SectionControlStatistics ⇒
      r = Some(res)

    case _ ⇒
      throw new RuntimeException()
  }
}

