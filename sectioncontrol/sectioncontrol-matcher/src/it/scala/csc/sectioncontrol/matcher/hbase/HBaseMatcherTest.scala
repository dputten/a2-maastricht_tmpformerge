/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.matcher.hbase

import java.util.TimeZone

import csc.hbase.utils.HBaseTableKeeper
import csc.hbase.utils.testframework.HBaseTestFramework
import csc.sectioncontrol.matcher._
import csc.sectioncontrol.messages.matcher.MatcherCorridorConfig
import csc.sectioncontrol.messages.{StatsDuration, VehicleMetadata, VehicleSpeedRecord, VehicleUnmatchedRecord}
import csc.sectioncontrol.storagelayer.hbasetables.{SpeedRecordTable, UnmatchedVehicleTable, VehicleTable}
import org.apache.hadoop.hbase.util.Bytes
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

class HBaseMatcherTest extends WordSpec with MustMatchers with HBaseTestFramework {

  val tableKeeper = new HBaseTableKeeper {}

  override protected def beforeAll() {
    super.beforeAll()
    implicit def string2ByteArray(s: String): Array[Byte] = Bytes.toBytes(s)
  }

  "HBase readers and writers" must {
    "properly serialize and deserialize" in {
      val inputGenerator = new InputGenerator(hbaseConfig, tableKeeper)
      inputGenerator.addVehicleRegistrations()

      val entryPrefix = "route1-entry1"
      val exitPrefix = "route1-exit1"

      val sourceReader = VehicleTable.makeReader(hbaseConfig, tableKeeper)
      Thread.sleep(1000)
      sourceReader.readRows((entryPrefix, inputGenerator.startTime), (entryPrefix, System.currentTimeMillis())).size must be(2)
      sourceReader.readRows((exitPrefix, inputGenerator.startTime), (exitPrefix, System.currentTimeMillis())).size must be(3)

      //println("1")
      val config = MatcherCorridorConfig("route1", "1", 1, "entry1", "exit1", 2500, 200, 3600000)
      val vMatcher = new VehicleMatcher(config)

      val matchedReader = SpeedRecordTable.makeReader(hbaseConfig, tableKeeper, "route1")
      matchedReader.readRows(inputGenerator.startTime, System.currentTimeMillis()).size must be(0)
      //println("2")
      val unmatched = UnmatchedVehicleTable.makeReader(hbaseConfig, tableKeeper)
      unmatched.readRows((entryPrefix, inputGenerator.startTime), (entryPrefix, System.currentTimeMillis())).size must be(0)
      //println("3")
      unmatched.readRows((exitPrefix, inputGenerator.startTime), (exitPrefix, System.currentTimeMillis())).size must be(0)
      //println("4")
      val (matchedCount, unmatchedCount, leftover) = vMatcher.process(inputGenerator.startTime,
        inputGenerator.endTime, inputGenerator.createLeftOvers(inputGenerator.startTime))
      matchedCount must be(2)
      unmatchedCount must be(2)
      leftover.size must be(1)
      //println("5")
      matchedReader.readRows(inputGenerator.startTime, System.currentTimeMillis()).size must be(2)
      unmatched.readRows((inputGenerator.systemId, inputGenerator.startTime), (inputGenerator.systemId, System.currentTimeMillis())).size must be(1)
    }
  }

  override protected def afterAll() {
    try {
      tableKeeper.closeTables()
    } finally {
      super.afterAll()
    }
  }

  class VehicleMatcher(config: MatcherCorridorConfig) {
    def process(begin: Long, end: Long, incomingLeftovers: List[VehicleMetadata]): (Int, Int, Seq[VehicleMetadata]) = {
      val incomingVehicleReader = VehicleTable.makeReader(hbaseConfig, tableKeeper)
      val entryPrefix = config.systemId + "-" + config.entry
      val exitPrefix = config.systemId + "-" + config.exit
      //println("A")
      val incomingData = incomingVehicleReader.readRows((entryPrefix, begin), (entryPrefix, end)) ++ incomingVehicleReader.readRows((exitPrefix, begin), (exitPrefix, end))
      val processor = new CorridorProcessor(config)
      val MatchProcessResult(matched, unmatched, leftover, Nil, _, _, _) = processor.process(incomingData, incomingLeftovers, begin, end)
      //println("B")
      //
      val speedRecordWriter = SpeedRecordTable.makeWriter(hbaseConfig, tableKeeper)
      speedRecordWriter.writeRows(matched, (record: VehicleSpeedRecord) ⇒ (record, Some(record.time)))
      //println("C")
      val unmatchedRecordWriter = UnmatchedVehicleTable.makeWriter(hbaseConfig, tableKeeper)
      val now = System.currentTimeMillis()
      val listUnmatched = unmatched.map(vr ⇒ VehicleUnmatchedRecord(vr.lane.laneId, entry = vr, corridorId = config.id,
        statsDuration = StatsDuration(now, TimeZone.getDefault), matcherRunEnd = now))

      //println("D")
      unmatchedRecordWriter.writeRows(listUnmatched, (record: VehicleUnmatchedRecord) ⇒ (record, Some(record.entry.eventTimestamp)))
      (matched.size, unmatched.size, leftover)
    }
  }

}

