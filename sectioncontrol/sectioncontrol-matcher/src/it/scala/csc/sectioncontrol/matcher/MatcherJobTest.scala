package csc.sectioncontrol.matcher

import java.util.{ TimeZone, Calendar }

import akka.actor.ActorSystem
import akka.event.Logging
import akka.testkit._
import akka.util.Timeout
import akka.util.duration._
import csc.akkautils.RunToken
import csc.curator.utils.{ CuratorToolsImpl, Curator }
import csc.hbase.utils.testframework.HBaseTestFramework
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.messages._
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storagelayer.hbasetables.ProcessingTable
import net.liftweb.json.DefaultFormats
import org.apache.curator.framework.CuratorFrameworkFactory
import org.apache.curator.retry.RetryNTimes
import org.scalatest.{ BeforeAndAfterEach, BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.matcher.MatcherJobRunner
import scala.Some
import csc.sectioncontrol.storage.ZkDirection
import csc.sectioncontrol.storage.ZkSection
import csc.sectioncontrol.messages.Lane
import csc.sectioncontrol.storage.ZkCorridor
import csc.akkautils.RunToken

/**
 * @author csomogyi
 */
class ITMatcherJobTest
  extends TestKit(ActorSystem("macherJobTest"))
  with ImplicitSender
  with WordSpec
  with MustMatchers
  with BeforeAndAfterAll
  with HBaseTestFramework
  with BeforeAndAfterEach {

  implicit val timeout = Timeout(1900 milliseconds)
  val log = Logging(system, self)
  val formats = DefaultFormats + Boot.enumSerializer

  val MINUTES = 60 * 1000
  val systemId = "A2"
  val corridorId = "C"
  val entryId = "X"
  val exitId = "Y"
  val jobPath = Paths.Corridors.getCorridorJobsPath(systemId, corridorId) + "/matcherJob"
  val referenceTime = { // we always start the times based on 08:00:00,000
  val cal = Calendar.getInstance()
    cal.set(Calendar.HOUR_OF_DAY, 8)
    cal.set(Calendar.MINUTE, 0)
    cal.set(Calendar.SECOND, 0)
    cal.set(Calendar.MILLISECOND, 0)
    cal.getTimeInMillis
  }
  val startTime = referenceTime - 30 * MINUTES // matching window start time
  val jobScheduledTime = referenceTime - 1 * MINUTES // scheduled time of the job
  val jobStartTime = referenceTime // actual start time of the job
  val runToken = RunToken(jobScheduledTime, jobStartTime, TimeZone.getDefault().getID)

  var curator: Curator = _
  var matcherJob: TestActorRef[MatcherJobRunner] = _
  var processingWriter: ProcessingTable.Writer = _

  val matcher = TestProbe()
  val runner = TestProbe()
  val matchCompletion = TestProbe()

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    val clientScope = Some(CuratorFrameworkFactory.newClient(zkConnectionString, new RetryNTimes(1, 1000)))
    clientScope.foreach(_.start())
    curator = new CuratorToolsImpl(clientScope, log, formats)
    createCuratorData()
  }

  override protected def beforeEach(): Unit = {
    super.beforeEach()
    //we cannot share matcherJob instance between tests, as it keeps state (lastKnownPeriod)
    matcherJob = TestActorRef(new MatcherJobRunner(runner.ref,matcher.ref,curator, zkConnectionString, runToken, jobPath, systemId, corridorId))
    processingWriter = ProcessingTable.makeWriter(matcherJob.underlyingActor.hbaseConfig, matcherJob.underlyingActor)
    createProcessingRecords()
  }

  def createCuratorData(): Unit = {
    // create path to jobPath
    curator createEmptyPath jobPath

    // create corridor made up from one section
    val section = ZkSection(id = "C1",
      systemId = systemId,
      name = "A2-C1",
      length = 1000.0,
      startGantryId = "X",
      endGantryId = "Y",
      matrixBoards = Seq(),
      msiBlackList = Seq())
    curator.put[ZkSection](Paths.Sections.getConfigPath(systemId, "C1"), section)
    val corridor = ZkCorridor(id = "C",
      name = "A2-C",
      roadType = 0,
      serviceType = ServiceType.SectionControl,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = false,
      dynamaxMqId = "alma",
      approvedSpeeds = Seq(120),
      pshtm = null,
      info = null,
      startSectionId = "C1",
      endSectionId = "C1",
      allSectionIds = Seq("C1"),
      direction = ZkDirection("X", "Y"),
      radarCode = 0,
      roadCode = 0,
      dutyType = "serving-for-good",
      deploymentCode = "???",
      codeText = "la-la-la")
    curator.put[ZkCorridor](Paths.Corridors.getConfigPath(systemId, corridorId), corridor)

    // create default schedule for (systemId, corridorId)
    val path = Paths.Corridors.getSchedulesPath(systemId, corridorId)
    val schedule = ZkSchedule(id = "whole-day",
      indicationRoadworks = ZkIndicationType.False,
      indicationDanger = ZkIndicationType.False,
      indicationActualWork = ZkIndicationType.False,
      invertDrivingDirection = ZkIndicationType.False,
      fromPeriodHour = 0, fromPeriodMinute = 0,
      toPeriodHour = 23, toPeriodMinute = 59,
      speedLimit = 120, signSpeed = 1, signIndicator = false,
      speedIndicatorType = None, supportMsgBoard = false, msgsAllowedBlank = 0)
    curator.put[List[ZkSchedule]](path, List(schedule))
  }

  def createProcessingRecords(): Unit = {
    val lane1 = Lane("A2-X-lane3", "lane3", "X", "A2", 45.2, 21.0)
    val lane2 = Lane("A2-Y-lane3", "lane3", "Y", "A2", 45.2, 21.0)
    val lane3 = Lane("A2-Y-lane2", "lane2", "Y", "A2", 45.2, 21.0)
    // cuckoo's egg - Z does not belong to the corridor
    val lane4 = Lane("A2-Z-lane2", "lane2", "Z", "A2", 45.2, 21.0)

    val time1 = referenceTime - 27 * MINUTES
    val time2 = referenceTime - 26 * MINUTES
    val time3 = referenceTime - 25 * MINUTES
    // this is the earliest time but it should not be in effect as gantry Z is not part of the corridor
    val time4 = referenceTime - 28 * MINUTES

    val record1 = GantryTimeProcessingRecord.create(lane1, time1)
    processingWriter.writeGantryTimeRecord(lane1, record1)
    val record2 = GantryTimeProcessingRecord.create(lane2, time2)
    processingWriter.writeGantryTimeRecord(lane2, record2)
    val record3 = GantryTimeProcessingRecord.create(lane3, time3)
    processingWriter.writeGantryTimeRecord(lane3, record3)
    val record4 = GantryTimeProcessingRecord.create(lane4, time4)
    processingWriter.writeGantryTimeRecord(lane4, record4)
  }

  override protected def afterAll(): Unit = {
    matcherJob.stop()
    system.shutdown()
    super.afterAll()
  }

  "calculatePeriod" must {
    "calculate next schedule when gantry times disabled" in {
      curator.safeDelete(jobPath + "/config")
      curator.put[MatcherJobConfig](jobPath + "/config", MatcherJobConfig.default)
      val matcherJobConfig = matcherJob.underlyingActor.loadMatcherJobConfig()
      val (canMatch, period) = matcherJob.underlyingActor.calculatePeriod(systemId, corridorId, startTime, runToken, matcherJobConfig)
      assert(canMatch)
      assert(period.from === startTime)
      assert(period.to === runToken.startTime - MatcherJobConfig.default.endTimeSafetyInterval)
    }
    "calculate next schedule when gantry times enabled" in {
      val gantryTimeEnabled = MatcherJobConfig(MatcherJobConfig.default.endTimeSafetyInterval, true)
      curator.safeDelete(jobPath + "/config")
      curator.put[MatcherJobConfig](jobPath + "/config", gantryTimeEnabled)
      val matcherJobConfig = matcherJob.underlyingActor.loadMatcherJobConfig()
      val (canMatch, period) = matcherJob.underlyingActor.calculatePeriod(systemId, corridorId, startTime, runToken, matcherJobConfig)
      assert(canMatch)
      assert(period.from === startTime)

      // it should equal to the earliest in HBase
      assert(period.to === referenceTime - 27 * MINUTES)
    }
  }
}
