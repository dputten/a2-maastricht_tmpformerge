/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.matcher

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.messages.matcher.MatcherCorridorConfig

class NoPassTest extends WordSpec with MustMatchers {
  val sectionReader = new SectionReader("one-corridor", "no-pass")

  "Incoming data" must {
    val (data, leftOver) = sectionReader.deserialize()

    "contain 2 incoming vrs" in {
      data.size must be(2)
    }
    "contain 0 leftovers" in {
      leftOver.size must be(0)
    }
  }

  "CorridorProcessor with no data to match" must {
    val processor = new CorridorProcessor(MatcherCorridorConfig("A2", "1", 1, "g1", "g2", 2500, 200, 3600000))
    val (data, leftOver) = sectionReader.deserialize()
    val MatchProcessResult(matched, unmatched, leftover, Nil, _, _, _) = processor.process(data, leftOver, 0, 1000000000000000L)
    "find no matches" in {
      matched.size must be(0)
    }
    "find no leftovers" in {
      leftover.size must be(0)
    }
    "find 2 unmatched" in {
      unmatched.size must be(2)
    }
  }
}
