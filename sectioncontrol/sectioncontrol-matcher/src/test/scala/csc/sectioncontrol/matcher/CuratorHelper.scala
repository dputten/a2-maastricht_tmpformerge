package csc.sectioncontrol.matcher

import org.apache.curator.framework.{ CuratorFramework, CuratorFrameworkFactory }
import org.apache.curator.retry.RetryUntilElapsed
import csc.akkautils.DirectLoggingAdapter
import csc.curator.utils.{ CuratorToolsImpl, Curator }

object CuratorHelper {
  def create(caller: AnyRef, client: Option[CuratorFramework]): Curator = {
    val log = new DirectLoggingAdapter(caller.getClass.getName)
    new CuratorToolsImpl(client, log)
  }
}