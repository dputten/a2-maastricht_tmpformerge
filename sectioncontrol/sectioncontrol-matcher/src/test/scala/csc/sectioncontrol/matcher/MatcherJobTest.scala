package csc.sectioncontrol.matcher

import java.util.{ TimeZone, Calendar }

import akka.actor.ActorSystem
import akka.event.Logging
import akka.testkit.{ TestProbe, TestActorRef, ImplicitSender, TestKit }
import akka.util.Timeout
import akka.util.duration._
import csc.akkautils.{ JobComplete, RunToken }
import csc.curator.utils.{ CuratorToolsImpl, Curator }
import csc.hbase.utils.testframework.HBaseTestFramework
import csc.sectioncontrol.storage._
import net.liftweb.json.DefaultFormats
import org.apache.curator.framework.CuratorFrameworkFactory
import org.apache.curator.retry.RetryNTimes
import org.scalatest.{ BeforeAndAfterEach, BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.messages.matcher.SetNextTimesMessage

class MatcherJobTest
  extends TestKit(ActorSystem("macherJobTest"))
  with ImplicitSender
  with WordSpec
  with MustMatchers
  with BeforeAndAfterAll
  with HBaseTestFramework
  with BeforeAndAfterEach {

  implicit val timeout = Timeout(1900 milliseconds)
  val log = Logging(system, self)
  val formats = DefaultFormats + Boot.enumSerializer

  val MINUTES = 60 * 1000
  val systemId = "A2"
  val corridorId = "C"
  val jobPath = Paths.Corridors.getCorridorJobsPath(systemId, corridorId) + "/matcherJob"
  val referenceTime = { // we always start the times based on 08:00:00,000
    val cal = Calendar.getInstance()
    cal.set(Calendar.HOUR_OF_DAY, 8)
    cal.set(Calendar.MINUTE, 0)
    cal.set(Calendar.SECOND, 0)
    cal.set(Calendar.MILLISECOND, 0)
    cal.getTimeInMillis
  }
  val startTime = referenceTime - 30 * MINUTES // matching window start time
  val jobScheduledTime = referenceTime - 1 * MINUTES // scheduled time of the job
  val jobStartTime = referenceTime // actual start time of the job
  val runToken = RunToken(jobScheduledTime, jobStartTime, TimeZone.getDefault().getID)
  val runToken2 = RunToken(referenceTime, referenceTime, TimeZone.getDefault().getID)

  var curator: Curator = _
  var matcherJob: TestActorRef[MatcherJobRunner] = _
  val matcher = TestProbe()
  val runner = TestProbe()
  val matchCompletion = TestProbe()

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    val clientScope = Some(CuratorFrameworkFactory.newClient(zkConnectionString, new RetryNTimes(1, 1000)))
    clientScope.foreach(_.start())
    curator = new CuratorToolsImpl(clientScope, log, formats)
  }

  override protected def beforeEach(): Unit = {
    super.beforeEach()
    //we cannot share matcherJob instance between tests, as it keeps state (lastKnownPeriod)
    matcherJob = TestActorRef(new MatcherJobRunner(runner.ref, matcher.ref, curator, zkConnectionString, runToken, jobPath, systemId, corridorId))
  }

  override protected def afterAll(): Unit = {
    matcherJob.stop()
    system.shutdown()
    super.afterAll()
  }

  "startFrom" must {
    "skip periods with a zero length" in {
      val matcherJobConfig = MatcherJobConfig(MatcherJobConfig.default.endTimeSafetyInterval, true)

      matcherJob.underlyingActor.startFrom(
        startTime + 180000,
        runToken,
        matcherJobConfig,
        matcher.ref,
        runner.ref,
        matchCompletion.ref)
      runner.expectMsgType[JobComplete](10 seconds)
    }

    "handle periods with a length > 0 but without matchperiods" in {
      val matcherJobConfig = MatcherJobConfig(MatcherJobConfig.default.endTimeSafetyInterval, true)
      matcherJob.underlyingActor.startFrom(
        startTime,
        runToken,
        matcherJobConfig,
        matcher.ref,
        runner.ref,
        matchCompletion.ref)

      matcher.expectMsgType[SetNextTimesMessage](10 seconds)
      matchCompletion.expectMsgType[MatchingFinished](10 seconds)
      runner.expectMsgType[JobComplete](10 seconds)
    }
  }
}
