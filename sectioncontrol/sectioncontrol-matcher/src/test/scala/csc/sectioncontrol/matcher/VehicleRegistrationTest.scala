/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.matcher

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.messages._
import java.util.Date

object VRBox {
  def apply(id: String, timestamp: Long, gantry: String, license: String, length: Option[Float], country: Option[String]): VehicleMetadata = {
    apply("23L", id, timestamp, gantry, license, length, country)
  }

  def apply(laneId: String, id: String, timestamp: Long, gantry: String, license: String, length: Option[Float], country: Option[String]): VehicleMetadata = {
    val lic = Some(ValueWithConfidence(license, 90))
    apply(laneId, id, timestamp, gantry, lic, length, country)
  }
  def apply(laneId: String, id: String, timestamp: Long, gantry: String, license: Option[ValueWithConfidence[String]],
            length: Option[Float], country: Option[String]): VehicleMetadata = {
    apply(laneId, id, timestamp, gantry, license, length, country, None)
  }
  def apply(laneId: String, id: String, timestamp: Long, gantry: String, license: Option[ValueWithConfidence[String]],
            length: Option[Float], country: Option[String], category: Option[ValueWithConfidence[String]]): VehicleMetadata = {
    val lane = Lane(laneId, laneId + "-at-" + gantry, gantry, "eg33", 25f, 37f)
    val len = length match {
      case None    ⇒ None
      case Some(l) ⇒ Some(ValueWithConfidence_Float(l, 90))
    }
    val cou = country match {
      case None    ⇒ None
      case Some(c) ⇒ Some(ValueWithConfidence(c, 90))
    }
    VehicleMetadata(lane, id, timestamp, Some(new Date(timestamp).toString), license, None, len, category, None, cou, Seq(), "", "", "SN1234")
  }
}

class VehicleRegistrationTest extends WordSpec with MustMatchers {
  "VRBox" must {
    val vr = VRBox("id1", 1000L, "g1", "AA00AA", Some(3.5f), Some("NL"))

    "construct normally with good parameters" in {
      vr.eventId must be("id1")
      vr.eventTimestamp must be(1000L)
      vr.lane.gantry must be("g1")
      vr.license.get.value must be("AA00AA")
    }
  }
}
