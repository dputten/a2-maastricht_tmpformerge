/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.matcher

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.messages.matcher.MatcherCorridorConfig
import csc.sectioncontrol.messages.ValueWithConfidence

class CorridorProcessorTest extends WordSpec with MustMatchers {

  "CorridorProcessor" must {
    val processor = new CorridorProcessor(MatcherCorridorConfig("A2", "1", 1, "g1", "g2", 2500, 200, 3600000))

    "find no match for no data" in {
      val MatchProcessResult(matched, unmatched, leftover, Nil, _, _, _) = processor.process(Nil, Nil, 1000L, 5000L)
      matched.size must be(0)
      leftover.size must be(0)
      unmatched.size must be(0)
    }

    "detect within" in {
      processor.isWithin(VRBox("id1", 2000L, "g1", "AA00AA", Some(3.5f), Some("NL")), 1000L, 5000L) must be(true)
      processor.isWithin(VRBox("id1", 1000L, "g2", "AA00AA", Some(3.5f), Some("NL")), 1000L, 5000L) must be(true)
      processor.isWithin(VRBox("id1", 5000L, "g2", "AA00AA", Some(3.5f), Some("NL")), 1000L, 5000L) must be(true)
    }

    "detect out" in {
      processor.isWithin(VRBox("id1", 2000L, "gFoo", "AA00AA", Some(3.5f), Some("NL")), 1000L, 5000L) must be(false)
      processor.isWithin(VRBox("id1", 100L, "g2", "AA00AA", Some(3.5f), Some("NL")), 1000L, 5000L) must be(false)
      processor.isWithin(VRBox("id1", 100000000L, "g2", "AA00AA", Some(3.5f), Some("NL")), 1000L, 5000L) must be(false)
    }

    "find no match for single entry data" in {
      val data = List(VRBox("id1", 2000L, "g1", "AA00AA", Some(3.5f), Some("NL")))
      val result: MatchedValue = processor.doMatch(data)
      result.matched.size must be(0)
      result.unmatched.size must be(1)
      result.doubles.size must be(0)
    }
    "find no match for single exit data" in {
      val data = List(VRBox("id1", 2000L, "g2", "AA00AA", Some(3.5f), Some("NL")))
      val result: MatchedValue = processor.doMatch(data)
      result.matched.size must be(0)
      result.unmatched.size must be(1)
      result.doubles.size must be(0)
    }
    "find match for single proper data" in {
      val data = List(VRBox("id1", 2000L, "g1", "AA00AA", Some(3.5f), Some("NL")),
        VRBox("id2", 52000L, "g2", "AA00AA", Some(3.5f), Some("NL")))
      val result: MatchedValue = processor.doMatch(data)
      result.matched.size must be(1)
      result.unmatched.size must be(0)
      result.doubles.size must be(0)
    }
    "find no match for single too-fast data" in {
      val data = List(VRBox("id1", 2000L, "g1", "AA00AA", Some(3.5f), Some("NL")),
        VRBox("id2", 2001L, "g2", "AA00AA", Some(3.5f), Some("NL")))
      val result: MatchedValue = processor.doMatch(data)
      result.matched.size must be(0)
      result.unmatched.size must be(2)
      result.doubles.size must be(0)
    }
    "find match for single data with varying confidence" in {
      val data = List(VRBox("23L", "id1", 2000L, "g1", Some(ValueWithConfidence("AA00AA", 65)), Some(3.5f), Some("NL")),
        VRBox("23L", "id2", 52001L, "g2", Some(ValueWithConfidence("AA00AA", 98)), Some(3.5f), Some("NL")))
      val result: MatchedValue = processor.doMatch(data)
      result.matched.size must be(1)
      result.unmatched.size must be(0)
      result.doubles.size must be(0)
    }
    "find no match for reversed single data" in {
      val data = List(VRBox("id1", 2000L, "g2", "AA00AA", Some(3.5f), Some("NL")),
        VRBox("id2", 2001L, "g1", "AA00AA", Some(3.5f), Some("NL")))
      val result: MatchedValue = processor.doMatch(data)
      result.matched.size must be(0)
      result.unmatched.size must be(2)
      result.doubles.size must be(0)
    }
    "find no match for double shoot" in {
      val data = List(VRBox("id1", 2000L, "g1", "AA00AA", Some(3.5f), Some("NL")),
        VRBox("id2", 2001L, "g1", "AA00AA", Some(3.5f), Some("NL")))
      val result: MatchedValue = processor.doMatch(data)
      result.matched.size must be(0)
      result.unmatched.size must be(1)
      result.doubles.size must be(1)
    }
    "find no double shoot for too big time difference" in {
      val data = List(VRBox("id1", 2000L, "g1", "AA00AA", Some(3.5f), Some("NL")),
        VRBox("id2", 2000000000001L, "g1", "AA00AA", Some(3.5f), Some("NL")),
        VRBox("id3", 52002L, "g2", "AA00AA", Some(3.5f), Some("NL")))
      val result: MatchedValue = processor.doMatch(data)
      result.matched.size must be(1)
      result.unmatched.size must be(1)
      result.doubles.size must be(0)
    }
    "find double shoot and match" in {
      val data = List(VRBox("id1", 2000L, "g1", "AA00AA", Some(3.5f), Some("NL")),
        VRBox("id2", 2005L, "g1", "AA00AA", Some(3.5f), Some("NL")),
        VRBox("id3", 52102L, "g2", "AA00AA", Some(3.5f), Some("NL")))
      val result: MatchedValue = processor.doMatch(data)
      result.matched.size must be(1)
      result.unmatched.size must be(0)
      result.doubles.size must be(1)
    }
    "choose the best confidence for double shoot with category (first)" in {
      val data = List(VRBox("23L", "id1", 2000L, "g1", Some(ValueWithConfidence("AA00AA", 80)), None, None, Some(ValueWithConfidence("Car", 90))),
        VRBox("24L", "id2", 2001L, "g1", Some(ValueWithConfidence("AA00AA", 90)), None, None, Some(ValueWithConfidence("CarTrailer", 70))))
      val result: MatchedValue = processor.doMatch(data)
      result.matched.size must be(0)
      result.unmatched.size must be(1)
      val best :: Nil = result.unmatched
      best.eventId must be("id1")
      result.doubles.size must be(1)
    }
    "choose the best confidence for double shoot with category (second)" in {
      val data = List(VRBox("23L", "id1", 2000L, "g1", Some(ValueWithConfidence("AA00AA", 90)), None, None, Some(ValueWithConfidence("Car", 70))),
        VRBox("24L", "id2", 2001L, "g1", Some(ValueWithConfidence("AA00AA", 80)), None, None, Some(ValueWithConfidence("CarTrailer", 90))))
      val result: MatchedValue = processor.doMatch(data)
      result.matched.size must be(0)
      result.unmatched.size must be(1)
      val best :: Nil = result.unmatched
      best.eventId must be("id2")
      result.doubles.size must be(1)
    }
    "choose the best confidence for double shoot with one category (first)" in {
      val data = List(VRBox("23L", "id1", 2000L, "g1", Some(ValueWithConfidence("AA00AA", 70)), None, None, Some(ValueWithConfidence("Car", 90))),
        VRBox("24L", "id2", 2001L, "g1", Some(ValueWithConfidence("AA00AA", 80)), None, None))
      val result: MatchedValue = processor.doMatch(data)
      result.matched.size must be(0)
      result.unmatched.size must be(1)
      val best :: Nil = result.unmatched
      best.eventId must be("id1")
      result.doubles.size must be(1)
    }
    "choose the best confidence for double shoot with one category (second)" in {
      val data = List(VRBox("23L", "id1", 2000L, "g1", Some(ValueWithConfidence("AA00AA", 90)), None, None),
        VRBox("24L", "id2", 2001L, "g1", Some(ValueWithConfidence("AA00AA", 80)), None, None, Some(ValueWithConfidence("CarTrailer", 70))))
      val result: MatchedValue = processor.doMatch(data)
      result.matched.size must be(0)
      result.unmatched.size must be(1)
      val best :: Nil = result.unmatched
      best.eventId must be("id2")
      result.doubles.size must be(1)
    }
    "choose the best confidence for double shoot without category (first)" in {
      val data = List(VRBox("23L", "id1", 2000L, "g1", Some(ValueWithConfidence("AA00AA", 90)), None, None),
        VRBox("24L", "id2", 2001L, "g1", Some(ValueWithConfidence("AA00AA", 80)), None, None))
      val result: MatchedValue = processor.doMatch(data)
      result.matched.size must be(0)
      result.unmatched.size must be(1)
      val best :: Nil = result.unmatched
      best.eventId must be("id1")
      result.doubles.size must be(1)
    }
    "choose the best confidence for double shoot without category (second)" in {
      val data = List(VRBox("23L", "id1", 2000L, "g1", Some(ValueWithConfidence("AA00AA", 70)), None, None),
        VRBox("24L", "id2", 2001L, "g1", Some(ValueWithConfidence("AA00AA", 80)), None, None))
      val result: MatchedValue = processor.doMatch(data)
      result.matched.size must be(0)
      result.unmatched.size must be(1)
      val best :: Nil = result.unmatched
      best.eventId must be("id2")
      result.doubles.size must be(1)
    }
    "must not fail if VR is not for this corridor" in {
      val data = List(VRBox("23333L", "id1", 2000L, "g444", Some(ValueWithConfidence("AA00AA", 70)), None, None),
        VRBox("24777L", "id2", 2001L, "g555", Some(ValueWithConfidence("AA00AA", 80)), None, None))
      val result: MatchedValue = processor.doMatch(data)
      result.matched.size must be(0)
      result.unmatched.size must be(2)
      result.doubles.size must be(0)
    }
  }

  "CorridorProcessor" must {
    val processor = new CorridorProcessor(MatcherCorridorConfig("A2", "1", 1, "g1", "g2", 2500, 200, 3600000))

    "find no left over data for a match" in {
      val data = List(VRBox("id1", 2000L, "g1", "AA00AA", Some(3.5f), Some("NL")),
        VRBox("id2", 52001L, "g2", "AA00AA", Some(3.5f), Some("NL")))
      val MatchProcessResult(matched, unmatched, leftover, Nil, _, _, _) = processor.process(data, Nil, 1000L, 55000L)
      matched.size must be(1)
      leftover.size must be(0)
      unmatched.size must be(0)
    }

    "find no left over data for a match with varying confidence" in {
      val data = List(VRBox("23L", "id1", 2000L, "g1", Some(ValueWithConfidence("AA00AA", 65)), Some(3.5f), Some("NL")),
        VRBox("23L", "id2", 52001L, "g2", Some(ValueWithConfidence("AA00AA", 98)), Some(3.5f), Some("NL")))
      val MatchProcessResult(matched, unmatched, leftover, Nil, _, _, _) = processor.process(data, Nil, 1000L, 55000L)
      matched.size must be(1)
      leftover.size must be(0)
      unmatched.size must be(0)
    }

    "find no left over data for a match with start with leftover" in {
      val data = List(VRBox("23L", "id1", 2000L, "g1", Some(ValueWithConfidence("AA00AA", 65)), Some(3.5f), Some("NL")))
      val leftOverStart = List(VRBox("23L", "id2", 52001L, "g2", Some(ValueWithConfidence("AA00AA", 98)), Some(3.5f), Some("NL")))
      val MatchProcessResult(matched, unmatched, leftover, Nil, _, _, _) = processor.process(data, leftOverStart, 1000L, 55000L)
      matched.size must be(1)
      leftover.size must be(0)
      unmatched.size must be(0)
    }

    "find no left over data for old timestamp" in {
      val data = List(VRBox("id1", 3300L, "g1", "AA00AA", Some(3.5f), Some("NL")),
        VRBox("id2", 30000001L, "g1", "AA11AA", Some(3.5f), Some("NL")))
      val MatchProcessResult(matched, unmatched, leftover, Nil, _, _, _) = processor.process(data, Nil, 1000L, 3000000000L)
      matched.size must be(0)
      leftover.size must be(0)
      unmatched.size must be(2)
    }

    "find left over data for new timestamp" in {
      val data = List(VRBox("id1", 4100L, "g1", "AA00AA", Some(3.5f), Some("NL")),
        VRBox("id2", 4000L, "g1", "AA11AA", Some(3.5f), Some("NL")))
      val MatchProcessResult(matched, unmatched, leftover, Nil, _, _, _) = processor.process(data, Nil, 1000L, 5000L)
      matched.size must be(0)
      leftover.size must be(2)
      unmatched.size must be(0)
    }

    "find no left over data for unmatched on exit" in {
      val data = List(VRBox("id1", 4100L, "g1", "AA00AA", Some(3.5f), Some("NL")),
        VRBox("id2", 1450L, "g2", "AA11AA", Some(3.5f), Some("NL")))
      val MatchProcessResult(matched, unmatched, leftover, Nil, _, _, _) = processor.process(data, Nil, 1000L, 5000L)
      matched.size must be(0)
      leftover.size must be(1)
      unmatched.size must be(1)
    }

    "report no unmatched data for double exit shoot" in {
      val data = List(VRBox("id1", 2000L, "g1", "AA00AA", Some(3.5f), Some("NL")),
        VRBox("id2", 52001L, "g2", "AA00AA", Some(3.5f), Some("NL")),
        VRBox("id3", 52011L, "g2", "AA00AA", Some(3.6f), Some("NL")))
      val MatchProcessResult(matched, unmatched, leftover, doubles, _, _, _) = processor.process(data, Nil, 1000L, 55000L)
      matched.size must be(1)
      leftover.size must be(0)
      unmatched.size must be(0)
      doubles.size must be(1)
    }

    "report no unmatched data for double shoot, 4 registrations -> 1 match" in {
      val data = List(VRBox("id1", 2000L, "g1", "AA00AA", Some(3.5f), Some("NL")),
        VRBox("id24", 1900L, "g1", "AA00AA", Some(3.6f), Some("NL")),
        VRBox("id2", 52001L, "g2", "AA00AA", Some(3.5f), Some("NL")),
        VRBox("id3", 52011L, "g2", "AA00AA", Some(3.6f), Some("NL")))
      val MatchProcessResult(matched, unmatched, leftover, doubles, _, _, _) = processor.process(data, Nil, 1000L, 55000L)
      matched.size must be(1)
      leftover.size must be(0)
      unmatched.size must be(0)
      doubles.size must be(2)
    }

    "find no match without license plate" in {
      val data = List(VRBox("lane1", "id1", 2000L, "g1", None, Some(3.5f), None),
        VRBox("lane2", "id2", 2001L, "g2", None, Some(3.5f), None))
      val MatchProcessResult(matched, unmatched, leftover, Nil, _, _, _) = processor.process(data, Nil, 1000L, 5000L)
      matched.size must be(0)
      leftover.size must be(0)
      unmatched.size must be(2)
    }
  }

  "CorridorProcessor with distance 100 km" must {
    val processor = new CorridorProcessor(MatcherCorridorConfig("A2", "1", 1, "g1", "g2", 100000, 200, 3600000))

    "create match with speed 100 km/hour" in {
      val start = VRBox("id1", 0L, "g1", "AA00AA", Some(3.5f), Some("NL"))
      val hour = 3600 * 1000
      val finish = VRBox("id3", hour, "g2", "AA00AA", Some(3.6f), Some("NL"))
      val matched = processor.createSpeedRecord(start, finish, 2002)
      matched.speed must be(100)
    }
  }
}
