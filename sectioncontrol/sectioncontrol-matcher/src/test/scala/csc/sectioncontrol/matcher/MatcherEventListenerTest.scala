/*
* Copyright (C) 2012 CSC. <http://www.csc.com>
*/
package csc.sectioncontrol.matcher

import org.scalatest.matchers.MustMatchers
import akka.testkit.TestActorRef
import akka.actor.ActorSystem
import org.scalatest.{ BeforeAndAfterEach, WordSpec }
import csc.sectioncontrol.messages.StatsDuration
import java.util.TimeZone
import csc.sectioncontrol.storagelayer.ServiceStatisticsRepositoryAccess
import csc.sectioncontrol.storage.SpeedFixedStatistics
import csc.sectioncontrol.storage.StatisticsPeriod
import scala.Some
import csc.sectioncontrol.messages.matcher.MatchingMessage
import csc.sectioncontrol.storage.RedLightStatistics
import csc.sectioncontrol.messages.Lane
import csc.sectioncontrol.storage.LaneInfo
import csc.sectioncontrol.storage.SectionControlStatistics
import csc.sectioncontrol.messages.matcher.MatcherCorridorConfig

class MatcherEventListenerTest extends WordSpec with MustMatchers with BeforeAndAfterEach {
  implicit val testSystem = ActorSystem("MatcherEventListenerTest")
  var testActor: TestActorRef[MatcherEventListener] = _
  var mockServiceStatisticsAccess: MockServiceStatisticsRepositoryAccess = _

  override protected def beforeEach() {
    super.beforeEach()
    mockServiceStatisticsAccess = new MockServiceStatisticsRepositoryAccess()
    testActor = TestActorRef(new MatcherEventListener("eg33", mockServiceStatisticsAccess))
  }

  override protected def afterEach() {
    testActor.stop()
    super.afterEach()
  }

  "ProcessStatistics" must {
    val zone = TimeZone.getTimeZone("Europe/Amsterdam")
    val request = MatchingMessage(StatsDuration("20120401", zone), 99999L, Some(88888L))
    val config = MatcherCorridorConfig("eg33", "1", 1, "g1", "g2", 2500, 200, 3600000)
    val lanes = List(LaneInfo(Lane("23L", "foo", "g1", "route1", 25f, 37f), 1),
      LaneInfo(Lane("24L", "foo", "g1", "route1", 25f, 37f), 300),
      LaneInfo(Lane("25L", "foo", "g1", "route1", 25f, 37f), 99))

    "save matcher statistics" in {
      val statPeriod = StatisticsPeriod(stats = request.stats, end = request.end, begin = request.begin.getOrElse(0))
      val event1 = SectionControlStatistics(statPeriod, config, 1000, 400, 100, 0, 85.0, 165, lanes)

      mockServiceStatisticsAccess.saveCalledForSectionControlStatistics must be(false)

      testSystem.eventStream.publish(event1)

      mockServiceStatisticsAccess.saveCalledForSectionControlStatistics must be(true)
      mockServiceStatisticsAccess.savedSectionControlStatisticsEqual("eg33", event1) must be(true)
    }
  }
}

/**
 * Mock implementation of the ServiceStatisticsRepositoryAccess trait.
 */
class MockServiceStatisticsRepositoryAccess extends ServiceStatisticsRepositoryAccess {

  private var savedSystemId: String = ""
  private var savedSectionControlStatistics: Option[SectionControlStatistics] = None

  def saveCalledForSectionControlStatistics = !savedSystemId.isEmpty && savedSectionControlStatistics.isDefined

  def savedSectionControlStatisticsEqual(systemId: String, statistics: SectionControlStatistics): Boolean = {
    savedSystemId == systemId && savedSectionControlStatistics == Some(statistics)
  }

  def getSectionControlStatistics(systemId: String, statsDuration: StatsDuration): Seq[SectionControlStatistics] = Seq() // Not needed for report tests

  def getRedLightStatistics(systemId: String, statsDuration: StatsDuration): Seq[RedLightStatistics] = Seq() // Not needed for report tests

  def getSpeedFixedStatistics(systemId: String, statsDuration: StatsDuration): Seq[SpeedFixedStatistics] = Seq() // Not needed for report tests

  def save(systemId: String, statistics: SectionControlStatistics): Unit = {
    savedSystemId = systemId
    savedSectionControlStatistics = Some(statistics)
  }

  def save(systemId: String, statistics: RedLightStatistics): Unit = {} // Not needed for report tests

  def save(systemId: String, statistics: SpeedFixedStatistics): Unit = {} // Not needed for report tests
}