/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.matcher
import java.io.File
import scala.io.Source
import java.text.SimpleDateFormat
import csc.sectioncontrol.messages.VehicleSpeedRecord
import csc.sectioncontrol.messages.VehicleMetadata
import org.slf4j.LoggerFactory

class SectionReader(sectionName: String, passName: String) {
  val prefix = SectionReader.FOLDER_NAME + File.separator + sectionName + File.separator + passName

  val inputDataSuffix = ".input.data.txt"
  val inputLeftOverSuffix = ".input.leftover.txt"
  val matchesSuffix = ".output.matched.txt"
  val outputLeftOverSuffix = ".output.leftover.txt"
  val outputUnmatchedSuffix = ".output.unmatched.txt"

  private def readLines(suffix: String): List[String] = {
    val fullName = prefix + suffix
    val input = getClass.getResourceAsStream(fullName)
    if (input == null)
      Nil
    else
      Source.fromInputStream(input).getLines().toList.filterNot(_.startsWith("//"))
  }

  private def readVRs(fileName: String): List[VehicleMetadata] = {
    readLines(fileName).map(SectionReader.parseVR(_))
  }

  def getOutputMatches(): List[String] = {
    readLines(matchesSuffix).map(SectionReader.parseContentEntry(_))
  }

  def deserialize(): (List[VehicleMetadata], List[VehicleMetadata]) = {
    (readVRs(inputDataSuffix), readVRs(inputLeftOverSuffix))
  }

}
object SectionReader {
  private val log = LoggerFactory.getLogger(this.getClass.getName)
  val BATCH_LIST = "/batch-projects.txt"
  val FOLDER_NAME = "/batch-projects"
  val ContentEntry = """\(([^\)]+)\).*""".r // anything inside brackets ()
  val VREntry = """([^,]+),([^,]+),([^,]+),([^,]+),(.+)""".r
  val dateFormat = new SimpleDateFormat("HH:mm:ss")

  def readProjectNames(): List[String] = {
    val lines = Source.fromInputStream(getClass.getResourceAsStream(SectionReader.BATCH_LIST)).getLines().toList
    val head :: tail = lines
    if (!head.startsWith("//test batch projects")) log.warn("Warning: batch file must start with '//test batch projects'")
    tail.map(_.trim).filterNot(_.startsWith("//")).toList
  }

  def parseVR(data: String): VehicleMetadata = {
    val line = parseContentEntry(data)
    line match {
      case VREntry(id, timestamp, gantry, license, length) ⇒ {
        val d = dateFormat.parse(timestamp)
        VRBox(id.trim, d.getTime(), gantry.trim, license.trim, Some(length.toFloat), Some("NL"))
      }
      case error ⇒ throw new RuntimeException("Unexpected VR: " + error)
    }
  }

  //matches
  def parseContentEntry(line: String): String = {
    line match {
      case ContentEntry(data) ⇒ data.trim
      case error              ⇒ throw new RuntimeException("Unexpected test entry: " + error)
    }
  }

  def convertToString(m: VehicleSpeedRecord): String = {
    "%s, %s".format(m.entry.eventId, m.exit.get.eventId)
  }

}

