/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.matcher

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.messages.matcher.MatcherCorridorConfig

class CompleteBatchTest extends WordSpec with MustMatchers {

  "no data for CorridorProcessor" must {
    val sectionReader = new SectionReader("one-corridor", "NON-EXISTENT")

    "find no match" in {
      val processor = new CorridorProcessor(MatcherCorridorConfig("A2", "1", 1, "g1", "g2", 2500, 200, 3600000))
      val (data, leftOver) = sectionReader.deserialize()
      val MatchProcessResult(matched, unmatched, leftover, Nil, _, _, _) = processor.process(data, leftOver, 0, 1000000000000000L)
      matched.size must be(0)
      unmatched.size must be(0)
      leftover.size must be(0)
    }
  }

  "no-pass data for CorridorProcessor" must {
    val sectionReader = new SectionReader("one-corridor", "no-pass")
    val processor = new CorridorProcessor(MatcherCorridorConfig("A2", "1", 1, "g1", "g2", 2500, 200, 3600000))
    val (data, leftOver) = sectionReader.deserialize()
    val MatchProcessResult(matched, unmatched, leftover, Nil, _, _, _) = processor.process(data, leftOver, 0, 1000000000000000L)

    "find no match" in {
      matched.size must be(0)
    }
    "find no leftovers" in {
      leftover.size must be(0)
    }
    "find 2 unmatched" in {
      unmatched.size must be(2)
    }
  }

  "single-pass CorridorProcessor" must {
    val sectionReader = new SectionReader("one-corridor", "single-pass")

    "find one match" in {
      val processor = new CorridorProcessor(MatcherCorridorConfig("A2", "1", 1, "g1", "g2", 2500, 200, 3600000))
      val (data, leftOver) = sectionReader.deserialize()
      val MatchProcessResult(matched, unmatched, leftover, Nil, _, _, _) = processor.process(data, leftOver, 0, 1000000000000000L)
      unmatched.size must be(0)
      leftover.size must be(0)
      val standard = sectionReader.getOutputMatches()
      matched.size must be(standard.size)
      val zipped = standard zip matched
      zipped.foreach {
        case (expected, value) ⇒ expected must be(SectionReader.convertToString(value))
      }

    }
  }

  "example1 data" must {
    val sectionReader = new SectionReader("one-corridor", "example1")
    val processor = new CorridorProcessor(MatcherCorridorConfig("A2", "1", 1, "g1", "g2", 2500, 200, 3600000))
    val (data, leftOver) = sectionReader.deserialize()
    val MatchProcessResult(matched, unmatched, leftover, Nil, _, _, _) = processor.process(data, leftOver, 0, 1000000000000000L)
    val standard = sectionReader.getOutputMatches()
    "find 3 matches" in {
      matched.size must be(standard.size)
      matched.size must be(3)
      val zipped = standard zip matched
      zipped.foreach {
        case (expected, value) ⇒ expected must be(SectionReader.convertToString(value))
      }
    }
  }
}
