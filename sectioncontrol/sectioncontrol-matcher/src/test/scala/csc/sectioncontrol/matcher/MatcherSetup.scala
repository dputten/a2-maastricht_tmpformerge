/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.matcher

import net.liftweb.json.DefaultFormats
import csc.config.Path
import csc.akkautils.{ DirectLoggingAdapter, JobSchedule }
import akka.util.duration._
import csc.sectioncontrol.storage._
import csc.sectioncontrol.messages.VehicleCode
import csc.json.lift.EnumerationSerializer
import csc.curator.utils.Curator

/**
 * Utility class to load a basic zookeeper config for testing the Matcher boot.
 * Also loads the necessary configuration for vehicle-registration to run.
 *
 * @author Maarten Hazewinkel
 */
object MatcherSetup extends App {

  val formats = DefaultFormats + new EnumerationSerializer(ZkWheelbaseType, VehicleCode,
    ConfigType)
  val log = new DirectLoggingAdapter(this.getClass.getName)
  val zkStore = Curator.createCuratorWithLogger(this, log, zkServers = "localhost:2181", extraFormats = formats)

  def saveZk[T <: AnyRef](p: String, v: T) {
    if (zkStore.exists(Path(p)))
      zkStore.deleteRecursive(Path(p))
    zkStore.put(Path(p), v)
  }

  val systemId = "A2"

  val p = ZkPreference(List(ZkWheelBase(ZkWheelbaseType.LBB2M, 2.5),
    ZkWheelBase(ZkWheelbaseType.LBB2M3M, 4.0),
    ZkWheelBase(ZkWheelbaseType.LBB3M4M, 5.0),
    ZkWheelBase(ZkWheelbaseType.LBB4M5M, 6.0),
    ZkWheelBase(ZkWheelbaseType.LBB5M, 7.5),
    ZkWheelBase(ZkWheelbaseType.LBB5M, 7.5),
    ZkWheelBase(ZkWheelbaseType.P2M, 2.0),
    ZkWheelBase(ZkWheelbaseType.P2M3M, 3.5),
    ZkWheelBase(ZkWheelbaseType.P3M4M, 4.5),
    ZkWheelBase(ZkWheelbaseType.P4M5M, 5.5),
    ZkWheelBase(ZkWheelbaseType.P5M, 7.0)),
    List(ZkVehicleMaxSpeed(VehicleCode.AB, 80),
      ZkVehicleMaxSpeed(VehicleCode.AO, 80),
      ZkVehicleMaxSpeed(VehicleCode.AS, 90),
      ZkVehicleMaxSpeed(VehicleCode.AT, 100),
      ZkVehicleMaxSpeed(VehicleCode.CA, 120),
      ZkVehicleMaxSpeed(VehicleCode.CB, 80),
      ZkVehicleMaxSpeed(VehicleCode.CP, 120),
      ZkVehicleMaxSpeed(VehicleCode.MF, 120),
      ZkVehicleMaxSpeed(VehicleCode.MV, 120),
      ZkVehicleMaxSpeed(VehicleCode.PA, 130),
      ZkVehicleMaxSpeed(VehicleCode.VA, 80)),
    List(ZkSpeedLimitMargin(30, 7),
      ZkSpeedLimitMargin(40, 7),
      ZkSpeedLimitMargin(50, 7),
      ZkSpeedLimitMargin(60, 7),
      ZkSpeedLimitMargin(70, 7),
      ZkSpeedLimitMargin(80, 7),
      ZkSpeedLimitMargin(90, 7),
      ZkSpeedLimitMargin(100, 8),
      ZkSpeedLimitMargin(110, 8),
      ZkSpeedLimitMargin(120, 8),
      ZkSpeedLimitMargin(130, 8)),
    1000, 40, 195, 50)
  saveZk(Paths.Preferences.getConfigPath(systemId), p)
  saveZk(Paths.Preferences.getConfigPath(systemId), p)

  val c = ZkCorridor("hh1", "1", 1, ServiceType.SectionControl, ZkCaseFileType.HHMVS40, false, "", Seq(),
    ZkPSHTMIdentification(true, true, true, false, false, false, false, false, false, "x"),
    ZkCorridorInfo(2, "1234", "TC UT A2 L-1 (100)", "a0020011", true, "location1"),
    "A", "A", Seq("A"), ZkDirection("Amsterdam", "Utrecht"), radarCode = 14, roadCode = 20,
    dutyType = "", deploymentCode = "", codeText = "")
  saveZk(Paths.Corridors.getConfigPath(systemId, "hh1"), c)

  val s1 = ZkSection("A", systemId, "A", 2983.0, "9547", "9542", Seq("989"), Nil)
  saveZk(Paths.Sections.getConfigPath(systemId, "A"), s1)

  val j = JobSchedule(true, 0, None, runAtMillisInterval = Some(300000L), timeZone = "Europe/Amsterdam")
  saveZk(Paths.Corridors.getCorridorJobsPath(systemId, "hh1") + "/matcherJob", j)
  val mc = MatcherJobConfig(5.minutes.toMillis)
  saveZk(Paths.Corridors.getCorridorJobsPath(systemId, "hh1") + "/matcherJob/config", mc)

  val r1 = PirRadarCameraSensorConfig("100", systemId, "9547", "lane1",
    CameraConfig("/home/mhazewinkel/simdata/ftproot/lane1_00", "localhost", 13010, 10, 10000),
    RadarConfig("mina:tcp://localhost:11010?textline=true", 33.5, 5.3, 1.0),
    PIRConfig("localhost", 12011, 2, 1000, "localhost", 12010))
  saveZk("/ctes/systems/" + systemId + "/gantries/9547/lanes/100/config", r1)
  val r2 = PirRadarCameraSensorConfig("300", systemId, "9542", "lane1",
    CameraConfig("/home/mhazewinkel/simdata/ftproot/lane3_00", "localhost", 13030, 10, 10000),
    RadarConfig("mina:tcp://localhost:11030?textline=true", 33.5, 5.3, 1.0),
    PIRConfig("localhost", 12031, 2, 1000, "localhost", 12030))
  saveZk("/ctes/systems/" + systemId + "/gantries/9542/lanes/300/config", r2)

  val g1 = Gantry("9547", systemId, "9547", 0.0, 0.0)
  saveZk("/ctes/systems/" + systemId + "/gantries/9547/config", g1)
  val g3 = Gantry("9542", systemId, "9542", 0.0, 0.0)
  saveZk("/ctes/systems/" + systemId + "/gantries/9542/config", g3)
}

case class PirRadarCameraSensorConfig(id: String,
                                      systemId: String,
                                      gantryId: String,
                                      name: String,
                                      relaySensor: CameraConfig,
                                      radar: RadarConfig,
                                      pir: PIRConfig)

case class PIRConfig(pirHost: String, pirPort: Int, pirAddress: Short, refreshPeriod: Long, vrHost: String, vrPort: Int)

case class RadarConfig(radarURI: String, measurementAngle: Double, height: Double, surfaceReflection: Double)

case class CameraConfig(relayURI: String, cameraHost: String, cameraPort: Int, maxTriggerRetries: Int, timeDisconnected: Long)

case class Gantry(id: String, systemId: String, name: String, longitude: Double, latitude: Double)
