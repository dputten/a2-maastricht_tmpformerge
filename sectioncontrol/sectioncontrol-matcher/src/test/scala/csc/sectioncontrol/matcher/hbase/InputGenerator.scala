/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.matcher.hbase

import org.apache.hadoop.hbase.client._
import csc.sectioncontrol.messages._
import java.util.Date
import org.apache.hadoop.hbase.util.Bytes
import csc.sectioncontrol.storagelayer.hbasetables.{ VehicleTable, SpeedRecordTable }
import org.apache.hadoop.conf.Configuration
import csc.hbase.utils.HBaseTableKeeper

/**
 * Generate Vehicle registrations
 */
class InputGenerator(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper) {
  val systemId = "route1"
  val writer = VehicleTable.makeWriter(hbaseConfig, tableKeeper)

  val startTime = System.currentTimeMillis() - 1000 * 3600 * 24 * 3 //3 days ago
  val endTime = System.currentTimeMillis() - 1000 * 3600 * 24 * 2 // 2 days ago

  def addVehicleRegistrations() {
    val list = List(createVR(startTime + 1, "entry1", "12AB34", Some(4.5f), Some("NL")),
      createVR(startTime + 60 * 10 * 1000, "exit1", "12AB34", Some(4.5f), Some("NL")),
      createVR(startTime + 3, "exit1", "12AB99", Some(4.5f), Some("NL")), // unmatched
      createVR(startTime + 4, "exit1", "12AB45", Some(4.5f), Some("NL")), // matched with leftover
      createVR(endTime - 1000 * 60 * 1, "entry1", "12ABWW", Some(4.5f), Some("NL"))) // next leftover
    writer.writeRows(list, (record: VehicleMetadata) ⇒ (record, None))
  }

  def createLeftOvers(start: Long): List[VehicleMetadata] = {
    val list = List(createVR(start - 1000 * 60 * 3, "entry1", "12AB45", Some(4.5f), Some("NL")),
      createVR(start - 1000 * 60 * 4, "exit1", "12ABQQ", Some(5.5f), Some("NL")))
    list
  }

  private def createVR(timestamp: Long, gantry: String, license: String, length: Option[Float], country: Option[String]): VehicleMetadata = {
    val lane = Lane("23L", "foo", gantry, systemId, 25f, 37f)
    val lic = Some(ValueWithConfidence(license, 90))
    val len = length match {
      case None    ⇒ None
      case Some(l) ⇒ Some(ValueWithConfidence_Float(l, 90))
    }
    val cou = country match {
      case None    ⇒ None
      case Some(c) ⇒ Some(ValueWithConfidence(c, 90))
    }
    VehicleMetadata(lane, timestamp.toString + "-" + license, timestamp, Some(new Date(timestamp).toString), lic, None, len, None, None, cou, Seq(), "", "", "SN1234")
  }

}

