/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.matcher

import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }
import akka.actor.ActorSystem
import akka.testkit.TestActorRef
import csc.sectioncontrol.storage.{ CompletionCorridorStatus, SystemCorridorCompletionStatus, Paths }
import csc.config.Path
import csc.curator.CuratorTestServer
import csc.curator.utils.{ CuratorToolsImpl, Curator }
import csc.akkautils.DirectLogging

/**
 *
 * @author Maarten Hazewinkel
 */
class ManageMatcherStatusTest extends WordSpec with MustMatchers
  with CuratorTestServer with BeforeAndAfterAll with DirectLogging {

  implicit val testSystem = ActorSystem("ManageMatcherStatusTest")
  var testMatchCompletion: TestActorRef[ManageMatcherStatus] = _
  private var curator: Curator = _

  override def beforeEach() {
    super.beforeEach()
    curator = new CuratorToolsImpl(clientScope, log)
    testMatchCompletion = TestActorRef(new ManageMatcherStatus(curator))
  }

  override def afterEach() {
    testMatchCompletion.stop()
    super.afterEach()
  }

  override protected def afterAll() {
    testSystem.shutdown()
    super.afterAll()
  }

  val testSystemId = "unittest"

  val testPath = Path(Paths.Systems.getMatchCompletionStatusPath(testSystemId))

  "ManageMatcherStatus" must {
    "load a default instance when nothing is stored" in {
      val (status, version) = testMatchCompletion.underlyingActor.loadStatusAndVersion(testPath)
      status must be(SystemCorridorCompletionStatus(Map()))
      version must be(0)
    }

    "update status for corridors" in {
      testMatchCompletion.underlyingActor.updateCorridorStatus(testSystemId, "corridor1", _.copy(lastUpdateTime = 1000, completeUntil = 500))
      testMatchCompletion.underlyingActor.updateCorridorStatus(testSystemId, "corridor1", _.copy(lastUpdateTime = 2000, completeUntil = 500))
      testMatchCompletion.underlyingActor.updateCorridorStatus(testSystemId, "corridor2", _.copy(lastUpdateTime = 2200, completeUntil = 1500))
      val (status, version) = testMatchCompletion.underlyingActor.loadStatusAndVersion(testPath)
      status must be(SystemCorridorCompletionStatus(Map("corridor1" -> CompletionCorridorStatus(2000, 500),
        "corridor2" -> CompletionCorridorStatus(2200, 1500))))
      version must be(3)
    }

    "process update messages" in {
      testMatchCompletion ! MatchingFinished(testSystemId, "corridor3", 1000, 500)
      testMatchCompletion ! MatchingStarted(testSystemId, "corridor3", 2000)
      testMatchCompletion ! MatchingFinished(testSystemId, "corridor4", 2200, 1500)
      val (status, version) = testMatchCompletion.underlyingActor.loadStatusAndVersion(testPath)
      status must be(SystemCorridorCompletionStatus(Map("corridor3" -> CompletionCorridorStatus(2000, 500),
        "corridor4" -> CompletionCorridorStatus(2200, 1500))))
      version must be(3)
    }
  }
}
