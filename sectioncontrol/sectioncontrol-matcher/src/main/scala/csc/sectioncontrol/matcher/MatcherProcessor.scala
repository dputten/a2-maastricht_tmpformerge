/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.matcher

import csc.sectioncontrol.messages._

/**
 * Matcher result with statistical data
 * @param matched matched vehicle registrations
 * @param unmatched unmatched vehicle registrations
 * @param leftover vehicles to be precessed by the next run
 * @param doubles detected double shoots of the same vehicle at the same gantry at the same time
 * @param sumSpeed sum of all speeds
 * @param maxSpeed max speed
 * @param laneStats vehicle registrations per Lane
 */
case class MatchProcessResult(matched: Seq[VehicleSpeedRecord],
                              unmatched: Seq[VehicleMetadata],
                              leftover: Seq[VehicleMetadata],
                              doubles: Seq[VehicleMetadata],
                              sumSpeed: Int, //Req 61: sum of all speed records
                              maxSpeed: Int, //req 61: max speed
                              /**
 * laneStats maps Lane to number of vehicle registrations (matched or unmatched)
 * Req61: het aantal gedetecteerde voertuigpassages, per rij- en vluchtstrook
 */
                              laneStats: Map[Lane, Int])

/**
 * Procedure to find matched vehicle registrations for a single corridor
 */
trait MatcherProcessor {

  /**
   * Process registrations and report matched and unmatched data
   * @param data - registrations from the current time corridor
   * @param leftOver - unmatched registrations from the previous run
   * @return matched data, unmatched records, left over registrations for the next process
   */
  def process(data: TraversableOnce[VehicleMetadata], leftOver: Seq[VehicleMetadata], begin: Long, end: Long): MatchProcessResult
}

