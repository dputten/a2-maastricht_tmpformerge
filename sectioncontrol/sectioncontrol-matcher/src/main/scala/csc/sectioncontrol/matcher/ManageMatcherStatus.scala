/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.matcher

import akka.actor.{ ActorLogging, Actor }

import csc.curator.utils.{ Curator, Versioned /*, CuratorActor, ZkStoreActor*/ }

import csc.sectioncontrol.storage.{ CompletionCorridorStatus, SystemCorridorCompletionStatus }
import csc.config.Path

import csc.sectioncontrol.messages.matcher._
import csc.sectioncontrol.storage.{ ZkGantry, Paths }

/**
 * Manages status information on the completion of matching jobs.
 *
 * @author Maarten Hazewinkel
 */
private[matcher] class ManageMatcherStatus(curator: Curator) extends Actor with ActorLogging {

  protected def receive = {
    case MatchingStarted(systemId, corridorId, startedAt) ⇒
      updateCorridorStatus(systemId, corridorId, _.copy(lastUpdateTime = startedAt))

    case MatchingFinished(systemId, corridorId, finishedAt, completedUntil) ⇒
      try {
        updateCorridorStatus(systemId, corridorId, _.copy(lastUpdateTime = finishedAt, completeUntil = completedUntil))
      } catch {
        // Catch any exception and queue a retry message in the mailbox.
        // This ensures a single retry for storing the new completion status.
        case e: Exception ⇒
          self ! MatchingFinishedRetry(systemId, corridorId, finishedAt, completedUntil)
          throw e
      }

    case MatchingFinishedRetry(systemId, corridorId, finishedAt, completedUntil) ⇒
      // Do one retry for storing the updated completion status.
      updateCorridorStatus(systemId, corridorId, _.copy(lastUpdateTime = finishedAt, completeUntil = completedUntil))
  }

  private[matcher] def updateCorridorStatus(systemId: String, corridorId: String,
                                            statusUpdate: (CompletionCorridorStatus ⇒ CompletionCorridorStatus)) {
    val path = Path(Paths.Systems.getMatchCompletionStatusPath(systemId))
    val (status, version) = loadStatusAndVersion(path)
    val oldCorridorStatus = status.corridors.getOrElse(corridorId, CompletionCorridorStatus(0, 0))
    val updatedCorridorStatus = statusUpdate(oldCorridorStatus)
    val updatedStatus = status.copy(corridors = status.corridors.updated(corridorId, updatedCorridorStatus))
    // Store exceptions will cause a single retry (see receive method). If the retry is not successful the
    // status update will be lost which may cause some issues with the next run of the matcher
    curator.set(path.toString(), updatedStatus, version.toInt)
    //zkStore.put(path, updatedStatus, version)
  }

  private[matcher] def loadStatusAndVersion(path: Path): (SystemCorridorCompletionStatus, Long) = {
    curator.getVersioned[SystemCorridorCompletionStatus](path.toString()) match {
      case Some(Versioned(s, v)) ⇒ (s, v)
      case None ⇒
        val status = SystemCorridorCompletionStatus(Map())
        curator.put(path.toString(), status)
        loadStatusAndVersion(path)
    }
  }

}

/**
 * Signal that matching is being started for the indicated system and corridor
 */
private[matcher] case class MatchingStarted(systemId: String, corridorId: String, startedAt: Long)

/**
 * Signal that matching has successfully completed for the indicated system and corridor
 */
private[matcher] case class MatchingFinished(systemId: String, corridorId: String, finishedAt: Long, completedUntil: Long)

/**
 * Signal that matching has successfully completed for the indicated system and corridor. Retry in case of update failure.
 */
private case class MatchingFinishedRetry(systemId: String, corridorId: String, finishedAt: Long, completedUntil: Long)
