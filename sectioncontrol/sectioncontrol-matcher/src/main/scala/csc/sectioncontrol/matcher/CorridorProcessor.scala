/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.matcher

import java.util.Date

import csc.akkautils.DirectLogging
import csc.sectioncontrol.messages._
import csc.sectioncontrol.measure.SpeedCalculator
import csc.sectioncontrol.messages.matcher.MatcherCorridorConfig
import csc.sectioncontrol.sign.SoftwareChecksum
import csc.sectioncontrol.storagelayer.hbasetables.SpeedRecordTable
import csc.sectioncontrol.matcher.MatcherProcessor

/**
 * Data about one license plate in one corridor during the matching period
 * @param matched detected matches
 * @param unmatched unmatched registrations
 */
private[matcher] case class MatchedValue(matched: Seq[VehicleSpeedRecord], unmatched: Seq[VehicleMetadata], doubles: Seq[VehicleMetadata])

class CorridorProcessor(config: MatcherCorridorConfig) extends MatcherProcessor with DirectLogging {
  // 5 km/hour is the minimum speed -> 60 * 60 * 1000 * distance / (5 * 1000)
  val timeLimitMinSpeed = 720 * config.distance
  // 360 km/hour is the maximum speed -> 60 * 60 * 1000 * distance / (360 * 1000)
  val timeLimitMaxSpeed = 10 * config.distance

  val calculator = new SpeedCalculator()
  val checksum = SoftwareChecksum.createHashFromRuntimeClass(calculator).getOrElse(SoftwareChecksum("", ""))

  /**
   * Detects if this registration should be processed by this processor.
   */
  def isWithin(vr: VehicleMetadata, begin: Long, end: Long): Boolean = {

    log.info(vr.license + " has timestamp " + new Date(vr.eventTimestamp))
    val r = vr.lane.gantry == config.entry || vr.lane.gantry == config.exit
    log.info("gantry check = " + r)

    (vr.lane.gantry == config.entry || vr.lane.gantry == config.exit) &&
      (vr.eventTimestamp >= begin && vr.eventTimestamp <= end)
  }

  /**
   * Detects if this registration is within the time limit for the next time slot
   */
  def isLeftOver(vr: VehicleMetadata, max: Long): Boolean = {
    val isEntry = vr.lane.gantry == config.entry
    val isNotLate = vr.eventTimestamp >= max
    isEntry && isNotLate
  }

  def process(data: TraversableOnce[VehicleMetadata], leftOver: Seq[VehicleMetadata], begin: Long, end: Long): MatchProcessResult = {

    val b = new Date(begin)
    val e = new Date(end)
    log.debug("Checking if data is between  " + b + " and " + e)

    val bucket = data.filter(isWithin(_, begin, end))

    val allData = leftOver ++ bucket
    allData.foreach(m ⇒ log.debug("Record found in alldata: %s, %s, %s, %s".format(m.eventId, m.eventTimestampStr, m.rawLicense, m.licensePlateData.map(_.rawLicense))))
    //map each license plate to the sequence of vehicle registrations for the license plate
    val groupedByLicense: Map[Option[String], Seq[VehicleMetadata]] = allData.groupBy(_.license.map(_.value))
    //separate no license plate (to be reported as unmatched)
    val noLicensePlate: Option[Seq[VehicleMetadata]] = groupedByLicense.get(None)

    val licenseData = groupedByLicense - None
    // find all matches for each car (license plate)
    val processed: Iterable[MatchedValue] = licenseData.map(v ⇒ doMatch(v._2))

    // merge all matches
    val allLicensePlates: MatchedValue = processed.foldLeft[MatchedValue](MatchedValue(Nil, Nil, Nil)) {
      case (a: MatchedValue, b: MatchedValue) ⇒
        MatchedValue(a.matched ++ b.matched, a.unmatched ++ b.unmatched, a.doubles ++ b.doubles)
    }

    log.debug(" all license plates matched amount: " + allLicensePlates.matched.size)

    // calculate speed sum, max speed and matches per lane
    val (sumSpeed: Int, maxSpeed: Int, stats) = allLicensePlates.matched.foldLeft((0, 0, Map.empty[Lane, Int])) {
      case (speedsAccumulator, speedRecord) ⇒ {
        val (sumSpeed, previousMax, stats) = speedsAccumulator
        val max = scala.math.max(previousMax, speedRecord.speed)
        val countEntry: Int = stats.getOrElse(speedRecord.entry.lane, 0) + 1 //passages at entry
        val countExit: Int = stats.getOrElse(speedRecord.exit.get.lane, 0) + 1 //passages at exit
        val newStats: Map[Lane, Int] = stats + (speedRecord.entry.lane -> countEntry) + (speedRecord.exit.get.lane -> countExit)
        log.info("Generated stats %s for speedRecord with license %s".format(newStats, speedRecord.entry.rawLicense))
        (sumSpeed + speedRecord.speed, max, newStats)
      }
    }

    log.info("Total stats count:" + stats.size)

    // unmatched and next leftovers are mixed
    val max = end - timeLimitMinSpeed //amount of milliseconds a vehicle mat stay on a corridor and be able to match
    val (nextLeftovers, unmatched) = allLicensePlates.unmatched.partition(isLeftOver(_, max))
    val laneStatsWithUnmatched = unmatched.foldLeft(stats) {
      //add data from unmatched records to statistics
      case (laneStatsMap, vr) ⇒
        val count: Int = laneStatsMap.getOrElse(vr.lane, 0) + 1
        laneStatsMap + (vr.lane -> count)
    }

    log.info("Total stats with unmatched added  count:" + laneStatsWithUnmatched.size)

    val sortedMatches = allLicensePlates.matched.sortWith {
      case (a, b) ⇒ a.exit.get.eventTimestamp < b.exit.get.eventTimestamp
    }
    val sortedLeftOvers = nextLeftovers.sortWith {
      case (a, b) ⇒ a.eventTimestamp < b.eventTimestamp
    }
    //join unmatched records with records without license plate into one sequence
    val finalUnmatched: Seq[VehicleMetadata] = noLicensePlate match {
      case None                 ⇒ unmatched
      case Some(withoutLicense) ⇒ withoutLicense ++ unmatched
    }
    val sortedUnmatched = finalUnmatched.sortWith {
      case (a, b) ⇒ a.eventTimestamp < b.eventTimestamp
    }
    val result = MatchProcessResult(sortedMatches,
      sortedUnmatched,
      sortedLeftOvers,
      allLicensePlates.doubles,
      sumSpeed,
      maxSpeed,
      laneStatsWithUnmatched)

    log.debug("\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    log.debug("Matching results are: ")
    log.debug("leftovers: " + result.leftover.size)
    log.debug("matched: " + result.matched.size)
    log.debug("unmatched: " + result.unmatched.size)

    result

  }

  def createSpeedRecord(first: VehicleMetadata, second: VehicleMetadata, corridorId: Int): VehicleSpeedRecord = {
    val speed = calculator.calculate(first.eventTimestamp, second.eventTimestamp, config.distance)
    VehicleSpeedRecord(SpeedRecordTable.makeKey(first.lane, second.eventTimestamp, corridorId), first, Some(second), corridorId, speed, checksum.checksum)
  }

  /**
   * Match vehicle registrations for one vehicle (one license plate).
   * There may be more then one match (the same car may go many times)
   * req 36: Het berekenen van trajectsnelheden moet onafhankelijk zijn van de rij- of
   * vluchtstrook, waarop het voertuig een passagepunt passeert.
   */
  def doMatch(input: Seq[VehicleMetadata]): MatchedValue = {
    //sort by timestamp
    val result = input.sortWith {
      case (a, b) ⇒ a.eventTimestamp < b.eventTimestamp
    } match {
      case Nil ⇒ MatchedValue(Nil, Nil, Nil)
      case head :: sortedByTimestamp ⇒ {
        //first filter out the double shoots (and leave the ones with the highest confidence)
        val (end, allButLast, doubles) = sortedByTimestamp.foldLeft[(Option[VehicleMetadata], List[VehicleMetadata], List[VehicleMetadata])](Some(head), List(), Nil) {
          case ((None, goodAccu, doobleAccu), next) ⇒ {
            (Some(next), goodAccu, doobleAccu) // previous vr was the double shoot
          }
          case ((Some(last), goodAccu, doobleAccu), next) ⇒ {
            isDoubleShoot(last, next, config.deltaForDoubleShoot) match {
              case false ⇒ (Some(next), last :: goodAccu, doobleAccu) //no double shoot, check next
              case true ⇒
                //decide which one to keep based on the best confidence
                val (good, bad) = (last.category, next.category) match {
                  case (Some(lastCat), Some(nextCat)) ⇒ if (lastCat.confidence > nextCat.confidence) {
                    (last, next)
                  } else {
                    (next, last)
                  }
                  case (Some(lastCat), None) ⇒ (last, next)
                  case (None, Some(nextCat)) ⇒ (next, last)
                  case (None, None) ⇒ if (last.license.get.confidence > next.license.get.confidence) {
                    (last, next)
                  } else {
                    (next, last)
                  }
                }
                (None, good :: goodAccu, bad :: doobleAccu)
            }
          }
        }
        val reversed: List[VehicleMetadata] = end match {
          case None    ⇒ allButLast
          case Some(v) ⇒ v :: allButLast
        }
        /**
         * Algorithm:
         * start from the end. Find the first exit record. Remember it and look for the entry record.
         * When entry is found, report a match and forget the exit record (assign to None). Continue as
         * from the very beginning of the algorithm. Everything which does not match this sequence of
         * events is reported as unmatched.
         */
        val (last, matched, unmatched) = reversed.foldLeft[(Option[VehicleMetadata], List[VehicleSpeedRecord], List[VehicleMetadata])]((None, Nil, Nil)) {
          case ((None, matches, unmatched), vr) if (vr.lane.gantry == config.exit) ⇒ {
            //No pending exit record; Remember this record and try to find the matching entry record
            (Some(vr), matches, unmatched)
          }
          case ((Some(end), matches, unmatched), vr) if (vr.lane.gantry == config.entry && end.eventTimestamp - vr.eventTimestamp > timeLimitMaxSpeed) ⇒ {
            //req 36: only the gantry is taken into account
            // additional condition: don't match excessive (over 360 km/h) speeds
            //Found an entry for a pending exit; report a match and forget exit
            (None, createSpeedRecord(vr, end, config.id) :: matches, unmatched)
          }
          case ((Some(end), matches, unmatched), vr) if (vr.lane.gantry == config.entry) ⇒ {
            //Found an entry too close to the exit time. Remember current exit and put entry in unmatched
            log.warning("Found entry and exit too close to be correct (speed > 360) for license %s".format(end.licensePlateData.map(_.license).getOrElse(end.license)))
            (Some(end), matches, vr :: unmatched)
          }
          case ((Some(end), matches, unmatched), vr) ⇒ {
            log.warning("Found multiple exits for license %s, make (%s, %s) unmatched".format(end.licensePlateData.map(_.license).getOrElse(end.license), end.eventId, end.eventTimestampStr))
            //Found another exit in a row; report previous as unmatched and remember this record
            // to find the matching entry record
            (Some(vr), matches, end :: unmatched)
          }
          case (acc, vr) ⇒ {
            //if it does not match anything else, report as unmatched
            acc.copy(_3 = vr :: acc._3)
          }
        }
        //if last is not empty (not None) then it is unmatched
        MatchedValue(matched, last.toList ++ unmatched, doubles)
      }
    }
    result
  }

  /**
   * check if 2 registrations may be a double shoot from two adjacent lanes
   * @param vr1 - first VehicleMetadata
   * @param vr2 - second VehicleMetadata
   * @param delta - time allowance in milliseconds
   * @return true when it is a double shoot
   */
  private def isDoubleShoot(vr1: VehicleMetadata, vr2: VehicleMetadata, delta: Int): Boolean = {
    (vr1.license, vr2.license) match {
      case (Some(firstLicense), Some(secondLicense)) ⇒
        val sameGantry = vr1.lane.gantry == vr2.lane.gantry
        val sameLicense = firstLicense.value == secondLicense.value
        val near = scala.math.abs(vr1.eventTimestamp - vr2.eventTimestamp) <= delta
        sameGantry && sameLicense && near
      case _ ⇒ false
    }

  }
}

