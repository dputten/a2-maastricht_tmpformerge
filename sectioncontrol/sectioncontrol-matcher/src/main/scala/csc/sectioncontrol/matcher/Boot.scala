/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.matcher

import akka.actor._

import org.apache.hadoop.conf.Configuration

import org.apache.curator.retry.RetryUntilElapsed

import net.liftweb.json.Formats

import csc.akkautils._
import csc.hbase.utils._
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.storage._
import csc.sectioncontrol.sign.certificate.Certifier
import csc.sectioncontrol.measure.SpeedCalculator
import csc.sectioncontrol.messages._

import csc.curator.utils.Curator
import csc.sectioncontrol.sign.SoftwareChecksum
import csc.sectioncontrol.storagelayer.ServiceStatisticsRepositoryAccess
import csc.sectioncontrol.storagelayer.servicestatistics.ServiceStatisticsHBaseRepositoryAccess
import csc.sectioncontrol.storagelayer.hbasetables._
import csc.akkautils.CreateJobActor
import csc.akkautils.RunToken

/**
 * Boot the matcher actors.
 *
 * @author Maarten Hazewinkel
 */
class Boot extends MultiSystemBoot with Certifier {

  import Boot._

  def componentName = "SectionControl-Matcher"

  /**
   * Startup the job scheduler that will kick of the matchers according to their schedules in zookeeper.
   *
   * The path where the scheduler looks for jobs is:
   * "/ctes/systems/ * /corridors/ * /jobs/matcherJob"
   *
   * For the future, to distribute matchers across multiple servers, the job path can be made a parameter or
   * a setting in zookeeper that relates it to the current server name.
   * That will allow fine grained control over where things run.
   */
  def startupSystemActors(system: ActorSystem, systemId: String, curator: Curator, hbaseZkServers: String) {
    RowKeyDistributorFactory.init(curator)
    startEventLogger[SystemEvent](systemId, curator, system)

    system.actorOf(Props(new RunScheduledJobs(jobPath(systemId), curator, startMatcherJob) with WaitForCompletion),
      "matcher_JobScheduler")

    val serviceStatisticsAccess: ServiceStatisticsRepositoryAccess = new ServiceStatisticsHBaseRepositoryAccess {
      protected val hbaseConfig: Configuration = HBaseTableKeeper.createCachedConfig(hbaseZkServers)
      protected val actorSystem: ActorSystem = system
    }
    system.actorOf(Props(new MatcherEventListener(systemId, serviceStatisticsAccess)))

    system.actorOf(Props(new ManageMatcherStatus(curator)), manageMatchStatusName)

    //create and register the JAR certificate
    SoftwareChecksum.createHashFromRuntimeClass(classOf[SpeedCalculator]) match {
      case Some(checksum) ⇒ {
        createAndRegisterCertificate(system, systemId, curator, Some(checksum.fileName), Some(checksum.checksum))
      }
      case None ⇒ {
        //very strange couldn't find checksum
        log.warning("Couldn't find checksum for SpeedCalculator")
      }
    }

    this.hbaseZkServers = hbaseZkServers

    log.debug("matcher_JobScheduler for %s has been created.".format(systemId))
  }

  private var hbaseZkServers: String = ""

  protected def zkServerQuorumConfigPath = "sectioncontrol.zkServers"

  protected def hbaseZkServerQuorumConfigPath = "sectioncontrol.hbaseZkServers"

  protected def moduleName = componentName

  override protected def retryPolicy = new RetryUntilElapsed(1000 * 60 * 60 * 24 * 3, 5000)

  override protected def formats(defaultFormats: Formats) = defaultFormats + enumSerializer

  protected def startMatcherJob(zkJobPath: String, runToken: Option[RunToken], runner: ActorRef, curator: Curator) {
    runner ! CreateJobActor(() ⇒ new MatcherJob(curator, hbaseZkServers, runToken, zkJobPath, getSystemId(zkJobPath), getCorridorId(zkJobPath)),
      StartMatcher)
  }
}

object Boot {

  val enumSerializer = new EnumerationSerializer(
    ZkWheelbaseType,
    VehicleCode,
    ZkIndicationType,
    ServiceType,
    ZkCaseFileType,
    EsaProviderType,
    VehicleImageType,
    SpeedIndicatorType,
    ConfigType)

  def jobPath(systemId: String) = Paths.Corridors.getCorridorJobsPath(systemId, "*") + "/matcherJob"

  val manageMatchStatusName = "matchCompletionManager"
  val manageMatchStatusPath = "/user/" + manageMatchStatusName

  private def getSystemId(jobPath: String) = {
    val index = Paths.Systems.getDefaultPath.split('/').length
    jobPath.split('/')(index)
  }

  private def getCorridorId(jobPath: String) = {
    val systemId = getSystemId(jobPath)
    val index = Paths.Corridors.getDefaultPath(systemId).split('/').length
    jobPath.split('/')(index)
  }
}
