package csc.sectioncontrol.matcher

import java.text.SimpleDateFormat
import java.util.{ Date, Calendar, TimeZone, GregorianCalendar }

import akka.actor.SupervisorStrategy.Stop
import akka.actor._
import akka.util.duration._
import csc.akkautils.{ JobFailed, JobComplete, RunToken }
import csc.config.Path
import csc.curator.utils.Curator
import csc.hbase.utils.HBaseTableKeeper
import csc.sectioncontrol.messages.StatsDuration
import csc.sectioncontrol.messages.matcher.{ GetLastTimesMessage, SetNextTimesMessage, MatchingMessage }
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storagelayer.corridor.CorridorService
import csc.sectioncontrol.storagelayer.hbasetables._

class MatcherJobRunner(runner: ActorRef, matcher: ActorRef, curator: Curator, hbaseZkServers: String, runToken: RunToken, zkJobPath: String, systemId: String, corridorId: String)
  extends Actor with ActorLogging with HBaseTableKeeper {
  var lastKnownPeriod: Option[Period] = None

  lazy val hbaseConfig = createConfig(hbaseZkServers)
  lazy val processingReader = ProcessingTable.makeReader(hbaseConfig, this)
  private lazy val matchCompletion = context.actorFor(Boot.manageMatchStatusPath)

  override def postStop() {
    try {
      closeTables()
    } finally {
      super.postStop()
    }
  }

  protected def receive = {
    case "start" ⇒
      matcher ! GetLastTimesMessage

    case Some(mm: MatchingMessage) ⇒
      log.debug("Continue matching from {}", new Date(mm.end))
      self ! StartFrom(mm.end)

    case None ⇒
      val next = runToken.scheduledTime - 1.day.toMillis
      log.info("No previous scheduled time found for matcher. Use {}", new Date(next))
      self ! StartFrom(next)

    case StartFrom(beginTime) ⇒
      context.watch(matcher)
      val config = loadMatcherJobConfig()

      startFrom(beginTime, runToken, config, matcher, runner, matchCompletion)

      log.debug("|systemId|corridorId| => |" + systemId + "|" + corridorId + "|")
      log.debug("|beginTime| => |" + dateHelper.toDateText(beginTime))
      log.debug("|runToken.startTime| => |" + dateHelper.toDateText(runToken.startTime))

    case mr @ SectionControlStatistics(StatisticsPeriod(_, endTime, _), _, _, _, _, _, _, _, _) ⇒
      log.info("completed matching for {} until {}. Result = {}", "MatcherJobRunner", endTime, mr)
      val config = loadMatcherJobConfig()
      matchCompletion ! MatchingFinished(systemId, corridorId, System.currentTimeMillis(), endTime)
      log.debug("matchCompletion:" + matchCompletion)
      log.debug("runner:" + runner)
      log.debug("JobComplete(Some(endTime + config.endTimeSafetyInterval)):" + JobComplete(Some(endTime + config.endTimeSafetyInterval)))
      runner ! JobComplete(Some(endTime + config.endTimeSafetyInterval))

    case Terminated(`matcher`) ⇒
      log.info("matcher actor MatcherJobRunner died")
      runner ! JobFailed
  }

  /**
   * @return a tuple of boolean indicating whether we can match or not and the period for which we can match
   */
  def calculatePeriod(systemId: String, corridorId: String, beginTime: Long, runToken: RunToken, config: MatcherJobConfig): (Boolean, Period) = {

    class IllegalPeriodException(msg: String) extends Exception

    val schedules = getSchedulesFromZookeeper
    log.debug("schedules:" + schedules)
    val schedulePeriods = toSchedulePeriods(schedules, beginTime, runToken.timezone)
    log.debug("schedulePeriods:" + schedulePeriods)

    val endOfMatchPeriod: Long = calculateEndOfMatchPeriod(beginTime, runToken.startTime, runToken.timezone, config)
    log.debug("endOfMatchPeriod:" + endOfMatchPeriod)

    val matchPeriod = lastKnownPeriod match {
      case None ⇒
        log.debug("None: Period(beginTime, endOfMatchPeriod)" + Period(beginTime, endOfMatchPeriod))

        Period(beginTime, endOfMatchPeriod)
      case Some(formerPeriod) ⇒

        if (endOfMatchPeriod < formerPeriod.to) {
          throw new IllegalPeriodException("timestamp mismatch:  %s was after %s ".format(endOfMatchPeriod, formerPeriod.to))
        }

        log.debug("lastKnownPeriod:" + lastKnownPeriod)

        val definitiveFrom = math.max(formerPeriod.to, beginTime)
        val definitiveTo = math.max(formerPeriod.to, endOfMatchPeriod)

        log.debug("definitiveFrom:" + lastKnownPeriod)
        log.debug("definitiveTo:" + lastKnownPeriod)

        Period(definitiveFrom, definitiveTo)
    }

    lastKnownPeriod = Some(matchPeriod)

    val matchPeriods: List[Period] = schedulePeriods.flatMap(calculatePeriodForMatching(matchPeriod, _))
    val top = matchPeriods.sortWith { (first, second) ⇒ first.from < second.from }.headOption
    log.info("Period found for {}-{}: {}", systemId, corridorId, top)

    top match {
      case Some(x) ⇒ lastKnownPeriod = Some(x); (true, x)
      case _       ⇒ (false, matchPeriod)
    }
  }

  /**
   * Rule: Match period end can never be later than end of current day
   */
  def calculateEndOfMatchPeriod(beginTime: Long, startTimeCurrentRun: Long, timezone: String, config: MatcherJobConfig): Long = {
    val endOfDay = dateHelper.resetHoursAndMinutesOfDate(beginTime, 24, 0, timezone)
    val matchPeriodEnd = math.min(endOfDay, startTimeCurrentRun - config.endTimeSafetyInterval)

    calculateGantryTime(config) match {
      case Some(gantryProcessingTimeEnd) ⇒ math.min(matchPeriodEnd, gantryProcessingTimeEnd)
      case None                          ⇒ matchPeriodEnd
    }
  }

  def toSchedulePeriods(schedules: List[ZkSchedule], beginTime: Long, timezone: String) = {
    schedules.map {
      schedule ⇒
        val from = dateHelper.resetHoursAndMinutesOfDate(beginTime, schedule.fromPeriodHour, schedule.fromPeriodMinute, timezone)
        val to = dateHelper.resetHoursAndMinutesOfDate(beginTime, schedule.toPeriodHour, schedule.toPeriodMinute, timezone)

        Period(from, to)
    }
  }

  def getSchedulesFromZookeeper: List[ZkSchedule] = {
    val path = Paths.Corridors.getSchedulesPath(systemId, corridorId)
    curator.get[List[ZkSchedule]](path).getOrElse(Nil)
  }

  /**
   * Calculcate for which period we can match. This depends not only on the matching period but also
   * on whether there is a valid schedule (periodeschema) for that period
   * @return a period for which we can start matching or none if there is none
   */
  def calculatePeriodForMatching(matchPeriod: Period, schedulePeriod: Period): Option[Period] = {
    if (matchPeriod.from >= schedulePeriod.from && matchPeriod.to <= schedulePeriod.to)
      Some(Period(matchPeriod.from, matchPeriod.to)) // |..<----->..|
    else if (matchPeriod.from >= schedulePeriod.from && matchPeriod.from < schedulePeriod.to)
      Some(Period(matchPeriod.from, schedulePeriod.to)) // |<---|-->
    else if (matchPeriod.from <= schedulePeriod.from && matchPeriod.to >= schedulePeriod.to)
      Some(Period(schedulePeriod.from, schedulePeriod.to)) // <--|.....|-->
    else if (matchPeriod.from < schedulePeriod.from && matchPeriod.to >= schedulePeriod.from)
      Some(Period(schedulePeriod.from, matchPeriod.to)) // <--|-->..|
    else
      None
  }

  /**
   * It fetches the gantry times from processing table and selects the earliest processing time for the current
   * corridor. First, it has to check if `useKeepalive` is enabled in [[MatcherJobConfig]]. Then it should extract
   * the entry and exit gantries for the given corridor from the Curator. Finally it should retrieve the processing
   * times for the given gantries and find the minimum.
   *
   * @return the minimum of the gantry processing times or None
   */
  def calculateGantryTime(config: MatcherJobConfig): Option[Long] = {
    if (config.useKeepalive) {
      log.debug("Gantry processing time calculation is enabled for corridor {}-{}", systemId, corridorId)
      val keepalives = CorridorService.getGantriesForCorridor(curator, systemId, corridorId).flatMap { gantryId ⇒
        processingReader.getGantryTimes(systemId, gantryId)
      }
      if (keepalives.isEmpty) log.debug("Gantry processing time records not found for corridor {}-{}", systemId, corridorId)
      else {
        log.info("Keepalives for corridor {}-{}:", systemId, corridorId)
        keepalives.foreach { m ⇒ log.info(m.toString) }
      }
      keepalives.map(_.time).reduceLeftOption(_ min _)
    } else None
  }

  def startFrom(beginTime: Long, runToken: RunToken, config: MatcherJobConfig, matcher: ActorRef,
                runner: ActorRef, matchCompletion: ActorRef) {
    val (canMatch, period) = calculatePeriod(systemId, corridorId, beginTime, runToken, config)
    log.info("Evaluating matching period " + period)

    val matchingMessage = MatchingMessage(
      StatsDuration(beginTime, TimeZone.getTimeZone(runToken.timezone)),
      period.to, Some(period.from))

    log.debug("beginTime:" + beginTime)
    log.debug("period.to:" + period.to)
    log.debug("period.from:" + period.from)

    if (period.from != period.to) {
      log.info("period.from != period.to")
      if (canMatch && period.from != period.to) {
        log.info("Let's go matching!!")
        log.info("Starting matching for MatcherJobRunner with message {}", matchingMessage)
        matcher ! matchingMessage
        matchCompletion ! MatchingStarted(systemId, corridorId, runToken.startTime)
      } else {
        log.info("No matching right now!!")
        matcher ! SetNextTimesMessage(matchingMessage)
        matchCompletion ! MatchingFinished(systemId, corridorId, System.currentTimeMillis(), period.to)
        runner ! JobComplete(Some(period.to))
      }
    } else {
      log.info("period.from==period.to are equal. Skip the matching")
      runner ! JobComplete(Some(period.to))
    }

    log.debug("|period.from| => |" + dateHelper.toDateText(period.from))
    log.debug("|period.to| => |" + dateHelper.toDateText(period.to))
  }

  /**
   * Load the matcher config from zookeeper, or fall back to a default config.
   */
  def loadMatcherJobConfig(): MatcherJobConfig = {
    curator.get[MatcherJobConfig](Seq(Path(zkJobPath + "/config")), MatcherJobConfig.default) match {
      case Some(mc) ⇒ mc
      case None     ⇒ MatcherJobConfig.default
    }
  }
}

case object StartMatcher

/**
 * Configuration of a matcher job
 */
case class MatcherJobConfig(endTimeSafetyInterval: Long, useKeepalive: Boolean = false)

object MatcherJobConfig {
  /**
   * Default matcher configuration if no specific config is stored in zookeeper.
   */
  val default = MatcherJobConfig(10.minutes.toMillis, false)
}

case class Period(from: Long, to: Long) {
  import java.util.Date
  require(from <= to, "Period (%s, %s) cannot be negative ('from' must be <= 'to')".format(from, to))

  override def toString = new Date(from).toString + " - " + new Date(to).toString
}

case class StartFrom(beginTime: Long)

object dateHelper {
  def resetHoursAndMinutesOfDate(date: Long, hour: Int, minutes: Int, timezone: String): Long = {
    val cal = new GregorianCalendar(TimeZone.getTimeZone(timezone))
    cal.setTimeInMillis(date)
    cal.set(Calendar.MINUTE, minutes)
    cal.set(Calendar.SECOND, 0)
    cal.set(Calendar.MILLISECOND, 0)
    cal.set(Calendar.HOUR_OF_DAY, hour)
    cal.getTimeInMillis
  }

  def toDateText(time: Long): String = {
    val format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
    format.format(new Date(time))
  }
}
