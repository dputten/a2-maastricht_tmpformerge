/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.matcher

import java.util.Date

import akka.util.duration._
import akka.actor.{ Actor, ActorLogging }

import csc.curator.utils.Curator
import csc.hbase.utils._

import csc.sectioncontrol.messages.{ VehicleUnmatchedRecord, VehicleSpeedRecord, VehicleMetadata }
import csc.sectioncontrol.messages.matcher._
import csc.sectioncontrol.storage.{ StatisticsPeriod, SectionControlStatistics, LaneInfo, Paths }
import csc.sectioncontrol.storagelayer.hbasetables.{ VehicleTable, MatcherLeftOverTable, UnmatchedVehicleTable, SpeedRecordTable }
import csc.sectioncontrol.matcher.MatchProcessResult

class Matcher(val systemId: String, curator: Curator, config: MatcherCorridorConfig, vrReader: VehicleTable.Reader,
              speedRecordWriter: SpeedRecordTable.Writer,
              unmatchedWriter: UnmatchedVehicleTable.Writer,
              leftOver: MatcherLeftOverTable.Store) extends Actor with ActorLogging {

  def receive = {
    case message @ MatchingMessage(period, end, beginOption) ⇒ {
      val begin: Long = beginOption match {
        case Some(b) ⇒ b
        case None ⇒ getPreviousMessage match {
          case Some(previous) ⇒ previous.end // last finish becomes new start
          case None ⇒ {
            //TODO should we ignore the leftovers if no previous time found?
            val newBegin = end - 1.day.toMillis // a day ago
            val path = Matcher.buildCorridorPath(systemId, config.zkCorridorId)
            log.error("Could not find last end time for Matcher in Zookeeper under '%s'. Use a day back from end time (%s)".format(path, newBegin))
            newBegin
          }
        }
      }
      val processor = new CorridorProcessor(config)
      val entryPrefix = config.systemId + "-" + config.entry // + "-"
      val exitPrefix = config.systemId + "-" + config.exit // + "-"

      log.debug("using entryprefix:" + entryPrefix)
      log.debug("using begin:" + new Date(begin))
      log.debug("using end:" + new Date(end))
      log.debug("using exitprefix:" + exitPrefix)

      val incomingData = vrReader.readRows((entryPrefix, begin), (entryPrefix, end)) ++ vrReader.readRows((exitPrefix, begin), (exitPrefix, end))

      log.debug("We have " + incomingData.size + " records!")

      val leftOvers = leftOver.readAllRows.filter(_.lane.system == config.systemId)

      log.debug("We have " + leftOvers.size + " all license plates matched amoun")

      val MatchProcessResult(matched, unmatched, nextLeftover, doubles, sum, max, stats) = processor.process(incomingData, leftOvers, begin, end)
      log.debug("Ignored %s double shoots for %s".format(doubles.size, message))

      log.debug("result of matched - unmatched : %s -  %s ".format(matched, unmatched))

      speedRecordWriter.writeRows(matched, (record: VehicleSpeedRecord) ⇒ (record, Some(record.time)))

      log.info("Written %s rows to speedRecord writer".format(matched.size))

      val unmatchedToWrite = unmatched.map(vr ⇒ VehicleUnmatchedRecord(vr.eventId, vr, config.id, period, end))
      unmatchedWriter.writeRows(unmatchedToWrite, (record: VehicleUnmatchedRecord) ⇒ {
        (record, Some(record.entry.eventTimestamp))
      })
      leftOver.deleteRows(leftOvers)
      leftOver.writeRows(nextLeftover)
      val averageSpeed = if (matched.size == 0) 0 else sum.toDouble / matched.size.toDouble
      val laneInfo: List[LaneInfo] = stats.map {
        case (key, value) ⇒ LaneInfo(key, value)
      }.toList.sortWith {
        case (a, b) ⇒ a.lane.name < b.lane.name
      }
      val result = SectionControlStatistics(StatisticsPeriod(stats = message.stats, end = message.end, begin = begin), config,
        incomingData.size, matched.size, unmatched.size, doubles.size, averageSpeed, max, laneInfo)
      storeMessage(message.copy(begin = Some(begin)))
      //send out reports
      context.system.eventStream.publish(result)
      log.debug("sender:" + sender)
      sender ! result
    }
    case CleanLeftoverMessage ⇒ {
      val leftOvers = leftOver.readAllRows
      leftOver.deleteRows(leftOvers)
      sender ! leftOvers.size
    }
    case GetLeftoverMessage ⇒ {
      val leftOvers = leftOver.readAllRows
      sender ! leftOvers
    }
    case GetLastTimesMessage ⇒ {
      val times = getPreviousMessage
      sender ! times
    }
    case SetLastTimesMessage(message) ⇒ {
      val previous = getPreviousMessage
      storeMessage(message)
      sender ! previous
    }
    case SetNextTimesMessage(message) ⇒ {
      storeMessage(message)
    }
  }

  private def storeMessage(message: MatchingMessage) {
    //put end time to Zookeeper to be used as begin time for the next run
    val valuePath = Matcher.buildCorridorPath(systemId, config.zkCorridorId) + "/" + Matcher.lastTimePrefix
    if (!curator.exists(valuePath)) {
      curator.put(valuePath, message)
    } else {
      //try 5 times
      //TODO 20120519 RR->AS: retrying 5 times, can also be done in supervision. Would that fit?
      val done = (1 to 5).exists(i ⇒
        try {
          val previous = curator.getVersioned[MatchingMessage](valuePath).get
          curator.set(valuePath, message, previous.version)
          log.debug("Matcher stored new state: %s".format(message))
          true
        } catch {
          case e: Exception ⇒
            log.error("Matcher could not store new state (%s time)".format(i), e)
            false /* try again */
        })
      if (!done)
        log.error("Matcher failed to store the new state. Start (begin) value must be provided for the next run.")
    }
  }

  private def getPreviousMessage: Option[MatchingMessage] = {
    val path = Matcher.buildCorridorPath(systemId, config.zkCorridorId) + "/" + Matcher.lastTimePrefix
    curator.get[MatchingMessage](path)
  }
}

object Matcher {
  val componentName: String = "processMatches"

  private val lastTimePrefix = "lastEndTime"

  private def buildCorridorPath(systemId: String, zkCorridorId: String): String = {
    Paths.Corridors.getCorridorJobsPath(systemId, zkCorridorId) + "/" + componentName
  }
}

