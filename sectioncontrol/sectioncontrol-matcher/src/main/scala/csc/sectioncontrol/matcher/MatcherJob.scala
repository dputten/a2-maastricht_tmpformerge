package csc.sectioncontrol.matcher

import akka.actor._
import akka.util.duration._
import csc.akkautils.{ JobFailed, RunToken }
import csc.config.Path
import csc.curator.utils.Curator
import csc.hbase.utils.{ HBaseStore, HBaseTableKeeper }
import csc.sectioncontrol.messages.VehicleMetadata
import csc.sectioncontrol.messages.matcher.MatcherCorridorConfig
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storagelayer.hbasetables._
import akka.actor.SupervisorStrategy.Stop
import csc.sectioncontrol.matcher.{ MatcherJobRunner, StartMatcher }
import csc.sectioncontrol.storage.ZkSection
import csc.akkautils.JobFailed
import scala.Some
import csc.sectioncontrol.storage.ZkCorridor
import akka.actor.AllForOneStrategy
import csc.akkautils.RunToken
import csc.sectioncontrol.messages.matcher.MatcherCorridorConfig

/**
 * Specific details for starting up the matcher jobs.
 * Matcher jobs always wait for completion before starting again by extending WaitForCompletionJob.
 */
class MatcherJob(curator: Curator, hbaseZkServers: String, runToken: Option[RunToken], zkJobPath: String, systemId: String, corridorId: String)
  extends Actor with ActorLogging with HBaseTableKeeper {

  lazy val hbaseConfig = createConfig(hbaseZkServers)

  override def supervisorStrategy() = AllForOneStrategy() {
    case e: Exception ⇒
      log.error(e, "Matcher instance failed. Scheduling rerun for: {}", zkJobPath)
      sender ! JobFailed(retry = true)
      Stop
  }

  protected def receive = {
    case StartMatcher ⇒
      createMatcherForCorridor() match {
        case Some(matcherActor) ⇒
          val _sender = sender
          val matcherJobRunner = context.actorOf(Props(new MatcherJobRunner(_sender, matcherActor, curator, hbaseZkServers, runToken.get, zkJobPath, systemId, corridorId)))
          matcherJobRunner ! "start"
        case None ⇒
          log.error("could not create matcher for job {}", zkJobPath)
          sender ! JobFailed
      }
  }

  override def postStop() {
    try {
      closeTables()
    } finally {
      super.postStop()
    }
  }

  /**
   * calculates the total length of a corridor
   */
  private def getCorridorLength(systemId: String, allSectionIds: Seq[String]): Double = {
    allSectionIds.foldLeft(0d) {
      (corridorLength, sectionId) ⇒
        val section = curator.get[ZkSection](Paths.Sections.getConfigPath(systemId, sectionId))
        section.map(corridorLength + _.length).getOrElse(corridorLength)
    }
  }

  /**
   * Load configuration data from zookeeper and instantiate the matcher actor.
   */
  private def createMatcherForCorridor(): Option[ActorRef] = {

    for {
      preferences ← {
        log.info("GlobalConfigPath: %s".format(Paths.Preferences.getGlobalConfigPath))
        log.info("Preferences config path: %s".format(Path(Paths.Preferences.getConfigPath(systemId))))
        val prefs = curator.get[ZkPreference](Seq(Path(Paths.Preferences.getGlobalConfigPath), Path(Paths.Preferences.getConfigPath(systemId))), ZkPreference.default)
        log.info("Prefs " + prefs)
        prefs
      }
      corridor ← {
        val corr = curator.get[ZkCorridor](Paths.Corridors.getConfigPath(systemId, corridorId))
        log.info("corridor " + corr)
        corr
      }
      startSection ← {
        val startSect = curator.get[ZkSection](Paths.Sections.getConfigPath(systemId, corridor.startSectionId))
        log.info("start section " + startSect)
        startSect
      }
      endSection ← {
        val endSect = curator.get[ZkSection](Paths.Sections.getConfigPath(systemId, corridor.endSectionId))
        log.info("end Sect " + endSect)
        endSect
      }
    } yield context.actorOf(Props(createMatcher(MatcherCorridorConfig(systemId,
      corridorId,
      corridor.info.corridorId,
      startSection.startGantryId,
      endSection.endGantryId,
      getCorridorLength(systemId, corridor.allSectionIds).toInt,
      preferences.duplicateTriggerDelta,
      preferences.followDistance.getOrElse(1.hour.toMillis)))),
      "matcher_" + systemId + "_" + corridorId)
  }

  /**
   * Connect to all the hbase tables needed by the matcher and create the matcher instance.
   */
  private def createMatcher(corridorConfig: MatcherCorridorConfig): Matcher = {
    val vrReader = VehicleTable.makeReader(hbaseConfig, this)
    val speedRecordWriter = SpeedRecordTable.makeWriter(hbaseConfig, this)
    val unmatchedWriter = UnmatchedVehicleTable.makeWriter(hbaseConfig, this)
    val leftOverStore: HBaseStore[VehicleMetadata] = MatcherLeftOverTable.makeStore(hbaseConfig, this, corridorId)

    new Matcher(systemId, curator, corridorConfig, vrReader, speedRecordWriter, unmatchedWriter, leftOverStore)
  }
}
