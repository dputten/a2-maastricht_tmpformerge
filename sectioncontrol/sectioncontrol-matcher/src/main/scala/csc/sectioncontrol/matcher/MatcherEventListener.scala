/*
* Copyright (C) 2012 CSC. <http://www.csc.com>
*/
package csc.sectioncontrol.matcher

import csc.sectioncontrol.storage.SectionControlStatistics
import akka.actor.{ Actor, ActorLogging }
import csc.sectioncontrol.storagelayer.ServiceStatisticsRepositoryAccess

/**
 * Actor which listens to the event stream and puts the statistic events from Matcher to Zookeeper.
 * @param systemId system ID (for instance A2)
 */
class MatcherEventListener(val systemId: String, serviceStatisticsAccess: ServiceStatisticsRepositoryAccess) extends Actor with ActorLogging {

  def receive = {
    case stat: SectionControlStatistics ⇒ saveMatchingStatistics(stat)
  }

  override def preStart() {
    super.preStart()
    context.system.eventStream.subscribe(self, classOf[SectionControlStatistics])
  }

  private def saveMatchingStatistics(event: SectionControlStatistics) {
    serviceStatisticsAccess.save(systemId, event)
  }
}

