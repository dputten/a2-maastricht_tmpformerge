/*
* Copyright (C) 2012 CSC. <http://www.csc.com>
*/
package csc.sectioncontrol.destroy

import java.io._

import akka.actor.ActorSystem
import akka.testkit.TestActorRef
import akka.util.Timeout
import akka.util.duration._
import csc.akkautils.DirectLogging
import csc.config.Path
import csc.curator.utils.{Curator, CuratorToolsImpl}
import csc.hbase.utils.testframework.HBaseTestFramework
import org.apache.curator.framework.CuratorFrameworkFactory
import org.apache.curator.retry.RetryNTimes
import org.apache.hadoop.hbase.client.{HBaseAdmin, HTable, Put}
import org.apache.hadoop.hbase.util.Bytes
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

class DestroyDataTest extends WordSpec with MustMatchers with HBaseTestFramework
  with DirectLogging {
  implicit val testSystem = ActorSystem("DestroyTest")
  implicit def string2ByteArray(s: String): Array[Byte] = Bytes.toBytes(s)
  private var zkStore: Curator = _
  var testActor: TestActorRef[DestroyData] = _
  val rootPath = System.getProperty("user.dir")
  val currentDir = rootPath + "/sectioncontrol-destroy/src/test/resources/"
  val scriptFile = new File(currentDir, "make-folders.sh")

  implicit val timeout = Timeout(5 seconds)

  //override protected def startZkCluster = false

  override protected def beforeAll() {
    super.beforeAll()
    val clientScope = Some(CuratorFrameworkFactory.newClient(zkConnectionString, new RetryNTimes(1, 1000)))
    clientScope.foreach(_.start())
    zkStore = new CuratorToolsImpl(clientScope, log)
  }

  private def createPaths() {

    if (zkStore exists "/ctes") zkStore.deleteRecursive(Path("/ctes"))
    zkStore.createEmptyPath(Path("/ctes") / "users")
    zkStore.createEmptyPath(Path("/ctes") / "configuration")
    zkStore.createEmptyPath(Path("/ctes") / "main")
    zkStore.createEmptyPath(Path("/ctes") / "main" / "sub1")
    zkStore.createEmptyPath(Path("/ctes") / "main" / "sub2")
  }

  "Destroy" must {
    "must start external script" in {
      testActor = TestActorRef(new DestroyData(
        zkStore,
        hbaseConfig,
        zkDestroyPaths = "",
        scriptFile = scriptFile,
        simulateDestroy = true,
        startScript = true,
        hbaseTableNames = ""))

      testActor ! DestroyMessage("bla-bla")
      Thread.sleep(1000)
      val folder = new File(rootPath + "/making-folders")

      folder.exists must be(true)
      folder.isDirectory must be(true)
      folder.delete()
    }

    "must not start external script" in {
      testActor = TestActorRef(new DestroyData(
        zkStore,
        hbaseConfig,
        zkDestroyPaths = "",
        scriptFile = scriptFile,
        simulateDestroy = true,
        startScript = false,
        hbaseTableNames = ""))

      testActor ! DestroyMessage("bla-bla")
      Thread.sleep(1000)
      val folder = new File(rootPath + "/making-folders")
      folder.exists must be(false)
    }
    "must not delete zookeeper paths" in {
      testActor = TestActorRef(new DestroyData(
        zkStore,
        hbaseConfig,
        zkDestroyPaths = "/ctes/users,/ctes/configuration,/ctes/main/sub1",
        scriptFile = scriptFile,
        simulateDestroy = true,
        startScript = false,
        hbaseTableNames = ""))

      createPaths()
      testActor ! DestroyMessage("bla-bla")
      Thread.sleep(1000)
      zkStore.exists(Path("/ctes") / "users") must be(true)
      zkStore.exists(Path("/ctes") / "configuration") must be(true)
      zkStore.exists(Path("/ctes") / "main" / "sub1") must be(true)
      zkStore.exists(Path("/ctes") / "main" / "sub2") must be(true)
    }

    "must delete hbase tables" in {
      val table1 = testUtil.createTable("table1", "cf")
      val table2 = testUtil.createTable("table2", "cf")
      val table3 = testUtil.createTable("table3", "cf")

      val now = System.currentTimeMillis()
      put(table1, "key1", "val1", now)
      table1.flushCommits()
      put(table2, "key2", "val2", now)
      table2.flushCommits()
      put(table3, "key3", "val3", now)
      table3.flushCommits()

      val admin = new HBaseAdmin(hbaseConfig)

      testActor = TestActorRef(new DestroyData(
        zkStore,
        hbaseConfig,
        zkDestroyPaths = "",
        scriptFile = scriptFile,
        simulateDestroy = false,
        startScript = false,
        hbaseTableNames = "table1,table3"))
      Thread.sleep(1000)
      testActor ! DestroyMessage("bla-bla")
      Thread.sleep(3000)

      admin.tableExists("table1") must be(false)
      admin.tableExists("table2") must be(true)
      admin.tableExists("table3") must be(false)
      admin.close()
    }
    "must delete zookeeper paths" in {
      testActor = TestActorRef(new DestroyData(
        zkStore,
        hbaseConfig,
        zkDestroyPaths = "/ctes/users,/ctes/configuration,/ctes/main/sub1",
        scriptFile = scriptFile,
        simulateDestroy = false,
        startScript = false,
        hbaseTableNames = ""))

      createPaths()
      testActor ! DestroyMessage("bla-bla")
      Thread.sleep(1000)
      zkStore.exists(Path("/ctes") / "users") must be(false)
      zkStore.exists(Path("/ctes") / "configuration") must be(false)
      zkStore.exists(Path("/ctes") / "main" / "sub1") must be(false)
      zkStore.exists(Path("/ctes") / "main" / "sub2") must be(true)
    }
  }

  def put(table: HTable, key: String, value: String, time: Long) {
    val put1 = new Put(Bytes.toBytes(key))
    put1.add(Bytes.toBytes("cf"), Bytes.toBytes("qual1"), time, Bytes.toBytes(value))
    table.put(put1)
  }
  override protected def afterAll() {
    zkStore.curator.foreach(_.close())
    testSystem.shutdown()
    super.afterAll()
  }

  def printToFile(f: java.io.File)(op: java.io.PrintWriter ⇒ Unit) {
    val p = new java.io.PrintWriter(f)
    try {
      op(p)
    } finally {
      p.close()
    }
  }

  def createTextFile(folder: File, name: String): File = {
    val file = new File(folder, name)
    printToFile(file)(p ⇒ {
      Array(name, name).foreach(p.println(_))
    })
    file
  }
}

