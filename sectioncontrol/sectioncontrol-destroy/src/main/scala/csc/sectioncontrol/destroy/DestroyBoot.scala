/*
* Copyright (C) 2012 CSC. <http://www.csc.com>
*/
package csc.sectioncontrol.destroy

import java.io.File
import java.util.concurrent.atomic.AtomicReference

import akka.actor.{ ActorSystem, Actor, Props }
import org.apache.curator.framework.{ CuratorFramework, CuratorFrameworkFactory }
import org.apache.curator.retry.RetryUntilElapsed
import csc.akkautils.{ MultiSystemBoot, GenericBoot }
import csc.curator.utils.{ Curator, CuratorToolsImpl, PathAdded, WatchNode }
import net.liftweb.json.{ DefaultFormats, Formats }
import akka.event.Logging
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.HBaseConfiguration
import csc.sectioncontrol.storagelayer.hbasetables.RowKeyDistributorFactory
import csc.hbase.utils.HBaseTableKeeper

object Boolean {
  def apply(s: String): Option[scala.Boolean] = try {
    Some(s.toBoolean)
  } catch {
    case _: java.lang.NumberFormatException ⇒ None
  }
}

/**
 * Boots Destroy
 */
class DestroyBoot extends GenericBoot {
  def componentName = "SectionControl-Destroy"
  private val curatorRef = new AtomicReference[Option[CuratorFramework]](None)

  val zkServerConfigPathName = "sectioncontrol.zkServers"
  val hbaseZkServerConfigPathName = "sectioncontrol.hbaseZkServers"
  val zkDestroyPathsName = "sectioncontrol.destroy.zookeeper-folders"
  val scriptConfigPathName = "sectioncontrol.destroy.script"
  val simulateDestroyPathName = "sectioncontrol.destroy.simulate"
  val startScriptPathName = "sectioncontrol.destroy.start-script"
  val hbaseTablesPathName = "sectioncontrol.destroy.hbase-tables"

  def startupActors() {
    val config = actorSystem.settings.config
    val missing: Option[String] = List(zkServerConfigPathName, zkDestroyPathsName, scriptConfigPathName,
      simulateDestroyPathName, startScriptPathName, hbaseTablesPathName).find(
        name ⇒ !config.hasPath(name))
    missing match {
      case None ⇒ {
        //all were found
        val zkServers = config.getString(zkServerConfigPathName)
        val hbaseZkServers = if (config.hasPath(hbaseZkServerConfigPathName)) {
          config.getString(hbaseZkServerConfigPathName)
        } else {
          zkServers
        }
        val zkDestroyPaths = config.getString(zkDestroyPathsName)
        val scriptPath = config.getString(scriptConfigPathName)
        val startScript = Boolean(config.getString(startScriptPathName)).getOrElse(false)
        val simulateDestroy = Boolean(config.getString(simulateDestroyPathName)).getOrElse(false)
        val hbaseTableNames = config.getString(hbaseTablesPathName)
        val scriptFile = if (config.hasPath("akka.home")) {
          val akkaHome = config.getString("akka.home")
          new File(akkaHome + "/config/" + scriptPath)
        } else {
          new File(scriptPath)
        }
        val retryPolicy = new RetryUntilElapsed(1000 * 60 * 60 * 24 * 3, 5000)
        curatorRef.set(Some(CuratorFrameworkFactory.newClient(zkServers, retryPolicy)))
        val curator = new CuratorToolsImpl(curatorRef.get(), log)
        RowKeyDistributorFactory.init(curator)

        val hbaseConfig = HBaseTableKeeper.createCachedConfig(zkServers)

        actorSystem.actorOf(Props(new DestroyData(curator, hbaseConfig, zkDestroyPaths, scriptFile,
          simulateDestroy, startScript, hbaseTableNames)), "destroy-all")
        log.debug("Destroy actor has been started.")
        try {
          startWatch(zkServers)
          log.debug("Watcher has been started.")
        } catch {
          case t: Throwable ⇒ log.error("failed to start watch of the destroy trigger %s".format(t))
        }
        if (scriptFile.exists) {
          log.info("Using destroy script {}.", scriptFile.getAbsolutePath)
        } else {
          log.warning("No destroy script found {}.", scriptFile.getAbsolutePath)
        }
      }
      case Some(error) ⇒ {
        log.error("required key {} not defined in config {}", error, config)
      }
    }
  }

  def startWatch(zkServerQuorum: String) {
    curatorRef.get().foreach {
      c ⇒
        c.start()
        val watchNode = new WatchNode(c, "/destroy", actorSystem.actorOf(Props(new Actor() {

          def zkServers = zkServerQuorum
          def receive = {
            case PathAdded(node, parent) ⇒ {
              log.warning("New Trigger found {}", node)
              val destroyer = actorSystem.actorFor(String.format("akka://%s/user/%s", componentName, "destroy-all"))
              destroyer ! DestroyMessage("Destroy data")
            }
          }
        }), "destroy-watcher"))
        watchNode.start()
    }
  }

  def shutdownActors() {
  }

  def createCurator(system: ActorSystem, zkServers: String, formats: Formats = DefaultFormats): Curator = {
    val retryPolicy = new RetryUntilElapsed(1000 * 60 * 60 * 24 * 3, 5000)
    val curator = CuratorFrameworkFactory.newClient(zkServers, retryPolicy)
    new CuratorToolsImpl(Some(curator), log, finalFormats = formats) {
      override protected def extendFormats(defaultFormats: Formats) = formats
    }
  }

}
