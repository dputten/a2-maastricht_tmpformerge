/*
* Copyright (C) 2012 CSC. <http://www.csc.com>
*/
package csc.sectioncontrol.destroy

import akka.actor.ActorLogging
import csc.config.Path
import csc.curator.utils.{ Curator, CuratorActor }
import java.io._
import org.apache.hadoop.hbase.client.HBaseAdmin
import org.apache.hadoop.conf.Configuration

/**
 * Destroys all data for security purposes. triggered by the monitor.
 * @param curator curator instance
 * @param zkDestroyPaths comma separated list of folders to be removed
 * @param scriptFile script to run
 * @param simulateDestroy are we simulating?
 * @param startScript should we run the script
 * @param hbaseTableNames comma separated list of hbase tables to delete
 */
class DestroyData(protected val curator: Curator, protected val hbaseConfig: Configuration, zkDestroyPaths: String, scriptFile: File, simulateDestroy: Boolean, startScript: Boolean, hbaseTableNames: String)
  extends CuratorActor {

  def receive = {
    case DestroyMessage(info) ⇒ {
      destroy()
    }
  }

  /**
   * main entry point for destroying zookeeper and HBase data
   */
  private def destroy() {
    if (simulateDestroy) {
      log.info("Simulating: Self destruct sequence started...")
      log.info("Simulating: Data destroyed.")
    } else {
      log.info("Self destruct sequence started...")
      destroyZookeeper()
      destroyHBase()
      log.info("Data destroyed.")
    }

    if (startScript) {
      if (scriptFile.exists) {
        log.info("Start script, path [{}]", scriptFile.getAbsolutePath)
        launchScript(scriptFile)
      } else {
        log.warning("Script does not exists, path [{}]", scriptFile.getAbsolutePath)
      }
    }
  }

  /**
   * delete number of zookeeper paths
   */
  private def destroyZookeeper() {
    log.info("Deleting zookeeper paths")
    val paths = if (zkDestroyPaths.trim.size > 0) zkDestroyPaths.split(",").map(_.trim) else Array[String]()
    paths.foreach(name ⇒ {
      val path = Path(name)
      log.info("Delete zookeeper path [{}]", name)
      curator.deleteRecursive(path)
    })
    log.info("Zookeeper paths destroyed")
  }

  /**
   * delete tables from HBase
   */
  private def destroyHBase() {
    log.info("Deleting HBase tables")
    val tableNames = if (hbaseTableNames.trim.size > 0) hbaseTableNames.split(",").map(_.trim) else Array[String]()
    val admin = new HBaseAdmin(hbaseConfig)
    tableNames.foreach(tableName ⇒ {
      log.info("Disable hbase table [{}]", tableName)
      admin.disableTable(tableName)
      log.info("Delete hbase table [{}]", tableName)
      admin.deleteTable(tableName)
    })
    admin.close()

    log.info("HBase tables deleted")
  }

  private[this] def launchScript(scriptPath: File): Int = {
    import scala.collection.JavaConverters._

    // variable to keep the first exception and prevent other exceptions to hide it.
    var cachedException: Exception = null
    var bufferedReader: BufferedReader = null
    var exitValue: Int = 0
    val arguments = List("sh", scriptPath.getAbsolutePath)
    val processBuilder = new ProcessBuilder(arguments.asJava)

    try {
      val process = processBuilder.start()

      bufferedReader = new BufferedReader(new InputStreamReader(process.getErrorStream))
      Stream.continually(bufferedReader.readLine()).takeWhile(_ ne null) foreach { line ⇒ log.error("Error while running script: {}", line) }

      exitValue = process.waitFor()
      log.debug("script exit value: " + exitValue)
    } catch {
      case e: Exception ⇒
        log.error(e, "launchScript failed.")
        cachedException = e
    } finally {
      cachedException = closeCloseable(bufferedReader, "BufferedReader cannot be closed.", cachedException)

      if (cachedException != null)
        throw cachedException
    }

    exitValue
  }

  def closeCloseable(closable: Closeable, logMsg: String, cachedException: Exception): Exception = {
    var result: Exception = cachedException
    try {
      if (closable != null)
        closable.close()
    } catch {
      case e: Exception ⇒
        log.error(e, logMsg)
        if (cachedException == null)
          result = e
    }

    result
  }
}

case class DestroyMessage(info: String)

/*
  private def recursiveListFiles(f: File): Array[File] = {
    val these = f.listFiles
    these ++ these.filter(_.isDirectory).flatMap(recursiveListFiles)
  }
*/

