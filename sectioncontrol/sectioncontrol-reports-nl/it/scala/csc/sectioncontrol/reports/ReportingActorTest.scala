/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.reports

import java.io.File
import java.util.TimeZone

import akka.actor.ActorSystem
import akka.dispatch.Await
import akka.pattern.ask
import akka.testkit.TestActorRef
import akka.util.Timeout
import akka.util.duration._
import csc.akkautils.DirectLogging
import csc.config.Path
import csc.curator.utils.{Curator, CuratorToolsImpl}
import csc.hbase.utils.testframework.HBaseTestFramework
import csc.sectioncontrol.enforce.{DayViolationsStatistics, ExportViolationsStatistics}
import csc.sectioncontrol.messages._
import csc.sectioncontrol.messages.matcher.MatcherCorridorConfig
import csc.sectioncontrol.reports.metrics.MetricsGenerator
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storagelayer.{CorridorConfig, SectionControlConfig}
import org.apache.curator.framework.CuratorFrameworkFactory
import org.apache.curator.retry.RetryNTimes
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.util.Bytes
import org.scalatest.matchers.MustMatchers
import org.scalatest.{BeforeAndAfterEach, WordSpec}

class ReportingActorTest extends WordSpec with MustMatchers with GZIPCompressor
  with HBaseTestFramework with DirectLogging with BeforeAndAfterEach {
  implicit val testSystem = ActorSystem("ReportingActorTest")
  val systemId = "A2"
  var actorRef: TestActorRef[ReportingActor] = _
  var testStorage: TestZkStorage = _
  var mockServiceStatisticsAccess: MockServiceStatisticsRepositoryAccess = _
  var logTable: HTable = _
  var curator: Curator = _
  //override def startZkCluster = false

  override def beforeAll() {
    super.beforeAll()
    implicit def string2ByteArray(s: String): Array[Byte] = Bytes.toBytes(s)
    logTable = testUtil.createTable(Paths.Systems.getSystemsLogTableName, "log")

    val formats = JsonFormats.formats

    val clientScope = Some(CuratorFrameworkFactory.newClient(zkConnectionString, new RetryNTimes(1, 1000)))
    clientScope.foreach(_.start())
    curator = new CuratorToolsImpl(clientScope, log, formats)

    mockServiceStatisticsAccess = new MockServiceStatisticsRepositoryAccess()
    actorRef = TestActorRef(new ReportingActor(systemId, curator, hbaseConfig, mockServiceStatisticsAccess))
    testStorage = new TestZkStorage(curator)
  }

  override def afterAll() {
    actorRef.stop()
    logTable.close()
    testSystem.shutdown()
    super.afterAll()
  }

  override protected def beforeEach(): Unit = {
    super.beforeEach()
    testStorage.setDayReportConfig()
  }

  "ReportGenerator" must {
    "deliver no report without runtime data" in {
      implicit val timeout = Timeout(5 seconds)
      val future = actorRef ? GenerateReport(systemId, StatsDuration("20120401", TimeZone.getTimeZone("Europe/Amsterdam")))
      val result = Await.result(future, timeout.duration).asInstanceOf[Option[ZkDayStatistics]]
      result must be(None)
    }

    "deliver report" in {
      //
      val stats = StatsDuration("20120401", TimeZone.getTimeZone("Europe/Amsterdam"))
      //configure ZooKeeper
      testStorage.createConfiguration1(systemId, stats)
      implicit val timeout = Timeout(15 seconds)
      val future = actorRef ? GenerateReport(systemId, stats)
      val result = Await.result(future, timeout.duration).asInstanceOf[Option[ZkDayStatistics]]
      val data: ZkDayStatistics = result.get
      data.id must be("20120401")
      data.fileName must be("A2_1_Amsterdam_20120401.xml.gz")
      data.day must be(stats.start)
      data.data.size > 500 must be(true)
      data.createDate < System.currentTimeMillis() must be(true)
      System.currentTimeMillis() - data.createDate < 10000 must be(true)

      val reportPath = Path(Paths.Systems.getReportPath(systemId, stats))
      val report: ZkDayStatistics = curator.getVersioned[ZkDayStatistics](reportPath).get.data
      data.data.size must be(report.data.size)
      data.data must be(report.data)
      testStorage.deleteConfiguration(systemId)
    }

    "create report to file" in {
      //
      val dir = new File("dagraportage")
      if (dir.exists) {
        //cleanup
        dir.listFiles().foreach(_.delete())
      }
      val configPath = Path(Paths.Configuration.getReportsConfigPath)
      val stats = StatsDuration("20120401", TimeZone.getTimeZone("Europe/Amsterdam"))
      //configure ZooKeeper
      testStorage.createConfiguration1(systemId, stats)
      implicit val timeout = Timeout(15 seconds)
      val future = actorRef ? GenerateReport(systemId, stats)
      val result = Await.result(future, timeout.duration).asInstanceOf[Option[ZkDayStatistics]]
      val data: ZkDayStatistics = result.get
      data.id must be("20120401")
      data.fileName must be("A2_1_Amsterdam_20120401.xml.gz")
      data.day must be(stats.start)
      data.data.size > 500 must be(true)
      //      val decompressed = decompress(data.data.map(_.toByte).toArray)
      //      val xml = (new String(decompressed.map(_.toChar)))
      //      FileUtils.writeStringToFile(new File("/tmp/DayReport_output.xml"), xml)

      data.createDate < System.currentTimeMillis() must be(true)
      System.currentTimeMillis() - data.createDate < 10000 must be(true)

      dir.exists() must be(true)
      dir.isDirectory must be(true)
      val files = dir.listFiles()
      files.length must be(1)
      val file = files(0)
      file.getName must be("A2_1_Amsterdam_20120401.xml")
      file.delete()
      dir.delete()
      testStorage.deleteConfiguration(systemId)
    }

    "calculate AllCorridorsStatistics" in {
      //
      val stats = StatsDuration("20120401", TimeZone.getTimeZone("Europe/Amsterdam"))
      //configure ZooKeeper
      val laneStats: List[LaneInfo] = List(LaneInfo(Lane("strook123", "Strook 1", "opritmaarsen", systemId, 10.1, 20.2), 1000),
        LaneInfo(Lane("strook321", "Strook 1", "afslagbreukelen", systemId, 40.1, 30.2), 900))
      val export: ExportViolationsStatistics = ExportViolationsStatistics(1234, 301, 311, 321, 331, 341, 351, 361, 371)

      testStorage.createConfiguration1(systemId, stats)
      //runtime matcher statistics
      val stat1 = SectionControlStatistics(StatisticsPeriod(stats, 1342173624653L, 1342173324653L),
        MatcherCorridorConfig("A2", "1", 1234, "9547", "9542", 2983, 1000, 3600000), 38, 14, 4, 0, 140.0f, 140, laneStats)
      val stat2 = SectionControlStatistics(StatisticsPeriod(stats, 1342173624653L, 1342173324653L),
        MatcherCorridorConfig("A2", "1", 1234, "9547", "9542", 2983, 1000, 3600000), 38, 14, 4, 0, 140.0f, 140, laneStats)
      mockServiceStatisticsAccess.setSectionControlStatistics(Seq(stat1, stat2))

      val implementation = actorRef.underlyingActor

      val dayStats = DayViolationsStatistics("A2", stats, Seq(export))
      val corData = createCorridorData()
      val serviceResults = implementation.retrieveData.readServiceResults(stats, Seq(corData))
      val allCorridorsStatistics = MetricsGenerator.calculate(dayStats, serviceResults)

      val laneStats2: List[LaneInfo] = List(LaneInfo(Lane("strook123", "Strook 1", "opritmaarsen", systemId, 10.1, 20.2), 2000),
        LaneInfo(Lane("strook321", "Strook 1", "afslagbreukelen", systemId, 40.1, 30.2), 1800))
      val corridorStatistics = SectionControlCorridorStatistics(1234, laneStats2, export, 76, 28, 8, 0, 140.0, 140)
      val allStatistics = AllCorridorsStatistics(systemId, stats, Seq(corridorStatistics))
      allCorridorsStatistics.toString must be(allStatistics.toString)
      testStorage.deleteConfiguration(systemId)
    }
  }

  def createZkDailyReportConfig():ZkDailyReportConfig={
    val dailyReportConfigSpeedEntry100 = DailyReportConfigSpeedEntry("100", "TCS UT A2 R-1 (100 km/h)", "a0021001")
    val dailyReportConfigSpeedEntry130 = DailyReportConfigSpeedEntry("130", "TCS UT A2 R-1 (130 km/h)", "a0021301")
    val dailyReportConfigSpeedEntryTotal = DailyReportConfigSpeedEntry("total", "TCS UT A2 R-1 (totaal)", "a0021001, a0021301 (totaal)")


    val ignoreStatisticsOfLane1 = IgnoreStatisticsOfLane("A2","E1","E1R1")
    val ignoreStatisticsOfLane2 = IgnoreStatisticsOfLane("A2","E1","E1R2")
    val ignoreStatisticsOfLanes = List(ignoreStatisticsOfLane1, ignoreStatisticsOfLane2)
    val speedEntries = List(dailyReportConfigSpeedEntry100, dailyReportConfigSpeedEntry130, dailyReportConfigSpeedEntryTotal)
    ZkDailyReportConfig(ignoreStatisticsOfLanes, speedEntries)
  }

  def createCorridorData(): CorridorData = {
    val lane = LaneData(id = "strook321", name = "Strook 1", bpsLaneId = Some("2 HR R- R"))
    val gantry = GantryData(id = "afslagbreukelen", name = "afslagbreukelen", hectometer = "49.9HM", lanes = Seq(lane))
    val section = SectionData(zkSection = ZkSection(id = "sec",
      systemId = systemId,
      name = "sec",
      length = 2000,
      startGantryId = "afslagbreukelen",
      endGantryId = "afslagbreukelen",
      matrixBoards = Seq("matrix"),
      msiBlackList = Nil),
      inGantry = gantry,
      outGantry = gantry)
    CorridorData(corridor = CorridorConfig(id = "1234",
      name = "1234",
      roadType = 1,
      serviceType = ServiceType.SectionControl,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = true,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = null,
      info = ZkCorridorInfo(corridorId = 1234,
        locationCode = "2",
        reportId = "TCS A2 Links  39.7 - 54.9",
        reportHtId = "A020011",
        reportPerformance = true,
        locationLine1 = "test"),
      startSectionId = "sec",
      endSectionId = "sec",
      allSectionIds = Seq("sec"),
      serviceConfig = SectionControlConfig()),
      startSection = section,
      endSection = section,
      createZkDailyReportConfig())
  }
}
