package csc.sectioncontrol.reports.export

/**
 * Copyright (C) 2016 CSC. <http://www.csc.com>
 *
 * Created on 5/11/16.
 */

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.messages.StatsDuration
import csc.sectioncontrol.reports.metrics.Period
import csc.sectioncontrol.storage.ZkState
import csc.sectioncontrol.reports.i18n.Messages
import java.util.TimeZone

class EnforceLogTest extends WordSpec with MustMatchers {
  val duration = StatsDuration("20140203", TimeZone.getTimeZone("Europe/Amsterdam"))
  val oneSecond = 1000L
  val oneMinute = 60 * oneSecond
  val tenMinutes = 10 * oneMinute
  val oneHour = 60 * oneMinute
  val systemUser = "System"

  import ZkState._
  "EnforceLog" must {
    "transform (schedule)periods to Seq[ZkState] and optionally insert extra ZkState at beginning of duration" when {

      "schedule starts at the beginning of duration" in {
        val periods = List(Period(duration.start, duration.start + 2 * oneHour))
        val states = EnforceLog.transformPeriodsToStates(duration, periods)

        states.size must be(2)
        states(0).timestamp must be(duration.start)
        states(0).state must be(enforceOn)

        states(1).timestamp must be(duration.start + 2 * oneHour)
        states(1).state must be(enforceOff)

      }
      "schedule does not start at the beginning of duration" in {
        val periods = List(Period(duration.start + oneHour, duration.start + 2 * oneHour))
        val states = EnforceLog.transformPeriodsToStates(duration, periods)

        states.size must be(3)
        states(0).timestamp must be(duration.start)
        states(0).state must be(enforceOff)

        states(1).timestamp must be(duration.start + oneHour)
        states(1).state must be(enforceOn)

        states(2).timestamp must be(duration.start + 2 * oneHour)
        states(2).state must be(enforceOff)
      }
    }

    "properly collate periods" when {
      val p00 = Period(duration.start, duration.start + oneHour)
      val p01 = Period(p00.to, p00.to + oneHour)
      val p02 = Period(p01.to, p01.to + oneHour)

      val p10 = Period(p02.to + oneHour, p02.to + 2 * oneHour)
      val p11 = Period(p10.to, p10.to + oneHour)
      val p12 = Period(p11.to, p11.to + oneHour)

      val p20 = Period(p12.to + oneHour, p12.to + 2 * oneHour)
      val p21 = Period(p20.to, p20.to + oneHour)
      val p22 = Period(p21.to, p21.to + oneHour)

      "the sequence is empty" in {
        EnforceLog.collatePeriods(Nil) must be(Nil)
      }

      "the sequence contains a single period" in {
        EnforceLog.collatePeriods(Seq(p00)) must be(Seq(p00))
      }

      "all periods coincide" in {
        val result = EnforceLog.collatePeriods(Seq(p01, p02, p00))
        result must be(Seq(Period(p00.from, p02.to)))
      }

      "no periods coincide" in {
        EnforceLog.collatePeriods(Seq(p00)) must be(Seq(p00))

        val result0 = EnforceLog.collatePeriods(Seq(p02, p00))
        result0 must be(Seq(p00, p02)) // Collating returns a sorted sequence
      }

      "some periods coincide and others do not" in {
        val result1 = EnforceLog.collatePeriods(Seq(p02, p01, p12, p10, p11))
        result1 must be(Seq(Period(p01.from, p02.to), Period(p10.from, p12.to)))
      }
    }

    "create enforcelog states" when {
      "the system is not enforcing and has no schedule" in {
        val states = List(ZkState.off, ZkState.enforceOff, ZkState.Maintenance, ZkState.standBy, ZkState.failure)
        states.foreach(state ⇒ {
          val result = EnforceLog.createEnforceLogStates(duration, Nil,
            Seq(ZkState(state, duration.start + 1, systemUser, Messages("csc.sectioncontrol.reports.start_schedule"))))
          result.map { s ⇒ s.state } must be(List(state, state))
        })
      }

      "the system is enforcing and there is a schedule covering the duration" in {
        val states = List(ZkState.enforceDegraded, ZkState.enforceOn)
        states.foreach((state) ⇒ {
          val result = EnforceLog.createEnforceLogStates(duration,
            Seq(Period(duration.start, duration.tomorrow.start)),
            Seq(ZkState(state, duration.start, systemUser, Messages("System is on"))))

          result.size must be(2)

          result(0).timestamp must be(duration.start)
          result(0).state must be(state)

          result(1).timestamp must be(duration.tomorrow.start)
          result(1).state must be(state)
        })
      }

      """the system is enforcing and a single schedule starts after beginning of the duration and ends before the end of the duration""" in {
        val result = EnforceLog.createEnforceLogStates(duration,
          Seq(Period(duration.start + oneHour, duration.start + 2 * oneHour)),
          Seq(ZkState(ZkState.enforceOn, duration.start, systemUser, Messages("csc.sectioncontrol.reports.no_start_of_schedule_event"))))

        result.size must be(4)
        result.map { s ⇒ s.state } must be(List(ZkState.enforceOff, ZkState.enforceOn, ZkState.enforceOff, ZkState.enforceOff))
      }

      """the system is not enforcing and a single schedule starts after beginning of the duration and ends before the end of the duration""" in {
        val result = EnforceLog.createEnforceLogStates(duration,
          Seq(Period(duration.start + oneHour, duration.start + 2 * oneHour)),
          Seq(ZkState(ZkState.standBy, duration.start, systemUser, Messages("csc.sectioncontrol.reports.no_start_of_schedule_event"))))

        result.size must be(4)
        result.map { s ⇒ s.state } must be(List(ZkState.standBy, ZkState.standBy, ZkState.standBy, ZkState.standBy))
      }
    }
  }
}
