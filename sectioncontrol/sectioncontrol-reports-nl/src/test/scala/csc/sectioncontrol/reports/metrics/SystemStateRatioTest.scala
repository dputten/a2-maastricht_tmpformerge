/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

package csc.sectioncontrol.reports.metrics

import common.Rounder
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import java.util.{ Date, Calendar }
import java.text.SimpleDateFormat
import csc.sectioncontrol.storage.ZkState
import java.util.concurrent.TimeUnit
import csc.sectioncontrol.reports.metrics.Period._
import sun.rmi.server.Activation.SystemRegistryImpl

object Extensions {

  def defaultDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")

  implicit def stringWrapper(value: String) = new StringExtensions(value)

  class StringExtensions(val value: String) {
    def toDate(implicit formatter: SimpleDateFormat = defaultDateFormat): Date = {
      formatter.parse(value)
    }
  }
}

import Extensions._

class StateRatioTest extends WordSpec with MustMatchers with Rounder {
  import ZkState._
  val periods100 = List(
    Period("2012-11-29T06:00:00", "2012-11-29T19:00:00"))

  val periods130 = List(
    Period("2012-11-29T00:00:00", "2012-11-29T06:00:00"),
    Period("2012-11-29T19:00:00", "2012-11-29T24:00:00"))

  val states_single = List(
    ZkState(enforceOn, "2012-11-29T10:00:00".toDate.getTime, "123", "test"))

  val states_simple = List(
    ZkState(enforceOn, "2012-11-29T00:00:00".toDate.getTime, "123", "test"),
    ZkState(enforceOn, "2012-11-29T24:00:00".toDate.getTime, "123", "test"))

  val states_complex_ordered = List(
    ZkState(enforceOn, "2012-11-29T00:00:00".toDate.getTime, "123", "test"),
    ZkState(enforceDegraded, "2012-11-29T02:00:00".toDate.getTime, "123", "test"),
    ZkState(enforceOn, "2012-11-29T08:00:00".toDate.getTime, "123", "test"),
    ZkState(standBy, "2012-11-29T11:00:00".toDate.getTime, "123", "test"),
    ZkState(enforceOn, "2012-11-29T12:00:00".toDate.getTime, "123", "test"),
    ZkState(standBy, "2012-11-29T24:00:00".toDate.getTime, "123", "test"))

  val states_complex_unordered = List(
    ZkState(standBy, "2012-11-29T24:00:00".toDate.getTime, "123", "test"),
    ZkState(enforceDegraded, "2012-11-29T02:00:00".toDate.getTime, "123", "test"),
    ZkState(enforceOn, "2012-11-29T08:00:00".toDate.getTime, "123", "test"),
    ZkState(standBy, "2012-11-29T11:00:00".toDate.getTime, "123", "test"),
    ZkState(enforceOn, "2012-11-29T00:00:00".toDate.getTime, "123", "test"),
    ZkState(enforceOn, "2012-11-29T12:00:00".toDate.getTime, "123", "test"))

  val maxDate: Long = (new Date(Long.MaxValue)).getTime
  //val maxDate: String = (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")).format(new Date(Long.MaxValue))

  val expected_result_states_complex_ordered = Map(
    enforceOn -> Seq(
      Period("2012-11-29T00:00:00", "2012-11-29T02:00:00"),
      Period("2012-11-29T08:00:00", "2012-11-29T11:00:00"),
      Period("2012-11-29T12:00:00", "2012-11-29T24:00:00")),
    //Period("2012-11-29T12:00:00", "2012-11-29T24:00:00")),
    enforceDegraded -> Seq(
      Period("2012-11-29T02:00:00", "2012-11-29T08:00:00")),
    standBy -> Seq(
      Period("2012-11-29T11:00:00", "2012-11-29T12:00:00"),
      Period("2012-11-29T24:00:00".toDate.getTime, maxDate)))

  val expected_result_states_complex_unordered = expected_result_states_complex_ordered

  def prettyPrint(periods: Map[String, List[Period]]) {
    println("BEGIN")
    periods.foreach {
      state ⇒
        println(state._1)
        state._2.foreach {
          period ⇒
            //val to =
            println("(" + new Date(period.from) + ", " + new Date(period.to))
        }
    }
    println("EIND")
  }

  def prettyPrint(ratios: Seq[StateRatio]) {
    println("BEGIN StateRatio")
    ratios.foreach {
      ratio ⇒
        println("State:  " + ratio.state)
        println("Ratio: " + ratio.ratio)
        println("Total: " + new Date(ratio.range.from) + ", " + new Date(ratio.range.to))
        println("Periods:")

        ratio.periods.foreach {
          period ⇒
            println("(" + new Date(period.from) + ", " + new Date(period.to) + ")")
        }

        println("Periods (given):")
        ratio.allPeriods.foreach {
          period ⇒
            println("(" + new Date(period.from) + ", " + new Date(period.to) + ")")
        }

        println("allPeriods: => " + ratio.timeAllPeriods)
        println("timePeriods: => " + ratio.timePeriods)

        //println("statePeriods:")
        ratio.statePeriods.foreach {
          statePeriod ⇒
          //println("(" + new Date(statePeriod.from) + ", " + new Date(statePeriod.to) + ")")
        }

    }
    println("TOTAL RATIO:  " + ratios.map(_.ratio).sum)
    println("EIND StateRatio")
    println()
  }

  "SystemStateRatio" must {
    "do the right thing " in {
      val states = List(
        ZkState(enforceOn, "2012-12-12T08:08:51".toDate.getTime, "123", "test"),
        ZkState(standBy, "2012-12-13T15:40:43".toDate.getTime, "123", "test"),
        ZkState(enforceOn, "2012-12-13T16:04:31".toDate.getTime, "123", "test"))
      //SystemState(enforceOn, "2012-12-13T24:00:00".toDate.getTime))

      val period = Period(from = "2012-12-13T06:00:00".toDate.getTime, to = "2012-12-13T19:00:00".toDate.getTime)

      /*
        period = 13 hour =
        enforce:
          2012-12-12T08:08:51 - 2012-12-13T15:40:43
          2012-12-13T16:04:31 - .....
        ratio:
          2012-12-13T06:00:00 - 2012-12-13T15:40:43 => 09:40:43 = 580 minutes (minus seconds)
          2012-12-13T16:04:31 - 2012-12-13T19:00:00 => 02:55:29 = 175 minutes (minus secs)
          = 755 minutes
          = 755/780
          = 0.967948718

        standby:
          2012-12-13T15:40:43 - 2012-12-13T16:04:31
        ratio: 19:07
          2012-12-13T15:40:43 - 2012-12-13T16:04:31 => 00:23:37 = 23 minutes (minus seconds)
          = 23 minutes
          = 23/780
          = 0.029487179
       */

      val ratio = StateRatio(List(period), states)
      val enforceState = ratio.find(_.state == enforceOn)
      val standByState = ratio.find(_.state == standBy)

      enforceState must not be (None)
      standByState must not be (None)

      roundBigDecimal(enforceState.get.ratio, 2) must be(0.97)
      roundBigDecimal(standByState.get.ratio, 2) must be(0.03)
    }
    "calculate the right begin and end period" when {
      "given an empty list of system states" in {
        StateRatio.statePeriods(Nil) must be(Map.empty)
      }
      "given a list of system states" in {
        val periods_ordered = StateRatio.statePeriods(states_complex_ordered)
        val periods_unordered = StateRatio.statePeriods(states_complex_unordered)

        periods_ordered must be(expected_result_states_complex_ordered)
        periods_unordered must be(expected_result_states_complex_unordered)
      }
      "given a single system state" in {
        val periods = StateRatio.statePeriods(List(ZkState(enforceOn, "2012-11-29T10:00:00".toDate.getTime, "123", "test")))
        val expected = Map(enforceOn -> Seq(Period("2012-11-29T10:00:00".toDate.getTime, (new Date(Long.MaxValue)).getTime)))

        periods must be(expected)
      }

      "given a list of system state that spread over multiple days" in {
        val states = List(
          ZkState(enforceOn, "2013-12-29T10:00:00".toDate.getTime, "123", "test"),
          ZkState(enforceOn, "2012-12-02T14:00:00".toDate.getTime, "123", "test"),
          ZkState(enforceOn, "2012-12-06T14:00:00".toDate.getTime, "123", "test"),
          ZkState(standBy, "2011-12-29T13:00:00".toDate.getTime, "123", "test"),
          ZkState(standBy, "2006-12-05T13:00:00".toDate.getTime, "123", "test"),
          ZkState(standBy, "2012-11-29T24:00:00".toDate.getTime, "123", "test"),
          ZkState(enforceDegraded, "2012-11-29T02:00:00".toDate.getTime, "123", "test"),
          ZkState(enforceOn, "2012-11-29T08:00:00".toDate.getTime, "123", "test"),
          ZkState(standBy, "2012-11-29T11:00:00".toDate.getTime, "123", "test"),
          ZkState(enforceOn, "2012-11-29T00:00:00".toDate.getTime, "123", "test"),
          ZkState(enforceOn, "2012-11-29T12:00:00".toDate.getTime, "123", "test"))

        val expected = Map(
          standBy -> Seq(
            Period("2006-12-05T13:00:00", "2011-12-29T13:00:00"),
            Period("2011-12-29T13:00:00", "2012-11-29T00:00:00"),
            Period("2012-11-29T11:00:00", "2012-11-29T12:00:00"),
            Period("2012-11-29T24:00:00", "2012-12-02T14:00:00")),
          enforceOn -> Seq(
            Period("2012-11-29T00:00:00", "2012-11-29T02:00:00"),
            Period("2012-11-29T08:00:00", "2012-11-29T11:00:00"),
            Period("2012-11-29T12:00:00", "2012-11-29T24:00:00"),
            Period("2012-12-02T14:00:00", "2012-12-06T14:00:00"),
            Period("2012-12-06T14:00:00", "2013-12-29T10:00:00"),
            Period("2013-12-29T10:00:00".toDate.getTime, (new Date(Long.MaxValue)).getTime)),
          enforceDegraded -> Seq(
            Period("2012-11-29T02:00:00", "2012-11-29T08:00:00")))

        val periods = StateRatio.statePeriods(states)

        periods must be(expected)
      }
    }
    "calculate the right range" when {
      "'from' and 'to' range is left outside of state period" in {
        val range = Period("2012-12-05T10:00:00", "2012-12-05T11:00:00")
        val statePeriod = Period("2012-12-05T13:00:00", "2012-12-05T15:00:00")

        val result = StateRatio.ranges(enforceOn, List(statePeriod), range, range)
        //val result = StateRatio.ranges(enforceOn, statePeriod, range)
        result.ratio must be(0.00)
      }
      "'from' and 'to' range is right outside of state period" in {
        val range = Period("2012-12-05T16:00:00", "2012-12-05T17:00:00")
        val statePeriod = Period("2012-12-05T13:00:00", "2012-12-05T15:00:00")

        val result = StateRatio.ranges(enforceOn, List(statePeriod), range, range)
        //result.ratio =
        result.ratio must be(0.00)
      }
      "'from' and 'to' period is within state period" in {
        val range = Period("2012-12-05T14:00:00", "2012-12-05T14:10:00")
        val statePeriod = Period("2012-12-05T13:00:00", "2012-12-05T15:00:00")

        /*        val result = StateRatio.ranges(enforceOn, statePeriod, range)
        result must not be (None)
        val (state, resultPeriod, rangePeriod) = result.get
        state must be(enforceOn)
        resultPeriod.from must be(range.from)
        resultPeriod.to must be(range.to)
        rangePeriod must be(range)*/

        val stateRatio = StateRatio.ranges(enforceOn, List(statePeriod), range, range)
        stateRatio.periods.length must be(1)

        val period = stateRatio.periods(0)
        stateRatio.state must be(enforceOn)
        period.from must be(range.from)
        period.to must be(range.to)

      }
      "'from' period is left outside and 'to' period is within state period" in {
        val range = Period("2012-12-05T09:00:00", "2012-12-05T11:00:00")
        val statePeriod = Period("2012-12-05T10:00:00", "2012-12-05T15:00:00")

        /*       val result = StateRatio.ranges(enforceOn, statePeriod, range)
        result must not be (None)
        val (state, resultPeriod, rangePeriod) = result.get
        state must be(enforceOn)
        resultPeriod.from must be(statePeriod.from)
        resultPeriod.to must be(range.to)
        rangePeriod must be(range)*/

        val stateRatio = StateRatio.ranges(enforceOn, List(statePeriod), range, range)
        stateRatio.periods.length must be(1)

        val period = stateRatio.periods(0)
        stateRatio.state must be(enforceOn)
        period.from must be(statePeriod.from)
        period.to must be(range.to)
      }
      "'to' period is right outside and 'from' period is within state period" in {
        val statePeriod = Period("2012-12-05T13:00:00", "2012-12-05T15:00:00")
        val range = Period("2012-12-05T14:00:00", "2012-12-05T16:00:00")

        /*        val result = StateRatio.ranges(enforceOn, statePeriod, range)
        result must not be (None)
        val (state, resultPeriod, rangePeriod) = result.get
        state must be(enforceOn)
        resultPeriod.from must be(range.from)
        resultPeriod.to must be(statePeriod.to)
        rangePeriod must be(range)*/

        val stateRatio = StateRatio.ranges(enforceOn, List(statePeriod), range, range)
        stateRatio.periods.length must be(1)

        val period = stateRatio.periods(0)
        stateRatio.state must be(enforceOn)
        period.from must be(range.from)
        period.to must be(statePeriod.to)
      }
    }
    "calculate the ratio" when {
      "given one 24 hour period within the same day and one state" in {
        /*
         Period: 2012-12-29T00:00:00 - 2012-12-29T24:00:00 = 24 uur

         enforceOn:
           2012-12-29T00:00:00 - 2012-12-29T24:00:00 = 24 uur
         ratio: 24/24 = 1.000
        */
        val states = List(
          ZkState(enforceOn, "2012-12-29T00:00:00".toDate.getTime, "123", "test"),
          ZkState(enforceOn, "2012-12-29T24:00:00".toDate.getTime, "123", "test"))

        val periods = List(
          Period("2012-12-29T00:00:00", "2012-12-29T24:00:00"))

        val ratios = StateRatio(periods, states)

        ratios.length must be(1)

        val ratio = ratios(0)

        ratio.state must be(enforceOn)
        ratio.ratio must be(1.00)
      }
      "given 24 hour period within the same day and multiple systemstates" in {
        /*
          Period: 2012-12-29T00:00:00 - 2012-12-29T24:00:00 = 24 uur

          standby:
            2012-12-29T06:00:00 - 2012-12-29T12:00:00 = 06 uur
          ratio: 6/24 = 0.25

          enforceOn:
            2012-12-29T00:00:00 - 2012-12-29T06:00:00 = 06 uur
            2012-12-29T12:00:00 - 2012-12-29T24:00:00 = 12 uur
          ratio: 18/24 = 0.75

          enforceDegraded:
            2012-12-29T24:00:00 - 20122012-12-29T24:00:00 = 0 uur
          ratio: 0/0 = 0.00 =? not 24 hours because it's outside the period
         */

        val states = List(
          ZkState(enforceOn, "2012-12-29T00:00:00".toDate.getTime, "123", "test"),
          ZkState(standBy, "2012-12-29T06:00:00".toDate.getTime, "123", "test"),
          ZkState(enforceOn, "2012-12-29T12:00:00".toDate.getTime, "123", "test"),
          ZkState(enforceDegraded, "2012-12-29T24:00:00".toDate.getTime, "123", "test"))

        val periods = List(
          Period("2012-12-29T00:00:00", "2012-12-29T24:00:00"))

        val ratios = StateRatio(periods, states)

        //prettyPrint(ratios)
        ratios.length must be(3)

        val ratioEnforceOn = ratios.find(_.state == enforceOn)
        val ratioStandby = ratios.find(_.state == standBy)
        val ratioEnforceDegraded = ratios.find(_.state == enforceDegraded)

        ratioEnforceOn must not be (None)
        ratioStandby must not be (None)
        ratioEnforceDegraded must not be (None)

        ratioEnforceOn.get.ratio must be(0.75)
        ratioStandby.get.ratio must be(0.25)
        ratioEnforceDegraded.get.ratio must be(0)
      }
      "given four 6 hour periods within the same day and multiple systemstates" in {
        /*
          Period: 6+4+3+2.5 = 15.5 hours

          standby:
            2012-12-29T03:00:00 - 2012-12-29T06:00:00
            2012-12-29T12:00:00 - 2012-12-29T15:00:00
            2012-12-29T15:00:00 - 2012-12-29T18:00:00
          ratio:
           2012-12-29T00:00:00 - 2012-12-29T06:00:00 = 3 uur
           2012-12-29T08:00:00 - 2012-12-29T12:00:00 = 0 uur
           2012-12-29T15:00:00 - 2012-12-29T18:00:00 = 3 uur
           2012-12-29T18:30:00 - 2012-12-29T24:00:00 = 0 uur

		      6/15 = 0.387096774

          enforceOn:
           2012-12-29T00:00:00 - 2012-12-29T03:00:00 = 03 uur
           2012-12-29T06:00:00 - 2012-12-29T09:00:00 = 03 uur
           2012-12-29T18:00:00 - 2012-12-29T21:00:00 = 03 uur
          ratio:
           2012-12-29T00:00:00 - 2012-12-29T06:00:00 = 3 uur
           2012-12-29T06:00:00 - 2012-12-29T06:00:00 = 0
           2012-12-29T08:00:00 - 2012-12-29T09:00:00 = 1
           012-12-29T18:00:00 - 2012-12-29T18:00:00 = 0
           012-12-29T18:30:00 - 2012-12-29T21:00:00 = 2.5

           6.5/15.5 = 0.419354839

          enforceDegraded:
            2012-12-29T09:00:00 - 2012-12-29T12:00:00 = 0 uur
            2012-12-29T21:00:00 - 2012-12-29T21:00:00 = 0 uur ('to' is end of period
          ratio:
           2012-12-29T09:00:00 - 2012-12-29T012:00:00 = 3 uur
           2012-12-29T21:00:00 - 2012-12-29T21:00:00 = 0 uur

           3/15.5 = 0.193548387
         */

        val states = List(
          ZkState(enforceOn, "2012-12-29T00:00:00".toDate.getTime, "123", "test"),
          ZkState(standBy, "2012-12-29T03:00:00".toDate.getTime, "123", "test"),
          ZkState(enforceOn, "2012-12-29T06:00:00".toDate.getTime, "123", "test"),
          ZkState(enforceDegraded, "2012-12-29T09:00:00".toDate.getTime, "123", "test"),
          ZkState(standBy, "2012-12-29T12:00:00".toDate.getTime, "123", "test"),
          ZkState(standBy, "2012-12-29T15:00:00".toDate.getTime, "123", "test"),
          ZkState(enforceOn, "2012-12-29T18:00:00".toDate.getTime, "123", "test"),
          ZkState(enforceDegraded, "2012-12-29T21:00:00".toDate.getTime, "123", "test"))
        //00:00:00 -
        val periods = List(
          Period("2012-12-29T00:00:00", "2012-12-29T06:00:00"),
          Period("2012-12-29T08:00:00", "2012-12-29T12:00:00"),
          Period("2012-12-29T15:00:00", "2012-12-29T18:00:00"),
          Period("2012-12-29T18:30:00", "2012-12-29T21:00:00"))

        val ratios = StateRatio(periods, states)

        ratios.length must be(3)

        val ratioEnforceOn = ratios.find(_.state == enforceOn)
        val ratioStandby = ratios.find(_.state == standBy)
        val ratioEnforceDegraded = ratios.find(_.state == enforceDegraded)

        ratioEnforceOn must not be (None)
        ratioStandby must not be (None)
        ratioEnforceDegraded must not be (None)

        ratios.filter(_.state == enforceOn).length must be(1)
        ratios.filter(_.state == standBy).length must be(1)
        ratios.filter(_.state == enforceDegraded).length must be(1)

        roundBigDecimal(ratioEnforceOn.get.ratio, 2) must be(0.42)
        roundBigDecimal(ratioStandby.get.ratio, 2) must be(0.39)
        roundBigDecimal(ratioEnforceDegraded.get.ratio, 2) must be(0.19)
      }
      "given multiple periods spread over multiple days" in {
        /*
          period = 6 + 4 + 3 + 5.5 = 18.5

          enforceOn:
          2010-12-29T00:00:00 - 2011-12-29T03:00:00
          2012-09-29T06:00:00 - 2012-12-29T09:00:00
          2012-12-29T18:00:00 - 2013-12-29T21:00:00
          ratio:
          2010-12-29T00:00:00 - 2011-12-29T03:00:00 = 3 uur
          2010-12-29T06:00:00 - 2011-12-29T06:00:00 = 0 uur
          2010-12-29T08:00:00 - 2011-12-29T09:00:00 = 1 uur
          2012-12-29T18:00:00 - 2012-12-29T18:00:00 = 0 uur
          2012-12-29T18:30:00 - 2012-12-29T24:00:00 = 2.5 uur
          = 12.5/18.5
          = 0.067567568

         */
        val states = List(
          ZkState(enforceOn, "2010-12-29T00:00:00".toDate.getTime, "123", "test"),
          ZkState(standBy, "2011-12-29T03:00:00".toDate.getTime, "123", "test"),
          ZkState(enforceOn, "2012-09-29T06:00:00".toDate.getTime, "123", "test"),
          ZkState(enforceDegraded, "2012-12-29T09:00:00".toDate.getTime, "123", "test"),
          ZkState(standBy, "2012-12-29T12:00:00".toDate.getTime, "123", "test"),
          ZkState(standBy, "2012-12-29T15:00:00".toDate.getTime, "123", "test"),
          ZkState(enforceOn, "2012-12-29T18:00:00".toDate.getTime, "123", "test"),
          ZkState(enforceDegraded, "2013-12-29T21:00:00".toDate.getTime, "123", "test"))

        val periods = List(
          Period("2001-12-29T00:00:00", "2022-12-29T06:00:00"),
          Period("2012-06-29T08:00:00", "2013-01-29T12:00:00"),
          Period("2012-04-29T15:00:00", "2012-08-29T18:00:00"),
          Period("2011-01-29T18:30:00", "2012-01-29T24:00:00"))

        val ratios = StateRatio(periods, states)

        ratios.length must be(3)

        val ratioEnforceOn = ratios.find(_.state == enforceOn)
        val ratioStandby = ratios.find(_.state == standBy)
        val ratioEnforceDegraded = ratios.find(_.state == enforceDegraded)

        ratioEnforceOn must not be (None)
        ratioStandby must not be (None)
        ratioEnforceDegraded must not be (None)

        ratios.filter(_.state == enforceOn).length must be(1)
        ratios.filter(_.state == standBy).length must be(1)
        ratios.filter(_.state == enforceDegraded).length must be(1)

        prettyPrint(ratios)

        roundBigDecimal(ratioEnforceOn.get.ratio, 2) must be(0.10)
        roundBigDecimal(ratioStandby.get.ratio, 2) must be(0.03)
        roundBigDecimal(ratioEnforceDegraded.get.ratio, 2) must be(0.39)
      }
    }
  }
}
