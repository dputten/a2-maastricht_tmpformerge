package csc.sectioncontrol.reports

import java.io.{ FileInputStream, FileFilter, File }
import java.util.zip.GZIPInputStream

import csc.sectioncontrol.reports.data.{ RetrieveReportData, ReportInput }
import csc.sectioncontrol.storage.{ SpeedFixedStatistics, SectionControlStatistics, RedLightStatistics, ServiceStatistics }
import csc.sectioncontrol.storagelayer._
import csc.sectioncontrol.storagelayer.corridor.CorridorIdMapping
import net.liftweb.json._
import org.apache.commons.io.{ IOUtils, FileUtils }

/**
 * Created by carlos on 19.10.15.
 */
object RecreateReportsTool {

  implicit val formats = JsonFormats.formats + AmbiguousImplDeserializer + MapSerializer

  val jsonFilter: FileFilter = new FileFilter {
    override def accept(pathname: File): Boolean = pathname.getName.endsWith(".json") || pathname.getName.endsWith(".json.gz")
  }

  def readJson(file: File): String = {
    val bytes = FileUtils.readFileToByteArray(file)
    new String(bytes)
  }

  def readGzipContentAsString(file: File): String = {
    val in = new GZIPInputStream(new FileInputStream(file))
    val bytes = IOUtils.toByteArray(in)
    in.close()
    new String(bytes)
  }

  def main(args: Array[String]): Unit = {
    //    val json = readJson(new File("/home/carlos/sampleData/tca-70/LogEntry.json"))
    //    val obj = Serialization.read[LogEntry[SystemEvent]](json)
    //    println(obj)
    val file = new File(args(0))
    execute(file)
  }

  def execute(file: File): Unit = {
    if (file.exists() && file.isFile) {
      generateReportFrom(file, file.getParentFile)
    } else if (file.isDirectory) {
      val files = file.listFiles(jsonFilter)
      files.foreach(generateReportFrom(_, file))
    }
  }

  def parseReportInput(json: String): ReportInput = {
    val input: ReportInput = Serialization.read[ReportInput](json)

    //maps with complex keys must be fixed
    val corridorStates = fixMapKey(input.corridorStates, corridorIdMappingParser)
    val corridorAlerts = fixMapKey(input.corridorAlerts, corridorIdMappingParser)
    val calibrationFailurePeriods = fixMapKey(input.calibrationFailurePeriods, corridorIdMappingParser)
    val serviceResults = fixMapKey(input.serviceResults, intParser)
    val periods = fixMapKey(input.periods, intParser)
    val corridorSchedules = fixMapKey(input.corridorSchedules, intParser)

    input.copy(corridorStates = corridorStates,
      corridorAlerts = corridorAlerts,
      calibrationFailurePeriods = calibrationFailurePeriods,
      serviceResults = serviceResults,
      periods = periods,
      corridorSchedules = corridorSchedules)
  }

  def readReportInput(inputFile: File): Option[ReportInput] = {
    val json = extension(inputFile) match {
      case Some(".json")    ⇒ Some(readJson(inputFile))
      case Some(".json.gz") ⇒ Some(readGzipContentAsString(inputFile))
      case other            ⇒ None //throw new IllegalArgumentException("Unable to process file " + inputFile.getAbsolutePath)
    }
    json map parseReportInput
  }

  def extension(file: File): Option[String] = file.getName.indexOf('.') match {
    case 0 ⇒ None
    case x ⇒ Some(file.getName.substring(x))
  }

  def generateReportFrom(inputFile: File, outputFolder: File): Unit = {
    val outFile = outputFile(inputFile, outputFolder)
    println("Reading input for " + inputFile.getName)
    readReportInput(inputFile) match {
      case None ⇒ println("Could not parse ReportInput from file " + inputFile.getAbsolutePath)
      case Some(input) ⇒ {
        //println(input)
        val builder = RetrieveReportData.create(input, "Dagrapport_%s", None)
        val xmlBytes = builder.output
        //println(new String(xmlBytes))
        println("Writing generated report to " + outFile.getAbsolutePath)
        FileUtils.writeByteArrayToFile(outFile, xmlBytes)
      }
    }
  }

  def outputFile(input: File, outputFolder: File): File = {
    val idx = input.getName.indexOf('.')
    val name = if (idx > 0) input.getName.substring(0, idx) else input.getName
    new File(outputFolder, name + ".xml")
  }

  def fixMapKey[K, V](source: Map[_, V], converter: String ⇒ K): Map[K, V] =
    source.map { e ⇒ converter(e._1.toString) -> e._2 } toMap

  val corridorIdMappingPattern = """CorridorIdMapping\((.+),(.+)\)""".r

  val corridorIdMappingParser: String ⇒ CorridorIdMapping = { str ⇒
    val it = corridorIdMappingPattern.findAllIn(str).matchData.map {
      m ⇒ CorridorIdMapping(m.group(1), m.group(2).toInt)
    }.toIterator
    if (it.hasNext) it.next() else sys.error("Unable to parse CorridorIdMapping from " + str)
  }

  val intParser: String ⇒ Int = { str ⇒ str.toInt }

}

//utility to deserialize ambiguous objects (from interfaces)
object AmbiguousImplDeserializer extends Serializer[AnyRef] {

  def serialize(implicit format: Formats): PartialFunction[Any, JValue] = Map.empty

  override def deserialize(implicit format: Formats): PartialFunction[(TypeInfo, JValue), AnyRef] = {
    case (ti, value) if ti.clazz == classOf[ServiceConfig] ⇒ { //ServiceConfig
      getAllFields(value).get("flgFileName") match {
        case Some(flg) ⇒ flg match {
          case "traject.flg"   ⇒ Extraction.extract[SectionControlConfig](value)
          case "roodlicht.flg" ⇒ Extraction.extract[RedLightConfig](value)
          case "snelheid.flg"  ⇒ Extraction.extract[SpeedFixedConfig](value)
          case "doelgroep.flg" ⇒ Extraction.extract[TargetGroupConfig](value)
          case _               ⇒ sys.error("Cannot safely guess which ServiceConfig impl it is")
        }
        case None ⇒ sys.error("Cannot safely guess which ServiceConfig impl it is")
      }
    }
    case (ti, value) if ti.clazz == classOf[ServiceStatistics] ⇒ { //ServiceStatistics
      val fields = getAllFields(value)
      if (fields.get("averageRedLight").isDefined) {
        Extraction.extract[RedLightStatistics](value)
      } else if (fields.get("input").isDefined) {
        Extraction.extract[SectionControlStatistics](value)
      } else if (fields.get("nrRegistrations").isDefined) {
        Extraction.extract[SpeedFixedStatistics](value)
      } else {
        sys.error("Cannot safely guess which ServiceStatistics impl it is")
      }
    }
    case (ti, value) if ti.clazz == classOf[Manifest[_]] ⇒ null //hack needed to deserialize the implicit field 'evidence' of LogEntry
    //case (ti, value) if check(ti, value) => null
  }

  def getAllFields(value: JValue): Map[String, Any] = {
    val obj = value.asInstanceOf[JObject]
    obj.values
  }

  def check(ti: TypeInfo, value: JValue): Boolean = {
    false
  }
}
