/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.reports

import export.DayReportBuilderV40Settings
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.messages.SystemEvent

class DayReportSettingTest extends WordSpec with MustMatchers {

  val longMatchableStringWithNewLinesAndTabs = """
                            |ctcs:Gebruiker [ctcs] met verbalisantcode [MD3214] heeft voor systeem
                            |[3215] de beheerparameters aangepast; - parameter
                            |       [Bordsnelheid] is van [80]
                          """.stripMargin

  val settings = new DayReportBuilderV40Settings()
  "DayReportBuilderV40Settings" must {
    "translate componetIds" in {
      settings.translateComponentToCode(systemEvent("Boot", "info")) must be("0300000")
      settings.translateComponentToCode(systemEvent("ftp", "info")) must be("0400000")
      settings.translateComponentToCode(systemEvent("sitebuffer", "failure", "gesloten")) must be("0200000")
      settings.translateComponentToCode(systemEvent("systemstate", "SystemStateChange", "Stand-by -&gt; Handhaven aan")) must be("3000000")
      settings.translateComponentToCode(systemEvent("systemstate", "systemstatechange", "Alarm -> Geen")) must be("1000000")
      settings.translateComponentToCode(systemEvent("web", "_", "Gebruiker bla met verbalisantcode MQ1234 heeft voor service [Snelheid] een status verandering van A naar B gevraagd")) must be("0103000")
      settings.translateComponentToCode(systemEvent("web", "_", longMatchableStringWithNewLinesAndTabs)) must be("0102000")
      settings.translateComponentToCode(systemEvent("systemstate", "systemstateChange", "Geen -> Alarm")) must be("4000000")
      settings.translateComponentToCode(systemEvent("ntp-checking", "info", "...")) must be("0300000")
      settings.translateComponentToCode(systemEvent("processmtmdata", "alert", "...")) must be("0300000")
      settings.translateComponentToCode(systemEvent("sitebuffer", "failure", "Temperatuur is buiten bereik, data from sitebuffer: too hot")) must be("0310000")
      settings.translateComponentToCode(systemEvent("1234-camera", "JAI-SOME-ERROR")) must be("0200000")
      settings.translateComponentToCode(systemEvent("unknown", "info")) must be("0000999")
    }
  }

  def systemEvent(componentid: String, eventType: String, reason: String = "") = SystemEvent(componentid, 1l, "somesystemid", eventType, "someuserid", Some(reason))

}