package csc.sectioncontrol.reports

import java.io.ByteArrayInputStream
import java.util.TimeZone
import javax.xml.XMLConstants
import javax.xml.transform.stream.StreamSource
import javax.xml.validation.SchemaFactory

import akka.actor.ActorSystem
import csc.akkautils.DirectLogging
import csc.config.Path
import csc.curator.CuratorTestServer
import csc.curator.utils.{ Curator, CuratorToolsImpl }
import csc.dlog.LogEntry
import csc.sectioncontrol.enforce.{ DayViolationsStatistics, ExportViolationsStatistics, MtmSpeedSetting, MtmSpeedSettings, _ }
import csc.sectioncontrol.messages.{ Lane, RedLightCorridorStatistics, SectionControlCorridorStatistics, SpeedFixedCorridorStatistics, _ }
import csc.sectioncontrol.messages.matcher.MatcherCorridorConfig
import csc.sectioncontrol.reports.data.RetrieveReportData
import csc.sectioncontrol.reports.metrics.red.RedMetrics
import csc.sectioncontrol.reports.metrics.{ MetricsGenerator, Period }
import csc.sectioncontrol.storage.{ LaneInfo, RedLightStatistics, SectionControlStatistics, SerialNumber, SpeedFixedStatistics, StatisticsPeriod, ZkAlert, ZkAlertHistory, ZkCorridor, ZkCorridorInfo, ZkDirection, ZkPSHTMIdentification, ZkSection, ZkSystem, ZkSystemLocation, ZkSystemPardonTimes, ZkSystemRetentionTimes, _ }
import csc.sectioncontrol.storagelayer.{ CorridorConfig, RedLightConfig }
import csc.sectioncontrol.storagelayer.alerts.AlertService
import csc.sectioncontrol.storagelayer.corridor.{ CorridorIdMapping, CorridorService }
import csc.sectioncontrol.storagelayer.excludelog.{ ExcludeLogLayer, SuspendEvent }
import csc.sectioncontrol.storagelayer.state.StateService
import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import csc.sectioncontrol.reports.schedules.CorridorSpeedSchedule
import csc.sectioncontrol.reports.export.DayReportBuilder

class ReportGeneratorTest extends WordSpec with MustMatchers with CuratorTestServer
  with DirectLogging with BeforeAndAfterAll {
  implicit val testSystem = ActorSystem("ReportGeneratorTest")
  val systemId = "A2"
  var testStorage: TestZkStorage = _
  var curator: Curator = _

  val oneSecond = 1000L
  val oneMinute = 60 * oneSecond
  val tenMinutes = 10 * oneMinute
  val oneHour = 60 * oneMinute

  override def beforeEach() {
    super.beforeEach()

    val formats = JsonFormats.formats

    curator = new CuratorToolsImpl(clientScope, log, formats)
    testStorage = new TestZkStorage(curator)
  }

  override def afterEach() {
    super.afterEach()
  }

  override def afterAll() {
    testSystem.shutdown()
    super.afterAll()
  }

  val corridorServiceMock = new CorridorServiceMock()

  "ReportGenerator" must {

    "create tc document" in {
      val mockServiceStatisticsAccess = new MockServiceStatisticsRepositoryAccess()
      val retrieveReportData = new RetrieveReportData(systemId, curator, testSystem, mockServiceStatisticsAccess, corridorServiceMock)

      val stats = StatsDuration("20120401", TimeZone.getTimeZone("Europe/Amsterdam"))
      //configure ZooKeeper
      val laneStats: List[LaneInfo] = List(LaneInfo(Lane("strook123", "Strook 1", "opritmaarsen", systemId, 10.1, 20.2), 1000),
        LaneInfo(Lane("strook321", "Strook 1", "afslagbreukelen", systemId, 40.1, 30.2), 900))

      testStorage.createConfiguration1(systemId, stats)
      val sectionData = retrieveReportData.retrieveSystemData.getSectionData("a")
      sectionData.zkSection.id must be("a")

      val systemData = retrieveReportData.retrieveSystemData.getSystemData
      log.info("systemData: " + systemData)
      systemData.corridors.size must be(3)
      val corridorId: String = systemData.corridors.head.corridor.id

      // schedules per corridor
      val schedules: Map[Int, Seq[CorridorSpeedSchedule]] = Map(
        1 -> Seq(
          CorridorSpeedSchedule(corridorId, 1, 100, stats.start, stats.start + 5 * oneHour),
          CorridorSpeedSchedule(corridorId, 1, 130, stats.start + 5 * oneHour, stats.tomorrow.start - 1)))

      // system states
      StateService.create(systemId, corridorId, ZkState("EnforceOn", 11111L, "user1", "Begin Universe"), curator)
      var result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("StandBy", stats.start - 1000, "user1", "Previous state"), result.get.version, curator)
      result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("EnforceOn", stats.start + 3 * oneMinute, "user2", "Begin meetdag"), result.get.version, curator)
      result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("StandBy", stats.start + 3 * oneHour, "user3", "Maintain"), result.get.version, curator)
      result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("Failure", stats.start + 13 * oneHour, "user4", "Continue"), result.get.version, curator)
      result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("StandBy", stats.tomorrow.start, "user4", "Go home"), result.get.version, curator)

      val systemStates = StateService.getStateLogHistory(systemId, corridorId, stats.start, stats.tomorrow.start, curator)
      log.info("stateLogHistory: " + systemStates)
      systemStates.size must be(4)
      // corridor alerts
      AlertService.create(systemId, corridorId, ZkAlert("qn-01", "", "Camera 5 uitgevallen", 1111122L, ConfigType.System, 0.05f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-02", "", "Camera 5 is terug", stats.start - 1, ConfigType.System, 0.15f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-03", "", "Lunch", stats.start, ConfigType.System, 0.25f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-04", "", "Feest", stats.start + 1000 * 60 * 60 * 4 + 23000, ConfigType.System, 0.288f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-05", "", "Onderhoud", stats.tomorrow.start, ConfigType.System, 0.32f), curator)

      val systemAlerts = AlertService.getSystemAlerts(curator, systemId, corridorId, stats.tomorrow.start)
      systemAlerts.size must be(4)
      //
      val events = List(LogEntry(1L, 3L, 5L, System.currentTimeMillis(), SystemEvent("com1", System.currentTimeMillis(), systemId, "ALARM", "user1", Some("Relax"))),
        LogEntry(1L, 4L, 6L, System.currentTimeMillis(), SystemEvent("com2", System.currentTimeMillis(), systemId, "ALARM", "user1", None)))
      //MTM
      val mtmList = List(MtmSpeedSetting(stats.start, "a020001", "100"), MtmSpeedSetting(stats.start + 100000, "a020001", "onbepaald"))
      val allMtm = new MtmSpeedSettings(mtmList, stats)
      val mtmStorePath = Path(Paths.Systems.getRuntimeMtmInfoPath(systemId, stats))
      curator.put(mtmStorePath, allMtm)

      //create metrics
      val export1: ExportViolationsStatistics = ExportViolationsStatistics(1, 100, 301, 311, 321, 331, 341, 351, 361, 371)
      val export2: ExportViolationsStatistics = ExportViolationsStatistics(2, 100, 301, 311, 321, 331, 341, 351, 361, 371)
      val export3: ExportViolationsStatistics = ExportViolationsStatistics(3, 100, 301, 311, 321, 331, 341, 351, 361, 371)
      val dayStats = DayViolationsStatistics("A2", stats, Seq(export1, export2, export3))

      val stat0 = SectionControlStatistics(StatisticsPeriod(stats, stats.tomorrow.start - 1, stats.start + 12 * oneHour),
        MatcherCorridorConfig("A2", "1", 1, "9547", "9542", 2983, 1000, 3600000), 1000, 990, 10, 2, 90.1, 180, laneStats)
      val stat1 = SectionControlStatistics(StatisticsPeriod(stats, stats.start + 12 * oneHour, stats.start),
        MatcherCorridorConfig("A2", "1", 1, "9547", "9542", 2983, 1000, 3600000), 1000, 990, 10, 2, 90.1, 180, laneStats)
      val stat2 = RedLightStatistics(StatisticsPeriod(stats, 1342173624653L, 1342173324653L),
        1000, 990, 1120.1, 1180, laneStats)
      val stat3 = SpeedFixedStatistics(StatisticsPeriod(stats, 1342173624653L, 1342173324653L),
        1000, 990, 90.1, 180, laneStats)
      mockServiceStatisticsAccess.setSectionControlStatistics(Seq(stat0, stat1))
      mockServiceStatisticsAccess.setRedLightStatistics(Seq(stat2))
      mockServiceStatisticsAccess.setSpeedFixedStatistics(Seq(stat3))

      val serviceResults = retrieveReportData.readServiceResults(stats, systemData.corridors)
      serviceResults.size must be(3)
      var list = serviceResults.get(1).get
      list.size must be(2)
      list.contains(stat0) must be(true)
      list.contains(stat1) must be(true)

      list = serviceResults.get(2).get
      list.size must be(1)
      list.head must be(stat2)
      list = serviceResults.get(3).get
      list.size must be(1)
      list.head must be(stat3)

      val allCorridorStatistics = MetricsGenerator.calculate(dayStats, serviceResults, schedules)
      val allStats = allCorridorStatistics.corridorData
      val scStats = allStats.collect { case s: SectionControlCorridorStatistics ⇒ s }
      scStats.size must be(3)

      val _NoneStats = scStats.find(s ⇒ s.corridorId == 1 && s.speedLimit == None).get // must be()
      val _100Stats = scStats.find(s ⇒ s.corridorId == 1 && s.speedLimit == Some(100)).get

      _100Stats must be(SectionControlCorridorStatistics(Some(100), 1, laneStats, export1, 1000, 990, 10, 2, 90.1, 180))
      _NoneStats must be(SectionControlCorridorStatistics(None, 1, LaneInfo.coalesce(laneStats ++ laneStats).toList, export1.copy(speedLimit = 0), 2000, 1980, 20, 4, 90.1, 180))

      val rlStats = allStats.collect { case s: RedLightCorridorStatistics ⇒ s }
      rlStats.size must be(1)
      rlStats(0) must be(RedLightCorridorStatistics(2, laneStats, export2, 1000, 990, 1120.1, 1180))

      val sfStats = allStats.collect { case s: SpeedFixedCorridorStatistics ⇒ s }
      sfStats.size must be(1)
      sfStats(0) must be(SpeedFixedCorridorStatistics(3, laneStats, export3, 1000, 990, 90.1, 180))

      //create report
      val dayReportBuilder: DayReportBuilder = retrieveReportData.create(stats, events.iterator, dayStats, DayReportVersion.HHM_V40, Some(dayReportConfig))
      //check the result
      dayReportBuilder.filenameZip must be("A2_1_Amsterdam_20120401.xml.gz")
      dayReportBuilder.key must be(StatsDuration("20120401", TimeZone.getTimeZone("Europe/Amsterdam")))

      val factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
      val xsdStream = getClass.getResourceAsStream("/HHM-dagrapport-v4.0.xsd")
      val schema = factory.newSchema(new StreamSource(xsdStream))
      val source = new ByteArrayInputStream(dayReportBuilder.output)
      val xml = new SchemaAwareFactoryAdapter(schema).load(source)

      val xmlChild = xml.child
      xmlChild.size must be(9)
    }

    "create tc document 4.2" in {
      val mockServiceStatisticsAccess = new MockServiceStatisticsRepositoryAccess()
      val retrieveReportData: RetrieveReportData = new RetrieveReportData(systemId, curator, testSystem, mockServiceStatisticsAccess, corridorServiceMock)

      val stats = StatsDuration("20120401", TimeZone.getTimeZone("Europe/Amsterdam"))
      //configure ZooKeeper
      val laneStats: List[LaneInfo] = List(LaneInfo(Lane("strook123", "Strook 1", "opritmaarsen", systemId, 10.1, 20.2), 1000),
        LaneInfo(Lane("strook321", "Strook 1", "afslagbreukelen", systemId, 40.1, 30.2), 900))

      // schedules per corridor
      val schedules: Map[Int, Seq[CorridorSpeedSchedule]] = Map(1 -> Seq(CorridorSpeedSchedule("test", 1, 100, stats.start, stats.tomorrow.start)))

      testStorage.createConfiguration1(systemId, stats)
      val sectionData = retrieveReportData.retrieveSystemData.getSectionData("a")
      sectionData.zkSection.id must be("a")

      val systemData = retrieveReportData.retrieveSystemData.getSystemData
      systemData.corridors.size must be(3)
      val corridorId = systemData.corridors.head.corridor.id
      // system states
      StateService.create(systemId, corridorId, ZkState("EnforceOn", 11111L, "user1", "Begin Universe"), curator)
      var result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("StandBy", stats.start - 1000, "user1", "Previous state"), result.get.version, curator)
      result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("Maintenance", stats.start + 1000 * 60 * 3, "user2", "Begin meetdag"), result.get.version, curator)
      result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("StandBy", stats.start + 1000 * 60 * 60 * 3, "user3", "Maintain"), result.get.version, curator)
      result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("Failure", stats.start + 1000 * 60 * 60 * 13, "user4", "Continue"), result.get.version, curator)
      result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("StandBy", stats.tomorrow.start, "user4", "Go home"), result.get.version, curator)

      val systemStates = StateService.getStateLogHistory(systemId, corridorId, stats.start, stats.tomorrow.start, curator)
      systemStates.size must be(4)
      // system alerts
      AlertService.create(systemId, corridorId, ZkAlert("qn-01", "", "Camera 5 uitgevallen", 1111122L, ConfigType.System, 0.05f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-02", "", "Camera 5 is terug", stats.start - 1, ConfigType.System, 0.15f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-03", "", "Lunch", stats.start, ConfigType.System, 0.25f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-04", "", "Feest", stats.start + 1000 * 60 * 60 * 4 + 23000, ConfigType.System, 0.288f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-05", "", "Onderhoud", stats.tomorrow.start, ConfigType.System, 0.32f), curator)

      val systemAlerts = AlertService.getSystemAlerts(curator, systemId, corridorId, stats.tomorrow.start)
      systemAlerts.size must be(4)
      //
      val events = List(LogEntry(1L, 3L, 5L, System.currentTimeMillis(), SystemEvent("com1", System.currentTimeMillis(), systemId, "ALARM", "user1", Some("Relax"))),
        LogEntry(1L, 4L, 6L, System.currentTimeMillis(), SystemEvent("com2", System.currentTimeMillis(), systemId, "ALARM", "user1", None)))
      //MTM
      val mtmList = List(MtmSpeedSetting(stats.start, "a020001", "100"), MtmSpeedSetting(stats.start + 100000, "a020001", "onbepaald"))
      val allMtm = new MtmSpeedSettings(mtmList, stats)
      val mtmStorePath = Path(Paths.Systems.getRuntimeMtmInfoPath(systemId, stats))
      if (curator.exists(mtmStorePath))
        curator.deleteRecursive(mtmStorePath)
      if (!curator.exists(mtmStorePath.parent))
        curator.createEmptyPath(mtmStorePath.parent)
      curator.put(mtmStorePath, allMtm)

      //create metrics
      val export1: ExportViolationsStatistics = ExportViolationsStatistics(1, 100, 301, 311, 321, 331, 341, 351, 361, 371)
      val export2: ExportViolationsStatistics = ExportViolationsStatistics(2, 100, 301, 311, 321, 331, 341, 351, 361, 371)
      val export3: ExportViolationsStatistics = ExportViolationsStatistics(3, 100, 301, 311, 321, 331, 341, 351, 361, 371)
      val dayStats = DayViolationsStatistics("A2", stats, Seq(export1, export2, export3))

      val stat1 = SectionControlStatistics(StatisticsPeriod(stats, stats.tomorrow.start - 1, stats.start),
        MatcherCorridorConfig("A2", "1", 1, "9547", "9542", 2983, 1000, 3600000), 1000, 990, 10, 2, 90.1, 180, laneStats)
      val stat2 = RedLightStatistics(StatisticsPeriod(stats, 1342173624653L, 1342173324653L),
        1000, 990, 1120.1, 1180, laneStats)
      val stat3 = SpeedFixedStatistics(StatisticsPeriod(stats, 1342173624653L, 1342173324653L),
        1000, 990, 90.1, 180, laneStats)
      mockServiceStatisticsAccess.setSectionControlStatistics(Seq(stat1))
      mockServiceStatisticsAccess.setRedLightStatistics(Seq(stat2))
      mockServiceStatisticsAccess.setSpeedFixedStatistics(Seq(stat3))

      val serviceResults = retrieveReportData.readServiceResults(stats, systemData.corridors)
      serviceResults.size must be(3)
      var list = serviceResults.get(1).get
      list.size must be(1)
      list.head must be(stat1)
      list = serviceResults.get(2).get
      list.size must be(1)
      list.head must be(stat2)
      list = serviceResults.get(3).get
      list.size must be(1)
      list.head must be(stat3)

      val allCorridorStatistics = MetricsGenerator.calculate(dayStats, serviceResults, schedules)
      allCorridorStatistics.corridorData.size must be(4)

      val allStats = allCorridorStatistics.corridorData
      val scStats = allStats.collect { case s: SectionControlCorridorStatistics ⇒ s }

      scStats.size must be(2)
      val _100Stats = scStats.find(s ⇒ s.corridorId == 1 && s.speedLimit == Some(100)).get
      log.info("scStats: " + scStats)
      _100Stats must be(SectionControlCorridorStatistics(Some(100), 1, laneStats, export1, 1000, 990, 10, 2, 90.1, 180))

      val rlStats = allStats.collect { case s: RedLightCorridorStatistics ⇒ s }
      rlStats.size must be(1)
      rlStats(0) must be(RedLightCorridorStatistics(2, laneStats, export2, 1000, 990, 1120.1, 1180))

      val sfStats = allStats.collect { case s: SpeedFixedCorridorStatistics ⇒ s }
      sfStats.size must be(1)
      sfStats(0) must be(SpeedFixedCorridorStatistics(3, laneStats, export3, 1000, 990, 90.1, 180))

      //create report
      val dayReportBuilder = retrieveReportData.create(stats, events.iterator, dayStats, DayReportVersion.HHM_V42, Some(dayReportConfig))

      //check the result
      dayReportBuilder.filenameZip must be("A2_1_Amsterdam_20120401.xml.gz")
      dayReportBuilder.key must be(StatsDuration("20120401", TimeZone.getTimeZone("Europe/Amsterdam")))
      //println(new String(dayReportBuilder.output, "ISO-8859-1"))
      val factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
      val xsdStream = getClass.getResourceAsStream("/HHM-dagrapport-v4.2.xsd")
      val schema = factory.newSchema(new StreamSource(xsdStream))
      val source = new ByteArrayInputStream(dayReportBuilder.output)
      val xml = new SchemaAwareFactoryAdapter(schema).load(source)

      val xmlChild = xml.child
      xmlChild.size must be(9)
    }

    "create tc document 4.21" in {
      val mockServiceStatisticsAccess = new MockServiceStatisticsRepositoryAccess()
      val retrieveReportData = new RetrieveReportData(systemId, curator, testSystem, mockServiceStatisticsAccess, corridorServiceMock)

      val stats = StatsDuration("20120401", TimeZone.getTimeZone("Europe/Amsterdam"))
      //configure ZooKeeper
      val laneStats: List[LaneInfo] =
        List(
          LaneInfo(Lane("strook123", "Strook 1", "opritmaarsen", systemId, 10.1, 20.2), 1000),
          LaneInfo(Lane("strook321", "Strook 1", "afslagbreukelen", systemId, 40.1, 30.2), 900))

      testStorage.createConfiguration1(systemId, stats)
      val sectionData = retrieveReportData.retrieveSystemData.getSectionData("a")
      sectionData.zkSection.id must be("a")

      // schedules per corridor
      val schedules: Map[Int, Seq[CorridorSpeedSchedule]] = Map(
        1 -> Seq(CorridorSpeedSchedule("test", 1, 130, stats.start, stats.start + 6 * oneHour),
          CorridorSpeedSchedule("test", 1, 100, stats.start + 6 * oneHour, stats.start + 19 * oneHour),
          CorridorSpeedSchedule("test", 1, 130, stats.start + 19 * oneHour, stats.tomorrow.start)))

      val systemData = retrieveReportData.retrieveSystemData.getSystemData
      systemData.corridors.size must be(3)
      val corridorId = "1" //systemData.corridors.head.corridor.id
      // system states
      StateService.create(systemId, corridorId, ZkState("EnforceOn", 11111L, "user1", "Begin Universe"), curator)
      var result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("StandBy", stats.start - 1000, "user1", "Previous state"), result.get.version, curator)
      result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("Maintenance", stats.start + 3 * oneMinute, "user2", "Camerareparatie"), result.get.version, curator)
      result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("StandBy", stats.start + 3 * oneHour, "user3", "Maintain"), result.get.version, curator)
      result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("Failure", stats.start + 13 * oneHour, "user4", "Temperatuur te hoog"), result.get.version, curator)
      result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("Off", stats.start + 16 * oneHour, "user4", "Turn it Off"), result.get.version, curator)
      result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("StandBy", stats.tomorrow.start, "user4", "Go home"), result.get.version, curator)

      val systemStates = StateService.getStateLogHistory(systemId, corridorId, stats.start, stats.tomorrow.start, curator)
      systemStates.size must be(5)
      // system alerts
      AlertService.create(systemId, corridorId, ZkAlert("qn-01", "", "Camera 5 uitgevallen", 1111122L, ConfigType.System, 0.05f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-02", "", "Camera 5 is terug", stats.start - 1, ConfigType.System, 0.15f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-03", "", "Lunch", stats.start, ConfigType.System, 0.25f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-04", "", "Feest", stats.start + 1000 * 60 * 60 * 4 + 23000, ConfigType.System, 0.288f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-05", "", "Onderhoud", stats.tomorrow.start, ConfigType.System, 0.32f), curator)

      val systemAlerts = AlertService.getSystemAlerts(curator, systemId, corridorId, stats.tomorrow.start)
      systemAlerts.size must be(4)
      //
      val events = List(LogEntry(1L, 3L, 5L, System.currentTimeMillis(), SystemEvent("com1", System.currentTimeMillis(), systemId, "ALARM", "user1", Some("Relax"))),
        LogEntry(1L, 4L, 6L, System.currentTimeMillis(), SystemEvent("com2", System.currentTimeMillis(), systemId, "ALARM", "user1", None)))
      //MTM
      val mtmList = List(MtmSpeedSetting(stats.start, "a020001", "100"), MtmSpeedSetting(stats.start + 100000, "a020001", "onbepaald"))
      val allMtm = new MtmSpeedSettings(mtmList, stats)
      val mtmStorePath = Path(Paths.Systems.getRuntimeMtmInfoPath(systemId, stats))
      if (curator.exists(mtmStorePath))
        curator.deleteRecursive(mtmStorePath)
      if (!curator.exists(mtmStorePath.parent))
        curator.createEmptyPath(mtmStorePath.parent)
      curator.put(mtmStorePath, allMtm)

      //create metrics
      val export0: ExportViolationsStatistics = ExportViolationsStatistics(1, 100, 301, 311, 321, 331, 341, 351, 361, 371)
      val export1: ExportViolationsStatistics = ExportViolationsStatistics(1, 130, 201, 211, 221, 231, 241, 251, 261, 271)
      val export2: ExportViolationsStatistics = ExportViolationsStatistics(2, 100, 301, 311, 321, 331, 341, 351, 361, 371)
      val export3: ExportViolationsStatistics = ExportViolationsStatistics(3, 100, 301, 311, 321, 331, 341, 351, 361, 371)
      val dayStats = DayViolationsStatistics("A2", stats, Seq(export0, export1, export2, export3))

      val stat0 = SectionControlStatistics(StatisticsPeriod(stats, stats.start + 12 * oneHour, stats.start),
        MatcherCorridorConfig("A2", "1", 1, "9547", "9542", 2983, 1000, 3600000), 1000, 990, 10, 2, 90.1, 180, laneStats)
      val stat1 = SectionControlStatistics(StatisticsPeriod(stats, stats.tomorrow.start - 1, stats.start + 12 * oneHour),
        MatcherCorridorConfig("A2", "1", 1, "9547", "9542", 2983, 1000, 3600000), 1000, 990, 10, 2, 90.1, 180, laneStats)
      val stat2 = RedLightStatistics(StatisticsPeriod(stats, 1342173624653L, 1342173324653L),
        1000, 990, 1120.1, 1180, laneStats)
      val stat3 = SpeedFixedStatistics(StatisticsPeriod(stats, 1342173624653L, 1342173324653L),
        1000, 990, 90.1, 180, laneStats)
      mockServiceStatisticsAccess.setSectionControlStatistics(Seq(stat0, stat1))
      mockServiceStatisticsAccess.setRedLightStatistics(Seq(stat2))
      mockServiceStatisticsAccess.setSpeedFixedStatistics(Seq(stat3))

      val serviceResults = retrieveReportData.readServiceResults(stats, systemData.corridors)
      serviceResults.size must be(3)
      var list = serviceResults.get(1).get
      list.size must be(2)
      list must be(Seq(stat0, stat1))
      list = serviceResults.get(2).get
      list.size must be(1)
      list.head must be(stat2)
      list = serviceResults.get(3).get
      list.size must be(1)
      list.head must be(stat3)

      val allCorridorStatistics: AllCorridorsStatistics = MetricsGenerator.calculate(dayStats, serviceResults, schedules)

      val allStats = allCorridorStatistics.corridorData
      val scStats = allStats.collect { case s: SectionControlCorridorStatistics ⇒ s }
      scStats.size must be(3)

      val rlStats = allStats.collect { case s: RedLightCorridorStatistics ⇒ s }
      rlStats.size must be(1)
      rlStats(0) must be(RedLightCorridorStatistics(2, laneStats, export2, 1000, 990, 1120.1, 1180))

      val sfStats = allStats.collect { case s: SpeedFixedCorridorStatistics ⇒ s }
      sfStats.size must be(1)
      sfStats(0) must be(SpeedFixedCorridorStatistics(3, laneStats, export3, 1000, 990, 90.1, 180))

      //create report
      val dayReportBuilder = retrieveReportData.create(stats, events.iterator, dayStats, DayReportVersion.HHM_V421, Some(dayReportConfig))
      println(new String(dayReportBuilder.output, "ISO-8859-1"))

      //check the result
      dayReportBuilder.filenameZip must be("A2_1_Amsterdam_20120401.xml.gz")
      dayReportBuilder.key must be(StatsDuration("20120401", TimeZone.getTimeZone("Europe/Amsterdam")))

      val factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
      val xsdStream = getClass.getResourceAsStream("/HHM-dagrapport-v4.21.xsd")
      val schema = factory.newSchema(new StreamSource(xsdStream))
      val source = new ByteArrayInputStream(dayReportBuilder.output)
      val xml = new SchemaAwareFactoryAdapter(schema).load(source)

      val xmlChild = xml.child
      xmlChild.size must be(9)
    }

    "create 3 type in document" in {
      val mockServiceStatisticsAccess = new MockServiceStatisticsRepositoryAccess()
      val retrieveReportData = new RetrieveReportData(systemId, curator, testSystem, mockServiceStatisticsAccess, corridorServiceMock)

      val stats = StatsDuration("20120401", TimeZone.getTimeZone("Europe/Amsterdam"))
      //configure ZooKeeper
      val laneStats: List[LaneInfo] = List(LaneInfo(Lane("strook123", "Strook 1", "opritmaarsen", systemId, 10.1, 20.2), 1000),
        LaneInfo(Lane("strook321", "Strook 1", "afslagbreukelen", systemId, 40.1, 30.2), 900))

      testStorage.createConfiguration1(systemId, stats)
      val sectionData = retrieveReportData.retrieveSystemData.getSectionData("a")
      sectionData.zkSection.id must be("a")

      val systemData = retrieveReportData.retrieveSystemData.getSystemData
      log.info("systemData: " + systemData)
      systemData.corridors.size must be(3)
      val corridorId: String = systemData.corridors.head.corridor.id

      // schedules per corridor
      val schedules: Map[Int, Seq[CorridorSpeedSchedule]] = Map(
        1 -> Seq(
          CorridorSpeedSchedule(corridorId, 1, 100, stats.start, stats.start + 5 * oneHour),
          CorridorSpeedSchedule(corridorId, 1, 130, stats.start + 5 * oneHour, stats.tomorrow.start - 1)))

      // system states
      StateService.create(systemId, corridorId, ZkState("EnforceOn", 11111L, "user1", "Begin Universe"), curator)
      var result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("StandBy", stats.start - 1000, "user1", "Previous state"), result.get.version, curator)
      result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("EnforceOn", stats.start + 3 * oneMinute, "user2", "Begin meetdag"), result.get.version, curator)
      result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("StandBy", stats.start + 3 * oneHour, "user3", "Maintain"), result.get.version, curator)
      result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("Failure", stats.start + 13 * oneHour, "user4", "Continue"), result.get.version, curator)
      result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("StandBy", stats.tomorrow.start, "user4", "Go home"), result.get.version, curator)

      val systemStates = StateService.getStateLogHistory(systemId, corridorId, stats.start, stats.tomorrow.start, curator)
      log.info("stateLogHistory: " + systemStates)
      systemStates.size must be(4)
      // corridor alerts
      AlertService.create(systemId, corridorId, ZkAlert("qn-01", "", "Camera 5 uitgevallen", 1111122L, ConfigType.System, 0.05f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-02", "", "Camera 5 is terug", stats.start - 1, ConfigType.System, 0.15f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-03", "", "Lunch", stats.start, ConfigType.System, 0.25f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-04", "", "Feest", stats.start + 1000 * 60 * 60 * 4 + 23000, ConfigType.System, 0.288f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-05", "", "Onderhoud", stats.tomorrow.start, ConfigType.System, 0.32f), curator)

      val systemAlerts = AlertService.getSystemAlerts(curator, systemId, corridorId, stats.tomorrow.start)
      systemAlerts.size must be(4)
      //
      val events = List(LogEntry(1L, 3L, 5L, System.currentTimeMillis(), SystemEvent("com1", System.currentTimeMillis(), systemId, "ALARM", "user1", Some("Relax"))),
        LogEntry(1L, 4L, 6L, System.currentTimeMillis(), SystemEvent("com2", System.currentTimeMillis(), systemId, "ALARM", "user1", None)))
      //MTM
      val mtmList = List(MtmSpeedSetting(stats.start, "a020001", "100"), MtmSpeedSetting(stats.start + 100000, "a020001", "onbepaald"))
      val allMtm = new MtmSpeedSettings(mtmList, stats)
      val mtmStorePath = Path(Paths.Systems.getRuntimeMtmInfoPath(systemId, stats))
      curator.put(mtmStorePath, allMtm)

      //create metrics
      val export1: ExportViolationsStatistics = ExportViolationsStatistics(1, 100, 301, 311, 321, 331, 341, 351, 361, 371)
      val export2: ExportViolationsStatistics = ExportViolationsStatistics(2, 100, 301, 311, 321, 331, 341, 351, 361, 371)
      val export3: ExportViolationsStatistics = ExportViolationsStatistics(3, 100, 301, 311, 321, 331, 341, 351, 361, 371)
      val dayStats = DayViolationsStatistics("A2", stats, Seq(export1, export2, export3))

      val stat0 = SectionControlStatistics(StatisticsPeriod(stats, stats.tomorrow.start - 1, stats.start + 12 * oneHour),
        MatcherCorridorConfig("A2", "1", 1, "9547", "9542", 2983, 1000, 3600000), 1000, 990, 10, 2, 90.1, 180, laneStats)
      val stat1 = SectionControlStatistics(StatisticsPeriod(stats, stats.start + 12 * oneHour, stats.start),
        MatcherCorridorConfig("A2", "1", 1, "9547", "9542", 2983, 1000, 3600000), 1000, 990, 10, 2, 90.1, 180, laneStats)
      val stat2 = RedLightStatistics(StatisticsPeriod(stats, 1342173624653L, 1342173324653L),
        1000, 990, 1120.1, 1180, laneStats)
      val stat3 = SpeedFixedStatistics(StatisticsPeriod(stats, 1342173624653L, 1342173324653L),
        1000, 990, 90.1, 180, laneStats)
      mockServiceStatisticsAccess.setSectionControlStatistics(Seq(stat0, stat1))
      mockServiceStatisticsAccess.setRedLightStatistics(Seq(stat2))
      mockServiceStatisticsAccess.setSpeedFixedStatistics(Seq(stat3))

      val serviceResults = retrieveReportData.readServiceResults(stats, systemData.corridors)
      serviceResults.size must be(3)
      var list = serviceResults.get(1).get
      list.size must be(2)
      list.contains(stat0) must be(true)
      list.contains(stat1) must be(true)

      list = serviceResults.get(2).get
      list.size must be(1)
      list.head must be(stat2)
      list = serviceResults.get(3).get
      list.size must be(1)
      list.head must be(stat3)

      val allCorridorStatistics = MetricsGenerator.calculate(dayStats, serviceResults, schedules)
      val allStats = allCorridorStatistics.corridorData
      val scStats = allStats.collect { case s: SectionControlCorridorStatistics ⇒ s }
      scStats.size must be(3)

      val _NoneStats = scStats.find(s ⇒ s.corridorId == 1 && s.speedLimit == None).get // must be()
      val _100Stats = scStats.find(s ⇒ s.corridorId == 1 && s.speedLimit == Some(100)).get

      _100Stats must be(SectionControlCorridorStatistics(Some(100), 1, laneStats, export1, 1000, 990, 10, 2, 90.1, 180))
      _NoneStats must be(SectionControlCorridorStatistics(None, 1, LaneInfo.coalesce(laneStats ++ laneStats).toList, export1.copy(speedLimit = 0), 2000, 1980, 20, 4, 90.1, 180))

      val rlStats = allStats.collect { case s: RedLightCorridorStatistics ⇒ s }
      rlStats.size must be(1)
      rlStats(0) must be(RedLightCorridorStatistics(2, laneStats, export2, 1000, 990, 1120.1, 1180))

      val sfStats = allStats.collect { case s: SpeedFixedCorridorStatistics ⇒ s }
      sfStats.size must be(1)
      sfStats(0) must be(SpeedFixedCorridorStatistics(3, laneStats, export3, 1000, 990, 90.1, 180))

      //create report
      val dayReportBuilder: DayReportBuilder = retrieveReportData.create(stats, events.iterator, dayStats, DayReportVersion.HHM_V40, Some(dayReportConfig))

      //check the result
      dayReportBuilder.filenamePlain must be("A2_1_Amsterdam_20120401.xml")
      dayReportBuilder.filenameZip must be("A2_1_Amsterdam_20120401.xml.gz")
      dayReportBuilder.key must be(StatsDuration("20120401", TimeZone.getTimeZone("Europe/Amsterdam")))

      val factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
      val xsdStream = getClass.getResourceAsStream("/HHM-dagrapport-v4.0.xsd")
      val schema = factory.newSchema(new StreamSource(xsdStream))
      val source = new ByteArrayInputStream(dayReportBuilder.output)

      val xml = new SchemaAwareFactoryAdapter(schema).load(source)

      xml.child.size must be(9)

      val entries = xml \ "snelheid" \ "handhaaflog" \ "handhaaflog-entry"
      val lastTimeEntry = entries.last \ "@tijd"
      lastTimeEntry.text must be("2012-04-01T23:59:59")
    }

    "create 3 types in document 5" in {
      val mockServiceStatisticsAccess = new MockServiceStatisticsRepositoryAccess()
      val retrieveReportData = new RetrieveReportData(systemId, curator, testSystem, mockServiceStatisticsAccess, corridorServiceMock)

      val stats = StatsDuration("20120401", TimeZone.getTimeZone("Europe/Amsterdam"))
      //configure ZooKeeper
      val laneStats: List[LaneInfo] = List(LaneInfo(Lane("strook123", "Strook 1", "opritmaarsen", systemId, 10.1, 20.2), 1000),
        LaneInfo(Lane("strook321", "Strook 1", "afslagbreukelen", systemId, 40.1, 30.2), 900))

      testStorage.createConfiguration1(systemId, stats)
      val sectionData = retrieveReportData.retrieveSystemData.getSectionData("a")
      sectionData.zkSection.id must be("a")

      val systemData = retrieveReportData.retrieveSystemData.getSystemData
      log.info("systemData: " + systemData)
      systemData.corridors.size must be(3)
      val corridorId: String = systemData.corridors.head.corridor.id

      // schedules per corridor
      val schedules: Map[Int, Seq[CorridorSpeedSchedule]] = Map(
        1 -> Seq(
          CorridorSpeedSchedule(corridorId, 1, 100, stats.start, stats.start + 5 * oneHour),
          CorridorSpeedSchedule(corridorId, 1, 130, stats.start + 5 * oneHour, stats.tomorrow.start - 1)))

      // system states
      StateService.create(systemId, corridorId, ZkState("EnforceOn", 11111L, "user1", "Begin Universe"), curator)
      var result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("StandBy", stats.start - 1000, "user1", "Previous state"), result.get.version, curator)
      result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("EnforceOn", stats.start + 3 * oneMinute, "user2", "Begin meetdag"), result.get.version, curator)
      result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("StandBy", stats.start + 3 * oneHour, "user3", "Maintain"), result.get.version, curator)
      result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("Failure", stats.start + 13 * oneHour, "user4", "Continue"), result.get.version, curator)
      result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("StandBy", stats.tomorrow.start, "user4", "Go home"), result.get.version, curator)

      val systemStates = StateService.getStateLogHistory(systemId, corridorId, stats.start, stats.tomorrow.start, curator)
      log.info("stateLogHistory: " + systemStates)
      systemStates.size must be(4)
      // corridor alerts
      AlertService.create(systemId, corridorId, ZkAlert("qn-01", "", "Camera 5 uitgevallen", 1111122L, ConfigType.System, 0.05f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-02", "", "Camera 5 is terug", stats.start - 1, ConfigType.System, 0.15f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-03", "", "Lunch", stats.start, ConfigType.System, 0.25f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-04", "", "Feest", stats.start + 1000 * 60 * 60 * 4 + 23000, ConfigType.System, 0.288f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-05", "", "Onderhoud", stats.tomorrow.start, ConfigType.System, 0.32f), curator)

      val systemAlerts = AlertService.getSystemAlerts(curator, systemId, corridorId, stats.tomorrow.start)
      systemAlerts.size must be(4)
      //
      val events = List(
        LogEntry(1L, 3L, 5L, System.currentTimeMillis(), SystemEvent("com1", System.currentTimeMillis(), systemId, "ALARM", "user1", Some("Relax"))),
        LogEntry(1L, 4L, 6L, System.currentTimeMillis(), SystemEvent("com2", System.currentTimeMillis(), systemId, "ALARM", "user1", None)),
        LogEntry(1L, 5L, 7L, System.currentTimeMillis(), SystemEvent("web", System.currentTimeMillis(), systemId, "WEB", "user1", Some("Yooo"))),
        LogEntry(1L, 5L, 8L, System.currentTimeMillis(), SystemEvent("1234-camera", System.currentTimeMillis(), systemId, "JAI-NOT-GOOD-ERROR", "user1", Some("camera niet in orde"))),
        LogEntry(1L, 5L, 18L, System.currentTimeMillis(), SystemEvent("1234-camera", System.currentTimeMillis(), systemId, "JAI-SOME-INFORMAITONAL", "user1", Some("camera, setting gewijzigd "))),
        LogEntry(1L, 5L, 28L, System.currentTimeMillis(), SystemEvent("certificaat.jar", System.currentTimeMillis(), systemId, "Alarm", "user1", Some("certificaat ongeldig "))),
        LogEntry(1L, 5L, 58L, System.currentTimeMillis(), SystemEvent("schedulemonitor", System.currentTimeMillis(), systemId, "enforceon", "user1", Some("handhaven AAN "))),
        LogEntry(1L, 5L, 59L, System.currentTimeMillis(), SystemEvent("schedulemonitor", System.currentTimeMillis(), systemId, "enforceoff", "user1", Some("handhaven UIT "))),
        LogEntry(1L, 5L, 9L, System.currentTimeMillis(), SystemEvent("sitebuffer", System.currentTimeMillis(), systemId, "Failure", "user1", Some("geopend"))),
        LogEntry(1L, 5L, 229L, System.currentTimeMillis(), SystemEvent("sitebuffer", System.currentTimeMillis(), systemId, "Failure", "user1", Some("scheefstand"))),
        LogEntry(1L, 6L, 10L, System.currentTimeMillis(), SystemEvent("selftest", System.currentTimeMillis(), systemId, "SELFTEST-SUCCESS", "user1", Some("Yooo"))))

      //create function starts
      //redlight
      val exclude = new ExcludeLogLayer(curator)
      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start + 1000 * 60 * 60 * 7,
        corridorId = Some(2),
        sequenceNumber = 1,
        suspend = false,
        suspensionReason = Some("Service on")))
      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start + 1000 * 60 * 60 * 8,
        corridorId = Some(2),
        sequenceNumber = 1,
        suspend = true,
        suspensionReason = Some("Service off")))
      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start + 1000 * 60 * 60 * 10,
        corridorId = Some(2),
        sequenceNumber = 1,
        suspend = false,
        suspensionReason = Some("Service on")))
      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start + 1000 * 60 * 60 * 12,
        corridorId = Some(2),
        sequenceNumber = 1,
        suspend = true,
        suspensionReason = Some("Service off")))
      //create metrics

      val export1: ExportViolationsStatistics = ExportViolationsStatistics(1, 100, 301, 311, 321, 331, 341, 351, 361, 371)
      val export2: ExportViolationsStatistics = ExportViolationsStatistics(2, 100, 301, 311, 321, 331, 341, 351, 361, 371)
      val export3: ExportViolationsStatistics = ExportViolationsStatistics(3, 100, 301, 311, 321, 331, 341, 351, 361, 371)
      val dayStats = DayViolationsStatistics("A2", stats, Seq(export1, export2, export3))

      val stat1 = SectionControlStatistics(StatisticsPeriod(stats, stats.tomorrow.start - 1, stats.start),
        MatcherCorridorConfig("A2", "1", 1, "9547", "9542", 2983, 1000, 3600000), 1000, 990, 10, 2, 90.1, 180, laneStats)
      val stat2 = RedLightStatistics(StatisticsPeriod(stats, 1342173624653L, 1342173324653L),
        1000, 990, 1120.1, 1180, laneStats)
      val stat3 = SpeedFixedStatistics(StatisticsPeriod(stats, 1342173624653L, 1342173324653L),
        1000, 990, 90.1, 180, laneStats)
      mockServiceStatisticsAccess.setSectionControlStatistics(Seq(stat1))
      mockServiceStatisticsAccess.setRedLightStatistics(Seq(stat2))
      mockServiceStatisticsAccess.setSpeedFixedStatistics(Seq(stat3))

      val serviceResults = retrieveReportData.readServiceResults(stats, systemData.corridors)
      serviceResults.size must be(3)
      var list = serviceResults.get(1).get
      list.size must be(1)
      list.head must be(stat1)
      list = serviceResults.get(2).get
      list.size must be(1)
      list.head must be(stat2)
      list = serviceResults.get(3).get
      list.size must be(1)
      list.head must be(stat3)

      val allCorridorsStatistics = MetricsGenerator.calculate(dayStats, serviceResults, schedules)
      allCorridorsStatistics.corridorData.size must be(4)
      //allCorridorsStatistics.corridorData(0) must be(SectionControlCorridorStatistics(Some(100), 1, laneStats, export1, 1000, 990, 10, 2, 90.1, 180))
      //allCorridorsStatistics.corridorData(1) must be(RedLightCorridorStatistics(2, laneStats, export2, 1000, 990, 1120.1, 1180))
      //allCorridorsStatistics.corridorData(2) must be(SpeedFixedCorridorStatistics(3, laneStats, export3, 1000, 990, 90.1, 180))

      val allStats = allCorridorsStatistics.corridorData
      val scStats = allStats.collect { case s: SectionControlCorridorStatistics ⇒ s }
      log.info("scStats: " + scStats)
      scStats.size must be(2)

      val _NoneStats = scStats.find(s ⇒ s.corridorId == 1 && s.speedLimit == None).get // must be()
      val _100Stats = scStats.find(s ⇒ s.corridorId == 1 && s.speedLimit == Some(100)).get

      _100Stats must be(SectionControlCorridorStatistics(Some(100), 1, laneStats, export1, 1000, 990, 10, 2, 90.1, 180))
      _NoneStats must be(SectionControlCorridorStatistics(None, 1, laneStats, export1.copy(speedLimit = 0), 1000, 990, 10, 2, 90.1, 180))

      val rlStats = allStats.collect { case s: RedLightCorridorStatistics ⇒ s }
      rlStats.size must be(1)
      rlStats(0) must be(RedLightCorridorStatistics(2, laneStats, export2, 1000, 990, 1120.1, 1180))

      val sfStats = allStats.collect { case s: SpeedFixedCorridorStatistics ⇒ s }
      sfStats.size must be(1)
      sfStats(0) must be(SpeedFixedCorridorStatistics(3, laneStats, export3, 1000, 990, 90.1, 180))

      //create report
      val dayReportBuilder = retrieveReportData.create(stats, events.iterator, dayStats, DayReportVersion.HHM_V50, None)

      //check the result
      dayReportBuilder.filenamePlain must be("a002_2012-04-01.xml")
      dayReportBuilder.filenameZip must be("a002_2012-04-01.gz")
      dayReportBuilder.key must be(StatsDuration("20120401", TimeZone.getTimeZone("Europe/Amsterdam")))

      val factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
      val xsdStream = getClass.getResourceAsStream("/HHM-dagrapport-v5.0.xsd")
      val schema = factory.newSchema(new StreamSource(xsdStream))
      val source = new ByteArrayInputStream(dayReportBuilder.output)

      val xml = new SchemaAwareFactoryAdapter(schema).load(source)

      xml.child.size must be(10)
    }

    "create tijdbeschikbaar" in {
      val mockServiceStatisticsAccess = new MockServiceStatisticsRepositoryAccess()
      val retrieveReportData = new RetrieveReportData(systemId, curator, testSystem, mockServiceStatisticsAccess, corridorServiceMock)

      val stats = StatsDuration("20120401", TimeZone.getTimeZone("Europe/Amsterdam"))

      //corridor
      val corridor = ZkCorridor(id = "S1",
        name = "S1-name",
        roadType = 0,
        serviceType = ServiceType.SectionControl,
        caseFileType = ZkCaseFileType.HHMVS40,
        isInsideUrbanArea = false,
        dynamaxMqId = "",
        approvedSpeeds = Seq(),
        pshtm = ZkPSHTMIdentification(useYear = false,
          useMonth = true,
          useDay = false,
          useReportingOfficerId = false,
          useNumberOfTheDay = true,
          useLocationCode = false,
          useHHMCode = false,
          useTypeViolation = false,
          useFixedCharacter = false,
          fixedCharacter = "T"),
        info = ZkCorridorInfo(corridorId = 1,
          locationCode = "2",
          reportId = "",
          reportHtId = "",
          reportPerformance = true,
          locationLine1 = ""),
        startSectionId = "sec1",
        endSectionId = "sec1",
        allSectionIds = Seq("sec1"),
        direction = ZkDirection(directionFrom = "",
          directionTo = ""),
        radarCode = 4,
        roadCode = 0,
        dutyType = "",
        deploymentCode = "",
        codeText = "")
      CorridorService.createCorridor(curator, systemId, corridor)

      // system states
      val systemStates = Map(CorridorIdMapping("S1", 1) -> Seq(
        new ZkState(state = "EnforceOn", timestamp = 11111L, userId = "user", reason = "Begin Universe"),
        new ZkState("StandBy", stats.start - 1000, "user", "Previous state"),
        new ZkState("EnforceOn", stats.start + 1000 * 60 * 3, "user", "Begin meetdag"),
        new ZkState("StandBy", stats.start + 1000 * 60 * 60 * 3, "user", "Maintain"),
        new ZkState("Failure", stats.start + 1000 * 60 * 60 * 13, "user", "Continue"),
        new ZkState("StandBy", stats.tomorrow.start, "user", "Go home")))
      // system alerts
      val alert = new ZkAlertHistory(path = "A2-X1-R1-Camera", alertType = "", configType = "Lane", message = "current event", reductionFactor = 0.05f, startTime = stats.start + 1000 * 60 * 60 * 12, endTime = stats.start + 1000 * 60 * 60 * 13)
      val systemAlerts = Map(CorridorIdMapping("S1", 1) -> Seq(
        new ZkAlertHistory(path = "qn-01",
          alertType = "",
          message = "Camera 5 uitgevallen",
          startTime = alert.startTime,
          endTime = alert.endTime,
          configType = ConfigType.System.toString,
          reductionFactor = alert.reductionFactor)))
      //create function starts
      //redlight
      val exclude = new ExcludeLogLayer(curator)
      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start + 1000 * 60 * 60 * 7,
        corridorId = Some(2),
        sequenceNumber = 1,
        suspend = false,
        suspensionReason = Some("Service on")))
      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start + 1000 * 60 * 60 * 8,
        corridorId = Some(2),
        sequenceNumber = 1,
        suspend = true,
        suspensionReason = Some("Service off")))
      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start + 1000 * 60 * 60 * 10,
        corridorId = Some(2),
        sequenceNumber = 1,
        suspend = false,
        suspensionReason = Some("Service on")))
      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start + 1000 * 60 * 60 * 12,
        corridorId = Some(2),
        sequenceNumber = 1,
        suspend = true,
        suspensionReason = Some("Service off")))
      //create calibration
      exclude.saveEvent(systemId, new SuspendEvent(componentId = "SelfTest",
        effectiveTimestamp = stats.start + 1000 * 60 * 60 * 10,
        corridorId = Some(1),
        sequenceNumber = 1,
        suspend = true,
        suspensionReason = Some("Service on")))
      exclude.saveEvent(systemId, new SuspendEvent(componentId = "SelfTest",
        effectiveTimestamp = stats.start + 1000 * 60 * 60 * 12,
        corridorId = Some(1),
        sequenceNumber = 1,
        suspend = false,
        suspensionReason = Some("Service off")))

      val calibration = retrieveReportData.retrieveSystemData.getCalibrationFailurePeriods(stats)
      calibration.size must be(1)
      calibration.values.head must be(List(new Period(stats.start + 1000 * 60 * 60 * 10, stats.start + 1000 * 60 * 60 * 12)))

      val zkSystem = new ZkSystem(id = "a2",
        name = "A2",
        title = "A2",
        mtmRouteId = "",
        violationPrefixId = "a002",
        location = ZkSystemLocation(
          description = "A2 Utrecht Amsterdam",
          systemLocation = "Amsterdam",
          region = "Randstad",
          viewingDirection = Some("zuid oost"),
          roadNumber = "2",
          roadPart = "Links"),
        serialNumber = SerialNumber("777"),
        orderNumber = 1,
        maxSpeed = 100,
        compressionFactor = 4,
        retentionTimes = ZkSystemRetentionTimes(
          nrDaysKeepTrafficData = 11,
          nrDaysKeepViolations = 5,
          nrDaysRemindCaseFiles = 3),
        pardonTimes = ZkSystemPardonTimes(pardonTimeEsa_secs = 5, pardonTimeTechnical_secs = 3))

      val corrInfo = new ZkCorridorInfo(corridorId = 1,
        locationCode = "1234",
        reportId = "TCS A2 Links  39.7 - 54.9",
        reportHtId = "A020011",
        reportPerformance = true,
        locationLine1 = "loc 1",
        locationLine2 = Some("loc 2"))
      val redServiceConfig = new RedLightConfig(
        flgFileName = Some("roodlicht.flg"),
        dynamicEnforcement = false,
        factNumber = "1234",
        pardonRedTime = 1000,
        minimumYellowTime = 1000)

      val corridor_1 = new CorridorConfig(id = "1",
        serviceType = ServiceType.RedLight,
        name = "1",
        dynamaxMqId = "",
        approvedSpeeds = Seq(),
        pshtm = null,
        info = corrInfo,
        startSectionId = "a",
        endSectionId = "a",
        allSectionIds = Seq("a"),
        roadType = 1,
        isInsideUrbanArea = false,
        serviceConfig = redServiceConfig)

      val gantry = new GantryData(id = "gantry1",
        name = "gantry 1",
        hectometer = "10HM",
        lanes = Seq(
          LaneData(id = "lane1", name = "lane1", seqName = Some("RD"),
            seqNumber = Some("1"), bpsLaneId = Some("1 HR R- R"))))

      val zkSection = new ZkSection(id = "a",
        systemId = "a2",
        name = "A",
        length = 2983.0,
        startGantryId = "opritmaarsen",
        endGantryId = "afslagbreukelen",
        matrixBoards = Seq("989"),
        msiBlackList = Nil)

      val section1 = new SectionData(zkSection = zkSection,
        inGantry = gantry,
        outGantry = gantry)

      def createZkDailyReportConfig(): ZkDailyReportConfig = {
        val dailyReportConfigSpeedEntry100 = DailyReportConfigSpeedEntry("100", "TCS UT A2 R-1 (100 km/h)", "a0021001")
        val dailyReportConfigSpeedEntry130 = DailyReportConfigSpeedEntry("130", "TCS UT A2 R-1 (130 km/h)", "a0021301")
        val dailyReportConfigSpeedEntryTotal = DailyReportConfigSpeedEntry("total", "TCS UT A2 R-1 (totaal)", "a0021001, a0021301 (totaal)")
        val ignoreStatisticsOfLane1 = IgnoreStatisticsOfLane("A2", "E1", "E1R1")
        val ignoreStatisticsOfLane2 = IgnoreStatisticsOfLane("A2", "E1", "E1R2")
        val ignoreStatisticsOfLanes = List(ignoreStatisticsOfLane1, ignoreStatisticsOfLane2)
        val speedEntries = List(dailyReportConfigSpeedEntry100, dailyReportConfigSpeedEntry130, dailyReportConfigSpeedEntryTotal)
        ZkDailyReportConfig(ignoreStatisticsOfLanes, speedEntries)
      }

      val corData = new CorridorData(corridor = corridor_1,
        startSection = section1,
        endSection = section1,
        createZkDailyReportConfig())

      val lane1 = new Lane("lane1", "lane1", "gantry1", "a2", 52F, 5F)
      val laneInfo = new LaneInfo(lane1, 200)
      val redCorStat = new RedLightCorridorStatistics(
        corridorId = 1,
        laneStats = List(laneInfo),
        violations = new ExportViolationsStatistics(corridorId = 1,
          speedLimit = 100,
          auto = 90,
          manual = 10,
          mobi = 0,
          mobiDoNotProcess = 0,
          mtmPardon = 0,
          doublePardon = 0,
          otherPardon = 0,
          manualAccordingToSpec = 2),
        nrRegistrations = 200,
        nrWithRedLight = 100,
        averageRedLight = 1000D,
        highestRedLight = 2000)

      val period = new Period(from = stats.start, to = stats.tomorrow.start)

      val redMetrics = new RedMetrics(zkSystem, corData, Seq(redCorStat), Seq(period), systemStates.values.head, Seq(alert), Seq(20))

      val system = MetricsGenerator.createSystemMetrics(stats,
        systemStates,
        systemAlerts,
        calibration,
        Map(CorridorIdKey(redCorStat.corridorId) -> redMetrics))

      //is available from 00 to 10
      //10 to 12 is NOT excluded due to calibration, because state is STANDBY
      //12 to 13 due to alert
      //13 to 24 due to failure state
      system.timeAvailable must be("PT12H0M")
      system.autoRatio must be(0.9)

    }

    "create 3 types in document 6" in {
      val mockServiceStatisticsAccess = new MockServiceStatisticsRepositoryAccess()
      val retrieveReportData = new RetrieveReportData(systemId, curator, testSystem, mockServiceStatisticsAccess, corridorServiceMock)

      val stats = StatsDuration("20120401", TimeZone.getTimeZone("Europe/Amsterdam"))
      //configure ZooKeeper
      val laneStats: List[LaneInfo] = List(LaneInfo(Lane("strook123", "Strook 1", "opritmaarsen", systemId, 10.1, 20.2), 1000),
        LaneInfo(Lane("strook321", "Strook 1", "afslagbreukelen", systemId, 40.1, 30.2), 900))

      testStorage.createConfiguration2(systemId, stats)
      val sectionData = retrieveReportData.retrieveSystemData.getSectionData("a")
      sectionData.zkSection.id must be("a")

      // schedules per corridor
      val schedules: Map[Int, Seq[CorridorSpeedSchedule]] = Map(1 -> Seq(CorridorSpeedSchedule("test", 1, 100, stats.start, stats.tomorrow.start)))

      val systemData = retrieveReportData.retrieveSystemData.getSystemData
      systemData.corridors.size must be(3)
      val corridorId = systemData.corridors.head.corridor.id
      // system states
      StateService.create(systemId, corridorId, ZkState("EnforceOn", 11111L, "user1", "Begin Universe"), curator)

      var result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("StandBy", stats.start - 1000, "user1", "Previous state"), result.get.version, curator)
      result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("EnforceOn", stats.start + 1000 * 60 * 3, "user2", "Begin meetdag"), result.get.version, curator)
      result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("StandBy", stats.start + 1000 * 60 * 60 * 3, "user3", "Maintain"), result.get.version, curator)
      result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("Failure", stats.start + 1000 * 60 * 60 * 13, "user4", "Continue"), result.get.version, curator)
      result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("StandBy", stats.tomorrow.start, "user4", "Go home"), result.get.version, curator)

      val systemStates = StateService.getStateLogHistory(systemId, corridorId, stats.start, stats.tomorrow.start, curator)
      systemStates.size must be(4)
      // system alerts
      AlertService.create(systemId, corridorId, ZkAlert("qn-01", "", "Camera 5 uitgevallen", 1111122L, ConfigType.System, 0.05f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-02", "", "Camera 5 is terug", stats.start - 1, ConfigType.System, 0.15f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-03", "", "Lunch", stats.start, ConfigType.System, 0.25f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-04", "", "Feest", stats.start + 1000 * 60 * 60 * 4 + 23000, ConfigType.System, 0.288f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-05", "", "Onderhoud", stats.tomorrow.start, ConfigType.System, 0.32f), curator)

      val systemAlerts = AlertService.getSystemAlerts(curator, systemId, corridorId, stats.tomorrow.start)
      systemAlerts.size must be(4)
      //
      val events = List(
        LogEntry(1L, 3L, 5L, System.currentTimeMillis(), SystemEvent("com1", System.currentTimeMillis(), systemId, "ALARM", "user1", Some("Relax"))),
        LogEntry(1L, 4L, 6L, System.currentTimeMillis(), SystemEvent("com2", System.currentTimeMillis(), systemId, "ALARM", "user1", None)),
        LogEntry(1L, 5L, 7L, System.currentTimeMillis(), SystemEvent("web", System.currentTimeMillis(), systemId, "WEB", "user1", Some("Yooo"))),
        LogEntry(1L, 5L, 8L, System.currentTimeMillis(), SystemEvent("1234-camera", System.currentTimeMillis(), systemId, "JAI-NOT-GOOD-ERROR", "user1", Some("camera niet in orde"))),
        LogEntry(1L, 5L, 18L, System.currentTimeMillis(), SystemEvent("1234-camera", System.currentTimeMillis(), systemId, "JAI-SOME-INFORMAITONAL", "user1", Some("camera, setting gewijzigd "))),
        LogEntry(1L, 5L, 28L, System.currentTimeMillis(), SystemEvent("certificaat.jar", System.currentTimeMillis(), systemId, "Alarm", "user1", Some("certificaat ongeldig "))),
        LogEntry(1L, 5L, 58L, System.currentTimeMillis(), SystemEvent("schedulemonitor", System.currentTimeMillis(), systemId, "enforceon", "user1", Some("handhaven AAN "))),
        LogEntry(1L, 5L, 59L, System.currentTimeMillis(), SystemEvent("schedulemonitor", System.currentTimeMillis(), systemId, "enforceoff", "user1", Some("handhaven UIT "))),
        LogEntry(1L, 5L, 9L, System.currentTimeMillis(), SystemEvent("sitebuffer", System.currentTimeMillis(), systemId, "Failure", "user1", Some("geopend"))),
        LogEntry(1L, 5L, 229L, System.currentTimeMillis(), SystemEvent("sitebuffer", System.currentTimeMillis(), systemId, "Failure", "user1", Some("leaning"))),
        LogEntry(1L, 6L, 10L, System.currentTimeMillis(), SystemEvent("selftest", System.currentTimeMillis(), systemId, "SELFTEST-SUCCESS", "user1", Some("Yooo"))))

      //create function starts
      //redlight
      val exclude = new ExcludeLogLayer(curator)
      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start + 1000 * 60 * 60 * 7,
        corridorId = Some(2),
        sequenceNumber = 1,
        suspend = false,
        suspensionReason = Some("Service on")))
      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start + 1000 * 60 * 60 * 8,
        corridorId = Some(2),
        sequenceNumber = 1,
        suspend = true,
        suspensionReason = Some("Service off")))
      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start + 1000 * 60 * 60 * 10,
        corridorId = Some(2),
        sequenceNumber = 1,
        suspend = false,
        suspensionReason = Some("Service on")))
      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start + 1000 * 60 * 60 * 12,
        corridorId = Some(2),
        sequenceNumber = 1,
        suspend = true,
        suspensionReason = Some("Service off")))

      //create metrics

      val export1: ExportViolationsStatistics = ExportViolationsStatistics(1, 100, 301, 311, 321, 331, 341, 351, 361, 371)
      val export2: ExportViolationsStatistics = ExportViolationsStatistics(2, 100, 301, 311, 321, 331, 341, 351, 361, 371)
      val export3: ExportViolationsStatistics = ExportViolationsStatistics(3, 100, 301, 311, 321, 331, 341, 351, 361, 371)
      val dayStats = DayViolationsStatistics("A2", stats, Seq(export1, export2, export3))

      val stat1 = SectionControlStatistics(StatisticsPeriod(stats, stats.tomorrow.start - 1, stats.start),
        MatcherCorridorConfig("A2", "1", 1, "9547", "9542", 2983, 1000, 3600000), 1000, 990, 10, 2, 90.1, 180, laneStats)
      val stat2 = RedLightStatistics(StatisticsPeriod(stats, 1342173624653L, 1342173324653L),
        1000, 990, 1120.1, 1180, laneStats)
      val stat3 = SpeedFixedStatistics(StatisticsPeriod(stats, 1342173624653L, 1342173324653L),
        1000, 990, 90.1, 180, laneStats)
      mockServiceStatisticsAccess.setSectionControlStatistics(Seq(stat1))
      mockServiceStatisticsAccess.setRedLightStatistics(Seq(stat2))
      mockServiceStatisticsAccess.setSpeedFixedStatistics(Seq(stat3))

      val serviceResults = retrieveReportData.readServiceResults(stats, systemData.corridors)
      serviceResults.size must be(3)
      var list = serviceResults.get(1).get
      list.size must be(1)
      list.head must be(stat1)
      list = serviceResults.get(2).get
      list.size must be(1)
      list.head must be(stat2)
      list = serviceResults.get(3).get
      list.size must be(1)
      list.head must be(stat3)

      val allCorridorStatistics = MetricsGenerator.calculate(dayStats, serviceResults, schedules)
      val allStats = allCorridorStatistics.corridorData
      val scStats = allStats.collect { case s: SectionControlCorridorStatistics ⇒ s }
      scStats.size must be(2)

      val _NoneStats = scStats.find(s ⇒ s.corridorId == 1 && s.speedLimit == None).get // must be()
      val _100Stats = scStats.find(s ⇒ s.corridorId == 1 && s.speedLimit == Some(100)).get

      _100Stats must be(SectionControlCorridorStatistics(Some(100), 1, laneStats, export1, 1000, 990, 10, 2, 90.1, 180))
      _NoneStats must be(SectionControlCorridorStatistics(None, 1, laneStats, export1.copy(speedLimit = 0), 1000, 990, 10, 2, 90.1, 180))

      val rlStats = allStats.collect { case s: RedLightCorridorStatistics ⇒ s }
      rlStats.size must be(1)
      rlStats(0) must be(RedLightCorridorStatistics(2, laneStats, export2, 1000, 990, 1120.1, 1180))

      val sfStats = allStats.collect { case s: SpeedFixedCorridorStatistics ⇒ s }
      sfStats.size must be(1)
      sfStats(0) must be(SpeedFixedCorridorStatistics(3, laneStats, export3, 1000, 990, 90.1, 180))

      //MsiBlanks
      val blankMsiList = List(PossibleDefectMSI(1, "a", "a"), PossibleDefectMSI(1, "b", "b"))
      val msiBlankEvent = MsiBlankEvent(1, blankMsiList, stats)
      val msiBlankEventStorePath = Path(Paths.Systems.getRuntimeMsiBlanksPath(systemId, stats))
      curator.put(msiBlankEventStorePath, msiBlankEvent)

      //create report
      val dayReportBuilder = retrieveReportData.create(stats, events.iterator, dayStats, DayReportVersion.HHM_V60, None)

      //check the result
      dayReportBuilder.filenamePlain must be("a002_20120401.xml")
      dayReportBuilder.filenameZip must be("a002_20120401.gz")
      dayReportBuilder.key must be(StatsDuration("20120401", TimeZone.getTimeZone("Europe/Amsterdam")))

      val factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
      val xsdStream = getClass.getResourceAsStream("/HHM-dagrapport-v6.0.xsd")
      val schema = factory.newSchema(new StreamSource(xsdStream))
      val source = new ByteArrayInputStream(dayReportBuilder.output)

      val xml = new SchemaAwareFactoryAdapter(schema).load(source)

      xml.child.size must be(7)
    }

    "create a 'begin meetperiode' entry at 00:00:00 in speed and redlight handhaven entry" in {
      val mockServiceStatisticsAccess = new MockServiceStatisticsRepositoryAccess()
      val retrieveReportData = new RetrieveReportData(systemId, curator, testSystem, mockServiceStatisticsAccess, corridorServiceMock)

      val stats = StatsDuration("20120401", TimeZone.getTimeZone("Europe/Amsterdam"))
      //configure ZooKeeper
      val laneStats: List[LaneInfo] = List(LaneInfo(Lane("strook123", "Strook 1", "opritmaarsen", systemId, 10.1, 20.2), 1000),
        LaneInfo(Lane("strook321", "Strook 1", "afslagbreukelen", systemId, 40.1, 30.2), 900))

      testStorage.createConfiguration3(systemId, stats)
      val sectionData = retrieveReportData.retrieveSystemData.getSectionData("a")
      sectionData.zkSection.id must be("a")

      // schedules per corridor
      val schedules: Map[Int, Seq[CorridorSpeedSchedule]] = Map(1 -> Seq(CorridorSpeedSchedule("test", 1, 100, stats.start, stats.tomorrow.start)))

      val systemData = retrieveReportData.retrieveSystemData.getSystemData
      systemData.corridors.size must be(3)
      val corridorId = systemData.corridors.head.corridor.id
      // system states
      StateService.create(systemId, corridorId, ZkState("EnforceOn", 11111L, "user1", "Begin Universe"), curator)
      var result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("StandBy", stats.start - 1000, "user1", "Previous state"), result.get.version, curator)
      result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("EnforceOn", stats.start + 1000 * 60 * 3, "user2", "Begin meetdag"), result.get.version, curator)

      // system alerts
      AlertService.create(systemId, corridorId, ZkAlert("qn-01", "", "Camera 5 uitgevallen", 1111122L, ConfigType.System, 0.05f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-02", "", "Camera 5 is terug", stats.start - 1, ConfigType.System, 0.15f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-03", "", "Lunch", stats.start, ConfigType.System, 0.25f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-04", "", "Feest", stats.start + 1000 * 60 * 60 * 4 + 23000, ConfigType.System, 0.288f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-05", "", "Onderhoud", stats.tomorrow.start, ConfigType.System, 0.32f), curator)

      val systemAlerts = AlertService.getSystemAlerts(curator, systemId, corridorId, stats.tomorrow.start)
      systemAlerts.size must be(4)
      //
      val events = List(
        LogEntry(1L, 3L, 5L, System.currentTimeMillis(), SystemEvent("com1", System.currentTimeMillis(), systemId, "ALARM", "user1", Some("Relax"))),
        LogEntry(1L, 4L, 6L, System.currentTimeMillis(), SystemEvent("com2", System.currentTimeMillis(), systemId, "ALARM", "user1", None)),
        LogEntry(1L, 5L, 7L, System.currentTimeMillis(), SystemEvent("web", System.currentTimeMillis(), systemId, "WEB", "user1", Some("Yooo"))),
        LogEntry(1L, 5L, 8L, System.currentTimeMillis(), SystemEvent("1234-camera", System.currentTimeMillis(), systemId, "JAI-NOT-GOOD-ERROR", "user1", Some("camera niet in orde"))),
        LogEntry(1L, 5L, 18L, System.currentTimeMillis(), SystemEvent("1234-camera", System.currentTimeMillis(), systemId, "JAI-SOME-INFORMAITONAL", "user1", Some("camera, setting gewijzigd "))),
        LogEntry(1L, 5L, 28L, System.currentTimeMillis(), SystemEvent("certificaat.jar", System.currentTimeMillis(), systemId, "Alarm", "user1", Some("certificaat ongeldig "))),
        LogEntry(1L, 5L, 58L, System.currentTimeMillis(), SystemEvent("schedulemonitor", System.currentTimeMillis(), systemId, "enforceon", "user1", Some("handhaven AAN "))),
        LogEntry(1L, 5L, 59L, System.currentTimeMillis(), SystemEvent("schedulemonitor", System.currentTimeMillis(), systemId, "enforceoff", "user1", Some("handhaven UIT "))),
        LogEntry(1L, 5L, 9L, System.currentTimeMillis(), SystemEvent("sitebuffer", System.currentTimeMillis(), systemId, "Failure", "user1", Some("geopend"))),
        LogEntry(1L, 5L, 229L, System.currentTimeMillis(), SystemEvent("sitebuffer", System.currentTimeMillis(), systemId, "Failure", "user1", Some("leaning"))),
        LogEntry(1L, 6L, 10L, System.currentTimeMillis(), SystemEvent("selftest", System.currentTimeMillis(), systemId, "SELFTEST-SUCCESS", "user1", Some("Yooo"))))

      //create function starts
      val exclude = new ExcludeLogLayer(curator)

      // Zorgt voor de regel <handhaaflog-entry tijd="2012-04-01T00:00:00" toestand="AAN"> Begin meetperiode </handhaaflog-entry>
      // Als dit event niet wordt toegevoegd zal de toestand 'UIT' zijn
      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start - 1000 * 60 * 60 * 8,
        corridorId = Some(3), //corridor 1 is roodlicht
        sequenceNumber = 1,
        suspend = false,
        suspensionReason = Some("Service on")))

      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start + 1000 * 60 * 60 * 9,
        corridorId = Some(3),
        sequenceNumber = 1,
        suspend = true,
        suspensionReason = Some("Service off")))

      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start + 1000 * 60 * 60 * 13,
        corridorId = Some(2),
        sequenceNumber = 1,
        suspend = false,
        suspensionReason = Some("Service on")))

      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start + 1000 * 60 * 60 * 14,
        corridorId = Some(3),
        sequenceNumber = 1,
        suspend = true,
        suspensionReason = Some("Service off")))

      // Begin met een status op gisteren zodat de we de volgende melding krijgen:
      // <handhaaflog-entry tijd="2012-04-01T00:00:00" toestand="AAN"> Begin meetperiode </handhaaflog-entry>
      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start - 1000 * 60,
        corridorId = Some(2),
        sequenceNumber = 1,
        suspend = false,
        suspensionReason = Some("Service on")))

      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start + 1000 * 60 * 60 * 9,
        corridorId = Some(2),
        sequenceNumber = 1,
        suspend = true,
        suspensionReason = Some("Service off")))

      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start - 1000 * 60 * 60 * 10,
        corridorId = Some(2),
        sequenceNumber = 1,
        suspend = false,
        suspensionReason = Some("Service on")))

      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start + 1000 * 60 * 60 * 11,
        corridorId = Some(2),
        sequenceNumber = 1,
        suspend = true,
        suspensionReason = Some("Service off")))

      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start + 1000 * 60 * 60 * 12,
        corridorId = Some(2),
        sequenceNumber = 1,
        suspend = false,
        suspensionReason = Some("Service on")))

      //create metrics

      val export1: ExportViolationsStatistics = ExportViolationsStatistics(1, 100, 301, 311, 321, 331, 341, 351, 361, 371)
      val export2: ExportViolationsStatistics = ExportViolationsStatistics(2, 100, 301, 311, 321, 331, 341, 351, 361, 371)
      val export3: ExportViolationsStatistics = ExportViolationsStatistics(3, 100, 301, 311, 321, 331, 341, 351, 361, 371)
      val dayStats = DayViolationsStatistics("A2", stats, Seq(export1, export2, export3))

      val stat1 = SectionControlStatistics(StatisticsPeriod(stats, stats.tomorrow.start - 1, stats.start),
        MatcherCorridorConfig("A2", "1", 1, "9547", "9542", 2983, 1000, 3600000), 1000, 990, 10, 2, 90.1, 180, laneStats)
      val stat2 = RedLightStatistics(StatisticsPeriod(stats, 1342173624653L, 1342173324653L),
        1000, 990, 1120.1, 1180, laneStats)
      val stat3 = SpeedFixedStatistics(StatisticsPeriod(stats, 1342173624653L, 1342173324653L),
        1000, 990, 90.1, 180, laneStats)
      mockServiceStatisticsAccess.setSectionControlStatistics(Seq(stat1))
      mockServiceStatisticsAccess.setRedLightStatistics(Seq(stat2))
      mockServiceStatisticsAccess.setSpeedFixedStatistics(Seq(stat3))

      val serviceResults = retrieveReportData.readServiceResults(stats, systemData.corridors)
      serviceResults.size must be(3)
      var list = serviceResults.get(1).get
      list.size must be(1)
      list.head must be(stat1)
      list = serviceResults.get(2).get
      list.size must be(1)
      list.head must be(stat2)
      list = serviceResults.get(3).get
      list.size must be(1)
      list.head must be(stat3)

      val allCorridorStatistics = MetricsGenerator.calculate(dayStats, serviceResults, schedules)
      val allStats = allCorridorStatistics.corridorData
      val scStats = allStats.collect { case s: SectionControlCorridorStatistics ⇒ s }
      scStats.size must be(2)

      val _NoneStats = scStats.find(s ⇒ s.corridorId == 1 && s.speedLimit == None).get // must be()
      val _100Stats = scStats.find(s ⇒ s.corridorId == 1 && s.speedLimit == Some(100)).get

      _100Stats must be(SectionControlCorridorStatistics(Some(100), 1, laneStats, export1, 1000, 990, 10, 2, 90.1, 180))
      _NoneStats must be(SectionControlCorridorStatistics(None, 1, laneStats, export1.copy(speedLimit = 0), 1000, 990, 10, 2, 90.1, 180))

      val rlStats = allStats.collect { case s: RedLightCorridorStatistics ⇒ s }
      rlStats.size must be(1)
      rlStats(0) must be(RedLightCorridorStatistics(2, laneStats, export2, 1000, 990, 1120.1, 1180))

      val sfStats = allStats.collect { case s: SpeedFixedCorridorStatistics ⇒ s }
      sfStats.size must be(1)
      sfStats(0) must be(SpeedFixedCorridorStatistics(3, laneStats, export3, 1000, 990, 90.1, 180))

      //create report
      val dayReportBuilder = retrieveReportData.create(stats, events.iterator, dayStats, DayReportVersion.HHM_V60, None)

      //check the result
      dayReportBuilder.filenamePlain must be("a002_20120401.xml")
      dayReportBuilder.filenameZip must be("a002_20120401.gz")
      dayReportBuilder.key must be(StatsDuration("20120401", TimeZone.getTimeZone("Europe/Amsterdam")))

      val factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
      val xsdStream = getClass.getResourceAsStream("/HHM-dagrapport-v6.0.xsd")
      val schema = factory.newSchema(new StreamSource(xsdStream))
      val source = new ByteArrayInputStream(dayReportBuilder.output)

      val xml = new SchemaAwareFactoryAdapter(schema).load(source)

      xml.child.size must be(7)
    }

    "create the correct software and configuration certificate elements in the report" in {
      val mockServiceStatisticsAccess = new MockServiceStatisticsRepositoryAccess()
      val retrieveReportData = new RetrieveReportData(systemId, curator, testSystem, mockServiceStatisticsAccess, corridorServiceMock)

      val stats = StatsDuration("20120401", TimeZone.getTimeZone("Europe/Amsterdam"))
      //configure ZooKeeper
      val laneStats: List[LaneInfo] = List(LaneInfo(Lane("strook123", "Strook 1", "opritmaarsen", systemId, 10.1, 20.2), 1000),
        LaneInfo(Lane("strook321", "Strook 1", "afslagbreukelen", systemId, 40.1, 30.2), 900))

      testStorage.createConfiguration3(systemId, stats)
      val sectionData = retrieveReportData.retrieveSystemData.getSectionData("a")
      sectionData.zkSection.id must be("a")

      // schedules per corridor
      val schedules: Map[Int, Seq[CorridorSpeedSchedule]] = Map(1 -> Seq(CorridorSpeedSchedule("test", 1, 100, stats.start, stats.tomorrow.start)))

      val systemData = retrieveReportData.retrieveSystemData.getSystemData
      systemData.corridors.size must be(3)
      val corridorId = systemData.corridors.head.corridor.id
      // system states
      StateService.create(systemId, corridorId, ZkState("EnforceOn", 11111L, "user1", "Begin Universe"), curator)
      var result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("StandBy", stats.start - 1000, "user1", "Previous state"), result.get.version, curator)
      result = StateService.getVersioned(systemId, corridorId, curator)
      StateService.update(systemId, corridorId, ZkState("EnforceOn", stats.start + 1000 * 60 * 3, "user2", "Begin meetdag"), result.get.version, curator)

      // system alerts
      AlertService.create(systemId, corridorId, ZkAlert("qn-01", "", "Camera 5 uitgevallen", 1111122L, ConfigType.System, 0.05f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-02", "", "Camera 5 is terug", stats.start - 1, ConfigType.System, 0.15f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-03", "", "Lunch", stats.start, ConfigType.System, 0.25f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-04", "", "Feest", stats.start + 1000 * 60 * 60 * 4 + 23000, ConfigType.System, 0.288f), curator)
      AlertService.create(systemId, corridorId, ZkAlert("qn-05", "", "Onderhoud", stats.tomorrow.start, ConfigType.System, 0.32f), curator)

      val systemAlerts = AlertService.getSystemAlerts(curator, systemId, corridorId, stats.tomorrow.start)
      systemAlerts.size must be(4)
      //
      val events = List(
        LogEntry(1L, 3L, 5L, System.currentTimeMillis(), SystemEvent("com1", System.currentTimeMillis(), systemId, "ALARM", "user1", Some("Relax"))),
        LogEntry(1L, 4L, 6L, System.currentTimeMillis(), SystemEvent("com2", System.currentTimeMillis(), systemId, "ALARM", "user1", None)),
        LogEntry(1L, 5L, 7L, System.currentTimeMillis(), SystemEvent("web", System.currentTimeMillis(), systemId, "WEB", "user1", Some("Yooo"))),
        LogEntry(1L, 5L, 8L, System.currentTimeMillis(), SystemEvent("1234-camera", System.currentTimeMillis(), systemId, "JAI-NOT-GOOD-ERROR", "user1", Some("camera niet in orde"))),
        LogEntry(1L, 5L, 18L, System.currentTimeMillis(), SystemEvent("1234-camera", System.currentTimeMillis(), systemId, "JAI-SOME-INFORMAITONAL", "user1", Some("camera, setting gewijzigd "))),
        LogEntry(1L, 5L, 28L, System.currentTimeMillis(), SystemEvent("certificaat.jar", System.currentTimeMillis(), systemId, "Alarm", "user1", Some("certificaat ongeldig "))),
        LogEntry(1L, 5L, 58L, System.currentTimeMillis(), SystemEvent("schedulemonitor", System.currentTimeMillis(), systemId, "enforceon", "user1", Some("handhaven AAN "))),
        LogEntry(1L, 5L, 59L, System.currentTimeMillis(), SystemEvent("schedulemonitor", System.currentTimeMillis(), systemId, "enforceoff", "user1", Some("handhaven UIT "))),
        LogEntry(1L, 5L, 9L, System.currentTimeMillis(), SystemEvent("sitebuffer", System.currentTimeMillis(), systemId, "Failure", "user1", Some("geopend"))),
        LogEntry(1L, 5L, 229L, System.currentTimeMillis(), SystemEvent("sitebuffer", System.currentTimeMillis(), systemId, "Failure", "user1", Some("leaning"))),
        LogEntry(1L, 6L, 10L, System.currentTimeMillis(), SystemEvent("selftest", System.currentTimeMillis(), systemId, "SELFTEST-SUCCESS", "user1", Some("Yooo"))))

      //create function starts
      val exclude = new ExcludeLogLayer(curator)

      // Zorgt voor de regel <handhaaflog-entry tijd="2012-04-01T00:00:00" toestand="AAN"> Begin meetperiode </handhaaflog-entry>
      // Als dit event niet wordt toegevoegd zal de toestand 'UIT' zijn
      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start - 1000 * 60 * 60 * 8,
        corridorId = Some(3), //corridor 1 is roodlicht
        sequenceNumber = 1,
        suspend = false,
        suspensionReason = Some("Service on")))

      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start + 1000 * 60 * 60 * 9,
        corridorId = Some(3),
        sequenceNumber = 1,
        suspend = true,
        suspensionReason = Some("Service off")))

      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start + 1000 * 60 * 60 * 13,
        corridorId = Some(2),
        sequenceNumber = 1,
        suspend = false,
        suspensionReason = Some("Service on")))

      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start + 1000 * 60 * 60 * 14,
        corridorId = Some(3),
        sequenceNumber = 1,
        suspend = true,
        suspensionReason = Some("Service off")))

      // Begin met een status op gisteren zodat de we de volgende melding krijgen:
      // <handhaaflog-entry tijd="2012-04-01T00:00:00" toestand="AAN"> Begin meetperiode </handhaaflog-entry>
      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start - 1000 * 60,
        corridorId = Some(2),
        sequenceNumber = 1,
        suspend = false,
        suspensionReason = Some("Service on")))

      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start + 1000 * 60 * 60 * 9,
        corridorId = Some(2),
        sequenceNumber = 1,
        suspend = true,
        suspensionReason = Some("Service off")))

      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start - 1000 * 60 * 60 * 10,
        corridorId = Some(2),
        sequenceNumber = 1,
        suspend = false,
        suspensionReason = Some("Service on")))

      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start + 1000 * 60 * 60 * 11,
        corridorId = Some(2),
        sequenceNumber = 1,
        suspend = true,
        suspensionReason = Some("Service off")))

      exclude.saveEvent(systemId, new SuspendEvent(componentId = "monitor",
        effectiveTimestamp = stats.start + 1000 * 60 * 60 * 12,
        corridorId = Some(2),
        sequenceNumber = 1,
        suspend = false,
        suspensionReason = Some("Service on")))

      //create metrics

      val export1: ExportViolationsStatistics = ExportViolationsStatistics(1, 100, 301, 311, 321, 331, 341, 351, 361, 371)
      val export2: ExportViolationsStatistics = ExportViolationsStatistics(2, 100, 301, 311, 321, 331, 341, 351, 361, 371)
      val export3: ExportViolationsStatistics = ExportViolationsStatistics(3, 100, 301, 311, 321, 331, 341, 351, 361, 371)
      val dayStats = DayViolationsStatistics("A2", stats, Seq(export1, export2, export3))

      val stat1 = SectionControlStatistics(StatisticsPeriod(stats, stats.tomorrow.start - 1, stats.start),
        MatcherCorridorConfig("A2", "1", 1, "9547", "9542", 2983, 1000, 3600000), 1000, 990, 10, 2, 90.1, 180, laneStats)
      val stat2 = RedLightStatistics(StatisticsPeriod(stats, 1342173624653L, 1342173324653L),
        1000, 990, 1120.1, 1180, laneStats)
      val stat3 = SpeedFixedStatistics(StatisticsPeriod(stats, 1342173624653L, 1342173324653L),
        1000, 990, 90.1, 180, laneStats)
      mockServiceStatisticsAccess.setSectionControlStatistics(Seq(stat1))
      mockServiceStatisticsAccess.setRedLightStatistics(Seq(stat2))
      mockServiceStatisticsAccess.setSpeedFixedStatistics(Seq(stat3))

      val serviceResults = retrieveReportData.readServiceResults(stats, systemData.corridors)
      serviceResults.size must be(3)
      var list = serviceResults.get(1).get
      list.size must be(1)
      list.head must be(stat1)
      list = serviceResults.get(2).get
      list.size must be(1)
      list.head must be(stat2)
      list = serviceResults.get(3).get
      list.size must be(1)
      list.head must be(stat3)

      val allCorridorStatistics = MetricsGenerator.calculate(dayStats, serviceResults, schedules)
      val allStats = allCorridorStatistics.corridorData
      val scStats = allStats.collect { case s: SectionControlCorridorStatistics ⇒ s }
      scStats.size must be(2)

      val _NoneStats = scStats.find(s ⇒ s.corridorId == 1 && s.speedLimit == None).get // must be()
      val _100Stats = scStats.find(s ⇒ s.corridorId == 1 && s.speedLimit == Some(100)).get

      _100Stats must be(SectionControlCorridorStatistics(Some(100), 1, laneStats, export1, 1000, 990, 10, 2, 90.1, 180))
      _NoneStats must be(SectionControlCorridorStatistics(None, 1, laneStats, export1.copy(speedLimit = 0), 1000, 990, 10, 2, 90.1, 180))

      val rlStats = allStats.collect { case s: RedLightCorridorStatistics ⇒ s }
      rlStats.size must be(1)
      rlStats(0) must be(RedLightCorridorStatistics(2, laneStats, export2, 1000, 990, 1120.1, 1180))

      val sfStats = allStats.collect { case s: SpeedFixedCorridorStatistics ⇒ s }
      sfStats.size must be(1)
      sfStats(0) must be(SpeedFixedCorridorStatistics(3, laneStats, export3, 1000, 990, 90.1, 180))

      //create report
      val dayReportBuilder = retrieveReportData.create(stats, events.iterator, dayStats, DayReportVersion.HHM_V60, None)

      //check the result
      dayReportBuilder.filenamePlain must be("a002_20120401.xml")
      dayReportBuilder.filenameZip must be("a002_20120401.gz")
      dayReportBuilder.key must be(StatsDuration("20120401", TimeZone.getTimeZone("Europe/Amsterdam")))

      val factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
      val xsdStream = getClass.getResourceAsStream("/HHM-dagrapport-v6.0.xsd")
      val schema = factory.newSchema(new StreamSource(xsdStream))
      val source = new ByteArrayInputStream(dayReportBuilder.output)

      val xml = new SchemaAwareFactoryAdapter(schema).load(source)

      // Search the string but skip the date part because its using current dates. Use a time mock in the future.
      xml.toString().contains("<component naam=\"Softwarezegel\"><certificaat soll=\"58d546d8a283271322d5a744cea2ef0\">") must be(true)
      xml.toString().contains("<ist>58d546d8a283271322d5a744cea2ef0</ist></certificaat></component><component naam=\"Configuratiezegel\"><certificaat soll=\"blah3\"><tijd>") must be(true)
      xml.toString().contains("</tijd><ist>blah3</ist></certificaat></component>") must be(true)
    }

  }

  lazy val dayReportConfig = RetrieveReportData.fallbackDayReportConfig.copy(dayReportName = "A2_1_Amsterdam_%s")
}
