package csc.sectioncontrol.reports

import csc.sectioncontrol.storage.ServiceStatistics
import csc.sectioncontrol.storagelayer._
import csc.util.test.ObjectBuilder
import net.liftweb.json.Serialization
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

/**
 * Created by carlos on 19.10.15.
 */
class DeserializerTest extends WordSpec with MustMatchers with ObjectBuilder {

  implicit val formats = JsonFormats.formats + MapSerializer //+ Inspector// + new CorridorIdMappingDeserializer

  "Deserializer" must {
    "deserialize ServiceConfig implementations" in {
      testSerializeDeserialize(create[ANPRConfig])
      testSerializeDeserialize(create[RedLightConfig])
      testSerializeDeserialize(create[SpeedFixedConfig])
      testSerializeDeserialize(create[SpeedMobileConfig])
      testSerializeDeserialize(create[TargetGroupConfig])
      testSerializeDeserialize(create[SectionControlConfig])
    }
    "deserialize ServiceStatistics implementations" in {
      testSerializeDeserialize(create[ANPRConfig])
      testSerializeDeserialize(create[RedLightConfig])
      testSerializeDeserialize(create[SpeedFixedConfig])
      testSerializeDeserialize(create[SpeedMobileConfig])
      testSerializeDeserialize(create[TargetGroupConfig])
      testSerializeDeserialize(create[SectionControlConfig])
    }
  }

  def testSerializeDeserialize(input: ServiceStatistics): Unit = {
    val json = Serialization.write[ServiceStatistics](input)
    val obj = Serialization.read[ServiceStatistics](json)
    obj must be(input)
  }

  def testSerializeDeserialize(input: ServiceConfig): Unit = {
    val json = Serialization.write[ServiceConfig](input)
    val obj = Serialization.read[ServiceConfig](json)
    obj must be(input)
  }
}
