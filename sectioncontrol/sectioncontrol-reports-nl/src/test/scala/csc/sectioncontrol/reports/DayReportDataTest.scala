/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

package csc.sectioncontrol.reports

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

import export.DayReportBuilderV42Settings
import csc.sectioncontrol.messages.SystemEvent

class DayReportDataTest extends WordSpec with MustMatchers {
  val settings = new DayReportBuilderV42Settings()
  "DayReport" must {
    "round a given float to default 3 decimals and roundingmode UP" in {
      //test with default nrdecimals and roundingmode
      settings.round(3.0f).toString must be("3.000")
      settings.round(0.33333f).toString must be("0.333")
      settings.round(1f).toString must be("1.000")
      settings.round(1.33333f).toString must be("1.333")
      settings.round(0.368421053f).toString must be("0.368") //0.368421053
      settings.round(0.368421053f, 2).toString must be("0.37") //0.368421053
      settings.round(0).toString must be("0.000")

      //test with non default nrDecimals and roundingmode
      settings.round(0.7122f, 2).toString must be("0.71")
      settings.round(0.3465f, 2).toString must be("0.35")
      settings.round(1.00f, 2).toString must be("1.00")
      settings.round(1.3363f, 2, BigDecimal.RoundingMode.DOWN).toString must be("1.33")
      settings.round(0.367f, 2, BigDecimal.RoundingMode.DOWN).toString must be("0.36") //0.368421053
      settings.round(10.00f, 2, BigDecimal.RoundingMode.DOWN).toString must be("10.00")
      settings.round(0, 2, BigDecimal.RoundingMode.DOWN).toString must be("0.00")
    }
    "translate mtmInfo setting" in {
      settings.translateSetting("stand-by") must be("standby")
      settings.translateSetting("standby") must be("standby")
      settings.translateSetting("100") must be("100")
      settings.translateSetting("onbepaald") must be("onbepaald")
    }
    "translate MeldingType" in {
      val event = SystemEvent(componentId = "RegisterViolations",
        timestamp = System.currentTimeMillis(),
        systemId = "",
        path = None,
        eventType = "Alarm",
        userId = "")
      settings.translateToType(event) must be("ALARM")
      settings.translateToType(event.copy(eventType = "ALERT")) must be("STORING")
      settings.translateToType(event.copy(eventType = "FailureSolved")) must be("INFO")
      settings.translateToType(event.copy(eventType = "Info")) must be("INFO")
      settings.translateToType(event.copy(eventType = "unknown")) must be("INFO")
      settings.translateToType(event.copy(eventType = "SELFTEST-SUCCESS")) must be("INFO")
      settings.translateToType(event.copy(eventType = "SystemStateChange")) must be("STATUS")
      settings.translateToType(event.copy(eventType = "StatusVerandering")) must be("STATUS")
      settings.translateToType(event.copy(eventType = "SystemState")) must be("STATUS")
      settings.translateToType(event.copy(eventType = "EnforceOn")) must be("STATUS")
      settings.translateToType(event.copy(eventType = "Off")) must be("STATUS")
    }
    "translate MeldingType code" in {
      val event = SystemEvent(componentId = "RegisterViolations",
        timestamp = System.currentTimeMillis(),
        systemId = "",
        corridorId = None,
        path = None,
        eventType = "Alarm",
        userId = "")
      settings.translateToTypeCode(event) must be("S")
      settings.translateToTypeCode(event.copy(eventType = "ALERT")) must be("S")
      settings.translateToTypeCode(event.copy(eventType = "FailureSolved")) must be("I")
      settings.translateToTypeCode(event.copy(eventType = "Info")) must be("I")
      settings.translateToTypeCode(event.copy(eventType = "unknown")) must be("I")
      settings.translateToTypeCode(event.copy(eventType = "SELFTEST-SUCCESS")) must be("I")
      settings.translateToTypeCode(event.copy(eventType = "SystemStateChange")) must be("T")
      settings.translateToTypeCode(event.copy(eventType = "StatusVerandering")) must be("T")
      settings.translateToTypeCode(event.copy(eventType = "SystemState")) must be("T")
      settings.translateToTypeCode(event.copy(eventType = "EnforceOn")) must be("T")
      settings.translateToTypeCode(event.copy(eventType = "Off")) must be("T")
    }
    "translate MeldingType from web" in {
      val event = SystemEvent(componentId = "web",
        timestamp = System.currentTimeMillis(),
        systemId = "",
        path = None,
        eventType = "Alarm",
        userId = "")
      settings.translateToType(event) must be("USER")
      settings.translateToType(event.copy(eventType = "FailureSolved")) must be("USER")
      settings.translateToType(event.copy(eventType = "Info")) must be("USER")
    }
  }
}