package csc.sectioncontrol.reports

import java.io.File

import csc.akkautils.DirectLogging
import csc.sectioncontrol.reports.data.RetrieveReportData
import org.apache.commons.io.{ IOUtils, FileUtils }
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

/**
 * Created by carlos on 20.10.15.
 */
class RetrieveReportDataTest
  extends WordSpec
  with MustMatchers
  with DirectLogging {

  val sampleFile = new File("sectioncontrol-reports-nl/src/test/resources/sample_report_input.json")

  "RetrieveReportData" must {

    "serialize properly ReportInput, filtering out some user fields" in {
      //baseline check on a user
      val user = referenceInput.users(0)
      user.phone must be("aphone1")
      user.email must be("mail1@csc.com")
      user.passwordHash must be("somehash")

      val output = File.createTempFile("RetrieveReportDataTest", RetrieveReportData.extension)
      log.info("writing output to " + output.getAbsolutePath)
      RetrieveReportData.saveReportInput(referenceInput, log, Some(output))
      val data = RecreateReportsTool.readReportInput(output).get

      val user2 = data.users(0)
      user2 must be(user.copy(phone = "", email = "", passwordHash = ""))
    }
  }

  lazy val referenceInput = {
    val in = getClass.getResourceAsStream("/sample_report_input.json")
    val json = IOUtils.toString(in)
    in.close()
    RecreateReportsTool.parseReportInput(json)
  }

}
