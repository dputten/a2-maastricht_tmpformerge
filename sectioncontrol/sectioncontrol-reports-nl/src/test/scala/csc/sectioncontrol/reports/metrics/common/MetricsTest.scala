package csc.sectioncontrol.reports.metrics.common

import java.text.SimpleDateFormat
import java.util.Date
import csc.sectioncontrol.storage.ZkState
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

/**
 * Created by dputten on 5/15/17.
 */

object Extensions {

  def defaultDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")

  implicit def stringWrapper(value: String) = new StringExtensions(value)

  class StringExtensions(val value: String) {
    def toDate(implicit formatter: SimpleDateFormat = defaultDateFormat): Date = {
      formatter.parse(value)
    }
  }
}

class MetricsEnforceRatioTest extends WordSpec with MustMatchers {

  import Extensions._

  val states = List(
    ZkState("EnforceOn", "2017-05-02T16:31:31".toDate.getTime, "Number 1", "Calibratie geslaagd, Stand-by -> Handhaven aan"),
    ZkState("EnforceOn", "2017-05-02T16:31:31".toDate.getTime, "Number 2", "Calibratie geslaagd, Stand-by -> Handhaven aan"),
    ZkState("EnforceOn", "2017-05-02T16:31:31".toDate.getTime, "Number 3", "Calibratie geslaagd, Stand-by -> Handhaven aan"),
    ZkState("EnforceOn", "2017-05-02T16:31:32".toDate.getTime, "Number 4", "Calibratie geslaagd, Stand-by -> Handhaven aan"),
    ZkState("EnforceOn", "2017-05-03T16:06:26".toDate.getTime, "Number 5", "Calibratie geslaagd, Handhaven aan -> Stand-by"),
    ZkState("EnforceOn", "2017-05-03T16:06:26".toDate.getTime, "Number 6", "Calibratie geslaagd, Handhaven aan -> Stand-by"),
    ZkState("EnforceOn", "2017-05-03T16:06:27".toDate.getTime, "Number 7", "Calibratie geslaagd, Handhaven aan -> Stand-by"),
    ZkState("StandBy", "2017-05-03T16:06:27".toDate.getTime, "Number 8", "Calibratie geslaagd, Handhaven aan -> Stand-by"))

  val states2 = List(
    ZkState("EnforceOn", "2017-05-02T16:31:31".toDate.getTime, "Number 1", "Calibratie geslaagd, Stand-by -> Handhaven aan"),
    ZkState("EnforceOn", "2017-05-02T16:31:31".toDate.getTime, "Number 2", "Calibratie geslaagd, Stand-by -> Handhaven aan"),
    ZkState("EnforceOn", "2017-05-02T16:31:31".toDate.getTime, "Number 3", "Calibratie geslaagd, Stand-by -> Handhaven aan"),
    ZkState("EnforceOn", "2017-05-02T16:31:32".toDate.getTime, "Number 4", "Calibratie geslaagd, Stand-by -> Handhaven aan"),
    ZkState("EnforceOn", "2017-05-03T16:06:26".toDate.getTime, "Number 5", "Calibratie geslaagd, Handhaven aan -> Stand-by"),
    ZkState("EnforceOn", "2017-05-03T16:06:26".toDate.getTime, "Number 6", "Calibratie geslaagd, Handhaven aan -> Stand-by"),
    ZkState("EnforceOn", "2017-05-03T16:06:27".toDate.getTime, "Number 7", "Calibratie geslaagd, Handhaven aan -> Stand-by"),
    ZkState("StandBy", "2017-05-03T16:06:27".toDate.getTime, "Number 8", "Calibratie geslaagd, Handhaven aan -> Stand-by"),
    ZkState("StandBy", "2017-05-03T20:00:00".toDate.getTime, "Number 9", "Calibratie geslaagd, Handhaven aan -> Stand-by"),
    ZkState("StandBy", "2017-05-03T20:00:00".toDate.getTime, "Number 10", "Calibratie geslaagd, Handhaven aan -> Stand-by"),
    ZkState("StandBy", "2017-05-03T20:00:00".toDate.getTime, "Number 10", "Calibratie geslaagd, Handhaven aan -> Stand-by"),
    ZkState("EnforceOn", "2017-05-03T20:00:00".toDate.getTime, "Number 11", "Calibratie geslaagd, Handhaven aan -> Stand-by"))

  "CalulateEnforceRatio" must {
    "calculate the enforce ratio one change today" in {

      val removedSystemStates = PerformanceMetrics.correctCombinedSystemStates(states)

      val enforceRatioSec = PerformanceMetrics.calculateEnforceRatio(removedSystemStates, ZkState.enforceOn, ZkState.enforceDegraded, ZkState.enforceOff).setScale(4, BigDecimal.RoundingMode.HALF_UP)

      enforceRatioSec must be(0.6711)
    }

    "calculate the enforce ratio more state changes today" in {

      val removedSystemStates = PerformanceMetrics.correctCombinedSystemStates(states2)

      val enforceRatioSec = PerformanceMetrics.calculateEnforceRatio(removedSystemStates, ZkState.enforceOn, ZkState.enforceDegraded, ZkState.enforceOff).setScale(4, BigDecimal.RoundingMode.HALF_UP)

      enforceRatioSec must be(0.8378)

    }
  }
}
