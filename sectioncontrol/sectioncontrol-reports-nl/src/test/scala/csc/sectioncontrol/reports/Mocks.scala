package csc.sectioncontrol.reports

import csc.sectioncontrol.storagelayer.ServiceStatisticsRepositoryAccess
import csc.sectioncontrol.storage._
import csc.sectioncontrol.messages.StatsDuration
import csc.sectioncontrol.storagelayer.corridor.{ CorridorIdMapping, CorridorServiceTrait }
import csc.curator.utils.Curator
import csc.sectioncontrol.storage.ZkCorridor
import csc.sectioncontrol.storage.SpeedFixedStatistics
import csc.sectioncontrol.storage.RedLightStatistics
import csc.sectioncontrol.storage.SectionControlStatistics
import csc.sectioncontrol.storagelayer.corridor.CorridorIdMapping

class CorridorServiceMock extends CorridorServiceTrait {
  override def existsDailyReportConfig(systemId: String, corridorId: String): Boolean = false

  override def deleteDailyReportConfig(systemId: String, corridorId: String): Unit = ()

  override def getDailyReportConfig(systemId: String, corridorId: String): Option[ZkDailyReportConfig] = {
    val dailyReportConfigSpeedEntry100 = DailyReportConfigSpeedEntry("100", "TCS UT A2 R-1 (100 km/h)", "a0021001")
    val dailyReportConfigSpeedEntry130 = DailyReportConfigSpeedEntry("130", "TCS UT A2 R-1 (130 km/h)", "a0021301")
    val dailyReportConfigSpeedEntryTotal = DailyReportConfigSpeedEntry("total", "TCS UT A2 R-1 (totaal)", "a0021001, a0021301 (totaal)")
    val ignoreStatisticsOfLane1 = IgnoreStatisticsOfLane("A2", "E1", "E1R1")
    val ignoreStatisticsOfLane2 = IgnoreStatisticsOfLane("A2", "E1", "E1R2")
    val ignoreStatisticsOfLanes = List(ignoreStatisticsOfLane1, ignoreStatisticsOfLane2)
    val speedEntries = List(dailyReportConfigSpeedEntry100, dailyReportConfigSpeedEntry130, dailyReportConfigSpeedEntryTotal)
    Some(ZkDailyReportConfig(ignoreStatisticsOfLanes, speedEntries))
  }

  override def createDailyReportConfig(systemId: String, corridorId: String, zkDailyReportConfig: ZkDailyReportConfig): Unit = ()

  override def createCorridor(systemId: String, corridor: ZkCorridor): Unit = ()

  override def getCorridorIdMapping(systemId: String, corridorId: Int): Option[CorridorIdMapping] = Some(CorridorIdMapping(corridorId.toString, corridorId))

  override def getCorridorIdMapping(systemId: String): Seq[CorridorIdMapping] = Seq(CorridorIdMapping("1", 1))

  override def getCorridorIds(systemId: String): Seq[String] = null // make it go BOOM on purpose

  override def getGantriesForCorridor(systemId: String, corridorId: String): Seq[String] = null // make it go BOOM on purpose
}

/**
 * Mock implementation of the ServiceStatisticsRepositoryAccess trait.
 */
class MockServiceStatisticsRepositoryAccess extends ServiceStatisticsRepositoryAccess {

  private var sectionControlStatistics: Seq[SectionControlStatistics] = Seq()
  private var redLightStatistics: Seq[RedLightStatistics] = Seq()
  private var speedFixedStatisticsStatistics: Seq[SpeedFixedStatistics] = Seq()

  def setSectionControlStatistics(statistics: Seq[SectionControlStatistics]): Unit = {
    sectionControlStatistics = statistics
  }

  def setRedLightStatistics(statistics: Seq[RedLightStatistics]): Unit = {
    redLightStatistics = statistics
  }

  def setSpeedFixedStatistics(statistics: Seq[SpeedFixedStatistics]): Unit = {
    speedFixedStatisticsStatistics = statistics
  }

  def getSectionControlStatistics(systemId: String, statsDuration: StatsDuration): Seq[SectionControlStatistics] = sectionControlStatistics

  def getRedLightStatistics(systemId: String, statsDuration: StatsDuration): Seq[RedLightStatistics] = redLightStatistics

  def getSpeedFixedStatistics(systemId: String, statsDuration: StatsDuration): Seq[SpeedFixedStatistics] = speedFixedStatisticsStatistics

  def save(systemId: String, statistics: SectionControlStatistics): Unit = {} // Not needed for report tests

  def save(systemId: String, statistics: RedLightStatistics): Unit = {} // Not needed for report tests

  def save(systemId: String, statistics: SpeedFixedStatistics): Unit = {} // Not needed for report tests
}
