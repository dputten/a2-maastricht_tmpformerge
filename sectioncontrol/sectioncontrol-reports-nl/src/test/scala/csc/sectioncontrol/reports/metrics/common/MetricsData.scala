package csc.sectioncontrol.reports.metrics.common

import csc.sectioncontrol.common.DayUtility
import csc.sectioncontrol.enforce.ExportViolationsStatistics
import csc.sectioncontrol.messages.{ StatsDuration, SectionControlCorridorStatistics, Lane }
import csc.sectioncontrol.reports.{ LaneData, GantryData, SectionData, CorridorData }
import csc.sectioncontrol.reports.metrics.Period
import csc.sectioncontrol.reports.metrics.tc.TCMetrics
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storagelayer.{ SectionControlConfig, CorridorConfig }
import csc.util.test.ObjectBuilder
import csc.sectioncontrol.reports.metrics.Extensions._

/**
 * Created by carlos on 09.10.15.
 */
trait MetricsData extends ObjectBuilder {

  lazy val statsDuration = StatsDuration("20121229", DayUtility.fileExportTimeZone.getID)

  def totalIn = (entryLaneInfo_11 ++ entryLaneInfo_12).map(_.count).sum //100+200+450+650
  def totalOut = (exitLaneInfo_11 ++ exitLaneInfo_12).map(_.count).sum //80+133+380+655
  def totalIn_11 = entryLaneInfo_11.map(_.count).sum
  def totalOut_11 = exitLaneInfo_11.map(_.count).sum
  def total_11 = math.max(totalIn_11, totalOut_11)

  val alerts = List(
    ZkAlertHistory(path = "A2-X1-R1-Camera", alertType = "", configType = "Lane", message = "current event", startTime = "2012-12-29T02:00:00".toDate.getTime, endTime = Long.MaxValue, reductionFactor = 0.1f),
    ZkAlertHistory(path = "A2-X1-R2-Camera", alertType = "", configType = "Lane", message = "current event", startTime = "2012-12-29T03:00:00".toDate.getTime, endTime = Long.MaxValue, reductionFactor = 0.4f),
    ZkAlertHistory(path = "A2-E1-R1-Camera", alertType = "", configType = "Lane", message = "current event", startTime = "2012-12-29T13:00:00".toDate.getTime, endTime = Long.MaxValue, reductionFactor = 0.2f))

  def systemStates: List[ZkState]

  def periods_130 = List(Period(from = "2012-12-29T00:00:00", to = "2012-12-29T06:00:00"),
    Period(from = "2012-12-29T19:00:00", to = "2012-12-29T24:00:00"))

  def periods_100 = List(Period(from = "2012-12-29T06:00:00", to = "2012-12-29T19:00:00"))

  def metrics_speedLimits = List(100, 200, 300)
  def metrics_11_speedLimits = List(33, 44, 55)

  lazy val metrics = TCMetrics(
    system = defaultZkSystem,
    corridorData = defaultCorridorData,
    corridorStatistics = listOfCorridors,
    schedulePeriods = periods_130,
    systemAlerts = alerts,
    systemStates = systemStates,
    speedLimits = metrics_speedLimits)

  lazy val metrics_without_statistics = TCMetrics(
    system = defaultZkSystem,
    corridorData = defaultCorridorData,
    corridorStatistics = Nil,
    schedulePeriods = periods_130,
    systemAlerts = alerts,
    systemStates = systemStates,
    speedLimits = metrics_speedLimits)

  lazy val metrics_11 = TCMetrics(
    system = defaultZkSystem,
    corridorData = defaultCorridorData,
    corridorStatistics = Seq(corridorStats_11),
    schedulePeriods = periods_130,
    systemAlerts = alerts,
    systemStates = systemStates,
    speedLimits = metrics_11_speedLimits)

  private lazy val defaultViolation = ExportViolationsStatistics(corridorId = 0,
    speedLimit = 100,
    auto = 456,
    manual = 112,
    mobi = 344,
    mobiDoNotProcess = 245,
    mtmPardon = 32,
    doublePardon = 45,
    otherPardon = 23,
    manualAccordingToSpec = 42)

  private lazy val entryLaneInfo_11 = List(
    LaneInfo(
      lane = Lane(
        laneId = "E3R1",
        name = "E3R1",
        gantry = "E3",
        system = "A2",
        sensorGPS_longitude = 1.2,
        sensorGPS_latitude = 1.3),
      count = 100),
    LaneInfo(
      lane = Lane(
        laneId = "E3R2",
        name = "E3R2",
        gantry = "E3",
        system = "A2",
        sensorGPS_longitude = 1.2,
        sensorGPS_latitude = 1.3),
      count = 200))

  private lazy val exitLaneInfo_11 = List(
    LaneInfo(
      lane = Lane(
        laneId = "X2R1",
        name = "X2R1",
        gantry = "X2",
        system = "A2",
        sensorGPS_longitude = 1.2,
        sensorGPS_latitude = 1.3),
      count = 80),
    LaneInfo(
      lane = Lane(
        laneId = "X2R2",
        name = "X2R2",
        gantry = "X2",
        system = "A2",
        sensorGPS_longitude = 1.27,
        sensorGPS_latitude = 1.37),
      count = 133))

  private lazy val entryLaneInfo_12 = List(
    LaneInfo(
      lane = Lane(
        laneId = "E3R1",
        name = "E3R1",
        gantry = "E3",
        system = "A2",
        sensorGPS_longitude = 1.2,
        sensorGPS_latitude = 1.3),
      count = 455),
    LaneInfo(
      lane = Lane(
        laneId = "E3R2",
        name = "E3R2",
        gantry = "E3",
        system = "A2",
        sensorGPS_longitude = 1.2,
        sensorGPS_latitude = 1.3),
      count = 650))

  private lazy val exitLaneInfo_12 = List(
    LaneInfo(
      lane = Lane(
        laneId = "X2R1",
        name = "X2R1",
        gantry = "X2",
        system = "A2",
        sensorGPS_longitude = 1.2,
        sensorGPS_latitude = 1.3),
      count = 380),
    LaneInfo(
      lane = Lane(
        laneId = "X2R2",
        name = "X2R2",
        gantry = "X2",
        system = "A2",
        sensorGPS_longitude = 1.27,
        sensorGPS_latitude = 1.37),
      count = 655))

  lazy val corridorStats_11 = SectionControlCorridorStatistics(
    corridorId = 11,
    laneStats = entryLaneInfo_11 ++ exitLaneInfo_11,
    violations = defaultViolation.copy(
      auto = 100,
      manual = 18,
      mobi = 50,
      manualAccordingToSpec = 8,
      mobiDoNotProcess = 10), //total 168
    input = 210,
    matched = 188,
    unmatched = 100,
    doubles = 10,
    averageSpeed = 20.4829178,
    highestSpeed = 100)

  lazy val corridorStats_12 = SectionControlCorridorStatistics(
    corridorId = 12,
    laneStats = entryLaneInfo_12 ++ exitLaneInfo_12,
    violations = defaultViolation.copy(
      auto = 50,
      manual = 40,
      mobi = 1,
      manualAccordingToSpec = 9,
      mobiDoNotProcess = 2), //total 90
    input = 200,
    matched = 50,
    unmatched = 100,
    doubles = 10,
    averageSpeed = 33,
    highestSpeed = 250)

  def listOfCorridors = List(corridorStats_11, corridorStats_12)

  def createZkDailyReportConfig(): ZkDailyReportConfig = {
    val dailyReportConfigSpeedEntry100 = DailyReportConfigSpeedEntry("100", "TCS UT A2 R-1 (100 km/h)", "a0021001")
    val dailyReportConfigSpeedEntry130 = DailyReportConfigSpeedEntry("130", "TCS UT A2 R-1 (130 km/h)", "a0021301")
    val dailyReportConfigSpeedEntryTotal = DailyReportConfigSpeedEntry("total", "TCS UT A2 R-1 (totaal)", "a0021001, a0021301 (totaal)")
    val ignoreStatisticsOfLane1 = IgnoreStatisticsOfLane("A2", "E1", "E1R1")
    val ignoreStatisticsOfLane2 = IgnoreStatisticsOfLane("A2", "E1", "E1R2")
    val ignoreStatisticsOfLanes = List(ignoreStatisticsOfLane1, ignoreStatisticsOfLane2)
    val speedEntries = List(dailyReportConfigSpeedEntry100, dailyReportConfigSpeedEntry130, dailyReportConfigSpeedEntryTotal)
    ZkDailyReportConfig(ignoreStatisticsOfLanes, speedEntries)
  }

  lazy val defaultZkSystem = ZkSystem(
    id = "A2",
    name = "A2",
    title = "A2",
    mtmRouteId = "",
    location = ZkSystemLocation(
      description = "Traject controle A2 Maarssen - Holendrecht",
      region = "Utrecht",
      viewingDirection = Some(""),
      roadNumber = "2002",
      roadPart = "Links",
      systemLocation = "Amsterdam"),
    maxSpeed = 100,
    retentionTimes = ZkSystemRetentionTimes(
      nrDaysKeepViolations = 5,
      nrDaysRemindCaseFiles = 3,
      nrDaysKeepTrafficData = 28),
    pardonTimes = ZkSystemPardonTimes(pardonTimeEsa_secs = 100, pardonTimeTechnical_secs = 300),
    compressionFactor = 20,
    orderNumber = 0,
    serialNumber = SerialNumber("sn12345"),
    violationPrefixId = "a002")

  val defaultCorridorData = CorridorData(
    corridor = CorridorConfig(
      id = "corridorId",
      name = "Name of corridor",
      serviceType = ServiceType.SectionControl,
      roadType = 12,
      isInsideUrbanArea = false,
      dynamaxMqId = "dynamaxMqId",
      approvedSpeeds = Nil,
      pshtm = ZkPSHTMIdentification(
        useYear = false,
        useMonth = false,
        useDay = false,
        useReportingOfficerId = false,
        useNumberOfTheDay = false,
        useLocationCode = false,
        useHHMCode = false,
        useTypeViolation = false,
        useFixedCharacter = false,
        fixedCharacter = "A"),
      info = ZkCorridorInfo(
        corridorId = 11,
        locationCode = "10",
        reportId = "TC UT A2 L-1 (100)",
        reportHtId = "a0020011",
        reportPerformance = true,
        locationLine1 = "locationLine1",
        locationLine2 = None),
      startSectionId = "",
      endSectionId = "",
      allSectionIds = Nil,
      serviceConfig = new SectionControlConfig()),
    startSection = SectionData(
      zkSection = ZkSection(
        id = "S3",
        systemId = "A2",
        name = "name of S3",
        length = 100,
        startGantryId = "E3",
        endGantryId = "X3",
        matrixBoards = Nil,
        msiBlackList = Nil),
      inGantry = GantryData(
        id = "E3",
        name = "E3",
        hectometer = "100L",
        lanes = List(
          LaneData(id = "E3R1", name = "E3R1", bpsLaneId = Some("1 HR R- R")),
          LaneData(id = "E3R2", name = "E3R2", bpsLaneId = Some("2 HR R- R")))),
      outGantry = GantryData(
        id = "X3",
        name = "X3",
        hectometer = "200L",
        lanes = List(
          LaneData(id = "X3R1", name = "X3R1", bpsLaneId = Some("1 HR R- R")),
          LaneData(id = "X3R2", name = "X3R2", bpsLaneId = Some("2 HR R- R"))))),
    endSection = SectionData(
      zkSection = ZkSection(
        id = "S2",
        systemId = "A2",
        name = "name of S2",
        length = 200,
        startGantryId = "E2",
        endGantryId = "X2",
        matrixBoards = Nil,
        msiBlackList = Nil),
      inGantry = GantryData(
        id = "E2",
        name = "E2",
        hectometer = "400L",
        lanes = List(
          LaneData(id = "E2R1", name = "E2R1", bpsLaneId = Some("1 HR R- R")),
          LaneData(id = "E2R2", name = "E2R2", bpsLaneId = Some("2 HR R- R")))),
      outGantry = GantryData(
        id = "X2",
        name = "X2",
        hectometer = "500L",
        lanes = List(
          LaneData(id = "X2R1", name = "X2R1", bpsLaneId = Some("1 HR R- R")),
          LaneData(id = "X2R2", name = "X2R2", bpsLaneId = Some("2 HR R- R"))))),
    createZkDailyReportConfig())

}
