package csc.sectioncontrol.reports

import csc.config.Path
import csc.sectioncontrol.reports.data.RetrieveReportData
import csc.sectioncontrol.storage._
import csc.sectioncontrol.messages.StatsDuration
import csc.sectioncontrol.messages.certificates._
import java.text.SimpleDateFormat
import java.util.Date
import csc.sectioncontrol.storagelayer.SpeedFixedConfig
import csc.sectioncontrol.messages.certificates.MeasurementMethodInstallation
import csc.sectioncontrol.enforce.DayViolationsStatistics
import csc.sectioncontrol.messages.certificates.TypeCertificate
import csc.sectioncontrol.storagelayer.RedLightConfig
import csc.sectioncontrol.messages.certificates.ActiveCertificate
import csc.sectioncontrol.messages.certificates.ComponentCertificate
import csc.sectioncontrol.storagelayer.SectionControlConfig
import csc.curator.utils.Curator

class TestZkStorage(val zkStore: Curator) {
  val format = "yyyy-MM-dd'T'hh:mm:ss"

  def toDate(text: String, format: String = format): Long = new SimpleDateFormat(format).parse(text).getTime

  def deleteConfiguration(systemId: String) {
    zkStore.deleteRecursive("/ctes")
  }

  def createConfiguration1(systemId: String, stats: StatsDuration) {
    // system config
    val zkSystem = ZkSystem(id = "a2", name = "A2", title = "A2", mtmRouteId = "", violationPrefixId = "a002",
      location = ZkSystemLocation(
        description = "A2 Utrecht Amsterdam",
        systemLocation = "Amsterdam",
        region = "Randstad",
        viewingDirection = Some("Zuid-Oost"),
        roadNumber = "2",
        roadPart = "Links"),
      serialNumber = SerialNumber("777"), orderNumber = 1,
      maxSpeed = 100, compressionFactor = 4,
      retentionTimes = ZkSystemRetentionTimes(
        nrDaysKeepTrafficData = 11,
        nrDaysKeepViolations = 5,
        nrDaysRemindCaseFiles = 30),
      pardonTimes = ZkSystemPardonTimes(pardonTimeEsa_secs = 0, pardonTimeTechnical_secs = 0))
    zkStore.put(Path(Paths.Systems.getConfigPath(systemId)), zkSystem)
    // section
    val zkSection = ZkSection(id = "a", systemId = "a2", name = "A",
      length = 2983.0, startGantryId = "opritmaarsen",
      endGantryId = "afslagbreukelen", matrixBoards = Seq("989"), msiBlackList = Nil)
    val zkSectionPath = Path(Paths.Sections.getConfigPath(systemId, "a"))
    zkStore.put(zkSectionPath, zkSection)

    val zkSection_b = ZkSection(id = "b", systemId = "a2", name = "B",
      length = 2983.0, startGantryId = "opritmaarsen",
      endGantryId = "afslagbreukelen", matrixBoards = Seq("989"), msiBlackList = Nil)
    val zkSectionPath_b = Path(Paths.Sections.getConfigPath(systemId, "b"))
    zkStore.put(zkSectionPath_b, zkSection_b)

    //  gantries
    val startGantryMap = Map("id" -> "opritmaarsen", "name" -> "Oprit Maarsen",
      "systemId" -> "A2", "hectometer" -> "40")
    val startGantryMapPath = Path(Paths.Gantries.getConfigPath(systemId, "opritmaarsen"))
    zkStore.put(startGantryMapPath, startGantryMap)

    val endGantryMap = Map("id" -> "afslagbreukelen", "name" -> "Afslag Breukelen",
      "systemId" -> "A2", "hectometer" -> "50")
    val endGantryMapPath = Path(Paths.Gantries.getConfigPath(systemId, "afslagbreukelen"))
    zkStore.put(endGantryMapPath, endGantryMap)
    //lanes
    val startLaneMap = Map("id" -> "strook123", "name" -> "Strook 1",
      "gantry" -> "opritmaarsen", "systemId" -> "A2")
    val startLaneMapPath = Path(Paths.Gantries.getLanesConfigPath(systemId, "opritmaarsen", "strook1"))
    zkStore.put(startLaneMapPath, startLaneMap)

    val endLaneMap = Map("id" -> "strook321", "name" -> "Strook 1",
      "gantry" -> "afslagbreukelen", "systemId" -> "A2")
    val endLaneMapPath = Path(Paths.Gantries.getLanesConfigPath(systemId, "afslagbreukelen", "strook1"))
    zkStore.put(endLaneMapPath, endLaneMap)

    /*
    object Extensions {
      implicit def stringWrapper(value: String) = new StringExtensions(value)

      class StringExtensions(val value: String) {
        def toDate(implicit formatter: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")): Date = {
          formatter.parse(value)
        }
      }

    }
    */

    // corridor
    val corrInfo = ZkCorridorInfo(corridorId = 1,
      locationCode = "1234",
      reportId = "TC UT A2 L-1 (100)",
      reportHtId = "a0020011",
      reportPerformance = true,
      locationLine1 = "loc 1",
      locationLine2 = Some("loc 2"))

    val corrInfo_2 = ZkCorridorInfo(corridorId = 2,
      locationCode = "12345",
      reportId = "TC UT A2 L-2 (100)",
      reportHtId = "a0020012",
      reportPerformance = true,
      locationLine1 = "loc 1",
      locationLine2 = Some("loc 2"))

    val corrInfo_3 = ZkCorridorInfo(corridorId = 3,
      locationCode = "123456",
      reportId = "TC UT A2 L-3",
      reportHtId = "a0020013",
      reportPerformance = true,
      locationLine1 = "loc 1",
      locationLine2 = Some("loc 2"))

    val corridor_1 = ZkCorridor(id = "1", serviceType = ServiceType.SectionControl, name = "1", dynamaxMqId = "", approvedSpeeds = Seq(),
      pshtm = null, info = corrInfo, startSectionId = "a", endSectionId = "a", allSectionIds = Seq("a"), roadType = 1, isInsideUrbanArea = false,
      direction = ZkDirection(directionTo = "Amsterdam", directionFrom = "Utrecht"),
      radarCode = 404, roadCode = 2, dutyType = "KL06", deploymentCode = "03", codeText = "")
    val corridorPath = Path(Paths.Corridors.getConfigPath(systemId, "1"))
    zkStore.put(corridorPath, corridor_1)
    var serPathSection = Paths.Corridors.getServicesPath(systemId, "1") + "/" + ServiceType.SectionControl.toString
    val serviceConfigSection = new SectionControlConfig()
    zkStore.put(serPathSection, serviceConfigSection)

    val corridor_2 = ZkCorridor(id = "2", serviceType = ServiceType.RedLight, name = "2", dynamaxMqId = "", approvedSpeeds = Seq(),
      pshtm = null, info = corrInfo_2, startSectionId = "a", endSectionId = "b", allSectionIds = Seq("a", "b"), roadType = 1, isInsideUrbanArea = false,
      direction = ZkDirection(directionTo = "Amsterdam", directionFrom = "Utrecht"),
      radarCode = 404, roadCode = 2, dutyType = "KL06", deploymentCode = "03", codeText = "")

    val corridorPath_2 = Path(Paths.Corridors.getConfigPath(systemId, "2"))
    zkStore.put(corridorPath_2, corridor_2)
    serPathSection = Paths.Corridors.getServicesPath(systemId, "2") + "/" + ServiceType.RedLight.toString
    val redLightServiceConfig = new RedLightConfig(
      flgFileName = Some("roodlicht.flg"),
      dynamicEnforcement = false,
      pardonRedTime = 1000,
      minimumYellowTime = 1000,
      factNumber = "12345")
    zkStore.put(serPathSection, redLightServiceConfig)

    val corridor_3 = ZkCorridor(id = "3", serviceType = ServiceType.SpeedFixed, name = "3", dynamaxMqId = "", approvedSpeeds = Seq(),
      pshtm = null, info = corrInfo_3, startSectionId = "a", endSectionId = "b", allSectionIds = Seq("a", "b"), roadType = 1, isInsideUrbanArea = false,
      direction = ZkDirection(directionTo = "Amsterdam", directionFrom = "Utrecht"),
      radarCode = 404, roadCode = 2, dutyType = "KL06", deploymentCode = "03", codeText = "")

    val corridorPath_3 = Path(Paths.Corridors.getConfigPath(systemId, "3"))
    zkStore.put(corridorPath_3, corridor_3)
    serPathSection = Paths.Corridors.getServicesPath(systemId, "3") + "/" + ServiceType.SpeedFixed.toString
    val fixedSpeedServiceConfig = new SpeedFixedConfig(
      flgFileName = Some("snelheid.flg"),
      dynamicEnforcement = false)
    zkStore.put(serPathSection, fixedSpeedServiceConfig)

    //val fcorridor = List(ZkFunctionalCorridor("a", "b", 999, "TC UT A2 L-2 (totaal)", "a0020011,a0020017 totaal", true))
    //val fcorridorPath = Path(Paths.Systems.getFunctionalCorridorPath(systemId))
    //zkStore.put(fcorridorPath, fcorridor)

    //schedules
    val schedules_cor_1 = List(
      new ZkSchedule(id = "11",
        indicationRoadworks = ZkIndicationType.None,
        indicationDanger = ZkIndicationType.None,
        indicationActualWork = ZkIndicationType.None,
        invertDrivingDirection = ZkIndicationType.None,
        fromPeriodHour = 0,
        fromPeriodMinute = 0,
        toPeriodHour = 6,
        toPeriodMinute = 0,
        speedLimit = 130,
        signSpeed = 130,
        signIndicator = true,
        supportMsgBoard = false),
      new ZkSchedule(id = "12",
        indicationRoadworks = ZkIndicationType.None,
        indicationDanger = ZkIndicationType.None,
        indicationActualWork = ZkIndicationType.None,
        invertDrivingDirection = ZkIndicationType.None,
        fromPeriodHour = 6,
        fromPeriodMinute = 0,
        toPeriodHour = 19,
        toPeriodMinute = 0,
        speedLimit = 100,
        signSpeed = 100,
        signIndicator = true,
        supportMsgBoard = false),
      new ZkSchedule(id = "21",
        indicationRoadworks = ZkIndicationType.None,
        indicationDanger = ZkIndicationType.None,
        indicationActualWork = ZkIndicationType.None,
        invertDrivingDirection = ZkIndicationType.None,
        fromPeriodHour = 19,
        fromPeriodMinute = 0,
        toPeriodHour = 24,
        toPeriodMinute = 0,
        speedLimit = 130,
        signSpeed = 130,
        signIndicator = true,
        supportMsgBoard = false))

    val schedules_cor_2 = List(new ZkSchedule(id = "21",
      indicationRoadworks = ZkIndicationType.None,
      indicationDanger = ZkIndicationType.None,
      indicationActualWork = ZkIndicationType.None,
      invertDrivingDirection = ZkIndicationType.None,
      fromPeriodHour = 6,
      fromPeriodMinute = 0,
      toPeriodHour = 19,
      toPeriodMinute = 0,
      speedLimit = 100,
      signSpeed = 100,
      signIndicator = true,
      supportMsgBoard = false))

    val schedules_cor_3 = List(
      new ZkSchedule(id = "31",
        indicationRoadworks = ZkIndicationType.None,
        indicationDanger = ZkIndicationType.None,
        indicationActualWork = ZkIndicationType.None,
        invertDrivingDirection = ZkIndicationType.None,
        fromPeriodHour = 0,
        fromPeriodMinute = 0,
        toPeriodHour = 6,
        toPeriodMinute = 0,
        speedLimit = 130,
        signSpeed = 130,
        signIndicator = true,
        supportMsgBoard = false),
      new ZkSchedule(id = "32",
        indicationRoadworks = ZkIndicationType.None,
        indicationDanger = ZkIndicationType.None,
        indicationActualWork = ZkIndicationType.None,
        invertDrivingDirection = ZkIndicationType.None,
        fromPeriodHour = 19,
        fromPeriodMinute = 0,
        toPeriodHour = 24,
        toPeriodMinute = 0,
        speedLimit = 130,
        signSpeed = 130,
        signIndicator = true,
        supportMsgBoard = false))

    val schedulePath_1 = Path(Paths.Corridors.getSchedulesPath(systemId, "1"))
    val schedulePath_2 = Path(Paths.Corridors.getSchedulesPath(systemId, "2"))
    val schedulePath_3 = Path(Paths.Corridors.getSchedulesPath(systemId, "3"))
    zkStore.put(schedulePath_1, schedules_cor_1)
    zkStore.put(schedulePath_2, schedules_cor_2)
    zkStore.put(schedulePath_3, schedules_cor_3)

    // certificates
    val entry1 = new MeasurementMethodInstallation(typeId = "entry1", timeFrom = toDate("2012-01-01T00:00:00"))
    val path = Path(Paths.Systems.getInstallCertificatePath(systemId))
    zkStore.put(path / "entry1", entry1)

    val globalType1 = new MeasurementMethodType(typeDesignation = "XX123",
      category = "A",
      unitSpeed = "km/h",
      unitRedLight = "s",
      unitLength = "m",
      restrictiveConditions = "",
      displayRange = "20-250 km/h",
      temperatureRange = "temperatuurbereik -10 to 50 graden Celsius",
      permissibleError = "toelatbare fout 3%",
      typeCertificate = new TypeCertificate("eg33-cert", 0,
        List(ComponentCertificate("matcher1", "blah1"),
          ComponentCertificate("vr2", "blah2"))))
    val pathGlobal = Path(Paths.Configuration.getCertificateMeasurementMethodTypePath(entry1.typeId))
    zkStore.put(pathGlobal, globalType1)

    val locationCert = new LocationCertificate(
      id = "3215",
      inspectionDate = toDate("2012-01-01T00:00:00"),
      validTime = toDate("2012-12-31T00:00:00"),
      components = List(ComponentCertificate("irose.depend", "blah3")))
    val pathLocal = Path(Paths.Systems.getLocationCertificatePath(systemId))
    zkStore.put(pathLocal / locationCert.id, locationCert)

    val activeCertificatePath = Path(Paths.Systems.getActiveCertificatesPath(systemId))
    zkStore.put(activeCertificatePath / "matcher1", ActiveCertificate(System.currentTimeMillis(), ComponentCertificate("matcher1", "blah1")))
    zkStore.put(activeCertificatePath / "vr2", ActiveCertificate(System.currentTimeMillis(), ComponentCertificate("vr2", "blah2")))
    zkStore.put(activeCertificatePath / "irose.depend", ActiveCertificate(System.currentTimeMillis(), ComponentCertificate("irose.depend", "blah3")))
    val dayViolationsStatistics = DayViolationsStatistics(systemId, stats, Nil)
    val dayViolationsStatisticsPath = Path(Paths.Systems.getRuntimeViolationsPath(systemId, stats))
    zkStore.put(dayViolationsStatisticsPath, dayViolationsStatistics)
    zkStore.createEmptyPath(Path(Paths.Systems.getSystemEventPath(systemId)))
  }

  def createConfiguration2(systemId: String, stats: StatsDuration) {
    // system config
    val zkSystem = ZkSystem(id = "a2", name = "A2", title = "A2", mtmRouteId = "", violationPrefixId = "a002",
      location = ZkSystemLocation(
        description = "A2 Utrecht Amsterdam",
        region = "Randstad", viewingDirection = Some("Zuid-Oost"), roadNumber = "2", roadPart = "Links", systemLocation = "Amsterdam"),
      serialNumber = SerialNumber("777"), orderNumber = 1, maxSpeed = 100, compressionFactor = 4,
      retentionTimes = ZkSystemRetentionTimes(
        nrDaysKeepTrafficData = 11,
        nrDaysKeepViolations = 5, nrDaysRemindCaseFiles = 3),
      pardonTimes = ZkSystemPardonTimes(pardonTimeEsa_secs = 5, pardonTimeTechnical_secs = 3))
    zkStore.put(Path(Paths.Systems.getConfigPath(systemId)), zkSystem)
    // section
    val zkSection = ZkSection(id = "a", systemId = "a2", name = "A",
      length = 2983.0, startGantryId = "opritmaarsen",
      endGantryId = "afslagbreukelen", matrixBoards = Seq("989"), msiBlackList = Nil)
    val zkSectionPath = Path(Paths.Sections.getConfigPath(systemId, "a"))
    zkStore.put(zkSectionPath, zkSection)

    val zkSection_b = ZkSection(id = "b", systemId = "a2", name = "B",
      length = 2983.0, startGantryId = "opritmaarsen",
      endGantryId = "afslagbreukelen", matrixBoards = Seq("989"), msiBlackList = Nil)
    val zkSectionPath_b = Path(Paths.Sections.getConfigPath(systemId, "b"))
    zkStore.put(zkSectionPath_b, zkSection_b)

    //  gantries
    val startGantryMap = Map("id" -> "opritmaarsen", "name" -> "Oprit Maarsen",
      "systemId" -> "A2", "hectometer" -> "40")
    val startGantryMapPath = Path(Paths.Gantries.getConfigPath(systemId, "opritmaarsen"))
    zkStore.put(startGantryMapPath, startGantryMap)

    val endGantryMap = Map("id" -> "afslagbreukelen", "name" -> "Afslag Breukelen",
      "systemId" -> "A2", "hectometer" -> "50")
    val endGantryMapPath = Path(Paths.Gantries.getConfigPath(systemId, "afslagbreukelen"))
    zkStore.put(endGantryMapPath, endGantryMap)
    //lanes
    val startLaneMap = Map("id" -> "strook123", "name" -> "Strook 1", "seqNumber" -> Some("1"), "seqName" -> Some("RD"),
      "gantry" -> "opritmaarsen", "systemId" -> "A2")
    val startLaneMapPath = Path(Paths.Gantries.getLanesConfigPath(systemId, "opritmaarsen", "strook1"))
    zkStore.put(startLaneMapPath, startLaneMap)

    val endLaneMap = Map("id" -> "strook321", "name" -> "Strook 1", "seqNumber" -> Some("2"), "seqName" -> Some("RA"),
      "gantry" -> "afslagbreukelen", "systemId" -> "A2")
    val endLaneMapPath = Path(Paths.Gantries.getLanesConfigPath(systemId, "afslagbreukelen", "strook1"))
    zkStore.put(endLaneMapPath, endLaneMap)

    object Extensions {
      implicit def stringWrapper(value: String) = new StringExtensions(value)

      class StringExtensions(val value: String) {
        def toDate(implicit formatter: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")): Date = {
          formatter.parse(value)
        }
      }

    }

    // corridor
    val corrInfo = ZkCorridorInfo(corridorId = 1,
      locationCode = "1234",
      reportId = "TCS A2 Links  39.7 - 54.9",
      reportHtId = "A020011",
      reportPerformance = true,
      locationLine1 = "loc 1",
      locationLine2 = Some("loc 2"))

    val corrInfo_2 = ZkCorridorInfo(corridorId = 2,
      locationCode = "12345",
      reportId = "TCS A2 Links  39.7 - 54.9",
      reportHtId = "A020011",
      reportPerformance = true,
      locationLine1 = "loc 1",
      locationLine2 = Some("loc 2"))

    val corrInfo_3 = ZkCorridorInfo(corridorId = 3,
      locationCode = "123456",
      reportId = "TCS A2 Links  39.7 - 54.9",
      reportHtId = "A020011",
      reportPerformance = true,
      locationLine1 = "loc 1",
      locationLine2 = Some("loc 2"))

    val corridor_1 = ZkCorridor(id = "1", serviceType = ServiceType.SectionControl, name = "1", dynamaxMqId = "", approvedSpeeds = Seq(),
      pshtm = null, info = corrInfo, startSectionId = "a", endSectionId = "a", allSectionIds = Seq("a"), roadType = 1, isInsideUrbanArea = false,
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 404, roadCode = 2, codeText = "", dutyType = "KL06", deploymentCode = "03")
    val corridorPath = Path(Paths.Corridors.getConfigPath(systemId, "1"))
    zkStore.put(corridorPath, corridor_1)
    val serPathSection = Paths.Corridors.getServicesPath(systemId, "1") + "/" + ServiceType.SectionControl.toString
    val serviceConfigSection = new SectionControlConfig()
    zkStore.put(serPathSection, serviceConfigSection)

    val corridor_2 = ZkCorridor(id = "2", serviceType = ServiceType.RedLight, name = "2", dynamaxMqId = "", approvedSpeeds = Seq(),
      pshtm = null, info = corrInfo_2, startSectionId = "a", endSectionId = "b", allSectionIds = Seq("a", "b"), roadType = 1, isInsideUrbanArea = false,
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 404, roadCode = 2, codeText = "", dutyType = "KL06", deploymentCode = "03")

    val corridorPath_2 = Path(Paths.Corridors.getConfigPath(systemId, "2"))
    zkStore.put(corridorPath_2, corridor_2)
    val serPath = Paths.Corridors.getServicesPath(systemId, "2") + "/" + ServiceType.RedLight.toString
    val serviceConfig = new RedLightConfig(
      flgFileName = Some("roodlicht.flg"),
      dynamicEnforcement = false,
      pardonRedTime = 1000,
      minimumYellowTime = 1000,
      factNumber = "12345")
    zkStore.put(serPath, serviceConfig)

    val corridor_3 = ZkCorridor(id = "3", serviceType = ServiceType.SpeedFixed, name = "3", dynamaxMqId = "", approvedSpeeds = Seq(),
      pshtm = null, info = corrInfo_3, startSectionId = "a", endSectionId = "b", allSectionIds = Seq("a", "b"), roadType = 1, isInsideUrbanArea = false,
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 404, roadCode = 2, codeText = "", dutyType = "KL06", deploymentCode = "03")

    val corridorPath_3 = Path(Paths.Corridors.getConfigPath(systemId, "3"))
    zkStore.put(corridorPath_3, corridor_3)
    val serPathSpeed = Paths.Corridors.getServicesPath(systemId, "3") + "/" + ServiceType.SpeedFixed.toString
    val serviceConfigSpeed = new SpeedFixedConfig(
      flgFileName = Some("snelheid.flg"),
      dynamicEnforcement = false)
    zkStore.put(serPathSpeed, serviceConfigSpeed)

    // val fcorridor = List(ZkFunctionalCorridor("a", "b", 999, "TCS A2 Links  39.7 - 54.9", "A020011", true))
    // val fcorridorPath = Path(Paths.Systems.getFunctionalCorridorPath(systemId))
    // zkStore.put(fcorridorPath, fcorridor)

    //schedules
    val schedules_cor_1 = List(new ZkSchedule(id = "1",
      indicationRoadworks = ZkIndicationType.None,
      indicationDanger = ZkIndicationType.None,
      indicationActualWork = ZkIndicationType.None,
      invertDrivingDirection = ZkIndicationType.None,
      fromPeriodHour = 0,
      fromPeriodMinute = 0,
      toPeriodHour = 24,
      toPeriodMinute = 0,
      speedLimit = 100,
      signSpeed = 100,
      signIndicator = true,
      supportMsgBoard = false))

    val schedules_cor_2 = List(new ZkSchedule(id = "2",
      indicationRoadworks = ZkIndicationType.None,
      indicationDanger = ZkIndicationType.None,
      indicationActualWork = ZkIndicationType.None,
      invertDrivingDirection = ZkIndicationType.None,
      fromPeriodHour = 6,
      fromPeriodMinute = 0,
      toPeriodHour = 19,
      toPeriodMinute = 0,
      speedLimit = 100,
      signSpeed = 100,
      signIndicator = true,
      supportMsgBoard = false))

    val schedules_cor_3 = List(new ZkSchedule(id = "3",
      indicationRoadworks = ZkIndicationType.None,
      indicationDanger = ZkIndicationType.None,
      indicationActualWork = ZkIndicationType.None,
      invertDrivingDirection = ZkIndicationType.None,
      fromPeriodHour = 0,
      fromPeriodMinute = 0,
      toPeriodHour = 6,
      toPeriodMinute = 0,
      speedLimit = 130,
      signSpeed = 130,
      signIndicator = true,
      supportMsgBoard = false),
      new ZkSchedule(id = "4",
        indicationRoadworks = ZkIndicationType.None,
        indicationDanger = ZkIndicationType.None,
        indicationActualWork = ZkIndicationType.None,
        invertDrivingDirection = ZkIndicationType.None,
        fromPeriodHour = 19,
        fromPeriodMinute = 0,
        toPeriodHour = 24,
        toPeriodMinute = 0,
        speedLimit = 130,
        signSpeed = 130,
        signIndicator = true,
        supportMsgBoard = false))

    val schedulePath_1 = Path(Paths.Corridors.getSchedulesPath(systemId, "1"))
    val schedulePath_2 = Path(Paths.Corridors.getSchedulesPath(systemId, "2"))
    val schedulePath_3 = Path(Paths.Corridors.getSchedulesPath(systemId, "3"))
    zkStore.put(schedulePath_1, schedules_cor_1)
    zkStore.put(schedulePath_2, schedules_cor_2)
    zkStore.put(schedulePath_3, schedules_cor_3)

    // certificates
    val entry1 = new MeasurementMethodInstallation(typeId = "entry1", timeFrom = toDate("2012-01-01T00:00:00"))
    val path = Path(Paths.Systems.getInstallCertificatePath(systemId))
    zkStore.put(path / "entry1", entry1)

    val globalType1 = new MeasurementMethodType(typeDesignation = "XX123",
      category = "A",
      unitSpeed = "km/h",
      unitRedLight = "s",
      unitLength = "m",
      restrictiveConditions = "",
      displayRange = "20-250 km/h",
      temperatureRange = "temperatuurbereik -10 to 50 graden Celsius",
      permissibleError = "toelatbare fout 3%",
      typeCertificate = new TypeCertificate("eg33-cert", 0,
        List(ComponentCertificate("matcher1", "blah1"),
          ComponentCertificate("vr2", "blah2"))))
    val pathGlobal = Path(Paths.Configuration.getCertificateMeasurementMethodTypePath(entry1.typeId))
    zkStore.put(pathGlobal, globalType1)

    val locationCert = new LocationCertificate(
      id = "3215",
      inspectionDate = toDate("2012-01-01T00:00:00"),
      validTime = toDate("2012-12-31T00:00:00"),
      components = List(ComponentCertificate("irose.depend", "blah3")))
    val pathLocal = Path(Paths.Systems.getLocationCertificatePath(systemId))
    zkStore.put(pathLocal / locationCert.id, locationCert)

    val activeCertificatePath = Path(Paths.Systems.getActiveCertificatesPath(systemId))
    zkStore.put(activeCertificatePath / "matcher1", ActiveCertificate(System.currentTimeMillis(), ComponentCertificate("matcher1", "blah1")))
    zkStore.put(activeCertificatePath / "vr2", ActiveCertificate(System.currentTimeMillis(), ComponentCertificate("vr2", "blah2")))
    zkStore.put(activeCertificatePath / "irose.depend", ActiveCertificate(System.currentTimeMillis(), ComponentCertificate("irose.depend", "blah3")))
    val dayViolationsStatistics = DayViolationsStatistics(systemId, stats, Nil)
    val dayViolationsStatisticsPath = Path(Paths.Systems.getRuntimeViolationsPath(systemId, stats))
    zkStore.put(dayViolationsStatisticsPath, dayViolationsStatistics)
    zkStore.createEmptyPath(Path(Paths.Systems.getSystemEventPath(systemId)))
  }

  def createConfiguration3(systemId: String, stats: StatsDuration) {
    // system config
    val zkSystem = ZkSystem(id = "a2", name = "A2", title = "A2", mtmRouteId = "", violationPrefixId = "a002",
      location = ZkSystemLocation(
        description = "A2 Utrecht Amsterdam",
        region = "Randstad", viewingDirection = Some("Zuid-Oost"), roadNumber = "2", roadPart = "Links",
        systemLocation = "Amsterdam"),
      serialNumber = SerialNumber("777"), orderNumber = 1, maxSpeed = 100, compressionFactor = 4,
      retentionTimes = ZkSystemRetentionTimes(
        nrDaysKeepTrafficData = 11,
        nrDaysKeepViolations = 5, nrDaysRemindCaseFiles = 3),
      pardonTimes = ZkSystemPardonTimes(pardonTimeEsa_secs = 5000, pardonTimeTechnical_secs = 5000))
    zkStore.put(Path(Paths.Systems.getConfigPath(systemId)), zkSystem)
    // section
    val zkSection = ZkSection(id = "a", systemId = "a2", name = "A",
      length = 2983.0, startGantryId = "opritmaarsen",
      endGantryId = "afslagbreukelen", matrixBoards = Seq("989"), msiBlackList = Nil)
    val zkSectionPath = Path(Paths.Sections.getConfigPath(systemId, "a"))
    zkStore.put(zkSectionPath, zkSection)

    val zkSection_b = ZkSection(id = "b", systemId = "a2", name = "B",
      length = 2983.0, startGantryId = "opritmaarsen",
      endGantryId = "afslagbreukelen", matrixBoards = Seq("989"), msiBlackList = Nil)
    val zkSectionPath_b = Path(Paths.Sections.getConfigPath(systemId, "b"))
    zkStore.put(zkSectionPath_b, zkSection_b)

    //  gantries
    val startGantryMap = Map("id" -> "opritmaarsen", "name" -> "Oprit Maarsen",
      "systemId" -> "A2", "hectometer" -> "40")
    val startGantryMapPath = Path(Paths.Gantries.getConfigPath(systemId, "opritmaarsen"))
    zkStore.put(startGantryMapPath, startGantryMap)

    val endGantryMap = Map("id" -> "afslagbreukelen", "name" -> "Afslag Breukelen",
      "systemId" -> "A2", "hectometer" -> "50")
    val endGantryMapPath = Path(Paths.Gantries.getConfigPath(systemId, "afslagbreukelen"))
    zkStore.put(endGantryMapPath, endGantryMap)
    //lanes
    val startLaneMap = Map("id" -> "strook123", "name" -> "Strook 1", "seqNumber" -> Some("1"), "seqName" -> Some("RD"),
      "gantry" -> "opritmaarsen", "systemId" -> "A2")
    val startLaneMapPath = Path(Paths.Gantries.getLanesConfigPath(systemId, "opritmaarsen", "strook1"))
    zkStore.put(startLaneMapPath, startLaneMap)

    val endLaneMap = Map("id" -> "strook321", "name" -> "Strook 1", "seqNumber" -> Some("2"), "seqName" -> Some("RA"),
      "gantry" -> "afslagbreukelen", "systemId" -> "A2")
    val endLaneMapPath = Path(Paths.Gantries.getLanesConfigPath(systemId, "afslagbreukelen", "strook1"))
    zkStore.put(endLaneMapPath, endLaneMap)

    object Extensions {
      implicit def stringWrapper(value: String) = new StringExtensions(value)

      class StringExtensions(val value: String) {
        def toDate(implicit formatter: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")): Date = {
          formatter.parse(value)
        }
      }

    }

    // corridor
    val corrInfo = ZkCorridorInfo(corridorId = 1,
      locationCode = "1234",
      reportId = "TCS A2 Links  39.7 - 54.9",
      reportHtId = "A020011",
      reportPerformance = true,
      locationLine1 = "loc 1", locationLine2 = Some("loc 2"))

    val corrInfo_2 = ZkCorridorInfo(corridorId = 2,
      locationCode = "12345",
      reportId = "TCS A2 Links  39.7 - 54.9",
      reportHtId = "A020011",
      reportPerformance = true,
      locationLine1 = "loc 1", locationLine2 = Some("loc 2"))

    val corrInfo_3 = ZkCorridorInfo(corridorId = 3,
      locationCode = "123456",
      reportId = "TCS A2 Links  39.7 - 54.9",
      reportHtId = "A020011",
      reportPerformance = true,
      locationLine1 = "loc 1", locationLine2 = Some("loc 2"))

    val corridor_1 = ZkCorridor(id = "1", serviceType = ServiceType.SectionControl, name = "1", dynamaxMqId = "", approvedSpeeds = Seq(),
      pshtm = null, info = corrInfo, startSectionId = "a", endSectionId = "a", allSectionIds = Seq("a"), roadType = 1, isInsideUrbanArea = false,
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 404, roadCode = 2, codeText = "", dutyType = "KL06", deploymentCode = "03")

    val corridorPath = Path(Paths.Corridors.getConfigPath(systemId, "1"))
    zkStore.put(corridorPath, corridor_1)
    val serPathSection = Paths.Corridors.getServicesPath(systemId, "1") + "/" + ServiceType.SectionControl.toString
    val serviceConfigSection = new SectionControlConfig()
    zkStore.put(serPathSection, serviceConfigSection)

    val corridor_2 = ZkCorridor(id = "2", serviceType = ServiceType.RedLight, name = "2", dynamaxMqId = "", approvedSpeeds = Seq(),
      pshtm = null, info = corrInfo_2, startSectionId = "a", endSectionId = "b", allSectionIds = Seq("a", "b"), roadType = 1, isInsideUrbanArea = false,
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 404, roadCode = 2, codeText = "", dutyType = "KL06", deploymentCode = "03")

    val corridorPath_2 = Path(Paths.Corridors.getConfigPath(systemId, "2"))
    zkStore.put(corridorPath_2, corridor_2)
    val serPath = Paths.Corridors.getServicesPath(systemId, "2") + "/" + ServiceType.RedLight.toString
    val serviceConfig = new RedLightConfig(
      flgFileName = Some("roodlicht.flg"),
      dynamicEnforcement = false,
      pardonRedTime = 1000,
      minimumYellowTime = 1000,
      factNumber = "12345")
    zkStore.put(serPath, serviceConfig)

    val corridor_3 = ZkCorridor(id = "3", serviceType = ServiceType.SpeedFixed, name = "3", dynamaxMqId = "", approvedSpeeds = Seq(),
      pshtm = null, info = corrInfo_3, startSectionId = "a", endSectionId = "b", allSectionIds = Seq("a", "b"), roadType = 1, isInsideUrbanArea = false,
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 404, roadCode = 2, codeText = "", dutyType = "KL06", deploymentCode = "03")

    val corridorPath_3 = Path(Paths.Corridors.getConfigPath(systemId, "3"))
    zkStore.put(corridorPath_3, corridor_3)
    val serPathSpeed = Paths.Corridors.getServicesPath(systemId, "3") + "/" + ServiceType.SpeedFixed.toString
    val serviceConfigSpeed = new SpeedFixedConfig(
      flgFileName = Some("snelheid.flg"),
      dynamicEnforcement = false)
    zkStore.put(serPathSpeed, serviceConfigSpeed)

    //val fcorridor = List(ZkFunctionalCorridor("a", "b", 999, "TCS A2 Links  39.7 - 54.9", "A020011", true))
    //val fcorridorPath = Path(Paths.Systems.getFunctionalCorridorPath(systemId))
    //zkStore.put(fcorridorPath, fcorridor)

    //schedules
    val schedules_cor_1 = List(new ZkSchedule(id = "1",
      indicationRoadworks = ZkIndicationType.None,
      indicationDanger = ZkIndicationType.None,
      indicationActualWork = ZkIndicationType.None,
      invertDrivingDirection = ZkIndicationType.None,
      fromPeriodHour = 0,
      fromPeriodMinute = 0,
      toPeriodHour = 24,
      toPeriodMinute = 0,
      speedLimit = 100,
      signSpeed = 100,
      signIndicator = true,
      supportMsgBoard = false))

    val schedules_cor_2 = List(new ZkSchedule(id = "2",
      indicationRoadworks = ZkIndicationType.None,
      indicationDanger = ZkIndicationType.None,
      indicationActualWork = ZkIndicationType.None,
      invertDrivingDirection = ZkIndicationType.None,
      fromPeriodHour = 0,
      fromPeriodMinute = 0,
      toPeriodHour = 24,
      toPeriodMinute = 0,
      speedLimit = 100,
      signSpeed = 100,
      signIndicator = true,
      supportMsgBoard = false))

    val schedules_cor_3 = List(new ZkSchedule(id = "3",
      indicationRoadworks = ZkIndicationType.None,
      indicationDanger = ZkIndicationType.None,
      indicationActualWork = ZkIndicationType.None,
      invertDrivingDirection = ZkIndicationType.None,
      fromPeriodHour = 0,
      fromPeriodMinute = 0,
      toPeriodHour = 24,
      toPeriodMinute = 0,
      speedLimit = 130,
      signSpeed = 130,
      signIndicator = true,
      supportMsgBoard = false))

    val schedulePath_1 = Path(Paths.Corridors.getSchedulesPath(systemId, "1"))
    val schedulePath_2 = Path(Paths.Corridors.getSchedulesPath(systemId, "2"))
    val schedulePath_3 = Path(Paths.Corridors.getSchedulesPath(systemId, "3"))
    zkStore.put(schedulePath_1, schedules_cor_1)
    zkStore.put(schedulePath_2, schedules_cor_2)
    zkStore.put(schedulePath_3, schedules_cor_3)

    // certificates
    val entry1 = new MeasurementMethodInstallation(typeId = "entry1", timeFrom = toDate("2012-01-01T00:00:00"))
    val path = Path(Paths.Systems.getInstallCertificatePath(systemId))
    zkStore.put(path / "entry1", entry1)

    val globalType1 = new MeasurementMethodType(typeDesignation = "XX123",
      category = "A",
      unitSpeed = "km/h",
      unitRedLight = "s",
      unitLength = "m",
      restrictiveConditions = "",
      displayRange = "20-250 km/h",
      temperatureRange = "temperatuurbereik -10 to 50 graden Celsius",
      permissibleError = "toelatbare fout 3%",
      typeCertificate = new TypeCertificate("eg33-cert", 0,
        List(ComponentCertificate("matcher1", "blah1"),
          ComponentCertificate("vr2", "blah2"))))
    val pathGlobal = Path(Paths.Configuration.getCertificateMeasurementMethodTypePath(entry1.typeId))
    zkStore.put(pathGlobal, globalType1)

    val locationCert = new LocationCertificate(
      id = "3215",
      inspectionDate = toDate("2012-01-01T00:00:00"),
      validTime = toDate("2012-12-31T00:00:00"),
      components = List(ComponentCertificate("irose.depend", "blah3")))
    val pathLocal = Path(Paths.Systems.getLocationCertificatePath(systemId))
    zkStore.put(pathLocal / locationCert.id, locationCert)

    val activeCertificatePath = Path(Paths.Systems.getActiveCertificatesPath(systemId))
    zkStore.put(activeCertificatePath / "matcher1", ActiveCertificate(System.currentTimeMillis(), ComponentCertificate("matcher1", "blah1")))
    zkStore.put(activeCertificatePath / "vr2", ActiveCertificate(System.currentTimeMillis(), ComponentCertificate("vr2", "blah2")))
    zkStore.put(activeCertificatePath / "irose.depend", ActiveCertificate(System.currentTimeMillis(), ComponentCertificate("irose.depend", "blah3")))
    val dayViolationsStatistics = DayViolationsStatistics(systemId, stats, Nil)
    val dayViolationsStatisticsPath = Path(Paths.Systems.getRuntimeViolationsPath(systemId, stats))
    zkStore.put(dayViolationsStatisticsPath, dayViolationsStatistics)
    zkStore.createEmptyPath(Path(Paths.Systems.getSystemEventPath(systemId)))
  }
}
