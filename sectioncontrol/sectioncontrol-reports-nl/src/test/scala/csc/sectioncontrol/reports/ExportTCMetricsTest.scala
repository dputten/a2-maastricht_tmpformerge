package csc.sectioncontrol.reports

import java.util.Date

import csc.sectioncontrol.reports.export.{ ExportTCMetrics, DayReportBuilderV421Settings }
import csc.sectioncontrol.reports.metrics.{ Extensions, Period }
import csc.sectioncontrol.reports.metrics.common.{ MetricsData, Rounder }
import csc.sectioncontrol.storage.ZkState._
import csc.sectioncontrol.storage.ZkState
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.reports.metrics.Extensions._

import scala.xml.{ Node, Elem }

class ExportTCMetricsTest extends WordSpec with MustMatchers with Rounder with MetricsData {

  val settings = new DayReportBuilderV421Settings()

  override val NR_DECIMALS: Int = settings.NR_DECIMALS

  override def periods_130 =
    List(Period(from = "2012-12-29T00:00:00", to = "2012-12-29T24:00:00")) //single period, to simplify xml interpretation

  override val alerts = Nil //no alerts, so the storingenratio is 1 and we get simpler figures

  def systemStates =
    List(
      ZkState(enforceOn, "2012-12-29T00:00:00".toDate.getTime, "123", "test"),
      ZkState(standBy, "2012-12-29T12:00:00".toDate.getTime, "123", "test"),
      ZkState(Maintenance, "2012-12-29T14:00:00".toDate.getTime, "123", "test"),
      ZkState(failure, "2012-12-29T16:00:00".toDate.getTime, "123", "test"),
      ZkState(off, "2012-12-29T18:00:00".toDate.getTime, "123", "test"),
      ZkState(enforceDegraded, "2012-12-29T20:00:00".toDate.getTime, "123", "test"),
      ZkState(enforceOff, "2012-12-29T22:00:00".toDate.getTime, "123", "test"),
      ZkState(enforceOff, "2012-12-29T23:59:59".toDate.getTime, "123", "test")) //all the possible states

  "ExportTCMetrics XML" must {
    val expectedAvtRatio = 0.750

    roundBigDecimal(metrics.performance.avtRatio) must be(expectedAvtRatio) //avtRatio control

    val xml = ExportTCMetrics.createTCMetricsXml(statsDuration, metrics, settings)
    val avtRatio = bigDecimalOf(xml, "av-t-ratio")
    roundBigDecimal(avtRatio) must be(expectedAvtRatio) //checking the same
    val entries = xml \\ "handhaaflog-entry"
    entries.size must be(systemStates.size)

    "have a <passages> element" in {
      val passages = xml \\ "passages"
      passages.size must be(1)

    }

    "have correct 'handhaaflog-entry' and 'av-t-ratio'" in {
      val zipped = systemStates.zip(entries)
      zipped.foreach { e ⇒ checkEntry(e._1, e._2) }
    }

    "have the label for 'Off' the same as for 'Maintenance' (TCA-70)" in {
      val offIndex = systemStates.indexWhere(s ⇒ s.state == off)
      entries(offIndex).attribute("systeemtoestand").toString must be(Some(settings.translateState(off)).toString())
    }
  }

  def checkEntry(state: ZkState, node: Node): Unit = {
    node.attribute("systeemtoestand").toString must be(Some(settings.translateState(state.state)).toString())
    val date = Extensions.defaultDateFormat.format(new Date(state.timestamp))
    node.attribute("tijd").toString must be(Some(date).toString())
  }

  def bigDecimalOf(elem: Elem, that: String): BigDecimal = {
    val ns = elem \\ that
    ns.size must be(1)
    val node = ns(0)
    BigDecimal(node.text)
  }

}

