/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.reports

import org.xml.sax.InputSource
import scala.xml.parsing.NoBindingFactoryAdapter
import scala.xml.{ TopScope, Elem }
import javax.xml.parsers.{ SAXParserFactory, SAXParser }
import javax.xml.validation.Schema

class SchemaAwareFactoryAdapter(schema: Schema) extends NoBindingFactoryAdapter {
  override def loadXML(source: InputSource, parser: SAXParser) = {
    val reader = parser.getXMLReader()
    val handler = schema.newValidatorHandler()
    handler.setContentHandler(this)
    reader.setContentHandler(handler)

    scopeStack.push(TopScope)
    reader.parse(source)
    scopeStack.pop
    rootElem.asInstanceOf[Elem]
  }

  override def parser: SAXParser = {
    val factory = SAXParserFactory.newInstance()
    factory.setNamespaceAware(true)
    factory.setFeature("http://xml.org/sax/features/namespace-prefixes", true)
    factory.newSAXParser()
  }
}

