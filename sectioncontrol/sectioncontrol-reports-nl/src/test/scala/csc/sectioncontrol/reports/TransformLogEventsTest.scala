/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.reports

import metrics.Period
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storage.ZkSystem
import csc.dlog.LogEntry
import csc.sectioncontrol.messages.{ EsaProviderType, SystemEvent }
import akka.util.duration._
import java.util.Calendar
import csc.sectioncontrol.storagelayer.{ SectionControlConfig, CorridorConfig }
import csc.sectioncontrol.reports.schedules.CorridorSpeedSchedule

class TransformLogEventsTest extends WordSpec with MustMatchers {

  def createZkDailyReportConfig(): ZkDailyReportConfig = {
    val dailyReportConfigSpeedEntry100 = DailyReportConfigSpeedEntry("100", "TCS UT A2 R-1 (100 km/h)", "a0021001")
    val dailyReportConfigSpeedEntry130 = DailyReportConfigSpeedEntry("130", "TCS UT A2 R-1 (130 km/h)", "a0021301")
    val dailyReportConfigSpeedEntryTotal = DailyReportConfigSpeedEntry("total", "TCS UT A2 R-1 (totaal)", "a0021001, a0021301 (totaal)")
    val ignoreStatisticsOfLane1 = IgnoreStatisticsOfLane("A2", "E1", "E1R1")
    val ignoreStatisticsOfLane2 = IgnoreStatisticsOfLane("A2", "E1", "E1R2")
    val ignoreStatisticsOfLanes = List(ignoreStatisticsOfLane1, ignoreStatisticsOfLane2)
    val speedEntries = List(dailyReportConfigSpeedEntry100, dailyReportConfigSpeedEntry130, dailyReportConfigSpeedEntryTotal)
    ZkDailyReportConfig(ignoreStatisticsOfLanes, speedEntries)
  }

  "getCorridorMapping" must {
    "create Empty mapping when systemData is empty" in {
      val systemData = getSystemData.copy(corridors = List())
      val mapping = TransformLogEvents.getCorridorMapping(systemData)
      mapping.size must be(0)
    }
    "create mapping" in {
      val systemData = getSystemData
      val mapping = TransformLogEvents.getCorridorMapping(systemData)
      mapping.size must be(2)
      mapping.get("E1X1") must be(Some("a0020011"))
      mapping.get("E2X2") must be(Some("a0020012"))
    }
  }
  "getGantryMapping" must {
    "create Empty mapping when systemData is empty" in {
      val systemData = getSystemData.copy(corridors = List())
      val mapping = TransformLogEvents.getGantryMapping(systemData, Map(), System.currentTimeMillis())
      mapping.size must be(0)
    }
    "create mapping even when schedules are empty" in {
      val systemData = getSystemData
      val mapping = TransformLogEvents.getGantryMapping(systemData, Map(), System.currentTimeMillis())
      mapping.size must be(4)
      val e1 = mapping.get("E1").get
      e1.size must be(1)
      e1.head.corridorLogId must be("a0020011")
      val x1 = mapping.get("X1").get
      x1.size must be(1)
      x1.head.corridorLogId must be("a0020011")

      val e2 = mapping.get("E2").get
      e2.size must be(1)
      e2.head.corridorLogId must be("a0020012")

      val x2 = mapping.get("X2").get
      x2.size must be(1)
      x2.head.corridorLogId must be("a0020012")
    }
    /*
    "create mapping with double corridors without functional corridors" in {
      val systemData = getSystemDataWithDoubleCorridors()

      val mapping = TransformLogEvents.getGantryMapping(systemData, Map(), System.currentTimeMillis())
      mapping.size must be(2)
      val e1 = mapping.get("E1").get
      e1.size must be(2)
      e1.head.corridorLogId must be("a0020011")
      e1.last.corridorLogId must be("a0020017")
      val x1 = mapping.get("X1").get
      x1.size must be(2)
      x1.head.corridorLogId must be("a0020011")
      x1.last.corridorLogId must be("a0020017")
    }
    */
  }
  "getCorridorLogId" must {
    "get correct logId when getting different times" in {
      val startTime = System.currentTimeMillis()
      val mapping1 = new TransformLogEvents.GantryMapping(gantryId = "X1", corridorLogId = "a0020011",
        periods = Seq(new Period(startTime + 1.hour.toMillis, startTime + 2.hour.toMillis)))
      val mapping2 = new TransformLogEvents.GantryMapping(gantryId = "X1", corridorLogId = "a0020017",
        periods = Seq(new Period(startTime + 3.hour.toMillis, startTime + 4.hour.toMillis)))
      val gantryMapping = Map(
        mapping1.gantryId -> Seq(mapping1, mapping2))
      var mapResult = TransformLogEvents.getCorridorLogId(gantryMapping, "X1", startTime + 30.minutes.toMillis)
      mapResult must be(Some("a0020011"))
      mapResult = TransformLogEvents.getCorridorLogId(gantryMapping, "X1", startTime + 90.minutes.toMillis)
      mapResult must be(Some("a0020011"))
      mapResult = TransformLogEvents.getCorridorLogId(gantryMapping, "X1", startTime + 149.minutes.toMillis)
      mapResult must be(Some("a0020011"))
      mapResult = TransformLogEvents.getCorridorLogId(gantryMapping, "X1", startTime + 151.minutes.toMillis)
      mapResult must be(Some("a0020017"))
      mapResult = TransformLogEvents.getCorridorLogId(gantryMapping, "X1", startTime + 210.minutes.toMillis)
      mapResult must be(Some("a0020017"))
      mapResult = TransformLogEvents.getCorridorLogId(gantryMapping, "X1", startTime + 270.minutes.toMillis)
      mapResult must be(Some("a0020017"))
    }
    "get None when getting unknown gantry" in {
      val startTime = System.currentTimeMillis()
      val mapping1 = new TransformLogEvents.GantryMapping(gantryId = "X1", corridorLogId = "a0020011",
        periods = Seq(new Period(startTime + 1.hour.toMillis, startTime + 2.hour.toMillis)))
      val mapping2 = new TransformLogEvents.GantryMapping(gantryId = "X1", corridorLogId = "a0020017",
        periods = Seq(new Period(startTime + 3.hour.toMillis, startTime + 4.hour.toMillis)))
      val gantryMapping = Map(
        mapping1.gantryId -> Seq(mapping1, mapping2))
      var mapResult = TransformLogEvents.getCorridorLogId(gantryMapping, "X2", startTime + 30.minutes.toMillis)
      mapResult must be(None)
    }
    "get correct logId when only one corridor" in {
      val startTime = System.currentTimeMillis()
      val mapping1 = new TransformLogEvents.GantryMapping(gantryId = "X1", corridorLogId = "a0020011",
        periods = Seq(new Period(startTime + 1.hour.toMillis, startTime + 2.hour.toMillis)))
      val gantryMapping = Map(
        mapping1.gantryId -> Seq(mapping1))
      var mapResult = TransformLogEvents.getCorridorLogId(gantryMapping, "X1", startTime + 30.minutes.toMillis)
      mapResult must be(Some("a0020011"))
      mapResult = TransformLogEvents.getCorridorLogId(gantryMapping, "X1", startTime + 90.minutes.toMillis)
      mapResult must be(Some("a0020011"))
      mapResult = TransformLogEvents.getCorridorLogId(gantryMapping, "X1", startTime + 149.minutes.toMillis)
      mapResult must be(Some("a0020011"))
    }
  }
  "toReportRecords" must {
    "create ReportRecord when systemData is empty and gantry is filled" in {
      val systemData = getSystemData.copy(corridors = List())
      val startTime = System.currentTimeMillis()
      val logEvent = new LogEntry[SystemEvent](logId = 1, logEntryId = 1, seqnr = 1, timestamp = startTime,
        event = SystemEvent(componentId = "Camera",
          timestamp = startTime,
          systemId = "A2",
          eventType = "FTP",
          userId = "me",
          reason = Some(""),
          corridorId = Some(""),
          gantryId = Some("X1"),
          laneId = Some("R1")))

      val mapped = TransformLogEvents.toReportRecords(systemData, Map(), Seq(logEvent))
      mapped.size must be(1)
      val record = mapped.head
      record.corridorLogId must be("a002****")
      record.seqnr must be(logEvent.seqnr)
      record.timestamp must be(logEvent.timestamp)
      record.event must be(logEvent.event)
    }
    "create ReportRecord when systemData is empty and corridor is filled" in {
      val systemData = getSystemData.copy(corridors = List())
      val startTime = System.currentTimeMillis()
      val logEvent = new LogEntry[SystemEvent](logId = 1, logEntryId = 1, seqnr = 1, timestamp = startTime,
        event = SystemEvent(componentId = "Camera",
          timestamp = startTime,
          systemId = "A2",
          eventType = "FTP",
          userId = "me",
          reason = Some(""),
          corridorId = Some("E1X1")))

      val mapped = TransformLogEvents.toReportRecords(systemData, Map(), Seq(logEvent))
      mapped.size must be(1)
      val record = mapped.head
      record.corridorLogId must be("a002****")
      record.seqnr must be(logEvent.seqnr)
      record.timestamp must be(logEvent.timestamp)
      record.event must be(logEvent.event)
    }
    "create ReportRecord when systemData is empty" in {
      val systemData = getSystemData.copy(corridors = List())
      val startTime = System.currentTimeMillis()
      val logEvent = new LogEntry[SystemEvent](logId = 1, logEntryId = 1, seqnr = 1, timestamp = startTime,
        event = SystemEvent(componentId = "Camera",
          timestamp = startTime,
          systemId = "A2",
          eventType = "FTP",
          userId = "me"))

      val mapped = TransformLogEvents.toReportRecords(systemData, Map(), Seq(logEvent))
      mapped.size must be(1)
      val record = mapped.head
      record.corridorLogId must be("a002****")
      record.seqnr must be(logEvent.seqnr)
      record.timestamp must be(logEvent.timestamp)
      record.event must be(logEvent.event)
    }
    "create ReportRecord when gantry and corridor are empty" in {
      val systemData = getSystemData
      val startTime = System.currentTimeMillis()
      val logEvent = new LogEntry[SystemEvent](logId = 1, logEntryId = 1, seqnr = 1, timestamp = startTime,
        event = SystemEvent(componentId = "Camera",
          timestamp = startTime,
          systemId = "A2",
          eventType = "FTP",
          userId = "me"))

      val mapped = TransformLogEvents.toReportRecords(systemData, Map(), Seq(logEvent))
      mapped.size must be(1)
      val record = mapped.head
      record.corridorLogId must be("a002****")
      record.seqnr must be(logEvent.seqnr)
      record.timestamp must be(logEvent.timestamp)
      record.event must be(logEvent.event)
    }
    "create ReportRecord when gantry is filled" in {
      val systemData = getSystemData
      val startTime = System.currentTimeMillis()
      val logEvent = new LogEntry[SystemEvent](logId = 1, logEntryId = 1, seqnr = 1, timestamp = startTime,
        event = SystemEvent(componentId = "Camera",
          timestamp = startTime,
          systemId = "A2",
          eventType = "FTP",
          userId = "me",
          gantryId = Some("X1"),
          laneId = Some("R1")))

      val mapped = TransformLogEvents.toReportRecords(systemData, Map(), Seq(logEvent))
      mapped.size must be(1)
      val record = mapped.head
      record.corridorLogId must be("a0020011")
      record.seqnr must be(logEvent.seqnr)
      record.timestamp must be(logEvent.timestamp)
      record.event must be(logEvent.event)
    }
    "create ReportRecord when corridor is filled" in {
      val systemData = getSystemData
      val startTime = System.currentTimeMillis()
      val logEvent = new LogEntry[SystemEvent](logId = 1, logEntryId = 1, seqnr = 1, timestamp = startTime,
        event = SystemEvent(componentId = "Camera",
          timestamp = startTime,
          systemId = "A2",
          eventType = "FTP",
          userId = "me" //,corridorId = Some("E1X1")
          ).copy(corridorId = Some("E1X1")))

      val mapped = TransformLogEvents.toReportRecords(systemData, Map(), Seq(logEvent))
      mapped.size must be(1)
      val record = mapped.head
      record.corridorLogId must be("a0020011")
      record.seqnr must be(logEvent.seqnr)
      record.timestamp must be(logEvent.timestamp)
      record.event must be(logEvent.event)
    }
    "create ReportRecord using corridor when corridor and gantry are filled" in {
      val systemData = getSystemData
      val startTime = System.currentTimeMillis()
      val logEvent = new LogEntry[SystemEvent](logId = 1, logEntryId = 1, seqnr = 1, timestamp = startTime,
        event = SystemEvent(componentId = "Camera",
          timestamp = startTime,
          systemId = "A2",
          eventType = "FTP",
          userId = "me",
          corridorId = Some("E2X2"),
          gantryId = Some("E1"),
          laneId = Some("R1")))

      val mapped = TransformLogEvents.toReportRecords(systemData, Map(), Seq(logEvent))
      mapped.size must be(1)
      val record = mapped.head
      record.corridorLogId must be("a0020012")
      record.seqnr must be(logEvent.seqnr)
      record.timestamp must be(logEvent.timestamp)
      record.event must be(logEvent.event)
    }
    "create ReportRecord when multiple corridors with same gantry" in {
      val systemData = getSystemDataWithDoubleCorridors()
      val startTime = System.currentTimeMillis()
      val schedules = getSchedules(startTime)

      val cal = Calendar.getInstance()
      cal.set(Calendar.HOUR_OF_DAY, 6)
      cal.set(Calendar.MINUTE, 30)
      val eventTime1 = cal.getTime.getTime

      val logEvent1 = new LogEntry[SystemEvent](logId = 1, logEntryId = 1, seqnr = 1, timestamp = eventTime1,
        event = SystemEvent(componentId = "Camera",
          timestamp = eventTime1,
          systemId = "A2",
          eventType = "FTP",
          userId = "me",
          gantryId = Some("X1"),
          laneId = Some("R1")))
      cal.set(Calendar.HOUR_OF_DAY, 12)
      cal.set(Calendar.MINUTE, 30)
      val eventTime2 = cal.getTime.getTime
      val logEvent2 = new LogEntry[SystemEvent](logId = 1, logEntryId = 1, seqnr = 2, timestamp = eventTime2,
        event = SystemEvent(componentId = "Camera",
          timestamp = eventTime2,
          systemId = "A2",
          eventType = "FTP",
          userId = "me",
          gantryId = Some("X1"),
          laneId = Some("R1")))

      val mapped = TransformLogEvents.toReportRecords(systemData, schedules, Seq(logEvent1, logEvent2))
      mapped.size must be(2)
      val record1 = mapped.head
      record1.corridorLogId must be("a0020011")
      record1.seqnr must be(logEvent1.seqnr)
      record1.timestamp must be(logEvent1.timestamp)
      record1.event must be(logEvent1.event)
      val record2 = mapped.last
      record2.corridorLogId must be("a0020017")
      record2.seqnr must be(logEvent2.seqnr)
      record2.timestamp must be(logEvent2.timestamp)
      record2.event must be(logEvent2.event)
    }

  }

  private def getSystemDataWithDoubleCorridors(): SystemData = {
    val systemInfo = ZkSystem(id = "A2",
      name = "A2",
      title = "A2",
      violationPrefixId = "a002",
      location = ZkSystemLocation(
        description = "",
        region = "",
        roadNumber = "2",
        roadPart = "",
        viewingDirection = None,
        systemLocation = "Amsterdam"),
      orderNumber = 1,
      maxSpeed = 130,
      compressionFactor = 20,
      retentionTimes = ZkSystemRetentionTimes(
        nrDaysKeepTrafficData = 28,
        nrDaysKeepViolations = 7,
        nrDaysRemindCaseFiles = 5),
      pardonTimes = ZkSystemPardonTimes(pardonTimeEsa_secs = 5000, pardonTimeTechnical_secs = 5000),
      mtmRouteId = "",
      esaProviderType = EsaProviderType.NoProvider,
      minimumViolationTimeSeparation = 1, /*minutes*/
      serialNumber = SerialNumber("sn1234"))

    val zkCorr1 = new CorridorConfig(id = "E1X1",
      name = "section 1",
      roadType = 0,
      serviceType = ServiceType.SectionControl,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = false,
      dynamaxMqId = "",
      approvedSpeeds = Seq[Int](),
      pshtm = new ZkPSHTMIdentification(useYear = false,
        useMonth = false,
        useDay = false,
        useReportingOfficerId = false,
        useNumberOfTheDay = false,
        useLocationCode = false,
        useHHMCode = false,
        useTypeViolation = false,
        useFixedCharacter = false,
        fixedCharacter = "X"),
      info = new ZkCorridorInfo(corridorId = 11,
        locationCode = "1234",
        reportId = "TC UT A2 L-1 (100)",
        reportHtId = "a0020011",
        reportPerformance = true,
        locationLine1 = "loc 1",
        locationLine2 = Some("loc 2")),
      startSectionId = "E1X1",
      endSectionId = "E1X1",
      allSectionIds = Seq("E1X1"),
      serviceConfig = new SectionControlConfig())
    val zkSection1 = new ZkSection(id = "E1X1",
      systemId = "A2",
      name = "E1X1",
      length = 2000,
      startGantryId = "E1",
      endGantryId = "X1",
      matrixBoards = Nil,
      msiBlackList = Nil)

    val lane1Data1 = LaneData("A2-E1-R1", name = "R1", bpsLaneId = Some("1 HR R- L"))
    val startGantry1 = GantryData(id = "E1", name = "E1", hectometer = "42,7", lanes = Seq(lane1Data1))
    val lane2Data1 = LaneData("A2-X1-R1", name = "R1", bpsLaneId = Some("2 HR R- L"))
    val endGantry1 = GantryData(id = "X1", name = "X1", hectometer = "39,7", lanes = Seq(lane2Data1))
    val section1 = SectionData(zkSection1, startGantry1, endGantry1)

    def createZkDailyReportConfig(): ZkDailyReportConfig = {
      val dailyReportConfigSpeedEntry100 = DailyReportConfigSpeedEntry("100", "TCS UT A2 R-1 (100 km/h)", "a0021001")
      val dailyReportConfigSpeedEntry130 = DailyReportConfigSpeedEntry("130", "TCS UT A2 R-1 (130 km/h)", "a0021301")
      val dailyReportConfigSpeedEntryTotal = DailyReportConfigSpeedEntry("total", "TCS UT A2 R-1 (totaal)", "a0021001, a0021301 (totaal)")
      val ignoreStatisticsOfLane1 = IgnoreStatisticsOfLane("A2", "E1", "E1R1")
      val ignoreStatisticsOfLane2 = IgnoreStatisticsOfLane("A2", "E1", "E1R2")
      val ignoreStatisticsOfLanes = List(ignoreStatisticsOfLane1, ignoreStatisticsOfLane2)
      val speedEntries = List(dailyReportConfigSpeedEntry100, dailyReportConfigSpeedEntry130, dailyReportConfigSpeedEntryTotal)
      ZkDailyReportConfig(ignoreStatisticsOfLanes, speedEntries)
    }

    val corData1 = CorridorData(zkCorr1, section1, section1, createZkDailyReportConfig())

    val zkCorr2 = CorridorConfig(id = "E1X1-130",
      name = "section 130",
      roadType = 0,
      serviceType = ServiceType.SectionControl,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = false,
      dynamaxMqId = "",
      approvedSpeeds = Seq[Int](),
      pshtm = ZkPSHTMIdentification(useYear = false,
        useMonth = false,
        useDay = false,
        useReportingOfficerId = false,
        useNumberOfTheDay = false,
        useLocationCode = false,
        useHHMCode = false,
        useTypeViolation = false,
        useFixedCharacter = false,
        fixedCharacter = "X"),
      info = ZkCorridorInfo(corridorId = 17,
        locationCode = "1234",
        reportId = "TC UT A2 L-1 (130)",
        reportHtId = "a0020017",
        reportPerformance = true,
        locationLine1 = "loc 2",
        locationLine2 = Some("loc 3")),
      startSectionId = "E1X1",
      endSectionId = "E1X1",
      allSectionIds = Seq("E1X1"),
      serviceConfig = SectionControlConfig())

    val corData2 = CorridorData(zkCorr2, section1, section1, createZkDailyReportConfig())

    SystemData(systemInfo, List(corData1, corData2))
  }
  private def getSystemData: SystemData = {
    val systemInfo = ZkSystem(id = "A2",
      name = "A2",
      title = "A2",
      violationPrefixId = "a002",
      location = ZkSystemLocation(
        description = "",
        region = "",
        roadNumber = "2",
        roadPart = "",
        viewingDirection = None,
        systemLocation = "Amsterdam"),
      orderNumber = 1,
      maxSpeed = 130,
      compressionFactor = 20,
      retentionTimes = ZkSystemRetentionTimes(
        nrDaysKeepTrafficData = 28,
        nrDaysKeepViolations = 7,
        nrDaysRemindCaseFiles = 5),
      pardonTimes = ZkSystemPardonTimes(pardonTimeEsa_secs = 5000, pardonTimeTechnical_secs = 5000),
      mtmRouteId = "",
      esaProviderType = EsaProviderType.NoProvider,
      minimumViolationTimeSeparation = 1, /*minutes*/
      serialNumber = SerialNumber("sn1234"))

    val zkCorr1 = new CorridorConfig(id = "E1X1",
      name = "section 1",
      roadType = 0,
      serviceType = ServiceType.SectionControl,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = false,
      dynamaxMqId = "",
      approvedSpeeds = Seq[Int](),
      pshtm = new ZkPSHTMIdentification(useYear = false,
        useMonth = false,
        useDay = false,
        useReportingOfficerId = false,
        useNumberOfTheDay = false,
        useLocationCode = false,
        useHHMCode = false,
        useTypeViolation = false,
        useFixedCharacter = false,
        fixedCharacter = "X"),
      info = new ZkCorridorInfo(corridorId = 11,
        locationCode = "1234",
        reportId = "TC UT A2 L-1 (100)",
        reportHtId = "a0020011",
        reportPerformance = true,
        locationLine1 = "loc 1",
        locationLine2 = Some("loc 2")),
      startSectionId = "E1X1",
      endSectionId = "E1X1",
      allSectionIds = Seq("E1X1"),
      serviceConfig = new SectionControlConfig())
    val zkSection1 = new ZkSection(id = "E1X1",
      systemId = "A2",
      name = "E1X1",
      length = 2000,
      startGantryId = "E1",
      endGantryId = "X1",
      matrixBoards = Nil,
      msiBlackList = Nil)

    val lane1Data1 = LaneData("A2-E1-R1", name = "R1", bpsLaneId = Some("1 HR R- L"))
    val startGantry1 = GantryData(id = "E1", name = "E1", hectometer = "42,7", lanes = Seq(lane1Data1))
    val lane2Data1 = LaneData("A2-X1-R1", name = "R1", bpsLaneId = Some("2 HR R- L"))
    val endGantry1 = GantryData(id = "X1", name = "X1", hectometer = "39,7", lanes = Seq(lane2Data1))
    val section1 = SectionData(zkSection1, startGantry1, endGantry1)
    val corData1 = CorridorData(zkCorr1, section1, section1, createZkDailyReportConfig())

    val zkCorr2 = CorridorConfig(id = "E2X2",
      name = "section 2",
      roadType = 0,
      serviceType = ServiceType.SectionControl,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = false,
      dynamaxMqId = "",
      approvedSpeeds = Seq[Int](),
      pshtm = ZkPSHTMIdentification(useYear = false,
        useMonth = false,
        useDay = false,
        useReportingOfficerId = false,
        useNumberOfTheDay = false,
        useLocationCode = false,
        useHHMCode = false,
        useTypeViolation = false,
        useFixedCharacter = false,
        fixedCharacter = "X"),
      info = ZkCorridorInfo(corridorId = 12,
        locationCode = "1234",
        reportId = "TC UT A2 L-1 (100)",
        reportHtId = "a0020012",
        reportPerformance = true,
        locationLine1 = "loc 2",
        locationLine2 = Some("loc 3")),
      startSectionId = "E2X2",
      endSectionId = "E2X2",
      allSectionIds = Seq("E2X2"),
      serviceConfig = SectionControlConfig())
    val zkSection2 = ZkSection(id = "E2X2",
      systemId = "A2",
      name = "E2X2",
      length = 2000,
      startGantryId = "E2",
      endGantryId = "X2",
      matrixBoards = Nil,
      msiBlackList = Nil)

    val lane1Data2 = LaneData("A2-E2-R1", name = "R1", bpsLaneId = Some("1 HR R- L"))
    val startGantry2 = GantryData(id = "E2", name = "E2", hectometer = "48,7", lanes = Seq(lane1Data2))
    val lane2Data2 = LaneData("A2-X2-R1", name = "R1", bpsLaneId = Some("2 HR R- L"))
    val endGantry2 = GantryData(id = "X2", name = "X2", hectometer = "44,1", lanes = Seq(lane2Data2))
    val section2 = SectionData(zkSection2, startGantry2, endGantry2)

    val corData2 = CorridorData(zkCorr2, section2, section2, createZkDailyReportConfig())

    SystemData(systemInfo, List(corData1, corData2))
  }

  def getSchedules(startTime: Long): Map[Int, Seq[CorridorSpeedSchedule]] = {
    val cal = Calendar.getInstance()
    cal.setTimeInMillis(startTime)
    cal.set(Calendar.HOUR_OF_DAY, 6)
    cal.set(Calendar.MINUTE, 0)
    val start1 = cal.getTimeInMillis

    cal.set(Calendar.HOUR_OF_DAY, 7)
    val end1 = cal.getTimeInMillis
    val s1 = CorridorSpeedSchedule(id = "s1", infoId = 11, speedLimit = 130, startTime = start1, endTime = end1)

    cal.set(Calendar.HOUR_OF_DAY, 8)
    val start2 = cal.getTimeInMillis

    cal.set(Calendar.HOUR_OF_DAY, 9)
    val end2 = cal.getTimeInMillis
    val s2 = CorridorSpeedSchedule(id = "s2", infoId = 17, speedLimit = 130, startTime = start2, endTime = end2)
    /*
    val schedule1 = new ZkSchedule(id = "s1",
      indicationRoadworks = ZkIndicationType.False,
      indicationDanger = ZkIndicationType.False,
      indicationActualWork = ZkIndicationType.False,
      invertDrivingDirection = ZkIndicationType.False,
      fromPeriodHour = 6,
      fromPeriodMinute = 0,
      toPeriodHour = 7,
      toPeriodMinute = 0,
      speedLimit = 130,
      signSpeed = 130,
      signIndicator = false,
      supportMsgBoard = false)

    val schedule2 = schedule1.copy(id = "s2",
      fromPeriodHour = 8,
      fromPeriodMinute = 0,
      toPeriodHour = 9,
      toPeriodMinute = 0)
*/
    Map(11 -> Seq(s1),
      17 -> Seq(s2))
  }
}