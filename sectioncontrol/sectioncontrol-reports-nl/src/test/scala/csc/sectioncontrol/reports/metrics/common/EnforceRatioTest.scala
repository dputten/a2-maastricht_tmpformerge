/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.reports.metrics.common

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.reports.metrics.MetricsGenerator
import csc.sectioncontrol.common.DayUtility
import akka.util.duration._
import csc.sectioncontrol.storage.ZkState

class EnforceRatioTest extends WordSpec with MustMatchers {
  val now = System.currentTimeMillis()
  val startDay = DayUtility.getStartDay(now)
  val endDay = DayUtility.getEndDay(now)
  "getEnforceRatio" must {
    "process empty state list" in {
      MetricsGenerator.getEnforceRatio(startDay, endDay, Seq()) must be(0F)
    }
    "process 1 state (not enforce) before start of day" in {
      val state = new ZkState(state = "StandBy", timestamp = startDay - 2.days.toMillis, reason = "test", userId = "123")
      MetricsGenerator.getEnforceRatio(startDay, endDay, Seq(state)) must be(0F)
    }

    "process 1 state before start of day" in {
      val state = new ZkState(state = "EnforceOn", timestamp = startDay - 2.days.toMillis, reason = "test", userId = "123")
      MetricsGenerator.getEnforceRatio(startDay, endDay, Seq(state)) must be(1F)
    }
    "process 1 state after start of day" in {
      val state = new ZkState(state = "EnforceOn", timestamp = startDay + 12.hours.toMillis, reason = "test", userId = "123")
      MetricsGenerator.getEnforceRatio(startDay, endDay, Seq(state)) must be(0.5F)
    }
    "process 1 state after end of day" in {
      val state = new ZkState(state = "EnforceOn", timestamp = startDay + 25.hours.toMillis, reason = "test", userId = "123")
      MetricsGenerator.getEnforceRatio(startDay, endDay, Seq(state)) must be(0F)
    }
    "process multiple states with different enforce type" in {
      val state1 = new ZkState(state = "EnforceOn", timestamp = startDay - 2.days.toMillis, reason = "test", userId = "123")
      val state2 = new ZkState(state = "EnforceOff", timestamp = startDay + 8.hours.toMillis, reason = "test", userId = "123")
      val state3 = new ZkState(state = "EnforceDegraded", timestamp = startDay + 16.hours.toMillis, reason = "test", userId = "123")
      val state4 = new ZkState(state = "EnforceOn", timestamp = startDay + 20.hours.toMillis, reason = "test", userId = "123")
      MetricsGenerator.getEnforceRatio(startDay, endDay, Seq(state1, state2, state3, state4)) must be(1F)
    }
    "process multiple states" in {
      val state1 = new ZkState(state = "EnforceOn", timestamp = startDay - 2.days.toMillis, reason = "test", userId = "123")
      val state2 = new ZkState(state = "Off", timestamp = startDay + 8.hours.toMillis, reason = "test", userId = "123")
      val state3 = new ZkState(state = "EnforceDegraded", timestamp = startDay + 16.hours.toMillis, reason = "test", userId = "123")
      val state4 = new ZkState(state = "StandBy", timestamp = startDay + 20.hours.toMillis, reason = "test", userId = "123")
      MetricsGenerator.getEnforceRatio(startDay, endDay, Seq(state1, state2, state3, state4)) must be(0.5F)
    }
    "process multiple states (not sorted)" in {
      val state1 = new ZkState(state = "EnforceOn", timestamp = startDay - 2.days.toMillis, reason = "test", userId = "123")
      val state2 = new ZkState(state = "Off", timestamp = startDay + 8.hours.toMillis, reason = "test", userId = "123")
      val state3 = new ZkState(state = "EnforceDegraded", timestamp = startDay + 16.hours.toMillis, reason = "test", userId = "123")
      val state4 = new ZkState(state = "StandBy", timestamp = startDay + 20.hours.toMillis, reason = "test", userId = "123")
      MetricsGenerator.getEnforceRatio(startDay, endDay, Seq(state4, state2, state1, state3)) must be(0.5F)
    }
    "process multiple enforce states before and after day period" in {
      val state1 = new ZkState(state = "EnforceOn", timestamp = startDay - 2.days.toMillis, reason = "test", userId = "123")
      val state2 = new ZkState(state = "EnforceOff", timestamp = startDay - 1.day.toMillis, reason = "test", userId = "123")
      val state3 = new ZkState(state = "EnforceDegraded", timestamp = startDay + 26.hours.toMillis, reason = "test", userId = "123")
      val state4 = new ZkState(state = "EnforceOn", timestamp = startDay + 2.days.toMillis, reason = "test", userId = "123")
      MetricsGenerator.getEnforceRatio(startDay, endDay, Seq(state4, state2, state1, state3)) must be(1F)
    }
    "process multiple states before and after day period" in {
      val state1 = new ZkState(state = "EnforceOn", timestamp = startDay - 2.days.toMillis, reason = "test", userId = "123")
      val state2 = new ZkState(state = "Off", timestamp = startDay - 1.day.toMillis, reason = "test", userId = "123")
      val state3 = new ZkState(state = "EnforceDegraded", timestamp = startDay + 26.hours.toMillis, reason = "test", userId = "123")
      val state4 = new ZkState(state = "StandBy", timestamp = startDay + 2.days.toMillis, reason = "test", userId = "123")
      MetricsGenerator.getEnforceRatio(startDay, endDay, Seq(state4, state2, state1, state3)) must be(0F)
    }
  }

}