/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.reports.metrics

import common.{ SystemMetrics, Metrics }
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.messages.{ CorridorKey, StatsDuration }
import csc.sectioncontrol.storage.{ ZkState, ZkAlertHistory }
import csc.sectioncontrol.storagelayer.corridor.CorridorIdMapping

class MetricsGeneratorTest extends WordSpec with MustMatchers {
  "SystemMetrics.timeAvailable" must {
    "be correct when there is no data" in {
      val stat = StatsDuration.local("20150105")
      val corridorStates = Map[CorridorIdMapping, Seq[ZkState]]()
      val corridorAlerts = Map[CorridorIdMapping, Seq[ZkAlertHistory]]()
      val calibrationExcludePeriods = Map[CorridorIdMapping, Seq[Period]]()
      val metrics = Map[CorridorKey, Metrics]()
      val systemMetric = MetricsGenerator.createSystemMetrics(stat, corridorStates, corridorAlerts, calibrationExcludePeriods, metrics)
      systemMetric.timeAvailable must be("PT24H0M")
    }
    "be correct when there is only calibration exclusions" in {
      val stat = StatsDuration.local("20150105")
      val states = Map[CorridorIdMapping, Seq[ZkState]]()
      val systemAlerts = Map[CorridorIdMapping, Seq[ZkAlertHistory]]()
      val period = Period("2015-01-05T12:00:00", "2015-01-05T14:00:00")
      val calibrationExcludePeriods = Map(CorridorIdMapping("S1", 11) -> Seq(period))
      val metrics = Map[CorridorKey, Metrics]()
      val systemMetric = MetricsGenerator.createSystemMetrics(stat, states, systemAlerts, calibrationExcludePeriods, metrics)
      systemMetric.timeAvailable must be("PT24H0M")
    }
    "be correct when there is only alerts" in {
      val stat = StatsDuration.local("20150105")
      val states = Map[CorridorIdMapping, Seq[ZkState]]()
      val period = Period("2015-01-05T12:00:00", "2015-01-05T14:00:00")
      val systemAlerts = Map(CorridorIdMapping("S1", 11) -> Seq(ZkAlertHistory(path = "A2-X1-X1R1-camera",
        alertType = "CAMERA_CONNECTION_ERROR",
        message = "Connection error",
        startTime = period.from,
        endTime = period.to,
        configType = "Lane",
        reductionFactor = 0.33F)))
      val calibrationExcludePeriods = Map[CorridorIdMapping, Seq[Period]]()
      val metrics = Map[CorridorKey, Metrics]()
      val systemMetric = MetricsGenerator.createSystemMetrics(stat, states, systemAlerts, calibrationExcludePeriods, metrics)
      systemMetric.timeAvailable must be("PT22H0M")
    }
    "be correct when in one state (standby) and no calibration" in {
      val corID = CorridorIdMapping("S1", 11)
      val stat = StatsDuration.local("20150105")
      val states = Map(corID ->
        Seq(ZkState(state = "StandBy", timestamp = stat.start, userId = "system", reason = "State change")))
      val systemAlerts = Map[CorridorIdMapping, Seq[ZkAlertHistory]]()
      val calibrationExcludePeriods = Map(corID -> Seq(Period(stat.start, stat.tomorrow.start)))
      val metrics = Map[CorridorKey, Metrics]()
      val systemMetric = MetricsGenerator.createSystemMetrics(stat, states, systemAlerts, calibrationExcludePeriods, metrics)
      systemMetric.timeAvailable must be("PT24H0M")
    }
    "be correct when in one state (enforce) and calibration exclusion period" in {
      val corID = CorridorIdMapping("S1", 11)
      val stat = StatsDuration.local("20150105")
      val period = Period("2015-01-05T12:00:00", "2015-01-05T14:00:00")

      val states = Map(corID -> Seq(ZkState(state = "EnforceOn", timestamp = stat.start, userId = "system", reason = "State change")))
      val systemAlerts = Map[CorridorIdMapping, Seq[ZkAlertHistory]]()
      val calibrationExcludePeriods = Map(corID -> Seq(Period(period.from, period.to)))
      val metrics = Map[CorridorKey, Metrics]()
      val systemMetric = MetricsGenerator.createSystemMetrics(stat, states, systemAlerts, calibrationExcludePeriods, metrics)
      systemMetric.timeAvailable must be("PT22H0M")
    }
    "be correct when in one state (Off) and calibration exclusion period" in {
      val corID = CorridorIdMapping("S1", 11)
      val stat = StatsDuration.local("20150105")
      val period = Period("2015-01-05T12:00:00", "2015-01-05T14:00:00")

      val states = Map(corID -> Seq(ZkState(state = "Off", timestamp = stat.start, userId = "system", reason = "State change")))
      val systemAlerts = Map[CorridorIdMapping, Seq[ZkAlertHistory]]()
      val calibrationExcludePeriods = Map(corID -> Seq(Period(period.from, period.to)))
      val metrics = Map[CorridorKey, Metrics]()
      val systemMetric = MetricsGenerator.createSystemMetrics(stat, states, systemAlerts, calibrationExcludePeriods, metrics)
      systemMetric.timeAvailable must be("PT0H0M")
    }
    "be correct when in enforce => standby => enforce and exclusion calibration while in standby" in {
      val corID = CorridorIdMapping("S1", 11)
      val stat = StatsDuration.local("20150105")
      val period = Period("2015-01-05T12:00:00", "2015-01-05T14:00:00")
      val states = Map(corID -> Seq(
        ZkState(state = "EnforceOn", timestamp = stat.start, userId = "system", reason = "State change"),
        ZkState(state = "StandBy", timestamp = period.from, userId = "system", reason = "State change"),
        ZkState(state = "EnforceOn", timestamp = period.to, userId = "system", reason = "State change")))
      val systemAlerts = Map[CorridorIdMapping, Seq[ZkAlertHistory]]()
      val calibrationExcludePeriods = Map(corID -> Seq(period))
      val metrics = Map[CorridorKey, Metrics]()
      val systemMetric = MetricsGenerator.createSystemMetrics(stat, states, systemAlerts, calibrationExcludePeriods, metrics)
      systemMetric.timeAvailable must be("PT24H0M")
    }
    "be correct when in enforce => standby => enforce and exclusion calibration while in standby with alerts" in {
      val corID = CorridorIdMapping("S1", 11)
      val stat = StatsDuration.local("20150105")
      val periodCal = Period("2015-01-05T12:00:00", "2015-01-05T14:00:00")
      val periodAlert = Period("2015-01-05T19:00:00", "2015-01-05T23:00:00")
      val states = Map(corID -> Seq(
        ZkState(state = "EnforceOn", timestamp = stat.start, userId = "system", reason = "State change"),
        ZkState(state = "StandBy", timestamp = periodCal.from, userId = "system", reason = "State change"),
        ZkState(state = "EnforceOn", timestamp = periodCal.to, userId = "system", reason = "State change")))
      val systemAlerts = Map(corID -> Seq(ZkAlertHistory(path = "A2-X1-X1R1-camera",
        alertType = "CAMERA_CONNECTION_ERROR",
        message = "Connection error",
        startTime = periodAlert.from,
        endTime = periodAlert.to,
        configType = "Lane",
        reductionFactor = 0.33F)))
      val calibrationExcludePeriods = Map(corID -> Seq(periodCal))
      val metrics = Map[CorridorKey, Metrics]()
      val systemMetric = MetricsGenerator.createSystemMetrics(stat, states, systemAlerts, calibrationExcludePeriods, metrics)
      systemMetric.timeAvailable must be("PT20H0M")
    }
  }

}