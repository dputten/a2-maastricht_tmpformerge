/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

package csc.sectioncontrol.reports.metrics

import csc.sectioncontrol.reports.metrics.common.{ MetricsData, Rounder }
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.storage._
import math.BigDecimal._
import java.util.Date
import tc.TCMetrics

class TCMetricsTest extends WordSpec with MustMatchers with Rounder with MetricsData {
  import ZkState._
  import Extensions._

  def systemStates = List(
    ZkState(enforceOn, "2012-12-29T00:00:00".toDate.getTime, "123", "test"),
    ZkState(standBy, "2012-12-29T03:00:00".toDate.getTime, "123", "test"),
    ZkState(enforceDegraded, "2012-12-29T09:00:00".toDate.getTime, "123", "test"),
    ZkState(enforceOff, "2012-12-29T24:00:00".toDate.getTime, "123", "test"))

  def prettyPrint(ratios: Seq[StateRatio]) {
    println("BEGIN StateRatio")
    ratios.foreach {
      ratio ⇒
        println("State:  " + ratio.state)
        println("Ratio: " + ratio.ratio)
        println("Total: " + new Date(ratio.range.from) + ", " + new Date(ratio.range.to))
        println("Periods:")

        ratio.periods.foreach {
          period ⇒
            println("(" + new Date(period.from) + ", " + new Date(period.to) + ")")
        }

        println("Periods (given):")
        ratio.allPeriods.foreach {
          period ⇒
            println("(" + new Date(period.from) + ", " + new Date(period.to) + ")")
        }

        println("allPeriods: => " + ratio.timeAllPeriods)
        println("timePeriods: => " + ratio.timePeriods)

        ratio.statePeriods.foreach {
          statePeriod ⇒
        }

    }
    println("TOTAL RATIO:  " + ratios.map(_.ratio).sum)
    println("EIND StateRatio")
    println()
  }

  def createTCMetrics(systemStates: Seq[ZkState], periods: Seq[Period]) = TCMetrics(
    system = defaultZkSystem,
    corridorData = defaultCorridorData,
    corridorStatistics = Nil,
    schedulePeriods = periods,
    systemAlerts = Nil,
    speedLimits = Nil,
    systemStates = systemStates)

  "TCMetrics" must {
    "calculate the default values like totalIn, totalOut etc" in {
      metrics.vehiclesPerLaneIn.size must be(2)
      metrics.vehiclesPerLaneOut.size must be(2)
      metrics.totalIn must be(totalIn)
      metrics.totalOut must be(totalOut)
    }
    "calculate ht, modus and id" in {
      metrics.reportHtId must be(defaultZkSystem.violationPrefixId + "%04d".format(defaultCorridorData.corridor.info.corridorId))
      metrics.modus must be("AUTONOOM")

      val id = defaultCorridorData.corridor.info.reportId
      metrics.reportId must be(id)

    }
    "calculate 'instellingen' values" must {
      "calculate 'snelheidslimiet' for one corridor" in {
        metrics_11.speedLimit must be(metrics_11_speedLimits.max)
      }

      "calculate 'ESA-scantijd' for one corridor" in {
        metrics_11.esaScanTime_secs must be(defaultZkSystem.pardonTimes.pardonTimeTechnical_secs)
      }

      "calculate 'ESA-pardontijd' for one corridor" in {
        metrics_11.esaPardonTime_secs must be(defaultZkSystem.pardonTimes.pardonTimeEsa_secs)
      }
    }

    "calculate 'portalen' values" must {
      "calculate 'portaal' for one corridor" in {
        pending
      }
    }

    "calculate 'TC-statistiek' values" must {

      "calculate 'passages' values" must {

        "calculate 'totaal-in' for one corridor" in {
          metrics_11.totalIn must be(totalIn_11)
        }

        "calculate 'totaal-uit' for one corridor" in {
          metrics_11.totalOut must be(totalOut_11)
        }

        "calculate 'strook' for one corridor" in {
          val entryLanes = defaultCorridorData.startSection.inGantry.lanes
          val exitLanes = defaultCorridorData.endSection.outGantry.lanes

          metrics_11.vehiclesPerLaneIn.size must be(entryLanes.length)
          metrics_11.vehiclesPerLaneOut.size must be(exitLanes.length)

          pending

        }
      }

      "calculate 'snelheden' values" must {
        "calculate 'gemiddeld' for one corridor" in {
          roundBigDecimal(metrics_11.averageSpeed, 0).toString must be(round(corridorStats_11.averageSpeed.toFloat, 0).toString())
        }

        "calculate 'gemiddeld' for multiple corridors" in {
          val totalMatchAverage = List(corridorStats_11, corridorStats_12).map(d ⇒ d.matched * d.averageSpeed).sum
          val totalMatches = List(corridorStats_11, corridorStats_12).map(d ⇒ d.matched).sum
          val averageSpeedRaw = if (totalMatches == 0) 0 else totalMatchAverage / totalMatches
          val averageSpeedRounded = round(averageSpeedRaw.toFloat, 0)

          roundBigDecimal(metrics.averageSpeed, 0).toString must be(averageSpeedRounded.toString())
        }

        "calculate 'max' for one corridor" in {
          metrics_11.maxSpeed must be(corridorStats_11.highestSpeed)
        }
      }

      "calculate 'matches' values" must {
        "calculate 'matches' for one corridor" in {
          metrics_11.matches must be(corridorStats_11.matched)
        }

        "calculate 'matches' for multiple corridors" in {
          val matches = List(corridorStats_11, corridorStats_12).map(_.matched).sum
          metrics.matches must be(matches)
        }
      }

      "calculate 'performance' values" must {
        "calculate 'matchRatio' for one corridor" in {
          val maxMatches = corridorStats_11.matched
          val result = divideAndRound(maxMatches, total_11)

          roundBigDecimal(metrics_11.performance.matchRatio, 2).toString must be(result.toString())
        }

        "calculate 'matchRatio' for multiple corridors" in {
          val maxMatches = listOfCorridors.map(_.matched).sum //188 + 50 = 238
          val total = math.max(totalIn, totalOut) //totalExists higher than totalEntries
          val result = divideAndRound(maxMatches, total) //maxMatches/total //0.169395018 = 0.169

          roundBigDecimal(metrics.performance.matchRatio, 2).toString must be(result.toString())
        }

        "calculate 'overtredingenratio' for one corridor" in {
          val allViolations = corridorStats_11.violations.auto +
            corridorStats_11.violations.manual +
            corridorStats_11.violations.mobi

          val result = divideAndRound(allViolations, total_11)

          metrics_11.performance.violationRatio.toString must be(result.toString())
        }

        "calculate 'overtredingenratio' for multiple corridors" in {
          val allViolations = listOfCorridors.map(stat ⇒ stat.violations.auto + stat.violations.manual + stat.violations.mobi).sum
          val total = math.max(totalIn, totalOut)
          val result = divideAndRound(allViolations, total)

          roundBigDecimal(metrics.performance.violationRatio, 2).toString must be(result.toString())
        }

        "calculate 'autoRatio' for one corridor" in {
          val allViolations = corridorStats_11.violations.auto +
            corridorStats_11.violations.manual +
            corridorStats_11.violations.mobi
          val allAuto = corridorStats_11.violations.auto

          val result = divideAndRound(allAuto, allViolations)

          roundBigDecimal(metrics_11.performance.autoRatio, 2).toString must be(result.toString())
        }

        "calculate 'autoRatio' for multiple corridors" in {
          val allViolations = listOfCorridors.map(stat ⇒ stat.violations.auto + stat.violations.manual + stat.violations.mobi).sum
          val allAuto = listOfCorridors.map(_.violations.auto).sum
          val result = divideAndRound(allAuto, allViolations)

          roundBigDecimal(metrics.performance.autoRatio, 2).toString must be(result.toString())
        }

        "calculate 'autoRatioV60' for one corridor" in {
          val allViolations = corridorStats_11.violations.auto +
            corridorStats_11.violations.manual +
            corridorStats_11.violations.mobi
          val allAuto = corridorStats_11.violations.auto +
            corridorStats_11.violations.mobiDoNotProcess

          val result = divideAndRound(allAuto, allViolations)

          roundBigDecimal(metrics_11.performance.autoRatioV60, 2).toString must be(result.toString())
        }

        "calculate 'autoRatioV60' for multiple corridors" in {
          val allViolations = listOfCorridors.map(stat ⇒ stat.violations.auto + stat.violations.manual + stat.violations.mobi).sum
          val allAuto = listOfCorridors.map(_.violations.auto).sum +
            listOfCorridors.map(_.violations.mobiDoNotProcess).sum
          val result = divideAndRound(allAuto, allViolations)

          roundBigDecimal(metrics.performance.autoRatioV60, 2).toString must be(result.toString())
        }

        "calculate 'autoRatioNetto' for one corridor" in {
          val allViolations =
            (corridorStats_11.violations.auto +
              corridorStats_11.violations.manual +
              corridorStats_11.violations.mobi) -
              corridorStats_11.violations.manualAccordingToSpec -
              corridorStats_11.violations.mobiDoNotProcess
          val allAuto = corridorStats_11.violations.auto

          val result = divideAndRound(allAuto, allViolations)

          roundBigDecimal(metrics_11.performance.autoRatioNetto, 2).toString must be(result.toString())
        }

        "calculate 'autoRatioNetto' for multiple corridors" in {
          val allViolations = listOfCorridors.map(stat ⇒
            (stat.violations.auto +
              stat.violations.manual +
              stat.violations.mobi) -
              stat.violations.manualAccordingToSpec -
              stat.violations.mobiDoNotProcess).sum
          val allAuto = listOfCorridors.map(_.violations.auto).sum

          val result = divideAndRound(allAuto, allViolations)

          roundBigDecimal(metrics.performance.autoRatioNetto, 2).toString must be(result.toString())
        }

        "calculate 'handhaafratio' " must {

          val nrDecimals: Int = 4

          "be able to handle low enforce ratio's (only minutes)" in {
            val systemStates = List(
              ZkState(standBy, "2015-11-06T00:00:00".toDate.getTime, "dummy", "dummy"),
              ZkState(enforceOn, "2015-11-06T00:16:21".toDate.getTime, "dummy", "dummy"),
              ZkState(standBy, "2015-11-06T00:50:27".toDate.getTime, "dummy", "dummy"),
              ZkState(Maintenance, "2015-11-06T00:51:32".toDate.getTime, "dummy", "dummy"),
              ZkState(standBy, "2015-11-06T23:58:43".toDate.getTime, "dummy", "dummy"),
              ZkState(standBy, "2015-11-06T23:59:59".toDate.getTime, "dummy", "dummy"))

            roundBigDecimal(createTCMetrics(systemStates, Seq(Period("2015-11-06T00:00:00", "2015-11-06T24:00:00"))).performance.enforceRatio, nrDecimals).toString must be("0.0237")
          }

          "include enforceOn, enforceDegraded and enforceOff as enforce time" in {
            val systemStates = List(
              ZkState(enforceOn, "2012-12-29T00:00:00".toDate.getTime, "dummy", "dummy"),
              ZkState(standBy, "2012-12-29T03:00:00".toDate.getTime, "dummy", "dummy"),
              ZkState(enforceDegraded, "2012-12-29T09:00:00".toDate.getTime, "dummy", "dummy"),
              ZkState(enforceOff, "2012-12-29T24:00:00".toDate.getTime, "dummy", "dummy"))

            roundBigDecimal(createTCMetrics(systemStates, Seq(Period("2012-12-29T00:00:00", "2012-12-29T24:00:00"))).performance.enforceRatio, nrDecimals).toString must be("0.7500")
          }

          "be able to handle unsorted system states" in {
            val systemStates = List(
              ZkState(standBy, "2012-12-29T03:00:00".toDate.getTime, "dummy", "dummy"),
              ZkState(enforceOff, "2012-12-29T24:00:00".toDate.getTime, "dummy", "dummy"),
              ZkState(enforceOn, "2012-12-29T00:00:00".toDate.getTime, "dummy", "dummy"),
              ZkState(enforceDegraded, "2012-12-29T09:00:00".toDate.getTime, "dummy", "dummy"))

            roundBigDecimal(createTCMetrics(systemStates, Seq(Period("2012-12-29T00:00:00", "2012-12-29T24:00:00"))).performance.enforceRatio, nrDecimals).toString must be("0.7500")
          }

          "be able to handle a first state from a day in the past (i.e. before the current day)" in {
            val systemStates = List(
              ZkState(enforceOn, "2015-11-01T00:10:00".toDate.getTime, "dummy", "dummy"),
              ZkState(standBy, "2015-11-06T06:00:00".toDate.getTime, "dummy", "dummy"),
              ZkState(enforceOn, "2015-11-06T12:00:00".toDate.getTime, "dummy", "dummy"),
              ZkState(enforceOn, "2015-11-06T23:59:59".toDate.getTime, "dummy", "dummy"))

            roundBigDecimal(createTCMetrics(systemStates, Seq(Period("2015-11-06T00:00:00", "2015-11-06T24:00:00"))).performance.enforceRatio, nrDecimals).toString must be("0.7500")
          }

          "be able to handle system states beginning later than midnight" in {
            val systemStates = List(
              ZkState(enforceOn, "2015-11-06T06:00:00".toDate.getTime, "dummy", "dummy"),
              ZkState(enforceOn, "2015-11-06T23:59:59".toDate.getTime, "dummy", "dummy"))

            roundBigDecimal(createTCMetrics(systemStates, Seq(Period("2015-11-06T00:00:00", "2015-11-06T24:00:00"))).performance.enforceRatio, nrDecimals).toString must be("0.7500")
          }

          "be able to handle a list with only one enforcing state" in {
            val systemStates = List(
              ZkState(enforceOn, "2015-11-06T00:00:00".toDate.getTime, "dummy", "dummy"))

            roundBigDecimal(createTCMetrics(systemStates, Seq(Period("2015-11-06T00:00:00", "2015-11-06T24:00:00"))).performance.enforceRatio, nrDecimals).toString must be("1.0000")
          }

          "be able to handle a list with only one non enforcing state" in {
            val systemStates = List(
              ZkState(standBy, "2015-11-06T00:00:00".toDate.getTime, "dummy", "dummy"))

            roundBigDecimal(createTCMetrics(systemStates, Seq(Period("2015-11-06T00:00:00", "2015-11-06T24:00:00"))).performance.enforceRatio, nrDecimals).toString must be("0.0000")
          }

          "be able to handle a list with only one enforcing state starting after midnight" in {
            val systemStates = List(
              ZkState(enforceOn, "2015-11-06T12:00:00".toDate.getTime, "dummy", "dummy"))

            roundBigDecimal(createTCMetrics(systemStates, Seq(Period("2015-11-06T00:00:00", "2015-11-06T24:00:00"))).performance.enforceRatio, nrDecimals).toString must be("1.0000")
          }

          "be able to handle the last state not being at 23:59:59" in {
            val systemStates = List(
              ZkState(enforceOn, "2015-11-01T00:10:00".toDate.getTime, "dummy", "dummy"),
              ZkState(standBy, "2015-11-06T06:00:00".toDate.getTime, "dummy", "dummy"),
              ZkState(enforceOn, "2015-11-06T12:00:00".toDate.getTime, "dummy", "dummy"))

            roundBigDecimal(createTCMetrics(systemStates, Seq(Period("2015-11-06T00:00:00", "2015-11-06T24:00:00"))).performance.enforceRatio, nrDecimals).toString must be("0.7500")
          }

          "return 0 (zero) when no states present" in {
            roundBigDecimal(createTCMetrics(List(), Seq(Period("2015-11-06T00:00:00", "2015-11-06T24:00:00"))).performance.enforceRatio, nrDecimals).toString must be("0.0000")
          }
        }

        "calculate 'storingenratio' for one corridor" in {
          roundBigDecimal(metrics_11.performance.failureRatio, 2).toString must be("0.30")
        }

        "calculate 'storingenratio' for multiple corridors" in {
          roundBigDecimal(metrics.performance.failureRatio, 2).toString must be("0.30")
        }
        "calculate 'av-t-ratio' over multiple periods 3" in {
          val states = List(
            ZkState(enforceOn, "2012-12-12T08:08:51".toDate.getTime, "123", "test"),
            ZkState(enforceOff, "2012-12-13T03:00:00".toDate.getTime, "123", "test"),
            ZkState(enforceOn, "2012-12-13T03:20:00".toDate.getTime, "123", "test"),
            ZkState(standBy, "2012-12-13T15:40:43".toDate.getTime, "123", "test"),
            ZkState(enforceOn, "2012-12-13T16:04:31".toDate.getTime, "123", "test"))

          val periods = List(
            Period(
              from = "2012-12-13T00:00:00".toDate.getTime,
              to = "2012-12-13T06:00:00".toDate.getTime),
            Period(
              from = "2012-12-13T19:00:00".toDate.getTime,
              to = "2012-12-13T24:00:00".toDate.getTime),
            Period(
              from = "2012-12-13T15:40:00".toDate.getTime,
              to = "2012-12-13T15:41:00".toDate.getTime))

          def metrics_av = TCMetrics(
            system = defaultZkSystem,
            corridorData = defaultCorridorData,
            corridorStatistics = Seq(corridorStats_11),
            schedulePeriods = periods,
            systemAlerts = Nil,
            speedLimits = Nil,
            systemStates = states)

          val ratios = StateRatio(periods, states)
          //prettyPrint(ratios)
          val enforceOnR = ratios.find(_.state == enforceOn)
          val standByR = ratios.find(_.state == standBy)
          val enforceOffR = ratios.find(_.state == enforceOff)

          enforceOnR.isDefined must be(true)
          standByR.isDefined must be(true)
          enforceOffR.isDefined must be(true)

          roundBigDecimal(enforceOnR.get.ratio, 2) must be(0.97)
          roundBigDecimal(standByR.get.ratio, 2) must be(0.00)
          roundBigDecimal(enforceOffR.get.ratio, 2) must be(0.03)

          metrics_av.performance.failureRatio must be(1.00)
          roundBigDecimal(metrics_av.performance.avtRatio, 2) must be(0.46)
        }
        "calculate 'av-t-ratio' over multiple periods 2" in {
          val states = List(
            ZkState(enforceOn, "2012-12-12T08:08:51".toDate.getTime, "123", "test"),
            ZkState(standBy, "2012-12-13T15:40:43".toDate.getTime, "123", "test"),
            ZkState(enforceOn, "2012-12-13T16:04:31".toDate.getTime, "123", "test"))

          val period = Period(
            from = "2012-12-13T06:00:00".toDate.getTime,
            to = "2012-12-13T19:00:00".toDate.getTime)

          def metrics_av = TCMetrics(
            system = defaultZkSystem,
            corridorData = defaultCorridorData,
            corridorStatistics = Seq(corridorStats_11),
            schedulePeriods = List(period),
            systemAlerts = Nil,
            speedLimits = Nil,
            systemStates = states)

          val ratios = StateRatio(List(period), states)

          val enforceOnR = ratios.find(_.state == enforceOn)
          val standByR = ratios.find(_.state == standBy)

          enforceOnR.isDefined must be(true)
          standByR.isDefined must be(true)

          roundBigDecimal(enforceOnR.get.ratio, 2) must be(0.97)
          roundBigDecimal(standByR.get.ratio, 2) must be(0.03)

          metrics_av.performance.failureRatio must be(1.00)
          roundBigDecimal(metrics_av.performance.avtRatio, 2) must be(0.54)
        }
        "calculate 'av-t-ratio' over multiple periods" in {
          def systemStates = List(
            ZkState(enforceOn, "2012-12-29T00:00:00".toDate.getTime, "123", "test"),
            ZkState(enforceDegraded, "2012-12-29T03:00:00".toDate.getTime, "123", "test"),
            ZkState(enforceOn, "2012-12-29T09:00:00".toDate.getTime, "123", "test"),
            ZkState(standBy, "2012-12-29T12:00:00".toDate.getTime, "123", "test"),
            ZkState(enforceOff, "2012-12-29T14:00:00".toDate.getTime, "123", "test"),
            ZkState(standBy, "2012-12-29T15:00:00".toDate.getTime, "123", "test"),
            ZkState(enforceOn, "2012-12-29T16:00:00".toDate.getTime, "123", "test"),
            ZkState(enforceDegraded, "2012-12-29T19:00:00".toDate.getTime, "123", "test"),
            ZkState(enforceOn, "2012-12-29T20:00:00".toDate.getTime, "123", "test"),
            ZkState(enforceOn, "2012-12-29T24:00:00".toDate.getTime, "123", "test"))

          val alerts = List(
            ZkAlertHistory(path = "A2-X1-R1-Camera", alertType = "", configType = "Lane", message = "current event", startTime = "2012-12-29T02:00:00".toDate.getTime, endTime = Long.MaxValue, reductionFactor = 0.1f),
            ZkAlertHistory(path = "A2-X1-R2-Camera", alertType = "", configType = "Lane", message = "current event", startTime = "2012-12-29T03:00:00".toDate.getTime, endTime = Long.MaxValue, reductionFactor = 0.4f),
            ZkAlertHistory(path = "A2-X1-R3-Camera", alertType = "", configType = "Lane", message = "current event", startTime = "2012-12-29T13:00:00".toDate.getTime, endTime = Long.MaxValue, reductionFactor = 0.2f))

          def periods_130 = List(Period(from = "2012-12-29T00:00:00", to = "2012-12-29T06:00:00"),
            Period(from = "2012-12-29T19:00:00", to = "2012-12-29T24:00:00"))

          def periods_100 = List(Period(from = "2012-12-29T06:00:00", to = "2012-12-29T19:00:00"))

          def metrics_av = TCMetrics(
            system = defaultZkSystem,
            corridorData = defaultCorridorData,
            corridorStatistics = Seq(corridorStats_11),
            schedulePeriods = periods_130 ++ periods_100,
            systemAlerts = alerts,
            speedLimits = Nil,
            systemStates = systemStates)

          val ratios = StateRatio(periods_130 ++ periods_100, systemStates)

          val enforceOnR = ratios.find(_.state == enforceOn)
          val standByR = ratios.find(_.state == standBy)
          val enforceOffR = ratios.find(_.state == enforceOff)
          val enforceDegradedR = ratios.find(_.state == enforceDegraded)

          enforceOnR.isDefined must be(true)
          standByR.isDefined must be(true)
          enforceOffR.isDefined must be(true)
          enforceDegradedR.isDefined must be(true)

          roundBigDecimal(enforceOnR.get.ratio, 2) must be(0.54)
          roundBigDecimal(standByR.get.ratio, 2) must be(0.13)
          roundBigDecimal(enforceOffR.get.ratio, 2) must be(0.04)
          roundBigDecimal(enforceDegradedR.get.ratio, 2) must be(0.29)

          roundBigDecimal(metrics_av.performance.failureRatio, 2) must be(0.30)

          roundBigDecimal(metrics_av.performance.avtRatio, 2) must be(0.80)
        }

        "calculate 'avl-f-ratio' for one corridor" in {
          val totalViolations =
            corridorStats_11.violations.auto +
              corridorStats_11.violations.manual +
              corridorStats_11.violations.mobi

          metrics_11.performance.avlf must be(totalViolations > 0)
        }

        "calculate 'avl-f-ratio' for multiple corridors" in {
          val totalViolations = List(corridorStats_11, corridorStats_12).map(
            s ⇒ s.violations.auto +
              s.violations.manual +
              s.violations.mobi).sum

          metrics.performance.avlf must be(totalViolations > 0)
        }
      }

      "calculate 'overtredingen' values" must {

        "calculate 'overtredingen-totaal' for one corridor" in {
          val total =
            corridorStats_11.violations.auto +
              corridorStats_11.violations.manual +
              corridorStats_11.violations.mobi

          metrics_11.violations.totalViolations must be(total)
        }

        "calculate 'overtredingen-totaal' for multiple corridors" in {
          val total = List(corridorStats_11, corridorStats_12).map(
            s ⇒ s.violations.auto +
              s.violations.manual +
              s.violations.mobi).sum

          metrics.violations.totalViolations must be(total)
        }

        "calculate 'hand' for one corridor" in {
          val total = corridorStats_11.violations.manual
          metrics_11.violations.manual must be(total)
        }

        "calculate 'hand' for multiple corridors" in {
          val total = List(corridorStats_11, corridorStats_12).map(_.violations.manual).sum
          metrics.violations.manual must be(total)
        }

        "calculate 'auto' for one corridor" in {
          val total = corridorStats_11.violations.auto
          metrics_11.violations.auto must be(total)
        }

        "calculate 'auto' for multiple corridors" in {
          val total = List(corridorStats_11, corridorStats_12).map(_.violations.auto).sum
          metrics.violations.auto must be(total)
        }

        "calculate 'hand-conform-specificatie' for one corridor" in {
          val total = corridorStats_11.violations.manualAccordingToSpec
          metrics_11.violations.manualAccordingToSpec must be(total)
        }

        "calculate 'hand-conform-specificatie' for multiple corridors" in {
          val total = List(corridorStats_11, corridorStats_12).map(_.violations.manualAccordingToSpec).sum
          metrics.violations.manualAccordingToSpec must be(total)
        }

        "calculate 'mobi-niet-verwerken' for one corridor" in {
          val total = corridorStats_11.violations.mobiDoNotProcess
          metrics_11.violations.mobiDoNotProcess must be(total)
        }

        "calculate 'mobi-niet-verwerken' for multiple corridors" in {
          val total = List(corridorStats_11, corridorStats_12).map(_.violations.mobiDoNotProcess).sum
          metrics.violations.mobiDoNotProcess must be(total)
        }

        "calculate 'MTMratio' for one corridor" in {
          val mtmRatio = corridorStats_11.violations.mtmPardon
          val totalViolations =
            corridorStats_11.violations.auto +
              corridorStats_11.violations.manual +
              corridorStats_11.violations.mobi
          val result = divideAndRound(mtmRatio, totalViolations + mtmRatio)

          roundBigDecimal(metrics_11.violations.mtmRatio, 2).toString must be(result.toString())
        }

        "calculate 'MTMratio' for multiple corridors" in {
          val mtmRatio = List(corridorStats_11, corridorStats_12).map(_.violations.mtmPardon).sum
          val totalViolations = List(corridorStats_11, corridorStats_12).map(
            s ⇒ s.violations.auto +
              s.violations.manual +
              s.violations.mobi).sum
          val result = divideAndRound(mtmRatio, totalViolations + mtmRatio)
          roundBigDecimal(metrics.violations.mtmRatio).toString must be(result.toString())
        }

        "calculate 'MTM-pardon' for one corridor" in {
          metrics_11.violations.mtmPardon must be(corridorStats_11.violations.mtmPardon)
        }

        "calculate 'MTM-pardon' for multiple corridors" in {
          val mtmPardon = List(corridorStats_11, corridorStats_12).map(_.violations.mtmPardon).sum
          metrics.violations.mtmPardon must be(mtmPardon)
        }

        "calculate 'Dubbele-overtredingen-pardon' for one corridor" in {
          metrics_11.violations.doublePardon must be(corridorStats_11.violations.doublePardon)
        }

        "calculate 'Dubbele-overtredingen-pardon' for multiple corridors" in {
          val doublePardon = List(corridorStats_11, corridorStats_12).map(_.violations.doublePardon).sum
          metrics.violations.doublePardon must be(doublePardon)
        }

        "calculate 'Overig-pardon' for one corridor" in {
          metrics_11.violations.otherPardon must be(corridorStats_11.violations.otherPardon)
        }

        "calculate 'Overig-pardon' for multiple corridors" in {
          val otherPardon = List(corridorStats_11, corridorStats_12).map(_.violations.otherPardon).sum
          metrics.violations.otherPardon must be(otherPardon)
        }
      }
    }
  }

}
