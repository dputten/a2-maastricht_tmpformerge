/*
*
*  * Copyright (C) 2012 CSC. <http://www.csc.com>
*
*/

package csc.sectioncontrol.reports

import javax.mail.internet.InternetAddress
import org.jvnet.mock_javamail.Mailbox
import org.scalatest.matchers.MustMatchers
import javax.mail._
import scala.Array
import org.apache.commons.io.IOUtils
import org.scalatest.WordSpec
import csc.sectioncontrol.messages.StatsDuration
import java.util.TimeZone
import csc.sectioncontrol.storage.ZkDayReportConfig

class EmailSenderTest extends WordSpec with MustMatchers with EmailSender {
  case class ReturnedEmailInfo(from: Seq[InternetAddress],
                               to: Seq[InternetAddress],
                               cc: Seq[InternetAddress],
                               bcc: Seq[InternetAddress],
                               subject: Option[String],
                               body: EmailBody)

  case class EmailBody(bodyText: Option[String] = None,
                       fileName: Option[String] = None,
                       contentType: Option[String] = None,
                       attachment: Option[Array[Byte]] = None)

  "EmailSender" must {
    //val receiver = "receiver.csc.com"
    val emailInfo = ZkDayReportConfig(
      subject = "subject",
      dateFormat = "",
      to = "someone@company.com",
      properties = Map("mail.smtp.host" -> "localhost", "mail.from" -> "admin@csc.com"),
      bodyText = "bodyText",
      useBBC = false)

    val statsDuration = StatsDuration("20120401", TimeZone.getTimeZone("Europe/Amsterdam"))
    val dayReport = ZkDayStatistics(statsDuration.key, statsDuration.start, System.currentTimeMillis(),
      data = "ddd".getBytes,
      fileName = "filename.gz")

    "retrieve attachment data" in {
      val tmpData = """efewfew
      fewfew
      fewf
      *&A(AEDMI@@ DJ(WD (W DWDW\||||\\
      dwqdqdwqdmiw7*&*@*C
      ewfewfew"""

      val dReport = dayReport.copy(data = tmpData.getBytes.toList.map(b ⇒ b.toInt))

      sendEmail(dest = Seq(emailInfo.to), emailInfo, dReport) match {
        case Left(exception) ⇒ fail(exception)
        case Right(_) ⇒
          val inbox = Mailbox.get(emailInfo.to)
          val email = getEmail(inbox.get(0))
          email.body.attachment.get must be(dReport.data.toArray.map(_.toByte))
          email.body.fileName.get must be(dReport.fileName)
      }
    }
    "set the system properties with given map" in {
      Mailbox.clearAll

      val eInfo = emailInfo.copy(properties = Map("prop1" -> "prop1.value", "prop2" -> "prop2.value"))
      eInfo.properties.foreach(keyValue ⇒ System.clearProperty(keyValue._1))
      sendEmail(dest = Seq(emailInfo.to), eInfo, dayReport)
      eInfo.properties.foreach {
        case (key, value) ⇒ System.getProperty(key) must be(value)
      }
    }
    "send email to multiple users" in {
      Mailbox.clearAll
      val to = Seq("a@csc1.com", "b@csc2.com", "c@csc3.com")
      sendEmail(dest = to, emailInfo, dayReport) match {
        case Left(exception) ⇒ fail(exception)
        case Right(_) ⇒

          val emailsReceived = to.foldLeft(List[ReturnedEmailInfo]()) {
            (list, receiver) ⇒
              val inbox = Mailbox.get(receiver)
              inbox.size must be(1)
              val msg = inbox.get(0)
              val email = getEmail(msg)
              email.from.length must be(1)
              email.to.length must be(to.length)
              email.cc.length must be(0)
              email.bcc.length must be(0)

              email :: list
          }

          val groupedSenders = emailsReceived.groupBy(x ⇒ x.from(0))
          groupedSenders.size must be(1) //is same sender
          groupedSenders.head._2.length must be(to.length) //number of emails sent
      }
    }

    "send email to user" in {
      Mailbox.clearAll
      sendEmail(dest = Seq(emailInfo.to), emailInfo, dayReport) match {
        case Left(exception) ⇒ fail(exception)
        case Right(_) ⇒
          val inbox = Mailbox.get(emailInfo.to)
          inbox.size must be(1)
          val msg = inbox.get(0)
          val email = getEmail(msg)

          email.from.length must be(1)
          email.from(0).getAddress must be("admin@csc.com")

          email.to.length must be(1)
          email.to(0).getAddress must be(emailInfo.to)

          email.cc.length must be(0)
          email.bcc.length must be(0)

          email.subject match {
            case Some(x) ⇒ x must be(emailInfo.subject)
            case None    ⇒ fail("subject is empty")
          }

          email.body.bodyText match {
            case Some(x) ⇒ x must be(emailInfo.bodyText)
            case None    ⇒ fail("bodyText is empty")
          }

          email.body.fileName match {
            case Some(x) ⇒ x must be(dayReport.fileName)
            case None    ⇒ fail("fileName is empty")
          }

          email.body.attachment match {
            case Some(x) ⇒ x must be(dayReport.data.toArray.map(_.toByte))
            case None    ⇒ fail("attachment is empty")
          }

          email.body.contentType match {
            case Some(x) ⇒ x must startWith("application/x-gzip")
            case None    ⇒ fail("contentType is empty")
          }
      }
    }
    "send email to user using BCC" in {
      Mailbox.clearAll
      sendEmail(dest = Seq(emailInfo.to), emailInfo.copy(useBBC = true), dayReport) match {
        case Left(exception) ⇒ fail(exception)
        case Right(_) ⇒
          val inbox = Mailbox.get(emailInfo.to)
          inbox.size must be(1)
          val msg = inbox.get(0)
          val email = getEmail(msg)

          email.from.length must be(1)
          email.from(0).getAddress must be("admin@csc.com")

          email.to.length must be(1)
          email.to(0).getAddress must be("admin@csc.com")

          email.bcc.length must be(1)
          email.bcc(0).getAddress must be(emailInfo.to)

          email.cc.length must be(0)

          email.subject match {
            case Some(x) ⇒ x must be(emailInfo.subject)
            case None    ⇒ fail("subject is empty")
          }

          email.body.bodyText match {
            case Some(x) ⇒ x must be(emailInfo.bodyText)
            case None    ⇒ fail("bodyText is empty")
          }

          email.body.fileName match {
            case Some(x) ⇒ x must be(dayReport.fileName)
            case None    ⇒ fail("fileName is empty")
          }

          email.body.attachment match {
            case Some(x) ⇒ x must be(dayReport.data.toArray.map(_.toByte))
            case None    ⇒ fail("attachment is empty")
          }

          email.body.contentType match {
            case Some(x) ⇒ x must startWith("application/x-gzip")
            case None    ⇒ fail("contentType is empty")
          }
      }
    }
  }

  private def getEmail(msg: Message): ReturnedEmailInfo = {
    val from = Option(msg.getFrom).map(_.asInstanceOf[Array[InternetAddress]].toList).getOrElse(Seq())
    val to = Option(msg.getRecipients(Message.RecipientType.TO)).map(_.asInstanceOf[Array[InternetAddress]].toList).getOrElse(Seq())
    val cc = Option(msg.getRecipients(Message.RecipientType.CC)).map(_.asInstanceOf[Array[InternetAddress]].toList).getOrElse(Seq())
    val bcc = Option(msg.getRecipients(Message.RecipientType.BCC)).map(_.asInstanceOf[Array[InternetAddress]].toList).getOrElse(Seq())
    val subject = Option(msg.getSubject).filter(_.nonEmpty)
    val body = getBody(msg)

    ReturnedEmailInfo(
      from = from,
      to = to,
      cc = cc,
      bcc = bcc,
      subject = subject,
      body = body)
  }

  private def getBody(msg: Message): EmailBody = {
    val content = msg.getContent.asInstanceOf[Multipart]
    var emailBody = EmailBody()
    for (i ← 0 until content.getCount) {
      val msgPart = content.getBodyPart(i)
      Option(msgPart.getDisposition) match {
        case Some(dp) if dp.equalsIgnoreCase(Part.ATTACHMENT) || dp.equalsIgnoreCase(Part.INLINE) ⇒
          emailBody = emailBody.copy(
            fileName = Option(msgPart.getFileName).filter(_.nonEmpty),
            contentType = Option(msgPart.getContentType).filter(_.nonEmpty),
            attachment = Option(IOUtils.toByteArray(msgPart.getInputStream)).map(_.toArray[Byte]).filter(_.nonEmpty))
        case None ⇒
          emailBody = emailBody.copy(bodyText = Option(msgPart.getContent.toString).filter(_.nonEmpty))
        case _ ⇒
      }
    }

    emailBody
  }
}
