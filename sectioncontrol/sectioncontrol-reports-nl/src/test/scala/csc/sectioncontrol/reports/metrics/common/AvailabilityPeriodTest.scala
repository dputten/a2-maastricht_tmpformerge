/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.reports.metrics.common

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.storage.ZkAlertHistory

class AvailabilityPeriodTest extends WordSpec with MustMatchers {

  // situation 1 <p -- p> |..|    => <p --p>
  // situation 2 | ..| <p -- p>   => <p--p>
  // situation 3 | ..<p-- p> ..|  => <p--p> with updated reduction
  // situation 4 | ..<p..|-- p>   => <p--><--p>
  // situation 5 <p -| -- p> ..|  => <p-><-p>
  // situation 6 <p -|..|-- p>    => <p-><-><-p>
  val time1 = 1000000
  val time2 = 2000000
  val time3 = 3000000
  val time4 = 4000000
  val time5 = 5000000
  val time6 = 6000000

  "AlertPeriod" must {
    "handle situation 1" in {
      // situation 1 <p -- p> |..|    => <p --p>
      val start = time1
      val end = time2
      val alert = new ZkAlertHistory(path = "",
        alertType = "",
        message = "",
        startTime = time3,
        endTime = time4,
        configType = "",
        reductionFactor = 1F)
      val periods = AvailabilityPeriod.createPeriods(start, end, Seq(alert))
      periods.size must be(1)
      val period1 = periods.head
      period1.start must be(start)
      period1.end must be(end)
      period1.reductionFactor must be(0F)
    }
    "handle situation 2" in {
      // situation 2 | ..| <p -- p>   => <p--p>
      val start = time3
      val end = time4
      val alert = new ZkAlertHistory(path = "",
        alertType = "",
        message = "",
        startTime = time1,
        endTime = time2,
        configType = "",
        reductionFactor = 1F)
      val periods = AvailabilityPeriod.createPeriods(start, end, Seq(alert))
      periods.size must be(1)
      val period1 = periods.head
      period1.start must be(start)
      period1.end must be(end)
      period1.reductionFactor must be(0F)
    }
    "handle situation 3" in {
      // situation 3 | ..<p-- p> ..|  => <p--p> with updated reduction
      val start = time2
      val end = time3
      val alert = new ZkAlertHistory(path = "",
        alertType = "",
        message = "",
        startTime = time1,
        endTime = time4,
        configType = "",
        reductionFactor = 1F)
      val periods = AvailabilityPeriod.createPeriods(start, end, Seq(alert))
      periods.size must be(1)
      val period1 = periods.head
      period1.start must be(start)
      period1.end must be(end)
      period1.reductionFactor must be(1F)
    }
    "handle situation 4" in {
      // situation 4 | ..<p..|-- p>   => <p--><--p>
      val start = time2
      val end = time4
      val alert = new ZkAlertHistory(path = "",
        alertType = "",
        message = "",
        startTime = time1,
        endTime = time3,
        configType = "",
        reductionFactor = 1F)
      val periods = AvailabilityPeriod.createPeriods(start, end, Seq(alert))
      periods.size must be(2)
      val period1 = periods.head
      period1.start must be(start)
      period1.end must be(time3)
      period1.reductionFactor must be(1F)
      val period2 = periods.last
      period2.start must be(time3)
      period2.end must be(time4)
      period2.reductionFactor must be(0F)
    }
    "handle situation 5" in {
      // situation 5 <p -| -- p> ..|  => <p-><-p>
      val start = time1
      val end = time3
      val alert = new ZkAlertHistory(path = "",
        alertType = "",
        message = "",
        startTime = time2,
        endTime = time4,
        configType = "",
        reductionFactor = 1F)
      val periods = AvailabilityPeriod.createPeriods(start, end, Seq(alert))
      periods.size must be(2)
      val period1 = periods.head
      period1.start must be(start)
      period1.end must be(time2)
      period1.reductionFactor must be(0F)
      val period2 = periods.last
      period2.start must be(time2)
      period2.end must be(time3)
      period2.reductionFactor must be(1F)
    }
    "handle situation 6" in {
      // situation 6 <p -|..|-- p>    => <p-><-><-p>
      val start = time1
      val end = time4
      val alert = new ZkAlertHistory(path = "",
        alertType = "",
        message = "",
        startTime = time2,
        endTime = time3,
        configType = "",
        reductionFactor = 1F)
      val periods = AvailabilityPeriod.createPeriods(start, end, Seq(alert))
      periods.size must be(3)
      val period1 = periods.head
      period1.start must be(start)
      period1.end must be(time2)
      period1.reductionFactor must be(0F)
      val period2 = periods(1)
      period2.start must be(time2)
      period2.end must be(time3)
      period2.reductionFactor must be(1F)
      val period3 = periods(2)
      period3.start must be(time3)
      period3.end must be(end)
      period3.reductionFactor must be(0F)
    }
    "handle multiple alerts" in {
      // situation 6 <p -|.x.|-x- p>
      val start = time1
      val end = time6
      val alert = new ZkAlertHistory(path = "",
        alertType = "",
        message = "",
        startTime = time2,
        endTime = time4,
        configType = "",
        reductionFactor = 1F)
      val alert2 = new ZkAlertHistory(path = "",
        alertType = "",
        message = "",
        startTime = time3,
        endTime = time5,
        configType = "",
        reductionFactor = 1F)
      val periods = AvailabilityPeriod.createPeriods(start, end, Seq(alert, alert2))
      periods.size must be(5)
      val period1 = periods(0)
      period1.start must be(time1)
      period1.end must be(time2)
      period1.reductionFactor must be(0F)
      val period2 = periods(1)
      period2.start must be(time2)
      period2.end must be(time3)
      period2.reductionFactor must be(1F)
      val period3 = periods(2)
      period3.start must be(time3)
      period3.end must be(time4)
      period3.reductionFactor must be(2F)
      val period4 = periods(3)
      period4.start must be(time4)
      period4.end must be(time5)
      period4.reductionFactor must be(1F)
      val period5 = periods(4)
      period5.start must be(time5)
      period5.end must be(time6)
      period5.reductionFactor must be(0F)
    }
  }

}