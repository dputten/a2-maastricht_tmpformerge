/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *  
 */

package csc.sectioncontrol.reports

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

class GZIPCompressorTest extends WordSpec with MustMatchers with GZIPCompressor {

  "ReportGenerator" must {
    "compress a given string" in {
      val tmpData = """efewfew
      fewfew
      fewf
      *&A(AEDMI@@ DJ(WD (W DWDW\||||\\
      dwqdqdwqdmiw7*&*@*C
      ewfewfew""".getBytes

      compress(tmpData) must not be (tmpData)
      compress(tmpData).length must be < (tmpData.length)
      compress(tmpData) must be(compress(tmpData)) //always the same result
    }
    "decompress a given compressed byte array" in {
      val tmpData = """efewfew
      fewfew
      fewf
      *&A(AEDMI@@ DJ(WD (W DWDW\||||\\
      dwqdqdwS(S*&(# #IUD&
      ce
      vewvewvewvwv
      ewvvvvv     223232 WDE qdmiw7*&*@*C
      ewfewfew""".getBytes

      decompress(compress(tmpData)) must be(tmpData)
      decompress(compress(tmpData)).length must be(tmpData.length)
      decompress(compress(tmpData)) must be(decompress(compress(tmpData))) //always the same result
    }
  }
}
