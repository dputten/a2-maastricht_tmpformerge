/*
* Copyright (C) 2012 CSC. <http://www.csc.com>
*/
package csc.sectioncontrol.reports.export

import csc.sectioncontrol.messages.certificates.{ ComponentCertificate, ActiveCertificate, TypeCertificate }
import java.text.SimpleDateFormat
import csc.sectioncontrol.messages.{ SystemEvent, StatsDuration }
import xml.{ PrettyPrinter, Elem }
import csc.sectioncontrol.enforce.{ MsiBlankEvent, MtmSpeedSetting }
import csc.sectioncontrol.storage._
import java.util.{ Locale, TimeZone, Calendar }
import csc.sectioncontrol.reports.{ ReportLogRecord, AllCorridorsStatistics, SystemData }
import csc.sectioncontrol.reports.metrics.common.Metrics
import csc.sectioncontrol.reports.metrics.Period
import csc.sectioncontrol.reports.metrics.tc.TCMetrics
import csc.sectioncontrol.reports.metrics.red.RedMetrics
import csc.sectioncontrol.reports.metrics.speed.SpeedMetrics
import csc.sectioncontrol.reports.i18n.Messages
import org.apache.commons.lang.StringEscapeUtils
import csc.akkautils.DirectLogging
import csc.sectioncontrol.storagelayer.corridor.CorridorIdMapping

/**
 * The day report according to: Specificatie XML-dagrapportage, versie 4.0, juni 2011
 */
class DayReportBuilderV40(systemId: String,
                          val key: StatsDuration,
                          systemData: SystemData,
                          runtimeResult: AllCorridorsStatistics,
                          components: Either[String, List[ComponentCertificate]],
                          activeCertificates: List[ActiveCertificate],
                          currentSystemState: ZkState,
                          systemAlerts: Map[CorridorIdMapping, Seq[ZkAlertHistory]],
                          mtmSettings: List[MtmSpeedSetting],
                          systemEvents: Seq[ReportLogRecord],
                          metrics: List[Metrics],
                          users: List[ZkUser],
                          reportName: String,
                          blankMsiEvent: Option[MsiBlankEvent]) extends DayReportBuilder with DirectLogging {
  def getDayReportSettings(): DayReportBuilderV40Settings = {
    return new DayReportBuilderV40Settings()
  }

  /**
   * wegnummer:
   * Nummer1 van de weg, bijvoorbeeld A12 of N919. Bij mobiele systemen wordt “MOB” ingevuld
   * (Indien de weg geen wegnummer heeft, kan ook de naam van de weg gebruikt worden.)
   */
  def roadNumber = systemData.zkSystem.name

  /**
   * volgnummer:
   *
   * Volgnummer van het HHM-systeem binnen het wegnummer om zo verschillende HHM-systemen op
   * dezelfde weg te kunnen onderscheiden.
   * Bij mobiele systemen wordt hier een afkorting voor de politieregio ingevuld, bijvoorbeeld ZHZ of GenV.
   */
  def orderNumber = systemData.zkSystem.orderNumber

  /**
   * locatieomschrijving:
   * Een locatieomschrijving van de locatie van het HHM: de pleegplaats. Bij mobiele systemen: het HHM-id.
   */
  def locationDescription = systemData.zkSystem.location.systemLocation

  /**
   * The name is generated according to p 1.2 of the specifications
   */
  def filenamePlain = reportName.format(key.key) + ".xml"
  def filenameZip = filenamePlain + ".gz"

  def xsd = "HHM-dagrapport v4.0.xsd"

  def version_text = "versie 4.0"
  def xml: Elem = {
    {
      log.info("Generate the XML ...")
    }

    <HHM-dagrapport datum={ key.datum } xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation={ xsd }>
      <algemene-info>
        { generalInfo }
      </algemene-info>{ systemInfo }{ for (metric ← metrics) yield createMetricsXml(metric) }<log>
                                                                                               { reportsLog }{ alarmesLog }{ if (!mtmSettings.isEmpty) mtmLog }
                                                                                             </log>
      <opmerkingen>
        Dagelijkse rapportage HHM volgens Specificatie XML-dagrapportage,{ version_text }
        .
        { getBlankMsiMessage(blankMsiEvent) }
      </opmerkingen>
    </HHM-dagrapport>
  }

  def getBlankMsiMessage(msiBlankEvent: Option[MsiBlankEvent]): String = {
    log.debug("msiBlankEvent:" + msiBlankEvent)
    msiBlankEvent match {
      case Some(_blankMsiEvent) ⇒

        val blankMsiEvents = _blankMsiEvent.msiBlanks.map(msi ⇒ {
          "Defecte signaalgever: Os_comm_id: %d, Os_locatie: %s, Msi_rijstrook: %s, MSI_stand: BL".format(msi.os_comm_id, msi.os_locatie, msi.msi_rijstr_pos)
        })
        val result = blankMsiEvents.mkString(". ")
        log.debug("Found blankMsiEvents:" + result)
        result

      case None ⇒
        log.debug("Not found any blankMsiEvents.")
        ""
    }
  }

  protected def systemInfo: Elem = {
    val componentsXML: List[Elem] = components match {
      case Left(_) ⇒ Nil
      case Right(comp) ⇒ {
        comp.map(compCertificate ⇒ {
          val naam = compCertificate.name
          val active = activeCertificates.find(_.componentCertificate.name == naam)
          val soll = compCertificate.checksum
          val (tijd, ist) = active match {
            case None       ⇒ ("", "")
            case Some(cert) ⇒ (getDayReportSettings.formatDateToCEST(cert.time), cert.componentCertificate.checksum)
          }
          <component naam={ naam }>
            <certificaat soll={ soll }>
              <tijd>{ tijd }</tijd>
              <ist>{ ist }</ist>
            </certificaat>
          </component>
        })
      }
    }
    <systeem>
      { for (elem ← componentsXML) yield elem }{ systemState }
    </systeem>
  }

  protected def systemState: Elem = {
    val stateNL = getDayReportSettings.translateState.getOrElse(currentSystemState.state, "Unknown (%s)".format(currentSystemState.state))
    <status toestand={ stateNL } toelichting={ currentSystemState.reason }></status>
  }

  def output: Array[Byte] = {
    prettyFormat(xml).getBytes("ISO-8859-1")
  }

  private[reports] def prettyFormat(xml: Elem): String = {
    val printer = new PrettyPrinter(width = 920, step = 4)
    val firstLines =
      """<?xml version="1.0" encoding="ISO-8859-1"?>
        |<!-- Created by CSC -->
      """.stripMargin
    val buffer = new StringBuilder(firstLines)
    printer.format(xml, buffer)
    buffer.toString
  }

  /**
   * HHM-dagrapport/algemene info.
   * Vrij tekstveld voor het verstrekken van additionele (statische) informatie over het HHM-systeem
   * (bijvoorbeeld omschrijving rijstrookindeling)
   */
  def generalInfo: String = systemData.zkSystem.location.description

  def createMetricsXml(metrics: Metrics): Elem = {
    metrics match {
      case m: TCMetrics    ⇒ ExportTCMetrics.createTCMetricsXml(key, m, getDayReportSettings())
      case m: SpeedMetrics ⇒ ExportSpeedMetrics.createSpeedMetricsXml(key, m, getDayReportSettings())
      case m: RedMetrics   ⇒ ExportRedMetrics.createRedMetricsXml(key, m, getDayReportSettings())
    }
  }

  def getUserName(userId: String) = {
    if (userId.toUpperCase == "SYSTEM")
      "systeem"
    else
      users.find(_.id == userId).map(_.username).getOrElse("Onbekend")
  }

  /**
   * Get SystemEvents
   * Informatieve meldingen rond de status van het systeem, conform systeemlogeisen
   * in de specificatie van het systeem.
   * Hieronder vallen:
   * - systeemstatuswijzigingen;
   * - alarmen incl. komend/gaand status;
   * - storingen;
   * - configuratiewijzigingen;
   * - gebruikersacties,
   * Onder systeemstatuswijzigingen worden minimaal gelogd:
   * - statuswijziging bedrijfstoestand (stand-by, operationeel, degraded, enz.);
   * - handhavingtoestand (autonoom, sectie).
   * Onder configuratiewijzigingen worden minimaal gelogd:
   * - wijzigingen handhavingtraject;
   * - wijziging snelheidslimiet, pardonmarge.
   */
  protected def reportsLog: Elem = {
    def entry(logEvent: ReportLogRecord): Elem = {
      val event = logEvent.event
      val date: String = getDayReportSettings.formatDateToCEST(event.timestamp)
      val systeemToestand: String = getDayReportSettings.translateToType(event)

      val reason = getAlternativeReasonText(event, systeemToestand).getOrElse(event.componentId + " " + event.eventType + " " + event.reason)

      <melding volgnr={ logEvent.seqnr.toString } tijd={ date } type={ systeemToestand } id={ getReportId(logEvent) }>
        { reason }
      </melding>
    }
    <meldingenlog>
      { for (event ← systemEvents) yield entry(event) }
    </meldingenlog>
  }

  protected def getReportId(event: ReportLogRecord): String = {
    event.corridorLogId
  }

  protected def getAlternativeReasonText(event: SystemEvent, systemState: String): Option[String] = {
    if (systemState == "USER")
      Some("%s:%s".format(getUserName(event.userId), event.reason))
    else
      None
  }

  /**
   * Process ZkSystemAlert
   * HHM-dagrapport/log/actuele-storingen/storingsmelding:
   * Tekst van de storingsmelding waaruit blijkt welk onderdeel van het systeem een storing heeft en wat de
   * aard van de storing is.
   */
  protected def alarmesLog: Elem = {
    val setting = getDayReportSettings()
    def entry(alert: ZkAlertHistory): Elem = {
      val date: String = getDayReportSettings.formatDateToCEST(alert.startTime)
      <storingsmelding tijd={ date } av-t-reductie={ setting.round(alert.reductionFactor).toString }>
        { alert.message }
      </storingsmelding>
    }
    <actuele-storingen>
      { for (alert ← getCurrentAlerts) yield entry(alert) }
    </actuele-storingen>
  }

  protected def getCurrentAlerts: Seq[ZkAlertHistory] = {
    val endTime = key.tomorrow.start //backward compatible
    systemAlerts.values.flatten.filter(alert ⇒ alert.endTime >= endTime).toSeq
  }

  /**
   * Toelichting: de beeldstandenlog is een afspiegeling van het mtmn999n.txt -bestand, zoals gespecificeerd
   * in de “interfacespecificatie HHM – verwerking”, versie 1.4, LPTV. Indien een systeem over meerdere
   * handhavingtrajecten rapporteert, dan dienen de beeldstandenlogs van de verschillende handhavingtrajecten
   * te worden samengevoegd tot één beeldstandenlog.
   */
  protected def mtmLog: Elem = {
    //TODO MTM for different systems must be merged into one
    def entry(mtm: MtmSpeedSetting, i: Int): Elem = {
      val date: String = getDayReportSettings.formatDateToCEST(mtm.time)
      val finalDate = if (!date.startsWith(key.datum) && date.endsWith("00:00:00")) {
        key.datum + "T24:00:00"
      } else {
        date
      }
      <beeldstand volgnr={ i.toString } tijd={ finalDate } locatie={ mtm.location } beeld={ getDayReportSettings.translateSetting(mtm.setting) }></beeldstand>
    }
    val indexed = mtmSettings.zipWithIndex
    <MTM-info>
      { for (mtm ← indexed) yield entry(mtm._1, mtm._2 + 1) }
    </MTM-info>
  }

}

class DayReportBuilderV40Settings {
  val version = DayReportVersion.HHM_V40

  val NR_DECIMALS: Int = 2
  val ROUNDING_MODE: BigDecimal.RoundingMode.RoundingMode = BigDecimal.RoundingMode.HALF_UP

  val translateState = Map[String, String](
    "Off" -> "HANDHAVEN (OFFLINE)", //OFF state isn't available
    "Maintenance" -> "HANDHAVEN (OFFLINE)", //OFF state isn't available
    "StandBy" -> "STAND-BY",
    "Alert" -> "ALARM",
    "Failure" -> "ALARM",
    "EnforceOn" -> "HANDHAVEN",
    "EnforceOff" -> "HANDHAVEN (OFFLINE)",
    "EnforceDegraded" -> "HANDHAVEN (DEGRADED)")
  def translateStateLogEntry = translateState

  val dayFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", new Locale("nl", "NL"))

  /**
   * round a given value of with the number of decimals and rounding mode
   * @param value the value to round off
   * @param nrDecimals the number of decimals that the result must have. default 3 decimals
   * @param roundingMode how the last resulting decimals must be rounded. default UP
   * @return resulting BigDecimal
   */
  def round(value: BigDecimal, nrDecimals: Int = NR_DECIMALS, roundingMode: BigDecimal.RoundingMode.RoundingMode = ROUNDING_MODE): BigDecimal = {
    value.setScale(nrDecimals, roundingMode)
  }

  /**
   * Translate MTM info setting to XSD mtm-info beeld
   * Only stand-by differs between MTM info file and XSD
   * @param setting
   * @return XSD correct value
   */
  def translateSetting(setting: String): String = {
    if (setting == "stand-by") {
      "standby"
    } else {
      setting
    }
  }

  /**
   * Determine the Type of the event. Possible values:
   * "ALARM","STORING","STATUS","USER","INFO"
   *
   * @param event
   * @return Type
   */
  def translateToType(event: SystemEvent): String = {
    if (event.componentId == "web") {
      return "USER"
    }
    val selfTest = ".*SELFTEST.*".r
    val solved = ".*SOLVED.*".r
    val status = ".*STATUS.*".r
    val state = ".*STATE.*".r

    event.eventType.toUpperCase match {
      case "ALARM"             ⇒ "ALARM"
      case "STORING"           ⇒ "STORING"
      case "STATUS"            ⇒ "STATUS"
      case "USER"              ⇒ "USER"
      case "INFO"              ⇒ "INFO"
      case "ALERT"             ⇒ "STORING"
      case "FAILURE"           ⇒ "ALARM"
      case "OFF"               ⇒ "STATUS"
      case "MAINTENANCE"       ⇒ "STATUS"
      case "STANDBY"           ⇒ "STATUS"
      case "ENFORCEON"         ⇒ "STATUS"
      case "ENFORCEOFF"        ⇒ "STATUS"
      case "ENFORCEDEGRADED"   ⇒ "STATUS"
      case "SYSTEMSTATECHANGE" ⇒ "STATUS"
      case status()            ⇒ "STATUS"
      case state()             ⇒ "STATUS"
      case selfTest()          ⇒ "INFO"
      case solved()            ⇒ "INFO"
      case other               ⇒ "INFO"
    }
  }

  /**
   * Determine the TypeCode of the event. Possible values:
   * "A","S","T","U","I"
   *
   * @param event
   * @return TypeCode
   */
  def translateToTypeCode(event: SystemEvent): String = {

    val jaiError = """JAI.*ERROR""".r
    val ntp = """ntp.*""".r

    (translateToType(event), event.componentId) match {
      case ("STATUS", _)                   ⇒ "T"
      case ("ALARM", "RegisterViolations") ⇒ "S"
      case ("INFO", ntp())                 ⇒ "S"
      case ("ALARM", _)                    ⇒ "A"
      case ("STORING", _)                  ⇒ "S"
      case ("USER", _)                     ⇒ "U"
      case ("INFO", "ftp")                 ⇒ "U" // in case of user ftp actions have it set to "U" (user), done here because the abstract class sends an info for other events as well

      case ("INFO", _)                     ⇒ "I"
      case (jaiError(), _)                 ⇒ "S"
      case (_, _)                          ⇒ "I"
    }
  }

  def translateComponentToCode(systemEvent: SystemEvent): String = {
    val jarfiles = """.*\.jar""".r
    val doorAlerts = """.*door.*""".r
    val camera = """.*camera.*""".r
    val ntp = """ntp.*""".r

    /**
     *  This code has been changed in order to be able to comply to the codes of CJIB
     *  As we loose some information in the process, for instance in the sitebuffer a sensor event is made with
     *  type information about what sensor and this information is lost in the process bringing the event to zookeeper (see SystemeventFactory classes etc)
     *  , we use the String(!!!!) to match a specific event. For instance does it contains "hot" so we now the temp was too hot....
     *
     *  This code is fragile; i.e. the moment the business decides they want the reason codes changes, some of the code below might BREAK!!
     *
     *  Be aware of this.
     *
     */

    def reasonMatches(regexp: String) = {
      // (?s) means can handle newlines, do not remove!
      systemEvent.reason matches "(?s)" + regexp
    }

    def reasonContains(s: String): Boolean = {
      val tokens = StringEscapeUtils.unescapeXml(systemEvent.reason).split(">")
      if (tokens.length == 2) { tokens(1).contains(s) } else { false }
    }

    (systemEvent.componentId.toLowerCase, systemEvent.eventType.toLowerCase) match {
      case ("sitebuffer", "failure") if (systemEvent.reason.contains("geopend")) ⇒ "0100000"
      case ("sitebuffer", "failure") if (systemEvent.reason.contains("gesloten")) ⇒ "0200000"
      case ("sitebuffer", "failure") if (systemEvent.reason.contains("scheefstand")) ⇒ "0400000"
      case ("sitebuffer", "failure") if (systemEvent.reason.contains("Relatieve vochtigheid")) ⇒ "0200000"
      case ("sitebuffer", "failure") if (List("Temperatuur is buiten bereik", "hot").count(systemEvent.reason.contains) == 2) ⇒ "0310000"
      case ("sitebuffer", "failure") if (List("Temperatuur is buiten bereik", "cold").count(systemEvent.reason.contains) == 2) ⇒ "0320000"
      case ("sitebuffer", "alert") if (systemEvent.reason.contains("Te lang geen berichten van de iRose ontvangen")) ⇒ "0200000"
      case ("sitebuffer", "sensorfailure") if (systemEvent.reason.contains("Ongeldige snelheidsmeting")) ⇒ "0200000"
      case ("sitebuffer", "failure") if (systemEvent.reason.contains("problemen met NTP")) ⇒ "1000000" // new mapping to be communicate to CJIB
      case ("sitebuffer", "failure") if (systemEvent.reason.contains("problemen met de temperatuursensor")) ⇒ "0200000"
      case ("sitebuffer", "failure") if (systemEvent.reason.contains("problemen met de vochtigheidssensor")) ⇒ "0200000"
      case ("sitebuffer", "failure") if (systemEvent.reason.contains("problemen met de accelerometer")) ⇒ "0200000"
      case ("sitebuffer", "loopfailure") ⇒ "0200000"
      case ("sitebuffer", "cameracomm") ⇒ "0200000"
      case ("schedulemonitor", "enforceoff") ⇒ "1000000"
      case ("schedulemonitor", "enforceon") ⇒ "3000000"
      case ("registerviolations", "alarm") ⇒ "0600000" // certificaat check fail
      case ("registerviolations", "info") ⇒ "0300000" //
      case ("registerviolations", "alert") ⇒ "0600000" //
      case ("processmtmdata", "alert") ⇒ "0300000"
      case ("selftest", "selftest-start") ⇒ "0201000" // calibratie start
      case ("selftest", "selftest-end") ⇒ "0202000" // calibratie einde
      case ("selftest", "selftest-success") ⇒ "0202000" // calibratie einde
      case ("selftest", "failure") ⇒ "0500000" // calibratie gefaald
      case ("vehicleregistration", "alarm") ⇒ "0600000"
      case (camera(), _) ⇒ "0200000"
      case (jarfiles(), "alarm") ⇒ "0000006" // certificaat failure
      case ("overtredingbestanden", "info") ⇒ "0600000"
      case ("signaaltijd", "info") ⇒ "0600000"
      case ("bewaartijd", "info") ⇒ "0600000"
      case ("ftp", "info") ⇒ "0400000" // wordt een user interactie melding
      case (ntp(), "info") ⇒ "0300000" // wordt gemapt naar STORING
      case ("smsbyemailnotifier", "info") ⇒ "0300000"
      case ("web", _) if reasonMatches(".*verbalisantcode.*heeft.*afgemeld.*") ⇒ "0103000"
      case ("web", _) if reasonMatches(".*verbalisantcode.*heeft.*bevestigd.*") ⇒ "0103000"
      case ("web", _) if reasonMatches(".*verbalisantcode.*heeft voor systeem.*status verandering van.*") ⇒ "0103000" // deze en hieronder
      case ("web", _) if reasonMatches(".*verbalisantcode.*heeft voor service.*status verandering van.*") ⇒ "0103000" // kunnen eigenlijk in 1 regel
      case ("web", _) if reasonMatches(".*verbalisantcode.*heeft voor systeem.*beheerparameters.*") ⇒ "0102000"
      case ("web", _) ⇒ "0300000"
      case ("invalidcertificate", _) ⇒ "0000006"
      case ("preselector", _) ⇒ "0300000"
      case ("boot", _) ⇒ "0300000"
      case ("systemstate", "failure") ⇒ "0500000"
      case ("systemstate", "info") ⇒ "0300000"
      case ("systemstate", "systemstatechange") if reasonContains("Off") ⇒ "1000000"
      case ("systemstate", "systemstatechange") if reasonContains("Geen") ⇒ "1000000"
      case ("systemstate", "systemstatechange") if reasonContains("Stand-by") ⇒ "2000000"
      case ("systemstate", "systemstatechange") if reasonContains("Handhaven") ⇒ "3000000"
      case ("systemstate", "systemstatechange") if reasonContains("Alarm") ⇒ "4000000"
      case ("systemstate", "systemstatechange") if reasonContains("Maintenance") ⇒ "5000000"
      case ("selftest", _) ⇒ "0200000"
      case ("opslagcapaciteit", _) ⇒ "0300000"
      case ("sftp", _) ⇒ "0100000"
      case ("watchtime", _) ⇒ "0300000"
      case (doorAlerts(), "storing") ⇒ "0200000"
      case (doorAlerts(), "info") ⇒ "0200000"
      case (doorAlerts(), "alarm") ⇒ "0100000"
      case (doorAlerts(), "user") ⇒ "0200000"
      case ("speedLog", "info") ⇒ "0300000"
      case ("reporting", "alarm") ⇒ "0300000" // translated into "Storing" as there is no CJIB code for this alarm
      case ("reporting", "info") ⇒ "0300000"
      case ("sitebuffer", _) ⇒ "0200000" //meldingen wegkant
      case (other, _) ⇒ "0000999"
    }
  }

  /**
   * Convert Date to String. This is not always consistent.
   * Sometimes users expect more then 23 hours in the String representation.
   */
  def formatDateToCEST(timestamp: Long): String = {
    val cal = Calendar.getInstance(TimeZone.getTimeZone("CET"))
    cal.setTimeInMillis(timestamp)
    dayFormat.format(cal.getTime)
  }

  def produceEnforceLogElem(duration: StatsDuration, s: ZkState): Elem = {
    val timeString =
      if (s.timestamp == duration.tomorrow.start) duration.datum + "T23:59:59" else formatDateToCEST(s.timestamp)

    val systemState = translateStateLogEntry.getOrElse(s.state, s.state)
    val reason = createReason(s)
    <handhaaflog-entry tijd={ timeString } systeemtoestand={ systemState }>
      { reason }
    </handhaaflog-entry>
  }

  def produceEnforceLog(duration: StatsDuration, states: Seq[ZkState]): Elem = {
    <handhaaflog>
      { for (state ← states) yield produceEnforceLogElem(duration, state) }
    </handhaaflog>
  }

  // No longer used for TCMetrics.
  // TODO: replace enforceLogFiltered for RedMetrics and SpeedMetrics.
  def enforceLogFiltered(key: StatsDuration, periods: Seq[Period], systemStates: Seq[ZkState]): Elem = {
    def beginOfPeriodeReason = Messages("csc.sectioncontrol.reports.no_start_of_schedule_event")
    def endOfPeriodeReason = Messages("csc.sectioncontrol.reports.no_end_of_schedule_event")

    def createLogEntries(period: Period, systemStates: Seq[ZkState]): Seq[ZkState] = {
      // cut off milliseconds and order ascending
      val statesInSeconds = systemStates.map(s ⇒ s.copy(timestamp = 1000 * (s.timestamp / 1000))).
        sortWith { (a, b) ⇒ a.timestamp < b.timestamp }

      // split in states before start of this period and this at or after start of the period.
      val (beforePeriodStart, afterPeriodStart) = statesInSeconds.span(_.timestamp < period.from)
      val optStateBeforePeriod = beforePeriodStart.reverse.headOption

      //println("optStateBeforePeriod: " + optStateBeforePeriod)
      val statesInPeriod = afterPeriodStart.takeWhile(_.timestamp <= period.to)

      //println("statesInPeriod: " + statesInPeriod)
      statesInPeriod.toList match {
        case Nil ⇒
          optStateBeforePeriod match {
            case Some(x) ⇒ x.copy(timestamp = period.from, reason = beginOfPeriodeReason) :: x.copy(timestamp = period.to, reason = endOfPeriodeReason) :: Nil
            case _       ⇒ Nil
          }
        case head :: rest ⇒
          val last = rest.reverse.headOption

          val restWithLast = last match {
            case Some(x) if (x.timestamp == period.to) ⇒ rest
            case Some(x) if (x.timestamp != period.to) ⇒ x.copy(timestamp = period.to, reason = endOfPeriodeReason) :: rest
            case _ ⇒ if (head.timestamp < period.to) {
              head.copy(timestamp = period.to, reason = endOfPeriodeReason) :: Nil
            } else {
              Nil
            }
          }

          head match {
            case x if (x.timestamp == period.from) ⇒ x :: restWithLast
            case _ ⇒
              optStateBeforePeriod match {
                case Some(x) ⇒
                  x.copy(timestamp = period.from, reason = beginOfPeriodeReason) :: head :: restWithLast
                case _ ⇒
                  head.copy(timestamp = period.from, reason = beginOfPeriodeReason) :: head :: restWithLast
              }
          }
      }
    }

    val statesWithMidnights: Seq[(String, String, String)] = periods
      .flatten(period ⇒ createLogEntries(period, systemStates))
      .sortWith {
        (first, second) ⇒ first.timestamp < second.timestamp
      }
      .map {
        s ⇒
          val date = if (s.timestamp == key.tomorrow.start) key.datum + "T23:59:59" else formatDateToCEST(s.timestamp)
          val systemState = translateStateLogEntry.getOrElse(s.state, s.state)
          (date, systemState, createReason(s))
      }

    //println("statesWithMidnights: " + statesWithMidnights)
    //filter double events (possible due to translation)
    val filteredStates = if (statesWithMidnights.isEmpty) {
      statesWithMidnights
    } else {

      val filter = statesWithMidnights.tail.foldLeft(Seq(statesWithMidnights.head)) {
        case (list, (time, state, reason)) ⇒ if (list.last._2 == state) {
          list
        } else {
          list :+ (time, state, reason)
        }
      }
      //always return the last entry (the midnight record)
      if (filter.contains(statesWithMidnights.last)) {
        filter
      } else {
        filter :+ statesWithMidnights.last
      }
    }

    <handhaaflog>
      { for (state ← filteredStates) yield createHandhaafLogEntry(state) }
    </handhaaflog>
  }

  def createHandhaafLogEntry(state: (String, String, String)): Elem = {
    <handhaaflog-entry tijd={ state._1 } systeemtoestand={ state._2 }>
      { state._3 }
    </handhaaflog-entry>
  }
  def createReason(state: ZkState): String = state.reason

}