/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.reports.data

import java.io.{ File, FileOutputStream }
import java.util.zip.GZIPOutputStream
import java.util.TimeZone

import akka.actor.ActorSystem
import akka.event.{ Logging, LoggingAdapter }
import com.google.common.io.BaseEncoding
import net.liftweb.json.Serialization
import org.apache.commons.io.IOUtils

import csc.config.Path
import csc.curator.utils.Curator
import csc.dlog.LogEntry
import csc.sectioncontrol.enforce.{ DayViolationsStatistics, MsiBlankEvent, MtmSpeedSetting, MtmSpeedSettings }
import csc.sectioncontrol.messages.certificates.{ ActiveCertificate, ComponentCertificate }
import csc.sectioncontrol.messages.{ CorridorKey, EsaProviderType, StatsDuration, SystemEvent }
import csc.sectioncontrol.reports.{ CorridorData, SystemData, _ }
import csc.sectioncontrol.reports.export._
import csc.sectioncontrol.reports.metrics.{ MetricsGenerator, Period }
import csc.sectioncontrol.sign.certificate.CertificateService
import csc.sectioncontrol.storage.{ ZkAlertHistory, ZkDayReportConfig, ZkSystem, ZkUser, _ }
import csc.sectioncontrol.storagelayer.ServiceStatisticsRepositoryAccess
import csc.sectioncontrol.storagelayer.alerts.AlertService
import csc.sectioncontrol.storagelayer.corridor.{ CorridorServiceTrait, CorridorIdMapping }
import csc.sectioncontrol.storagelayer.state.StateService
import csc.sectioncontrol.reports.schedules.{ LoadSchedules, CorridorSpeedSchedule }
import csc.sectioncontrol.reports.metrics.common.Metrics
import scala.annotation.tailrec
import csc.sectioncontrol.reports.metrics.tc.{ TCMetricsGenerator, TCMetrics }

class RetrieveReportData(systemId: String,
                         curator: Curator,
                         system: ActorSystem,
                         serviceStatisticsAccess: ServiceStatisticsRepositoryAccess,
                         corridorService: CorridorServiceTrait) {
  val log = Logging(system, classOf[RetrieveReportData])

  // Get the system data
  val retrieveSystemData = new RetrieveSystemData(systemId, curator, system, corridorService)

  def getActiveCertificates(key: StatsDuration): List[ActiveCertificate] = {
    val activeSoftwareCertificate = CertificateService.getActiveSoftwareCertificate(curator, systemId, key.tomorrow.start)
    val activeConfigurationCertificate = CertificateService.getActiveConfigurationCertificate(curator, systemId, key.tomorrow.start)

    // Get these to be able to use the time of the latest active certificate.
    val mostRecentActiveSoftwareCertificate = CertificateService.getActiveSoftwareCertificateComponents(curator, systemId, key.tomorrow.start).sortBy(_.time).last
    val mostRecentActiveConfigurationCertificate = CertificateService.getActiveConfigurationCertificateComponents(curator, systemId, key.tomorrow.start).sortBy(_.time).last

    // Create two active certificates, one for software and one for configuration with the time of the last created active certificate.
    List(ActiveCertificate(mostRecentActiveSoftwareCertificate.time, activeSoftwareCertificate), ActiveCertificate(mostRecentActiveConfigurationCertificate.time, activeConfigurationCertificate))
  }

  // Only two checksums will be shown as installed in the report. The software and configuration.
  // For all components in the software certificate a combined hash will be calculated and assigned to a software ComponentCertificate.
  // The same is done for the configuration.
  def getInstalledComponents(key: StatsDuration): List[ComponentCertificate] = {
    List(CertificateService.getInstalledSoftwareCertificate(curator, systemId, key.tomorrow.start),
      CertificateService.getInstalledConfigurationCertificate(curator, systemId, key.tomorrow.start))
  }
  /**
   * Main entry point for creating an report
   * @param key key in ZooKeeper for which the report must be generated for
   * @return
   */
  def create(key: StatsDuration,
             systemEvents: Iterator[LogEntry[SystemEvent]],
             violationStatistics: DayViolationsStatistics,
             exportVersion: DayReportVersion.Value,
             config: Option[ZkDayReportConfig]): DayReportBuilder = {

    val endTime = key.tomorrow.start

    // Get the corridor information for this sytem
    val systemData = retrieveSystemData.getSystemData

    // Get the states of the corridors during the day
    val corridorStates: Map[CorridorIdMapping, Seq[ZkState]] = StateService.getStateLogHistory(systemId, key.start, endTime, curator)

    // And what is the current state of all the corridors?
    val currentSystemState = getCurrentSystemState(corridorStates)

    // Get all the corridor alerts during the day
    val corridorAlerts: Map[CorridorIdMapping, List[ZkAlertHistory]] = AlertService.getAlertLogHistory(curator, systemId, 0L, endTime)

    // Get the mtm (esa) settings for the the system
    val mtmSettings: List[MtmSpeedSetting] = getMtmSettings(key)

    // Get the schedule periods for the corridors
    val corridorSpeedSchedules: Map[Int, Seq[CorridorSpeedSchedule]] = LoadSchedules.getSchedules(systemId, key.start, key.tomorrow.start, TimeZone.getTimeZone(key.timezoneID), curator)

    // Get all the users of the system
    val users: List[ZkUser] = getUsers

    // Get the Service Statistics like matched, unmatched, average speed (ServiceStatistics table HBase) for the corridors
    val serviceResults = readServiceResults(key, systemData.corridors)

    // Create start-end time period per schedule
    val periods = createPeriods(key.start, corridorSpeedSchedules)

    // Get the configuration for parallel corridors for this report
    val optParallelConfigs = curator.get[List[ParallelCorridorConfig]](Paths.Systems.getParallelCorridorConfigPath(systemId))

    // Get the stored MsiBlankEvent with the essential info to construct the lines for the 'opmerkingen' element
    val msiBlankLines = getMsiBlanksLines(systemId, key, curator)

    log.info("corridorStates: " + corridorStates)
    log.info("corridorSpeedSchedules: " + corridorSpeedSchedules)
    log.info("periods: " + periods)

    import RetrieveReportData._

    val calibrationFailurePeriods: Map[CorridorIdMapping, Seq[Period]] =
      if (requiresCalibration(exportVersion)) retrieveSystemData.getCalibrationFailurePeriods(key) else Map.empty

    // Create the input for the report module
    val input = ReportInput(systemId, key, exportVersion, systemEvents.toList, getInstalledComponents(key), getActiveCertificates(key),
      violationStatistics, systemData, corridorStates, currentSystemState, corridorAlerts, mtmSettings, corridorSpeedSchedules,
      users, serviceResults, periods, calibrationFailurePeriods, optParallelConfigs.getOrElse(List()), msiBlankLines)

    saveReportInput(input, log, None) //this can be really useful for troubleshooting, if needed

    val dayReportConfig = config.getOrElse(fallbackDayReportConfig)

    val reportName = dayReportConfig.dayReportName

    // Create the (XML) report
    RetrieveReportData.create(input, reportName, Some(log))
  }

  private def getCurrentSystemState(corridorStates: Map[CorridorIdMapping, Seq[ZkState]]): ZkState = {
    val systemStates = StateService.aggregatedStates(corridorStates.values.toSeq)
    systemStates.lastOption.getOrElse(ZkState("Failure", 0L, "System", "StateLog missing"))
  }

  private def getUsers: List[ZkUser] = {
    curator.getChildren(Paths.Users.getRootPath).foldLeft(List[ZkUser]()) {
      (list, path) ⇒ curator.get[ZkUser](path).map(_ :: list).getOrElse(list)
    }
  }

  private def getMtmSettings(stats: StatsDuration): List[MtmSpeedSetting] = {
    val systemCfgPath = Paths.Systems.getConfigPath(systemId)
    val esaProvider = curator.get[ZkSystem](systemCfgPath).map(_.esaProviderType).getOrElse(EsaProviderType.NoProvider)
    if (esaProvider == EsaProviderType.NoProvider) {
      //Don't get data when there isn't a esa provider
      return Nil
    }
    val mtmPath = Path(Paths.Systems.getRuntimeMtmInfoPath(systemId, stats))
    curator.get[MtmSpeedSettings](mtmPath) match {
      case None       ⇒ Nil
      case Some(mtms) ⇒ mtms.speedSettings
    }
  }

  private def createPeriods(start: Long, schedules: Map[Int, Seq[CorridorSpeedSchedule]]): Map[Int, Seq[Period]] = {

    schedules.foldLeft(Map[Int, Seq[Period]]()) {
      (map, schedule) ⇒
        val (corridorId, schedules) = schedule
        val periods = schedules.map {
          s ⇒
            Period(s.startTime, s.endTime)
        }
        map + (corridorId -> periods)
    }
  }

  private[reports] def readServiceResults(stats: StatsDuration, corridorData: Seq[CorridorData]): Map[Int, Seq[ServiceStatistics]] = {
    corridorData.map(corridor ⇒ {
      val infoId = corridor.corridor.info.corridorId

      // Depends on the Corridor Service type what kind of statistics are collected
      val statistics = corridor.corridor.serviceType match {
        case ServiceType.SectionControl ⇒
          serviceStatisticsAccess.getSectionControlStatistics(systemId, stats).filter { scStats ⇒ scStats.config.id == infoId }

        case ServiceType.RedLight ⇒
          serviceStatisticsAccess.getRedLightStatistics(systemId, stats)

        case ServiceType.SpeedFixed ⇒
          serviceStatisticsAccess.getSpeedFixedStatistics(systemId, stats)

        case _ ⇒ Seq[ServiceStatistics]()
      }
      infoId -> statistics
    }).toMap
  }

  // Get the stored MsiBlankEvent with the essential info to construct the lines for the 'opmerkingen' element
  private def getMsiBlanksLines(systemId: String, key: StatsDuration, curator: Curator): Option[MsiBlankEvent] = {
    val msiBlanksPath = Path(Paths.Systems.getRuntimeMsiBlanksPath(systemId, key))
    log.debug("msiBlanksPath:" + msiBlanksPath)
    curator.get[MsiBlankEvent](msiBlanksPath) match {
      case None ⇒ None
      case Some(msiBlankEvent) ⇒
        log.debug("Found MsiBlankEvent:" + MsiBlankEvent)
        Some(msiBlankEvent)
    }
  }

}

object RetrieveReportData {
  val Off = "Off"
  val Maintenance = "Maintenance"
  val StandBy = "StandBy"
  val Failure = "Failure"
  val EnforceOn = "EnforceOn"
  val EnforceOff = "EnforceOff"
  val EnforceDegraded = "EnforceDegraded"
  val enforce = Seq(EnforceOn, EnforceOff, EnforceDegraded)

  val formats = JsonFormats.formats + MapSerializer

  private[data] def mergeStates(systemStates: Seq[ZkState], corridorStates: Seq[ZkState]): Seq[ZkState] = {
    //todo: check for inconsistency of states
    @tailrec
    def doMerge(mergedStates: Seq[ZkState], previousState: ZkState, input: Seq[ZkState]): Seq[ZkState] = input match {
      case Nil ⇒ (previousState +: mergedStates).reverse
      case currentState :: tail ⇒
        val timesMatch = previousState.timestamp == currentState.timestamp
        (timesMatch, previousState.state, currentState.state) match {
          case (_, StandBy, StandBy) ⇒ doMerge(mergedStates, previousState, tail) // Keep first standBy state
          case (true, _, StandBy)    ⇒ doMerge(mergedStates, previousState, tail) // skip current standBy state
          case (true, StandBy, _)    ⇒ doMerge(mergedStates, currentState, tail) // skip previous standBy state
          case other                 ⇒ doMerge(previousState +: mergedStates, currentState, tail)
        }
    }

    // Only keep states Off, StandBy, Failure and Maintenance. Remove the enforce states from system
    val filtered = systemStates.filter(state ⇒ state.state == Off || state.state == StandBy || state.state == Failure || state.state == Maintenance)

    //and uses the corridorStates
    val sortedStates = (filtered ++ corridorStates).sortWith { (first, second) ⇒ first.timestamp < second.timestamp }
    sortedStates match {
      case Nil    ⇒ Nil
      case h :: t ⇒ doMerge(Seq[ZkState](), h, t)
    }
  }

  def fallbackDayReportConfig = new ZkDayReportConfig(
    "Dagrapport",
    to = "",
    dateFormat = "yyyyMMss",
    properties = Map.empty)

  /**
   * This function creates the report.
   * WARNING: this method should not depend on retrieving data. All data should be available in the ReportInput
   * @param input all the data needed to create a report
   * @param reportName the name of the report
   * @param log logadatper
   * @return DayReportBuilder
   */
  def create(input: ReportInput, reportName: String, log: Option[LoggingAdapter]): DayReportBuilder = {

    val components = Right(input.components)

    val systemData = input.systemData
    val corridorStates = input.corridorStates
    val corridorAlerts = input.corridorAlerts
    val currentSystemState = input.currentSystemState
    val mtmSettings = input.mtmSettings
    val schedules: Map[Int, Seq[CorridorSpeedSchedule]] = input.corridorSchedules
    val users: List[ZkUser] = input.users

    val corridorStatistics: AllCorridorsStatistics = MetricsGenerator.calculate(input.violationStatistics,
      input.serviceResults,
      input.corridorSchedules,
      log)
    // CorridorID and the States during the day
    val corridorInfoStates = corridorStates.map { case (key, value) ⇒ key.infoId -> value }

    // CooridorID and the alerts during the day
    val corridorInfoAlerts = corridorAlerts.map { case (key, value) ⇒ key.infoId -> value }

    val metrics: Map[CorridorKey, Metrics] = MetricsGenerator.generateMetrics(input.key, systemData.zkSystem, systemData.corridors,
      corridorStatistics.corridorData, input.periods, corridorInfoStates, corridorInfoAlerts, schedules)

    val (tcMetricsMap_, speedAndRedMetrics) = metrics.partition((kv) ⇒ {
      val (_, value) = kv
      value match {
        case tc: TCMetrics ⇒ true
        case _             ⇒ false
      }
    })

    val tcMetricsMap = tcMetricsMap_.mapValues { v ⇒ v.asInstanceOf[TCMetrics] }

    // If there is any parallel config found for this system create the statistics for the
    // parallel corridors and add to the metricsList
    val tcMetricsList = if (!input.parallelConfigs.isEmpty)
      TCMetricsGenerator.applyParallelCorridorsToMetrics(tcMetricsMap, input.parallelConfigs)
    else
      tcMetricsMap_.values.toList

    val sortedMetrics = (tcMetricsList ++ speedAndRedMetrics.values).sortBy(m ⇒ m.metricsTypeOrder).toList

    log.foreach { logger ⇒ logger.info("Total number of metrics instances: {}", sortedMetrics.size) }

    val logRecords = TransformLogEvents.toReportRecords(systemData, schedules, input.systemEvents)
    val calibration = input.calibrationFailurePeriods

    // Return the right reportbuilder with initiated data.
    input.exportVersion match {
      case DayReportVersion.HHM_V40 ⇒
        new DayReportBuilderV40(input.systemId, input.key, systemData, corridorStatistics, components, input.activeCertificates,
          currentSystemState, corridorAlerts, mtmSettings, logRecords, sortedMetrics, users, reportName, input.MsiBlanksLines)
      case DayReportVersion.HHM_V42 ⇒
        new DayReportBuilderV42(input.systemId, input.key, systemData, corridorStatistics, components, input.activeCertificates,
          currentSystemState, corridorAlerts, mtmSettings, logRecords, sortedMetrics, users, reportName, input.MsiBlanksLines)
      case DayReportVersion.HHM_V421 ⇒
        new DayReportBuilderV421(input.systemId, input.key, systemData, corridorStatistics, components, input.activeCertificates,
          currentSystemState, corridorAlerts, mtmSettings, logRecords, sortedMetrics, users, reportName, input.MsiBlanksLines)
      case DayReportVersion.HHM_V50 ⇒
        val system = MetricsGenerator.createSystemMetrics(input.key, corridorStates, corridorAlerts, calibration, metrics)
        new DayReportBuilderV50(input.systemId, input.key, systemData, corridorStatistics, components, input.activeCertificates,
          currentSystemState, corridorAlerts, mtmSettings, logRecords, sortedMetrics, system, users, reportName, input.MsiBlanksLines)
      case DayReportVersion.HHM_V60 ⇒
        val system = MetricsGenerator.createSystemMetrics(input.key, corridorStates, corridorAlerts, calibration, metrics)
        new DayReportBuilderV60(input.systemId, input.key, systemData, corridorStatistics, components, input.activeCertificates,
          currentSystemState, corridorAlerts, mtmSettings, logRecords, sortedMetrics, system, users, reportName, input.MsiBlanksLines)
    }
  }

  def requiresCalibration(version: DayReportVersion.Value): Boolean = version match {
    case DayReportVersion.HHM_V50 ⇒ true
    case DayReportVersion.HHM_V60 ⇒ true
    case _                        ⇒ false
  }

  /**
   * Tries to save (best effort) the ReportInput json in a file.
   * If for some reason fails to save the file, dumps its base64 in the log.
   * This will never throw an exception
   * @param source
   * @param log
   * @param file
   */
  def saveReportInput(source: ReportInput, log: LoggingAdapter, file: Option[File]): Unit = {
    var base64: Option[String] = None

    try {
      val targetFile: File = file.getOrElse(reportInputLogFile(source))
      val data = stripUnecessaryFields(source) //removes some fields
      val json = Serialization.write(data)(formats)
      //println(json)
      val jsonBytes = json.getBytes
      base64 = Some(BaseEncoding.base64().encode(jsonBytes)) //base64 of json
      targetFile.getParentFile.mkdirs()
      log.info("Writing report input data to file {}", targetFile.getAbsolutePath)
      //FileUtils.writeByteArrayToFile(targetFile, jsonBytes)
      writeToGzip(targetFile, jsonBytes)
    } catch {
      case e: Throwable ⇒
        e.printStackTrace
        //we cannot let this ever stop the report
        log.warning("Unable to write file for report input for key {}: {} ", source.key.toString, e.toString)
        base64 map { text ⇒
          log.warning("Dumping base64 contents:")
          log.warning(text)
        }
    }
  }

  val extension = ".json.gz"

  private def writeToGzip(file: File, data: Array[Byte]): Unit = {
    val out = new GZIPOutputStream(new FileOutputStream(file))
    try {
      IOUtils.write(data, out)
      out.finish()
    } finally {
      out.close()
    }
  }

  private def reportInputLogFile(source: ReportInput): File = {
    val base1 = new File("logs")
    val base2 = new File("/tmp")
    val base = if (base1.exists() && base1.canWrite) base1 else base2
    val folder = new File(base, "reportInputs")
    val filename = source.key.key + "_" + System.currentTimeMillis() + extension
    new File(folder, filename)
  }

  /**
   * Removes fields that are not relevant and should not be logged
   * @param input
   * @return
   */
  private def stripUnecessaryFields(input: ReportInput): ReportInput = {
    val users = input.users.map { u ⇒ relevantUser(u) }
    input.copy(users = users)
  }

  //clears user fields that should not be logged
  private def relevantUser(user: ZkUser): ZkUser = user.copy(phone = "", email = "", passwordHash = "")
}

/**
 * WARNING: there are problems with maps using Int als key.
 * when adding Map with Int as key you should also use the IntFix in RecreateReportsTool.parseReportInput
 * @param systemId
 * @param key
 * @param exportVersion
 * @param systemEvents
 * @param components
 * @param activeCertificates
 * @param violationStatistics
 * @param systemData
 * @param corridorStates
 * @param currentSystemState
 * @param corridorAlerts
 * @param mtmSettings
 * @param corridorSchedules
 * @param users
 * @param serviceResults
 * @param periods
 * @param calibrationFailurePeriods
 * @param parallelConfigs
 * @param MsiBlanksLines
 */
case class ReportInput(systemId: String,
                       key: StatsDuration,
                       exportVersion: DayReportVersion.Value,
                       systemEvents: List[LogEntry[SystemEvent]],
                       components: List[ComponentCertificate],
                       activeCertificates: List[ActiveCertificate],
                       violationStatistics: DayViolationsStatistics,
                       systemData: SystemData,
                       corridorStates: Map[CorridorIdMapping, Seq[ZkState]],
                       currentSystemState: ZkState,
                       corridorAlerts: Map[CorridorIdMapping, List[ZkAlertHistory]],
                       mtmSettings: List[MtmSpeedSetting],
                       corridorSchedules: Map[Int, Seq[CorridorSpeedSchedule]],
                       users: List[ZkUser],
                       serviceResults: Map[Int, Seq[ServiceStatistics]],
                       periods: Map[Int, Seq[Period]],
                       calibrationFailurePeriods: Map[CorridorIdMapping, Seq[Period]],
                       parallelConfigs: List[ParallelCorridorConfig],
                       MsiBlanksLines: Option[MsiBlankEvent])
