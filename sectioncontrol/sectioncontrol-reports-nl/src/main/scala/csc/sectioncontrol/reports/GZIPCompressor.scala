/*
* Copyright (C) 2012 CSC. <http://www.csc.com>
*/

package csc.sectioncontrol.reports

import java.io.{ ByteArrayInputStream, ByteArrayOutputStream }
import java.util.zip.{ GZIPInputStream, Deflater, GZIPOutputStream }
import scala.Array
import resource._

/**
 * used for compressing(gzip) byte arrays
 */
trait GZIPCompressor {
  /**
   * compresses a byte array using GZip
   * @param data to be compressed
   * @return compressed version of data
   */
  def compress(data: Array[Byte]): Array[Byte] = {
    managed(new ByteArrayOutputStream).acquireAndGet { os ⇒
      managed(new GZIPOutputStream(os, Deflater.BEST_COMPRESSION)).acquireAndGet(_.write(data))
      os.toByteArray
    }
  }

  /**
   * decompress a byte array using GZip.
   * Warning: This will only work if memory is larger than the given data and the decompress data combined
   * @param data to be decompressed
   * @return decompressed version of data
   */
  def decompress(data: Array[Byte]): Array[Byte] = {
    managed(new GZIPInputStream(new ByteArrayInputStream(data))) acquireAndGet {
      gzip ⇒ Stream.continually(gzip.read()).takeWhile(-1 != _).map(_.toByte).toArray
    }
  }
}
