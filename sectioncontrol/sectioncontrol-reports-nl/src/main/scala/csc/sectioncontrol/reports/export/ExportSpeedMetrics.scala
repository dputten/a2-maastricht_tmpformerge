/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.reports.export

import xml.Elem
import csc.sectioncontrol.messages.StatsDuration
import csc.sectioncontrol.storage.DayReportVersion
import csc.sectioncontrol.reports.metrics.speed.SpeedMetrics

object ExportSpeedMetrics {
  def createSpeedMetricsXml(key: StatsDuration, metrics: SpeedMetrics, settings: DayReportBuilderV40Settings): Elem = {
    <snelheid id={ metrics.id }>
      <instellingen>
        <snelheidslimiet eenheid="kmpuur">{ metrics.speedLimit }</snelheidslimiet>
        {
          if (settings.version != DayReportVersion.HHM_V60 && metrics.hasESA) {
            <ESA-scantijd eenheid="seconden">{ metrics.esaScanTime_secs }</ESA-scantijd>
            <ESA-pardontijd eenheid="seconden">{ metrics.esaPardonTime_secs }</ESA-pardontijd>
          }
        }
      </instellingen>
      <passagepunt stroken={ metrics.lanes.size.toString() }/>
      {
        if (metrics.hasStatistics) {
          <FIP-statistiek>
            <passages totaal={ metrics.total.toString }>
              { for (lane ← metrics.lanes) yield <strook locatie={ lane._1 }>{ lane._2.toString }</strook> }
            </passages>
            <snelheden>
              <gemiddeld eenheid="kmpuur">{ metrics.averageSpeed.toString }</gemiddeld>
              <max eenheid="kmpuur">{ metrics.maxSpeed.toString }</max>
            </snelheden>
            <overtredingen>
              <overtredingen-totaal>{ metrics.violations.totalViolations.toString }</overtredingen-totaal>
              <hand>{ metrics.violations.manual }</hand>
              <auto>{ metrics.violations.auto }</auto>
              <mobi-niet-verwerken>{ metrics.violations.mobiDoNotProcess }</mobi-niet-verwerken>
              {
                if (settings.version != DayReportVersion.HHM_V60 && metrics.hasESA) {
                  <MTMratio>{ settings.round(metrics.violations.mtmRatio).toString }</MTMratio>
                  <MTM-pardon>{ metrics.violations.mtmPardon }</MTM-pardon>
                }
              }
            </overtredingen>
            <performance>
              <overtredingenratio>{ settings.round(metrics.performance.violationRatio).toString }</overtredingenratio>
              {
                if (settings.version == DayReportVersion.HHM_V50 || settings.version == DayReportVersion.HHM_V60) {
                  <measurementratio>{ metrics.measurementRatio.toString }</measurementratio>
                }
              }
              <autoratio>{ settings.round(metrics.performance.autoRatio).toString }</autoratio>
              {
                if (settings.version == DayReportVersion.HHM_V40 || settings.version == DayReportVersion.HHM_V42 ||
                  settings.version == DayReportVersion.HHM_V421) {
                  <autoratio-netto>{ settings.round(metrics.performance.autoRatioNetto).toString }</autoratio-netto>
                  <handhaafratio>{ settings.round(metrics.performance.enforceRatio).toString }</handhaafratio>
                  <storingenratio>{ settings.round(metrics.performance.failureRatio).toString() }</storingenratio>
                  <av-t-ratio>{ settings.round(metrics.performance.avtRatio).toString() }</av-t-ratio>
                  <avl-f-ratio>{ metrics.performance.avlf }</avl-f-ratio>
                } else {
                  <handhaafratio>{ settings.round(metrics.performance.enforceRatio).toString }</handhaafratio>
                }
              }
            </performance>
          </FIP-statistiek>
        } else {
          <FIP-statistiek/>
        }
      }
      { settings.enforceLogFiltered(key, metrics.periods, metrics.systemStates) }
    </snelheid>
  }

  def createSpeedMetricsXmlV60(key: StatsDuration, metrics: SpeedMetrics, settings: DayReportBuilderV40Settings): Elem = {
    //<snelheid id={ metrics.id }>
    <snelheid id={ metrics.idV6 }>
      <instellingen>
        <snelheidslimiet eenheid="kmpuur">{ metrics.speedLimit }</snelheidslimiet>
        {
          if (settings.version != DayReportVersion.HHM_V60 && metrics.hasESA) {
            <ESA-scantijd eenheid="seconden">{ metrics.esaScanTime_secs }</ESA-scantijd>
            <ESA-pardontijd eenheid="seconden">{ metrics.esaPardonTime_secs }</ESA-pardontijd>
          }
        }
      </instellingen>
      <passagepunt stroken={ metrics.lanesV6.size.toString() }/>
      {
        if (metrics.hasStatistics) {
          <FIP-statistiek>
            <passages totaal={ metrics.total.toString }>
              { for (lane ← metrics.lanesV6) yield <strook locatie={ lane._1 }>{ lane._2.toString }</strook> }
            </passages>
            <snelheden>
              <gemiddeld eenheid="kmpuur">{ metrics.averageSpeed.toString }</gemiddeld>
              <max eenheid="kmpuur">{ metrics.maxSpeed.toString }</max>
            </snelheden>
            <overtredingen>
              <overtredingen-totaal>{ metrics.violations.totalViolations.toString }</overtredingen-totaal>
              <hand>{ metrics.violations.manual }</hand>
              <auto>{ metrics.violations.auto }</auto>
              <mobi-niet-verwerken>{ metrics.violations.mobiDoNotProcess }</mobi-niet-verwerken>
              {
                if (settings.version != DayReportVersion.HHM_V60 && metrics.hasESA) {
                  <MTMratio>{ settings.round(metrics.violations.mtmRatio).toString }</MTMratio>
                  <MTM-pardon>{ metrics.violations.mtmPardon }</MTM-pardon>
                }
              }
            </overtredingen>
            <performance>
              <overtredingenratio>{ settings.round(metrics.performance.violationRatio).toString }</overtredingenratio>
              {
                if (settings.version == DayReportVersion.HHM_V50 || settings.version == DayReportVersion.HHM_V60) {
                  <measurementratio>{ metrics.measurementRatio.toString }</measurementratio>
                }
              }
              <autoratio>{ settings.round(metrics.performance.autoRatioV60).toString }</autoratio>
              {
                if (settings.version == DayReportVersion.HHM_V40) {
                  <autoratio-netto>{ settings.round(metrics.performance.autoRatioNetto).toString }</autoratio-netto>
                  <handhaafratio>{ settings.round(metrics.performance.enforceRatio).toString }</handhaafratio>
                  <storingenratio>{ settings.round(metrics.performance.failureRatio).toString }</storingenratio>
                  <av-t-ratio>{ settings.round(metrics.performance.avtRatio).toString }</av-t-ratio>
                  <avl-f-ratio>{ metrics.performance.avlf }</avl-f-ratio>
                } else {
                  <handhaafratio>{ settings.round(metrics.performance.enforceRatio).toString }</handhaafratio>
                }
              }
            </performance>
          </FIP-statistiek>
        } else {
          <FIP-statistiek/>
        }
      }
      { settings.enforceLogFiltered(key, metrics.periods, metrics.systemStates) }
    </snelheid>
  }

}