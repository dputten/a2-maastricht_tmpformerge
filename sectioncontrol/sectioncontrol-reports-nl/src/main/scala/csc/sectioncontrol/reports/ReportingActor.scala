/*
* Copyright (C) 2012 CSC. <http://www.csc.com>
*/

package csc.sectioncontrol.reports

import java.io.File
import java.text.SimpleDateFormat
import java.util.Date

import csc.sectioncontrol.reports.export.DayReportBuilder
import org.apache.commons.io.FileUtils
import org.apache.hadoop.hbase.client.HTable

import akka.actor.ActorLogging
import net.liftweb.json.DefaultFormats

import csc.json.lift.LiftSerialization
import csc.curator.utils.{ Versioned, Curator, CuratorActor }

import csc.sectioncontrol.messages._
import csc.sectioncontrol.storage._
import csc.sectioncontrol.enforce.DayViolationsStatistics
import csc.config.Path
import csc.dlog.hbase.EventLog
import csc.sectioncontrol.reports.i18n.Messages
import csc.hbase.utils.HBaseAdminTool
import data.RetrieveReportData
import csc.sectioncontrol.storagelayer.ServiceStatisticsRepositoryAccess
import org.apache.hadoop.conf.Configuration
import csc.sectioncontrol.storagelayer.corridor.{ CorridorService, CorridorServiceCuratorImpl }

class ReportingActor(val systemId: String, val curator: Curator, hbaseConfig: Configuration, serviceStatisticsAccess: ServiceStatisticsRepositoryAccess) extends CuratorActor with ActorLogging with GZIPCompressor with EmailSender {

  // Service for getting the corridor information from Zookeeper
  val corridorService = new CorridorServiceCuratorImpl(curator)

  // Retrieve data for this report from the system (HBase, Zookeeper, disk)
  val retrieveData = new RetrieveReportData(systemId, curator, context.system, serviceStatisticsAccess, corridorService)

  private def prepareRuntimeResult(statsDuration: StatsDuration): Option[DayViolationsStatistics] = {
    /*
     * Get the path where the runtime statistics for RegisterViolations run is
     * stored (DayViolationsStatistics per corridor)
     */
    val runtimeResultPath = Path(Paths.Systems.getRuntimeViolationsPath(systemId, statsDuration))
    curator.get[DayViolationsStatistics](runtimeResultPath)
  }

  /*
   * Possible messages
   * 	- GenerateReport
   */
  protected def receive = {
    case gr: GenerateReport ⇒ {
      log.info("Receive message 'GenerateReport'")

      val closeEvent = SystemEvent("reporting", System.currentTimeMillis(),
        systemId, "CloseLog", "system",
        reason = Some(Messages("csc.sectioncontrol.reports.create_day_report")))

      sendEvent(closeEvent)
      log.info("Wait 3000ms to continue")
      Thread.sleep(3000)

      // Path to report day (previous day)
      val key = gr.statsDuration
      val reportPath = Path(Paths.Systems.getReportPath(systemId, key))

      // Is there already a report created for this day?
      curator.getVersioned[ZkDayStatistics](reportPath) match {
        case None      ⇒ log.info("No previous day report found. Creating...")
        case Some(old) ⇒ log.warning("Day Report for date [%s] already exist. Overwriting...".format(old.data.id))
      }

      val report: Option[ZkDayStatistics] = prepareRuntimeResult(key) match {
        case None ⇒ {
          log.error("Runtime result for report [" + key.key + "] is not yet in.")
          None
        }
        case Some(violationStatistics) ⇒
          // The violations statistic are in for the report day, the report can created
          log.info("Creating report for date [%s]".format(key))
          try {
            val createdReport: ZkDayStatistics = createReport(key, violationStatistics, reportPath)
            Some(createdReport)
          } catch {
            case e: Exception ⇒
              log.error(e, "Failed creating day report [%s]".format(key.key))
              val reason = Messages("csc.sectioncontrol.reports.create_day_report_failed", e.getMessage)

              val alarmEvent = SystemEvent("reporting", System.currentTimeMillis(), systemId, "Alarm", "system", reason = Some(reason))
              sendEvent(alarmEvent)
              None
          }
      }
      // Send the report back to the sender
      sender ! report
    }
  }

  private def createReport(key: StatsDuration,
                           violationStatistics: DayViolationsStatistics,
                           reportPath: Path): ZkDayStatistics = {
    val reportPath = Path(Paths.Systems.getReportPath(systemId, key)) // Why is reportPath defined again?

    // How to format the Json strings?
    val serialization = new LiftSerialization {
      implicit def formats = DefaultFormats
    }

    // Get the logging from the Global system log table
    HBaseAdminTool.ensureTableExists(hbaseConfig, Paths.Systems.getSystemsLogTableName, "log")
    val logTable = new HTable(hbaseConfig, Paths.Systems.getSystemsLogTableName)
    val reader = EventLog.createReader(logTable, systemId, serialization)
    val iterator = reader.iterator[SystemEvent](key.start, key.tomorrow.start)

    //Report configuration in Zookeeper
    val configPath = Path(Paths.Configuration.getReportsConfigPath)

    // What type of report should be created for this system
    val createdReport: ZkDayStatistics = curator.getVersioned[ZkDayReportConfig](configPath) match {
      case None ⇒
        log.error("No configuration found to send the day report under {}.", configPath)
        //backward compatible: return default report version
        val data = retrieveData.create(key, iterator, violationStatistics, DayReportVersion.HHM_V40, None)

        // Return the default report
        ZkDayStatistics(key.key, key.start, System.currentTimeMillis(),
          data = compress(data.output), fileName = data.filenameZip)
      case Some(config) ⇒
        // Report configuration is found. Create the report as defined in the config
        val dayReportBuilder = retrieveData.create(key, iterator, violationStatistics, config.data.dayReportVersion, Some(config.data))

        // The dayReportBuilder contains are the information that is needed the create the right version of the report.
        val reportOutput = dayReportBuilder.output

        val report = ZkDayStatistics(key.key, key.start, System.currentTimeMillis(),
          data = compress(reportOutput), fileName = dayReportBuilder.filenameZip)

        //list of email recipients
        sendEmails(config, report, key)
        //write to file
        config.data.exportDirectory.foreach(directory ⇒ writeToFile(directory, dayReportBuilder, reportOutput))
        report
    }

    //write to zookeeper
    log.info("Storing day report [%s]...".format(key.key))

    // Overwrite; remove existing report
    if (curator.exists(reportPath))
      curator.deleteRecursive(reportPath)

    curator.put(reportPath, createdReport)

    log.info("Day report stored [%s].".format(key.key))
    logTable.close()

    createdReport
  }

  private def writeToFile(directory: String, data: DayReportBuilder, reportOutput: Array[Byte]): Unit = {
    val dirFile = new File(directory)
    try {
      if (!dirFile.exists()) {
        dirFile.mkdirs()
      }
      val file = new File(dirFile, data.filenamePlain)
      FileUtils.writeByteArrayToFile(file, reportOutput)
      sendEvent(SystemEvent("reporting", System.currentTimeMillis(), systemId, "ReportWrite", "system",
        Some(Messages("csc.sectioncontrol.reports.write_day_report", file.getAbsolutePath))))
      log.info("Write day report [%s]".format(file.getAbsolutePath))
    } catch {
      case ex: Exception ⇒ {
        log.error("Day report failed to write to %s: %s".format(dirFile.getAbsolutePath, ex.getMessage))
        sendEvent(SystemEvent("reporting", System.currentTimeMillis(), systemId, "ReportWriteFailed", "system",
          Some(Messages("csc.sectioncontrol.reports.write_day_report_failed", ex.getMessage))))
      }
    }
  }

  private def sendEmails(config: Versioned[ZkDayReportConfig], report: ZkDayStatistics, key: StatsDuration): Unit = {
    val toEmails: List[String] = config.data.to.split(";").filter(_.trim.length > 0).toList
    if (!toEmails.isEmpty) {
      // Mail the report
      val toEmailsForLogging: String = toEmails.mkString("\r\n")

      val subjectReportDate = (new SimpleDateFormat(config.data.dateFormat)).format(new Date(key.start))
      val subject = config.data.subject.format(subjectReportDate)
      val dayConfig = config.data.copy(subject = subject)

      val reportEvent = SystemEvent("reporting", System.currentTimeMillis(), systemId, "ReportSend", "system")
      sendEmail(toEmails, dayConfig, report) match {
        case Left(problem) ⇒
          log.error("E-mail with day report was not sent: %s".format(problem))
          sendEvent(reportEvent.copy(reason = Messages("csc.sectioncontrol.reports.send_day_report_failed", toEmailsForLogging)))
        case Right(count) ⇒ {
          log.info("%s e-mails with day report have been sent to %s".format(count, toEmailsForLogging))
          sendEvent(reportEvent.copy(reason = Messages("csc.sectioncontrol.reports.send_day_report", toEmailsForLogging)))
        }
      }
    }
  }

  private def sendEvent(systemEvent: SystemEvent) {
    val pathToEvents = Path(Paths.Systems.getLogEventsPath(systemId))
    if (!curator.exists(pathToEvents)) {
      curator.createEmptyPath(pathToEvents)
    }
    curator.appendEvent(pathToEvents / "qn-", systemEvent)
  }

}

