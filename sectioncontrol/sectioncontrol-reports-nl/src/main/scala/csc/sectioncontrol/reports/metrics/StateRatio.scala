/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

package csc.sectioncontrol.reports.metrics

import annotation.tailrec
import common.{ Ratio, Rounder }
import csc.sectioncontrol.storage.ZkState

case class StateRatio(state: String, periods: Seq[Period], statePeriods: Seq[Period], range: Period, allPeriods: Seq[Period] = Nil) {
  def timePeriods: Long = {
    if (periods.length > 0)
      periods.map(sp ⇒ sp.to - sp.from).sum
    else 0
  }

  def timeAllPeriods: Long = {
    if (allPeriods.length > 0)
      allPeriods.map(sp ⇒ sp.to - sp.from).sum
    else 0
  }

  val total = range.to - range.from
  def ratio: BigDecimal = Ratio.divide(timePeriods, timeAllPeriods)
}

object StateRatio {
  def apply(periods: Seq[Period], systemStates: Seq[ZkState]): Seq[StateRatio] =
    calculateRatios(periods, systemStates)

  def calculateRatios(periods: Seq[Period], systemStates: Seq[ZkState]): Seq[StateRatio] = {
    val total = Period(periods.map(_.from).min, periods.map(_.to).max)

    val states = statePeriods(systemStates)

    states.foldLeft(List[StateRatio]()) {
      (list, sr) ⇒
        val (state, statePeriods) = sr

        val periodsPerState = periods.map {
          period ⇒ ranges(state, statePeriods, period, total)
        }

        val totalPeriods = periodsPerState.flatten(_.periods)
        val mergedPeriods = merge(totalPeriods.toList)
        val totalRatio = StateRatio(state = state, periods = mergedPeriods, statePeriods = statePeriods, range = total, allPeriods = periods)
        totalRatio :: list
    }
  }

  @tailrec
  private def collapse(rs: List[Period], sep: List[Period] = Nil): List[Period] = rs match {
    case x :: y :: rest ⇒
      if (y.from > x.to)
        collapse(y :: rest, x :: sep)
      else
        collapse(Period(x.from, x.to max y.to) :: rest, sep)
    case _ ⇒
      (rs ::: sep).reverse
  }

  def merge(rs: List[Period]): List[Period] = collapse(rs.sortBy(_.from))

  def statePeriods(states: Seq[ZkState], endTime: Long = Long.MaxValue): Map[String, List[Period]] = {
    val sortedStates = states.sortWith { (first, second) ⇒ first.timestamp < second.timestamp }

    @tailrec
    def loop(state: ZkState, states: Seq[ZkState], systemStates: Map[String, List[Period]]): Map[String, List[Period]] = {
      states match {
        case Nil ⇒
          processPeriods(state, None, systemStates)
        case head :: rest ⇒
          loop(head, rest, processPeriods(state, Some(head), systemStates))
      }
    }

    def processPeriods(previous: ZkState, current: Option[ZkState], statePeriods: Map[String, List[Period]]): Map[String, List[Period]] = {
      val to = current.map(t ⇒ t.timestamp).getOrElse(endTime)
      val newPeriod = Period(previous.timestamp, to)

      val sortedPeriods = statePeriods
        .get(previous.state) //get current period list of state
        .map(_ :+ newPeriod) //add new period to current period list
        .getOrElse(List(newPeriod)) //no current period list? than create list with only new period
        .sortWith { (first, second) ⇒ first.from < second.from } //sort from period 00:00:00, 00:01:00, 01.00.00 etc

      //add or replace current state with new list of periods
      statePeriods + (previous.state -> sortedPeriods)

    }

    sortedStates match {
      case Nil          ⇒ Map.empty
      case head :: tail ⇒ loop(head, tail, Map.empty)
    }
  }

  def ranges(state: String, statePeriods: Seq[Period], period: Period, total: Period): StateRatio = {

    def calRange(statePeriod: Period, period: Period): Option[Period] = {
      val fromState = statePeriod.from
      val toState = statePeriod.to
      val fromPeriod = period.from
      val toPeriod = period.to

      if (fromState >= fromPeriod && fromState <= toPeriod && toState >= fromPeriod && toState <= toPeriod)
        Some(Period(fromState, toState)) // |..<----->..|
      else if (fromState >= fromPeriod && fromState <= toPeriod && toState >= toPeriod)
        Some(Period(fromState, toPeriod)) // |<---|-->
      else if (fromState <= fromPeriod && toState >= fromPeriod && toState <= toPeriod)
        Some(Period(fromPeriod, toState)) // <--|-->..|
      else if (fromState <= fromPeriod && toState >= toPeriod)
        Some(Period(fromPeriod, toPeriod)) // <--|.....|-->
      else
        None
    }

    val periods = statePeriods.foldLeft(List[Period]()) {
      (list, statePeriod) ⇒
        calRange(statePeriod, period) match {
          case Some(x) ⇒ x :: list
          case _       ⇒ list
        }
    }

    StateRatio(state, periods, statePeriods, total)
  }
}
