/*
* Copyright (C) 2012 CSC. <http://www.csc.com>
*/

package csc.sectioncontrol.reports

import csc.sectioncontrol.messages.{ CorridorStatistics, StatsDuration }

/**
 * Accumulated statistics for all the corridors during one day.
 * It is not stored in ZooKeeper but calculated in the report.
 */
case class AllCorridorsStatistics(systemId: String, stats: StatsDuration, corridorData: Seq[CorridorStatistics])

