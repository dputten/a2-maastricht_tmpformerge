/*
* Copyright (C) 2012 CSC. <http://www.csc.com>
*/

package csc.sectioncontrol.reports

import javax.mail._
import javax.mail.internet._
import java.util.Date
import csc.sectioncontrol.storage.ZkDayReportConfig

/**
 * used for sending emails
 */
trait EmailSender {

  /**
   *
   * @param dest
   * @param reportConfig contains all ino regarding sending an email
   * @param dayReport content is send as an attachment
   * @return either an exception (left) or the number of recipients (right)
   */
  def sendEmail(dest: Seq[String], reportConfig: ZkDayReportConfig, dayReport: ZkDayStatistics): Either[Throwable, Int] = {
    try {
      val properties = System.getProperties

      reportConfig.properties.foreach {
        case (key, value) ⇒ properties.setProperty(key, value)
      }

      val auth: Authenticator = if (reportConfig.requiresAuthentication) {
        new Authenticator() {
          override def getPasswordAuthentication: PasswordAuthentication = {
            new PasswordAuthentication(reportConfig.username.getOrElse(""), reportConfig.password.getOrElse(""))
          }
        }
      } else {
        null
      }

      val session = Session.getInstance(properties, auth)
      val message = new MimeMessage(session)
      val recipients: Array[Address] = dest.map(new InternetAddress(_)).toArray

      message.setFrom()

      if (reportConfig.useBBC) {
        message.setRecipients(Message.RecipientType.TO, properties.getProperty("mail.from"))
        message.setRecipients(Message.RecipientType.BCC, recipients)
      } else {
        message.setRecipients(Message.RecipientType.TO, recipients)
      }

      message.setSubject(reportConfig.subject)

      val content = new MimeMultipart
      val contentText = new MimeBodyPart
      val attachment = new MimeBodyPart

      contentText.setText(reportConfig.bodyText)
      attachment.setDisposition(Part.ATTACHMENT)
      attachment.setFileName(dayReport.fileName)
      //application/tar, application/x-tar application/x-gzip
      attachment.setContent(dayReport.data.toArray.map(_.toByte), "application/x-gzip")

      content.addBodyPart(contentText)
      content.addBodyPart(attachment)

      message.setContent(content)
      message.setSentDate(new Date())

      Transport.send(message)
      Right(recipients.size)
    } catch {
      case e: Exception ⇒ Left(e)
    }
  }
}
