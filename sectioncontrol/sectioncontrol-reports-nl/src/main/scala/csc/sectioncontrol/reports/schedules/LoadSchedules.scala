package csc.sectioncontrol.reports.schedules

import java.util.{ Calendar, GregorianCalendar, TimeZone }
import csc.curator.utils.Curator
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storage.ZkSystem
import csc.sectioncontrol.storage.ZkSection
import csc.sectioncontrol.storage.ZkCorridor
import csc.config.Path

/**
 * Copyright (C) 2016 CSC. <http://www.csc.com>
 *
 * Created on 3/23/16.
 */

case class CorridorSpeedSchedule(id: String,
                                 infoId: Int,
                                 speedLimit: Int,
                                 startTime: Long,
                                 endTime: Long)

object LoadSchedules {

  def getSchedules(systemId: String, dayStart: Long, dayEnd: Long, timezone: TimeZone, curator: Curator): Map[Int, List[CorridorSpeedSchedule]] = {
    val zkCorridors: List[ZkCorridor] = curator.getChildren(Path(Paths.Corridors.getDefaultPath(systemId))).flatMap(path ⇒ {
      val configPath = Path(Paths.Corridors.getConfigPath(systemId, path.nodes.last.name))
      curator.get[ZkCorridor](configPath)
    }).toList
    val schedules: List[CorridorSpeedSchedule] = zkCorridors.flatMap {
      zkCorridor ⇒ LoadSchedules.getCorridorSchedules(systemId, zkCorridor.id, dayStart, dayEnd, timezone, curator)
    }
    schedules.groupBy(_.infoId)
  }

  def getCorridorSchedules(systemId: String, corridorId: String, dayStart: Long, dayEnd: Long, timezone: TimeZone, curator: Curator): List[CorridorSpeedSchedule] = {
    val currentSchedulesPath = Paths.Corridors.getSchedulesPath(systemId, corridorId)
    val currentSchedules = loadCorridorSchedules(systemId, corridorId, dayStart, dayEnd, currentSchedulesPath, timezone, curator)

    val historicScheduleSaves: List[Long] = curator.getChildNames(Paths.Corridors.getScheduleHistoryPath(systemId, corridorId)).
      map(_.toLong).
      filter(t ⇒ t > dayStart && t < dayEnd).
      sorted.
      toList
    val historicIntervals = (dayStart :: historicScheduleSaves).zip(historicScheduleSaves)
    val historicSchedules = historicIntervals.map { interval ⇒
      val historicPath = Paths.Corridors.getScheduleHistoryPath(systemId, corridorId) + "/" + interval._2.toString
      loadCorridorSchedules(systemId, corridorId, interval._1, interval._2, historicPath, timezone, curator)
    }
    val endTimeLastHistoric = historicScheduleSaves.lastOption.getOrElse(0L)
    //filter schedules which ended before endTimeLastHistoric
    val activeCurrentSchedules = currentSchedules.filter(cur ⇒ cur.endTime > endTimeLastHistoric)
    updateCurrentSchedules(activeCurrentSchedules, endTimeLastHistoric) ++ historicSchedules.flatten
  }

  def loadCorridorSchedules(systemId: String, corridorId: String, timeStart: Long, timeEnd: Long, schedulesZkPath: String, timezone: TimeZone, curator: Curator): List[CorridorSpeedSchedule] = {

    val corridorSchedules = for {
      system ← curator.get[ZkSystem](Paths.Systems.getConfigPath(systemId))
      corridor ← curator.get[ZkCorridor](Paths.Corridors.getConfigPath(systemId, corridorId))
      schedules ← curator.get[List[ZkSchedule]](schedulesZkPath)
      startSection ← curator.get[ZkSection](Paths.Sections.getConfigPath(systemId, corridor.startSectionId))
      endSection ← curator.get[ZkSection](Paths.Sections.getConfigPath(systemId, corridor.endSectionId))
    } yield schedules.map(schedule ⇒ CorridorSpeedSchedule(corridor.id, corridor.info.corridorId,
      schedule.speedLimit,
      calculateDayTime(timeStart, schedule.fromPeriodHour, schedule.fromPeriodMinute, timezone),
      calculateDayTime(timeStart, schedule.toPeriodHour, schedule.toPeriodMinute, timezone))).flatMap(schedule ⇒
      if (schedule.startTime >= timeStart && schedule.endTime <= timeEnd) {
        List(schedule)
      } else if (schedule.startTime >= timeEnd || schedule.endTime <= timeStart) {
        List()
      } else {
        List(schedule.copy(startTime = math.max(schedule.startTime, timeStart),
          endTime = math.min(schedule.endTime, timeEnd)))
      })
    corridorSchedules.getOrElse(List())
  }

  private def updateCurrentSchedules(current: List[CorridorSpeedSchedule], endTimeLastHistoric: Long): List[CorridorSpeedSchedule] = {
    current.map(schedule ⇒ {
      if (endTimeLastHistoric > schedule.startTime) {
        schedule.copy(startTime = endTimeLastHistoric)
      } else {
        schedule
      }
    })
  }

  private def calculateDayTime(dayStart: Long, hour: Int, minute: Int, timezone: TimeZone): Long = {
    val cal = new GregorianCalendar(timezone)
    cal.setTimeInMillis(dayStart)
    cal.set(Calendar.HOUR_OF_DAY, hour)
    cal.set(Calendar.MINUTE, minute)
    cal.getTimeInMillis
  }

}
