/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.reports.export

import csc.sectioncontrol.messages.StatsDuration
import csc.sectioncontrol.reports.{ ReportLogRecord, AllCorridorsStatistics, SystemData }
import csc.sectioncontrol.messages.certificates.{ ActiveCertificate, ComponentCertificate }
import csc.sectioncontrol.storage.{ DayReportVersion, ZkUser, ZkAlertHistory, ZkState }
import csc.sectioncontrol.enforce.{ MsiBlankEvent, MtmSpeedSetting }
import csc.sectioncontrol.reports.metrics.common.{ SystemMetrics, Metrics }
import csc.sectioncontrol.storagelayer.corridor.CorridorIdMapping

class DayReportBuilderV421(systemId: String, key: StatsDuration, systemData: SystemData, runtimeResult: AllCorridorsStatistics,
                           components: Either[String, List[ComponentCertificate]], activeCertificates: List[ActiveCertificate],
                           currentSystemState: ZkState, systemAlerts: Map[CorridorIdMapping, Seq[ZkAlertHistory]],
                           mtmSettings: List[MtmSpeedSetting], systemEvents: Seq[ReportLogRecord], metrics: List[Metrics],
                           users: List[ZkUser], reportName: String, blankMsiEvent: Option[MsiBlankEvent])

  extends DayReportBuilderV42(systemId, key, systemData, runtimeResult, components, activeCertificates, currentSystemState,
    systemAlerts, mtmSettings, systemEvents, metrics, users, reportName, blankMsiEvent) {

  override def xsd = "HHM-dagrapport v4.21.xsd"

  override def version_text = "versie 4.21"

  override def getDayReportSettings(): DayReportBuilderV40Settings = {
    return new DayReportBuilderV421Settings()
  }

}

class DayReportBuilderV421Settings extends DayReportBuilderV40Settings {
  override val version = DayReportVersion.HHM_V421
  override val NR_DECIMALS: Int = 3

  override val translateState = Map[String, String](
    "Off" -> "OFF",
    "Maintenance" -> "ONDERHOUD",
    "StandBy" -> "STAND-BY",
    "Alert" -> "ALARM",
    "Failure" -> "ALARM",
    "EnforceOn" -> "HANDHAVEN",
    "EnforceOff" -> "HANDHAVEN (OFFLINE)",
    "EnforceDegraded" -> "HANDHAVEN (DEGRADED)")

}