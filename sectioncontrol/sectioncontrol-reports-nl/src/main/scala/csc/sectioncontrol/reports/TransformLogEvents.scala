/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.reports

import csc.dlog.LogEntry
import csc.sectioncontrol.messages.SystemEvent
import metrics.Period
import csc.sectioncontrol.storage.ZkSchedule
import java.util.Calendar
import csc.sectioncontrol.reports.schedules.CorridorSpeedSchedule

case class ReportLogRecord(corridorLogId: String, seqnr: Long, timestamp: Long, event: SystemEvent)

/**
 * Transfer Log events into ReportLogRecord, which contains the corridorLog Id.
 * The Id is determined using the corridorId or Gantry Id within the SystemEvent.
 * The corridorId is use to map to the corridor when this is supplied within the SystemEvent.
 * If the corridor ID is empty and the gantryId is supplied. the gantryId is used to map to the corridor.
 * When both are empty the default is used.
 * The gantry mapping is using the functional corridors when there are multiple corridors for the same gantry.
 * When there are no functional corridors it uses the schedules to determine the corridor.
 *
 */
object TransformLogEvents {
  /**
   * Transfer LogEntry into ReportLogRecord
   * @param systemData system data and corridor data
   * @param schedules the schedules of the corridor
   * @param systemEvents The transfer LogEntries
   * @return list of ReportLogRecord
   */
  def toReportRecords(systemData: SystemData, schedules: Map[Int, Seq[CorridorSpeedSchedule]], systemEvents: Seq[LogEntry[SystemEvent]]): Seq[ReportLogRecord] = {
    val corridorMapping = getCorridorMapping(systemData)
    val timeWithinProcessingDay = systemEvents.headOption.map(_.event.timestamp).getOrElse(System.currentTimeMillis())
    val gantryMapping = getGantryMapping(systemData, schedules, timeWithinProcessingDay)
    val defaultLogId = systemData.zkSystem.violationPrefixId + "****"

    systemEvents.map(logEvent ⇒ {
      val event = logEvent.event
      val corridorLogId = (event.corridorId, event.gantryId) match {
        case (Some(id), _) ⇒ {
          //use corridorId
          corridorMapping.get(id).getOrElse(defaultLogId)
        }
        case (None, Some(id)) ⇒ {
          //use gantryId
          getCorridorLogId(gantryMapping, id, event.timestamp).getOrElse(defaultLogId)
        }
        case (None, None) ⇒ {
          //create a system log record
          defaultLogId
        }
      }

      ReportLogRecord(corridorLogId = corridorLogId, seqnr = logEvent.seqnr, timestamp = logEvent.timestamp, event = event)
    }).toSeq
  }

  /**
   * Create a mapping from corridor Id to the needed corridorLogId
   * @param systemData the system Data including the all the corridors
   * @return map of corridorId -> corridorLogId
   */
  def getCorridorMapping(systemData: SystemData): Map[String, String] = {
    systemData.corridors.map(cor ⇒ {
      val ht = "%s%04d".format(systemData.zkSystem.violationPrefixId, cor.corridor.info.corridorId)
      val corridorId = cor.corridor.id
      (corridorId, ht)
    }).toMap
  }

  /**
   *
   * Create a mapping from gantry Id to the needed corridorLogId.
   * When there are functional corridors use them instead of the actual corridors
   * If there are still multiple possible mappings use the schedules.
   *
   * @param systemData the system Data including the all the corridors and its gantries
   * @param schedules the list of schedules of all corridors
   * @param processDate the current date to determine the process day
   * @return map of gantryId -> corridorLogId
   */
  def getGantryMapping(systemData: SystemData, schedules: Map[Int, Seq[CorridorSpeedSchedule]], processDate: Long): Map[String, Seq[GantryMapping]] = {

    //start create mapping
    val mappingList = systemData.corridors.flatMap(cor ⇒ {
      val ht = "%s%04d".format(systemData.zkSystem.violationPrefixId, cor.corridor.info.corridorId)
      val firstGantryId = cor.startSection.inGantry.id
      val periods = schedules.get(cor.corridor.info.corridorId).getOrElse(Seq()).map {
        schedule ⇒ Period(schedule.startTime, schedule.endTime)
      }

      val mapping1 = new GantryMapping(
        gantryId = firstGantryId,
        corridorLogId = ht,
        periods = periods)
      val lastGantryId = cor.endSection.outGantry.id
      val mapping2 = new GantryMapping(
        gantryId = lastGantryId,
        corridorLogId = ht,
        periods = periods)
      Seq(mapping1, mapping2)
    })
    mappingList.groupBy(_.gantryId)
  }

  /**
   * Find the best corridor log Id.
   * When there is only one use that corridor always
   * When there are more corridors use the one which has the closest schedule from the event time.
   * Within the period is the closest possible (weight = 0).
   * @param gantryMapping the mapping
   * @param gantryId the used gantry id
   * @param time the time of the event
   * @return Option of a CorridorLogId
   */
  def getCorridorLogId(gantryMapping: Map[String, Seq[GantryMapping]], gantryId: String, time: Long): Option[String] = {
    val corridorList = gantryMapping.get(gantryId)

    corridorList.map(list ⇒ {
      if (list.isEmpty) {
        None
      }
      if (list.size == 1) {
        list.head.corridorLogId
      }
      //find best corridor using time
      //The corridor which has a period closest to the given time
      //will be returned weight is the smallest
      val head = list.head
      val dist = calculateWeight(time, head.periods)
      val (_, result) = list.tail.foldLeft(dist, head) {
        case ((minWeigh, result), mapping) ⇒ {
          val currentWeigh = calculateWeight(time, mapping.periods)
          //Is the current mapping closer to the given time
          if (currentWeigh < minWeigh) {
            //calculateWeigh is lower
            (currentWeigh, mapping)
          } else {
            //the result was better than this mapping
            (minWeigh, result)
          }
        }
      }
      result.corridorLogId
    })
  }

  /**
   * Convert schedules to periods
   * @param start time indication which day is processed
   * @param schedules list of schedules
   * @return list of periods
   */
  private def createPeriods(start: Long, schedules: Seq[ZkSchedule]): Seq[Period] = {
    def getTime(start: Long, hour: Int, minute: Int): Long = {
      val cal = Calendar.getInstance()
      cal.clear()
      cal.setTimeInMillis(start)
      cal.set(Calendar.HOUR_OF_DAY, hour)
      cal.set(Calendar.MINUTE, minute)
      cal.getTimeInMillis
    }
    schedules.map {
      s ⇒
        {
          val from = getTime(start, s.fromPeriodHour, s.fromPeriodMinute)
          val to = getTime(start, s.toPeriodHour, s.toPeriodMinute)
          Period(from, to)
        }
    }
  }

  /**
   * return the smallest offset between the time and the period
   * @param time The event time
   * @param periods the periods
   * @return 0 is within a period else weight is the time between the given time and the border of the period
   */
  private def calculateWeight(time: Long, periods: Seq[Period]): Long = {
    periods.foldLeft(Long.MaxValue) {
      case (minDist, period) ⇒ {
        val distance = if (time < period.from) {
          period.from - time
        } else if (time > period.to) {
          time - period.to
        } else {
          0
        }
        math.min(distance, minDist)
      }
    }
  }
  case class GantryMapping(gantryId: String, corridorLogId: String, periods: Seq[Period])
}