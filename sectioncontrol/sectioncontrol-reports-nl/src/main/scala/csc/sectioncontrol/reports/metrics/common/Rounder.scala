/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

package csc.sectioncontrol.reports.metrics.common

trait Rounder {
  val NR_DECIMALS: Int = 2
  val ROUNDING_MODE: BigDecimal.RoundingMode.RoundingMode = BigDecimal.RoundingMode.HALF_UP

  def divideAndRound(dividend: BigDecimal, divisor: BigDecimal, nrDecimals: Int = NR_DECIMALS, roundingMode: BigDecimal.RoundingMode.RoundingMode = BigDecimal.RoundingMode.HALF_UP): BigDecimal = {
    //making sure the dividing by zero is not possible
    val dvisor = divisor match {
      case x if (x <= 0) ⇒ BigDecimal(1)
      case y             ⇒ y
    }

    (dividend / dvisor).setScale(nrDecimals, roundingMode)
  }

  /**
   * round a given value of with the number of decimals and rounding mode
   * @param value the value to round off
   * @param nrDecimals the number of decimals that the result must have. default 3 decimals
   * @param roundingMode how the last resulting decimals must be rounded. default UP
   * @return resulting BigDecimal
   */
  def round(value: Float, nrDecimals: Int = NR_DECIMALS, roundingMode: BigDecimal.RoundingMode.RoundingMode = ROUNDING_MODE): BigDecimal = {
    internalDivideAndRound(BigDecimal(value), BigDecimal(1), nrDecimals, roundingMode)
  }

  def roundBigDecimal(value: BigDecimal, nrDecimals: Int = NR_DECIMALS, roundingMode: BigDecimal.RoundingMode.RoundingMode = ROUNDING_MODE): BigDecimal = {
    internalDivideAndRound(value, BigDecimal(1), nrDecimals, roundingMode)
  }

  /**
   * internal implementation to calculate the ratio and round it of with the given number of decimals and rounding mode
   * @param dividend the fist value
   * @param divisor the second value
   * @param nrDecimals the number of decimals that the result must have. default 3 decimals
   * @param roundingMode how the last resulting decimals must be rounded. default UP
   * @return resulting BigDecimal
   */
  private def internalDivideAndRound(dividend: BigDecimal, divisor: BigDecimal, nrDecimals: Int = NR_DECIMALS, roundingMode: BigDecimal.RoundingMode.RoundingMode = ROUNDING_MODE): BigDecimal = {
    //making sure the dividing by zero is not possible
    val dvisor = divisor match {
      case x if (x <= 0) ⇒ BigDecimal(1)
      case y             ⇒ y
    }

    (dividend / dvisor).setScale(nrDecimals, roundingMode)
  }
}
