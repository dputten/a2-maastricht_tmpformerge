/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.reports.export

import csc.sectioncontrol.messages.SystemEvent
import csc.sectioncontrol.messages.StatsDuration
import xml.Elem
import csc.sectioncontrol.reports.{ ReportLogRecord, AllCorridorsStatistics, SystemData }
import csc.sectioncontrol.messages.certificates.{ ComponentCertificate, ActiveCertificate, TypeCertificate }
import csc.sectioncontrol.enforce.{ MsiBlankEvent, MtmSpeedSetting }
import csc.dlog.LogEntry
import csc.sectioncontrol.reports.metrics.common.{ SystemMetrics, Metrics }
import csc.sectioncontrol.storage.{ ZkUser, DayReportVersion, ZkAlertHistory, ZkState }
import csc.sectioncontrol.reports.i18n.Messages
import csc.sectioncontrol.storagelayer.corridor.CorridorIdMapping

class DayReportBuilderV50(systemId: String,
                          key: StatsDuration,
                          systemData: SystemData,
                          runtimeResult: AllCorridorsStatistics,
                          components: Either[String, List[ComponentCertificate]],
                          activeCertificates: List[ActiveCertificate],
                          currentSystemState: ZkState,
                          systemAlerts: Map[CorridorIdMapping, Seq[ZkAlertHistory]],
                          mtmSettings: List[MtmSpeedSetting],
                          systemEvents: Seq[ReportLogRecord],
                          metrics: List[Metrics],
                          systemMetrics: SystemMetrics,
                          users: List[ZkUser],
                          reportName: String,
                          blankMsiEvent: Option[MsiBlankEvent]) extends DayReportBuilderV40(systemId,
  key,
  systemData,
  runtimeResult,
  components,
  activeCertificates,
  currentSystemState,
  systemAlerts,
  mtmSettings,
  systemEvents,
  metrics,
  users,
  reportName,
  blankMsiEvent) {

  override def getDayReportSettings(): DayReportBuilderV40Settings = {
    return new DayReportBuilderV50Settings()
  }

  val hhmId = systemData.zkSystem.violationPrefixId

  private def formatDateString(dateString: String): String = {
    val expectedDateFormat = "(\\d{4})(\\d{2})(\\d{2})".r
    dateString match {
      case expectedDateFormat(year, month, day) ⇒ "%s-%s-%s".format(year, month, day)
      case _                                    ⇒ dateString
    }
  }

  override def filenameZip = "%s_%s.gz".format(hhmId, formatDateString(key.key))

  override def filenamePlain = "%s_%s.xml".format(hhmId, formatDateString(key.key))

  override def xsd = "HHM-dagrapport-v5.0.xsd"

  override def xml: Elem = {
    <HHM-dagrapport id={ hhmId } datum={ key.datum } xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation={ xsd }>
      <algemene-info>
        { generalInfo }
      </algemene-info>
      { systemInfo }
      {
        for (metric ← metrics) yield createMetricsXml(metric)
      }
      { HHMStats }
      <log>
        { reportsLog }
        { alarmesLog }
        { if (!mtmSettings.isEmpty) mtmLog }
      </log>
      <opmerkingen>
        Dagelijkse rapportage HHM volgens Specificatie XML-dagrapportage, versie 5.0.
        { getBlankMsiMessage(blankMsiEvent) }
      </opmerkingen>
    </HHM-dagrapport>
  }

  def HHMStats: Elem = {
    val settings = getDayReportSettings()
    <HHM-statistiek>
      <performance>
        <overtredingenratio>{ settings.round(systemMetrics.violationRatio).toString }</overtredingenratio>
        <autoratio>{ settings.round(systemMetrics.autoRatio).toString }</autoratio>
        <handhaafratio>{ getDayReportSettings.round(systemMetrics.enforceRatio).toString }</handhaafratio>
      </performance>
      <tijdvolledigbeschikbaar>{ systemMetrics.timeAvailable }</tijdvolledigbeschikbaar>
    </HHM-statistiek>
  }

  override def getReportId(logEvent: ReportLogRecord): String = {
    "%s%s".format(
      getDayReportSettings().translateToTypeCode(logEvent.event),
      getDayReportSettings().translateComponentToCode(logEvent.event))
  }

  override def getAlternativeReasonText(event: SystemEvent, systemState: String): Option[String] = {
    def doGetAlternativeReasonText(): Option[String] = event.componentId match {
      case "selftest" ⇒ Some(Messages("csc.sectioncontrol.reports.eventType.%s".format(event.eventType)))
      case _          ⇒ None
    }

    super.getAlternativeReasonText(event, systemState) match {
      case Some(text) ⇒ Some(text)
      case None       ⇒ doGetAlternativeReasonText()
    }
  }

  override def systemState: Elem = {
    val stateNL = getDayReportSettings.translateState.getOrElse(currentSystemState.state, "Unknown (%s)".format(currentSystemState.state))
    val alertStateNL = if (getCurrentAlerts.isEmpty) "GEEN STORING" else "STORING"
    <status toestand={ stateNL } storingsstatus={ alertStateNL } toelichting={ currentSystemState.reason }></status>
  }

}

class DayReportBuilderV50Settings extends DayReportBuilderV40Settings {
  override val version = DayReportVersion.HHM_V50
  override val NR_DECIMALS: Int = 2

  override val translateState = Map[String, String](
    "Off" -> "OFF",
    "Maintenance" -> "OFF", //State Maintenance (ONDERHOUD) not supported in this version
    "StandBy" -> "STAND-BY",
    "Alert" -> "ALARM",
    "Failure" -> "ALARM",
    "EnforceOn" -> "HANDHAVEN",
    "EnforceOff" -> "HANDHAVEN",
    "EnforceDegraded" -> "HANDHAVEN")

}

