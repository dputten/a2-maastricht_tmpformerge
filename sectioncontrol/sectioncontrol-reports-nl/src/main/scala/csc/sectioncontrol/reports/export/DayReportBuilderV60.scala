/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.reports.export

import csc.sectioncontrol.messages.StatsDuration
import xml.Elem
import csc.sectioncontrol.reports.{ ReportLogRecord, AllCorridorsStatistics, SystemData }
import csc.sectioncontrol.messages.certificates.{ ComponentCertificate, ActiveCertificate, TypeCertificate }
import csc.sectioncontrol.enforce.{ MsiBlankEvent, MtmSpeedSetting }
import csc.sectioncontrol.reports.metrics.common.{ SystemMetrics, Metrics }
import csc.sectioncontrol.storage.{ ZkUser, DayReportVersion, ZkAlertHistory, ZkState }
import csc.sectioncontrol.reports.i18n.Messages
import csc.sectioncontrol.reports.metrics.tc.TCMetrics
import csc.sectioncontrol.reports.metrics.speed.SpeedMetrics
import csc.sectioncontrol.reports.metrics.red.RedMetrics
import csc.sectioncontrol.reports.metrics.Period
import csc.sectioncontrol.storagelayer.corridor.CorridorIdMapping

/**
 * Version defined by CJIB. This doesn't contain a section-control tag.
 * Looks like they stripped every thing they didn't want.
 *
 * @param systemId
 * @param key
 * @param systemData
 * @param runtimeResult
 * @param components
 * @param activeCertificates
 * @param currentSystemState
 * @param systemAlerts
 * @param mtmSettings
 * @param systemEvents
 * @param metrics
 * @param systemMetrics
 * @param users
 */
class DayReportBuilderV60(systemId: String,
                          key: StatsDuration,
                          systemData: SystemData,
                          runtimeResult: AllCorridorsStatistics,
                          components: Either[String, List[ComponentCertificate]],
                          activeCertificates: List[ActiveCertificate],
                          currentSystemState: ZkState,
                          systemAlerts: Map[CorridorIdMapping, Seq[ZkAlertHistory]],
                          mtmSettings: List[MtmSpeedSetting],
                          systemEvents: Seq[ReportLogRecord],
                          metrics: List[Metrics],
                          systemMetrics: SystemMetrics,
                          users: List[ZkUser],
                          reportName: String,
                          blankMsiEvent: Option[MsiBlankEvent]) extends DayReportBuilderV50(systemId,
  key,
  systemData,
  runtimeResult,
  components,
  activeCertificates,
  currentSystemState,
  systemAlerts,
  mtmSettings,
  systemEvents,
  metrics,
  systemMetrics,
  users,
  reportName,
  blankMsiEvent) {

  override def getDayReportSettings(): DayReportBuilderV40Settings = new DayReportBuilderV60Settings()

  override def filenameZip = "%s_%s.gz".format(hhmId, key.key)

  override def filenamePlain = "%s_%s.xml".format(hhmId, key.key)

  override def xsd = "HHM-dagrapport-v6.0.xsd"

  override def xml: Elem = {
    <HHM-dagrapport id={ hhmId } datum={ key.datum } xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation={ xsd }>
      <algemene-info>
        { generalInfo }
      </algemene-info>
      { systemInfo }
      {
        for (metric ← metrics) yield {
          metric match {
            case m: TCMetrics⇒ //isn't supported in this version
            case m: SpeedMetrics⇒ ExportSpeedMetrics.createSpeedMetricsXmlV60(key, m, getDayReportSettings())
            case m: RedMetrics⇒ ExportRedMetrics.createRedMetricsXmlV60(key, m, getDayReportSettings())
          }
        }
      }
      { HHMStats }
      <log>
        { reportsLog }
      </log>
      <opmerkingen>
        Dagelijkse rapportage HHM volgens Specificatie XML-dagrapportage, versie 6.0.
        { getBlankMsiMessage(blankMsiEvent) }
      </opmerkingen>
    </HHM-dagrapport>
  }

  override def systemState: Elem = {
    val stateNL = getDayReportSettings().translateState.getOrElse(currentSystemState.state, "Unknown (%s)".format(currentSystemState.state))
    <status toestand={ stateNL } toelichting={ currentSystemState.reason }></status>
  }

}

class DayReportBuilderV60Settings extends DayReportBuilderV50Settings {
  override val version = DayReportVersion.HHM_V60

  override val translateStateLogEntry = Map[String, String](
    "Off" -> "UIT",
    "Maintenance" -> "UIT",
    "StandBy" -> "UIT",
    "Alert" -> "UIT",
    "Failure" -> "UIT",
    "EnforceOn" -> "AAN",
    "EnforceOff" -> "UIT",
    "EnforceDegraded" -> "AAN")

  override def createHandhaafLogEntry(state: (String, String, String)): Elem = {
    <handhaaflog-entry tijd={ state._1 } toestand={ state._2 }>
      { state._3 }
    </handhaaflog-entry>
  }

  override def createReason(state: ZkState): String = {
    state.state match {
      case "Off" ⇒ "Handhaaf status is op off gezet"
      case "Maintenance" ⇒ "Handhaaf status is op onderhoud gezet"
      case "StandBy" if state.reason.startsWith("Functie") ⇒ "Functie uitgezet door operator"
      case "StandBy" ⇒ state.reason
      case "EnforceOn" if state.reason.startsWith("Functie") ⇒ "Functie aangezet door operator"
      case "EnforceOn" ⇒ state.reason
      case "EnforceOff" ⇒ "Functie uitgezet door handhaaf periode"
      case "EnforceDegraded" ⇒ state.reason
      case _ ⇒ state.reason
    }
  }

  override def enforceLogFiltered(key: StatsDuration, periods: Seq[Period], systemStates: Seq[ZkState]): Elem = {
    def beginOfPeriodeReason = Messages("csc.sectioncontrol.reports.no_start_of_schedule_event")
    def endOfPeriodeReason = Messages("csc.sectioncontrol.reports.no_end_of_schedule_event")

    def createLogEntriesV60(period: Period, systemStates: Seq[ZkState]): Seq[ZkState] = {
      val statesMinusMills: Seq[ZkState] = systemStates.map(s ⇒ s.copy(timestamp = 1000 * (s.timestamp / 1000)))
      val allBetween = statesMinusMills
        .filter {
          state ⇒
            //remove milliseconds.
            // This is needed because the state change timestamp is a couple of milliseconds
            // past the schedule 'to' time
            val timestamp = 1000 * (state.timestamp / 1000)
            timestamp >= period.from && timestamp <= period.to
        }
        .sortWith {
          (first, second) ⇒ first.timestamp < second.timestamp
        }

      val before = statesMinusMills
        .filter(state ⇒ state.timestamp < period.from)
        .sortWith {
          (first, second) ⇒ first.timestamp > second.timestamp
        }
        .headOption

      allBetween match {
        case Nil ⇒
          before match {
            case Some(x) ⇒ x.copy(timestamp = period.from, reason = beginOfPeriodeReason) :: x.copy(timestamp = period.to, reason = endOfPeriodeReason) :: Nil
            case _       ⇒ Nil
          }
        case head :: rest ⇒
          val last = rest.reverse.headOption

          val restWithLast = last match {
            case Some(x) if x.timestamp == period.to ⇒ rest
            case Some(x) if x.timestamp != period.to ⇒ x.copy(timestamp = period.to, reason = endOfPeriodeReason) :: rest
            case _ ⇒ if (head.timestamp < period.to) {
              head.copy(timestamp = period.to, reason = endOfPeriodeReason) :: Nil
            } else {
              Nil
            }
          }

          head match {
            case x if x.timestamp == period.from ⇒ x :: restWithLast
            case _ ⇒
              before match {
                case Some(x) ⇒
                  x.copy(timestamp = period.from, reason = beginOfPeriodeReason) :: head :: restWithLast
                case _ ⇒
                  head.copy(timestamp = period.from, reason = beginOfPeriodeReason) :: head :: restWithLast
              }
          }
      }
    }

    val statesWithMidnights: Seq[(String, String, String)] = periods
      .flatten(period ⇒ createLogEntriesV60(period, systemStates))
      .sortWith {
        (first, second) ⇒ first.timestamp < second.timestamp
      }
      .map {
        s ⇒
          var reason = createReason(s)
          val date =
            if (s.timestamp == key.tomorrow.start)
              key.datum + "T23:59:59"
            else
              formatDateToCEST(s.timestamp)

          if (s.timestamp == key.start)
            reason = beginOfPeriodeReason

          val systemState = translateStateLogEntry.getOrElse(s.state, s.state)

          (date, systemState, reason)
      }

    //filter double events (possible due to translation)
    val filteredStates = if (statesWithMidnights.isEmpty) {
      statesWithMidnights
    } else {
      val filter: Seq[(String, String, String)] = statesWithMidnights.tail.foldLeft(Seq(statesWithMidnights.head)) {
        case (list, (time, state, reason)) ⇒ if (list.last._2 == state) {
          list
        } else {
          list :+ (time, state, reason)
        }
      }
      //always return the last entry (the midnight record)
      if (filter.contains(statesWithMidnights.last)) {
        filter
      } else {
        filter :+ statesWithMidnights.last
      }
    }
    <handhaaflog>
      { for (state ← filteredStates) yield createHandhaafLogEntry(state) }
    </handhaaflog>
  }

}
