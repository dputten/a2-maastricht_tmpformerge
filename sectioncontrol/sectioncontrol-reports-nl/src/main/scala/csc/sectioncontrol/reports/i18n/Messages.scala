/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

package csc.sectioncontrol.reports.i18n

import java.text.MessageFormat

object Messages {
  val messages = Map(
    "csc.sectioncontrol.reports.no_start_of_schedule_event" -> "Begin meetperiode",
    "csc.sectioncontrol.reports.no_end_of_schedule_event" -> "Einde meetperiode",
    // TODO: Misnomer for key???
    "csc.sectioncontrol.reports.startDay" -> "Begin handhaaf-periode",
    "csc.sectioncontrol.reports.endDay" -> "Einde handhaaf-periode",

    "csc.sectioncontrol.reports.start_schedule" -> "Begin handhaaf-periode",
    "csc.sectioncontrol.reports.end_schedule" -> "Einde handhaaf-periode",

    "csc.sectioncontrol.reports.write_day_report" -> "Dagrapportage weggeschreven {0}",
    "csc.sectioncontrol.reports.write_day_report_failed" -> "Dagrapportage weggeschreven gefaald\n{0}",
    "csc.sectioncontrol.reports.create_day_report" -> "Aanmaken dagrapportage",
    "csc.sectioncontrol.reports.create_day_report_failed" -> "Aanmaken dagrapportage gefaald\n{0}",
    "csc.sectioncontrol.reports.send_day_report" -> "Dagraportage verzonden naar {0}",
    "csc.sectioncontrol.reports.send_day_report_failed" -> "Gefaald dagraportage versturen naar {0}",
    "csc.sectioncontrol.reports.eventType.SELFTEST-START" -> "Calibratie gestart",
    "csc.sectioncontrol.reports.eventType.SELFTEST-END" -> "Calibratie beëindigd",
    "csc.sectioncontrol.reports.eventType.SELFTEST-SUCCESS" -> "Calibratie geslaagd",
    "csc.sectioncontrol.reports.eventType.Failure" -> "Calibratie gefaald",
    "csc.sectioncontrol.reports.eventType.SELFTEST-ERROR" -> "Calibratie gefaald")

  /**
   * Translates a message.
   *
   * Uses `java.text.MessageFormat` internally to format the message.
   *
   * @param key the message key
   * @param args the message arguments
   * @return the formatted message or a default rendering if the key wasn’t defined
   */
  def apply(key: String, args: Any*): String = {
    translate(key, args).getOrElse(noMatch(key, args))
  }

  /**
   * Translates a message.
   *
   * Uses `java.text.MessageFormat` internally to format the message.
   *
   * @param key the message key
   * @param args the message arguments
   * @return the formatted message, if this key was defined
   */
  private def translate(key: String, args: Seq[Any]): Option[String] = {
    if (messages.contains(key)) {
      Some(new MessageFormat(messages(key)).format(args.map(_.asInstanceOf[java.lang.Object]).toArray))
    } else {
      None
    }
  }

  private def noMatch(key: String, args: Seq[Any]) = key
}
