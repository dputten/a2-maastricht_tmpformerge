/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.reports.export

import csc.sectioncontrol.messages.StatsDuration

trait DayReportBuilder {
  def output: Array[Byte]
  def filenameZip: String
  def filenamePlain: String
  def xsd: String
  val key: StatsDuration
}