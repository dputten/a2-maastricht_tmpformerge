package csc.sectioncontrol.reports.metrics.red

import csc.sectioncontrol.reports.CorridorData
import csc.sectioncontrol.messages.{ RedLightCorridorStatistics, CorridorStatistics }
import csc.sectioncontrol.storage.{ ZkState, ZkAlertHistory, LaneInfo, ZkSystem }
import csc.sectioncontrol.reports.metrics.common._
import csc.sectioncontrol.reports.metrics.Period
import csc.sectioncontrol.storagelayer.RedLightConfig
import csc.sectioncontrol.reports.CorridorData
import csc.sectioncontrol.messages.RedLightCorridorStatistics
import csc.sectioncontrol.storagelayer.RedLightConfig
import scala.Some
import csc.sectioncontrol.storage.ZkSystem
import csc.sectioncontrol.storage.ZkAlertHistory

/**
 * Metrics for the Redlight services
 * @param system
 * @param corridorData
 * @param corridorStatistics
 * @param periods
 * @param systemStates
 * @param systemAlerts
 * @param speedLimits
 */
class RedMetrics(system: ZkSystem, val corridorData: CorridorData, corridorStatistics: Seq[CorridorStatistics],
                 val periods: Seq[Period], val systemStates: Seq[ZkState], systemAlerts: Seq[ZkAlertHistory],
                 speedLimits: Seq[Int], val metricsTypeOrder: Int = 300) extends Rounder with Metrics {

  def this(system: ZkSystem, corridorData: CorridorData, corridorStatistics: Option[CorridorStatistics], periods: Seq[Period], systemStates: Seq[ZkState], systemAlerts: Seq[ZkAlertHistory], speedLimits: Seq[Int]) =
    this(system, corridorData, corridorStatistics.map(List(_)).getOrElse(Nil), periods, systemStates, systemAlerts, speedLimits)

  def hasStatistics: Boolean = corridorStatistics.length > 0

  private def roadNumber = system.name
  private def orderNumber = system.orderNumber
  private val direction = system.location.roadPart
  private val viewingDirection: String = system.location.viewingDirection.getOrElse("")
  private val idV6RoadNumber = if (system.location.roadNumber == null || system.location.roadNumber.trim() == "") corridorData.corridor.info.locationLine1.trim
  else system.location.roadNumber.trim

  val id = "RLC %s %s %s-%s".format(system.location.region, roadNumber, direction, orderNumber)
  val idV6 = "RLC %s %s %s - %s".format(system.location.region, idV6RoadNumber, viewingDirection, orderNumber)
  val speedLimit: Int = speedLimits.reduceOption(_ max _).getOrElse(0)

  val firstGantry = corridorData.startSection.inGantry
  val beginPortal = firstGantry.name + " " + firstGantry.hectometer + " " + direction

  val stats: Seq[LaneInfo] = corridorStatistics.flatMap(_.laneStats.filter(laneInfo ⇒ laneInfo.lane.gantry == firstGantry.id))

  val lanes: Seq[(String, Int)] = firstGantry.lanes.map(laneData ⇒ {
    val location: String = beginPortal + " " + laneData.name
    val number: Int = stats.filter(info ⇒ info.lane.name == laneData.name).map(_.count).sum
    (location, number)
  })

  val lanesV6: Seq[(String, Int)] = {
    val resultWithSortOrder = firstGantry.lanes.map { laneData ⇒
      val sortOrder = laneData.seqNumber.map(_.toInt).getOrElse(0)
      val location: String = laneData.seqNumber.getOrElse("") + " " + laneData.seqName.getOrElse("")
      val number: Int = stats.filter(info ⇒ info.lane.name == laneData.name).map(_.count).sum
      (sortOrder, location.trim, number)
    }

    resultWithSortOrder.sortBy(_._1).map(x ⇒ (x._2, x._3))
  }

  val total = lanes.foldLeft[Int](0) { (akku, tuple) ⇒ akku + tuple._2 }

  val redlightStatistics = corridorStatistics.foldLeft(List[RedLightCorridorStatistics]()) {
    (list, corridorStat) ⇒
      corridorStat match {
        case redStat: RedLightCorridorStatistics ⇒ redStat :: list
        case _                                   ⇒ list
      }
  }
  def averageRed: BigDecimal = {
    val totalAverage = redlightStatistics.map(redStat ⇒ redStat.nrWithRedLight * redStat.averageRedLight).sum
    val totalRed = redlightStatistics.map(redStat ⇒ redStat.nrWithRedLight).sum

    val averageRed = if (totalRed == 0) 0 else totalAverage / totalRed

    round(averageRed.toFloat / 1000)
  }

  def maxRed: BigDecimal = round(redlightStatistics.map(_.highestRedLight)
    .foldLeft[Int](0) { (akku, value) ⇒ akku.max(value) }.toFloat / 1000)

  val violations = ViolationMetrics(corridorStatistics)
  val performance = PerformanceMetrics(corridorStatistics, total, total, systemStates, periods, systemAlerts)

  val service = corridorData.corridor.serviceConfig match {
    case cfg: RedLightConfig ⇒ Some(cfg)
    case _                   ⇒ None
  }
  val redPardon = round(service.map(_.pardonRedTime).getOrElse(0L).toFloat / 1000F)
  val yellowTime = round(service.map(_.minimumYellowTime).getOrElse(0L).toFloat / 1000F)
}