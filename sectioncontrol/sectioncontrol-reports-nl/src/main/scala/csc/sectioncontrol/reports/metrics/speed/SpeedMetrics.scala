package csc.sectioncontrol.reports.metrics.speed

import csc.sectioncontrol.messages.CorridorStatistics
import csc.sectioncontrol.storage._
import csc.sectioncontrol.reports.metrics.common._
import csc.sectioncontrol.reports.metrics.Period
import csc.sectioncontrol.messages.SpeedFixedCorridorStatistics
import csc.sectioncontrol.reports.CorridorData
import csc.sectioncontrol.storage.ZkSystem
import csc.sectioncontrol.storage.LaneInfo
import csc.sectioncontrol.messages.SpeedFixedCorridorStatistics
import csc.sectioncontrol.reports.CorridorData
import csc.sectioncontrol.storage.ZkSystem
import csc.sectioncontrol.storage.ZkAlertHistory

/**
 * metrics used for speedFixed services
 * @param system
 * @param corridorData
 * @param corridorStatistics
 * @param periods
 * @param systemStates
 * @param systemAlerts
 * @param speedLimits
 */
class SpeedMetrics(system: ZkSystem, val corridorData: CorridorData, corridorStatistics: Seq[CorridorStatistics],
                   val periods: Seq[Period], val systemStates: Seq[ZkState], val systemAlerts: Seq[ZkAlertHistory],
                   val speedLimits: Seq[Int], val metricsTypeOrder: Int = 200) extends Rounder with Metrics {

  def this(system: ZkSystem, corridorData: CorridorData, corridorStatistics: Option[CorridorStatistics], periods: Seq[Period], systemStates: Seq[ZkState], systemAlerts: Seq[ZkAlertHistory], speedLimits: Seq[Int]) =
    this(system, corridorData, corridorStatistics.map(List(_)).getOrElse(Nil), periods, systemStates, systemAlerts, speedLimits)

  def hasStatistics: Boolean = corridorStatistics.length > 0
  val hasESA = false

  //private val nrOfStats = corridorStatistics.length
  private def roadNumber = system.name
  private def orderNumber = system.orderNumber
  private val direction = system.location.roadPart
  private val viewingDirection: String = system.location.viewingDirection.getOrElse("")
  private val idV6RoadNumber = if (system.location.roadNumber == null || system.location.roadNumber.trim() == "") corridorData.corridor.info.locationLine1.trim
  else system.location.roadNumber.trim

  val id = "FIP %s %s %s-%s".format(system.location.region, roadNumber, direction, orderNumber)
  val idV6 = "FIP %s %s %s - %s".format(system.location.region, idV6RoadNumber, viewingDirection, orderNumber)
  val speedLimit: Int = speedLimits.reduceOption(_ max _).getOrElse(0)
  val esaScanTime_secs: Long = system.pardonTimes.pardonTimeTechnical_secs
  val esaPardonTime_secs: Long = system.pardonTimes.pardonTimeEsa_secs
  val firstGantry = corridorData.startSection.inGantry
  val beginPortal = firstGantry.name + " " + firstGantry.hectometer + " " + direction

  val stats: Seq[LaneInfo] = corridorStatistics.flatMap(_.laneStats.filter(laneInfo ⇒ laneInfo.lane.gantry == firstGantry.id))

  val lanes: Seq[(String, Int)] = firstGantry.lanes.map(laneData ⇒ {
    val location: String = beginPortal + " " + laneData.name
    val number: Int = stats.filter(info ⇒ info.lane.name == laneData.name).map(_.count).sum
    (location, number)
  })

  val lanesV6: Seq[(String, Int)] = {
    val resultWithSortOrder = firstGantry.lanes.map { laneData ⇒
      val sortOrder = laneData.seqNumber.map(_.toInt).getOrElse(0)
      val location: String = laneData.seqNumber.getOrElse("") + " " + laneData.seqName.getOrElse("")
      val number: Int = stats.filter(info ⇒ info.lane.name == laneData.name).map(_.count).sum
      (sortOrder, location.trim, number)
    }

    resultWithSortOrder.sortBy(_._1).map(x ⇒ (x._2, x._3))
  }

  val total = lanes.foldLeft[Int](0) { (akku, tuple) ⇒ akku + tuple._2 }

  val speedStatistics = corridorStatistics.foldLeft(List[SpeedFixedCorridorStatistics]()) {
    (list, corridorStat) ⇒
      corridorStat match {
        case speedStat: SpeedFixedCorridorStatistics ⇒ speedStat :: list
        case _                                       ⇒ list
      }
  }

  private val totalMeasurement = speedStatistics.map(d ⇒ d.nrSpeedMeasurement).sum

  def averageSpeed: BigDecimal = {
    val totalMatchAverage = speedStatistics.map(stat ⇒ stat.nrSpeedMeasurement * stat.averageSpeed).sum

    val averageSpeed = if (totalMeasurement == 0) 0 else totalMatchAverage / totalMeasurement

    round(averageSpeed.toFloat)
  }

  def maxSpeed: Int = speedStatistics
    .map(_.highestSpeed)
    .foldLeft[Int](0) { (akku, value) ⇒ akku.max(value) }

  def measurementRatio: BigDecimal = {
    val total = speedStatistics.map(_.nrRegistrations).sum
    val ratio = if (total == 0) 0F else totalMeasurement.toFloat / total.toFloat
    round(ratio)
  }
  val violations = ViolationMetrics(corridorStatistics)
  val performance = PerformanceMetrics(corridorStatistics, total, total, systemStates, periods, systemAlerts)
}