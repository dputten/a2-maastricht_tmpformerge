package csc.sectioncontrol.reports.metrics.tc

import math.BigDecimal._
import csc.sectioncontrol.messages.CorridorStatistics
import csc.sectioncontrol.reports.metrics.common._
import csc.sectioncontrol.reports.metrics.Period
import csc.sectioncontrol.reports.{ CorridorData, LaneData }
import csc.sectioncontrol.messages.SectionControlCorridorStatistics
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storage.{ LaneInfo, ZkSystem }
import csc.sectioncontrol.reports.metrics.tc.TCMetrics.VehiclesPerLane
import csc.sectioncontrol.storagelayer.state.StateService
import org.slf4j.LoggerFactory

// TODO: remove the Boolean reportPerformance ivar and use Option[PerformanceMetrics] instead.
// TODO: remove schedulePeriods and systemStates. They're are only used in ExportTCMetrics to create EnforceLog
case class TCMetrics(reportId: String, reportHtId: String, reportPerformance: Boolean,
                     violations: ViolationMetrics, performance: PerformanceMetrics, modus: String,
                     hasStatistics: Boolean, esaScanTime_secs: Long, esaPardonTime_secs: Long,
                     speedLimit: Int, beginPortal: String, endPortal: String,
                     vehiclesPerLaneIn: VehiclesPerLane, vehiclesPerLaneOut: VehiclesPerLane,
                     matches: Int, maxSpeed: BigDecimal, averageSpeed: BigDecimal,
                     schedulePeriods: Seq[Period], systemStates: Seq[ZkState], metricsTypeOrder: Int = 100) extends Metrics {

  import TCMetrics._

  def +(that: TCMetrics): TCMetrics = {
    val intermediateResult = this.copy(reportPerformance = this.reportPerformance && that.reportPerformance,
      violations = this.violations + that.violations,
      hasStatistics = this.hasStatistics || that.hasStatistics,
      vehiclesPerLaneIn = addVehiclesPerLane(this.vehiclesPerLaneIn, that.vehiclesPerLaneIn),
      vehiclesPerLaneOut = addVehiclesPerLane(this.vehiclesPerLaneOut, that.vehiclesPerLaneOut),
      matches = this.matches + that.matches,
      maxSpeed = this.maxSpeed max that.maxSpeed,
      averageSpeed = if ((this.matches + that.matches) != 0) {
        (this.averageSpeed * this.matches + that.averageSpeed * that.matches) / (this.matches + that.matches)
      } else { 0 },
      systemStates = StateService.aggregatedStates(Seq(this.systemStates, that.systemStates)))

    val newPerformanceMetrics = intermediateResult.createPerformanceMetrics
    intermediateResult.copy(performance = newPerformanceMetrics)

  }

  def combine(that: TCMetrics): TCMetrics = {
    val intermediateResult = this.copy(
      reportPerformance = this.reportPerformance && that.reportPerformance,
      violations = this.violations + that.violations,
      hasStatistics = this.hasStatistics || that.hasStatistics,
      vehiclesPerLaneIn = this.vehiclesPerLaneIn ++ that.vehiclesPerLaneIn,
      vehiclesPerLaneOut = this.vehiclesPerLaneOut ++ that.vehiclesPerLaneOut,
      matches = this.matches + that.matches,
      maxSpeed = this.maxSpeed max that.maxSpeed,
      averageSpeed = if ((this.matches + that.matches) != 0) {
        (this.averageSpeed * this.matches + that.averageSpeed * that.matches) / (this.matches + that.matches)
      } else { 0 },
      systemStates = StateService.aggregatedStates(Seq(this.systemStates, that.systemStates)))

    //val newPerformanceMetrics = intermediateResult.createPerformanceMetrics
    //intermediateResult.copy(performance = newPerformanceMetrics)
    intermediateResult
  }

  def totalIn = vehiclesPerLaneIn.values.sum

  def totalOut = vehiclesPerLaneOut.values.sum

  def createPerformanceMetrics: PerformanceMetrics = {
    PerformanceMetrics(matchRatio = Ratio.divide(matches, totalIn max totalOut),
      violationRatio = Ratio.divide(violations.totalViolations, matches),
      violationRatioMatches = Ratio.divide(violations.totalViolations, matches),
      autoRatio = Ratio.divide(violations.auto, violations.totalViolations),
      autoRatioNetto = Ratio.divide(violations.auto, violations.totalViolations - violations.manualAccordingToSpec - violations.mobiDoNotProcess),
      avlf = violations.totalViolations > 0,
      enforceRatio = PerformanceMetrics.calculateEnforceRatio(systemStates, ZkState.enforceOn, ZkState.enforceDegraded, ZkState.enforceOff), // TODO: check calculations
      totalViolations = violations.totalViolations,
      autoRatioV60 = 0,
      failureRatio = 0,
      avtRatio = 0)
  }

  def correctCombinedSystemStates(systemStates: Seq[ZkState]): Seq[ZkState] = {
    PerformanceMetrics.correctCombinedSystemStates(systemStates)
  }
}

object TCMetrics {

  type VehiclesPerLane = Map[String, Int]
  val modus = "AUTONOOM"

  def apply(aReportId: String, aReportHtId: String, aBeginPortal: String, anEndPortal: String): TCMetrics = {
    new TCMetrics(reportId = aReportId, reportHtId = aReportHtId, reportPerformance = false, violations = ViolationMetrics(),
      performance = PerformanceMetrics(), modus = "AUTONOOM", hasStatistics = false, esaScanTime_secs = 0, esaPardonTime_secs = 0,
      speedLimit = 0, beginPortal = aBeginPortal, endPortal = anEndPortal, vehiclesPerLaneIn = Map(), vehiclesPerLaneOut = Map(),
      matches = 0, maxSpeed = 0, averageSpeed = 0, schedulePeriods = Nil, systemStates = Nil)
  }

  def apply(system: ZkSystem, corridorData: CorridorData, corridorStatistics: Seq[CorridorStatistics],
            schedulePeriods: Seq[Period], systemStates: Seq[ZkState], systemAlerts: Seq[ZkAlertHistory],
            speedLimits: Seq[Int]) = {
    require(schedulePeriods != null && schedulePeriods != Nil, "schedulePeriods cannot be null or Nil")
    // Intermediate values:
    val tcStatistics = corridorStatistics.collect {
      case stat: SectionControlCorridorStatistics ⇒ stat
    }
    val firstGantry = corridorData.startSection.inGantry
    val lastGantry = corridorData.endSection.outGantry
    val direction = system.location.roadPart

    val inStats: Seq[LaneInfo] = corridorStatistics.flatMap(_.laneStats.filter(laneInfo ⇒ laneInfo.lane.gantry == firstGantry.id))
    val outStats: Seq[LaneInfo] = corridorStatistics.flatMap(_.laneStats.filter(laneInfo ⇒ laneInfo.lane.gantry == lastGantry.id))

    val lanesToUseIn = getLanesWithoutLanesToIgnore(firstGantry.lanes, corridorData.dailyReportConfig.ignoreStatisticsOfLanes)
    val lanesToUseOut = getLanesWithoutLanesToIgnore(lastGantry.lanes, corridorData.dailyReportConfig.ignoreStatisticsOfLanes)

    // ivars for constructing case class
    val reportId = corridorData.corridor.info.reportId
    val reportHtId = corridorData.corridor.info.reportHtId
    val reportPerformance = corridorData.corridor.info.reportPerformance
    val hasStatistics: Boolean = corridorStatistics.length > 0
    val esaScanTime_secs = system.pardonTimes.pardonTimeTechnical_secs
    val esaPardonTime_secs = system.pardonTimes.pardonTimeEsa_secs
    val speedLimit: Int = speedLimits.reduceOption(_ max _).getOrElse(0)
    val beginPortal = firstGantry.name + " " + firstGantry.hectometer + " " + direction
    val endPortal = lastGantry.name + " " + lastGantry.hectometer + " " + direction
    val vehiclesPerLaneIn = calculateVehiclesPerLane(lanesToUseIn, inStats)
    val vehiclesPerLaneOut = calculateVehiclesPerLane(lanesToUseOut, outStats)

    val violationMetrics = ViolationMetrics(corridorStatistics)
    val performanceMetrics = PerformanceMetrics(corridorStatistics, vehiclesPerLaneIn.values.sum, vehiclesPerLaneOut.values.sum,
      systemStates, schedulePeriods, systemAlerts)

    val matches = tcStatistics.map(_.matched).sum
    def maxSpeed: Int = tcStatistics.map(_.highestSpeed).reduceOption(_ max _).getOrElse(0)
    val averageSpeed: BigDecimal = {
      val sumOfSpeeds = tcStatistics.map(stat ⇒ stat.matched * stat.averageSpeed).sum
      val totalMatches = tcStatistics.map(d ⇒ d.matched).sum
      Ratio.divide(sumOfSpeeds, totalMatches)
    }

    new TCMetrics(reportId, reportHtId, reportPerformance, violationMetrics, performanceMetrics, modus,
      hasStatistics, esaScanTime_secs, esaPardonTime_secs, speedLimit, beginPortal, endPortal, vehiclesPerLaneIn,
      vehiclesPerLaneOut, matches, maxSpeed, averageSpeed, schedulePeriods, systemStates)
  }

  /**
   *  Combine two lists of vehiclesperLane. And prevent that vehicles counted double.
   *  Each corridor counts its vehicles. When they have lanes in common the count is doubled.
   *  Therefor two lanes shouldn be added, but only one count should be used.
   *  It should not matther which one, because they should be equal, but in case of problems we take
   *  the max count. A Corridor probably missed vehicles. in stead of counting them double
   * @param vpl1
   * @param vpl2
   * @return
   */
  def addVehiclesPerLane(vpl1: VehiclesPerLane, vpl2: VehiclesPerLane): VehiclesPerLane = {
    val keys = (vpl1.keys ++ vpl2.keys).toSet
    keys.map {
      k ⇒
        k -> {
          val countV1 = vpl1.getOrElse(k, 0)
          val countV2 = vpl2.getOrElse(k, 0)
          if (countV1 != countV2) {
            val log = LoggerFactory.getLogger(this.getClass.getName)
            log.warn("Found different counts for lane %s [%d:%d]".format(k, countV1, countV2))
          }
          math.max(countV1, countV2)
        }
    }.toMap
  }

  private def calculateVehiclesPerLane(lanesToUse: Seq[LaneData], laneStatistics: Seq[LaneInfo]): VehiclesPerLane = {
    lanesToUse.map(laneData ⇒ {
      val bps_location = laneData.bpsLaneId.getOrElse("")
      val number: Int = laneStatistics.filter(info ⇒ info.lane.name == laneData.name).map(_.count).sum
      bps_location -> number
    }).toMap
  }

  private def getLanesWithoutLanesToIgnore(lanes: Seq[LaneData], ignoreStatisticsOfLanes: List[IgnoreStatisticsOfLane]): Seq[LaneData] = {
    lanes.filterNot(lane ⇒ ignoreStatisticsOfLane(ignoreStatisticsOfLanes, lane.id))
  }

  private def ignoreStatisticsOfLane(ignoreStatisticsOfLanes: List[IgnoreStatisticsOfLane], laneId: String): Boolean = {
    ignoreStatisticsOfLanes.exists(e ⇒ e.laneId == laneId)
  }

}
