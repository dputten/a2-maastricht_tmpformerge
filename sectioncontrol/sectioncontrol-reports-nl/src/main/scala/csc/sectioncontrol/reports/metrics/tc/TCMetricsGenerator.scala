package csc.sectioncontrol.reports.metrics.tc

import csc.sectioncontrol.messages._
import csc.sectioncontrol.storage.{ ParallelCorridorHHTrajectConfig, SectionControlStatistics, ZkSystem, _ }
import csc.sectioncontrol.reports.metrics.{ Period, tc }
import csc.sectioncontrol.enforce.ExportViolationsStatistics
import csc.sectioncontrol.reports.CorridorData
import org.slf4j.LoggerFactory
import csc.sectioncontrol.reports.schedules.CorridorSpeedSchedule
import csc.sectioncontrol.reports.metrics.common.{ PerformanceMetrics, ViolationMetrics }

object Extensions {

  implicit def stringWrapper(value: String) = new StringExtensions(value)

  class StringExtensions(val value: String) {
    def toOptionInt = {
      try {
        Some(value.toInt)
      } catch {
        case e: Exception ⇒ None
      }
    }
  }

}

object TCMetricsGenerator {
  val log = LoggerFactory.getLogger(this.getClass.getName)

  /**
   * Create the metrics of the SectionControl services
   * @param system              - the given system
   * @param corridorsData       - a sequence of descriptions for all corridors in the given system
   * @param corridorsStatistics - violations and lane statistics for each corridor
   * @param periods             - map of periods for which a schedule is active per corridor
   * @param corridorStates      - map of states per corridor
   * @param corridorAlerts      - map of alerts per corridor
   * @param schedules           - map of schedules per corridor
   * @return                    - map of statistics per (corridorId/speed)
   */
  def generateMetrics(system: ZkSystem,
                      corridorsData: Seq[CorridorData],
                      corridorsStatistics: Seq[CorridorStatistics],
                      periods: Map[Int, Seq[Period]],
                      corridorStates: Map[Int, Seq[ZkState]],
                      corridorAlerts: Map[Int, Seq[ZkAlertHistory]],
                      schedules: Map[Int, Seq[CorridorSpeedSchedule]]): Map[CorridorKey, TCMetrics] = {

    val sectionControlCorridorStatistics = corridorsStatistics.collect {
      case stats: SectionControlCorridorStatistics ⇒ stats
    }
    corridorsData.map(corridorData ⇒ {
      val corridorId = corridorData.corridor.info.corridorId
      val corridorSchedules: Seq[CorridorSpeedSchedule] = schedules.getOrElse(corridorId, Nil)
      val scheduleSpeeds = corridorSchedules.map(_.speedLimit).toSet
      val speedEntries = DailyReportConfigSpeedEntry.augmentWithSpeeds(corridorData.dailyReportConfig.speedEntries, scheduleSpeeds)

      log.info("corridorId: " + corridorId)
      log.info("dailyReportConfig: " + corridorData.dailyReportConfig)

      speedEntries.map(speedEntry ⇒ {
        val optSpeed: Option[Int] = speedEntry.speedLimit.toOptionInt
        log.info("speedEntry: " + speedEntry)
        val corridorStatistics = sectionControlCorridorStatistics.filter {
          case stats ⇒ stats.speedLimit == optSpeed && stats.corridorId == corridorId
        }
        log.info("corridorStatistics: " + corridorStatistics)
        optSpeed match {
          case None ⇒
            val corridorPeriods: Seq[Period] = periods.getOrElse(corridorId, Nil)
            log.info("corridorPeriods: " + corridorPeriods)
            val corridorSpeedLimits: Seq[Int] = schedules.getOrElse(corridorId, Nil).map(_.speedLimit).distinct
            log.info("corridorSpeedLimits: " + corridorSpeedLimits)
            val alerts: Seq[ZkAlertHistory] = corridorAlerts.getOrElse(corridorId, Seq())
            log.info("alerts: " + alerts)
            val states: Seq[ZkState] = corridorStates.getOrElse(corridorId, Seq())
            log.info("states: " + states)

            val tcMetrics = TCMetrics(system, corridorData, corridorStatistics, corridorPeriods, states, alerts, corridorSpeedLimits).
              copy(reportId = speedEntry.id, reportHtId = speedEntry.ht, speedLimit = 0)
            CorridorIdKey(corridorId) -> tcMetrics

          case Some(speed) ⇒
            val corridorPeriods: Seq[Period] = periods.getOrElse(corridorId, Nil).filter(p ⇒ {
              speedAtTime(corridorSchedules, p.from) == optSpeed
            })
            log.info("corridorPeriods: " + corridorPeriods)
            val corridorSpeedLimits: Seq[Int] = Seq(speed)
            val alerts: Seq[ZkAlertHistory] = corridorAlerts.getOrElse(corridorId, Nil).filter(
              zkAlert ⇒ speedAtTime(corridorSchedules, zkAlert.startTime) == optSpeed)
            log.info("alerts: " + alerts)
            val states: Seq[ZkState] = corridorStates.getOrElse(corridorId, Nil) /*.filter(
              zkState ⇒ speedAtTime(corridorSchedules, zkState.timestamp) == optSpeed)*/
            log.info("states: " + states)
            val metrics = if (speedEntries.size == 1) {
              TCMetrics(system, corridorData, corridorStatistics, corridorPeriods, states, alerts, corridorSpeedLimits).
                copy(reportId = speedEntry.id, reportHtId = speedEntry.ht)

            } else {
              TCMetrics(system, corridorData, corridorStatistics, corridorPeriods, states, alerts, corridorSpeedLimits).
                copy(performance = PerformanceMetrics(), reportId = speedEntry.id, reportHtId = speedEntry.ht)

            }
            CorridorSpeedKey(corridorId, Some(speed)) -> metrics
        }
      })

    }).flatten.toMap
  }

  /**
   *
   * @param systemId            - section-control system
   * @param stats               - day and timezone for which to calculate the statistics
   * @param violationStatistics - sequence of violation statistics per corridor
   * @param serviceResults      - map of statistics per corridor
   * @param corridorSchedules   - map fo schedules per corridor
   * @return
   */
  def calculateCorridorStatistics(systemId: String, stats: StatsDuration, violationStatistics: Seq[ExportViolationsStatistics],
                                  serviceResults: Map[Int, Seq[ServiceStatistics]],
                                  corridorSchedules: Map[Int, Seq[CorridorSpeedSchedule]]): Seq[CorridorStatistics] = {

    log.info("Calculate the Day Report TC-statistics for system  %s day %s".format(systemId, stats))
    val speedNone: Option[Int] = None
    // create map of (corridorId, None) to the sum of all violations per corridor regardless of speed
    val violationsPerCorridor: Map[(Int, Option[Int]), ExportViolationsStatistics] =
      violationStatistics.
        groupBy((v) ⇒ (v.corridorId, speedNone)).
        mapValues(ExportViolationsStatistics.sum).mapValues {
          s ⇒ s.copy(speedLimit = 0)
        }
    log.info("violationsPerCorridor: " + violationsPerCorridor)

    val violationsPerCorridorSpeed: Map[(Int, Some[Int]), ExportViolationsStatistics] = violationStatistics.groupBy(
      (v) ⇒ (v.corridorId, Some(v.speedLimit))).map {
        case (k, v) ⇒ (k, ExportViolationsStatistics.sum(v))
      }
    log.info("violationsPerCorridorSpeed: " + violationsPerCorridorSpeed)

    val violationsMap = violationsPerCorridor ++ violationsPerCorridorSpeed

    // convert serviceResults to Seq[SectionControlCorridorStatistics]
    val t = serviceResults.values.flatten.collect {
      case s: SectionControlStatistics ⇒ s
    }.map((s) ⇒ {
      val corridorId = s.config.id
      val optSpeed = speedAtTime(corridorSchedules.getOrElse(corridorId, Nil), s.period.begin)
      // TODO: Check if we need empty ExportViolationsStatistics or violationsPerCorridor((corridorId, None)
      SectionControlCorridorStatistics(optSpeed, corridorId, s.laneStats, ExportViolationsStatistics(), s.input,
        s.matched, s.unmatched, s.doubles, s.averageSpeed, s.highestSpeed)
    }).toSeq
    log.info("subTotals: " + t)

    // Calculate total stats per corridor. This covers all day, not just the schedule periods
    val statsPerCorridor = t.groupBy((s) ⇒ (s.corridorId, speedNone)).mapValues(SectionControlCorridorStatistics.sum)
    log.info("statsPerCorridor: " + statsPerCorridor)

    val statsPerCorridorSpeed = t.
      filter(_.speedLimit != speedNone).
      groupBy((s) ⇒ (s.corridorId, s.speedLimit))
      .mapValues(SectionControlCorridorStatistics.sum)
    log.info("statsPerCorridorSpeed: " + statsPerCorridorSpeed)

    val allStats: List[SectionControlCorridorStatistics] = (statsPerCorridor ++ statsPerCorridorSpeed).map((tuple) ⇒ {
      val (corId, optSpeedLimit) = tuple._1
      val value: SectionControlCorridorStatistics = tuple._2
      val defaultExport = ExportViolationsStatistics().copy(corridorId = corId, speedLimit = optSpeedLimit.getOrElse(0))

      // lookup violations in violationsMap. If not found, use empty violations
      val newViolations: ExportViolationsStatistics = violationsMap.getOrElse((corId, optSpeedLimit), defaultExport)

      // create new SectionControlCorridorStatistics
      val newValue = value.copy(corridorId = corId, speedLimit = optSpeedLimit, violations = newViolations)

      (corId, optSpeedLimit) -> newValue
    }).values.toList

    log.info("allStats: " + allStats)

    allStats

  }

  /**
   * Generate new metrics for groups of corridors. Existing Metrics for single corridors are modified,
   * because the values have no meaning if they're part of a group of corridors.
   *
   * @param tcMetricsMap contains the metrics calculated for physical corridors
   * @param configs configuration of groups of parallel corridors
   * @return a sequence of metrics
   */
  def applyParallelCorridorsToMetrics(tcMetricsMap: Map[CorridorKey, TCMetrics], configs: Seq[ParallelCorridorConfig]): Seq[TCMetrics] = {
    // For corridors that are part of a group of parallel corridors most values are meaningless. Set them to a sensible default
    def cleanMetricsMap(): Map[CorridorKey, TCMetrics] = {
      val allParallelCorridorIds = configs.flatMap(_.parallelCorridorIds).toSet
      log.info("allParallelCorridorIds: {}", allParallelCorridorIds)
      val answer = tcMetricsMap.map {
        case (corridorKey, tcMetrics) ⇒
          if (allParallelCorridorIds.contains(corridorKey.corridorId)) {
            log.info("Cleared PerformanceMetrics for corridor {}", corridorKey.corridorId)
            val newMetrics = tcMetrics.copy(performance = PerformanceMetrics(),
              hasStatistics = true, reportPerformance = false, matches = 0)
            (corridorKey, newMetrics)
          } else {
            (corridorKey, tcMetrics)
          }
      }
      log.info("cleanedMetricsMaps: {}", answer)
      answer
    }

    // create metrics for a given combination of parallel corridors
    def applyConfig(config: ParallelCorridorConfig) = {
      log.info("ParallelCorridorConfig {}", config)

      val keySet = tcMetricsMap.keySet

      val availableSpeedLimits = keySet.foldLeft(Set[Int]()) {
        case (list, key: CorridorSpeedKey) if config.parallelCorridorIds.contains(key.corridorId) ⇒ list ++ key.speed
        case (list, key) ⇒ list
      }.toSeq

      log.info("available speedLimits: {}", availableSpeedLimits)

      if (availableSpeedLimits.isEmpty) {
        log.error("speedLimits in ParallelCorridorConfig do not match with speedLimits in TCMetricsMap")
        log.error("TCMetricsMap.keySet = '{}'", keySet)
        Seq()
        // check if this is a single speed limit configuration
      } else if (availableSpeedLimits.size == 1) {
        val speedLimit = availableSpeedLimits.head

        //get the config for found speed, if the speed isn't configured use the total
        val conf: ParallelCorridorHHTrajectConfig = config.parallelHHTrajectConfigs.find(_.speedLimit == speedLimit.toString).getOrElse(
          ParallelCorridorHHTrajectConfig(speedLimit.toString, config.parallelCorridorIds) //Config not found use default
          )

        // Get all the individual metrics of the corridors
        val corridorMetrics: List[TCMetrics] = config.parallelCorridorIds.map {
          corrId ⇒ tcMetricsMap(CorridorSpeedKey(corridorId = corrId, speed = conf.speedLimit.toOptionInt))
        }

        log.info("Individual corridorMetrics: {}", corridorMetrics)

        // Make the combined, put all the violation statistics off all the corridors together is one combined
        val combinedMetrics = corridorMetrics.reduce(_ combine _).
          copy(reportId = conf.id, reportHtId = conf.ht, beginPortal = conf.entryGantryName, endPortal = conf.exitGantryName)

        log.info("combinedMetrics = {}", combinedMetrics)

        // Calculate the avt ratio for the combined
        val newAvtRatio = corridorMetrics.map(_.performance.avtRatio).sum / corridorMetrics.size

        // Calculate the failure ratio for combined
        val combinedFailureRatio = corridorMetrics.map(_.performance.failureRatio).sum / corridorMetrics.size

        /*
         Before creating the total package it's necessary to correct the combined enforce states to expected
         sequence
          */
        val combinedMetricsCorrectedSystemStates = combinedMetrics.correctCombinedSystemStates(combinedMetrics.systemStates)

        val correctedCombinedMetrics = combinedMetrics.copy(systemStates = combinedMetricsCorrectedSystemStates)

        // Create PerformanceMetrics for the combined
        //  createPerformanceMetrics is a 'method' that calculate all the performances.
        // Wrong enforceRatio and failureRatio is set to 0!
        val newPerformance = correctedCombinedMetrics.createPerformanceMetrics.copy(avtRatio = newAvtRatio, failureRatio = combinedFailureRatio)

        Seq(correctedCombinedMetrics.copy(performance = newPerformance))

        // Multiple speedLimits and a total
      } else {
        val allMetricsForCorridorAndSpeed: Seq[TCMetrics] = availableSpeedLimits.map {
          speedLimit ⇒
            val optSpeed = Some(speedLimit)
            val conf: ParallelCorridorHHTrajectConfig = config.parallelHHTrajectConfigs.find(_.speedLimit == speedLimit.toString).getOrElse(
              ParallelCorridorHHTrajectConfig(speedLimit.toString, config.parallelCorridorIds) //Config not found use default
              )

            // Get metrics for all corridors for the given speed and combine them
            val combinedMetrics = config.parallelCorridorIds.flatMap {
              corridorId ⇒
                tcMetricsMap.get(CorridorSpeedKey(corridorId, optSpeed))
            }.reduce(_ combine _).copy(reportId = conf.id, reportHtId = conf.ht, beginPortal = conf.entryGantryName, endPortal = conf.exitGantryName)
            combinedMetrics.copy(performance = PerformanceMetrics())
        }

        // Add the metrics for all speeds to create total metrics for all speeds
        val configForTotal = config.parallelHHTrajectConfigs.find(_.speedLimit == ParallelCorridorHHTrajectConfig.speedLimitTotal).getOrElse(
          ParallelCorridorHHTrajectConfig(ParallelCorridorHHTrajectConfig.speedLimitTotal, config.parallelCorridorIds) //Config not found use default
          )

        val totalMetrics = allMetricsForCorridorAndSpeed.reduce(_ + _).copy(reportId = configForTotal.id, reportHtId = configForTotal.ht, speedLimit = 0,
          beginPortal = configForTotal.entryGantryName, endPortal = configForTotal.exitGantryName)
        allMetricsForCorridorAndSpeed.map(_.copy(performance = PerformanceMetrics())) :+ totalMetrics
      }
    } // applyConfig

    val combinedMetrics = configs.map(applyConfig).flatten ++ cleanMetricsMap().values.toList
    combinedMetrics
  }

  private def scheduleAtTime(schedules: Seq[CorridorSpeedSchedule], time: Long): Option[CorridorSpeedSchedule] = {
    schedules.find(s ⇒ s.startTime <= time && s.endTime > time)
  }

  private def speedAtTime(schedules: Seq[CorridorSpeedSchedule], time: Long): Option[Int] = {
    scheduleAtTime(schedules, time).map(_.speedLimit)
  }
}
