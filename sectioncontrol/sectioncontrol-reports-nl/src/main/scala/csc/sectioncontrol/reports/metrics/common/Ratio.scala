/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.reports.metrics.common

object Ratio {

  def divide(dividend: BigDecimal, divisor: BigDecimal): BigDecimal = {
    // TODO: Review all uses of this method and decide on a per case basis
    val dvisor = divisor match {
      case x if (x <= 0) ⇒ BigDecimal(1)
      case y             ⇒ y
    }
    (dividend / dvisor)
  }
  def makeRatio(ratio: BigDecimal): BigDecimal = {
    // TODO: Review all uses of this method and decide on a per case basis
    ratio match {
      case x if (x <= 0) ⇒ BigDecimal(0)
      case r if (r > 1)  ⇒ BigDecimal(1)
      case y             ⇒ y
    }
  }

}