/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.reports

import csc.sectioncontrol.storage.{ ZkDailyReportConfig, ZkSystem, ZkSection }
import csc.sectioncontrol.storagelayer.CorridorConfig

case class SystemData(zkSystem: ZkSystem, corridors: List[CorridorData]) {
  require(zkSystem != null)
  require(corridors != null)
}

case class CorridorData(corridor: CorridorConfig,
                        startSection: SectionData,
                        endSection: SectionData,
                        dailyReportConfig: ZkDailyReportConfig) {
  require(corridor != null)
  require(startSection != null)
  require(endSection != null)
  require(dailyReportConfig != null)
}

case class SectionData(zkSection: ZkSection,
                       inGantry: GantryData,
                       outGantry: GantryData) {
  require(zkSection != null)
  require(inGantry != null)
  require(outGantry != null)
}

case class GantryData(id: String, name: String, hectometer: String, lanes: Seq[LaneData]) {
  require(id != null)
  require(!id.isEmpty)
  require(name != null)
  require(!name.isEmpty)
  require(hectometer != null)
  require(!hectometer.isEmpty)
  require(lanes != null)
  require(!lanes.isEmpty)
}

case class LaneData(id: String, name: String, seqNumber: Option[String] = None, seqName: Option[String] = None, bpsLaneId: Option[String] = None) {
  require(id != null)
  require(!id.isEmpty)
  require(name != null)
  require(!name.isEmpty)
}