package csc.sectioncontrol.reports.metrics

import common.{ AvailabilityPeriod, SystemMetrics, Metrics }
import csc.sectioncontrol.messages.{ CorridorKey, CorridorIdKey, CorridorSpeedKey }
import csc.sectioncontrol.messages._
import csc.sectioncontrol.reports.{ AllCorridorsStatistics, CorridorData }
import csc.sectioncontrol.storage._
import red.{ RedMetrics, RedMetricsGenerator }
import speed.{ SpeedMetrics, SpeedMetricsGenerator }
import tc.{ TCMetrics, TCMetricsGenerator }
import csc.sectioncontrol.enforce.{ ExportViolationsStatistics, DayViolationsStatistics }
import csc.sectioncontrol.reports.CorridorData
import csc.sectioncontrol.storage.SpeedFixedStatistics
import csc.sectioncontrol.storage.RedLightStatistics
import csc.sectioncontrol.storage.ZkSystem
import csc.sectioncontrol.reports.AllCorridorsStatistics
import csc.sectioncontrol.storage.SectionControlStatistics
import akka.event.LoggingAdapter
import csc.sectioncontrol.storagelayer.corridor.CorridorIdMapping
import csc.curator.utils.Curator
import csc.sectioncontrol.storagelayer.state.StateService
import org.slf4j.LoggerFactory
import java.text.SimpleDateFormat
import java.util.Date
import csc.sectioncontrol.reports.schedules.CorridorSpeedSchedule
import csc.akkautils.DirectLogging

object MetricsGenerator extends DirectLogging {

  /**
   * Create all the different metrics, by calling the correct generator for the services
   * @param stat
   * @param system
   * @param corridors
   * @param corridorStatistics
   * @param periods
   * @param corridorStates
   * @param corridorAlerts
   * @param schedules
   * @return
   */
  def generateMetrics(stat: StatsDuration,
                      system: ZkSystem,
                      corridors: Seq[CorridorData],
                      corridorStatistics: Seq[CorridorStatistics],
                      periods: Map[Int, Seq[Period]],
                      corridorStates: Map[Int, Seq[ZkState]],
                      corridorAlerts: Map[Int, Seq[ZkAlertHistory]],
                      schedules: Map[Int, Seq[CorridorSpeedSchedule]]): Map[CorridorKey, Metrics] = {
    //split data into corridor of the same type
    //SectionControl
    val tcCorridors = corridors.filter(cor ⇒ cor.corridor.serviceType == ServiceType.SectionControl)
    val tcInfoIds = tcCorridors.map(_.corridor.info.corridorId)
    val (tcCorStats, tcPeriod, tcSchedules, tcCorState) = filterCorridors(tcInfoIds, corridorStatistics, periods, schedules, corridorStates)
    val tcMetrics: Map[CorridorKey, TCMetrics] = TCMetricsGenerator.generateMetrics(system, tcCorridors, tcCorStats, tcPeriod, tcCorState, corridorAlerts, tcSchedules)
    log.info("tcMetrics: " + tcMetrics)
    //SpeedFixed
    val spCorridors = corridors.filter(cor ⇒ cor.corridor.serviceType == ServiceType.SpeedFixed)
    val spInfoIds = spCorridors.map(_.corridor.info.corridorId)
    val (spCorStats, spPeriod, spSchedules, spCorState) = filterCorridors(spInfoIds, corridorStatistics, periods, schedules, corridorStates)
    val spMetrics: Map[CorridorKey, SpeedMetrics] = SpeedMetricsGenerator.generateMetrics(system, spCorridors, spCorStats, spPeriod, spCorState, corridorAlerts, spSchedules)
    //RedLight
    val redCorridors = corridors.filter(cor ⇒ cor.corridor.serviceType == ServiceType.RedLight)
    val redInfoIds = redCorridors.map(_.corridor.info.corridorId)
    val (redCorStats, redPeriod, redSchedules, redCorState) = filterCorridors(redInfoIds, corridorStatistics, periods, schedules, corridorStates)
    val redMetrics: Map[CorridorKey, RedMetrics] = RedMetricsGenerator.generateMetrics(system, redCorridors, redCorStats, redPeriod, redCorState, corridorAlerts, redSchedules)
    tcMetrics ++ spMetrics ++ redMetrics
  }

  /**
   * Create the system metrics by adding all metrics together
   * @param stat
   * @param corridorAlerts
   * @param metrics
   * @return
   */
  def createSystemMetrics(stat: StatsDuration, corridorStates: Map[CorridorIdMapping, Seq[ZkState]], corridorAlerts: Map[CorridorIdMapping, Seq[ZkAlertHistory]], calibrationExcludePeriods: Map[CorridorIdMapping, Seq[Period]], metrics: Map[CorridorKey, Metrics]): SystemMetrics = {
    val corridorIds = (calibrationExcludePeriods.keySet ++ corridorStates.keySet ++ corridorAlerts.keySet)

    val summaries = corridorIds.map(corridorId ⇒ {
      val calibrationCor = calibrationExcludePeriods.get(corridorId).getOrElse(Seq())
      val metricsCor = metrics.get(CorridorIdKey(corridorId.infoId))
      val statesCor = corridorStates.get(corridorId).getOrElse(Seq())
      val alertsCor = corridorAlerts.get(corridorId).getOrElse(Seq())
      createCorridorMetrics(stat, statesCor, alertsCor, calibrationCor, Seq() ++ metricsCor)
    })

    //merge corridorData into one system metric
    val systemStates = StateService.aggregatedStates(corridorStates.values.toSeq)
    val startMetric = new SystemMetrics(nrTotal = 0,
      nrViolations = 0,
      nrAuto = 0,
      nrHand = 0,
      enforceRatio = getEnforceRatio(start = stat.start, end = stat.tomorrow.start, states = systemStates),
      availablePeriods = Seq(new AvailabilityPeriod(stat.start, stat.tomorrow.start, 0)))
    summaries.foldLeft(startMetric) { (result, metric) ⇒
      {
        result.copy(nrTotal = math.max(result.nrTotal, metric.nrTotal),
          nrViolations = result.nrViolations + metric.nrViolations,
          nrAuto = result.nrAuto + metric.nrAuto,
          nrHand = result.nrHand + metric.nrHand,
          availablePeriods = AvailabilityPeriod.mergePeriods(result.availablePeriods, metric.availablePeriods))
      }
    }
  }

  def createCorridorMetrics(stat: StatsDuration, states: Seq[ZkState], systemAlerts: Seq[ZkAlertHistory], calibrationExcludePeriods: Seq[Period], metrics: Seq[Metrics]): SystemMetrics = {
    val avail = createAvailablePeriods(start = stat.start, end = stat.tomorrow.start,
      states = states, calibrationExcludePeriods = calibrationExcludePeriods,
      systemAlerts = systemAlerts)

    val startMetrics = new SystemMetrics(nrTotal = 0,
      nrViolations = 0,
      nrAuto = 0,
      nrHand = 0,
      enforceRatio = getEnforceRatio(start = stat.start, end = stat.tomorrow.start, states = states),
      availablePeriods = avail)
    if (metrics.isEmpty) {
      return startMetrics
    }
    //total: don't count registrations more than once
    // So use only one type
    val countType = metrics.head match {
      case redMetric: RedMetrics     ⇒ ServiceType.RedLight
      case speedMetric: SpeedMetrics ⇒ ServiceType.SpeedFixed
      case tcMetric: TCMetrics       ⇒ ServiceType.SectionControl
    }
    val sumMetrics = metrics.foldLeft(startMetrics) { (system, metric) ⇒
      metric match {
        case redMetric: RedMetrics ⇒
          val total = if (countType == ServiceType.RedLight)
            system.nrTotal + redMetric.total
          else
            system.nrTotal
          system.copy(nrTotal = total,
            nrViolations = system.nrViolations + redMetric.violations.totalViolations,
            nrAuto = system.nrAuto + redMetric.violations.auto,
            nrHand = system.nrHand + redMetric.violations.manual)

        case speedMetric: SpeedMetrics ⇒
          val total = if (countType == ServiceType.SpeedFixed)
            system.nrTotal + speedMetric.total
          else
            system.nrTotal
          system.copy(nrTotal = total,
            nrViolations = system.nrViolations + speedMetric.violations.totalViolations,
            nrAuto = system.nrAuto + speedMetric.violations.auto,
            nrHand = system.nrHand + speedMetric.violations.manual)

        case tcMetric: TCMetrics ⇒
          val total = if (countType == ServiceType.SectionControl)
            system.nrTotal + math.max(tcMetric.totalIn, tcMetric.totalIn)
          else
            system.nrTotal
          system.copy(nrTotal = total,
            nrViolations = system.nrViolations + tcMetric.violations.totalViolations,
            nrAuto = system.nrAuto + tcMetric.violations.auto,
            nrHand = system.nrHand + tcMetric.violations.manual)
      }
    }
    sumMetrics
  }

  def createAvailablePeriods(start: Long, end: Long, states: Seq[ZkState], calibrationExcludePeriods: Seq[Period],
                             systemAlerts: Seq[ZkAlertHistory]): Seq[AvailabilityPeriod] = {
    def getReductionForState(state: String): Float = {
      state match {
        case "Off"         ⇒ 1F
        case "Maintenance" ⇒ 1F
        case "Failure"     ⇒ 1F
        case _             ⇒ 0F
      }
    }
    case class StateAvailabilityPeriod(state: String, period: AvailabilityPeriod)
    //create AlertPeriods from states
    val sorted: Seq[ZkState] = states sortWith { (first, second) ⇒ first.timestamp < second.timestamp }
    //When there isn't any state assume StandBy (should not happen)
    if (sorted.isEmpty) {
      val log = LoggerFactory.getLogger(getClass)
      val dateFormat = new SimpleDateFormat("dd/MM/yyyy'T'HH:mm:ss.SSS")
      log.warn("Received no states while creating AvailablePeriods for %s to %s".format(dateFormat.format(new Date(start)), dateFormat.format(new Date(end))))
    }
    val initial = if (sorted.isEmpty) 0F else getReductionForState(sorted.head.state)
    val state = sorted.headOption.map(_.state).getOrElse("StandBy")
    val initialPeriod = Seq(StateAvailabilityPeriod(state, new AvailabilityPeriod(start, end, initial)))

    val startPeriods = if (sorted.isEmpty) {
      initialPeriod
    } else {
      sorted.tail.foldLeft(initialPeriod) {
        (periodList, state) ⇒
          {
            //split last period
            val init = periodList.init
            val last = periodList.last
            val firstPeriod = StateAvailabilityPeriod(last.state, last.period.copy(end = state.timestamp))
            val lastPeriod = StateAvailabilityPeriod(state.state, last.period.copy(start = state.timestamp, reductionFactor = getReductionForState(state.state)))
            init ++ Seq(firstPeriod, lastPeriod)
          }
      }
    }
    //merge calibration period only with enforce periods
    val calibrationPeriods = calibrationExcludePeriods.map(p ⇒ new AvailabilityPeriod(p.from, p.to, 1F))
    val mergedCalibration = startPeriods.foldLeft(Seq[AvailabilityPeriod]()) {
      (periodList, period) ⇒
        {
          if (isEnforceState(period.state)) {
            periodList ++ AvailabilityPeriod.mergePeriods(Seq(period.period), calibrationPeriods)
          } else {
            periodList :+ period.period
          }
        }
    }
    //create AlertPeriods from alerts
    AvailabilityPeriod.createPeriods(mergedCalibration, systemAlerts)
  }

  def isEnforceState(state: String): Boolean = {
    state match {
      case "EnforceOn"       ⇒ true
      case "EnforceOff"      ⇒ true
      case "EnforceDegraded" ⇒ true
      case _                 ⇒ false
    }
  }

  def getEnforceRatio(start: Long, end: Long, states: Seq[ZkState]): Float = {

    def getTime(state: ZkState): Long = {
      val minValue = math.max(start, state.timestamp)
      math.min(end, minValue)
    }

    if (states.isEmpty)
      return 0F

    val sorted: Seq[ZkState] = states sortWith { (first, second) ⇒ first.timestamp < second.timestamp }

    val startTime = sorted.headOption.flatMap(state ⇒
      if (isEnforceState(state.state))
        Some(getTime(state)) else None)

    val (sum, startPeriod) = sorted.tail.foldLeft(0L, startTime) {
      case ((sum, startPeriod), state) ⇒ {
        val newSum = startPeriod match {
          case Some(startTime) ⇒ sum + (getTime(state) - startTime)
          case _               ⇒ sum
        }
        val newStartPeriod = if (isEnforceState(state.state)) {
          Some(getTime(state))
        } else {
          None
        }
        (newSum, newStartPeriod)
      }
    }
    val newSum = startPeriod match {
      case Some(startTime) ⇒ sum + (end - startTime)
      case _               ⇒ sum
    }
    //use minutes to calculate the rate
    (newSum / 60000).toFloat / (24 * 60).toFloat
  }

  private def filterCorridors(tcInfoIds: Seq[Int],
                              corridorStatistics: Seq[CorridorStatistics],
                              periods: Map[Int, Seq[Period]],
                              schedules: Map[Int, Seq[CorridorSpeedSchedule]],
                              corridorStates: Map[Int, Seq[ZkState]]): (Seq[CorridorStatistics], Map[Int, Seq[Period]], Map[Int, Seq[CorridorSpeedSchedule]], Map[Int, Seq[ZkState]]) = {
    val corStats = corridorStatistics.filter(stat ⇒ tcInfoIds.contains(stat.corridorId))
    val per = periods.filter { case (corId, _) ⇒ tcInfoIds.contains(corId) }
    val sched = schedules.filter { case (corId, _) ⇒ tcInfoIds.contains(corId) }
    val corState = corridorStates.filter { case (corId, _) ⇒ tcInfoIds.contains(corId) }
    (corStats, per, sched, corState)
  }

  /**
   * Calculate the statistics per each type of enforcement.
   * @param statistics the statistics for a given system and a given day containing stats on violations for each corridor
   * @param serviceResults Maps corridorId to ServiceStatistics
   * @param log Optional logging adaptor
   * @return Aggregated statistics for all corridors
   *
   * Nb: Each corridor has a specific type of enforcement (SectionControl, SpeedFixed, RedLight). This cannot be infered form
   *     information in the DayViolationStatistics. However, each entry in serviceResults contains instances of one specific
   *     subclass of ServiceStatistics (i.e. SectionControlStatistics, RedLightStatistics, SpeedFixedStatistics). So we're left
   *     with the option to determine the type of a given corridor by the type of statistics available in serviceResults.
   *     HINT: Inject ServiceType into ExportViolationStatistics.
   */
  def calculate(statistics: DayViolationsStatistics, serviceResults: Map[Int, Seq[ServiceStatistics]],
                schedules: Map[Int, Seq[CorridorSpeedSchedule]], log: Option[LoggingAdapter] = None): AllCorridorsStatistics = {

    /**
     * Test if the serviceResults for the given corridorID satisfies the given partial function. It gets the serviceResults
     * for the given corridorId and performs a collectFirst on the serviceResults using PF. If the result is None there are
     * no corresponding serviceResults for this corridor.
     * @param corridorId the corridorId
     * @param pf the partial function used to perform the test
     * @return True if there are serviceResults for this corridorId for which the partial function is defined, false otherwise
     */
    def corridorHasStatisticsOfType(corridorId: Int, pf: PartialFunction[Any, ServiceStatistics]): Boolean = {
      // get the corresponding ServiceStatistics or Nil (empty sequence)
      val firstResult = serviceResults.getOrElse(corridorId, Nil).headOption
      // get the first element for which pf is defined and return as Option
      for (x ← firstResult) {
        return pf isDefinedAt x
      }
      false
    }

    log.foreach(_.info("Calculate the Day Report statistics for system  %s day %s".
      format(statistics.systemId, statistics.stats)))

    // ExportViolationStatistics for all corridors enforcing SectionControl
    val tcData: Seq[ExportViolationsStatistics] = statistics.corridorData.filter(d ⇒
      { corridorHasStatisticsOfType(d.corridorId, { case s: SectionControlStatistics ⇒ s }) })

    // Get the corridors that enforce RedLight
    val redData = statistics.corridorData.filter(d ⇒ { corridorHasStatisticsOfType(d.corridorId, { case s: RedLightStatistics ⇒ s }) })

    // Get the Corridors that enforce SpeedFixed
    val speedData = statistics.corridorData.filter(d ⇒ { corridorHasStatisticsOfType(d.corridorId, { case s: SpeedFixedStatistics ⇒ s }) })

    // Now generate statistics for each type of enforcement.
    val tcStats = TCMetricsGenerator.calculateCorridorStatistics(statistics.systemId, statistics.stats, tcData, serviceResults, schedules)
    val redStats = RedMetricsGenerator.calculate(statistics.systemId, statistics.stats, redData, serviceResults)
    val speedStats = SpeedMetricsGenerator.calculate(statistics.systemId, statistics.stats, speedData, serviceResults)

    // And aggregate them into AllCorridorStatistics
    AllCorridorsStatistics(statistics.systemId, statistics.stats, tcStats ++ redStats ++ speedStats)
  }

}
