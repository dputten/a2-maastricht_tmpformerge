/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.reports.metrics.common

import java.util.Calendar

import csc.akkautils.DirectLogging
import csc.sectioncontrol.messages.{ SectionControlCorridorStatistics, CorridorStatistics }
import csc.sectioncontrol.reports.metrics.{ StateRatio, Period }
import csc.sectioncontrol.storage.{ ZkAlertHistory, ZkState }
import csc.sectioncontrol.storagelayer.alerts.AlertService

/*
 * Base trait for all metrics.
 * Ordering of metrics types is required by the xsd's. The ordering required
 * is TCMetric, SpeedMetric, RedMetrics
 */

trait Metrics {
  val violations: ViolationMetrics
  val performance: PerformanceMetrics
  val metricsTypeOrder: Int
}

case class ViolationMetrics(totalViolations: Int = 0, manual: Int = 0, auto: Int = 0, mobi: Int = 0,
                            manualAccordingToSpec: Int = 0, mobiDoNotProcess: Int = 0, mtmPardon: Int = 0,
                            doublePardon: Int = 0, otherPardon: Int = 0) {

  def mtmRatio: BigDecimal = Ratio.divide(mtmPardon, totalViolations + mtmPardon)

  def +(that: ViolationMetrics): ViolationMetrics = {
    ViolationMetrics(totalViolations + that.totalViolations, manual + that.manual, auto + that.auto, mobi + that.mobi,
      manualAccordingToSpec + that.manualAccordingToSpec, mobiDoNotProcess + that.mobiDoNotProcess, mtmPardon + that.mtmPardon,
      doublePardon + that.doublePardon, otherPardon + that.otherPardon)
  }
}

object ViolationMetrics {
  def apply(corridorStatistics: Seq[CorridorStatistics]) = {
    // Sum all nine statistics with a single foldLeft.
    val accu = (0, 0, 0, 0, 0, 0, 0, 0, 0)
    val (totalViolations, manual, auto, mobi, manualAccordingToSpec, mobiDoNotProcess, mtmPardon, doublePardon, otherPardon) =
      corridorStatistics.foldLeft(accu)((accu, tc) ⇒ {
        val v = tc.violations
        val (aTotal, aManual, aAuto, aMobi, aManualATS, aMobiDontProcess, aMtmPardon, aDoublePardon, aOtherPardon) = accu
        (aTotal + v.total, aManual + v.manual, aAuto + v.auto, aMobi + v.mobi, aManualATS + v.manualAccordingToSpec,
          aMobiDontProcess + v.mobiDoNotProcess, aMtmPardon + v.mtmPardon, aDoublePardon + v.doublePardon,
          aOtherPardon + v.otherPardon)
      })
    new ViolationMetrics(totalViolations, manual, auto, mobi, manualAccordingToSpec, mobiDoNotProcess, mtmPardon,
      doublePardon, otherPardon)
  }
}

case class PerformanceMetrics(totalViolations: Int = 0,
                              matchRatio: BigDecimal = 0,
                              violationRatio: BigDecimal = 0,
                              violationRatioMatches: BigDecimal = 0,
                              autoRatio: BigDecimal = 0,
                              autoRatioV60: BigDecimal = 0,
                              autoRatioNetto: BigDecimal = 0,
                              avlf: Boolean = false,
                              enforceRatio: BigDecimal = 0,
                              failureRatio: BigDecimal = 0,
                              avtRatio: BigDecimal = 0)

object PerformanceMetrics {
  def apply(corridorStatistics: Seq[CorridorStatistics],
            totalIn: Int,
            totalOut: Int,
            systemStates: Seq[ZkState],
            periods: Seq[Period],
            systemAlerts: Seq[ZkAlertHistory]) = {

    if (periods == Nil) {
      new PerformanceMetrics()
    } else {
      // Some recurring intermediate values
      val tcStatistics: Seq[SectionControlCorridorStatistics] = corridorStatistics.collect {
        case tc: SectionControlCorridorStatistics ⇒ tc
      }
      val maxTotalInOut = math.max(totalIn, totalOut)
      val totalMatches = tcStatistics.map(_.matched).sum
      val totalViolations = tcStatistics.map(_.violations.total).sum
      val autoViolations = tcStatistics.map(_.violations.auto).sum
      val nettoViolations = tcStatistics.map {
        tc ⇒
          tc.violations.total - tc.violations.manualAccordingToSpec - tc.violations.mobiDoNotProcess
      }.sum

      // Ratios
      val matchRatio: BigDecimal = Ratio.divide(totalMatches, BigDecimal(maxTotalInOut))
      val violationRatio = Ratio.divide(totalViolations, BigDecimal(maxTotalInOut))
      val violationRatioMatches = Ratio.divide(totalViolations, totalMatches)
      val autoRatio = Ratio.divide(autoViolations, totalViolations)
      val autoRatioV60 = Ratio.divide(autoViolations + tcStatistics.map(_.violations.mobiDoNotProcess).sum, totalViolations)
      val autoRatioNetto = Ratio.divide(autoViolations, nettoViolations)
      val avlf = totalViolations > 0
      val enforceRatio: BigDecimal = calculateEnforceRatio(systemStates, ZkState.enforceOn, ZkState.enforceDegraded, ZkState.enforceOff)
      val failureRatio: BigDecimal = Ratio.makeRatio(1F - AlertService.getReductionFactor(systemAlerts))
      val avtRatio: BigDecimal = calculateAvtRatio(periods, systemStates, failureRatio)

      new PerformanceMetrics(totalViolations, matchRatio, violationRatio, violationRatioMatches, autoRatio,
        autoRatioV60, autoRatioNetto, avlf, enforceRatio, failureRatio, avtRatio)
    }
  }

  def calculateEnforceRatio(systemStates: Seq[ZkState], enforceStates: String*): BigDecimal = {
    if (systemStates.isEmpty) return 0

    // If we have only one state we treat it as the state for the whole report day
    if (systemStates.size == 1) return if (enforceStates.contains(systemStates.head.state)) 1 else 0

    val sortedSystemStates = sortAndCorrectForBeginningAndEndOfDay(systemStates)

    val enforceTimeInMillis = sortedSystemStates
      .zip(sortedSystemStates.tail)
      .map { case (from, to) ⇒ if (enforceStates.contains(from.state)) to.timestamp - from.timestamp else 0 }
      .sum

    val enforceTimeInMinutes = BigDecimal(enforceTimeInMillis) / (1000 * 60)
    enforceTimeInMinutes / (24 * 60)
  }

  //private def sortAndCorrectForBeginningAndEndOfDay(systemStates: Seq[ZkState]) = {
  def sortAndCorrectForBeginningAndEndOfDay(systemStates: Seq[ZkState]) = {
    val sortedSystemStates = systemStates.sortBy(_.timestamp)

    sortedSystemStates match {
      case firstState :: secondState :: xs if firstState.timestamp < startOfDay(secondState.timestamp) ⇒
        // The first state can be in the past so we use the second state to determine the day we are actually processing
        val correctedFirstState = firstState.copy(timestamp = startOfDay(secondState.timestamp))
        // We append a state for 23:59:59 at the end to be sure we capture the whole day
        val extraEndState = sortedSystemStates.last.copy(timestamp = startOfNextDay(secondState.timestamp) - 1)
        (correctedFirstState :: secondState :: xs) :+ extraEndState
      case other ⇒ other // no reset required
    }
  }
  private def calculateAvtRatio(periods: Seq[Period], systemStates: Seq[ZkState], failureRatio: BigDecimal): BigDecimal = {

    val ratios = StateRatio(periods, systemStates)

    val onRatio: Long = ratios.filter(
      s ⇒
        s.state == ZkState.enforceOn ||
          s.state == ZkState.standBy ||
          s.state == ZkState.enforceOff)
      .map(_.timePeriods)
      .sum

    val totalTime: Long = {
      if (periods.length > 0)
        periods.map(sp ⇒ sp.to - sp.from).sum
      else 0
    }

    val degradedRatio: Long = ratios
      .find(_.state == ZkState.enforceDegraded)
      .map(_.timePeriods)
      .getOrElse(0l)

    Ratio.divide(onRatio + degradedRatio * failureRatio.toFloat, 24 * 60 * 60 * 1000)
  }

  // TODO Should be moved to some utility package or implicit predef
  private def startOfDay(day: Long): Long = {
    resetTimeOfDayTo(day, 0, 0, 0)
  }

  // TODO Should be moved to some utility package or implicit predefug.
  // TODO BUG: endOfDay does not exist. There's only the beginning of the next day
  private def endOfDay(day: Long): Long = {
    resetTimeOfDayTo(day, 23, 59, 59)
  }

  private def startOfNextDay(day: Long): Long = {
    resetTimeOfDayTo(day, 24, 0, 0)
  }

  // TODO Should be moved to some utility package or implicit predef
  private def resetTimeOfDayTo(day: Long, hours: Int, minutes: Int, seconds: Int): Long = {
    val date: Calendar = Calendar.getInstance()
    date.setTimeInMillis(day)
    // reset hour, minutes, seconds and millis (== beginning of day)
    date.set(Calendar.HOUR_OF_DAY, hours)
    date.set(Calendar.MINUTE, minutes)
    date.set(Calendar.SECOND, seconds)
    date.set(Calendar.MILLISECOND, 0)
    date.getTimeInMillis
  }

  /*
   Cleanup the systemState list of a combined 'section/corridor'
   The systems states of a combined corridor contains all the states of all the corridors.
   That means that there are double states of the previous and current day.
     This methode will cleanup thos double states.
   */
  def correctCombinedSystemStates(systemStates: Seq[ZkState]): Seq[ZkState] = {
    /*
     It's expected that the systems states come in order <prev day> ... <current day>
     To remove the equal times we start at the last systemState of the current day.
    */
    val removeEqTimeStates = removeEqualTimeSystemStates(systemStates.reverse)

    /*
     Remove the equal states (EnforceOn, ....), but start with yesterday's state
     */
    removeEqualStates(removeEqTimeStates.reverse)
  }

  private def removeEqualTimeSystemStates(states: Seq[ZkState]): Seq[ZkState] = {

    def accumulator(states: Seq[ZkState], newList: Seq[ZkState]): Seq[ZkState] = {

      states match {
        case Nil ⇒ {
          // End of processing return the created new list
          newList
        }

        case first :: Nil ⇒ {
          // Add the last element to the new list
          accumulator(Nil, newList :+ states.head)
        }

        case first :: rest ⇒ {
          // Has the the element and second element in the list the same time
          if (states.tail.head.timestamp == states.head.timestamp) {
            // Times are equal; remove the second element from the list and process the list again
            val aList = states.take(1) ++ states.drop(2)

            accumulator(aList, newList)
          } else {
            // The times are not equal; first element is now 'unique' and becomes part of the new list
            accumulator(states.tail, newList :+ states.head)
          }
        }
      }
    }

    accumulator(states, List[ZkState]())

  }

  def removeEqualStates(states: Seq[ZkState]): Seq[ZkState] = {

    def accumulator(states: Seq[ZkState], newList: Seq[ZkState]): Seq[ZkState] = {

      states match {
        case Nil ⇒ {
          newList
        }

        case first :: Nil ⇒ {
          accumulator(Nil, newList :+ states.head)
        }

        case first :: rest ⇒ {
          if (states.tail.head.state == states.head.state) {
            val aList = states.take(1) ++ states.drop(2)

            accumulator(aList, newList)
          } else {
            accumulator(states.tail, newList :+ states.head)
          }
        }

      }
    }

    accumulator(states, List[ZkState]())

  }
}

/**
 * Metrics class of the complete system
 * @param nrTotal total number of registrations
 * @param nrViolations total number of the violations
 * @param nrAuto total number of violations in the auto files
 * @param nrHand total number of violations in the manual files
 * @param enforceRatio the enforceRatio
 * @param availablePeriods the availability periods
 */
case class SystemMetrics(nrTotal: Int,
                         nrViolations: Int,
                         nrAuto: Int,
                         nrHand: Int,
                         enforceRatio: Float,
                         availablePeriods: Seq[AvailabilityPeriod]) {
  //both values should be equal but to be sure calculate again
  val nrExportViolations = nrAuto + nrHand

  def violationRatio: BigDecimal = Ratio.divide(nrViolations, nrTotal)

  def autoRatio: BigDecimal = Ratio.divide(nrAuto, nrExportViolations)

  def timeAvailable = {
    //sum all periods where reductionFactor is 0
    val milliSeconds = availablePeriods.foldLeft(0L) { (sum, period) ⇒
      if (period.reductionFactor == 0F)
        sum + (period.end - period.start)
      else
        sum
    }
    val minutes = milliSeconds / 60000

    "PT%dH%dM".format(minutes / 60, minutes % 60)
  }

}

/**
 * Class to create periods with it reduction factor
 * @param start start of the period
 * @param end end of the period
 * @param reductionFactor the reduction factor
 */
case class AvailabilityPeriod(start: Long, end: Long, reductionFactor: Float)

/**
 * Suporting object to create AlertPeriods
 */
object AvailabilityPeriod {
  /**
   * Create the reduction factor periods for the given period
   * @param start start of the period
   * @param end end of the period
   * @param alerts list of alerts
   * @return list of periods with the reduction factor
   */
  def createPeriods(start: Long, end: Long, alerts: Seq[ZkAlertHistory]): Seq[AvailabilityPeriod] = {
    createPeriods(Seq(new AvailabilityPeriod(start, end, 0F)), alerts)
  }

  /**
   * Create the reduction factor periods for the given periods
   * @param startPeriods list of initial periods
   * @param alerts list of alerts
   * @return list of periods with the reduction factor
   */
  def createPeriods(startPeriods: Seq[AvailabilityPeriod], alerts: Seq[ZkAlertHistory]): Seq[AvailabilityPeriod] = {

    alerts.map(alert ⇒ new AvailabilityPeriod(alert.startTime, alert.endTime, alert.reductionFactor))
      .foldLeft(startPeriods) { (periodList, hist) ⇒
        //split periods based on current history period
        periodList.flatMap(period ⇒ splitPeriod(period, hist))
      }
  }

  def mergePeriods(startPeriods: Seq[AvailabilityPeriod], periods: Seq[AvailabilityPeriod]): Seq[AvailabilityPeriod] = {
    periods.foldLeft(startPeriods) { (periodList, hist) ⇒
      //split periods based on current history period
      periodList.flatMap(period ⇒ splitPeriod(period, hist))
    }
  }

  private def splitPeriod(period: AvailabilityPeriod, hist: AvailabilityPeriod): Seq[AvailabilityPeriod] = {
    //split period
    if (period.end < hist.start || // <p -- p> |..|    => <p --p>
      period.start > hist.end) { // | ..| <p -- p>   => <p--p>
      Seq(period)
    } else if (period.start > hist.start) {
      if (period.end < hist.end) { // | ..<p-- p> ..|  => <p--p> with updated reduction
        Seq(period.copy(reductionFactor = period.reductionFactor + hist.reductionFactor))
      } else { // | ..<p..|-- p>   => <p--><--p>
        val startPeriod = period.copy(end = hist.end, reductionFactor = period.reductionFactor + hist.reductionFactor)
        val endPeriod = period.copy(start = hist.end)
        Seq(startPeriod, endPeriod)
      }
    } else {
      if (period.end < hist.end) { // <p -| -- p> ..|  => <p-><-p>
        val startPeriod = period.copy(end = hist.start)
        val endPeriod = period.copy(start = hist.start, reductionFactor = period.reductionFactor + hist.reductionFactor)
        Seq(startPeriod, endPeriod)
      } else { // <p -|..|-- p>    => <p-><-><-p>
        val startPeriod = period.copy(end = hist.start)
        val middlePeriod = period.copy(start = hist.start, end = hist.end, reductionFactor = period.reductionFactor + hist.reductionFactor)
        val endPeriod = period.copy(start = hist.end)
        Seq(startPeriod, middlePeriod, endPeriod)
      }
    }
  }
}

/**
 * The performance metrics
 * //@param corridorStatistics
 * //@param totalIn
 * //@param totalOut
 * //@param systemStates
 * //@param periods
 * //@param systemAlerts
 */
/*
class PerformanceMetrics(corridorStatistics: Seq[CorridorStatistics],
                         totalIn: Int,
                         totalOut: Int,
                         systemStates: Seq[ZkState],
                         periods: Seq[Period],
                         systemAlerts: Seq[ZkAlertHistory]) extends BasePerformanceMetrics
with DirectLogging {

  def totalViolations = corridorStatistics.map(_.violations.total).sum

  override def matchRatio: BigDecimal = {
    val maxMatches = corridorStatistics.map(stat ⇒ stat match {
      case tc: SectionControlCorridorStatistics ⇒ tc.matched
      case _                                    ⇒ 0
    }).sum
    Ratio.divide(maxMatches, BigDecimal(math.max(totalIn, totalOut)))
  }

  override def violationRatio: BigDecimal = {
    Ratio.divide(totalViolations, BigDecimal(math.max(totalIn, totalOut)))
  }

  override def violationRatioMatches: BigDecimal = {
    val maxMatches = corridorStatistics.map(stat ⇒ stat match {
      case tc: SectionControlCorridorStatistics ⇒ tc.matched
      case _                                    ⇒ 0
    }).sum
    Ratio.divide(totalViolations, maxMatches)
  }

  override def autoRatio: BigDecimal = {
    val allAutos = corridorStatistics.map(_.violations.auto).sum
    Ratio.divide(allAutos, totalViolations)
  }

  override def autoRatioV60: BigDecimal = {
    val allAutos = corridorStatistics.map(_.violations.auto).sum +
      corridorStatistics.map(_.violations.mobiDoNotProcess).sum
    val allViolations = corridorStatistics.map(_.violations.total).sum
    Ratio.divide(allAutos, totalViolations)
  }

  override def autoRatioNetto: BigDecimal = {
    val allFilterd = corridorStatistics.map(stat ⇒
      stat.violations.total -
        stat.violations.manualAccordingToSpec -
        stat.violations.mobiDoNotProcess).sum
    val allAutos = corridorStatistics.map(_.violations.auto).sum

    Ratio.divide(allAutos, allFilterd)
  }

  override def avlf: Boolean = totalViolations > 0

  override def enforceRatio: BigDecimal = {
    calculateWeightedRatio(ZkState.enforceOn, ZkState.enforceDegraded, ZkState.enforceOff)
  }

  /**
   * Ratio: 1 min de som van alle attributen av-t-reductie van alle storingen in element
   * log/actueelstoringen/storingsmelding (zie §2.1.3).
   */
  override def failureRatio: BigDecimal = {
    val ratio = 1F - AlertService.getReductionFactor(systemAlerts)
    Ratio.makeRatio(ratio)
  }

  private def calculateWeightedRatio(enforceStates: String*): BigDecimal = {
    if (systemStates.isEmpty) return 0
    // If we have only one state we treat it as the state for the whole report day
    if (systemStates.size == 1) return if (enforceStates.contains(systemStates.head.state)) 1 else 0

    val sortedSystemStates = sortAndCorrectForBeginningAndEndOfDay(systemStates)

    val enforceTimeInMillis = sortedSystemStates
      .zip(sortedSystemStates.tail)
      .map { case (from, to) ⇒ if (enforceStates.contains(from.state)) to.timestamp - from.timestamp else 0 }
      .sum

    val enforceTimeInMinutes = BigDecimal(enforceTimeInMillis) / (1000 * 60)
    enforceTimeInMinutes / (24 * 60)
  }

  private def sortAndCorrectForBeginningAndEndOfDay(systemStates: Seq[ZkState]) = {
    val sortedSystemStates = systemStates.sortBy(_.timestamp)
    sortedSystemStates match {
      case firstState :: secondState :: xs if firstState.timestamp < startOfDay(secondState.timestamp) ⇒
        // The first state can be in the past so we use the second state to determine the day we are actually processing
        val correctedFirstState = firstState.copy(timestamp = startOfDay(secondState.timestamp))
        // We append a state for 23:59:59 at the end to be sure we capture the whole day
        val extraEndState = sortedSystemStates.last.copy(timestamp = endOfDay(secondState.timestamp))
        (correctedFirstState :: secondState :: xs) :+ extraEndState
      case other ⇒ other // no reset required
    }
  }

  private def startOfDay(day: Long): Long = {
    resetTimeOfDayTo(day, 0, 0, 0)
  }

  // TODO Should be moved to some utility package or implicit predef
  private def endOfDay(day: Long): Long = {
    resetTimeOfDayTo(day, 23, 59, 59)
  }

  // TODO Should be moved to some utility package or implicit predef
  private def resetTimeOfDayTo(day: Long, hours: Int, minutes: Int, seconds: Int): Long = {
    val date: Calendar = Calendar.getInstance()
    date.setTimeInMillis(day)
    // reset hour, minutes, seconds and millis (== beginning of day)
    date.set(Calendar.HOUR_OF_DAY, hours)
    date.set(Calendar.MINUTE, minutes)
    date.set(Calendar.SECOND, seconds)
    date.set(Calendar.MILLISECOND, 0)
    date.getTimeInMillis
  }

  override def avtRatio: BigDecimal = {

    val ratios = StateRatio(periods, systemStates)

    val onRatio: Long = ratios.filter(
      s ⇒
        s.state == ZkState.enforceOn ||
          s.state == ZkState.standBy ||
          s.state == ZkState.enforceOff)
      .map(_.timePeriods)
      .sum

    val totalTime: Long = {
      if (periods.length > 0)
        periods.map(sp ⇒ sp.to - sp.from).sum
      else 0
    }

    val degradedRatio: Long = ratios
      .find(_.state == ZkState.enforceDegraded)
      .map(_.timePeriods)
      .getOrElse(0l)

    Ratio.divide((onRatio + (degradedRatio * failureRatio.toFloat)), totalTime)
  }
}
*/ 