package csc.sectioncontrol.reports.export

import scala.math.max

import csc.sectioncontrol.reports.metrics.Period
import csc.sectioncontrol.storage.ZkState
import csc.sectioncontrol.messages.StatsDuration
import scala.annotation.tailrec
import org.slf4j.LoggerFactory
import csc.sectioncontrol.reports.i18n.Messages

/**
 * Copyright (C) 2016 CSC. <http://www.csc.com>
 *
 * Created on 5/10/16.
 */

object EnforceLog {

  import ZkState._
  val systemUser = "System"
  private val log = LoggerFactory.getLogger(this.getClass)

  def createEnforceLogStates(duration: StatsDuration, inSchedulePeriods: Seq[Period], inCorridorStates: Seq[ZkState]): Seq[ZkState] = {

    def removeDoubleStates(states: Seq[ZkState], schedulePeriods: Seq[Period]): Seq[ZkState] = {
      @tailrec
      def remove(accu: Seq[ZkState], states: Seq[ZkState]): Seq[ZkState] = {
        states match {
          case h :: tail ⇒
            val newTail = tail.dropWhile { s ⇒ s.state == h.state && !isScheduleEdge(s.timestamp, schedulePeriods) }
            remove(h +: accu, newTail)
          case Nil ⇒ accu.reverse
        }
      }

      remove(Nil, states)
    }
    log.info("SchedulePeriods: {}", inSchedulePeriods)
    log.info("inCorridorStates: {}", inCorridorStates)
    val collatedPeriods = collatePeriods(inSchedulePeriods)
    // transform schedule periods to sequence of enforceOn / enforceOff / standby / ... states
    val scheduleStates = transformPeriodsToStates(duration, collatedPeriods)
    log.info("ScheduleStates: {}", scheduleStates)
    // Sort given corridorStates according to timestamp ascending and truncate timestamp to seconds
    val corridorStates = inCorridorStates.sortWith((s1, s2) ⇒ s1.timestamp < s2.timestamp).map {
      s ⇒ s.copy(timestamp = (s.timestamp / 1000) * 1000)
    }

    val significantMoments = (scheduleStates ++ corridorStates).map {
      s ⇒ s.timestamp
    }.distinct.filter(_ >= duration.start).sortWith((a, b) ⇒ a < b)

    val statesInTime: Seq[ZkState] = significantMoments.map((time) ⇒ {
      val scheduleState = stateAtMoment(scheduleStates, time)
      val corridorState = stateAtMoment(corridorStates, time)
      val newState: String = combineStates(scheduleState.state, corridorState.state)
      if (corridorState.timestamp == time)
        ZkState(newState, time, corridorState.userId, corridorState.reason)
      else
        ZkState(newState, time, scheduleState.userId, scheduleState.reason)
    })

    val states = removeDoubleStates(statesInTime, collatedPeriods)
    val lastState = states.last
    val result = if (lastState.timestamp != duration.tomorrow.start)
      states :+ lastState.copy(timestamp = duration.tomorrow.start, reason = Messages("csc.sectioncontrol.reports.no_end_of_schedule_event"))
    else
      states
    log.info("Final states: {}", result)
    result
  }

  private[export] def isScheduleEdge(timestamp: Long, schedules: Seq[Period]): Boolean = {
    schedules.exists { s ⇒ s.from == timestamp || s.to == timestamp }
  }

  private[export] def combineStates(scheduleState: String, corridorState: String): String = {
    (scheduleState, corridorState) match {
      case (ZkState.enforceOn, _)                    ⇒ corridorState
      case (ZkState.enforceOff, ZkState.off)         ⇒ ZkState.off
      case (ZkState.enforceOff, ZkState.Maintenance) ⇒ ZkState.Maintenance
      case (ZkState.enforceOff, ZkState.standBy)     ⇒ ZkState.standBy
      case (ZkState.enforceOff, ZkState.failure)     ⇒ ZkState.failure
      case (ZkState.enforceOff, _)                   ⇒ ZkState.enforceOff
    }
  }

  private[export] def stateAtMoment(states: Seq[ZkState], moment: Long): ZkState = {
    states.takeWhile((s) ⇒ s.timestamp <= moment).last
  }
  /**
   * Combine coinciding periods in the given sequence of periods
   * @param periods a sequence of possibly overlapping periods (can be empty).
   * @return the collated sequence of periods
   */
  // TODO: use Period#coalesce
  private[export] def collatePeriods(periods: Seq[Period]): Seq[Period] = {

    @tailrec
    def go(accu: Seq[Period], previous: Period, periodsToGo: Seq[Period]): Seq[Period] = {
      if (periodsToGo == Nil) {
        (previous +: accu).reverse
      } else {
        val (h :: t) = periodsToGo
        val (newAccu, newPrevious) = if (h.from <= previous.to) {
          val merged = previous.copy(to = max(h.to, previous.to))
          (accu, merged)
        } else {
          (previous +: accu, h)
        }
        go(newAccu, newPrevious, t)
      }
    }

    // Nil or a single period do not coincide
    if (periods.size < 2)
      periods
    else {
      val sortedPeriods = periods.sortWith((p1, p2) ⇒ p1.from < p2.from)
      val (h :: t) = sortedPeriods
      go(Nil, h, t)
    }
  }

  /**
   * transform a sequence of schedule periods into a sequence of enforceOn/enforceOff
   * states. If the first schedulePeriod isn't at the beginning of the day, prepend
   * a enforceOff state to ensure the scheduleStates cover the statsDuration.
   * @param duration the day and the timeZone. Used to determine start and end time
   *                 of the day.
   * @param schedulePeriods the (possibly empty) sequence of schedule periods.
   * @return
   */
  private[export] def transformPeriodsToStates(duration: StatsDuration, schedulePeriods: Seq[Period]): Seq[ZkState] = {
    // Add extra enforceOff state if no periods available or if first period doesn't start at
    // the beginning of the day.
    log.info("Collated periods: {}", schedulePeriods)
    val accu: Seq[ZkState] =
      if (schedulePeriods == Nil || schedulePeriods(0).from != duration.start)
        Seq(ZkState(enforceOff, duration.start, systemUser, Messages("csc.sectioncontrol.reports.no_start_of_schedule_event")))
      else
        Nil

    schedulePeriods.foldLeft(accu) {
      (accu, period) ⇒
        {
          val newAcc1 = ZkState(enforceOn, period.from, systemUser, Messages("csc.sectioncontrol.reports.start_schedule")) +: accu
          // skip 
          val newAcc2 = if (period.to != duration.tomorrow.start)
            ZkState(enforceOff, period.to, systemUser, Messages("csc.sectioncontrol.reports.end_schedule")) +: newAcc1
          else
            newAcc1
          newAcc2
        }
    }.reverse
  }
}
