/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.reports.export

import xml.Elem
import csc.sectioncontrol.messages.StatsDuration
import csc.sectioncontrol.storage.DayReportVersion
import csc.sectioncontrol.reports.metrics.red.RedMetrics

object ExportRedMetrics {
  def createRedMetricsXml(key: StatsDuration, metrics: RedMetrics, settings: DayReportBuilderV40Settings): Elem = {
    <roodlicht id={ metrics.id }>
      {
        if (metrics.hasStatistics) {
          <RLC-statistiek>
            <passages totaal={ metrics.total.toString }>
              { for (lane ← metrics.lanesV6) yield <strook locatie={ lane._1 }>{ lane._2.toString }</strook> }
            </passages>
            <roodtijden>
              <gemiddelde eenheid="seconden">{ metrics.averageRed.toString }</gemiddelde>
              <max eenheid="seconden">{ metrics.maxRed.toString }</max>
            </roodtijden>
            <overtredingen>
              <overtredingen-totaal>{ metrics.violations.totalViolations.toString }</overtredingen-totaal>
              <hand>{ metrics.violations.manual }</hand>
              <auto>{ metrics.violations.auto }</auto>
              <mobi-niet-verwerken>{ metrics.violations.mobiDoNotProcess }</mobi-niet-verwerken>
            </overtredingen>
            <performance>
              <overtredingenratio>{ settings.round(metrics.performance.violationRatio).toString }</overtredingenratio>
              <autoratio>{ settings.round(metrics.performance.autoRatio).toString }</autoratio>
              {
                if (settings.version == DayReportVersion.HHM_V40 || settings.version == DayReportVersion.HHM_V42 ||
                  settings.version == DayReportVersion.HHM_V421) {
                  <autoratio-netto>{ settings.round(metrics.performance.autoRatioNetto).toString }</autoratio-netto>
                  <handhaafratio>{ settings.round(metrics.performance.enforceRatio).toString }</handhaafratio>
                  <storingenratio>{ settings.round(metrics.performance.failureRatio).toString() }</storingenratio>
                  <av-t-ratio>{ settings.round(metrics.performance.avtRatio).toString() }</av-t-ratio>
                  <avl-f-ratio>{ metrics.performance.avlf }</avl-f-ratio>
                } else {
                  <handhaafratio>{ settings.round(metrics.performance.enforceRatio).toString }</handhaafratio>
                }
              }
            </performance>
          </RLC-statistiek>
        } else {
          <RLC-statistiek/>
        }
      }
      <RLC-instellingen>
        {
          if (settings.version == DayReportVersion.HHM_V60) {
            <min_roodtijd eenheid="seconden">{ metrics.redPardon }</min_roodtijd>
          } else {
            <rood_pardontijd eenheid="seconden">{ metrics.redPardon }</rood_pardontijd>
          }
        }
        <drempelsnelheid eenheid="kmpuur">{ metrics.speedLimit }</drempelsnelheid>
        <min_geeltijd eenheid="seconden">{ metrics.yellowTime }</min_geeltijd>
      </RLC-instellingen>
      <passagepunt stroken={ metrics.lanes.size.toString() }/>
      { settings.enforceLogFiltered(key, metrics.periods, metrics.systemStates) }
    </roodlicht>
  }

  def createRedMetricsXmlV60(key: StatsDuration, metrics: RedMetrics, settings: DayReportBuilderV40Settings): Elem = {
    <roodlicht id={ metrics.idV6 }>
      {
        if (metrics.hasStatistics) {
          <RLC-statistiek>
            <passages totaal={ metrics.total.toString }>
              { for (lane ← metrics.lanesV6) yield <strook locatie={ lane._1 }>{ lane._2.toString }</strook> }
            </passages>
            <roodtijden>
              <gemiddelde eenheid="seconden">{ metrics.averageRed.toString }</gemiddelde>
              <max eenheid="seconden">{ metrics.maxRed.toString }</max>
            </roodtijden>
            <overtredingen>
              <overtredingen-totaal>{ metrics.violations.totalViolations.toString }</overtredingen-totaal>
              <hand>{ metrics.violations.manual }</hand>
              <auto>{ metrics.violations.auto }</auto>
              <mobi-niet-verwerken>{ metrics.violations.mobiDoNotProcess }</mobi-niet-verwerken>
            </overtredingen>
            <performance>
              <overtredingenratio>{ settings.round(metrics.performance.violationRatio).toString }</overtredingenratio>
              <autoratio>{ settings.round(metrics.performance.autoRatioV60).toString }</autoratio>
              {
                if (settings.version == DayReportVersion.HHM_V40 || settings.version == DayReportVersion.HHM_V42 ||
                  settings.version == DayReportVersion.HHM_V421) {
                  <autoratio-netto>{ settings.round(metrics.performance.autoRatioNetto).toString }</autoratio-netto>
                  <handhaafratio>{ settings.round(metrics.performance.enforceRatio).toString }</handhaafratio>
                  <storingenratio>{ settings.round(metrics.performance.failureRatio).toString() }</storingenratio>
                  <av-t-ratio>{ settings.round(metrics.performance.avtRatio).toString() }</av-t-ratio>
                  <avl-f-ratio>{ metrics.performance.avlf }</avl-f-ratio>
                } else {
                  <handhaafratio>{ settings.round(metrics.performance.enforceRatio).toString }</handhaafratio>
                }
              }
            </performance>
          </RLC-statistiek>
        } else {
          <RLC-statistiek/>
        }
      }
      <RLC-instellingen>
        {
          if (settings.version == DayReportVersion.HHM_V60) {
            <min_roodtijd eenheid="seconden">{ metrics.redPardon }</min_roodtijd>
          } else {
            <rood_pardontijd eenheid="seconden">{ metrics.redPardon }</rood_pardontijd>
          }
        }
        <drempelsnelheid eenheid="kmpuur">{ metrics.speedLimit }</drempelsnelheid>
        <min_geeltijd eenheid="seconden">{ metrics.yellowTime }</min_geeltijd>
      </RLC-instellingen>
      <passagepunt stroken={ metrics.lanes.size.toString() }/>
      { settings.enforceLogFiltered(key, metrics.periods, metrics.systemStates) }
    </roodlicht>
  }

}