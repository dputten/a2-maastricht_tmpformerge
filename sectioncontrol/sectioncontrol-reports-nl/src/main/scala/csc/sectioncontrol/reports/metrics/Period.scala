/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */
package csc.sectioncontrol.reports.metrics

import java.text.SimpleDateFormat
import java.util.Date

object Extensions {
  implicit def stringWrapper(value: String) = new StringExtensions(value)

  class StringExtensions(val value: String) {
    def toDate(implicit formatter: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")): Date = {
      formatter.parse(value)
    }
  }
}

case class Period(from: Long, to: Long) {
  require(from <= to, "'from' must be at or before 'to'")
  override def toString: String = "[%s (%s) - %s (%s)]".format(new Date(from), from, new Date(to), to)
}

object Period {
  import Extensions._
  import scala.math.{ min, max }

  def apply(from: String, to: Option[String] = None): Period =
    Period(from.toDate.getTime, to.map(t ⇒ t.toDate.getTime).getOrElse(from.toDate.getTime))

  def apply(from: String, to: String): Period =
    Period(from.toDate.getTime, to.toDate.getTime)

  def coalesce(periods: Seq[Period]): Seq[Period] = {
    def scan(p: Period, ps: Seq[Period]): (Period, Seq[Period]) = {
      ps match {
        // merge h and p into a single period, continue scan
        case h :: tail if h.from <= p.to ⇒ scan(p.copy(to = h.to max p.to), tail)
        // end of scan becasue h and p do not overlap
        case h :: tail                   ⇒ (p, ps)
        // end of scan because list exhausted
        case Nil                         ⇒ (p, ps)
      }
    }
    def doCoalesce(accu: Seq[Period], ps: Seq[Period]): Seq[Period] = {
      ps match {
        case h :: tail ⇒
          val (p, rest) = scan(h, tail)
          doCoalesce(p +: accu, rest)
        case Nil ⇒ accu.reverse
      }
    }
    doCoalesce(Nil, periods)
  }

}
