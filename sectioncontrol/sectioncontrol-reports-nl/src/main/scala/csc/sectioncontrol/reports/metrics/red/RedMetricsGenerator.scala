package csc.sectioncontrol.reports.metrics.red

import csc.sectioncontrol.messages._
import csc.sectioncontrol.storage._
import csc.sectioncontrol.reports.metrics.Period
import csc.sectioncontrol.enforce.ExportViolationsStatistics
import org.slf4j.LoggerFactory
import csc.sectioncontrol.reports.CorridorData
import csc.sectioncontrol.messages.RedLightCorridorStatistics
import csc.sectioncontrol.storage.RedLightStatistics
import csc.sectioncontrol.messages.Lane
import csc.sectioncontrol.storage.ZkSystem
import csc.sectioncontrol.storage.LaneInfo
import csc.sectioncontrol.reports.schedules.CorridorSpeedSchedule

object RedMetricsGenerator {

  /**
   * Create the metrics for the RedLight services
   * @param system
   * @param corridors
   * @param corridorStatistics
   * @param periods
   * @param corridorStates
   * @param corridorAlerts
   * @param schedules
   * @return
   */
  def generateMetrics(system: ZkSystem,
                      corridors: Seq[CorridorData],
                      corridorStatistics: Seq[CorridorStatistics],
                      periods: Map[Int, Seq[Period]],
                      corridorStates: Map[Int, Seq[ZkState]],
                      corridorAlerts: Map[Int, Seq[ZkAlertHistory]],
                      schedules: Map[Int, Seq[CorridorSpeedSchedule]]): Map[CorridorKey, RedMetrics] = {

    corridors.map(corridorData ⇒ {
      val corridorId = corridorData.corridor.info.corridorId
      val corridorStats = corridorStatistics.find(_.corridorId == corridorData.corridor.info.corridorId)
      val corridorPeriods: Seq[Period] = periods.get(corridorId).getOrElse(Nil)
      val corridorSpeedLimits = schedules
        .get(corridorId)
        .getOrElse(Nil)
        .map(_.speedLimit)
      val corState = corridorStates.get(corridorId).getOrElse(Seq())

      val alerts = corridorAlerts.get(corridorId).getOrElse(Seq())

      CorridorIdKey(corridorId) -> new RedMetrics(system, corridorData, corridorStats, corridorPeriods, corState, alerts, corridorSpeedLimits)
    }).toMap
  }

  /**
   * calculate the day report data
   */
  def calculate(systemId: String, stats: StatsDuration, corridorData: Seq[ExportViolationsStatistics], serviceResults: Map[Int, Seq[ServiceStatistics]]): Seq[CorridorStatistics] = {
    // TODO: Refactor. See TCMetricsGenerator.scala
    val log = LoggerFactory.getLogger("RedMetricsGenerator")
    log.info("Calculate the Day Report Red-statistics for system  %s day %s".
      format(systemId, stats))
    //calculate the day report data
    corridorData.map(export ⇒ {
      val corridor: Int = export.corridorId
      val matcherEvents = serviceResults.get(corridor).getOrElse(Seq()).collect { case stat: RedLightStatistics ⇒ stat }
      matcherEvents match {
        case Nil ⇒
          log.error("No matcher events found for corridor: %s(%s) at %s".format(corridor, systemId, stats))
          RedLightCorridorStatistics(corridor, Nil, export, 0, 0, 0.0, 0)
        case head :: rest ⇒
          val zero = RedLightCorridorStatistics(corridor, head.laneStats, export, head.nrRegistrations, head.nrWithRedLight,
            head.averageRedLight, head.highestRedLight)
          val corridorStats = rest.foldLeft[RedLightCorridorStatistics](zero) {
            case (accu, next) ⇒ {
              val newAverageSpeed = (accu.nrWithRedLight + next.nrWithRedLight) match {
                case 0       ⇒ 0
                case nonZero ⇒ (accu.nrWithRedLight * accu.averageRedLight + next.nrWithRedLight * next.averageRedLight) / nonZero
              }
              //create new Lane statistics. For this we have to create maps, merge them and restore back the list
              val aMap: Map[Lane, Int] = accu.laneStats.map(info ⇒ (info.lane -> info.count)).toMap
              val bMap: Map[Lane, Int] = next.laneStats.map(info ⇒ (info.lane -> info.count)).toMap
              val newLaneKeys: Set[Lane] = aMap.keySet ++ bMap.keySet
              val newLaneStats = newLaneKeys.map {
                case lane: Lane ⇒
                  val countA: Int = aMap.getOrElse(lane, 0)
                  val countB: Int = bMap.getOrElse(lane, 0)
                  LaneInfo(lane, countA + countB)
              }.toList
              val result = RedLightCorridorStatistics(corridor, newLaneStats, export, accu.nrRegistrations + next.nrRegistrations,
                accu.nrWithRedLight + next.nrWithRedLight,
                newAverageSpeed, math.max(accu.highestRedLight, next.highestRedLight))
              // Return value
              result
            }
          }
          corridorStats
      }
    })
  }

}
