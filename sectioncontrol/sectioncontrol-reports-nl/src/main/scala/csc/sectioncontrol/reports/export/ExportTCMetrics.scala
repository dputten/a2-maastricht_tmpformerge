/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.reports.export

import xml.Elem
import csc.sectioncontrol.reports.metrics.tc.TCMetrics
import csc.sectioncontrol.messages.StatsDuration
import csc.sectioncontrol.storage.{ ZkState, DayReportVersion }

object ExportTCMetrics {
  // TODO: Check if use of vehiclesPerLaneIn and vehiclesPerLaneOut in <portaal> element is correct.
  def createTCMetricsXml(key: StatsDuration, metrics: TCMetrics, settings: DayReportBuilderV40Settings): Elem = {
    <HH-traject id={ metrics.reportId } modus={ metrics.modus } ht={ metrics.reportHtId }>
      <instellingen>
        {
          <snelheidslimiet eenheid="kmpuur">{ metrics.speedLimit }</snelheidslimiet>
          <ESA-scantijd eenheid="seconden">{ metrics.esaScanTime_secs }</ESA-scantijd>
          <ESA-pardontijd eenheid="seconden">{ metrics.esaPardonTime_secs }</ESA-pardontijd>
        }
      </instellingen>
      <portalen>
        <portaal type="BEGIN" stroken={ metrics.vehiclesPerLaneIn.size.toString }>{ metrics.beginPortal }</portaal>
        <portaal type="EINDE" stroken={ metrics.vehiclesPerLaneOut.size.toString }>{ metrics.endPortal }</portaal>
      </portalen>
      {
        <TC-statistiek>
          <passages totaal-in={ metrics.vehiclesPerLaneIn.values.sum.toString } totaal-uit={ metrics.vehiclesPerLaneOut.values.sum.toString }>
            { for ((laneName, count) ← metrics.vehiclesPerLaneIn) yield <strook passage="IN" locatie={ laneName }>{ count }</strook> }
            { for ((laneName, count) ← metrics.vehiclesPerLaneOut) yield <strook passage="UIT" locatie={ laneName }>{ count }</strook> }
          </passages>
          <snelheden>
            {
              if (settings.version == DayReportVersion.HHM_V40) {
                <gemiddeld eenheid="kmpuur">{ settings.round(metrics.averageSpeed, 2) }</gemiddeld>
              } else {
                <gemiddeld eenheid="kmpuur">{ settings.round(metrics.averageSpeed, 1) }</gemiddeld>
              }
            }
            <max eenheid="kmpuur">{ metrics.maxSpeed.toString() }</max>
          </snelheden>
          <matches>{ metrics.matches.toString }</matches>
          <overtredingen>
            <overtredingen-totaal>{ metrics.violations.totalViolations }</overtredingen-totaal>
            <hand>{ metrics.violations.manual }</hand>
            <auto>{ metrics.violations.auto }</auto>
            <mobi>{ metrics.violations.mobi }</mobi>
            <hand-conform-specificatie>{ metrics.violations.manualAccordingToSpec }</hand-conform-specificatie>
            <mobi-niet-verwerken>{ metrics.violations.mobiDoNotProcess }</mobi-niet-verwerken>
            <MTMratio>{ settings.round(metrics.violations.mtmRatio) }</MTMratio>
            <MTM-pardon>{ metrics.violations.mtmPardon }</MTM-pardon>
            <Dubbele-overtredingen-pardon>{ metrics.violations.doublePardon }</Dubbele-overtredingen-pardon>
            <Overig-pardon>{ metrics.violations.otherPardon }</Overig-pardon>
          </overtredingen>
          <performance>
            <matchratio>{ if (metrics.hasStatistics) settings.round(metrics.performance.matchRatio) else "1" }</matchratio>
            {
              if (settings.version == DayReportVersion.HHM_V40) {
                <overtredingenratio>{ settings.round(metrics.performance.violationRatio) }</overtredingenratio>
              } else {
                <overtredingenratio>{ settings.round(metrics.performance.violationRatioMatches) }</overtredingenratio>
              }
            }{
              if (metrics.hasStatistics) {
                if (metrics.reportPerformance) {
                  <autoratio>{ settings.round(metrics.performance.autoRatio) }</autoratio>
                  <autoratio-netto>{ settings.round(metrics.performance.autoRatioNetto) }</autoratio-netto>
                  <handhaafratio>{ settings.round(metrics.performance.enforceRatio) }</handhaafratio>
                  <storingenratio>{ settings.round(metrics.performance.failureRatio) }</storingenratio>
                  <av-t-ratio>{ settings.round(metrics.performance.avtRatio) }</av-t-ratio>
                  <avl-f-ratio>{ metrics.performance.avlf }</avl-f-ratio>
                } else {
                  <autoratio>{ 0 }</autoratio>
                  <autoratio-netto>{ 0 }</autoratio-netto>
                  <handhaafratio>{ 0 }</handhaafratio>
                  <storingenratio>{ 0 }</storingenratio>
                  <av-t-ratio>{ 0 }</av-t-ratio>
                  <avl-f-ratio>{ false }</avl-f-ratio>
                }
              } else {
                <autoratio>{ 1 }</autoratio>
                <autoratio-netto>{ 1 }</autoratio-netto>
                <handhaafratio>{ settings.round(metrics.performance.enforceRatio) }</handhaafratio>
                <storingenratio>{ settings.round(metrics.performance.failureRatio) }</storingenratio>
                <av-t-ratio>{ settings.round(metrics.performance.avtRatio) }</av-t-ratio>
                <avl-f-ratio>{ metrics.performance.avlf }</avl-f-ratio>
              }
            }
          </performance>
        </TC-statistiek>
      }
      {
        val states = EnforceLog.createEnforceLogStates(key, metrics.schedulePeriods, metrics.systemStates)
        settings.produceEnforceLog(key, states)
      }
    </HH-traject>

  }

}