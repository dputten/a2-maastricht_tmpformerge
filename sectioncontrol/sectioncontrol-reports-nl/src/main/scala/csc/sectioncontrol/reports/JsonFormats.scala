package csc.sectioncontrol.reports

import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.messages.{ VehicleImageType, EsaProviderType, VehicleCode }
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storagelayer.ServiceConfig
import net.liftweb.json._

/**
 * Created by carlos on 09.10.15.
 */
object JsonFormats {

  val formats = DefaultFormats + new EnumerationSerializer(
    ZkWheelbaseType,
    VehicleCode,
    ZkIndicationType,
    ServiceType,
    ZkCaseFileType,
    EsaProviderType,
    VehicleImageType,
    DayReportVersion,
    ConfigType) + FullTypeHints(List(classOf[ServiceConfig], classOf[ServiceStatistics]))

}

object MapSerializer extends Serializer[Map[Any, Any]] {

  def serialize(implicit format: Formats): PartialFunction[Any, JValue] = {
    case m: Map[_, _] ⇒ mapValue(m)
  }

  def mapValue(m: Map[_, _])(implicit format: Formats): JValue = {
    JObject(m.map({
      case (k, v) ⇒ JField(
        k match {
          case ks: String ⇒ ks
          case ks: Symbol ⇒ ks.name
          case ks: Any    ⇒ ks.toString
        },
        Extraction.decompose(v))
    }).toList)
  }

  override def deserialize(implicit format: Formats): PartialFunction[(TypeInfo, JValue), Map[Any, Any]] = Map.empty

}
