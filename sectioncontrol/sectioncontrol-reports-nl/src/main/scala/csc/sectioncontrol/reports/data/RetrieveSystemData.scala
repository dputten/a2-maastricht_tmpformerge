/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.reports.data

import akka.event.Logging
import akka.actor.ActorSystem

import csc.curator.utils.Curator
import csc.config.Path

import csc.sectioncontrol.messages.StatsDuration
import csc.sectioncontrol.reports.CorridorData
import csc.sectioncontrol.reports.SectionData
import csc.sectioncontrol.reports.LaneData
import csc.sectioncontrol.reports.SystemData
import csc.sectioncontrol.reports.GantryData
import csc.sectioncontrol.reports.metrics.Period
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storagelayer.SystemConfiguration
import csc.sectioncontrol.storagelayer.excludelog.{ Exclusions, ExcludeLogLayer }
import csc.sectioncontrol.storagelayer.corridor.{ CorridorServiceTrait, CorridorIdMapping }

class RetrieveSystemData(systemId: String, curator: Curator, system: ActorSystem, corridorService: CorridorServiceTrait) {
  val log = Logging(system, classOf[RetrieveSystemData])

  // The system configuration for the system; corridor info, system type (tc, redlight), speed information, ...
  private val systemCfg = new SystemConfiguration(curator)

  // The interface to the exclude log information
  private val excludeLog = new ExcludeLogLayer(curator)

  def getSystemData: SystemData = {
    val systemInfo: ZkSystem = curator.get[ZkSystem](Path(Paths.Systems.getConfigPath(systemId))) match {
      case None ⇒ {
        val error = "Cannot generate the report for %s. No config found under %s".format(systemId, Paths.Systems.getConfigPath(systemId))
        log.error(error)
        throw new RuntimeException(error) //fail to create the report
      }
      case Some(info) ⇒ info
    }
    val corridorList = systemCfg.getAllCorridors(systemId)
    val corridorData = corridorList.map(corrCfg ⇒ {
      val startSectionId: String = corrCfg.startSectionId
      val endSectionId: String = corrCfg.endSectionId
      CorridorData(corrCfg, getSectionData(startSectionId), getSectionData(endSectionId),
        corridorService.getDailyReportConfig(systemId, corrCfg.id).getOrElse(null))
    })
    SystemData(systemInfo, corridorData.toList)
  }

  /**
   * Get Section data from ZooKeeper
   */
  def getSectionData(sectionId: String): SectionData = {
    val sectionPath = Path(Paths.Sections.getConfigPath(systemId, sectionId))
    val section: ZkSection = curator.get[ZkSection](sectionPath) match {
      case None ⇒ {
        val error = "Cannot generate the report for %s. No section config found under %s".format(systemId, sectionPath)
        log.error(error)
        throw new RuntimeException(error) //fail to create the report
      }
      case Some(section) ⇒ section
    }
    //get gantries
    val gantriesPath = Path(Paths.Gantries.getDefaultPath(systemId))
    val zkGantries: Seq[Map[String, String]] = curator.getChildren(gantriesPath).map(path ⇒ {
      val configPath = Path(Paths.Gantries.getConfigPath(systemId, path.nodes.last.name))
      curator.get[Map[String, String]](configPath) match {
        case None ⇒ {
          val error = "Cannot generate the report for %s. No gantry config found under %s".format(systemId, configPath)
          log.error(error)
          throw new RuntimeException(error) //fail to create the report
        }
        case Some(gantry) ⇒ gantry
      }
    })
    val start = zkGantries.find(config ⇒ section.startGantryId == config("id"))
    val end = zkGantries.find(config ⇒ section.endGantryId == config("id"))
    val startGantry = getGantryData("start", start, gantriesPath)
    val endGantry = getGantryData("end", end, gantriesPath)
    SectionData(section, startGantry, endGantry)
  }

  /**
   * Get Gantry data from ZooKeeper
   * @param label mark for logging
   * @param gantryMap gantry data
   */
  def getGantryData(label: String, gantryMap: Option[Map[String, String]], path: Path): GantryData = {
    val gantry = gantryMap match {
      case Some(s) ⇒ s
      case _ ⇒ {
        val error = "Cannot generate the report for %s. No matching %s gantry found under %s".format(systemId, label, path)
        log.error(error)
        throw new RuntimeException(error) //fail to create the report
      }
    }
    //
    val lanesPath = Path(Paths.Gantries.getLanesPath(systemId, gantry("id")))
    val lanes: Seq[LaneData] = curator.getChildren(lanesPath).map(path ⇒ {
      val lanePath = Path(Paths.Gantries.getLanesConfigPath(systemId, gantry("id"), path.nodes.last.name))
      curator.get[LaneData](lanePath) match {
        case None ⇒ {
          val error = "Cannot generate the report for %s. No %s lane found under %s.".format(systemId, label, lanePath)
          log.error(error)
          throw new RuntimeException(error) //fail to create the report
        }
        case Some(laneMap) ⇒ laneMap
      }
    })
    GantryData(gantry("id"), gantry("name"), gantry("hectometer"), lanes)
  }

  /**
   * Get the calibration failure periods
   * @param stats the dusration
   * @return the selftest failures periods
   */
  def getCalibrationFailurePeriods(stats: StatsDuration): Map[CorridorIdMapping, Seq[Period]] = {
    val corridorIds = corridorService.getCorridorIdMapping(systemId)

    corridorIds.map(ids ⇒ {
      val events = excludeLog.readEvents(systemId, ids.infoId)

      //only use selftest events
      val selfEvents = events.filter(_.componentId == "SelfTest")

      val exclusions = Exclusions(0, selfEvents)
      val periods = exclusions.exclusions.foldLeft(Seq[Period]()) {
        (list, excl) ⇒
          if (excl.componentId == "SelfTest" && excl.to >= stats.start) {
            list :+ new Period(from = excl.from, to = excl.to)
          } else {
            list
          }
      }
      ids -> periods
    }).toMap
  }

}