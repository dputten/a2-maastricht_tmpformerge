/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.reports.metrics.common

trait Ratios {

  def divide(dividend: BigDecimal, divisor: BigDecimal): BigDecimal = {
    //making sure the dividing by zero is not possible
    val dvisor = divisor match {
      case x if (x <= 0) ⇒ BigDecimal(1)
      case y             ⇒ y
    }
    (dividend / dvisor)
  }
  def makeRatio(ratio: BigDecimal): BigDecimal = {
    ratio match {
      case x if (x <= 0) ⇒ BigDecimal(0)
      case r if (r > 1)  ⇒ BigDecimal(1)
      case y             ⇒ y
    }
  }

}