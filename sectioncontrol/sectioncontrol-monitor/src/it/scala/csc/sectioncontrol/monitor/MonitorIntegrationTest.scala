/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.monitor

import java.util.concurrent.{CountDownLatch, TimeUnit}

import akka.util.Duration
import akka.util.duration._
import com.typesafe.config.{Config, ConfigFactory}
import csc.akkautils.DirectLogging
import csc.config.Path
import csc.curator.utils.{Curator, CuratorToolsImpl}
import csc.dlog.SimpleDistributedQueueTaker
import csc.hbase.utils.testframework.HBaseTestFramework
import csc.json.lift.LiftSerialization
import csc.sectioncontrol.storage.{ZkCorridor, ZkCorridorInfo, ZkDirection, ZkUser, _}
import csc.sectioncontrol.storagelayer.state.StateService
import csc.sectioncontrol.storagelayer.{RedLightConfig, SpeedFixedConfig}
import net.liftweb.json.DefaultFormats
import org.apache.curator.framework.CuratorFrameworkFactory
import org.apache.curator.retry.RetryNTimes
import org.scalatest.matchers.MustMatchers
import org.scalatest.{BeforeAndAfterEach, WordSpec}

/**
 * Tests if the Boot of Monitor starts up in Standby according to the state transition model.
 * Starts in Off, switches to StandBy. (Req 7)
 * @author Raymond Roestenburg
 */

class MonitorIntegrationTest extends WordSpec with MustMatchers with HBaseTestFramework with BeforeAndAfterEach
  with DirectLogging {
  val startupLatch = new CountDownLatch(1)
  val shutdownLatch = new CountDownLatch(1)

  "The Boot" must {
    "boot correctly " in {
      //boot moved into the test because zookeeper isn't started yet
      val boot = new Boot() {
        override def loadConfiguration(configFileName: String): Option[Config] = {
          val configFallback = super.loadConfiguration(configFileName)
          val config = ConfigFactory.parseString("sectioncontrol.zkServers=\"%s\"\nsectioncontrol.hbaseZkServers=\"%s\"".format(zkConnectionString, zkConnectionString))
          val result = configFallback.map(cfg ⇒ config.withFallback(cfg))
          Some(result.getOrElse(config))
        }

        override def startupActors() {
          super.startupActors()
          startupLatch.countDown()
        }

        override def shutdown() {
          super.shutdown()
          shutdownLatch.countDown()
        }
      }

      boot.startup()
      startupLatch.await(60, TimeUnit.SECONDS)

      zkStore.curator.foreach {
        client ⇒
          // read the states history
          val qTaker = new SimpleDistributedQueueTaker[ZkState](client, Paths.State.getHistoryPath("a2", "corridor1"), new LiftSerialization {
            implicit def formats = DefaultFormats
          })

          qTaker.start()
          val stateHistory = getStateHistory(qTaker)
          stateHistory.size must be(2)
          stateHistory.count(state ⇒ state.state == "Off") must be(1)
          stateHistory.count(state ⇒ state.state == "StandBy") must be(1)
          StateService.get("a2", "corridor1", zkStore).get.state must be("StandBy")
          qTaker.close()

          boot.shutdown()
          shutdownLatch.await(60, TimeUnit.SECONDS)
      }
    }
  }

  private def takeState(qTaker: SimpleDistributedQueueTaker[ZkState], timeout: Duration): Option[ZkState] = {
    var state: Option[ZkState] = None
    val endTime = System.currentTimeMillis() + timeout.toMillis
    while (System.currentTimeMillis() < endTime && state.isEmpty) {
      Thread.sleep(100)
      state = qTaker.take()
    }
    state
  }

  private def getStateHistory(qTaker: SimpleDistributedQueueTaker[ZkState]): Seq[ZkState] = {
    def doGetStateHistory(accu: Seq[ZkState], elem: Option[ZkState], qTaker: SimpleDistributedQueueTaker[ZkState]): Seq[ZkState] = {
      elem match {
        case None ⇒ accu
        case Some(state) ⇒ {
          doGetStateHistory(state +: accu, takeState(qTaker, 10.seconds), qTaker)
        }
      }
    }
    doGetStateHistory(Seq[ZkState](), takeState(qTaker, 10.seconds), qTaker).sortWith((s1, s2) ⇒ s1.timestamp < s2.timestamp)
  }

  private var zkStore: Curator = _

  override protected def afterEach() {
    super.afterEach()
    zkStore.safeDelete(Paths.State.getCurrentPath("a2", "corridor1"))
    zkStore.safeDelete(Paths.State.getHistoryPath("a2", "corridor1"))
  }

  override protected def beforeAll() {
    super.beforeAll()
    val clientScope = Some(CuratorFrameworkFactory.newClient(zkConnectionString, new RetryNTimes(1, 1000)))
    clientScope.foreach(_.start())
    zkStore = new CuratorToolsImpl(clientScope, log)
    zkStore.createEmptyPath(Path("/ctes") / "systems" / "a2")
    zkStore.createEmptyPath(Path("/ctes") / "users")
    zkStore.createEmptyPath(Path("/ctes") / "configuration")
    zkStore.put(Path("/ctes") / "users" / "testUser", new ZkUser("testUser", "testReportingOfficer", "", "", "", "", "", "", "", List()))

    //create 2 corridors
    val config1Path = Path(Paths.Corridors.getConfigPath("a2", "corridor1"))

    zkStore.createEmptyPath(config1Path)
    zkStore.set(config1Path, new ZkCorridor(id = "corridor1",
      name = "first corridor",
      roadType = 0,
      serviceType = ServiceType.RedLight,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = true,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = null,
      info = new ZkCorridorInfo(corridorId = 10,
        locationCode = "1",
        reportId = "TCS A2 Links  39.7 - 54.9",
        reportHtId = "A020011",
        reportPerformance = true,
        locationLine1 = "loc 1"),
      startSectionId = "1",
      endSectionId = "1",
      allSectionIds = Seq("1"),
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 402,
      roadCode = 2,
      dutyType = "KA2",
      deploymentCode = "03",
      codeText = ""), 0)
    val rootPathToServices = Path(Paths.Corridors.getServicesPath("a2", "corridor1"))
    val pathToService = (rootPathToServices / ServiceType.RedLight.toString)
    zkStore.createEmptyPath(pathToService)
    zkStore.set(pathToService, new RedLightConfig(factNumber = "1234", pardonRedTime = 1000, minimumYellowTime = 1000), 0)

    val config2Path = Path(Paths.Corridors.getConfigPath("a2", "corridor2"))

    zkStore.createEmptyPath(config2Path)
    zkStore.set(config2Path, new ZkCorridor(id = "corridor2",
      name = "first corridor",
      roadType = 0,
      serviceType = ServiceType.SpeedFixed,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = true,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = null,
      info = new ZkCorridorInfo(corridorId = 20,
        locationCode = "1",
        reportId = "TCS A2 Links  39.7 - 54.9",
        reportHtId = "A020011",
        reportPerformance = true,
        locationLine1 = "loc 2"),
      startSectionId = "2",
      endSectionId = "2",
      allSectionIds = Seq("2"),
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 402,
      roadCode = 2,
      dutyType = "KA2",
      deploymentCode = "03",
      codeText = ""), 0)
    val rootPathToServices2 = Path(Paths.Corridors.getServicesPath("a2", "corridor2"))
    val pathToService2 = (rootPathToServices2 / ServiceType.SpeedFixed.toString)
    zkStore.createEmptyPath(pathToService2)
    zkStore.set(pathToService2, new SpeedFixedConfig(), 0)
  }

  override protected def afterAll() {
    zkStore.curator.foreach(_.close())
    super.afterAll()
  }

}
