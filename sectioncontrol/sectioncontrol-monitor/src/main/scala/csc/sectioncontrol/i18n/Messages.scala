/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

package csc.sectioncontrol.i18n

/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

import java.text.MessageFormat

object Messages {
  val messages = Map(
    "csc.sectioncontrol.monitor.state.Failure" -> "Alarm",
    "csc.sectioncontrol.monitor.state.none" -> "Geen",
    "csc.sectioncontrol.monitor.state.StandBy" -> "Stand-by",
    "csc.sectioncontrol.monitor.state.EnforceOn" -> "Handhaven aan",
    "csc.sectioncontrol.monitor.state.EnforceOff" -> "Handhaven (offline)",
    "csc.sectioncontrol.monitor.state.EnforceDegraded" -> "Handhaven (degraded)",
    "csc.sectioncontrol.monitor.state.Off" -> "Off",
    "csc.sectioncontrol.monitor.state.Maintenance" -> "Onderhoud",
    "csc.sectioncontrol.monitor.state.CalibrationOff" -> "Calibreren Uit",
    "csc.sectioncontrol.monitor.state.CalibrationOn" -> "Calibreren Aan",
    "csc.sectioncontrol.monitor.statechange" -> "{0} -> {1}",
    "csc.sectioncontrol.monitor.logIncorrectActionForStateEvent" -> "Incorrect actie aangevraagd, kan niet status wijzigin van [{0}] naar [{1}]",
    "csc.sectioncontrol.monitor.logSelfTestAlreadyRunning" -> "Calibratie test is al gestart, kan calibratie test niet starten, in {0}",
    "csc.sectioncontrol.monitor.logNoUserFoundForSelfTest" -> "Geen gebruiker gevonden om calibratie test te starten in {0}",
    "csc.sectioncontrol.monitor.adhocCalibration" -> "Ad hoc aanvraag door gebruiker",
    "csc.sectioncontrol.monitor.enforceOn" -> "Calibratie geslaagd, Handhaven aan -> Stand-by",
    "csc.sectioncontrol.monitor.standByToEnforceOn" -> "Calibratie geslaagd, Stand-by -> Handhaven aan",
    "csc.sectioncontrol.monitor.standByToEnforceOff" -> "Calibratie geslaagd, Stand-by -> Handhaven offline",
    "csc.sectioncontrol.monitor.standByToDegraded" -> "Calibratie geslaagd, Stand-by -> Handhaven (degraded)",
    "csc.sectioncontrol.monitor.off" -> "Deur Open tijdens [Off] status, verwacht beheer",
    "csc.sectioncontrol.monitor.maintenance" -> "Deur Open tijdens [onderhoud] status, verwacht beheer",
    "csc.sectioncontrol.monitor.sendEnforceOnline" -> "Rooster aanwezig voor huidige tijd",
    "csc.sectioncontrol.monitor.sendEnforceOffline" -> "Geen rooster aanwezig voor huidige tijd",
    "csc.sectioncontrol.monitor.defaultSystemState" -> "Geen system status gevonden, creëer initiële Off status")

  /**
   * Translates a message.
   *
   * Uses `java.text.MessageFormat` internally to format the message.
   *
   * @param key the message key
   * @param args the message arguments
   * @return the formatted message or a default rendering if the key wasn’t defined
   */
  def apply(key: String, args: Any*): String = {
    translate(key, args).getOrElse(noMatch(key, args))
  }

  /**
   * Translates a message.
   *
   * Uses `java.text.MessageFormat` internally to format the message.
   *
   * @param key the message key
   * @param args the message arguments
   * @return the formatted message, if this key was defined
   */
  private def translate(key: String, args: Seq[Any]): Option[String] = {
    if (messages.contains(key)) {
      Some(new MessageFormat(messages(key)).format(args.map(_.asInstanceOf[java.lang.Object]).toArray))
    } else {
      None
    }
  }

  private def noMatch(key: String, args: Seq[Any]) = key
}
