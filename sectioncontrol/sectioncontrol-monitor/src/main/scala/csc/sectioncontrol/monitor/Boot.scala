/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.monitor

import java.io.File
import java.util.concurrent.atomic.AtomicReference
import akka.util.duration._
import akka.actor._
import org.apache.zookeeper.CreateMode
import org.apache.curator.framework.CuratorFramework
import org.apache.hadoop.hbase.client.HTable
import net.liftweb.json.{ Formats, DefaultFormats }
import com.typesafe.config.Config
import csc.dlog.{ LogRunner, SimpleDistributedQueueTaker }
import csc.dlog.hbase.EventLog
import csc.curator.utils.Curator
import csc.json.lift.EnumerationSerializer
import csc.hbase.utils.{ HBaseTableKeeper, HBaseAdminTool }
import csc.sectioncontrol.digitalswitch.DetectDigitalInput
import csc.sectioncontrol.CheckNTPActor
import csc.sectioncontrol.storage._
import csc.sectioncontrol.messages._
import csc.akkautils.MultiSystemBoot
import csc.sectioncontrol.notification.{ SmsByEmailNotifier, EventNotificationHandler }
import csc.akkautils.DirectLogging
import csc.config.Path
import csc.sectioncontrol.storagelayer.hbasetables.RowKeyDistributorFactory
import csc.sectioncontrol.storage.ZkSystemDefinition
import csc.sectioncontrol.storagelayer.SystemConfiguration
import csc.sectioncontrol.storagelayer.state.StateService
import csc.sectioncontrol.storagelayer.log.LogService

/**
 * TODO add a trait that listens on the systems path in zookeeper and notifies the multiSystemBoot to start actorsystem,
 * or shut it down and remove it.
 * Boot the monitors
 * @author Raymond Roestenburg
 */
class Boot extends MultiSystemBoot {
  private val monitors = new AtomicReference[Map[String, Seq[ActorRef]]](Map())
  private val clients = new AtomicReference[Map[String, CuratorFramework]](Map())
  private val logRunners = new AtomicReference[Map[String, LogRunner[SystemEvent]]](Map())
  def componentName = "SectionControl-Monitor"
  private val digitalInputUriKey = "digital.input.uri"
  private val skipDetectDigitalInputKey = "skip.detect.digital.input"
  private val NTP_PathKey = "check.ntp.path"
  private val NTP_maxNrFailuresKey = "check.ntp.maxNrFailures"
  private val NTP_maxStratumKey = "check.ntp.maxStratum"
  private val NTP_timerKey = "check.ntp.timer"
  private val NTP_reportWindowKey = "check.ntp.reportWindow"

  private val PLANNED_REBOOT_PathKey = "check.reboot.path"
  private val PLANNED_REBOOT_FREQKey = "check.reboot.frequency"
  private val plannedReboot = new AtomicReference[Boolean](false)

  override protected def formats(defaultFormats: Formats) = defaultFormats +
    new EnumerationSerializer(
      ConfigType,
      SystemStateType,
      VehicleCategory,
      ZkWheelbaseType,
      VehicleCode,
      ZkIndicationType,
      ServiceType,
      ZkCaseFileType,
      EsaProviderType,
      VehicleImageType,
      SpeedIndicatorType,
      ConfigType)

  override def startupActors() {

    super.startupActors()

    startNtpActor()
  }

  /**
   * Startup the actors for a monitor on the system.
   */
  def startupSystemActors(system: ActorSystem, systemId: String, curator: Curator, hbaseZkServers: String) {
    RowKeyDistributorFactory.init(curator)
    val config = system.settings.config
    val configWrapper: ConfigWrapper = new ConfigWrapper(config)
    val digitalInputUri = configWrapper.getStringFromConfig(digitalInputUriKey)
    val skipDetectDigitalInput = configWrapper.getBooleanFromConfig(skipDetectDigitalInputKey)

    startEventLogger[SystemEvent](systemId, curator, system)

    val client = curator.curator.get
    val qTaker = new SimpleDistributedQueueTaker[SystemEvent](curator.curator.get, "/ctes/systems/" + systemId + "/events", curator.serialization)

    val defintionPath = Paths.Systems.getSystemDefinitionPath(systemId)
    val systemDefinition = if (curator.exists(defintionPath)) {
      curator.get[ZkSystemDefinition](Paths.Systems.getSystemDefinitionPath(systemId)).getOrElse(new ZkSystemDefinition())
    } else {
      new ZkSystemDefinition()
    }

    val stepSystemStateActor = system.actorOf(Props(new StepSystemState(curator, systemId, initialTime = System.currentTimeMillis(), definition = systemDefinition)), "stepSystemState_" + systemId)

    val notificationHandlerActor = createNotificationHandler(system, systemId, curator)
    //TODO move to object or package so you can test it
    def copyEventsToLogListener(event: SystemEvent) {
      client.create().withMode(CreateMode.PERSISTENT_SEQUENTIAL).forPath("/ctes/systems/" + systemId + "/log-events/qn-", curator.serialization.serialize(event))
    }
    def stepSystemState(event: SystemEvent) {
      stepSystemStateActor ! event
    }
    def copyEventsToNotificationHandler(event: SystemEvent) {
      notificationHandlerActor ! event
    }
    val monitorFunctions = List[SystemEvent ⇒ Unit](copyEventsToLogListener, stepSystemState, copyEventsToNotificationHandler)
    val monitor = system.actorOf(Props(new MonitorActor(systemId, qTaker, monitorFunctions)), "Monitor" + systemId)

    val path = if (config.hasPath(PLANNED_REBOOT_PathKey)) {
      config.getString(PLANNED_REBOOT_PathKey)
    } else {
      "plannedReboot"
    }
    if (systemDefinition.startInStandBy) {
      //val config = system.settings.config
      checkPlannedReboot(path)
      if (!plannedReboot.get()) {
        //Req 7
        stepSystemStateActor ! SystemEvent("Boot", System.currentTimeMillis(), systemId, "StandBy", "system", Some("Bootstrap"), None, None, None)
      }
    }
    val hbaseConfig = HBaseTableKeeper.createCachedConfig(hbaseZkServers)
    HBaseAdminTool.ensureTableExists(hbaseConfig, Paths.Systems.getSystemsLogTableName, "log")
    val logTable = new HTable(hbaseConfig, Paths.Systems.getSystemsLogTableName)

    val logRunner = EventLog.createLogRunner[SystemEvent](logTable, systemId, client,
      "/ctes/systems/" + systemId + "/log-events", "/ctes/systems/" + systemId + "/log-events-lock", curator.serialization)
    logRunner.start()
    // Startup the logrunner that writes to bookkeeper
    var runnersMap = logRunners.get()
    while (!logRunners.compareAndSet(runnersMap, runnersMap + (systemId -> logRunner))) {
      runnersMap = logRunners.get()
    }

    // start the detect digital input.
    var digital: Option[ActorRef] = None
    // Alleen voor traject controle.
    if (!skipDetectDigitalInput)
      digital = Some(system.actorOf(Props(new DetectDigitalInput(digitalInputUri, systemId, curator))))

    var monitorMap = monitors.get()
    while (!monitors.compareAndSet(monitorMap, monitorMap + (systemId -> (Seq(monitor) ++ digital)))) {
      monitorMap = monitors.get()
    }

    if (systemDefinition.startInStandBy) {
      val frequency = if (config.hasPath(PLANNED_REBOOT_FREQKey)) {
        config.getLong(PLANNED_REBOOT_FREQKey)
      } else {
        60000L
      }
      system.scheduler.schedule(frequency.millis, frequency.millis) {
        checkPlannedReboot(path)
      }
    } else {
      plannedReboot.getAndSet(true)
    }
  }

  /**
   * Shut down the actors in a single system-specific ActorSystem. Optional to be implemented by
   * the concrete implementation.
   */
  override def shutdownSystemActors(system: ActorSystem, systemId: String) {
    if (!plannedReboot.get()) {
      val curator = createCurator(system, formats(DefaultFormats))
      executeOffState(systemId, curator)
    }
    val clientMap = clients.get()
    val runnersMap = logRunners.get()
    try runnersMap.get(systemId) map (_.stop()) finally
      clientMap.get(systemId) map (_.close())
    val actorList = monitors.get().get(systemId)
    actorList.foreach(_.foreach(system.stop(_)))
  }

  def checkPlannedReboot(filePath: String) {
    val file = new File(filePath)
    val isPlanned = file.exists()
    val oldValue = plannedReboot.get()
    plannedReboot.set(isPlanned)
    if (oldValue != isPlanned) {
      if (isPlanned) {
        log.warning("Planned reboot is turned on")
      } else {
        log.warning("Planned reboot is turned off")
      }
    }
  }

  def createNotificationHandler(system: ActorSystem, systemId: String, curator: Curator): ActorRef = {
    val emailBySmsNotifierId = "SmsByEmail"
    val emailBySmsNotifier = system.actorOf(Props(new SmsByEmailNotifier(emailBySmsNotifierId, systemId, curator)))
    val notifiers = Map(
      (emailBySmsNotifierId, emailBySmsNotifier))
    system.actorOf(Props(new EventNotificationHandler(systemId, curator, notifiers)))
  }

  def startNtpActor() {

    val system = actorSystem
    val config = system.settings.config

    val ntpqPath = config.getString(NTP_PathKey)
    val maxNrFailures = config.getInt(NTP_maxNrFailuresKey)
    val maxStratum = config.getInt(NTP_maxStratumKey)
    val timer = config.getInt(NTP_timerKey)
    val reportWindow = config.getInt(NTP_reportWindowKey)

    val curator = createCurator(system, formats(DefaultFormats))

    system.actorOf(Props(new CheckNTPActor(curator,
      ntpqPath = ntpqPath,
      maxNrFailures = maxNrFailures,
      maxStratum = maxStratum,
      timer = timer.seconds,
      reportWindow = reportWindow.seconds)), "NTP-Cluster")

  }

  def executeOffState(systemId: String, curator: Curator) {
    val systemCfg = new SystemConfiguration(curator)
    val corridors = systemCfg.getCorridorIds(systemId)
    corridors.foreach(corridorId ⇒ {
      val newState = ZkState("Off", System.currentTimeMillis(), "system", "System shutdown")
      val oldState = StateService.getVersioned(systemId, corridorId, curator) match {
        case Some(vo) ⇒
          StateService.update(systemId, corridorId, newState, vo.version, curator)
          Some(vo.data)
        case None ⇒
          StateService.create(systemId, corridorId, newState, curator)
          None
      }
      LogService.create(systemId, SystemEvent("SystemState", System.currentTimeMillis(), systemId, "Info", newState.userId, Some(oldState.getOrElse("None") + " -> " + newState)), curator)
    })

  }

  protected def zkServerQuorumConfigPath = "sectioncontrol.zkServers"
  protected def hbaseZkServerQuorumConfigPath = "sectioncontrol.hbaseZkServers"
  protected def moduleName = componentName
}

class ConfigWrapper(config: Config) extends DirectLogging {
  val missingKey = "config key :%s is missing, please provide."
  val invalidValueForKey = "config key :%s contains a invalid value."

  def getStringFromConfig(key: String): String = {
    require(!key.trim().isEmpty && key != null)

    if (config.hasPath(key)) {
      try {
        val value: String = config.getString(key)
        if (value == null || value.trim().isEmpty)
          handleError(key, invalidValueForKey.format(key))
        else
          value
      } catch {
        case _: java.lang.IllegalArgumentException ⇒
          handleError(key, invalidValueForKey.format(key))
      }
    } else {
      handleError(key, missingKey.format(key))
    }
  }

  def getBooleanFromConfig(key: String): Boolean = {
    require(!key.trim().isEmpty && key != null)
    if (config.hasPath(key)) {
      try {
        config.getString(key).toBoolean
      } catch {
        case _: java.lang.IllegalArgumentException ⇒
          handleError(key, invalidValueForKey.format(key))
      }
    } else {
      handleError(key, missingKey.format(key))
    }
  }

  private def handleError(key: String, msg: String): Nothing = {
    log.error(msg)
    throw new RuntimeException(msg)
  }
}

