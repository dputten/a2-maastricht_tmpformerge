package csc.sectioncontrol.monitor

import akka.actor._
import akka.util.duration._

import csc.config.Path
import csc.curator.utils.{ CuratorActor, Curator }
import csc.sectioncontrol.messages.SystemEvent
import csc.sectioncontrol.i18n.Messages
import csc.sectioncontrol.monitor.ScheduleMonitor.SystemEventDispatcher
import events._
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storagelayer.calibrationstate.CalibrationStateService
import csc.sectioncontrol.storagelayer.state.{ FailureService, StateService }
import csc.sectioncontrol.storage.ZkSystemDefinition
import scala.Some
import csc.sectioncontrol.monitor.events.EventTypeOffline
import csc.sectioncontrol.storagelayer.alerts.AlertService
import csc.sectioncontrol.storagelayer.user.UserServiceZooKeeper
import csc.sectioncontrol.storagelayer.lane.LaneService
import csc.sectioncontrol.storagelayer.corridor.CorridorService
import csc.sectioncontrol.storagelayer.log.LogService
import csc.sectioncontrol.storagelayer.offlineevents.OfflineEventsService
import csc.sectioncontrol.storagelayer.roles.RoleServiceZooKeeper
import csc.sectioncontrol.storage.ZkSystemDefinition
import scala.Some
import csc.sectioncontrol.monitor.events.EventTypeOffline
import csc.sectioncontrol.storage.ZkFailure
import csc.sectioncontrol.storage.ZkAlert
import csc.sectioncontrol.storage.ZkTriggerData
import csc.sectioncontrol.monitor.events.PastSystemAlert
import csc.sectioncontrol.storage.ZkAlertHistory

/**
 * Steps through the SystemStates. A state machine.
 * @param corridorId the corridor for which to handle messages
 * @param curator zookeeper connection
 * @param systemId system id
 * @param stateListener an optional listener to state changes.
 * @param initialTime (approximate) time that the actor was started/created
 * @param staleMapsPeriod the period in milliseconds at which the translationMaps are seen as stale, and reloaded if older. Internal scheduler for refresh always runs per minute, so setting
 *                        this value lower than one minute has no effect.
 */
class StepCorridorState(protected val corridorId: String,
                        protected val curator: Curator,
                        protected val systemId: String,
                        stateListener: Option[ActorRef] = None,
                        initialTime: Long = System.currentTimeMillis(),
                        staleMapsPeriod: Int = 60 * 1000,
                        definition: ZkSystemDefinition = new ZkSystemDefinition(),
                        eventDispatcher: Option[SystemEventDispatcher] = None) extends CuratorActor with EventTranslator {

  private var currentState: ZkState = _
  private val _corridorId = corridorId
  private var reloadTranslationMapsSchedule: Option[Cancellable] = None
  private val scheduleMonitor = context.actorOf(ScheduleMonitor.props(systemId, corridorId, curator, Some(definition), eventDispatcher), "ScheduleMonitor")
  private def now = System.currentTimeMillis()

  override def preRestart(reason: Throwable, message: Option[Any]) {
    log.error("Restarting StepCorridorState for corridorId %s while processing %s".format(corridorId, message), reason)
    super.preRestart(reason, message)
  }

  override def preStart() {
    super.preStart()
    reloadTranslationMapsSchedule = Some(context.system.scheduler.schedule(1 second, 1 minute, self, ReloadTranslationMaps(staleMapsPeriod)))

    loadTranslationMapsFromZookeeper()

    StateService.createEmptyHistoryPath(systemId, corridorId, curator)

    currentState = StateService.get(systemId, corridorId, curator) match {
      case Some(state) ⇒
        stateListener.foreach(_ ! state)
        state
      case None ⇒
        currentState = ZkState(SystemStateType.Off.toString, initialTime, "system",
          Messages("csc.sectioncontrol.monitor.defaultSystemState"))
        executeState(context, currentState)
    }

    becomeState(context)
  }

  override def postStop() {
    reloadTranslationMapsSchedule.foreach(_.cancel())
    reloadTranslationMapsSchedule = None
    super.postStop()
  }

  def receive = off

  private def becomeState(context: ActorContext) {
    log.debug("become: {}", currentState.state)
    currentState match {
      case Off()             ⇒ context.become(doNotHandleMessage orElse off orElse defaultReceive)
      case Maintenance()     ⇒ context.become(doNotHandleMessage orElse maintenance orElse defaultReceive)
      case StandBy()         ⇒ context.become(doNotHandleMessage orElse standBy orElse defaultReceive)
      case Failure()         ⇒ context.become(doNotHandleMessage orElse failure orElse defaultReceive)
      case EnforceOn()       ⇒ context.become(doNotHandleMessage orElse enforceAll orElse defaultReceive)
      case EnforceOff()      ⇒ context.become(doNotHandleMessage orElse enforceAll orElse defaultReceive)
      case EnforceDegraded() ⇒ context.become(doNotHandleMessage orElse enforceAll orElse defaultReceive)
    }
  }

  /*Prevent messages NOT for this corridor to be handled. This is the exit point for the become(s).*/
  private def doNotHandleMessage: Receive = {
    case event: SystemEvent if isNotSystemWide(event)
      && isNotForThisCorridor(event.corridorId)
      && isGantryNotUsedByThisCorridor(event.gantryId)
      && isLaneNotUsedByGantriesInThisCorridor(event.laneId) ⇒ //do nothing
  }

  private def isNotSystemWide(event: SystemEvent): Boolean = {
    if (event.systemId == systemId && event.corridorId == None && event.gantryId == None && event.laneId == None)
      false
    else
      true
  }

  private def isNotForThisCorridor(corridorId: Option[String]): Boolean = {
    corridorId match {
      case Some(id) ⇒
        if (id == _corridorId) false else true
      case None ⇒
        true
    }
  }

  private def isGantryNotUsedByThisCorridor(gantryId: Option[String]): Boolean = {
    gantryId match {
      case Some(id) ⇒
        val gantries = CorridorService.getGantriesForCorridor(curator, systemId, _corridorId)
        !gantries.exists(_ == id)
      case None ⇒ true
    }
  }

  private def isLaneNotUsedByGantriesInThisCorridor(laneId: Option[String]): Boolean = {
    laneId match {
      case Some(id) ⇒
        val gantriesInCorridor = CorridorService.getGantriesForCorridor(curator, systemId, _corridorId)
        val laneIdsInGantries = gantriesInCorridor.map(g ⇒ LaneService.getAllLaneIdsForGantry(systemId, g, curator)).flatten
        !laneIdsInGantries.exists(_ == id)
      case None ⇒ true
    }
  }

  private def defaultReceive: Receive = {
    case KnownPastAlertEvent(pastAlert)                  ⇒ alertToHistory(pastAlert)
    case ReloadTranslationMaps(stalePeriod)              ⇒ updateTranslationMapsIfNeeded(stalePeriod)
    case systemEvent @ AdhocSelfTestRequest()            ⇒ sendSelfTestTrigger(currentState, currentState, true, systemEvent.userId)
    case FailureSignOff(path, time)                      ⇒ FailureService.signOff(systemId, corridorId, path, time, curator)
    case AlertSignOff(path, time)                        ⇒ AlertService.signOff(systemId, corridorId, path, time, curator)
    case event: SystemEvent if isAutomaticSignOff(event) ⇒ automaticSignOff(event, getAlertsWithPath, getFailuresWithPath, signOffAlert, signOffFailure, logInfo)
    case event: SystemEvent                              ⇒ logIgnoringEvent(event)
    case obj: Any                                        ⇒ log.error("Unexpected event:" + obj)
  }

  def isAutomaticSignOff(systemEvent: SystemEvent): Boolean = { systemEvent.eventType.endsWith("-SOLVED") }

  def automaticSignOff(systemEvent: SystemEvent, getAlerts: () ⇒ List[(String, ZkAlert)], getFailures: () ⇒ List[(String, ZkFailure)],
                       signOffAlert: (String, Long, SystemEvent) ⇒ Unit, signOffFailure: (String, Long, SystemEvent) ⇒ Unit,
                       logInfo: (String, SystemEvent) ⇒ Unit) = {
    val alerts = getAlerts()

    val failures = getFailures()
    val eventType = systemEvent.eventType.replaceAllLiterally("-SOLVED", "")

    alerts.foreach {
      case (path, alert) ⇒
        log.debug("zookeeperPath:" + path)
        log.debug("alert.alertType:" + alert.alertType)
    }

    failures.foreach {
      case (path, failure) ⇒
        log.debug("zookeeperPath:" + path)
        log.debug("failure.failureType:" + failure.failureType)
    }

    // Find the alert(s) in zookeeper with the same eventtype and path
    val existingAlerts = alerts.filter { case (zookeeperPath, zkAlert) ⇒ zkAlert.alertType == eventType && zkAlert.path == systemEvent.componentId }
    if (existingAlerts.isEmpty) {
      //try to find failures
      val existingFailures = failures.filter { case (zookeeperPath, zkFailure) ⇒ zkFailure.failureType == eventType && zkFailure.path == systemEvent.componentId }
      if (existingFailures.isEmpty) {
        logInfo("Automatic sign-off was not found in alerts and failures for systemEvent:", systemEvent)
      } else {
        existingFailures.foreach(result ⇒ {
          logInfo("Automatic sign-off failure for systemEvent:", systemEvent)
          signOffFailure(result._1, now, systemEvent)
        })
      }
    } else {
      existingAlerts.foreach(result ⇒ {
        logInfo("Automatic sign-off alert for systemEvent:", systemEvent)
        signOffAlert(result._1, now, systemEvent)
      })
    }
  }

  private def logInfo(message: String, systemEvent: SystemEvent) = {
    log.info(message + systemEvent)
  }

  def getAlertsWithPath(): List[(String, ZkAlert)] = { AlertService.getSystemAlertsWithPath(curator, systemId, corridorId, now) }

  def getFailuresWithPath(): List[(String, ZkFailure)] = { FailureService.getFailuresWithPath(systemId, corridorId, now, curator).toList }

  private def addToLog(systemEvent: SystemEvent) {
    LogService.create(systemId, systemEvent, curator)
  }

  private def off: Receive = {
    case systemEvent @ DoorOpen() if !definition.acceptFailureInOffMode ⇒ addToLog(SystemEvent("SystemState", now, systemId, "Info", "system", Some(Messages("csc.sectioncontrol.monitor.off"))))
    case systemEvent @ DoorOpen()                                       ⇒ sendDestroySignal(systemEvent)
    case systemEvent @ OffLineEvent(offlineEventType)                   ⇒ processOfflineEvent(systemEvent, offlineEventType)
    case systemEvent @ KnownEvent(nextState, systemError) ⇒
      systemError.foreach(sendError(_))
      nextSystemStateInOff(systemEvent, nextState)
  }

  private def maintenance: Receive = {
    case systemEvent @ DoorOpen()                     ⇒ addToLog(SystemEvent("SystemState", now, systemId, "Info", "system", Some(Messages("csc.sectioncontrol.monitor.maintenance"))))
    case systemEvent @ OffLineEvent(offlineEventType) ⇒ processOfflineEvent(systemEvent, offlineEventType)
    case systemEvent @ KnownEvent(nextState, systemError) ⇒
      systemError.foreach(sendError(_))
      nextSystemStateInMaintenance(systemEvent, nextState)
  }

  private def standBy: Receive = {
    case systemEvent @ DoorOpen()                     ⇒ sendDestroySignal(systemEvent)
    case systemEvent @ OffLineEvent(offlineEventType) ⇒ processOfflineEvent(systemEvent, offlineEventType)
    case systemEvent @ KnownEvent(nextState, systemError) ⇒

      systemError.foreach(sendError(_))
      nextSystemStateInStandBy(systemEvent, nextState)
    case systemEvent @ SystemEvent("selftest", timestamp, `systemId`, "SELFTEST-SUCCESS", userId, "StandBy", token, _, _, _, _, _) ⇒
      if (OfflineEventsService.offlineEventsExist(systemId, corridorId, curator)) {
        executeState(context, ZkState(SystemStateType.EnforceOff.toString, timestamp, userId, Messages("csc.sectioncontrol.monitor.standByToEnforceOff")))
      } else if (AlertService.alertsExist(curator, systemId, corridorId)) {
        executeState(context, ZkState(SystemStateType.EnforceDegraded.toString, timestamp, userId, Messages("csc.sectioncontrol.monitor.standByToDegraded")))
      } else {
        executeState(context, ZkState(SystemStateType.EnforceOn.toString, timestamp, userId, Messages("csc.sectioncontrol.monitor.standByToEnforceOn")))
      }
  }

  private def enforceAll: Receive = {
    case systemEvent @ DoorOpen() ⇒ sendDestroySignal(systemEvent)
    case systemEvent @ OffLineEvent(offlineEventType) ⇒
      val requestedStateChange = processOfflineEvent(systemEvent, offlineEventType)
      nextSystemStateInEnforceAll(systemEvent, requestedStateChange)
    case systemEvent @ KnownEvent(nextState, systemError) ⇒
      systemError.foreach(sendError(_))
      nextSystemStateInEnforceAll(systemEvent, nextState)
    case systemEvent @ SystemEvent("selftest", timestamp, `systemId`, "SELFTEST-SUCCESS", userId, _, token, _, _, _, _, _) if (currentState.state == systemEvent.reason) ⇒
      executeState(context, ZkState(SystemStateType.StandBy.toString, timestamp, userId, Messages("csc.sectioncontrol.monitor.enforceOn")))
    case systemEvent @ AlertSignOff(path, time) ⇒ signOffAlert(path, time, systemEvent)
  }

  def signOffAlert(path: String, time: Long, systemEvent: SystemEvent) {
    AlertService.signOff(systemId, corridorId, path, time, curator)
    if ((currentState.state == SystemStateType.EnforceDegraded.toString) && !AlertService.alertsExist(curator, systemId, corridorId)) {
      if (OfflineEventsService.offlineEventsExist(systemId, corridorId, curator)) {
        //we have some offline event => go to offline
        executeState(context, ZkState(SystemStateType.EnforceOff.toString, systemEvent.timestamp, currentState.userId, systemEvent.reason))
      } else if (definition.enforceDetail) {
        //enforce detail => delegate on ScheduleMonitor the decision about next state
        scheduleMonitor ! CheckSchedules(corridorId, time)
      } else {
        //fallback => just go directly to EnforceOn
        executeState(context, ZkState(SystemStateType.EnforceOn.toString, systemEvent.timestamp, currentState.userId, systemEvent.reason))
      }
    }
  }

  private def failure: Receive = {
    case systemEvent @ DoorOpen()                     ⇒ sendDestroySignal(systemEvent)
    case systemEvent @ OffLineEvent(offlineEventType) ⇒ processOfflineEvent(systemEvent, offlineEventType)
    case systemEvent @ FailureSignOff(path, time)     ⇒ signOffFailure(path, time, systemEvent)
    case systemEvent @ KnownEvent(nextState, systemError) ⇒
      systemError.foreach {
        error ⇒ sendError(error)
      }
  }

  private def signOffFailure(path: String, time: Long, systemEvent: SystemEvent) {
    FailureService.signOff(systemId, corridorId, path, time, curator)
    if (!FailureService.failuresExist(systemId, corridorId, curator)) {
      executeState(context, ZkState(SystemStateType.StandBy.toString, systemEvent.timestamp, systemEvent.userId, systemEvent.reason))
    }
  }

  private def sendDestroySignal(event: SystemEvent) {

    if (definition.destroyData) {
      log.info("Destroying the system, an open door was detected, broadcasting destroy signal!")
      val r = scala.util.Random
      Thread.sleep(r.nextInt(500))
      if (!curator.exists(Path("/destroy") / event.timestamp.toString)) {
        curator.put(Path("/destroy") / event.timestamp.toString, event)
      }
    } else {
      log.warning("An open door was detected, but destroy data is turned off")
    }

    event match {
      case systemEvent @ KnownEvent(nextState, systemError) ⇒
        systemError.foreach(sendError(_))
        executeState(context, nextState)
    }
  }

  private def executeState(context: ActorContext, updateState: ZkState): ZkState = {
    //When no detailed status needed all enforce states are mapped to EnforceOn
    //And when already in EnforceOn do nothing
    var newState = updateState
    if (!definition.enforceDetail && SystemStateType.EnforceStates.contains(updateState.state)) {
      if (currentState.state == SystemStateType.EnforceOn.toString)
        return currentState
      newState = updateState.copy(state = SystemStateType.EnforceOn.toString)
    }
    //process the state change
    log.debug("Current state: {}", currentState.state)
    val oldState = currentState
    currentState = newState

    StateService.getVersioned(systemId, corridorId, curator) match {
      case Some(vo) ⇒
        StateService.update(systemId, corridorId, newState, vo.version, curator)
      case None ⇒
        StateService.create(systemId, corridorId, newState, curator)
    }

    val newTime = now
    val reason = Messages("csc.sectioncontrol.monitor.statechange", translateState(oldState), translateState(newState))
    addToLog(SystemEvent("SystemState", newTime, systemId, "SystemStateChange", currentState.userId, Some(reason), Some(corridorId), None, None))
    becomeState(context)

    if (SystemStateType.EnforceStates.contains(oldState.state) && SystemStateType.EnforceStates.contains(newState.state) == false) {
      scheduleMonitor ! "stop"

    } else if (SystemStateType.EnforceStates.contains(newState.state) && !SystemStateType.EnforceStates.contains(oldState.state)) {
      scheduleMonitor ! "start"
    }

    stateListener.foreach(_ ! currentState)
    log.debug("New state: {}", currentState.state)
    currentState
  }

  //EnforceDegraded is never sent by the client, but is created when any Alert occurs.
  //the alertsExist is set to true if any alert is received, and cleared when AlertSolved.
  private def nextSystemStateInOff(systemEvent: SystemEvent, nextState: ZkState) {
    log.debug("In Off state, received {} which causes next state {}", systemEvent, nextState)
    nextState match {
      case Off()         ⇒ // do nothing, already there
      case Maintenance() ⇒ logIncorrectActionForStateEvent(currentState, nextState)
      case StandBy() ⇒
        if (FailureService.failuresExist(systemId, corridorId, curator)) {
          executeState(context, nextState.copy(state = SystemStateType.Failure.toString))
        } else {
          executeState(context, nextState)
        }
      case Failure() ⇒
        if (definition.acceptFailureInOffMode) {
          executeState(context, nextState)
        } else {
          logIncorrectActionForStateEvent(currentState, nextState)
        }
      case EnforceOn()       ⇒ logIncorrectActionForStateEvent(currentState, nextState)
      case EnforceOff()      ⇒ logIncorrectActionForStateEvent(currentState, nextState)
      case EnforceDegraded() ⇒ logIncorrectActionForStateEvent(currentState, nextState)
    }
  }

  private def nextSystemStateInMaintenance(systemEvent: SystemEvent, nextState: ZkState) {
    log.debug("In Maintenance state, received {} which causes next state {}", systemEvent, nextState)
    nextState match {
      case Off()         ⇒ logIncorrectActionForStateEvent(currentState, nextState)
      case Maintenance() ⇒ // do nothing, already there
      case StandBy() ⇒
        if (FailureService.failuresExist(systemId, corridorId, curator)) {
          executeState(context, nextState.copy(state = SystemStateType.Failure.toString))
        } else {
          executeState(context, nextState)
        }
      case Failure() ⇒
        if (definition.acceptFailureInOffMode) {
          executeState(context, nextState)
        } else {
          logIncorrectActionForStateEvent(currentState, nextState)
        }
      case EnforceOn()       ⇒ logIncorrectActionForStateEvent(currentState, nextState)
      case EnforceOff()      ⇒ logIncorrectActionForStateEvent(currentState, nextState)
      case EnforceDegraded() ⇒ logIncorrectActionForStateEvent(currentState, nextState)
    }
  }

  private def nextSystemStateInStandBy(systemEvent: SystemEvent, nextState: ZkState) {
    log.debug("in StandBy, received {} which causes next state {}", systemEvent, nextState)

    nextState match {
      case Off()         ⇒ executeState(context, nextState)
      case Maintenance() ⇒ executeState(context, nextState)
      case StandBy()     ⇒ // do nothing, already there
      case Failure() ⇒
        {
          executeState(context, nextState)
        }
      case EnforceOn()       ⇒ sendSelfTestTrigger(currentState, nextState) //Req 9, bullet 1
      case EnforceOff()      ⇒ logIncorrectActionForStateEvent(currentState, nextState)
      case EnforceDegraded() ⇒ //do nothing
    }
  }

  private def nextSystemStateInEnforceAll(systemEvent: SystemEvent, nextState: ZkState) {
    log.debug("in {}, received {} which causes next state {}", currentState, systemEvent, nextState)

    if (currentState.state != nextState.state) {
      nextState match {
        case Off()         ⇒ logIncorrectActionForStateEvent(currentState, nextState)
        case Maintenance() ⇒ logIncorrectActionForStateEvent(currentState, nextState)
        case StandBy()     ⇒ sendSelfTestTrigger(currentState, nextState)
        case Failure()     ⇒ executeState(context, nextState)
        case EnforceOn() | EnforceDegraded() | EnforceOff() ⇒
          if (OfflineEventsService.offlineEventsExist(systemId, corridorId, curator)) {
            if (currentState.state != SystemStateType.EnforceOff.toString)
              executeState(context, nextState.copy(state = SystemStateType.EnforceOff.toString, userId = currentState.userId))
          } else if (AlertService.alertsExist(curator, systemId, corridorId)) {
            executeState(context, nextState.copy(state = SystemStateType.EnforceDegraded.toString, userId = currentState.userId))
          } else {
            executeState(context, nextState.copy(state = SystemStateType.EnforceOn.toString, userId = currentState.userId))
          }
      }
    }
  }

  private def sendSelfTestTrigger(oldState: ZkState, newState: ZkState, adHoc: Boolean = false, adHocUserId: String = "") {
    log.debug("Trying to send {}selftest trigger", if (adHoc) "ad hoc " else "")

    val reason = oldState.state
    val logReason = translateState(oldState)
    val userId = if (adHoc) adHocUserId else newState.userId
    val userService = new UserServiceZooKeeper(curator, new RoleServiceZooKeeper(curator))

    userService.get(userId) match {
      case Some(user) ⇒
        if (!CalibrationStateService.isCalibrating(systemId, corridorId, curator)) {
          if (adHoc) {
            CalibrationStateService.create(systemId, corridorId, ZkTriggerData(now, user.id, user.reportingOfficerId, false, Messages("csc.sectioncontrol.monitor.adhocCalibration")), curator)
            log.debug("Sent ad hoc selftest trigger based on request from user {}", user.id)
          } else {
            CalibrationStateService.create(systemId, corridorId, ZkTriggerData(now, user.id, user.reportingOfficerId, false, reason), curator)
            log.debug("Sent selftest trigger for state change from state {} to {}, by user {} {}.", reason, newState, user.id, user.name)
          }
        } else {
          logSelfTestAlreadyRunning(currentState, newState, logReason)
          log.info("Selftest already running, not starting selftest from state {} to {}, by user {} {}.", reason, newState, user.id, user.name)
        }
      case None ⇒
        logNoUserFoundForSelfTest(currentState, newState, logReason)
    }
  }

  private def sendError(error: SystemError) {
    error match {
      case f: ZkFailure ⇒
        FailureService.exists(systemId, corridorId, f, curator) match {
          case false ⇒
            FailureService.create(systemId, corridorId, f, curator)
            addToLog(SystemEvent(error.path, error.timestamp, systemId, "Failure", "system", Some(error.message)))
          case true ⇒
            //skip it
            log.warning("Double SystemFailure detected and ignored. (%s)".format(f))
        }
      case a: ZkAlert ⇒
        AlertService.exists(systemId, corridorId, a, curator) match {
          case false ⇒
            AlertService.create(systemId, corridorId, a, curator)
            addToLog(SystemEvent(error.path, error.timestamp, systemId, "Alert", "system",
              Some(error.message + ", impact on " + a.configType.toString + ", reductionFactor " + a.reductionFactor)))

          case true ⇒
            //skip it
            log.warning("Double SystemAlert detected and ignored. (%s)".format(a))
        }
    }
  }

  private def alertToHistory(alert: PastSystemAlert) {
    // Write alert to history log
    alert.beginTimestamp match {
      case Some(beginTimestamp) ⇒
        AlertService.addToHistory(systemId: String, corridorId, new ZkAlertHistory(path = alert.path,
          alertType = alert.alertType,
          message = alert.message,
          startTime = beginTimestamp,
          endTime = alert.endTimestamp,
          configType = alert.configType.toString,
          reductionFactor = alert.reductionFactor), curator: Curator)
      case None ⇒
        // Cannot log this alert
        log.error("Received past alert without begin timestamp. This alert cannot be processed [%s]".format(alert))
    }
  }

  private def processOfflineEvent(systemEvent: SystemEvent, offlineEventType: EventTypeOffline): ZkState = {
    //createPath from event
    val nodeName = "%s-%s".format(systemEvent.componentId, offlineEventType.eventName).replaceAll("/", "-")
    if (offlineEventType.stateToOffline) {
      //goto state offline
      if (OfflineEventsService.exists(systemId, corridorId, nodeName, curator)) {
        log.warning("Offline event %s for component %s already exists".format(offlineEventType.eventName, systemEvent.componentId))
      } else {
        //add event to zookeeper
        OfflineEventsService.create(systemId, corridorId, nodeName, curator, systemEvent)
      }
      //return requested state
      ZkState(SystemStateType.EnforceOff.toString, systemEvent.timestamp, systemEvent.userId, systemEvent.reason)
    } else {
      //goto state online
      try {
        //remove event from zookeeper
        OfflineEventsService.delete(systemId, corridorId, nodeName, curator)
      } catch {
        case ex: Exception ⇒
          log.warning("Offline event %s for component %s could not be removed: %s".format(offlineEventType.eventName, systemEvent.componentId, ex.getMessage))
      }
      //return requested state
      ZkState(SystemStateType.EnforceOn.toString, systemEvent.timestamp, systemEvent.userId, systemEvent.reason)
    }
  }

  protected def logNoUserFoundForSelfTest(currentState: ZkState, systemState: ZkState, reason: String) {
    val msg = Messages("csc.sectioncontrol.monitor.logNoUserFoundForSelfTest", reason)
    addToLog(SystemEvent("SystemState", now, systemId, SystemStateType.Failure.toString, systemState.userId, Some(msg)))
  }

  protected def logSelfTestAlreadyRunning(currentState: ZkState, systemState: ZkState, reason: String) {
    val msg = Messages("csc.sectioncontrol.monitor.logSelfTestAlreadyRunning", reason)
    addToLog(SystemEvent("SystemState", now, systemId, "Info", systemState.userId, Some(msg)))
  }
  protected def logIncorrectActionForStateEvent(currentState: ZkState, newState: ZkState) {
    log.info("Incorrect action " + currentState.state + ", " + newState.state + ", will not change SystemState.")
    val reason = Messages("csc.sectioncontrol.monitor.logIncorrectActionForStateEvent", translateState(currentState), translateState(newState))
    addToLog(SystemEvent("SystemState", now, systemId, "Info", newState.userId, Some(reason)))
  }
  protected def logIgnoringEvent(event: SystemEvent) {
    log.info("Ignoring event " + event + ", will not impact SystemState.")
  }
  private def updateTranslationMapsIfNeeded(reloadInterval: Int) {
    if (now - lastLoaded > reloadInterval) {
      loadTranslationMapsFromZookeeper()
    }
  }

  private def translateState(state: ZkState) = Messages("csc.sectioncontrol.monitor.state." + state.state)
}

private[monitor] case class ReloadTranslationMaps(staleMapsPeriod: Int)

