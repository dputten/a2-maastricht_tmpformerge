/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.monitor.events

import csc.sectioncontrol.messages.SystemEvent
import csc.sectioncontrol.monitor.{ SystemStateType }
import csc.sectioncontrol.storage.ZkState

private[monitor] object DoorOpen {
  def unapply(event: SystemEvent): Boolean = {
    if (event.eventType == "Open") true else false
  }
}

private[monitor] object SelfTestStart {
  def unapply(event: SystemEvent): Boolean = {
    if (event.eventType == "SELFTEST-START") true else false
  }
}

private[monitor] object SelfTestEnd {
  def unapply(event: SystemEvent): Boolean = {
    if (event.eventType == "SELFTEST-END") true else false
  }
}

private[monitor] object ServiceCreated {
  def unapply(event: SystemEvent): Boolean = {
    if (event.eventType == "ServiceCreated") true else false
  }
}

private[monitor] object AdhocSelfTestRequest {
  def unapply(event: SystemEvent): Boolean = {
    if (event.eventType == "CalibrationOn") true else false
  }
}
private[monitor] object AlertSignOff {
  def unapply(event: SystemEvent): Option[(String, Long)] = {
    if (event.eventType == "AlertSignOff") Some(event.path.getOrElse(""), event.timestamp) else None
  }
}

private[monitor] object FailureSignOff {
  def unapply(event: SystemEvent): Option[(String, Long)] = {
    if (event.eventType == "FailureSignOff") Some(event.path.getOrElse(""), event.timestamp) else None
  }
}

private[monitor] object EnforceOff {
  def unapply(state: ZkState): Boolean = {
    if (state.state == SystemStateType.EnforceOff.toString) true else false
  }
}

private[monitor] object EnforceOn {
  def unapply(state: ZkState): Boolean = {
    if (state.state == SystemStateType.EnforceOn.toString) true else false
  }
}

private[monitor] object EnforceDegraded {
  def unapply(state: ZkState): Boolean = {
    if (state.state == SystemStateType.EnforceDegraded.toString) true else false
  }
}

private[monitor] object Failure {
  def unapply(state: ZkState): Boolean = {
    if (state.state == SystemStateType.Failure.toString) true else false
  }
}

private[monitor] object StandBy {
  def unapply(state: ZkState): Boolean = {
    if (state.state == SystemStateType.StandBy.toString) true else false
  }
}

private[monitor] object Off {
  def unapply(state: ZkState): Boolean = {
    if (state.state == SystemStateType.Off.toString) true else false
  }
}

private[monitor] object Maintenance {
  def unapply(state: ZkState): Boolean = {
    if (state.state == SystemStateType.Maintenance.toString) true else false
  }
}

private[monitor] object Test {
  def unapply(state: ZkState): Boolean = {
    false
  }
}