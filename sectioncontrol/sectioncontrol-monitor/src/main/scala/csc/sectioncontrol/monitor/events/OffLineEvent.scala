/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.monitor.events

import java.util.concurrent.atomic.AtomicReference
import csc.sectioncontrol.messages.SystemEvent

case class EventTypeOffline(eventName: String, setOffline: Boolean) {
  def stateToOffline = setOffline
  def stateToEnforce = !setOffline
}

object OffLineEvent {
  val defaultOffLineEvents = List(
    EventOfflineMapping(SystemEventIdentifier(eventType = Some("NoActiveSchedule")), eventName = "Schedules", setOffline = true),
    EventOfflineMapping(SystemEventIdentifier(eventType = Some("ActiveSchedule")), eventName = "Schedules", setOffline = false))
  private val offLineEvent = new AtomicReference[List[EventOfflineMapping]](defaultOffLineEvents)

  def updateEvents(addEvents: List[EventOfflineMapping]) {
    var offLineEventMappings = offLineEvent.get()
    val newIdentifiers = addEvents.map(_.eventIdentifier)

    while (!offLineEvent.compareAndSet(offLineEventMappings, {
      val filter = offLineEventMappings.filterNot(mapping ⇒ newIdentifiers.contains(mapping.eventIdentifier))
      filter ++ addEvents
    })) {
      offLineEventMappings = offLineEvent.get()
    }
  }

  // Return the EventOffline (if any) that matches with the given system event.
  def unapply(event: SystemEvent): Option[EventTypeOffline] = {
    offLineEvent.get().find(mapping ⇒ (event, mapping.eventIdentifier) match {
      case ExtractSystemEventIdentifier(identifier) ⇒ true
      case _                                        ⇒ false
    }).map(mapping ⇒ EventTypeOffline(mapping.eventName, mapping.setOffline))
  }
}