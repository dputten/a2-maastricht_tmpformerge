/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.monitor.events

import java.util.concurrent.atomic.AtomicReference
import csc.sectioncontrol.messages.SystemEvent
import csc.sectioncontrol.storage.{ ConfigType }

private[monitor] case class PastSystemAlert(path: String, alertType: String, message: String, beginTimestamp: Option[Long], endTimestamp: Long, configType: ConfigType.Value, reductionFactor: Float, confirmed: Boolean = false)

object KnownPastAlertEvent {
  val defaultEventsToPastAlerts = List[EventPastAlertMapping]()
  private val eventsToPastAlerts = new AtomicReference[List[EventPastAlertMapping]](defaultEventsToPastAlerts)

  def updateEventsToAlerts(addEventsToPastAlerts: List[EventPastAlertMapping]) {
    var eventToPastAlertMappings = eventsToPastAlerts.get()
    val newIdentifiers = addEventsToPastAlerts.map(_.eventIdentifier)

    while (!eventsToPastAlerts.compareAndSet(eventToPastAlertMappings, {
      val filter = eventToPastAlertMappings.filterNot(mapping ⇒ newIdentifiers.contains(mapping.eventIdentifier))
      filter ++ addEventsToPastAlerts
    })) {
      eventToPastAlertMappings = eventsToPastAlerts.get()
    }
  }

  // Return the PastSystemAlert (if any) that matches with the given system event.
  def unapply(event: SystemEvent): Option[PastSystemAlert] = {
    val identifiers = eventsToPastAlerts.get().map(mapping ⇒ mapping.eventIdentifier -> (mapping.configType, mapping.reductionFactor))

    identifiers.foldLeft[Option[(SystemEventIdentifier, (ConfigType.Value, Float))]](None) { (accu, next) ⇒
      (event, next._1) match {
        case ExtractSystemEventIdentifier(identifier) ⇒ Some((identifier, next._2))
        case _                                        ⇒ accu
      }
    }.map {
      case (i, (configType, reductionFactor)) ⇒ {
        createPastSystemAlert(event = event, configType = Some(configType), reductionFactor = Some(reductionFactor))
      }
    }
  }

  private def createPastSystemAlert(event: SystemEvent, configType: Option[ConfigType.Value], reductionFactor: Option[Float]): PastSystemAlert = {
    val defaultTemplate = "component: %s, eventType: %s, notificatie reden: %s, gebruiker: %s"
    val noReductionFactorErrorText = "Reductie factor is onbekend, neem contact op met de administrator. \n"

    val reason = event.reason.trim match {
      case ""   ⇒ "<onbekend>"
      case text ⇒ text
    }
    val msg = if (reductionFactor.isEmpty) {
      noReductionFactorErrorText + defaultTemplate.format(event.componentId, event.eventType, reason, event.userId)
    } else {
      defaultTemplate.format(event.componentId, event.eventType, reason, event.userId)
    }

    PastSystemAlert(path = event.componentId,
      alertType = event.eventType,
      message = msg,
      beginTimestamp = event.startTimestamp,
      endTimestamp = event.timestamp,
      configType = configType.getOrElse(ConfigType.System),
      reductionFactor = reductionFactor.getOrElse(0f))
  }
}

