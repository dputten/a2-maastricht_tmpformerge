/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.monitor

import akka.actor._
import csc.curator.utils.{ CuratorActor, Curator }
import csc.sectioncontrol.monitor.ScheduleMonitor.SystemEventDispatcher
import csc.sectioncontrol.storagelayer._
import events._
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storage.ZkSystemDefinition

/**
 * Steps through the SystemStates. A state machine.
 * @param curator zookeeper connection
 * @param systemId system id
 * @param stateListener an optional listener to state changes.
 * @param initialTime (approximate) time that the actor was started/created
 * @param staleMapsPeriod the period in milliseconds at which the translationMaps are seen as stale, and reloaded if older. Internal scheduler for refresh always runs per minute, so setting
 *                        this value lower than one minute has no effect.
 */
class StepSystemState(protected val curator: Curator,
                      protected val systemId: String,
                      stateListener: Option[ActorRef] = None,
                      initialTime: Long = System.currentTimeMillis(),
                      staleMapsPeriod: Int = 60 * 1000,
                      definition: ZkSystemDefinition = new ZkSystemDefinition(),
                      eventDispatcher: Option[SystemEventDispatcher] = None) extends CuratorActor with EventTranslator {
  private val systemCfg = new SystemConfiguration(curator)
  private var corridorActorContainers: Seq[CorridorActorContainer] = null

  override def preRestart(reason: Throwable, message: Option[Any]) {
    log.error("Restarting StepSystemState for systemId %s while processing %s".format(systemId, message), reason)
    super.preRestart(reason, message)
  }

  def receive = {
    case event: Any ⇒
      // get all corridors
      val corridors = systemCfg.getCorridorIds(systemId)

      stopCorridors(corridors)

      // remove the actors that are not available anymore from the list
      corridorActorContainers = corridorActorContainers.filter(c ⇒ corridors.exists(_ == c.id))

      addCorridors(corridors)

      corridorActorContainers.foreach(c ⇒ {
        c.actorRef ! event
      })
  }

  // add new corridor actors to the list
  def addCorridors(corridors: Seq[String]) {
    corridors.foreach(id ⇒ {
      if (!corridorActorContainers.exists(_.id == id)) {
        corridorActorContainers = corridorActorContainers :+ createCorridorActorContainer(id)
      }
    })
  }

  // check if corridor still available if not stop the actor
  def stopCorridors(corridors: Seq[String]) {
    corridorActorContainers.foreach(c ⇒ {
      if (!corridors.exists(_ == c.id)) {
        context.stop(c.actorRef)
      }
    })
  }

  def createCorridorActorContainer(id: String): StepSystemState.this.type#CorridorActorContainer = {
    CorridorActorContainer(id, context.actorOf(Props(new StepCorridorState(id, curator, systemId, stateListener, initialTime = System.currentTimeMillis(),
      definition = definition, eventDispatcher = eventDispatcher)), "stepSystemState_" + systemId + "_" + id))
  }

  override def postStop() {
    corridorActorContainers.foreach(c ⇒ { context.stop(c.actorRef) })

    super.postStop()
  }
  override def preStart() {
    super.preStart()

    // if zookeeper has no translation maps, initialize with defaults
    initializeTranslationMaps()

    if (curator.exists(Paths.Systems.getSystemPath(systemId)) && !curator.exists(Paths.Systems.getLogEventsPath(systemId))) {
      curator.createEmptyPath(Paths.Systems.getLogEventsPath(systemId))
    }
    corridorActorContainers = systemCfg.getCorridorIds(systemId).map(id ⇒ {
      createCorridorActorContainer(id)
    })
  }

  case class CorridorActorContainer(id: String, actorRef: ActorRef)
}