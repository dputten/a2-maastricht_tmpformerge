/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.monitor

import akka.actor.{ ActorLogging, Actor }
import csc.sectioncontrol.messages.SystemEvent
import akka.util.duration._
import csc.dlog.QueueTaker

object Tick

/**
 * Actor to manage the monitor triggers
 *
 * @param systemId systemID only used for logging
 * @param queueTaker reference to the event queue
 * @param systemEventFunctions List of functions processing the new event
 * @param blockWaitDuration time between the processing of events
 */
class MonitorActor(systemId: String,
                   queueTaker: QueueTaker[SystemEvent],
                   systemEventFunctions: Seq[SystemEvent ⇒ Unit],
                   blockWaitDuration: Int = 100) extends Actor with ActorLogging {

  override def preStart() {
    super.preStart()
    queueTaker.start()

    //use a continue schedule to make sure never to lose a Tick message
    context.system.scheduler.schedule(blockWaitDuration millis, blockWaitDuration millis, self, Tick)
    log.info("System %s: Monitor Start reading events".format(systemId))
  }

  override def postStop() {
    super.postStop()
    queueTaker.close()
    log.info("System %s: Monitor Stop reading events".format(systemId))
  }

  def receive = {
    case Tick ⇒
      for (event ← queueTaker.take()) {
        systemEventFunctions.foreach(f ⇒ f(event))
      }
  }
}