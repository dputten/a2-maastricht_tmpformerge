/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.monitor.events

import csc.sectioncontrol.monitor._
import java.util.concurrent.atomic.AtomicReference
import csc.sectioncontrol.messages.SystemEvent
import csc.sectioncontrol.storage._
import scala.Some
import csc.sectioncontrol.storage.ZkFailure

/**
 * TODO: Separate functionality detect failures, detect alerts and request state changes
 * This can be refactored when supporting state per corridor
 */
object KnownEvent {
  def updateEventsToAlerts(addEventsToAlerts: List[EventAlertMapping]) {
    var eventToAlertMappings = eventsToAlerts.get()
    val newIdentifiers = addEventsToAlerts.map(_.eventIdentifier)
    while (!eventsToAlerts.compareAndSet(eventToAlertMappings, {
      val filter = eventToAlertMappings.filterNot(mapping ⇒ newIdentifiers.contains(mapping.eventIdentifier))
      filter ++ addEventsToAlerts
    })) {
      eventToAlertMappings = eventsToAlerts.get()
    }
  }
  def updateEventsToStates(addEventsToStates: List[EventStateMapping]) {
    var eventToStateMappings = eventsToStates.get()
    while (!eventsToStates.compareAndSet(eventToStateMappings, (eventToStateMappings ++ addEventsToStates).toSet.toList)) {
      eventToStateMappings = eventsToStates.get()
    }
  }
  val defaultEventsToStates = List(
    //Req 6 (map Alert to EnforceDegraded)
    //Req 15 (map SELFTEST-ERROR to Failure)
    EventStateMapping(SystemEventIdentifier(eventType = Some(SystemStateType.Off.toString)), SystemStateType.Off),
    EventStateMapping(SystemEventIdentifier(eventType = Some(SystemStateType.Maintenance.toString)), SystemStateType.Maintenance),
    EventStateMapping(SystemEventIdentifier(eventType = Some(SystemStateType.EnforceDegraded.toString)), SystemStateType.EnforceDegraded),
    EventStateMapping(SystemEventIdentifier(eventType = Some(SystemStateType.EnforceOff.toString)), SystemStateType.EnforceOff),
    EventStateMapping(SystemEventIdentifier(eventType = Some(SystemStateType.EnforceOn.toString)), SystemStateType.EnforceOn),
    EventStateMapping(SystemEventIdentifier(eventType = Some(SystemStateType.Failure.toString)), SystemStateType.Failure),
    EventStateMapping(SystemEventIdentifier(eventType = Some(SystemStateType.StandBy.toString)), SystemStateType.StandBy),
    EventStateMapping(SystemEventIdentifier(eventType = Some("Alert")), SystemStateType.EnforceDegraded),
    EventStateMapping(SystemEventIdentifier(eventType = Some("SELFTEST-ERROR")), SystemStateType.Failure),
    EventStateMapping(SystemEventIdentifier(eventType = Some("Alarm")), SystemStateType.Failure),
    EventStateMapping(SystemEventIdentifier(eventType = Some("OutOfRange")), SystemStateType.Failure),
    EventStateMapping(SystemEventIdentifier(eventType = Some("Open")), SystemStateType.Failure),
    EventStateMapping(SystemEventIdentifier(eventType = Some("JAI-CAMERA-CONNECTION-ERROR")), SystemStateType.EnforceDegraded),
    EventStateMapping(SystemEventIdentifier(eventType = Some("JAI-CAMERA-FTP-ERROR")), SystemStateType.EnforceDegraded),
    EventStateMapping(SystemEventIdentifier(eventType = Some("JAI-CAMERA-COMMUNICATION-ERROR")), SystemStateType.EnforceDegraded),
    EventStateMapping(SystemEventIdentifier(eventType = Some("JAI-CAMERA-HOUSING-OPEN")), SystemStateType.EnforceDegraded),
    EventStateMapping(SystemEventIdentifier(eventType = Some("JAI-CAMERA-TRIGGER-RETRIES")), SystemStateType.EnforceDegraded),
    EventStateMapping(SystemEventIdentifier(eventType = Some("JAI-INTERFACE-ERROR")), SystemStateType.EnforceDegraded),
    EventStateMapping(SystemEventIdentifier(eventType = Some("NTP-SYNC")), SystemStateType.Failure))
  val defaultEventsToAlerts = List[EventAlertMapping]()
  private val eventsToAlerts = new AtomicReference[List[EventAlertMapping]](defaultEventsToAlerts)
  private val eventsToStates = new AtomicReference[List[EventStateMapping]](defaultEventsToStates)

  def unapply(event: SystemEvent): Option[(ZkState, Option[SystemError])] = {
    val map = eventsToStates.get().map(mapping ⇒ (mapping.eventIdentifier -> mapping.stateType))
    val identifiers = map
    identifiers.foldLeft[Option[(SystemEventIdentifier, SystemStateType.Value)]](None) { (accu, next) ⇒
      (event, next._1) match {
        case ExtractSystemEventIdentifier(identifier) ⇒ Some((identifier, next._2))
        case _                                        ⇒ accu
      }
    }.map { case (i, stateType) ⇒ eventToState(stateType, event, i) }
  }

  def eventToState(stateType: SystemStateType.Value, event: SystemEvent, eventIdentifier: SystemEventIdentifier): (ZkState, Option[SystemError]) = {
    val state = ZkState(stateType.toString, event.timestamp, event.userId, event.reason)
    val error = eventToError(stateType, event, eventIdentifier)
    (state, error)
  }

  def eventToError(stateType: SystemStateType.Value, event: SystemEvent, eventIdentifier: SystemEventIdentifier): Option[SystemError] = {
    stateType match {
      case SystemStateType.Failure ⇒
        Some(ZkFailure(event.componentId, event.eventType, event.reason, event.timestamp, ConfigType.System))
      case SystemStateType.EnforceDegraded ⇒ eventToAlert(event, eventIdentifier)
      case _                               ⇒ None
    }
  }
  def eventToAlert(event: SystemEvent, eventIdentifier: SystemEventIdentifier): Option[SystemError] = {
    val map = eventsToAlerts.get().map(mapping ⇒ (mapping.eventIdentifier -> (mapping.configType, mapping.reductionFactor)))

    val identifiers = map
    identifiers.foldLeft[Option[(SystemEventIdentifier, (ConfigType.Value, Float))]](None) { (accu, next) ⇒
      (event, next._1) match {
        case ExtractSystemEventIdentifier(identifier) ⇒ Some((identifier, next._2))
        case _                                        ⇒ accu
      }
    }.map {
      case (i, (configType, reductionFactor)) ⇒ {
        val msg = new StringBuffer()
        msg.append("component: " + event.componentId)
        msg.append(" eventType: " + event.eventType)
        if (!event.reason.isEmpty) {
          msg.append(" notificatie reden: " + event.reason)
        }
        ZkAlert(event.componentId, event.eventType, msg.toString, event.timestamp, configType, reductionFactor)
      }
    }
      .orElse {
        Some(ZkAlert(event.componentId, event.eventType, "Reductie factor is onbekend, neem contact op met de administrator. \nNotificatie reden:" + event.reason +
          ",\neventType:" + event.eventType + ",\ncomponent:" + event.componentId + ",\ngebruiker:" + event.userId, event.timestamp, ConfigType.System, 0f))
      }
  }

}
