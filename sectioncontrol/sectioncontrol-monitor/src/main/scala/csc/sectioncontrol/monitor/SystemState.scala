/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.monitor

private[monitor] object SystemStateType extends Enumeration {
  type SystemStateType = Value
  val Off, StandBy, Maintenance, Failure, EnforceOn, EnforceOff, EnforceDegraded = Value

  val EnforceStates = Set(EnforceOn.toString, EnforceOff.toString, EnforceDegraded.toString)
}
