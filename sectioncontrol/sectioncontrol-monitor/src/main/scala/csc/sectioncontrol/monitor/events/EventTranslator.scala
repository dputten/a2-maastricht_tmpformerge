/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.monitor.events

import csc.config.Path
import csc.curator.utils.Curator

import csc.sectioncontrol.messages.SystemEvent
import csc.sectioncontrol.monitor._
import csc.sectioncontrol.storage.{ ConfigType, SystemError, ZkState }

/**
 * Translates events to system states, alerts and failures.
 * Reads the mapping from zookeeper.
 * Events that indicate a state change directly cannot be changed and are processed in StepSystemState.
 * The EventTranslator rereads zookeeper config every x minutes
 * @author Raymond Roestenburg
 */
private[monitor] trait EventTranslator {
  def log: akka.event.LoggingAdapter
  protected def curator: Curator
  private var lastVersion = -1L
  private var lastLoad = 0L
  val zkTranslationMapsPath = Path("/ctes") / "configuration" / "eventTranslationMaps"

  def initializeTranslationMaps() {
    if (!curator.exists(zkTranslationMapsPath)) {
      curator.put(zkTranslationMapsPath, new TranslationMaps(KnownEvent.defaultEventsToStates, KnownEvent.defaultEventsToAlerts, KnownPastAlertEvent.defaultEventsToPastAlerts, OffLineEvent.defaultOffLineEvents))
      curator.getVersioned[TranslationMaps](zkTranslationMapsPath).map(v ⇒ lastVersion = v.version)
      lastLoad = now
    }
  }
  /**
   * Translates a system event to possibly a SystemState and a SystemError
   * @param event event
   * @return maybe new state and maybe error causing the new state.
   */
  def translate(event: SystemEvent): Option[(ZkState, Option[SystemError])] = {
    event match {
      case KnownEvent(state, error) ⇒
        Some(state, error)
      case _ ⇒
        None
    }
  }
  def lastLoaded = lastLoad
  def loadTranslationMapsFromZookeeper() {
    curator.getVersioned[TranslationMaps](zkTranslationMapsPath).map { mapsVersion ⇒
      if (lastVersion != mapsVersion.version) {
        KnownEvent.updateEventsToAlerts(mapsVersion.data.eventsToAlerts)
        KnownEvent.updateEventsToStates(mapsVersion.data.eventsToStates)
        KnownPastAlertEvent.updateEventsToAlerts(mapsVersion.data.eventsToPastAlerts)
        OffLineEvent.updateEvents(mapsVersion.data.eventsToOffline)
        lastLoad = now
      }
    }
  }
  private def now = System.currentTimeMillis()
}

object ExtractSystemEventIdentifier {
  def unapply(eventAndIdentifier: (SystemEvent, SystemEventIdentifier)): Option[SystemEventIdentifier] = {
    val event = eventAndIdentifier._1
    val identifier = eventAndIdentifier._2

    def matchElement(identifierElement: String, eventElement: String): Boolean = {
      val Pattern = identifierElement.r
      eventElement match {
        case Pattern() ⇒ true
        case _         ⇒ false
      }
    }
    // if identifier says None, it means don't care, so matches, if Some, elements must match on pattern.
    val componentIdMatch = identifier.componentId.map(matchElement(_, event.componentId)).orElse(Some(true)).getOrElse(false)
    val eventTypeMatch = identifier.eventType.map(matchElement(_, event.eventType)).orElse(Some(true)).getOrElse(false)
    val reasonMatch = identifier.reason.map(matchElement(_, event.reason)).orElse(Some(true)).getOrElse(false)
    val tokenNameMatch = event.token.flatMap(token ⇒ identifier.tokenName.map(matchElement(_, token.token))
      .orElse(Some(true)))
      .orElse(Some(if (identifier.tokenName.isEmpty) true else false)).getOrElse(false)
    val lastTokenMatch = event.token.flatMap(token ⇒ identifier.lastToken.map(matchElement(_, token.last.toString))
      .orElse(Some(true)))
      .orElse(Some(if (identifier.lastToken.isEmpty) true else false)).getOrElse(false)
    if (componentIdMatch
      && eventTypeMatch
      && reasonMatch
      && tokenNameMatch
      && lastTokenMatch) {
      Some(SystemEventIdentifier(Some(event.componentId), Some(event.eventType), Some(event.reason), event.token.map(_.token), event.token.map(_.last.toString)))
    } else {
      None
    }
  }
}

private[monitor] case class SystemEventIdentifier(componentId: Option[String] = None,
                                                  eventType: Option[String] = None,
                                                  reason: Option[String] = None,
                                                  tokenName: Option[String] = None,
                                                  lastToken: Option[String] = None)

private[monitor] case class EventStateMapping(eventIdentifier: SystemEventIdentifier,
                                              stateType: SystemStateType.Value)
private[monitor] case class EventAlertMapping(eventIdentifier: SystemEventIdentifier, configType: ConfigType.Value, reductionFactor: Float)
private[monitor] case class EventPastAlertMapping(eventIdentifier: SystemEventIdentifier, configType: ConfigType.Value, reductionFactor: Float)

private[monitor] case class EventOfflineMapping(eventIdentifier: SystemEventIdentifier, eventName: String, setOffline: Boolean)

private[monitor] case class TranslationMaps(eventsToStates: List[EventStateMapping],
                                            eventsToAlerts: List[EventAlertMapping],
                                            eventsToPastAlerts: List[EventPastAlertMapping],
                                            eventsToOffline: List[EventOfflineMapping])

