/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.monitor

import csc.sectioncontrol.monitor.ScheduleMonitor.SystemEventDispatcher

import java.util.{ Date, Calendar, GregorianCalendar, TimeZone }
import akka.util.duration._
import akka.actor._
import csc.curator.utils.{ Curator }
import csc.sectioncontrol.storage._
import csc.sectioncontrol.messages.SystemEvent
import csc.sectioncontrol.i18n.Messages

case class CheckSchedules(corridorId: String, checkTime: Long)

/**
 * @param dataService
 * @param eventDispatcher alternative event dispatcher to override the default (for testing purposes)
 */
class ScheduleMonitor(dataService: DataService, eventDispatcher: Option[SystemEventDispatcher] = None)
  extends Actor with ActorLogging {

  override def preStart() {
    log.debug("ScheduleMonitor starting")
    super.preStart()
  }
  override def postStop() {
    log.debug("ScheduleMonitor stopping")
    scheduled.foreach(canc ⇒ if (!canc.isCancelled) canc.cancel())
    scheduled = None
    backupSchedules()
    super.postStop()
  }

  private def systemId = dataService.systemId

  protected def receive = {
    case "start" ⇒
      if (scheduled.isEmpty) {
        log.debug("ScheduleMonitor.receive start")
        val now = System.currentTimeMillis()
        checkSchedules(dataService.corridorId, now)
      }
    case "stop" ⇒
      log.debug("ScheduleMonitor.receive stop")
      scheduled.foreach(canc ⇒ if (!canc.isCancelled) canc.cancel())
      scheduled = None
      backupSchedules()
    case check: CheckSchedules ⇒ checkSchedules(check.corridorId, check.checkTime)
  }

  private val timeZone = TimeZone.getTimeZone("Europe/Amsterdam")
  private var scheduled: Option[Cancellable] = None

  private def setNextCheck(corridorId: String, checkTime: Long) {
    val now = System.currentTimeMillis()
    scheduled = Some(context.system.scheduler.scheduleOnce((checkTime - now).millis, self, CheckSchedules(corridorId, checkTime)))
    log.debug("ScheduleMonitor next check [%d msec] %s.".format((checkTime - now), (new Date(checkTime)).toString))
  }

  private[monitor] def checkSchedules(corridorId: String, checkTime: Long) {
    val intervals = loadScheduleIntervals(corridorId, checkTime)

    if (dataService.hasEnforceDetail) {
      log.debug("ScheduleMonitor enforce detail on. SystemEvent with SystemStateType.EnforceOff or SystemStateType.EnforceOn send.")
      if (intervals.find(i ⇒ i._1 <= checkTime && i._2 > checkTime).isEmpty) {
        sendEnforceOffline(checkTime, corridorId)
      } else {
        sendEnforceOnline(checkTime, corridorId)
      }
    } else {
      log.debug("ScheduleMonitor enforce detail off. No SystemEvent with SystemStateType.EnforceOff or SystemStateType.EnforceOn send.")
    }
    setNextCheck(corridorId, nextChangeTime(intervals, checkTime))
  }

  private[monitor] def backupSchedules() {
    val backupTime = System.currentTimeMillis()
    dataService.corridorSchedules(dataService.corridorId) match {
      case Some(currentSchedule) ⇒
        dataService.scheduleHistoryIds(dataService.corridorId).map(_.toLong).sorted.lastOption match {
          case None ⇒
            dataService.saveScheduleBackup(dataService.corridorId, currentSchedule, backupTime)
          case Some(lastBackup) ⇒
            val lastBackupSchedule = dataService.corridorScheduleHistory(dataService.corridorId, lastBackup.toString)
            if (lastBackupSchedule.isEmpty || currentSchedule != lastBackupSchedule.get) {
              dataService.saveScheduleBackup(dataService.corridorId, currentSchedule, backupTime)
            }
        }
      case None ⇒ ()
    }
  }

  private def sendEnforceOffline(time: Long, corridorId: String) {
    val component = "ScheduleMonitor-" + corridorId
    val event = SystemEvent(component,
      time,
      systemId,
      "NoActiveSchedule",
      "system",
      Some(Messages("csc.sectioncontrol.monitor.sendEnforceOffline")),
      corridorId = Some(corridorId))

    dispatch(event)
  }

  private def sendEnforceOnline(time: Long, corridorId: String) {
    val component = "ScheduleMonitor-" + corridorId
    val event = SystemEvent(component,
      time,
      systemId,
      "ActiveSchedule",
      "system",
      Some(Messages("csc.sectioncontrol.monitor.sendEnforceOnline")),
      corridorId = Some(corridorId))

    dispatch(event)
  }

  private[monitor] def loadScheduleIntervals(corridorId: String, day: Long): List[(Long, Long)] = {
    def addInterval(iList: List[(Long, Long)], iNew: (Long, Long)): List[(Long, Long)] = {
      iList match {
        case Nil ⇒ iNew :: Nil
        case iOld :: iRest ⇒
          if (iNew._1 <= iOld._2) {
            (iOld._1, iNew._2 max iOld._2) :: iRest
          } else {
            iNew :: iOld :: iRest
          }
      }
    }

    val schedules = dataService.corridorSchedules(corridorId).getOrElse(List())
    val intervals = schedules.map(s ⇒ (makeTimestamp(day, s.fromPeriodHour, s.fromPeriodMinute),
      makeTimestamp(day, s.toPeriodHour, s.toPeriodMinute))).sorted
    intervals.foldLeft(List[(Long, Long)]())(addInterval).reverse
  }

  private def makeTimestamp(day: Long, hour: Int, minute: Int): Long = {
    val cal = new GregorianCalendar(timeZone)
    cal.setTimeInMillis(day)
    cal.set(Calendar.HOUR_OF_DAY, hour)
    cal.set(Calendar.MINUTE, minute)
    cal.set(Calendar.SECOND, 0)
    cal.set(Calendar.MILLISECOND, 0)
    cal.getTimeInMillis
  }

  private[monitor] def nextChangeTime(intervals: List[(Long, Long)], baseTime: Long): Long = {
    def nextWithInterval(nextTime: Long, interval: (Long, Long)): Long = {
      if (interval._1 > baseTime && interval._1 < nextTime) {
        interval._1
      } else if (interval._2 > baseTime && interval._2 < nextTime) {
        interval._2
      } else {
        nextTime
      }
    }

    val nextMidnight = makeTimestamp(baseTime, 24, 0)
    val nextChangeTime = intervals.foldLeft(nextMidnight)(nextWithInterval)
    nextChangeTime
  }

  private def dispatch(event: SystemEvent): Unit = eventDispatcher match {
    case Some(dispatcher) ⇒ dispatcher(event)
    case _                ⇒ context.system.eventStream.publish(event)
  }

}

object ScheduleMonitor {
  type SystemEventDispatcher = Function[SystemEvent, Unit]

  def props(systemId: String, corridorId: String, curator: Curator, definition: Option[ZkSystemDefinition], eventDispatcher: Option[SystemEventDispatcher] = None) =
    Props(new ScheduleMonitor(new ZkDataService(systemId, corridorId, curator, definition), eventDispatcher))

}

trait DataService {

  def systemId: String
  //def corridorIds: Seq[String]
  def corridorId: String
  def hasEnforceDetail: Boolean
  def corridorSchedules(corridorId: String): Option[List[ZkSchedule]]
  def corridorScheduleHistory(corridorId: String, id: String): Option[List[ZkSchedule]]
  def scheduleHistoryIds(corridorId: String): Seq[String]
  def saveScheduleBackup(corridorId: String, schedule: List[ZkSchedule], backupTime: Long): Unit

}

class ZkDataService(val systemId: String, val corridorId: String, curator: Curator, definition: Option[ZkSystemDefinition]) extends DataService {

  private def scheduleHistoryPath(corridorId: String): String = Paths.Corridors.getScheduleHistoryPath(systemId, corridorId)

  private def scheduleHistoryPath(corridorId: String, id: String): String = scheduleHistoryPath(corridorId) + "/" + id

  //override def corridorIds: Seq[String] = curator.getChildNames(Paths.Corridors.getDefaultPath(systemId))

  // only sectioncontrol has different states for enforce.
  override def hasEnforceDetail: Boolean = definition match {
    case None         ⇒ false
    case Some(config) ⇒ config.enforceDetail
  }

  override def corridorSchedules(corridorId: String): Option[List[ZkSchedule]] =
    curator.get[List[ZkSchedule]](Paths.Corridors.getSchedulesPath(systemId, corridorId))

  override def corridorScheduleHistory(corridorId: String, id: String): Option[List[ZkSchedule]] =
    curator.get[List[ZkSchedule]](scheduleHistoryPath(corridorId, id))

  override def scheduleHistoryIds(corridorId: String): Seq[String] =
    curator.getChildNames(scheduleHistoryPath(corridorId))

  override def saveScheduleBackup(corridorId: String, schedule: List[ZkSchedule], backupTime: Long): Unit =
    curator.put(scheduleHistoryPath(corridorId, backupTime.toString), schedule)

}
