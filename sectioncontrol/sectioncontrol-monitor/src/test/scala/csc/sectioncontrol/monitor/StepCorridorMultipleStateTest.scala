package csc.sectioncontrol.monitor

import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.messages._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import com.typesafe.config.ConfigFactory
import akka.testkit.{ TestActorRef, TestProbe, TestKit }
import csc.config.Path
import akka.actor.{ ActorRef, ActorSystem }
import csc.sectioncontrol.storage._
import csc.curator.CuratorTestServer
import csc.curator.utils.Curator
import csc.akkautils.DirectLogging
import net.liftweb.json.DefaultFormats
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.storage.ZkCorridorInfo
import scala.Some
import csc.sectioncontrol.storagelayer.SpeedFixedConfig
import csc.sectioncontrol.storage.ZkDirection
import csc.sectioncontrol.storage.ZkSection
import csc.sectioncontrol.storage.ZkUser
import csc.sectioncontrol.storage.ZkCorridor

/**
 * Companion for the test
 */
object StepCorridorMultipleStateTest {
  val config = ConfigFactory.parseString("""
                  akka.loglevel = INFO
                  event-handlers = ["csc.akkautils.Slf4jEventHandler"]
                  akka.actor.debug {
                    receive = on
                    lifecycle = on
                  }
                                         """)
  val system = ActorSystem("StepCorridorMultipleStateTest", config)
}

class StepCorridorMultipleStateTest(_system: ActorSystem) extends TestKit(_system) with WordSpec with MustMatchers
  with CuratorTestServer with BeforeAndAfterAll with DirectLogging {
  private var zkStore: Curator = _

  def this() = this(StepCorridorMultipleStateTest.system)

  def doEnforceStateTest(probe: TestProbe, stepper: ActorRef, theTime: Long) {
    stepper ! SystemEvent("web", theTime, "a2", SystemStateType.StandBy.toString, "testUser", None, Some("X1"), None)
    stepper ! SystemEvent("web", theTime, "a2", SystemStateType.EnforceOn.toString, "testUser", None, Some("X1"), None)

    stepper ! SystemEvent("camera", theTime, "a2", SystemStateType.StandBy.toString, "testUser", None, Some("X1"), None)
    stepper ! SystemEvent("camera", theTime, "a2", SystemStateType.EnforceOn.toString, "testUser", None, Some("X1"), None)

    Thread.sleep(3000)
    val messages = probe.receiveN(4).map(_.asInstanceOf[ZkState].state)
    messages.count(_ == "Off") must be(2)
    messages.count(_ == "StandBy") must be(2)
  }

  def time = System.currentTimeMillis()
  "The StepCorridorMultipleStateTest" must {
    "Return two messages when one gantry is used by two corridors " in {
      val probe = TestProbe()
      val stepper = TestActorRef(new StepSystemState(zkStore, "a2", Some(probe.ref)))
      val theTime = time

      doEnforceStateTest(probe, stepper, theTime)

      stepper ! SystemEvent("selftest", theTime, "a2", "SELFTEST-SUCCESS", "testUser", Some("StandBy"), None, Some("X1"), None)
      val messages = probe.receiveN(2).map(_.asInstanceOf[ZkState].state)
      messages(0) must be(SystemStateType.EnforceOn.toString)
      messages(1) must be(SystemStateType.EnforceOn.toString)
      //sender
      system.stop(stepper)
    }

    "Return two messages when one gantry is used by two corridors and " +
      "one messages after one corridor is deleted and " +
      "two messages after one corridor is added again" in {
        val probe = TestProbe()
        val stepper = TestActorRef(new StepSystemState(zkStore, "a2", Some(probe.ref)))
        val theTime = time

        doEnforceStateTest(probe, stepper, theTime)

        stepper ! SystemEvent("selftest", theTime, "a2", "SELFTEST-SUCCESS", "testUser", Some("StandBy"), None, Some("X1"), None)
        var messages = probe.receiveN(2).map(_.asInstanceOf[ZkState].state)
        messages(0) must be(SystemStateType.EnforceOn.toString)
        messages(1) must be(SystemStateType.EnforceOn.toString)

        // Remove one corridor
        zkStore.deleteRecursive(Paths.Corridors.getRootPath("a2", "corridor2"))

        stepper ! SystemEvent("selftest", theTime, "a2", SystemStateType.Failure.toString, "testUser", Some("StandBy"), None, Some("X1"), None)
        messages = probe.receiveN(1).map(_.asInstanceOf[ZkState].state)
        messages(0) must be(SystemStateType.Failure.toString)

        val failPath = Paths.Failure.getCurrentPath("a2", "corridor3")
        val failures = zkStore.getChildren(failPath)
        failures.size must be(1)
        stepper ! new SystemEvent("web", theTime, "a2", "FailureSignOff", "user1", "", None, None, Some(failures.head), Some("corridor3"), None, None)

        val stateUpdate = probe.expectMsgType[ZkState]
        stateUpdate.state must be("StandBy")

        //add one corridor
        val config2Path = Path(Paths.Corridors.getConfigPath("a2", "corridor2"))
        zkStore.createEmptyPath(config2Path)
        zkStore.set(config2Path, new ZkCorridor(id = "corridor2",
          name = "second corridor",
          roadType = 0,
          serviceType = ServiceType.SpeedFixed,
          caseFileType = ZkCaseFileType.HHMVS40,
          isInsideUrbanArea = true,
          dynamaxMqId = "",
          approvedSpeeds = Seq(),
          pshtm = null,
          info = new ZkCorridorInfo(corridorId = 20,
            locationCode = "1",
            reportId = "TCS A2 Links  39.7 - 54.9",
            reportHtId = "A020011",
            reportPerformance = true,
            locationLine1 = "loc 2"),
          endSectionId = "S1",
          startSectionId = "S1",
          allSectionIds = Seq("S1"),
          direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
          radarCode = 402,
          roadCode = 2,
          dutyType = "KA2",
          deploymentCode = "03",
          codeText = ""), 0)
        val rootPathToServices2 = Path(Paths.Corridors.getServicesPath("a2", "corridor2"))
        val pathToService2 = rootPathToServices2 / ServiceType.SectionControl.toString
        zkStore.createEmptyPath(pathToService2)
        zkStore.set(pathToService2, new SpeedFixedConfig(), 0)

        stepper ! SystemEvent("selftest", theTime, "a2", SystemStateType.Failure.toString, "testUser", Some("StandBy"), None, Some("X1"), None)

        Thread.sleep(3000)
        messages = probe.receiveN(2).map(_.asInstanceOf[ZkState].state)
        // expect an off from corridor 2 the newly created. (acceptFailureInOffMode=false) so no more
        // expect an failure from corridor3
        system.stop(stepper)
      }
  }

  def checkLogEventTypesAlphabetically(alphabeticOrderedListOfStates: List[String]) {
    val logEntries = zkStore.getChildren(Path("/ctes/systems/a2/log-events"))
    val stateChanges = logEntries.flatMap { p ⇒ val se = zkStore.get[SystemEvent](p); log.info(se.toString); se }.filter(_.eventType == "SystemStateChange")
    stateChanges.size must be(alphabeticOrderedListOfStates.size)
    //test in alphabetic order
    stateChanges.map(_.reason).toList.sortWith(_ < _) must be(alphabeticOrderedListOfStates)
  }

  private def current = { "current" }
  private def failures = { "failures" }
  private def alerts = { "alerts" }
  private def errors = { "errors" }
  private def control = { "control" }

  override def beforeEach() {
    super.beforeEach()

    val formats = DefaultFormats + new EnumerationSerializer(
      ZkWheelbaseType,
      VehicleCode,
      ZkIndicationType,
      ServiceType,
      ZkCaseFileType,
      EsaProviderType,
      VehicleImageType,
      ConfigType)

    zkStore = CuratorHelper.create(this, clientScope, formats = formats)
    zkStore.createEmptyPath(Path("/destroy"))
    zkStore.createEmptyPath(Path("/ctes") / "systems" / "a2")
    zkStore.createEmptyPath(Path("/ctes") / "systems" / "a2" / "corridors" / "corridor2" / control / errors / failures / current)
    zkStore.createEmptyPath(Path("/ctes") / "systems" / "a2" / "corridors" / "corridor3" / control / errors / failures / current)
    zkStore.createEmptyPath(Path("/ctes") / "systems" / "a2" / "corridors" / "corridor2" / control / errors / alerts / current)
    zkStore.createEmptyPath(Path("/ctes") / "systems" / "a2" / "corridors" / "corridor3" / control / errors / alerts / current)
    zkStore.createEmptyPath(Path("/ctes") / "users")
    zkStore.createEmptyPath(Path("/ctes") / "configuration")
    zkStore.put(Path("/ctes") / "users" / "testUser", new ZkUser("testUser", "testReportingOfficer", "", "", "", "", "", "", "", List()))
    zkStore.put(Path("/ctes") / "users" / "testUser1", new ZkUser("testUser1", "testReportingOfficer", "", "", "", "", "", "", "", List()))

    val config2Path = Path(Paths.Corridors.getConfigPath("a2", "corridor2"))
    zkStore.createEmptyPath(config2Path)
    zkStore.set(config2Path, new ZkCorridor(id = "corridor2",
      name = "second corridor",
      roadType = 0,
      serviceType = ServiceType.SpeedFixed,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = true,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = null,
      info = new ZkCorridorInfo(corridorId = 20,
        locationCode = "1",
        reportId = "TCS A2 Links  39.7 - 54.9",
        reportHtId = "A020011",
        reportPerformance = true,
        locationLine1 = "loc 2"),
      endSectionId = "S1",
      startSectionId = "S1",
      allSectionIds = Seq("S1"),
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 402,
      roadCode = 2,
      dutyType = "KA2",
      deploymentCode = "03",
      codeText = ""), 0)
    val rootPathToServices2 = Path(Paths.Corridors.getServicesPath("a2", "corridor2"))
    val pathToService2 = rootPathToServices2 / ServiceType.SectionControl.toString
    zkStore.createEmptyPath(pathToService2)
    zkStore.set(pathToService2, new SpeedFixedConfig(), 0)

    val section1 = ZkSection(id = "S1",
      systemId = "a2",
      name = "S1",
      length = 8000,
      startGantryId = "E1",
      endGantryId = "X1",
      matrixBoards = Seq(), msiBlackList = Seq())
    zkStore.put(Paths.Sections.getConfigPath("a2", "S1"), section1)

    // ---------------------------------------------------------------------------

    val config3Path = Path(Paths.Corridors.getConfigPath("a2", "corridor3"))
    zkStore.createEmptyPath(config3Path)
    zkStore.set(config3Path, new ZkCorridor(id = "corridor3",
      name = "third corridor",
      roadType = 0,
      serviceType = ServiceType.SpeedFixed,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = true,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = null,
      info = new ZkCorridorInfo(corridorId = 30,
        locationCode = "1",
        reportId = "TCS A2 Links  39.7 - 54.9",
        reportHtId = "A020011",
        reportPerformance = true,
        locationLine1 = "loc 2"),
      startSectionId = "S2",
      endSectionId = "S2",
      allSectionIds = Seq("S2"),
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 402,
      roadCode = 2,
      dutyType = "KA2",
      deploymentCode = "03",
      codeText = ""), 0)
    val rootPathToServices3 = Path(Paths.Corridors.getServicesPath("a2", "corridor3"))
    val pathToService3 = rootPathToServices3 / ServiceType.SectionControl.toString
    zkStore.createEmptyPath(pathToService3)
    zkStore.set(pathToService3, new SpeedFixedConfig(), 0)

    val section2 = ZkSection(id = "S2",
      systemId = "a2",
      name = "S2",
      length = 8000,
      startGantryId = "X1",
      endGantryId = "X2",
      matrixBoards = Seq(), msiBlackList = Seq())
    zkStore.put(Paths.Sections.getConfigPath("a2", "S2"), section2)

    val camera = CameraConfig("a", "a", 1, 1, 1)
    val radar = RadarConfig("a", 1, 1, 1)
    val pir = PIRConfig("a", 1, 1, 1, "a", 1)
    val decorate = None
    val newLane = ZkLaneConfig("a1", "lane", Some("qaz"), camera, radar, pir, decorate)
    zkStore.put(Paths.Gantries.getLaneRootPath("a2", "E2", "a1") + "/" + "config", newLane)
  }

  override protected def afterAll() {
    system.shutdown()
    super.afterAll()
  }
}
