/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.monitor

import events._
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.messages.{ VehicleImageType, EsaProviderType, VehicleCode, SystemEvent }
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import akka.testkit.{ TestProbe, TestKit }
import akka.util.duration._
import csc.config.Path
import akka.actor.{ ActorRef, ActorSystem }
import csc.sectioncontrol.i18n.Messages
import akka.actor.Props
import csc.sectioncontrol.storage._
import csc.curator.CuratorTestServer
import csc.curator.utils.{ CuratorToolsImpl, Curator }
import csc.akkautils.DirectLogging
import csc.sectioncontrol.storagelayer.calibrationstate.CalibrationStateService
import csc.sectioncontrol.monitor.events.SystemEventIdentifier
import csc.sectioncontrol.storage.ZkCalibrationState
import csc.sectioncontrol.storage.ZkSystemDefinition
import csc.sectioncontrol.monitor.events.EventAlertMapping
import scala.Some
import csc.sectioncontrol.monitor.events.TranslationMaps
import net.liftweb.json.DefaultFormats
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.monitor.events.EventPastAlertMapping
import csc.sectioncontrol.storagelayer.{ SpeedFixedConfig, RedLightConfig }

/**
 * Test for StepSystemState
 * With state changes when the enforce details are turn off
 */
class StepSystemStateEnforceTest() extends TestKit(ActorSystem("StepSystemStateEnforceTest")) with WordSpec
  with MustMatchers with CuratorTestServer with BeforeAndAfterAll with DirectLogging {
  private var zkStore: Curator = _

  def time = System.currentTimeMillis()
  "The StepSystemStateTest" must {

    "stay in EnforceOn when an Alert occurs, move back to EnforceOn after user request to EnforceOn with Alert not solved" in {
      val probe = TestProbe()
      val stepper = system.actorOf(Props(new StepSystemState(zkStore, "a2", Some(probe.ref), definition = new ZkSystemDefinition(enforceDetail = false))))
      val theTime = time
      doEnforceStateTest(probe, stepper, theTime)
      probe.expectNoMsg(200 millis)
      stepper ! SystemEvent("camera", theTime, "a2", "Alert", "testUser", Some("corridor1"), None, None)
      probe.expectNoMsg(200 millis)

      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.StandBy.toString, "user4", Some("corridor1"), None, None)
      stepper ! SystemEvent("selftest", theTime, "a2", "SELFTEST-SUCCESS", "user4", Some("EnforceOn"), Some("corridor1"), None, None)
      probe.expectMsgPF() {
        case s: ZkState if s.state == "StandBy" ⇒ s.userId must be("user4")
        case s: ZkState                         ⇒ fail("unexpected msg => " + s)
      }
      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.EnforceOn.toString, "user4", Some("corridor1"), None, None)
      stepper ! SystemEvent("selftest", theTime, "a2", "SELFTEST-SUCCESS", "user4", Some("StandBy"), Some("corridor1"), None, None)
      probe.expectMsgPF() {
        case s: ZkState if s.state == "EnforceOn" ⇒ s.userId must be("user4")
        case s: ZkState                           ⇒ fail("unexpected msg => " + s)
      }

      system.stop(stepper) //stepper ! PoisonPill
    }

    "ignore a DoorOpen event and go to failure when destroyData = false" in {
      val probe = TestProbe()
      val stepper = system.actorOf(Props(new StepSystemState(zkStore, "a2", Some(probe.ref), definition = new ZkSystemDefinition(destroyData = false, enforceDetail = false))))
      val theTime = time
      doEnforceStateTest(probe, stepper, theTime)
      probe.expectNoMsg(200 millis)
      stepper ! SystemEvent("web", theTime, "a2", "Open", "user1", Some("corridor1"), None, None)
      val messages = probe.receiveN(1).map(_.asInstanceOf[ZkState].state)
      messages(0) must be("Failure")

      system.stop(stepper) //stepper ! PoisonPill
    }

    "start in the Off state move to StandBy and receive DoorOpen and solve failure" in {
      val probe = TestProbe()
      val stepper = system.actorOf(Props(new StepSystemState(zkStore, "a2", Some(probe.ref), definition = new ZkSystemDefinition(enforceDetail = false))))
      val theTime = time
      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.StandBy.toString, "user1", Some("corridor1"), None, None)
      stepper ! SystemEvent("web", theTime, "a2", "Open", "user1", Some("corridor1"), None, None)
      val messages = probe.receiveN(4).map(_.asInstanceOf[ZkState].state)
      messages.count(_ == "Off") must be(2)
      messages.count(_ == "StandBy") must be(1)
      messages.count(_ == "Failure") must be(1)

      val failures = zkStore.getChildren(Paths.Failure.getCurrentPath("a2", "corridor1"))
      failures.size must be(1)

      stepper ! new SystemEvent("web", theTime, "a2", "FailureSignOff", "user1", "", None, None, Some(failures.head), Some("corridor1"), None, None)

      val stateUpdate = probe.expectMsgType[ZkState]
      stateUpdate.state must be("StandBy")

      system.stop(stepper) //stepper ! PoisonPill
    }

    "start in the Off state and move to StandBy" in {
      val probe = TestProbe()
      val stepper = system.actorOf(Props(new StepSystemState(zkStore, "a2", Some(probe.ref), definition = new ZkSystemDefinition(enforceDetail = false))))
      val theTime = time
      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.StandBy.toString, "user1", Some("corridor1"), None, None)
      val messages = probe.receiveN(3).map(_.asInstanceOf[ZkState].state)
      messages.count(_ == "Off") must be(2)
      messages.count(_ == "StandBy") must be(1)
      system.stop(stepper) //stepper ! PoisonPill
    }

    "start in the Off state and stay there when incorrect State requested (anything other than StandBy)" in {
      val probe = TestProbe()
      val stepper = system.actorOf(Props(new StepSystemState(zkStore, "a2", Some(probe.ref), definition = new ZkSystemDefinition(enforceDetail = false))))
      val theTime = time
      probe.ignoreMsg {
        case Off() ⇒ true
        case _     ⇒ false
      }
      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.EnforceDegraded.toString, "user1")
      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.EnforceOff.toString, "user1")
      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.EnforceOn.toString, "user1")
      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.Failure.toString, "user1")
      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.Off.toString, "user1")
      probe.expectNoMsg()
      system.stop(stepper) //stepper ! PoisonPill
    }

    "start in the Off state, move to StandBy, move to Failure, stay there until FailureSolved" in {
      val probe = TestProbe()
      val stepper = system.actorOf(Props(new StepSystemState(zkStore, "a2", Some(probe.ref), definition = new ZkSystemDefinition(enforceDetail = false))))
      val theTime = time
      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.StandBy.toString, "user1", Some("corridor1"), None, None)
      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.Failure.toString, "user1", Some("corridor1"), None, None)
      val messages = probe.receiveN(4).map(_.asInstanceOf[ZkState].state)
      messages.count(_ == "Off") must be(2)
      messages.count(_ == "StandBy") must be(1)
      messages.count(_ == "Failure") must be(1)

      val failures = zkStore.getChildren(Paths.Failure.getCurrentPath("a2", "corridor1"))
      failures.size must be(1)
      stepper ! new SystemEvent("web", theTime, "a2", "FailureSignOff", "user1", "", None, None, Some(failures.head), Some("corridor1"), None, None)

      val stateUpdate = probe.expectMsgType[ZkState]
      stateUpdate.state must be("StandBy")

      system.stop(stepper) //stepper ! PoisonPill
    }

    "start in the Off state, move to StandBy, move to Enforce after Selftest success" in {
      val probe = TestProbe()
      val stepper = system.actorOf(Props(new StepSystemState(zkStore, "a2", Some(probe.ref), definition = new ZkSystemDefinition(enforceDetail = false))))
      val theTime = time
      doEnforceStateTest(probe, stepper, theTime)
      system.stop(stepper) //stepper ! PoisonPill
    }

    "start in the Off state, move to StandBy, move to Failure when a failure occurs, according to Req 5" in {
      val probe = TestProbe()
      val stepper = system.actorOf(Props(new StepSystemState(zkStore, "a2", Some(probe.ref), definition = new ZkSystemDefinition(enforceDetail = false))))
      val theTime = time
      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.StandBy.toString, "user1", Some("corridor1"), None, None)
      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.Failure.toString, "user1", Some("corridor1"), None, None)
      val messages = probe.receiveN(4).map(_.asInstanceOf[ZkState].state)
      messages.count(_ == "Off") must be(2)
      messages.count(_ == "StandBy") must be(1)
      messages.count(_ == "Failure") must be(1)
      system.stop(stepper) //stepper ! PoisonPill
    }

    "stay in EnforceOn when an Alert occurs" in {
      val probe = TestProbe()
      val stepper = system.actorOf(Props(new StepSystemState(zkStore, "a2", Some(probe.ref), definition = new ZkSystemDefinition(enforceDetail = false))))
      val theTime = time
      doEnforceStateTest(probe, stepper, theTime)
      stepper ! SystemEvent("camera", theTime, "a2", "Alert", "alertUser", Some("corridor1"), None, None)
      probe.expectNoMsg(200 millis)
      system.stop(stepper) //stepper ! PoisonPill
    }

    "stay in EnforceOn, when an Alert occurs and stay in EnforceOn when all alerts are resolved" in {
      val probe = TestProbe()
      val stepper = system.actorOf(Props(new StepSystemState(zkStore, "a2", Some(probe.ref), definition = new ZkSystemDefinition(enforceDetail = false))))
      val theTime = time
      doEnforceStateTest(probe, stepper, theTime)
      stepper ! SystemEvent("camera", theTime, "a2", "Alert", "alertUser", Some("corridor1"), None, None)
      probe.expectNoMsg(200 millis)

      //remove all alerts from zookeeper
      val children = zkStore.getChildren(Path("/ctes") / "systems" / "a2" / "alerts")
      children.foreach(zkStore.delete(_))
      stepper ! SystemEvent("web", theTime, "a2", "AlertSolved", "user4", Some("corridor1"), None, None)
      probe.expectNoMsg(200 millis)
      system.stop(stepper) //stepper ! PoisonPill
    }

    "stay in EnforceOn, when receiving a PastAlert" in {
      val probe = TestProbe()
      val stepper = system.actorOf(Props(new StepSystemState(zkStore, "a2", Some(probe.ref), definition = new ZkSystemDefinition(enforceDetail = false))))
      val theTime = time
      doEnforceStateTest(probe, stepper, theTime)
      Thread.sleep(100)
      val path = Path("/ctes/configuration/eventTranslationMaps")
      zkStore.getVersioned[TranslationMaps](path).map {
        voMaps ⇒
          {
            log.info("VERSION: " + voMaps.version)
            zkStore.set(path.toString(), voMaps.data.copy(eventsToPastAlerts =
              List(EventPastAlertMapping(SystemEventIdentifier(eventType = Some("WasDown")), ConfigType.Lane, 0.3f))), voMaps.version)
          }
      }
      stepper ! ReloadTranslationMaps(0) //todo:test
      stepper ! SystemEvent("camera", theTime, "a2", "WasDown", "pastAlertUser", Some("corridor1"), None, None)
      probe.expectNoMsg(200 millis)
      system.stop(stepper) //stepper ! PoisonPill
    }

    "Stay in EnforceOn" in {
      val probe = TestProbe()
      val stepper = system.actorOf(Props(new StepSystemState(zkStore, "a2", Some(probe.ref), definition = new ZkSystemDefinition(enforceDetail = false))))
      val theTime = time
      doEnforceStateTest(probe, stepper, theTime)
      stepper ! SystemEvent("camera", theTime, "a2", "EnforceOff", "offUser", Some("corridor1"), None, None)
      probe.expectNoMsg(200 millis)
      system.stop(stepper) //stepper ! PoisonPill
    }

    "Stay in enforce, when receiving EnforceOff" in {
      val probe = TestProbe()
      val stepper = system.actorOf(Props(new StepSystemState(zkStore, "a2", Some(probe.ref), definition = new ZkSystemDefinition(enforceDetail = false))))
      val theTime = time
      doEnforceStateTest(probe, stepper, theTime)
      stepper ! SystemEvent("camera", theTime, "a2", "Alert", "camera", Some("corridor1"), None, None)
      probe.expectNoMsg(200 millis)
      stepper ! SystemEvent("web", theTime, "a2", "EnforceOff", "offUser", Some("corridor1"), None, None)
      probe.expectNoMsg(200 millis)
      stepper ! SystemEvent("web", theTime, "a2", "EnforceOn", "degradedUser", Some("corridor1"), None, None)
      probe.expectNoMsg(200 millis)

      checkLogEventTypesAlphabetically(List(
        translateStates("Off", "Off"),
        translateStates("Off", "Off"),
        translateStates("Off", "StandBy"),
        translateStates("StandBy", "EnforceOn")))

      system.stop(stepper) //stepper ! PoisonPill
    }

    "move to EnforceOn, from StandBy, if an Alert was received previously, and EnforceOn was received" in {
      val probe = TestProbe()
      val stepper = system.actorOf(Props(new StepSystemState(zkStore, "a2", Some(probe.ref), definition = new ZkSystemDefinition(enforceDetail = false))))
      val theTime = time
      // send an alert, while in Off
      stepper ! SystemEvent("camera", theTime, "a2", "Alert", "camera", Some("corridor1"), None, None)
      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.StandBy.toString, "user1", Some("corridor1"), None, None)
      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.EnforceOn.toString, "user1", Some("corridor1"), None, None)
      val messages = probe.receiveN(3).map(_.asInstanceOf[ZkState].state)
      messages.count(_ == "Off") must be(2)
      messages.count(_ == "StandBy") must be(1)

      stepper ! SystemEvent("selftest", theTime, "a2", "SELFTEST-SUCCESS", "testUser", Some("StandBy"), Some("corridor1"), None, None)
      probe.expectMsgPF() {
        case s: ZkState if s.state == "EnforceOn" ⇒ s.userId must be("testUser")
        case _                                    ⇒ fail("unexpected msg")
      }
      //alphabetic order to ease testing
      checkLogEventTypesAlphabetically(
        List(
          translateStates("Off", "Off"),
          translateStates("Off", "Off"),
          translateStates("Off", "StandBy"),
          translateStates("StandBy", "EnforceOn")))

      system.stop(stepper) //stepper ! PoisonPill
    }

    "move from Off to Alarm, when moving to StandBy and an alarm was received while Off" in {
      val probe = TestProbe()
      val stepper = system.actorOf(Props(new StepSystemState(zkStore, "a2", Some(probe.ref), definition = new ZkSystemDefinition(enforceDetail = false))))
      val theTime = time
      stepper ! SystemEvent("ntp", theTime, "a2", "Failure", "NTP", Some("corridor1"), None, None)
      probe.expectMsgPF() {
        case s: ZkState if s.state == "Off" ⇒ s.userId must be("system")
        case _                              ⇒ fail("unexpected msg")
      }

      probe.expectMsgPF() {
        case s: ZkState if s.state == "Off" ⇒ s.userId must be("system")
        case _                              ⇒ fail("unexpected msg")
      }

      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.StandBy.toString, "user1", Some("corridor1"), None, None)
      probe.expectMsgPF() {
        case s: ZkState if s.state == "Failure" ⇒ s.userId must be("user1")
        case _                                  ⇒ fail("unexpected msg")
      }
      system.stop(stepper) //stepper ! PoisonPill
    }

    "set the calibration status correctly when the selftest is started and ended" in {
      val probe = TestProbe()
      val stepper = system.actorOf(Props(new StepSystemState(zkStore, "a2", Some(probe.ref), definition = new ZkSystemDefinition(enforceDetail = false))))
      val theTime = time
      stepper ! SystemEvent("web", theTime, "a2", "SELFTEST-START", "adhoc", Some("corridor1"), None, None)
      stepper ! SystemEvent("web", theTime, "a2", "SELFTEST-END", "adhoc", Some("corridor1"), None, None)
      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.StandBy.toString, "user1", Some("corridor1"), None, None)
      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.EnforceOn.toString, "user1", Some("corridor1"), None, None)

      val messages = probe.receiveN(3).map(_.asInstanceOf[ZkState].state)
      messages.count(_ == "Off") must be(2)
      messages.count(_ == "StandBy") must be(1)

      Thread.sleep(4000)
      val calState = CalibrationStateService.getTriggerData("a2", "corridor1", zkStore)
      calState.get.userId must be("user1")

      system.stop(stepper) //stepper ! PoisonPill
    }

    "log when the states transition" in {
      val probe = TestProbe()
      val stepper = system.actorOf(Props(new StepSystemState(zkStore, "a2", Some(probe.ref), definition = new ZkSystemDefinition(enforceDetail = false))))
      val theTime = time
      stepper ! SystemEvent("web", theTime, "a2", "SELFTEST-START", "adhoc", Some("corridor1"), None, None)
      stepper ! SystemEvent("web", theTime, "a2", "SELFTEST-END", "adhoc", Some("corridor1"), None, None)
      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.StandBy.toString, "user1", Some("corridor1"), None, None)
      val messages = probe.receiveN(3).map(_.asInstanceOf[ZkState].state)
      messages.count(_ == "Off") must be(2)
      messages.count(_ == "StandBy") must be(1)

      //alphabetic order to ease testing
      checkLogEventTypesAlphabetically(
        List(
          translateStates("Off", "Off"),
          translateStates("Off", "Off"),
          translateStates("Off", "StandBy")))
      system.stop(stepper)
    }

    "reload translationmaps and have effect on reducionfactor and configtype" in {
      //clean alerts
      zkStore.getChildren(Paths.Alert.getCurrentPath("a2", "corridor1")).foreach {
        path ⇒ zkStore.delete(path)
      }
      zkStore.getChildren(Paths.Alert.getCurrentPath("a2", "corridor1")).size must be(0)

      val probe = TestProbe()
      val stepper = system.actorOf(Props(new StepSystemState(zkStore, "a2", Some(probe.ref), definition = new ZkSystemDefinition(enforceDetail = false))))
      val theTime = time
      Thread.sleep(100)
      val path = Path("/ctes/configuration/eventTranslationMaps")
      zkStore.getVersioned[TranslationMaps](path).map {
        voMaps ⇒
          {
            log.info("VERSION: " + voMaps.version)
            zkStore.set(path, voMaps.data.copy(eventsToAlerts = List(EventAlertMapping(SystemEventIdentifier(componentId =
              Some(".*camera")), ConfigType.Lane, 0.0f))), voMaps.version)
          }
      }
      stepper ! ReloadTranslationMaps(0) //todo:test

      stepper ! SystemEvent("camera", theTime, "a2", "Alert", "camera", Some("corridor1"), None, None)
      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.StandBy.toString, "user1", Some("corridor1"), None, None)
      val messages = probe.receiveN(3).map(_.asInstanceOf[ZkState].state)
      messages.count(_ == "Off") must be(2)
      messages.count(_ == "StandBy") must be(1)

      zkStore.getChildren(Paths.Alert.getCurrentPath("a2", "corridor1")).size must be(1)

      zkStore.getChildren(Paths.Alert.getCurrentPath("a2", "corridor1")).map {
        path ⇒
          zkStore.get[ZkAlert](path).get.reductionFactor must be(0f)
          zkStore.delete(path)
      }.size must be(1)
      zkStore.getVersioned[TranslationMaps](path).map {
        voMaps ⇒
          {
            zkStore.set(path, voMaps.data.copy(eventsToAlerts = List(EventAlertMapping(
              SystemEventIdentifier(componentId = Some(".*camera")), ConfigType.Lane, 0.5f))), voMaps.version)
          }
      }
      stepper ! ReloadTranslationMaps(0)
      stepper ! SystemEvent("camera", theTime, "a2", "Alert", "camera", Some("corridor1"), None, None)
      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.Off.toString, "user1", Some("corridor1"), None, None)

      probe.expectMsgPF() {
        case ZkState("Off", _, _, _) ⇒
        case msg: Any                ⇒ fail("unexpected msg " + msg)
      }

      zkStore.getChildren(Paths.Alert.getCurrentPath("a2", "corridor1")).map {
        path ⇒
          zkStore.get[ZkAlert](path).foreach {
            alert ⇒
              alert.reductionFactor must be(0.5f)
              alert.configType must be(ConfigType.Lane)
          }
          zkStore.delete(path)
      }.size must be(1)
      system.stop(stepper)
    }
  }

  private def translateState(state: String) = Messages("csc.sectioncontrol.monitor.state." + state)
  private def translateStates(from: String, to: String) = Messages("csc.sectioncontrol.monitor.statechange", translateState(from), translateState(to))

  def checkLogEventTypesAlphabetically(alphabeticOrderedListOfStates: List[String]) {
    val logEntries = zkStore.getChildren(Path("/ctes/systems/a2/log-events"))
    val stateChanges = logEntries.flatMap { p ⇒ val se = zkStore.get[SystemEvent](p); log.info(se.toString); se }.filter(_.eventType == "SystemStateChange")
    stateChanges.size must be(alphabeticOrderedListOfStates.size)
    //test in alphabetic order
    stateChanges.map(_.reason).toList.sortWith(_ < _) must be(alphabeticOrderedListOfStates)
  }

  def doEnforceStateTest(probe: TestProbe, stepper: ActorRef, theTime: Long) {
    stepper ! SystemEvent("web", theTime, "a2", SystemStateType.StandBy.toString, "user1", Some("corridor1"), None, None)
    stepper ! SystemEvent("web", theTime, "a2", SystemStateType.EnforceOn.toString, "user1", Some("corridor1"), None, None)
    val messages = probe.receiveN(3).map(_.asInstanceOf[ZkState].state)
    messages.count(_ == "Off") must be(2)
    messages.count(_ == "StandBy") must be(1)

    stepper ! SystemEvent("selftest", theTime, "a2", "SELFTEST-SUCCESS", "testUser", Some("StandBy"), Some("corridor1"), None, None)
    probe.expectMsgPF() {
      case s: ZkState if s.state == "EnforceOn" ⇒ s.userId must be("testUser")
      case _                                    ⇒ fail("unexpected msg")
    }
  }

  override def afterEach() {
    super.afterEach()
    zkStore = null
  }

  override def beforeEach() {
    super.beforeEach()

    val formats = DefaultFormats + new EnumerationSerializer(
      ZkWheelbaseType,
      VehicleCode,
      ZkIndicationType,
      ServiceType,
      ZkCaseFileType,
      EsaProviderType,
      VehicleImageType,
      ConfigType)

    zkStore = CuratorHelper.create(this, clientScope, formats = formats)
    zkStore.createEmptyPath(Path("/destroy"))
    zkStore.createEmptyPath(Path("/ctes") / "systems" / "a2")
    zkStore.createEmptyPath(Path("/ctes") / "users")
    zkStore.createEmptyPath(Path("/ctes") / "configuration")
    zkStore.put(Path("/ctes") / "users" / "testUser", new ZkUser("testUser", "testReportingOfficer", "", "", "", "", "", "", "", List()))
    zkStore.put(Path("/ctes") / "users" / "user1", new ZkUser("user1", "testReportingOfficer", "", "", "", "", "", "", "", List()))
    zkStore.put(Path("/ctes") / "users" / "user4", new ZkUser("user4", "testReportingOfficer", "", "", "", "", "", "", "", List()))

    //create 2 corridors
    val config1Path = Path(Paths.Corridors.getConfigPath("a2", "corridor1"))

    zkStore.createEmptyPath(config1Path)
    zkStore.set(config1Path, new ZkCorridor(id = "corridor1",
      name = "first corridor",
      roadType = 0,
      serviceType = ServiceType.RedLight,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = true,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = null,
      info = new ZkCorridorInfo(corridorId = 10,
        locationCode = "1",
        reportId = "TCS A2 Links  39.7 - 54.9",
        reportHtId = "A020011",
        reportPerformance = true,
        locationLine1 = "loc 1"),
      startSectionId = "1",
      endSectionId = "1",
      allSectionIds = Seq("1"),
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 402,
      roadCode = 2,
      dutyType = "KA2",
      deploymentCode = "03",
      codeText = ""), 0)
    val rootPathToServices = Path(Paths.Corridors.getServicesPath("a2", "corridor1"))
    val pathToService = rootPathToServices / ServiceType.RedLight.toString
    zkStore.createEmptyPath(pathToService)
    zkStore.set(pathToService, new RedLightConfig(factNumber = "1234", pardonRedTime = 1000, minimumYellowTime = 1000), 0)

    val config2Path = Path(Paths.Corridors.getConfigPath("a2", "corridor2"))

    zkStore.createEmptyPath(config2Path)
    zkStore.set(config2Path, new ZkCorridor(id = "corridor2",
      name = "first corridor",
      roadType = 0,
      serviceType = ServiceType.SpeedFixed,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = true,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = null,
      info = new ZkCorridorInfo(corridorId = 20,
        locationCode = "1",
        reportId = "TCS A2 Links  39.7 - 54.9",
        reportHtId = "A020011",
        reportPerformance = true,
        locationLine1 = "loc 2"),
      startSectionId = "2",
      endSectionId = "2",
      allSectionIds = Seq("2"),
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 402,
      roadCode = 2,
      dutyType = "KA2",
      deploymentCode = "03",
      codeText = ""), 0)
    val rootPathToServices2 = Path(Paths.Corridors.getServicesPath("a2", "corridor2"))
    val pathToService2 = rootPathToServices2 / ServiceType.SpeedFixed.toString
    zkStore.createEmptyPath(pathToService2)
    zkStore.set(pathToService2, new SpeedFixedConfig(), 0)
  }

  override protected def afterAll() {
    system.shutdown()
    super.afterAll()
  }
}