/*
* Copyright (C) 2012 CSC. <http://www.csc.com>
*/

package csc.sectioncontrol.monitor

import csc.sectioncontrol.monitor.ScheduleMonitor.SystemEventDispatcher
import csc.sectioncontrol.storagelayer.offlineevents.OfflineEventsService
import events._
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.messages.{ VehicleImageType, EsaProviderType, VehicleCode, SystemEvent }
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import com.typesafe.config.ConfigFactory
import akka.testkit.{ TestProbe, TestKit }
import akka.util.duration._
import csc.config.Path
import akka.actor.{ ActorRef, Props, ActorSystem }
import csc.sectioncontrol.i18n.Messages
import csc.sectioncontrol.storage._
import csc.curator.CuratorTestServer
import csc.curator.utils.Curator
import csc.akkautils.DirectLogging
import csc.sectioncontrol.monitor.events.SystemEventIdentifier
import csc.sectioncontrol.storage.ZkSystemDefinition
import csc.sectioncontrol.monitor.events.EventAlertMapping
import csc.sectioncontrol.monitor.events.TranslationMaps
import net.liftweb.json.DefaultFormats
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.storagelayer.{ SpeedFixedConfig, RedLightConfig }

/**
 * Companion for the test
 */
object StepSystemStateTest {
  val config = ConfigFactory.parseString("""
                  akka.loglevel = INFO
                  event-handlers = ["csc.akkautils.Slf4jEventHandler"]
                  akka.actor.debug {
                    receive = on
                    lifecycle = on
                  }
                                         """)
  val system = ActorSystem("StepSystemStateTest", config)
}
/**
 * Test for StepSystemState
 * With state changes when the enforce details are turn on
 * @author Raymond Roestenburg
 */
class StepSystemStateTest(_system: ActorSystem) extends TestKit(_system) with WordSpec with MustMatchers
  with CuratorTestServer with BeforeAndAfterAll with DirectLogging {
  private var zkStore: Curator = _

  //used in ScheduleMonitor eventDispatcher for testing purposes
  private var eventStreamDelegate: Option[ActorRef] = None
  private val eventHandler: SystemEventDispatcher = evt ⇒ { eventStreamDelegate map (_ ! evt) }
  private def setEventReceiver(actor: ActorRef) = eventStreamDelegate = Some(actor)

  def this() = this(StepSystemStateTest.system)

  def doEnforceStateTest2(probe: TestProbe, stepper: ActorRef, theTime: Long) {
    stepper ! standbyEvent(theTime)
    stepper ! enforceOn(theTime)

    stepper ! standbyEvent(theTime, "corridor2")
    stepper ! enforceOn(theTime, "corridor2")

    val messages = probe.receiveN(4).map(_.asInstanceOf[ZkState].state)

    messages.count(_ == "Off") must be(2)
    messages.count(_ == "StandBy") must be(2)

    stepper ! SystemEvent("selftest", theTime, "a2", "SELFTEST-SUCCESS", "testUser", Some("StandBy"), Some("corridor1"), None, None)
    //stepper ! SystemEvent("selftest", theTime, "a2", "SELFTEST-SUCCESS", "testUser1", Some("StandBy"), Some("corridor2"), None, None)
    // use the sleep to have some control over the order of handling actors
    Thread.sleep(1000)
    stepper ! SystemEvent("web", theTime, "a2", SystemStateType.Off.toString, "user1", Some("corridor2"), None, None)
    stepper ! standbyEvent(theTime, "corridor2")
    stepper ! SystemEvent("web", theTime, "a2", SystemStateType.Off.toString, "user1", Some("corridor2"), None, None)
    stepper ! standbyEvent(theTime, "corridor2")
    stepper ! SystemEvent("selftest", theTime, "a2", "SELFTEST-SUCCESS", "testUser", Some("StandBy"), Some("corridor2"), None, None)

    val messages1 = probe.receiveN(6).map(_.asInstanceOf[ZkState].state)
    messages1.count(_ == "Off") must be(2)
    messages1.count(_ == "StandBy") must be(2)
    messages1.count(_ == "EnforceOn") must be(2)

  }

  def create(enforceDetail: Boolean = true): (TestProbe, ActorRef, Long) = {
    val probe = TestProbe()
    val stepper = system.actorOf(Props(new StepSystemState(zkStore, "a2", Some(probe.ref),
      definition = ZkSystemDefinition(enforceDetail = enforceDetail), eventDispatcher = Some(eventHandler))))

    eventStreamDelegate = None //Some(stepper)
    (probe, stepper, time)
  }

  def time = System.currentTimeMillis()

  def standbyEvent(theTime: Long, corridor: String = "corridor1") =
    SystemEvent("web", theTime, "a2", SystemStateType.StandBy.toString, "user1", Some(corridor), None, None)

  def cameraAlert(theTime: Long) =
    SystemEvent("camera", theTime, "a2", "Alert", "alertUser", Some("corridor1"), None, None)

  def failureEvent(theTime: Long) =
    SystemEvent("web", theTime, "a2", SystemStateType.Failure.toString, "user1", Some("corridor1"), None, None)

  def enforceOn(theTime: Long, corridor: String = "corridor1") =
    SystemEvent("web", theTime, "a2", SystemStateType.EnforceOn.toString, "user1", Some(corridor), None, None)

  def stateOff(theTime: Long) = SystemEvent("web", theTime, "a2", SystemStateType.Off.toString, "user1", Some("corridor1"), None, None)

  def doorOpen(theTime: Long) = SystemEvent("web", theTime, "a2", "Open", "user1", Some("corridor1"), None, None)

  def failureSignoff(theTime: Long, corridor: String = "corridor1") = {
    val failPath = Paths.Failure.getCurrentPath("a2", corridor)
    val failures = zkStore.getChildren(failPath)
    failures.size must be(1)
    new SystemEvent("web", theTime, "a2", "FailureSignOff", "user1", "", None, None, Some(failures.head), Some(corridor), None, None)
  }

  def assertState(probe: TestProbe, expectedState: String, expectedUser: String): Unit = {
    Thread.sleep(100)
    probe.expectMsgPF() {
      case s: ZkState ⇒
        s.state must be(expectedState)
        s.userId must be(expectedUser)
      case other ⇒ fail("unexpected msg: " + other)
    }
  }

  "The StepSystemStateTest" must {
    "Test with multiple corridors the move to EnforceDegraded when in EnforceOn an Alert occurs, move back to EnforceDegraded when user swich to EnforceOn with Alert not solved" in {
      val (probe, stepper, theTime) = create()

      doEnforceStateTest2(probe, stepper, theTime)

      stepper ! cameraAlert(theTime)
      probe.expectMsgPF(1 seconds) {
        case s: ZkState if s.state == "EnforceDegraded" ⇒ s.userId must be("testUser")
        case _ ⇒ fail("unexpected msg")
      }

      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.StandBy.toString, "user4", Some("corridor1"), None, None)
      stepper ! SystemEvent("selftest", theTime, "a2", "SELFTEST-SUCCESS", "user4", Some("EnforceDegraded"), Some("corridor1"), None, None)
      probe.expectMsgPF(1 seconds) {
        case s: ZkState if s.state == "StandBy" ⇒ s.userId must be("user4")
        case s: ZkState                         ⇒ fail("unexpected msg => " + s)
      }

      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.EnforceOn.toString, "user4", Some("corridor1"), None, None)
      stepper ! SystemEvent("selftest", theTime, "a2", "SELFTEST-SUCCESS", "user4", Some("StandBy"), Some("corridor1"), None, None)
      probe.expectMsgPF() {
        case s: ZkState if s.state == "EnforceDegraded" ⇒ s.userId must be("user4")
        case s: ZkState                                 ⇒ fail("unexpected msg => " + s)
      }

      system.stop(stepper) //stepper ! PoisonPill
    }

    "move to EnforceDegraded when in EnforceOn an Alert occurs, move back to EnforceDegraded when user swich to EnforceOn with Alert not solved" in {
      val (probe, stepper, theTime) = create()
      doEnforceStateTest(probe, stepper, theTime)
      stepper ! cameraAlert(theTime)
      Thread.sleep(100)
      probe.expectMsgPF() {
        case s: ZkState if s.state == "EnforceDegraded" ⇒ s.userId must be("testUser")
        case _ ⇒ fail("unexpected msg")
      }
      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.StandBy.toString, "user4", Some("corridor1"), None, None)
      stepper ! SystemEvent("selftest", theTime, "a2", "SELFTEST-SUCCESS", "user4", Some("EnforceDegraded"), Some("corridor1"), None, None)
      Thread.sleep(100)
      probe.expectMsgPF() {
        case s: ZkState if s.state == "StandBy" ⇒ s.userId must be("user4")
        case s: ZkState                         ⇒ fail("unexpected msg => " + s)
      }

      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.EnforceOn.toString, "user4", Some("corridor1"), None, None)
      stepper ! SystemEvent("selftest", theTime, "a2", "SELFTEST-SUCCESS", "user4", Some("StandBy"), Some("corridor1"), None, None)
      Thread.sleep(100)
      probe.expectMsgPF() {
        case s: ZkState if s.state == "EnforceDegraded" ⇒ s.userId must be("user4")
        case s: ZkState                                 ⇒ fail("unexpected msg => " + s)
      }

      system.stop(stepper) //stepper ! PoisonPill
    }

    "start in Off state receive a Failure, move to Alert, singoff Alert and move to Standby state" in {
      val probe = TestProbe()
      val theTime = time
      val stepper = system.actorOf(Props(new StepSystemState(zkStore, "a2", Some(probe.ref),
        definition = new ZkSystemDefinition(enforceDetail = false, destroyData = false, acceptFailureInOffMode = true))))
      stepper ! failureEvent(theTime)
      Thread.sleep(100)
      val messages = probe.receiveN(3).map(_.asInstanceOf[ZkState].state)
      messages.count(_ == "Off") must be(2)
      messages.count(_ == "Failure") must be(1)

      stepper ! failureSignoff(theTime)
      Thread.sleep(100)
      val stateUpdate = probe.expectMsgType[ZkState]
      stateUpdate.state must be("StandBy")

      system.stop(stepper) //stepper ! PoisonPill
    }

    "start in Off state receive a Open event, move to Alert, singoff Alert and move to Standby state" in {
      val probe = TestProbe()
      val theTime = time
      val stepper = system.actorOf(Props(new StepSystemState(zkStore, "a2", Some(probe.ref), definition = new ZkSystemDefinition(enforceDetail = false, destroyData = false, acceptFailureInOffMode = true))))
      stepper ! doorOpen(theTime)
      Thread.sleep(100)
      val messages = probe.receiveN(3).map(_.asInstanceOf[ZkState].state)
      messages.count(_ == "Off") must be(2)
      messages.count(_ == "Failure") must be(1)

      stepper ! failureSignoff(theTime)
      Thread.sleep(100)
      val stateUpdate = probe.expectMsgType[ZkState]
      stateUpdate.state must be("StandBy")

      system.stop(stepper) //stepper ! PoisonPill
    }

    //23-12-15 TODO This test is flaky and @ the moment we dont have time to fix it now
    //So lets hope some day someone will fix it
    "start in the Off state move to StandBy and receive DoorOpen on system level, go into Failure for all corridors " +
      "and solve the Failures" ignore {
        val (probe, stepper, theTime) = create()

        stepper ! standbyEvent(theTime)
        stepper ! standbyEvent(theTime, "corridor2")

        stepper ! SystemEvent("web", theTime, "a2", "Open", "user1", None, None, None)

        Thread.sleep(1000)
        val messages = probe.receiveN(6).map(_.asInstanceOf[ZkState].state)
        messages.count(_ == "Off") must be(2)
        messages.count(_ == "StandBy") must be(2)
        messages.count(_ == "Failure") must be(2)

        stepper ! failureSignoff(theTime)
        Thread.sleep(100)
        var stateUpdate = probe.expectMsgType[ZkState]
        stateUpdate.state must be("StandBy")

        stepper ! failureSignoff(theTime, "corridor2")
        Thread.sleep(100)
        stateUpdate = probe.expectMsgType[ZkState]
        stateUpdate.state must be("StandBy")

        system.stop(stepper) //stepper ! PoisonPill
      }

    "start in the Off state move to StandBy and receive DoorOpen and solve failure" in {
      val (probe, stepper, theTime) = create()

      stepper ! standbyEvent(theTime)
      stepper ! doorOpen(theTime)
      Thread.sleep(100)
      val messages = probe.receiveN(4).map(_.asInstanceOf[ZkState].state)
      messages.count(_ == "Off") must be(2)
      messages.count(_ == "StandBy") must be(1)
      messages.count(_ == "Failure") must be(1)

      stepper ! failureSignoff(theTime)
      Thread.sleep(100)
      val stateUpdate = probe.expectMsgType[ZkState]
      stateUpdate.state must be("StandBy")

      system.stop(stepper) //stepper ! PoisonPill
    }

    "start in the Off state and move to StandBy" in {
      val (probe, stepper, theTime) = create()
      stepper ! standbyEvent(theTime)
      Thread.sleep(100)
      val messages = probe.receiveN(3).map(_.asInstanceOf[ZkState].state)
      messages.count(_ == "Off") must be(2)
      messages.count(_ == "StandBy") must be(1)

      system.stop(stepper) //stepper ! PoisonPill
    }

    "start in the Off state and stay there when incorrect State requested (anything other than StandBy)" in {
      val (probe, stepper, theTime) = create()
      probe.ignoreMsg {
        case Off() ⇒ true
        case _     ⇒ false
      }
      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.Maintenance.toString, "user1", Some("corridor1"), None, None)
      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.EnforceDegraded.toString, "user1", Some("corridor1"), None, None)
      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.EnforceOff.toString, "user1", Some("corridor1"), None, None)
      stepper ! enforceOn(theTime)
      stepper ! failureEvent(theTime)
      stepper ! stateOff(theTime)
      Thread.sleep(100)
      probe.expectNoMsg()
      system.stop(stepper) //stepper ! PoisonPill
    }

    "start in the Off state, move to StandBy, move to Failure, stay there until FailureSolved" in {
      val (probe, stepper, theTime) = create()
      stepper ! standbyEvent(theTime)
      stepper ! failureEvent(theTime)
      Thread.sleep(100)
      val messages = probe.receiveN(4).map(_.asInstanceOf[ZkState].state)
      messages.count(_ == "Off") must be(2)
      messages.count(_ == "StandBy") must be(1)
      messages.count(_ == "Failure") must be(1)

      stepper ! failureSignoff(theTime)
      Thread.sleep(100)
      val stateUpdate = probe.expectMsgType[ZkState]
      stateUpdate.state must be("StandBy")

      system.stop(stepper) //stepper ! PoisonPill
    }

    "start in the Off state, move to StandBy, move to Enforce after Selftest success" in {
      val (probe, stepper, theTime) = create()
      doEnforceStateTest(probe, stepper, theTime)
      system.stop(stepper) //stepper ! PoisonPill
    }

    "start in the Off state, move to StandBy, move to Maintenance and back to standby" in {
      val (probe, stepper, theTime) = create()
      stepper ! standbyEvent(theTime)
      stepper ! SystemEvent("web", theTime + 1000, "a2", SystemStateType.Maintenance.toString, "user1", Some("corridor1"), None, None)
      stepper ! SystemEvent("web", theTime + 2000, "a2", SystemStateType.StandBy.toString, "user1", Some("corridor1"), None, None)
      Thread.sleep(100)
      val messages = probe.receiveN(5).map(_.asInstanceOf[ZkState].state)
      messages.count(_ == "Off") must be(2)
      messages.count(_ == "StandBy") must be(2)
      messages.count(_ == "Maintenance") must be(1)

      system.stop(stepper) //stepper ! PoisonPill
    }

    "start in the Off state, move to StandBy, move to Enforce and ignore maintenance request" in {
      val (probe, stepper, theTime) = create()
      doEnforceStateTest(probe, stepper, theTime)
      stepper ! SystemEvent("web", theTime + 1000, "a2", SystemStateType.Maintenance.toString, "user1", Some("corridor1"), None, None)
      Thread.sleep(100)
      probe.expectNoMsg()
      system.stop(stepper) //stepper ! PoisonPill
    }

    "start in the Off state, move to StandBy, move to Failure and ignore maintenance request" in {
      val (probe, stepper, theTime) = create()
      stepper ! standbyEvent(theTime)
      stepper ! SystemEvent("web", theTime + 1000, "a2", SystemStateType.Failure.toString, "user1", Some("corridor1"), None, None)
      Thread.sleep(100)
      val messages = probe.receiveN(4).map(_.asInstanceOf[ZkState].state)
      messages.count(_ == "Off") must be(2)
      messages.count(_ == "StandBy") must be(1)
      messages.count(_ == "Failure") must be(1)

      stepper ! SystemEvent("web", theTime + 2000, "a2", SystemStateType.Maintenance.toString, "user1", Some("corridor1"), None, None)
      probe.expectNoMsg()
      system.stop(stepper) //stepper ! PoisonPill
    }

    "start in the Off state, move to StandBy, move to Failure when a failure occurs, according to Req 5" in {
      val (probe, stepper, theTime) = create()
      stepper ! standbyEvent(theTime)
      stepper ! failureEvent(theTime)
      Thread.sleep(100)
      val messages = probe.receiveN(4).map(_.asInstanceOf[ZkState].state)
      messages.count(_ == "Off") must be(2)
      messages.count(_ == "StandBy") must be(1)
      messages.count(_ == "Failure") must be(1)

      system.stop(stepper) //stepper ! PoisonPill
    }

    "move to EnforceDegraded when in EnforceOn and an Alert occurs, according to Req 6" in {
      val (probe, stepper, theTime) = create()
      doEnforceStateTest(probe, stepper, theTime)
      stepper ! cameraAlert(theTime)
      Thread.sleep(100)
      probe.expectMsgPF() {
        case s: ZkState if s.state == "EnforceDegraded" ⇒ s.userId must be("testUser")
        case _ ⇒ fail("unexpected msg")
      }
      system.stop(stepper) //stepper ! PoisonPill
    }

    "move back to EnforceOff, when in EnforceDegraded receives an AlertSignOff for the last alert and there is no schedule" in {
      val (probe, stepper, theTime) = create()
      doEnforceStateTest(probe, stepper, theTime)
      stepper ! cameraAlert(theTime)

      assertState(probe, "EnforceDegraded", "testUser")

      //remove all alerts from zookeeper
      val children = zkStore.getChildren(Paths.Alert.getCurrentPath("a2", "corridor1"))
      setEventReceiver(stepper) //set our stepper actor as the receiver of SystemEvents from ScheduleMonitor

      children.foreach(path ⇒
        stepper ! new SystemEvent("web", theTime, "a2", "AlertSignOff", "user4", "", None, None, Some(path), Some("corridor1"), None, None))

      assertState(probe, "EnforceOff", "testUser")
      system.stop(stepper) //stepper ! PoisonPill
    }

    "move back to EnforceOn, when in EnforceDegraded receives an AlertSignOff for the last alert and there is a schedule" in {
      val (probe, stepper, theTime) = create()
      doEnforceStateTest(probe, stepper, theTime)
      stepper ! cameraAlert(theTime)

      assertState(probe, "EnforceDegraded", "testUser")

      //remove all alerts from zookeeper
      val children = zkStore.getChildren(Paths.Alert.getCurrentPath("a2", "corridor1"))
      //add schedules to zookeeper
      createSchedules("a2", "corridor1")

      setEventReceiver(stepper) //set our stepper actor as the receiver of SystemEvents from ScheduleMonitor

      children.foreach(path ⇒
        stepper ! new SystemEvent("web", theTime, "a2", "AlertSignOff", "user4", "", None, None, Some(path), Some("corridor1"), None, None))

      assertState(probe, "EnforceOn", "testUser")
      system.stop(stepper) //stepper ! PoisonPill
    }

    "move back to EnforceOff, when in EnforceDegraded receives an AlertSignOff for the last alert and there is some OfflineEvent" in {
      val (probe, stepper, theTime) = create()
      doEnforceStateTest(probe, stepper, theTime)
      stepper ! cameraAlert(theTime)

      assertState(probe, "EnforceDegraded", "testUser")

      //remove all alerts from zookeeper
      val children = zkStore.getChildren(Paths.Alert.getCurrentPath("a2", "corridor1"))
      //add schedules to zookeeper (just to make sure we dont fall in the other case)
      createSchedules("a2", "corridor1")
      //create offline event
      createOfflineEvent("a2", "corridor1", theTime - 100000)

      setEventReceiver(stepper) //set our stepper actor as the receiver of SystemEvents from ScheduleMonitor

      children.foreach(path ⇒
        stepper ! new SystemEvent("web", theTime, "a2", "AlertSignOff", "user4", "", None, None, Some(path), Some("corridor1"), None, None))

      assertState(probe, "EnforceOff", "testUser")
      system.stop(stepper) //stepper ! PoisonPill
    }

    "move back and forth from EnforceOn, to EnforceOff, back to EnforceOn" in {
      val (probe, stepper, theTime) = create()
      doEnforceStateTest(probe, stepper, theTime)
      stepper ! SystemEvent("ScheduleMonitor-1", theTime, "a2", "NoActiveSchedule", "offUser", Some("corridor1"), None, None)
      Thread.sleep(100)
      probe.expectMsgPF() {
        case s: ZkState if s.state == "EnforceOff" ⇒ s.userId must be("testUser")
        case msg: AnyRef                           ⇒ fail("unexpected msg: " + msg.toString)
      }
      system.stop(stepper) //stepper ! PoisonPill
    }

    "move back and forth from EnforceDegraded, to EnforceOff, back to EnforceDegraded" in {
      val (probe, stepper, theTime) = create()
      doEnforceStateTest(probe, stepper, theTime)
      stepper ! SystemEvent("camera", theTime, "a2", "Alert", "camera", Some("corridor1"), None, None)
      Thread.sleep(100)
      probe.expectMsgPF() {
        case s: ZkState if s.state == "EnforceDegraded" ⇒ s.userId must be("testUser")
        case msg: AnyRef                                ⇒ fail("unexpected msg: " + msg.toString)
      }
      stepper ! SystemEvent("ScheduleMonitor-1", theTime, "a2", "NoActiveSchedule", "offUser", Some("corridor1"), None, None)
      Thread.sleep(100)
      probe.expectMsgPF() {
        case s: ZkState if s.state == "EnforceOff" ⇒ s.userId must be("testUser")
        case msg: AnyRef                           ⇒ fail("unexpected msg: " + msg.toString)
      }
      stepper ! SystemEvent("ScheduleMonitor-1", theTime, "a2", "ActiveSchedule", "degradedUser", Some("corridor1"), None, None)
      Thread.sleep(100)
      probe.expectMsgPF(4 seconds) {
        case s: ZkState if s.state == "EnforceDegraded" ⇒ s.userId must be("testUser")
        case msg: AnyRef                                ⇒ fail("unexpected msg: " + msg.toString)
      }

      checkLogEventTypesAlphabetically(List(
        translateStates("EnforceDegraded", "EnforceOff"),
        translateStates("EnforceOff", "EnforceDegraded"),
        translateStates("EnforceOn", "EnforceDegraded"),
        translateStates("Off", "Off"),
        translateStates("Off", "Off"),
        translateStates("Off", "StandBy"),
        translateStates("StandBy", "EnforceOn")))

      system.stop(stepper) //stepper ! PoisonPill
    }

    "move to EnforceDegraded, from StandBy, if an Alert was received previously, and EnforceOn was received" in {
      val (probe, stepper, theTime) = create()
      // send an alert, while in Off
      stepper ! SystemEvent("camera", theTime, "a2", "Alert", "camera", Some("corridor1"), None, None)
      stepper ! standbyEvent(theTime)
      stepper ! enforceOn(theTime)
      Thread.sleep(100)
      val messages = probe.receiveN(3).map(_.asInstanceOf[ZkState].state)
      messages.count(_ == "Off") must be(2)
      messages.count(_ == "StandBy") must be(1)

      stepper ! SystemEvent("selftest", theTime, "a2", "SELFTEST-SUCCESS", "testUser", Some("StandBy"), Some("corridor1"), None, None)
      Thread.sleep(100)
      probe.expectMsgPF() {
        case s: ZkState if s.state == "EnforceDegraded" ⇒ s.userId must be("testUser")
        case _ ⇒ fail("unexpected msg")
      }
      //alphabetic order to ease testing
      checkLogEventTypesAlphabetically(
        List(
          translateStates("Off", "Off"),
          translateStates("Off", "Off"),
          translateStates("Off", "StandBy"),
          translateStates("StandBy", "EnforceDegraded")))

      system.stop(stepper) //stepper ! PoisonPill
    }

    "move from Off to Alarm, when moving to StandBy and an alarm was received while Off" in {
      val (probe, stepper, theTime) = create()
      stepper ! SystemEvent("ntp", theTime, "a2", "Failure", "NTP", Some("corridor1"), None, None)
      Thread.sleep(100)
      probe.expectMsgPF() {
        case s: ZkState if s.state == "Off" ⇒ s.userId must be("system")
        case _                              ⇒ fail("unexpected msg")
      }
      Thread.sleep(100)
      probe.expectMsgPF() {
        case s: ZkState if s.state == "Off" ⇒ s.userId must be("system")
        case _                              ⇒ fail("unexpected msg")
      }

      stepper ! standbyEvent(theTime)
      Thread.sleep(100)
      probe.expectMsgPF() {
        case s: ZkState if s.state == "Failure" ⇒ s.userId must be("user1")
        case _                                  ⇒ fail("unexpected msg")
      }
      system.stop(stepper) //stepper ! PoisonPill
    }

    "set the calibration status correctly when the seltest is started and ended" in {
      val (probe, stepper, theTime) = create()
      stepper ! SystemEvent("web", theTime, "a2", "SELFTEST-START", "adhoc", Some("corridor1"), None, None)
      stepper ! SystemEvent("web", theTime, "a2", "SELFTEST-END", "adhoc", Some("corridor1"), None, None)
      stepper ! standbyEvent(theTime)
      Thread.sleep(100)
      val messages = probe.receiveN(3).map(_.asInstanceOf[ZkState].state)
      messages.count(_ == "Off") must be(2)
      messages.count(_ == "StandBy") must be(1)

      system.stop(stepper) //stepper ! PoisonPill
    }

    "log when the states transition" in {
      val (probe, stepper, theTime) = create()
      stepper ! SystemEvent("web", theTime, "a2", "SELFTEST-START", "adhoc", Some("corridor1"), None, None)
      stepper ! SystemEvent("web", theTime, "a2", "SELFTEST-END", "adhoc", Some("corridor1"), None, None)
      stepper ! standbyEvent(theTime)
      Thread.sleep(100)
      val messages = probe.receiveN(3).map(_.asInstanceOf[ZkState].state)
      messages.count(_ == "Off") must be(2)
      messages.count(_ == "StandBy") must be(1)

      //alphabetic order to ease testing
      checkLogEventTypesAlphabetically(
        List(
          translateStates("Off", "Off"),
          translateStates("Off", "Off"),
          translateStates("Off", "StandBy")))
      system.stop(stepper)
    }

    "reload translationmaps and have effect on reducionfactor and configtype" in {
      //clean alerts
      zkStore.getChildren(Path("/ctes/systems/a2/alerts")).foreach {
        path ⇒ zkStore.delete(path)
      }
      val probe = TestProbe()
      val stepper = system.actorOf(Props(new StepSystemState(zkStore, "a2", Some(probe.ref))))
      val theTime = time
      Thread.sleep(100)
      val path = Path("/ctes/configuration/eventTranslationMaps")
      zkStore.getVersioned[TranslationMaps](path).map {
        voMaps ⇒
          {
            zkStore.set(path, voMaps.data.copy(eventsToAlerts = List(EventAlertMapping(
              SystemEventIdentifier(componentId = Some(".*camera")), ConfigType.Lane, 0.0f))), voMaps.version)
          }
      }

      stepper ! ReloadTranslationMaps(0) //todo:test

      stepper ! SystemEvent("camera", theTime, "a2", "Alert", "camera", Some("corridor1"), None, None)
      stepper ! standbyEvent(theTime)
      Thread.sleep(100)
      val messages = probe.receiveN(3).map(_.asInstanceOf[ZkState].state)
      messages.count(_ == "Off") must be(2)
      messages.count(_ == "StandBy") must be(1)

      zkStore.getChildren(Paths.Alert.getCurrentPath("a2", "corridor1")).size must be(1)
      zkStore.getChildren(Paths.Alert.getCurrentPath("a2", "corridor1")).map {
        path ⇒
          zkStore.get[ZkAlert](path).get.reductionFactor must be(0f)
          zkStore.delete(path)
      }.size must be(1)
      zkStore.getVersioned[TranslationMaps](path).map {
        voMaps ⇒
          {
            zkStore.set(path, voMaps.data.copy(eventsToAlerts = List(EventAlertMapping(
              SystemEventIdentifier(componentId = Some(".*camera")), ConfigType.Lane, 0.5f))), voMaps.version)
          }
      }
      stepper ! ReloadTranslationMaps(0)

      stepper ! SystemEvent("camera", theTime, "a2", "Alert", "camera", Some("corridor1"), None, None)
      stepper ! stateOff(theTime)
      Thread.sleep(100)
      probe.expectMsgPF() {
        case ZkState("Off", _, _, _) ⇒
        case msg: Any                ⇒ fail("unexpected msg " + msg)
      }

      zkStore.getChildren(Paths.Alert.getCurrentPath("a2", "corridor1")).map {
        path ⇒
          zkStore.get[ZkAlert](path).foreach {
            alert ⇒
              alert.reductionFactor must be(0.5f)
              alert.configType must be(ConfigType.Lane)
          }
          zkStore.delete(path)
      }.size must be(1)
      system.stop(stepper)
      Thread.sleep(2000)
    }
  }

  private def translateState(state: String) = Messages("csc.sectioncontrol.monitor.state." + state)
  private def translateStates(from: String, to: String) = Messages("csc.sectioncontrol.monitor.statechange", translateState(from), translateState(to))

  def checkLogEventTypesAlphabetically(alphabeticOrderedListOfStates: List[String]) {
    val logEntries = zkStore.getChildren(Path("/ctes/systems/a2/log-events"))
    val stateChanges = logEntries.flatMap { p ⇒ val se = zkStore.get[SystemEvent](p); log.info(se.toString); se }.filter(_.eventType == "SystemStateChange")
    stateChanges.size must be(alphabeticOrderedListOfStates.size)
    //test in alphabetic order
    stateChanges.map(_.reason).toList.sortWith(_ < _) must be(alphabeticOrderedListOfStates)
  }

  def doEnforceStateTest(probe: TestProbe, stepper: ActorRef, theTime: Long) {
    stepper ! standbyEvent(theTime)
    stepper ! enforceOn(theTime)
    Thread.sleep(100)
    val messages = probe.receiveN(3).map(_.asInstanceOf[ZkState].state)

    messages.count(_ == "Off") must be(2)
    messages.count(_ == "StandBy") must be(1)

    stepper ! SystemEvent("selftest", theTime, "a2", "SELFTEST-SUCCESS", "testUser", Some("StandBy"), Some("corridor1"), None, None)
    Thread.sleep(100)
    probe.expectMsgPF() {
      case s: ZkState if s.state == "EnforceOn" ⇒ s.userId must be("testUser")
      case _                                    ⇒ fail("unexpected msg")
    }
  }

  private def current = { "current" }
  private def failures = { "failures" }
  private def alerts = { "alerts" }
  private def errors = { "errors" }
  private def control = { "control" }

  override def beforeEach() {
    super.beforeEach()

    val formats = DefaultFormats + new EnumerationSerializer(
      ZkWheelbaseType,
      VehicleCode,
      ZkIndicationType,
      ServiceType,
      ZkCaseFileType,
      EsaProviderType,
      VehicleImageType,
      ConfigType)

    zkStore = CuratorHelper.create(this, clientScope, formats = formats)
    zkStore.createEmptyPath(Path("/destroy"))
    zkStore.createEmptyPath(Path("/ctes") / "systems" / "a2")
    zkStore.createEmptyPath(Path("/ctes") / "systems" / "a2" / "corridors" / "corridor1" / control / errors / failures / current)
    zkStore.createEmptyPath(Path("/ctes") / "systems" / "a2" / "corridors" / "corridor2" / control / errors / failures / current)
    zkStore.createEmptyPath(Path("/ctes") / "systems" / "a2" / "corridors" / "corridor1" / control / errors / alerts / current)
    zkStore.createEmptyPath(Path("/ctes") / "systems" / "a2" / "corridors" / "corridor2" / control / errors / alerts / current)
    zkStore.createEmptyPath(Path("/ctes") / "users")
    zkStore.createEmptyPath(Path("/ctes") / "configuration")

    zkStore.put(Path("/ctes") / "users" / "testUser", new ZkUser("testUser", "testReportingOfficer", "", "", "", "", "", "", "", List()))
    zkStore.put(Path("/ctes") / "users" / "testUser1", new ZkUser("testUser1", "testReportingOfficer", "", "", "", "", "", "", "", List()))
    zkStore.put(Path("/ctes") / "users" / "user1", new ZkUser("user1", "testReportingOfficer", "", "", "", "", "", "", "", List()))

    //create 2 corridors
    val config1Path = Path(Paths.Corridors.getConfigPath("a2", "corridor1"))

    zkStore.createEmptyPath(config1Path)
    zkStore.set(config1Path, new ZkCorridor(id = "corridor1",
      name = "first corridor",
      roadType = 0,
      serviceType = ServiceType.RedLight,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = true,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = null,
      info = new ZkCorridorInfo(corridorId = 10,
        locationCode = "1",
        reportId = "TCS A2 Links  39.7 - 54.9",
        reportHtId = "A020011",
        reportPerformance = true,
        locationLine1 = "loc 1"),
      startSectionId = "1",
      endSectionId = "1",
      allSectionIds = Seq("1"),
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 402,
      roadCode = 2,
      dutyType = "KA2",
      deploymentCode = "03",
      codeText = ""), 0)
    val rootPathToServices = Path(Paths.Corridors.getServicesPath("a2", "corridor1"))
    val pathToService = rootPathToServices / ServiceType.SectionControl.toString
    zkStore.createEmptyPath(pathToService)
    zkStore.set(pathToService, new RedLightConfig(factNumber = "1234", pardonRedTime = 1000, minimumYellowTime = 1000), 0)

    val config2Path = Path(Paths.Corridors.getConfigPath("a2", "corridor2"))
    zkStore.createEmptyPath(config2Path)
    zkStore.set(config2Path, new ZkCorridor(id = "corridor2",
      name = "second corridor",
      roadType = 0,
      serviceType = ServiceType.SpeedFixed,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = true,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = null,
      info = new ZkCorridorInfo(corridorId = 20,
        locationCode = "1",
        reportId = "TCS A2 Links  39.7 - 54.9",
        reportHtId = "A020011",
        reportPerformance = true,
        locationLine1 = "loc 2"),
      startSectionId = "2",
      endSectionId = "2",
      allSectionIds = Seq("2"),
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 402,
      roadCode = 2,
      dutyType = "KA2",
      deploymentCode = "03",
      codeText = ""), 0)
    val rootPathToServices2 = Path(Paths.Corridors.getServicesPath("a2", "corridor2"))
    val pathToService2 = rootPathToServices2 / ServiceType.SectionControl.toString
    zkStore.createEmptyPath(pathToService2)
    zkStore.set(pathToService2, new SpeedFixedConfig(), 0)

  }

  override protected def afterAll() {
    system.shutdown()
    super.afterAll()
  }

  def createOfflineEvent(systemId: String, corridorId: String, time: Long): Unit = {
    val component = "somecomponent"
    val oet = EventTypeOffline("temperature", true)
    val nodeName = "%s-%s".format(component, oet.eventName).replaceAll("/", "-")
    val event: SystemEvent = SystemEvent(component, time, systemId, oet.eventName, "user1")
    OfflineEventsService.create(systemId, corridorId, nodeName, zkStore, event)
  }

  def createSchedules(systemId: String, corridorId: String): Unit = {
    val schedules = List(mkSchedule(0, 24))
    zkStore.put(Paths.Corridors.getSchedulesPath(systemId, corridorId), schedules)
  }

  def mkSchedule(fromHour: Int, ToHour: Int, fromMinute: Int = 0, toMinute: Int = 0): ZkSchedule = {
    ZkSchedule("1", ZkIndicationType.None, ZkIndicationType.False, ZkIndicationType.False,
      ZkIndicationType.None, fromHour, fromMinute, ToHour, toMinute, 100, 100, true, None, false)
  }

}
