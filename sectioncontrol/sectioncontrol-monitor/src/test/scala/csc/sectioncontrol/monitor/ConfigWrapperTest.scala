package csc.sectioncontrol.monitor

import org.scalatest.matchers.MustMatchers
import org.scalatest.WordSpec
import com.typesafe.config.{ Config, ConfigFactory }
import java.lang.RuntimeException

class ConfigWrapperTest extends WordSpec with MustMatchers {
  "CanReadConfigData" must {
    "read correct values" in {
      val config: Config = ConfigFactory.load("SectionControl-Monitor.conf")

      val configWrapper: ConfigWrapper = new ConfigWrapper(config)

      configWrapper.getBooleanFromConfig("boolean.true") must be(right = true)
      configWrapper.getBooleanFromConfig("boolean.false") must be(right = false)
      configWrapper.getStringFromConfig("string.filled") must be("filled")

      evaluating { configWrapper.getStringFromConfig("not_present") } must produce[RuntimeException]
      evaluating { configWrapper.getBooleanFromConfig("boolean.fail") } must produce[RuntimeException]
      evaluating { configWrapper.getStringFromConfig("string.empty") } must produce[RuntimeException]
      evaluating { configWrapper.getStringFromConfig("") } must produce[IllegalArgumentException]
      evaluating { configWrapper.getBooleanFromConfig("") } must produce[IllegalArgumentException]
    }
  }
}
