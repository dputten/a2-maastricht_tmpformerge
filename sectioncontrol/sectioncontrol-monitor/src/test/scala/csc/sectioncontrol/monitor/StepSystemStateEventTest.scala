/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.monitor

import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.messages.{ VehicleImageType, EsaProviderType, VehicleCode, SystemEvent }
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import akka.testkit.{ TestProbe, TestKit }
import akka.util.duration._
import csc.config.Path
import akka.actor.{ ActorRef, ActorSystem }
import akka.actor.Props
import csc.sectioncontrol.storage._
import csc.curator.CuratorTestServer
import csc.curator.utils.Curator
import csc.akkautils.DirectLogging
import csc.sectioncontrol.monitor.events.SystemEventIdentifier
import csc.sectioncontrol.storage.ZkSystemDefinition
import csc.sectioncontrol.storage.ZkFailureHistory
import scala.Some
import csc.sectioncontrol.monitor.events.TranslationMaps
import net.liftweb.json.DefaultFormats
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.monitor.events.EventPastAlertMapping
import csc.sectioncontrol.storagelayer.{ SpeedFixedConfig, RedLightConfig }

/**
 * Test for StepSystemState
 * With state changes when the enforce details are turn off
 */
class StepSystemStateEventTest() extends TestKit(ActorSystem("StepSystemStateEventTest")) with WordSpec
  with MustMatchers with CuratorTestServer with BeforeAndAfterAll with DirectLogging {
  private var zkStore: Curator = _

  def time = System.currentTimeMillis()
  "The StepSystemStateEventTest" must {

    "create History log when solving failure" in {
      val probe = TestProbe()
      val stepper = system.actorOf(Props(new StepSystemState(zkStore, "a2", Some(probe.ref), definition = new ZkSystemDefinition(enforceDetail = false))))
      val theTime = time
      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.StandBy.toString, "user1", Some("corridor1"), None, None)
      val failMsg = SystemEvent("web", theTime, "a2", "Open", "user1", Some("corridor1"), None, None)
      stepper ! failMsg
      val messages = probe.receiveN(4).map(_.asInstanceOf[ZkState].state)
      messages.count(_ == "Off") must be(2)
      messages.count(_ == "StandBy") must be(1)
      messages.count(_ == "Failure") must be(1)

      val failures = zkStore.getChildren(Paths.Failure.getCurrentPath("a2", "corridor1"))

      failures.size must be(1)

      stepper ! new SystemEvent("web", theTime + 600, "a2", "FailureSignOff", "user1", "", None, None, Some(failures.head), Some("corridor1"), None, None)

      val stateUpdate = probe.expectMsgType[ZkState]
      stateUpdate.state must be("StandBy")

      //check history log
      val pathHistory = Paths.Failure.getHistoryPath("a2", "corridor1")
      val history = zkStore.getChildren(pathHistory)
      history.size must be(1)

      val failHistory = zkStore.get[ZkFailureHistory](history.head).get
      failHistory.path must be(failMsg.componentId)
      failHistory.startTime must be(failMsg.timestamp)
      failHistory.endTime must be(failMsg.timestamp + 600)
      failHistory.configType must be(ConfigType.System.toString)

      system.stop(stepper)
    }
    "create History log when solving alert" in {
      val probe = TestProbe()
      val stepper = system.actorOf(Props(new StepSystemState(zkStore, "a2", Some(probe.ref), definition = new ZkSystemDefinition(enforceDetail = false))))
      val theTime = time
      doEnforceStateTest(probe, stepper, theTime)

      probe.expectNoMsg(200 millis)
      val alertMsg = SystemEvent("web", theTime + 200, "a2", "Alert", "user1", Some("corridor1"), None, None)
      stepper ! alertMsg
      probe.expectNoMsg(1 second)

      val failPath = Paths.Alert.getCurrentPath("a2", "corridor1")
      val failures = zkStore.getChildren(failPath)
      failures.size must be(1)

      stepper ! new SystemEvent("web", theTime + 400, "a2", "AlertSignOff", "user1", "", None, None, Some(failures.head), Some("corridor1"), None, None)

      stepper ! SystemEvent("web", theTime + 600, "a2", SystemStateType.StandBy.toString, "user1", Some("corridor1"), None, None)
      stepper ! SystemEvent("selftest", theTime + 700, "a2", "SELFTEST-SUCCESS", "user4", Some("EnforceOn"), Some("corridor1"), None, None)

      val stateUpdate = probe.expectMsgType[ZkState]
      stateUpdate.state must be("StandBy")

      //check history log
      val pathHistory = Paths.Alert.getHistoryPath("a2", "corridor1")
      val history = zkStore.getChildren(pathHistory)
      history.size must be(1)

      val alertHistory = zkStore.get[ZkAlertHistory](history.head).get
      alertHistory.path must be(alertMsg.componentId)
      alertHistory.startTime must be(alertMsg.timestamp)
      alertHistory.endTime must be(alertMsg.timestamp + 200)
      alertHistory.configType must be(ConfigType.System.toString)

      system.stop(stepper)
    }
    "create History log when receiving a past alert" in {
      val probe = TestProbe()
      val stepper = system.actorOf(Props(new StepSystemState(zkStore, "a2", Some(probe.ref), definition = new ZkSystemDefinition(enforceDetail = false))))
      val theTime = time
      doEnforceStateTest(probe, stepper, theTime)
      probe.expectNoMsg(200 millis)
      Thread.sleep(100)
      val path = Path("/ctes/configuration/eventTranslationMaps")
      zkStore.getVersioned[TranslationMaps](path).map {
        voMaps ⇒
          {
            log.info("VERSION: " + voMaps.version)
            zkStore.set(path, voMaps.data.copy(eventsToPastAlerts = List(EventPastAlertMapping(SystemEventIdentifier(eventType = Some("WasDown")), ConfigType.Lane, 0.3f))), voMaps.version)
          }
      }
      stepper ! ReloadTranslationMaps(0) //todo:test

      val pastAlertMsg = SystemEvent(componentId = "web",
        timestamp = theTime + 200,
        systemId = "a2",
        eventType = "WasDown",
        userId = "user1",
        reason = None,
        token = None,
        startTimestamp = Some(theTime),
        corridorId = Some("corridor1"))
      stepper ! pastAlertMsg
      probe.expectNoMsg(1 second)

      //check history log
      val pathHistory = Paths.Alert.getHistoryPath("a2", "corridor1")
      val history = zkStore.getChildren(pathHistory)
      history.size must be(1)

      val alertHistory = zkStore.get[ZkAlertHistory](history.head).get
      alertHistory.path must be(pastAlertMsg.componentId)
      alertHistory.startTime must be(pastAlertMsg.startTimestamp.get)
      alertHistory.endTime must be(pastAlertMsg.timestamp)
      alertHistory.configType must be(ConfigType.Lane.toString)

      system.stop(stepper)
    }
    "create no History log when receiving a past alert without a start timestamp" in {
      val probe = TestProbe()
      val stepper = system.actorOf(Props(new StepSystemState(zkStore, "a2", Some(probe.ref), definition = new ZkSystemDefinition(enforceDetail = false))))
      val theTime = time
      doEnforceStateTest(probe, stepper, theTime)
      probe.expectNoMsg(200 millis)
      Thread.sleep(100)
      val path = Path("/ctes/configuration/eventTranslationMaps")
      zkStore.getVersioned[TranslationMaps](path).map {
        voMaps ⇒
          {
            log.info("VERSION: " + voMaps.version)
            zkStore.set(path, voMaps.data.copy(eventsToPastAlerts = List(EventPastAlertMapping(SystemEventIdentifier(eventType = Some("WasDown")), ConfigType.Lane, 0.3f))), voMaps.version)
          }
      }
      stepper ! ReloadTranslationMaps(0) //todo:test

      val pastAlertMsg = SystemEvent(componentId = "web",
        timestamp = theTime + 200,
        systemId = "a2",
        eventType = "WasDown",
        userId = "user1",
        reason = None,
        token = None,
        startTimestamp = None,
        corridorId = Some("corridor1"), gantryId = None, laneId = None)
      stepper ! pastAlertMsg
      probe.expectNoMsg(1 second)

      //check history log
      val pathHistory = Paths.Alert.getHistoryPath("a2", "corridor1")
      val history = zkStore.getChildren(pathHistory)
      history.size must be(0)

      system.stop(stepper)
    }

    def doEnforceStateTest(probe: TestProbe, stepper: ActorRef, theTime: Long) {
      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.StandBy.toString, "user1", Some("corridor1"), None, None)
      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.EnforceOn.toString, "user1", Some("corridor1"), None, None)
      val messages = probe.receiveN(3).map(_.asInstanceOf[ZkState].state)
      messages.count(_ == "Off") must be(2)
      messages.count(_ == "StandBy") must be(1)
      stepper ! SystemEvent("selftest", theTime, "a2", "SELFTEST-SUCCESS", "testUser", Some("StandBy"), Some("corridor1"), None, None)
      probe.expectMsgPF() {
        case s: ZkState if s.state == "EnforceOn" ⇒ s.userId must be("testUser")
        case _                                    ⇒ fail("unexpected msg")
      }
    }
  }
  override def beforeEach() {
    super.beforeEach()

    val formats = DefaultFormats + new EnumerationSerializer(
      ZkWheelbaseType,
      VehicleCode,
      ZkIndicationType,
      ServiceType,
      ZkCaseFileType,
      EsaProviderType,
      VehicleImageType,
      ConfigType)

    zkStore = CuratorHelper.create(this, clientScope, formats = formats)
    zkStore.createEmptyPath(Path("/destroy"))
    zkStore.createEmptyPath(Path("/ctes") / "systems" / "a2")
    zkStore.createEmptyPath(Path("/ctes") / "users")
    zkStore.createEmptyPath(Path("/ctes") / "systems" / "a2" / "selftest")
    zkStore.createEmptyPath(Path("/ctes") / "systems" / "a2" / "alerts")
    zkStore.createEmptyPath(Path("/ctes") / "systems" / "a2" / "failures")
    zkStore.createEmptyPath(Path("/ctes") / "configuration")
    zkStore.put(Path("/ctes") / "users" / "testUser", new ZkUser("testUser", "testReportingOfficer", "", "", "", "", "", "", "", List()))

    //create 2 corridors
    val config1Path = Path(Paths.Corridors.getConfigPath("a2", "corridor1"))

    zkStore.createEmptyPath(config1Path)
    zkStore.set(config1Path, new ZkCorridor(id = "corridor1",
      name = "first corridor",
      roadType = 0,
      serviceType = ServiceType.RedLight,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = true,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = null,
      info = new ZkCorridorInfo(corridorId = 10,
        locationCode = "1",
        reportId = "TCS A2 Links  39.7 - 54.9",
        reportHtId = "A020011",
        reportPerformance = true,
        locationLine1 = "loc 1"),
      startSectionId = "1",
      endSectionId = "1",
      allSectionIds = Seq("1"),
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 402,
      roadCode = 2,
      dutyType = "KA2",
      deploymentCode = "03",
      codeText = ""), 0)
    val rootPathToServices = Path(Paths.Corridors.getServicesPath("a2", "corridor1"))
    val pathToService = rootPathToServices / ServiceType.RedLight.toString
    zkStore.createEmptyPath(pathToService)
    zkStore.set(pathToService, new RedLightConfig(factNumber = "1234", pardonRedTime = 1000, minimumYellowTime = 1000), 0)

    val config2Path = Path(Paths.Corridors.getConfigPath("a2", "corridor2"))

    zkStore.createEmptyPath(config2Path)
    zkStore.set(config2Path, new ZkCorridor(id = "corridor2",
      name = "first corridor",
      roadType = 0,
      serviceType = ServiceType.SpeedFixed,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = true,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = null,
      info = new ZkCorridorInfo(corridorId = 20,
        locationCode = "1",
        reportId = "TCS A2 Links  39.7 - 54.9",
        reportHtId = "A020011",
        reportPerformance = true,
        locationLine1 = "loc 2"),
      startSectionId = "2",
      endSectionId = "2",
      allSectionIds = Seq("2"),
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 402,
      roadCode = 2,
      dutyType = "KA2",
      deploymentCode = "03",
      codeText = ""), 0)
    val rootPathToServices2 = Path(Paths.Corridors.getServicesPath("a2", "corridor2"))
    val pathToService2 = rootPathToServices2 / ServiceType.SpeedFixed.toString
    zkStore.createEmptyPath(pathToService2)
    zkStore.set(pathToService2, new SpeedFixedConfig(), 0)
  }

  override protected def afterAll() {
    system.shutdown()
    super.afterAll()
  }
}