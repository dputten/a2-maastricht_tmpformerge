/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.monitor

import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.messages.SystemEvent
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import akka.testkit.{ TestProbe, TestKit }
import akka.util.duration._
import csc.config.Path
import akka.actor.ActorSystem
import akka.actor.Props
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storage.ZkCorridor
import csc.sectioncontrol.storagelayer.{ SpeedFixedConfig, RedLightConfig }
import csc.curator.CuratorTestServer
import csc.curator.utils.Curator

/**
 * Test Startup of the StepSystemState
 * First time start in off second time
 * keep last state stored in zookeepeer
 *
 */
class StepSystemStateStartTest() extends TestKit(ActorSystem("StepSystemStateStartTest")) with WordSpec
  with MustMatchers with CuratorTestServer with BeforeAndAfterAll {
  private var zkStore: Curator = _

  def time = System.currentTimeMillis()
  "The StepSystemState" must {

    "Restore state when restarting" in {
      val probe = TestProbe()
      val stepper = system.actorOf(Props(new StepSystemState(zkStore, "a2", Some(probe.ref), definition = new ZkSystemDefinition(enforceDetail = false))))
      val theTime = time

      stepper ! SystemEvent("web", theTime, "a2", SystemStateType.StandBy.toString, "testUser", Some("corridor1"), None, None)

      stepper ! SystemEvent(componentId = "web",
        timestamp = theTime,
        systemId = "a2",
        eventType = "SERVICE-START",
        userId = "testUser",
        corridorId = Some("corridor1"),
        path = None)

      var messages = probe.receiveN(3).map(_.asInstanceOf[ZkState].state)
      messages.count(_ == "Off") must be(2)
      messages.count(_ == "StandBy") must be(1)

      stepper ! SystemEvent("selftest", theTime, "a2", "SELFTEST-SUCCESS", "testUser", Some("StandBy"), Some("corridor1"), None, None)
      probe.expectMsgPF() {
        case s: ZkState if s.state == "EnforceOn" ⇒ s.userId must be("testUser")
        case _                                    ⇒ fail("unexpected msg")
      }
      system.stop(stepper)
      //create new StepState
      val stepState = system.actorOf(Props(new StepSystemState(zkStore, "a2", Some(probe.ref), definition = new ZkSystemDefinition(enforceDetail = false))))
      messages = probe.receiveN(2, 10 second).map(_.asInstanceOf[ZkState].state)
      messages.count(_ == "Off") must be(1)
      messages.count(_ == "EnforceOn") must be(1)
      system.stop(stepState)

    }
  }

  override def beforeEach() {
    super.beforeEach()
    zkStore = CuratorHelper.create(this, clientScope)
    zkStore.createEmptyPath(Path("/destroy"))
    zkStore.createEmptyPath(Path("/ctes") / "systems" / "a2")
    zkStore.createEmptyPath(Path("/ctes") / "users")
    zkStore.createEmptyPath(Path("/ctes") / "configuration")
    zkStore.put(Path("/ctes") / "users" / "testUser", new ZkUser("testUser", "testReportingOfficer", "", "", "", "", "", "", "", List()))
    //create 2 corridors
    val config1Path = Path(Paths.Corridors.getConfigPath("a2", "corridor1"))

    zkStore.createEmptyPath(config1Path)
    zkStore.set(config1Path, new ZkCorridor(id = "corridor1",
      name = "first corridor",
      roadType = 0,
      serviceType = ServiceType.RedLight,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = true,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = null,
      info = new ZkCorridorInfo(corridorId = 10,
        locationCode = "1",
        reportId = "TCS A2 Links  39.7 - 54.9",
        reportHtId = "A020011",
        reportPerformance = true,
        locationLine1 = "loc 1"),
      startSectionId = "1",
      endSectionId = "1",
      allSectionIds = Seq("1"),
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 402,
      roadCode = 2,
      dutyType = "KA2",
      deploymentCode = "03",
      codeText = ""), 0)
    val rootPathToServices = Path(Paths.Corridors.getServicesPath("a2", "corridor1"))
    val pathToService = rootPathToServices / ServiceType.RedLight.toString
    zkStore.createEmptyPath(pathToService)
    zkStore.set(pathToService, new RedLightConfig(factNumber = "1234", pardonRedTime = 1000, minimumYellowTime = 1000), 0)

    val config2Path = Path(Paths.Corridors.getConfigPath("a2", "corridor2"))

    zkStore.createEmptyPath(config2Path)
    zkStore.set(config2Path, new ZkCorridor(id = "corridor2",
      name = "first corridor",
      roadType = 0,
      serviceType = ServiceType.SpeedFixed,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = true,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = null,
      info = new ZkCorridorInfo(corridorId = 20,
        locationCode = "1",
        reportId = "TCS A2 Links  39.7 - 54.9",
        reportHtId = "A020011",
        reportPerformance = true,
        locationLine1 = "loc 2"),
      startSectionId = "2",
      endSectionId = "2",
      allSectionIds = Seq("2"),
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 402,
      roadCode = 2,
      dutyType = "KA2",
      deploymentCode = "03",
      codeText = ""), 0)
    val rootPathToServices2 = Path(Paths.Corridors.getServicesPath("a2", "corridor2"))
    val pathToService2 = rootPathToServices2 / ServiceType.SpeedFixed.toString
    zkStore.createEmptyPath(pathToService2)
    zkStore.set(pathToService2, new SpeedFixedConfig(), 0)
  }

  override protected def afterAll() {
    system.shutdown()
    super.afterAll()
  }
}
