package csc.sectioncontrol.monitor

import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.messages._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import com.typesafe.config.ConfigFactory
import akka.testkit.{ TestActorRef, TestProbe, TestKit }
import csc.config.Path
import akka.actor.{ ActorRef, ActorSystem }
import csc.sectioncontrol.storage._
import csc.curator.CuratorTestServer
import csc.curator.utils.Curator
import csc.akkautils.DirectLogging
import net.liftweb.json.DefaultFormats
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.storagelayer.SpeedFixedConfig
import csc.sectioncontrol.storage.ZkCorridorInfo
import scala.Some
import csc.sectioncontrol.storagelayer.SpeedFixedConfig
import csc.sectioncontrol.storage.ZkDirection
import csc.sectioncontrol.storage.ZkSection
import csc.sectioncontrol.storagelayer.RedLightConfig
import csc.sectioncontrol.storage.ZkUser
import csc.sectioncontrol.storage.ZkCorridor
import csc.sectioncontrol.storagelayer.lane.LaneService
import csc.sectioncontrol.storagelayer.state.StateService

/**
 * Companion for the test
 */
object StepCorridorStateTest {
  val config = ConfigFactory.parseString("""
                  akka.loglevel = INFO
                  event-handlers = ["csc.akkautils.Slf4jEventHandler"]
                  akka.actor.debug {
                    receive = on
                    lifecycle = on
                  }
                                         """)
  val system = ActorSystem("StepSystemStateTest", config)
}
/**
 * Test for StepCorridorState
 * @author Raymond Roestenburg
 */
class StepCorridorStateTest(_system: ActorSystem) extends TestKit(_system) with WordSpec with MustMatchers
  with CuratorTestServer with BeforeAndAfterAll with DirectLogging {
  private var zkStore: Curator = _

  def this() = this(StepCorridorStateTest.system)

  def doEnforceStateTest(probe: TestProbe, stepper: ActorRef, theTime: Long) {
    stepper ! SystemEvent("web", theTime, "a2", SystemStateType.StandBy.toString, "user1", Some("corridor1"), None, None)
    stepper ! SystemEvent("web", theTime, "a2", SystemStateType.EnforceOn.toString, "user1", Some("corridor1"), None, None)

    stepper ! SystemEvent("web", theTime, "a2", SystemStateType.StandBy.toString, "user1", None, Some("E1"), None)
    stepper ! SystemEvent("web", theTime, "a2", SystemStateType.EnforceOn.toString, "user1", None, Some("E1"), None)

    stepper ! SystemEvent("camera", theTime, "a2", SystemStateType.StandBy.toString, "testUser", None, None, Some("a1"))
    stepper ! SystemEvent("camera", theTime, "a2", SystemStateType.EnforceOn.toString, "testUser", None, None, Some("a1"))

    Thread.sleep(500)
    //val messages = probe.wait(3) receiveN(6).map(_.asInstanceOf[ZkState].state)
    val messages = probe.receiveN(6).map(_.asInstanceOf[ZkState].state)
    messages.count(_ == "Off") must be(3)
    messages.count(_ == "StandBy") must be(3)
  }

  def time = System.currentTimeMillis()
  "The StepCorridorStateTest" must {

    "With multiple corridors first move all to Off, Standby, EnforceOn" +
      "the move to EnforceDegraded when in EnforceOn an Alert occurs, move back to EnforceDegraded when user swich to EnforceOn with Alert not solved" in {
        val probe = TestProbe()
        val stepper = TestActorRef(new StepSystemState(zkStore, "a2", Some(probe.ref)))
        val theTime = time

        doEnforceStateTest(probe, stepper, theTime)

        stepper ! SystemEvent("selftest", theTime, "a2", "SELFTEST-SUCCESS", "testUser", Some("StandBy"), None, Some("E1"), None)
        Thread.sleep(100)
        var messages = probe.receiveN(1).map(_.asInstanceOf[ZkState].state)
        messages(0) must be(SystemStateType.EnforceOn.toString)
        StateService.get("a2", "corridor2", zkStore).get.state must be(SystemStateType.EnforceOn.toString)

        stepper ! SystemEvent("selftest", theTime, "a2", "SELFTEST-SUCCESS", "testUser", Some("StandBy"), Some("corridor3"), None, None)
        messages = probe.receiveN(1).map(_.asInstanceOf[ZkState].state)
        messages(0) must be(SystemStateType.EnforceOn.toString)
        StateService.get("a2", "corridor3", zkStore).get.state must be(SystemStateType.EnforceOn.toString)

        stepper ! SystemEvent("camera", theTime, "a2", "Alert", "testUser", None, None, Some("a1"))
        Thread.sleep(100)
        probe.expectMsgPF() {
          case s: ZkState if s.state == "EnforceDegraded" ⇒ s.userId must be("testUser")
          case _ ⇒ fail("unexpected msg")
        }
        StateService.get("a2", "corridor3", zkStore).get.state must be(SystemStateType.EnforceDegraded.toString)

        system.stop(stepper)
      }

    "expect the correct messages when sending a message for a gantry or a lane handle the correct corridor" in {
      val probe = TestProbe()
      val stepper = TestActorRef(new StepSystemState(zkStore, "a2", Some(probe.ref)))
      val theTime = time

      doEnforceStateTest(probe, stepper, theTime)

      stepper ! SystemEvent("selftest", theTime, "a2", "SELFTEST-SUCCESS", "testUser", Some("StandBy"), None, Some("E1"), None)
      Thread.sleep(100)
      var messages = probe.receiveN(1).map(_.asInstanceOf[ZkState].state)
      messages(0) must be(SystemStateType.EnforceOn.toString)

      stepper ! SystemEvent("selftest", theTime, "a2", "SELFTEST-SUCCESS", "testUser", Some("StandBy"), Some("corridor3"), None, None)
      Thread.sleep(100)
      messages = probe.receiveN(1).map(_.asInstanceOf[ZkState].state)
      messages(0) must be(SystemStateType.EnforceOn.toString)
      Thread.sleep(100)
      stepper ! SystemEvent("camera", theTime, "a2", "Alert", "testUser", None, None, Some("a1"))
      probe.expectMsgPF() {
        case s: ZkState if s.state == "EnforceDegraded" ⇒ s.userId must be("testUser")
        case _ ⇒ fail("unexpected msg")
      }

      system.stop(stepper)
    }

  }

  "StepCorridorState.isAutomaticSignOff" must {
    "return false if a error ends with -SOLVED" in {
      val actorRef = getStepCorridorState
      actorRef.underlyingActor.isAutomaticSignOff(getSystemEventE1R2("JAI-CAMERA-NTP-DOWN-ERROR")) must be(false)
      Thread.sleep(200)
      actorRef.stop()
    }
    "return true if a error ends with -SOLVED" in {
      val actorRef = getStepCorridorState
      actorRef.underlyingActor.isAutomaticSignOff(getSystemEventE1R2("JAI-CAMERA-NTP-DOWN-ERROR-SOLVED")) must be(true)
      actorRef.stop()
    }
  }

  "StepCorridorState.automaticSignOff" must {
    "sign-off a specific alert when found" in {
      val actorRef = getStepCorridorState
      actorRef.underlyingActor.automaticSignOff(getSystemEventE1R2("JAI-CAMERA-NTP-DOWN-ERROR-SOLVED"),
        getAlerts, getFailures, signOffAlertIsCalled, null, logInfo)
      actorRef.stop()
    }

    "sign-off a double alerts when found" in {
      val actorRef = getStepCorridorState
      def alertList() = {
        val list = getAlerts
        list :+ list(1)
      }
      var nrCalled = 0
      def signOffAlertIsCalledNr(path: String, time: Long, systemEvent: SystemEvent) {
        path must be("path")
        systemEvent must be(getSystemEventE1R2("JAI-CAMERA-NTP-DOWN-ERROR-SOLVED"))
        nrCalled += 1
      }

      actorRef.underlyingActor.automaticSignOff(getSystemEventE1R2("JAI-CAMERA-NTP-DOWN-ERROR-SOLVED"),
        alertList, getFailures, signOffAlertIsCalledNr, null, logInfo)
      nrCalled must be(2)
      actorRef.stop()
    }

    "sign-off a specific failure when found" in {
      val actorRef = getStepCorridorState
      actorRef.underlyingActor.automaticSignOff(getSystemEventE1R3("JAI-CAMERA-HOUSING-OPEN-SOLVED"),
        getAlerts, getFailures, null, signOffFailureIsCalled, logInfo)
      actorRef.stop()
    }
    "sign-off a double failure when found" in {
      val actorRef = getStepCorridorState
      def getFailuresList() = {
        val list = getFailures
        list :+ list.last
      }
      var nrCalled = 0
      def signOffFailureIsCalledNr(path: String, time: Long, systemEvent: SystemEvent) {
        path must be("path")
        systemEvent must be(getSystemEventE1R3("JAI-CAMERA-HOUSING-OPEN-SOLVED"))
        nrCalled += 1
      }

      actorRef.underlyingActor.automaticSignOff(getSystemEventE1R3("JAI-CAMERA-HOUSING-OPEN-SOLVED"),
        getAlerts, getFailuresList, null, signOffFailureIsCalledNr, logInfo)
      nrCalled must be(2)
      actorRef.stop()
    }

    "log the system-event when the alert or failure cannot be found" in {
      val actorRef = getStepCorridorState
      actorRef.underlyingActor.automaticSignOff(getSystemEventWithoutCorrespondingAlertOrFailure("JAI-CAMERA-NTP-DOWN-ERROR-SOLVED"),
        getAlerts, getFailures, null, null, debugInfoIsCalled)
      actorRef.stop()
    }
  }

  private def logInfo(message: String, systemEvent: SystemEvent) = {
    println(message + systemEvent)
  }

  private def signOffFailureIsCalled(path: String, time: Long, systemEvent: SystemEvent) {
    path must be("path")
    systemEvent must be(getSystemEventE1R3("JAI-CAMERA-HOUSING-OPEN-SOLVED"))
  }

  private def signOffAlertIsCalled(path: String, time: Long, systemEvent: SystemEvent) {
    path must be("path")
    systemEvent must be(getSystemEventE1R2("JAI-CAMERA-NTP-DOWN-ERROR-SOLVED"))
  }

  private def debugInfoIsCalled(message: String, systemEvent: SystemEvent) {
    message must startWith("Automatic sign-off was not found in alerts and failures for systemEvent:")
  }

  private def getAlerts(): List[(String, ZkAlert)] = {
    val alert1 = ("path", ZkAlert(path = "A2-E1-E1R1-camera", alertType = "", configType = ConfigType.Lane, message = "current event", timestamp = time, reductionFactor = 0.1f))
    val alert2 = ("path", ZkAlert(path = "A2-E1-E1R2-camera", alertType = "JAI-CAMERA-NTP-DOWN-ERROR", configType = ConfigType.Lane, message = "current event", timestamp = time, reductionFactor = 0.4f))
    val alert3 = ("path", ZkAlert(path = "A2-E1-E1R3-camera", alertType = "", configType = ConfigType.Lane, message = "current event", timestamp = time, reductionFactor = 0.2f))

    List(alert1, alert2, alert3)
  }

  private def getFailures(): List[(String, ZkFailure)] = {
    val failure1 = ("path", ZkFailure(path = "A2-E1-E1R1-camera", failureType = "", message = "Connect error", timestamp = time, configType = ConfigType.Lane, confirmed = false))
    val failure2 = ("path", ZkFailure(path = "A2-E1-E1R2-camera", failureType = "", message = "Connect error", timestamp = time, configType = ConfigType.Lane, confirmed = false))
    val failure3 = ("path", ZkFailure(path = "A2-E1-E1R3-camera", failureType = "JAI-CAMERA-HOUSING-OPEN", message = "Connect error", timestamp = time, configType = ConfigType.Lane, confirmed = false))

    List(failure1, failure2, failure3)
  }

  private def getStepCorridorState(): TestActorRef[StepCorridorState] = {
    TestActorRef(new StepCorridorState("a", zkStore, "a2", None, 1, 1))
  }

  private def getSystemEventWithoutCorrespondingAlertOrFailure(eventType: String): SystemEvent = {
    SystemEvent("web", 1L, "a2", eventType, "user1", Some("corridor1"), None, None)
  }

  private def getSystemEventE1R2(eventType: String): SystemEvent = {
    SystemEvent("A2-E1-E1R2-camera", 1L, "irrelevant", eventType, "irrelevant", Some("irrelevant"), Some("E1"), Some("A2-E1-E1R2"))
  }

  private def getSystemEventE1R3(eventType: String): SystemEvent = {
    SystemEvent("A2-E1-E1R3-camera", 1L, "irrelevant", eventType, "irrelevant", Some("irrelevant"), Some("E1"), Some("A2-E1-E1R3"))
  }

  private def current = { "current" }
  private def failures = { "failures" }
  private def alerts = { "alerts" }
  private def errors = { "errors" }
  private def control = { "control" }

  override def beforeEach() {
    super.beforeEach()

    val formats = DefaultFormats + new EnumerationSerializer(
      ZkWheelbaseType,
      VehicleCode,
      ZkIndicationType,
      ServiceType,
      ZkCaseFileType,
      EsaProviderType,
      VehicleImageType,
      ConfigType)

    zkStore = CuratorHelper.create(this, clientScope, formats = formats)
    zkStore.createEmptyPath(Path("/destroy"))
    zkStore.createEmptyPath(Path("/ctes") / "systems" / "a2")
    zkStore.createEmptyPath(Path("/ctes") / "systems" / "a2" / "corridors" / "corridor1" / control / errors / failures / current)
    zkStore.createEmptyPath(Path("/ctes") / "systems" / "a2" / "corridors" / "corridor2" / control / errors / failures / current)
    zkStore.createEmptyPath(Path("/ctes") / "systems" / "a2" / "corridors" / "corridor1" / control / errors / alerts / current)
    zkStore.createEmptyPath(Path("/ctes") / "systems" / "a2" / "corridors" / "corridor2" / control / errors / alerts / current)
    zkStore.createEmptyPath(Path("/ctes") / "users")
    zkStore.createEmptyPath(Path("/ctes") / "configuration")
    zkStore.put(Path("/ctes") / "users" / "testUser", new ZkUser("testUser", "testReportingOfficer", "", "", "", "", "", "", "", List()))
    zkStore.put(Path("/ctes") / "users" / "testUser1", new ZkUser("testUser1", "testReportingOfficer", "", "", "", "", "", "", "", List()))

    //create 2 corridors
    val config1Path = Path(Paths.Corridors.getConfigPath("a2", "corridor1"))

    zkStore.createEmptyPath(config1Path)
    zkStore.set(config1Path, new ZkCorridor(id = "corridor1",
      name = "first corridor",
      roadType = 0,
      serviceType = ServiceType.RedLight,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = true,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = null,
      info = new ZkCorridorInfo(corridorId = 10,
        locationCode = "1",
        reportId = "TCS A2 Links  39.7 - 54.9",
        reportHtId = "A020011",
        reportPerformance = true,
        locationLine1 = "loc 1"),
      startSectionId = "1",
      endSectionId = "1",
      allSectionIds = Seq("1"),
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 402,
      roadCode = 2,
      dutyType = "KA2",
      deploymentCode = "03",
      codeText = ""), 0)
    val rootPathToServices = Path(Paths.Corridors.getServicesPath("a2", "corridor1"))
    val pathToService = rootPathToServices / ServiceType.SectionControl.toString
    zkStore.createEmptyPath(pathToService)
    zkStore.set(pathToService, new RedLightConfig(factNumber = "1234", pardonRedTime = 1000, minimumYellowTime = 1000), 0)

    // ---------------------------------------------------------------------------

    val config2Path = Path(Paths.Corridors.getConfigPath("a2", "corridor2"))
    zkStore.createEmptyPath(config2Path)
    zkStore.set(config2Path, new ZkCorridor(id = "corridor2",
      name = "second corridor",
      roadType = 0,
      serviceType = ServiceType.SpeedFixed,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = true,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = null,
      info = new ZkCorridorInfo(corridorId = 20,
        locationCode = "1",
        reportId = "TCS A2 Links  39.7 - 54.9",
        reportHtId = "A020011",
        reportPerformance = true,
        locationLine1 = "loc 2"),
      endSectionId = "S1",
      startSectionId = "S1",
      allSectionIds = Seq("S1"),
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 402,
      roadCode = 2,
      dutyType = "KA2",
      deploymentCode = "03",
      codeText = ""), 0)
    val rootPathToServices2 = Path(Paths.Corridors.getServicesPath("a2", "corridor2"))
    val pathToService2 = rootPathToServices2 / ServiceType.SectionControl.toString
    zkStore.createEmptyPath(pathToService2)
    zkStore.set(pathToService2, new SpeedFixedConfig(), 0)

    val section1 = ZkSection(id = "S1",
      systemId = "a2",
      name = "S1",
      length = 8000,
      startGantryId = "E1",
      endGantryId = "X1",
      matrixBoards = Seq(), msiBlackList = Seq())
    zkStore.put(Paths.Sections.getConfigPath("a2", "S1"), section1)

    // ---------------------------------------------------------------------------

    val config3Path = Path(Paths.Corridors.getConfigPath("a2", "corridor3"))
    zkStore.createEmptyPath(config3Path)
    zkStore.set(config3Path, new ZkCorridor(id = "corridor3",
      name = "third corridor",
      roadType = 0,
      serviceType = ServiceType.SpeedFixed,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = true,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = null,
      info = new ZkCorridorInfo(corridorId = 30,
        locationCode = "1",
        reportId = "TCS A2 Links  39.7 - 54.9",
        reportHtId = "A020011",
        reportPerformance = true,
        locationLine1 = "loc 2"),
      startSectionId = "S2",
      endSectionId = "S2",
      allSectionIds = Seq("S2"),
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 402,
      roadCode = 2,
      dutyType = "KA2",
      deploymentCode = "03",
      codeText = ""), 0)
    val rootPathToServices3 = Path(Paths.Corridors.getServicesPath("a2", "corridor3"))
    val pathToService3 = rootPathToServices3 / ServiceType.SectionControl.toString
    zkStore.createEmptyPath(pathToService3)
    zkStore.set(pathToService3, new SpeedFixedConfig(), 0)

    val section2 = ZkSection(id = "S2",
      systemId = "a2",
      name = "S2",
      length = 8000,
      startGantryId = "E2",
      endGantryId = "X2",
      matrixBoards = Seq(), msiBlackList = Seq())
    zkStore.put(Paths.Sections.getConfigPath("a2", "S2"), section2)

    val camera = CameraConfig("a", "a", 1, 1, 1)
    val radar = RadarConfig("a", 1, 1, 1)
    val pir = PIRConfig("a", 1, 1, 1, "a", 1)
    val decorate = None
    val newLane = ZkLaneConfig("a1", "lane", Some("qaz"), camera, radar, pir, decorate)
    zkStore.put(Paths.Gantries.getLaneRootPath("a2", "E2", "a1") + "/" + "config", newLane)
  }

  override protected def afterAll() {
    system.shutdown()
    super.afterAll()
    Thread.sleep(100)
  }
}
