/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

package csc.sectioncontrol.monitor

import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers
import csc.akkautils.DirectLoggingAdapter
import akka.actor.{ Actor, ActorSystem }
import akka.testkit.TestActorRef
import csc.sectioncontrol.storage._
import csc.sectioncontrol.messages.SystemEvent
import csc.sectioncontrol.storage.ZkSystemDefinition
import csc.curator.CuratorTestServer
import csc.curator.utils.Curator

/**
 *
 * @author Maarten Hazewinkel
 */
class ScheduleMonitorTest extends WordSpec with MustMatchers with CuratorTestServer with BeforeAndAfterAll {

  implicit val testSystem = ActorSystem("ScheduleMonitorTest")
  var testScheduleMonitor: TestActorRef[ScheduleMonitor] = _
  val systemId = "eg33"
  var storage: Curator = _

  var lastSystemEvent: Option[SystemEvent] = None
  val eventListener = TestActorRef(new Actor {
    def receive = {
      case se: SystemEvent ⇒ lastSystemEvent = Some(se)
    }
  })

  testSystem.eventStream.subscribe(eventListener, classOf[SystemEvent])

  override def beforeEach() {
    super.beforeEach()
    storage = CuratorHelper.create(this, clientScope)
    testScheduleMonitor = TestActorRef(new ScheduleMonitor(new ZkDataService(systemId, "1", storage, Some(ZkSystemDefinition()))))
    lastSystemEvent = None
  }

  override def afterEach() {
    testSystem.stop(testScheduleMonitor)
    super.afterEach()
  }

  override protected def afterAll() {
    eventListener.stop()
    testSystem.shutdown()
  }

  protected def log = new DirectLoggingAdapter(this.getClass.getName)

  "ScheduleMonitor" must {
    "load zero schedules from zookeeper" in {
      testScheduleMonitor.underlyingActor.loadScheduleIntervals("1", 1) must be(List())
    }

    "load one set of schedules from zookeeper" in {
      val schedules = List(mkSchedule(0, 10), mkSchedule(11, 12), mkSchedule(12, 20))
      storage.put(Paths.Corridors.getSchedulesPath(systemId, "1"), schedules)
      testScheduleMonitor.underlyingActor.loadScheduleIntervals("1", 1) must be(List(-3600000L -> 32400000L,
        36000000L -> 68400000L))
    }

    "load multiple schedules from zookeeper" in {
      val schedules1 = List(mkSchedule(1, 4), mkSchedule(11, 12), mkSchedule(16, 20))
      val schedules2 = List(mkSchedule(2, 10), mkSchedule(11, 12), mkSchedule(20, 21))
      val schedules3 = List(mkSchedule(2, 10), mkSchedule(10, 12), mkSchedule(20, 21))
      storage.put(Paths.Corridors.getSchedulesPath(systemId, "1"), schedules1)
      storage.put(Paths.Corridors.getSchedulesPath(systemId, "2"), schedules2)
      storage.put(Paths.Corridors.getSchedulesPath(systemId, "3"), schedules3)
      testScheduleMonitor.underlyingActor.loadScheduleIntervals("1", 1) must be(List(0L -> 10800000L, 36000000L -> 39600000L, 54000000L -> 68400000L))
      testScheduleMonitor.underlyingActor.loadScheduleIntervals("2", 1) must be(List(3600000L -> 32400000L, 36000000L -> 39600000L, 68400000L -> 72000000L))
      testScheduleMonitor.underlyingActor.loadScheduleIntervals("3", 1) must be(List(3600000L -> 39600000L, 68400000L -> 72000000L))
    }

    "determine next change time for interval schedule" in {
      val schedules = List(mkSchedule(0, 10), mkSchedule(11, 12), mkSchedule(12, 20))
      storage.put(Paths.Corridors.getSchedulesPath(systemId, "1"), schedules)
      val intervals = testScheduleMonitor.underlyingActor.loadScheduleIntervals("1", 1)

      testScheduleMonitor.underlyingActor.nextChangeTime(intervals, 1) must be(32400000L)
      testScheduleMonitor.underlyingActor.nextChangeTime(intervals, 32400000L) must be(36000000L)
      testScheduleMonitor.underlyingActor.nextChangeTime(intervals, 36000000L) must be(68400000L)
      testScheduleMonitor.underlyingActor.nextChangeTime(intervals, 68400000L) must be(82800000L)
    }

    "send EnforceOn when starting up in active schedule" in {
      val schedules = List(mkSchedule(0, 10), mkSchedule(11, 12), mkSchedule(12, 20))
      storage.put(Paths.Corridors.getSchedulesPath(systemId, "1"), schedules)
      testScheduleMonitor.underlyingActor.checkSchedules("1", 1)
      lastSystemEvent must be(Some(SystemEvent("ScheduleMonitor-1",
        1,
        systemId,
        "ActiveSchedule",
        "system",
        Some("Rooster aanwezig voor huidige tijd"),
        corridorId = Some("1"))))
    }

    "send EnforceOff when starting up outside active schedule" in {
      val schedules = List(mkSchedule(0, 10), mkSchedule(11, 12), mkSchedule(12, 20))
      storage.put(Paths.Corridors.getSchedulesPath(systemId, "1"), schedules)
      testScheduleMonitor.underlyingActor.checkSchedules("1", 32400001L)
      lastSystemEvent must be(Some(SystemEvent("ScheduleMonitor-1",
        32400001L,
        systemId,
        "NoActiveSchedule",
        "system",
        Some("Geen rooster aanwezig voor huidige tijd"),
        corridorId = Some("1"))))
    }
    "send EnforceOff when starting up without schedules" in {
      testScheduleMonitor.underlyingActor.checkSchedules("1", 32400001L)
      lastSystemEvent must be(Some(SystemEvent("ScheduleMonitor-1",
        32400001L,
        systemId,
        "NoActiveSchedule",
        "system",
        Some("Geen rooster aanwezig voor huidige tijd"),
        corridorId = Some("1"))))
    }

    "send EnforceOff when transitioning to no active schedule" in {
      val schedules = List(mkSchedule(0, 10), mkSchedule(11, 12), mkSchedule(12, 20))
      storage.put(Paths.Corridors.getSchedulesPath(systemId, "1"), schedules)
      testScheduleMonitor.underlyingActor.checkSchedules("1", 32400000L)
      lastSystemEvent must be(Some(SystemEvent("ScheduleMonitor-1",
        32400000L,
        systemId,
        "NoActiveSchedule",
        "system",
        Some("Geen rooster aanwezig voor huidige tijd"),
        corridorId = Some("1"))))
    }

    "send EnforceOn when transitioning midnight with active schedule" in {
      val schedules = List(mkSchedule(0, 10), mkSchedule(16, 24))
      storage.put(Paths.Corridors.getSchedulesPath(systemId, "1"), schedules)
      testScheduleMonitor.underlyingActor.checkSchedules("1", 82800000L)
      lastSystemEvent must be(Some(SystemEvent("ScheduleMonitor-1",
        82800000L,
        systemId,
        "ActiveSchedule",
        "system",
        Some("Rooster aanwezig voor huidige tijd"),
        corridorId = Some("1"))))
    }

    "make a backup of the schedule when no backup exists" in {
      val schedules = List(mkSchedule(0, 10), mkSchedule(16, 24))
      storage.put(Paths.Corridors.getSchedulesPath(systemId, "1"), schedules)
      val time = System.currentTimeMillis()
      testScheduleMonitor.underlyingActor.backupSchedules()
      val backupNames = storage.getChildNames(Paths.Corridors.getScheduleHistoryPath(systemId, "1"))
      backupNames must have size (1)
      (backupNames.head.toLong - time) must be < (1000L)
      val backupSchedule = storage.get[List[ZkSchedule]](Paths.Corridors.getScheduleHistoryPath(systemId, "1") + "/" + backupNames.head)
      backupSchedule.get must be(schedules)
    }

    "make no backup of the schedule when a last identical backup exists" in {
      val schedules = List(mkSchedule(0, 10), mkSchedule(16, 24))
      storage.put(Paths.Corridors.getSchedulesPath(systemId, "1"), schedules)
      storage.put(Paths.Corridors.getScheduleHistoryPath(systemId, "1") + "/1000000", schedules)
      testScheduleMonitor.underlyingActor.backupSchedules()
      val backupNames = storage.getChildNames(Paths.Corridors.getScheduleHistoryPath(systemId, "1"))
      backupNames must have size (1)
      backupNames.head must be("1000000")
    }

    "make a backup of the schedule when the last backup is not identical" in {
      val schedules = List(mkSchedule(0, 10), mkSchedule(16, 24))
      storage.put(Paths.Corridors.getSchedulesPath(systemId, "1"), schedules)
      storage.put(Paths.Corridors.getScheduleHistoryPath(systemId, "1") + "/1000000", schedules.tail)
      val time = System.currentTimeMillis()
      testScheduleMonitor.underlyingActor.backupSchedules()
      val backupNames = storage.getChildNames(Paths.Corridors.getScheduleHistoryPath(systemId, "1")).map(_.toLong).sorted
      backupNames must have size (2)
      backupNames.head must be(1000000L)
      (backupNames.last - time) must be < (1000L)
      val backupSchedule = storage.get[List[ZkSchedule]](Paths.Corridors.getScheduleHistoryPath(systemId, "1") + "/" + backupNames.last)
      backupSchedule.get must be(schedules)
    }

    "make a backup of the schedule when the last backup is not identical but an earlier identical backup exists" in {
      val schedules = List(mkSchedule(0, 10), mkSchedule(16, 24))
      storage.put(Paths.Corridors.getSchedulesPath(systemId, "1"), schedules)
      storage.put(Paths.Corridors.getScheduleHistoryPath(systemId, "1") + "/1000000", schedules)
      storage.put(Paths.Corridors.getScheduleHistoryPath(systemId, "1") + "/2000000", schedules.tail)
      val time = System.currentTimeMillis()
      testScheduleMonitor.underlyingActor.backupSchedules()
      val backupNames = storage.getChildNames(Paths.Corridors.getScheduleHistoryPath(systemId, "1")).map(_.toLong).sorted
      backupNames must have size (3)
      backupNames.head must be(1000000L)
      (backupNames.last - time) must be < (1000L)
      val backupSchedule = storage.get[List[ZkSchedule]](Paths.Corridors.getScheduleHistoryPath(systemId, "1") + "/" + backupNames.last)
      backupSchedule.get must be(schedules)
    }
  }

  def mkSchedule(fromHour: Int, ToHour: Int, fromMinute: Int = 0, toMinute: Int = 0): ZkSchedule = {
    ZkSchedule("1", ZkIndicationType.None, ZkIndicationType.False, ZkIndicationType.False,
      ZkIndicationType.None, fromHour, fromMinute, ToHour, toMinute, 100, 100, true, None, false)
  }
}
