/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

package csc.sectioncontrol.monitor

import org.apache.curator.framework.{ CuratorFramework, CuratorFrameworkFactory }
import org.apache.curator.retry.RetryUntilElapsed
import csc.akkautils.DirectLoggingAdapter
import csc.curator.utils.{ CuratorToolsImpl, Curator }
import org.apache.curator.RetryPolicy
import net.liftweb.json.{ DefaultFormats, Formats }
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.storage._
import csc.sectioncontrol.messages.{ EsaProviderType, VehicleImageType, VehicleCode }

object CuratorHelper {
  private lazy val retryPolicy = new RetryUntilElapsed(1000 * 60 * 60 * 24 * 3, 5000)
  private lazy val formats: Formats = DefaultFormats

  def create(caller: AnyRef, clientScope: Option[CuratorFramework], retryPolicy: RetryPolicy = retryPolicy, formats: Formats = formats): Curator = {
    val log = new DirectLoggingAdapter(caller.getClass.getName)
    val formats = DefaultFormats + new EnumerationSerializer(
      SystemStateType,
      ConfigType,
      ZkWheelbaseType,
      VehicleCode,
      ZkIndicationType,
      ServiceType,
      ZkCaseFileType,
      EsaProviderType,
      VehicleImageType,
      ConfigType)

    new CuratorToolsImpl(clientScope, log, formats)
  }

  def close(curator: Curator) {
    curator.curator.foreach(_.close())
  }
}