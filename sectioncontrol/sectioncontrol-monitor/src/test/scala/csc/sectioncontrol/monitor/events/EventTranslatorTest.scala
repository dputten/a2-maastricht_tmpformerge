/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.monitor.events

import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import csc.json.lift.{ EnumerationSerializer, LiftSerialization }
import net.liftweb.json.DefaultFormats
import csc.sectioncontrol.messages.SystemEvent
import csc.config.Path
import akka.event.LoggingAdapter
import csc.curator.CuratorTestServer
import csc.curator.utils.Curator
import csc.sectioncontrol.monitor._
import csc.sectioncontrol.storage.{ ZkAlert, ConfigType, ZkFailure, ZkState }

/**
 * Test for EventTranslator
 * @author Raymond Roestenburg
 */
class EventTranslatorTest extends WordSpec with BeforeAndAfterAll with MustMatchers with CuratorTestServer {
  val nowFixed = System.currentTimeMillis()
  private var storage: Curator = _

  override def beforeEach() {
    super.beforeEach()
    storage = CuratorHelper.create(this, clientScope)
  }

  override def afterEach() {
    super.afterEach()
  }

  "The EventTranslator" must {
    "record a map of event to state, event to alert mappings and event to past alert mappings in an appropriate json format" in {
      val ser = new LiftSerialization {
        def formats = DefaultFormats + new EnumerationSerializer(ConfigType, SystemStateType,
          ConfigType)
      }

      val eid = SystemEventIdentifier(Some("web"), Some("Cam-Error"), Some("Camera Failed, intermittent"))
      val eventToState = EventStateMapping(eid, SystemStateType.EnforceDegraded)
      val eventToAlert = EventAlertMapping(eid, ConfigType.Lane, 0.1f)
      val eventToPastAlert = EventPastAlertMapping(eid, ConfigType.Lane, 0.1f)
      val eventToOffline = EventOfflineMapping(eid, "NoSchedules", true)

      val maps = TranslationMaps(List(eventToState), List(eventToAlert), List(eventToPastAlert), List(eventToOffline))
      val bytes = ser.serialize(maps)
      val mapsPhoenix = ser.deserialize[TranslationMaps](bytes)
      mapsPhoenix.eventsToAlerts(0).eventIdentifier must be(eid)
    }

    "Translate directly translatable events" in {
      val (trans, zkStore) = newEventTranslatorAndZkStore
      (SystemStateType.values - SystemStateType.EnforceDegraded).foreach { value ⇒
        trans.translate(SystemEvent("web", nowFixed, "a2", value.toString, "user")) must
          be(Some((ZkState(value.toString, nowFixed, "user", ""), if (value == SystemStateType.Failure) Some(ZkFailure("web", "Failure", "", nowFixed, ConfigType.System)) else None)))
      }
      trans.translate(SystemEvent("web", nowFixed, "a2", SystemStateType.EnforceDegraded.toString, "user")) must
        be(Some((ZkState(SystemStateType.EnforceDegraded.toString, nowFixed, "user", ""),
          Some(ZkAlert("web", "EnforceDegraded", "component: %s eventType: %s".format("web", SystemStateType.EnforceDegraded), nowFixed, ConfigType.Lane, 0.1f)))))

      zkStore.get[TranslationMaps](trans.zkTranslationMapsPath).map { maps ⇒
        maps.eventsToAlerts.size must be(1)
        //random check of one of the defaults
        maps.eventsToStates.find(p ⇒ p.eventIdentifier.eventType == Some("SELFTEST-ERROR")) must be(Some(EventStateMapping(SystemEventIdentifier(eventType = Some("SELFTEST-ERROR")), SystemStateType.Failure)))
      }
    }
    "Translate to Alert with a correct ConfigType and reductionFactor" in {
      val (trans, zkStore) = newEventTranslatorAndZkStore
      zkStore.getVersioned[TranslationMaps](trans.zkTranslationMapsPath).map { v ⇒
        val maps = v.data.copy(eventsToAlerts = List(EventAlertMapping(SystemEventIdentifier(Some(".*/camera")), ConfigType.Lane, 0.1f)))
        zkStore.set(trans.zkTranslationMapsPath, maps, v.version)
      }
      trans.loadTranslationMapsFromZookeeper()
      trans.translate(SystemEvent("/a/b/c/d/camera", nowFixed, "a2", "EnforceDegraded", "user")) must be(
        Some(
          (ZkState(SystemStateType.EnforceDegraded.toString, nowFixed, "user", ""),
            Some(ZkAlert("/a/b/c/d/camera", "EnforceDegraded", "component: %s eventType: %s".format("/a/b/c/d/camera", SystemStateType.EnforceDegraded), nowFixed, ConfigType.Lane, 0.1f)))))
    }

  }

  def newEventTranslatorAndZkStore = {
    storage.createEmptyPath(Path("/ctes") / "configuration")

    val trans = new EventTranslator() {
      def curator: Curator = storage
      def log = new LoggingAdapter {
        def isWarningEnabled = false

        def isErrorEnabled = false

        protected def notifyInfo(message: String) {}

        protected def notifyDebug(message: String) {}

        protected def notifyWarning(message: String) {}

        def isDebugEnabled = false

        def isInfoEnabled = false

        protected def notifyError(message: String) {}

        protected def notifyError(cause: Throwable, message: String) {}
      }
      initializeTranslationMaps()

    }

    storage.getVersioned[TranslationMaps](trans.zkTranslationMapsPath).map { v ⇒
      val newData = v.data.copy(eventsToAlerts = EventAlertMapping(SystemEventIdentifier(eventType = Some("EnforceDegraded")), ConfigType.Lane, 0.1f) :: v.data.eventsToAlerts)
      storage.set(trans.zkTranslationMapsPath, newData, v.version)
    }
    trans.loadTranslationMapsFromZookeeper()
    (trans, storage)
  }
}

