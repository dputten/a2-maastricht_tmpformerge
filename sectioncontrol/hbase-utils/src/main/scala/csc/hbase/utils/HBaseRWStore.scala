/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.hbase.utils

import org.apache.hadoop.hbase.client.HTable
import net.liftweb.json.{ DefaultFormats, Formats }

/**
 * Generic trait to store and retrieve values.
 *
 * The keys to use are of type K and the values to write are of type T.
 *
 * @author Maarten Hazewinkel
 */
trait RWStore[K, T <: AnyRef] extends GenReader[K, T] with GenWriter[K, T]

/**
 * Generic trait to store and retrieve values with HBase.
 * This is a fairly simple store that uses a single column only.
 *
 * The keys to use are of type K and the values to write are of type T.
 *
 * @author Maarten Hazewinkel
 */
trait HBaseRWStore[K, T <: AnyRef] extends RWStore[K, T] with HBaseReader[K, T] with HBaseWriter[K, T] {
  /**
   * Defines the table to store the data in.
   */
  def hbaseTable: HTable

  /**
   * Defines the column family to store the data in.
   */
  def hbaseDataColumnFamily: String

  /**
   * Defines the exact column name (column qualifier) to store the data in.
   */
  def hbaseDataColumnName: String

  /**
   * Transforms a key value into a byte array that will be used as the row index in HBase.
   */
  def makeKey(keyValue: K): Array[Byte]

  final def hbaseWriterTable = hbaseTable
  final def hbaseWriterDataColumnFamily = hbaseDataColumnFamily
  final def hbaseWriterDataColumnName = hbaseDataColumnName
  final def makeWriterKey(keyValue: K) = makeKey(keyValue)

  final def hbaseReaderTable = hbaseTable
  final def hbaseReaderDataColumnFamily = hbaseDataColumnFamily
  final def hbaseReaderDataColumnName = hbaseDataColumnName
  final def makeReaderKey(keyValue: K) = makeKey(keyValue)
}

trait JsonHBaseRWStore[K, T <: AnyRef] extends HBaseRWStore[K, T] with JsonHBaseReader[K, T] with JsonHBaseWriter[K, T] {
  /**
   * The default formats can be overridden to add support for otherwise not serializable
   * types. For instance Enumerations can be supported by overriding this with:
   * {{{
   * override def jsonReaderFormats = super.jsonReaderFormats + new EnumerationSerializer(EnumClass1, EnumClass2)
   * }}}
   */
  def jsonFormats: Formats = DefaultFormats

  final override def jsonReaderFormats = jsonFormats
  final override def jsonWriterFormats = jsonFormats
}