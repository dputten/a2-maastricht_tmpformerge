/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.hbase.utils

import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.hbase.client.{ Put, HTable }
import net.liftweb.json.{ Formats, Serialization, DefaultFormats }
import com.sematext.hbase.wd.AbstractRowKeyDistributor

/**
 * Generic trait to write values to HBase.
 * This is a fairly simple writer that writes a single column only.
 *
 * The keys to use are of type K and the values to write are of type T.
 *
 * @author Maarten Hazewinkel
 */
trait GenWriter[K, T <: AnyRef] {
  /**
   * Writes a single row.
   */
  def writeRow(key: K, value: T, timestamp: Option[Long] = None)

  /**
   * Writes a series of rows.
   */
  def writeRows(keysAndValues: Seq[(K, T)])

  /**
   * Writes a series of rows with explicit timestamps.
   */
  def writeRowsWithTimestamp(keysAndValues: Seq[(K, T, Long)])

  /**
   * Writes a series of rows taking the key from the instance. If timestamp is None then
   * current time is used.
   */
  def writeRows(values: Seq[T], getKey: T ⇒ (K, Option[Long]))

}

/**
 * Generic trait to write values to HBase.
 * This is a fairly simple writer that writes a single column only.
 *
 * The keys to use are of type K and the values to write are of type T.
 *
 * @author Maarten Hazewinkel
 */
trait HBaseWriter[K, T <: AnyRef] extends GenWriter[K, T] {
  val rowKeyDistributer: AbstractRowKeyDistributor

  /**
   * Defines the table to write to.
   */
  def hbaseWriterTable: HTable

  /**
   * Defines the column family to write the data to.
   */
  def hbaseWriterDataColumnFamily: String

  /**
   * Defines the exact column name (column qualifier) to write the data to.
   */
  def hbaseWriterDataColumnName: String

  /**
   * Transforms a key value into a byte array that will be used as the row index in HBase.
   */
  def makeWriterKey(keyValue: K): Array[Byte]

  /**
   * Transforms the value type T into a byte array that can be stored in HBase.
   */
  def serialize(value: T): Array[Byte]

  private implicit def string2ByteArray(s: String): Array[Byte] = Bytes.toBytes(s)

  /**
   * Writes a single row to HBase.
   */
  override def writeRow(key: K, value: T, timestamp: Option[Long] = None) {
    val keyBytes = rowKeyDistributer.getDistributedKey(makeWriterKey(key))
    val put = new Put(keyBytes)
    if (timestamp.isDefined) {
      put.add(hbaseWriterDataColumnFamily, hbaseWriterDataColumnName, timestamp.get, serialize(value))
    } else {
      put.add(hbaseWriterDataColumnFamily, hbaseWriterDataColumnName, serialize(value))
    }

    hbaseWriterTable.put(put)
    hbaseWriterTable.flushCommits()
  }

  /**
   * Writes a series of rows to HBase.
   */
  override def writeRows(keysAndValues: Seq[(K, T)]) {
    keysAndValues.foreach { kv ⇒
      val keyBytes = rowKeyDistributer.getDistributedKey(makeWriterKey(kv._1))
      val put = new Put(keyBytes)
      put.add(hbaseWriterDataColumnFamily, hbaseWriterDataColumnName, serialize(kv._2))
      hbaseWriterTable.put(put)
    }
    hbaseWriterTable.flushCommits()
  }

  /**
   * Writes a series of rows with explicit timestamps.
   */
  override def writeRowsWithTimestamp(keysAndValues: Seq[(K, T, Long)]) {
    keysAndValues.foreach { kv ⇒
      val keyBytes = rowKeyDistributer.getDistributedKey(makeWriterKey(kv._1))
      val put = new Put(keyBytes)
      put.add(hbaseWriterDataColumnFamily, hbaseWriterDataColumnName, kv._3, serialize(kv._2))
      hbaseWriterTable.put(put)
    }
    hbaseWriterTable.flushCommits()
  }

  /**
   * Writes a series of rows taking the key from the instance
   */
  override def writeRows(values: Seq[T], getKey: T ⇒ (K, Option[Long])) {
    values.foreach {
      v ⇒
        val (key, timestamp) = getKey(v)
        val keyBytes = rowKeyDistributer.getDistributedKey(makeWriterKey(key))
        val put = new Put(keyBytes)
        timestamp match {
          case None     ⇒ put.add(hbaseWriterDataColumnFamily, hbaseWriterDataColumnName, serialize(v))
          case Some(ts) ⇒ put.add(hbaseWriterDataColumnFamily, hbaseWriterDataColumnName, ts, serialize(v))
        }
        hbaseWriterTable.put(put)
    }
    hbaseWriterTable.flushCommits()
  }

}

/**
 * Extend the HbaseWriter trait to use lift-json based serialization.
 *
 * @author Maarten Hazewinkel
 */
trait JsonHBaseWriter[K, T <: AnyRef] extends HBaseWriter[K, T] {
  /**
   * Serialize value of type T into a byte array using lift-json serialization.
   */
  override def serialize(value: T): Array[Byte] = {
    Bytes.toBytes(Serialization.write(value)(jsonWriterFormats))
  }

  /**
   * The default formats can be overridden to add support for otherwise not serializable
   * types. For instance Enumerations can be supported by overriding this with:
   * {{{
   * override def jsonWriterFormats = super.jsonWriterFormats + new EnumerationSerializer(EnumClass1, EnumClass2)
   * }}}
   */
  def jsonWriterFormats: Formats = DefaultFormats
}
