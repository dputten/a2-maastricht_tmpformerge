/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.hbase.utils

import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.hbase.client.{ Put, HTable }
import net.liftweb.json.{ Formats, Serialization, DefaultFormats }
import com.sematext.hbase.wd.AbstractRowKeyDistributor

/**
 * Generic trait to write multi-column values to HBase.
 *
 * The keys to use are of type K and the values to write are of type T.
 *
 * @author Maarten Hazewinkel
 * @author csomogyi
 */
trait HBaseMultiWriter[K, T <: AnyRef] {
  type Serializer = (T) ⇒ Option[Array[Byte]]

  val rowKeyDistributer: AbstractRowKeyDistributor

  /**
   * Defines the table to write to.
   */
  def hbaseWriterTable: HTable

  /**
   * Defines the column family to write the data to.
   */
  def hbaseWriterDataColumnFamily: String

  /**
   * Defines the exact column name (column qualifier) to write the data to.
   */
  def hbaseWriterDataColumns: Traversable[(String, Serializer)]

  /**
   * Transforms a key value into a byte array that will be used as the row index in HBase.
   */
  def makeKey(keyValue: K): Array[Byte]

  private implicit def string2ByteArray(s: String): Array[Byte] = Bytes.toBytes(s)

  def writeRowInternal(key: K, value: T, timestamp: Option[Long]): Put = {
    val keyBytes = rowKeyDistributer.getDistributedKey(makeKey(key))
    val put = new Put(keyBytes)
    hbaseWriterDataColumns foreach { tuple ⇒
      val (columnName, serialize) = tuple
      val serialized = serialize(value)
      if (serialized.isDefined)
        if (timestamp.isDefined) {
          put.add(hbaseWriterDataColumnFamily, columnName, timestamp.get, serialized.get)
        } else {
          put.add(hbaseWriterDataColumnFamily, columnName, serialized.get)
        }
    }
    put
  }

  /**
   * Writes a single row to HBase.
   */
  def writeRow(key: K, value: T, timestamp: Option[Long] = None) {
    val put = writeRowInternal(key, value, timestamp)

    hbaseWriterTable.put(put)
    hbaseWriterTable.flushCommits()
  }

  /**
   * Writes a series of rows to HBase.
   */
  def writeRows(keysAndValues: Seq[(K, T)]) {
    keysAndValues.foreach { kv ⇒
      val (key, value) = kv
      val put = writeRowInternal(key, value, None)
      hbaseWriterTable.put(put)
    }
    hbaseWriterTable.flushCommits()
  }

  /**
   * Writes a series of rows with explicit timestamps.
   */
  def writeRowsWithTimestamp(keysAndValues: Seq[(K, T, Long)]) {
    keysAndValues.foreach { kv ⇒
      val (key, value, timestamp) = kv
      val put = writeRowInternal(key, value, Some(timestamp))
      hbaseWriterTable.put(put)
    }
    hbaseWriterTable.flushCommits()
  }

  /**
   * Writes a series of rows taking the key from the instance
   */
  def writeRows(values: Seq[T], getKey: T ⇒ (K, Option[Long])) {
    values.foreach {
      v ⇒
        val (key, timestamp) = getKey(v)
        val keyBytes = rowKeyDistributer.getDistributedKey(makeKey(key))
        val put = writeRowInternal(key, v, timestamp)
        hbaseWriterTable.put(put)
    }
    hbaseWriterTable.flushCommits()
  }

}

/**
 * Extend the HbaseMultiWriter trait to add lift-json based serialization.
 *
 * @author Maarten Hazewinkel
 * @author csomogyi
 */
trait JsonHBaseMultiWriter[K, T <: AnyRef] extends HBaseMultiWriter[K, T] {
  /**
   * Serialize value of type T into a byte array using lift-json serialization.
   */
  def serialize[U <: AnyRef: Manifest](value: U): Array[Byte] = {
    Bytes.toBytes(Serialization.write[U](value)(jsonWriterFormats))
  }

  /**
   * The default formats can be overridden to add support for otherwise not serializable
   * types. For instance Enumerations can be supported by overriding this with:
   * {{{
   * override def jsonWriterFormats = super.jsonWriterFormats + new EnumerationSerializer(EnumClass1, EnumClass2)
   * }}}
   */
  def jsonWriterFormats: Formats = DefaultFormats
}
