/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.hbase.utils

import org.apache.hadoop.hbase.client.HBaseAdmin
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.hbase.{ TableName, TableExistsException, HColumnDescriptor, HTableDescriptor }
import org.apache.hadoop.conf.Configuration

/**
 * Admin utility methods for working with HBase
 *
 * @author Maarten Hazewinkel
 */
object HBaseAdminTool {
  /**
   * Verify the existence of the hbase table and column family.
   * Creates the table if it does not exist already.
   */
  def ensureTableExists(hbaseConfig: Configuration, name: String, columnFamily: String) {
    // TODO create mutex around this with netflix curator (lock in zookeeper)
    val hbaseAdmin = new HBaseAdmin(hbaseConfig)
    val familyName = Bytes.toBytes(columnFamily)

    try {
      val tableName = TableName.valueOf(name)
      if (!hbaseAdmin.tableExists(tableName)) {
        val desc = new HTableDescriptor(tableName)
        desc.addFamily(new HColumnDescriptor(familyName))
        try {
          hbaseAdmin.createTable(desc)
        } catch {
          case ex: TableExistsException ⇒ //someone beat use in creating the table, which isn't a problem
        }
      }

      val tableDescriptor = hbaseAdmin.getTableDescriptor(tableName)
      if (!tableDescriptor.hasFamily(familyName)) {
        hbaseAdmin.disableTableAsync(tableName)
        while (!hbaseAdmin.isTableDisabled(tableName))
          Thread.sleep(100)
        hbaseAdmin.addColumn(tableName, new HColumnDescriptor(familyName))
        hbaseAdmin.enableTableAsync(tableName)
        while (!hbaseAdmin.isTableEnabled(tableName))
          Thread.sleep(100)
      }
    } finally {
      hbaseAdmin.close()
    }
  }
}
