/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.hbase.utils

import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.HBaseConfiguration
import collection.mutable
import org.apache.hadoop.conf.Configuration

/**
 *
 * @author Maarten Hazewinkel
 */
trait HBaseTableKeeper {
  private var tables: List[HTable] = List()

  def addTable(table: HTable) {
    this.synchronized {
      tables = table :: tables
    }
  }

  def closeTables() {
    this.synchronized {
      tables.reverse.foreach { table ⇒
        try {
          table.close()
        } catch {
          case e: Exception ⇒
            e.printStackTrace()
        }
      }
      tables = List()
    }
  }

  def createConfig(zkServers: String) = {
    HBaseTableKeeper.createCachedConfig(zkServers)
  }
}

object HBaseTableKeeper {
  def createCachedConfig(zkServers: String) = {
    hbaseConfigs.synchronized {
      hbaseConfigs.get(zkServers) match {
        case Some(config) ⇒
          config

        case None ⇒
          val parts = zkServers.split(":")
          val (quorum, port) = if (parts.size != 2) {
            (zkServers, None)
          } else {
            (parts(0), Some(parts(1)))
          }
          val config = HBaseConfiguration.create()
          config.set("hbase.zookeeper.quorum", quorum)
          port.foreach(p ⇒ config.set("hbase.zookeeper.property.clientPort", p))
          hbaseConfigs.put(zkServers, config)

          config
      }
    }
  }

  private val hbaseConfigs = mutable.Map[String, Configuration]()
}