/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.hbase.utils

import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.hbase.client.{ Scan, Delete, HTable }

/**
 * Generic trait to delete values.
 *
 * The keys to use are of type K.
 *
 * @author Maarten Hazewinkel
 */
trait Deleter[K] {
  /**
   * Deletes a single row.
   */
  def deleteRow(key: K)

  /**
   * Deletes a series of rows.
   */
  def deleteRows(keys: Seq[K])

  /**
   * Writes a series of rows with explicit timestamps.
   */
  def deleteRowsByTime(from: Long, to: Long)
}

/**
 * Generic trait to delete values from HBase.
 *
 * The keys to use are of type K.
 *
 * @author Maarten Hazewinkel
 */
trait HBaseDeleter[K] extends Deleter[K] {
  /**
   * Defines the table to delete from.
   */
  def hbaseDeleterTable: HTable

  /**
   * Transforms a key value into a byte array that will be used as the row index in HBase.
   */
  def makeDeleterKey(keyValue: K): Array[Byte]

  private implicit def string2ByteArray(s: String): Array[Byte] = Bytes.toBytes(s)

  /**
   * Deletes a single row from HBase.
   */
  override def deleteRow(key: K) {
    val keyBytes = makeDeleterKey(key)
    hbaseDeleterTable.delete(new Delete(keyBytes))
    hbaseDeleterTable.flushCommits()
  }

  /**
   * Deletes a series of rows from HBase.
   */
  override def deleteRows(keys: Seq[K]) {
    import scala.collection.JavaConversions._
    val deletes = keys.map(k ⇒ new Delete(makeDeleterKey(k)))
    hbaseDeleterTable.delete(deletes)
    hbaseDeleterTable.flushCommits()
  }

  /**
   * Deletes a series of rows by timestamp.
   */
  override def deleteRowsByTime(from: Long, to: Long) {
    import collection.JavaConversions._

    val scan = new Scan()
    scan.setTimeRange(from, to)
    val scanner = hbaseDeleterTable.getScanner(scan)
    try {
      scanner.foreach { r ⇒
        val delete = new Delete(r.getRow)
        hbaseDeleterTable.delete(delete)
      }
    } finally {
      scanner.close()
      hbaseDeleterTable.flushCommits()
    }
  }
}
