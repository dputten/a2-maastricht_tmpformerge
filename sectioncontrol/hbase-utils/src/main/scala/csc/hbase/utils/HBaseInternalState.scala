/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.hbase.utils

import org.apache.hadoop.hbase.util.Bytes
import net.liftweb.json.{ Serialization, DefaultFormats, Formats }
import org.apache.hadoop.hbase.client.{ Delete, Put, Scan, HTable }

/**
 * Generic trait to keep internal state
 * @tparam T
 */
trait HBaseStore[T <: AnyRef] {

  /**
   * Reads all rows. The whole Seq will be loaded entirely in memory
   */
  def readAllRows(implicit mt: Manifest[T]): Seq[T]

  /**
   * Write all the records to the database. The key will be calculated from the instance to be stored
   * @param values  values to store
   */
  def writeRows(values: Seq[T])

  /**
   * Removes a series of rows taking the key from the instance
   */
  def deleteRows(values: Seq[T])
}

trait JsonHBaseStore[T <: AnyRef] extends HBaseStore[T] {
  /**
   * Defines the table to store the data in.
   */
  def hbaseTable: HTable

  /**
   * Defines the column family to store the data in.
   */
  def hbaseDataColumnFamily: String

  /**
   * Defines the exact column name (column qualifier) to store the data in.
   */
  def hbaseDataColumnName: String

  /**
   * Transforms a key value into a byte array that will be used as the row index in HBase.
   */
  def makeKey(v: T): Array[Byte]

  private implicit def string2ByteArray(s: String): Array[Byte] = Bytes.toBytes(s)

  def readAllRows(implicit mt: Manifest[T]): Seq[T] = {
    import collection.JavaConversions._

    val scan = new Scan()
    scan.addColumn(hbaseDataColumnFamily, hbaseDataColumnName)
    val scanner = hbaseTable.getScanner(scan)
    try {
      scanner.map(r ⇒ deserialize(r.getValue(hbaseDataColumnFamily, hbaseDataColumnName))).toSeq
    } finally {
      scanner.close()
    }
  }

  /**
   * Deserialize the byte array into the target type T using lift-json serialization.
   */
  def deserialize(bytes: Array[Byte])(implicit mt: Manifest[T]): T = {
    Serialization.read[T](Bytes.toString(bytes))(jsonFormats, mt)
  }

  /**
   * Serialize value of type T into a byte array using lift-json serialization.
   */
  def serialize(value: T): Array[Byte] = {
    Bytes.toBytes(Serialization.write(value)(jsonFormats))
  }

  /**
   * The default formats can be overridden to add support for otherwise not serializable
   * types. For instance Enumerations can be supported by overriding this with:
   * {{{
   * override def jsonFormats = super.jsonReaderFormats + new EnumerationSerializer(EnumClass1, EnumClass2)
   * }}}
   */
  def jsonFormats: Formats = DefaultFormats

  /**
   * Writes a series of rows taking the key from the instance
   */
  def writeRows(values: Seq[T]) {
    values.foreach {
      v ⇒
        val keyBytes = makeKey(v)
        val put = new Put(keyBytes)
        put.add(hbaseDataColumnFamily, hbaseDataColumnName, serialize(v))
        hbaseTable.put(put)
    }
    hbaseTable.flushCommits()
  }

  /**
   * Removes a series of rows taking the key from the instance
   */
  def deleteRows(values: Seq[T]) {
    values.foreach {
      v ⇒
        val keyBytes = makeKey(v)
        val delete = new Delete(keyBytes)
        delete.deleteColumns(hbaseDataColumnFamily, hbaseDataColumnName)
        hbaseTable.delete(delete)
    }
    hbaseTable.flushCommits()
  }
}

