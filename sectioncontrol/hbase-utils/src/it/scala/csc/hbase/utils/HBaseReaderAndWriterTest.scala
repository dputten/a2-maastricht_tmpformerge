/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.hbase.utils

import csc.hbase.utils.testframework.HBaseTestFramework
import net.liftweb.json.DefaultFormats
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.util.Bytes
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

/**
 * @author Maarten Hazewinkel
 */
class HBaseReaderAndWriterTest extends WordSpec with MustMatchers with HBaseTestFramework {

  var testTable: Option[HTable] = None

  override protected def beforeAll() {
    super.beforeAll()
    testTable = Some(testUtil.createTable("TD".getBytes, "T".getBytes))
  }

  implicit val jsonDefaultFormats = DefaultFormats

  val td1 = TestData("row1", None)
  val td2 = TestData("row2", Some(2))
  val td3 = TestData("row3", Some(64))

  "HBaseReader/HBaseWriter" must {
    "read nothing from empty table" in {
      HBaseReaderImpl.readRows(0, 100) must be('empty)
    }

    "write a single value" in {
      HBaseWriterImpl.writeRow(10L, td1)
    }

    "read a single value in range" in {
      HBaseReaderImpl.readRows(0, 100) must be(List(td1))
    }

    "write multipe values" in {
      HBaseWriterImpl.writeRows(Seq(20L -> td2, 30L -> td3))
    }

    "read multiple values in range" in {
      HBaseReaderImpl.readRows(0, 25).toSet must be(Set(td1, td2))
    }

    "read nothing when the range doesn't match any values" in {
      HBaseReaderImpl.readRows(23, 25) must be(List())
    }

    "read a value for an exact key" in {
      HBaseReaderImpl.readRow(20) must be(Some(td2))
    }

    "read Nothing for a missing key" in {
      HBaseReaderImpl.readRow(42) must be(None)
    }
  }

  override protected def afterAll() {
    testTable.foreach(_.close())
    super.afterAll()
  }

  object HBaseReaderImpl extends JsonHBaseReader[Long, TestData] {
    val rowKeyDistributer = new NoDistributionOfRowKey()
    val hbaseReaderTable = testTable.get
    val hbaseReaderDataColumnFamily = "T"
    val hbaseReaderDataColumnName = "D"
    def makeReaderKey(keyValue: Long) = Bytes.toBytes(keyValue)
  }

  object HBaseWriterImpl extends JsonHBaseWriter[Long, TestData] {
    val rowKeyDistributer = new NoDistributionOfRowKey()
    val hbaseWriterTable = testTable.get
    val hbaseWriterDataColumnFamily = "T"
    val hbaseWriterDataColumnName = "D"
    def makeWriterKey(keyValue: Long) = Bytes.toBytes(keyValue)
  }
}

case class TestData(f1: String, f2: Option[Int])

