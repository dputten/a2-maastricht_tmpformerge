/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.hbase.utils

/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

import csc.hbase.utils.testframework.HBaseTestFramework
import net.liftweb.json.DefaultFormats
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.util.Bytes
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

/**
 * @author Maarten Hazewinkel
 */
class HBaseRWStoreTest extends WordSpec with MustMatchers with HBaseTestFramework {

  var testTable: Option[HTable] = None

  override protected def beforeAll() {
    super.beforeAll()
    testTable = Some(testUtil.createTable("TD".getBytes, "T".getBytes))
  }

  implicit val jsonDefaultFormats = DefaultFormats

  val td1 = TestData("row1", None)
  val td2 = TestData("row2", Some(2))
  val td3 = TestData("row3", Some(64))

  "HBaseRWStore" must {
    "read nothing from empty table" in {
      HBaseRWStoreImpl.readRows(0, 100) must be('empty)
    }

    "write a single value" in {
      HBaseRWStoreImpl.writeRow(10L, td1)
    }

    "read a single value in range" in {
      HBaseRWStoreImpl.readRows(0, 100) must be(List(td1))
    }

    "write multipe values" in {
      HBaseRWStoreImpl.writeRows(Seq(20L -> td2, 30L -> td3))
    }

    "read multiple values in range" in {
      HBaseRWStoreImpl.readRows(0, 25).toSet must be(Set(td1, td2))
    }

    "return an empty list when the range doesn't match any values" in {
      HBaseRWStoreImpl.readRows(23, 25) must be(Nil)
    }

    "read a value for an exact key" in {
      HBaseRWStoreImpl.readRow(20) must be(Some(td2))
    }

    "return None for a missing key" in {
      HBaseRWStoreImpl.readRow(42) must be(None)
    }
  }

  override protected def afterAll() {
    testTable.foreach(_.close())
    super.afterAll()
  }

  object HBaseRWStoreImpl extends JsonHBaseRWStore[Long, TestData] {
    val rowKeyDistributer = new NoDistributionOfRowKey()

    val hbaseTable = testTable.get
    val hbaseDataColumnFamily = "T"
    val hbaseDataColumnName = "D"

    def makeKey(keyValue: Long) = Bytes.toBytes(keyValue)
  }
}

