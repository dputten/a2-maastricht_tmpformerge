/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package models.storage

import csc.sectioncontrol.messages.VehicleImageType
import com.sun.corba.se.impl.activation.ProcessMonitorThread
import models.web.Direction


case class Label(nodePath:String,  nodeType:String,  labelName:String)

object SpeedIndicatorType extends Enumeration {
  val A1, A3 = Value
}

case class SystemDefinition(enforceDetail: Boolean = true, destroyData: Boolean = true, startInStandBy: Boolean = true, acceptFailureInOffMode:Boolean = false)

case class PossibleTreatyCountry(isoCode: String)
case class ClassifyCountry(mobiCountries: Seq[String],similarCountries:Seq[String])

/**
 *
 * @param sessionTimeout Max time to wait to complete a calibration
 * @param iRoseTimeOut Max time of ssh connection to iRose
 * @param iRoseNextRetry Time to wait before retrying sending calibration command to iRose after failure
 * @param iRoseMessage Message used to check the calibration command has successfully executed
 * @param iRoseSpeedMargin Lower and upper deviation from input speed for a successful match
 * @param iRoseLengthMargin Lower and upper deviation form input length for a successful match
 * @param matcherTimeout Max time to match the input with the output form the iRose
 * @param matcherNextRetry Time to wait before retrying to match the input with the output from the iRose
 * @param matchInterval Time interval within an output can be matched with an input from the iRose
 * @param waitingBeforeResend the max time to wait before a new calibration request is send for a specific lane
 */
case class CalibrationJobConfig(sessionTimeout: Long,
                                iRoseTimeOut: Long,
                                iRoseNextRetry: Long,
                                iRoseMessage: String,
                                iRoseSpeedMargin: Deviation,
                                iRoseLengthMargin: Deviation,
                                matcherTimeout: Long,
                                matcherNextRetry: Long,
                                matchInterval: Long,
                                waitingBeforeResend: Long)

object CalibrationJobConfig {
  /**
   * Default calibration configuration if no specific config is stored in zookeeper.
   */
  val millisecond = 1
  val second = 1000 * millisecond
  val minute = 60 * second
  val hour = 60 * minute
  val day = 24 * hour
  val default = CalibrationJobConfig(sessionTimeout = 2 * hour,
    iRoseTimeOut = 1 * second,
    iRoseNextRetry = 1 * minute,
    iRoseMessage = "Calibratietest",    // This is the prefix of the  messsage returnd by executing a calibratie command
    iRoseSpeedMargin = Deviation(lower = 1, upper = 1),
    iRoseLengthMargin = Deviation(lower = 1, upper = 1),
    matcherTimeout = 5 * minute,
    matcherNextRetry = 30 * second,
    matchInterval = 1 * minute,
    waitingBeforeResend = 2 * minute
  )
}

case class SystemSmsByEmailNotifierConfig(val phoneNumbers: Seq[String])

case class NotificationStateConfig(val isEnabled: Boolean)

case class SmsByEmailNotifierConfig(
                                     emailSubject: Option[String] = None,
                                     emailBodyTemplate: String,
                                     fromEmailAddress: String,
                                     smsGatewayDomain: String,
                                     smtpHost: String,
                                     smtpPort: Option[Int],
                                     smtpUserName: Option[String],
                                     smtpPassword: Option[String],
                                     smtpProperties: Option[Map[String, String]])

case class SmsByEmailNotifierEvents(events:Seq[String])

case class Deviation(lower: Int, upper: Int)

case class SensorConfig(id: String, name: String, bpsLaneId: Option[String] = None, camera: CameraConfig, radar: RadarConfig,pir:PIRConfig)

case class Field[T](name:Symbol, value:T, isEnabled:Boolean=true, displayName:String="", displayDescription:String="", canAdd:Boolean=false, canChange:Boolean=false, canRead:Boolean=false)

/**
 *
 * @param uom
 * @param permissibleError
 * @param category
 * @param displayRange
 * @param temperatureRange
 * @param restrictiveConditions
 */
case class NmiCertificate(uom:Option[String] = None,
                          permissibleError:Option[String] = None,
                          category:Option[String] = None,
                          displayRange:Option[String] = None,
                          temperatureRange:Option[String] = None,
                          restrictiveConditions:Option[String] = None){

  /**
   *
   * @return
   */
  def isEmpty:Boolean={
    uom.isEmpty &&
      permissibleError.isEmpty &&
      category.isEmpty &&
      displayRange.isEmpty &&
      temperatureRange.isEmpty &&
      restrictiveConditions.isEmpty
  }

  /**
   *
   * @param that
   * @return
   */
  def union(that:NmiCertificate)={
    this.copy(
      uom = if(this.uom.isEmpty) that.uom else this.uom,
      permissibleError = if(this.permissibleError.isEmpty) that.permissibleError else this.permissibleError,
      category = if(this.category.isEmpty) that.category else this.category,
      displayRange = if(this.displayRange.isEmpty) that.displayRange else this.displayRange,
      temperatureRange = if(this.temperatureRange.isEmpty) that.temperatureRange else this.temperatureRange,
      restrictiveConditions = if(this.restrictiveConditions.isEmpty) that.restrictiveConditions else this.restrictiveConditions
    )
  }

  /**
   *
   * @param that
   * @return
   */
  def diff(that:NmiCertificate)={
    this.copy(
      uom = if(this.uom.isEmpty || this.uom == that.uom) None else this.uom,
      permissibleError = if(this.permissibleError.isEmpty || this.permissibleError == that.permissibleError) None else this.permissibleError,
      category = if(this.category.isEmpty || this.category == that.category) None else this.category,
      displayRange = if(this.displayRange.isEmpty || this.displayRange == that.displayRange) None else this.displayRange,
      temperatureRange = if(this.temperatureRange.isEmpty || this.temperatureRange == that.temperatureRange) None else this.temperatureRange,
      restrictiveConditions = if(this.restrictiveConditions.isEmpty || this.restrictiveConditions == that.restrictiveConditions) None else this.restrictiveConditions
    )
  }
}

/*
* Indicates what version of zaakbestand has to be used
*/
object CaseFileVersionType extends Enumeration {
  val TCVS33 /* old - IRS TC-VS versie 3.3 */ , HHMVS14 /* new - IRS HHM-VS 1.4 */ = Value
}

/**
 * information regarding the version of zaakbestand to use for reporting
 * @param version version of zaakbestand to use
 */
case class CaseFile(version:CaseFileVersionType.Value=CaseFileVersionType.TCVS33)

/**
 * represent a url. used to store as json in storage
 * @param url
 */
case class Url(url:String)

/**
 * Used by Dynamax This is created and stored once system wide
 * @param id
 * @param uri
 * @param checksumApp
 * @param checksumCfg
 * @param timeoutmSec
 * @param maxCacheUse
 * @param reportFile
 */
case class DynamaxIface(id:String, uri:String, checksumApp:String, checksumCfg:String, timeoutmSec:Long, maxCacheUse:Long,reportFile: String)

object ErrorSignOffType extends Enumeration {
  val FailureSignOff, AlertSignOff = Value
}

object MonitorEventsType extends Enumeration {
  val
  ServiceStart,
  ServiceStop,
  ServiceCreated = Value
}

/*
* Is used as an storage object that contains everything of a system
* @param id of system
* @param name of system
* @param that part of the system that is used to replace 'n999n' when creating case file mtmn999n.txt
* @param violationPrefixId used as part of folder name and by reporting. Not to be set by the user
* @param description of the system
* @param serialNumber of the system
* @param orderNumber used by day report
* @param radarCode of the system
* @param drivingDirectionFrom
* @param drivingDirectionTo
* @param maxSpeed
* @param compressionFactor
* @param nrDaysKeepTrafficData
* @param nrDaysKeepViolations
* @param region
* @param roadNumber
* @param roadCode
* @param roadPart
* @param esaProviderType
*/
case class System(   id:String,
                     name:String,
                     location:SystemLocation,
                     violationPrefixId:String,
                     orderNumber:Int,
                     maxSpeed:Int,
                     compressionFactor:Int,
                     retentionTimes:SystemRetentionTimes,
                     pardonTimes:ZkSystemPardonTimes,
                     esaProviderType : EsaProviderType.Value = EsaProviderType.NoProvider,
                     mtmRouteId:String,
                     minimumViolationTimeSeparation: Int = 0, /*minutes*/
                     serialNumber: SerialNumber,
                     title:String,
                     pshtmId:String)
{

  def isDynamax = esaProviderType == EsaProviderType.Dynamax
}

case class ZkSystemPardonTimes(pardonTimeEsa_secs: Int = 0,pardonTimeTechnical_secs: Int =0)
case class SystemRetentionTimes(nrDaysRemindCaseFiles:Int,
                          nrDaysKeepTrafficData:Int,
                          nrDaysKeepViolations:Int)
case class SystemLocation(description:String,
                    viewingDirection :Option[String],
                    region:String,
                    roadNumber:String,
                    roadPart:String,
                    systemLocation: String)

case class SerialNumber(serialNumber:String,
                        serialNumberPrefix: String = "",
                        serialNumberLength: Int = 5)

object EsaProviderType extends Enumeration {
  val Dynamax, Mtm, NoProvider,TubesProvider = Value
}

/**
 * Represents statistics for a day
 * @param id of DayStatistics
 * @param day for specific day
 * @param createDate when report was created
 * @param data compressed binary data that contains xml
 * @param fileName name of file
 */
case class DayStatistics(id:String, day:Long, createDate:Long, data:List[Int], fileName:String)

/*
* Is used as an storage object that contains everything of a Corridor
* @param id of corridor
* @param name of corridor
* @param roadType
* @param isInsideUrbanArea
* @param dynamaxMqId used by dynamax
* @param approvedSpeeds speeds used for dynamax
* @param pardonTimeEsa_secs
* @param pardonTimeTechnical_secs
* @param pshtm
* @param info
* @param startSectionId
* @param endSectionId
* @param allSectionIds
*/
case class Corridor(id:String,
                    name:String,
                    roadType:Int,
                    serviceType: ServiceType.Value,
                    caseFileType: ZkCaseFileType.Value = ZkCaseFileType.HHMVS20,
                    isInsideUrbanArea:Boolean,
                    dynamaxMqId:String,
                    approvedSpeeds:Seq[Int],
                    pshtm:PSHTMIdentification,
                    info:CorridorInfo,
                    startSectionId:String,
                    endSectionId:String,
                    allSectionIds:Seq[String],
                    direction:Direction,
                    radarCode:Int,
                    roadCode:Int,
                    dutyType:String,
                    deploymentCode:String,
                    codeText:String)


abstract class Service{
  val flgFileName: Option[String]
  val dynamicEnforcement: Boolean
  val pardonRedTime:Long = 0
  val minimumYellowTime:Long = 0
  val factNumber:String = ""
  val firstImage: Option[VehicleImageType.Value]
  val secondImage: Option[VehicleImageType.Value]
}

case class SpeedFixedService(flgFileName: Option[String] = Some("snelheid.flg"),
                             firstImage: Option[VehicleImageType.Value] = None,
                             secondImage: Option[VehicleImageType.Value] = None,
                             dynamicEnforcement: Boolean = false) extends Service

case class RedLightService(flgFileName: Option[String] = Some("roodlicht.flg"),
                           dynamicEnforcement: Boolean = false,
                           firstImage: Option[VehicleImageType.Value] = None,
                           secondImage: Option[VehicleImageType.Value] = None,
                           override val pardonRedTime: Long,
                           override val minimumYellowTime: Long,
                           override val factNumber:String) extends Service

object ZkCaseFileType extends Enumeration {
  val TCVS33 /* old - IRS TC-VS versie 3.3 */ = Value
  val HHMVS14 /* new - IRS HHM-VS 1.4 */ = Value
  val HHMVS40 /* new - IRS HHM-VS 4.0 */ = Value
  val HHMVS20  = Value


}
//service type of corridor
object ServiceType extends Enumeration {
  val RedLight, SpeedFixed, SpeedMobile, SectionControl, ANPR, TargetGroup = Value
}

/*
* Is used as an storage object that contains everything of a Corridor
* @param corridorId of corridor
* @param locationCode
* @param locationLine1
* @param locationLine2
*/
case class CorridorInfo(corridorId:Int,
                        locationCode:String,
                        locationLine1:String,
                        locationLine2:Option[String]=None,
                        reportId:Option[String]=None,
                        reportHtId:Option[String]=None,
                        reportPerformance:Boolean
)

/*
* Is used as an storage object that represents what parts of a PSHTM number must be used
* @param useYear
* @param useMonth
* @param useDay
* @param useReportingOfficerId
* @param useNumberOfTheDay
* @param useLocationCode
* @param useTypeViolation
* @param useFixedCharacter
* @param fixedCharacter
*/
case class PSHTMIdentification(useYear:Boolean,
                               useMonth:Boolean,
                               useDay:Boolean,
                               useReportingOfficerId:Boolean,
                               useNumberOfTheDay:Boolean,
                               useLocationCode:Boolean,
                               useHHMCode:Boolean,
                               useTypeViolation:Boolean,
                               useFixedCharacter:Boolean,
                               fixedCharacter:String,
                               elementOrderString:Option[String] = None)



/*
* Indicates to what configurable item an Alert/Failure belongs to
*/
object ConfigType extends Enumeration {
  val System, Corridor, Section, Gantry, Lane, Other = Value
}


trait SystemError {
  val path:String
  val message:String
  val timestamp:Long
  val configType:ConfigType.Value
  val confirmed:Boolean
}

/*
* Represents a System Failure
* @param path indicates the storage path where an error belongs to (e.g. ctes/systems/a2/corridors/c1)
* @param message contains the error message
* @param timestamp when the error happened
* @param confirmed indicates that user had confirmed the failure
* @param configType indicates to what configured item the error belongs to (e.g. System, Corridor ..)
*/
case class SystemFailure(path:String,  failureType: String,message:String, timestamp:Long, configType:ConfigType.Value, confirmed:Boolean=false) extends SystemError


/*
* Represents a System Alert
* @param path indicates the storage path where an error belongs to (e.g. ctes/systems/a2/corridors/c1)
* @param message contains the error message
* @param timestamp when the error happened
* @param confirmed indicates that user had confirmed the alert
* @param configType indicates to what configured item the error belongs to (e.g. System, Corridor ..)
* @param reductionFactor
*/
case class SystemAlert(path:String, alertType: String,message:String, timestamp:Long, configType:ConfigType.Value, confirmed:Boolean=false, reductionFactor: Float) extends SystemError


/*
* Represents a Gantry
* @param id
* @param systemId
* @param name
* @param hectometer
* @param longitude
* @param latitude
*/
case class Gantry(id:String, systemId:String, name:String, hectometer:String, longitude:Double, latitude:Double)


/*
* Represents a Pir
* @param pirHost
* @param pirPort
* @param pirAddress
* @param refreshPeriod
* @param vrHost
* @param vrPort
*/
case class PIRConfig(pirHost:String, pirPort:Int, pirAddress:Short, refreshPeriod:Long, vrHost:String, vrPort:Int)


/*
* Represents the config of a radar
* @param radarURI
* @param measurementAngle
* @param height
* @param surfaceReflection
*/
case class RadarConfig(radarURI:String, measurementAngle:Double, height:Double, surfaceReflection:Double)


/*
Camera Configuration needed for each Lane
@param relayURI path where the data is places for this lane
@param cameraHost The host of the camera
@param cameraPort The ports of the camera
@param maxTriggerRetries The maximum number of retrying to send a camera trigger
@param timeDisconnected The disconnected time when a disconnected systemEvent is send
 */
case class CameraConfig(relayURI: String, cameraHost:String, cameraPort:Int, maxTriggerRetries:Int, timeDisconnected:Long )


/*
* Indicates to what class a vehicle belongs to
*/
object VehicleCode extends Enumeration {
  val BS, BB,BF,PA, CA, CB, CP, MF, MV, AB, AT, VA, AO, AS,MH  = Value
}

/*
* Indicates a wheelbase type
*/
object ZkWheelbaseType extends Enumeration {
  val P2M, P2M3M, P3M4M, P4M5M, P5M, LBB2M, LBB2M3M, LBB3M4M, LBB4M5M, LBB5M = Value
}


/*
* Part of preference
* @param wheelBaseType
* @param value
*/
case class WheelBase(wheelBaseType:ZkWheelbaseType.Value, value:Double)


/*
* Part of preference
* @param vehicleType
* @param maxSpeed
*/
case class VehicleMaxSpeed(vehicleType:VehicleCode.Value, maxSpeed:Int)

/**
 * part of preferences
 * @param speedLimit
 * @param speedMargin
 */
case class SpeedLimitMargin(speedLimit:Int, speedMargin:Int=0)

/*
* Represents the preferences for a system
* @param wheelBase
* @param vehicleMaxSpeed
* @param marginSpeedLimits
* @param duplicateTriggerDelta
* @param inaccuracyMarginForLength
* @param minimumLengthTrailer
* @param shortVehicleLengthThreshold
* @param longVehicleLengthThreshold
* @param countryCodeConfidence
* @param maxLengthMoped
*/
case class Preference(wheelBase:List[WheelBase],
                      vehicleMaxSpeed:List[VehicleMaxSpeed],
                      marginSpeedLimits:List[SpeedLimitMargin],
                      duplicateTriggerDelta:Int = 0,
                      inaccuracyMarginForLength:Int = 0,
                      minimumLengthTrailer:Int = 0,
                      shortVehicleLengthThreshold:Int = 0,
                      longVehicleLengthThreshold:Int = 0,
                      countryCodeConfidence:Int,
                      licenseConfidence:Int=50,
                      maxLengthMoped:Double,
                      testLicenseConfidence:Boolean = false)

case class PreferenceSmallForPreferences(version:Int)

//Permitted actions
object ActionType extends Enumeration {
  type ActionType = Value

  val
  ChangeSystemStateOffStandbyMaintenance,// alleen voor technisch
  DownloadPassagegegevens,// alleen politie
  CorridorsCrud,// alleen politie, en technisch beheer (alleen corridor naam en passagepunten)
  UpdateSystem,// alleen politie
  CorridorsView,// iedereen
  SchedulesCrud,//politie
  SchedulesView,//iedereen
  SystemLog,// log inzien ->iedereen
  ErrorsSignOff,//afmelden van fouten, alleen tactisch
  AllRoles,// iedereen
  HHMStateChange,// wordt niet gebruikt op actions maar wel in formats
  ChangeSystemStateHandhavenStandby,// politie
  SystemsUpdate,// systeem updaten, vanuit bediening alleen politie
  CertificatesChange,// alleen technisch
  CertificatesView,//iedereen
  ParametersChange,// is preferences, alleen politie
  ParametersView,// is preferences, iedereen
  TreatyCountriesChange,// verdragslanden alleen tactisch
  TacticalAndTechnicalView,// Leesrechten voor tactisch en technisch beheer beheer
  TechnicalCud,// create, update, delete rechten. Alleen technisch beheer
  MonitorProcessView,// cloudera manager bekijken iedereen
  RolesView,// tachtisch & technisch
  RolesChange,// tachtisch
  UsersView,// tachtisch & technisch
  UsersCreate,// tachtisch
  UsersChange,// tachtisch
  UsersDelete// tachtisch

  = Value
}

import ActionType._
import csc.dlog.Event

/*
* Permitted actions that a role is allowed to do
* @param id
* @param actionType
* @param name
* @param description
*/
case class PermittedAction(id:String, actionType:ActionType, group:Option[String] = None, name:String="", description:String="",reportingofficeridRequired:Boolean=false)

/*
* Role where an user belongs to
* @param id
* @param name
* @param description
* @param permittedActions
*/
case class Role(id:String, name:String, description:String, permittedActions:List[PermittedAction])


//Schedule
object ZkIndicationType extends Enumeration {
  val True, False, None  = Value
}

/*
* Role where an user belongs to
* @param id
* @param indicationRoadworks
* @param indicationDanger
* @param indicationActualWork
* @param invertDrivingDirection
* @param fromPeriodHour
* @param fromPeriodMinute
* @param toPeriodHour
* @param toPeriodMinute
* @param fromPeriod
* @param toPeriod
* @param speedLimit
* @param signSpeed
* @param codeText
* @param signIndicator
* @param dutyType
* @param deploymentCode
*/
case class Schedule(id:String,
                    indicationRoadworks:ZkIndicationType.Value,
                    indicationDanger:ZkIndicationType.Value,
                    indicationActualWork:ZkIndicationType.Value,
                    invertDrivingDirection:ZkIndicationType.Value,
                    fromPeriodHour:Int,
                    fromPeriodMinute:Int,
                    toPeriodHour:Int,
                    toPeriodMinute:Int,
                    speedLimit:Int,
                    signSpeed:Int,
                    signIndicator:Boolean,
                    speedIndicatorType:Option[SpeedIndicatorType.Value] = None,
                    supportMsgBoard:Boolean,
                    msgsAllowedBlank:Int)


/*
* Role where an user belongs to
* @param id
* @param systemId
* @param name
* @param description
* @param length
* @param startGantryId
* @param endGantryId
* @param matrixBoards sequence of matrix board ids
*/
case class Section(id:String, systemId:String, name:String, description:String, length:Int, startGantryId:String, endGantryId:String, matrixBoards:Seq[String],msiBlackList: Seq[ZkMsiIdentification])

/**
 * ZkMsiIdentification uniquely define a Msi in a MTM file
 * @param os_comm_id
 * @param msi_rijstr_pos
 */
case class ZkMsiIdentification(os_comm_id: Int, msi_rijstr_pos: String, omschrijving: String = "") {
  override def equals(other: Any): Boolean = {
    other match {
      case that: ZkMsiIdentification ⇒
        (that canEqual this) &&
          that.os_comm_id == os_comm_id &&
          that.msi_rijstr_pos == msi_rijstr_pos
      case _ ⇒ false
    }
  }

  def canEqual(other: Any): Boolean =
    other.isInstanceOf[ZkMsiIdentification]

  override def hashCode: Int =
    41 * (this.os_comm_id.hashCode() + 41) + this.msi_rijstr_pos.hashCode
}
/*
* An user
* @param id
* @param reportingOfficerId
* @param phone
* @param ipAddress
* @param name
* @param username
* @param email
* @param passwordHash
* @param roleId
* @param sectionIds
*/
case class User(id:String,
                reportingOfficerId:String,
                phone:String,
                ipAddress:String,
                name:String,
                username:String,
                email:String,
                passwordHash:String,
                roleId:String,
                sectionIds:List[String],
                prevPasswordHash1:String,
                prevPasswordHash2:String,
                badLoginCount: Option[Int])


/*
* State of the system
* @param state
* @param timestamp
* @param userId
* @param reason
*/
case class SystemState(state:String, timestamp:Long, userId:String, reason:String)
case class CorridorState(state:String, timestamp:Long, userId:String, reason:String)


/*
* used by SystemEvent to indicate if a request for an action required one or more requests/events
* @param token
* @param last
*/
case class SystemEventToken(token:String, last:Boolean)


/*
* Used to request the backend for an action
* @param componentId
* @param timestamp
* @param systemId
* @param eventType
* @param userId
* @param reason
* @param token
*/
case class SystemEvent(componentId: String,
                       timestamp: Long,
                       systemId:String,
                       corridorId:Option[String] = None,
                       path:Option[String] = None,
                       eventType: String,
                       userId: String,
                       reason: String) extends Event


/*
* represents a log entry
* @param sequenceNr
* @param componentId
* @param timestamp
* @param systemId
* @param eventType
* @param userId
* @param reason
* @param token
*/
case class LogEntry(sequenceNr:Long,
                    componentId: String,
                    timestamp: Long,
                    systemId:String,
                    eventType: String,
                    userId: String,
                    userRole:String,
                    userName:String,
                    reason: String,
                    token:Option[SystemEventToken]=None,
                    corridorId:Option[String] = None)

/*
* represents a log entry
* @param sequenceNr
* @param componentId
* @param timestamp
* @param systemId
* @param eventType
* @param userId
* @param reason
*/
case class LogEntryModel(sequenceNr:Long,
                         componentId: String,
                         timestamp: Long,
                         systemId:String,
                         eventType: String,
                         userId: String,
                         systemName:String,
                         timestampDisplay: String,
                         eventDisplay: String,
                         componentDisplay: String,
                         userDisplay: String,
                         reason: String,
                         userRole:String="",
                         corridor:String="")

