package models.web

/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

import java.util.Date

import common.Models
import csc.curator.utils.Versioned
import csc.sectioncontrol.messages.certificates.{LocationCertificate, MeasurementMethodType}
import models.storage._
import play.api.i18n.Messages

import scala.collection.mutable.ListBuffer


case class PropertyChange(property:String,  previousValue:Any, newValue:Any){
  def showChange={
    if(previousValue.toString != newValue.toString)
      Some(Messages("property.change", property, previousValue, newValue))
    else
      None
  }
}

case class ServiceState(corridorId:String = "", allowStateChange:Boolean = false,  version:Int = 0, speed:Int = 0)
case class SystemUpdate(
                         name:String,
                         id:String,
                         version:Int,
                         isCalibrating:Boolean,
                         allowStateChange:Boolean,
                         alerts:Int,
                         failures:Int,
                         state:String,
                         stateDuration:String,
                         redLight:ServiceState,
                         speedFixed:ServiceState,
                         labels:String)


case class SpeedField(speedLimit:Int)
case class RedLightUpdate(isEnabled:Boolean, speedLimit:Int)
case class SpeedFixedUpdate(isEnabled:Boolean, speedLimit:Int)

case class HHM( systemId:String = "",
                signSpeed:Int = 0,
                redLight:RedLightUpdate = RedLightUpdate(false, 0),
                speedFixed:SpeedFixedUpdate = SpeedFixedUpdate(false, 0),
                signIndicator:Boolean = false,
                speedIndicatorType:Option[SpeedIndicatorType.Value] = None,
                indicationRoadworks:ZkIndicationType.Value = ZkIndicationType.None,
                indicationDanger:ZkIndicationType.Value = ZkIndicationType.None,
                indicationActualWork:ZkIndicationType.Value = ZkIndicationType.None,
                nrDaysRemindCaseFiles:Int = 0,
                sms1:String = "",
                sms2:String = "",
                sms3:String = "",
                version:Int=0)

case class HHMCRUD( systemId:String = "",
                    signSpeed:Int = 0,
                    redLight:SpeedField = SpeedField(0),
                    speedFixed:SpeedField = SpeedField(0),
                    signIndicator:Boolean = false,
                    speedIndicatorType:Option[SpeedIndicatorType.Value] = None,
                    nrDaysRemindCaseFiles:Int,
                    indicationRoadworks:ZkIndicationType.Value = ZkIndicationType.None,
                    indicationDanger:ZkIndicationType.Value = ZkIndicationType.None,
                    indicationActualWork:ZkIndicationType.Value = ZkIndicationType.None,
                    sms1:String = "",
                    sms2:String = "",
                    sms3:String = "",
                    version:Int=0)

case class ServiceEnabler(redLightEnabled:Boolean, speedFixedEnabled:Boolean)
case class SystemCopy(
                       services:ServiceEnabler,
                       violationPrefixId:String,
                       name:String,
                       description:String,
                       mtmRouteId:String,
                       hectometer:String,
                       longitude:Double,
                       latitude:Double
                       )

/**
 * used for updating nrDaysKeepTrafficData for a system
 * @param version
 * @param nrDaysKeepTrafficData
 */
case class UpdateDaysKeepTrafficData(version:Int, nrDaysKeepTrafficData:Int)

/**
 *
 * @param total
 * @param date
 */
case class ErrorInfo(total:Int, date:String)

/**
 * information regarding the version of zaakbestand to use for reporting
 * @param version version of zaakbestand to use
 */
case class CaseFileCRUD(version:CaseFileVersionType.Value)

/**
 * Used for zipping files
 * @param fileName name of individual file in zip file
 * @param data the content of the file
 */
case class FileItem(fileName:String, data:Array[Byte])
case class EsaProviderModel(name:String, value:EsaProviderType.Value)
case class RoadInfo(roadNumber:String, roadCode:Int, roadPart:String)
case class SystemModelCRUD(id:Option[String]=None,
                              version:Option[Int]=None,
                              nrDaysKeepViolations :Int,
                              nrDaysKeepTrafficData:Int,
                              pardonTimeEsa_secs: Int = 0,
                              pardonTimeTechnical_secs: Int = 0 )

case class SystemModelCRUDTechnical(id:Option[String]=None,
                            version:Option[Int]=None,
                            title:String,
                            description:String,
                            region:String,
                            roadNumber:String,
                            maxSpeed:Int,
                            compressionFactor:Int,
                            orderNumber:Int ,
                            serialNumber:String,
                            serialNumberPrefix:String,
                            serialNumberLength:Int)

/*
* Used to create or update a Gantry. An ajax call using json as data format must comply with this structure
* @param id of gantry, optional
* @param version of gantry, optional
* @param name of gantry
* @param hectometer
* @param longitude of gantry
* @param latitude of gantry
*/
case class GantryCRUD(id:Option[String]=None,
                      version:Option[Int]=None,
                      name:String,
                      hectometer:String,
                      longitude:Double,
                      latitude:Double)

/*
* Used to create or update a schedule. An ajax call using json as data format must comply with this structure
* @param id of corridor, optional
* @param version of corridor, optional
* @param newCorridorId
* @param newSystemId
* @param indicationRoadworks
* @param indicationDanger
* @param indicationActualWork
* @param invertDrivingDirection
* @param fromPeriodHour
* @param fromPeriodMinute
* @param toPeriodHour
* @param toPeriodMinute
* @param speedLimit
* @param signSpeed
* @param codeText
* @param signIndicator
* @param dutyType
* @param deploymentCode
*/
case class ScheduleCRUD(id:Option[String]=None,
                        version:Option[Int]=None,
                        newCorridorId:String,
                        newSystemId:String,
                        indicationRoadworks:ZkIndicationType.Value,
                        indicationDanger:ZkIndicationType.Value,
                        indicationActualWork:ZkIndicationType.Value,
                        invertDrivingDirection:ZkIndicationType.Value,
                        fromPeriodHour:Int,
                        fromPeriodMinute:Int,
                        toPeriodHour:Int,
                        toPeriodMinute:Int,
                        speedLimit:Int,
                        signSpeed:Int,
                        signIndicator:Boolean,
                        speedIndicatorType:Option[SpeedIndicatorType.Value] = None,
                        supportMsgBoard:Boolean,
                        msgsAllowedBlank:Int)

/**
 * Represents a model of storage object DayStatistics
 * @param id of DayStatistics
 * @param version of DayStatistics object in storage
 * @param day where stats belong to
 * @param dayDisplay localized display name
 * @param createDate date when Statistics was created
 * @param createDateDisplay localized display name
 */
case class StatisticsModel(id:String, version:Int, day:Date, dayDisplay:String, createDate:Date, createDateDisplay:String)

/*
* Is the overall model that contains everything of a system. Used by views and as return value for some ajax calls
* @param system object stored in storage
* @param isCalibrating indicates if the current system is calibrating
* @param hasCalibratePermission indicates if the current system can be calibrated by the user
* @param errors contains all alerts and failures of the current system
* @param availableStates contains all next states that the current system can be placed in
* @param isEditable indicates if the current system can be edited. Depends on the current state
* @param state current state of the system
* @param preferences contains all preferences of the system
* @param sections contains all configured sections of the system
* @param gantries contains all configured gantries of the system
* @param corridors contains all configured corridors of the system
*/
case class SystemModel(
                        system:Versioned[System],
                        isCalibrating:Boolean,
                        errors:Seq[ErrorModel],
                        availableStates:Seq[SystemNextStateModel],
                        preferences:Seq[Versioned[Preference]],
                        sections:Seq[SectionModel],
                        gantries:Seq[GantryModel],
                        corridors:Seq[CorridorModel],
                        dateOfCurrentState:String=""
                        ){

  /**
   * retrieves all statics for given system id
   */
  lazy val getStatistics:Seq[StatisticsModel] = Models.getStatistics(system.data.id)
}

trait KeyValue{
  val id:String
  val name:String
}


case class GantrySmallHandhaving(id:String,hectometer:String)
case class SectionSmall(id:String,startGantryId:String,endGantryId:String)
case class CorridorSmall(name:String,startSectionId:String,endSectionId:String)
case class SystemModelHandhaving(name:String,systemId:String,isCalibrating:Boolean,sections:Seq[SectionSmall],corridors:Seq[CorridorSmall],gantries:Seq[GantrySmallHandhaving],title:String)
case class GantrySmall(id:String, name:String)extends KeyValue
case class SystemModelSmall(id:String, name:String,violationPrefixId:String,gantries:Seq[KeyValue])extends KeyValue
case class SystemModelSmallForLogs(id:String, name:String)
case class SystemModelSmallForErrors(id:String, name:String,violationPrefixId:String,errors:Seq[ErrorModel])
case class SystemModelSmallForLanesList(systemId:String, systemName:String,gantryId:String,gantryName:String,laneId:String,laneName:String)

/*
* Used by schedule to indicate what sign bord type it is
*/
object LogType extends Enumeration {
  val System, Corridor, Section, Gantry, Lane, Other = Value
}

/*
* Used to request a system to calibrate itself. Used by Systems
*/
object CalibrationType extends Enumeration {
  type CalibrationType = Value

  val CalibrationOn, CalibrationOff = Value
}


/*
* Used to request a system state change. An ajax call using json as data format must comply with this structure
* @param from the current system state
* @param to the requested next state
* @param version of the current state
*/
case class StateChangeCRUD(from:SystemStateType.Value, to:SystemStateType.Value, version:Int)


case class ServiceStateRequest(systemId:String,
                               corridorId:String,
                               from:Boolean,
                               to:Boolean,
                               version:Int)

/*
* Used to indicate what other available states a system can be placed in, based on the current state. Used by SystemModel
* @param state next possible system state
* @param name localized name of state
*/
case class SystemNextStateModel(state:SystemStateType.Value, name:String)

/*
* The different states that a system can be placed in
*/
object SystemStateType extends Enumeration {
  type SystemStateType = Value
  val Off, StandBy, Maintenance, Failure, EnforceOn, EnforceOff, EnforceDegraded = Value
}

case class CorridorStateContainer(state: SystemStateType.Value, reductionFactor: Int, version: Int)

/*
* Is the overall model that contains everything of a gantry, used by SystemModel
* @param id of gantry
* @param version of gantry
* @param systemId id of system
* @param name of gantry
* @param longitude of gantry
* @param latitude of gantry
* @param laneConfigs contains all configured lanes of a gantry
*/
case class GantryModel(id:String, version:Int, systemId:String, name:String, hectometer:String,  longitude:Double, latitude:Double, laneConfigs:Seq[LaneConfigModel])
case class GantryModelSmallForSystems(longitude:Double, latitude:Double)
case class GantryModelSmallForCorridors(id:String, version:Int, systemId:String, name:String, hectometer:String,  longitude:Double, latitude:Double, laneConfigs:Seq[LaneConfigModelSmall])
case class GantryModelForList(id:String, version:Int,systemId:String, name:String, hectometer:String, longitude:Double, latitude:Double,isInUse:Boolean)

/*
* Is the overall model that contains everything of a lane, used by GantryModel
* @param id of lane, optional
* @param version of lane, optional
* @param name of lane
* @param camera The host of the camera
* @param radar configuration of a lane
* @param pir configuration of a lane
*/
case class LaneConfigModel(version:Int, sensor:SensorConfig)
case class LaneConfigModelSmall()

/*
* Used to create or update a section. An ajax call using json as data format must comply with this structure
* @param id of section, optional
* @param version of section, optional
* @param name of section
* @param description of section
* @param length of section in meters
* @param startGantryId is the first gantry of a section
* @param endGantryId is the end gantry of a section
*/
case class SectionCRUD(id:Option[String]=None, version:Option[Int]=None, name:String, description:String,
                       length:Int, startGantryId:String, endGantryId:String, matrixBoards:String)



/*
* Is the overall model that contains everything of a section, used by SystemModel
* @param id of section
* @param version of
* @param name of section
* @param description of section
* @param length of section in meters
* @param sortingOrder used by gui for sorting purposes
* @param isStartInChain first one configured in chain of sections
* @param isLastInChain last one configured in chain of sections
* @param startGantryId is the first gantry of a section
* @param endGantryId is the end gantry of a section
* @param matrixBoards sequence of matrix board ids
*/
case class SectionModel(id:String,
                        version:Int,
                        name:String,
                        description:String,
                        length:Int,
                        sortingOrder:Int,
                        isStartInChain:Boolean,
                        isLastInChain:Boolean,
                        startGantryId:String,
                        startGantryName:String,
                        endGantryId:String,
                        endGantryName:String,
                        matrixBoards:Seq[String],
                        isInUse:Boolean)

case class ClassifyCountriesModel(version:Int,
                        mobiCountries:Seq[String],
                        similarCountries:Seq[String])

case class ScheduleInfo(deploymentCode:String, codeText:String, dutyType:String, invertDrivingDirection:ZkIndicationType.Value)

/*
* Used to create or update a corridor. An ajax call using json as data format must comply with this structure
* @param id of corridor, optional
* @param version of corridor, optional
* @param name of corridor
* @param approvedSpeeds approved speeds for dynamax
* @param roadType
* @param dynamaxMqId
* @param isInsideUrbanArea
* @param pshtm represents PSHTM
* @param pardonTimeEsa_secs
* @param pardonTimeTechnical_secs
* @param locationCode
* @param locationLine1
* @param locationLine2
* @param specificCorridorId
* @param startSectionId
* @param endSectionId
*/
case class CorridorModelCRUD(id:Option[String]=None,
                             version:Option[Int]=None,
                             name:String,
                             roadType:Int,
                             isInsideUrbanArea:Boolean,
                             locationCode:String,
                             locationLine1:String,
                             locationLine2:String,
                             direction:Direction,
                             startSectionId:String,
                             endSectionId:String,
                             dutyType:String,
                             deploymentCode:String,
                             radarCode:Int,
                             codeText:String,
                             roadCode:Int
                             )

/*
* Is the overall model that contains everything of a corridor, used by SystemModel
* @param id of corridor
* @param version of corridor
* @param name of corridor
* @param roadType
* @param isInsideUrbanArea
* @param dynamaxMqId
* @param approvedSpeeds speeds used for dynamax
* @param pshtm represents PSHTM
* @param pardonTimeEsa_secs
* @param pardonTimeTechnical_secs
* @param locationCode
* @param locationLine1
* @param locationLine2
* @param specificCorridorId
* @param startSectionId
* @param endSectionId
*/
case class CorridorModel(id:String,
                         version:Int,
                         service:Option[Service],
                         name:String,
                         roadType:Int,
                         isInsideUrbanArea :Boolean,
                         serviceType: ServiceType.Value,
                         caseFileType: ZkCaseFileType.Value,
                         dynamaxMqId:String,
                         approvedSpeeds:Seq[Int],
                         pshtm:PSHTMIdentification,
                         schedules:Seq[ScheduleModel],
                         locationCode:String,
                         locationLine1:String,
                         locationLine2:String,
                         specificCorridorId:Int,
                         startSectionId:String,
                         endSectionId:String,
                         allSectionIds:Seq[String],
                         direction:Direction,
                         radarCode:Int=0)

case class Direction(directionFrom:String="",directionTo:String="")
case class CorridorModelForCorridors(id:String,
                                     version:Int,
                                     serviceType: ServiceType.Value,
                                     locationCode:String,
                                     locationLine1:String,
                                     locationLine2:String,
                                     specificCorridorId:Int
                                      )
case class CorridorModelForHhm(service:Option[Service],serviceType: ServiceType.Value)

/*
* Is the overall model that contains everything of a schedule, used by CorridorModel
* @param id of corridor, optional
* @param version of corridor, optional
* @param indicationRoadworks
* @param indicationDanger
* @param indicationActualWork
* @param invertDrivingDirection
* @param fromPeriodHour
* @param fromPeriodMinute
* @param toPeriodHour
* @param toPeriodMinute
* @param speedLimit
* @param signSpeed
* @param codeText
* @param signIndicator
* @param dutyType
* @param deploymentCode
*/
case class ScheduleModel(id:String,
                         version:Int,
                         isActive:Boolean,
                         indicationRoadworks:ZkIndicationType.Value,
                         indicationDanger:ZkIndicationType.Value,
                         indicationActualWork:ZkIndicationType.Value,
                         invertDrivingDirection:ZkIndicationType.Value,
                         fromPeriodHour:Int,
                         fromPeriodMinute:Int,
                         toPeriodHour:Int,
                         toPeriodMinute:Int,
                         speedLimit:Int,
                         signSpeed:Int,
                         signIndicator:Boolean,
                         speedIndicatorType:Option[SpeedIndicatorType.Value] = None
                          )


/*
* Represents an Alert or a Failure
* @param id of alert/failure
* @param systemId if of system
* @param systemName name of system
* @param path indicates the storage path where an error belongs to (e.g. ctes/systems/a2/corridors/c1)
* @param message contains the error message
* @param timestamp when the error happened
* @param timestampDisplay localized date format of timestamp
* @param configType indicates to what configured item the error belongs to (e.g. System, Corridor ..)
* @param configTypeDisplay localized name of configType
* @param errorType if the error is an Alert or a Failure
* @param errorTypeDisplay localized name of errorType
*/
case class ErrorModel(id:String,
                      systemId:String,
                      systemName:String,
                      path:String,
                      message:String,
                      timestamp:Long,
                      timestampDisplay:String,
                      confirmed:Boolean=false,
                      configType:ConfigType.Value,
                      configTypeDisplay:String,
                      errorType:String,
                      errorTypeDisplay:String) extends SystemError

/*
* Used by ErrorModel to indicate what type of error it is
*/
object ErrorType extends Enumeration {
  type ErrorType = Value
  val Failure, Alert = Value
}

case class OtherPreferences(treatyCountries:List[String])

case class PreferenceModel(preferences:Versioned[Preference])//, other:OtherPreferences)

/*
* Used to update an existing preference for a system. An ajax call using json as data format must comply with this structure
* @param version of preference
* @param preference contains all preferences of current system
*/
case class PreferenceCRUD(version:Int, preference:Preference)


/*
* Used to delete an object in storage. An ajax call using json as data format must comply with this structure
* @param version of an object in storage
*/
case class DeleteModel(version:Int)


/*
* Used by ResponseMsg to indicate what type of response message it is
*/
object ResponseType extends Enumeration {
  type ResponseType = Value
  val System, Validate, Other,Import = Value
}

import models.web.ResponseType._

/*
* Used as a response message to the user to indicates if a requested action was successfully completed
* @param success indicates if the requested action was successfully completed
* @param typeName the type of response
* @param description of the response message
* @param fields used when there are validating errors (typeName=Validate).
*/
case class ResponseMsg(success:Boolean=false, id:String = "",  typeName:ResponseType.Value=Validate, description:String="", fields:Seq[FieldError]=List())


/*
* Used by ResponseMsg to indicates if a argument of a case class is incorrect
* example
*
* case class Person(age:Int)
* val person = Person(age=-10)
* If the rule is that a person's age must me >=0 than a FieldError could look like this
*
* FieldError(field="age", value="-10", error="age must be =>0")
*
* @param field indicates the name of the field where its value is incorrect
* @param value contains the value of the field that is incorrect
* @param error contains an user friendly description why the value of a field is incorrect
*/
case class FieldError(field:String="", value:String="", error:String)


//TODO CLEAN THIS UP
//===========================================================================================================

case class RoleModel(id:String,name:String,description:String,numberOfPeople:Int=0,version:Int,actionIds:List[String], permittedActions:List[PermittedAction])

case class UpdateRoleModel(id:String,name:String, description:String,version:Int,actionIds:List[String]){
  import common.Extensions._
  private val items = new ListBuffer[FieldError]
  val numberOfPeople:Int=0
  val permittedActions:List[PermittedAction]=List()

  validate

  private def validate{
    if(name == null || name.toOption.isEmpty)
      items.append(FieldError(field="name", value=name, error=Messages("validate.name_is_required")))
    if(description == null || description.toOption.isEmpty)
      items.append(FieldError(field="description", value=description, error=Messages("validate.description_is_required")))
  }

  def hasErrors:Boolean={
    items.length > 0
  }

  def errors:Option[List[FieldError]]={
    if(hasErrors){
      Some(items.toList)
    }else{
      None
    }
  }
}




case class UpdateUserModel(id:String,
                           reportingOfficerId:String,
                           name:String,
                           email:String,
                           phone:String,
                           ipAddress:String,
                           username:String,
                           password:String,
                           roleId:String,
                           sectionIds:List[String],
                           changePassword:Boolean,
                           version:Int){

  private val items = new ListBuffer[FieldError]
  validate

  private def validate{

    val isNewUser = id.isEmpty

    if(isNewUser && password.isEmpty)
      items.append(FieldError(field="password", value=password, error=Messages("validate.password_is_required")))

    if(!isNewUser && changePassword && password.isEmpty)
      items.append(FieldError(field="password", value=password, error=Messages("validate.password_is_required")))

    if(name.isEmpty)
      items.append(FieldError(field="name", value=name, error=Messages("validate.name_is_required")))

    if(username.isEmpty)
      items.append(FieldError(field="username", value=username, error=Messages("validate.username_is_required")))

    if(roleId.isEmpty)
      items.append(FieldError(field="roleId", value=roleId, error=Messages("validate.role_is_required")))

    if(!this.reportingOfficerId.isEmpty && !this.reportingOfficerId.matches("[A-Z0-9]{6}"))
      items.append(FieldError(field="reportingOfficerId", value=roleId, error=Messages("validate.reportingOfficerId_is_required")))
  }

  def hasErrors: Boolean = items.nonEmpty

  def errors:Option[List[FieldError]]={
    if(hasErrors){
      Some(items.toList)
    }else{
      None
    }
  }
}

case class UserModel(id:String,
                     reportingOfficerId:String,
                     name:String,
                     email:String,
                     phone:String,
                     ipAddress:String,
                     username:String,
                     password:String,
                     roleId:String,
                     sectionIds:List[String],
                     role:RoleModel,
                     passwordHash:String,
                     version:Int,
                     sections:Seq[Versioned[System]],
                     prevPasswordHash1:String,
                     prevPasswordHash2:String,
                     userBlocked: Boolean
                      ){
  def hasPermissions(actionTypes:ActionType.Value*) = role.permittedActions.exists(s => actionTypes.contains(s.actionType))
  def hasPermission(actionType:ActionType.Value) = role.permittedActions.exists(_.actionType == actionType)
  def sectionNames = sections.sortBy(_.data.name).map(_.data.name).mkString(",")
  def permittedActions = role.permittedActions
}

case class Seal(seal:String)
case class LocationCertificateCRUD(id:Option[String] = None,version:Option[Int] = None, systemId:String, certificate:LocationCertificate)
case class TypeCertificateCRUD(id:Option[String] = None,version:Option[Int] = None, certificate:MeasurementMethodType)
case class InstalledCertificateShow(id:String, version:Int, systemId:String, systemName: String, installDate: Long, active:Boolean, nrInstalled:Int, certificate:MeasurementMethodType,installDateDisplay:String="")
case class InstalledCertificateCreate(installDate: Long, typeId: String)
case class InstalledCertificateMultiCreate(installDate: Long, typeId: String, systems:List[String])
case class ComponentProcessedUntil(step:String, lastUpdated:String, processedUntil: String, details:List[ComponentProcessedUntil])
case class MonitorProcessUntil(systemId:String, systemName:String, components:List[ComponentProcessedUntil])
case class TypeCertificateList(isAvailable:Boolean,id:Option[String] = None,typeCertificateId : String, revisionNr : Int,typeDesignation:String,category:String,unitSpeed:String,unitLength:String)
case class Credential(username:String, password:String)
case class Authorized(isAuthorized:Boolean, isBlocked:Boolean, description:String)