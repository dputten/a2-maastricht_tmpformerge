package common

import models.storage.{SpeedIndicatorType, ZkIndicationType, ZkCaseFileType, EsaProviderType}


object DefaultSettings {
  import play.api.Play.current
  private lazy val config = play.api.Play.configuration
  object System{
    def compressionFactor = config.getInt("ctes.settings.system.compressionFactor").getOrElse(0)
    def compressCaseFiles = config.getBoolean("ctes.settings.system.compressCaseFiles").getOrElse(false)
    def minimumViolationTimeSeparation = config.getInt("ctes.settings.system.minimumViolationTimeSeparation").getOrElse(0)
    def nrDaysKeepTrafficData = config.getInt("ctes.settings.system.nrDaysKeepTrafficData").getOrElse(0)
    def esaProviderType = EsaProviderType.withName(config.getString("ctes.settings.system.esaProviderType").getOrElse("NoProvider"))
    def mtmRouteId = config.getString("ctes.settings.system.mtmRouteId").getOrElse("a002l")
  }
  object Gantry{
    def id = Some(config.getString("ctes.settings.gantry.id").getOrElse("1"))
    def name = config.getString("ctes.settings.gantry.name").getOrElse("1")
  }
  object Section{
    def name = config.getString("ctes.settings.section.name").getOrElse("1")
    def description = config.getString("ctes.settings.section.description").getOrElse("")
    def length = config.getInt("ctes.settings.section.length").getOrElse(1)
    def matrixBoards = config.getString("ctes.settings.section.matrixBoards").getOrElse("")
  }
  object Corridor{
    def caseFileType = ZkCaseFileType.withName(config.getString("ctes.settings.corridor.caseFileType").getOrElse("HHMVS20"))
    def pardonTimeEsa_secs = config.getInt("ctes.settings.corridor.pardonTimeEsa_secs").getOrElse(0)
    def dynamaxMqId = config.getString("ctes.settings.corridor.dynamaxMqId").getOrElse("")
    def pardonTimeTechnical_secs = config.getInt("ctes.settings.corridor.pardonTimeTechnical_secs").getOrElse(0)
  }
  object Schedule{
    def indicationRoadworks = ZkIndicationType.withName(config.getString("ctes.settings.schedule.indicationRoadworks").getOrElse("None"))
    def indicationDanger = ZkIndicationType.withName(config.getString("ctes.settings.schedule.indicationDanger").getOrElse("None"))
    def indicationActualWork = ZkIndicationType.withName(config.getString("ctes.settings.schedule.indicationActualWork").getOrElse("None"))
    def fromPeriodHour = config.getInt("ctes.settings.schedule.fromPeriodHour").getOrElse(0)
    def fromPeriodMinute = config.getInt("ctes.settings.schedule.fromPeriodMinute").getOrElse(0)
    def toPeriodHour = config.getInt("ctes.settings.schedule.toPeriodHour").getOrElse(24)
    def toPeriodMinute = config.getInt("ctes.settings.schedule.toPeriodMinute").getOrElse(0)
    def signSpeed = config.getInt("ctes.settings.schedule.signSpeed").getOrElse(50)
    def signIndicator = config.getBoolean("ctes.settings.schedule.signIndicator").getOrElse(true)
    def speedIndicatorType = SpeedIndicatorType.withName(config.getString("ctes.settings.schedule.speedIndicatorType").getOrElse("A1"))

    object RedLight{
      def speedLimit = config.getInt("ctes.settings.schedule.redlight.speedLimit").getOrElse(20)
    }
    object SpeedFixed{
      def speedLimit = config.getInt("ctes.settings.schedule.speedfixed.speedLimit").getOrElse(80)
    }
  }
}
