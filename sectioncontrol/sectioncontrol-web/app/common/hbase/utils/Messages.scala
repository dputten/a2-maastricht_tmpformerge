package common.hbase.utils

import csc.sectioncontrol.messages.VehicleMetadata

case class Vehicle(metadata: VehicleMetadata, overview: Option[Array[Byte]], license: Option[Array[Byte]],metadataNew: VehicleMetadata = null)
