package common.hbase.utils

/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

import csc.sectioncontrol.messages.{ValueWithConfidence_Float, ValueWithConfidence, VehicleMetadata}
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.hbase.client.{Get, HTable, Scan}
import net.liftweb.json.{Formats, Serialization, DefaultFormats}

import scala.math
import scala.math.Ordering

/**
 * Generic trait to read values from somewhere.
 *
 * The keys to use are of type K and the values are read as type T.
 *
 * @author Maarten Hazewinkel
 */
trait Reader[K, T <: AnyRef] {
  /**
   * Reads a single row, indicated by the exact key value supplied.
   */
  def readRow(key: K)(implicit mt: Manifest[T]): Option[T]

  /**
   * Reads a range of rows based on the from and to key values.
   * The key values need not match actual rows exactly but can also lie between actual row indexes.
   */
  def readRows(from: K, to: K)(implicit mt: Manifest[T]): Seq[T]
}

/**
 * Generic trait to read values from HBase.
 * This is a fairly simple reader that reads a single column only.
 *
 * The keys to use are of type K and the values are read as type T.
 *
 * @author Maarten Hazewinkel
 */
trait HBaseReader[K, T <: AnyRef] extends Reader[K, T] {
  /**
   * Defines the table to read from.
   */
  def hbaseReaderTable: HTable

  /**
   * Defines the column family to read the data from.
   */
  def hbaseReaderDataColumnFamily: String

  /**
   * Defines the exact column name (column qualifier) to read the data from.
   */
  def hbaseReaderDataColumnName: String

  /**
   * Transforms a key value into a byte array that will be used as the row index in HBase.
   */
  def makeReaderKey(keyValue: K): Array[Byte]

  /**
   * Transforms a byte array read from HBase into the value target type T.
   */
  def deserialize(bytes: Array[Byte])(implicit mt: Manifest[T]): T

  private implicit def string2ByteArray(s: String): Array[Byte] = Bytes.toBytes(s)

  /**
   * Reads a single row from HBase, indicated by the exact key value supplied.
   */
  override def readRow(key: K)(implicit mt: Manifest[T]): Option[T] = {
    val get = new Get(makeReaderKey(key))
    get.addColumn(hbaseReaderDataColumnFamily, hbaseReaderDataColumnName)
    val result = hbaseReaderTable.get(get)
    if (!result.isEmpty) {
      Some(deserialize(result.getValue(hbaseReaderDataColumnFamily, hbaseReaderDataColumnName)))
    } else {
      None
    }
  }

  /**
   * Reads a range of rows from HBase based on the from and to key values.
   * The key values need not match actual rows exactly but can also lie between actual row indexes.
   *
   * Note that all rows are read into memory and returned in a single object. Suggest not to use to
   * read a lot of large objects at once.
   */
  override def readRows(from: K, to: K)(implicit mt: Manifest[T]): Seq[T] = {
    import collection.JavaConversions._

    val (startKey, stopKey) = (makeReaderKey(from), makeReaderKey(to))
    val scan = new Scan(startKey, stopKey)
    scan.addColumn(hbaseReaderDataColumnFamily, hbaseReaderDataColumnName)
    val scanner = hbaseReaderTable.getScanner(scan)
    try {
      scanner.map(r ⇒ deserialize(r.getValue(hbaseReaderDataColumnFamily, hbaseReaderDataColumnName))).toSeq
    } finally {
      scanner.close()
    }
  }
}

/**
 * Extend the HbaseReader trait to use lift-json based serialization.
 *
 * @author Maarten Hazewinkel
 */
trait JsonHBaseReader[K, T <: AnyRef] extends HBaseReader[K, T] {
  /**
   * Deserialize the byte array into the target type T using lift-json serialization.
   */
  override def deserialize(bytes: Array[Byte])(implicit mt: Manifest[T]): T = {
    Serialization.read[T](Bytes.toString(bytes))(jsonReaderFormats, mt)
  }

  /**
   * The default formats can be overridden to add support for otherwise not serializable
   * types. For instance Enumerations can be supported by overriding this with:
   * {{{
   * override def jsonReaderFormats = super.jsonReaderFormats + new EnumerationSerializer(EnumClass1, EnumClass2)
   * }}}
   */
  def jsonReaderFormats: Formats = DefaultFormats
}

//case class ValueWithConfidence_Float(value: Float, confidence: Int = 0)

/**
 * Two vehicle registrations matched on a corridor
 * @param id ID of the entry
 * @param entry entry vehicle registration
 * @param exit  exit vehicle registration
 * @param corridorId  corridorId of the corridor
 * @param speed measured or detected speed
 * @param measurementSHA SHA of the JAR file when the speed was assigned
 * @param extLength externally provided length (when detected on other locations then entry or exit gantries)
 */
case class VehicleSpeedRecord(id: String,
                              entry: VehicleMetadata,
                              exit: Option[VehicleMetadata],
                              corridorId: Int,
                              speed: Int,
                              measurementSHA: String,
                              extLength: Option[ValueWithConfidence_Float] = None) {


  def time: Long = if (exit.isDefined) exit.get.eventTimestamp else entry.eventTimestamp

  /**
   * Find the length with max confidence value (from entry, exit and externally provided values)
   */
  def length: Option[ValueWithConfidence_Float] = {
    var values: Seq[ValueWithConfidence_Float] = Nil
    values ++= exit.flatMap(_.length)
    values ++= extLength
    values ++= entry.length
    values.reduceOption( (x,y) => if (x.confidence > y.confidence) x else y ) //returns the one with highest confidence
  }

  def country: Option[ValueWithConfidence[String]] = {
    var values: Seq[ValueWithConfidence[String]] = Nil
    values ++= getCountry(entry)
    values ++= exit.flatMap(getCountry(_))
    values.reduceOption( (x,y) => if (x.confidence > y.confidence) x else y ) //returns the one with highest confidence
  }

  def getCountry(msg: VehicleMetadata): Option[ValueWithConfidence[String]] = {
    val country = msg.licensePlateData match {
      case Some(lpd) => lpd.country //try first the country of licensePlateData
      case None => None
    }

    if (country.isEmpty) msg.country //otherwise legacy field
    else country
  }

}