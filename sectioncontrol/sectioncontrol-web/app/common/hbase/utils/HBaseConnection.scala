/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package common.hbase.utils

import play.Play
import org.apache.hadoop.hbase.HBaseConfiguration

trait HBaseConnection {
  self : common.Config =>
  lazy val hbaseConfig = {
    val conn = Config.Hbase.host match {
      case null | "" => Config.Zookeeper.host
      case x => x
    }

    val config = HBaseConfiguration.create()
    config.set("hbase.zookeeper.quorum", conn)
    config
  }
}