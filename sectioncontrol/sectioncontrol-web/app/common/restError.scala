package common

import play.api.i18n.Messages
import models.web.{ResponseType, ResponseMsg}

object ErrorFactory{
  def systemNotFound():ResponseMsg=
  {
    ResponseMsg(success=false, typeName=ResponseType.System, description=Messages("system.not_found"))
  }
}
