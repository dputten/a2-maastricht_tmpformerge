/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package common

import java.security.MessageDigest

import csc.curator.utils.Curator
import sun.misc.BASE64Encoder

//import play.classloading.enhancers.LocalvariablesNamesEnhancer
import csc.config.Path
import org.apache.hadoop.hbase.util.Bytes

object Int {
  def unapply(s : String) : Option[Int] = try {
    Some(s.toInt)
  } catch {
    case _ : java.lang.NumberFormatException => None
  }
}

/**
 * extensions
 */
object Extensions {
  implicit def stringWrapper(value:String) = new StringExtensions(value)
  implicit def CuratorWrapper(curator:Curator) = new CuratorExtensions(curator)

  /**
   * function that creates a key name that is used as a zookeeper node
   * @param key
   */
  def asConfigKey(key:String)=key.replaceAll("[^a-zA-Z0-9_]","")

}

/**
 * wrapper class containing ZkPathStorage extension methods
 */
class CuratorExtensions(val curator:Curator){

  /**
   * 
   * @param from
   * @param to
   */
  def copyDataRecursively(from:Path, to:Path){
    def loop(path:Path){
      curator.getChildren(path).foreach{ childPath =>
        val newToPath = Path(childPath.toString().replaceFirst (from.toString(),to.toString()))
        copyData(childPath, newToPath)
        loop(childPath)
      }
    }
    loop(from)
  }
  
  /**
   * copy the raw content (byte array) from one path and paste it to the given path
   * @param from path to read the data from
   * @param to path were to copy the data to
   */
  def copyData(from:Path, to:Path){
    writeData(to, readData(from))
  }

  /**
   * Reads the raw dat (byte array) from zookeeper
   * @param path to read data from
   * @return the raw data stored in zookeeper
   */
  def readData(path:Path):Array[Byte]={
    curator.curator.map(_.getData.forPath(path.toString)).getOrElse(Array.empty)
  }

  /**
   * Writes a byter array to storage
   * @param to path to write to
   * @param data byte array
   */
  def writeData(to:Path, data:Array[Byte]){
    curator.curator.map(_.create().creatingParentsIfNeeded().forPath(to.toString, data))
  }

  def updateData(to:Path, data:Array[Byte], version:Int){
    curator.curator.map(_.setData().withVersion(version).forPath(to.toString, data))
  }

  /**
   * reads the data as byte array and transform is to string
   * @param path to data to read
   * @return string representation of byte array value stored in storage
   */
  def getAsString(path:Path):String={
    Bytes.toString(readData(path))
  }
}


/**
 * wrapper class containing String extension methods
 */
class StringExtensions(val value:String){

  /**
   * encodes a string into base64
   * @param algorithm
   */
  def digest(algorithm:String="SHA-256"):String ={
    val md = MessageDigest.getInstance(algorithm);
    md.update(value.getBytes("UTF-8"));
    (new BASE64Encoder).encode(md.digest)
  }

  /**
   * extension method that turns a null or empty string into an option
   */
  def toOption:Option[String]={
    if(value == null || value.trim.length == 0) None else Some(value)
  }
}

object PasswordHasher {

  def main(args: Array[String]): Unit = {
    println(new StringExtensions(args(0)).digest())
  }
}

