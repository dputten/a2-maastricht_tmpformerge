/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package common

import java.io.{ ByteArrayInputStream, ByteArrayOutputStream }
import java.util.zip.{ GZIPInputStream, Deflater, GZIPOutputStream }

//TODO 20120504 MJ->RR: need a v2.8 version of scala-arm

/**
 * used for compressing(gzip) byte arrays
 */
trait GZIPCompressor {
  /**
   * compresses a byte array using GZip
   * @param data to be compressed
   * @return compressed version of data
   */
  def compress(data: Array[Byte]): Array[Byte] = {
    val os = new ByteArrayOutputStream
    val gos = new GZIPOutputStream(os, Deflater.BEST_COMPRESSION)

    try {
      gos.write(data)
      gos.close()
      os.toByteArray
    } finally {
      //TODO 20120502 RR->MJ: try gos.close finally os.close
      //TODO 20120502 MJ->RR Nope gos.close() has to come before os.toByteArray otherwise an exception occurs
      gos.close()
      os.close()
    }
  }

  /**
   * decompress a byte array using GZip.
   * Warning: This will only work if memory is larger than the given data and the decompress data combined
   * @param data to be decompressed
   * @return decompressed version of data
   */
  def decompress(data: Array[Byte]): Array[Byte] = {
    //TODO: how to get ride of result altogether
    var result = new Array[Byte](0)

    using(new GZIPInputStream(new ByteArrayInputStream(data))) {
      is ⇒ result = Stream.continually(is.read()).takeWhile(-1 != _).map(_.toByte).toArray
    }

    result
  }

  //TODO 20120502 RR->MJ: use scala-arm instead.
  private def using[T <: { def close() }](resource: T)(block: T ⇒ Unit) {
    try {
      block(resource)
    } finally {
      if (resource != null) resource.close()
    }
  }
}
