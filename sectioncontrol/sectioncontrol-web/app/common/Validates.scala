/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package common

import models.storage._
import net.liftweb.json.DefaultFormats
import csc.json.lift.EnumerationSerializer
import models.web._
import csc.curator.utils.Versioned
import play.api.i18n.Messages
import services.{Corridors, Lanes, Preferences, Systems}
import csc.sectioncontrol.storage.Paths

/**
 * contains validators that the services use by creating, updating and deleting objects
 */
trait Validates extends EnumHack {

  def systemWideUnique2(systemId:String)(f:(System, Seq[Corridor], Seq[SensorConfig]) => Boolean)(implicit user:UserModel):Boolean={
    Systems.findById(systemId).exists {
      system => f(
        system.data,
        Corridors.getAllBySystemId(systemId).map(_.data),
        Lanes.getAllBySystemId(systemId).map(_.data))
    }
  }

  /**
   * validates that a System is in edit mode
   * @param systemId is id of system
   * @param fieldKey is the field name given back to a FieldError fails
   * @param errorKey is the message id that is contained in conf/messages
   * @return an optional FieldError
   */
  def systemMustBeInEditMode(systemId:String, fieldKey:String="systemId", errorKey:String="validate.system_not_in_edit_mode"):Option[FieldError]={
    if(!Systems.isEditable(systemId)){
      Some(FieldError(field=fieldKey, error=Messages(errorKey)))
    }else{
      None
    }
  }

  /**
   * validates that a System has a default state in storage
   * @param systemId is id of system
   * @param model contains the state tat the user sees (from) and the state where it wants to go (to)
   * @param fieldKey is the field name given back to a FieldError fails
   * @param errorKey is the message id that is contained in conf/messages
   * @return an optional FieldError
   */
  def stateMustBeSameAsPreviousState(systemId:String, corridorId:String,model:StateChangeCRUD, fieldKey:String="", errorKey:String="validate.state_does_not_exist"):Option[FieldError]={
    Systems.getStatePath(systemId,corridorId).map{
      versionOf =>
        if(versionOf.version == model.version && versionOf.data.state == model.from.toString){
          None
        }else{
          Some(FieldError(field=fieldKey, error=Messages(errorKey)))
        }
    }.getOrElse(None)
  }


  /**
   * validates that a System has a default state in storage
   * @param systemId is id of system
   * @param fieldKey is the field name given back to a FieldError fails
   * @param errorKey is the message id that is contained in conf/messages
   * @return an optional FieldError
   */
  def stateMustExist(systemId:String, corridorId:String,fieldKey:String="", errorKey:String="validate.state_does_not_exist"):Option[FieldError]={

    if(!Systems.statePathExists(systemId,corridorId)){
      println("ERROR")
      Some(FieldError(field=fieldKey, error=Messages(errorKey)))
    }else{
      None
    }
  }

  /**
   * validates that a System with the a systemId exists in storage
   * @param systemId is id of system
   * @param fieldKey is the field name given back to a FieldError fails
   * @param errorKey is the message id that is contained in conf/messages
   * @return an optional FieldError
   */
  def systemMustExist(systemId:String, fieldKey:String="systemId", errorKey:String="validate.system_does_not_exist"):Option[FieldError]={
    if(!Systems.exists(systemId)){
      Some(FieldError(field=fieldKey, error=Messages(errorKey)))
    }else{
      None
    }
  }

  /**
   * validates that a Preference with the a systemId exists in storage
   * @param systemId is id of system
   * @param fieldKey is the field name given back to a FieldError fails
   * @param errorKey is the message id that is contained in conf/messages
   * @return an optional FieldError
   */
  def preferenceMustExist(systemId:String, fieldKey:String="", errorKey:String="validate.preference_does_not_exist"):Option[FieldError]={
    if(!Preferences.exists(systemId)){
      Some(FieldError(field=fieldKey, error=Messages(errorKey)))
    }else{
      None
    }
  }

  /**
   * validates that a Preference with the given systemId does not exists in storage
   * @param systemId is id of system
   * @param fieldKey is the field name given back to a FieldError fails
   * @param errorKey is the message id that is contained in conf/messages
   * @return an optional FieldError
   */
  def preferenceMustNotExist(systemId:String, fieldKey:String="", errorKey:String="validate.preference_already_exist"):Option[FieldError]={
    if(Preferences.exists(systemId)){
      Some(FieldError(field=fieldKey, error=Messages(errorKey)))
    }else{
      None
    }
  }

  /**
   * validates that a given value is not null or empty
   * @param value to validate
   * @param fieldKey is the field name given back to a FieldError fails
   * @param errorKey is the message id that is contained in conf/messages
   * @return an optional FieldError
   */
  def notEmpty(value:String, fieldKey:String, errorKey:String):Option[FieldError]={
    val tmp:Option[String] = Option(value).filter(_.trim.nonEmpty)
    if(tmp.isEmpty){
      Some(FieldError(field=fieldKey, error=Messages(errorKey)))
    } else{
      None
    }
  }
  
  /**
   * validates that a value is between two given values
   * @param value to be checked
   * @param from minimum that value is allowed to be
   * @param to max that value is allowed to be
   * @param fieldKey is the field name given back to a FieldError when the check fails
   * @param errorKey is the message id that is contained in conf/messages
   * @return an optional FieldError
   */
  def betweenRange[T <% Ordered[T]](value:T, from:T, to:T, fieldKey:Option[String]=None, errorKey:Option[String]=None):Option[FieldError]={
    if(value < from || value > to){
      val field = fieldKey.getOrElse("")
      val defaultMessage = if(from == to)
                              Messages("validate.value_not_between_range_same", from.toString)
                           else
                              Messages("validate.value_not_between_range", from.toString, to.toString)
      val message = errorKey.map(Messages(_, from.toString, to.toString)).getOrElse(defaultMessage)
      Some(FieldError(field=field, error=message))
    }else{
      None
    }
  }

  /**
   * validates that a value is at least higher then given value
   * @param value to be checked
   * @param from minimum that value is allowed to be
   * @param fieldKey is the field name given back to a FieldError when the check fails
   * @param errorKey is the message id that is contained in conf/messages
   * @return an optional FieldError
   */
  def notLowerThen[T <% Ordered[T]](value:T, from:T, fieldKey:Option[String]=None, errorKey:Option[String]=None):Option[FieldError]={
    if(value < from){
      val field = fieldKey.getOrElse("")
      // added .tostring because 1025 became 1,025
      val message = errorKey.getOrElse(Messages("validate.value_to_low", from.toString))
      Some(FieldError(field=field, error=message))
    }else{
      None
    }
  }
}