package common.actors

import csc.imageretriever._

case class StartDownloadingImagesFromGantry(systemId: String, gantryId: String, imageKeys: Seq[ImageKey])

case class GetDownloadStatus(refId: String)

sealed trait DownloadStatus { def refId: String }

case class DownloadInProgress(refId: String) extends DownloadStatus

case class DownloadCompleted(refId: String) extends DownloadStatus
