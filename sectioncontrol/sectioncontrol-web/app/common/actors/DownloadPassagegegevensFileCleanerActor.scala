package common.actors

import java.io.File

import akka.actor.Actor
import akka.event.LoggingReceive
import play.Logger

object Wakeup

/**
 * Actor to trigger the FileCleaner
 */
class DownloadPassagegegevensFileCleanerActor(fileCleaner: FileCleaner) extends Actor {
  override protected def receive = LoggingReceive {
    case Wakeup =>
      Logger.debug("Received wake up message. Time to start cleaning.")
      fileCleaner.clean()
    case unknown =>
      Logger.debug("Received an unknown message: " + unknown)
  }
}

/**
 * Clean files at a certain location if they are older than the interval
 */
class FileCleaner(path: String, retentionInSeconds: Int) {
  def clean() {
    val nowEpochInSeconds = toSeconds(java.lang.System.currentTimeMillis())
    val file = new File(path)

    if (file.isDirectory)
      file.listFiles.foreach(f => {
        if (nowEpochInSeconds - toSeconds(f.lastModified()) > retentionInSeconds) {
          Logger.debug("Deleting file: " + f.getName)
          f.delete()
        }
      })
  }

  private def toSeconds(millis: Long): Long = millis / 1000
}
