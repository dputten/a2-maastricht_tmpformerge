package common.actors

import akka.actor.{Actor, ActorLogging, ActorRef}
import akka.event.LoggingReceive
import csc.imageretriever._

/**
 * Used to keep track of the status of different download jobs for images
 * 
 * @param imageRetrieverActor reference to the actor that actually downloads the images from the gantry 
 */
class DownloadPassageGegevensImagesActor(val imageRetrieverActor: ActorRef) extends Actor with ActorLogging {
  var jobId = System.currentTimeMillis() // Use time to make sure id will be different even after restart
  var downloadCompleted = Map.empty[String, Boolean] // (RefId -> isCompleted?)

  override protected def receive = LoggingReceive {

    case StartDownloadingImagesFromGantry(systemId, gantryId, imageKeys) =>
      val refId = jobId.toString
      log.info("[refId: %s] Starting to download images from Gantry...".format(refId))
      imageRetrieverActor ! GetImageBatchRequest(refId, systemId, gantryId, imageKeys.toList)
      sender ! DownloadInProgress(refId)
      downloadCompleted += (refId -> false)
      jobId = jobId + 1

    case BatchProgressResponse(refId, total, count, errorCount) =>
      log.info("[refId: %s] Downloading in progress %d/%d with errors %d".format(refId, count, total, errorCount))
      if (count + errorCount == total) downloadCompleted = downloadCompleted.updated(refId, true)

    case GetDownloadStatus(refId) =>
      downloadCompleted.get(refId).foreach { isCompleted =>
        log.info("[refId: %s] Completed = %b".format(refId, isCompleted))
        if (isCompleted) {
          sender ! DownloadCompleted(refId)
          downloadCompleted -= refId
        } else {
          sender ! DownloadInProgress(refId)
        }
      }
  }
}