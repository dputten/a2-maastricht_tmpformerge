package common

import java.util.UUID

import akka.actor.{ActorRef, Props}
import com.typesafe.config.ConfigFactory
import common.Extensions._
import common.actors.{FileCleaner, DownloadPassagegegevensFileCleanerActor, Wakeup, DownloadPassageGegevensImagesActor}
import csc.imageretriever._
import csc.imageretriever.actor.RetrieveImageActor
import models.storage.ActionType._
import models.storage._
import models.web.RoleModel
import play.api.Play.current
import play.api._
import play.api.libs.concurrent.Akka
import services._
import akka.util.duration._

object Global extends GlobalSettings with Config{

  private lazy val imageRetriever: ActorRef = Akka.system.actorOf(Props(new RetrieveImageActor(ImageRetrieverConfig(ConfigFactory.load()))))
  private lazy val downloadPassagegegevensFileCleaner: ActorRef = Akka.system.actorOf(Props(new DownloadPassagegegevensFileCleanerActor(
    new FileCleaner(Config.DownloadPassagegegevens.path,Config.DownloadPassagegegevens.retentionInSeconds))))
  var downloadPassageGegevensImageActor: ActorRef = _

  override def onStart(app: Application) {
    InitialUsersAndRoles.doJob
    downloadPassageGegevensImageActor = Akka.system.actorOf(Props(new DownloadPassageGegevensImagesActor(imageRetriever)))
    Akka.system.scheduler.schedule(1 second, Config.DownloadPassagegegevens.cleanInterval seconds, downloadPassagegegevensFileCleaner, Wakeup)
  }

}

object InitialUsersAndRoles extends Config with Storage{
    private val policeRolePermittedAction =
    List(
      DownloadPassagegegevens,// alleen politie
      CorridorsCrud,// alleen politie
      UpdateSystem,// alleen politie
      CorridorsView,// iedereen
      SchedulesCrud,//politie
      SchedulesView,//iedereen
      SystemLog,// log inzien ->iedereen
      AllRoles,// iedereen
      ChangeSystemStateHandhavenStandby,// politie
      SystemsUpdate,// systeem updaten, vanuit bediening alleen politie
      CertificatesView,//iedereen
      ParametersChange,// is preferences, alleen politie,
      ParametersView,// is preferences, alleen politie
      MonitorProcessView// cloudera manager bekijken iedereen
    )

    private val tacticalRolePermittedAction =
    List(
      TacticalAndTechnicalView,// Leesrechten voor tactisch en technisch beheer beheer
      ParametersView,// is preferences, alleen politie
      CorridorsView,// iedereen
      SchedulesView,//iedereen
      SystemLog,// log inzien ->iedereen
      ErrorsSignOff,//afmelden van fouten
      AllRoles,// iedereen
      CertificatesView,//iedereen
      TreatyCountriesChange,// verdragslanden alleen tactisch
      MonitorProcessView,// cloudera manager bekijken iedereen
      RolesView,// tachtisch & technisch
      RolesChange,// tachtisch
      UsersView,// tachtisch & technisch
      UsersCreate,// tachtisch
      UsersChange,// tachtisch
      UsersDelete// tachtisch
)

    private val technicalRolePermittedAction =
    List(
      TechnicalCud,// create, update, delete rechten. Alleen technisch beheer
      TacticalAndTechnicalView,// Leesrechten voor tactisch en technisch beheer beheer
      ParametersView,// is preferences, alleen politie
      ChangeSystemStateOffStandbyMaintenance,// alleen voor technisch
      CorridorsView,// iedereen
      SchedulesView,//iedereen
      SystemLog,// log inzien ->iedereen
      ErrorsSignOff,//afmelden van fouten
      AllRoles,// iedereen
      SystemsUpdate,// systeem updaten, vanuit bediening alleen politie
      CertificatesChange,// alleen technisch
      CertificatesView,//iedereen
      MonitorProcessView,// cloudera manager bekijken iedereen
      RolesView,// tachtisch & technisch
      UsersView// tachtisch & technisch
    )

    private val policeRole = Config.DefaultRoles.police.copy (
      id = UUID.randomUUID.toString,
      permittedActions = PermittedActions.getAll.filter{pa => policeRolePermittedAction.exists(_ == pa.actionType)}
    )

    private val tacticalRole = Config.DefaultRoles.tactical.copy (
      id = UUID.randomUUID.toString,
      permittedActions = PermittedActions.getAll.filter{pa => tacticalRolePermittedAction.exists(_ == pa.actionType)}
    )

    private val technicalRole =  Config.DefaultRoles.technical.copy (
      id = UUID.randomUUID.toString,
      permittedActions = PermittedActions.getAll.filter{pa => technicalRolePermittedAction.exists(_ == pa.actionType)}
    )

  private val defaultUser = Config.defaultUser.copy(
    id = UUID.randomUUID.toString,
    passwordHash = Config.defaultUser.username.digest(),
    roleId = technicalRole.id,
    sectionIds = Systems.getAll.map(s=>s.data.id).toList
  )

  private val defaultUserTactical = Config.defaultUser.copy(
    name="lptv",
    username="lptv",
    id = UUID.randomUUID.toString,
    passwordHash = "lptv".digest(),
    roleId = tacticalRole.id,
    sectionIds = Systems.getAll.map(s=>s.data.id).toList
  )

  private val defaultUserPolitie = Config.defaultUser.copy(
    name="politie",
    username="politie",
    id = UUID.randomUUID.toString,
    passwordHash = "politie".digest(),
    roleId = policeRole.id,
    sectionIds = Systems.getAll.map(s=>s.data.id).toList
  )

    /**
     * inserts a default User an Role in zookeeper
     */
    def doJob(){
      if(Config.inDevelopmentMode)
      {
        val roles:Seq[RoleModel]=Roles.getAll
        roles.foreach(r=>Roles.deleteOne(r.id,r.version))

        val users=Users.getAll
        users.foreach(r=>Users.deleteOne(r.id,r.version))

        addRole(technicalRole)
        addRole(tacticalRole)
        addRole(policeRole)

        addUser(defaultUser,technicalRole)
        addUser(defaultUserTactical,tacticalRole)
        addUser(defaultUserPolitie,policeRole)
      }
    }

    private def addUser(newUser:User,role:Role){
      Roles.findByName(role.name) match{
        case Some(role) =>
          Users.findByUserName(newUser.username) match{
            case Some(user) =>
            case None => curator.put(Paths.User.default / newUser.id, newUser.copy(roleId = role.id))
          }
        case None =>
      }
    }

    private def addRole(newRole:Role){
      Roles.getAll.find(_.name == newRole.name) match {
        case Some(role) =>
        case None => curator.put(Paths.Role.default / newRole.id, newRole)
      }
    }
}

