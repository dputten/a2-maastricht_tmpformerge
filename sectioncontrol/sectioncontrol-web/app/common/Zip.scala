package common

import java.io.FileOutputStream
import java.util.zip.{ZipEntry, ZipOutputStream}

import models.web.FileItem
import play.Logger

/**
 * Create a zip file and add the files in fileItems at a certain location
 */
class Zip {
  def compress(path: String, fileName: String, fileItems: Seq[FileItem]) {
    val fos: FileOutputStream = new FileOutputStream(path + fileName)
    val zos: ZipOutputStream = new ZipOutputStream(fos)

    Logger.debug("creating passagegegevens zip at %s with name %s".format(path, fileName))

    try {
      fileItems.foreach(f => {
        Logger.debug("adding %s to zip".format(f.fileName))
        zos.putNextEntry(new ZipEntry(f.fileName))
        zos.write(f.data)
        zos.closeEntry()
      })
    } catch {
      case e: Exception => Logger.error(e.getMessage)
    } finally {
      zos.close()
      fos.close()
    }
  }
}
