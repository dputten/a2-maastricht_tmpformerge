package common

import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.messages.VehicleImageType
import csc.sectioncontrol.storage.Decorate.DecorationType
import models.storage._
import models.web.SystemStateType
import net.liftweb.json.DefaultFormats

/**
 * Because we use fully qualified enum names in zookeeper (e.g. VehicleImageType$Overview)
 * the (de)serialization needed to be adapted. Now we need to register all the enums that
 * require a special treatment to prevent the (de)serialization from failing.
 */
trait EnumHack {

  val enumFormats = DefaultFormats + new EnumerationSerializer(
    ActionType,
    ConfigType,
    SystemStateType,
    ZkWheelbaseType,
    VehicleCode,
    ZkIndicationType,
    ZkCaseFileType,
    ServiceType,
    EsaProviderType,
    SpeedIndicatorType,
    DecorationType,
    VehicleImageType
  )

}
