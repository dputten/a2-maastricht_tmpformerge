/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package common

import services.Systems
import play.api.i18n.Messages
import models.web.UserModel

/**
 * Logger used to send system events to storage
 */
trait SystemLogger {
  /**
   * helper method to retrieve a message via play framework
   * @param messageId
   * @param args
   * @return
   */
  def getMessage(messageId:String, args:List[Any]=List()):String={
    Messages(messageId, args.map(_.asInstanceOf[AnyRef]) : _*)
  }

  /**
   * send system event to storage
   * @param messageId
   * @param systemId
   */
  def log(messageId:String, systemId:String="", args:List[Any]=List())(implicit user:UserModel){
    Systems.sendSystemEvent(getMessage(messageId, args), systemId=systemId)
  }

  /**
   * send system event to storage
   * @param messageId
   * @param systemId
   */
  def logWithEventType(messageId:String, systemId:String="", args:List[Any]=List(), eventType:String)(implicit user:UserModel){
    Systems.sendSystemEvent(getMessage(messageId, args), systemId=systemId, eventType=eventType)
  }

  /**
   * send system event to given systemids
   * @param messageId
   * @param systemIds
   */
  def logMultipleSystemIds(messageId:String, systemIds:Seq[String], args:List[Any]=List())(implicit user:UserModel){
    val reason = getMessage(messageId, args)
    systemIds.foreach{
      systemId =>
        log(reason, systemId=systemId)
    }
  }

  /**
   * send system event to all available systems
   * @param messageId
   */
  def logForAllSystems(messageId:String, args:List[Any]=List())(implicit user:UserModel){
    val reason = getMessage(messageId, args)
    Systems.getAll.foreach{
      system =>
        Systems.sendSystemEvent(reason, systemId=system.data.id)
    }
  }
}
