/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package common

import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.zip.{ZipEntry, ZipOutputStream}
import java.util.{Calendar, Date, Locale, TimeZone}

import csc.curator.utils.Versioned
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.messages.VehicleImageType
import csc.sectioncontrol.storage.Decorate.DecorationType
import csc.sectioncontrol.storagelayer.alerts.AlertService
import models.storage.{Gantry, LogEntry, LogEntryModel, PSHTMIdentification, PreferenceSmallForPreferences, Section, SensorConfig, System, _}
import models.web.{CorridorModel, Direction, ErrorModel, FileItem, GantryModel, GantryModelSmallForCorridors, GantryModelSmallForSystems, GantrySmall, LaneConfigModel, LaneConfigModelSmall, ScheduleModel, SectionModel, ServiceState, StatisticsModel, SystemModel, SystemModelSmall, SystemModelSmallForErrors, SystemModelSmallForLanesList, SystemModelSmallForLogs, SystemStateType, UserModel, _}
import net.liftweb.json._
import play.api.i18n.Messages
import services._

/**
 * used by SystemModel because max 22 parameters limit had been reached
 */
object Models extends Formats

/**
 * used to convert a zookeeper object to a model
 */
trait Formats extends Config with Storage {

  implicit def comp2ord[A <: Comparable[A]] = new Ordering[A] {
    def compare(x : A, y : A) = x compareTo y
  }

  implicit def ordering2order[A : Ordering](x : A): Ordered[A] with Object {def compare(y: A): Int} = new Ordered[A] {
    def compare(y : A) = implicitly[Ordering[A]].compare(x, y)
  }

  /**
   * convert an object to json
   */
  def toLiftJson[T<:AnyRef](model:T):String={
    Serialization.write[T](model)(enumFormats)
  }

  /**
   * convert json to model
   */
  def toModel[T<:AnyRef](json:String)(implicit u : Manifest[T]):T={
    Serialization.read[T](json)(enumFormats, manifest[T])
  }

  /**
   * zip all files
   */
  def zip(fileItems:Seq[FileItem]):Array[Byte]={
    val baos = new ByteArrayOutputStream
    val zos = new ZipOutputStream(baos)

    try{
      fileItems.foreach{
        fileItem =>
          val entry = new ZipEntry(fileItem.fileName)
          entry.setSize(fileItem.data.length)
          zos.putNextEntry(entry)
          zos.write(fileItem.data)
          zos.closeEntry()
      }
    }catch {
      case e:Exception =>
    }finally {
      zos.close()
    }

    baos.toByteArray
  }

  /**
   * gets a SystemModel
   */
  protected def getSystem(systemId:String)(implicit user:UserModel):Option[SystemModel]={
    Systems.findById(systemId) match {
      case Some(versionOf) => Some(createSystemModel(versionOf))
      case _ => None
    }
  }

  def getReductionFactor(systemId:String):Float={
    val corridors=Corridors.getAllBySystemId(systemId)
    val corridorIds= corridors.map(versionOf => versionOf.data.id)

    val result: Seq[Float] =corridorIds.map(corridorId => AlertService.getReductionFactor(curator, systemId, corridorId))
    result.head
  }

  case class CorridorKeyValue(id:String,name:String)

  /**
   * creates a sequence of LogEntryModels from a given list of Log Entries
   */
  protected def toLogEntryModel(logs:Seq[LogEntry],systemId:String):Seq[LogEntryModel]={
    type Key = String
    type Translation = String

    val translations:Map[Key, Translation] = Configuration.translations(Config.language)
    def translate(key:Key):Translation = translations.getOrElse(key, key)

    val corridors = Corridors.getAllBySystemId(systemId)
    val corridorsForSystem = corridors.map {
      versionOf =>
        CorridorKeyValue(
          id = versionOf.data.id,
          name = versionOf.data.name
        )
    }

    logs.foldLeft(List[LogEntryModel]()){
      (list, logEntry) =>
        val eventTypeDisplay = "logs.eventType." + logEntry.eventType.replaceAll("-",".")
        LogEntryModel(
          sequenceNr=logEntry.sequenceNr,
          componentId=logEntry.componentId,
          timestamp=logEntry.timestamp,
          systemId=logEntry.systemId,
          eventType=logEntry.eventType,
          userId=logEntry.userId,
          systemName=Systems.findById(logEntry.systemId).map(_.data.name).getOrElse(logEntry.systemId),
          timestampDisplay=formatDateToCEST(logEntry.timestamp),
          eventDisplay=translate(eventTypeDisplay) match{
            case nt:String if nt == eventTypeDisplay => logEntry.eventType
            case t:String => t
          },
          componentDisplay=translate("logs.componentType." + logEntry.componentId),
          userDisplay=logEntry.userName,
          userRole=logEntry.userRole,
          reason = logEntry.reason,
          corridor = corridorsForSystem.find(corridor => logEntry.corridorId.exists(_ == corridor.id))
                                       .map(corridor => corridor.name).getOrElse("")
        ) :: list
    }
  }
  case class ServiceStateInfo(redLight:ServiceState, speedFixed:ServiceState)

  /**
   * returns a sequence of error models
   */
  protected def getErrors(systemId:String, systemName:String)(implicit user:UserModel):Seq[models.web.ErrorModel]={
    Errors.getFailures(systemId).foldLeft(List[ErrorModel]()){
      (list, versionOf) => createErrorModel(systemId, Some(systemName), versionOf._1, versionOf._3.data, ErrorType.Failure.toString,versionOf._2) :: list
    } ::: Errors.getAlerts(systemId).foldLeft(List[ErrorModel]()){
      (list, versionOf) => createErrorModel(systemId, Some(systemName), versionOf._1, versionOf._3.data, ErrorType.Alert.toString,versionOf._2) :: list
    }
  }

  /**
   * creates a ErrorModel that is based on a SystemFailure or SystemAlert object
   */
  def createErrorModel(systemId:String, systemName:Option[String], errorId:String, error:SystemError, errorType:String,corridorId:String):ErrorModel={
    ErrorModel(
      id = errorId,
      path = corridorId,
      systemId = systemId,
      systemName = systemName.getOrElse(Systems.findById(systemId).map{_.data.name}.getOrElse("")),
      message = error.message,
      timestamp = error.timestamp,
      timestampDisplay = formatDateToCEST(error.timestamp),
      configType = error.configType,
      configTypeDisplay = Messages("errors.configType." +  error.configType.toString),
      errorType = errorType,
      confirmed = error.confirmed,
      errorTypeDisplay = Messages("errors.errorType." + errorType.toString)
    )
  }

  /**
   * transforms a given date into a CEST string format
   */
  def formatDateToCEST(timestamp:Long, dateFormat:String="local.dateformat"):String={
    val cal = Calendar.getInstance(TimeZone.getTimeZone("CET"))
    val localCountry = Messages("local.country")
    val localLanguage = Messages("local.language")
    val localFormat = Messages(dateFormat)
    val format = new SimpleDateFormat(localFormat, new Locale(localLanguage, localCountry))

    cal.setTimeInMillis(timestamp)
    format.format(cal.getTime)
  }

  /**
   * transforms a given ControlSystem object into a system model
   */
  def createSystemModel(versionOf:Versioned[System])(implicit user:UserModel):SystemModel={
    SystemModel(
      system = versionOf,
      errors = getErrors(versionOf.data.id, versionOf.data.name),
      availableStates = null,
      isCalibrating = Systems.isCalibrating(versionOf.data.id),
      preferences = Preferences.getBySystemId(versionOf.data.id).toSeq,
      sections = getSections(versionOf.data.id),
      gantries = getGantries(versionOf.data.id),
      corridors = getCorridors(versionOf.data.id),
      dateOfCurrentState = formatDateToCEST(Calendar.getInstance().getTimeInMillis,"local.dateformat.datetime"))
  }

  def createSystemModelSmallForLogs(versionOf:Versioned[System])(implicit user:UserModel):SystemModelSmallForLogs={
    SystemModelSmallForLogs(id=versionOf.data.id,name=versionOf.data.name)
  }

  def createSystemModelSmall(versionOf:Versioned[System])(implicit user:UserModel):SystemModelSmall={
    val gantries: Seq[GantryModel] = getGantries(versionOf.data.id)
    val gantriesSmall =gantries.map(g=>new GantrySmall(id=g.id,name=g.name))
    SystemModelSmall(id=versionOf.data.id,name=versionOf.data.title,violationPrefixId=versionOf.data.violationPrefixId,gantriesSmall)
  }

  def createSystemModelSmallForErrors(versionOf:Versioned[System])(implicit user:UserModel):SystemModelSmallForErrors={
    SystemModelSmallForErrors(id=versionOf.data.id,name=versionOf.data.name,violationPrefixId=versionOf.data.violationPrefixId,errors = getErrors(versionOf.data.id, versionOf.data.name))
  }

  def createSystemModelSmallForLanesList(versionOf:Versioned[System])(implicit user:UserModel):Seq[SystemModelSmallForLanesList] ={
    val gantries: Seq[GantryModel] = getGantries(versionOf.data.id)

    val x: Seq[SystemModelSmallForLanesList] =gantries.flatMap(g => {
      g.laneConfigs.map(l => SystemModelSmallForLanesList(
        systemId = versionOf.data.id,
        systemName = versionOf.data.name,
        gantryId = g.id,
        gantryName = g.name,
        laneId = l.sensor.id,
        laneName = l.sensor.name
      ))
    })

    x
  }

  /**
   * get all system models that an user has permission te see
   */
  def getAllSystems(implicit user:UserModel):Seq[SystemModel]={
    Systems.getAll.map(createSystemModel)
  }

  def getSystemsForErrors(implicit user:UserModel):Seq[SystemModelSmallForErrors]={
    Systems.findByIds(user.sectionIds.toSet.toSeq).map(createSystemModelSmallForErrors)
  }

  // Get all systems and map them to a id-name pair
  def getAllSystemsSmall(implicit user:UserModel):Seq[SystemModelSmall]={
    Systems.getAll.map(createSystemModelSmall)
  }

  def getSystems(implicit user:UserModel):Seq[SystemModel]={
    Systems.findByIds(user.sectionIds.toSet.toSeq).map(createSystemModel)
  }

  def getSystemsForLogs(implicit user:UserModel):Seq[SystemModelSmallForLogs]={
    Systems.findByIds(user.sectionIds.toSet.toSeq).map(createSystemModelSmallForLogs)
  }

  def getSystemsForLanesForList(implicit user:UserModel):Seq[SystemModelSmallForLanesList]={
    val result: Seq[Seq[SystemModelSmallForLanesList]] =Systems.findByIds(user.sectionIds.toSet.toSeq).map(createSystemModelSmallForLanesList)
    result.flatten
  }

  /**
   * creates and returns a sequence of Statistics models based on the systemId
   */
  def getStatistics(systemId:String):Seq[StatisticsModel]={
    val statistics = Statistics.getAllBySystemId(systemId)
    statistics.map{
      versionOf =>
        StatisticsModel(
          id = versionOf.data.id,
          version = versionOf.version,
          day = new Date(versionOf.data.day),
          dayDisplay = formatDateToCEST(versionOf.data.day, "local.dateformat.days"),
          createDate = new Date(versionOf.data.createDate),
          createDateDisplay = formatDateToCEST(versionOf.data.createDate)
        )
    }.sortWith((a, b) => a.day after b.day)
  }

  /**
   * creates and returns a Gantry model based on the systemId and Gantry
   */
  protected def createGantryModel(systemId:String, versionOf:Versioned[Gantry])(implicit user:UserModel):GantryModel={
    GantryModel(
      version = versionOf.version,
      id = versionOf.data.id,
      systemId = versionOf.data.systemId,
      name = versionOf.data.name,
      hectometer = versionOf.data.hectometer,
      longitude = versionOf.data.longitude,
      latitude = versionOf.data.latitude,
      laneConfigs = getLanes(systemId,versionOf.data.id))
  }



  protected def createGantryModelSmallForCorridors(systemId:String, versionOf:Versioned[Gantry])(implicit user:UserModel):GantryModelSmallForCorridors={
    GantryModelSmallForCorridors(
      version = versionOf.version,
      id = versionOf.data.id,
      systemId = versionOf.data.systemId,
      name = versionOf.data.name,
      hectometer = versionOf.data.hectometer,
      longitude = versionOf.data.longitude,
      latitude = versionOf.data.latitude,
      laneConfigs = getLanesSmall(systemId,versionOf.data.id))
  }

  /**
   *creates and returns multiple Corridor models
   */
  def getCorridors(systemId:String)(implicit user:UserModel):Seq[CorridorModel]={
    val corridors = Corridors.getAllBySystemId(systemId)
    corridors.map {
      versionOf =>
        val coridorService = Services.get(systemId, versionOf.data.id)
        CorridorModel(
          version = versionOf.version,
          id = versionOf.data.id,
          service = coridorService,
          serviceType = versionOf.data.serviceType,
          caseFileType = versionOf.data.caseFileType,
          name = versionOf.data.name,
          roadType = versionOf.data.roadType,
          isInsideUrbanArea = versionOf.data.isInsideUrbanArea,
          dynamaxMqId = versionOf.data.dynamaxMqId,
          approvedSpeeds = versionOf.data.approvedSpeeds,
          pshtm = PSHTMIdentification(
            useYear = versionOf.data.pshtm.useYear,
            useMonth = versionOf.data.pshtm.useMonth,
            useDay = versionOf.data.pshtm.useDay,
            useReportingOfficerId = versionOf.data.pshtm.useReportingOfficerId,
            useNumberOfTheDay = versionOf.data.pshtm.useNumberOfTheDay,
            useLocationCode = versionOf.data.pshtm.useLocationCode,
            useHHMCode = versionOf.data.pshtm.useHHMCode,
            useTypeViolation = versionOf.data.pshtm.useTypeViolation,
            useFixedCharacter = versionOf.data.pshtm.useFixedCharacter,
            fixedCharacter = versionOf.data.pshtm.fixedCharacter,
            elementOrderString = versionOf.data.pshtm.elementOrderString),
          schedules = getSchedules(systemId, versionOf.data.id),
          locationCode = versionOf.data.info.locationCode,
          locationLine1 = versionOf.data.info.locationLine1,
          locationLine2 = versionOf.data.info.locationLine2.getOrElse(""),
          specificCorridorId = versionOf.data.info.corridorId,
          startSectionId = versionOf.data.startSectionId,
          endSectionId = versionOf.data.endSectionId,
          allSectionIds = versionOf.data.allSectionIds,
          direction = Direction(versionOf.data.direction.directionFrom, versionOf.data.direction.directionTo)
        )
    }
  }

  /**
   * creates and returns multiple Section models
   */
  protected def getSchedules(systemId:String, corridorId:String)(implicit user:UserModel):Seq[ScheduleModel]={
    val schedules = Schedules.getByCorridorId(systemId, corridorId)
    schedules.map {
      schedule =>
        val version = schedule.version
        val all = schedule.data
        all.map{
          s =>
            ScheduleModel(
              version = version,
              id = s.id,
              isActive = Schedules.getActiveSchedules(systemId, corridorId).exists(_.id==s.id),
              fromPeriodHour = s.fromPeriodHour,
              fromPeriodMinute = s.fromPeriodMinute,
              toPeriodHour = s.toPeriodHour,
              toPeriodMinute = s.toPeriodMinute,
              speedLimit = s.speedLimit,
              signIndicator = s.signIndicator,
              speedIndicatorType = s.speedIndicatorType,
              signSpeed = s.signSpeed,
              indicationRoadworks = s.indicationRoadworks,
              indicationDanger = s.indicationDanger,
              indicationActualWork = s.indicationActualWork,
              invertDrivingDirection = s.invertDrivingDirection
            )
        }
    }.getOrElse(List())
  }

  /**
   * creates and returns multiple Section models
   */
  def getSections(systemId:String):Seq[SectionModel]={
    val sections = Sections.getAllBySystemId(systemId)
    sections.map {
      versionOf => createSectionModel(systemId, versionOf)
    }.sortBy(_.sortingOrder)
  }



  /**
   * creates and returns a Section models
   */
  protected def createSectionModel(systemId:String,  versionOf:Versioned[Section])={
    val isLastInChain = Sections.isLastInChain(systemId, versionOf.data.id)
    val isStartInChain = Sections.isStartInChain(systemId, versionOf.data.id)
    val sortingOrder = if(isStartInChain && !isLastInChain) 1
    else if(!isStartInChain && isLastInChain) 3
    else 2

    var startGantryName=""
    val startGantry=Gantries.getConfig(systemId, versionOf.data.startGantryId)
    startGantry match{
      case Some(versionOf) =>
          startGantryName= versionOf.data.name
      case  _ =>
        startGantryName=""
    }

    var endGantryName=""
    val endGantry=Gantries.getConfig(systemId, versionOf.data.endGantryId)
    endGantry match{
      case Some(versionOf) =>
        endGantryName= versionOf.data.name
      case  _ =>
        startGantryName=""
    }

    SectionModel(
      id = versionOf.data.id,
      version = versionOf.version,
      name = versionOf.data.name,
      description = versionOf.data.description,
      length = versionOf.data.length,
      isStartInChain = isStartInChain,
      isLastInChain = isLastInChain,
      sortingOrder = sortingOrder,
      startGantryId = versionOf.data.startGantryId,
      startGantryName= startGantryName,
      endGantryId = versionOf.data.endGantryId,
      endGantryName =  endGantryName,
      matrixBoards = versionOf.data.matrixBoards,
      isInUse=Corridors.isSectionInUseByCorridor(systemId,versionOf.data.id))
  }

  /**
   * creates and returns multiple Gantry models
   */
  def getGantries(systemId:String)(implicit user:UserModel):Seq[GantryModel]={
    val gantries = Gantries.getBySystemId(systemId)
    gantries.map {
      versionOf => createGantryModel(systemId, versionOf)
    }
  }

  def getPreferencesSmallForPreferences(systemId:String)(implicit user:UserModel):Seq[PreferenceSmallForPreferences]={

    val preferences=Preferences.getBySystemId(systemId)
    preferences.map{
      versionOf => new PreferenceSmallForPreferences(versionOf.version)
    }.toSeq
  }

  def getGantriesSmallForSystems(systemId:String)(implicit user:UserModel):Seq[GantryModelSmallForSystems]={
    val gantries = Gantries.getBySystemId(systemId)
    gantries.map {
      versionOf => new GantryModelSmallForSystems(versionOf.data.longitude, versionOf.data.latitude)
    }
  }

  def getGantriesSmallForCorridors(systemId:String)(implicit user:UserModel):Seq[GantryModelSmallForCorridors]={
    val gantries = Gantries.getBySystemId(systemId)
    gantries.map {
      versionOf => createGantryModelSmallForCorridors(systemId, versionOf)
    }
  }

  /**
   * creates and returns multiple Lane models
   */
  protected def getLanes(systemId:String,gantryId:String)(implicit user:UserModel):Seq[LaneConfigModel]={
    val lanes = Lanes.getAllByGantryId(systemId, gantryId)
    lanes.map {
      versionOf => createLaneModel(versionOf)
    }
  }

  protected def getLanesSmall(systemId:String,gantryId:String)(implicit user:UserModel):Seq[LaneConfigModelSmall]={
    val lanes: Seq[Versioned[SensorConfig]] = Lanes.getAllByGantryId(systemId, gantryId)
    lanes.map {
      versionOf => new LaneConfigModelSmall()
    }
  }


  /**
   * creates and returns a Lane model
   */
  protected def createLaneModel(versionOf:Versioned[SensorConfig]):LaneConfigModel={
    LaneConfigModel(
      version = versionOf.version,
      sensor = versionOf.data)
  }
}
