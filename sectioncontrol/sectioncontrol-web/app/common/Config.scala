package common

import java.util.UUID

import csc.config.Path
import models.storage.{Role, User}

trait Config {
  import play.api.Play.current
  private lazy val config = play.api.Play.configuration

  object Paths {
    def ctes = Path("/ctes")
    object User{
      def default:Path =  ctes / "users"
    }
    object Role{
      def default:Path =  ctes / "roles"
    }
  }

  object Config{
    def version:String = config.getString("ctes.version").getOrElse("TODO")
    def pathToExport:String = config.getString("ctes.pathToExport").getOrElse("EMPTY")
    def name:String = config.getString("ctes.application.name").getOrElse("EMPTY")
    def eventlog:String = config.getString("ctes.eventlog.secret").getOrElse("EMPTY")
    def inDevelopmentMode:Boolean = config.getBoolean("ctes.devmode").getOrElse(false)
    def language:String = config.getString("ctes.language").getOrElse("NL")
    def timeout:Int = config.getInt("ctes.timeout").getOrElse(2000)
    def badLoginMax:Int = config.getInt("ctes.badLoginMax").getOrElse(5)

    object Zookeeper{
      def host = config.getString("ctes.zookeeper.host").getOrElse("EMPTY")
      def sessionTimeout = config.getInt("ctes.zookeeper.sessionTimeout").getOrElse(1000)
      def connectionTimeout = config.getInt("ctes.zookeeper.connectionTimeout").getOrElse(1000)
      def systemsLogTableName = config.getString("ctes.zookeeper.systemsLogTableName").getOrElse("EMPTY")
    }

    object Hbase{
      def host = config.getString("ctes.zookeeper.hbase.host").getOrElse("")
    }

    object DownloadPassagegegevens {
      def path = config.getString("ctes.downloadPassagegegevens.Path").getOrElse("")
      def retentionInSeconds = config.getInt("ctes.downloadPassagegegevens.RetentionInSeconds").getOrElse(86400)
      def cleanInterval = config.getInt("ctes.downloadPassagegegevens.cleanupIntervalInSeconds").getOrElse(3600)
    }

    def defaultUser = User(
      id = UUID.randomUUID.toString,
      reportingOfficerId = config.getString("ctes.user.technical.reportingOfficerId").getOrElse(""),
      phone = config.getString("ctes.user.technical.phone").getOrElse(""),
      ipAddress = config.getString("ctes.user.technical.ipAddress").getOrElse("EMPTY"),
      name = config.getString("ctes.user.technical.name").getOrElse("EMPTY"),
      username = config.getString("ctes.user.technical.username").getOrElse("EMPTY"),
      email = config.getString("ctes.user.technical.email").getOrElse("EMPTY"),
      passwordHash = "",
      roleId = "",
      sectionIds = Nil,
      prevPasswordHash1="",prevPasswordHash2="",badLoginCount = Some(0))

    object DefaultRoles{
      def police = Role(
        id=UUID.randomUUID.toString,
        name=config.getString("ctes.role.police.name").getOrElse("EMPTY"),
        description=config.getString("ctes.role.police.description").getOrElse("EMPTY"),
        permittedActions = Nil
      )
      def tactical = Role(
        id=UUID.randomUUID.toString,
        name=config.getString("ctes.role.tactical.name").getOrElse("EMPTY"),
        description=config.getString("ctes.role.tactical.description").getOrElse("EMPTY"),
        permittedActions = Nil
      )
      def technical = Role(
        id=UUID.randomUUID.toString,
        name=config.getString("ctes.role.technical.name").getOrElse("EMPTY"),
        description=config.getString("ctes.role.technical.description").getOrElse("EMPTY"),
        permittedActions = Nil
      )
    }
  }
}
