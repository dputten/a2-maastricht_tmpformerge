package controllers
import models.storage.ActionType
import models.web.UserModel
import play.api.libs.iteratee.{Done, Input}
import play.api.mvc._
import services.Users

/**
 * Provide security features
 */
trait Secured {

  /**
   * Retrieve the connected user email.
   */
  def user(request: RequestHeader) = Users.findByUserName(request.session.get("username").getOrElse(""))

  /**
   * Redirect to login if the user in not authorized.
   */
  private def onUnauthorized(request: RequestHeader) = Results.Unauthorized("Gebruiker niet gevonden of wachtwoord ongeldig.")

  def IsAuthenticated(f: => UserModel => Request[AnyContent] => Result) = {
    Authenticated(user, onUnauthorized){
      usr => Action(request => f(usr)(request))
    }
  }

  def Authenticated[A](
                        username: RequestHeader => Option[A],
                        onUnauthorized: RequestHeader => Result)(action: A => Action[AnyContent]): Action[(Action[AnyContent], AnyContent)] = {

    val authenticatedBodyParser = BodyParser { request =>
      username(request).map { user =>
        val innerAction = action(user)
        innerAction.parser(request).mapDone { body =>
          body.right.map(innerBody => (innerAction, innerBody))
        }
      }.getOrElse {
        Done(Left(onUnauthorized(request)), Input.Empty)
      }
    }

    Action(authenticatedBodyParser) { request =>
      val (innerAction, innerBody) = request.body
      innerAction(request.map(_ => innerBody))
    }

  }

  def IsPermitted(actions:ActionType.Value*)(f: => UserModel => Request[AnyContent] => Result) = IsAuthenticated { user => request =>
    if(actions.length > 0){
      val permissions = actions.map(action => user.hasPermission(action))
      if(permissions.isEmpty || permissions.exists(_ == false)){
        Results.Forbidden
      }  else {
        f(user)(request)
      }
    }else{
      f(user)(request)
    }
  }

  def IsMemberOf(systemId:String, actions:ActionType.Value*)(f: => UserModel => Request[AnyContent] => Result) = IsPermitted(actions:_*) { user => request =>
    val isMember = user.sectionIds.contains(systemId)

    if(isMember) {
      f(user)(request)
    } else {
      Results.Forbidden
    }
  }
}
