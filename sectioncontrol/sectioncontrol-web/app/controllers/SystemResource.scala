package controllers

import play.api.mvc.Controller
import common.Formats
import services.{Errors, SystemResources}
import play.api.libs.json.Json._

object SystemResource extends Controller with Formats with Secured{
  /**
   * retrieve link to ganglia
   */
  def getLink = SystemResources.getLink

  def getLinkSystemResources() = IsAuthenticated{ implicit user => implicit request =>
      Ok(toJson(getLink))
  }
}
