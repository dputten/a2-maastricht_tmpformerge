package controllers

import play.api.mvc.{Results, Controller}
import common.{SystemLogger, Formats}
import views.html
import csc.curator.utils.Versioned
import models.web.ResponseMsg._
import play.api.i18n.Messages._
import play.api.i18n.Messages
import services._
import models.web._
import models.storage.ActionType
import models.web.ResponseMsg
import csc.curator.utils.Versioned
import models.web.SectionCRUD
import scala.Some
import models.web.DeleteModel

object Section extends Controller with Formats with Secured with SystemLogger{
  import models.storage.ActionType._
  val permissions = ActionType.values.toList

  def availableSystems = IsPermitted(AllRoles){ implicit user => implicit request =>
    val systems = Sections.getAvailableSystems.filter(s => Users.IsMemberOf(user.username, s.data.id))
    Ok(toLiftJson(systems))
  }

  def systemsWithAvailableSections =  IsAuthenticated{ implicit user  => implicit request =>
    val systemsFiltered = Sections.getAvailableSystems.filter(s => Users.IsMemberOf(user.username, s.data.id))
    val systems = systemsFiltered.foldLeft(List[Versioned[models.storage.System]]()){
      (list, system) =>
        val asg = Sections.getAvailableStartGantries(system.data.id)
        val f = asg.filter(g => Sections.getAvailableLastGantries(system.data.id, g.data.id).length > 0)
        if(f.length > 0){
          system :: list
        }else{
          list
        }
    }
    Ok(toLiftJson(systems))
  }

  def getAllForSystem(systemId:String)= IsMemberOf(systemId, AllRoles){ implicit user  => implicit request =>
    Ok(toLiftJson(getSections(systemId)))
  }

  /*
  * return all gantries that are qualified to be used as a first gantry e.g. in a dropdown box
  * @param systemId is id of system
  * @return json object containing gantries models
  */
  def availableFirstGantries(systemId:String) = IsMemberOf(systemId, AllRoles){ implicit user => implicit request =>
    Ok(toLiftJson(Sections.getAvailableStartGantries(systemId)))
  }

  /*
  * return all gantries that are qualified to be used as a last gantry e.g. in a dropdown box
  * @param systemId is id of system
  * @param gantryId is id of gantry
  * @return json object containing gantries models
  */
  def availableLastGantries(systemId:String, gantryId:String) = IsMemberOf(systemId, AllRoles){ implicit user => implicit request =>
    Ok(toLiftJson(Sections.getAvailableLastGantries(systemId, gantryId)))
  }

  /*
  * get Section by a given id
  * @param systemId is id of system
  * @param sectionId is id of section
  * @return json object containing a Section model or a
  * json ResponseMsg object when he section could not be found
  */
  def get(systemId:String, sectionId:String) = IsMemberOf(systemId, TacticalAndTechnicalView){ implicit user => implicit request =>
    Sections.findById(systemId, sectionId) match {
      case Some(section) =>
        Ok(toLiftJson(createSectionModel(systemId, section)))
      case _ =>
        Ok(toLiftJson(ResponseMsg(
          typeName = ResponseType.System,
          description = Messages("section.actions.get.description.failed"))))
    }
  }

  /*
  * create new Section
  * @param systemId is id of system
  * @return json object containing the result of creating a new section
  */
  def create(systemId:String) = IsMemberOf(systemId, TechnicalCud){ implicit user => implicit request =>
    val body = request.body.asJson.get.toString
    val model = toModel[SectionCRUD](body)
    val responseMsg = Sections.create(systemId, model)

    Ok(toLiftJson(responseMsg))
  }

  /*
  * update existing System
  * @param systemId is id of system
  * @return json object containing the result of updating a existing System
  */
  def update(systemId:String, sectionId:String) = IsMemberOf(systemId, TechnicalCud){ implicit user => implicit request =>
    val body = request.body.asJson.get.toString
    val model = toModel[SectionCRUD](body)

    val previous = Sections.findById(systemId, sectionId).get.data
    val response = Sections.update(systemId, sectionId, model)

    if (response.success) {
      val updated = Sections.findById(systemId, sectionId).get.data

      val changes = List(
        PropertyChange(Messages("sections.form.title.name"), previous.name, updated.name),
        PropertyChange(Messages("gantries.form.title.description"), previous.description, updated.description),
        PropertyChange(Messages("gantries.form.title.length"), previous.length, updated.length),
        PropertyChange(Messages("gantries.form.title.matrixBoards"), previous.matrixBoards, updated.matrixBoards)
      ).flatMap(_.showChange)

      if (changes.length > 0)
        log(messageId = "screen.update.show.no.changes", systemId = systemId, List(user.username, user.reportingOfficerId, systemId, Messages("sections.menu.title"), Messages("sections.form.title.name"), previous.name))
      else

        log(messageId = "screen.showed", systemId = systemId, List(Messages("systems.menu.title")))

      Ok(toLiftJson(response))
    }
    else
      Ok(toLiftJson(response))
  }

  /*
  * delete Gantry by given id and version
  * @param systemId is id of system
  * @param sectionId is id of section
  * @return json object containing the result of deleting a
  * section or a json ResponseMsg object when deleting fails
  */
  def delete(systemId:String, sectionId:String) = IsMemberOf(systemId, TechnicalCud){ implicit user => implicit request =>

    if(Corridors.isSectionInUseByCorridor(systemId,sectionId)==false)
    {
      val body = request.body.asJson.get.toString
      try{
        val model = toModel[DeleteModel](body)
        Sections.delete(systemId, sectionId, model.version) match {
          case true =>
            Ok(toLiftJson(ResponseMsg(
              success=true,
              typeName = ResponseType.System,
              description=Messages("sections.actions.delete.description.success"))))
          case false =>
            Ok(toLiftJson(ResponseMsg(
              typeName = ResponseType.System,
              description=Messages("sections.actions.delete.description.failed"))))
        }
      }catch{
        case e:Exception =>
          Ok(toLiftJson(ResponseMsg(
            typeName = ResponseType.System,
            description=Messages("actions.unkown.description"))))
      }
    }
    else
      Ok(toLiftJson(ResponseMsg(
        typeName = ResponseType.System,
        description=Messages("sections.actions.delete.description.failed"))))
  }
}
