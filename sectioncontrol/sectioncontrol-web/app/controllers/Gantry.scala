package controllers

import common.{Formats, SystemLogger}
import csc.curator.utils.Versioned
import models.storage.{Gantry => ModelGantry, ActionType}
import models.web._
import play.api.i18n.Messages
import play.api.mvc.Controller
import services.{Gantries, Sections}


object Gantry extends Controller with Formats with Secured with SystemLogger{
  import models.storage.ActionType._
  val permissions = ActionType.values.toList

  /*
  * get GantryModel by a given id
  * @param systemId is id of system
  * @param gantryId is id of gantry
  */
  def get(systemId:String, gantryId:String) = IsMemberOf(systemId, TacticalAndTechnicalView){ implicit user => implicit request =>
    Gantries.findById(systemId, gantryId) match {
      case Some(gantry) =>
        Ok(toLiftJson(createGantryModel(systemId, gantry)))
      case _ =>
        Ok(toLiftJson(ResponseMsg(
          typeName = ResponseType.System,
          description = Messages("gantries.actions.get.description.failed\""))))
    }
  }

 /*
  * get GantryModel by a given id
  * @param systemId is id of system
  * @param gantryId is id of gantry
  */
  def getBySystemId(systemId:String) = IsMemberOf(systemId, TacticalAndTechnicalView){ implicit user => implicit request =>
    val gantries: Seq[Versioned[ModelGantry]] =Gantries.getBySystemId(systemId)
    val gantryModelForList: Seq[GantryModelForList] =gantries.map(g=>
      GantryModelForList(
        id=g.data.id,
        version=g.version,
        systemId=g.data.systemId,
        name=g.data.name,
        hectometer=g.data.hectometer,
        longitude=g.data.longitude,
        latitude=g.data.latitude,
        isInUse=isInUse(systemId,g.data.id)
    ))
    Ok(toLiftJson(gantryModelForList))
  }

  /*
  * create new Gantry
  * @param systemId is id of system
  */
  def create(systemId:String) = IsMemberOf(systemId, TechnicalCud){ implicit user => implicit request =>
    val body = request.body.asJson.get.toString
    val model = toModel[GantryCRUD](body)
    val responseMsg = Gantries.create(systemId, model)

    Ok(toLiftJson(responseMsg))
  }

  /*
  * update existing Gantry
  * @param systemId is id of system
  * @param gantryId is id of gantry
  */
  def update(systemId:String, gantryId:String) = IsMemberOf(systemId, TechnicalCud){ implicit user => implicit request =>
    val body = request.body.asJson.get.toString
    val model = toModel[GantryCRUD](body)

    val previous = Gantries.findById(systemId, gantryId).get.data
    val response = Gantries.update(systemId, gantryId, model)

    if (response.success) {
      val updated = Gantries.findById(systemId, gantryId).get.data

      val changes = List(
        PropertyChange(Messages("gantries.form.title.name"), previous.name, updated.name),
        PropertyChange(Messages("gantries.form.title.hectometer"), previous.hectometer, updated.hectometer),
        PropertyChange(Messages("gantries.form.title.longitude"), previous.longitude, updated.longitude),
        PropertyChange(Messages("gantries.form.title.latitude"), previous.latitude, updated.latitude)
      ).flatMap(_.showChange)

      if (changes.length > 0)
        log(messageId = "screen.update.show.no.changes", systemId = systemId, List(user.username, user.reportingOfficerId, systemId, Messages("gantries.menu.title"), Messages("gantries.form.title.name"), previous.name))
      else

        log(messageId = "screen.showed", systemId = systemId, List(Messages("systems.menu.title")))

      Ok(toLiftJson(response))
    }
    else
      Ok(toLiftJson(response))

    //Ok(toLiftJson(Gantries.update(systemId, gantryId, model)))
  }

  private def isInUse(systemId:String, gantryId:String):Boolean={
    val allSectionsGantries:Seq[String] = Sections.getAllGantryIds(systemId)
    allSectionsGantries.exists(s=>s==gantryId)
  }

  /*
  * delete Gantry by given id and version
  * @param systemId is id of system
  * @param gantryId is id of gantry
  */
  def delete(systemId:String, gantryId:String) = IsMemberOf(systemId, TechnicalCud){ implicit user => implicit request =>
    if(isInUse(systemId,gantryId))
    {
      Ok(toLiftJson(ResponseMsg(
        typeName = ResponseType.System,
        description=Messages("gantries.actions.delete.description.failed"))))
    }
    else{
      val body = request.body.asJson.get.toString
      try{
        val model = toModel[DeleteModel](body)
        Gantries.delete(systemId, gantryId, model.version) match {
          case true =>
            Ok(toLiftJson(ResponseMsg(
              success=true,
              typeName = ResponseType.System,
              description=Messages("gantries.actions.delete.description.success"))))
          case false =>
            Ok(toLiftJson(ResponseMsg(
              typeName = ResponseType.System,
              description=Messages("gantries.actions.delete.description.failed"))))
        }
      }catch{
        case e:Exception =>
          Ok(toLiftJson(ResponseMsg(
            typeName = ResponseType.System,
            description=Messages("actions.unkown.description"))))
      }
    }
  }
}
