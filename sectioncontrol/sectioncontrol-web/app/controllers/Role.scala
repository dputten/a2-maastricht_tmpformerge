package controllers

import play.api.mvc.Controller
import views.html
import services.{PermittedActions, Roles, Users}
import models.web._
import play.api.i18n.Messages
import models.storage.ActionType._
import common.{SystemLogger, Formats}

object Role extends Controller with Formats with SystemLogger with Secured{

  def listActions = IsPermitted(RolesView){ implicit user  => implicit request =>
    Ok(toLiftJson(PermittedActions.getAll))
  }

  def list = IsPermitted(RolesView){ implicit user  => implicit request =>
    Ok(toLiftJson(Roles.getAll))
  }

  /*
  * update existing role
  */
  def update = createOrUpdate(RolesChange)

  /*
  * creates/updates a role
  */
  private def createOrUpdate(permission:ActionType) = IsPermitted(permission){ implicit user  => implicit request =>
    val body = request.body.asJson.get.toString

    try{
      val roleModel:UpdateRoleModel = toModel[UpdateRoleModel](body)

      permission match{
        case RolesChange =>
          val previous = roleModel
          val prev = Roles.findById(previous.id)
          val response = Roles.createOrUpdate(roleModel) match {
            case Left(role:RoleModel) => ResponseMsg(success=true, typeName=ResponseType.System)
            case Right(errors:ResponseMsg) => errors
          }
          val updated = Roles.findById(previous.id)

          if(prev.isDefined && updated.isDefined){
            updated match{
              case Some(role) =>
                val changes = List(
                  PropertyChange(Messages("roles.form.title.name"), prev.get.name, updated.get.name),
                  PropertyChange(Messages("roles.form.title.description"), prev.get.description, updated.get.description),
                  PropertyChange(Messages("roles.form.title.permission"), Messages("property.from" + " " + prev.get.actionIds.size), Messages("property.to" + " " + updated.get.actionIds.size))
                ).flatMap(_.showChange)

                if(changes.length > 0)
                  log(messageId="screen.update.allsystems.show.no.changes", args=List(user.username, user.reportingOfficerId, Messages("roles.menu.title"), Messages("roles.menu.title.single"), prev.get.name))
                else
                  log(messageId="screen.showed", args=List(Messages("roles.menu.title")))
              case _ =>
            }
          }

          Ok(toLiftJson(response))
      }
    }catch{
      case e:Exception =>
        throw e
        val response = ResponseMsg(typeName=ResponseType.System, description="Unknown system error occured")
        Ok(toLiftJson(response))
    }
  }

  /*
  * retrieve a role by given id
  */
  def get(id:String)= IsPermitted(RolesView){ implicit user  => implicit request =>
    Roles.findById(id) match {
      case Some(role) => Ok(toLiftJson(role))
      case _ => NoContent
    }
  }
}
