package controllers

import play.api.mvc.Controller
import views.html
import services._
import common.{SystemLogger, Formats}
import models.storage.ActionType
import models.web.GantryModel


object Corridor extends Controller with Formats with SystemLogger with Secured{
  import models.storage.ActionType._
  val permissions = ActionType.values.toList

  /*
* get GantryModel by a given id
* @param systemId is id of system
* @param sectionId is id of section
*/
  def getStartGantryAndEndGantry(systemId:String, corridorId:String) = IsMemberOf(systemId, AllRoles){ implicit user => implicit request =>

    val corridor=Corridors.findById(systemId,corridorId)

    val startSection = Sections.getConfig(systemId, corridor.get.data.startSectionId)
    val endSection = Sections.getConfig(systemId, corridor.get.data.endSectionId)

    var startGantry:GantryModel=null
    var endGantry:GantryModel=null

    Gantries.findById(systemId, startSection.get.data.startGantryId) match {
      case Some(gantry) =>
        startGantry=createGantryModel(systemId, gantry)
      case _ =>
    }

    Gantries.findById(systemId, endSection.get.data.endGantryId) match {
      case Some(gantry) =>
        endGantry=createGantryModel(systemId, gantry)
      case _ =>
    }

    Ok(toLiftJson(Seq[GantryModel](startGantry,endGantry)))
  }

  /**
   * return all ifaces for dynamax
   */
  def availableIfaces = IsPermitted(AllRoles){ implicit user => implicit request =>
    Ok(toLiftJson(Configuration.getDynamaxIfaces))
  }

  /*
  * retrieve all systems that can be connected to a corridor
  */
  def availableSystems = IsPermitted(AllRoles){ implicit user => implicit request =>
    Ok(toLiftJson(Sections.getAvailableSystems.filter(s => user.sectionIds.exists(_ == s.data.id))))
  }

  /*
  * retrieve all sections that can be connected to a corridor
  * @param systemId is id of system
  */
  def availableFirstSections(systemId:String) = IsMemberOf(systemId, AllRoles){ implicit user => implicit request =>
    Ok(toLiftJson(Corridors.getAvailableFirstSections(systemId)))
  }

  /*
  * retrieve all sections that can be connected to a corridor
  * @param systemId is id of system
  * @param sectionId is id of section
  */
  def availableLastSections(systemId:String, sectionId:String) = IsMemberOf(systemId, AllRoles){ implicit user => implicit request =>
    val sections = Corridors.getAvailableSecondSections(systemId, sectionId)
    Ok(toLiftJson(sections))
  }

  def getState(systemId:String, corridorId:String) = IsMemberOf(systemId, AllRoles){ implicit user => implicit request =>
    Ok(toLiftJson(Corridors.getState(systemId, corridorId)))
  }
}
