package controllers

import play.api.mvc.Controller

import play.api.i18n.Messages
import services.Lanes
import models.web._
import models.storage.ActionType._
import models.storage.ActionType
import common.{SystemLogger, Formats}
import scala.Some
import models.web.ResponseMsg
import models.web.PropertyChange
import models.web.LaneConfigModel
import scala.Some
import models.web.DeleteModel

object Lane extends Controller with Formats with SystemLogger with Secured {
  val permissions = ActionType.values.toList

  /*
* get LaneModel by a given id
* @param systemId is id of system
* @param gantryId is id of gantry
* @param laneId is id of lane
* @return json object containing a lane model or a ResponseMsg json object
*/
  def all = IsPermitted(TacticalAndTechnicalView) {
    implicit user => implicit request =>
      Ok(toLiftJson(getSystemsForLanesForList))
  }

  /*
  * get LaneModel by a given id
  * @param systemId is id of system
  * @param gantryId is id of gantry
  * @param laneId is id of lane
  * @return json object containing a lane model or a ResponseMsg json object
  */
  def get(systemId: String, gantryId: String, laneId: String) = IsMemberOf(systemId, TacticalAndTechnicalView) {
    implicit user => implicit request =>
      Lanes.findById(systemId, gantryId, laneId) match {
        case Some(lane) => Ok(toLiftJson(createLaneModel(lane)))
        case _ =>
          Ok(toLiftJson(ResponseMsg(
            typeName = ResponseType.System,
            description = Messages("lanes.actions.get.description.failed"))))
      }
  }

  /*
  * create new Lane
  * @param systemId is id of system
  * @param gantryId is id of gantry
  * @return json object containing the result of creating a new lane
  */
  def create(systemId: String, gantryId: String) = IsMemberOf(systemId, TechnicalCud) {
    implicit user => implicit request =>
      val body = request.body.asJson.get.toString
      val model = toModel[LaneConfigModel](body)
      val responseMsg = Lanes.create(systemId, gantryId, model.sensor)

      Ok(toLiftJson(responseMsg))
  }

  /*
  * update existing lane
  * @param systemId is id of system
  * @param gantryId is id of gantry
  * @param laneId is id of lane
  * @return json object containing the result of updating a lane
  */
  def update(systemId: String, gantryId: String, laneId: String) = IsMemberOf(systemId, TechnicalCud) {
    implicit user => implicit request =>
      val body = request.body.asJson.get.toString
      val laneConfigModel = toModel[LaneConfigModel](body)
      val model = laneConfigModel.sensor

      val previous = Lanes.findById(systemId, gantryId, laneId).get.data
      val response = Lanes.update(systemId, gantryId, laneId, model, laneConfigModel.version)
      if (response.success) {
        val updated = Lanes.findById(systemId, gantryId, laneId).get.data

        val changes = List(
          PropertyChange(Messages("lanes.form.title.name2"), previous.name, updated.name),
          PropertyChange(Messages("lanes.form.title.bpsLaneId"), previous.bpsLaneId, updated.bpsLaneId),

          PropertyChange(Messages("lanes.form.title.camera_relayURI"), previous.camera.relayURI, updated.camera.relayURI),
          PropertyChange(Messages("lanes.form.title.camera_cameraHost"), previous.camera.cameraHost , updated.camera.cameraHost),
          PropertyChange(Messages("lanes.form.title.camera_cameraPort"), previous.camera.cameraPort, updated.camera.cameraPort),
          PropertyChange(Messages("lanes.form.title.camera_maxTriggerRetries"), previous.camera.maxTriggerRetries, updated.camera.maxTriggerRetries),
          PropertyChange(Messages("lanes.form.title.camera_timeDisconnected"), previous.camera.timeDisconnected, updated.camera.timeDisconnected),

          PropertyChange(Messages("lanes.form.title.radar_uri"), previous.radar.radarURI, updated.radar.radarURI),
          PropertyChange(Messages("lanes.form.title.radar_measurement_angle"), previous.radar.measurementAngle, updated.radar.measurementAngle),
          PropertyChange(Messages("lanes.form.title.radar_height"), previous.radar.height, updated.radar.height),
          PropertyChange(Messages("lanes.form.title.radar_surface_reflection"), previous.radar.surfaceReflection, updated.radar.surfaceReflection),

          PropertyChange(Messages("lanes.form.title.pir_host"), previous.pir.pirHost, updated.pir.pirHost),
          PropertyChange(Messages("lanes.form.title.pir_port"), previous.pir.pirPort, updated.pir.pirPort),
          PropertyChange(Messages("lanes.form.title.pir_pirAddress"), previous.pir.pirAddress, updated.pir.pirAddress),
          PropertyChange(Messages("lanes.form.title.pir_refreshPeriod"),previous.pir.refreshPeriod, updated.pir.refreshPeriod),
          PropertyChange(Messages("lanes.form.title.pir_vrHost"), previous.pir.vrHost, updated.pir.vrHost),
          PropertyChange(Messages("lanes.form.title.pir_vrPort"), previous.pir.vrPort, updated.pir.vrPort)

        ).flatMap(_.showChange)

        if (changes.length > 0)
          log(messageId = "screen.update.show.no.changes", systemId = systemId, List(user.username, user.reportingOfficerId, systemId, Messages("lanes.menu.title"), Messages("lanes.form.title.name"), previous.name))
        else

          log(messageId = "screen.showed", systemId = systemId, List(Messages("systems.menu.title")))

        Ok(toLiftJson(response))
      }
      else
        Ok(toLiftJson(response))
  }

  /*
  * delete Lane by given id and version
  * @param systemId is id of system
  * @param gantryId is id of gantry
  * @param laneId is id of lane
  * @return json object containing the result of deleting a lane
  */
  def delete(systemId: String, gantryId: String, laneId: String) = IsMemberOf(systemId, TechnicalCud) {
    implicit user => implicit request =>
      val body = request.body.asJson.get.toString
      try {
        val model = toModel[DeleteModel](body)
        Lanes.delete(systemId, gantryId, laneId, model.version) match {
          case true =>
            Ok(toLiftJson(ResponseMsg(
              success = true,
              typeName = ResponseType.System,
              description = Messages("lanes.actions.delete.description.success"))))
          case false =>
            Ok(toLiftJson(ResponseMsg(
              typeName = ResponseType.System,
              description = Messages("lanes.actions.delete.description.failed"))))
        }
      } catch {
        case e: Exception =>
          Ok(toLiftJson(ResponseMsg(
            typeName = ResponseType.System,
            description = Messages("actions.unkown.description"))))
      }
  }
}