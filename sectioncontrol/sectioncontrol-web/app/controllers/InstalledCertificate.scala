package controllers

import play.api.mvc.Controller
import views.html
import services._
import common.{SystemLogger, Formats}
import models.storage.ActionType._
import models.web.{InstalledCertificateShow, InstalledCertificateMultiCreate, InstalledCertificateCreate, LocationCertificateCRUD}
import play.api.i18n.Messages

object InstalledCertificate extends Controller with Formats with SystemLogger with Secured{

  def getAll(systemId:String) = IsPermitted(CertificatesView){ implicit user => implicit request =>

    val certificates: Seq[InstalledCertificateShow] = Certificates.getAllSystemsInstalledCertificates
    val certificatesForSystem: Seq[InstalledCertificateShow] =certificates.filter(c=>c.systemId==systemId)
    val result=certificatesForSystem.map(i=>i.copy(installDateDisplay=formatDateToCEST(i.installDate, "EEE MMM dd yyyy HH:mm")))
    Ok(toLiftJson(result))
  }

  def create(systemId:String) = IsPermitted(CertificatesChange){ implicit user => implicit request =>
    val body = request.body.asJson.get.toString
    val model = toModel[InstalledCertificateCreate](body)

    Ok(toLiftJson(Certificates.createInstalledCertificate(systemId, model)))
  }
  def multicreate() = IsPermitted(CertificatesChange){ implicit user => implicit request =>
    val body = request.body.asJson.get.toString
    val model = toModel[InstalledCertificateMultiCreate](body)

    Ok(toLiftJson(Certificates.multiCreateInstalledCertificate(model)))
  }

  def delete(systemId:String, locationId:String) = IsPermitted(CertificatesChange){ implicit user => implicit request =>
    Ok(toLiftJson(Certificates.deleteInstalledCertificate(systemId, locationId)))
  }

  def getById(systemId:String, certId:String) = IsPermitted(CertificatesView){ implicit user => implicit request =>
    Ok(toLiftJson(Certificates.getInstalledCertificate(systemId, certId)))
  }
}
