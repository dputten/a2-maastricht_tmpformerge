package controllers

import play.api.mvc._
import models.storage.ActionType._
import views.html
import play.api.i18n.Messages
import models.web._
import play.api.libs.json.Json._
import models.storage.{ActionType, ZkIndicationType, ServiceType}
import services._
import common._
import models.web.ResponseMsg
import scala.Some
import scala.Some
import models.web.SystemStateType.SystemStateType


object System extends Controller with Formats with SystemLogger with Secured{
  /**
   * determine if dynamax is available
   * @return true if dynamax is available
   */
  def isDynamaxActive = IsPermitted(){ implicit user => implicit request =>
    Ok(toJson(Systems.isDynamaxActive))
  }

  /**
  * retrieve all systems
  * @return json object containing a sequence of all systems
  */
  def list = IsAuthenticated{ implicit user => implicit request =>
    Ok(toLiftJson(getAllSystems))
  }

  /**
  * retrieve all systems
  * @return json object containing a sequence of all systems
  */
  def allSmall (): Action[(Action[AnyContent], AnyContent)] = IsAuthenticated{ implicit user => implicit request =>
    Ok(toLiftJson(getAllSystemsSmall))
  }

  /*
 * retrieve all systems that are editable
 * @return json object containing a sequence of all systems
 */
  def isSystemEditable(systemId: String) = IsAuthenticated {
    implicit user => implicit request =>
      Ok(toJson(Systems.isEditable(systemId)))
  }

  case class SimpleSystemState(state:SystemStateType)
  /**
  * get System by a given id
  * @param systemId is id of System
  * @return json object containing the System state model or
  * json when the System could not be found
  * Todo: Dit gedeelt wordt door JW Luiten gebruikt in een call. Die zal aangepast moeten worden.
  */
  def getSystemState(systemId: String) = Action{
    new SystemStateBuilder(systemId,new StateMapper(),new StatePicker(new StateMapper())).
      getSystemState match {
        case Some(state) => Ok(toLiftJson(SimpleSystemState(state=state))).withHeaders(CONTENT_TYPE -> "application/json")
        case _ =>  BadRequest(toLiftJson(ErrorFactory.systemNotFound()))
      }
  }

  def getStates(systemId:String) = IsMemberOf(systemId, AllRoles){ implicit user => implicit request =>
    Ok(toLiftJson(Systems.getWebStates(systemId)))
  }
}