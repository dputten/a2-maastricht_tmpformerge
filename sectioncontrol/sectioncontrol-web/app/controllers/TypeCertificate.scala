package controllers

import play.api.mvc.Controller
import views.html
import services._
import common.{SystemLogger, Formats}
import models.storage.ActionType._
import models.web.{TypeCertificateList, TypeCertificateCRUD, LocationCertificateCRUD}
import play.api.i18n.Messages

object TypeCertificate extends Controller with Formats with SystemLogger with Secured{

  def create() = IsPermitted(CertificatesChange){ implicit user => implicit request =>
    val body = request.body.asJson.get.toString
    val model = toModel[TypeCertificateCRUD](body)
    Ok(toLiftJson(Certificates.createOrUpdateTypeCertificate(model)))
  }

  def update(certId:String) = IsPermitted(CertificatesChange){ implicit user => implicit request =>
    val body = request.body.asJson.get.toString
    val model = toModel[TypeCertificateCRUD](body)

    Ok(toLiftJson(Certificates.createOrUpdateTypeCertificate(model)))
  }

  def delete(certId:String) = IsPermitted(CertificatesChange){ implicit user => implicit request =>
    Ok(toLiftJson(Certificates.deleteTypeCertificate(certId)))
  }

  def getById(certId:String) = IsPermitted(CertificatesView){ implicit user => implicit request =>
    Ok(toLiftJson(Certificates.getTypeCertificate(certId)))
  }

  def getAllAvailableCertificates(systemId: String) = IsPermitted(CertificatesView){ implicit user => implicit request =>
    Ok(toLiftJson(Certificates.getAvailableTypeCertificates(systemId)))
  }

  def getAllTypeCertificatesList(systemId: String) = IsPermitted(CertificatesView){ implicit user => implicit request =>
    val availableCertificates=Certificates.getAvailableTypeCertificates(systemId)
    val allCertificates = Certificates.getAllTypeCertificates()
    val result=allCertificates.map(c=>
        new TypeCertificateList(isAvailable =availableCertificates.exists(a=>a.id==c.id),
          id=c.id,
          typeCertificateId = c.certificate.typeCertificate.id,
          revisionNr =   c.certificate.typeCertificate.revisionNr,
          typeDesignation = c.certificate.typeDesignation,
          category = c.certificate.category,
          unitSpeed = c.certificate.unitSpeed,
          unitLength = c.certificate.unitLength
        )
        )

    Ok(toLiftJson(result))
  }

  def getAllUsedSystems(certId: String) = IsPermitted(CertificatesView){ implicit user => implicit request =>
    Ok(toLiftJson(Certificates.getAllUsedSystems(certId)))
  }

  def getAllPossibleSystems(certId: String) = IsPermitted(CertificatesChange){ implicit user => implicit request =>
    Ok(toLiftJson(Certificates.getAllPossibleSystems(certId)))
  }
}
