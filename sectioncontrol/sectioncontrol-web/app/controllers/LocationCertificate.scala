package controllers

import play.api.mvc.Controller
import services._
import common.{SystemLogger, Formats}
import models.storage.ActionType._
import models.web.LocationCertificateCRUD

object LocationCertificate extends Controller with Formats with SystemLogger with Secured{

  def getAll(systemId:String) = IsPermitted(CertificatesView){ implicit user => implicit request =>
    val all: Seq[LocationCertificateCRUD] =Certificates.getAllSystemsLocationCertificates

    val allForSystem=all.filter(p=>p.systemId==systemId)

    Ok(toLiftJson(allForSystem))
  }

  def create(systemId:String) = IsPermitted(CertificatesChange){ implicit user => implicit request =>
    val body = request.body.asJson.get.toString
    val model = toModel[LocationCertificateCRUD](body)

    Ok(toLiftJson(Certificates.createOrUpdateLocationCertificate(model)))
  }

  def update(systemId:String, certId:String) = IsPermitted(CertificatesChange){ implicit user => implicit request =>
    val body = request.body.asJson.get.toString
    val model = toModel[LocationCertificateCRUD](body)

    Ok(toLiftJson(Certificates.createOrUpdateLocationCertificate(model)))
  }
  def delete(systemId:String, locationId:String) = IsPermitted(CertificatesChange){ implicit user => implicit request =>
    Ok(toLiftJson(Certificates.deleteLocationCertificate(systemId, locationId)))
  }

  def getById(systemId:String, certId:String) = IsPermitted(CertificatesView){ implicit user => implicit request =>

    Ok(toLiftJson(Certificates.getLocationCertificate(systemId, certId)))
  }


}
