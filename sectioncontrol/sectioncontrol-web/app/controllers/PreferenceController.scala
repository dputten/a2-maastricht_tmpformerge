package controllers

import play.api.mvc.Controller
import views.html
import models.web.ResponseMsg._
import play.api.i18n.Messages._
import play.api.i18n.Messages
import models.web._
import csc.curator.utils.Versioned
import services._
import models.storage.{ClassifyCountry, PossibleTreatyCountry, Preference, ActionType}
import common.{SystemLogger, Formats}

object PreferenceController extends Controller with Formats with SystemLogger with Secured{
  import models.storage.ActionType._
  val permissions = ActionType.values.toList

  def getPossibleTreatyCountries = IsPermitted(TacticalAndTechnicalView){ implicit user  => implicit request =>
    Ok(toLiftJson(Configuration.getTreatyCountries))
  }

  def getClassifyCountries = IsPermitted(TacticalAndTechnicalView){ implicit user  => implicit request =>
    Ok(toLiftJson(Configuration.getClassifyCountries))
  }

  def updateClassifyCountry() = IsPermitted(TreatyCountriesChange){ implicit user  => implicit request =>
    val body = request.body.asJson.get.toString
    val model = toModel[ClassifyCountriesModel](body)
    Ok(toLiftJson(Configuration.updateClassifyCountries(model)))
  }

  def deleteCountry(isoCode:String)=IsPermitted(TreatyCountriesChange){ implicit user  => implicit request =>
    val response = Configuration.deleteTreatyCountry(isoCode)
    if(response.success)
      log(messageId="logging.countrycode.delete", args=List(isoCode))
    Ok(toLiftJson(response))
  }

  def getCountry(isoCode:String)=IsPermitted(TacticalAndTechnicalView){ implicit user  => implicit request =>
    Ok(toLiftJson(Configuration.getTreatyCountriesById(isoCode)))
  }

  def updateCountry(isoCode:String)=IsPermitted(TreatyCountriesChange){ implicit user  => implicit request =>
    val previous = Configuration.getTreatyCountries
    val body = request.body.asJson.get.toString
    val model = toModel[PossibleTreatyCountry](body)
    val response = Configuration.updateTreatyCountries(isoCode, model)
    val updated = Configuration.getTreatyCountries

    val properties = updated.foldLeft(List[PropertyChange]()){
      case (list, update) =>
        val prevValue = previous.find(_.isoCode == update.isoCode).get

        List(PropertyChange(Messages("countries.menu.title1"), prevValue.isoCode, update.isoCode))
    }.flatMap(_.showChange)

    if(properties.length > 0)
      log(messageId="screen.update.allsystems.show.no.changes", args=List(user.username, user.reportingOfficerId, Messages("countries.menu.title"), properties.mkString("\r\n","\r\n","\r\n")))
    else
      log(messageId="screen.showed", args=List(Messages("countries.menu.title")))
    Ok(toLiftJson(response))
  }

  def createCountry=IsPermitted(TreatyCountriesChange){ implicit user  => implicit request =>
    val body = request.body.asJson.get.toString
    val model = toModel[PossibleTreatyCountry](body)
    val response = Configuration.createTreatyCountries(model)

    if(response.success)
      log(messageId="logging.countrycode.create", args=List(model.isoCode))
    else
      log(messageId="screen.showed", args=List(Messages("countries.menu.title")))

    Ok(toLiftJson(response))
  }

  /**
   * retrieve zaakbestand info
   * @param systemId
   */
  def getCaseFile(systemId:String) = IsMemberOf(systemId, permissions:_*){ implicit user  => implicit request =>
    Preferences.getCaseFile(systemId) match {
      case Some(pref) =>
        Ok(toLiftJson(pref))
      case _ =>
        Ok(toLiftJson(ResponseMsg(
          typeName = ResponseType.System,
          description = Messages("preferences.actions.get.description.failed"))))
    }
  }

  /**
   * updates preferences
   * @param systemId is id of system
   */
  def updateCaseFile(systemId:String) = IsMemberOf(systemId, permissions:_*){ implicit user  => implicit request =>
    val body = request.body.asJson.get.toString
    val model = toModel[CaseFileCRUD](body)

    Ok(toLiftJson(Preferences.updateCaseFile(systemId, model)))
  }
}
