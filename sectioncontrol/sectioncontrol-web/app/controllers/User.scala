package controllers

import views._
import play.api.mvc._
import services.{Certificates, Roles, Users, Storage}
import models.storage.ActionType._
import common.{SystemLogger, Formats}
import csc.config
import models.storage.{User, PermittedAction, ActionType}
import common.{Config, SystemLogger}
import common.Extensions._
import scala._
import models.web._
import play.api.i18n.Messages
import common.{ SystemLogger}

object User extends Controller with Formats with SystemLogger with Secured with Storage{

  def list = IsPermitted(UsersView){ implicit user  => implicit request =>
    Ok(toLiftJson(Users.getAll))
  }

 def delete(userId:String) = IsPermitted(UsersDelete){ implicit user  => implicit request =>
   val body = request.body.asJson.get.toString
    try{
      val model = toModel[DeleteModel](body)
      Users.delete(userId, model.version, user.username) match {
        case true =>
          Ok(toLiftJson(ResponseMsg(
            success=true,
            typeName=ResponseType.System,
            description=Messages("user.actions.delete.description.success"))))
        case false =>
          Ok(toLiftJson(ResponseMsg(
            typeName=ResponseType.System,
            description=Messages("user.actions.delete.description.failed"))))
      }
    }catch{
      case e:Exception =>
        val response = ResponseMsg(typeName=ResponseType.System, description="Unknown system error occured")
        Ok(toLiftJson(response))
    }
  }

  /*
  * update existing user
  */
 def update = createOrUpdate(UsersChange)

  /*
  * creates new user
  */
  def create = createOrUpdate(UsersCreate)

  /*
  * private method that combines updating or saving a User
  */
 private def createOrUpdate(permission:ActionType) = IsPermitted(permission){ implicit user  => implicit request =>
    val body = request.body.asJson.get.toString

    try{
      val usrModel: UpdateUserModel = toModel[UpdateUserModel](body)

      permission match{
        case UsersChange =>
          val previous = usrModel
          val prev = Users.findById(usrModel.id)
          val response = Users.createOrUpdate(usrModel) match {
            case Left(usr:UserModel) => ResponseMsg(success=true, typeName=ResponseType.System)
            case Right(errors:ResponseMsg) => errors
          }
          val updated = Users.findById(previous.id)
          if(prev.isDefined && updated.isDefined){
            updated match{
              case Some(usr) =>
                val changes = List(
                  PropertyChange(Messages("users.form.title.name"), prev.get.name, updated.get.name),
                  PropertyChange(Messages("users.form.title.reportingOfficerId"), prev.get.reportingOfficerId, updated.get.reportingOfficerId),
                  PropertyChange(Messages("users.form.title.username"), prev.get.username, updated.get.username),
                  PropertyChange(Messages("users.form.title.password"), prev.get.password, updated.get.password),
                  PropertyChange(Messages("users.form.title.role"), prev.get.role.id, updated.get.role.id)
                ).flatMap(_.showChange)

                if(changes.length > 0)
                  log(messageId="screen.update.allsystems.show.no.changes", args=List(user.username, user.reportingOfficerId, Messages("users.menu.title"), Messages("users.menu.title.single"), prev.get.name))
                else
                  log(messageId="screen.showed", args=List(Messages("users.menu.title")))
              case _ =>
            }
          }
          Ok(toLiftJson(response))
        case UsersCreate =>
          val response = Users.createOrUpdate(usrModel) match {
            case Left(usr:UserModel) =>
              log(messageId="logging.users.create", args=List(usr.name))
              ResponseMsg(success=true, typeName=ResponseType.System)
            case Right(errors:ResponseMsg) => errors
          }
          Ok(toLiftJson(response))
      }
    }catch{
      case e:Exception =>
        val response = ResponseMsg(typeName=ResponseType.System, description="Unknown system error occured")
        Ok(toLiftJson(response))
    }
  }

  /*
  * Retrieves a user by id
  */
  def get(id:String) = IsPermitted(UsersView){ implicit user  => implicit request =>
    Users.findById(id) match {
      case Some(usr) => Ok(toLiftJson(usr))
      case _ => NoContent
    }
  }

  case class ChangePasswordModel(oldPassword: String,newPassword: String)

  def changePasswordCurrentUser()= IsPermitted(UsersView){ implicit user  => implicit request =>
    Users.findById(user.id) match {
      case Some(userModel) => {
        val body = request.body.asJson.get.toString
        try{
          val changePasswordModel: ChangePasswordModel = toModel[ChangePasswordModel](body)

          if(userModel.password==changePasswordModel.oldPassword){
            // do a hash of the new password.
            val passwordHash =  changePasswordModel.newPassword.digest()

            val storageUser=curator.getVersioned[User](Paths.User.default / userModel.id)
            var badLoginCount=0
            if(storageUser.isDefined)
            {
              val x=storageUser.get
              badLoginCount=x.data.badLoginCount.getOrElse(0)
            }

            val newUser = models.storage.User(
              id = userModel.id,
              reportingOfficerId = userModel.reportingOfficerId,
              name = userModel.name,
              username = userModel.username,
              phone = userModel.phone,
              ipAddress = userModel.ipAddress,
              email = userModel.email,
              passwordHash = passwordHash,
              roleId = userModel.roleId,
              sectionIds = userModel.sectionIds.toSet.toList,
              prevPasswordHash1 = "",prevPasswordHash2 = "",badLoginCount=Some(badLoginCount))

            curator.set(Paths.User.default / newUser.id, newUser, userModel.version)

            Ok(toLiftJson(userModel))
          }
          NoContent
        }
        catch{
          case e:Exception =>
            val response = ResponseMsg(typeName=ResponseType.System, description="Unknown system error occured")
            Ok(toLiftJson(response))
        }
      }
      case _ => NoContent
    }
  }

  def getCurrentUser() = IsPermitted(UsersView){ implicit user  => implicit request =>
    Users.findById(user.id) match {
      case Some(usr) => Ok(toLiftJson(usr))
      case _ => NoContent
    }
  }

  def getRole ()= IsAuthenticated{ implicit user => implicit request =>
    Ok(toLiftJson(user.role.description))
  }

}
