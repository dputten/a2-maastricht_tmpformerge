package controllers

import java.util.Calendar

import common.Formats
import models.storage.ActionType._
import models.web.{ErrorInfo, ResponseMsg, _}
import play.api.Play.current
import play.api.i18n.Messages
import play.api.libs.concurrent.Akka
import play.api.libs.json.Json._
import play.api.mvc.Controller
import services.Errors

object Error extends Controller with Formats with Secured {

  /*
   * return the total number of errors (Alerts en Failures) for a given system
   * @param systemId is id of system
   * @return an int that represent the total number of errors  in json format.
   */
  def getErrors(systemId: String) = IsMemberOf(systemId) { implicit user => implicit request =>
    val x = getSystemsForErrors
    val result: Seq[SystemModelSmallForErrors] = x.filter(_.id == systemId)
    Ok(toLiftJson(result))
  }

  /*
   * return the total number of errors (Alerts en Failures) for a given system
   * @param systemId is id of system
   * @return an int that represent the total number of errors  in json format.
   */
  def getTotalNumberOfErrorsPerSystem(systemId: String) = IsMemberOf(systemId) { implicit user => implicit request =>
    Ok(toJson(Errors.getAlerts(systemId).length + Errors.getFailures(systemId).length))
  }

  /*
   * return the total number of errors (Alerts en Failures) for all systems that
   * an user has access to
   * @return an int that represent the total number of errors in json format.
   */
  def getTotalNumberOfErrors = IsAuthenticated { implicit user => implicit request =>
    val total = user.sectionIds.foldLeft(0) {
      (list, systemId) =>
        val nrAlerts = Errors.getAlerts(systemId).length
        val nrFailures = Errors.getFailures(systemId).length

        (nrAlerts + nrFailures) + list
    }

    Ok(toLiftJson(ErrorInfo(total = total, date = formatDateToCEST(Calendar.getInstance().getTimeInMillis, "local.dateformat.datetime"))))
  }

  /*
   * used to sign off an error
   * @param systemId is id of system
   * @param errorId is id of error
   * @param errorType type of Error
   * @return a ResponseMsg in json format.
   */
  def signOff(systemId: String, corridorId: String, errorId: String, errorType: String) = IsMemberOf(systemId, ErrorsSignOff) { implicit user => implicit request =>
    def isErrorSuccessfullySignedOff = Akka.future {

      def errorStillExists = getSystemsForErrors.exists(_.errors.exists(error => error.id == errorId))

      Errors.signOff(systemId, corridorId, errorId, ErrorType.withName(errorType))

      var isErrorProcessed = false

      for (i <- 1 to 30) {      // 30 * 100 ms == 3000 ms == timeout of this future
        if (errorStillExists) { // give the system a little bit more time
          Thread.sleep(100)     // don't poll too often, since we're doing requests to Zookeeper
        } else {
          isErrorProcessed = true
        }
      }

      isErrorProcessed
    }

    Async {
      val SIGNOFF_PROCESSING_TIMEOUT: Long = 3000

      isErrorSuccessfullySignedOff.orTimeout("Error took too long to process", SIGNOFF_PROCESSING_TIMEOUT).map { eitherProcessedOrTimeout =>
        eitherProcessedOrTimeout.fold(
          success => createResponse(wasSuccessful = true, messageId = "errors.actions.delete.description.success"),
          timeout => createResponse(wasSuccessful = false, messageId = "errors.actions.delete.description.timeout")
        )
      }
    }
  }

  private def createResponse(wasSuccessful: Boolean, messageId: String) = {
    Ok(toLiftJson(ResponseMsg(
      success = wasSuccessful,
      typeName = ResponseType.System,
      description = Messages(messageId)))
    )
  }

  /*
   * used to confirm an error
   * @param systemId is id of system
   * @param errorId is id of error
   * @param errorType type of Error
   * @return a ResponseMsg in json format.
   */
  def confirm(systemId: String, corridorId: String, errorId: String, errorType: String) = IsMemberOf(systemId) { implicit user => implicit request =>
    Errors.confirmError(systemId, corridorId, errorId, ErrorType.withName(errorType)) match {
      case true =>
        Ok(toLiftJson(ResponseMsg(
          success = true,
          typeName = ResponseType.System,
          description = Messages("errors.confirmed.success"))))
      case false =>
        Ok(toLiftJson(ResponseMsg(
          success = false,
          typeName = ResponseType.System,
          description = Messages("errors.confirmed.failed"))))
    }
  }

  case class ErrorPath(path: String)

  /*
   * get an error by a given system and error id
   * @param systemId is id of system
   * @param errorId is id of error
   * @param errorType type of Error
   * @return json object containing a Error model or a
   * json ResponseMsg object when he System could not be found
   */
  def get(systemId: String, corridorId: String, errorId: String, errorType: String) = IsMemberOf(systemId) { implicit user => implicit request =>
    val defaultFailed = Ok(toLiftJson(ResponseMsg(
      typeName = ResponseType.System,
      description = Messages("errors.actions.get.description.failed"))))

    ErrorType.withName(errorType) match {
      case ErrorType.Alert =>
        Errors.getAlert(systemId, errorId,corridorId).map{
          alert => Ok(toLiftJson(createErrorModel(systemId, None, errorId, alert._3.data, ErrorType.Alert.toString,alert._2)))
        }.getOrElse(defaultFailed)
      case ErrorType.Failure =>
        Errors.getFailure(systemId, errorId,corridorId).map{
          failure => Ok(toLiftJson(createErrorModel(systemId, None, errorId, failure._3.data, ErrorType.Failure.toString,failure._2)))
        }.getOrElse(defaultFailed)
      case _ => defaultFailed
    }
  }
}
