package controllers

import play.api.mvc.Controller
import views.html
import services._
import common.{SystemLogger, Formats}
import models.storage.ActionType._
import models.web.LocationCertificateCRUD
import play.api.i18n.Messages

object MonitorProcess extends Controller with Formats with SystemLogger with Secured{

  def getDetails(systemId:String, compName:String) = IsMemberOf(systemId, MonitorProcessView){ implicit user  => implicit request =>
     Ok(toLiftJson(MonitorProcessStatus.getDetails(systemId,compName)))
  }
}
