package controllers

import java.io._
import java.text.SimpleDateFormat
import java.util.{Calendar, TimeZone}

import akka.dispatch.Await
import play.api.Logger
import akka.pattern.ask
import akka.util.Timeout
import akka.util.duration._
import common.Extensions._
import common._
import common.actors.{DownloadCompleted, DownloadInProgress, GetDownloadStatus, StartDownloadingImagesFromGantry, _}
import common.hbase.utils.Vehicle
import models.storage.ActionType._
import models.storage.{ActionType, LogEntryModel, _}
import models.web.{Authorized, CorridorModelCRUD, Credential, DeleteModel, FileItem, LocationCertificateCRUD, PreferenceCRUD, PropertyChange, ResponseMsg, ScheduleCRUD, StateChangeCRUD, SystemModelCRUD, SystemModelCRUDTechnical, UserModel, _}
import play.api.i18n.Messages
import play.api.libs.iteratee.Enumerator
import play.api.mvc.{Cookie, ResponseHeader, SimpleResult, _}
import services.Statistics.Statistiek
import services.{Certificates, Storage, Users, _}
import csc.sectioncontrol.messages.certificates.ComponentCertificate
import csc.sectioncontrol.sign.certificate.CertificateService

object Bediening extends Controller with Formats with SystemLogger with Secured with Storage with GZIPCompressor {

  val permissions = ActionType.values.toList

  def index = Action {
    Redirect("/assets/bediening politie/index.html")
  }

  private def updateFlitscloudSystem(systemId: String, systemModel: SystemModelCRUD)(implicit user: UserModel): ResponseMsg = {
    val previous = getSystem(systemId).get
    val systemResp = Systems.update(systemId, systemModel)

    if (!systemResp.success) {
      systemResp
    }

    val updated = getSystem(systemId).get

    val changes = List(
      PropertyChange(Messages("systems.form.title.nrDaysKeepViolations"), previous.system.data.retentionTimes.nrDaysKeepViolations, updated.system.data.retentionTimes.nrDaysKeepViolations),
      PropertyChange(Messages("systems.form.title.nrDaysKeepTrafficData"), previous.system.data.retentionTimes.nrDaysKeepTrafficData, updated.system.data.retentionTimes.nrDaysKeepTrafficData),
      PropertyChange(Messages("systems.form.title.pardonTimeEsa_secs"), previous.system.data.pardonTimes.pardonTimeEsa_secs, updated.system.data.pardonTimes.pardonTimeEsa_secs),
      PropertyChange(Messages("systems.form.title.pardonTimeTechnical_secs"), previous.system.data.pardonTimes.pardonTimeTechnical_secs, updated.system.data.pardonTimes.pardonTimeTechnical_secs)

    ).flatMap(_.showChange)

    if (changes.length > 0)
      log(messageId = "screen.update.show.no.changes", systemId = systemId, List(user.username, user.reportingOfficerId, systemId, Messages("systems.menu.title"), Messages("systems.menu.title.single"), previous.system.data.name))
    else
      log(messageId = "screen.showed", systemId = systemId, List(Messages("systems.menu.title")))

    systemResp
  }

  private def updateFlitscloudSystemTechnical(systemId: String, systemModel: SystemModelCRUDTechnical)(implicit user: UserModel): ResponseMsg = {
    val previous = getSystem(systemId).get
    val systemResp = Systems.updateTechnical(systemId, systemModel)

    if (!systemResp.success) {
      systemResp
    }

    val updated = getSystem(systemId).get

    val changes = List(
      PropertyChange(Messages("systems.form.title.title"), previous.system.data.title, updated.system.data.title),
      PropertyChange(Messages("systems.form.title.description"), previous.system.data.location.description, updated.system.data.location.description),
      PropertyChange(Messages("systems.form.title.region"), previous.system.data.location.region, updated.system.data.location.region),
      PropertyChange(Messages("systems.form.title.roadNumber"), previous.system.data.location.roadNumber, updated.system.data.location.roadNumber),
      PropertyChange(Messages("systems.form.title.maxSpeed"), previous.system.data.maxSpeed, updated.system.data.maxSpeed),
      PropertyChange(Messages("systems.form.title.compressionFactor"), previous.system.data.compressionFactor, updated.system.data.compressionFactor),
      PropertyChange(Messages("systems.form.title.orderNumber"), previous.system.data.orderNumber, updated.system.data.orderNumber),
      PropertyChange(Messages("systems.form.title.serialNumber"), previous.system.data.serialNumber.serialNumber, updated.system.data.serialNumber.serialNumber),
      PropertyChange(Messages("systems.form.title.serialPrefix"), previous.system.data.serialNumber.serialNumberPrefix, updated.system.data.serialNumber.serialNumberPrefix),
      PropertyChange(Messages("systems.form.title.serialLength"), previous.system.data.serialNumber.serialNumberLength, updated.system.data.serialNumber.serialNumberLength)
    ).flatMap(_.showChange)

    if (changes.length > 0)
      log(messageId = "screen.update.show.no.changes", systemId = systemId, List(user.username, user.reportingOfficerId, systemId, Messages("systems.menu.title"), Messages("systems.menu.title.single"), previous.system.data.name))
    else
      log(messageId = "screen.showed", systemId = systemId, List(Messages("systems.menu.title")))

    systemResp
  }


  /*
  * update existing System
  * @param systemId is id of system
  * @return json object containing the result of updating a existing System
  */
  def update(systemId: String) = IsMemberOf(systemId, UpdateSystem) {
    implicit user => implicit request =>
      val body = request.body.asJson.get.toString
      val model = toModel[SystemModelCRUD](body)

      Ok(toLiftJson(updateFlitscloudSystem(systemId, model)))
  }

  /*
  * retrieve all systems
  * @return json object containing a sequence of all systems
  */
  def list = IsAuthenticated {
    implicit user => implicit request =>
      Ok(toLiftJson(getAllSystems))
  }

  case class userRole(role: String)

  def currentUser() = IsAuthenticated {
    implicit user => implicit request =>
      Ok(toLiftJson(userRole(role = user.role.description)))

  }

  def locationCertificatesGetAll(systemId: String) = IsPermitted(CertificatesView) {
    implicit user => implicit request =>
      try {

        val all: Seq[LocationCertificateCRUD] = Certificates.getAllSystemsLocationCertificates
        val allForSystem: Seq[LocationCertificateCRUD] = all.filter(p => p.systemId == systemId)

        Ok(toLiftJson(allForSystem))
      } catch {
        case e: Exception =>
          BadRequest(toLiftJson(ResponseMsg(
            typeName = ResponseType.System,
            description = Messages("actions.unkown.description"))))
      }
  }

  def getCurrentUser() = IsAuthenticated {
    implicit user => implicit request =>
      Users.findById(user.id) match {
        case Some(usr) => Ok(toLiftJson(usr))
        case _ => {
          BadRequest("User not found.")
        }

      }
  }

  def getByTimeDownload(systemId: String, logType: String, from: Long, to: Long, id: String) = IsAuthenticated {
    implicit user => implicit request =>
      Logs.getByTime(systemId, from, to) match {
        case Right(xx) =>
          val result: Seq[LogEntryModel] = toLogEntryModel(xx, systemId)
          val header =
            "Datum" + "|" +
              "Tijd" + "|" +
              "Actie" + "|" +
              "Sectie" + "|" +
              "Gebruiker" + "|" +
              "Rol"

          val resultList: Seq[String] = result.map(x => {
            asPrettyDate(x.timestamp, "dd-MM-yyyy") + "|" +
              asPrettyDate(x.timestamp, "HH:mm") + "|" +
              "Gebeurtenis: " + x.eventDisplay + ", Bericht: " + x.reason + "|" +
              x.corridor + "|" +
              x.userDisplay + "|" +
              x.userRole
          }
          )

          val output = header + "\r\n" + resultList.mkString("\r\n")

          Ok(output)
            .withHeaders(CONTENT_TYPE -> "application/octet-stream")
            .withHeaders(CONTENT_DISPOSITION -> "attachment;filename=logboek.csv")
            .withCookies(Cookie(name = "id", value = id, httpOnly = false, maxAge = 2))
        case Left(x) => BadRequest(toLiftJson(x))
      }
  }

  /**
   * Used to retrieve logging info based on a systemId
 *
   * @param systemId id of system
   * @param logType the type of log to view
   * @param from a long indicating start date/time of request
   * @param to a long indicating end date/time of request
   * @return retrieve logging info or an error message
   */
  def getByTime(systemId: String, logType: String, from: Long, to: Long) = IsAuthenticated {
    implicit user => implicit request =>
      Logs.getByTime(systemId, from, to) match {
        case Right(x) => Ok(toLiftJson(toLogEntryModel(x, systemId)))
        case Left(x) => BadRequest(toLiftJson(x))
      }
  }

  /**
   * returns the values of enum ConfigType including local translation
   * Used by the views
   */
  def getConfigTypes: List[(String, String)] =
    ConfigType.values.foldLeft(List[(String, String)]()) { (list, value) =>
      val key = "logs.configType." + value.toString
      val translated = Messages(key)
      if (translated == key) list else (value.toString, translated) :: list
    }.sortBy {
      _._2
    }

  /**
   * returns the values of enum LogType including local translation
   * Used by the views
   */
  def getLogTypes: List[(String, String)] = {
    LogType.values.foldLeft(List[(String, String)]()) {
      (list, value) =>
        val key = "logs.logType." + value.toString
        val translated = Messages(key)
        if (translated == key) list else (value.toString, translated) :: list
    }.sortBy {
      _._2
    }
  }

  def getStatisticsTraject(systemId: String, from: String, to: String) = IsAuthenticated {
    implicit user => implicit request =>
      try {
        Ok(toLiftJson(Statistics.getStatisticsForSystem(systemId, from, to)))
      }
      catch {
        case e: Exception =>
          Logger.error("Error retrieving statistics for traject", e)
          val response = ResponseMsg(typeName = ResponseType.System, description = "Unknown system error occured")
          BadRequest(toLiftJson(response))
      }
  }


  def downloadStatisticsTraject(systemId: String, from: String, to: String, id: String) = IsAuthenticated {
    implicit user => implicit request =>
      try {
        val result: Statistiek = Statistics.getStatisticsForSystem(systemId, from, to)
        val output = createCsvStatistics(result)

        Ok(output)
          .withHeaders(CONTENT_TYPE -> "application/octet-stream")
          .withHeaders(CONTENT_DISPOSITION -> "attachment;filename=statistiek.csv")
          .withCookies(Cookie(name = "id", value = id, httpOnly = false, maxAge = 2))
      }
      catch {
        case e: Exception =>
          Logger.error("Error downloading statistics for traject", e)
          val response = ResponseMsg(typeName = ResponseType.System, description = "Unknown system error occured")
          BadRequest(toLiftJson(response))
      }
  }

  def createCsvStatistics(result: Statistics.Statistiek): String = {
    val header = "Dag" + "|" +
      "Gemiddelde snelheid" + "|" +
      "Hoogst gemeten" + "|" +
      "Passages" + "|" +
      "Auto verw." + "|" +
      "Hand verw." + "|" +
      "overtredingen"

    val resultList: Seq[String] = result.statistiekRegel.map(x => {
      val csvString =
        x.datum + "|" +
          x.gemiddeld + "|" +
          x.max + "|" +
          x.totaalInPassages + "|" +
          x.overtredingenAuto + "|" +
          x.overtredingenHand + "|" +
          x.overtredingenTotaal
      csvString
    }
    )

    header + "\r\n" + resultList.mkString("\r\n")
  }

  def downloadStatisticsSectie(systemId: String, sectionId: String, from: String, to: String, id: String) = IsAuthenticated {
    implicit user => implicit request =>
      try {

        val result: Statistiek = Statistics.getStatisticsForSection(systemId, sectionId, from, to)
        val output = createCsvStatistics(result)

        Ok(output)
          .withHeaders(CONTENT_TYPE -> "application/octet-stream")
          .withHeaders(CONTENT_DISPOSITION -> "attachment;filename=statistiek.csv")
          .withCookies(Cookie(name = "id", value = id, httpOnly = false, maxAge = 2))
      }
      catch {
        case e: Exception =>
          Logger.error("Error downloading statistics for sectie", e)
          val response = ResponseMsg(typeName = ResponseType.System, description = "Unknown system error occured")
          BadRequest(toLiftJson(response))
      }
  }

  def getStatisticsSectie(systemId: String, sectionId: String, from: String, to: String) = IsAuthenticated {
    implicit user => implicit request =>
      Ok(toLiftJson(Statistics.getStatisticsForSection(systemId, sectionId, from, to)))
  }

  case class ChangePasswordError(errorCode: String, errorMessage: String)

  case class ChangePasswordModel(oldPassword: String, newPassword: String)

  def changePasswordCurrentUser() = IsAuthenticated {
    implicit user => implicit request =>
      Users.findById(user.id) match {
        case Some(userModel) =>
          var result: Result = NoContent
          val body = request.body.asJson.get.toString()
          try {
            val changePasswordModel: ChangePasswordModel = toModel[ChangePasswordModel](body)

            // If the passwords match it is allowed to change the password.
            if (userModel.passwordHash == changePasswordModel.oldPassword.digest()) {
              if (!isPasswordValid(changePasswordModel))
                result = BadRequest(toLiftJson(ChangePasswordError("0", "Het wachtwoord voldoet niet aan de volgende regel: 1 alfanumeriek karakter, 1 numeriek karakter, " +
                  "1 niet alfanumeriek en numeriek character.")))

              // Password cannot be the same as username
              else if (changePasswordModel.newPassword == userModel.username) {
                result = BadRequest(toLiftJson(ChangePasswordError("1", "Een wachtwoord kan niet hetzelfde zijn als de gebruikersnaam.")))
              }
              else if (changePasswordModel.newPassword.digest() == userModel.passwordHash ||
                changePasswordModel.newPassword.digest() == userModel.prevPasswordHash1 ||
                changePasswordModel.newPassword.digest() == userModel.prevPasswordHash2) {
                result = BadRequest(toLiftJson(ChangePasswordError("2", "Het nieuwe wachtwoord mag niet hetzelfde zijn als de 3 vorige wachtwoorden.")))
              }
              else {
                // do a hash of the new password.
                val passwordHash = changePasswordModel.newPassword.digest()

                val storageUser = curator.getVersioned[User](Paths.User.default / userModel.id)
                var badLoginCount = 0
                if (storageUser.isDefined) {
                  val x = storageUser.get
                  badLoginCount = x.data.badLoginCount.getOrElse(0)
                }

                val newUser = models.storage.User(
                  id = userModel.id,
                  reportingOfficerId = userModel.reportingOfficerId,
                  name = userModel.name,
                  username = userModel.username,
                  phone = userModel.phone,
                  ipAddress = userModel.ipAddress,
                  email = userModel.email,
                  passwordHash = passwordHash,
                  roleId = userModel.roleId,
                  sectionIds = userModel.sectionIds.toSet.toList,
                  prevPasswordHash1 = userModel.passwordHash,
                  prevPasswordHash2 = userModel.prevPasswordHash1,
                  badLoginCount = Some(badLoginCount))

                curator.set(Paths.User.default / newUser.id, newUser, userModel.version)

                result = Ok(toLiftJson(userModel))
              }

              result
            }
            else {
              BadRequest("Current passwords differ.")
            }
          }
          catch {
            case e: Exception =>
              val response = ResponseMsg(typeName = ResponseType.System, description = "Unknown system error occured")
              Ok(toLiftJson(response))
          }
        case _ =>
          val response = ResponseMsg(typeName = ResponseType.System, description = "Unknown system error occured")
          Ok(toLiftJson(response))
      }
  }


  def isPasswordValid(changePasswordModel: Bediening.ChangePasswordModel): Boolean = {
    val patternAlphanumeric = "[A-Za-z]".r
    val patternNumeric = "[\\d]".r
    val patternNotAlphanumericAndNotNumeric = "[\\W]".r

    val doesConformPatternAlphanumeric = ((patternAlphanumeric findAllIn changePasswordModel.newPassword).length >= 1)
    val doesConformPatternNumeric = ((patternNumeric findAllIn changePasswordModel.newPassword).length >= 1)
    val doesConformPatternNotAlphanumericAndNotNumeric = ((patternNotAlphanumericAndNotNumeric findAllIn changePasswordModel.newPassword).length >= 1)

    if (changePasswordModel.newPassword.length >= 8 &&
      doesConformPatternAlphanumeric &&
      doesConformPatternNumeric &&
      doesConformPatternNotAlphanumericAndNotNumeric
    ) {
      return true
    }

    false
  }

  /**
   * Handle login form submission.
   */
  def authenticate = Action {
    implicit request =>
      val body = request.body.asJson.get.toString
      val model = toModel[Credential](body)
      val result = Users.authenticate(model.username, model.password)

      result match {
        case Some(userModel) if userModel.userBlocked =>
            Unauthorized(toLiftJson(Authorized(false, true, "blocked")))
        case Some(userModel) =>
            Ok(toLiftJson(Authorized(true, false, "authorized"))).withSession("username" -> userModel.username)
        case None =>
          Unauthorized(toLiftJson(Authorized(false, false,"unauthorized")))
      }
  }

  def getTitle = Action {
    request =>{
      val log = Logger.logger
      try {
        log.info("getTitle called")
        val systems = Systems.getAll
        val title = systems.headOption.map(_.data.title).getOrElse("Bediening Politie")
        Ok(title)
      } catch {
        case t: Throwable => {
          log.error("Failed to retrieve the title return default", t)
          Ok("Bediening Politie")
        }
      }
    }
  }


  def getRoleCurrentUser = IsAuthenticated {
    implicit user => implicit request =>
      Ok(toLiftJson(userRole(role = user.role.description)));
  }

  /**
   * Logout and clean the session.
   */
  def logoutBediening() = IsAuthenticated {
    implicit user => implicit request =>
      Ok("Logged-out").withNewSession
  }

  def getAllAvailableCertificates(systemId: String) = IsPermitted(CertificatesView) {
    implicit user => implicit request =>
      Ok(toLiftJson(Certificates.getAvailableTypeCertificates(systemId)))
  }

  def getAllLocationCertificates(systemId: String) = IsPermitted(CertificatesView) {
    implicit user => implicit request =>
      val all: Seq[LocationCertificateCRUD] = Certificates.getAllSystemsLocationCertificates
      val allForSystem = all.filter(p => p.systemId == systemId)

      Ok(toLiftJson(allForSystem))
  }

  /*
* retrieve all sections that can be connected to a corridor
* @param systemId is id of system
* @param sectionId is id of section
*/
  def availableBeginSections(systemId: String, sectionId: String) = IsMemberOf(systemId, AllRoles) {
    implicit user => implicit request =>
      Ok(toLiftJson(Corridors.getAvailableFirstSections(systemId)))
  }

  /*
* retrieve all sections that can be connected to a corridor
* @param systemId is id of system
* @param sectionId is id of section
*/
  def availableEndSections(systemId: String, sectionId: String) = IsMemberOf(systemId, AllRoles) {
    implicit user => implicit request =>
      Ok(toLiftJson(Corridors.getAvailableSecondSections(systemId, sectionId)))
  }

  /**
   * get all schedule for a given corridorId
 *
   * @param systemId
   * @param corridorId
   * @return
   */
  def getByCorridorId(systemId: String, corridorId: String) = IsMemberOf(systemId, SchedulesView) {
    implicit user => implicit request =>
      val schedules = Schedules.getByCorridorId(systemId, corridorId)
      if (schedules.isDefined)
        Ok(toLiftJson(schedules))
      else
        BadRequest(toLiftJson("No schedule found."))
  }

  /*
* get schedule by id
* @param systemId is id of system
* @param corridorId is id of corridor
* @param scheduleId is id of schedule
* @return json object of the requested schedule
*/
  def getPeriodeSchemaById(systemId: String, corridorId: String, scheduleId: String) = IsMemberOf(systemId, SchedulesView) {
    implicit user => implicit request =>
      Schedules.getById(systemId, corridorId, scheduleId) match {
        case Some(sched) => {
          Ok(toLiftJson(sched))
        }
        case _ => {
          BadRequest(toLiftJson(ResponseMsg(
            typeName = ResponseType.System,
            description = Messages("Schedules.getById failed"))))
        }
      }
  }

  /*
* update existing Schedule
* @param systemId is id of system
* @param corridorId is id of corridor
* @param scheduleId is id of schedule
* @return json object containing the result of updating a schedule
*/
  def updatePeriodeSchema(systemId: String, corridorId: String, scheduleId: String) = IsMemberOf(systemId, SchedulesCrud) {
    implicit user => implicit request =>
      val body = request.body.asJson.get.toString
      val model = toModel[ScheduleCRUD](body)
      val result: ResponseMsg = Schedules.update(systemId, corridorId, scheduleId, model)

      Ok(toLiftJson(result))
  }

  /*
* delete schedule by given id and version
* @param systemId is id of system
* @param corridorId is id of corridor
* @param scheduleId is id of schedule
* @return json object containing the result of deleting a schedule
*/
  def deletePeriodeSchema(systemId: String, corridorId: String, scheduleId: String) = IsMemberOf(systemId, SchedulesCrud) {
    implicit user => implicit request =>
      val body = request.body.asJson.get.toString

      val model = toModel[DeleteModel](body)
      Schedules.delete(systemId, corridorId, scheduleId, model.version) match {
        case true =>
          Ok(toLiftJson(ResponseMsg(
            success = true,
            typeName = ResponseType.System,
            description = Messages("schedules.actions.delete.description.success"))))
        case false =>
          BadRequest(Messages("schedules.actions.delete.description.failed"))
      }
  }

  /*
  * create new Schedule
  * @param systemId is id of system
  * @param corridorId is id of corridor
  * @return json object containing the result of creating a new schedule
  */
  def createPeriodeSchema(systemId: String, corridorId: String) = IsMemberOf(systemId, SchedulesCrud) {
    implicit user => implicit request =>
      val body = request.body.asJson.get.toString
      val model = toModel[ScheduleCRUD](body)
      val responseMsg = Schedules.create(systemId, corridorId, model)

      Ok(toLiftJson(responseMsg))
  }

  /**
   * updates preferences
 *
   * @param systemId is id of system
   */
  def updatePreferences(systemId: String) = IsMemberOf(systemId, ParametersChange) {
    implicit user => implicit request =>
      val previous = Preferences.findById(systemId).get
      val body = request.body.asJson.get.toString
      val model = toModel[PreferenceCRUD](body)
      val response = Preferences.update(systemId, model)
      val updated = Preferences.findById(systemId).get

      val wheelBases = updated.data.wheelBase.foldLeft(List[PropertyChange]()) {
        case (list, update) =>
          val name = Messages("preferences.wheelBaseType.name") + ": " + Messages("preferences.wheelBaseType.name." + update.wheelBaseType.toString)
          val prevValue = previous.data.wheelBase.find(_.wheelBaseType == update.wheelBaseType).get.value
          PropertyChange(name, prevValue, update.value) :: list
      }.flatMap(_.showChange)

      val vehicleMaxSpeeds = updated.data.vehicleMaxSpeed.foldLeft(List[PropertyChange]()) {
        case (list, update) =>
          val name = Messages("preferences.vehicleMaxSpeed.name") + ": " + Messages("preferences.verhicleMaxSpeed.name." + update.vehicleType.toString) + " (" + update.vehicleType.toString + ")"
          val prevValue = previous.data.vehicleMaxSpeed.find(_.vehicleType == update.vehicleType).get.maxSpeed
          PropertyChange(name, prevValue, update.maxSpeed) :: list
      }.flatMap(_.showChange)

      val marginSpeedLimits = updated.data.marginSpeedLimits.foldLeft(List[PropertyChange]()) {
        case (list, update) =>
          val name = Messages("preferences.marginSpeedLimitType.name") + ": " + Messages("preferences.marginSpeedLimit.name", update.speedLimit)
          val prevValue = previous.data.marginSpeedLimits.find(_.speedLimit == update.speedLimit).get.speedMargin
          PropertyChange(name, prevValue, update.speedMargin) :: list
      }.flatMap(_.showChange)

      val parameterType = Messages("preferences.corridorType.name") + ": "
      val other = List(
        PropertyChange(parameterType + Messages("preferences.duplicateTriggerDelta.name"), previous.data.duplicateTriggerDelta, updated.data.duplicateTriggerDelta),
        PropertyChange(parameterType + Messages("preferences.inaccuracyMarginForLength.name"), previous.data.inaccuracyMarginForLength, updated.data.inaccuracyMarginForLength),
        PropertyChange(parameterType + Messages("preferences.minimumLengthTrailer.name"), previous.data.minimumLengthTrailer, updated.data.minimumLengthTrailer),
        PropertyChange(parameterType + Messages("preferences.maxLengthMoped.name"), previous.data.maxLengthMoped, updated.data.maxLengthMoped),
        PropertyChange(parameterType + Messages("preferences.countryCodeConfidence.name"), previous.data.countryCodeConfidence, updated.data.countryCodeConfidence),
        PropertyChange(parameterType + Messages("preferences.licenseConfidence.name"), previous.data.licenseConfidence, updated.data.licenseConfidence),
        PropertyChange(parameterType + Messages("preferences.testLicenseConfidence.name"), previous.data.testLicenseConfidence, updated.data.testLicenseConfidence),
        PropertyChange(parameterType + Messages("preferences.shortVehicleLengthThreshold.name"), previous.data.shortVehicleLengthThreshold, updated.data.shortVehicleLengthThreshold),
        PropertyChange(parameterType + Messages("preferences.longVehicleLengthThreshold.name"), previous.data.longVehicleLengthThreshold, updated.data.longVehicleLengthThreshold)
      ).flatMap(_.showChange)

      val changes = wheelBases ++ vehicleMaxSpeeds ++ marginSpeedLimits ++ other
      if (changes.length > 0)
        log(messageId = "screen.update.show.no.changes", systemId = systemId, List(user.username, user.reportingOfficerId, systemId, Messages("preferences.menu.title"), "parameters", ""))
      else
        log(messageId = "screen.showed", systemId = systemId, List(Messages("preferences.menu.title")))

      if (response.success)
        Ok(toLiftJson(response))
      else
        BadRequest(toLiftJson(response))
  }

  /*
* get an existing preference
* @param systemId is id of system
* @param preferenceId is id of preference
*/
  def getPreferences(systemId: String) = IsMemberOf(systemId, ParametersView) {
    implicit user => implicit request =>
      Preferences.findById(systemId) match {
        case Some(pref) => {
          Ok(toLiftJson(pref))
        }
        case _ =>
          BadRequest(toLiftJson(ResponseMsg(
            typeName = ResponseType.System,
            description = Messages("preferences.actions.get.description.failed"))))
      }
  }

  /*
* request to change the state of a given system
* @param systemId is id of system
* @return json object containing if the creating of the request was successful (handhaven aan uit)
*/
  def changeStateCorridor(systemId: String, corridorId: String) = IsMemberOf(systemId, ChangeSystemStateHandhavenStandby) {
    implicit user => implicit request =>
      val body = request.body.asJson.get.toString
      val model = toModel[StateChangeCRUD](body)

      // when in devmode change the state in zookeeper. So changes are shown directly in the GUI.
      //val versionedState= curator.getVersioned[CorridorState](Corridors.getStatePath(systemId, corridorId))
      //val newState=CorridorState(model.to.toString,versionedState.get.data.timestamp,user.id,"test")

      val result: ResponseMsg = Systems.changeState(systemId, corridorId, model = model)
      //curator.set(Corridors.getStatePath(systemId, corridorId), newState, versionedState.get.version)

      if (result.success)
        Ok(toLiftJson(result))
      else
        BadRequest(toLiftJson(result))
  }

  /*
* request to change the state of a given system
* @param systemId is id of system
* @return json object containing if the creating of the request was successful (handhaven aan uit)
*/
  def changeStateOffStandbyMaintenanceCorridor(systemId: String, corridorId: String) = IsMemberOf(systemId, ChangeSystemStateOffStandbyMaintenance) {
    implicit user => implicit request =>
      val body = request.body.asJson.get.toString
      val model = toModel[StateChangeCRUD](body)

      // when in devmode change the state in zookeeper. So changes are shown directly in the GUI.
      //val versionedState= curator.getVersioned[CorridorState](Corridors.getStatePath(systemId, corridorId))
      //val newState=CorridorState(model.to.toString,versionedState.get.data.timestamp,user.id,"test")
      val result: ResponseMsg = Systems.changeState(systemId, corridorId, model = model)
      //curator.set(Corridors.getStatePath(systemId, corridorId), newState, versionedState.get.version)

      if (result.success)
        Ok(toLiftJson(result))
      else
        BadRequest(toLiftJson(result))
  }

  def getNmiCertificate(systemId: String) = IsMemberOf(systemId, CertificatesView) {
    implicit user => implicit request =>
      log(messageId = "screen.showed", systemId = systemId, List(Messages("nmi.menu.title")))
      try {
        Ok(toLiftJson(Certificates.getSystemActiveCertificate(systemId)))
      } catch {
        case e: Exception =>
          BadRequest(toLiftJson(ResponseMsg(
            typeName = ResponseType.System,
            description = Messages("actions.unkown.description"))))
      }
  }

  /*
* update existing System
* @param systemId is id of system
* @return json object containing the result of updating a existing System
*/
  def updateSystem(systemId: String) = IsMemberOf(systemId, SystemsUpdate) {
    implicit user => implicit request =>
      try {
        val body = request.body.asJson.get.toString()
        val model = toModel[SystemModelCRUD](body)
        val response = updateFlitscloudSystem(systemId, model)

        if (response.success)
          Ok(toLiftJson(response))
        else
          BadRequest(toLiftJson(response))
      } catch {
        case e: Exception =>
          BadRequest(toLiftJson(ResponseMsg(
            typeName = ResponseType.System,
            description = Messages("actions.unkown.description"))))
      }
  }

  /*
* update existing System
* @param systemId is id of system
* @return json object containing the result of updating a existing System
*/
  def updateSystemTechnical(systemId: String) = IsMemberOf(systemId, TechnicalCud) {
    implicit user => implicit request =>
      try {
        val body = request.body.asJson.get.toString()
        val model = toModel[SystemModelCRUDTechnical](body)
        val response = updateFlitscloudSystemTechnical(systemId, model)

        if (response.success)
          Ok(toLiftJson(response))
        else
          BadRequest(toLiftJson(response))
      } catch {
        case e: Exception =>
          BadRequest(toLiftJson(ResponseMsg(
            typeName = ResponseType.System,
            description = Messages("actions.unkown.description"))))
      }
  }



  /*
* get System by a given id
* @param systemId is id of system
* @param sectionId is id of section
* @return json object containing a System model or a
* json ResponseMsg object when he System could not be found
*/
  def getTraject(systemId: String) = IsMemberOf(systemId, AllRoles) {
    implicit user => implicit request =>
      val system = Systems.findById(systemId) match {
        case Some(versionOf) => Some(createSystemModel(versionOf))
        case _ => None
      }

      system match {
        case Some(model) =>
          Ok(toLiftJson(model))
        case _ =>
          BadRequest(toLiftJson(ResponseMsg(
            typeName = ResponseType.System,
            description = Messages("sectionsystem.actions.get.description.failed"))))
      }
  }

  def getCorridor(systemId: String, corridorId: String) = IsMemberOf(systemId, AllRoles) {
    implicit user => implicit request =>
      Corridors.findById(systemId, corridorId) match {
        case Some(corridor) =>
          Ok(toLiftJson(corridor))
        case _ =>
          BadRequest(toLiftJson(ResponseMsg(
            typeName = ResponseType.System,
            description = Messages("corridor.actions.get.description.failed\""))))
      }
  }

  case class CorridorReportInfo(reportId: String, reportHtId: String)

  def getCorridorReportInfo(systemId: String, corridorId: String) = IsMemberOf(systemId, AllRoles) {
    implicit user => implicit request =>

      Corridors.findById(systemId, corridorId) match {
        case Some(corridor) =>
          val corridorReportInfo = CorridorReportInfo(corridor.data.info.reportId.getOrElse(""), corridor.data.info.reportHtId.getOrElse(""))
          Ok(toLiftJson(corridorReportInfo))

        case _ =>
          BadRequest(toLiftJson(ResponseMsg(
            typeName = ResponseType.System,
            description = Messages("corridor.actions.get.description.failed\""))))
      }
  }

  /*
* updates a corridor
* @param systemId is id of system
* @param corridorId is id of corridor
* @return json object containing the result of updating a section
*/
  def putCorridor(systemId: String, corridorId: String) = IsMemberOf(systemId, CorridorsCrud) {
    implicit user => implicit request =>
      val body = request.body.asJson.get.toString()
      val model = toModel[CorridorModelCRUD](body)

      val responseMsg = updateFlitscloudCorridor(systemId, corridorId, model)
      if (responseMsg.success)
        Ok(toLiftJson(responseMsg))
      else
        BadRequest(toLiftJson(responseMsg.fields.map(x => x.error).mkString(",")))
  }

  private def updateFlitscloudCorridor(systemId: String, corridorId: String, model: CorridorModelCRUD)(implicit user: UserModel): ResponseMsg = {
    val previous = getCorridors(systemId).find(_.id == corridorId)
    val corridorResp = Corridors.update(systemId, corridorId, model)

    val responseMsg = if (corridorResp.success) {
      corridorResp
    } else {
      corridorResp
    }

    val updated = getCorridors(systemId).find(_.id == corridorId)
    if (previous.isDefined && updated.isDefined) {
      val prev = previous.get
      val up = updated.get

      val changes = List(
        PropertyChange(Messages("corridors.form.title.first_section"), prev.startSectionId, up.startSectionId),
        PropertyChange(Messages("corridors.form.title.last_section"), prev.endSectionId, up.endSectionId),
        PropertyChange(Messages("corridors.form.title.name"), prev.name, up.name),
        PropertyChange(Messages("corridors.form.title.roadType"), Messages("corridors.form.title.roadType." + prev.roadType), Messages("corridors.form.title.roadType." + up.roadType)),

        PropertyChange(Messages("services.form.title.service"), Messages("services.form.title." + prev.serviceType), Messages("services.form.title." + up.serviceType)),
        PropertyChange(Messages("preferences.other.caseFile.title"), Messages("property." + prev.caseFileType), Messages("property." + up.caseFileType)),
        PropertyChange(Messages("corridors.form.title.isInsideUrbanArea"), Messages("property." + prev.isInsideUrbanArea), Messages("property." + up.isInsideUrbanArea)),
        PropertyChange(Messages("corridors.form.title.isInsideUrbanArea"), Messages("property." + prev.isInsideUrbanArea), Messages("property." + prev.isInsideUrbanArea)),

        PropertyChange(Messages("corridors.form.title.specificCorridorId"), prev.specificCorridorId, up.specificCorridorId),
        PropertyChange(Messages("corridors.form.title.locationCode2"), prev.locationCode, up.locationCode),
        PropertyChange(Messages("corridors.form.title.locationLine1"), prev.locationLine1, up.locationLine1),
        PropertyChange(Messages("corridors.form.title.locationLine2"), prev.locationLine2, up.locationLine2)
      )

      val all = changes.flatMap(_.showChange)

      if (all.nonEmpty)
        log(messageId = "screen.update.show.no.changes", systemId = systemId, List(user.username, user.reportingOfficerId, systemId, Messages("corridors.menu.title"), Messages("corridors.menu.title.single"), prev.name))
      else
        log(messageId = "screen.showed", systemId = systemId, List(Messages("corridors.menu.title")))
    }

    responseMsg
  }

  /**
   * format a given long values as a date
 *
   * @param time to format
   * @param format optional date format
   * @return string representation of a given date
   */
  private def asPrettyDate(time: Long, format: String = "yyyy-MM-dd HHmmss.SSS zzz"): String = {
    val cal = Calendar.getInstance(TimeZone.getTimeZone("CET"))
    val sdf = new SimpleDateFormat(format)
    cal.setTimeInMillis(time)
    sdf.format(cal.getTime)
  }

  // Needed for ask pattern with actor to check for download status of images
  implicit val timeout = Timeout(5 seconds)

  case class DownloadImagesStatusHttpResponse(refId: String, status: String)

  /**
   * Start a process to retrieve all images from the gantry. Uses actors underneath.
   */
  def downloadImagesForPassageGegevens(systemId: String, gantryId: String, from: Long, to: Long,kenteken:String) = IsPermitted() { implicit user => implicit request =>
    val imageKeys = Vehicles.getImageKeysForVehiclesWithMissingImages(systemId, gantryId, from, to,kenteken)
    if (imageKeys.isEmpty) {
      Ok
    } else {
      val future = Global.downloadPassageGegevensImageActor ? StartDownloadingImagesFromGantry(systemId, gantryId, imageKeys)
      val status = Await.result(future, timeout.duration).asInstanceOf[DownloadInProgress]
      Ok(toLiftJson(DownloadImagesStatusHttpResponse(status.refId, "IN_PROGRESS")))
    }
  }

  def getStatusOfImageRetrievalForPassageGegevens(refId: String) = IsPermitted() { implicit user => implicit request =>
    val future = Global.downloadPassageGegevensImageActor ? GetDownloadStatus(refId)
    val status = Await.result(future, timeout.duration).asInstanceOf[DownloadStatus]
    status match {
      case DownloadInProgress(refId) => Ok(toLiftJson(DownloadImagesStatusHttpResponse(refId, "IN_PROGRESS")))
      case DownloadCompleted(refId) => Ok(toLiftJson(DownloadImagesStatusHttpResponse(refId, "COMPLETED")))
    }
  }

  /**
   * return all vehicles between a given date range
 *
   * @param fromEpoch a long that represents a from date
   * @param toEpoch a long that represents a to date
   * @param withPhotos indicate if photos must be retrieved as well
   * @return a zip file containing all vehicles info
   *         and an overview and license picture that are between
   *         the given time range
   */
  def getPassagegegevensForDownloadAvailable(systemId: String, gantryId: String, fromEpoch: Long, toEpoch: Long, withPhotos: Boolean, licensePlate: String) = IsPermitted() {

    implicit user => implicit request =>
      case class downloadAvailableResult(result: Boolean)

      val maxMinutes = ((toEpoch - fromEpoch) / 60).toInt
      val error = withPhotos && maxMinutes > 30 * 60

      if (!error) {
        val start = Calendar.getInstance()
        val end = Calendar.getInstance()

        start.setTimeInMillis(fromEpoch)
        end.setTimeInMillis(toEpoch)

        var vehicles: Seq[Vehicle] = null

        val allVehiclesInTimeFrame = Vehicles.getByTime(systemId, gantryId, start.getTimeInMillis, end.getTimeInMillis, withPhotos,licensePlate)
        if (licensePlate.trim.isEmpty) {
          vehicles = allVehiclesInTimeFrame
        } else {
          vehicles = allVehiclesInTimeFrame.filter(v => {
            val license: String = v.metadataNew.license.map(_.value).getOrElse("")
            license == licensePlate
          })
        }

        Ok(toLiftJson(downloadAvailableResult(result = !vehicles.isEmpty)))
      }
      else {
        BadRequest("Interval is te groot voor 'met foto', max is 30 minuten.")
      }
  }

  def calculateSeal() = IsPermitted(CertificatesView){ implicit user => implicit request =>
    val body = request.body.asJson.get.toString
    val model = toModel[List[ComponentCertificate]](body)
    CertificateService.calculateSeal(model)
    Ok(toLiftJson(Seal(CertificateService.calculateSeal(model))))
  }

  /**
   * return all vehicles between a given date range
 *
   * @param fromEpoch a long that represents a from date
   * @param toEpoch a long that represents a to date
   * @param withPhotos indicate if photos must be retrieved as well
   * @return a zip file containing all vehicles info
   *         and an overview and license picture that are between
   *         the given time range
   */
  def getPassagegegevens(systemId: String, gantryId: String, fromEpoch: Long, toEpoch: Long, withPhotos: Boolean, licensePlate: String, id: String) = IsPermitted() {
    implicit user => implicit request =>
      val maxMinutes = ((toEpoch - fromEpoch) / 60).toInt
      val error = withPhotos && maxMinutes > 30 * 60

      if (!error) {
        Systems.sendSystemEvent(
          reason = "Downloaded vehicle data %s photos, for gantry %s from %s ,maxMinutes [%s]".format(
            if (withPhotos) "with" else "without",
            systemId + ":" + gantryId,
            new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new java.util.Date(fromEpoch * 1000)),
            maxMinutes),
          systemId = systemId
        )

        val start = Calendar.getInstance()
        val end = Calendar.getInstance()

        start.setTimeInMillis(fromEpoch)
        end.setTimeInMillis(toEpoch)

        var gantries = Map[String, String]()
        val vehicles = Vehicles.getByTime(systemId, gantryId, start.getTimeInMillis, end.getTimeInMillis, withPhotos,licensePlate)
        val fileItems: List[FileItem] = vehicles.foldLeft(List[FileItem]()) {
          (list, vehicle) =>
            val key = vehicle.metadataNew.lane.system + ":" + vehicle.metadataNew.lane.gantry
            if (!gantries.contains(key)) {
              gantries += key -> Gantries
                .findById(vehicle.metadataNew.lane.system, vehicle.metadataNew.lane.gantry)
                .map(_.data.hectometer)
                .getOrElse("")
            }

            val overviewFileName = vehicle.metadataNew.lane.laneId + " " + asPrettyDate(vehicle.metadataNew.eventTimestamp) + " Overview.jpg"
            val licenseFileName = vehicle.metadataNew.lane.laneId + " " + asPrettyDate(vehicle.metadataNew.eventTimestamp) + " License.jpg"
            val overview = vehicle.overview.map(x => FileItem(fileName = overviewFileName, data = x) :: list).getOrElse(list)
            val license: List[FileItem] = vehicle.license.map(x => FileItem(fileName = licenseFileName, data = x) :: overview).getOrElse(overview)

            val header = List(
              "datum",
              "tijdstip passagepunt",
              "kilometerpunt passagepunt",
              "rijstrook",
              "overview foto identificatie",
              "kenteken foto identificatie",
              "kenteken",
              "landcode"
            ).mkString("|")

            val csvString = List(
                asPrettyDate(vehicle.metadataNew.eventTimestamp, "dd-MM-yyyy"),
                asPrettyDate(vehicle.metadataNew.eventTimestamp, "HH:mm"),
                gantries(key),
                vehicle.metadataNew.lane.name,
                overviewFileName,
                licenseFileName,
                vehicle.metadataNew.license.map(_.value.toString).getOrElse(""),
                vehicle.metadataNew.country.map(_.value).getOrElse("")
            ).mkString("|")

            val csvFileName = vehicle.metadataNew.lane.laneId + " " + asPrettyDate(vehicle.metadataNew.eventTimestamp) + ".csv"

            val output = header + "\r\n" + csvString

            FileItem(fileName = csvFileName, data = output.getBytes) :: license
        }


        val cal = Calendar.getInstance()
        val fileName = "attachment;filename=passagegegevens_%s.zip".format(cal.getTimeInMillis.toString)
        val zipFileName = "passagegegevens_%s.zip".format(cal.getTimeInMillis.toString)

        if (fileItems.isEmpty) {
          // use the in memory zip. There are no results so memory should be no problem
          val fn = "geen data gevonden."
          val file = List(FileItem(fileName = fn, data = "".getBytes))
          val zipResult = zip(file)

          SimpleResult(
            header = ResponseHeader(200, Map(CONTENT_DISPOSITION -> fileName)),
            body = Enumerator.fromStream(new ByteArrayInputStream(zipResult))
          ).withCookies(Cookie(name = "id", value = id, httpOnly = false, maxAge = 2))
        } else {
          // Use the filesystem zip because of the possible size
          val zip = new Zip()
          zip.compress(Config.DownloadPassagegegevens.path, zipFileName, fileItems)
          val zipFile = new File(Config.DownloadPassagegegevens.path + zipFileName)

          SimpleResult(
            header = ResponseHeader(200, Map(
              CONTENT_DISPOSITION -> fileName,
              CONTENT_LENGTH -> zipFile.length().toString)),
            body = Enumerator.fromFile(zipFile)
          ).withCookies(Cookie(name = "id", value = id, httpOnly = false, maxAge = 2))
        }
      }
      else {
        BadRequest("Interval is te groot voor 'met foto', max is 30 minuten.")
      }
  }

  /**
   * Aangezien het lokaal lastig is te testen met passagegevens deze lokale oplossing om files
   * te downloaden en laten opruimen door de filecleaner.
   * Gewoon lekker laten staan.
   */
//  def downloadPassagegegevensTest() = IsAuthenticated {
//    import scala.collection.mutable.ListBuffer
//    implicit user => implicit request =>
//      val cal = Calendar.getInstance()
//      val fileName = "attachment;filename=passagegegevens_%s.zip".format(cal.getTimeInMillis.toString)
//      val zipFileName = "passagegegevens_%s.zip".format(cal.getTimeInMillis.toString)
//
//      val bytes = new Array[Byte](20000)
//      var fileItems = new ListBuffer[FileItem]()
//
//      for (a <- 1 to 20) {
//        val fileItem = new FileItem("test" + a.toString, bytes)
//        fileItems += fileItem
//      }
//
//      val zip = new Zip()
//      zip.compress(Config.DownloadPassagegegevens.path, zipFileName, fileItems)
//      val zipFile = new File(Config.DownloadPassagegegevens.path + zipFileName)
//
//      SimpleResult(
//        header = ResponseHeader(200, Map(
//          CONTENT_DISPOSITION -> fileName,
//          CONTENT_LENGTH -> zipFile.length().toString)),
//        body = Enumerator.fromFile(zipFile)
//      ).withCookies(Cookie(name = "id", value = "1", httpOnly = false, maxAge = 2))
//  }
}
