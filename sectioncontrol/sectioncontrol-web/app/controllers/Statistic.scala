package controllers

import common.{Models, Formats}
import views.html
import models.storage.ActionType._
import services._
import play.api.mvc.SimpleResult._
import play.api.mvc.ResponseHeader._
import play.api.libs.iteratee.Enumerator
import java.io.ByteArrayInputStream
import csc.config.Path._
import play.api.mvc._
import csc.config.Path
import play.api.i18n.Messages
import models.web.{InstalledCertificateShow, UserModel, StatisticsModel}
import play.api.mvc.ResponseHeader
import play.api.mvc.SimpleResult

object Statistic extends Controller with Formats with Secured{
  /**
   * retrieves statistics for a given system
   * @param systemId id of system
   */
  def getStatisticsForSystem(systemId:String): Action[(Action[AnyContent], AnyContent)] = IsMemberOf(systemId){ implicit user  => implicit request =>
    Ok(toLiftJson(Models.getStatistics(systemId)))
  }

  /**
   * retrieves statistics for a given system and report id
   * @param systemId id of system
   * @param reportId id of report
   * @return daily statistics in xml format
   */
  def getById(systemId:String, reportId:String) = IsMemberOf(systemId){ implicit user  => implicit request =>
    Ok(Statistics.getById(systemId, reportId).toString()).as("text/xml")
  }

  /**
   * returns the day report
   * @param systemId id of system
   * @param reportId id of report
   * @return DayStatistics
   */
  def getDayReport(systemId:String, reportId:String) = IsMemberOf(systemId){ implicit user  => implicit request =>
    Statistics.getDayReport(systemId, reportId).map{
      dayReport =>
        val fileName = "attachment; filename=%s".format(
          dayReport.fileName.
            replaceAllLiterally(" ", "_"))
        SimpleResult(
          header = ResponseHeader(200, Map(
              CONTENT_DISPOSITION -> fileName,
              CONTENT_TYPE -> "x-gzip"
          )),
          body = Enumerator.fromStream(new ByteArrayInputStream(dayReport.data.map(_.toByte).toArray))
        )
    }.getOrElse{
      val fileName = "attachment; filename=%s".format(Messages("statistics.menu.title.no-download-file"))
      SimpleResult(
        header = ResponseHeader(200, Map(CONTENT_DISPOSITION -> fileName)),
        body = Enumerator.fromStream(new ByteArrayInputStream(Array[Byte]()))
      )
    }
  }
}
