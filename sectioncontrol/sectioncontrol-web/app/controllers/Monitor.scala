package controllers

import java.text.SimpleDateFormat
import java.util.{Calendar, Date}

import common.hbase.utils.Vehicle
import common.{Formats, GZIPCompressor}
import csc.curator.utils.Versioned
import models.storage.Gantry
import models.web.UserModel
import play.api.mvc.Controller
import services._

import scala.xml.Elem

object Monitor extends Controller with Storage with Secured with GZIPCompressor with Formats{

  case class Average(licenseConfidence:Float=0.0f, speedConfidence:Float=0.0f, countryConfidence:Float=0.0f, licensesWithSpeed:Long=0, averageLicensesWithSpeed:Float=0.0f, licensesWithCategory:Long=0, averageLicensesWithCategory:Float=0.0f, minSpeed:Float=0.0f, maxSpeed:Float=0.0f, averageSpeed:Float=0.0f)
  case class PassageInfo(isVehicle:Boolean,  speed:Float=0.0f, rawLicense:String, country:String, license:String, length:Float=0.0f, category:String, eventTimestamp:Long, eventTimestampStr:String, confidence:Confidence)
  case class TimeInfo(hour:Int, vehicles:Long, licenses:Long, recognized:Float, average:Average, passages:Seq[PassageInfo]=Nil)
  case class CameraInfo(id:String, vehicles:Long, licenses:Long, recognized:Float,times:Seq[TimeInfo], average:Average)
  case class GantryInfo(id:String, vehicles:Long, licenses:Long, recognized:Float, cameras:Seq[CameraInfo], average:Average)
  case class SystemInfo(id:String, from:Date, to:Date)
  case class Confidence(country:String, license:String, category:String, length:String, speed:String)
  
  def getReport(zip:Boolean=false, systemId:String, gantries:String, licenses:String, from:String, to:String, output:String="xml", withHours:Boolean=false, withAverages:Boolean=false, minutes:Int=20, withPassages:Boolean=false, filterOnVehicles:Boolean=false, categories:String, lengthGreater:String, lengthSmaller:String, lengths:String)= IsAuthenticated{ implicit user => implicit request =>
    val (fromDate:Date, toDate:Date) = (from, to) match {
      case (null, _) | (_, null) | ("", _) | (_, "") =>
        val today = new Date
        (addMinutes(today, -minutes), today)
      case (f, t) =>
        val formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        (formatter.parse(f), formatter.parse(t))
    }

    //gantry ids
    val gIds:Seq[String] = gantries match {
      case null | "" => Gantries.getBySystemId(systemId).map(_.data.id)
      case _ => gantries.split(",").map(_.trim).filter(_!="").toList
    }

    //licenses
    val licns:Seq[String] = licenses match {
      case null | "" => Nil
      case _ => licenses.split(",").map(_.trim).filter(_!="").toList
    }

    val lg:Option[Float] = lengthGreater match {
      case null | "" => None
      case _ => Some(lengthGreater.toFloat)
    }

    val ls:Option[Float] = lengthSmaller match {
      case null | "" => None
      case _ => Some(lengthSmaller.toFloat)
    }

    val lngs:Seq[Float] = lengths match {
      case null | "" => Nil
      case _ => lengths.split(",").map(_.trim).filter(_!="").map(_.toFloat).toList
    }

    val cats:Seq[String] = categories match {
      case null | "" => Nil
      case _ => categories.split(",").map(_.trim).filter(_!="").toList
    }

    val calculatedReport = calculateReport(systemId = systemId, gantryIds = gIds, from = fromDate, to = toDate, withAverage = withAverages, withPassages = withPassages, categories = cats, lengths = lngs, lengthGreater = lg, lengthSmaller = ls, licenses = licns)
    lazy val xml = toXml(calculatedReport, fromDate, toDate, withHours, withAverages, withPassages, filterOnVehicles)
    lazy val csv = toCSV(calculatedReport, fromDate, toDate, withHours, withAverages, withPassages, filterOnVehicles)

    (output, zip) match {
      case ("xml",false) => Ok(xml.toString())
      case ("csv",false) => Ok(csv)
      case ("xml",true) => Ok(compress(xml.toString().getBytes))
      case ("csv",true) => Ok(compress(csv.getBytes))
      case _  => Ok(xml.toString())
    }
  }

  private def getBetweenHours(from:Date, to:Date):Seq[Int]={
    val calFrom = Calendar.getInstance()
    val calTo = Calendar.getInstance()

    calFrom.setTimeInMillis(from.getTime)
    calTo.setTimeInMillis(to.getTime)

    var hours = List[Int]()
    while(calFrom < calTo){
      val hour = calFrom.get(Calendar.HOUR_OF_DAY)
      if(!hours.exists(_ == hour)) hours ::= hour
      calFrom.add(Calendar.MINUTE, 1)
    }

    hours.sorted
  }
  
  private def createPassages(passages:Seq[Vehicle]):Seq[PassageInfo]={
    passages.foldLeft(List[PassageInfo]()){
      (list, vehicle) =>
        PassageInfo(
          isVehicle=vehicle.metadata.license.isDefined,
          license = vehicle.metadata.license.map(_.value).getOrElse(""),
          length = vehicle.metadata.length.map(_.value).getOrElse(0.0f),
          speed = vehicle.metadata.speed.map(_.value).getOrElse(0.0f),
          rawLicense = vehicle.metadata.rawLicense.getOrElse(""),
          country = vehicle.metadata.country.map(_.value).getOrElse(""),
          category = vehicle.metadata.category.map(_.value).getOrElse(""),
          eventTimestamp = vehicle.metadata.eventTimestamp,
          eventTimestampStr = vehicle.metadata.eventTimestampStr.getOrElse(""),
          confidence = Confidence(
            license = vehicle.metadata.license.map(_.confidence.toString).getOrElse(""),
            country = vehicle.metadata.country.map(_.confidence.toString).getOrElse(""),
            category = vehicle.metadata.category.map(_.confidence.toString).getOrElse(""),
            length = vehicle.metadata.length.map(_.confidence.toString).getOrElse(""),
            speed = vehicle.metadata.speed.map(_.confidence.toString).getOrElse(""))
        ) :: list
    }.sortBy(_.eventTimestamp)
  }

  private def calculateReport(systemId:String, gantryIds:Seq[String], from:Date, to:Date, withAverage:Boolean, withPassages:Boolean, categories:Seq[String], lengths:Seq[Float], lengthGreater:Option[Float], lengthSmaller:Option[Float], licenses:Seq[String])(implicit user:UserModel):List[GantryInfo]={
    val cal = Calendar.getInstance()
    val gantries:Seq[Versioned[Gantry]] = gantryIds.flatten(g => Gantries.getConfig(systemId, g))
    val hours = getBetweenHours(from, to)
    gantries.foldLeft(List[GantryInfo]()){
      (list, gantry) =>
        val allCameras = Lanes.getAllByGantryId(systemId, gantry.data.id).sortBy(_.data.id)
        val vehicles = Vehicles.getByTime(systemId, gantry.data.id, from.getTime, to.getTime, false,"")
        val totalVehicles =  vehicles.length
        val totalLicenses = vehicles.filter(_.metadata.license.isDefined).length
        val recognized = calculatePercentage(totalLicenses, totalVehicles)
        val groupedByCameras = vehicles.groupBy[String](_.metadata.lane.laneId)

        val cameras =
          allCameras.foldLeft(List[CameraInfo]()){
            (list, camera) =>
              val laneId = systemId + "-" + gantry.data.id + "-" + camera.data.id //A2-E3-E3R2
              val dataOfCamera = groupedByCameras.get(laneId).getOrElse(List())
              val vehiclesPerCamera = dataOfCamera.length
              val licensesPerCamera = dataOfCamera.filter(_.metadata.license.isDefined).length
              val recognizedPerCamera = calculatePercentage(licensesPerCamera, vehiclesPerCamera)

              val times = hours.foldLeft(List[TimeInfo]()){
                (list, hour) =>
                  val allVehiclesPerHour = dataOfCamera.filter{g =>
                    cal.clear()
                    cal.setTimeInMillis(g.metadata.eventTimestamp)
                    cal.get(Calendar.HOUR_OF_DAY) == hour
                  }
                  val allLicensesPerHour:Seq[Vehicle] = allVehiclesPerHour.filter(_.metadata.license.isDefined)
                  val vehiclesPerHour:Int = allVehiclesPerHour.length
                  val licensesPerHour:Int = allLicensesPerHour.length
                  if(vehiclesPerHour == 0 || licensesPerHour == 0){
                    TimeInfo(hour = hour, vehicles=0, licenses=0, recognized=0.0f, average=Average()) :: list
                  }else{

                    val recognizedPerHour:Float = calculatePercentage(licensesPerHour, vehiclesPerHour)
                    val passages_1:Seq[PassageInfo] = if(withPassages) createPassages(allVehiclesPerHour) else Nil
                    //val passages = if(categories.length > 0) passages2.filter(p => categories.contains(p.category)) else passages2
                    val passages_2 = (lengthGreater, lengthSmaller) match {
                      case (None, Some(a)) => passages_1.filter(p => p.length < a)
                      case (Some(a), None) => passages_1.filter(p => p.length > a)
                      case (Some(a), Some(b)) => passages_1.filter(p => p.length > a && p.length < b)
                      case (None, None) => passages_1
                    }
                    val passages = if(licenses.length > 0)
                      passages_1.filter(p => licenses.contains(p.license))
                    else if(categories.length > 0)
                      passages_2.filter(p => categories.contains(p.category))
                    else passages_2
                    if(withAverage){
                      val allLicenseConfidencesPerHour:Seq[Float] = allLicensesPerHour.flatMap(_.metadata.license.map(_.confidence.toFloat))//.getOrElse(0.0f))
                      val allSpeedConfidencesPerHour:Seq[Float] = allLicensesPerHour.flatMap(_.metadata.speed.map(_.confidence.toFloat))//.getOrElse(0.0f))
                      val allCountryConfidencesPerHour:Seq[Float] = allLicensesPerHour.flatMap(_.metadata.country.map(_.confidence.toFloat))//.getOrElse(0.0f))
                      val allSpeedsPerHour:Seq[Float] = allLicensesPerHour.flatMap(_.metadata.speed.map(_.value))

                      val licenseConfidencesPerHour:Float = if(allLicenseConfidencesPerHour == Nil) 0.0f else allLicenseConfidencesPerHour.sum / licensesPerHour
                      val speedConfidencesPerHour:Float = if(allSpeedConfidencesPerHour == Nil) 0.0f else allSpeedConfidencesPerHour.sum / licensesPerHour
                      val countryConfidencesPerHour:Float = if(allCountryConfidencesPerHour == Nil) 0.0f else allCountryConfidencesPerHour.sum / licensesPerHour
                      val licensesWithSpeed:Int = allLicensesPerHour.filter(_.metadata.speed.isDefined).length
                      val averageLicensesWithSpeed:Float = calculatePercentage(licensesWithSpeed, allLicensesPerHour.length)

                      val licensesWithCategory:Int = allLicensesPerHour.filter(_.metadata.category.isDefined).length
                      val averageLicensesWithCategory:Float = calculatePercentage(licensesWithCategory, allLicensesPerHour.length)

                      val averageSpeed:Float = allSpeedsPerHour.sum / allSpeedsPerHour.length match {case x if x.isNaN => 0.0f case x => x}
                      val minSpeed:Float = if(allSpeedsPerHour == Nil) 0 else allSpeedsPerHour.min
                      val maxSpeed:Float = if(allSpeedsPerHour == Nil) 0 else allSpeedsPerHour.max

                      val averagePerHour = Average(licenseConfidence=licenseConfidencesPerHour,
                        speedConfidence = speedConfidencesPerHour,
                        countryConfidence = countryConfidencesPerHour,
                        licensesWithSpeed = licensesWithSpeed,
                        averageLicensesWithSpeed = averageLicensesWithSpeed,
                        licensesWithCategory = licensesWithCategory,
                        averageLicensesWithCategory = averageLicensesWithCategory,
                        minSpeed = minSpeed,
                        maxSpeed = maxSpeed,
                        averageSpeed = averageSpeed)

                      TimeInfo(hour = hour, vehicles=vehiclesPerHour, licenses=licensesPerHour, recognized=recognizedPerHour, average=averagePerHour, passages=passages) :: list
                    }else{
                      TimeInfo(hour = hour, vehicles=vehiclesPerHour, licenses=licensesPerHour, recognized=recognizedPerHour, average=Average(), passages=passages) :: list
                    }
                  }
              }

              CameraInfo(id = camera.data.id, vehicles=vehiclesPerCamera, licenses=licensesPerCamera, recognized=recognizedPerCamera, times=times, average=sumAverages(times.map(_.average), licensesPerCamera)) :: list
          }
        GantryInfo(id = gantry.data.id, vehicles=totalVehicles, licenses=totalLicenses, recognized=recognized, cameras=cameras, average=sumAverages(cameras.map(_.average),totalLicenses)) :: list
    }
  }

  private def calculatePercentage(x:Int, y:Int):Float={
    val result = (x.toFloat / y.toFloat) * 100
    result match {
      case x if x.isNaN => 0.0f
      case x => x
    }
  }

  private def addMinutes(date:Date, minutes:Int):Date={
    val cal = Calendar.getInstance()

    cal.setTimeInMillis(date.getTime)
    cal.add(Calendar.MINUTE, minutes)
    cal.getTime
  }

  private def sumAverages(averages:Seq[Average],totalLicenses:Int):Average={
    val total = averages.length
    val withSpeed = averages.filter(_.averageSpeed!=0.0f)
    Average(licenseConfidence = averages.map(_.licenseConfidence).sum/total,
      speedConfidence  =  averages.map(_.speedConfidence).sum/total,
      countryConfidence  = averages.map(_.countryConfidence).sum/total,
      licensesWithSpeed  = averages.map(_.licensesWithSpeed).sum,
      averageLicensesWithSpeed  = calculatePercentage(averages.map(_.licensesWithSpeed).sum.toInt, totalLicenses),
      licensesWithCategory  = averages.map(_.licensesWithCategory).sum,
      averageLicensesWithCategory  = calculatePercentage(averages.map(_.licensesWithCategory).sum.toInt, totalLicenses),
      averageSpeed  = withSpeed.map(_.averageSpeed).sum/withSpeed.length match {case x if x.isNaN => 0.0f case x => x},
      minSpeed  = averages.map(_.minSpeed).filter(_!=0.0f) match { case Nil => 0.0f case x => x.min},
      maxSpeed  = if(averages.map(_.maxSpeed).length == 0) 0.0f else averages.map(_.maxSpeed).max)
  }

  private def toCSV(report:List[GantryInfo], from:Date, to:Date, withHours:Boolean, withAverages:Boolean, withPassages:Boolean, filterOnVehicles:Boolean):String={
    val formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    val headerDate = List("from " + formatter.format(from) + " to " + formatter.format(to)).mkString(",")
    val header = List("gantry", "camera", "hour", "license", "rawLicense", "length", "category", "speed","country",  "vehicles", "licenses", "recognized", "licenseConfidence", "speedConfidence", "countryConfidence", "categoryConfidence", "lengthConfidence","licensesWithSpeed", "averageLicensesWithSpeed", "licensesWithCategory", "averageLicensesWithCategory", "minSpeed", "maxSpeed", "averageSpeed").mkString(",")

    val reports = report.foldLeft(List[String]()){
      (list, gantry) =>
        val gantryLine = List(gantry.id,
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          gantry.vehicles.toString,
          gantry.licenses.toString,
          gantry.recognized.toString,
          gantry.average.licenseConfidence.toString,
          gantry.average.speedConfidence.toString,
          gantry.average.countryConfidence.toString,
          "","",
          gantry.average.licensesWithSpeed.toString,
          gantry.average.averageLicensesWithSpeed.toString,
          gantry.average.licensesWithCategory.toString,
          gantry.average.averageLicensesWithCategory.toString,
          gantry.average.minSpeed.toString,
          gantry.average.maxSpeed.toString,
          gantry.average.averageSpeed.toString).mkString(",")


        val cameras = gantry.cameras.foldLeft(List[String]()){
          (list, camera) =>
            val cameraLine = List("",
              camera.id,
              "",
              "",
              "",
              "","","","",
              camera.vehicles.toString,
              camera.licenses.toString,
              camera.recognized.toString,
              camera.average.licenseConfidence.toString,
              camera.average.speedConfidence.toString,
              camera.average.countryConfidence.toString,
              "","",
              camera.average.licensesWithSpeed.toString,
              camera.average.averageLicensesWithSpeed.toString,
              camera.average.licensesWithCategory.toString,
              camera.average.averageLicensesWithCategory.toString,
              camera.average.minSpeed.toString,
              camera.average.maxSpeed.toString,
              camera.average.averageSpeed.toString).mkString(",")

            if(withHours){
              val times = camera.times.foldLeft(List[String]()){
                (list, time) =>
                  val timeLine = List(
                    "",
                    "",
                    time.hour.toString,
                    "",
                    "",
                    "","","","",
                    time.vehicles.toString,
                    time.licenses.toString,
                    time.recognized.toString,
                    time.average.licenseConfidence.toString,
                    time.average.speedConfidence.toString,
                    time.average.countryConfidence.toString,
                    "","",
                    time.average.licensesWithSpeed.toString,
                    time.average.averageLicensesWithSpeed.toString,
                    time.average.licensesWithCategory.toString,
                    time.average.averageLicensesWithCategory.toString,
                    time.average.minSpeed.toString,
                    time.average.maxSpeed.toString,
                    time.average.averageSpeed.toString).mkString(",")


                  val passageLines = time.passages.foldLeft(List[String]()){
                    (passages, passage) =>
                      List(
                        "",
                        "",
                        "",
                        passage.license.toString,
                        passage.rawLicense,
                        passage.length.toString,
                        passage.category.toString,
                        passage.speed.toString,
                        passage.country.toString,
                        "",
                        "",
                        "",
                        passage.confidence.license,
                        passage.confidence.speed,
                        passage.confidence.country,
                        passage.confidence.category,
                        passage.confidence.length,

                        "","","","","","","").mkString(",") :: passages
                  }

                  if(withPassages) timeLine :: passageLines ::: list else timeLine :: list

              }

              list ::: cameraLine :: times
            }else{
              list ::: cameraLine :: Nil
            }
        }
        list ::: gantryLine :: cameras
    }

    List(headerDate, header, reports.mkString("\r\n")).mkString("\r\n")
  }

  private def createAverage(average:Average):Elem={
    <average>
        <confidence speed={average.speedConfidence.toString} license={average.licenseConfidence.toString} country={average.countryConfidence.toString}/>
        <speed min={average.minSpeed.toString} max={average.maxSpeed.toString} average={average.averageSpeed.toString}/>
        <licenseSpeed licensesWithSpeed={average.licensesWithSpeed.toString} averageLicensesWithSpeed={average.averageLicensesWithSpeed.toString}/>
        <licenseCategory licensesWithCategory={average.licensesWithCategory.toString} averageLicensesWithCategory={average.averageLicensesWithCategory.toString}/>
    </average>
  }

  private def createPassagesXml(passages:Seq[PassageInfo]):scala.xml.NodeSeq={
    passages.foldLeft(List[Elem]()){
      (list, passage) =>
          <passage license={passage.license} length={passage.length.toString} rawLicense={passage.rawLicense}  category={passage.category} country={passage.country.toString} speed={passage.speed.toString} eventTimestampStr={passage.eventTimestampStr}/> :: list
    }
  }

  private def toXml(report:List[GantryInfo], from:Date, to:Date, withHours:Boolean, withAverages:Boolean, withPassages:Boolean=false, filterOnVehicles:Boolean=false):scala.xml.NodeSeq={
    val formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    <report from={formatter.format(from)} to={formatter.format(to)}>
      {
      report.foldLeft(List[Elem]()){
        (list, gantry) =>
          <gantry id={gantry.id}
                  vehicles={gantry.vehicles.toString}
                  licenses={gantry.licenses.toString}
                  recognized={gantry.recognized.toString}>
            {
            if(withAverages){
              createAverage(gantry.average)
            }
            }
            {
            gantry.cameras.foldLeft(List[Elem]()){
              (list, camera) =>
                <camera laneId={camera.id}
                        vehicles={camera.vehicles.toString}
                        licenses={camera.licenses.toString}
                        recognized={camera.recognized.toString}>
                  {
                  if(withAverages){
                    createAverage(camera.average)
                  }
                  }
                  {
                  if(withHours){
                    camera.times.foldLeft(List[Elem]()){
                      (list, time) =>
                        <time hour={time.hour.toString}
                              vehicles={time.vehicles.toString}
                              licenses={time.licenses.toString}
                              recognized={time.recognized.toString}>
                          {
                          if(withAverages){
                            createAverage(time.average)
                          }
                          }
                          {
                          if(withPassages){
                            if(filterOnVehicles)
                              createPassagesXml(time.passages.filter(_.isVehicle))
                            else
                              createPassagesXml(time.passages)
                          }
                          }
                        </time> :: list
                    }
                  }
                  }
                </camera> :: list
            }
            }
          </gantry> :: list
      }
      }
    </report>
  }
}
