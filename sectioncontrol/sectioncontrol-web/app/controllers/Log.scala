package controllers

import play.api.mvc.Controller
import common.Formats
import views.html
import models.storage.ActionType._
import services._
import models.storage.ConfigType
import play.api.i18n.Messages
import models.web.LogType

object Log extends Controller with Formats with Secured{
  /**
   * Used to retrieve logging info based on a systemId
   * @param systemId id of system
   * @param logType the type of log to view
   * @param from a long indicating start date/time of request
   * @param to a long indicating end date/time of request
   * @return retrieve logging info or an error message
   */
  def getByTime(systemId:String, logType:String, from:Long, to:Long) = IsMemberOf(systemId, SystemLog){ implicit user  => implicit request =>
    Logs.getByTime(systemId, from, to) match{
      case Right(x) => Ok(toLiftJson(toLogEntryModel(x,systemId)))
      case Left(x) => Ok(toLiftJson(x))
    }
  }

  /**
   * returns the values of enum ConfigType including local translation
   * Used by the views
   */
  def getConfigTypes:List[(String,String)] = {
    ConfigType.values.foldLeft(List[(String,String)]()){
      (list, value) =>
        val key = "logs.configType." + value.toString
        val translated = Messages(key)
        if(translated == key) list else (value.toString, translated) :: list
    }.toList.sortBy{_._2}
  }

  /**
   * returns the values of enum LogType including local translation
   * Used by the views
   */
  def getLogTypes:List[(String,String)]={
    LogType.values.foldLeft(List[(String,String)]()){
      (list, value) =>
        val key = "logs.logType." + value.toString
        val translated = Messages(key)
        if(translated == key) list else (value.toString, translated) :: list
    }.toList.sortBy{_._2}
  }
}
