/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package services

import csc.config.Path
import models.storage._
import common.{Formats, SystemLogger}
import csc.curator.utils.Versioned
import play.api.i18n.Messages
import models.web.{UserModel, ErrorType}

object Errors extends SystemLogger with Storage with Formats{
  /**
   * deletes an alarm or failure that the user has singed off end sends an system event
   * if there are no more Alerts of Failures left
   * @param systemId is id of system
   * @param errorId is id of error
   * @param errorType type of error
   * @return true is deleting the event was successful
   */
  def signOff(systemId:String, corridorId:String,errorId:String, errorType:ErrorType.Value)(implicit user:UserModel){
    var path:Option[String]=None

    val (eventType:String, messageId:String) = errorType match{
      case ErrorType.Alert =>
        path=Some(csc.sectioncontrol.storage.Paths.Alert.getCurrentPath(systemId,corridorId)+"/"+errorId)
        (ErrorSignOffType.AlertSignOff.toString, "errors.alert.signoff.success")
      case ErrorType.Failure =>
        path=Some(csc.sectioncontrol.storage.Paths.Failure.getCurrentPath(systemId,corridorId)+"/"+errorId)
        (ErrorSignOffType.FailureSignOff.toString, "errors.failure.signoff.success")
    }

    val event = Systems.getDefaultEvent(systemId).copy(
      eventType = eventType,
      corridorId=Some(corridorId),
      path = path,
      reason = getMessage(messageId, List(user.username, user.reportingOfficerId, errorId))
    )

    Systems.sendEvent(systemId, event)
  }

  private def confirmAlert(systemId:String, corridorId:String, errorId:String)(implicit user:UserModel):(Boolean,String)={
    getAlert(systemId, errorId,corridorId).map{
      error =>
        try{
          val path=csc.sectioncontrol.storage.Paths.Alert.getCurrentPath(systemId,corridorId)+"/"+ errorId
          val confirmedError = error._3.data.copy(confirmed = true)
          curator.set(path, confirmedError, error._3.version)
          (true, "")
        }catch{
          case e:Exception => (false, e.getMessage)
        }
    }.getOrElse((false, Messages("errors.no_error_found")))
  }
  
  private def confirmFailure(systemId:String, corridorId:String, errorId:String)(implicit user:UserModel):(Boolean,String)={
    getFailure(systemId, errorId,corridorId).map{
      error =>
        try{
          val path=csc.sectioncontrol.storage.Paths.Failure.getCurrentPath(systemId,corridorId)+"/"+ errorId
          val confirmedError = error._3.data.copy(confirmed = true)
          curator.set(path, confirmedError, error._3.version)
          (true, "")
        }catch{
          case e:Exception => (false, e.getMessage)
        }
    }.getOrElse((false, Messages("errors.no_error_found")))
  }

  /**
   * use confirm errors
   * @param systemId
   * @param errorId
   * @param errorType
   * @return
   */
  def confirmError(systemId:String,corridorId:String, errorId:String, errorType:ErrorType.Value)(implicit user:UserModel):Boolean={
    val reportingOfficerId = user.reportingOfficerId

    errorType match{
      case ErrorType.Alert =>
        val (isConfirmed, error) = confirmAlert(systemId,corridorId, errorId)
        if(isConfirmed){
          logWithEventType("errors.alert.confirmed.success", systemId, List(user.username, reportingOfficerId, errorId), "ConfirmedSuccess")
        }else{
          logWithEventType("errors.alert.confirmed.failed", systemId, List(user.username, reportingOfficerId, errorId, error), "ConfirmedFailed")
        }
        isConfirmed
      case ErrorType.Failure =>
        val (isConfirmed, error) = confirmFailure(systemId,corridorId, errorId)
        if(isConfirmed){
          logWithEventType("errors.failure.confirmed.success", systemId, List(user.username, reportingOfficerId, errorId), "ConfirmedSuccess")
        }else{
          logWithEventType("errors.failure.confirmed.failed", systemId, List(user.username, reportingOfficerId, errorId, error), "ConfirmedFailed")
        }
        isConfirmed
      case _ => false
    }
  }

  /**
   * retrieve a SystemFailure
   * @param systemId is id of system
   * @param errorId is id of error
   * @return an optional tuple whereby the first item is the name of the event(used as an id)
   * and the second item is VersionOf[SystemFailure]
   */
  def getFailure(systemId:String, errorId:String,corridorId:String)(implicit user:UserModel):Option[(String,String,Versioned[SystemFailure])]={
    getErrors[SystemFailure](systemId, Path(csc.sectioncontrol.storage.Paths.Failure.getCurrentPath(systemId,corridorId))).find(_._1 == errorId)
  }

  /**
   * retrieve a sequence of SystemFailure case classes
   * @param systemId is id of system
   * @return an sequence of tuples whereby the first item is the name of the event(used as an id)
   * and the second item is VersionOf[SystemFailure]
   */
  def getFailures(systemId:String)(implicit user:UserModel):Seq[(String,String,Versioned[SystemFailure])]={
    val corridors = Corridors.getAllBySystemId(systemId).map(c=>c.data.id)
    corridors.flatMap(c =>
      getErrors[SystemFailure](systemId, Path(csc.sectioncontrol.storage.Paths.Failure.getCurrentPath(systemId, c))))
  }

  case class errorId(id:String)

  /**
   * retrieve a SystemAlert
   * @param systemId is id of system
   * @param errorId is id of error
   * @return an optional tuple whereby the first item is the name of the event(used as an id)
   * and the second item is VersionOf[SystemAlert]
   */
  def getAlert(systemId:String, errorId:String,corridorId:String)(implicit user:UserModel):Option[(String,String,Versioned[SystemAlert])]={
    getErrors[SystemAlert](systemId, Path(csc.sectioncontrol.storage.Paths.Alert.getCurrentPath(systemId,corridorId))).find(_._1 == errorId)
  }

  /**
   * retrieve a sequence of SystemAlert case classes
   * @param systemId is id of system
   * @return an sequence of tuples whereby the first item is the name of the event(used as an id)
   * and the second item is VersionOf[SystemAlert]
   */
  def getAlerts(systemId:String)(implicit user:UserModel):Seq[(String,String,Versioned[SystemAlert])]={
    val corridors = Corridors.getAllBySystemId(systemId).map(c=>c.data.id)
    val x=corridors.map(c=>
      getErrors[SystemAlert](systemId, Path(csc.sectioncontrol.storage.Paths.Alert.getCurrentPath(systemId,c))))
    x.flatten
  }

  /**
   * used to return the path of an given errorId. The errorId is actually the event name that has
   * start with the prefix "alert" or "failure". The prefix determines what path is returned
   * @param systemId id of system
   * @param path to an alert or an failure leaf
   * @return an sequence of tuples whereby the first item is the name of the event(used as an id)
   * and the second item is VersionOf[T]
   */
  private def getErrors[T<:AnyRef](systemId:String, path:Path)(implicit m : Manifest[T]):Seq[(String,String,Versioned[T])]={
    if(curator.exists(path)){
      val errors = curator.getChildren(path)
      errors.foldLeft(List[(String,String,Versioned[T])]()){
        (list, pathToError) =>
          curator.getVersioned[T](pathToError).map{
            error =>
              val x=pathToError.toString().split("/")
              val errorId = pathToError.nodes.reverse.head.name
              (errorId, x(5),error) :: list
          }.getOrElse{
            list
          }
      }
    }else{
      List()
    }
  }
}