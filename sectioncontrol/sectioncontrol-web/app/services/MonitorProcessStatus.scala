/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package services

import common.{Formats, SystemLogger, Validates}
import csc.sectioncontrol.storage.{SystemCorridorCompletionStatus, SystemLaneCompletionStatus}
import csc.sectioncontrol.classify.nl.ClassificationCompletionStatus
import math.{max,min}
import models.web.{UserModel, ComponentProcessedUntil, MonitorProcessUntil}

/**
 * Used for retrieving Process status of systems
 */
object MonitorProcessStatus extends Formats with SystemLogger with Storage{

  def getPreSelectorCompletionStatusPath(systemId: String) = Systems.getRootPath(systemId) / "jobs" / "preSelectorCompletionStatus"
  def getMatchCompletionStatusPath(systemId: String) = Systems.getRootPath(systemId) / "jobs" / "matchCompletionStatus"
  def getClassificationCompletionStatusPath(systemId: String) = Systems.getRootPath(systemId) / "jobs" / "classificationCompletionStatus"

  def getAllSystemProcessUntilStatus():Seq[MonitorProcessUntil] = {
    Systems.getAll.foldLeft(Seq[MonitorProcessUntil]()){
      (list, system) => list :+ getSystemProcessUntilStatus(system.data.id, system.data.name)
    }
  }

  def getSystemProcessUntilStatus(implicit user:UserModel):Seq[MonitorProcessUntil] = {
    Systems.findByIds(user.sectionIds).foldLeft(List[MonitorProcessUntil]()){
      (list, system) => getSystemProcessUntilStatus(system.data.id, system.data.name) :: list
    }
  }

  def formatDateTime(time:Long): String ={
    formatDateToCEST(time,"EEE MMM dd yyyy HH:mm:ss.SSS")
  }

  def getSystemProcessUntilStatus(systemId:String, systemName:String):MonitorProcessUntil = {
    //get preselector data
    val currentTime = System.currentTimeMillis()
    val statusPreselect = curator.get[SystemLaneCompletionStatus](getPreSelectorCompletionStatusPath(systemId)).map(stat => {
      val details = stat.lanes.map { case (laneId, status) => {
        new ComponentProcessedUntil(
          step = laneId,
          lastUpdated = formatDateTime(status.lastUpdateTime),
          processedUntil = formatDateTime(status.completeUntil),
          details = List())
      }}
      val (lastUpdated:Long, processedUntil:Long) = stat.lanes.foldLeft(0L,currentTime) {
        case ((last,until), comp) => {
          (max(last, comp._2.lastUpdateTime),min(until, comp._2.completeUntil))
        }
      }
      new ComponentProcessedUntil(
        step = "Preselector",
        lastUpdated = formatDateTime(lastUpdated),
        processedUntil = formatDateTime(processedUntil),
        details = details.toList)
    })

    //get vehiclestat data
    val statusVehicleStat = curator.get[SystemCorridorCompletionStatus](getMatchCompletionStatusPath(systemId)).map(stat => {
      val details = stat.corridors.map { case (corridorId, status) => {
        new ComponentProcessedUntil(
          step = corridorId,
          lastUpdated = formatDateTime(status.lastUpdateTime),
          processedUntil = formatDateTime(status.completeUntil),
          details = List())
      }}
      val (lastUpdated: Long, processedUntil:Long) = stat.corridors.foldLeft(0L,currentTime) {
        case ((last,until), comp) => {
          (max(last, comp._2.lastUpdateTime),min(until, comp._2.completeUntil))
        }
      }
      new ComponentProcessedUntil(
        step = "VehicleStat",
        lastUpdated = formatDateTime(lastUpdated),
        processedUntil = formatDateTime(processedUntil),
        details = details.toList)
    })
    //get classification data
    val statusClassify = curator.get[ClassificationCompletionStatus](getClassificationCompletionStatusPath(systemId)).map(stat => {
      new ComponentProcessedUntil(
        step = "Classify",
        lastUpdated = formatDateTime(stat.lastUpdateTime),
        processedUntil = formatDateTime(stat.completeUntil),
        details = List())
    })
    //create result
    new MonitorProcessUntil(systemId = systemId, systemName = systemName, components = List() ++ statusPreselect ++ statusVehicleStat++ statusClassify)
  }

  def getDetails(systemId:String, compName:String):List[ComponentProcessedUntil] = {
    getSystemProcessUntilStatus(systemId,"").components.find(_.step == compName).map(_.details).getOrElse(List())
  }
}
