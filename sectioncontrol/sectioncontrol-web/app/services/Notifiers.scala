package services

import common.{SystemLogger, Validates}
import csc.config.Path
import models.storage.CaseFile._
import models.storage.{SmsByEmailNotifierEvents, SmsByEmailNotifierConfig, SystemSmsByEmailNotifierConfig, NotificationStateConfig}


object Notifiers extends Validates with SystemLogger with Storage{

  def rootPath(systemId:String) = Systems.getRootPath(systemId) / "notifications"

  def notifierPath(systemId:String, notifierId:String) = Systems.getRootPath(systemId) / "notifications" / notifierId / "config"

  private def smsByEmailNotifier = "SmsByEmail"

  def createDefaultsPerSystem(systemId:String){
    val pathConfig = Notifiers.notifierPath(systemId, smsByEmailNotifier)

    if(!curator.exists(pathConfig)){
      curator.put(pathConfig, SystemSmsByEmailNotifierConfig(phoneNumbers = Nil))
    }
  }

  def createDefaults(path:Path){
    val pathState = path / "notification" / "state"
    val pathConfig = path / "notification" / "notifiers" / smsByEmailNotifier / "config"
    val pathEvents = path / "notification" / "notifiers" / smsByEmailNotifier / "events"

    if(!curator.exists(pathState)){
      curator.put(pathState, NotificationStateConfig(true))
    }

    if(!curator.exists(pathConfig)){
      curator.put(pathConfig, SmsByEmailNotifierConfig(
        emailSubject = Some("EG100 Notificatie"),
        emailBodyTemplate = "<hhm-id>:<reason>:<location>:<date-time>",
        fromEmailAddress = "fromEmailAddress",
        smsGatewayDomain = "mail-sms.nl",
        smtpHost = "172.16.0.227",
        smtpPort = Some(25),
        smtpUserName = Some("smtpUserName"),
        smtpPassword = Some("smtpPassword"),
        smtpProperties = Some(Map("key" -> "value"))
      ))
    }

    if(!curator.exists(pathEvents)){
      curator.put(pathEvents, SmsByEmailNotifierEvents(events = Seq()))
    }
  }
  
  def update(systemId:String, phoneNumbers: Seq[String]){
    val path = Notifiers.notifierPath(systemId, smsByEmailNotifier)

    createDefaultsPerSystem(systemId)

    curator.getVersioned[SystemSmsByEmailNotifierConfig](path).map{
      notifier => 
        val newNumbers:Seq[String] = phoneNumbers.filter(_.trim().length() > 0)
        val updatedNotifier = notifier.data.copy(phoneNumbers = newNumbers)
        curator.set(path, updatedNotifier, notifier.version)
    }
  }
  
  def getPhoneNumbersBySystemId(systemId:String):Seq[String]={
    val path = Notifiers.notifierPath(systemId, smsByEmailNotifier)
    curator
      .get[SystemSmsByEmailNotifierConfig](path)
      .map(_.phoneNumbers)
      .getOrElse(Nil)
  }
}
