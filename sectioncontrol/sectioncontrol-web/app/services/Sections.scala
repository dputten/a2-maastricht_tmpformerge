/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package services

import common.Extensions._
import models.web._
import models.storage._
import common.SystemLogger
import csc.curator.utils.Versioned
import play.api.i18n.Messages

/**
 *Used for CRUD actions on Gantry objects
 */
object Sections extends SystemLogger with Storage{

 /**
   * return de default path where all sections reside
   * @param systemId is id of system
   * @return default Path to Sections
   */
  def getDefaultPath(systemId:String) = Systems.getDefaultPath / systemId / "sections"

  /**
   * return de root path where a section reside
   * @param systemId is id of system
   * @param sectionId is id of section
   * @return Path to Sections
   */
  def getRootPath(systemId:String, sectionId:String) = getDefaultPath(systemId) / sectionId

  /**
   * return the path of the config leaf
   * @param systemId is id of system
   * @param sectionId is id of section
   * @return path of the config leaf
   */
  def getConfigPath(systemId:String, sectionId:String) = getRootPath(systemId, sectionId) / "config"

  /**
   * checks of a given sectionId and systemId exists
   * @param systemId is id of system
   * @param sectionId is id of section
   * @return Boolean that indicates if a config path exists
   */
  def exists(systemId:String, sectionId:String) = curator.exists(getRootPath(systemId, sectionId))

  /**
   * retrieve config from storage
   * @param systemId is id of system
   * @param sectionId is id of section
   * @return config from storage
   */
  def getConfig(systemId:String, sectionId:String) = curator.getVersioned[Section](getConfigPath(systemId, sectionId))

  /**
   * retrieve a Section based on given id
   * @param systemId is id of system
   * @param sectionId is id of section
   * @return section from storage
   */
  def findById(systemId:String, sectionId:String) = getConfig(systemId, sectionId)


  /**
   * retrieve all systems that have lanes not connected to a section
   */
  def getAvailableSystems(implicit user:UserModel):Seq[Versioned[System]]={
    val systemIds:List[String] = Systems.getAll.foldLeft(List[String]()){
      (list, system) =>
        if(Systems.isEditable(system.data.id)){
          Sections.getAvailableStartGantries(system.data.id).map{f => f.data.systemId}.toList ::: list
        }else{
          list
        }
    }

    Systems.findByIds(systemIds.toSet.toSeq).sortBy(_.data.name)
  }

  /**
   * retrieve all gantries for a system that;
   * - are defined as an endGantryId
   * - not defined as a startGantryId
   * - if no sections are configured return all gantries
   * @param systemId is id of system
   * @return all gantries for a system that have lanes
   */
  def getAvailableStartGantries(systemId:String)(implicit user:UserModel):Seq[Versioned[Gantry]]={
    val allSystemSections = Sections.getAllBySystemId(systemId)
    val gantryStartIds = allSystemSections.map{_.data.startGantryId}
    val gantryEndIds = allSystemSections.map{_.data.endGantryId}

    //end gantries that are not also not start gantries
    val filteredGantries = gantryEndIds.filterNot(id => gantryStartIds.contains(id))

    //if no sections are creates show all gantries as start gantries
    val startGantries:Seq[String] = if(filteredGantries.size == 0)
                          Gantries.getBySystemId(systemId).map(_.data.id)
                         else filteredGantries

    //only show gantries that have a lane
    startGantries.foldLeft(List[Versioned[Gantry]]()){
      (list, gantryId) =>
        val lanes = Lanes.getAllByGantryId(systemId, gantryId)
        val gantry = Gantries.findById(systemId, gantryId)
        if(lanes.length > 0)
          gantry.map(_::list).getOrElse(list)
        else list
    }.sortBy(_.data.name)
  }

  /**
   * determine is the given sectionId is the last one (exit) of all sections of a given system
   * @param systemId id of system
   * @param sectionId id of section
   */
  def isLastInChain(systemId:String, sectionId:String):Boolean={
    val lastGantryId = Sections.findById(systemId, sectionId).get.data.endGantryId
    val allSections = Sections.getAllBySystemId(systemId)
    val startGantries = allSections.map(_.data.startGantryId)

    !startGantries.contains(lastGantryId) || allSections.length == 1
  }

  /**
   * determine is the given sectionId is the first one (entry) of all sections of a given system
   * @param systemId id of system
   * @param sectionId id of section
   */
  def isStartInChain(systemId:String, sectionId:String):Boolean={
    val startGantryId = Sections.findById(systemId, sectionId).get.data.startGantryId
    val allSections = Sections.getAllBySystemId(systemId)
    val endGantries = allSections.map(_.data.endGantryId)

    !endGantries.contains(startGantryId)
  }
  
  /**
   * retrieve all gantries that are part of all sections within a given system
   * @param systemId id of system
   */
   def getAllGantryIds(systemId:String):Seq[String]={
    val allSections = Sections.getAllBySystemId(systemId)
    val allGantries:Seq[String] = allSections.map(_.data.startGantryId) ++ allSections.map(_.data.endGantryId)

    allGantries.toSet.toSeq
  }
  
  /**
   * retrieve all available last gantries for a system that have lanes
   * the last gantries can only be the ones that are not already part of and
   * section last gantry id and including the given gantry id
   * @param systemId is id of system
   * @return all gantries for a system that have lanes
   */
  def getAvailableLastGantries(systemId:String, gantryId:String)(implicit user:UserModel):Seq[Versioned[Gantry]]={
    val allSectionsGantries:Seq[String] = (gantryId :: Nil ++ Sections.getAllGantryIds(systemId)).toSet.toSeq
    val allSystemGantries = Gantries.getBySystemId(systemId).toSet.toSeq

    allSystemGantries
      .filterNot(gantry => allSectionsGantries.contains(gantry.data.id))
      .sortBy(_.data.name)
  }

  /**
   * return all sections of a given systemId
   * @param systemId is id of system
   * @return all gantries for a system that have lanes
   */
  def getAllBySystemId(systemId:String):Seq[Versioned[Section]]={
    if(curator.exists(Sections.getDefaultPath(systemId))){
      val allSectionPaths = curator.getChildren(getDefaultPath(systemId))
      allSectionPaths.foldLeft(List[Versioned[Section]]()){
        (list, sectionPath) =>
          curator.getVersioned[Section](sectionPath / "config").map{
            path =>
              path :: list
          }.getOrElse{
            list
          }
      }
    }else{
      List()
    }
  }

  /**
   * delete a Section from zookeeper
   * @param systemId is id of system
   * @param sectionId is id of section
   * @return returns true is deleting was successful. false if failed or by version conflict
   */
  def delete(systemId:String, sectionId:String, version:Long)(implicit user:UserModel):Boolean={
    Sections.findById(systemId, sectionId).map{
      section =>
        if(version == section.version){
          log(
            messageId="logging.sections.delete",
            systemId=systemId, List(sectionId))
          curator.deleteRecursive(getRootPath(systemId, sectionId))
          Systems.updateHash(systemId)
          true
        }else{
          log(
            messageId="logging.sections.delete.failed",
            systemId=systemId, List(sectionId))
          false
        }
    }.getOrElse(false)
  }

  /**
   * validates the given model
   * @param systemId is id of system
   * @param model what will be validated
   * @return list of field errors
   */
  private def validate(systemId:String, model:SectionCRUD):List[FieldError]={
    var errors = List[FieldError]()

    val hasEmptyValues:Boolean = model.matrixBoards.toOption.exists(
        _.replaceAll("""(?m)\s+$""", "") //remove all whitespace
          .split(",")
          .toSeq
          .exists(_.toOption.isEmpty)
    )

    if(hasEmptyValues){
      errors ::= FieldError(field="matrixBoards", error=Messages("sections.validate.matrixBoards_has_empty_values"))
    }

    if(model.name == null || model.name.trim.length < 1 || model.name.trim.length > 6 ){
      errors ::= FieldError(field="name", error=Messages("sections.validate.name_is_not_correct_length"))
    }

    if(model.length <= 0){
    //if(model.length < 0){ //eg100 is OK to have 0 as length
      errors ::= FieldError(field="length", error=Messages("sections.validate.length"))
    }

    if(systemId == null || systemId.trim().length() == 0 || !Systems.exists(systemId)){
      errors ::= FieldError(field="systemId", error=Messages("validate.system_does_not_exist"))
    }

    if(!Gantries.exists(systemId, model.endGantryId)){
      errors ::= FieldError(field="systemId,endGantryId", error=Messages("sections.validate.systemId_and_endGantryId_not_exist"))
    }

    if(model.startGantryId == model.endGantryId){
      errors ::= FieldError(field="gantryId", error=Messages("sections.validate.gantryids_are_the_same"))
    }

    errors
  }

  /**
   * updates a section
   * @param systemId id of system
   * @param sectionId id of section
   * @param model the model to update
   * @return
   */
  def update(systemId:String, sectionId:String, model:SectionCRUD)(implicit user:UserModel):ResponseMsg={
    var errors = validate(systemId, model)

    if(sectionId == null || sectionId.trim().length() == 0 || !Sections.exists(systemId, sectionId)){
      errors ::= FieldError(field="sectionId", error=Messages("sections.validate.section_does_not_exist"))
    }

    if(model.version.isEmpty){
      errors ::= FieldError(field="version", error=Messages("sections.validate.no_version_received"))
    }

    if(errors.length > 0){
      ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors.toList.distinct)
    }else{
      val version = model.version.get
      // have to make sure that an empty sequence is created otherwise lift-json will create
      // "matrixBoards":[""] instead of "matrixBoards":[]
      val matrixBoards:Seq[String] = if(model.matrixBoards.trim.size > 0)
        model.matrixBoards.trim.split(",")
      else List()
      try{
        Sections.findById(systemId, sectionId).foreach{
          section =>
            curator.set(
              Sections.getConfigPath(systemId, sectionId),
              section.data.copy(name=model.name,startGantryId=model.startGantryId, endGantryId=model.endGantryId,  description=model.description, matrixBoards=matrixBoards,length = model.length),
              version
            )
            Systems.updateHash(systemId)

            log(
              messageId="logging.sections.update",
              systemId=systemId, List(sectionId))
        }

        ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.update"))
      }catch{
        case e:Exception =>
          log(
            messageId="logging.sections.update.failed",
            systemId=systemId, List(sectionId))
          ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.success.failed"))
      }
    }
  }

  /**
   * Creates a new Gantry
   * @param systemId is id of system
   * @param model what will be validated
   * @return ResponseMsg indicating if create a new Section was successful
   */
  def create(systemId:String, model:SectionCRUD)(implicit user:UserModel):ResponseMsg={
    if(model.name == null || model.name.trim().length() == 0){
      val error = FieldError(field="name", error=Messages("sections.validate.name_is_empty"))
      return ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=Seq(error))
    }

    var errors = validate(systemId, model)
    val sectionKey = asConfigKey(model.name)

    if(Sections.exists(systemId, sectionKey)){
      errors ::= FieldError(field="name", error=Messages("sections.validate.name_is_taken"))
    }

    //Gantry can only be startGantry once
    //Gantry can only be endGantry once
    if(curator.exists(getDefaultPath(systemId))){
      val allSectionPaths = curator.getChildren(getDefaultPath(systemId))
      allSectionPaths.foreach{ sectionPath =>
        val config = curator.getVersioned[Section](sectionPath / "config")
        config.foreach{
          con =>
            if(con.data.startGantryId == model.startGantryId){
              errors ::= FieldError(field="startGantryId", error=Messages("sections.validate.startGantryId_already_as_first_section"))
            }
            if(con.data.endGantryId == model.endGantryId){
              errors ::= FieldError(field="endGantryId", error=Messages("sections.validate.endGantryId_already_as_end_section"))
            }
            if(con.data.name == model.endGantryId){
              errors ::= FieldError(field="endGantryId", error=Messages("sections.validate.endGantryId_already_as_end_section"))
            }
        }
      }
    }

    if(errors.length > 0){
      ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors.toList.distinct)
    }else{

      // have to make sure that an empty sequence is created otherwise lift-json will create
      // "matrixBoards":[""] instead of "matrixBoards":[]
      val matrixBoards:Seq[String] = if(model.matrixBoards.trim.size > 0)
                                        model.matrixBoards.trim.split(",")
                                      else List()

      val newSection = Section(
        id = sectionKey,
        systemId = systemId,
        name = model.name,
        description = model.description,
        length = model.length,
        startGantryId = model.startGantryId,
        endGantryId=model.endGantryId,
        matrixBoards = matrixBoards,
        msiBlackList = List())

      try{
        val sectionPath = Sections.getDefaultPath(systemId) / sectionKey
        curator.put(sectionPath / "config", newSection)
        Systems.updateHash(systemId)
        log(
          messageId="logging.sections.create",
          systemId=systemId, List(sectionKey))
        ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.insert"))
      }catch{
        case e:Exception =>
          log(
            messageId="logging.sections.create.failed",
            systemId=systemId, List(sectionKey))
          ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.success.failed"))
      }
    }
  }
}
