/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package services

import java.util.UUID
import models.web._
import models.storage._
import common.{SystemLogger}
import csc.curator.utils.Versioned
import play.api.i18n.Messages

/**
 *Used for CRUD actions on Role objects
 */
object Roles extends SystemLogger with Storage{

  /**
   * retrieve all Role object from zookeeper
   */
  def getAll:Seq[RoleModel]={
    curator.getChildren(Paths.Role.default).foldLeft(List[RoleModel]()){
      (list, path) =>
        curator
          .getVersioned[Role](path)
          .map(toModel(_) :: list)
          .getOrElse(list)
    }
  }

  /**
   * retrieve a RoleModel based on given id
   */
  def findById(id:String):Option[RoleModel]={
    curator.getVersioned[Role](Paths.Role.default / id).map(toModel(_))
  }

  /**
   * retrieve a RoleModel based on name
   */
  def findByName(name:String):Option[RoleModel] = Roles.getAll.find(_.name == name)
  
  /**
   * creates/updates a Role
   */
  def createOrUpdate(roleModel:UpdateRoleModel)(implicit user:UserModel):Either[RoleModel, ResponseMsg]={
    var errors = List[FieldError]()

    roleModel.errors match{
      case Some(x) => //role contains errors
        Right(ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=x.toList))
      case _ => //no role errors found
        val currentRole = Roles.findByName(roleModel.name)
        val id:Option[String] = Some(roleModel.id).filter(_.trim.nonEmpty)

        val officeridRequiredForRole= roleModel.actionIds.map(actionId=> PermittedActions.getAll.find(p=>p.id==actionId && p.reportingofficeridRequired==true)).filter(_ != None).length>0
        if(officeridRequiredForRole)
        {
          val users: Seq[UserModel] =Users.getAll
          val cannotAddAction: Boolean =users.filter(user=>user.roleId==id.get && (user.reportingOfficerId==null ||user.reportingOfficerId.isEmpty)).length>0
          if(cannotAddAction)
          {
            errors = FieldError(field="action-id", value="", error=Messages("responsemsg.roles.action_not_allowed")) :: errors
          }
        }

        roleModel match {
          //new role, name is NOT unique
          case u:RoleModel if id.isEmpty && currentRole.isDefined =>
            errors = FieldError(field="name", value="", error=Messages("responsemsg.roles.name_already_exists")) :: errors

          //existing role, name is NOT unique
          case u:RoleModel if id.isDefined && currentRole.isDefined && currentRole.get.id != id.get =>
            errors = FieldError(field="name", value="", error=Messages("responsemsg.roles.name_already_exists")) :: errors
          //disregard
          case _ =>
        }
        (errors.length > 0)  match {
          //create/role user
          case false =>

            val newRole = Role(
              id = if(id.isEmpty) UUID.randomUUID.toString else id.get,
              name = roleModel.name,
              description = roleModel.description,
              permittedActions = PermittedActions.getAll.filter{cc=> roleModel.actionIds.contains(cc.id)}.toList)

            id match {
              case Some(x) =>
                curator.set(Paths.Role.default / newRole.id, newRole, roleModel.version)
                //logMultipleSystemIds(messageId="logging.roles.update", systemIds=Systems.getAll.map(_.data.id), args=List(newRole.name))
              case _ =>
                curator.put(Paths.Role.default / newRole.id, newRole)
                //logMultipleSystemIds(messageId="logging.roles.create", systemIds=Systems.getAll.map(_.data.id), args=List(newRole.name))
            }
            Left(Roles.findById(newRole.id).get)
          case true =>
            Right(ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors.toList))
        }
    }
  }

  def deleteOne(id: String, version: Long): Boolean = {
    findById(id) match {
      case Some(c) =>
        curator.delete(Paths.Role.default / id)
        true
      case _ => false
    }
  }

  /**
   * deletes a role
   */
  def delete(id:String, version:Long)(implicit user:UserModel):Boolean={
    findById(id) match{
      case Some(c) => if(c.numberOfPeople > 0){
        false
      } else{
        curator.delete(Paths.Role.default / id)
        true
      }
      case _ => false
    }
  }

  /**
   * used to create a RoleModel that represents a Role object stored in zookeeper
   */
 def toModel(versionOf:Versioned[Role]):RoleModel={
    RoleModel(
      id = versionOf.data.id,
      name = versionOf.data.name,
      description = versionOf.data.description,
      numberOfPeople = numberOfPeople(versionOf.data.id),
      version = versionOf.version,
      actionIds = versionOf.data.permittedActions.map{_.id}.toList,
      permittedActions = versionOf.data.permittedActions)
  }
  
  def numberOfPeople(roleId:String):Int={
    val allUsers = curator.getChildren(Paths.User.default).foldLeft(List[User]()){
      (list, path) =>
        curator
          .get[User](path)
          .map(_ :: list)
          .getOrElse(list)
    }

    allUsers.filter(_.roleId == roleId).length
  }
}
