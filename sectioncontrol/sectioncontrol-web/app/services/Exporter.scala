/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package services

import common.Extensions._
import csc.config.Path
import xml.{PCData, Elem}
import common.Config

/**
 * exports configuration
 */
object Exporter extends Config with Storage{

  /**
   *
   * @param path
   * @param name
   * @param data
   */
  case class ZNode(path:Path, name:String, data:Option[String]){
    def hasData = data.isDefined
    def parent = path.parent
  }

  /**
   * Used to export all nodes and its data
   * @param path to export (including all its children and data)
   * @return xml with the complete export of the given path
   */
  def exportConfig(path:Path):Elem={
    val xml = getTree(path).foldLeft(List[Elem]()){
      (list, zNode) =>
        <znode>
          <path>{zNode.path.toString()}</path>
          <data>{PCData(zNode.data.getOrElse(""))}</data>
        </znode> :: list
    }

    <export version={Config.version}>
      <export-path>{path.toString()}</export-path>
      <znodes>
        {xml}
      </znodes>
    </export>
  }

  /**
   * retrieves a zookeeper tree structure of a given begin path
   * @param path
   * @return
   */
  def getTree(path:Path):List[ZNode]={
    def getAllPaths(path:Path):List[Path]={
      var paths = List[Path]()
      def loop(path:Path){
        curator.getChildren(path).map{p =>
          paths ::= p
          loop(p)
        }
      }
      loop(path)
      paths.sortBy(_.toString)
    }

    def createZNode(path:Path):ZNode={
      val data = curator.getAsString(path) match {
        case "" | null => None
        case x if x.length == 0 => None
        case d => Some(d)
      }

      ZNode(path, path.nodes.last.name, data)
    }

    getAllPaths(path).map(createZNode(_))
  }
}
