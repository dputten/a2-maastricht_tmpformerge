/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package services


import csc.config.{Path}
import common.Extensions._
import org.apache.zookeeper.KeeperException.BadVersionException
import models.web._
import models.storage._
import common.{SystemLogger, Validates}
import csc.curator.utils.Versioned
import play.api.i18n.Messages

/**
 *Used for CRUD actions on Gantry objects
 */
object Gantries extends Validates with SystemLogger with Storage{

  /**
   * retrieve config from zookeeper
   * @param systemId is id of system
   * @param gantryId is id of gantry
   * @return optional VersionOf[Gantry]
   */
  def getConfig(systemId:String, gantryId:String):Option[Versioned[Gantry]]={
    curator.getVersioned[Gantry](getConfigPath(systemId, gantryId))
  }

  /**
   * return de default path where all gantries reside
   * @param systemId is id of system
   * @return Path to Gantries
   */
  def getDefaultPath(systemId:String):Path = Systems.getDefaultPath / systemId / "gantries"

  /**
   * checks of a given gantryI and systemId exists
   * @param systemId is id of system
   * @param gantryId is id of gantry
   * @return Boolean that indicates if a Gantry path exists
   */
  def exists(systemId:String, gantryId:String)=curator.exists(Systems.getDefaultPath / systemId / "gantries" / gantryId )

  /**
   * return the path of the config leaf
   * @param systemId is id of system
   * @param gantryId is id of gantry
   * @return Path to specific gantry config leaf
   */
  def getConfigPath(systemId:String, gantryId:String):Path = Gantries.getDefaultPath(systemId) / gantryId / "config"

  /**
   * returns the current path where of a given gantryId
   * @param systemId is id of system
   * @param gantryId is id of gantry
   * @return the current path where of a given gantryId
   */
  def getCurrentGantryPath(systemId:String, gantryId:String):Path = Gantries.getDefaultPath(systemId) / gantryId


  /**
   * returns all gantries within a given systemId
   * @param systemId is id of system
   * @return sequence of VersionOf[Gantry]
   */
  def getBySystemId(systemId:String)(implicit user:UserModel):Seq[Versioned[Gantry]]={
    var gantries = List[Versioned[Gantry]]()
    val allSystemPaths = Systems.getAllPaths.filter{p => p.nodes.reverse.head.name == systemId}

    allSystemPaths.foreach{ systemPath =>
      val pathToGantries = systemPath / "gantries"
      if(curator.exists(pathToGantries)){
        val allGantryPaths = curator.getChildren(pathToGantries)
        allGantryPaths.foreach{ gantryPath =>
          val versionOfGantry = curator.getVersioned[Gantry](gantryPath / "config")
          if(versionOfGantry.isDefined)
            gantries ::= versionOfGantry.get
        }
      }
    }
    gantries
  }


  /**
   * retrieve a GantryModel based on given id
   * @param systemId is id of system
   * @param gantryId is id of gantry
   * @return optional VersionOf[Gantry]
   */
  def findById(systemId:String, gantryId:String)(implicit user:UserModel):Option[Versioned[Gantry]]={
    getConfig(systemId, gantryId)
  }

  /**
   * delete a Gantry from zookeeper
   * @param systemId is id of system
   * @param gantryId is id of gantry
   * @param version of the gantry to delete
   * @return true is deletion was successful else false
   */
  def delete(systemId:String, gantryId:String, version:Long)(implicit user:UserModel):Boolean={
    val path = getCurrentGantryPath(systemId,gantryId)

    if(curator.exists(path)){
      log(messageId="logging.gantries.delete", systemId=systemId, List(gantryId))
      curator.deleteRecursive(path)
      true
    }else{
      false
    }
  }

  /**
   * updates a Gantry
   * @param systemId is id of system
   * @param gantryId is id of gantry
   * @param model to update a gantry
   * @return a ResponseMsg indicate if updating a gantry was successful
   */
  def update(systemId:String, gantryId:String, model:GantryCRUD)(implicit user:UserModel):ResponseMsg={
    var errors = List[FieldError]()

    notEmpty(value=model.hectometer, fieldKey="hectometer", errorKey="gantries.validate.hectometer_is_required").map{errors ::= _ }
    betweenRange(value=model.hectometer.length, from=1, to=6, fieldKey=Some("hectometer")).map{errors ::= _ }

    gantryNameMustExist(model.name).map{errors ::= _ }
    systemMustExist(systemId).map{errors ::= _ }
    gantryMustExist(systemId, gantryId).map{errors ::= _ }

    if(errors.length > 0){
      ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors.toList)
    }else{
      val gantry = getConfig(systemId, gantryId)
      gantry match{
        case Some(versionOf) =>
          try{
            val updatedGantry = versionOf.data.copy(
              name = model.name,
              hectometer = model.hectometer,
              longitude = model.longitude,
              latitude = model.latitude)
            curator.set(getCurrentGantryPath(systemId,gantryId) / "config", updatedGantry, model.version.getOrElse(0))
            //log(messageId="logging.gantries.update", systemId=systemId, List(gantryId))
            ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.update"))
          }catch{
            case e:BadVersionException =>
              ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.failed.update_version_conflict"))
            case e:Exception =>
              ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.failed.update"))
          }
        case  _ =>
          ResponseMsg(typeName=ResponseType.System, description=Messages("No gantry found"))
      }
    }
  }

  /**
   * Creates a new Gantry
   * @param systemId is id of system
   * @param model to create a gantry
   * @return a ResponseMsg indicate if creating a gantry was successful
   */
  def create(systemId:String, model:GantryCRUD)(implicit user:UserModel):ResponseMsg={
    var errors = List[FieldError]()

    notEmpty(value=model.id.getOrElse(""), fieldKey="gantryId", errorKey="gantries.validate.gantryId_is_required").map{errors ::= _ }
    notEmpty(value=model.hectometer, fieldKey="hectometer", errorKey="gantries.validate.hectometer_is_required").map{errors ::= _ }
    betweenRange(value=model.hectometer.length, from=1, to=6, fieldKey=Some("hectometer")).map{errors ::= _ }

    gantryNameMustExist(model.name).map{errors ::= _ }
    systemMustExist(systemId).map{errors ::= _ }

    if(errors.length > 0){
      return ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors.toList)
    }

    gantryMustNotExist(systemId, model.id.get).map{errors ::= _ }
    //gantryMustNotExist(systemId, model.name).map{errors ::= _ }

    if(errors.length > 0){
      return ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors.toList)
    }else{
      val gantryKey = asConfigKey(model.id.get)
      val newGantry = Gantry(
        id = gantryKey,
        systemId = systemId,
        name = model.name,
        hectometer = model.hectometer,
        longitude = model.longitude,
        latitude = model.latitude)

      try{
        val gantryPath = Gantries.getDefaultPath(systemId) / gantryKey
        curator.put(gantryPath / "config", newGantry)
        log(messageId="logging.gantries.create", systemId=systemId, List(gantryKey))
        return ResponseMsg(success=true, id=gantryKey, typeName=ResponseType.System, description=Messages("responsemsg.success.insert"))
      }catch{
        case e:Exception =>
          return ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.success.failed"))
      }
    }
  }

  /**
   * check if the name of a new gantry is not empty
   * @param gantryName
   * @return an optional FieldError
   */
  private def gantryNameMustExist(gantryName:String):Option[FieldError]={
    Some(gantryName).filter(_.trim.nonEmpty) match {
      case Some(x) => None
      case _ => Some(FieldError(field="name", error=Messages("gantries.validate.name_is_empty")))
    }
  }

  /**
   * check if a given gantryId exists
   * @param systemId is id of system
   * @param gantryId is id of gantry
   * @return an optional FieldError
   */
  private def gantryMustExist(systemId:String, gantryId:String):Option[FieldError]={
    Gantries.exists(systemId, gantryId) match {
      case true => None
      case _ => Some(FieldError(error=Messages("gantries.validate.id_cannot_be_changed")))
    }
  }

  /**
   * checks that the given systemId exists
   * @param systemId is id of system
   * @return an optional FieldError
   */
  private def systemMustExist(systemId:String):Option[FieldError]={
    Systems.exists(systemId) match {
      case true =>  None
      case _ =>  Some(FieldError(error=Messages("gantries.validate.system_does_not_exist")))
    }
  }

  /**
   * checks that the given gantryId does not exist
   * @param systemId is id of system
   * @param gantryId is id of gantry
   * @return an optional FieldError
   */
  private def gantryMustNotExist(systemId:String, gantryId:String):Option[FieldError]={
    val gantryKey = asConfigKey(gantryId)
    Gantries.exists(systemId, gantryKey) match {
      case true =>
        Some(FieldError(field="name", error=Messages("gantries.validate.name_already_exists")))
      case _ => None
    }
  }
}
