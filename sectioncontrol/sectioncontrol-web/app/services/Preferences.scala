/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package services

import models.web._
import models.storage._
import common.{SystemLogger, Validates}
import csc.curator.utils.Versioned
import play.api.i18n.Messages
import csc.config.Path

/**
 * Used for CRUD actions on Preference objects
 */
object Preferences extends Validates with SystemLogger with Storage{
import SystemStateType._

  private val marginSpeedLimits:Seq[Int] = for(i <-20 to 130 by 10) yield i

  val defaultPreference = Preference(
    wheelBase = ZkWheelbaseType.values.map{wheelBaseType => WheelBase(wheelBaseType = wheelBaseType, value = 0.0)}.toList,
    vehicleMaxSpeed = VehicleCode.values.map{vehicleType => VehicleMaxSpeed(vehicleType = vehicleType, maxSpeed = 0)}.toList,
    marginSpeedLimits = marginSpeedLimits.map{sl => SpeedLimitMargin(speedLimit=sl)}.toList,
    duplicateTriggerDelta = 0,

    inaccuracyMarginForLength = 0,
    minimumLengthTrailer = 0,
    shortVehicleLengthThreshold = 500,
    longVehicleLengthThreshold = 600,
    countryCodeConfidence = 50,
    licenseConfidence = 50,
    maxLengthMoped = 0.0,
    testLicenseConfidence = false
  )

  /**
   * return the default path where all preferences reside
   * @param systemId is id of system
   * @return Path to preferences
   */
  def getDefaultPath(systemId:String) = Systems.getDefaultPath / systemId / "preferences"


  def getGlobalConfigPath() = Path("/ctes") / "configuration" / "preferences"

  /**
   * return the path of the preference leaf
   * @param systemId is id of system
   * @return Path to the preference config leaf
   */
  def getConfigPath(systemId:String) = getDefaultPath(systemId) / "config"

  /**
   * checks if a preference path exists
   * @param systemId is id of system
   * @return true is path exists else false
   */
  def exists(systemId:String) = curator.exists(getConfigPath(systemId))

  /**
   * retrieve config from storage
   * @param systemId is id of system
   * @return the preferences
   */
  def getBySystemId(systemId:String) = curator.getVersioned[Preference](Seq(getGlobalConfigPath, getConfigPath(systemId)), defaultPreference)

  /**
   * validates before a new preference is created
   * @param systemId is id of system
   * @return list of field errors
   */
  private def validate(systemId:String):List[FieldError]={
    var errors = List[FieldError]()

    systemMustExist(systemId=systemId).map{errors ::= _ }
    preferenceMustNotExist(systemId=systemId).map{errors ::= _ }

    errors
  }


  /**
   * retrieve preferences by systemId
   * @param systemId is id of system
   * @return optional VersionOf[Preference]
   */
  def findById(systemId:String):Option[Versioned[Preference]]={
    Preferences.getBySystemId(systemId)
  }

  /**
   * retrieve zaakbestand by systemId
   * @param systemId is id of system
   * @return optional VersionOf[CaseFile]
   */
  def getCaseFile(systemId:String):Option[Versioned[CaseFile]]={
    curator.getVersioned[CaseFile](Configuration.getCaseFileForSystem(systemId))
  }

  /**
   * updates zaakbestand for given system id
   * @param systemId id of system
   * @param model caseFile
   * @return ResponseMsg
   */
  def updateCaseFile(systemId:String, model:CaseFileCRUD)(implicit user:UserModel):ResponseMsg={
    var errors = List[FieldError]()
    
    systemMustExist(systemId=systemId).map{errors ::= _ }
    val caseFile = Preferences.getCaseFile(systemId)
    
    if(caseFile.isEmpty){
      errors ::= FieldError(field="version", error=Messages("preferences.other.caseFile.does_not_exist"))
    }

    if(errors.length > 0){
      ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors.toList)
    }else{
      try{
        curator.set(
          Configuration.getCaseFileForSystem(systemId),
          caseFile.get.data.copy(version = model.version),
          caseFile.get.version
        )
        log(messageId="logging.preferences.other.casefile.update", systemId=systemId)
        ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.update"))
      }catch{
        case e:Exception =>
          ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.success.failed"))
      }
    }
  }
  
  /**
   * updates preferences
   * @param systemId is id of system
   * @param model
   * @return a ResponseMsg that contains the status of updating a preference
   */
  def update(systemId:String, model:PreferenceCRUD)(implicit user:UserModel):ResponseMsg={
    var errors = List[FieldError]()
    
    systemMustExist(systemId=systemId).map{errors ::= _ }
    preferenceMustExist(systemId=systemId).map{errors ::= _ }
    systemMustBeInEditMode(systemId).map{errors ::= _ }

    if(errors.length > 0){
      ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors.toList)
    }else{
      try{
          curator.set(Preferences.getDefaultPath(systemId)/"config", model.preference, model.version)
          //log(messageId="logging.preferences.update", systemId=systemId)
          ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.update"))

      }catch{
        case e:Exception =>
          ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.failed.update"))
      }
    }
  }

  /**
   * creates default preferences. Called by Systems every time a new System is created
   * @param systemId is id of system
   * @return list of field errors
   */
  def createDefaultPreferences(systemId:String)(implicit user:UserModel):ResponseMsg={
    val errors = validate(systemId)

    if(errors.length > 0){
      ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors.toList)
    }else{
      try{
        curator.put(Preferences.getDefaultPath(systemId)/"config", defaultPreference)
        log(messageId="logging.preferences.createDefaultPreferences", systemId=systemId)
        ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.insert"))
      }catch{
        case e:Exception =>
          e.printStackTrace()
          ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.success.failed"))
      }
    }
  }
}