/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package services

import csc.config.{Path, VersionOf}
import common.Extensions._
import models.web._
import models.storage._
import common.{SystemLogger, Validates}
import csc.curator.utils.Versioned
import play.api.i18n.Messages

/**
 *Used for CRUD actions on Lane objects within a Gantry object
 */
object Lanes extends Validates with SystemLogger with Storage{

  /**
   * return de default path where all lanes reside
   * @param systemId is id of system
   * @param gantryId is id of gantry
   * @return Path to Lanes
   */
  def getDefaultPath(systemId:String, gantryId:String):Path = Gantries.getDefaultPath(systemId) / gantryId / "lanes"

  /**
   * return the path of the lane leaf
   * @param systemId is id of system
   * @param gantryId is id of gantry
   * @param laneId is id of lane
   * @return config path to a lane
   */
  def getConfigPath(systemId:String, gantryId:String, laneId:String):Path = Gantries.getCurrentGantryPath(systemId, gantryId) / "lanes" / laneId / "config"

  /**
   * returns the current path where of a given laneId
   * @param systemId is id of system
   * @param gantryId is id of gantry
   * @param laneId is id of lane
   * @return current lane path
   */
  def getCurrentLanePath(systemId:String, gantryId:String, laneId:String):Path = Gantries.getCurrentGantryPath(systemId, gantryId) / "lanes" / laneId

  /**
   * retrieve config from zookeeper
   * @param systemId is id of system
   * @param gantryId is id of gantry
   * @param laneId is id of lane
   * @return optional VersionOf[PirRadarCameraSensorConfig]
   */
  def getConfig(systemId:String, gantryId:String, laneId:String):Option[Versioned[SensorConfig]]={
    curator.getVersioned[SensorConfig](getConfigPath(systemId, gantryId, laneId))
  }

  /**
   * checks of a given laneId, gantryId and systemId exists
   * @param systemId is id of system
   * @param gantryId is id of gantry
   * @param laneId is id of lane
   * @return Boolean that indicates if a lane path exists
   */
  def exists(systemId:String, gantryId:String, laneId:String):Boolean={
    curator.exists(Lanes.getDefaultPath(systemId, gantryId) / laneId )
  }

  /**
   * returns all lanes within a given systemId
   * @param systemId is id of system
   * @return sequence of VersionOf[PirRadarCameraSensorConfig]
   */
  def getAllBySystemId(systemId:String)(implicit user:UserModel):Seq[Versioned[SensorConfig]]={
    Gantries.getBySystemId(systemId).foldLeft(List[Versioned[SensorConfig]]()){
      (list, gantry) => Lanes.getAllByGantryId(systemId, gantry.data.id).toList ::: list
    }
  }


  /**
   * returns all lanes within a given gantryId
   * @param systemId is id of system
   * @param gantryId is id of gantry
   * @return sequence of VersionOf[PirRadarCameraSensorConfig]
   */
  def getAllByGantryId(systemId:String, gantryId:String)(implicit user:UserModel):Seq[Versioned[SensorConfig]]={
    var lanes = List[Versioned[SensorConfig]]()
    val pathToLanes = Lanes.getDefaultPath(systemId, gantryId)

    if(curator.exists(pathToLanes)){
      val allLanes = curator.getChildren(pathToLanes)
      allLanes.foreach{ lanePath =>
        val versionOfLane = curator.getVersioned[SensorConfig](lanePath / "config")
        if(versionOfLane.isDefined)
          lanes ::= versionOfLane.get
      }
    }
    lanes
  }

  /**
   * delete a PirRadarCameraSensorConfig from a Gantry object in zookeeper
   * @param systemId is id of system
   * @param gantryId is id of gantry
   * @param laneId is id of lane
   * @param version of the gantry to delete
   * @return true by deletion else false
   */
  def delete(systemId:String, gantryId:String, laneId:String, version:Long)(implicit user:UserModel):Boolean={
    if(Systems.isEditable(systemId)){
      val path = getCurrentLanePath(systemId,gantryId,laneId)
      log(
        messageId="logging.lanes.delete",
        systemId=systemId, List(laneId))
      curator.deleteRecursive(path)
      true
    }else{
      false
    }
  }

  def systemWideUnique(systemId:String)(f:(System, Seq[SensorConfig]) => Boolean)(implicit user:UserModel):Boolean={
    Systems.findById(systemId).map{
      system => f(system.data, Lanes.getAllBySystemId(systemId).map(_.data))
    }.getOrElse(false)
  }

  /**
   * update a PirRadarCameraSensorConfig in zookeeper
   * @param systemId is id of system
   * @param gantryId is id of gantry
   * @param laneId is id of lane
   * @param model to update the lane with
   * @return a ResponseMsg indicate if updating a lane was successful
   */
  def update(systemId:String, gantryId:String, laneId:String, model:SensorConfig,version:Int)(implicit user:UserModel):ResponseMsg={
    var errors = List[FieldError]()

    //if gantries exists, it means that System exist as well
    if(!Gantries.exists(systemId, gantryId)){
      errors ::= FieldError(error=Messages("lanes.validate.system_does_not_exist"))
    }

    //check name has value.
    notEmpty(value=model.name, fieldKey="name", errorKey="lanes.validate.name_is_empty").map{errors ::= _ }
    if(errors.length > 0){
      return ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors.toList)
    }

    systemMustBeInEditMode(systemId).map{errors ::= _ }

    validate(model).foreach(e=>errors::=e)

    //check that new laneId not already exists
    if(!Lanes.exists(systemId, gantryId, laneId)){
      errors ::= FieldError(value=model.name, field="name", error=Messages("lanes.validate.laneId.not.exists"))
    }

    if(errors.length > 0){
      return ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors.toList)
    }else{
      val lane = getConfig(systemId, gantryId, laneId)
      lane match{
        case Some(versionOf) =>
          try{
            val updatedLane = versionOf.data.copy(
              name = model.name,
              bpsLaneId=model.bpsLaneId,
              camera = model.camera.copy(),
              pir = model.pir.copy(),
              radar=model.radar.copy()
            )

            curator.set(getCurrentLanePath(systemId,gantryId, laneId)/"config", updatedLane, version)
            return ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.update"))
          }catch{
            case e:Exception =>
              return  ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.failed.update"))
          }
        case  _ =>
          return  ResponseMsg(typeName=ResponseType.System, description=Messages("TODO No lane found"))
      }
    }
    return  ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.insert"))
  }

  /**
   * retrieve a LaneModel based on given id
   * @param systemId is id of system
   * @param gantryId is id of gantry
   * @param laneId is id of lane
   * @return an optional VersionOf[PirRadarCameraSensorConfig]
   */
  def findById(systemId:String, gantryId:String, laneId:String)(implicit user:UserModel):Option[Versioned[SensorConfig]]={
    getConfig(systemId, gantryId, laneId)
  }

  /**
   * Create a new PirRadarCameraSensorConfig in zookeeper
   * @param systemId is id of system
   * @param gantryId is id of gantry
   * @param model used to create a new lane
   * @return a ResponseMsg indicate if creating a lane was successful
   */
    def create(systemId:String, gantryId:String, model:SensorConfig)(implicit user:UserModel):ResponseMsg={
        var errors = List[FieldError]()

        //if gantries exists, it means that System exist as well
        if(!Gantries.exists(systemId, gantryId)){
          errors ::= FieldError(error=Messages("lanes.validate.system_does_not_exist"))
        }

        //check name has value.
        notEmpty(value=model.name, fieldKey="name", errorKey="lanes.validate.name_is_empty").map{errors ::= _ }
        if(errors.length > 0){
          return ResponseMsg(description=Messages("responsemsg.unkown-input"), fields=errors.toList)
        }

        systemMustBeInEditMode(systemId).map{errors ::= _ }

        validate(model).foreach(e=>errors::=e)

        val laneId = gantryId + asConfigKey(model.name)

        //check that new laneId not already exists
        if(Lanes.exists(systemId, gantryId, laneId)){
          errors ::= FieldError(value=model.name, field="name", error=Messages("lanes.validate.name_already_exists"))
        }

        if(errors.length > 0){
          ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors.toList)
        }else{

          val newLane = SensorConfig(
            id = laneId,
            name = model.name,
            bpsLaneId=model.bpsLaneId,
            camera = model.camera.copy(),
            radar = model.radar.copy(),
            pir = model.pir.copy()
            )

          try{
            val nwLanePath = Lanes.getDefaultPath(systemId, gantryId) / laneId
            curator.put(nwLanePath / "config", newLane)

            log(
              messageId="logging.lanes.create",
              systemId=systemId, List(laneId))
            ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.insert"))
          }catch{
            case e:Exception =>
              ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.success.failed"))
          }
        }
    ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.insert"))
  }

  def validate(model: SensorConfig):List[FieldError]= {
    var errors = List[FieldError]()

    notEmpty(value=model.bpsLaneId.getOrElse(""), fieldKey="bps", errorKey="lanes.validate.bps").map{errors ::= _ }

    betweenRange(value = model.pir.pirAddress, from = 1, to = 255, fieldKey = Some(Messages("lanes.form.title.pir_pirAddress.description"))).map {
      errors ::= _
    }
    notLowerThen(value = model.pir.refreshPeriod, from = 0, fieldKey = Some("pir-refreshPeriod")).map {
      errors ::= _
    }
    notLowerThen(value = model.pir.vrPort, from = 1025, fieldKey = Some("pir-vrPort")).map {
      errors ::= _
    }
    notLowerThen(value = model.pir.pirPort, from = 1, fieldKey = Some("pir-port")).map {
      errors ::= _
    }
    notEmpty(value = model.pir.pirHost, fieldKey = "pir-host", errorKey = "lanes.validate.pir.host").map {
      errors ::= _
    }
    notEmpty(value = model.pir.vrHost, fieldKey = "pir-vrHost", errorKey = "lanes.validate.pir.vrHost").map {
      errors ::= _
    }
    notEmpty(value = model.camera.relayURI, fieldKey = "camera-relayURI", errorKey = "lanes.validate.camera.relayURI").map {
      errors ::= _
    }
    notEmpty(value = model.radar.radarURI, fieldKey = "radar-radarURI", errorKey = "lanes.validate.radar.radarURI").map {
      errors ::= _
    }
    notEmpty(value = model.camera.cameraHost, fieldKey = "camera-cameraHost", errorKey = "validate.lane-cameraHost-is-empty").map {
      errors ::= _
    }
    notLowerThen(value = model.camera.cameraPort, from = 0, fieldKey = Some("camera-cameraPort")).map {
      errors ::= _
    }
    notLowerThen(value = model.camera.maxTriggerRetries, from = 0, fieldKey = Some(Messages("lanes.form.title.camera_maxTriggerRetries"))).map {
      errors ::= _
    }
    notLowerThen(value = model.camera.timeDisconnected, from = 0, fieldKey = Some("camera-timeDisconnected")).map {
      errors ::= _
    }

    errors
  }
}
