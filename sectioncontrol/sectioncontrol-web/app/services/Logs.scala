/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package services

import models.web._
import models.storage.{LogEntry, SystemEvent}
import csc.json.lift.LiftSerialization
import org.apache.hadoop.hbase.client.HTable
import csc.dlog.hbase.EventLog
import csc.dlog.EventLogReader
import common.hbase.utils.HBaseConnection
import common.{Config, SystemLogger, Validates}
import play.api.i18n.Messages
import csc.dlog

/**
 * main entry point for retrieving logging info
 */
object Logs extends Validates with SystemLogger with HBaseConnection with Config {
  val serialization = new LiftSerialization {
    implicit def formats = enumFormats
  }

  private lazy val systemsLogTable = new HTable(hbaseConfig, Config.Zookeeper.systemsLogTableName)

  /**
   * Used to retrieve logging info based on a systemId
   * @param systemId id of system
   * @param from a long indicating start date/time of request
   * @param to a long indicating end date/time of request
   * @return retrieve logging info or an error message
   */
  def getByTime(systemId:String, from:Long, to:Long)(implicit user:UserModel):Either[ResponseMsg,Seq[LogEntry]]={
    var errors = List[FieldError]()
    systemMustExist(systemId).map{errors ::= _ }

    if(from > to){
      errors ::= FieldError(field="from", value=from.toString, error=Messages("logs.validate.from_before_to"))
    }

    if(errors.length > 0){
      Left(ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors.toList))
    }else{
      try{
        log(messageId="logging.logs.getByTime", systemId=systemId, List(systemId))

        val result: Seq[LogEntry] =retrieveLogs(systemId,  from, to)
        Right(result)
      }catch{
        case e:Exception =>
          Left(ResponseMsg(typeName=ResponseType.System, description=Messages("logs.failed.retrieve.logs")))
      }
    }
  }


  def getUserName(users:Seq[UserModel],userId:String):String={
    users.find(_.id==userId)
    match{
    case Some(user) =>
      user.name
    case None =>
      ""
    }
  }

  def getUserRole(users:Seq[UserModel],userId:String):String={
    users.find(_.id==userId)
    match{
      case Some(user) =>
        user.role.name
      case None =>
        ""
    }
  }

  /**
   * Used to retrieve logging info based on a systemId
   * @param systemId id of system
   * @param from a long indicating start date/time of request
   * @param to a long indicating end date/time of request
   * @return retrieve logging info or an error message
   */
  private def retrieveLogs(systemId:String, from:Long, to:Long)(implicit user:UserModel):Seq[LogEntry]={
    val reader: EventLogReader = EventLog.createReader(systemsLogTable, systemId, serialization)
    val users: Seq[UserModel] =Users.getAll
    val events: Iterator[dlog.LogEntry[SystemEvent]] = reader.iterator[SystemEvent](fromTimestamp=from, tillTimestamp=to)
    events.foldLeft(List[LogEntry]()){
      (list, event) =>
        LogEntry(
          sequenceNr = event.seqnr,
          userId = event.event.userId,
          userName = getUserName(users,event.event.userId),
          userRole = getUserRole(users,event.event.userId),
          componentId = event.event.componentId,
          timestamp = event.event.timestamp,
          systemId = event.event.systemId,
          eventType = event.event.eventType,
          reason = event.event.reason,
          corridorId = event.event.corridorId) :: list
    }
  }
}