/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package services
import models._
import common.Extensions._
import java.util.UUID
import scala.collection.mutable.ListBuffer
import scala._
import csc.config.{Path}
import models.web._
import csc.curator.utils.Versioned
import models.storage.{PermittedAction, ActionType, User}
import play.api.i18n.Messages
import common.{Config, SystemLogger}
import play.mvc.Http
import controllers.User.ChangePasswordModel

/**
 *Used for CRUD actions on User objects
 */
object Users extends SystemLogger with Storage{

  def IsMemberOf(username:String, systemId:String):Boolean = {
    Users.findByUserName(username).map(_.sectionIds.contains(systemId)).getOrElse(false)
  }

  def IsPermitted(username:String, actions:ActionType.Value*):Boolean={
    Users
      .findByUserName(username)
      .map{
      usr =>
        val permissions = actions.map(action => usr.hasPermission(action))
        if(permissions.isEmpty || permissions.exists(_ == false)) false else true
    }.getOrElse(false)
  }

  /**
   * Give all users permission to an existing system
   * @param systemId id of system
   */
  def addSystemToUsers(systemId:String)(implicit user:UserModel){
    val paths = curator.getChildren(Paths.User.default)
    paths.foreach{ userPath =>
      val usr = curator.getVersioned[User](userPath)
      usr.foreach{
        us =>
          val sections = (systemId :: us.data.sectionIds).toSet.toList
          val updatedUser = us.data.copy(sectionIds = sections)
          curator.set(Paths.User.default / updatedUser.id, updatedUser, us.version)
          log(messageId="logging.users.addSystemToUsers", systemId=systemId, List(us.data.username))
      }
    }
  }

  /**
   * remove systemId from all users
   * @param systemId id of system
   */
  def removeSystemFromUsers(systemId:String)(implicit usr:UserModel){
    val paths = curator.getChildren(Paths.User.default)
    paths.foreach{ userPath =>
      val user = curator.getVersioned[User](userPath)
      user.foreach{
        u =>
          val sections = u.data.sectionIds.filterNot(_ == systemId).toSet.toList
          val updateUser = u.data.copy(sectionIds=sections)
          curator.set(Paths.User.default / updateUser.id, updateUser, u.version)
          log(messageId="logging.users.removeSystemFromUsers",
            systemId=systemId,
            List(u.data.username)
          )
      }
    }
  }

  /**
   * creates or updates a given user
   */
  def createOrUpdate(usrModel:UpdateUserModel)(implicit user:UserModel):Either[UserModel, ResponseMsg]={
    val errors = new ListBuffer[FieldError]

    usrModel.errors match{
      case Some(x) => //user contains errors
        Right(ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=x.toList))
      case _ => //no user errors found
        //val currentUser: Option[UserModel] = Users.findByUserName(usrModel.username)
        val currentUser: Option[UserModel] = Users.findById(usrModel.id)
        val role: Option[RoleModel] = Roles.findById(usrModel.roleId)
        val id = usrModel.id.toOption
        // Check his role for permittedactions with reportingofficeridRequired
        val permittedActionsWithReportingofficeridRequired =role.get.permittedActions.filter(pa=>pa.reportingofficeridRequired==true).length>0

        if (permittedActionsWithReportingofficeridRequired &&
          usrModel.reportingOfficerId!=null &&
          usrModel.reportingOfficerId.isEmpty)
        {
          errors.append(FieldError(field="role-id", value="", error=Messages("responsemsg.roles.role_not_allowed")))
        }

        usrModel match {

          //role does NOT exists
          case u:UserModel if role.isEmpty =>
            errors.append(FieldError(field="roleId", value="", error=Messages("responsemsg.roles.role_not_exists")))

          //new user, username is NOT unique
          case u:UserModel if id.isEmpty && currentUser.isDefined =>
            errors.append(FieldError(field="username", value="", error=Messages("responsemsg.users.username_already_exists")))

          //existing user, username is NOT unique
          case u:UserModel if id.isDefined && currentUser.isDefined && currentUser.get.id != id.get =>
            errors.append(FieldError(field="username", value="", error=Messages("responsemsg.users.username_already_exists")))

          //disregard
          case _ =>
        }
        (errors.length > 0)  match {
          //create/update user
          case false =>
            var passwordHash = ""
            var passwordHash1 = ""
            var passwordHash2 = ""
            var reportingOfficerIdCleared=false

            // bestaande gebruiker en ww niet gewijzigd. Gebruik bestaande ww
            if(currentUser.isDefined && !usrModel.changePassword)
            {
              passwordHash = currentUser.get.passwordHash
              passwordHash1 = currentUser.get.prevPasswordHash1
              passwordHash2 = currentUser.get.prevPasswordHash2
            }
            // nieuwe gebruiker of bestaande gebruiker waar ww van gewijzigd moet worden.
            else if(currentUser.isDefined==false || (currentUser.isDefined && usrModel.changePassword))
            {
              // ongeldig ww?
              if(!isPasswordValid(usrModel.password))
              {
                errors.append(FieldError(field="password", value="", error="Het wachtwoord voldoet niet aan de volgende regel: 1 alfanumeriek karakter, 1 numeriek karakter, 1 niet alfanumeriek en numeriek character."))
              }

              else if(usrModel.password==usrModel.username){
                errors.append(FieldError(field="password", value="", error="Een wachtwoord kan niet hetzelfde zijn als de gebruikersnaam."))
              }

              // bestaande gebruiker en het nieuwe ww hetzelfde als huidige of als vorige twee
              else if(currentUser.isDefined){
                if(usrModel.password.digest()==currentUser.get.passwordHash ||
                  usrModel.password.digest()==currentUser.get.prevPasswordHash1 ||
                  usrModel.password.digest()==currentUser.get.prevPasswordHash2){
                  errors.append(FieldError(field="password", value="", error="Het nieuwe wachtwoord mag niet hetzelfde zijn als de 3 vorige wachtwoorden."))
                }
                else
                {
                  passwordHash = usrModel.password.digest()
                  passwordHash1 = currentUser.get.passwordHash
                  passwordHash2 = currentUser.get.prevPasswordHash1
                }
              }
              else {
                passwordHash = usrModel.password.digest()
              }
            }

            (errors.length >0)  match {
              //create/update user
              case false =>
                val reportingOfficerIdFilledIncurrentUser=currentUser.isDefined &&  currentUser.get.reportingOfficerId!=null &&
                  currentUser.get.reportingOfficerId.isEmpty==false

                val reportingOfficerIdClearedInGui=usrModel.reportingOfficerId==null || usrModel.reportingOfficerId.isEmpty
                if(reportingOfficerIdFilledIncurrentUser && reportingOfficerIdClearedInGui)
                  reportingOfficerIdCleared=true

                var badLoginCount=0
                if(!id.isEmpty){
                  val versionOf=curator.getVersioned[User](Paths.User.default / id.get)
                  if(versionOf.isDefined){

                    val x=versionOf.get
                    badLoginCount=x.data.badLoginCount.getOrElse(0)

                    // When the password is changed and the badLoginCount is on max the reset the badLoginCount
                    if(usrModel.changePassword && badLoginCount==Config.badLoginMax)
                    {
                      badLoginCount=0
                    }

                  }
                }

                val newUser = User(
                  id = if(id.isEmpty) UUID.randomUUID.toString else id.get,
                  reportingOfficerId = usrModel.reportingOfficerId,
                  name = usrModel.name,
                  username = usrModel.username,
                  phone = usrModel.phone,
                  ipAddress = usrModel.ipAddress,
                  email = usrModel.email,
                  passwordHash = passwordHash,
                  roleId = usrModel.roleId,
                  sectionIds = usrModel.sectionIds.toSet.toList,
                  prevPasswordHash1 = passwordHash1,
                  prevPasswordHash2 = passwordHash2,
                  badLoginCount =Some(badLoginCount)
                )

                id match {
                  case Some(x) =>
                    curator.set(Paths.User.default / newUser.id, newUser, usrModel.version)
                    if(reportingOfficerIdCleared)
                      logMultipleSystemIds(messageId="logging.users.update_reportingofficerid_cleared", systemIds=newUser.sectionIds, List(newUser.username))
                  case _ =>
                    curator.put(Paths.User.default / newUser.id, newUser)
                }
                Left(Users.findById(newUser.id).get)
              case true =>
                Right(ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors.toList))
            }
          case true =>
            Right(ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors.toList))
        }
    }
  }

  def isPasswordValid(newPassword:String):Boolean={
    val patternAlphanumeric = "[A-Za-z]".r
    val patternNumeric = "[\\d]".r
    val patternNotAlphanumericAndNotNumeric = "[\\W]".r

    val doesConformPatternAlphanumeric = ((patternAlphanumeric findAllIn newPassword).length>=1)
    val doesConformPatternNumeric = ((patternNumeric findAllIn newPassword).length>=1)
    val doesConformPatternNotAlphanumericAndNotNumeric= ((patternNotAlphanumericAndNotNumeric findAllIn newPassword).length>=1)

    if(newPassword.length>=8 &&
      doesConformPatternAlphanumeric &&
      doesConformPatternNumeric &&
      doesConformPatternNotAlphanumericAndNotNumeric
    ){
      return true
    }
    return false
  }

  def deleteOne(id: String, version: Long): Boolean = {
    Users.findById(id) match {
      case Some(c) => curator.delete(Paths.User.default / id)
        true
      case _ => false
    }
  }

  /**
   * deletes a user from zookeeper
   */
  def delete(id:String, version:Long, username:String)(implicit user:UserModel):Boolean={
    val connUser = Users.findByUserName(username)
    if(connUser.isDefined && (id != connUser.get.id)){
      val userToBeDeleted = Users.findById(id).map(_.username).getOrElse("")
      connUser.foreach{
        usr =>
          logMultipleSystemIds(messageId="logging.users.delete", systemIds=usr.sectionIds, List(userToBeDeleted))
      }
      curator.delete(Paths.User.default / id)
      true
    }else{
      false
    }
  }

  /**
   * retrieve all user model objects
   */
  def getAll:Seq[UserModel]={
    curator.getChildren(Paths.User.default).foldLeft(List[UserModel]()){
      (list, path) =>
        curator
          .getVersioned[User](path)
          .map(toModel(_) :: list)
          .getOrElse(list)
    }
  }

  /**
   * retrieve user by id
   */
  def findById(id:String):Option[UserModel]={
    curator.getVersioned[User](Paths.User.default / id).map(toModel(_))
  }

  /**
   * Retrieve user by username
   */
  def findByUserName(username:String):Option[UserModel]={
    Users.getAll.find(_.username == username)
  }

  /**
   * Authenticate an user
   */
  def authenticate(username:String, password:String):Option[UserModel]={
    Users.getAll.find(user => user.username == username ) match {
      case Some(userModel) =>
        val user=curator.getVersioned[User](Paths.User.default / userModel.id)
        if (user.isDefined) {
          val badLoginCount=user.get.data.badLoginCount.getOrElse(0)
          if (badLoginCount< Config.badLoginMax) {
            if (userModel.passwordHash == password.digest()) {
              // Er was ergens een req dat als iemand goed inlogt de badLoginCount naar 0 gaat.
              if (badLoginCount > 0) {
                val newUser = user.get.data.copy(badLoginCount = Some(0))
                curator.set(Paths.User.default / user.get.data.id, newUser, user.get.version)
              }
              Some(userModel)
            } else {
              val newUser = user.get.data.copy(badLoginCount = Some(badLoginCount + 1))
              curator.set(Paths.User.default / user.get.data.id, newUser, user.get.version)
              None
            }
          } else{
            Some(userModel.copy(userBlocked=true))
          }
        }
        else
          None
      case _ =>
        None
    }
  }

  /**
   * used to create a UserModel that represents a User object stored in zookeeper
   */
  private def toModel(versionOf:Versioned[User]):UserModel={
    UserModel(
      id = versionOf.data.id,
      reportingOfficerId = versionOf.data.reportingOfficerId,
      name = versionOf.data.name,
      username = versionOf.data.username,
      email = versionOf.data.email,
      phone = versionOf.data.phone,
      ipAddress = versionOf.data.ipAddress,
      password = "",
      passwordHash = versionOf.data.passwordHash,
      roleId = versionOf.data.roleId,
      sectionIds = versionOf.data.sectionIds.toSet.toList,
      role = Roles.findById(versionOf.data.roleId).getOrElse(null),
      version = versionOf.version,
      sections = Systems.findByIds(versionOf.data.sectionIds),
      prevPasswordHash1 = versionOf.data.prevPasswordHash1,
      prevPasswordHash2 = versionOf.data.prevPasswordHash2,
      userBlocked= versionOf.data.badLoginCount.getOrElse(0)==Config.badLoginMax
    )
  }
}



















