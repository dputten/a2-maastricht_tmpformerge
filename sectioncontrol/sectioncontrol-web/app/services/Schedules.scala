/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package services

import java.util.{UUID, Date, Calendar}
import org.apache.commons.lang.time.DateUtils
import models.web._
import models.storage._
import org.apache.zookeeper.KeeperException.BadVersionException
import common.{Int, SystemLogger, Validates}
import csc.curator.utils.Versioned
import play.api.i18n.Messages
import org.joda.time.Interval
import csc.sectioncontrol.storagelayer.state.StateService
import csc.sectioncontrol.storagelayer.calibrationstate.CalibrationStateService

/**
 * Used for CRUD actions on Schedules objects
 */
object Schedules extends Validates with SystemLogger with Storage{

  /**
   * return the default path where all schedules reside
   * @param systemId is id of system
   * @param corridorId is id of corridor
   * @return default Path to schedules
   */
  def getDefaultPath(systemId:String, corridorId:String) = Corridors.getRootPath(systemId, corridorId)

  /**
   * return the path of the Schedule leaf
   * @param systemId is id of system
   * @param corridorId is id of corridor
   * @return Path to schedules
   */
  def getSchedulePath(systemId:String, corridorId:String) = getDefaultPath(systemId, corridorId) / "schedule"

  /**
   * checks if a corridor path exists
   * @param systemId is id of system
   * @param corridorId is id of corridor
   * @return true is path exists else false
   */
  def exists(systemId:String, corridorId:String) = curator.exists(getSchedulePath(systemId, corridorId))

  /**
   * retrieve config from zookeeper
   * @param systemId is id of system
   * @param corridorId is id of corridor
   * @return list of schedules
   */
  def getSchedules(systemId:String, corridorId:String) = curator.getVersioned[List[Schedule]](getSchedulePath(systemId, corridorId))

  /**
   * retrieve a list of schedules base of the corridorId
   * @param systemId is id of system
   * @param corridorId is id of corridor
   * @return list of schedules
   */
  def getByCorridorId(systemId:String, corridorId:String):Option[Versioned[List[Schedule]]]={
    getSchedules(systemId, corridorId)
  }

  /**
   * get schedule by id
   * @param systemId is id of system
   * @param corridorId is id of corridor
   * @param scheduleId is id of schedule
   * @return schedule
   */
  def getById(systemId:String, corridorId:String, scheduleId:String)(implicit user:UserModel):Option[Versioned[Schedule]]={
    val schedulesVersionOf = Schedules.getByCorridorId(systemId, corridorId)

    schedulesVersionOf match {
      case Some(x) => 
        val found = x.data.find{p => p.id==scheduleId}
        if(found.isDefined){
          Some(Versioned(version = x.version, data = found.get))
        }else{
          None
        }
      case _ =>
        None
    }
  }

  /**
   * retrieve the schedules that are active by a given corridor
   * @param systemId is id of system
   * @param corridorId is id of corridor
   * @return sequence of active schedules
   */
  def getActiveSchedules(systemId:String, corridorId:String)(implicit user:UserModel):Seq[Schedule]={
    val current = Calendar.getInstance()
    val cal = Calendar.getInstance()

    cal.clear()
    cal.set(Calendar.HOUR_OF_DAY, current.get(Calendar.HOUR_OF_DAY))
    cal.set(Calendar.MINUTE, current.get(Calendar.MINUTE))
    cal.set(Calendar.SECOND, current.get(Calendar.SECOND))
    cal.set(Calendar.MILLISECOND, current.get(Calendar.MILLISECOND))

    //if(Systems.isEnforcing(systemId,corridorId) && !Systems.isCalibrating(systemId)){
    //if(Systems.isEnforcing(systemId) && !CalibrationStateService.isCalibrating(systemId,corridorId,curator)){
    if(Systems.isEnforcing(systemId) && !Systems.isCalibrating(systemId)){
      Schedules.getSchedules(systemId, corridorId).foldLeft(List[Schedule]()){
        (list, schedule) =>
          schedule.data.foldLeft(List[Schedule]()){
            (l, s) =>
              val from = Calendar.getInstance()
              val to = Calendar.getInstance()

              val fromDate = getTime(s.fromPeriodHour, s.fromPeriodMinute)
              val toDate = getTime(s.toPeriodHour, s.toPeriodMinute)
              val fromPeriod = fromDate.getTime
              val toPeriod = toDate.getTime


              from.setTimeInMillis(fromPeriod)
              to.setTimeInMillis(toPeriod)
//              from.setTimeInMillis(s.fromPeriod)
//              to.setTimeInMillis(s.toPeriod)

              if(cal.after(from) && cal.before(to)){
                s :: l
              }else{
                l
              }
          } ::: list
    }
    }else{
      List()
    }
  }

//  /**
//   * get all available systems
//   * @return sequence of systems
//   */
//  def getAvailableSystems(implicit user:UserModel):Seq[Versioned[System]]={
//    var systemIds = List[String]()
//    Systems.getEditableSystems.foreach{
//      system =>
//        val corridors = Corridors.getAllBySystemId(system.data.id)
//        if(corridors.length > 0){
//          systemIds ::= system.data.id
//        }
//    }
//
//    Systems.findByIds(systemIds).sortBy(_.data.name).toSeq
//  }

  /**
   * get all available Corridors
   * @param systemId is id of system
   * @return Corridors
   */
  def getAvailableCorridors(systemId:String)(implicit user:UserModel):Seq[Versioned[Corridor]]={
    Corridors.getAllBySystemId(systemId)
  }


  /**
   * removes a schedule
   * @param systemId is id of system
   * @param corridorId is id of corridor
   * @param scheduleId is id of schedule
   * @param version of schedule
   * @return true if schedule is deleted else false
   */
  def delete(systemId:String, corridorId:String, scheduleId:String, version:Int)(implicit user:UserModel):Boolean={
    if(!Systems.isEditable(systemId))
      return false

    val path = Schedules.getSchedulePath(systemId,corridorId)

    if(curator.exists(path)){
      val schedulesVersionOf = Schedules.getByCorridorId(systemId, corridorId)
      schedulesVersionOf.foreach{
        sched =>
          val toBeDeleted = sched.data.find{f => f.id==scheduleId}
          if(sched.version == version){
            toBeDeleted.foreach{
              val remainingSchedules = sched.data.filterNot{f => f.id==scheduleId}
              val path = Schedules.getDefaultPath(systemId, corridorId)
              curator.set(path / "schedule", remainingSchedules, sched.version)
              log(
                messageId="logging.schedules.delete",
                systemId=systemId, List(corridorId))
              return true
            }
          }
      }
    }
    false
  }

  /**
   * validates a given model
   * @param systemId is id of system
   * @param corridorId is id of corridor
   * @param model
   * @return list of field errors
   */
  private def validate(systemId:String, corridorId:String, model:ScheduleCRUD)(implicit user:UserModel):List[FieldError]={
    var errors = List[FieldError]()

    if(!Systems.exists(systemId)){
      errors ::= FieldError(field="systemId", error=Messages("schedules.validate.systemId_not_exist"))
    }

    systemMustBeInEditMode(systemId).map{errors ::= _ }

    if(!Corridors.exists(systemId, corridorId)){
      errors ::= FieldError(field="corridorId", error=Messages("schedules.validate.corridorId_not_exist"))
    }

    if(model.fromPeriodHour < 0 || model.fromPeriodHour > 24){
      errors ::= FieldError(field="fromPeriodHour", error=Messages("corridors.validate.fromPeriodHour"))
    }

    if(model.fromPeriodMinute < 0 || model.fromPeriodMinute > 59){
      errors ::= FieldError(field="fromPeriodMinute", error=Messages("corridors.validate.fromPeriodMinute"))
    }

    if(model.toPeriodHour < 0 || model.toPeriodHour > 24){
      errors ::= FieldError(field="toPeriodHour", error=Messages("corridors.validate.toPeriodHour"))
    }

    if(model.toPeriodMinute < 0 || model.toPeriodMinute > 59){
      errors ::= FieldError(field="toPeriodMinute", error=Messages("corridors.validate.toPeriodMinute"))
    }

    if(model.speedLimit < 20 || model.speedLimit > 250){
      errors ::= FieldError(field="speedlimit", error=Messages("corridors.validate.speedLimit"))
    }

    if(model.signIndicator && model.signSpeed <= 0){
      errors ::= FieldError(field="signSpeed", error=Messages("corridors.validate.signSpeed_must_fill_in"))
    }

    if(model.signIndicator && model.speedIndicatorType.isEmpty){
      errors ::= FieldError(field="speedIndicatorType", error=Messages("corridors.validate.speedIndicatorType_required"))
    }

    val fromDate = getTime(model.fromPeriodHour, model.fromPeriodMinute)
    val toDate = getTime(model.toPeriodHour, model.toPeriodMinute)

    if(fromDate.after(toDate)){
      errors ::= FieldError(field="fromPeriod", error=Messages("corridors.validate.fromPeriod_greater_than_toPeriod"))
    }

    if(!DateUtils.isSameDay(fromDate, toDate)){
      errors ::= FieldError(field="fromPeriod", error=Messages("corridors.validate.toPeriod_not_same_day_as_fromPeriod"))
    }

    errors
  }

  /**
   * checks for overlapping schedule periods
   * @param systemId is id of system
   * @param corridorId is id of corridor
   * @param scheduleId is id of schedule
   * @param model
   * @return list of field errors
   */
  private def checkForOverlapping(systemId:String, corridorId:String, scheduleId:String, model:ScheduleCRUD):List[FieldError]={
    var errors = List[FieldError]()
    val newSchedule = createSchedule(Some(scheduleId), model)
    val schedulesVersionOf = Schedules.getSchedules(systemId, corridorId)

    if(schedulesVersionOf.isDefined){
      val schedules = schedulesVersionOf.get.data

      val newFromDate = getTime(newSchedule.fromPeriodHour, newSchedule.fromPeriodMinute)
      val newToDate = getTime(newSchedule.toPeriodHour, newSchedule.toPeriodMinute)
      val newFromPeriod = newFromDate.getTime
      val newToPeriod = newToDate.getTime

      val overlaps = schedules.filter{
        schedule =>{
          val fromDate = getTime(schedule.fromPeriodHour, schedule.fromPeriodMinute)
          val toDate = getTime(schedule.toPeriodHour, schedule.toPeriodMinute)
          val fromPeriod = fromDate.getTime
          val toPeriod = toDate.getTime

          (newFromPeriod >=  fromPeriod && newFromPeriod < toPeriod) ||
            (newToPeriod > fromPeriod  && newToPeriod <= toPeriod)
        }
      }

      val filterOriginalSchedule = overlaps.filterNot{p => p.id==scheduleId}
      if(filterOriginalSchedule.length > 0){
        errors ::= FieldError(field="fromPeriod", error=Messages("corridors.validate.overlaps"))
      }
    }
    errors
  }

  /**
   * updates a schedule
   * @param systemId is id of system
   * @param corridorId is id of corridor
   * @param scheduleId is id of schedule
   * @param model
   * @return a ResponseMsg that contains the status of updating a schedule
   */
  def update(systemId:String, corridorId:String, scheduleId:String, model:ScheduleCRUD)(implicit user:UserModel):ResponseMsg={
    var errors = validate(systemId, corridorId, model)

    if(errors.length > 0){
      return ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors.toList)
    }else{
      val path = Schedules.getSchedulePath(systemId,corridorId)

      if(curator.exists(path)){
        val schedulesVersionOf = Schedules.getByCorridorId(systemId, corridorId)
        schedulesVersionOf.foreach{
          sched =>
          val toBeUpdated = sched.data.find{f => f.id==scheduleId}
          if(toBeUpdated.isEmpty){
            errors ::= FieldError(field="", error=Messages("corridors.validate.schedule_does_not_exist"))
            return ResponseMsg(description=Messages("responsemsg.unknown-input"), fields=errors.toList)
          }
          else{
            errors :::= checkForOverlapping(systemId, corridorId, scheduleId, model)
            if(errors.length > 0){
              return ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors.toList)
            }
            else if(model.newSystemId != systemId || model.newCorridorId != corridorId){
              //delete current and create new one
              val result = Schedules.create(model.newSystemId, model.newCorridorId, model)
              if(result.success){
                Schedules.delete(systemId, corridorId, scheduleId, model.version.getOrElse(0))
              }
              return result

            }else{ //update current
              val path = Schedules.getSchedulePath(systemId, corridorId)
              val updatedSchedule = createSchedule(Some(scheduleId), model)
              val remainingSchedules = sched.data.filterNot{f => f.id==scheduleId}
              val allItems =  updatedSchedule :: remainingSchedules
              try{
                curator.set(path, allItems, sched.version)
              }catch{
                case e:BadVersionException =>
                  ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.failed.update_version_conflict"))
                case e:Exception =>
                  ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.failed.update"))
              }
            }
            return  ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.insert"))
          }
        }
      }
    }
    ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.success.failed"))
  }

  /**
   * creates a new schedule model
   * @param model
   * @return a schedule
   */
  private def createSchedule(scheduleId:Option[String], model:ScheduleCRUD):Schedule={
    Schedule(
      id = scheduleId.getOrElse(UUID.randomUUID.toString),
      fromPeriodHour = model.fromPeriodHour,
      fromPeriodMinute = model.fromPeriodMinute,
      toPeriodHour = model.toPeriodHour,
      toPeriodMinute = model.toPeriodMinute,
      speedLimit = model.speedLimit,
      signSpeed = model.signSpeed,
      signIndicator = model.signIndicator,
      speedIndicatorType = if(model.signIndicator) model.speedIndicatorType else None,
      indicationRoadworks = model.indicationRoadworks,
      indicationDanger = model.indicationDanger,
      indicationActualWork = model.indicationActualWork,
      invertDrivingDirection = model.invertDrivingDirection,
      supportMsgBoard=model.supportMsgBoard,
      msgsAllowedBlank=model.msgsAllowedBlank
    )
  }

  /**
   * creates and stores a new schedule model in zookeeper
   * @param systemId is id of system
   * @param corridorId is id of corridor
   * @param model
   * @return a ResponseMsg that contains the status of creating a schedule
   */
  def create(systemId:String, corridorId:String, model:ScheduleCRUD)(implicit user:UserModel):ResponseMsg={
    val errors = validate(systemId, corridorId, model) ::: checkForOverlapping(systemId, corridorId, UUID.randomUUID.toString, model)
    val schedulesVersionOf = Schedules.getSchedules(systemId, corridorId)

    if(errors.length > 0){
      ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors.toList)
    }else{
      val newSchedule = createSchedule(None, model)
      try{
        val path = Schedules.getSchedulePath(systemId, corridorId)
        if(schedulesVersionOf.isDefined){
          val schedules = schedulesVersionOf.get.data
          val newList = newSchedule :: schedules
          curator.set(path, newList, schedulesVersionOf.get.version)
        }else{
          curator.put(path, List(newSchedule))
          log(
            messageId="logging.schedules.create",
            systemId=systemId, List(corridorId))
        }
        ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.insert"))
      }catch{
        case e:Exception =>
          ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.success.failed"))
      }
    }

  }

  /**
   * used to transform a given period into a date
   * @param hour
   * @param minute
   * @return date
   */
  private def getTime(hour:Int, minute:Int):Date={
    val cal = Calendar.getInstance()
    cal.clear()
    if(hour == 24){
      cal.set(Calendar.HOUR_OF_DAY, 23)
      cal.set(Calendar.MINUTE, 59)
      cal.set(Calendar.SECOND, 59)
      cal.set(Calendar.MILLISECOND, 999)
    }else {
      cal.set(Calendar.HOUR_OF_DAY, hour)
      cal.set(Calendar.MINUTE, minute)
    }

    cal.getTime
  }
}
