/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package services

import csc.config.{Path}
import java.util.{Date}
import org.apache.zookeeper.KeeperException.BadVersionException
import models.web._
import models.storage._
import common.{SystemLogger, Validates}
import scala.Predef._
import csc.curator.utils.Versioned
import play.api.i18n.Messages
import csc.sectioncontrol.messages.certificates.ActiveCertificate
import csc.sectioncontrol.storagelayer.calibrationstate.CalibrationStateService

/**
 *Used for CRUD actions on System objects
 */
object Systems extends Validates with SystemLogger with Storage{

  import SystemStateType._

  /**
   * this represents the next possible states that an user can request for including the permission required
   */
  private val userStatesAndPermissions = Map[SystemStateType.Value, Map[SystemStateType.Value,ActionType.Value]](
    Off -> Map(
      StandBy -> ActionType.AllRoles /*Power on*/),
    StandBy -> Map(
      Off -> ActionType.AllRoles /*Power off*/)
  )

  /**
   * determine if dynamax is available
   * @return true if dynamax is available
   */
  def isDynamaxActive:Boolean=Configuration.getDynamaxIfaces.size > 0


  def getAvailableEsaProviders:Seq[EsaProviderType.Value]={
    val providers = EsaProviderType.values.toSeq

    if(Systems.isDynamaxActive)
      providers
    else
      providers.filterNot(_ == EsaProviderType.Dynamax)
  }

  /**
   * determine if a given systemId is enforcing
   * @param systemId is id of system
   * @return true is system has state EnforceOn
   */
  def isEnforcing(systemId:String):Boolean={
    import SystemStateType._
    getSystemStateBuilder(systemId).getSystemState.exists(s => s == EnforceOn)
  }

  /**
   * returns true if the current system is calibrating
   * @param systemId is id of system
   * @return returns true if the current system is calibrating
   */
  def isCalibrating(systemId:String):Boolean={
    val corridors = Corridors.getAllBySystemId(systemId)
    val isCalibrations=corridors.map(c=>CalibrationStateService.isCalibrating(systemId,c.data.id,curator))
    isCalibrations.contains(true)
  }

  /**
   * creates and returns a pre-filled SystemEvent
   * @param systemId is id of system
   * @return a pre-filled SystemEvent
   */
  def getDefaultEvent(systemId:String)(implicit user:UserModel)={
    SystemEvent(
      userId = user.id,
      componentId = "web",
      timestamp = new Date().getTime,
      systemId = systemId,
      eventType = "",
      reason = ""
    )
  }

  /**
   * Stores an updated system hash
   * @param systemId id of system
   */
  def updateHash(systemId:String){
    Certificates.createOrUpdateActiveCertificate(systemId, getConfigCertificateName, Systems.createHash(systemId))
  }

  /**
   * used for sending system events
   * @param systemId id of system
   * @param reason the reason of the event
   * @param eventType the type of event. default "Info"
   */
  def sendSystemEvent(reason:String, systemId:String="", eventType:String="Info")(implicit user:UserModel){
    if(systemId != "" && Systems.exists(systemId))
      Systems.sendEvent(systemId, Systems.getDefaultEvent(systemId).copy(eventType=eventType, reason=reason))
    else{
      Systems.getAll.foreach{
        case system =>
          Systems.sendEvent(system.data.id, Systems.getDefaultEvent(system.data.id).copy(eventType=eventType, reason=reason))
      }
    }
  }

  /**
   * used to append an event in storage
   * @param systemId is id of system
   * @param systemEvent to be appended
   */
  def sendEvent(systemId:String, systemEvent:SystemEvent)(implicit user:UserModel){
    val pathToEvents = Systems.getEventPath(systemId)

    if(!curator.exists(pathToEvents)){
      curator.createEmptyPath(pathToEvents)
    }
    curator.appendEvent(pathToEvents / "qn-", systemEvent)
  }

  /**
   * creates a system state event
   * @param systemId is id of system
   * @param user who request the state change
   * @param model containing the new and previous state
   * @return a ResponseMsg to indicate if sending an system event state was successful
   */
  def changeState(systemId:String, corridorId:String,model:StateChangeCRUD)(implicit user:UserModel):ResponseMsg={
    var errors = List[FieldError]()

    systemMustExist(systemId).map{errors ::= _ }
    stateMustExist(systemId,corridorId).map{errors ::= _ }
    stateMustBeSameAsPreviousState(systemId, corridorId,model).map{errors ::= _ }

    if(errors.length > 0){

      ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors.toList)
    }else{
      try{
        val fromState = getMessage("logs.eventType." + model.from.toString.replaceAll("-","."))
        val toState = getMessage("logs.eventType." + model.to.toString.replaceAll("-","."))
        val reason = getMessage("logging.systems.changeState", List(user.username, user.reportingOfficerId, systemId, fromState, toState))
        val event = getDefaultEvent(systemId).copy(eventType = model.to.toString,reason = reason,corridorId=Some(corridorId))

        sendEvent(systemId, event)
        ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.insert"))
      }catch{
        case e:Exception =>
          ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.failed.insert"))
      }
    }
  }

  /**
   * creates a hash of the length for each sections within a system
   * @param systemId is id of system
   */
  def createHash(systemId:String):String={
    import common.Extensions._
    val sections = Sections.getAllBySystemId(systemId).sortBy(_.data.id)

    if(sections.length > 0){
      sections.map{
        sec =>
          "systemId=" + systemId +
            ";sectionId=" + sec.data.id +
            ";startGantryId=" + sec.data.startGantryId +
            ";endGantryId=" + sec.data.endGantryId +
            ";length=" + sec.data.length
      }.mkString("|").digest()
    }else{
      "".digest()
    }
  }

  /**
   * return the event path where all events for a system resides
   * @param systemId is id of system
   * @return Path to system events
   */
  def getEventPath(systemId:String) = getRootPath(systemId) / "events"

  /**
   * return the log event path what is used by the monitor
   * @param systemId is id of system
   * @return Path to system events
   */
  def getLogEventPath(systemId:String) = getRootPath(systemId) / "log-events"

  /**
   * return de root path where a system reside
   * @param systemId is id of system
   * @return Path to a specific System
   */
  def getRootPath(systemId:String) = getDefaultPath / systemId

  def getStatePath(systemId:String,corridorId:String):Option[Versioned[SystemState]]={
    curator.getVersioned[SystemState](csc.sectioncontrol.storage.Paths.State.getCurrentPath(systemId,corridorId))

  }

  def getConfigCertificateName = "Configuratiezegel"
  /**
   * retrieve config hash from storage
   * @param systemId is id of system
   */
  def getConfigHash(systemId:String)(implicit user:UserModel):Option[Versioned[ActiveCertificate]]={
    Certificates.getActiveCertificate(systemId, getConfigCertificateName)
  }

  def statePathExists(systemId:String,corridorId:String):Boolean={
    curator.exists(csc.sectioncontrol.storage.Paths.State.getCurrentPath(systemId,corridorId))
  }

  def getConfigPath(systemId:String) = getRootPath(systemId) / "config"

  /**
   * retrieve path from storage
   * @param key is id of system
   */
  def getConfig(key:String):Option[Versioned[System]]={
    curator.getVersioned[System](Path("/ctes") / "systems" / key / "config")
  }

  /**
   * return the default path where all systems reside
   */
  def getDefaultPath:Path = Path("/ctes") / "systems"

  /**
   * return de path where all systems resides
   */
  def getAllPaths:Seq[Path]={
    curator.getChildren(Path("/ctes") / "systems")
  }

  /**
   * retrieve a SectionControlSystemModel based on given id
   * @param key is id of system
   */
  def findById(key:String):Option[Versioned[System]]={
    getConfig(key)
  }

  /**
   * checks of a given systemId exists
   * @param key is id of system
   */
  def exists(key:String):Boolean={
    curator.exists(Path("/ctes") / "systems" / key)
  }

  def getLabels(systemId:String):Seq[Label]={
    val path = Path("/ctes/labels")
    val labels = curator.get[List[Label]](path).getOrElse(List[Label]())
    labels.filter(_.nodePath == Systems.getRootPath(systemId).toString())
  }

  /**
   * retrieve multiple ControlSystems based on given ids
   * @param keys are the system ids
   */
  def findByIds(keys:Seq[String]):Seq[Versioned[System]]={
    keys.foldLeft(List[Versioned[System]]()){
      (list, key) =>
        findById(key).map{system => system::list}.getOrElse(list)
    }
  }

  /**
   * delete a SectionControlSystem from zookeeper
   * @param key if of system
   * @param version of system to delete
   */
  def delete(key:String, version:Int)(implicit user:UserModel):Boolean={
    val path = Path("/ctes") / "systems" / key
    Systems.findById(key).exists(d =>
      if (Systems.isEditable(key) && version == d.version) {
        curator.deleteRecursive(path)
        Users.removeSystemFromUsers(key)
        true
      } else {
        false
      })
  }

  /**
   * delete a SectionControlSystem from zookeeper
   * @param key if of system
   * @param version of system to delete
   */
  def deleteAnyway(key:String, version:Int)(implicit user:UserModel):Boolean={
    val path = Path("/ctes") / "systems" / key
    Systems.findById(key).exists(d =>
      if (version == d.version) {
        curator.deleteRecursive(path)
        Users.removeSystemFromUsers(key)
        true
      } else {
        false
      })
  }

  def updateField(systemId:String, field:Symbol, value:Any){
    Systems.getConfig(systemId).map{
      system =>
        val (config, version) = (field match {
          case 'nrDaysRemindCaseFiles =>
            system.data.copy(retentionTimes=system.data.retentionTimes.copy(nrDaysRemindCaseFiles = value.toString.toInt))
          case _ => system.data
        }, system.version)

        curator.set(Systems.getConfigPath(systemId), config, version)
    }
  }

  /**
   *
   * @param systemId
   * @param violationPrefixId
   * @return
   */
  def violationPrefixIdIsUnique(systemId:Option[String], violationPrefixId:Option[String]):Option[FieldError]={

    val alreadyExists = (systemId, violationPrefixId) match{
      case (Some(id), Some(prefix)) => Systems.getAll.exists(system => system.data.id != id && system.data.violationPrefixId == prefix)
      case (None, Some(prefix)) => Systems.getAll.exists(system => system.data.violationPrefixId == prefix)
      case _ => false
    }

    if(alreadyExists)
      Some(FieldError(field="violationPrefixId", error=Messages("system.validate.violationPrefixId_exists")))
    else
      None
  }

  /**
   * update a SectionControlSystem in zookeeper
   * @param systemId id of system
   * @param model to update system with
   * @return ResponseMsg with result of updating a system
   */
  def updateTechnical(systemId:String, model:SystemModelCRUDTechnical)(implicit user:UserModel):ResponseMsg={
    var errors = List[FieldError]()
    val sectionId = Some(systemId).filter(_.trim.nonEmpty)

    systemMustBeInEditMode(systemId).map{errors ::= _ }
    betweenRange(value=model.maxSpeed, from=20, to=250, fieldKey=Some("maxSpeed")).map{errors ::= _ }
    notEmpty(value=model.serialNumber, fieldKey="serialNumber",errorKey="system.validate.serialNumber-is-empty").map{errors ::= _ }
    notEmpty(value=model.region, fieldKey="region",errorKey="system.validate.region-is-empty").map{errors ::= _ }

    if(model.compressionFactor < 0 || model.compressionFactor > 100){
      errors ::= FieldError(field="compressionFactor", error=Messages("system.validate.compressionFactor"))
    }

    if(errors.length > 0){
      ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors.toList)
    }else{
      val sec: Option[Versioned[System]] = getConfig(sectionId.get)

      sec match{
        case Some(versionOf) =>
          try{
            val updatedSec = versionOf.data.copy(
                title=model.title,
                location = versionOf.data.location.copy(description = model.description,region = model.region,roadNumber = model.roadNumber),
                maxSpeed = model.maxSpeed,
                compressionFactor = model.compressionFactor,
                orderNumber = model.orderNumber,
                serialNumber = SerialNumber(serialNumber = model.serialNumber,serialNumberLength = model.serialNumberLength,serialNumberPrefix = model.serialNumberPrefix)
            )
            curator.set(Path("/ctes/systems/%s".format(sectionId.get))/"config", updatedSec, model.version.getOrElse(0))

            ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.update"))
          }catch{
            case e:BadVersionException =>
              ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.failed.update_version_conflict"))
            case e:Exception =>
              ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.failed.update"))
          }
        case  _ =>
          ResponseMsg(typeName=ResponseType.System, description=Messages("No system found"))
      }
    }
  }


  /**
   * update a SectionControlSystem in zookeeper
   * @param systemId id of system
   * @param model to update system with
   * @return ResponseMsg with result of updating a system
   */
  def update(systemId:String, model:SystemModelCRUD)(implicit user:UserModel):ResponseMsg={
    var errors = List[FieldError]()
    val sectionId = Some(systemId).filter(_.trim.nonEmpty)

    betweenRange(value=model.pardonTimeEsa_secs, from=0, to=60, fieldKey=Some("pardonTimeEsa_secs")).map{errors ::= _ }
    betweenRange(value=model.pardonTimeTechnical_secs, from=0, to=60, fieldKey=Some("pardonTimeTechnical_secs")).map{errors ::= _ }

    if(model.nrDaysKeepViolations < 1 || model.nrDaysKeepViolations > 5){
      errors ::= FieldError(field="nrDaysKeepViolations", error=Messages("system.validate.nrDaysKeepViolations"))
    }

    if(model.nrDaysKeepTrafficData < 1 || model.nrDaysKeepTrafficData > 8){
      errors ::= FieldError(field="nrDaysKeepTrafficData", error=Messages("system.validate.nrDaysKeepTrafficData"))
    }

    if(errors.length > 0){
      ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors.toList)
    }else{
      val sec: Option[Versioned[System]] = getConfig(sectionId.get)

      sec match{
        case Some(versionOf) =>
          try{
            val updatedSec = versionOf.data.copy(
                pardonTimes=ZkSystemPardonTimes(pardonTimeEsa_secs=model.pardonTimeEsa_secs,pardonTimeTechnical_secs=model.pardonTimeTechnical_secs),
                retentionTimes= SystemRetentionTimes(
                  nrDaysKeepViolations=model.nrDaysKeepViolations,
                  nrDaysRemindCaseFiles= versionOf.data.retentionTimes.nrDaysRemindCaseFiles,
                  nrDaysKeepTrafficData=model.nrDaysKeepTrafficData
                )
            )
            curator.set(Path("/ctes/systems/%s".format(sectionId.get))/"config", updatedSec, model.version.getOrElse(0))

            ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.update"))
          }catch{
            case e:BadVersionException =>
              ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.failed.update_version_conflict"))
            case e:Exception =>
              ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.failed.update"))
          }
        case  _ =>
          ResponseMsg(typeName=ResponseType.System, description=Messages("No system found"))
      }
    }
  }

  /**
   * retrieve all systems that current user has permission for
   */
  def getAll:Seq[Versioned[System]]={
    Systems.getAllSystems
  }

  /**
   * retrieve all systems
   */
  def getAllSystems:Seq[Versioned[System]]={

    var systems = List[Versioned[System]]()
    if(curator.exists(Path("/ctes") / "systems")){
      val allSystems = curator.getChildren(Path("/ctes") / "systems")
      allSystems.foreach{ system =>
        if(curator.exists(system / "config")){
          val versionOf = curator.getVersioned[System](system / "config")
          versionOf.foreach { versionOfItem =>
            systems ::= versionOfItem
          }
        }
      }
    }
    systems
  }

  /**
   * checks if a given system has the correct state to be editable
   * @param systemId id of system
   * @return true if the system is in state Off or StandBy else false
   */
  def isEditable(systemId:String):Boolean={
    import SystemStateType._
    getSystemStateBuilder(systemId).getSystemState.exists(s =>
        s == Off ||
        s == StandBy ||
        s == Maintenance ||
        s == Failure) && !isCalibrating(systemId)
  }

  def getWebStates(systemId:String):WebSystemState={
    getSystemStateBuilder(systemId).getSystemWebState
  }

  private def getSystemStateBuilder(systemId:String):SystemStateBuilder={
    new SystemStateBuilder(systemId,new StateMapper(),new StatePicker(new StateMapper()))
  }
}
