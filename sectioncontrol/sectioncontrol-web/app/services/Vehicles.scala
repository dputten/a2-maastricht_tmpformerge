/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package services

import common.hbase.utils._
import common.{Config, DefaultSettings}
import csc.curator.utils.Curator
import csc.hbase.utils.HBaseTableKeeper
import csc.imageretriever.ImageKey
import csc.sectioncontrol.decorate.{DecorateService, ImageAndContents}
import csc.sectioncontrol.messages
import csc.sectioncontrol.messages.{MetadataVersion, VehicleImageType, VehicleMetadata}
import csc.sectioncontrol.storage.Decorate.DecorationType
import csc.sectioncontrol.storagelayer.hbasetables.{RowKeyDistributorFactory, VehicleImageTable, VehicleTable}
import net.liftweb.json._
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.util.Bytes
import csc.sectioncontrol.storage.HBaseTableDefinitions._
import csc.akkautils.DirectLogging

/**
 * Main entry point for retrieving vehicles from hbase
 */
object Vehicles extends Config with HBaseConnection with Storage with DirectLogging{
  type HbaseImageKey = (String, Long, String)

  private def makeSpeedRecordJsonReader(systemId: String) = {
    new JsonHBaseReader[Long, VehicleSpeedRecord] {
      def hbaseReaderTable = new HTable(hbaseConfig, "speedRecord")
      def hbaseReaderDataColumnFamily = "V"
      def hbaseReaderDataColumnName = "SR"
      def makeReaderKey(keyValue: Long) = Bytes.toBytes("%s-%s".format(systemId, keyValue.toString))

      private val optionFields = List("exit", "extLength")
      /**
       * Deserialize the byte array into the target type T using lift-json serialization.
       */
      override def deserialize(bytes: Array[Byte])(implicit mt: Manifest[VehicleSpeedRecord]) = {
        val json = parse(Bytes.toString(bytes))
        val jsonTransformed = optionFields.foldLeft(json) {
          (jValue, fieldName) =>
            val isEmpty = json.find {
              case JField(`fieldName`, _) => true
              case _ => false
            }.isEmpty

            if (isEmpty) jValue ++ JField(fieldName, JNull) else jValue
        }
        super.deserialize(compact(render(jsonTransformed)).getBytes)
      }
    }
  }

  /**
   * returns all vehicles and it's pictures (overview and license) from
   * storage that are recorded between the given range
   * @param from a long representing a from date
   * @param to a long representing a to date
   * @param withPhotos indicate if photos must be retrieved as well
   * @return all vehicles and it's pictures (overview and license) from
   */
  def getByTime(systemId: String, gantryId: String, from:Long, to:Long, withPhotos:Boolean,licensePlate:String):Seq[Vehicle] = {
    log.debug("getByTime")
    RowKeyDistributorFactory.init(curator)
    val imagetableKeeper = new HBaseTableKeeper {}
    val imagetableKeeper1 = new HBaseTableKeeper {}
    val overviewDecorator = new OverviewDecorator(curator, systemId)

    val reader1: VehicleImageTable.Reader = VehicleImageTable.makeReader(hbaseConfig: Configuration, imagetableKeeper: HBaseTableKeeper,VehicleImageType.Overview)
    val reader2: VehicleImageTable.Reader = VehicleImageTable.makeReader(hbaseConfig: Configuration, imagetableKeeper1: HBaseTableKeeper,VehicleImageType.License)

    try {
      val prefix = systemId + "-" + gantryId + "-"
      val vehiclesMetaData: Seq[messages.VehicleMetadata] = getMetadataForVehicles(systemId, gantryId, from, to,licensePlate)

      vehiclesMetaData.foldLeft(List[Vehicle]()) {
        (list, metaData) =>
          val imageKey: HbaseImageKey = (prefix, metaData.eventTimestamp, "-" + metaData.lane.name)
          val hasLicense = metaData.license.isDefined
          Vehicle(
            metadata = null,
            metadataNew=metaData,
            overview =
              if (withPhotos) {
                overviewDecorator.decorate(metaData, reader1.readRow(imageKey))
              }
              else None,
            license =
              if (withPhotos && hasLicense) {
                reader2.readRow(imageKey)
              }
              else None
            ) :: list
      }
    } finally {
      imagetableKeeper.closeTables()
      imagetableKeeper1.closeTables()
    }
  }

  /**
   * @param systemId id of the system
   * @param gantryId id of the gantry vehicles have passed
   * @param from     start of time window
   * @param to       end of time window
   * @return         the keys of the images that are not yet downloaded from the gantry
   */
  def getImageKeysForVehiclesWithMissingImages(
    systemId: String, gantryId: String, from: Long, to: Long,license:String,
    metadataProvider: (String, String, Long, Long,String) => Seq[messages.VehicleMetadata] = getMetadataForVehicles,
    imageProvider: HbaseImageKey => Option[vehicleImageRecordType] = getOverviewImage
  ): Seq[ImageKey] = {
    log.debug("getImageKeysForVehiclesWithMissingImages")
    def toHbaseImageKey(vehicleMetadata: messages.VehicleMetadata): HbaseImageKey = {
      val prefix = systemId + "-" + gantryId + "-"
      (prefix, vehicleMetadata.eventTimestamp, "-" + vehicleMetadata.lane.name)
    }

    def toImageKey(vehicleMetadata: messages.VehicleMetadata): ImageKey = {
      val overviewImage = vehicleMetadata.getImage(VehicleImageType.Overview)
      ImageKey(
        uri = overviewImage.map(_.uri).getOrElse(""),
        time = vehicleMetadata.eventTimestamp,
        imageType = VehicleImageType.Overview,
        systemId = systemId,
        gantry = gantryId,
        lane = vehicleMetadata.lane.name
      )
    }

    metadataProvider(systemId, gantryId, from, to,license)
      .filter(metadata => imageProvider(toHbaseImageKey(metadata)).isEmpty)
      .map(toImageKey)
  }

  /**
   * Returns the metadata for all vehicles passing a gantry given a specific time window
   *
   * @param systemId id of the system
   * @param gantryId id of the gantry vehicles have passed
   * @param from     start of time window
   * @param to       end of time window
   * @return         a sequence of metadata for each vehicle that passed during the time window
   */

  private def getMetadataForVehicles(systemId: String, gantryId: String, from: Long, to: Long,license:String): Seq[messages.VehicleMetadata] = {
    log.debug("getMetadataForVehicles: systemId:%s , gantryId:%s , from:%d , to:%d ,license:%s".format(systemId, gantryId, from, to,license))
    RowKeyDistributorFactory.init(curator)
    val tableKeeper = new HBaseTableKeeper {}
    val reader = VehicleTable.makeReader(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper)
    try {
        val result=reader.readRows(systemId, gantryId, license.trim, from, to)
        log.debug("result from reader.readRows(systemId:%s, gantryId:%s, license:%s, from:%d, to:%d)".format(systemId, gantryId,license, from, to) + result)

        result
    } finally {
      tableKeeper.closeTables()
    }
  }

  private def getOverviewImage(imageKey: HbaseImageKey): Option[vehicleImageRecordType] = {
    RowKeyDistributorFactory.init(curator)
    val tableKeeper = new HBaseTableKeeper {}
    val reader: VehicleImageTable.Reader = VehicleImageTable.makeReader(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper, VehicleImageType.Overview)
    try {
      reader.readRow(imageKey)
    } finally {
      tableKeeper.closeTables()
    }
  }

  def getSpeedRecords(systemId: String, from:Long, to:Long):Seq[VehicleSpeedRecord] = {
    val jsonReader = makeSpeedRecordJsonReader(systemId)
    try {
      jsonReader.readRows(from, to)
    } finally {
      jsonReader.hbaseReaderTable.close()
    }
  }
}

class OverviewDecorator(curator: Curator, systemId: String) {

  val decorateService = new DecorateService(curator, systemId)
  val compressionFactor = DefaultSettings.System.compressionFactor

  def decorate(msg: VehicleMetadata, contents: Option[Array[Byte]]): Option[Array[Byte]] = contents match {
    case Some(data) => {
      if (requiresDecoration(msg)) {
        val imageAndContents = ImageAndContents(msg.getImage(VehicleImageType.Overview), Some(data))
        val jpegQuality = if (compressionFactor > 0) compressionFactor else 80

        //decorate
        decorateService.getDecoratedImage(msg, DecorationType.Overview_1, imageAndContents, None, jpegQuality)
      } else {
        //doesn't require decoration: return original
        contents
      }
    }
    case None => None
  }

  def requiresDecoration(msg: VehicleMetadata): Boolean = msg.version match {
    case Some(version) => version >= MetadataVersion.withLicensePlateData.id
    case None => false
  }

}