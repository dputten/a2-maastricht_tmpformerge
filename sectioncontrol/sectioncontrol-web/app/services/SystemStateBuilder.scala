package services

import csc.curator.utils.Versioned
import models.storage.{Corridor, CorridorState}
import models.web.{CorridorStateContainer, SystemStateType}
import models.web.SystemStateType.{SystemStateType, _}
import play.api.i18n.Messages
import org.slf4j.LoggerFactory

case class WebSystemState(name: String, drivingDirection: String, state: String, isCalibrating: Boolean, corridorStates: Seq[WebCorridorState])

case class WebCorridorState(corridorId: String, name: String, state: String, systemState: String, drivingDirection: String, version: Int)

/**
 * A class to redude the number of states
 */
class StateMapper() {
  // Map a state to the clientstate
  def mapToClientState(state: SystemStateType): String = {
    import SystemStateType._
    state match {
      case Off => Messages("systems.form.state.off")
      case StandBy => Messages("systems.form.state.standby")
      case Failure => Messages("systems.form.state.failure")
      case EnforceOn => Messages("systems.form.state.enforceon")
      case Maintenance => Messages("systems.form.state.maintenance")
      case _  => ""
    }
  }
}

/**
 * A class to determine the state of the system based on the different corridor states.
 */
class StatePicker(stateMapper: StateMapper) {
  def getState(corridorStates: Seq[CorridorStateContainer]): Option[SystemStateType] = {
    getHandhaven(corridorStates) orElse getStoring(corridorStates) orElse getStandBy
  }

  private def getHandhaven(corridorStates: Seq[CorridorStateContainer]): Option[SystemStateType] = {
    (getEnforceOn(corridorStates) orElse getEnforceOff(corridorStates) orElse getEnforceDegradedOn(corridorStates)).map(p=>EnforceOn)
  }

  private def getStoring(corridorStates: Seq[CorridorStateContainer]): Option[SystemStateType] = {
    val result = corridorStates.filter(a => a.state == Maintenance || a.state == Off || a.state == Failure || (a.state == EnforceDegraded && a.reductionFactor == 100))
    val x: Int = corridorStates.length

    result.length match {
      case `x` => getMaintenance(corridorStates) orElse getOff(corridorStates) orElse getFailure
      case _ => None
    }
  }

  private def getStandBy: Option[SystemStateType] = {
    Some(StandBy)
  }

  private def getFailure: Option[SystemStateType] = {
    Some(Failure)
  }

  private def getMaintenance(coridorStates: Seq[CorridorStateContainer]): Option[SystemStateType] = {
    coridorStates.map(_.state).contains(Maintenance) match {
      case true => Some(Maintenance)
      case false => None
    }
  }

  private def getOff(coridorStates: Seq[CorridorStateContainer]): Option[SystemStateType] = {
    val result = coridorStates.filter(a => a.state == Off)
    val x: Int = coridorStates.length

    result.length match {
      case `x` => Some(Off)
      case _ => None
    }
  }

  private def getEnforceOn(coridorStates: Seq[CorridorStateContainer]): Option[SystemStateType] = {
    coridorStates.map(_.state).contains(EnforceOn) match {
      case true => Some(EnforceOn)
      case false => None
    }
  }

  private def getEnforceOff(coridorStates: Seq[CorridorStateContainer]): Option[SystemStateType] = {
    coridorStates.map(_.state).contains(EnforceOff) match {
      case true => Some(EnforceOff)
      case false => None
    }
  }

  private def getEnforceDegradedOn(coridorStates: Seq[CorridorStateContainer]): Option[SystemStateType] = {
    coridorStates.find(s => s.state == EnforceDegraded) match {
      case Some(state) if state.reductionFactor < 100 => Some(EnforceDegraded)
      case other => None
    }
  }

  // reduce the number off states to the ones we use for the client
  def mapToState(s: CorridorStateContainer): SystemStateType = {
    import SystemStateType._
    (s.state, s.reductionFactor) match {
      case (Off, _) => Off
      case (StandBy, _) => StandBy
      case (Failure, _) => Failure
      case (EnforceOff, _) | (EnforceOn, _) => EnforceOn
      case (EnforceDegraded, 100) => Failure
      case (EnforceDegraded, x) if x < 100 => EnforceOn
      case (Maintenance, x) => Maintenance
    }
  }

}

/**
 * A class to create the system state and corridor states
 */
class SystemStateBuilder(systemId: String, stateMapper: StateMapper, systemStatePicker: StatePicker) extends Storage {

  def getSystemState: Option[SystemStateType] = {
    val corridors = Corridors.getAllBySystemId(systemId)
    systemStatePicker.getState(getCorridorStates(corridors))
  }

  def getSystemWebState: WebSystemState = {
    val corridors = Corridors.getAllBySystemId(systemId)
    val webCorridorStates = corridors.map(versioned => createWebCorridorState(versioned))
    val systemState = systemStatePicker.getState(getCorridorStates(corridors)).get

    WebSystemState(Messages("systems.form.state.all"),
      Messages("systems.form.state.all"),
      stateMapper.mapToClientState(systemState),
      Systems.isCalibrating(systemId),
      webCorridorStates)
  }

  private def getCorridorStates(corridors: Seq[Versioned[Corridor]]): Seq[CorridorStateContainer] = {
    corridors.map(versioned => {
      getCorridorState(versioned.data)
    })
  }

  private def getCorridorState(corridor: Corridor): CorridorStateContainer = {
      val versionedState = curator.getVersioned[CorridorState](Corridors.getStatePath(systemId, corridor.id))
      val state = SystemStateType.withName(versionedState.get.data.state)
      val reductionFact = Corridors.getReductionFactor(systemId, corridor.id)
      new CorridorStateContainer(state, reductionFact, versionedState.get.version)
  }

  private def createWebCorridorState(versioned: Versioned[Corridor]): WebCorridorState = {
    // The state for the corridor
    val versionedState = getCorridorState(versioned.data)

    // The sections belonging to the corridor
    val startSection = Sections.getConfig(systemId, versioned.data.startSectionId)
    val endSection = Sections.getConfig(systemId, versioned.data.endSectionId)

    val startGantry = Gantries.getConfig(systemId, startSection.get.data.startGantryId)
    val endGantry = Gantries.getConfig(systemId, endSection.get.data.endGantryId)

    val stateType = systemStatePicker.mapToState(versionedState)
    val state = stateMapper.mapToClientState(stateType)

    WebCorridorState(versioned.data.id, versioned.data.name, state, versionedState.state.toString, startGantry.get.data.hectometer + " - " + endGantry.get.data.hectometer, versionedState.version)
  }
}
