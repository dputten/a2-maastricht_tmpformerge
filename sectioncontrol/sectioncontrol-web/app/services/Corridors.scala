/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package services

import csc.config.{VersionOf}
import common.Extensions._
import models.web._
import models.storage._
import common.{Formats, SystemLogger, Validates}
import csc.curator.utils.Versioned
import play.api.i18n.Messages
import play.api.Logger
import csc.sectioncontrol.storagelayer.alerts.AlertService
import scala.Some
import models.storage.Gantry
import models.storage.Section
import models.storage.SystemState
import csc.curator.utils.Versioned
import models.storage.Corridor
import models.web.SystemStateType._
import scala.Some
import models.storage.Gantry
import models.storage.Section
import models.storage.SystemState
import csc.curator.utils.Versioned
import models.storage.Corridor
import scala.Some
import models.storage.Gantry
import models.storage.Section
import models.storage.SystemState
import csc.curator.utils.Versioned
import models.storage.Corridor
import models.web.ResponseMsg
import scala.Some
import models.web.SystemStateType
import models.storage.Gantry
import models.web.CorridorModelCRUD
import models.storage.Section
import models.web.UserModel
import models.web.FieldError
import models.storage.SystemState
import csc.curator.utils.Versioned
import models.web.Direction
import models.storage.Corridor
import models.web.ResponseMsg
import scala.Some
import models.web.SystemStateType
import models.storage.Gantry
import models.web.CorridorModelCRUD
import models.storage.Section
import models.web.UserModel
import models.web.FieldError
import models.storage.SystemState
import csc.curator.utils.Versioned
import models.web.Direction
import models.storage.Corridor
import models.web.ResponseMsg
import scala.Some
import models.web.SystemStateType
import models.storage.Gantry
import models.web.CorridorModelCRUD
import models.storage.Section
import models.web.UserModel
import models.web.FieldError
import models.storage.SystemState
import csc.curator.utils.Versioned
import models.web.Direction
import models.storage.Corridor
import models.web.ResponseMsg
import scala.Some
import models.web.SystemStateType
import models.storage.Gantry
import models.web.CorridorModelCRUD
import models.storage.Section
import models.web.UserModel
import models.web.FieldError
import models.storage.SystemState
import csc.curator.utils.Versioned
import models.web.Direction
import models.storage.Corridor
import models.web.ResponseMsg
import scala.Some
import models.web.SystemStateType
import models.storage.Gantry
import models.web.CorridorModelCRUD
import models.storage.Section
import models.web.UserModel
import models.web.FieldError
import models.storage.SystemState
import csc.curator.utils.Versioned
import models.web.Direction
import models.storage.Corridor
import models.web.ResponseMsg
import scala.Some
import models.web.SystemStateType
import models.web.SystemStateType.SystemStateType
import models.storage.Gantry
import models.web.CorridorModelCRUD
import models.storage.Section
import models.web.UserModel
import models.web.FieldError
import models.storage.SystemState
import csc.curator.utils.Versioned
import models.web.Direction
import models.storage.Corridor

/**
 * Used for CRUD actions on Corridor objects
 */
object Corridors extends Validates with SystemLogger with Storage{

  /**
   * return the default path where all gantries reside
   * @param systemId is id of system
   * @return Path to corridors
   */
  def getDefaultPath(systemId:String) = Systems.getDefaultPath / systemId / "corridors"

  /**
   * return the root path of given corridorId
   * @param systemId is id of system
   * @param corridorId is id of corridor
   * @return Path to specific corridor
   */
  def getRootPath(systemId:String, corridorId:String) = getDefaultPath(systemId) / corridorId

  /**
   * return the path of the config leaf
   * @param systemId is id of system
   * @param corridorId is id of corridor
   * @return Path to specific corridor config leaf
   */
  def getConfigPath(systemId:String, corridorId:String) = getRootPath(systemId, corridorId) / "config"

  /**
   * return the path of the config leaf
   * @param systemId is id of system
   * @param corridorId is id of corridor
   * @return Path to specific corridor config leaf
   */
  def getStatePath(systemId:String, corridorId:String) = csc.sectioncontrol.storage.Paths.State.getCurrentPath(systemId,corridorId)

  /**
   * checks if a corridor by a given systemId and corridorId exists
   * @param systemId is id of system
   * @param corridorId is id of corridor
   * @return Boolean that indicates if a corridor path exists
   */
  def exists(systemId:String, corridorId:String) = curator.exists(getRootPath(systemId, corridorId))


  /**
   * retrieve config from zookeeper
   * @param systemId is id of system
   * @param corridorId is id of corridor
   * @return VersionOf[Corridor]
   */
  def getConfig(systemId:String, corridorId:String) = curator.getVersioned[Corridor](getConfigPath(systemId, corridorId))

  def getReductionFactor(systemId:String, corridorId: String):Int={

    (AlertService.getReductionFactor(curator, systemId, corridorId)*100+0.5).toInt
  }

  /**
   * retrieve state from storage
   * @param systemId is id of system
   */
  def getState(systemId:String,corridorId:String):Option[Versioned[SystemState]]={
    curator.getVersioned[SystemState](getStatePath(systemId,corridorId))
  }

  /**
   * retrieve a Corridor based on given id
   * @param systemId is id of system
   * @param corridorId is id of corridor
   * @return optional VersionOf[Corridor]
   */
  def findById(systemId:String, corridorId:String) = getConfig(systemId, corridorId)

  case class GantryForList(gantryId:String,hectometer:String,sectionId:String)

  def getGantriesBySystemId(systemId:String):Seq[Versioned[Gantry]]={
    var gantries = List[Versioned[Gantry]]()
    val allSystemPaths = Systems.getAllPaths.filter{p => p.nodes.reverse.head.name == systemId}

    allSystemPaths.foreach{ systemPath =>
      val pathToGantries = systemPath / "gantries"
      if(curator.exists(pathToGantries)){
        val allGantryPaths = curator.getChildren(pathToGantries)
        allGantryPaths.foreach{ gantryPath =>
          val versionOfGantry = curator.getVersioned[Gantry](gantryPath / "config")
          if(versionOfGantry.isDefined)
            gantries ::= versionOfGantry.get
        }
      }
    }
    gantries
  }

  def getAvailableBeginSections(systemId:String,corridorFirstSectionId:String):Seq[GantryForList]={
    // Get the sections in the system
    val sections=Sections.getAllBySystemId(systemId).sortBy(_.data.name)

    // Get all corridors in the system and map to a list to get the startSectionId's
    val corridors =getAllBySystemId(systemId).map(_.data.startSectionId)

    // Get all sections available as startsection for corridors. These are sections not already in use.
    val availableSections=sections.filterNot(section=>corridors.contains(section.data.id))
    val gantries=getGantriesBySystemId(systemId);

    // Loop through the result sections and get the gantries
    val gantriesForList: Seq[GantryForList] = availableSections.map(
      section => {
        val g = gantries.find(x => x.data.id == section.data.startGantryId)
        new GantryForList(g.get.data.id, g.get.data.hectometer, section.data.id)
      })

    // Als er al een firstsection is haal dan de bijbehorenden gantry op want die mag wel
    if(corridorFirstSectionId!="")
    {
      Sections.findById(systemId,corridorFirstSectionId).map{
        currentSection =>{
          val gantry = Gantries.getConfig(systemId, currentSection.data.startGantryId)
          gantry match{
            case Some(value) =>
              val gantryForLists=gantriesForList:+new GantryForList(value.data.id,value.data.hectometer,currentSection.data.id)
              return gantryForLists
            case None => Seq()
          }
        }
      }.getOrElse(List())
    }

    return gantriesForList

  }

  def getAvailableEndSections(systemId:String,corridorFirstSectionId:String):Seq[GantryForList]={
    val x=Sections.findById(systemId,corridorFirstSectionId).map{
      currentSection =>
        val sections = Sections.getAllBySystemId(systemId)
        getConnectedSections(true, currentSection, sections).sortBy(_.data.name)
    }.getOrElse(List())

    // Get the sections in the system
    val sections=Sections.getAllBySystemId(systemId).sortBy(_.data.name)

    // Get all corridors in the system and map to a list to get the endSectionId's
    val usedEndSectionIds: Seq[Any] =getAllBySystemId(systemId).map(_.data.endSectionId)

    // Get all sections available as endsection for corridors. These are sections not already in use.
    var availableSections=sections.filterNot(section=>usedEndSectionIds.contains(section.data.id))
    availableSections=x

    val gantriesAvailableForSystem=getGantriesBySystemId(systemId)

    // Loop through the result sections and get the gantries
    val gantriesForList: Seq[GantryForList] =availableSections.map(
      section=>{
        val g=gantriesAvailableForSystem.find(x=>x.data.id==section.data.endGantryId)
        new GantryForList(g.get.data.id,g.get.data.hectometer,section.data.id)
      })

    return gantriesForList

  }
  /**
   * retrieve sections that do not have a corridor
   * Get the sections that can be used as the first section of a corridor, that are not
   * already used in a corridor
   * @param systemId is id of system
   * @return a sequence of VersionOf[Section]
   */
  def getAvailableFirstSections(systemId:String):Seq[GantryForList]={
    val sections=Sections.getAllBySystemId(systemId).sortBy(_.data.name)
    val gantries=getGantriesBySystemId(systemId)

    // Loop through the result sections and get the gantries
    sections.map(
      section => {
        val g = gantries.find(x => x.data.id == section.data.startGantryId)
        new GantryForList(g.get.data.id, g.get.data.hectometer, section.data.id)
      })
  }
  /**
   * retrieve sections that do not have a corridor
   * Get the sections that can be used as the second section of a corridor (given the first section id), 
   * that are not already used in a corridor
   * @param systemId is id of system
   * @param sectionId is id of the first section
   * @return a sequence of VersionOf[Section]
   */
  def getAvailableSecondSections(systemId:String, sectionId:String)(implicit user:UserModel):Seq[GantryForList]={
    val sections=Sections.findById(systemId,sectionId).map{
      currentSection =>
        val sections = Sections.getAllBySystemId(systemId)
        getConnectedSections(true, currentSection, sections).sortBy(_.data.name)
    }.getOrElse(List())

    val gantries=getGantriesBySystemId(systemId)

    // Loop through the result sections and get the gantries
    sections.map(
      section => {
        val g = gantries.find(x => x.data.id == section.data.endGantryId)
        new GantryForList(g.get.data.id, g.get.data.hectometer, section.data.id)
      })
  }

  /**
   * retrieve corridor by section id
   * @param systemId is id of system
   * @param sectionId is id of the first section
   * @return a optional VersionOf[Corridor]
   */
  def getBySectionId(systemId:String, sectionId:String)(implicit user:UserModel):Option[Versioned[Corridor]]={
    val corridors = Corridors.getAllBySystemId(systemId)
    corridors.find{_.data.allSectionIds.contains(sectionId)}
  }

  /**
   * retrieve all corridors by a given system id
   * @param systemId is id of system
   * @return a sequence of VersionOf[Corridor] or an empty list
   */
  def getAllBySystemId(systemId:String):Seq[Versioned[Corridor]]={
    curator.exists(Corridors.getDefaultPath(systemId)) match {
      case true =>
        val allCorridorsPaths = curator.getChildren(getDefaultPath(systemId))
        allCorridorsPaths.foldLeft(List[Versioned[Corridor]]()){
          (list, corridorPath) =>
            curator.getVersioned[Corridor](corridorPath / "config").map(_::list).getOrElse(list) // :: list
        }
      case false => List()
    }
  }

  /**
   * retrieve all corridors by a given system id
   * @param systemId is id of system
   * @return a sequence of VersionOf[Corridor] or an empty list
   */
  def isSectionInUseByCorridor(systemId:String,sectionId:String):Boolean={
    getAllBySystemId(systemId).map(c=>c.data.allSectionIds.contains(sectionId)).contains(true)
  }

  /**
   * delete a Section from zookeeper
   * @param systemId is id of system
   * @param corridorId is id of corridor
   * @return a true indicating deleting was successful else a false
   */
  def delete(systemId:String, corridorId:String, version:Long)(implicit user:UserModel):Boolean={
    if(Systems.isEditable(systemId)){
      val path = getRootPath(systemId, corridorId)

      //TODO Check is handhaving bestaat met section. zoja niet verwijderen
      if(curator.exists(path)){
        log(messageId="logging.corridors.delete", systemId=systemId, List(corridorId))
        curator.deleteRecursive(path)
        true
      }else{
        false
      }
    }else{
      false
    }
  }

  private def isValidRoadType(roadType:Int):Boolean=List(1,2,3,5).exists(_==roadType)

  /**
   * validates a given model
   * @param systemId is id of system
   * @param model
   * @return a sequence of FieldErrors of an error occured else a empty list
   */
  private def validate(systemId:String, model:CorridorModelCRUD):List[FieldError]={
    var errors = List[FieldError]()

    systemMustBeInEditMode(systemId).map{errors ::= _ }

    if(!isValidRoadType(model.roadType)){
      errors ::= FieldError(field="roadType", error=Messages("corridors.validate.roadType_not_valid"))
    }

    if(model.locationLine1 == null || model.locationLine1.trim().length() == 0){
      errors ::= FieldError(field="locationLine1", error=Messages("corridors.validate.locationLine1_is_empty"))
    } else if(model.locationLine1.length < 1 || model.locationLine1.length > 45){
      errors ::= FieldError(field="locationLine1", error=Messages("corridors.validate.locationLine1_is_empty"))
    }

    if(model.locationLine2 != null && model.locationLine2.trim.length > 45){
      errors ::= FieldError(field="locationLine2", error=Messages("corridors.validate.locationLine2_is_to_long"))
    }

    if(!model.locationCode.matches("[0-9 ]{5}")){
      errors ::= FieldError(field="locationCode", error=Messages("corridors.validate.locationCode"))
    }

    if(!Sections.exists(systemId, model.startSectionId)){
      errors ::= FieldError(field="sectionIds", error=Messages("corridors.validate.systemId_and_startSectionId_not_exist"))
    }

    if(!Sections.exists(systemId, model.endSectionId)){
      errors ::= FieldError(field="sectionIds", error=Messages("corridors.validate.systemId_and_endSectionId_not_exist"))
    }
    errors
  }

  /**
   * updates a corridor
   * @param systemId id of system
   * @param corridorId id of corridor
   * @param model
   * @return
   */
  def update(systemId:String, corridorId:String, model:CorridorModelCRUD)(implicit user:UserModel):ResponseMsg={
    var errors = validate(systemId, model)

    if(model.name == null || model.name.trim.length == 0){
      errors ::= FieldError(field="name", error=Messages("corridors.validate.name_is_empty"))
    }

    if(corridorId == null || corridorId.trim().length() == 0 || !Corridors.exists(systemId, corridorId)){
      errors ::= FieldError(field="corridorId", error=Messages("corridors.unkown_corridor"))
    }

    if(model.version.isEmpty){
      errors ::= FieldError(field="version", error=Messages("corridors.validate.no_version_received"))
    }

    if(errors.length > 0){
      ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors.toList)
    }else{
      try{

        val sections = Sections.getAllBySystemId(systemId)
        val startSection = Sections.findById(systemId, model.startSectionId).get
        val endSection = Sections.findById(systemId, model.endSectionId).get
        val betweenSections = getSectionsBetween(startSection, endSection, sections)

        val version = model.version.get
        Corridors.findById(systemId, corridorId).foreach{
          corridor =>
            curator.set(
              Corridors.getConfigPath(systemId, corridorId),

              corridor.data.copy(
                name = model.name,
                roadType = model.roadType,
                isInsideUrbanArea = model.isInsideUrbanArea,
                info = corridor.data.info.copy(
                  locationCode = model.locationCode,
                  locationLine1 = model.locationLine1,
                  locationLine2 = Some(model.locationLine2)
                ),
                direction=Direction(model.direction.directionFrom,model.direction.directionTo),
                radarCode=model.radarCode,
                dutyType=model.dutyType,
                deploymentCode=model.deploymentCode,
                codeText=model.codeText,
                roadCode=model.roadCode,
                startSectionId=model.startSectionId,
                endSectionId=model.endSectionId,
                allSectionIds = betweenSections.map(_.data.id)
              ),
              version
            )
        }
        ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.update"))
      }catch{
        case e:Exception =>
          log(
            messageId="logging.corridors.update.failed",
            systemId=systemId, List(corridorId))
          ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.success.failed"))
      }
    }
  }

  /**
   * retrieve sections that are connected between the first and second (last) section
   * @param first section
   * @param second second that
   * @param sections used to find connections point between the first en last section
   * @return sequence of sections that are connected between the first and last sections
   */
  private def getSectionsBetween(first:Versioned[Section], second:Versioned[Section], sections:Seq[Versioned[Section]]):Seq[Versioned[Section]]={
    if(first == second){
      List(first)
    }else{
      val start = getConnectedSections(true, first, sections)
      val end = getConnectedSections(false, second, sections)
      start.intersect(end)
    }
  }

  /**
   * used to retrieve the link between sections
   * @param forward indicates if links between section and sections should go from start => end or backwards (end => start)
   * @param section is the starting point of the search for linked sections
   * @param sections are the sections where the linked sections are looked for
   */
  private def getConnectedSections(forward:Boolean, section:Versioned[Section], sections:Seq[Versioned[Section]]):Seq[Versioned[Section]]={
    var sectionList = sections
    var result = List[Versioned[Section]]()
    var tmpResult = if(forward)
      sectionList.find{_.data.endGantryId == section.data.endGantryId}
    else
      sectionList.find{_.data.startGantryId == section.data.startGantryId}
    //TODO 16032012 RR->MJ: write as a fold(Left).
    while(tmpResult.isDefined){
      //TODO 16032012 RR->MJ:you cant just call get. You will get NPEs.
      //use foreach or map orElse or something like that.
      // also, that will take care of the fact that you dont do the .get many times. In the context of there beging something you do the code,
      // and you always know for certain you dont get NPEs.
      sectionList = sectionList.filterNot{_ == tmpResult.get}
      result ::= tmpResult.get
      tmpResult = if(forward)
        sectionList.find{_.data.startGantryId == tmpResult.get.data.endGantryId}
      else
        sectionList.find{_.data.endGantryId == tmpResult.get.data.startGantryId}
    }

    result
  }
}
