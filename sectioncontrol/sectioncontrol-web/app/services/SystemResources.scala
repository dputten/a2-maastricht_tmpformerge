/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package services

import common.Validates
import csc.config.{Path}
import models.storage.{Url}

/**
 * Main entry point for everything related to system resources
 */
object SystemResources extends Validates with Storage{
  /**
   * retrieve link to ganglia
   */
  def getLink:String={
    curator.get[Url](Configuration.getSystemResourcesGanglia).map(_.url).getOrElse("")
  }
}