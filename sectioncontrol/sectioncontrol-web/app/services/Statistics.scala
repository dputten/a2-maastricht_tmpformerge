/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package services

import java.util.Date

import common.{GZIPCompressor, SystemLogger}
import csc.config.Path
import csc.curator.utils.Versioned
import csc.json.lift.EnumerationSerializer
import models.storage.{DayStatistics, _}
import models.web.{SystemStateType, UserModel}
import net.liftweb.json.{DefaultFormats, Serialization}

import scala.collection.immutable
import scala.collection.mutable.ListBuffer
import scala.xml._
import csc.sectioncontrol.storagelayer.corridor.CorridorServiceCuratorImpl
import csc.akkautils.DirectLogging
import csc.sectioncontrol.storage.{ParallelCorridorConfig, Paths}
import org.slf4j.LoggerFactory


/**
 * Used for retrieving statistics
 */
object Statistics extends GZIPCompressor with SystemLogger with Storage {
  val logger = LoggerFactory.getLogger(getClass)
  val noConfig = "NoConfig"
  val noConfigCellResult = "config"

  /**
    * Internally used class, containing the statisics found in the daily report
    * for each HH-traject a sectie class is created
    * @param corridorName The corridor name releated to this HH-traject
    * @param corridorId A list of corridorIds releated to this HH-traject (multiple for parallelscetions)
    * @param ht The ht attribute of the HH-traject
    * @param sectieId the id attribute of the HH-traject
    * @param speedLimit the speedlimit of HH-traject
    * @param gemiddeld the average reported in the HH-traject
    * @param max the maximum speed reported in the HH-traject
    * @param matches the matches reported in the HH-traject
    * @param totaalInPassages the nr passages in reported in the HH-traject
    * @param totaalUitPassages the nr passages out reported in the HH-traject
    * @param overtredingenAuto the nr automatic violations reported in the HH-traject
    * @param overtredingenHand the nr manual violations reported in the HH-traject
    * @param overtredingenTotaal the total nr of violations reported in the HH-traject
    */
  case class Sectie(corridorName: String, corridorId: Seq[String], ht: String, sectieId: String,  speedLimit: String,  gemiddeld: String, max: String, matches: String, totaalInPassages:String, totaalUitPassages:String, overtredingenAuto: String, overtredingenHand: String, overtredingenTotaal:String)

  /**
    * Internally used class, representing a dailyreport
    * @param name the value of the tag algemene-info found in the dailyreport
    * @param datum date of the report
    * @param secties list of HH-traject found in the report
    */
  case class Traject(name: String, datum:String, secties:Seq[Sectie])

  /**
    * Root Output class. containing the requested results
    * @param totaalInPassages total number of passages In reported
    * @param totaalOvertredingen total number of violations reported
    * @param totaalOvertredingenAuto total number of automatic violations reported
    * @param totaalOvertredingenHand total number of manual violations reported
    * @param totaalGemiddeld Average of the speeds reported
    * @param statistiekRegel List of statistics for each day
    */
  case class Statistiek(totaalInPassages:String, totaalOvertredingen:String, totaalOvertredingenAuto: String, totaalOvertredingenHand: String, totaalGemiddeld:String, statistiekRegel:Seq[StatistiekRegel])

  /**
    * Output class. represent one line in the statistics result page. containing the information of one day
    * @param datum date of the statistics
    * @param gemiddeld average speed found at specified day
    * @param max maximum speed found at specified day
    * @param totaalInPassages total number of passages In
    * @param totaalUitPassages total number of passages Out
    * @param overtredingenAuto number of automatic violations
    * @param overtredingenHand number of manual violations
    * @param overtredingenTotaal number of violations
    */
  case class StatistiekRegel(datum:String, gemiddeld: String, max: String,  totaalInPassages:String, totaalUitPassages:String, overtredingenAuto: String, overtredingenHand: String, overtredingenTotaal:String)

  /**
    * Internally used class, containing the configuration data to be able to map a HH-traject to a corridor
    * @param ht The ht attribute in a HH-traject
    * @param id the id attribute in a HH-traject
    * @param speedLimit the speedlimit in a HH-traject
    * @param corridorName the corridor name
    * @param corridorId the corridorIds (multiple for parallele corridors)
    */
  case class ConfigMapping(ht: String, id:String, speedLimit: String, corridorName: String, corridorId: Seq[String])

  /**
    * return the default path where all statistics reside
    *
    * @param systemId is id of system
    */
  def getDefaultPath(systemId: String): Path = Systems.getRootPath(systemId) / "reports"

  /**
    * return the path of a specific day report
    *
    * @param systemId is id of system
    * @param reportId Id of report
    * @return Path to a specific System
    */
  def getReportPath(systemId: String, reportId:String):Path = Statistics.getDefaultPath(systemId) / reportId

  /**
   * return de path where all statistics resides
   */
  def getAllPaths(systemId: String): Seq[Path] = curator.getChildren(Statistics.getDefaultPath(systemId))

  /**
    * retrieve all statistics by a given system id
    *
    * @param systemId is id of system
    * @return a sequence of VersionOf[Corridor] or an empty list
    */
  def getAllBySystemId(systemId:String):Seq[Versioned[DayStatistics]]={
    if(curator.exists(Statistics.getDefaultPath(systemId))){
      Statistics.getAllPaths(systemId).foldLeft(List[Versioned[DayStatistics]]()){
        (list, path) => curator.getVersioned[DayStatistics](path).map(_::list).getOrElse(list)
      }
    }else{
      List()
    }
  }

  /**
   * convert an object to json
   */
  def toLiftJson[T<:AnyRef](model:T):String={
    Serialization.write[T](model)(enumFormats)
  }
  /**
    * retrieves statistics for a given system and report id
    * Warning: this can consume a large amount of memory depending of the xml size
    *
    * @param systemId id of system
    * @param reportId id of report
    * @return daily statistics in xml format
    */
  def getById(systemId:String, reportId:String)(implicit user:UserModel):Elem={
    val dayReport = curator.getVersioned[DayStatistics](Statistics.getReportPath(systemId, reportId))
    log(messageId="logging.statistics.getById", systemId=systemId, List(systemId))

    dayReport.map{
      dr =>
        val decompressed = decompress(dr.data.data.map(_.toByte).toArray)
        val xml = (new String(decompressed.map(_.toChar)))


        val root: Elem = XML.load(Source.fromString(xml))
        //val root: Elem = XML.load("mine_dagrapportage_4.2.xml") //voor mock
        val corridors: NodeSeq = root \ "HH-traject"

        val result = corridors.foldLeft(List[Elem]()){
          (list, corridor) =>
            val id = (corridor \ "@id").text + " " +  (corridor \ "@ht").text
            val index = (corridor \ "@ht")
            val stats = corridor \ "TC-statistiek"
            <HH-traject id={id} index={index}>{stats}</HH-traject> :: list
        }
        <HH-trajecten>{result}</HH-trajecten>

    }.getOrElse(<HH-trajecten></HH-trajecten>)
  }

  def getHead(lijst:immutable.Seq[String], default: String = ""):String =
    if(lijst.nonEmpty) lijst.head else default

  private def asPrettyDate(time:String, dateFormat:String="dd-MM-yyyy"):String={
    val format = new java.text.SimpleDateFormat("yyyy-MM-dd")
    val prettyFormat = new java.text.SimpleDateFormat(dateFormat)
    val date: Date =format.parse(time)
    prettyFormat.format(date)
  }


  /**
    * This method gets the daily report configuration from zookeeper and creates the ConfigMapping classes.
    * The parallel configurations are found at node /ctes/systems/<systemId>/dailyreport/config
    * And for each corridor the sections configurations are found at node /ctes/systems/<systemId>/corridors/<corridorId>/dailyreport/config
    * @param systemId the system Id
    * @return list of ConfigMapping
    */
  private def getConfiguration(systemId: String): Seq[ConfigMapping] = {
    val corridors=Corridors.getAllBySystemId(systemId)
    val idMapping = corridors.map(cor => cor.data.info.corridorId -> cor.data.id).toMap
    //get daily report for parallel configurations
    val parallelCfg = curator.get[List[ParallelCorridorConfig]](csc.sectioncontrol.storage.Paths.Systems.getParallelCorridorConfigPath(systemId))
    val parallelMappings = parallelCfg match {
      case Some(listCfg) => listCfg.flatMap(par => {
        val ids = par.parallelCorridorIds.map(infoId => idMapping.get(infoId).getOrElse(infoId.toString))
        val name = "total-"+ids.mkString("-")
        par.parallelHHTrajectConfigs.map(parSpeed =>{
          ConfigMapping(parSpeed.ht, parSpeed.id, parSpeed.speedLimit, name, ids)
        })
      })
      case None => Seq()
    }
    parallelMappings
    //get daily report for sections configurations
    val corridorService = new CorridorServiceCuratorImpl(curator)
    val sectionMapping = corridors.flatMap(corridor=>
    {
      logger.debug("systemId:"+systemId)
      logger.debug("corridorName:"+corridor.data.id)
      val config = corridorService.getDailyReportConfig(systemId,corridor.data.id)
      config.map(_.speedEntries).map(entries => entries.map(cfg => ConfigMapping(cfg.ht, cfg.id, cfg.speedLimit, corridor.data.name, Seq(corridor.data.id))))
    }).flatten
    parallelMappings ++ sectionMapping
  }

  /**
    * The method finds the correct configuration given the ht,id and speedlimit
    * @param ht the ht attribute found in the HH-traject
    * @param id the id attribute found in the HH-traject
    * @param speedLimit the speedlimit found in the HH-traject
    * @param mappings List of configuration mappings
    * @return the correct mapping
    */
  private def findConfig(ht:String, id: String, speedLimit: String, mappings: Seq[ConfigMapping]): Option[ConfigMapping] = {
    if(speedLimit == "" || speedLimit == "0") {
      mappings.find(m => m.ht == ht && m.id == id && m.speedLimit == "total")
    } else {
      mappings.find(m => m.ht == ht && m.id == id && m.speedLimit == speedLimit)
    }
  }

  /**
    * Get all list of daily report data specified
    * @param systemId the systemId
    * @param from Start date
    * @param to end date
    * @return list of traject data
    */
  def getTrajecten(systemId:String, from:String,to:String):Seq[Traject]={
    val fromInt=from.toInt
    val toInt=to.toInt
    var trajecten= new ListBuffer[Traject]()

    val configList = getConfiguration(systemId)

    // de bestandsnamen hebben een datum naam
    // loop door de bestanden heen
    for( a:Int <- fromInt to toInt){
      val dayReport = curator.getVersioned[DayStatistics](Statistics.getReportPath(systemId, a.toString))

      //val corridorService = new CorridorServiceCuratorImpl(curator)

      var trajectNaam:String=null
      var datum:String=null
      var secties: immutable.Seq[Sectie]=null

      dayReport.map{
        dr =>
          val decompressed = decompress(dr.data.data.map(_.toByte).toArray)
          val xml = (new String(decompressed.map(_.toChar)))
          var root: Elem = XML.load(Source.fromString(xml))

          trajectNaam=getHead((root \\ "algemene-info").map({root=>root.text}))
          datum=asPrettyDate((root \ "@datum").text)

          secties = (root \\ "HH-traject" ).map { xmlSectie =>

            val sectienaam=(xmlSectie \ "@ht").text
            val sectieId = (xmlSectie \ "@id").text
            val speedLimit:String = (xmlSectie \ "instellingen" \ "snelheidslimiet").text
            var overtredingenTotaal:String = null
            var overtredingenHand :String = null
            var overtredingenAuto :String = null
            var totaalInPassages:String = null
            var totaalUitPassages:String = null
            var snelheidGemiddeld:String = null
            var snelheidMax:String = null
            var matches: String = null

            (xmlSectie \\ "TC-statistiek" ).map { statistiek =>
              totaalInPassages=getHead((statistiek \ "passages" \ "@totaal-in").map { x =>  x.text}, "0")
              totaalUitPassages=getHead((statistiek \ "passages" \ "@totaal-uit").map { x =>  x.text}, "0")

              snelheidGemiddeld=getHead((statistiek \ "snelheden" \ "gemiddeld").map { x =>  x.text}, "0")
              snelheidMax=getHead((statistiek \ "snelheden" \ "max").map { x =>  x.text}, "0")

              matches=getHead((statistiek \ "matches").map { x =>  x.text}, "0")

              overtredingenTotaal = getHead((statistiek \ "overtredingen" \ "overtredingen-totaal").map { x =>  x.text}, "0")
              overtredingenHand = getHead((statistiek \ "overtredingen" \ "hand").map { x =>  x.text}, "0")
              overtredingenAuto = getHead((statistiek \ "overtredingen" \ "auto").map { x =>  x.text}, "0")
            }
            val cfg = findConfig(sectienaam,sectieId,speedLimit,configList)
            val sectionId = cfg.map(_.corridorName).getOrElse(noConfig)
            val corridorId = cfg.map(_.corridorId).getOrElse(Seq())
            Sectie(sectionId, corridorId, sectienaam,sectieId, speedLimit, snelheidGemiddeld,snelheidMax,matches, totaalInPassages,totaalUitPassages,overtredingenAuto,overtredingenHand,overtredingenTotaal)
          }

          if(secties.length > 0){
            trajecten+=Traject(trajectNaam,datum,secties)
          }
      }.getOrElse("error")
    }

    trajecten
  }


  /**
    * Convert a string to a float used to get the total numbers
    * @param x a float as a string, empty or "config"
    * @return the Float or 0
    */
  def addFloat(x: String): Float= {
    if(x.trim == "" || x == noConfigCellResult)return 0
    x.toFloat
  }

  /**
    * Are all characterss digits
    * @param x a string
    * @return true when all digits otherwise false
    */
  def isAllDigits(x: String) = x forall Character.isDigit

  /**
    * Convert a string to a long used to get the total numbers
    * @param x a float as a string, empty or "config"
    * @return the Long or 0
    */
  def addLong(x: String): Long= {
    if(x.trim=="")return 0
    if(isAllDigits(x))
    {
      return x.toLong
    }
    0
  }

  /**
    * This method creates one StatistiekRegel given a list of HH-trajects
    * @param sectionId the systemId
    * @param date the date
    * @param reportSections list of HH-trajects
    * @return the resulted StatistiekRegel on None when a problem occured
    */
  private def createStatistiekRegel(sectionId: String, date: String, reportSections: Seq[Sectie]): Option[StatistiekRegel] = {
    reportSections.size match {
      case 0 =>
        logger.debug("Did not found one report sections for day %s and section: %s".format(date, sectionId))
        None
      case 1 => {
        val section = reportSections.head
        logger.debug("Found one report section using single speed: %s %s".format(date, section))
        Some(StatistiekRegel(date,
          section.gemiddeld,
          section.max.toString,
          section.totaalInPassages,
          section.totaalUitPassages,
          section.overtredingenAuto,
          section.overtredingenHand,
          section.overtredingenTotaal))
      }
      case other => {
        //multiple speed limits => find total => speedlimit == 0
        reportSections.find(_.speedLimit == "0") match {
          case Some(section) =>
            logger.debug("found a multiple speed TOTAL report statistics: %s %s".format(date, sectionId))
            Some(StatistiekRegel( date,
              section.gemiddeld,
              section.max.toString,
              section.totaalInPassages,
              section.totaalUitPassages,
              section.overtredingenAuto,
              section.overtredingenHand,
              section.overtredingenTotaal))
          case None => {
            logger.warn("Found muliple reported sections but not the total section %s: %s".format(date, reportSections))
            //todo: what to do could not find combined speed?  sumerize all sections? or skip
            None
          }
        }
      }
    }
  }

  /**
    * Create a Statistiek class given the list of StatistiekRegel
    * @param lines list of StatistiekRegel
    * @return Statistiek containing the summary numbers
    */
  private def createStatistiek(lines: Seq[StatistiekRegel]): Statistiek = {
    // class used to summarize all attributes
    case class StatistiekSum(totaalInPassages1: Long = 0L,
                          totaalOvertredingen1: Long = 0L,
                          totaalOvertredingenAuto1: Long = 0L,
                          totaalOvertredingenHand1: Long = 0L,
                          gemTotal: Float = 0L)
    var nrAverage = 0
    //summarize all
    val total = lines.foldLeft(StatistiekSum()) { case (total, currentLine) => {
      val average = addFloat(currentLine.gemiddeld)
      if(average > 0) {
        nrAverage += 1
      }
      StatistiekSum(total.totaalInPassages1 + addLong(currentLine.totaalInPassages),
      total.totaalOvertredingen1 + addLong(currentLine.overtredingenTotaal),
      total.totaalOvertredingenAuto1+ addLong(currentLine.overtredingenAuto),
      total.totaalOvertredingenHand1 + addLong(currentLine.overtredingenHand),
      total.gemTotal + average)
    }}
    //Simple calculation of the average.
    //Take the average of all the filled averages
    val gem = if(nrAverage != 0 && total.gemTotal != 0){
        val x=(total.gemTotal/nrAverage)
        (BigDecimal(x).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble).toString
      } else
        ""
    Statistiek(total.totaalInPassages1.toString,
      total.totaalOvertredingen1.toString,
      total.totaalOvertredingenAuto1.toString,
      total.totaalOvertredingenHand1.toString,
      gem,
      lines)
  }

  /**
    * add multiple StatistiekRegel into one StatistiekRegel
    * @param date the date
    * @param lines the lines to be merged into one
    * @return the merged result
    */
  private def mergeLines(date: String, lines: Seq[StatistiekRegel]): StatistiekRegel = {
    // class used to summarize all attributes
    case class SumLines(gemTotal: Float = 0L, max: Long = 0L,
                        totaalInPassages:Long = 0L, totaalUitPassages:Long = 0L,
                        overtredingenAuto: Long = 0L, overtredingenHand: Long = 0L,
                        overtredingenTotaal:Long = 0L)

    logger.debug("Summarize sections %s: %s".format(date,lines))
    var nrAverage = 0
    val total = lines.foldLeft(SumLines()) { case (total, currentLine) => {
      val average = addFloat(currentLine.gemiddeld)
      if(average > 0) {
        nrAverage += 1
      }
      SumLines(total.gemTotal + average,
        math.max(total.max, addLong(currentLine.max)),
        total.totaalInPassages + addLong(currentLine.totaalInPassages),
        total.totaalUitPassages + addLong(currentLine.totaalUitPassages),
      total.overtredingenAuto + addLong(currentLine.overtredingenAuto),
        total.overtredingenHand + addLong(currentLine.overtredingenHand),
      total.overtredingenTotaal + addLong(currentLine.overtredingenTotaal))
    }}
    //Simple calculation of the average.
    //Take the average of all the filled averages
    val gem = if(nrAverage != 0 && total.gemTotal != 0){
      val x=(total.gemTotal/nrAverage)
      (BigDecimal(x).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble).toString
    } else
      ""
    val result = StatistiekRegel(date, gem, total.max.toString,  total.totaalInPassages.toString, total.totaalUitPassages.toString,
      total.overtredingenAuto.toString, total.overtredingenHand.toString, total.overtredingenTotaal.toString)
    logger.debug("Total sections %s: %s".format(date, result))
    result
  }

  /**
    * retrieves statistics for a given system and report id
    * Warning: this can consume a large amount of memory depending of the xml size
    *
    * @param systemId id of system
    * @param sectionId id of corridor/section
    * @return daily statistics in xml format
    */
  def getStatisticsForSection(systemId:String, sectionId:String, from:String, to:String)(implicit user:UserModel):Statistiek={

    logger.debug("getStatisicsForSection: " + sectionId)
    val trajecten=getTrajecten(systemId, from,to)

    //each traject is one day
    val statsLines = trajecten.flatMap(trajectReportedStatistics => {
      logger.debug("traject:"+trajectReportedStatistics)
      if(trajectReportedStatistics.secties.exists(sec => sec.corridorName == noConfig)) {
        //there is a configuration issue so the results are not reliable
        logger.info("Found configuration issues for sections %s: %s".format(trajectReportedStatistics.datum, trajectReportedStatistics.secties))
        Some(StatistiekRegel(trajectReportedStatistics.datum, noConfigCellResult, noConfigCellResult, noConfigCellResult,
          noConfigCellResult, noConfigCellResult, noConfigCellResult, noConfigCellResult))
      } else {
        val reportSections = trajectReportedStatistics.secties.filter(_.corridorName == sectionId)
        createStatistiekRegel(sectionId, trajectReportedStatistics.datum, reportSections)
      }
    })

    //sum all found results
    createStatistiek(statsLines)
  }


  /**
    * retrieves statistics for a given system and report id
    * Warning: this can consume a large amount of memory depending of the xml size
    *
    * @param systemId id of system
    * @return daily statistics in xml format
    */
  def getStatisticsForSystem(systemId:String, from:String, to:String)(implicit user:UserModel):Statistiek={
    logger.debug("getStatiticsForSystem: " + systemId)

    val trajecten=getTrajecten(systemId, from,to)

    //each traject is one day
    val statsLines = trajecten.flatMap(trajectReportedStatistics => {
      logger.debug("traject:"+trajectReportedStatistics)
      if(trajectReportedStatistics.secties.exists(sec => sec.corridorName == noConfig)) {
        //there is a configuration issue so the results are not reliable
        logger.info("Found configuration issues for sections %s: %s".format(trajectReportedStatistics.datum, trajectReportedStatistics.secties))
        Some(StatistiekRegel(trajectReportedStatistics.datum, noConfigCellResult, noConfigCellResult, noConfigCellResult,
          noConfigCellResult, noConfigCellResult, noConfigCellResult, noConfigCellResult))
      } else {
        //collect all combined sections and skip the containing single sections
        val combinedSections = trajectReportedStatistics.secties.filter(_.corridorName.startsWith("total-"))
        val combinedIds = combinedSections.flatMap(_.corridorId).distinct
        logger.info("Found combined corridors for %s %s".format(trajectReportedStatistics.datum, combinedIds))

        //remove the single corridors (from the combined sections) and keep the rest
        val sections = trajectReportedStatistics.secties.filterNot(sec => sec.corridorId.size == 1 && combinedIds.contains(sec.corridorId.head))
        //get section totals
        val groupReportSections = sections.groupBy(_.corridorName)
        //create statistics for all sections and combined sections
        val totalSections = groupReportSections.flatMap { case (corridorName, reportSections) => {
          if (corridorName == noConfig) {
            //skip the sections which has no configuration
            logger.debug("Skipped sections %s: %s".format(trajectReportedStatistics.datum, reportSections))
            None
          } else {
            createStatistiekRegel(corridorName, trajectReportedStatistics.datum, reportSections)
          }
        }}.toSeq
        //summarize all sections
        if (totalSections.size > 0) {
          logger.debug("Summarize statisics %s: %s".format(trajectReportedStatistics.datum, totalSections))
          Some(mergeLines(totalSections.head.datum, totalSections))
        } else
          None
      }
    })

    //sum all found day results
    createStatistiek(statsLines)
  }

  /**
    * returns the day report
    *
    * @param systemId id of system
    * @param reportId id of report
    * @return DayStatistics
    */
  def getDayReport(systemId:String, reportId:String)(implicit user:UserModel):Option[DayStatistics]={
    log(messageId="logging.statistics.getDayReport", systemId=systemId, List(reportId))
    curator.get[DayStatistics](Statistics.getReportPath(systemId, reportId))
  }
}
