package services

import common.{SystemLogger, Validates}
import csc.curator.utils.Versioned
import play.api.i18n.Messages
import models.storage._
import models.web._

object Services extends Validates with SystemLogger with Storage{
  /**
   * return the default service path
   * @param systemId is id of system
   * @param corridorId is id of corridor
   * @return Path to corridors
   */
  def getDefaultPath(systemId:String, corridorId:String) = Corridors.getRootPath(systemId, corridorId) / "services"

  /**
   * return the service path for a given corridorId
   * @param systemId is id of system
   * @param corridorId is id of corridor
   * @param serviceType is type of service
   * @return Path to specific corridor
   */
  def getServicePath(systemId:String, corridorId:String, serviceType:ServiceType.Value) = getDefaultPath(systemId, corridorId) / serviceType.toString
  

  def get(systemId:String, corridorId:String):Option[Service]={
    Services.getVersioned(systemId, corridorId) match {
      case Some((service, _)) => Some(service)
      case _ => None
    }
  }

  def getVersioned(systemId:String, corridorId:String):Option[(Service,Int)]={
    Corridors.findById(systemId, corridorId) match {
      case Some(cor) =>
        val serviceType = cor.data.serviceType
        val path = Services.getServicePath(systemId, corridorId, serviceType)
        curator.exists(path) match {
          case true =>
            import ServiceType._
            val sss = serviceType match {
              case RedLight => curator.getVersioned[RedLightService](path)
              case SpeedFixed => curator.getVersioned[SpeedFixedService](path)
              case _ => None
            }
            sss match {
              case Some(s) => Some(s.data,s.version)
              case _ => None
            }
          case false => None
        }
      case _ => None
    }
  }
  
  def create(systemId:String, corridorId:String, minimumYellowTime:Long, pardonRedTime:Long, factNumber:String){
    Corridors.findById(systemId, corridorId) match {
      case Some(x) =>
        val serviceType = x.data.serviceType
        val defaultPath = Services.getDefaultPath(systemId, corridorId)
        val servicePath = Services.getServicePath(systemId, corridorId, serviceType)

        if(curator.exists(defaultPath))
          curator.deleteRecursive(defaultPath)
        import ServiceType._
        serviceType match {
          case RedLight => curator.put(servicePath, RedLightService(pardonRedTime = pardonRedTime, minimumYellowTime = minimumYellowTime, factNumber = factNumber))
          case SpeedFixed => curator.put(servicePath, SpeedFixedService())
          case _ => None
        }
      case _ =>
    }
  }

  def sendServiceCreatedEvent(systemId:String, corridorId:String, serviceType:ServiceType.Value)(implicit user:UserModel){
    import MonitorEventsType._
    val serviceName = getServiceName(systemId, corridorId, Some(serviceType))

    val event = Systems.getDefaultEvent(systemId).copy(
      eventType = ServiceCreated.toString,
      corridorId = Some(corridorId),
      reason = getMessage("services.events.sendServiceCreatedEvent", List(serviceName))
    )

    Systems.sendEvent(systemId, event)
  }

  private def serviceStateMustBeSameAsPreviousState(systemId:String, corridorId:String, request:ServiceStateRequest, fieldKey:String="", errorKey:String="validate.state_does_not_exist"):Option[FieldError]={
    Services.getVersioned(systemId, corridorId).map{
      case (service, version) =>
        if(version != request.version)
          Some(FieldError(field="version", error=Messages("services.wrong_version")))
//        else if(service.isActive != request.from)
//          Some(FieldError(field="isActive", error=Messages("services.same_begin_state")))
        else
          None
    }.getOrElse(Some(FieldError(field="systemId, corridorId", error=Messages("services.unkown_corridor_systemId"))))
  }
  
  def stateChange(systemId:String, corridorId:String, request:ServiceStateRequest)(implicit user:UserModel):ResponseMsg={
    import MonitorEventsType._
    var errors = List[FieldError]()
    
    systemMustExist(systemId).map{errors ::= _ }
    if(corridorId == null || corridorId.trim().length() == 0 || !Corridors.exists(systemId, corridorId)){
      errors ::= FieldError(field="corridorId", error=Messages("corridors.unkown_corridor"))
    }
    serviceStateMustBeSameAsPreviousState(systemId, corridorId, request).map{errors ::= _ }

  if(errors.length > 0){
      ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors.toList)
    }else{
      try{
        val eventType = if(request.to) ServiceStart else ServiceStop
        val (currentState, newState) = if(request.to) (ServiceStop, ServiceStart) else (ServiceStart, ServiceStop)
        val serviceName = getServiceName(systemId, corridorId)
        val args =  List(user.username, user.reportingOfficerId, serviceName, systemId, Messages("services.state." + currentState), Messages("services.state." + newState))
        val reason = getMessage("services.events.stateChange", args)

        val stateEvent = Systems.getDefaultEvent(systemId).copy(
          eventType = eventType.toString,
          corridorId = Some(corridorId),
          reason = reason
        )

        Systems.sendEvent(systemId, stateEvent)
        //log(messageId = "services.events.stateChange", systemId = systemId, args = args)
        ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("services.state_request_success"))
      }catch{
        case e:Exception =>
          ResponseMsg(typeName=ResponseType.System, description=Messages("services.unknown_system_error"))
      }
    }
  }
  
  private def getServiceName(systemId:String, corridorId:String, serviceType:Option[ServiceType.Value]=None):String={
    serviceType.map{
      st => getMessage("services.form.title." + st.toString)
    }.getOrElse{
      Corridors.findById(systemId, corridorId) match {
        case Some(x) => getMessage("services.form.title." + x.data.serviceType.toString, Nil)
        case _ => getMessage("services.form.title.Unknown", Nil)
      }
    }
  }

  def update(systemId:String, corridorId:String, minimumYellowTime:Long,  pardonRedTime:Long, factNumber:String){
    Services.getVersioned(systemId, corridorId) match {
      case Some((service:RedLightService, version)) =>

        val path = Services.getServicePath(systemId, corridorId, ServiceType.RedLight)

        val copy = service.copy(
          minimumYellowTime=minimumYellowTime,
          pardonRedTime=pardonRedTime,
          factNumber=factNumber)

        curator.set(path, copy, version)
      case _ =>
    }
  }
}
