/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package services

import models.storage.PermittedAction
import models.storage.ActionType._
import play.api.i18n.Messages
import models.storage.PermittedAction._
import controllers.MonitorProcess

/**
 * regarding PermittedActions will go in hear (e.g. adding a new PermittedAction via the website)
 *
 * All permitted actions.
 */
object PermittedActions{
  def getAll:List[PermittedAction]={

    val list = List(
      PermittedAction(
        id = "ChangeSystemStateOffStandbyMaintenance",
        group = Some("ChangeSystemStateOffStandbyMaintenance"),
        actionType = ChangeSystemStateOffStandbyMaintenance,
        name = Messages("permittedActions.ChangeSystemStateOffStandbyMaintenance.name"),
        description = Messages("permittedActions.ChangeSystemStateOffStandbyMaintenance.description"),
        reportingofficeridRequired = false
      ),

      PermittedAction(
        id = "DownloadPassagegegevens",
        group = Some("DOWNLOADPASSAGEGEGEVENS"),
        actionType = DownloadPassagegegevens,
        name = Messages("permittedActions.DownloadPassagegegevens.name"),
        description = Messages("permittedActions.DownloadPassagegegevens.description"),
        reportingofficeridRequired = true
      ),

      PermittedAction(
        id = "CorridorsCrud",
        group = Some("CORRIDORSCRUD"),
        actionType = CorridorsCrud,
        name = Messages("permittedActions.Corridors.name"),
        description = Messages("permittedActions.Corridors.description"),
        reportingofficeridRequired = true
      ),

      PermittedAction(
        id = "CorridorsView",
        group = Some("CORRIDORSVIEW"),
        actionType = CorridorsView,
        name = Messages("permittedActions.CorridorsView.name"),
        description = Messages("permittedActions.CorridorsView.description"),
        reportingofficeridRequired = false
      ),

      PermittedAction(
        id = "Schedules",
        group = Some("SCHEDULES"),
        actionType = SchedulesCrud,
        name = Messages("permittedActions.Schedules.name"),
        description = Messages("permittedActions.Schedules.description"),
        reportingofficeridRequired = true
      ),

      PermittedAction(
        id = "SchedulesView",
        group = Some("SCHEDULESVIEW"),
        actionType = SchedulesView,
        name = Messages("permittedActions.SchedulesView.name"),
        description = Messages("permittedActions.SchedulesView.description"),
        reportingofficeridRequired = false
      ),

      PermittedAction(
        id = "HHMUpdate",
        group = Some("HHM"),
        actionType = SystemsUpdate,
        name = Messages("permittedActions.HHMUpdate.name"),
        description = Messages("permittedActions.HHMUpdate.description"),
        reportingofficeridRequired = false
      ),

      PermittedAction(
        id = "ChangeSystemStateHandhavenStandby",
        group = Some("HHM"),
        actionType = ChangeSystemStateHandhavenStandby,
        name = Messages("permittedActions.ChangeSystemStateHandhavenStandby.name"),
        description = Messages("permittedActions.ChangeSystemStateHandhavenStandby.description"),
        reportingofficeridRequired = true
      ),
      PermittedAction(
        id = "ParametersView",
        group = Some("ParametersView"),
        actionType = ParametersView,
        name = Messages("permittedActions.ParametersView.name"),
        description = Messages("permittedActions.ParametersView.description"),
        reportingofficeridRequired = false
      ),
      PermittedAction(
        id = "TacticalAndTechnicalView",
        group = Some("TacticalAndTechnicalView"),
        actionType = TacticalAndTechnicalView,
        name = Messages("permittedActions.TacticalAndTechnicalView.name"),
        description = Messages("permittedActions.TacticalAndTechnicalView.description"),
        reportingofficeridRequired = false
      ),
      PermittedAction(
        id = "TechnicalCud",
        group = Some("TechnicalCud"),
        actionType = TechnicalCud,
        name = Messages("permittedActions.TechnicalCud.name"),
        description = Messages("permittedActions.TechnicalCud.description"),
        reportingofficeridRequired = false
      ),
      PermittedAction(
        id = "AllRoles",
        group = Some("AllRoles"),
        actionType = AllRoles,
        name = Messages("permittedActions.AllRoles.name"),
        description = Messages("permittedActions.AllRoles.description"),
        reportingofficeridRequired = false
      ),
      PermittedAction(
        id = "SystemLog",
        group = Some("SystemLog"),
        actionType = SystemLog,
        name = Messages("permittedActions.SystemLog.name"),
        description = Messages("permittedActions.SystemLog.description"),
        reportingofficeridRequired = false
      ),
      PermittedAction(
        id = "ErrorsSignOff",
        group = Some("ErrorsSignOff"),
        actionType = ErrorsSignOff,
        name = Messages("permittedActions.ErrorsSignOff.name"),
        description = Messages("permittedActions.ErrorsSignOff.description"),
        reportingofficeridRequired = false
      ),
      PermittedAction(
        id = "CertificatesChange",
        group = Some("Certificates"),
        actionType = CertificatesChange,
        name = Messages("permittedActions.CertificatesChange.name"),
        description = Messages("permittedActions.CertificatesChange.description"),
        reportingofficeridRequired = false
      ),
        //Certificates
      PermittedAction(
        id = "CertificatesView",
        group = Some("Certificates"),
        actionType = CertificatesView,
        name = Messages("permittedActions.CertificatesView.name"),
        description = Messages("permittedActions.CertificatesView.description"),
        reportingofficeridRequired = false
      ),

      PermittedAction(
        id = "MonitorProcess",
        group = Some("Monitor"),
        actionType = MonitorProcessView,
        name = Messages("permittedActions.MonitorProcess.name"),
        description = Messages("permittedActions.MonitorProcess.description"),
        reportingofficeridRequired = false
      ),
      PermittedAction(
        id = "ParametersChange",
        group = Some("ParametersChange"),
        actionType = ParametersChange,
        name = Messages("permittedActions.ParametersChange.name"),
        description = Messages("permittedActions.ParametersChange.description"),
        reportingofficeridRequired = false
      ),

      PermittedAction(
        id = "TreatyCountriesChange",
        group = Some("TreatyCountriesChange"),
        actionType = TreatyCountriesChange,
        name = Messages("permittedActions.TreatyCountriesChange.name"),
        description = Messages("permittedActions.TreatyCountriesChange.description"),
        reportingofficeridRequired = false
      ),
    //ROLES
      PermittedAction(
        id = "RolesView",
        group = Some("Roles"),
        actionType = RolesView,
        name = Messages("permittedActions.RolesView.name"),
        description = Messages("permittedActions.RolesView.description"),
        reportingofficeridRequired = false
      ),
      PermittedAction(
        id = "RolesChange",
        group = Some("Roles"),
        actionType = RolesChange,
        name = Messages("permittedActions.RolesChange.name"),
        description = Messages("permittedActions.RolesChange.description"),
        reportingofficeridRequired = false
      ),
      PermittedAction(
        id = "UpdateSystem",
        group = Some("UpdateSystem"),
        actionType = UpdateSystem,
        name = Messages("permittedActions.UpdateSystem.name"),
        description = Messages("permittedActions.UpdateSystem.description"),
        reportingofficeridRequired = false
      ),
      //USERS
      PermittedAction(
        id = "UsersView",
        group = Some("Users"),
        actionType = UsersView,
        name = Messages("permittedActions.UsersView.name"),
        description = Messages("permittedActions.UsersView.description"),
        reportingofficeridRequired = false
      ),
      PermittedAction(
        id = "UsersCreate",
        group = Some("Users"),
        actionType = UsersCreate,
        name = Messages("permittedActions.UsersCreate.name"),
        description = Messages("permittedActions.UsersCreate.description"),
        reportingofficeridRequired = false
      ),
      PermittedAction(
        id = "UsersChange",
        group = Some("Users"),
        actionType = UsersChange,
        name = Messages("permittedActions.UsersChange.name"),
        description = Messages("permittedActions.UsersChange.description"),
        reportingofficeridRequired = false
      ),
      PermittedAction(
        id = "UsersDelete",
        group = Some("Users"),
        actionType = UsersDelete,
        name = Messages("permittedActions.UsersDelete.name"),
        description = Messages("permittedActions.UsersDelete.description"),
        reportingofficeridRequired = false
      )
)

    list.sortBy(_.name).toList
  }
}
