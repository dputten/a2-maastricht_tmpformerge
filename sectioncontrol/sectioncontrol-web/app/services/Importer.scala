/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package services

import java.io.File
import xml.{XML}
import models.web._
import play.{Play}
import csc.config.Path
import org.apache.zookeeper.data.Stat
import common.Config
import common.Extensions._
import play.api.i18n.Messages

/**
 * used to import a xml file and create the configuration that is in the file
 */
object Importer extends Config with Storage {

  /**
   * retrieve version of existing node/leaf
   * @param path
   * @return
   */
  def getVersion(path: Path): Int = {
    //FIXME
/*    val stat = new Stat
    curator.readData[Array[Byte]](path.toString, stat)
    stat.getVersion*/
    0
  }

  /**
   * create zookeeper tree wih given xml file
   * @param xml of zookeeper structure
   * @return
   */
  def importConfig(xml:File):Seq[ResponseMsg]={
    val root = XML.loadFile(xml)
    val version = (root \ "@version").text

    if(version != Config.version){
      List(ResponseMsg(description=Messages("configAdmin.failed.version_not_supported", Config.version)))
    }else{
      val nodes = root \ "znodes" \ "znode"
      nodes.map{
        node =>
          val path = (node \ "path").text
          val data = (node \ "data").text

          try{
            if(!curator.exists(Path(path))){
              curator.createEmptyPath(Path(path))
            }
            val version = getVersion(Path(path))
            curator.updateData(Path(path), data.getBytes, version)
            ResponseMsg(success=true, typeName=ResponseType.Import, description=Messages("configAdmin.path.create.success", path))
          }catch{
            case e:Exception =>
              ResponseMsg(typeName=ResponseType.Import, description=Messages("configAdmin.path.create.failed", path))
          }
      }
    }
  }
}
