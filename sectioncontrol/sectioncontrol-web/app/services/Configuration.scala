/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package services

import csc.config.{Path}
import common.Extensions._
import models.storage._
import csc.curator.utils.Versioned
import play.api.i18n.Messages._
import play.api.i18n.Messages
import common.{Validates, SystemLogger}
import models.web._
import models.web.ResponseMsg
import models.web.UserModel
import models.web.FieldError
import models.web.ResponseMsg
import models.storage.DynamaxIface
import models.storage.SystemDefinition
import models.web.UserModel
import models.web.FieldError
import models.storage.PossibleTreatyCountry
import models.storage.ClassifyCountry
import models.web.ClassifyCountriesModel

/**
 * main entry for system wide default configuration
 */
object Configuration extends Validates with SystemLogger with Storage {

  /***
   * root path to every config item regarding the web site
   * @return
   */
  def getRootWebPath = Configuration.getRootPath / "web"

  /**
   * root path to translation
   * @return
   */
  def getRootTranslationsPath = Configuration.getRootWebPath / "translations"

  /**
   * logs path to translation
   * @return
   */
  def getTranslationsSystemLogPath = Configuration.getRootTranslationsPath / "systemlog"

  /**
   * path to translation used by systemlog
   * @param country
   * @return
   */
  def getTranslationsPath(country:String) = Configuration.getRootTranslationsPath / country

  /**
   * return the default path where all default configuration reside
   */
  def getRootPath:Path = Path("/ctes") / "configuration"

  /**
   * return the default path where all reporting default configuration reside
   */
  def getRootReportsPath:Path = Configuration.getRootPath / "reports"

  /**
   * return the default path where all jobs default configuration reside
   */
  def getRootJobsPath:Path = Configuration.getRootPath / "jobs"

  /**
   * return the default path where all digital input configuration reside
   */
  def getRootDigitalInputPath:Path = Configuration.getRootPath / "digital-inputs"

  /**
   * return the default path where all dynamax configuration reside
   */
  def getRootDynamaxPath:Path = Configuration.getRootPath / "dynamax"

  /**
   * return the default path where all system resources configuration reside
   */
  def getRootSystemResources:Path = Configuration.getRootPath / "system-resources"

  /**
   * return the default path where all Auditors configuration reside
   */
  def getRootAuditors:Path = Configuration.getRootPath / "auditor"

  /**
   * return System Resources ganglia specific configuration
   */
  def getSystemResourcesGanglia:Path = Configuration.getRootSystemResources / "ganglia"

  /**
   * return dynamax Iface specific configuration
   */
  def getDynamaxIfacePath:Path = Configuration.getRootDynamaxPath / "iface"

  /**
   *  return dynamax approvedSpeeds specific configuration
   */
  def getDynamaxApprovedSpeedsPath:Path = Configuration.getRootDynamaxPath / "approvedSpeeds"

  /**
   *  return dynamax specific configuration
   */
  def getDynamaxConfigPath:Path = Configuration.getRootDynamaxPath / "config"

  /**
   * Path to all default jobs for all systems
   * @return Path
   */
  def getRootSystemPath:Path = Configuration.getRootJobsPath / "systems"

  /**
   * Path to all default jobs for all corridors
   * @return Path
   */
  def getRootCorridorPath:Path = Configuration.getRootJobsPath / "corridors"

  /**
   * Path to all jobs for a specific system
   * @param systemId id of system
   * @return Path
   */
  def getJobsPathForSystem(systemId:String):Path = Systems.getRootPath(systemId) / "jobs"

  /**
   * Path to all digital inputs for a specific system
   * @param systemId id of system
   * @return Path
   */
  def getDigitalInputPathForSystem(systemId:String):Path = Systems.getRootPath(systemId) / "digital-inputs"

  /**
   * Path to all auditors for a specific system
   * @param systemId id of system
   * @return Path
   */
  def getAuditorsForSystem(systemId:String):Path = Systems.getRootPath(systemId) / "auditor"

  /**
   * Path to zaakbestand info
   * @param systemId id of system
   * @return Path
   */
  def getCaseFileForSystem(systemId:String):Path = Systems.getRootPath(systemId) / "case-file"

  /**
   * Path to calibration config
   * @param systemId id of system
   * @return Path
   */
  def getCalibrationForSystem(systemId:String):Path = Systems.getRootPath(systemId) / "selftest" / "config"

  /**
   * Path to monitor definition
   * @param systemId id of system
   * @return Path
   */
  def getMonitorDefinitionForSystem(systemId:String):Path = Systems.getRootPath(systemId) / "definition"


  /**
   * Path to zaakbestand info
   * @return Path
   */
  def getRootCaseFile:Path = Configuration.getRootPath / "case-file"

  /**
   * Path to calibration config
   * @return Path
   */
  def getRootCalibrationConfig:Path = Configuration.getRootPath / "selftest" / "config"

  /**
   * path to verdragslanden
   * @return
   */
  def getPathPossibleTreatyCountries = Configuration.getRootPath / "possibleTreatyCountries"

  /**
   * path to verdragslanden
   * @return
   */
  def getPathClassifyCountry = Configuration.getRootPath / "classifyCountry"


  /**
   * Path to all jobs for a specific corridor
   * @param systemId id of system
   * @param corridorId id of corridor
   * @return Path
   */
  def getJobsPathForCorridor(systemId:String, corridorId:String):Path = Corridors.getRootPath(systemId, corridorId) / "jobs"


  /**
   * Copies the default digital input configuration for systems to a specific system
   * @param systemId id of system
   */
  private def createDefaultSystemDigitalInputs(systemId:String)(implicit user:UserModel){
    if(!curator.exists(Configuration.getDigitalInputPathForSystem(systemId))){
      curator.copyData(
        Configuration.getRootDigitalInputPath,
        Configuration.getDigitalInputPathForSystem(systemId))

      //log(messageId="logging.configuration.createDefaultSystemDigitalInputs", systemId=systemId)
    }
  }

  /**
   * main entry point or creating default configuration for a given systemId
   * @param systemId id of system
   */
  def createDefaultConfiguration(systemId:String)(implicit user:UserModel){
    createDefaultSystemJobs(systemId)
    createDefaultSystemDigitalInputs(systemId)
    createDefaultAuditors(systemId)
    //createDefaultCaseFile(systemId)
    createDefaultCalibrationConfig(systemId)
    createMonitorDefinition(systemId)
  }

  def translations(country:String):Map[String, String]={
    curator.get[Map[String, String]](Configuration.getTranslationsPath(country)).getOrElse(Map())
  }

  private def createMonitorDefinition(systemId:String)(implicit user:UserModel){
    val path = Configuration.getMonitorDefinitionForSystem(systemId)
    if(!curator.exists(path)){
      curator.put(path, SystemDefinition(false,false,false,true))

      log(messageId="logging.configuration.createMonitorDefinition", systemId=systemId)
    }
  }

  private def createDefaultCalibrationConfig(systemId:String)(implicit user:UserModel){
    if(!curator.exists(Configuration.getCalibrationForSystem(systemId))){
      curator.copyData(
        Configuration.getRootCalibrationConfig,
        Configuration.getCalibrationForSystem(systemId))

      log(messageId="logging.configuration.createCalibrationConfig", systemId=systemId)
    }
  }

  private def createDefaultCaseFile(systemId:String)(implicit user:UserModel){
    if(!curator.exists(Configuration.getCaseFileForSystem(systemId))){
      curator.copyData(
        Configuration.getRootCaseFile,
        Configuration.getCaseFileForSystem(systemId))

      log(messageId="logging.configuration.createDefaultCaseFile", systemId=systemId)
    }
  }

  /**
   * Copies the default Auditors for systems to a specific system
   * @param systemId id of system
   */
  private def createDefaultAuditors(systemId:String)(implicit user:UserModel){
    if(!curator.exists(Configuration.getAuditorsForSystem(systemId))){
      curator.copyData(
        Configuration.getRootAuditors,
        Configuration.getAuditorsForSystem(systemId))

      log(messageId="logging.configuration.createDefaultAuditors", systemId=systemId)
    }
  }
  /**
   * Copies the default jobs configuration for systems to a specific system
   * @param systemId id of system
   */
  private def createDefaultSystemJobs(systemId:String)(implicit user:UserModel){
    if(!curator.exists(Configuration.getJobsPathForSystem(systemId))){

      curator.createEmptyPath(Configuration.getJobsPathForSystem(systemId))

      curator.copyData(
        Configuration.getRootSystemPath / "classifyJob-nl",
        Configuration.getJobsPathForSystem(systemId) / "classifyJob-nl")

      curator.copyData(
        Configuration.getRootSystemPath / "cleanupJob",
        Configuration.getJobsPathForSystem(systemId) / "cleanupJob")

      curator.copyData(
        Configuration.getRootSystemPath / "cleanupEnforceJob",
        Configuration.getJobsPathForSystem(systemId) / "cleanupEnforceJob")

      curator.copyData(
        Configuration.getRootSystemPath / "enforceConfig-nl",
        Configuration.getJobsPathForSystem(systemId) / "enforceConfig-nl")

      curator.copyData(
        Configuration.getRootSystemPath / "processMtmDataJob",
        Configuration.getJobsPathForSystem(systemId) / "processMtmDataJob")

      curator.copyData(
        Configuration.getRootSystemPath / "processMtmDataJob" / "config",
        Configuration.getJobsPathForSystem(systemId) / "processMtmDataJob" / "config")

      curator.copyData(
        Configuration.getRootSystemPath / "qualifyViolationsJob",
        Configuration.getJobsPathForSystem(systemId) / "qualifyViolationsJob")

      curator.copyData(
        Configuration.getRootSystemPath / "registerViolationsJob",
        Configuration.getJobsPathForSystem(systemId) / "registerViolationsJob")

      curator.copyData(
        Configuration.getRootSystemPath / "registerViolationsJob" / "config",
        Configuration.getJobsPathForSystem(systemId) / "registerViolationsJob" / "config")

      log(messageId="logging.configuration.createDefaultSystemJobs", systemId=systemId)
    }
  }

  /**
   * updates the verdragslanden
   * @param update
   */
  def updateTreatyCountries(update:Seq[String]){
    val path = Configuration.getPathPossibleTreatyCountries
    curator.getVersioned[List[String]](path).map{
      versioned =>
        curator.set(path, update, versioned.version)
    }
  }

  /**
   * creates and returns a Section models
   * @param versionOf
   * @return section model
   */
  protected def createClassifyCountriesModel(versionOf:Versioned[ClassifyCountry])={
    ClassifyCountriesModel(
      version = versionOf.version,
      mobiCountries = versionOf.data.mobiCountries,
      similarCountries = versionOf.data.similarCountries)
  }
  /**
   * retrieve mobi countries
   * @return
   */
  def getTreatyCountries(implicit user:UserModel):Seq[PossibleTreatyCountry]={
    val path = Configuration.getPathPossibleTreatyCountries
    return curator.get[List[PossibleTreatyCountry]](path).getOrElse(Nil)
  }

  /**
   * retrieve mobi countries
   * @return
   */
  def getClassifyCountries(implicit user:UserModel):ClassifyCountriesModel={
   val path = Configuration.getPathClassifyCountry
//    return curator.get[ClassifyCountry](path).getOrElse(null)
    createClassifyCountriesModel(curator.getVersioned[ClassifyCountry](path).getOrElse(null))
//    Ok(toLiftJson(createClassifyCountriesModel(curator.get[ClassifyCountry](path).getOrElse(null))))

//    Sections.findById(systemId, sectionId) match {
//      case Some(section) =>
//        Ok(toLiftJson(createSectionModel(curator.get[ClassifyCountry](path).getOrElse(null)))
//      case _ =>
//        Ok(toLiftJson(ResponseMsg(
//          typeName = ResponseType.System,
//          description = Messages("section.actions.get.description.failed"))))
//    }

  }


//  curator.getVersioned[Role](Paths.Role.default / id).map(toModel(_))

//
//  def toModel(versionOf:Versioned[Role]):RoleModel={
//    RoleModel(
//      id = versionOf.data.id,
//      name = versionOf.data.name,
//      description = versionOf.data.description,
//      numberOfPeople = numberOfPeople(versionOf.data.id),
//      version = versionOf.version,
//      actionIds = versionOf.data.permittedActions.map{_.id}.toList,
//      permittedActions = versionOf.data.permittedActions)
//  }

    def toModel(versionOf:Versioned[ClassifyCountry]):ClassifyCountry={
      ClassifyCountry(
        mobiCountries = versionOf.data.mobiCountries,
        similarCountries = versionOf.data.similarCountries)
    }


  def updateClassifyCountries(model:ClassifyCountriesModel)(implicit user:UserModel):ResponseMsg={
    val path = Configuration.getPathClassifyCountry

    try{
      var result= curator.getVersioned[ClassifyCountry](path).map(toModel(_))
      curator.set(path, result.get.copy(mobiCountries=model.mobiCountries), model.version)
      ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.delete"))
    }catch{
      case e:Exception =>
        ResponseMsg(typeName=ResponseType.System, description="update failed")
    }




//      .map{
//      versioned =>
//        curator.set(path, versioned.data.copy(mobiCountries=model.mobiCountries), model.version)
//    }

//    curator.getVersioned[ClassifyCountry](path).map{
//      versioned =>
//        curator.set(path, versioned.data.copy(mobiCountries=model.mobiCountries), model.version)
//    }

//    Sections.findById(systemId, sectionId).foreach{
//      section =>
//        curator.set(
//          Sections.getConfigPath(systemId, sectionId),
//          section.data.copy(name=model.name, description=model.description, matrixBoards=matrixBoards,length = model.length),
//          version
//        )





  }



  def getTreatyCountriesById(isoCode:String)(implicit user:UserModel):Option[PossibleTreatyCountry]={
    getTreatyCountries.find(_.isoCode == isoCode)
  }

  def deleteTreatyCountry(isoCode:String)(implicit user:UserModel):ResponseMsg={
    val path = Configuration.getPathPossibleTreatyCountries
    val version = curator.getVersioned[List[PossibleTreatyCountry]](path).map(_.version).getOrElse(0)
    val toSave = getTreatyCountries.filterNot(_.isoCode == isoCode)
    try{
      curator.set(Configuration.getPathPossibleTreatyCountries, toSave, version)
      ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.delete"))
    }catch{
      case e:Exception =>
        ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.failed.delete"))
    }
  }

  /**
   * validate a newly added/updated treaty country
   * @param model
   * @return list with fielderrors
   */
  private def validateCountries(isNew:Boolean, model:PossibleTreatyCountry)(implicit user:UserModel):List[FieldError]={
    var errors = List[FieldError]()

    notEmpty(value=model.isoCode, fieldKey="classifyCountryCode",errorKey="country.validate.classifyCountryCode_is_empty").map{errors ::= _ }

    if(isNew){
      if(getTreatyCountriesById(model.isoCode).isDefined)
        errors ::= FieldError(field="isoCode", error=Messages("country.validate.classifyCountryCode_already_taken"))
    }else{
      val caseFileAlreadyTaken = getTreatyCountries.exists(c =>
        c.isoCode != model.isoCode &&
        c.isoCode == model.isoCode)
      if(caseFileAlreadyTaken)
        errors ::= FieldError(field="caseFileCountryCode", error=Messages("country.validate.caseFileCountryCode_already_taken"))
    }

    errors
  }

  def createTreatyCountries(model:PossibleTreatyCountry)(implicit user:UserModel):ResponseMsg={
    val errors = validateCountries(isNew = true, model = model)
    val path = Configuration.getPathPossibleTreatyCountries
    val version = curator.getVersioned[List[PossibleTreatyCountry]](path).map(_.version).getOrElse(0)

    if(errors.length > 0){
      ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors.toList)
    }else{
      try{
        val updated:List[PossibleTreatyCountry] = model :: getTreatyCountries.toList
        if(curator.exists(path))
          curator.set(Configuration.getPathPossibleTreatyCountries, updated, version)
        else
          curator.put(Configuration.getPathPossibleTreatyCountries, updated)
        ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.update"))
      }catch{
        case e:Exception =>
          ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.failed.update"))
      }
    }
  }

  def updateTreatyCountries(isoCode:String, model:PossibleTreatyCountry)(implicit user:UserModel):ResponseMsg={
    val errors = validateCountries(isNew = false, model = model)
    val path = Configuration.getPathPossibleTreatyCountries
    val version = curator.getVersioned[List[PossibleTreatyCountry]](path).map(_.version).getOrElse(0)

    if(errors.length > 0){
      ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors.toList)
    }else{
      try{
        val updated:List[PossibleTreatyCountry] = model :: getTreatyCountries.filterNot(_.isoCode == isoCode).toList
        curator.set(Configuration.getPathPossibleTreatyCountries, updated, version)
        ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.update"))
      }catch{
        case e:Exception =>
          ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.failed.update"))
      }
    }
  }

  /**
   * Copies the default jobs configuration for corridors to a specific corridor
   * @param systemId id of system
   * @param corridorId id of corridor
   */
  def createDefaultCorridorJobs(systemId:String, corridorId:String)(implicit user:UserModel){
    if(!curator.exists(Configuration.getJobsPathForCorridor(systemId, corridorId))){
      curator.createEmptyPath(Configuration.getJobsPathForCorridor(systemId, corridorId))

      curator.copyData(
        Configuration.getRootCorridorPath / "matcherJob",
        Configuration.getJobsPathForCorridor(systemId, corridorId) / "matcherJob")

      curator.copyData(
        Configuration.getRootCorridorPath / "matcherJob" / "config",
        Configuration.getJobsPathForCorridor(systemId, corridorId) / "matcherJob" / "config")

      log(messageId="logging.configuration.createDefaultCorridorJobs", systemId=systemId, List(corridorId))
    }
  }

  /**
   * return all ifaces for dynamax
   */
  def getDynamaxIfaces:Seq[DynamaxIface]={
    curator.get[List[DynamaxIface]](Configuration.getDynamaxIfacePath).getOrElse(List())
  }

  /**
   * return all approved speeds for dynamax
   */
  def getDynamaxApprovedSpeeds:Seq[Int]={
    curator.get[List[Int]](Configuration.getDynamaxApprovedSpeedsPath).getOrElse(List())
  }
}
