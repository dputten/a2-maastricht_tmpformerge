/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package services

import java.util.Date

import common.{SystemLogger, Validates}
import csc.curator.utils.Versioned
import csc.sectioncontrol.messages.certificates.{ActiveCertificate, LocationCertificate, MeasurementMethodInstallation, MeasurementMethodType, _}
import models.storage.System
import models.web.{FieldError, InstalledCertificateCreate, InstalledCertificateMultiCreate, InstalledCertificateShow, LocationCertificateCRUD, PropertyChange, ResponseMsg, TypeCertificateCRUD, UserModel, _}
import org.apache.zookeeper.KeeperException.BadVersionException
import play.api.i18n.Messages
import csc.sectioncontrol.sign.certificate.CertificateService

/**
 *Used for retrieving certificates
 */
object Certificates extends SystemLogger with Storage with Validates {

  /**
   * Get all the systems the user is allowed to see/modify
   * @param user
   * @return
   */
  def getSystems(implicit user:UserModel): Seq[Versioned[System]] ={
    Systems.findByIds(user.sectionIds.toSet.toSeq)
  }

  /**
   * Return the path where to look for the provided Location certificate
   */
  def getLocationCertificatePath(systemId: String) = Systems.getRootPath(systemId) / "certificate" / "location"

  /**
   * Return the path where to look for the global provided Type certificates
   */
  def getTypeCertificatePath = Configuration.getRootPath / "certificate" / "measurementMethodType"
  /**
   * Return the path where to look for the System installed Type certificates
   */
  def getInstalledTypePath(systemId: String) = Systems.getRootPath(systemId) / "certificate" / "measurementMethod"

  /**
   * return de default path where the active certificate of a system reside
   * @param systemId is id of system
   * @return Path to a typeCertificate
   */
  def getActiveCertificatePath(systemId:String) = Systems.getRootPath(systemId) / "activeCertificates"


  /**
   * get the location certificates of all systems the user is allowed to
   * @return
   */
  def getAllSystemsLocationCertificates(implicit user:UserModel): Seq[LocationCertificateCRUD] =
    getSystems.flatMap(versionedSystem => getAllLocationCertificate(versionedSystem.data.id))

  /**
   * Get all the Location certificates available for the speciefied system
   * @param systemId the specified system
   * @return list of available location certificates of the system
   */
  private def getAllLocationCertificate(systemId: String): Seq[LocationCertificateCRUD] = {
    val path = getLocationCertificatePath(systemId)
    val children = curator.getChildren(path)

    children.flatMap(childPath => {
      curator.getVersioned[LocationCertificate](childPath).map(versioned => {
      new LocationCertificateCRUD(
        id = Some(childPath.nodes.last.name),
        version = Some(versioned.version),
        systemId = systemId,
        certificate = versioned.data)
      })
    })
  }

  /**
   * Validate the LocationCertificateCRUD
   * @param cert the certificate to be validated
   * @param user the user requesting the creation or change
   * @return
   */
  private def validateLocationCertificate(cert: LocationCertificateCRUD)(implicit user:UserModel):List[FieldError]={
    var errors = List[FieldError]()
    // check locationNr has value
    notEmpty(value=cert.certificate.id, fieldKey="locationNr", errorKey="locationcertificate.validate.nr_is_empty").map{errors ::= _ }
    // check id and inspectionDate is unique
    val allCert = getAllLocationCertificate(cert.systemId)
    val same = allCert.find(c => c.certificate.id == cert.certificate.id &&
      c.certificate.inspectionDate == cert.certificate.inspectionDate &&
      c.id != cert.id
    )
    if (same.isDefined) {
      errors ::= FieldError(field="all", error=Messages("locationcertificate.validate.same_key"))
    }
    errors
  }

  /**
   * Create or update a location certificate
   * @param cert the new or updated certificate
   * @param user the usere requesting the change or creation
   * @return ResponseMsg indicating if the operation was successful or not
   */
  def createOrUpdateLocationCertificate(cert: LocationCertificateCRUD)(implicit user:UserModel): ResponseMsg = {
    val pathToCertificate = getLocationCertificatePath(cert.systemId)
    val id = cert.id.getOrElse(cert.certificate.id + cert.certificate.inspectionDate.toString)

    val errors = validateLocationCertificate(cert)

    if(errors.length > 0){
      return ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors.toList)
    }

    if(cert.version.isDefined) {
      //update
      try {
        curator.get[LocationCertificate](pathToCertificate / id) match {
          case Some(previous) => {
            val changes = List(
              PropertyChange(Messages("locationcertificate.form.title.id"), previous.id, cert.certificate.id),
              PropertyChange(Messages("locationcertificate.form.title.from"), previous.inspectionDate, cert.certificate.inspectionDate),
              PropertyChange(Messages("locationcertificate.form.title.to"), previous.validTime, cert.certificate.validTime),
              PropertyChange(Messages("locationcertificate.form.title.components"), previous.components, cert.certificate.components)).flatMap(_.showChange)

            if(changes.length > 0) {
              curator.set(pathToCertificate / id, cert.certificate, cert.version.getOrElse(0))
              log(messageId="logging.locationcertificate.update", systemId=cert.systemId, args=List(changes.mkString("\r\n",";\r\n","\r\n")))
            }

            ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.update"))
          }
          case None => {
            ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.failed.update.not_exists"))
          }
        }
      }catch{
        case e:BadVersionException =>
          ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.failed.update_version_conflict"))
        case e:Exception =>
          ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.failed.update"))
      }
    } else { // insert
      try {
        if(!curator.exists(pathToCertificate)){
          curator.createEmptyPath(pathToCertificate)
        }
        curator.put(pathToCertificate / id, cert.certificate)
        log(
          messageId="logging.locationcertificate.create",
          systemId=cert.systemId, args=List(cert.certificate.id))
        ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.insert"))
      }catch{
        case e:Exception =>
          ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.success.failed"))
      }
    }
  }

  /**
   * Delete the specified location certificate
   * @param systemId the systemID
   * @param certificateId the id of the specified
   * @param user the user requested the delete
   * @return ResponseMsg indicating if the operation was successful or not
   */
  def deleteLocationCertificate(systemId:String, certificateId:String)(implicit user:UserModel): ResponseMsg = {
    val pathToCertificate = getLocationCertificatePath(systemId)
    try {
      curator.delete(pathToCertificate / certificateId)
      log(
        messageId="logging.locationcertificate.delete",
        systemId=systemId, args=List(certificateId))
      ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.delete"))
    }catch{
      case e:Exception =>
        ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.failed.delete"))
    }
  }

  /**
   * Get a speciefied location certificate
   * @param systemId the systemID
   * @param certificateId the id of the specified
   * @param user the user requested the delete
   * @return option of the requested location certificate
   */
  def getLocationCertificate(systemId:String, certificateId:String)(implicit user:UserModel): Option[LocationCertificateCRUD] = {
    val pathToCertificate = getLocationCertificatePath(systemId)
    try {
      curator.getVersioned[LocationCertificate](pathToCertificate / certificateId).map(cert => {
        new LocationCertificateCRUD(
                id = Some(certificateId),
                version = Some(cert.version),
                systemId = systemId,
                certificate = cert.data)
      })
    }catch{
      case e:Exception => None
    }
  }

  /**
   * Get all defined type/measurementMethod certificates.
   * @return list of TypeCertificateCRUD
   */
  def getAllTypeCertificates(): Seq[TypeCertificateCRUD] = {
    val path = getTypeCertificatePath
    val children = curator.getChildren(path)

    children.flatMap(childPath => {
      curator.getVersioned[MeasurementMethodType](childPath).map(versioned => {
      new TypeCertificateCRUD(
        id = Some(childPath.nodes.last.name),
        version = Some(versioned.version),
        certificate = versioned.data)
      })
    })
  }

  /**
   * Create or update the type/measurementMethod certificate.
   * @param cert the new or changed type certificate
   * @param user the user requesting the operation
   * @return ResponseMsg indicating if the operation was successful or not
   */
  def createOrUpdateTypeCertificate(cert:TypeCertificateCRUD)(implicit user:UserModel): ResponseMsg = {
    val pathToCertificate = getTypeCertificatePath
    val id = cert.id.getOrElse(cert.certificate.typeCertificate.id + cert.certificate.typeCertificate.revisionNr.toString)

    val errors = validateTypeCertificate(cert)

    if(errors.length > 0){
      return ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors.toList)
    }

    if(cert.version.isDefined) {
      //update
      try {
        curator.get[MeasurementMethodType](pathToCertificate / id) match {
          case (Some(previous)) => {
            curator.set(pathToCertificate / id, cert.certificate, cert.version.getOrElse(0))
            ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.update"))
          }
          case None => {
            ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.failed.update.not_exists"))
          }
        }
      }catch{
        case e:BadVersionException =>
          ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.failed.update_version_conflict"))
        case e:Exception =>
          ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.failed.update"))
      }
    } else {
      //insert
      try {
        if(!curator.exists(pathToCertificate)){
          curator.createEmptyPath(pathToCertificate)
        }
        curator.put(pathToCertificate / id, cert.certificate)
        ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.insert"))
      }catch{
        case e:Exception =>
          ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.success.failed"))
      }
    }
  }

  /**
   * Validate the new or changed type/measurementMethod certificate.
   * @param cert the new or changed type certificate
   * @param user the user requesting the operation
   * @return List of errors
   */
  private def validateTypeCertificate(cert: TypeCertificateCRUD)(implicit user:UserModel):List[FieldError]={
    var errors = List[FieldError]()
    //check MeasurementType fields has a value
    notEmpty(value=cert.certificate.typeDesignation, fieldKey="typeDesignation", errorKey="typecertificate.validate.typeDesignation_is_empty").map{errors ::= _ }
    notEmpty(value=cert.certificate.category, fieldKey="category", errorKey="typecertificate.validate.category_is_empty").map{errors ::= _ }
    notEmpty(value=cert.certificate.unitSpeed, fieldKey="speed", errorKey="typecertificate.validate.speed_is_empty").map{errors ::= _ }
    notEmpty(value=cert.certificate.unitLength, fieldKey="length", errorKey="typecertificate.validate.length_is_empty").map{errors ::= _ }
    notEmpty(value=cert.certificate.unitRedLight, fieldKey="redlight", errorKey="typecertificate.validate.redlight_is_empty").map{errors ::= _ }
    notEmpty(value=cert.certificate.displayRange, fieldKey="displayRange", errorKey="typecertificate.validate.displayRange_is_empty").map{errors ::= _ }
    notEmpty(value=cert.certificate.temperatureRange, fieldKey="tempRange", errorKey="typecertificate.validate.tempRange_is_empty").map{errors ::= _ }
    notEmpty(value=cert.certificate.permissibleError, fieldKey="permissibleError", errorKey="typecertificate.validate.permissibleError_is_empty").map{errors ::= _ }

    //checkTypeCertificate
    notEmpty(value=cert.certificate.typeCertificate.id, fieldKey="typeNr", errorKey="typecertificate.validate.typeNr_is_empty").map{errors ::= _ }
    notLowerThen[Int](value=cert.certificate.typeCertificate.revisionNr, fieldKey=Some("revision"), errorKey=Some("typecertificate.validate.revision_is_empty"), from=0).map{errors ::= _ }

    //check id and revision is unique
    val pathToCertificates = getTypeCertificatePath
    val children = curator.getChildren(pathToCertificates)

    val allCert = children.map(childPath => (childPath.nodes.last.name, curator.get[MeasurementMethodType](childPath)))

    val same = allCert.find {
      case (path, Some(c)) => c.typeCertificate.id == cert.certificate.typeCertificate.id &&
        c.typeCertificate.revisionNr == cert.certificate.typeCertificate.revisionNr &&
        path != cert.id.getOrElse("")
      case other => false
    }

    if (same.isDefined) {
      errors ::= FieldError(field="all", error=Messages("typecertificate.validate.same_key"))
    }
    errors
  }
  /**
   * Get all systemId which have installed this type/measurementMethod certificate.
   * @param certificateId the specified type certificate
   * @param user the user requesting the operation
   * @return List of systemIds
   */
  def getSystemsUsedTypeCertificate(certificateId:String)(implicit user:UserModel):Seq[String] ={
    getSystems.foldLeft(Seq[String]()) {
      case (list, system) => {
        val allInstalled = getInstalledCertificate(system.data.id)
        val used = allInstalled.find(_.typeId == certificateId)
        list ++ used.map(_ => system.data.id)
      }
    }
  }

  /**
   * Get all installed type/measurementMethod certificate for a specified system
   * @param systemId the specified systemID
   * @return List of MeasurementMethodInstallation
   */
  private def getInstalledCertificate(systemId:String): Seq[MeasurementMethodInstallation] = {
    val path = getInstalledTypePath(systemId)
    val children = curator.getChildren(path)
    children.flatMap(curator.get[MeasurementMethodInstallation](_))
  }

  /**
   * Delete a specified type/measurementMethod certificate
   * @param certificateId the id of the  type/measurementMethod certificate
   * @param user the user requested the operation
   * @return ResponseMsg indicating if the operation was successful or not
   */
  def deleteTypeCertificate(certificateId:String)(implicit user:UserModel): ResponseMsg = {
    val pathToCertificate = getTypeCertificatePath
    //check if it isn't used
    val systems = getSystemsUsedTypeCertificate(certificateId)
    if (systems.isEmpty) {
      try {
        curator.delete(pathToCertificate / certificateId)
        ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.delete"))
      }catch{
        case e:Exception =>
          ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.failed.delete"))
      }
    } else {
      ResponseMsg(description=Messages("responsemsg.unkown_input"),
        fields=List(FieldError(field="all", error=Messages("typecertificate.validate.used"))))
    }
  }

  /**
   * Get a specified type/measurementMethod certificate.
   * @param certificateId the Id of the specified typeCertificate
   * @param user the user requesting the operation
   * @return the requested typeCertificate
   */
  def getTypeCertificate(certificateId:String)(implicit user:UserModel): Option[TypeCertificateCRUD] = {
    val pathToCertificate = getTypeCertificatePath
    try {
      curator.getVersioned[MeasurementMethodType](pathToCertificate / certificateId).map(cert => {
        new TypeCertificateCRUD(
                id = Some(certificateId),
                version = Some(cert.version),
                certificate = cert.data)
      })
    }catch{
      case e:Exception => None
    }
  }

  /**
   * Get all available type/measurementMethod certificate for a system.
   * Only certificates which are not installed yet are available
   * @param systemId the specified system
   * @return list of available type/measurementMethod certificates.
   */
  def getAvailableTypeCertificates(systemId:String): Seq[TypeCertificateCRUD] = {
    val all: Seq[TypeCertificateCRUD] = getAllTypeCertificates()
    val usedBySystem: Seq[Any] = getInstalledCertificate(systemId).map(_.typeId)
    all.filterNot(cert => usedBySystem.contains(cert.id.getOrElse("")))
  }

  /**
   * Get all installed type/measurementMethod certificate of all systems available for the given user.
   * @param user the user requesting the operation
   * @return List of all type/measurementMethod certificates.
   */
  def getAllSystemsInstalledCertificates(implicit user:UserModel): Seq[InstalledCertificateShow] = {
    getSystems.foldLeft(Seq[InstalledCertificateShow]()){
      (list, system) => list ++ getAllInstalledCertificates(system.data.id, system.data.name)
    }
  }

  /**
   * Get all systems used by the specified type/measurementMethod certificate.
   * @param certId The id of the specified type certificate
   * @param user the user requesting the operation
   * @return List of all systems
   */
  def getAllUsedSystems(certId:String)(implicit user:UserModel): Seq[System] = {
    val systems = getSystemsUsedTypeCertificate(certId)
    systems.flatMap(systemId => Systems.findById(systemId).map(_.data))
  }

  /**
   * Get all systems which are not using the specified type/measurementMethod certificate.
   * @param certId The id of the specified type certificate
   * @param user the user requesting the operation
   * @return List of all systems
   */
  def getAllPossibleSystems(certId:String)(implicit user:UserModel): Seq[System] = {
    val systemIds = getSystemsUsedTypeCertificate(certId)
    getSystems.filterNot(system => systemIds.contains(system.data.id)).map(_.data)
  }

  /**
   * Get all type/measurementMethod certificate installed for specified system.
   * @param systemId the id of the specified system
   * @param systemName the name of the specified system used to create InstalledCertificateShow
   * @param user the user requesting the operation
   * @return List of all InstalledCertificateShow
   */
  private def getAllInstalledCertificates(systemId: String, systemName:String)(implicit user:UserModel): Seq[InstalledCertificateShow] = {
    val path = getInstalledTypePath(systemId)
    val children = curator.getChildren(path)

    val allInstall  =children.flatMap(childPath => {
      curator.getVersioned[MeasurementMethodInstallation](childPath).flatMap(versioned => {
        val cert = getTypeCertificate(versioned.data.typeId)
        cert.map(typeCert => {
          new InstalledCertificateShow(
          id = childPath.nodes.last.name,
          version = versioned.version,
          systemId = systemId,
          systemName = systemName,
          installDate = versioned.data.timeFrom,
          active = false,
          nrInstalled = children.size,
          certificate = typeCert.certificate)
        })
      })
    })
    val sorted = allInstall.sortBy(_.installDate)
    //update last not in the future to be active
    val now = java.lang.System.currentTimeMillis()
    val (past,future) = sorted.partition(_.installDate <= now)
    if (past.isEmpty) {
      past ++ future
    } else {
      (past.init :+ past.last.copy(active = true)) ++ future
    }
  }

  /**
   * Validate the installed certificate
   * @param systemId the systemId
   * @param cert the new installed Certificate
   * @param user the user requesting the operation
   * @return List of errors
   */
  private def validateInstalledCertificate(systemId: String, cert: InstalledCertificateCreate)(implicit user:UserModel):List[FieldError]={
    var errors = List[FieldError]()
    //check locationNr has value

    //check id and inspectionDate is unique
    val allCert = getAllInstalledCertificates(systemId, "")
    val same = allCert.find(c => c.id == cert.typeId)
    if (same.isDefined) {
      errors ::= FieldError(field="all", error=Messages("installcertificate.validate.same_key"))
    }
    errors
  }

  /**
   * Create a new installed certificate
   * @param systemId the systemId
   * @param cert the new installed Certificate
   * @param user the user requesting the operation
   * @return ResponseMsg indicating if the operation was successful or not
   */
  def createInstalledCertificate(systemId: String, cert: InstalledCertificateCreate)(implicit user:UserModel): ResponseMsg = {
    val pathToCertificate = getInstalledTypePath(systemId)
    val id = cert.typeId

    //validate
    val errors = validateInstalledCertificate(systemId, cert)

    if(errors.length > 0){
      return ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors.toList)
    }

    //insert
    try {
      if(!curator.exists(pathToCertificate)){
        curator.createEmptyPath(pathToCertificate)
      }
      curator.put(pathToCertificate / id, new MeasurementMethodInstallation(typeId = cert.typeId, timeFrom = cert.installDate))
      log(
        messageId="logging.installedcertificate.create",
        systemId=systemId, args=List(cert.typeId))
      ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.insert"))
    }catch{
      case e:Exception =>
        ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.success.failed"))
    }
  }
  /**
   * Create multiple installed certificate for different systems
   * @param cert the multiple installed Certificates
   * @param user the user requesting the operation
   * @return ResponseMsg indicating if the operation was successful or not
   */
  def multiCreateInstalledCertificate(cert: InstalledCertificateMultiCreate)(implicit user:UserModel): ResponseMsg = {

    val listCert = cert.systems.map(systemId => (systemId, new InstalledCertificateCreate(cert.installDate, cert.typeId)))

    //validate
    val errors = listCert.flatMap { case (systemId, install) => {
      val list = validateInstalledCertificate(systemId, install)
      //create detail error info
      list.map(err => err.copy(field = "install", error = systemId + ": "+ err.error))
    }}
    if(errors.length > 0){
      return ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=errors)
    }
    val results = listCert.map{case (systemId, install) =>  (systemId,createInstalledCertificate(systemId, install))}
    //create response
    //find failures
    val failures = results.filterNot {case (systemId, response) => response.success }
    if (failures.isEmpty) {
      //OK
      ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.insert"))
    } else {
      val msg = Messages("responsemsg.installedcertificate.failed.multicreate",failures.map(_._1).mkString(", "))
      return ResponseMsg(description=Messages("responsemsg.unkown_input"), fields=List(FieldError("",error=msg)))
    }

  }

  /**
   * Delete an installed certificate
   * @param systemId The systemId
   * @param certificateId the id of the specified install certificate
   * @param user the user requesting the operation
   * @return ResponseMsg indicating if the operation was successful or not
   */
  def deleteInstalledCertificate(systemId:String, certificateId:String)(implicit user:UserModel): ResponseMsg = {
    val pathToCertificate = getInstalledTypePath(systemId)
    try {
      curator.delete(pathToCertificate / certificateId)
      log(
        messageId="logging.installedcertificate.delete",
        systemId=systemId, args = List(certificateId))
      ResponseMsg(success=true, typeName=ResponseType.System, description=Messages("responsemsg.success.delete"))
    }catch{
      case e:Exception =>
        ResponseMsg(typeName=ResponseType.System, description=Messages("responsemsg.failed.delete"))
    }
  }

  /**
   * Get a specified installed certificate
   * @param systemId The systemId
   * @param certificateId the id of the specified install certificate
   * @param user the user requesting the operation
   * @return The requested certificate
   */
  def getInstalledCertificate(systemId:String, certificateId:String)(implicit user:UserModel): Option[InstalledCertificateShow] = {
    val system = Systems.findById(systemId)
    //use get all because we need to now if the installation is active
    val allInstall = getAllInstalledCertificates(systemId, system.map(_.data.name).getOrElse(""))
    //find the correct certificate
    allInstall.find(_.id == certificateId)
  }

  /**
   * retrieves a active installed type certificate for a given system id
   * @param systemId the systemId
   * @param user the user requesting the operation
   * @return the installed type certificate
   */
  def getSystemActiveCertificate(systemId:String)(implicit user:UserModel):Option[InstalledCertificateShow] = {
    val system = Systems.findById(systemId)

    system.flatMap(sys => {
      val installedCerts = getAllInstalledCertificates(systemId,sys.data.name)
      installedCerts.find(_.active)
    })
  }

  /**
   * Get all the Active certificates for all available systems
   * @param user the user requesting the operation
   * @return the installed type certificate
   */
  def getAllSystemActiveCertificate()(implicit user:UserModel):Seq[InstalledCertificateShow] = {
    val system = getSystems

    system.flatMap(sys => {
      val installedCerts = getAllInstalledCertificates(sys.data.id,sys.data.name)
      installedCerts.find(_.active)
    })
  }

  /**
   * updated a active certificate with the new checksum. If certificate does not exists a new one is created
   * @param certificateName name of certificate
   * @param checksum new checksum
   * @param time optional time
   * @return
   */
  def createOrUpdateActiveCertificate(systemId:String, certificateName:String, checksum:String, time:Long=(new Date).getTime){
    val pathToCertificate = Certificates.getActiveCertificatePath(systemId)
    if(!curator.exists(pathToCertificate)){
      curator.createEmptyPath(pathToCertificate)
    }

    val activeCertificate = curator.getVersioned[ActiveCertificate](pathToCertificate / certificateName)
    if(activeCertificate.isDefined){
      activeCertificate.map{
        certificate =>
          val updatedComponentCertificate = certificate.data.componentCertificate.copy(checksum = checksum)
          if(certificate.data.componentCertificate.checksum != checksum){
            curator.set(pathToCertificate / certificateName,
              certificate.data.copy(time=time, componentCertificate=updatedComponentCertificate),
              certificate.version)
          }
      }
    }else{
      val newCertificate = ActiveCertificate(time, ComponentCertificate(certificateName, checksum))
      curator.put(pathToCertificate / certificateName, newCertificate)
    }
  }

  /**
   * Get an active Certificate
   * @param systemId the systemID
   * @param certificateName The component name
   * @param user the user requesting the operation
   * @return the active certificate
   */
  def getActiveCertificate(systemId:String, certificateName:String)(implicit user:UserModel):Option[Versioned[ActiveCertificate]]={
    curator.getVersioned[ActiveCertificate](Certificates.getActiveCertificatePath(systemId) / certificateName)
  }
}
