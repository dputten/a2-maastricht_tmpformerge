/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package services

import common.{Config, EnumHack}
import csc.akkautils.DirectLoggingAdapter
import csc.curator.utils.{Curator, CuratorToolsImpl}
import csc.sectioncontrol.storagelayer.StorageFactory
import org.apache.curator.retry.RetryUntilElapsed

trait Storage extends Config with EnumHack {

  lazy val curator: Curator = {
    val ncurator = StorageFactory.newClient(Config.Zookeeper.host, new RetryUntilElapsed(1000 * 60 * 60 * 24 * 3, 5000))
    ncurator.foreach(c => if(!c.isStarted) c.start())
    new CuratorToolsImpl(
      curator = ncurator,
      log = new DirectLoggingAdapter(this.getClass.getName), finalFormats = enumFormats)
  }
}
