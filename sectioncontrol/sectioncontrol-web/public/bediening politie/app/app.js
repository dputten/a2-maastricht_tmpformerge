//'use strict';
var app = angular.module('BedieningTrajectControle', ['ui.bootstrap', 'ui.router',
    'ngCookies','appDirectives','ngMessages'])
    .constant("config", {
        "HTTP_BASE_URL": "/",
        "HTTP_GET_CURRENTUSER_URL":"bediening/getcurrentuser",
        "HTTP_POST_WIJZIGENWACHTWOORD_URL":"bediening/wijzigenwachtwoord",
        "HTTP_GET_AVAILABLE_TYPECERTIFICATES_URL":"bediening/markings/{0}",
        "HTTP_GET_LOCATIONCERTIFICATES_URL":"bediening/locationcertificates/all/{0}",
        "HTTP_PUT_LOCATIONCERTIFICATES_URL":"systems/{0}/certificates/{1}",
        "HTTP_PUT_TYPE_CERTIFICATES_URL":"typecertificates/{0}",
        "HTTP_POST_TYPE_CERTIFICATES_URL":"typecertificates",
        "HTTP_DELETE_TYPE_CERTIFICATES_URL":"typecertificates/{0}/delete",
        "HTTP_POST_LOCATIONCERTIFICATES_URL":"systems/{0}/certificates",
        "HTTP_POST_GEINSTALLEERDCERTIFICATES_URL":"systems/{0}/installedcertificates",
        "HTTP_DELETE_LOCATIONCERTIFICATES_URL":"systems/{0}/certificates/{1}/delete",
        "HTTP_DELETE_GEINSTALLEERDCERTIFICATES_URL":"systems/{0}/installedcertificates/{1}/delete",
        "HTTP_GET_ALL_INSTALLED_CERTIFICATES_URL":"installedcertificates/{0}",
        "HTTP_GET_ALL_TYPE_CERTIFICATES_URL":"typecertificateslist/{0}",
        "HTTP_GET_ALL_AVAILABLE_INSTALLED_CERTIFICATES_URL":"typecertificates/available/{0}",
        "HTTP_GET_TYPE_CERTIFICATE_BY_ID_URL":"typecertificates/{0}",
        "HTTP_GET_LOCATION_CERTIFICATE_BY_ID_URL":"systems/{0}/certificates/{1}",
        "HTTP_PUT_NAAR_STANDBY_CORRIDOR_URL":"bediening/{0}/states/{1}",
        "HTTP_PUT_NAAR_STANDBY_OFF_MAINTENANCE_CORRIDOR_URL":"bediening/{0}/changeStateOffStandbyMaintenance/{1}",
        "HTTP_PUT_TRAJECT_PREFERENCES_URL":"bediening/{0}/preferences",
        "HTTP_GET_TRAJECT_PREFERENCES_URL":"bediening/{0}/preferences",
        "HTTP_GET_SEARCH_LOGS_URL":"bediening/{0}/logs/system?from={1}&to={2}",
        "HTTP_GET_DOWNLOAD_LOGS_URL":"bediening/{0}/downloadlogs/system?from={1}&to={2}",
        "HTTP_POST_LOGIN_URL":"authenticate",
        "HTTP_GET_LOGOUT_URL":"bediening/logoutBediening",
        "HTTP_GET_TITLE_URL":"title",
        "HTTP_GET_DOWNLOAD_IMAGES_PASSAGEGEGEVENS_STATUS":"bediening/downloadImagesForPassageGegevensStatus?refId={0}",
        "HTTP_GET_DOWNLOAD_IMAGES_PASSAGEGEGEVENS":"bediening/downloadImagesForPassageGegevens?systemId={0}&gantryId={1}&from={2}&to={3}&kenteken={4}",
        "HTTP_DOWNLOAD_PASSAGEGEGEVENS": "{0}bediening/downloadpassagegegevens?systemId={1}&gantryId={2}&fromEpoch={3}&toEpoch={4}&withPhotos={5}&kenteken={6}&id={7}",
        "HTTP_GET_DOWNLOAD_PASSAGEGEGEVENS":"bediening/downloadpassagegegevens?systemId={0}&gantryId={1}&fromEpoch={2}&toEpoch={3}&withPhotos={4}&kenteken={5}",
        "HTTP_GET_DOWNLOAD_PASSAGEGEGEVENS_AANWEZIG_URL":"bediening/getPassagegegevensForDownloadAvailable?systemId={0}&gantryId={1}&fromEpoch={2}&toEpoch={3}&withPhotos={4}&kenteken={5}",
        "HTTP_GET_SEARCH_STATISTICS_TRAJECT_URL":"bediening/{0}/statistics/?from={1}&to={2}",
        "HTTP_GET_STATISTICS_TRAJECT_URL":"statistics/{0}",
        "HTTP_GET_DOWNLOAD_STATISTICS_TRAJECT_URL":"bediening/{0}/statistics/download/?from={1}&to={2}&id={3}",
        "HTTP_GET_DOWNLOAD_STATISTIC_TRAJECT_URL":"systems/{0}/dayreport/{1}",
        "HTTP_GET_STATISTIC_TRAJECT_URL":"systems/{0}/statistics/{1}",
        "HTTP_GET_SEARCH_STATISTICS_SECTIE_URL":"bediening/{0}/statistics/{1}?from={2}&to={3}",
        "HTTP_GET_DOWNLOAD_STATISTICS_SECTIE_URL":"bediening/{0}/statistics/download/{1}?from={2}&to={3}&id={4}",
        "HTTP_GET_PERIODESCHEMAS":"bediening/{0}/corridors/{1}/schedules",
        "HTTP_GET_PERIODESCHEMA_BY_ID":"bediening/{0}/corridors/{1}/schedules/{2}",
        "HTTP_PUT_PERIODESCHEMA":"bediening/{0}/corridors/{1}/schedules/{2}",
        "HTTP_DELETE_PERIODESCHEMA":"bediening/{0}/corridors/{1}/schedules/{2}",
        "HTTP_POST_PERIODESCHEMA":"bediening/{0}/corridors/{1}/schedules",
        "HTTP_GET_BESCHIKBARE_PASSAGEPUNTEN_IN":"bediening/availablefirst/systems/{0}/sections/{1}",
        "HTTP_GET_BESCHIKBARE_PASSAGEPUNTEN_UIT":"bediening/availablelast/systems/{0}/sections/{1}",
        "HTTP_GET":"get",
        "HTTP_PUT":"put",
        "HTTP_DELETE":"delete",
        "HTTP_POST":"post",
        "HTTP_GET_CORRIDOR":"bediening/{0}/corridors/{1}",
        "HTTP_GET_CORRIDOR_GET_START_AND_END_GANTRY":"systems/{0}/corridors/{1}/getStartGantryAndEndGantry",
        "HTTP_GET_CORRIDOR_REPORTINFO":"bediening/{0}/getCorridorReportInfo/{1}",
        "HTTP_PUT_CORRIDOR":"bediening/{0}/corridors/{1}",
        "HTTP_GET_SYSTEM":"bediening/getTraject/{0}",
        "HTTP_GET_SYSTEM_STATE":"systems/{0}/state",
        "HTTP_PUT_SYSTEM":"bediening/updateSystem/{0}",
        "HTTP_PUT_SYSTEM_TECHNICAL":"bediening/updateSystemTechnical/{0}",
        "HTTP_GET_ALL_USERS":"users/all",
        "HTTP_GET_USER":"users/{0}",
        "HTTP_DELETE_USER":"users/{0}",
        "HTTP_POST_USER":"users",
        "HTTP_PUT_USER":"users",
        "HTTP_GET_ALL_ROLES":"roles/all",
        "HTTP_GET_ALL_LANES":"lanes/all",
        "HTTP_GET_ALL_ACTIONS":"actions/all",
        "HTTP_PUT_LANE":"systems/{0}/gantries/{1}/lanes/{2}",
        "HTTP_POST_LANE":"systems/{0}/gantries/{1}/lanes",
        "HTTP_GET_LANE":"systems/{0}/gantries/{1}/lanes/{2}",
        "HTTP_GET_GANTRY":"systems/{0}/gantries/{1}",
        "HTTP_PUT_GANTRY":"systems/{0}/gantries/{1}",
        "HTTP_POST_GANTRY":"systems/{0}/gantries",
        "HTTP_DELETE_GANTRY":"gantries/{0}/gantries/{1}",
        "HTTP_GET_GANTRIES_FOR_SYSTEM":"gantries/{0}",
        "HTTP_GET_SECTION":"systems/{0}/sections/{1}",
        "HTTP_GET_SECTIONS":"systems/{0}/sections",
        "HTTP_PUT_SECTION":"systems/{0}/sections/{1}",
        "HTTP_POST_SECTION":"sections/{0}",
        "HTTP_DELETE_SECTION":"systems/{0}/sections/{1}",
        "HTTP_GET_ALARMEN_STORINGEN":"errors/{0}",
        "HTTP_GET_ALARMEN_STORING":"systems/{0}/corridors/{1}/errors/{2}/errorType/{3}",
        "HTTP_GET_ALARMEN_STORING_AFMELDEN":"systems/{0}/corridors/{1}/errors/{2}/errorType/{3}/signoff",
        "HTTP_GET_ALARMEN_STORING_NR_OF_ERRORS":"systems/{0}/numberoferrors",
        "HTTP_GET_ALARMEN_STORING_BEVESTIGEN":"systems/{0}/corridors/{1}/errors/{2}/errorType/{3}/confirm",
        "HTTP_GET_ROLE":"roles/{0}",
        "HTTP_PUT_ROLE":"roles",
        "HTTP_GET_ALL_VERDRAGSLANDEN":"system/getPossibleTreatyCountries",
        "HTTP_GET_ALL_CLASSIFY_COUNTRIES":"system/getClassifyCountries",
        "HTTP_PUT_CLASSIFY_COUNTRIES":"system/classifyCountries",
        "HTTP_GET_ALL_SYSTEMS_SMALL":"systems/allSmall",
        "HTTP_GET_SYSTEMS_IS_EDITABLE":"systems/iseditable/{0}",
        "HTTP_GET_SYSTEM_RESOURCE_LINK":"systemResource/link",
        "HTTP_DELETE_LANE":"systems/{0}/gantries/{1}/lanes/{2}",
        "HTTP_POST_CALCULATE_SEAL":"bediening/calculateSeal"

    })

    .run(function ($rootScope, $state, $stateParams,loginService,ApplicationCache) {

        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $rootScope.$on("$stateChangeStart",
            function (event, toState) {
                if (toState.authenticate && !loginService.isLoggedIn ()) {
                    event.preventDefault();
                    $state.go('login');
                }
            });

        $rootScope.$on('$routeChangeError', function(event, current, previous, error) {
            if(error.status === 404) {
                ApplicationCache.put("error","Pagina is niet gevonden.");
                $state.go("error");
            }
        });
    });

app.config(function ($httpProvider, $stateProvider, $urlRouterProvider,$logProvider) {
    $logProvider.debugEnabled(true);

    //initialize get if not there
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // http://stackoverflow.com/questions/16098430/angular-ie-caching-issue-for-http
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = '0';

    $stateProvider
        .state('handhaving', {
            url: "/handhaving",
            authenticate: true,
            views: {
                "contentView1": {
                    templateUrl: 'app/views/handhaving/autonome handhaving-traject-partial.html',
                    controller: 'handhaafController'
                },
                "contentView2": {
                    templateUrl: 'app/views/handhaving/handhaving-per-sectie-partial.html',
                    controller: 'handhaafController'
                },
                "contentView3": {
                    templateUrl: 'app/views/handhaving/certificering-partial.html',
                    controller: 'certificeringController'
                },
                "header": {
                    templateUrl: 'app/views/partial-top-header.html'
                },
                "headermenu": {
                    templateUrl: 'app/views/partial-top-tab-menu-header.html'
                },
                "logoutView": {
                    templateUrl: 'app/views/gebruikers/logout-partial.html',
                    controller: 'logincontroller'
                }
            }
        })

        .state('login', {
            url: "/login",
            authenticate: false,
            views: {
                "loginView": {
                    controller: 'login-controller'
                }
            }
        })

        .state('gebruikersaccount', {
            url: "/gebruikersaccount",
            authenticate: true,
            views: {
                "contentView1": {
                    templateUrl: 'app/views/gebruikers/gebruikers-partial.html',
                    controller: 'gebruikersaccountController'
                },
                "header": {
                    templateUrl: 'app/views/partial-top-header.html'
                },
                "headermenu": {
                    templateUrl: 'app/views/partial-top-tab-menu-header.html'
                },
                "logoutView": {
                    templateUrl: 'app/views/gebruikers/logout-partial.html',
                    controller: 'logincontroller'
                }
            }
        })

        .state('instellingen', {
            url: "/instellingen",
            url: "/instellingen/:title",
            authenticate: true,

            views: {
                "contentView1": {
                    templateUrl: 'app/views/instellingen/instellingen-partial.html',
                    controller: 'instellingenController',
                    resolve: {
                        isSystemEditable: function (ApplicationCache, trajectService) {
                            var traject = ApplicationCache.get('traject');
                            return trajectService.getIsSystemEditable(traject.systemId);
                        }
                    }
                },

                "header": {
                    templateUrl: 'app/views/partial-top-header.html'
                },
                "headermenu": {
                    templateUrl: 'app/views/partial-top-tab-menu-header.html'
                },
                "logoutView": {
                    templateUrl: 'app/views/gebruikers/logout-partial.html',
                    controller: 'logincontroller'
                },
                "submenubox": {
                    templateUrl: 'app/views/sub - dynamic.html'
                }
            }
        })

        .state('systeem', {
            url: "/systeem",
            authenticate: true,

            views: {
                "contentView1": {
                    templateUrl: 'app/views/systeem/systeem-partial.html',
                    controller: 'systeemcontroller',
                    resolve: {
                        getLinkSystemResources: function ( resourcesService) {
                            return resourcesService.getLinkSystemResources();
                        }
                    }
                },

                "header": {
                    templateUrl: 'app/views/partial-top-header.html'
                },
                "headermenu": {
                    templateUrl: 'app/views/partial-top-tab-menu-header.html',
                    controller:'topheadercontroller'
                },
                "logoutView": {
                    templateUrl: 'app/views/gebruikers/logout-partial.html',
                    controller: 'logincontroller'
                },
                "submenubox": {
                    templateUrl: 'app/views/systeem/systeem-pills-menu.html',
                    controller: 'systeemcontroller',
                    resolve: {
                        getLinkSystemResources: function ( resourcesService) {
                            return resourcesService.getLinkSystemResources();
                        }
                    }
                }
            }
        })

        .state('systeem.onderhoudentechnisch', {
            url: "/onderhoudentechnisch",
            authenticate: true,
            parent: 'systeem',
            templateUrl: "app/views/systeem/onderhouden-technisch-partial.html",
            controller: 'onderhoudenTechnischController'
        })

        .state('systeem.onderhoudengebruikers', {
            url: "/onderhoudengebruikers",
            authenticate: true,
            parent: 'systeem',
            templateUrl: "app/views/systeem/onderhouden-gebruikers-partial.html",
            controller: 'onderhoudenGebruikersController'
        })

        .state('systeem.onderhoudenverdragslanden', {
            url: "/onderhoudenverdragslanden",
            authenticate: true,
            parent: 'systeem',
            templateUrl: "app/views/systeem/onderhouden-verdragslanden-partial.html",
            controller: 'onderhoudenVerdragslandenController',
            resolve: {
                isSystemEditable: function (ApplicationCache, trajectService) {
                    var traject = ApplicationCache.get('traject');
                    return trajectService.getIsSystemEditable(traject.systemId);
                }
            }
        })

        .state('systeem.onderhoudenmeldingenstoringen', {
            url: "/onderhoudenmeldingenstoringen",
            authenticate: true,
            parent: 'systeem',
            templateUrl: "app/views/systeem/onderhouden-meldingen-storingen-partial.html",
            controller: 'onderhoudenmeldingenstoringenController'
        })

        .state('systeem.onderhoudencertificaten', {
            url: "/onderhoudencertificaten",
            authenticate: true,
            parent: 'systeem',
            templateUrl: "app/views/systeem/onderhouden-certificaten-partial.html",
            controller: 'onderhoudenCertificatenController',
            resolve: {
                isSystemEditable: function (ApplicationCache, trajectService) {
                    var traject = ApplicationCache.get('traject');
                    return trajectService.getIsSystemEditable(traject.systemId);
                }
            }
        })

        .state('systeem.onderhoudenCertificatenGeinstalleerd', {
            url: "/onderhoudenCertificatenGeinstalleerd",
            authenticate: true,
            parent: 'systeem',
            templateUrl: "app/views/systeem/onderhouden-certificaten-geinstalleerd-partial.html",
            controller: 'onderhoudenCertificatenGeinstalleerdController',
            resolve: {
                isSystemEditable: function (ApplicationCache, trajectService) {
                    var traject = ApplicationCache.get('traject');
                    return trajectService.getIsSystemEditable(traject.systemId);
                }
            }
        })

        .state('systeem.onderhoudenCertificatenlocatie', {
            url: "/onderhoudenCertificatenlocatie",
            authenticate: true,
            parent: 'systeem',
            templateUrl: "app/views/systeem/onderhouden-certificaten-locatie-partial.html",
            controller: 'onderhoudenCertificatenlocatieController',
            resolve: {
                isSystemEditable: function (ApplicationCache, trajectService) {
                    var traject = ApplicationCache.get('traject');
                    return trajectService.getIsSystemEditable(traject.systemId);
                }
            }
        })

        .state('systeem.onderhoudenpassagepunten', {
            url: "/onderhoudenpassagepunten",
            authenticate: true,
            parent: 'systeem',
            templateUrl: "app/views/systeem/onderhouden-passagepunten-partial.html",
            controller: 'onderhoudenPassagepuntenController',
            resolve: {
                isSystemEditable: function (ApplicationCache, trajectService) {
                    var traject = ApplicationCache.get('traject');
                    return trajectService.getIsSystemEditable(traject.systemId);
                }
            }
        })

        .state('systeem.onderhoudenrijbanen', {
            url: "/onderhoudenrijbanen",
            authenticate: true,
            parent: 'systeem',
            templateUrl: "app/views/systeem/onderhouden-rijbanen-partial.html",
            controller: 'onderhoudenrijbanenController',
            resolve: {
                isSystemEditable: function (ApplicationCache, trajectService) {
                    var traject = ApplicationCache.get('traject');

                    return trajectService.getIsSystemEditable(traject.systemId);
                }
            }
        })

        .state('systeem.onderhoudenSecties', {
            url: "/onderhoudenSecties",
            authenticate: true,
            parent: 'systeem',
            templateUrl: "app/views/systeem/onderhouden-secties-partial.html",
            controller: 'onderhoudenSectiesController',
            resolve: {
                isSystemEditable: function (ApplicationCache, trajectService) {
                    var traject = ApplicationCache.get('traject');
                    return trajectService.getIsSystemEditable(traject.systemId);
                }
            }
        })

        .state('systeem.onderhoudenCorridors', {
            url: "/onderhoudenCorridors",
            authenticate: true,
            parent: 'systeem',
            templateUrl: "app/views/systeem/onderhouden-corridors-partial.html",
            controller: 'onderhoudenCorridorsController',
            resolve: {
                isSystemEditable: function (ApplicationCache, trajectService) {
                    var traject = ApplicationCache.get('traject');
                    return trajectService.getIsSystemEditable(traject.systemId);
                }
            }
        })

        .state('systeem.onderhoudenTrajectcontrolesysteem', {
            url: "/onderhoudenTrajectcontrolesysteem",
            authenticate: true,
            parent: 'systeem',
            templateUrl: "app/views/systeem/onderhouden-trajectcontrolesysteem-partial.html",
            controller: 'onderhoudenTrajectcontrolesysteemController',
            resolve: {
                isSystemEditable: function (ApplicationCache, trajectService) {
                    var traject = ApplicationCache.get('traject');
                    return trajectService.getIsSystemEditable(traject.systemId);
                }
            }
        })

        .state('systeem.onderhoudenRollen', {
            url: "/onderhoudenRollen",
            authenticate: true,
            parent: 'systeem',
            templateUrl: "app/views/systeem/onderhouden-rollen-partial.html",
            controller: 'onderhoudenRollenController'
        })

        //http://stackoverflow.com/questions/25962417/how-to-handle-error-in-angular-ui-routers-resolve
        .state('systeem.onderhoudenStatistics', {
            url: "/onderhoudenStatistics",
            authenticate: true,
            parent: 'systeem',
            templateUrl: "app/views/systeem/onderhouden-statistics-partial.html",
            controller: 'onderhoudenStatisticsController',
            resolve: {
                statistics: function (ApplicationCache, StatisticsService) {
                    var traject = ApplicationCache.get('traject');
                    return StatisticsService.getStatisticsForSystem(traject.systemId);
                }
            }
        })

        .state('passagegegevens', {
            url: "/passagegegevens",
            authenticate: true,

            views: {
                "contentView1": {
                    templateUrl: 'app/views/passagegegevens/passagegegevens-partial.html',
                    controller: 'passagegegevensController'
                },

                "header": {
                    templateUrl: 'app/views/partial-top-header.html'
                },
                "headermenu": {
                    templateUrl: 'app/views/partial-top-tab-menu-header.html'
                },
                "logoutView": {
                    templateUrl: 'app/views/gebruikers/logout-partial.html',
                    controller: 'logincontroller'
                }
            }
        })

        .state('error', {
            url: "/error",
            authenticate: true,

            views: {
                "contentView1": {
                    templateUrl: 'app/views/error.html',
                    controller: 'errorController'
                },

                "header": {
                    templateUrl: 'app/views/partial-top-header.html'
                },
                "headermenu": {
                    templateUrl: 'app/views/partial-top-tab-menu-header.html'
                },
                "logoutView": {
                    templateUrl: 'app/views/gebruikers/logout-partial.html',
                    controller: 'logincontroller'
                }
            }
        })

        .state('statistieken', {
            url: "/statistieken",
            url: "/statistieken/:title",
            authenticate: true,

            views: {
                "contentView1": {
                    templateUrl: 'app/views/statistieken/statistieken-partial.html',
                    controller: 'statistiekenController'
                },

                "header": {
                    templateUrl: 'app/views/partial-top-header.html'
                },
                "headermenu": {
                    templateUrl: 'app/views/partial-top-tab-menu-header.html'
                },
                "logoutView": {
                    templateUrl: 'app/views/gebruikers/logout-partial.html',
                    controller: 'logincontroller'
                },
                "submenubox": {
                    templateUrl: 'app/views/sub - dynamic.html'
                }
            }
        })

        .state('logout', {
            url: "/logout",
            authenticate: true
        })

        .state('logboek', {
            url: "/logboek",
            authenticate: true,

            views: {
                "contentView1": {
                    templateUrl: 'app/views/logboek/logboek-partial.html',
                    controller: 'logboekController'
                },

                "header": {
                    templateUrl: 'app/views/partial-top-header.html'
                },
                "headermenu": {
                    templateUrl: 'app/views/partial-top-tab-menu-header.html'
                },
                "logoutView": {
                    templateUrl: 'app/views/gebruikers/logout-partial.html',
                    controller: 'logincontroller'
                }
            }
        })
    ;

    // catch all route and send users to the handhaving page
    $urlRouterProvider.otherwise('/handhaving');

});

//http://weblogs.asp.net/dwahlin/building-an-angularjs-modal-service
app.service('modalService', ['$modal',
    function ($modal) {

        var modalDefaults = {
            backdrop: true,
            keyboard: true,
            modalFade: true,
            templateUrl: 'app/views/systeem/modal-confirm-partial.html'
        };

        var modalOptions = {
            closeButtonText: 'Nee',
            actionButtonText: 'Ja',
            headerText: 'Verwijderen?',
            bodyText: 'Weet u zeker dat u de geselecteerde gegevens wilt verwijderen?'
        };

        this.showModal = function (customModalDefaults, customModalOptions) {
            if (!customModalDefaults) customModalDefaults = {};
            customModalDefaults.backdrop = 'static';
            return this.show(customModalDefaults, customModalOptions);
        };

        this.show = function (customModalDefaults, customModalOptions) {
            //Create temp objects to work with since we're in a singleton service
            var tempModalDefaults = {};
            var tempModalOptions = {};

            //Map angular-ui modal custom defaults to modal defaults defined in service
            angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

            //Map modal.html $scope custom properties to defaults defined in service
            angular.extend(tempModalOptions, modalOptions, customModalOptions);

            if (!tempModalDefaults.controller) {
                tempModalDefaults.controller = function ($scope, $modalInstance) {
                    $scope.modalOptions = tempModalOptions;
                    $scope.modalOptions.ok = function (result) {
                        $modalInstance.close(result);
                    };
                    $scope.modalOptions.close = function (result) {
                        $modalInstance.dismiss('cancel');
                    };
                }
            }

            return $modal.open(tempModalDefaults).result;
        };
    }]);

// Set up the cache
app.factory('ApplicationCache', function($cacheFactory) {
    return $cacheFactory('ApplicationCache');
});
