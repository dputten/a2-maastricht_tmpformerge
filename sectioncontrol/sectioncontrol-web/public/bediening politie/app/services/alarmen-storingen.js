app.factory("AlarmenSoringenService", function($cookies, $http, $log, $q, config) {
    return {
        getAlarmenStoringen: function(systemId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_ALARMEN_STORINGEN.format(systemId)
            }).success(function(data) {

                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        getAlarmenStoring: function(systemId,errorId,errorType,corridorId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_ALARMEN_STORING.format(systemId,corridorId,errorId,errorType)
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        bevestigen: function(systemId,errorId,errorType,corridorId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_ALARMEN_STORING_BEVESTIGEN.format(systemId,corridorId,errorId,errorType)
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        afmelden: function(systemId,errorId,errorType,corridorId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_ALARMEN_STORING_AFMELDEN.format(systemId,corridorId,errorId,errorType)
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        numberOfErrors: function(systemId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_ALARMEN_STORING_NR_OF_ERRORS.format(systemId)
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        }
    }
});

