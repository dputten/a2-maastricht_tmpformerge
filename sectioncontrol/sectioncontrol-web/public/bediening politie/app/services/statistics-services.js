/**
 * Created by CSC on 10/21/14.
 */
app.factory("StatisticsService", function ($cookies, $http, $log, $q,config) {
    return {
        getStatistics: function (begin,end,systemId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_SEARCH_STATISTICS_TRAJECT_URL.format(systemId,begin,end)
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });
            return deferred.promise;
        },
        getStatisticsSectie: function (begin,end,systemId,sectieId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_SEARCH_STATISTICS_SECTIE_URL.format(systemId,sectieId,begin,end)
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });
            return deferred.promise;
        },
        getCorridorReportInfo: function (systemId,corridorId) {
        var deferred = $q.defer();
        $http({
            method: config.HTTP_GET,
            url: config.HTTP_BASE_URL+ config.HTTP_GET_CORRIDOR_REPORTINFO.format(systemId,corridorId)
        }).success(function(data) {
            deferred.resolve(data);
        }).error(function(data, status, headers, config) {
            $log.debug(data);
            $log.debug(status);
            $log.debug(headers);
            $log.debug(config);
            deferred.reject(data);
        });
        return deferred.promise;
    },
        getStatisticsForSystem: function (systemId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_STATISTICS_TRAJECT_URL.format(systemId)
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });
            return deferred.promise;
        },
        getStatisticsReportForSystem: function (systemId,reportId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_STATISTIC_TRAJECT_URL.format(systemId,reportId)
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });
            return deferred.promise;
        }
    }
});