/**
 * Created by CSC on 10/21/14.
 */
app.factory("VerdragslandenService", function($cookies, $http, $log, $q, config) {
    return {
        getAllVerdragslanden: function() {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_ALL_VERDRAGSLANDEN
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                 $log.debug(data);
                 $log.debug(status);
                 $log.debug(headers);
                 $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        getClassifyLanden: function() {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_ALL_CLASSIFY_COUNTRIES
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                 $log.debug(data);
                 $log.debug(status);
                 $log.debug(headers);
                 $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        putClassifyLanden: function(dataToSave) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_PUT,
                url: config.HTTP_BASE_URL + config.HTTP_PUT_CLASSIFY_COUNTRIES,
                data:dataToSave
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                 $log.debug(data);
                 $log.debug(status);
                 $log.debug(headers);
                 $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        }
    }
});

