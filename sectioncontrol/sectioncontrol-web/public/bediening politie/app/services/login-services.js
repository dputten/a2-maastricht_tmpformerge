app.factory("loginService", function ($cookies, $http, $log, $q, config) {
    var isUserLoggedIn;
    var userName;

    return {
        getUserName:function()
        {
            return userName;
        },
        logUserOut:function()
        {
            isUserLoggedIn=false;
        },
        isLoggedIn:function()
        {
            if (typeof isUserLoggedIn === 'undefined'){
                return false;
            }
            else {
                return isUserLoggedIn;
            }
        },
        login: function (username, password) {
            var deferred = $q.defer();

            $http({
                method: config.HTTP_POST,
                url: config.HTTP_BASE_URL + config.HTTP_POST_LOGIN_URL,
                data: {
                    username: username,
                    password: password
                }
            }).success(function (data) {
                isUserLoggedIn=true;
                userName=username;

                deferred.resolve(data);
            }).error(function (msg, code) {
                $log.debug(msg);
                $log.debug(code);
                isUserLoggedIn=false;
                deferred.reject(msg);
            });

            return deferred.promise;
        },
        logout: function () {
            var deferred = $q.defer();

            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_LOGOUT_URL
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(msg, code) {
                $log.debug(msg);
                $log.debug(code);
                deferred.reject(msg);
            });
            return deferred.promise;
        },
        getTitle: function () {
             var deferred = $q.defer();

             $http({
                 method: config.HTTP_GET,
                 url: config.HTTP_BASE_URL + config.HTTP_GET_TITLE_URL
             }).success(function(data) {
                 deferred.resolve(data);
             }).error(function(msg, code) {
                 $log.debug(msg);
                 $log.debug(code);
                 deferred.reject(msg);
             });
             return deferred.promise;
        }
    }
});
