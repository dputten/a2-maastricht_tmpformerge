/**
 * Created by CSC on 10/21/14.
 */
app.factory("OnderhoudenGebruikersService", function($cookies, $http, $log, $q, config) {
    return {
        getAllGebruikers: function() {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_ALL_USERS
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(msg, code) {
                $log.debug(msg);
                $log.debug(code);
                deferred.reject(msg, code);
            });

            return deferred.promise;
        },
        getGebruiker: function(id) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_USER.format(id)
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(msg, code) {
                $log.debug(msg);
                $log.debug(code);
                deferred.reject(msg, code);
            });

            return deferred.promise;
        },
        getAllRoles: function() {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_ALL_ROLES
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(msg, code) {
                $log.debug(msg);
                $log.debug(code);
                deferred.reject(msg, code);
            });

            return deferred.promise;
        },
        putGebruiker: function(dataToSend) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_PUT,
                url: config.HTTP_BASE_URL + config.HTTP_PUT_USER,
                data:dataToSend
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(msg, code) {
                $log.debug(msg);
                $log.debug(code);
                deferred.reject(msg, code);
            });

            return deferred.promise;
        },
        postGebruiker: function(dataToSend) {
        var deferred = $q.defer();
        $http({
            method: config.HTTP_POST,
            url: config.HTTP_BASE_URL + config.HTTP_POST_USER,
            data:dataToSend
        }).success(function(data) {
            deferred.resolve(data);
        }).error(function(msg, code) {
            $log.debug(msg);
            $log.debug(code);
            deferred.reject(msg, code);
        });

        return deferred.promise;
        },
        deleteGebruiker: function(id,version) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_DELETE,
                url: config.HTTP_BASE_URL + config.HTTP_DELETE_USER.format(id),
                headers: {"Content-Type": "application/json;charset=utf-8"},
                data:
                {version:version}
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(msg, code) {
                $log.debug(msg);
                $log.debug(code);
                deferred.reject(msg, code);
            });

            return deferred.promise;
        },
        getAllSystemsSmall: function() {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_ALL_SYSTEMS_SMALL
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(msg, code) {
                $log.debug(msg);
                $log.debug(code);
                deferred.reject(msg, code);
            });

            return deferred.promise;
        }
    }
});