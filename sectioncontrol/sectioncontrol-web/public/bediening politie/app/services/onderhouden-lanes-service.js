/**
 * Created by CSC on 10/21/14.
 */
app.factory("OnderhoudenLanesService", function($cookies, $http, $log, $q, config) {
    return {
        getAllLanes: function() {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_ALL_LANES
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        getLane: function(systemId,gantryId,laneId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_LANE.format(systemId,gantryId,laneId)
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        putLane: function(systemId,gantryId,laneId,dataToSend) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_PUT,
                url: config.HTTP_BASE_URL + config.HTTP_PUT_LANE.format(systemId,gantryId,laneId),
                data:dataToSend
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        postLane: function(systemId,gantryId,laneId,dataToSend) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_POST,
                url: config.HTTP_BASE_URL + config.HTTP_POST_LANE.format(systemId,gantryId),
                data:dataToSend
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        deleteLane: function(systemId,gantryId,laneId,dataToSend) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_DELETE,
                url: config.HTTP_BASE_URL + config.HTTP_DELETE_LANE.format(systemId,gantryId,laneId),
                headers: {"Content-Type": "application/json;charset=utf-8"},
                data:{version:dataToSend}
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        }
    }
});

