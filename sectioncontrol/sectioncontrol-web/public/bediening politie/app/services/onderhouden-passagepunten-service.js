/**
 * Created by CSC on 10/21/14.
 */
app.factory("PassagepuntenService", function($cookies, $http, $log, $q, config) {
    return {
        getGantry: function(systemId,gantryId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_GANTRY.format(systemId,gantryId)
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        putGantry: function(systemId,gantryId,dataToSend) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_PUT,
                url: config.HTTP_BASE_URL + config.HTTP_PUT_GANTRY.format(systemId,gantryId),
                data:dataToSend
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        postGantry: function(systemId,dataToSend) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_POST,
                url: config.HTTP_BASE_URL + config.HTTP_POST_GANTRY.format(systemId),
                data:dataToSend
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        deleteGantry: function(systemId,gantryId,dataToSend) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_DELETE,
                url: config.HTTP_BASE_URL + config.HTTP_DELETE_GANTRY.format(systemId,gantryId),
                headers: {"Content-Type": "application/json;charset=utf-8"},
                data:{version:dataToSend}
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        getGantries: function(systemId) {
        var deferred = $q.defer();
        $http({
            method: config.HTTP_GET,
            url: config.HTTP_BASE_URL + config.HTTP_GET_GANTRIES_FOR_SYSTEM.format(systemId)
        }).success(function(data) {
            deferred.resolve(data);
        }).error(function(data, status, headers, config) {
            $log.debug(data);
            $log.debug(status);
            $log.debug(headers);
            $log.debug(config);
            deferred.reject(data);
        });

        return deferred.promise;
    }
    }
});



