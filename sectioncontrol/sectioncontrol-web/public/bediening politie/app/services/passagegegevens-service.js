/**
 * Created by CSC on 10/21/14.
 */
app.factory("PassagegegevensService", function ($cookies, $http, $log, $q,config) {
    return {
        downloadImagesForPassageGegevens: function(searchData) {
            $log.debug("downloadImagesForPassageGegevens: " + JSON.stringify(searchData));
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_DOWNLOAD_IMAGES_PASSAGEGEGEVENS.format(
                    searchData.systemId,
                    searchData.gantryId,
                    searchData.fromEpoch,
                    searchData.toEpoch,
                    searchData.kenteken)
            }).success(function(data) {
                $log.debug("downloadImagesForPassageGegevens http success");
                $log.debug("data: " + JSON.stringify(data));
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug("downloadImagesForPassageGegevens failed");
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });
            return deferred.promise;
        },
        checkDownloadStatusForImagesForPassageGegevens: function(refId) {
            $log.debug("checkDownloadStatusForImagesForPassageGegevens: " + refId)
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_DOWNLOAD_IMAGES_PASSAGEGEGEVENS_STATUS.format(refId)
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug("checkDownloadStatusForImagesForPassageGegevens failed")
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });
            return deferred.promise;
        },
        downloadPassagegegevens: function (searchData) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_DOWNLOAD_PASSAGEGEGEVENS.format(
                    searchData.systemId,
                    searchData.gantryId,
                    searchData.fromEpoch,
                    searchData.toEpoch,
                    searchData.withPhotos?1:0,
                    searchData.kenteken)
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });
            return deferred.promise;
        },
        passagegegevensAanwezig: function (searchData) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_DOWNLOAD_PASSAGEGEGEVENS_AANWEZIG_URL.format(
                    searchData.systemId,
                    searchData.gantryId,
                    searchData.fromEpoch,
                    searchData.toEpoch,
                    searchData.withPhotos?1:0,
                    searchData.kenteken)
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });
            return deferred.promise;
        }

    }
});

