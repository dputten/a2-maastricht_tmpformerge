/**
 * Created by CSC on 10/21/14.
 */
app.factory("SectionsService", function($cookies, $http, $log, $q, config) {
    return {
        getSection: function (systemId, sectionId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_SECTION.format(systemId, sectionId)
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        getSections: function (systemId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_SECTIONS.format(systemId)
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },

        putSection: function (systemId, sectionId, dataToSend) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_PUT,
                url: config.HTTP_BASE_URL + config.HTTP_PUT_SECTION.format(systemId, sectionId),
                data: dataToSend
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        postSection: function (systemId, dataToSend) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_POST,
                url: config.HTTP_BASE_URL + config.HTTP_POST_SECTION.format(systemId),
                data: dataToSend
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        deleteSection: function (systemId, sectionId, dataToSend) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_DELETE,
                url: config.HTTP_BASE_URL + config.HTTP_DELETE_SECTION.format(systemId, sectionId),
                headers: {"Content-Type": "application/json;charset=utf-8"},
                data: {version: dataToSend}
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        }
    }
});

