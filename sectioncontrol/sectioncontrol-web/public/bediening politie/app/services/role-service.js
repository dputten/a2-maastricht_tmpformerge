/**
 * Created by CSC on 11/5/14.
 */
app.factory("RoleService", ['ApplicationCache',function (applicationCache) {
    return {
        isInRolePolitie: function () {
            var gebruiker = applicationCache.get('gebruiker');
            if(gebruiker!=undefined)
                return gebruiker.rol == 'Politie';
            else
                return false;
        },

        isInRoleTactischBeheer: function () {
            var  gebruiker = applicationCache.get('gebruiker');
            if(gebruiker!=undefined)
                return gebruiker.rol == 'Tactisch beheer';
            else
                return false;
        },

        isInRoleTechnischBeheer: function () {
            var  gebruiker = applicationCache.get('gebruiker');
            if(gebruiker!=undefined)
                return gebruiker.rol == 'Technisch beheer';
            else
                return false;
        }
    }
}]);