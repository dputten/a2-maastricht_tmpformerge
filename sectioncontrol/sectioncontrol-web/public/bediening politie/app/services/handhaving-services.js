app.factory("HandhavingService", function($cookies, $http, $log, $q, config) {
    return {
        veranderCorridorStatus: function(systemId, from,to,version,corridorId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_PUT,
                url: config.HTTP_BASE_URL + config.HTTP_PUT_NAAR_STANDBY_CORRIDOR_URL.format(systemId,corridorId),
                data: {
                    from: from,
                    to: to,
                    version: version
                }
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });
            return deferred.promise;
        },
        veranderCorridorStatusBeheer: function(systemId, from,to,version,corridorId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_PUT,
                url: config.HTTP_BASE_URL + config.HTTP_PUT_NAAR_STANDBY_OFF_MAINTENANCE_CORRIDOR_URL.format(systemId,corridorId),
                data: {
                    from: from,
                    to: to,
                    version: version
                }
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });
            return deferred.promise;
        },
        getSystemState: function (systemId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL+ config.HTTP_GET_SYSTEM_STATE.format(systemId)
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });
        return deferred.promise;
        }
    }
});
