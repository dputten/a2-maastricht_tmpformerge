/**
 * Created by CSC on 11/7/14.
 */
/*
 Deze service bundeld de certificaten en geeft ze als 1 resultaat terug.
 */
app.factory("CertificeringService", function ($http, $log, $q, config) {
    return {
        getCertificeringForTraject: function (systemId) {
            var certificaten = [];
            var typePromise = this.getTypeCertificering(systemId);
            var locatiePromise = this.getLocatieCertificering(systemId);

            $q.all({ first: typePromise, second: locatiePromise})
                .then(function (results) {
                    $log.debug(results.first);
                    $log.debug(results.second);

                    if (results.first != undefined) {
                        certificaten.push({
                            keuringsNummer: results.first.certificate.typeCertificate.id,
                            keuringsDatum: getDateFromString(results.first.installDate),
                            geldigTot: "",
                            omschrijving:"Type"
                        });
                    }

                    // Handle locatiePromise
                    for (var x = 0; x < results.second.length; x++) {
                        certificaten.push({
                            keuringsNummer: results.second[x].certificate.id,
                            keuringsDatum: getDateFromEpochString(results.second[x].certificate.inspectionDate),
                            geldigTot: getDateFromEpochString(results.second[x].certificate.validTime),
                            omschrijving:"Configuratie"
                        });
                    }

                }, function (reason) {
                    $log.debug("in de error");
                    $log.debug(reason.first, reason.second);
                });

            return certificaten;
        },
        getTypeCertificering: function (systemId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_AVAILABLE_TYPECERTIFICATES_URL.format(systemId)
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (msg, code) {
                $log.debug(msg);
                $log.debug(code);
                deferred.reject(msg);
            });
            return deferred.promise;
        },
        getLocatieCertificering: function (systemId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_LOCATIONCERTIFICATES_URL.format(systemId)
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (msg, code) {
                $log.debug(msg);
                $log.debug(code);
                deferred.reject(msg);
            });
            return deferred.promise;
        },
        getAllInstalledCertificering: function (systemId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_ALL_INSTALLED_CERTIFICATES_URL.format(systemId)
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (msg, code) {
                $log.debug(msg);
                $log.debug(code);
                deferred.reject(msg);
            });
            return deferred.promise;
        },
        getAllTypeCertificering: function (systemId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_ALL_TYPE_CERTIFICATES_URL.format(systemId)
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (msg, code) {
                $log.debug(msg);
                $log.debug(code);
                deferred.reject(msg);
            });
            return deferred.promise;
        },
        getTypeCertificeringById: function (certId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_TYPE_CERTIFICATE_BY_ID_URL.format(certId)
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (msg, code) {
                $log.debug(msg);
                $log.debug(code);
                deferred.reject(msg);
            });
            return deferred.promise;
        },
        getLocatieCertificeringById: function (systemId, certId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_LOCATION_CERTIFICATE_BY_ID_URL.format(systemId, certId)
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (msg, code) {
                $log.debug(msg);
                $log.debug(code);
                deferred.reject(msg);
            });
            return deferred.promise;
        },
        calculateSeal: function (dataToSend) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_POST,
                url: config.HTTP_BASE_URL + config.HTTP_POST_CALCULATE_SEAL,
                data: dataToSend
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        getAllAvailableInstalledCertificering: function (systemId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_ALL_AVAILABLE_INSTALLED_CERTIFICATES_URL.format(systemId)
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (msg, code) {
                $log.debug(msg);
                $log.debug(code);
                deferred.reject(msg);
            });
            return deferred.promise;
        },
        putLocatieCertificering: function (systemId, certId, dataToSend) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_PUT,
                url: config.HTTP_BASE_URL + config.HTTP_PUT_LOCATIONCERTIFICATES_URL.format(systemId, certId),
                data: dataToSend
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        postLocatieCertificering: function (systemId,dataToSend) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_POST,
                url: config.HTTP_BASE_URL + config.HTTP_POST_LOCATIONCERTIFICATES_URL.format(systemId),
                data: dataToSend
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        deleteLocatieCertificering: function (systemId, certId, dataToSend) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_DELETE_LOCATIONCERTIFICATES_URL.format(systemId, certId),
                data: {version: dataToSend}
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        deleteGeinstalleerdCertificering: function (systemId, certId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_DELETE_GEINSTALLEERDCERTIFICATES_URL.format(systemId, certId)
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        postGeinstalleerdeCertificering: function (systemId, dataToSend) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_POST,
                url: config.HTTP_BASE_URL + config.HTTP_POST_GEINSTALLEERDCERTIFICATES_URL.format(systemId),
                data: dataToSend
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        putTypeCertificering: function (certId, dataToSend) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_PUT,
                url: config.HTTP_BASE_URL + config.HTTP_PUT_TYPE_CERTIFICATES_URL.format(certId),
                data: dataToSend
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        postTypeCertificering: function (dataToSend) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_POST,
                url: config.HTTP_BASE_URL + config.HTTP_POST_TYPE_CERTIFICATES_URL,
                data: dataToSend
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        deleteTypeCertificering: function (certId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_DELETE_TYPE_CERTIFICATES_URL.format(certId)
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        }
    }
});
