/**
 * Created by CSC on 10/21/14.
 */
app.factory("GebruikersService", function($cookies, $http, $log, $q, config) {
    return {
        wijzigenWachtwoord: function(huidigWachtwoord, nieuwWachtwoord) {
            var deferred = $q.defer();
            $log.debug(huidigWachtwoord);
            $log.debug(nieuwWachtwoord);
            $http({
                method: config.HTTP_PUT,
                url: config.HTTP_BASE_URL + config.HTTP_POST_WIJZIGENWACHTWOORD_URL,
                data: {
                    oldPassword: huidigWachtwoord,
                    newPassword: nieuwWachtwoord
                }
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(msg, code) {
                $log.debug(msg);
                $log.debug(code);
                deferred.reject(msg, code);
            });

            return deferred.promise;
        },
        getCurrentUser: function() {

            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_CURRENTUSER_URL
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });
            return deferred.promise;
        }
    }
});