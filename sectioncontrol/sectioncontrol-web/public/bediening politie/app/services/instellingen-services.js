/**
 * Created by CSC on 10/3/14.
 */

app.factory("InstellingenServiceTraject", function ($cookies, $http, $log, $q) {
    return {
        getTraject: function (trajectId) {
            var traject={
                esaPardonTijd:6,
                technischePardontijd:8,
                dataBewaarPeriode:12
            };

            return traject;
        }
    }
});

app.factory("InstellingenServiceSnelheidLimietenMapper", function () {
    return {
        getSnelheidslimietOmschrijving: function (code) {
            var snelheidslimieten=[
                {
                    code:"AB",
                    voertuigklasse:"Autobus"
                },
                {
                    code:"AO",
                    voertuigklasse:"Motorvoertuig"
                },
                {
                    code:"AS",
                    voertuigklasse:"Motorvoertuig"
                },
                {
                    code:"AT",
                    voertuigklasse:"Autobus"
                },
                {
                    code:"CA",
                    voertuigklasse:"Bestelauto"
                },
                {
                    code:"CB",
                    voertuigklasse:"Kampeerauto"
                },
                {
                    code:"CP",
                    voertuigklasse:"Kampeerauto"
                },
                {
                    code:"MF",
                    voertuigklasse:"Motorfiets"
                },
                {
                    code:"MV",
                    voertuigklasse:"Personenauto"
                },
                {
                    code:"PA",
                    voertuigklasse:"Personenauto"
                },
                {
                    code:"VA",
                    voertuigklasse:"Vrachtauto"
                },
                {
                    code:"MH",
                    voertuigklasse:"Motorfiets met aanhangwagen"
                }
            ];

            var result=snelheidslimieten.find(function(e){ return e.code == code; });

            if(result==undefined)
                return "";

            return result.voertuigklasse;
        }
    }
});

app.factory("InstellingenServiceSectieSoortWeg", function () {
    return {
        getSoortenWeg: function () {
            var soorten=[];
            soorten.push(
                {
                    id:1,
                    Omschrijving:"autosnelweg"
                },
                {
                    id:2,
                    Omschrijving:"autoweg"
                },
                {
                    id:3,
                    Omschrijving:"weg buiten de bebouwde kom"
                },
                {
                    id:5,
                    Omschrijving:"weg binnen de bebouwde kom"
                })
            ;

            return soorten;
        }
    }
});

app.factory("InstellingenService", function ($cookies, $http, $log, $q,config) {
    return {
        postToleranties: function (systemId,dataToSave) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_PUT,
                url: config.HTTP_BASE_URL+ config.HTTP_PUT_TRAJECT_PREFERENCES_URL.format(systemId),
                data: {
                    version:dataToSave.version,
                    preference:dataToSave.data}
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        getToleranties: function (systemId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL+ config.HTTP_GET_TRAJECT_PREFERENCES_URL.format(systemId)
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });
            return deferred.promise;
        },
        getPeriodeSchemas: function (systemId,corridorId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL+ config.HTTP_GET_PERIODESCHEMAS.format(systemId,corridorId)
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });
            return deferred.promise;
        },
        getPeriodeSchemaById: function (systemId,corridorId,scheduleId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL+ config.HTTP_GET_PERIODESCHEMA_BY_ID.format(systemId,corridorId,scheduleId)
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });
            return deferred.promise;
        },
        putPeriodeSchema: function (systemId,corridorId,scheduleId,dataToSave) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_PUT,
                url: config.HTTP_BASE_URL+ config.HTTP_PUT_PERIODESCHEMA.format(systemId,corridorId,scheduleId),
                data: dataToSave
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        postPeriodeSchema: function (systemId,corridorId,dataToSave) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_POST,
                url: config.HTTP_BASE_URL+ config.HTTP_POST_PERIODESCHEMA.format(systemId,corridorId),
                data:dataToSave
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        // Angular by default sends the Content-Type as text/plain for DELETE requests.
        // Set : headers: {"Content-Type": "application/json;charset=utf-8"}
        deletePeriodeSchema: function (systemId,corridorId,scheduleId,dataToDelete) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_DELETE,
                url: config.HTTP_BASE_URL+ config.HTTP_DELETE_PERIODESCHEMA.format(systemId,corridorId,scheduleId),
                headers: {"Content-Type": "application/json;charset=utf-8"},
                data:
                {version:dataToDelete}
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        getCorridor: function (systemId,corridorId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL+ config.HTTP_GET_CORRIDOR.format(systemId,corridorId)
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });
            return deferred.promise;
        },
        getStartGantryAndEndGantryForCorridor: function (systemId,corridorId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL+ config.HTTP_GET_CORRIDOR_GET_START_AND_END_GANTRY.format(systemId,corridorId)
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });
            return deferred.promise;
        },
        getBeschikbarePassagepuntenIn: function (systemId,startSectionId) {
        var deferred = $q.defer();
        $http({
            method: config.HTTP_GET,
            url: config.HTTP_BASE_URL+ config.HTTP_GET_BESCHIKBARE_PASSAGEPUNTEN_IN.format(systemId,startSectionId)
        }).success(function(data) {
            deferred.resolve(data);
        }).error(function(data, status, headers, config) {
            $log.debug(data);
            $log.debug(status);
            $log.debug(headers);
            $log.debug(config);
            deferred.reject(data);
        });
        return deferred.promise;
    },
        getBeschikbarePassagepuntenUit: function (systemId,passagepuntId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL+ config.HTTP_GET_BESCHIKBARE_PASSAGEPUNTEN_UIT.format(systemId,passagepuntId)
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });
            return deferred.promise;
        },
        getSystem: function (systemId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL+ config.HTTP_GET_SYSTEM.format(systemId)
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });
            return deferred.promise;
        },
        putSystem: function (systemId,dataToSave) {
            var deferred = $q.defer();
            var data = {
                id: systemId,
                version: parseInt(dataToSave.system.version,10),
                nrDaysKeepViolations : dataToSave.system.data.retentionTimes.nrDaysKeepViolations,
                nrDaysKeepTrafficData : dataToSave.system.data.retentionTimes.nrDaysKeepTrafficData,
                pardonTimeEsa_secs:dataToSave.system.data.pardonTimes.pardonTimeEsa_secs,
                pardonTimeTechnical_secs:dataToSave.system.data.pardonTimes.pardonTimeTechnical_secs
            };

            $log.debug(data);

            $http({
                method: config.HTTP_PUT,
                url: config.HTTP_BASE_URL+ config.HTTP_PUT_SYSTEM.format(systemId),
                data: data
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        putSystemTechnical: function (systemId,dataToSave) {
            var deferred = $q.defer();
            var data = {
                id: systemId,
                version: parseInt(dataToSave.system.version,10),
                title : dataToSave.system.data.title,
                description : dataToSave.system.data.location.description,
                region:dataToSave.system.data.location.region,
                roadNumber:dataToSave.system.data.location.roadNumber,
                maxSpeed:dataToSave.system.data.maxSpeed,
                compressionFactor:dataToSave.system.data.compressionFactor,
                orderNumber:dataToSave.system.data.orderNumber,
                serialNumber:dataToSave.system.data.serialNumber.serialNumber,
                serialNumberPrefix:dataToSave.system.data.serialNumber.serialNumberPrefix,
                serialNumberLength:dataToSave.system.data.serialNumber.serialNumberLength
            };

            $log.debug(data);

            $http({
                method: config.HTTP_PUT,
                url: config.HTTP_BASE_URL+ config.HTTP_PUT_SYSTEM_TECHNICAL.format(systemId),
                data: data
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        putCorridor: function (systemId,dataToSave) {
            var deferred = $q.defer();

            var data = {
                id: dataToSave.corridorId,
                version: parseInt(dataToSave.version,10),
                name:dataToSave.naam,
                roadType:dataToSave.soortWeg.id,
                isInsideUrbanArea:dataToSave.binnenBebouwdeKom,
                locationCode:dataToSave.codePleegPlaats,
                locationLine1:dataToSave.locatieBegin,
                locationLine2:dataToSave.locatieEind,
                direction:{directionFrom:dataToSave.richtingVan,directionTo:dataToSave.richtingNaar},
                startSectionId:dataToSave.hmpPassagepuntIn.sectionId,
                endSectionId:dataToSave.hmpPassagepuntUit.sectionId,
                dutyType:dataToSave.projectCode.substring(0, 4),
                deploymentCode:dataToSave.projectCode.substring(4, 6),
                radarCode:dataToSave.radarCode,
                codeText:dataToSave.codeTekst,
                roadCode:parseInt(dataToSave.wegCode, 10)
            };

            $http({
                method: config.HTTP_PUT,
                url: config.HTTP_BASE_URL+ config.HTTP_PUT_CORRIDOR.format(systemId,dataToSave.corridorId),
                data: data
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        },
        // verschilt van de gewone put omdat de name, startSectionId en endSectionId niet kunnen worden gewijzigd.
        // hierin staan dan ook de opgehaalde waardes
        putCorridorInstellingen: function (systemId,dataToSave) {
            var deferred = $q.defer();

            var data = {
                id: dataToSave.corridorId,
                version: parseInt(dataToSave.version,10),
                name:dataToSave.naam,
                roadType:dataToSave.soortWeg.id,
                isInsideUrbanArea:dataToSave.binnenBebouwdeKom,
                locationCode:dataToSave.codePleegPlaats,
                locationLine1:dataToSave.locatieBegin,
                locationLine2:dataToSave.locatieEind,
                direction:{directionFrom:dataToSave.richtingVan,directionTo:dataToSave.richtingNaar},
                startSectionId:dataToSave.startSectionId,
                endSectionId:dataToSave.endSectionId,
                dutyType:dataToSave.projectCode.substring(0, 4),
                deploymentCode:dataToSave.projectCode.substring(4, 6),
                radarCode:dataToSave.radarCode,
                codeText:dataToSave.codeTekst,
                roadCode:parseInt(dataToSave.wegCode, 10)
            };
            $http({
                method: config.HTTP_PUT,
                url: config.HTTP_BASE_URL+ config.HTTP_PUT_CORRIDOR.format(systemId,dataToSave.corridorId),
                data: data
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });

            return deferred.promise;
        }
    }
});




