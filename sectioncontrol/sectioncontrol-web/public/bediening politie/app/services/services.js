app.factory("trajectService", function ($http, $q,config,$log) {
    return {
        getTraject1: function () {
            var deferred = $q.defer();

                $http({
                    method: "get",
                    url: config.HTTP_BASE_URL + "systems/all"
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (msg, code) {
                    $log.debug(msg);
                    $log.debug(code);
                    deferred.reject(msg);
                });
            return deferred.promise;
        },
        getIsSystemEditable: function (systemId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_SYSTEMS_IS_EDITABLE.format(systemId)
            }).success(function (data) {
                deferred.resolve(data=="true");
            }).error(function (msg, code) {
                $log.debug(msg);
                $log.debug(code);
                deferred.reject(msg);
            });
            return deferred.promise;
        }
    }
});

app.factory("resourcesService", function ($http, $q,config,$log) {
    return {
        getLinkSystemResources: function () {
            var deferred = $q.defer();

            $http({
                method: "get",
                url: config.HTTP_BASE_URL + config.HTTP_GET_SYSTEM_RESOURCE_LINK
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (msg, code) {
                $log.debug(msg);
                $log.debug(code);
                deferred.reject(msg);
            });
            return deferred.promise;
        }
    }
});

app.factory("appInitService",['$http','$q','trajectService','ApplicationCache','GebruikersService','$log','$state',
    function ($http, $q,trajectService,applicationCache,gebruikersService,$log,$state) {
    return {
        initialize: function () {
            var deferred = $q.defer();

            // Haal de gebruikersinformatie op.
            var promiseGebruiker = gebruikersService.getCurrentUser();
            var gebruiker=null;
            promiseGebruiker.then(function (data) {
                var gebruiker = {naam:data.name,gebruikersNaam:data.username,verbalisantNummer:data.reportingOfficerId,rol:data.role.name,id:data.id};

                $log.debug("GEBRUIKER=" + gebruiker.naam);
                $log.debug("GEBRUIKER=" + gebruiker.rol);

                applicationCache.put('gebruiker', gebruiker);
            }, function (data) {
                $log.debug(data);
                applicationCache.put("error",data);
                $state.go("error");
            });

            var promise=trajectService.getTraject1();
            promise.then(function (data) {

            var traject;
            var tabs=[];
            var index = 0;

            for (var i = 0; i < data.length; i++) {
                traject=({
                    title: data[i].system.data.title,
                    id: index,secties:[],
                    systemId:data[i].system.data.id,
                    gantries:[],
                    corridors:[]});

                $log.debug("TRAJECT");
                $log.debug(traject);

                // Maak het traject beschikbaar voor het menu.
                tabs.push({title: "Traject", id: index,systemId:data[i].system.data.id,coridorId:-1})

                index++;
                // SECTIES
                for (var ii = 0; ii < data[i].sections.length; ii++) {
                    $log.debug("lengte=" + data[i].sections[ii].name + index);

                    traject.secties.push({title: data[i].sections[ii].name, id: index,
                        description: data[i].sections[ii].description,
                        sectionId: data[i].sections[ii].id});
                }
                $log.debug("SECTIES "+ traject.secties);

                // GANTRIES
                for (var ii = 0; ii < data[i].gantries.length; ii++) {
                    traject.gantries.push({id: data[i].gantries[ii].id, name: data[i].gantries[ii].name, hectometer: data[i].gantries[ii].hectometer});
                }
                $log.debug("GANTRIES "+ traject.gantries);

                // CORRIDORS
                for (var ii = 0; ii < data[i].corridors.length; ii++) {
                    traject.corridors.push({id: data[i].corridors[ii].id,
                                            tabId: index,
                                            name: data[i].corridors[ii].name,
                                            allSectionIds: data[i].corridors[ii].allSectionIds
                                            });
                    // Maak de zookeeper corridor beschikbaar voor het menu.
                    tabs.push({title: data[i].corridors[ii].name, id: index,systemId:-1,corridorId:data[i].corridors[ii].id})

                    index++;
                }
                $log.debug("CORRIDORS "+ traject.corridors);

                var cache = applicationCache.get('traject');
                if (!cache) {
                    applicationCache.put('traject', traject);
                }

                cache = applicationCache.get('tabs');
                if (!cache) {
                    applicationCache.put('tabs', tabs);
                }

                break;//alleen het eerste traject is nodig/
            }

            deferred.resolve();
            }, function (reason) {
                $log.debug("in de error terug naar login " + reason);
                deferred.reject(reason);
            });

            return deferred.promise;
        }
    }
}]);
