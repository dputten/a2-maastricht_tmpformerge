/**
 * Created by CSC on 10/21/14.
 */
app.factory("LogboekServiceGet", function ($cookies, $http, $log, $q,config) {
    return {
        getLogboek: function (begin,end,systemId) {
            var deferred = $q.defer();
            $http({
                method: config.HTTP_GET,
                url: config.HTTP_BASE_URL + config.HTTP_GET_SEARCH_LOGS_URL.format(systemId,begin,end)
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(data, status, headers, config) {
                $log.debug(data);
                $log.debug(status);
                $log.debug(headers);
                $log.debug(config);
                deferred.reject(data);
            });
            return deferred.promise;
        }
    }
});
