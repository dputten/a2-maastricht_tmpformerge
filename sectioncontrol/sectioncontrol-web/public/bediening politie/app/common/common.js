/**
 * Created by CSC on 10/30/14.
 */

$('div#parent div:last').each(function() {
    var p = $(this).parent();
    $(this).height(p.height() - ($(this).offset().top - p.offset().top));
});

if (!String.prototype.chunk) {
    String.prototype.chunk = function (n) {
        if (typeof n == 'undefined')
        {n=1;}

        var re = new RegExp('.{1,' + n + '}', 'g');
        return this.match(re);
    };
}

if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}

if (typeof Array.prototype.forEach != 'function') {
    Array.prototype.forEach = function(callback){
        for (var i = 0; i < this.length; i++){
            callback.apply(this, [this[i], i, this]);
        }
    };
}

if(typeof String.prototype.trim !== 'function') {
    String.prototype.trim = function() {
        return this.replace(/^\s+|\s+$/g, '');
    }
}

getDateFromEpochString=function(dateString){
    var date = new Date(dateString);
    return addLeadingZero(date.getFullYear().toString()) +
            addLeadingZero(date.getMonth()+1).toString()+
            addLeadingZero(date.getDate()).toString();
};

getDutchDateTimeFromEpochString=function(dateString){
    var date = new Date(dateString);
    return  addLeadingZero(date.getDate()).toString()+"-"+
            addLeadingZero(date.getMonth()+1).toString()+"-"+
            addLeadingZero(date.getFullYear().toString()) + " " +
            addLeadingZero(date.getHours().toString()) + ":" +
            addLeadingZero(date.getMinutes().toString()) ;
};

getDutchDateFromEpochString=function(dateString){
    var date = new Date(dateString);
    return  addLeadingZero(date.getDate()).toString()+"-"+
        addLeadingZero(date.getMonth()+1).toString()+"-"+
        addLeadingZero(date.getFullYear().toString())  ;
};

getDutchTimeFromEpochString=function(dateString){
    var date = new Date(dateString);
    return  addLeadingZero(date.getHours().toString()) + ":" +
            addLeadingZero(date.getMinutes().toString()) ;
};

getDateFromString=function(dateString){
    var date = new Date(dateString);
    return date.getFullYear().toString()+ addLeadingZero(date.getMonth()+1).toString()+addLeadingZero(date.getDate()).toString() ;
};

getDateFromStringDutch=function(dateString){
    var date = new Date(dateString);
    return addLeadingZero(date.getDate()).toString()+"-" + addLeadingZero(date.getMonth()+1).toString()+"-" + date.getFullYear().toString() ;
};

getTimeFromString = function (dateString) {
    var time = new Date(dateString);
    return addLeadingZero(time.getHours()).toString() + ":" + addLeadingZero(time.getMinutes()).toString();
};

addLeadingZero=function (number){
    if(number <= 9)
        return number = '0'+number;
    return number;
};

guid=function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};

if (!Array.prototype.find) {
    Array.prototype.find = function(predicate) {
        if (this == null) {
            throw new TypeError('Array.prototype.find called on null or undefined');
        }
        if (typeof predicate !== 'function') {
            throw new TypeError('predicate must be a function');
        }
        var list = Object(this);
        var length = list.length >>> 0;
        var thisArg = arguments[1];
        var value;

        for (var i = 0; i < length; i++) {
            value = list[i];
            if (predicate.call(thisArg, value, i, list)) {
                return value;
            }
        }
        return undefined;
    };
}

//http://stackoverflow.com/questions/3629183/why-doesnt-indexof-work-on-an-array-ie8
if (!Array.prototype.indexOf)
{
    Array.prototype.indexOf = function(elt /*, from*/)
    {
        var len = this.length >>> 0;

        var from = Number(arguments[1]) || 0;
        from = (from < 0)
            ? Math.ceil(from)
            : Math.floor(from);
        if (from < 0)
            from += len;

        for (; from < len; from++)
        {
            if (from in this &&
                this[from] === elt)
                return from;
        }
        return -1;
    };
}

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
if (!Array.prototype.filter) {
    Array.prototype.filter = function(fun/*, thisArg*/) {
        'use strict';

        if (this === void 0 || this === null) {
            throw new TypeError();
        }

        var t = Object(this);
        var len = t.length >>> 0;
        if (typeof fun !== 'function') {
            throw new TypeError();
        }

        var res = [];
        var thisArg = arguments.length >= 2 ? arguments[1] : void 0;
        for (var i = 0; i < len; i++) {
            if (i in t) {
                var val = t[i];

                // NOTE: Technically this should Object.defineProperty at
                //       the next index, as push can be affected by
                //       properties on Object.prototype and Array.prototype.
                //       But that method's new, and collisions should be
                //       rare, so use the more-compatible alternative.
                if (fun.call(thisArg, val, i, t)) {
                    res.push(val);
                }
            }
        }

        return res;
    };
}

// Production steps of ECMA-262, Edition 5, 15.4.4.21
// Reference: http://es5.github.io/#x15.4.4.21
if (!Array.prototype.reduce) {
    Array.prototype.reduce = function(callback /*, initialValue*/) {
        'use strict';
        if (this == null) {
            throw new TypeError('Array.prototype.reduce called on null or undefined');
        }
        if (typeof callback !== 'function') {
            throw new TypeError(callback + ' is not a function');
        }
        var t = Object(this), len = t.length >>> 0, k = 0, value;
        if (arguments.length == 2) {
            value = arguments[1];
        } else {
            while (k < len && !(k in t)) {
                k++;
            }
            if (k >= len) {
                throw new TypeError('Reduce of empty array with no initial value');
            }
            value = t[k++];
        }
        for (; k < len; k++) {
            if (k in t) {
                value = callback(value, t[k], k, t);
            }
        }
        return value;
    };
}
