//http://henriquat.re/modularizing-angularjs/modularizing-angular-applications/modularizing-angular-applications.html
//http://blog.brunoscopelliti.com/angularjs-directive-to-test-the-strength-of-a-password

//https://github.com/wcurrie/ng-password-demo
var appDirectives = angular.module("appDirectives", []);

appDirectives.directive('pwCheck', [function () {
        return {
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {
                var firstPassword = '#' + attrs.pwCheck;
                elem.add(firstPassword).on('keyup', function () {
                    scope.$apply(function () {
                        var v = elem.val()===$(firstPassword).val();
                        ctrl.$setValidity('pwmatch', v);
                    });
                });
            }
        }
    }]);

var FLOAT_REGEXP = /^\-?\d+((\.)\d+)?$/;
app.directive('smartFloat', function() {
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function(viewValue) {
                if (FLOAT_REGEXP.test(viewValue)) {
                    ctrl.$setValidity('float', true);
                    return parseFloat(viewValue);
                } else {
                    ctrl.$setValidity('float', false);
                    return undefined;
                }
            });
        }
    };
});

var HMP_REGEXP = /^\-?\d+((\.)\d+)?$/;
app.directive('hectometerPaal', function() {
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function(viewValue) {
                if (HMP_REGEXP.test(viewValue)) {
                    ctrl.$setValidity('hmp', true);
                    return viewValue;
                } else {
                    ctrl.$setValidity('hmp', false);
                    return viewValue;
                }
            });
        }
    };
});

var TIME_REGEXP = /^([0-9]|0[0-9]|1[0-9]|2[0-4]):[0-5][0-9]$/;
app.directive('hourminuteTime', function() {
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function(viewValue) {
                if (TIME_REGEXP.test(viewValue)) {
                    if(viewValue.substring(0,2)=="24")
                    {
                        if( viewValue.substring(3,5)=="00") {
                            ctrl.$setValidity('hourminutetime', true);
                        }
                        else{
                            ctrl.$setValidity('hourminutetime', false);
                        }

                    }
                    else{
                        ctrl.$setValidity('hourminutetime', true);
                    }

                    return viewValue;
                } else {
                    ctrl.$setValidity('hourminutetime', false);
                    return undefined;
                }
            });
        }
    };
});

//http://sandmoose.com/post/94697505437/angular-directive-to-validate-password-complexity
appDirectives.directive('complexPassword', function($log) {
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function(password) {
                var hasLetter= /[A-Z]/.test(password)||/[a-z]/.test(password);
                var hasNumbers = /\d/.test(password);
                var hasNonalphas = /\W/.test(password);
                // adding booleans results in a number;

                $log.debug('complexPassword');
                $log.debug(hasLetter);
                $log.debug(hasNumbers);
                $log.debug(hasNonalphas);

                var characterGroupCount = hasLetter + hasNumbers + hasNonalphas;
                $log.debug(characterGroupCount);

                if ((password.length >= 8) && (characterGroupCount >= 3)) {
                    ctrl.$setValidity('complexity', true);
                    return password;
                }
                else {
                    ctrl.$setValidity('complexity', false);
                    $log.debug("ctrl.$setValidity('complexity', false);");
                    return password;
                }

            });
        }
    }
});

appDirectives.directive('focus',
    function() {
        return {
            link: function(scope, element, attrs) {
                element[0].focus();
            }
        };
    });

app.directive('intmax', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elem, attr, ctrl) {
            var maxValidator = function (value) {
                var max = scope.$eval(attr.intmax) || Infinity;
                if (value > max) {
                    ctrl.$setValidity('intmax', false);
                    return value;
                } else {
                    ctrl.$setValidity('intmax', true);
                    return value;
                }
            };

            ctrl.$parsers.push(maxValidator);
            ctrl.$formatters.push(maxValidator);
        }
    };
});

app.directive('intmin', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elem, attr, ctrl) {
            var minValidator = function (value) {
                var min = scope.$eval(attr.intmin) || 0;
                if (value < min) {
                    ctrl.$setValidity('intmin', false);
                    return value;
                } else {
                    ctrl.$setValidity('intmin', true);
                    return value;
                }
            };

            ctrl.$parsers.push(minValidator);
            ctrl.$formatters.push(minValidator);
        }
    };
});

var INTEGER_REGEXP = /^\d+$/;
app.directive('integer', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elem, attr, ctrl) {
            var intValidator = function (value) {
                if (INTEGER_REGEXP.test(value)) {
                    ctrl.$setValidity('int', true);
                    return parseFloat(value);
                } else {
                    ctrl.$setValidity('int', false);
                    return undefined;
                }
            };

            ctrl.$parsers.push(intValidator);
            ctrl.$formatters.push(intValidator);
        }
    };
});

/*
 This directive allows us to pass a function in on an enter key to do what we want.
 */
appDirectives.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

appDirectives.directive('customDatepicker',function($compile){
    return {
        replace:true,
        templateUrl:'custom-datepicker.html',
        scope: {
            ngModel: '=',
            dateOptions: '='
        },
        link: function($scope, $element, $attrs, $controller){
            var $button = $element.find('button');
            var $input = $element.find('input');
            $button.on('click',function(){
                if($input.is(':focus')){
                    $input.trigger('blur');
                } else {
                    $input.trigger('focus');
                }
            });
        }
    };
});

/**
 * Checklist-model
 * AngularJS directive for list of checkboxes
 */
appDirectives.directive('checklistModel', ['$parse', '$compile', function($parse, $compile) {
        // contains
        function contains(arr, item) {
            if (angular.isArray(arr)) {
                for (var i = 0; i < arr.length; i++) {
                    if (angular.equals(arr[i], item)) {
                        return true;
                    }
                }
            }
            return false;
        }

        // add
        function add(arr, item) {
            arr = angular.isArray(arr) ? arr : [];
            for (var i = 0; i < arr.length; i++) {
                if (angular.equals(arr[i], item)) {
                    return arr;
                }
            }
            arr.push(item);
            return arr;
        }

        // remove
        function remove(arr, item) {
            if (angular.isArray(arr)) {
                for (var i = 0; i < arr.length; i++) {
                    if (angular.equals(arr[i], item)) {
                        arr.splice(i, 1);
                        break;
                    }
                }
            }
            return arr;
        }

        // http://stackoverflow.com/a/19228302/1458162
        function postLinkFn(scope, elem, attrs) {
            // compile with `ng-model` pointing to `checked`
            $compile(elem)(scope);

            // getter / setter for original model
            var getter = $parse(attrs.checklistModel);
            var setter = getter.assign;

            // value added to list
            var value = $parse(attrs.checklistValue)(scope.$parent);

            // watch UI checked change
            scope.$watch('checked', function(newValue, oldValue) {
                if (newValue === oldValue) {
                    return;
                }
                var current = getter(scope.$parent);
                if (newValue === true) {
                    setter(scope.$parent, add(current, value));
                } else {
                    setter(scope.$parent, remove(current, value));
                }
            });

            // watch original model change
            scope.$parent.$watch(attrs.checklistModel, function(newArr, oldArr) {
                scope.checked = contains(newArr, value);
            }, true);
        }

        return {
            restrict: 'A',
            priority: 1000,
            terminal: true,
            scope: true,
            compile: function(tElement, tAttrs) {
                if (tElement[0].tagName !== 'INPUT' || !tElement.attr('type', 'checkbox')) {
                    throw 'checklist-model should be applied to `input[type="checkbox"]`.';
                }

                if (!tAttrs.checklistValue) {
                    throw 'You should provide `checklist-value`.';
                }

                // exclude recursion
                tElement.removeAttr('checklist-model');

                // local scope var storing individual checkbox model
                tElement.attr('ng-model', 'checked');

                return postLinkFn;
            }
        };
    }]);
