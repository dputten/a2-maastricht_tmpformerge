app.controller('statistiekenController', ['$scope', '$stateParams', '$state', 'StatisticsService','ApplicationCache','config','$log',
    '$window','$timeout','$interval',
    function($scope, $stateParams, $state,
             StatisticsService,
             applicationCache,
             config,
             $log,
             $window,
             $timeout,
             $interval
        ) {

        function getCookieValue(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');

            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1);
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }

        var now = new Date();// is nu utc
        var localDate;
        $scope.buttonDisabledDownload = true;
        $scope.downloadAdres = "";
        $scope.cookieKey = "";
        $scope.guid="";
        var timer;

        $scope.cookieChecker = function () {
            timer = $interval(function () {
                var cookie = getCookieValue("id");
                if (cookie != "") {
                    if (getCookieValue("id") == $scope.guid) {
                        document.cookie = "id=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
                        $scope.guid="";
                        $scope.buttonDisabledDownload = false;
                        if(angular.isDefined(timer))
                        {
                            $log.debug("$destroy");
                            $interval.cancel(timer);
                            timer=undefined;
                        }
                    }
                }
        }, 100);
        };


        $scope.download=function(){
            $scope.buttonDisabledDownload=true;
            $scope.cookieChecker();
            $window.location.href = $scope.downloadAdres;
        };

        $scope.$on('$destroy', function(){

            if(angular.isDefined(timer))
            {
                $log.debug("$destroy");
                $interval.cancel(timer);
                timer=undefined;
            }
        });

        localDate=new Date(now.getFullYear(), now.getMonth(), now.getDate(),0,0,0);
        $log.debug(localDate);
        $scope.statistiekenBeginDatum= new Date(now.getFullYear(), now.getMonth(), now.getDate(),0,0,0);
        localDate= new Date(now.getFullYear(), now.getMonth(), now.getDate(),23,59,59);
        $log.debug(localDate);
        $scope.statistiekenEindDatum = new Date(now.getFullYear(), now.getMonth(), now.getDate(),23,59,59);

        $scope.isSearching=false;
        $scope.searchIsInvalid=false;

        // Dit is alleen voor de titel.
        var traject=applicationCache.get('traject');
        $scope.traject=traject;
        var isTraject=($stateParams.title=='0' || $stateParams.title=='' || $stateParams.title==undefined);
        if(isTraject)
        {
            $scope.title="";
            $scope.inTrajectPartial=true;
        }
        else
        {
            $scope.inTrajectPartial=false;
            for (var i = 0; i < traject.corridors.length; i++) {
                if(traject.corridors[i].tabId==$stateParams.title)
                {
                    $scope.title=traject.corridors[i].name;
                    $scope.corridorId=traject.corridors[i].id;
                    $log.debug($scope.corridorId)
                }
            }
        }

        $scope.$watch('statistiekenBeginDatum', function () {
            $scope.noResults=false;
            $scope.validate();
        });

        $scope.$watch('statistiekenEindDatum', function () {
            $scope.noResults=false;
            $scope.validate();
        });

        $scope.maxEndDate=new Date(new Date($scope.statistiekenBeginDatum).setMonth($scope.statistiekenBeginDatum.getMonth()+1));
        $scope.minEndDate=$scope.statistiekenBeginDatum;
        $scope.minBeginDate=now.setMonth(now.getMonth()-2);

        // id wordt gebruikt om de traject of sectie partial te tonen.
        $scope.id = $stateParams.title;

        $scope.currentLocation=function(){return "statistieken";};

        $scope.zoekenStatistieken=function(){
            $scope.isSearching=true;

            var begin=new Date($scope.statistiekenBeginDatum.getFullYear(),$scope.statistiekenBeginDatum.getMonth(),$scope.statistiekenBeginDatum.getDate());
            var end=new Date($scope.statistiekenEindDatum.getFullYear(),$scope.statistiekenEindDatum.getMonth(),$scope.statistiekenEindDatum.getDate());

            var isTraject=($stateParams.title=='0' || $stateParams.title=='' || $stateParams.title==undefined);
            $scope.getStatistieken(StatisticsService,$scope.traject.systemId,begin,end,isTraject,config,applicationCache,$state);

        };

        $scope.adresStatistieken=function(){
            return $scope.downloadAdres;
        };

        $scope.getStatistieken= function (StatisticsService,systemId,begin,end,isTraject,config,applicationCache,$state) {
            $log.debug(getDateFromString(begin));
            $log.debug(getDateFromString(end));
            $scope.errorMsg="";
            $scope.buttonDisabledDownload=true;

            if (isTraject){
                StatisticsService.getStatistics(getDateFromString(begin), getDateFromString(end), systemId)
                    .then(function (data) {

                        if(data.statistiekRegel==undefined || data.statistiekRegel.length==0){
                            $scope.errorMsg="Geen zoekresultaten gevonden voor de ingevoerde datums.";
                            $scope.noResults=true;
                        }

                        var statistiek = $scope.mapToTrajectStatistieken(data);
                        $log.debug(statistiek);
                        $scope.setStatistiekOnScope($scope, statistiek);
                        $scope.isSearching = false;
                        $scope.buttonDisabledDownload=false;
                        $scope.guid=guid();
                        $scope.downloadAdres=config.HTTP_BASE_URL+ config.HTTP_GET_DOWNLOAD_STATISTICS_TRAJECT_URL.format(systemId,getDateFromString(begin),getDateFromString(end),$scope.guid);

                    }, function (data) {
                        $scope.isSearching = false;
                        $scope.buttonDisabledDownload=true;
                        $log.debug(data);
                        applicationCache.put("error",data);
                        $state.go("error");
                    });
            }
            else {

                var reportInfoPromise = StatisticsService.getCorridorReportInfo(systemId,$scope.corridorId);
                reportInfoPromise.then(function (data) {
                    $scope.reportHtId=data.reportHtId;

                    var promise = StatisticsService.getStatisticsSectie(getDateFromString(begin), getDateFromString(end), systemId, $scope.reportHtId);
                    promise.then(function (data) {
                        if(data.statistiekRegel==undefined || data.statistiekRegel.length==0){
                            $scope.errorMsg="Geen zoekresultaten gevonden voor de ingevoerde datums.";
                            $scope.noResults=true;
                        }

                        var statistiek = $scope.mapToTrajectStatistieken(data);
                        $scope.setStatistiekOnScope($scope, statistiek);
                        $scope.isSearching = false;
                        $scope.buttonDisabledDownload = false;
                        $scope.guid=guid();
                        $scope.downloadAdres=config.HTTP_BASE_URL+ config.HTTP_GET_DOWNLOAD_STATISTICS_SECTIE_URL.format(systemId,$scope.reportHtId,getDateFromString(begin),getDateFromString(end),$scope.guid)

                    }, function (data) {
                        $scope.isSearching = false;
                        $scope.buttonDisabledDownload = true;
                        applicationCache.put("error", data);
                        $state.go("error");
                    });
                }, function (data) {
                    $scope.isSearching = false;
                    $scope.buttonDisabledDownload=true;
                    applicationCache.put("error",data);
                    $state.go("error");
                });
            }
        };


        $scope.mapToTrajectStatistieken=function(data){
            var statistieken=[];
            data.statistiekRegel.forEach(function (statistiekRegel)
            {
                $log.debug(statistiekRegel);
                $log.debug(statistiekRegel.gemiddeld);
                statistieken.push({ datum: statistiekRegel.datum,
                    passagesTotaal: statistiekRegel.totaalInPassages,
                    overtredingenTotaal:statistiekRegel.overtredingenTotaal,
                    overtredingenAuto:statistiekRegel.overtredingenAuto,
                    overtredingenHand: statistiekRegel.overtredingenHand,
                    snelheidGemiddeld: statistiekRegel.gemiddeld,
                    snelheidMax: statistiekRegel.max});
            });

            var result={
                totaalInPassages:data.totaalInPassages,
                overtredingenTotaal:data.totaalOvertredingen,
                overtredingenAuto:data.totaalOvertredingenAuto,
                overtredingenHand:data.totaalOvertredingenHand,
                snelheidGemiddeld:data.totaalGemiddeld,
                statistiekRegels:statistieken
            };
            $log.debug(result);
            return result;
        };

        $scope.setStatistiekOnScope=function ($scope,statistiek) {
            $log.debug("statistiek");
            $log.debug(statistiek);
            $scope.statistieken = statistiek.statistiekRegels;
            $scope.totaalInPassages = statistiek.totaalInPassages;
            $scope.overtredingenTotaal = statistiek.overtredingenTotaal;
            $scope.overtredingenAuto = statistiek.overtredingenAuto;
            $scope.overtredingenHand = statistiek.overtredingenHand;
            $scope.snelheidGemiddeld=statistiek.snelheidGemiddeld;

            $log.debug($scope.statistieken);
        };

        $scope.validate=function(){
            $scope.maxEndDate=new Date(new Date($scope.statistiekenBeginDatum).setMonth($scope.statistiekenBeginDatum.getMonth()+1));
            $scope.minEndDate=$scope.statistiekenBeginDatum;
            $scope.errorMsg="";

            var beginDatumJaar=$scope.statistiekenBeginDatum.getFullYear ();
            var beginDatumMaand=$scope.statistiekenBeginDatum.getMonth();
            var eindDatumJaar=$scope.statistiekenEindDatum.getFullYear();
            var eindDatumMaand=$scope.statistiekenEindDatum.getMonth();
            var beginDatumDag=$scope.statistiekenBeginDatum.getDate();
            var eindDatumDag=$scope.statistiekenEindDatum.getDate();
            var huidigeMaand=new Date().getMonth();
            var huidigeJaar=new Date().getFullYear ();
            var huidigeDag=new Date().getDate ();
            var huidigeAantalMaanden=(huidigeMaand+1)+(12*huidigeJaar);
            var beginDatumAantalMaanden=(beginDatumMaand+1)+(12*beginDatumJaar);
            var eindDatumAantalMaanden=(eindDatumMaand+1)+(12*eindDatumJaar);

            if(huidigeAantalMaanden-beginDatumAantalMaanden>2){
                $scope.searchIsInvalid=true;
                if($scope.searchIsInvalid)
                    $scope.errorMsg="Geen geldige datums ingevoerd. De termijn waarin gezocht kan worden is maximaal twee maanden geleden.";
            }
            else if(huidigeAantalMaanden-beginDatumAantalMaanden==2){
                $scope.searchIsInvalid=(beginDatumDag<huidigeDag);
                if($scope.searchIsInvalid){
                    $scope.errorMsg="Geen geldige datums ingevoerd. De termijn waarin gezocht kan worden is maximaal twee maanden geleden.";
                }
                else{
                    if(eindDatumAantalMaanden-beginDatumAantalMaanden>1){
                        $scope.searchIsInvalid=true;
                        if($scope.searchIsInvalid)
                            $scope.errorMsg="Zoekperiode is te lang. De periode voor zoekopdrachten is maximaal één maand.";
                    } else if(eindDatumAantalMaanden-beginDatumAantalMaanden==1){
                        $scope.searchIsInvalid=(eindDatumDag>beginDatumDag);
                        if($scope.searchIsInvalid)
                            $scope.errorMsg="Zoekperiode is te lang. De periode voor zoekopdrachten is maximaal één maand.";
                    }
                    else if(eindDatumAantalMaanden-beginDatumAantalMaanden==0){
                        $scope.searchIsInvalid=(eindDatumDag< beginDatumDag);
                        if($scope.searchIsInvalid)
                            $scope.errorMsg="Het is niet mogelijk een einddatum te selecteren die voor de begindatum ligt.";
                    }
                    else if(eindDatumAantalMaanden-beginDatumAantalMaanden<0){
                        $scope.searchIsInvalid=true;
                        if($scope.searchIsInvalid)
                            $scope.errorMsg="Het is niet mogelijk een einddatum te selecteren die voor de begindatum ligt.";
                    }
                    else
                    {
                        $scope.searchIsInvalid=false;
                    }
                }
            }
            else if(eindDatumAantalMaanden-beginDatumAantalMaanden>1){
                $scope.searchIsInvalid=true;
                if($scope.searchIsInvalid)
                    $scope.errorMsg="Zoekperiode is te lang. De periode voor zoekopdrachten is maximaal één maand.";
            }
            else if(eindDatumAantalMaanden-beginDatumAantalMaanden==1){
                $scope.searchIsInvalid=(eindDatumDag>beginDatumDag);
                if($scope.searchIsInvalid)
                    $scope.errorMsg="Zoekperiode is te lang. De periode voor zoekopdrachten is maximaal één maand.";
            }
            else if(eindDatumAantalMaanden-beginDatumAantalMaanden==0){
                $scope.searchIsInvalid=(eindDatumDag< beginDatumDag);
                if($scope.searchIsInvalid)
                    $scope.errorMsg="Het is niet mogelijk een einddatum te selecteren die voor de begindatum ligt.";
            }
            else if(eindDatumAantalMaanden-beginDatumAantalMaanden<0){
                $scope.searchIsInvalid=true;
                if($scope.searchIsInvalid)
                    $scope.errorMsg="Het is niet mogelijk een einddatum te selecteren die voor de begindatum ligt.";
            }
            else
            {
                $scope.searchIsInvalid=false;
            }

            $scope.$digest;
            if(!$scope.$$phase) {
                $scope.$apply();
            }
        };
    }
]);


