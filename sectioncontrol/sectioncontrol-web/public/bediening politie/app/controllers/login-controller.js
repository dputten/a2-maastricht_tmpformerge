app.controller('login-controller', ['$rootScope', '$cookieStore', '$location', '$scope', '$stateParams', '$state',
    'loginService', 'appInitService', 'ApplicationCache','RoleService','$log',
    function ($rootScope, $cookieStore, $location, $scope, $stateParams, $state, loginService, appInitService, applicationCache,roleService,$log) {
        $log.debug("In logincontroller");
        $scope.trajectTitle="Bediening Politie";

        var promise=loginService.getTitle();
        promise.then(function (data) {
            $scope.trajectTitle= data;
        }, function (reason) {
             $log.error("in de error terug naar login " + reason);
        });


        var isLogginIn=false;
        $scope.loginInfo = {username: "", password: "", loginError: false, loginErrorMessage: ""};

        $scope.mandatoryFieldsFilled = function () {
            return !!($scope.loginInfo.username.trim() != "" && $scope.loginInfo.password.trim() != "");
        };

        $scope.isUserLoggedIn = function () {
            return loginService.isLoggedIn();
        };

        $scope.isInRolePolitie = function () {

            return roleService.isInRolePolitie();
        };

        $scope.login = function () {
            $log.debug("$scope.login");
            if(isLogginIn==false) {
                isLogginIn = true;
                applicationCache.removeAll();

                var promise = loginService.login($scope.loginInfo.username, $scope.loginInfo.password);
                promise.then(function () {

                    // Als de login is gelukt willen de applicatie initialiseren. Dat gebeurt hieronder.
                    var promiseappInitService = appInitService.initialize();
                    promiseappInitService.then(function () {
                        // Pas als de init klaar is kunnen we verder.
                        // Maak de invoer velden alvast leeg. In de logout wilde het niet lukken.
                        $scope.loginInfo.username = "";
                        $scope.loginInfo.password = "";
                        $scope.loginInfo.loginError = "";
                        isLogginIn = false;
                        $state.go("handhaving");

                    }, function (reason) {
                        $log.debug("in de error terug naar login " + reason);
                        $scope.loginInfo.loginErrorMessage = "De combinatie gebruikersnaam en wachtwoord is onjuist of niet bekend. Nadat voor een gebruikersnaam 5 keer een foutief wachtwoord gegeven is, wordt het account geblokkeerd.";
                        $scope.loginInfo.loginError = true;
                        isLogginIn = false;
                        $state.go('login');
                    });

                }, function (reason) {
                    $log.debug("in de error terug naar login " + reason);
                    if(reason.isBlocked) {
                        $scope.loginInfo.loginErrorMessage = "U hebt 5x een verkeerd wachtwoord ingevoerd. Uw gebruikersaccount is geblokkeerd. Neem contact op met de tactisch beheerder.";
                    } else {
                        $scope.loginInfo.loginErrorMessage = "De combinatie gebruikersnaam en wachtwoord is onjuist of niet bekend. Nadat voor een gebruikersnaam 5 keer een foutief wachtwoord gegeven is, wordt het account geblokkeerd.";
                    }
                    $scope.loginInfo.loginError = true;
                    isLogginIn = false;
                    $state.go('login');
                });
            }
        };

        $scope.logout = function () {
            $log.debug("$scope.logout");

            // In beide gevallen uitloggen
            var promise = loginService.logout();
            promise.then(function () {
                loginService.logUserOut();
                    $location.path('/home');
            }, function (reason) {
                $log.debug(reason);
                loginService.logUserOut();
                    $location.path('/home');
            });
        }
    }]);
