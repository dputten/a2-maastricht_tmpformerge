app.controller('instellingenController', ['$scope', '$stateParams', '$state',
    'InstellingenServiceSnelheidLimietenMapper',
    'InstellingenServiceSectieSoortWeg',
    'ApplicationCache','RoleService','InstellingenService','$modal','HandhavingService','$log','isSystemEditable',
    function($scope, $stateParams, $state,
             InstellingenServiceSnelheidLimietenMapper,
             InstellingenServiceSectieSoortWeg,
             applicationCache,
             roleService,
             instellingenService,
             $modal,
             handhavingService,
             $log,isSystemEditable) {

        // CONSTRUCTOR
        var traject=applicationCache.get('traject');
        $scope.currentLocation="instellingen";
        $scope.currentLocation=function(){return "instellingen";};
        $scope.edit=false;
        $scope.editingInProgress=false;
        $scope.addNew=false;
        $scope.getSoortBord=["A1","A3"];

        // id wordt gebruikt om de traject of sectie partial te tonen.
        $scope.id = $stateParams.title;
        $scope.inputDisabledTraject=true;
        $scope.inputDisabledSectie=true;
        $scope.inputDisabledToleranties=true;
        $scope.inputDisabledSnelheidslimieten=true;
        $scope.buttonDisabledTraject=false;
        $scope.buttonDisabledSectie=false;
        $scope.buttonDisabledToleranties=false;
        $scope.buttonDisabledSnelheidslimieten=false;
        $scope.showTrajectErrors=false;
        $scope.showTolerantieErrors=false;
        $scope.showSectieErrors=false;
        $scope.showPeriodeSchemaErrors=false;

        $scope.timePattern = (function() {
            var regexp =/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/;
            return {
                test: function(value) {
                    return regexp.test(value);
                }
            };
        })();

        $scope.snelheden=[{key:30,value:30},{key:50,value:50},{key:60,value:60},{key:70,value:70},{key:80,value:80},{key:90,value:90},{key:100,value:100},{key:110,value:110},{key:120,value:120},{key:130,value:130}];

        // Dit is alleen voor de titel.
        var traject=applicationCache.get('traject');
        $scope.traject=traject;
        var isTraject=($stateParams.title=='0' || $stateParams.title=='' || $stateParams.title==undefined);
        if(isTraject)
        {
            $scope.title="";
        }
        else
        {
            for (var i = 0; i < traject.corridors.length; i++) {
                if(traject.corridors[i].tabId==$stateParams.title)
                {
                    $scope.title=traject.corridors[i].name;
                }
            }
        }

        $scope.filterFn = function(vehicleMaxSpeed)
        {
             return !(vehicleMaxSpeed.vehicleType == 'VehicleCode$BF' || vehicleMaxSpeed.vehicleType == 'VehicleCode$BS' || vehicleMaxSpeed.vehicleType == 'VehicleCode$BB');
        };

        $scope.getSnelheidslimietOmschrijving=function(code){return InstellingenServiceSnelheidLimietenMapper.getSnelheidslimietOmschrijving(code);};

        $scope.isEditDisabledForUser=function(){return roleService.isInRolePolitie()==false || !isSystemEditable ;};

        $scope.isMaxPeriodeschemasBereikt=function() {
            if ($scope.periodeschemas != undefined)
                return $scope.periodeschemas.length == 10;
            else
                return false;
        };

        $scope.fillPeriodeSchemas=function(){
            // ik wil in corrodors zoeken naar de corridor die het sectieid van de huidige sectie bevat in sectionids
            // met het systemid en het corridorid zoeken naar de periodeschema's via instellingenservice.getPeriodeSchemas
            var traject=applicationCache.get('traject');
            var corridorId;

            for (var i = 0; i < traject.corridors.length; i++) {
                if(traject.corridors[i].tabId==$stateParams.title)
                {
                    corridorId=traject.corridors[i].id;
                }
            }

            if(corridorId!=undefined)
            {
                var promise =  instellingenService.getPeriodeSchemas(traject.systemId,corridorId);
                promise.then(function (data) {
                    $log.debug(data);
                    var periodeSchemas=[];
                    for (var i = 0; i < data.data.length; i++) {
                        periodeSchemas.push({
                            index:i,
                            systemId:traject.systemId,
                            corridorId:corridorId,
                            id:data.data[i].id,
                            editMode:false,
                            beginTijd: addLeadingZero(data.data[i].fromPeriodHour)+ ":"+ addLeadingZero(data.data[i].fromPeriodMinute),
                            eindTijd: addLeadingZero(data.data[i].toPeriodHour)+ ":"+ addLeadingZero(data.data[i].toPeriodMinute),
                            snelheidsLimiet:data.data[i].speedLimit,
                            indicatieBord:data.data[i].signIndicator,
                            soortBord: data.data[i].speedIndicatorType==undefined?"": data.data[i].speedIndicatorType.substring(data.data[i].speedIndicatorType.indexOf("$")+1),
                            supportMsgBoard:data.data[i].supportMsgBoard,
                            msgsAllowedBlank:data.data[i].msgsAllowedBlank});
                    }

                    periodeSchemas.sort(function (a, b) {
                        return new Date('1970/01/01 ' + a.beginTijd) - new Date('1970/01/01 ' + b.beginTijd);
                    });

                    $log.debug("periodeSchemas");
                    $log.debug(periodeSchemas);

                    $scope.periodeschemas=periodeSchemas;

                }, function (data) {
                    $log.debug(data);
                    applicationCache.put("error",data);
                    $state.go("error");
                });

            }

            if(!$scope.$$phase) {
                $scope.$apply();
            }
        };

        $scope.getCorridor=function(){
            // Verkrijg het actuele/geselecteerde corridor via het tabs menu
            var corridorId;
            var corridorName;
            for (var i = 0; i < traject.corridors.length; i++) {
                if(traject.corridors[i].tabId==$stateParams.title) {
                    corridorId=traject.corridors[i].id;
                    corridorName=traject.corridors[i].id;
                }
            }

            var promiseCorridor =  instellingenService.getCorridor(
                traject.systemId,
                corridorId);

            promiseCorridor.then(function (data) {
                $scope.corridor = data;
                // Haal de passagepunten in
                // dit zou moeten zijn een koppeling tussen sectie en startgantry

                // Haal de passagepuntenin op.
                var promisePassagepuntenIn = instellingenService.getStartGantryAndEndGantryForCorridor(traject.systemId,corridorId);
                promisePassagepuntenIn.then(function (result) {
                    $scope.passagepuntIn=result.hectometer;

                    $scope.passagepuntIn=result[0].hectometer;
                    $scope.passagepuntUit=result[1].hectometer;

                    $scope.soortenWeg= InstellingenServiceSectieSoortWeg.getSoortenWeg();

                    var sectie={
                        startSectionId:$scope.corridor.data.startSectionId,
                        endSectionId:$scope.corridor.data.endSectionId,
                        corridorId:corridorId,
                        version:$scope.corridor.version,
                        naam:$scope.corridor.data.name,
                        hmpPassagepuntIn:$scope.corridor.data.startGantryId,
                        hmpPassagepuntUit:$scope.corridor.data.endGantryId,
                        locatieBegin:$scope.corridor.data.info.locationLine1,
                        locatieEind:$scope.corridor.data.info.locationLine2,
                        richtingVan:$scope.corridor.data.direction.directionFrom,
                        richtingNaar:$scope.corridor.data.direction.directionTo,
                        codePleegPlaats:pad($scope.corridor.data.info.locationCode.toString(),5),
                        projectCode:$scope.corridor.data.dutyType+$scope.corridor.data.deploymentCode,
                        radarCode:parseInt($scope.corridor.data.radarCode, 10),
                        codeTekst:$scope.corridor.data.codeText,
                        wegCode:pad($scope.corridor.data.roadCode.toString(),4),
                        binnenBebouwdeKom:$scope.corridor.data.isInsideUrbanArea,
                        soortWeg:$scope.soortenWeg.find(function(e){ return e.id == $scope.corridor.data.roadType; })};

                    var tabs=applicationCache.get('tabs');
                    var result= tabs.find(function(e){ return e.corridorId == corridorId; });
                    if(result!=undefined) {
                        if(result.title!=sectie.naam) {
                            result.title=sectie.naam;
                            applicationCache.remove('tabs');
                            applicationCache.put('tabs',tabs)
                        }
                    }
                    $scope.sectie=sectie;
                    $scope.sectieAsLoaded= angular.copy(sectie);

                }, function (reason) {
                    $log.debug(reason );
                });
            }, function (data) {
                $log.debug(data);
                applicationCache.put("error",data);
                $state.go("error");
            });
        };

        // Als traject partial
        // $stateParams.title is het id wat ik zelf heb toegekend om te kunnen mappen vanuit de route naar het traject of de sectie
        // TRAJECT
        if($stateParams.title==0 || $stateParams.title=="" || $stateParams.title==undefined) {

            $scope.instellingenServiceTraject = {technischePardontijd: 0, dataBewaarPeriode: 0, esaPardonTijd: 0};

            // Todo:JE-> hier kan een $q.all
            getPreferences(instellingenService, $scope, traject.systemId,applicationCache,$state);

            var promise =instellingenService.getSystem(traject.systemId);
            promise.then(function (data) {
                $scope.system= data;
                //$scope.state= data.state.state;
                $scope.esaProviderType= $scope.system.system.data.esaProviderType;
                $scope.systemAsLoaded= angular.copy(data);

                $log.debug($scope.system);
            }, function (data) {
                $log.debug(data);
                applicationCache.put("error",data);
                $state.go("error");
            });
        }
        // SECTIE / CORRIDOR
        else
        {
            instellingenService.getSystem(traject.systemId)
                .then(function (data) {
                    //$scope.state= data.state.state;
                    $scope.esaProviderType= data.system.data.esaProviderType;
                }, function (data) {
                    $log.debug(data);
                    applicationCache.put("error",data);
                    $state.go("error");
                });

            $scope.fillPeriodeSchemas();

            $scope.getCorridor();

        }



        $scope.setEdit=function(edit){
            $log.debug('setEdit');
            $scope.edit=edit;
        };

        $scope.test=function(instellingen_traject_partial){
            $log.debug("inputDisabledTraject="+$scope.inputDisabledTraject)
            if($scope.inputDisabledTraject==true)
            {
                return false;
            }

            if($scope.inputDisabledTraject==false){
                return instellingen_traject_partial.$invalid;
            }
        };

        /**
         * SECTIE
         */
        $scope.wijzigenSectie = function () {
            $scope.showSectieErrors=true;
            $scope.inputDisabledSectie=false;
        };

        $scope.bewaarSectie = function () {
            $log.debug($scope.sectie.hmpPassagepuntIn);

            $scope.inputDisabledSectie=true;
            $scope.buttonDisableSectie=true;
            if(!$scope.$$phase) {
                $scope.$apply();
            }

            var traject=applicationCache.get('traject');

            $log.debug("SECTIE");
            $log.debug($scope.sectie);
            var promise =  instellingenService.putCorridorInstellingen(traject.systemId,$scope.sectie );
            promise.then(function () {
                $scope.getCorridor();

                var cache = applicationCache.get('traject');
                if (cache) {
                    for (var i = 0; i < traject.corridors.length; i++) {
                        if(traject.corridors[i].tabId==$stateParams.title)
                        {
                            $scope.corridorId=traject.corridors[i].id;
                        }
                    }
                    applicationCache.put('traject', traject);
                }

                $scope.showSectieErrors=false;
                $scope.inputDisabledSectie=true;
                $scope.buttonDisableSectie=false;

                if(!$scope.$$phase) {
                    $scope.$apply();
                }

            }, function (data) {
                $scope.inputDisabledSectie=false;
                $scope.buttonDisableSectie=false;
                if(!$scope.$$phase) {
                    $scope.$apply();
                }
                applicationCache.put("error",data);
                $state.go("error");
            });
        };

        $scope.annulerenSectie=function(){
            $scope.inputDisabledSectie=true;
            $scope.buttonDisabledSectie=false;
            $scope.showSectieErrors=false;
            angular.copy($scope.sectieAsLoaded, $scope.sectie);

            if(!$scope.$$phase) {
                $scope.$apply();
            }
        };

        $scope.updateModel=function(index){
            $log.debug("updateModel");
            $log.debug($scope.periodeschemas[index].indicatieBord);
            if(!$scope.$$phase) {
                $scope.$apply();
            }
        };

        $scope.getinputDisabledSectie=function(){return $scope.inputDisabledSectie;};

        $scope.annuleerPeriodeschema=function(index){

            if($scope.addNew)
            {
                $scope.periodeschemas.splice(index,1);
            }
            else
            {
                angular.copy($scope.periodeschemaAsLoaded, $scope.periodeschemas[index]);
                $scope.periodeschemas[index].editMode=false;
            }

            $scope.editingInProgress=false;
            $scope.errorPeriodeSchema=false;
            $scope.errorMessagePeriodeSchema="";
            $scope.addNew=false;
            $scope.showPeriodeSchemaErrors=false;

            if(!$scope.$$phase) {
                $scope.$apply();
            }
        };

        $scope.addPeriodeSchema=function(index){

            var traject=applicationCache.get('traject');
            var corridorId;

            for (var i = 0; i < traject.corridors.length; i++) {
                if(traject.corridors[i].tabId==$stateParams.title)
                {
                    corridorId=traject.corridors[i].id;
                }
            }

            var beginTijd=$scope.periodeschemas[index].beginTijd.split(":");
            var eindTijd=$scope.periodeschemas[index].eindTijd.split(":");

            $log.debug("$scope.periodeschemas[index].indicatieBord$scope.periodeschemas[index].indicatieBord$scope.periodeschemas[index].indicatieBord$scope.periodeschemas[index].indicatieBord");
            $log.debug($scope.periodeschemas[index].indicatieBord);

            if($scope.periodeschemas[index].indicatieBord==undefined || $scope.periodeschemas[index].indicatieBord=="")
                $scope.periodeschemas[index].indicatieBord=false;
            if($scope.periodeschemas[index].supportMsgBoard==undefined || $scope.periodeschemas[index].supportMsgBoard=="")
                $scope.periodeschemas[index].supportMsgBoard=false;

            var theSpeedLimit = parseInt($scope.periodeschemas[index].snelheidsLimiet, 10);
            var periodeschemaToSave=
            {
                id:null,
                version : "",
                newSystemId :  "",
                newCorridorId : "",
                fromPeriodHour : parseInt(beginTijd[0], 10),
                fromPeriodMinute : parseInt(beginTijd[1], 10),
                toPeriodHour : parseInt(eindTijd[0], 10),
                toPeriodMinute : parseInt(eindTijd[1], 10),
                speedLimit : theSpeedLimit,
                signSpeed : theSpeedLimit,
                signIndicator : $scope.periodeschemas[index].indicatieBord,
                codeText : "",
                indicationRoadworks: "None",
                indicationDanger: "None",
                indicationActualWork: "None",
                invertDrivingDirection: "None",
                dutyType : "",
                deploymentCode : "01",
                speedIndicatorType:$scope.periodeschemas[index].soortBord==""?"None":"SpeedIndicatorType$" + $scope.periodeschemas[index].soortBord,
                supportMsgBoard:$scope.periodeschemas[index].supportMsgBoard,
                msgsAllowedBlank:0// bij toevoegen op 0 zetten
            };

            var putPromise =  instellingenService.postPeriodeSchema(
                traject.systemId,
                corridorId,
                periodeschemaToSave
            );
            putPromise.then(function (data) {
                if(data.success) {
                    $scope.periodeschemas[index].editMode = false;
                    $scope.editingInProgress = false;
                    $scope.errorPeriodeSchema = false;
                    $scope.errorMessagePeriodeSchema = "";
                    $scope.addNew = false;

                    $scope.fillPeriodeSchemas();

                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                }
                else if(!data.success && data.typeName=="Validate")
                {
                    $scope.errorPeriodeSchema=true;
                    $scope.errorMessagePeriodeSchema=data;
                    if(!$scope.$$phase) {
                        $scope.$apply();
                    }
                }
                else
                {
                    applicationCache.put("error",data);
                    $state.go("error");
                }

            }, function (data) {
                applicationCache.put("error",data);
                $state.go("error");
            });
        };

        $scope.deletePeriodeschema= function(index) {
            // Haal het specifieke periodeschema op.
            var promise =  instellingenService.getPeriodeSchemaById(
                $scope.periodeschemas[index].systemId,
                $scope.periodeschemas[index].corridorId,
                $scope.periodeschemas[index].id);

            promise.then(function (data) {
                var version=data.version;

                var deletePromise =  instellingenService.deletePeriodeSchema(
                    $scope.periodeschemas[index].systemId,
                    $scope.periodeschemas[index].corridorId,
                    $scope.periodeschemas[index].id,
                    version
                );
                deletePromise.then(function () {

                    $scope.periodeschemas[index].editMode=false;
                    $scope.editingInProgress=false;
                    $scope.errorPeriodeSchema=false;
                    $scope.errorMessagePeriodeSchema="";

                    $scope.fillPeriodeSchemas();

                    if(!$scope.$$phase) {
                        $scope.$apply();
                    }

                }, function (data) {
                    applicationCache.put("error",data);
                    $state.go("error");
                });

                $log.debug(data);
            }, function (data) {
                $log.debug(data);
                applicationCache.put("error",data);
                $state.go("error");
            });
        };

        $scope.savePeriodeschema = function(index) {
            if($scope.addNew){
                $scope.addPeriodeSchema(index);
            }
            if(!$scope.addNew)
            {
                // Haal het specifieke periodeschema op.
                var promise =  instellingenService.getPeriodeSchemaById(
                    $scope.periodeschemas[index].systemId,
                    $scope.periodeschemas[index].corridorId,
                    $scope.periodeschemas[index].id);


                promise.then(function (data) {
                    var beginTijd=$scope.periodeschemas[index].beginTijd.split(":");
                    var eindTijd=$scope.periodeschemas[index].eindTijd.split(":");
                    var periodeschema=data;// dit is degene die we binnen kregen.
                    var _signSpeed=-1
                    if($scope.periodeschemas[index].indicatieBord)
                    {
                        _signSpeed=parseInt($scope.periodeschemas[index].snelheidsLimiet, 10);
                    }

                    var periodeschemaToSave=
                    {
                        id:null,// gaat mee in querystring
                        version : periodeschema.version,
                        newSystemId :  $scope.periodeschemas[index].systemId,
                        newCorridorId : $scope.periodeschemas[index].corridorId,
                        fromPeriodHour : parseInt(beginTijd[0], 10),
                        fromPeriodMinute : parseInt(beginTijd[1], 10),
                        toPeriodHour : parseInt(eindTijd[0], 10),
                        toPeriodMinute : parseInt(eindTijd[1], 10),
                        speedLimit : parseInt($scope.periodeschemas[index].snelheidsLimiet, 10),
                        signSpeed : _signSpeed,
                        //signSpeed : parseInt(periodeschema.data.signSpeed, 10),
                        signIndicator : $scope.periodeschemas[index].indicatieBord,
                        codeText : periodeschema.data.codeText,
                        indicationRoadworks: periodeschema.data.indicationRoadworks,
                        indicationDanger: periodeschema.data.indicationDanger,
                        indicationActualWork: periodeschema.data.indicationActualWork,
                        invertDrivingDirection: periodeschema.data.invertDrivingDirection,
                        dutyType : periodeschema.data.dutyType,
                        deploymentCode : periodeschema.data.deploymentCode,
                        speedIndicatorType:$scope.periodeschemas[index].soortBord==""?"None":"SpeedIndicatorType$" + $scope.periodeschemas[index].soortBord,
                        supportMsgBoard:$scope.periodeschemas[index].supportMsgBoard,
                        msgsAllowedBlank:$scope.periodeschemas[index].msgsAllowedBlank// bij edit met rust laten
                    };

                    var putPromise =  instellingenService.putPeriodeSchema(
                        $scope.periodeschemas[index].systemId,
                        $scope.periodeschemas[index].corridorId,
                        $scope.periodeschemas[index].id,
                        periodeschemaToSave
                    );

                    putPromise.then(function (data) {

                        if(data.success) {
                            $scope.periodeschemas[index].editMode = false;
                            $scope.editingInProgress = false;
                            $scope.errorPeriodeSchema = false;
                            $scope.errorMessagePeriodeSchema = "";
                            $scope.addNew = false;
                            $scope.showPeriodeSchemaErrors=false;

                            $scope.fillPeriodeSchemas();

                            if (!$scope.$$phase) {
                                $scope.$apply();
                            }
                        }
                        else if(!data.success && data.typeName=="Validate")
                        {
                            $scope.errorPeriodeSchema=true;
                            $scope.errorMessagePeriodeSchema=data;
                            if(!$scope.$$phase) {
                                $scope.$apply();
                            }
                        }
                        else
                        {
                            applicationCache.put("error",data);
                            $state.go("error");
                        }


                    }, function (data) {
                        applicationCache.put("error",data);
                        $state.go("error");
                    });

                }, function (data) {
                    $log.debug(data);
                    applicationCache.put("error",data);
                    $state.go("error");
                });

                if ($scope.editing !== false) {
                    $log.debug(index);
                    $log.debug($scope.periodeschemas[index]);
                }
            }};

        $scope.editPeriodeschema = function(index) {
            $scope.editingInProgress=true;
            $scope.editMode=true;
            $scope.showPeriodeSchemaErrors=true;
            for (i = 0; i < $scope.periodeschemas.length; i++) {
                // disable alle edits behalve degene waar we mee bezig zijn
                if(i==index)
                {
                    $log.debug($scope.periodeschemas[i].id + " editMode is true");
                    $scope.periodeschemas[i].editMode=true;
                    $scope.periodeschemaAsLoaded= angular.copy($scope.periodeschemas[i]);
                }

                else{
                    $log.debug($scope.periodeschemas[i].id + " editMode is false");
                    $scope.periodeschemas[i].editMode=false;
                }

            }

            if(!$scope.$$phase) {
                $scope.$apply();
            }
        };

        $scope.editMode=false;
        $scope.toevoegenPeriode = function() {
            $scope.addNew=true;
            $scope.showPeriodeSchemaErrors=true;
            $scope.editingInProgress=true;

            for (var i = 0; i < $scope.periodeschemas.length; i++) {
                // disable all edits
                $scope.periodeschemas[i].editMode=false;
            }

            $scope.periodeschemas.push({editMode:true, beginTijd:"",eindTijd:"",snelheidsLimiet:"",indicatieBord:"",soortBord:"",supportMsgBoard:""})

        };

        /**
         * TRAJECT
         */

        $scope.annulerenTraject=function(){
            $scope.inputDisabledTraject=true;
            $scope.buttonDisabledTraject=false;
            $scope.showTrajectErrors=false;
            angular.copy($scope.systemAsLoaded, $scope.system);
            if(!$scope.$$phase) {
                $scope.$apply();
            }
        };

        $scope.annulerenToleranties=function(){
            $scope.inputDisabledToleranties=true;
            $scope.buttonDisabledToleranties=false;
            $scope.showTolerantieErrors=false;
            angular.copy($scope.itemsAsLoaded, $scope.items);
            if(!$scope.$$phase) {
                $scope.$apply();
            }
        };

        $scope.annulerenSnelheidslimieten=function(){
            $scope.inputDisabledSnelheidslimieten=true;
            $scope.buttonDisabledSnelheidslimieten=false;
            angular.copy($scope.itemsAsLoaded, $scope.items);
            if(!$scope.$$phase) {
                $scope.$apply();
            }
        };

        $scope.bewaarTraject = function (instellingen_traject_partial) {
// Ff laten staan als vb voor controller validatie
//            if(instellingen_traject_partial.$valid)
//            {
//                $scope.inputDisabledTraject=true;
//            }
            $scope.inputDisabledTraject=true;
            $scope.buttonDisabledTraject=true;

            if(!$scope.$$phase) {
                $scope.$apply();
            }

            var promise =  instellingenService.putSystem(traject.systemId,$scope.system);
            promise.then(function () {

                var promiseGet =instellingenService.getSystem(traject.systemId);
                promiseGet.then(function (data) {
                    $scope.systemAsLoaded= angular.copy(data);
                    $scope.system= data;

                }, function (data) {
                    $log.debug(data);
                    applicationCache.put("error",data);
                    $state.go("error");
                });

                $scope.showTrajectErrors=false;
                $scope.inputDisabledTraject=true;
                $scope.buttonDisabledTraject=false;
                if(!$scope.$$phase) {
                    $scope.$apply();
                }

            }, function (data) {
                $log.debug(data);
                $scope.inputDisabledTraject=false;
                $scope.buttonDisabledTraject=false;

                applicationCache.put("error",data);
                $state.go("error");

                if(!$scope.$$phase) {
                    $scope.$apply();
                }
            });
        };

        $scope.wijzigenTraject = function () {
            $scope.inputDisabledTraject=false;
            $scope.showTrajectErrors=true;};

        $scope.wijzigenSnelheidslimieten = function () {$scope.inputDisabledSnelheidslimieten=false;};

        $scope.getinputDisabledTraject=function(){return $scope.inputDisabledTraject;};

        $scope.getinputDisabledToleranties=function(){return $scope.inputDisabledToleranties;};

        $scope.getinputDisabledSnelheidslimieten=function(){return $scope.inputDisabledSnelheidslimieten;};

        $scope.wijzigenToleranties = function () {
            $scope.showTolerantieErrors=true;
            $scope.inputDisabledToleranties=false;};

        $scope.bewaarToleranties= function () {
            {
                var traject=applicationCache.get('traject');
                $scope.inputDisabledToleranties=true;
                $scope.buttonDisabledToleranties=true;

                if(!$scope.$$phase) {
                    $scope.$apply();
                }

                instellingenService.postToleranties(traject.systemId,$scope.items)
                    .then(function () {

                        instellingenService.getToleranties(traject.systemId)
                            .then(function (data) {
                                $scope.items = data;
                                $scope.itemsAsLoaded= angular.copy(data);
                            }, function (data) {
                                $log.debug(data);
                                applicationCache.put("error",data);
                                $state.go("error");
                            });
                        $scope.showTolerantieErrors=false;
                        $scope.inputDisabledToleranties=true;
                        $scope.buttonDisabledToleranties=false;
                        if(!$scope.$$phase) {
                            $scope.$apply();
                        }

                    }, function (reason) {
                        $log.debug(reason);
                        $scope.inputDisabledToleranties=false;
                        $scope.buttonDisabledToleranties=false;
                        if(!$scope.$$phase) {
                            $scope.$apply();
                        }
                    });
            }
        };

        $scope.bewaarSnelheidslimieten= function () {
            {
                var traject=applicationCache.get('traject');
                $scope.inputDisabledSnelheidslimieten=true;
                $scope.buttonDisabledSnelheidslimieten=true;
                if(!$scope.$$phase) {
                    $scope.$apply();
                }

                instellingenService.postToleranties(traject.systemId,$scope.items)
                    .then(function () {

                        instellingenService.getToleranties(traject.systemId)
                            .then(function (data) {
                                $scope.items = data;
                                $scope.itemsAsLoaded= angular.copy(data);
                            }, function (data) {
                                $log.debug(data);
                                applicationCache.put("error",data);
                                $state.go("error");
                            });

                        $scope.inputDisabledSnelheidslimieten=true;
                        $scope.buttonDisabledSnelheidslimieten=false;
                        if(!$scope.$$phase) {
                            $scope.$apply();
                        }

                    }, function (data) {
                        $log.debug(data);
                        $scope.inputDisabledSnelheidslimieten=false;
                        $scope.buttonDisabledSnelheidslimieten=false;
                        if(!$scope.$$phase) {
                            $scope.$apply();
                        }

                        $log.debug(data);
                        applicationCache.put("error",data);
                        $state.go("error");
                    });
            }
        };
    }
]);

function getPreferences(instellingenService,$scope,systemId,applicationCache,$state)
{
    var promise = instellingenService.getToleranties(systemId);
    promise.then(function (data) {
        $scope.items = data;
        $scope.itemsAsLoaded= angular.copy(data);
    }, function (data) {
        $log.debug("error",data);
        applicationCache.put("error",data);
        $state.go("error");
    });
}
