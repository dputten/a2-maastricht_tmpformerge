/**
 * Created by CSC on 10/3/14.
 */
app.controller('handhaafController', ['$scope', '$stateParams', '$state','$timeout',
    'HandhavingService','ApplicationCache','RoleService','$modal','InstellingenService','$log',
    function($scope,
             $stateParams,
             $state,
             $timeout,
             HandhavingService,
             applicationCache,
             roleService,
             $modal,
             instellingenService,
             $log) {
        $log.debug("In handhaafController");
        $log.debug(location.host);

        $scope.isInRolePolitie=roleService.isInRolePolitie();
        $scope.isInRoleTechnischBeheer=roleService.isInRoleTechnischBeheer();

        var traject=applicationCache.get('traject');

        var promise;
        $scope.data = [];

        $scope.$on('$destroy', function(){
            $log.debug("$destroy");
            $timeout.cancel(promise);
        });

        $scope.$on('$locationChangeStart', function(){
            $log.debug("$locationChangeStart");
            $timeout.cancel(promise);});

        (function tick() {
            // ALs we in het handhaven scherm zitten, anders niet.
            if($state.current.name=="handhaving") {
                var promiseGet =HandhavingService.getSystemState(traject.systemId);
                promiseGet.then(function (data) {
                    $scope.system= data;

                    var trajecten=[];
                    trajecten.push( {
                        id: traject.systemId,
                        sectie:data.drivingDirection,
                        rijRichting:data.drivingDirection,
                        state:data.state,
                        isCalibrating:data.isCalibrating
                    });

                    $scope.trajecten=trajecten;

                    var secties=[];
                    for(var i=0;i<data.corridorStates.length;i++){
                        (function() {
                            secties.push({
                                id: data.corridorStates[i].corridorId,
                                sectie: data.corridorStates[i].name,
                                rijRichting:  data.corridorStates[i].drivingDirection,
                                state: data.corridorStates[i].state,
                                systemState:data.corridorStates[i].systemState,
                                version:data.corridorStates[i].version
                            });
                        })() ;
                    }

                    $scope.secties=secties;
                    promise =$timeout(tick, 3000);

                }, function (data) {
                    $log.debug(data);
                    applicationCache.put("error",data);
                    $state.go("error");
                });
            }
        })();

        $scope.getbackgroundColor=function(state){
            return $scope.mapStateToColor(state);
        };

        $scope.naarHandhaven=function(id){
            var promise=null;
            if(id=='system')
            {
                $scope.secties.forEach(function(e){
                    if(e.systemState!="EnforceOn") {
                    promise = HandhavingService.veranderCorridorStatus(traject.systemId,e.systemState,"EnforceOn",e.version, e.id);
                        promise.then(function (data) {
                            $log.debug(data);
                        }, function (data) {
                            applicationCache.put("error", data);
                            $state.go("error");
                        });
                    }
                });

                $scope.openModal();
            }
            else
            {
                var corridor=$scope.secties.find(function(e){return e.id==id});
                promise = HandhavingService.veranderCorridorStatus(traject.systemId,corridor.systemState,"EnforceOn",corridor.version,id);
                promise.then(function (data) {
                    $log.debug(data);
                    $scope.openModal();
                }, function (data) {
                    applicationCache.put("error",data);
                    $state.go("error");
                });
            }
        };

        $scope.naarStandby=function(id){
            var promise=null;
            if(id=='system')
            {
                $scope.secties.forEach(function(e){
                    if(e.systemState!="StandBy") {
                        promise = HandhavingService.veranderCorridorStatus(traject.systemId, e.systemState, "StandBy", e.version, e.id);
                        promise.then(function (data) {
                            $log.debug(data);
                        }, function (data) {
                            applicationCache.put("error", data);
                            $state.go("error");
                        });
                    }
                });

                $scope.openModal();
            }
            else
            {
                var corridor=$scope.secties.find(function(e){return e.id==id});
                promise = HandhavingService.veranderCorridorStatus(traject.systemId,corridor.systemState,"StandBy",corridor.version,id);
                promise.then(function (data) {
                    $log.debug(data);
                    $scope.openModal();
                }, function (data) {
                    applicationCache.put("error",data);
                    $state.go("error");
                });
            }


        };

        $scope.naarOnderhoud=function(id){
            var promise=null;
            if(id=='system')
            {
                $scope.secties.forEach(function(e){
                    if(e.systemState!="Maintenance") {
                        promise = HandhavingService.veranderCorridorStatusBeheer(traject.systemId,e.systemState,"Maintenance",e.version, e.id);
                        promise.then(function (data) {
                            $log.debug(data);
                        }, function (data) {
                            applicationCache.put("error",data);
                            $state.go("error");
                        });
                    }
                });

                $scope.openModal();
            }
            else
            {
                var corridor=$scope.secties.find(function(e){return e.id==id});
                promise = HandhavingService.veranderCorridorStatusBeheer(traject.systemId,corridor.systemState,"Maintenance",corridor.version,id);
                promise.then(function (data) {
                    $log.debug(data);
                    $scope.openModal();
                }, function (data) {
                    applicationCache.put("error",data);
                    $state.go("error");
                });
            }


        };

        $scope.naarStandbyFromOff=function(id){
            var promise=null;
            if(id=='system'){
                $scope.secties.forEach(function(e){
                    if(e.systemState!="StandBy") {
                        promise = HandhavingService.veranderCorridorStatusBeheer(traject.systemId, e.systemState, "StandBy", e.version, e.id);
                        promise.then(function (data) {
                            $log.debug(data);
                        }, function (data) {
                            applicationCache.put("error", data);
                            $state.go("error");
                        });
                    }
                });

                $scope.openModal();
            }
            else
            {
                var corridor=$scope.secties.find(function(e){return e.id==id});
                promise = HandhavingService.veranderCorridorStatusBeheer(traject.systemId,corridor.systemState,"StandBy",corridor.version,id);
                promise.then(function (data) {
                    $log.debug(data);
                    $scope.openModal();
                }, function (data) {
                    applicationCache.put("error",data);
                    $state.go("error");
                });
            }
        };

        $scope.naarOff=function(id){
            var promise=null;
            if(id=='system')
            {
                $scope.secties.forEach(function(e){
                    if(e.systemState!="Off") {
                        promise = HandhavingService.veranderCorridorStatusBeheer(traject.systemId,e.systemState,"Off",e.version, e.id);
                        promise.then(function (data) {
                            $log.debug(data);
                        }, function (data) {
                            applicationCache.put("error",data);
                            $state.go("error");
                        });
                    }
                });

                $scope.openModal();
            }
            else
            {
                var corridor=$scope.secties.find(function(e){return e.id==id});
                promise = HandhavingService.veranderCorridorStatusBeheer(traject.systemId,corridor.systemState,"Off",corridor.version,id);
                promise.then(function (data) {
                    $log.debug(data);
                    $scope.openModal();
                }, function (data) {
                    applicationCache.put("error",data);
                    $state.go("error");
                });
            }
        };

//        $scope.getMaintenanceDisabled=function(id){
//            if(roleService.isInRoleTechnischBeheer()) {
//
//                if(id=='system') {
//                    if ($scope.system.state == "Stand-by" && $scope.system.isCalibrating == false )
//                        return false;
//                }
//                else
//                {
//                    var section = $scope.getCorridor(id);
//                    if (section!=undefined && section.state == "Stand-by" && $scope.system.isCalibrating == false)
//                        return false;
//                }
//            }
//            return true
//        };

        $scope.getHandhavenDisabled=function(id){
            if(roleService.isInRolePolitie()) {
                if(id=='system') {
                    if (($scope.system.state == "Stand-by" || $scope.system.state == "Handhaven aan") && $scope.system.isCalibrating == false)
                    {
                        // Als alle corridors in handhaven staan dan ook ook disable
                        var nrOffHandhaven = $scope.secties.reduce(function(n, section) {
                            return n + (section.state== 'Handhaven aan');
                        }, 0);

                        var nrOffHandhavenAndStandby = $scope.secties.reduce(function(n, section) {
                            return n + (section.state== 'Handhaven aan' || section.state== 'Stand-by');
                        }, 0);

                        // Alle in handhaven: knop uit
                        if(nrOffHandhaven==$scope.secties.length)
                            return true;

                        // Alle in handhaven of standby: knop aan. Wordt alleen uitgevoerd als hierboven niet waar
                        if(nrOffHandhavenAndStandby==$scope.secties.length)
                            return false;

                        // Alle andere gevallen: knop uit.
                        return true;
                    }
                }
                else
                {
                    var section = $scope.getCorridor(id);
                    if (section!=undefined && section.state == "Stand-by" && $scope.system.isCalibrating == false)
                        return false;
                }
            }

            return true
        };

        $scope.getStandbyDisabled=function(id){
            if(roleService.isInRolePolitie()) {
                if(id=='system') {
                    if (($scope.system.state == "Stand-by" || $scope.system.state == "Handhaven aan") && $scope.system.isCalibrating == false)
                    {
                        // Als alle corridors in handhaven staan dan ook ook disable
                        var nrOffHandhaven = $scope.secties.reduce(function(n, section) {
                            return n + (section.state== 'Stand-by');
                        }, 0);

                        var nrOffHandhavenAndStandby = $scope.secties.reduce(function(n, section) {
                            return n + (section.state== 'Handhaven aan' || section.state== 'Stand-by');
                        }, 0);

                        // Alle in handhaven: knop uit
                        if(nrOffHandhaven==$scope.secties.length)
                            return true;

                        // Alle in handhaven of standby: knop aan. Wordt alleen uitgevoerd als hierboven niet waar
                        if(nrOffHandhavenAndStandby==$scope.secties.length)
                            return false;

                        // Alle andere gevallen: knop uit.
                        return true;
                    }
                }
                else
                {
                    var section = $scope.getCorridor(id);
                    if(section.state== "Handhaven aan" && $scope.system.isCalibrating==false)
                        return false;
                }
            }

            if(roleService.isInRoleTechnischBeheer()) {
                if(id=='system') {
                    if ($scope.system.state == "Uit" && $scope.system.isCalibrating == false)
                        return false;

                    if ($scope.system.state == "Onderhoud" && $scope.system.isCalibrating == false)
                        return false;
                }
                else
                {
                    var section = $scope.getCorridor(id);
                    if(section.state== "Uit" && $scope.system.isCalibrating==false)
                        return false;
                    if(section.state== "Onderhoud" && $scope.system.isCalibrating==false)
                        return false;
                }
            }

            return true
        };

        $scope.getOnderhoudDisabled=function(id){
            if(roleService.isInRoleTechnischBeheer()) {
                var nrStandby = $scope.secties.reduce(function(n, section) {
                    return n + (section.state== 'Stand-by');
                }, 0);

                if(id=='system') {
                    if ($scope.system.state == "Stand-by" && $scope.system.isCalibrating == false && nrStandby==$scope.secties.length)
                        return false;
                }
                else
                {
                    var section = $scope.getCorridor(id);
                    if (section!=undefined && section.state == "Stand-by" && $scope.system.isCalibrating == false)
                        return false;
                }
            }
            return true
        };

        $scope.getOffDisabled=function(id){
            if(roleService.isInRoleTechnischBeheer()) {
                if(id=='system') {
                    var nrStandby = $scope.secties.reduce(function(n, section) {
                        return n + (section.state== 'Stand-by');
                    }, 0);

                    if ($scope.system.state == "Stand-by" && $scope.system.isCalibrating == false && nrStandby==$scope.secties.length)
                        return false;
                }
                else
                {
                    var section = $scope.getCorridor(id);
                    if (section!=undefined && section.state == "Stand-by" && $scope.system.isCalibrating == false)
                        return false;
                }
            }
            return true
        };

        $scope.getCorridor=function(id){
            var corridor = $scope.secties.find(function (e) {
                return e.id == id
            });

            if (corridor==undefined)
                return undefined;
            else{
//                $log.debug("CORRIDOR");
//                $log.debug(corridor);
                return corridor;
            }
        };

        $scope.mapStateToColor=function(state){
            if(state=="Uit")
                return "rowRed";
            if(state=="Stand-by")
                return "rowOrange";
            if(state=="Storing" || state=="Uit" || state=="Onderhoud")
                return "rowRed";
            if(state=="Handhaven aan")
                return "rowGreen";
        };

        $scope.openModal = function (size) {

            var modalInstance = $modal.open({
                templateUrl: 'app/views/handhaving/modal-message-partial.html',
                controller: 'modalHandhaafController',
                size: size
            });

            modalInstance.result.then(function () {

            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
    }
]);

app.controller('modalHandhaafController', function ($scope, $modalInstance) {
    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});