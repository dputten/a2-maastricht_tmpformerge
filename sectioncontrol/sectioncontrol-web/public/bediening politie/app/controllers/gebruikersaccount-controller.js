/**
 * Created by CSC on 10/3/14.
 */
app.controller('gebruikersaccountController',
    ['$scope', '$stateParams', '$state','$cookieStore','GebruikersService','ApplicationCache','$log',
    function($scope, $stateParams, $state,$cookieStore,GebruikersService,applicationCache,$log) {

        $log.debug("In gebruikersaccountController");
        $scope.huidigeWachtwoordError=false;
        $scope.buttonDisabledWachtwoord=false;
        $scope.inputDisabledWachtwoord=true;
        $scope.isEditing=false;

        var promise = GebruikersService.getCurrentUser();
        promise.then(function (data) {
            $scope.gebruiker = {naam:data.name,gebruikersNaam:data.username,verbalisantNummer:data.reportingOfficerId,rol:data.role.name};
        }, function (data) {
            $log.debug(data);
            applicationCache.put("error",data);
            $state.go("error");
        });

        $scope.getinputDisabledWachtwoord=function(){return $scope.inputDisabledWachtwoord;};
        $scope.getHuidigeWachtwoordError=function(){return $scope.huidigeWachtwoordError;};
        $scope.wachtwoordModel={huidigWachtwoord:"",nieuwWachtwoord:"",nogmaalsnieuwWachtwoord:"",error:""};

        $scope.wijzigenWachtwoord = function () {
            $scope.inputDisabledWachtwoord=false;
            $scope.isEditing=true;
        };

        $scope.$watch('wachtwoordModel.huidigWachtwoord', function () {
            $scope.huidigeWachtwoordError=false;
        });

        $scope.wachtwoordOpslaan=function(wachtwoord_partial)
        {
            $scope.inputDisabledWachtwoord=true;
            $scope.buttonDisabledWachtwoord=true;

            if(!$scope.$$phase) {
                $scope.$apply();
            }

            var promise = GebruikersService.wijzigenWachtwoord($scope.wachtwoordModel.huidigWachtwoord,$scope.wachtwoordModel.nieuwWachtwoord);
            promise.then(function () {
                $scope.wachtwoordModel={huidigWachtwoord:"",nieuwWachtwoord:"",nogmaalsnieuwWachtwoord:"",error:""};
                $scope.inputDisabledWachtwoord=true;
                $scope.buttonDisabledWachtwoord=false;
                $scope.huidigeWachtwoordError=false;
                $scope.isEditing=false;
                wachtwoord_partial.$setPristine();

                if(!$scope.$$phase) {
                    $scope.$apply();
                }
            }, function (msg) {
                var result=msg;

                $scope.inputDisabledWachtwoord=false;
                $scope.buttonDisabledWachtwoord=false;
                $scope.huidigeWachtwoordError=true;

                if(result.errorCode=="0")
                    {$scope.wachtwoordModel.error="Het bestaat uit minimaal acht tekens. Het moet tenminste één letter, één cijfer en één leesteken bevatten."}
                else if(result.errorCode=="1")
                    {$scope.wachtwoordModel.error="Het wachtwoord mag niet gelijk zijn aan de gebruikersnaam."}
                else if(result.errorCode=="2")
                    {$scope.wachtwoordModel.error="Het wachtwoord mag niet gelijk zijn aan de 3 laatst gebruikte wachtwoorden."}
                else
                {
                }

                if(!$scope.$$phase) {
                    $scope.$apply();
                }
            });
        };

        function Rule(description, validator) {
            this.description = description;
            this.className = function () {
                return validator() ? "icon-check" : "icon-exclamation-sign";
            }
        }

        $scope.wachtwoordModel.nieuwWachtwoord = "";
        $scope.wachtwoordModel.nogmaalsnieuwWachtwoord = "";
        $scope.rules = [
            new Rule('Contain a digit', function () {
                return $scope.wachtwoordModel.nieuwWachtwoord.match(/[0-9]/);
            }),
            new Rule('Contain an upper case character', function () {
                return $scope.wachtwoordModel.nieuwWachtwoord.match(/[A-Z]/);
            }),
            new Rule('Contain a lower case character', function () {
                return $scope.wachtwoordModel.nieuwWachtwoord.match(/[a-z]/);
            })
        ];

        $scope.match = function() {
            $log.debug("in de match");
            return !$scope.wachtwoordModel.nieuwWachtwoord || !$scope.wachtwoordModel.nogmaalsnieuwWachtwoord || $scope.wachtwoordModel.nieuwWachtwoord == $scope.wachtwoordModel.nogmaalsnieuwWachtwoord;
        };
    }
]);

