/**
 // * Created by CSC on 10/29/14.
 */
app.controller('onderhoudenCertificatenController', ['$scope', 'loginService', 'ApplicationCache', 'RoleService', '$stateParams', '$state', 'CertificeringService', '$log', 'isSystemEditable', 'modalService',
    function ($scope, loginService, applicationCache, roleService, $stateParams, $state, certificeringService, $log, isSystemEditable, modalService) {
        $scope.isbtnDisabled = false;
        $scope.isbtnDisabledComponents = false;

        $scope.showDetails = false;
        $scope.showDetailsComponents = false;

        $scope.isAddnew = false;
        $scope.isAddnewComponent = false;
        $scope.errors = [];
        $scope.components = [];

        $scope.traject = applicationCache.get('traject');
        $scope.systemId = $scope.traject.systemId;
        $scope.systemName = $scope.traject.systemName;

        var promise = certificeringService.getAllTypeCertificering($scope.traject.systemId);
        promise.then(function (data) {
            $log.debug(data);
            $scope.certificaten = data;

            $log.debug($scope.certificaten);
        }, function (reason) {
            $log.debug(reason);
        });

        $scope.isEditDisabledForUser = function () {
            return !roleService.isInRoleTechnischBeheer() || isSystemEditable == false;
        };

        $scope.disableLinksComponents = function () {
            $scope.isbtnDisabledComponents = true;
        };

        $scope.enableLinksComponents = function () {
            $scope.isbtnDisabledComponents = false;
        };

        $scope.disableLinks = function () {
            $scope.isbtnDisabled = true;
        };

        $scope.enableLinks = function () {
            $scope.isbtnDisabled = false;
        };

        $scope.doShowDetails = function () {
            $scope.showDetails = true;
        };

        $scope.doShowDetailsComponents = function () {
            $scope.showDetailsComponents = true;
        };

        $scope.hideDetails = function () {
            $scope.showDetails = false;
        };

        $scope.hideDetailsComponents = function () {
            $scope.showDetailsComponents = false;
        };

        $scope.getChecksum = function (checksum) {
            return checksum.chunk(50).join('\n');
        };

        $scope.verwijderComponent = function (index) {
            var foundIndex = -1;
            for (var i = 0; i < $scope.components.length; i++) {
                if ($scope.components[i].index == index) {
                    foundIndex = i;
                }
            }

            if (foundIndex > -1) {
                $scope.components.splice(foundIndex, 1);
            }

            $scope.getSeal();
        };

        $scope.verwijder = function (certId) {
            modalService.showModal({}).then(function (result) {
                $log.debug(result);

                var promiseDelete = certificeringService.deleteTypeCertificering(certId);
                promiseDelete.then(function (data) {
                    if (data.success) {
                        var promise = certificeringService.getAllTypeCertificering($scope.traject.systemId);
                        promise.then(function (data) {
                            $log.debug(data);
                            $scope.certificaten = data;
                            $scope.typeCertificate = null;
                            $scope.components = [];

                            $scope.hideDetails();
                            $scope.enableLinks();

                            $scope.isAddnew = false;
                        }, function (reason) {
                            $log.debug(reason);
                        });
                    }
                    else {
                        if (data.fields !== undefined && data.fields.length > 0) {
                            $scope.errors = data.fields;
                            $log.debug($scope.errors);
                        }
                        else {
                            var x = {field: "", error: data.description};
                            $log.debug(x);
                            $scope.errors = [];
                            $scope.errors.push(x);
                            $log.debug($scope.errors);
                        }
                    }
                }, function (reason) {
                    $log.debug(reason);
                });
            });
        };

        $scope.addnewComponent = function () {
            $scope.doShowDetailsComponents();
            $scope.disableLinksComponents();

            $scope.showDetailsComponents = true;
            $scope.isAddnewComponent = true;
            $scope.errors = [];
        };

        $scope.addnew = function () {
            $scope.showDetails = true;
            $scope.isAddnew = true;
            $scope.disableLinks();
            $scope.errors = [];

            $scope.typeCertificate = {
                "id": null,
                "version": null,
                "certificate": {
                    "typeDesignation": "",
                    "category": "",
                    "unitSpeed": "",
                    "unitRedLight": "not used",
                    "unitLength": "",
                    "restrictiveConditions": "",
                    "displayRange": "",
                    "temperatureRange": "",
                    "permissibleError": "",
                    "typeCertificate": {
                        "id": "",
                        "revisionNr": "",
                        "components": []
                    }
                }
            }
        };

        $scope.wijzigenComponent = function (index) {
            var reference = $scope.components.find(function (e) {
                return e.index == index
            });

            // Create a copy otherwise the list binding will also show the changes immediately in the GUI.
            $scope.component = {index: reference.index, name: reference.name, checksum: reference.checksum};
            $scope.doShowDetailsComponents();
            $scope.disableLinksComponents();
        };

        $scope.wijzigen = function (certId) {
            var promise = certificeringService.getTypeCertificeringById(certId);
            promise.then(function (data) {
                $scope.errors = [];
                $scope.typeCertificate = data;
                $scope.components = [];

                if (data.certificate.typeCertificate.components != undefined) {
                    for (var i = 0; i < data.certificate.typeCertificate.components.length; i++) {
                        $scope.components.push({
                                index: i,
                                name: data.certificate.typeCertificate.components[i].name,
                                checksum: data.certificate.typeCertificate.components[i].checksum
                            }
                        )
                    }
                }

                $scope.getSeal();

                $scope.component = null;
                $log.debug(data);
                $scope.doShowDetails();
                $scope.disableLinks();
            }, function (data) {
                applicationCache.put("error", data);
                $state.go("error");
            });
        };

        $scope.bewaar = function () {
            var model = null;
            var promise;
            if ($scope.isAddnew) {
                $scope.typeCertificate.certificate.typeCertificate.components = $scope.components;
                model = $scope.typeCertificate;

                promise = certificeringService.postTypeCertificering(model);
            }
            else {
                $scope.typeCertificate.certificate.typeCertificate.components = $scope.components;
                model = $scope.typeCertificate;

                promise = certificeringService.putTypeCertificering(model.id, model);
            }

            promise.then(function (data) {
                if (data.success) {
                    var promise = certificeringService.getAllTypeCertificering($scope.traject.systemId);
                    promise.then(function (data) {
                        $log.debug(data);
                        $scope.certificaten = data;
                        $scope.typeCertificate = null;
                        $scope.components = [];

                        $scope.hideDetails();
                        $scope.enableLinks();

                        $scope.isAddnew = false;
                    }, function (reason) {
                        $log.debug(reason);
                    });
                }
                else {
                    if (data.fields !== undefined && data.fields.length > 0) {
                        $scope.errors = data.fields;
                        $log.debug($scope.errors);
                    }
                    else {
                        var x = {field: "", error: data.description};
                        $log.debug(x);
                        $scope.errors = [];
                        $scope.errors.push(x);
                        $log.debug($scope.errors);
                    }
                }
            }, function (reason) {
                $log.debug(reason);
            });

        };

        $scope.bewaarComponent = function () {
            if ($scope.isAddnewComponent) {
                $scope.component.index = $scope.components.length + 1;
                $scope.components.push({index: $scope.component.index, name: $scope.component.name, checksum: $scope.component.checksum});
            }
            else {
                var reference = $scope.components.find(function (e) {
                    return e.index == $scope.component.index
                });

                reference.index = $scope.component.index;
                reference.name = $scope.component.name;
                reference.checksum = $scope.component.checksum;
            }

            $scope.component = null;
            $scope.hideDetailsComponents();
            $scope.enableLinksComponents();
            $scope.isAddnewComponent = false;

            $scope.getSeal();
        };

        $scope.annuleer = function () {
            $scope.typeCertificate = null;
            $scope.components = [];
            $scope.hideDetails();
            $scope.enableLinks();
            $scope.isAddnew = false;
        };

        $scope.annuleerComponent = function () {
            $scope.component = null;
            $scope.hideDetailsComponents();
            $scope.enableLinksComponents();
            $scope.isAddnewComponent = false;
        };

        $scope.getSeal=function(){
            var promise = certificeringService.calculateSeal($scope.components);
            promise.then(function (data) {
                $scope.errors = [];
                $scope.softwareSeal=data.seal
            }, function (data) {
                applicationCache.put("error", data);
                $state.go("error");
            });
        }
    }
]);




