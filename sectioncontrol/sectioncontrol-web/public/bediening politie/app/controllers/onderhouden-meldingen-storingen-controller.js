/**
 * Created by CSC on 10/29/14.
 */
app.controller('onderhoudenmeldingenstoringenController',
    ['$scope','loginService','ApplicationCache','RoleService','VerdragslandenService','$stateParams', '$state','AlarmenSoringenService','$log',
        function($scope,loginService,applicationCache,roleService,verdragslandenService,$stateParams, $state,alarmenSoringenService,$log) {
            $log.debug("IN onderhoudenmeldingenstoringenController ");

            $scope.isbtnDisabled = false;
            $scope.isbtnDisabled2 = false;
            $scope.showDetails = false;
            $scope.showDetails2 = false;
            $scope.busy = false;

            $scope.traject = applicationCache.get('traject');
            var promise = alarmenSoringenService.getAlarmenStoringen($scope.traject.systemId);
            promise.then(function(data) {
                $scope.alarmenStoringen = data;
                $log.debug($scope.alarmenStoringen);
            }, function(reason) {
                $log.debug(reason);
            });

            $scope.disableLinks = function() {
                $scope.isbtnDisabled = true;
            };

            $scope.enableLinks = function() {
                $scope.isbtnDisabled = false;
            };

            $scope.disableButtons = function() {
                $scope.isbtnDisabled2 = true;
            };

            $scope.enableButtons = function() {
                $scope.isbtnDisabled2 = false;
            };

            $scope.doShowDetails = function(){
                $scope.showDetails = true;
            };

            $scope.hideDetails = function(){
                $scope.showDetails = false;
            };

            $scope.isInRolePolitie = function () {return roleService.isInRolePolitie();};

            $scope.isAfmeldenDisabledForUser= function () {
                if (roleService.isInRolePolitie())return true;
            };

            $scope.isBevestigenDisabledForUser= function () {
                if (roleService.isInRolePolitie())return true;
                if ($scope.alarmStoring==undefined)return true;

                return $scope.alarmStoring.confirmed;
            };

            $scope.wijzigen=function(errorId,errorType,corridorId){
                $scope.doShowDetails();
                $scope.disableLinks();

                var promise = alarmenSoringenService.getAlarmenStoring($scope.traject.systemId,errorId,errorType,corridorId);
                promise.then(function (data) {
                    $scope.alarmStoring = data;
                    $log.debug( data);
                }, function (data) {
                    applicationCache.put("error",data);
                    $state.go("error");
                });
            };

            $scope.afmelden=function(errorId,errorType,corridorId){
                $scope.doShowDetails();
                $scope.disableLinks();

                var promise = alarmenSoringenService.afmelden($scope.traject.systemId,errorId,errorType,corridorId);
                promise.then(function (data) {
                    if(data.success) {
                        $scope.hideDetails();
                        $scope.enableLinks();
                        $scope.errors = null;

                        var promiseAll = alarmenSoringenService.getAlarmenStoringen($scope.traject.systemId);
                        promiseAll.then(function(data) {
                            $scope.alarmenStoringen = data;
                            $log.debug($scope.alarmenStoringen);
                        }, function(data) {
                            applicationCache.put("error",data);
                            $state.go("error");
                        });
                    } else {
                        applicationCache.put("error",data);
                        $state.go("error");
                    }
                    $log.debug( data);
                }, function (data) {
                    applicationCache.put("error",data);
                    $state.go("error");
                });
            };

            $scope.bevestigen=function(errorId,errorType,corridorId){
                var promise = alarmenSoringenService.bevestigen($scope.traject.systemId,errorId,errorType,corridorId);
                promise.then(function (data) {
                    if(data.success) {
                        $scope.hideDetails();
                        $scope.enableLinks();
                        $scope.errors = null;

                        var promiseAll = alarmenSoringenService.getAlarmenStoringen($scope.traject.systemId);
                        promiseAll.then(function(data) {
                            $scope.alarmenStoringen = data;
                            $log.debug($scope.alarmenStoringen);
                        }, function(data) {
                            applicationCache.put("error",data);
                            $state.go("error");
                        });
                    }
                    $log.debug( data);
                }, function (data) {
                    applicationCache.put("error",data);
                    $state.go("error");
                });
            };

            $scope.getAantalStoringen=function()
            {
                if($scope.busy==false) {
                    $scope.busy=true;
                    var promise = alarmenSoringenService.numberOfErrors($scope.traject.systemId);
                    promise.then(function (data) {
                        if (data.success) {
                            $scope.errors = null;
                            $scope.busy=false;
                            return data;
                        }
                        $log.debug(data);
                    }, function (data) {
                        $scope.busy=false;
                        applicationCache.put("error", data);
                        $state.go("error");
                    });
                }
            }
        }
    ]);

