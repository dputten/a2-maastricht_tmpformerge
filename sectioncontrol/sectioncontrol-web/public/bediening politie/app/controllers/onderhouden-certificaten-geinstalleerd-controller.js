/**
 * Created by CSC on 10/29/14.
 */
app.controller('onderhoudenCertificatenGeinstalleerdController', ['$scope', 'loginService', 'ApplicationCache', 'RoleService', '$stateParams',
    '$state', 'trajectService', 'CertificeringService', '$log', 'isSystemEditable', '$modal', 'modalService',
    function ($scope, loginService, applicationCache, roleService, $stateParams, $state, trajectService, certificeringService, $log, isSystemEditable, $modal, modalService) {
        $scope.isbtnDisabled = false;
        $scope.showDetails = false;
        $scope.isAddnew = false;
        $scope.errors = [];
        $scope.traject = applicationCache.get('traject');
        $scope.systemId = $scope.traject.systemId;

        $scope.isEditDisabledForUser = function () {
            return !roleService.isInRoleTechnischBeheer() || isSystemEditable == false;
        };

        var promise = certificeringService.getAllInstalledCertificering($scope.traject.systemId);
        promise.then(function (data) {
            $log.debug(data);
            $scope.certificaten = data;
            $log.debug($scope.certificaten);
        }, function (reason) {
            $log.debug(reason);
        });

        $scope.disableLinks = function () {
            $scope.isbtnDisabled = true;
        };

        $scope.enableLinks = function () {
            $scope.isbtnDisabled = false;
        };

        $scope.disableButtons = function () {
            $scope.isbtnDisabled2 = true;
        };

        $scope.enableButtons = function () {
            $scope.isbtnDisabled2 = false;
        };

        $scope.doShowDetails = function () {
            $scope.showDetails = true;
        };

        $scope.hideDetails = function () {
            $scope.showDetails = false;
        };

        $scope.bewaar = function () {
            saveCertificate($scope, certificeringService, $log);
        };

        $scope.mapAvailableInstalledCertificeringToView = function (data) {
            var availableInstalledCertificering = [];
            data.forEach(function (regel) {
                $log.debug(regel);
                availableInstalledCertificering.push({
                    id: regel.id,
                    name: regel.certificate.typeCertificate.id + " revision: " + regel.certificate.typeCertificate.revisionNr
                });
            });
            return availableInstalledCertificering;
        };

        $scope.addnew = function () {

            var promise = certificeringService.getAllAvailableInstalledCertificering($scope.traject.systemId);
            promise.then(function (data) {
                $log.debug(data);

                $scope.availableInstalledCertificering = $scope.mapAvailableInstalledCertificeringToView(data);
                $log.debug($scope.availableInstalledCertificering);

                $scope.showDetails = true;
                $scope.isAddnew = true;
                $scope.disableLinks();
                $scope.errors = [];
                $scope.installedCertificate = {
                    typId: undefined,
                    installDate: undefined,
                    installDateTime: undefined
                };

            }, function (reason) {
                $log.debug(reason);
            });


        };

        $scope.annuleer = function () {
            $scope.installedCertificate = null;
            $scope.hideDetails();
            $scope.enableLinks();
        };

        $scope.deleteGeinstalleerdCertificaat = function (certId) {
            modalService.showModal({}).then(function (result) {
                var promise = certificeringService.deleteGeinstalleerdCertificering($scope.traject.systemId, certId);
                promise.then(function (data) {
                    if (data.success) {
                        var promise = certificeringService.getAllInstalledCertificering($scope.traject.systemId);
                        promise.then(function (data) {
                            $log.debug(data);
                            $scope.certificaten = data;
                            $log.debug($scope.certificaten);
                        }, function (reason) {
                            $log.debug(reason);
                        });
                    }
                    else {
                        if (data.fields !== undefined && data.fields.length > 0) {
                            $scope.errors = data.fields;
                            $log.debug($scope.errors);
                        }
                        else {
                            var x = {field: "", error: data.description};
                            $log.debug(x);
                            $scope.errors = [];
                            $scope.errors.push(x);
                            $log.debug($scope.errors);
                        }
                    }
                }, function (reason) {
                    $log.debug(reason);
                });
            });
        };
    }
]);

function saveCertificate($scope, certificeringService, $log) {
    var model = null;
    var beginHour = $scope.installedCertificate.installDateTime.substring(0, 2);
    var beginMinute = $scope.installedCertificate.installDateTime.substring(3, 5);

    var beginDay = $scope.installedCertificate.installDate.substring(0, 2);
    var beginMonth = $scope.installedCertificate.installDate.substring(3, 5);
    var beginYear = $scope.installedCertificate.installDate.substring(6, 10);

    var begin = new Date(beginYear, beginMonth - 1, beginDay, beginHour, beginMinute);
    var epochBegin = begin.getTime();

    var promise;
    if ($scope.isAddnew) {
        $log.debug($scope.traject);
        model =
        {
            "installDate": epochBegin,
            "typeId": $scope.installedCertificate.typeId
        };

        promise = certificeringService.postGeinstalleerdeCertificering($scope.systemId, model);
    }

    promise.then(function (data) {
        if (data.success) {
            var promiseAll = certificeringService.getAllInstalledCertificering($scope.traject.systemId);
            promiseAll.then(function (data) {
                $log.debug(data);
                $scope.certificaten = data;
                $scope.hideDetails();
                $scope.enableLinks();
                $log.debug($scope.certificaten);
            }, function (reason) {
                $log.debug(reason);
            });
        }
        else {
            if (data.fields !== undefined && data.fields.length > 0) {
                $scope.errors = data.fields;
                $log.debug($scope.errors);
            }
            else {
                var x = {field: "", error: data.description};
                $log.debug(x);
                $scope.errors = [];
                $scope.errors.push(x);
                $log.debug($scope.errors);
            }
        }
    }, function (reason) {
        $log.debug(reason);
    });
}



