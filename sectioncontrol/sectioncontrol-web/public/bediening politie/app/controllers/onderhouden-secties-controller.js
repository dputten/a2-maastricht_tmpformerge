app.controller('onderhoudenSectiesController', ['$scope','loginService','ApplicationCache','RoleService','SectionsService','$state','$log','PassagepuntenService','isSystemEditable',
    function($scope,loginService,applicationCache,roleService,sectionsService,$state,$log,
    passagepuntenService,isSystemEditable) {
        $scope.isbtnDisabled = false;
        $scope.showDetails = false;
        $scope.isAddnew = false;
        $scope.errors=[];

        $scope.traject = applicationCache.get('traject');
        $scope.systemId=$scope.traject.systemId;

        var promise = sectionsService.getSections($scope.systemId);
        promise.then(function (data) {
            $scope.sections = data;

            var passagepuntenPromise = passagepuntenService.getGantries($scope.systemId);
            passagepuntenPromise.then(function (data) {
                $log.debug(data);
                $scope.gantries = data;
            }, function (reason) {
                $log.debug(reason);
            });
        }, function (reason) {
            $log.debug(reason);
        });

        $scope.disableLinks = function() {
            $scope.isbtnDisabled = true;
        };

        $scope.enableLinks = function() {
            $scope.isbtnDisabled = false;
        };

        $scope.doShowDetails = function(){
            $scope.showDetails = true;
        };

        $scope.hideDetails = function(){
            $scope.showDetails = false;
        };

        $scope.isInRolePolitie1=function(){
            return roleService.isInRolePolitie();
        };

        $scope.isEditDisabledForUser=function(){ return !roleService.isInRoleTechnischBeheer() || isSystemEditable==false;};

        $scope.getType=function(section)
        {
            if(section.isStartInChain && !section.isLastInChain){
                return "Entree";
            } else if(!section.isStartInChain && section.isLastInChain){
                return "Exit";
            } else if(section.isStartInChain && section.isLastInChain){
                return "Entree/Exit";
            }else{
                return "Midden";
            }
        };

        $scope.addnew = function(){
            $scope.showDetails = true;
            $scope.isAddnew = true;
            $scope.disableLinks();
            $scope.errors=[];
            $scope.section={
                description:"",
                endGantryId:"",
                id:"",
                length:"",
                matrixBoards:"",
                name:"",
                startGantryId:"",
                version:""
            };

        };

        $scope.wijzigen=function(sectionId){
            var promise = sectionsService.getSection($scope.systemId,sectionId);
            promise.then(function (data) {
                $scope.errors=[];
                $scope.section = data;
                $scope.matrixBoards=$scope.section.matrixBoards.join();
                $log.debug( data);
                $scope.doShowDetails();
                $scope.disableLinks();
            }, function (data) {
                applicationCache.put("error",data);
                $state.go("error");
            });
        };

        $scope.bewaar=function(sectionId){
            saveSectie($scope,sectionsService,sectionId,$log);
        };

        $scope.annuleer=function()
        {
            $scope.section = null;
            $scope.matrixBoards=null;
            $scope.hideDetails();
            $scope.enableLinks();
        };

        $scope.verwijder=function(sectionId)
        {
            var sectionGetPromise = sectionsService.getSection($scope.systemId,sectionId);
            sectionGetPromise.then(function (data) {
                $scope.section = data;
                var deletePromise = sectionsService.deleteSection($scope.systemId,sectionId,$scope.section.version);
                deletePromise.then(function (data) {
                    if(data.success){
                        $scope.errors=[];
                        var promise = sectionsService.getSections($scope.systemId);
                        promise.then(function (data) {
                            $scope.sections = data;
                            $scope.errors=[];
                            $scope.hideDetails();
                            $scope.enableLinks();
                        }, function (reason) {
                            $log.debug(reason);
                        });
                    }
                    else
                    {
                        if(data.fields!==undefined && data.fields.length>0)
                        {
                            $scope.errors=data.fields;
                            $log.debug($scope.errors);
                        }
                        else
                        {
                            var x={field:"",error:data.description};
                            $log.debug(x);
                            $scope.errors=[];
                            $scope.errors.push(x);
                            $log.debug($scope.errors);
                        }
                    }
                }, function (reason) {
                    $log.debug(reason);
                });
            }, function (data) {
                applicationCache.put("error",data);
                $state.go("error");
            });
        };
    }
]);

function saveSectie($scope,sectionsService,sectionId,$log)
{
    $scope.section.matrixBoards=$scope.matrixBoards;
    var model={
                description:$scope.section.description,
                endGantryId:$scope.section.endGantryId,
                id:$scope.section.id,
                length:$scope.section.length,
                matrixBoards:$scope.section.matrixBoards,
                name:$scope.section.name,
                startGantryId:$scope.section.startGantryId,
                version:$scope.section.version
    };

    var promise;
    if($scope.isAddnew ){
        $log.debug($scope.systemId);
        promise = sectionsService.postSection($scope.systemId, model);
    }
    else {
        promise = sectionsService.putSection($scope.systemId,sectionId, model);
    }

    promise.then(function (data) {
        if(data.success){
            var sectionsPromise = sectionsService.getSections($scope.systemId);
            sectionsPromise.then(function (data) {
                $scope.sections = data;
                $scope.section=null;
                $scope.hideDetails();
                $scope.enableLinks();
            }, function (reason) {
                $log.debug(reason);
            });
        }
        else
        {
            if(data.fields!==undefined && data.fields.length>0)
            {
                $scope.errors=data.fields;
                $log.debug($scope.errors);
            }
            else
            {
                var x={field:"",error:data.description};
                $log.debug(x);
                $scope.errors=[];
                $scope.errors.push(x);
                $log.debug($scope.errors);
            }
        }
    }, function (reason) {
        $log.debug(reason);
    });
}
