//http://stackoverflow.com/questions/8636617/how-to-get-start-and-end-of-day-in-javascript
//http://www.w3schools.com/jsref/jsref_utc.asp
app.controller('logboekController', ['$scope', '$stateParams', '$state', 'LogboekServiceGet', 'ApplicationCache','config','$log','$window','$timeout','$interval',
    function ($scope, $stateParams, $state, LogboekServiceGet, applicationCache,config,$log,$window,$timeout,$interval) {
        $log.debug("logboekController");

        var now = new Date();// is nu utc
        $scope.logboekBeginDatum= new Date(now.getFullYear(), now.getMonth(), now.getDate(),0,0,0);
        $scope.logboekEindDatum = new Date(now.getFullYear(), now.getMonth(), now.getDate(),23,59,59);
        $scope.startingup=true;
        $scope.buttonDisabledZoeken=false;
        $scope.buttonDisabledDownload=true;
        $scope.searchIsInvalid=false;

        $scope.getDateFromStringLogboek=function(dateString){
            return getDateFromStringDutch(dateString)  ;
        };

        $scope.getTimeFromStringLogboek = function (dateString) {
            return getTimeFromString(dateString);
        };

        var endTime = new Date().getTime();// nu
        var beginTime = endTime-((1000*60)*60*24);// 24 uur

        // Beperk de selectie mogelijkheid in de to kalender.
        $scope.maxEndDate=new Date(new Date($scope.logboekBeginDatum).setMonth($scope.logboekBeginDatum.getMonth()+1));
        $scope.minEndDate= $scope.logboekBeginDatum;
        $scope.traject = applicationCache.get('traject');
        $scope.beginTime=beginTime;
        $scope.endTime=endTime;

        function getCookieValue(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');

            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1);
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }

        $scope.guid="";
        var timer;

        $scope.cookieChecker = function () {
            timer = $interval(function () {
                var cookie = getCookieValue("id");
                if (cookie != "") {
                    if (getCookieValue("id") == $scope.guid) {
                        document.cookie = "id=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
                        $scope.buttonDisabledDownload = false;
                        $scope.guid="";
                        if(angular.isDefined(timer))
                        {
                            $log.debug("$destroy");
                            $interval.cancel(timer);
                            timer=undefined;
                        }
                    }
                }
            }, 100);
        };

        $scope.downloadLogboek=function(){
            $scope.buttonDisabledDownload=true;
            $scope.cookieChecker();
            $window.location.href = $scope.adres();
        };

        $scope.adres=function(){
            $scope.guid=guid();
            return  "{0}bediening/{1}/downloadlogs/system?from={2}&to={3}&id={4}".format(config.HTTP_BASE_URL,$scope.traject.systemId,$scope.beginTime,$scope.endTime,$scope.guid)
        };

        getLogboek(LogboekServiceGet, $scope,beginTime,endTime,applicationCache,$state,$log);

        $scope.zoeken = function () {
            var begin;
            var end;
            begin=$scope.logboekBeginDatum.getTime();
            end=$scope.logboekEindDatum.getTime();

            $scope.beginTime=begin;
            $scope.endTime=end;

            getLogboek(LogboekServiceGet, $scope,begin,end,applicationCache,$state,$log);
            $scope.startingup=false;
        };

        $scope.$watch('logboekBeginDatum', function () {
            $scope.validate();
        });

        $scope.$watch('logboekEindDatum', function () {
            $scope.validate();
        });

        $scope.validate=function(){
            $scope.maxEndDate=new Date(new Date($scope.logboekBeginDatum).setMonth($scope.logboekBeginDatum.getMonth()+1));
            $scope.minEndDate=$scope.logboekBeginDatum;
            $scope.errorMsg="";
            $scope.searchIsInvalid=false;
            $scope.noResults=false;

            var beginDatumJaar=$scope.logboekBeginDatum.getFullYear ();
            var beginDatumMaand=$scope.logboekBeginDatum.getMonth();
            var eindDatumJaar=$scope.logboekEindDatum.getFullYear();
            var eindDatumMaand=$scope.logboekEindDatum.getMonth();
            var beginDatumDag=$scope.logboekBeginDatum.getDate();
            var eindDatumDag=$scope.logboekEindDatum.getDate();
            var huidigeMaand=new Date().getMonth();
            var huidigeJaar=new Date().getFullYear ();
            var huidigeDag=new Date().getDate ();
            var huidigeAantalMaanden=(huidigeMaand+1)+(12*huidigeJaar);
            var beginDatumAantalMaanden=(beginDatumMaand+1)+(12*beginDatumJaar);
            var eindDatumAantalMaanden=(eindDatumMaand+1)+(12*eindDatumJaar);

            if(huidigeAantalMaanden-beginDatumAantalMaanden>12){
                $scope.searchIsInvalid=true;
                if($scope.searchIsInvalid)
                    $scope.errorMsg="Geen geldige datums ingevoerd. De termijn waarin gezocht kan worden is maximaal 1 jaar terug.";
            }
            else if(huidigeAantalMaanden-beginDatumAantalMaanden==12){
                $scope.searchIsInvalid=(beginDatumDag<huidigeDag);
                if($scope.searchIsInvalid){
                    $scope.errorMsg="Geen geldige datums ingevoerd. De termijn waarin gezocht kan worden is maximaal 1 jaar terug.";
                }
                else{
                    if(eindDatumAantalMaanden-beginDatumAantalMaanden>2){
                        $scope.searchIsInvalid=true;
                        if($scope.searchIsInvalid)
                            $scope.errorMsg="Zoekperiode is te lang. De periode voor zoekopdrachten is maximaal twee maanden.";
                    } else if(eindDatumAantalMaanden-beginDatumAantalMaanden==2){
                        $scope.searchIsInvalid=(eindDatumDag>beginDatumDag);
                        if($scope.searchIsInvalid)
                            $scope.errorMsg="Zoekperiode is te lang. De periode voor zoekopdrachten is maximaal twee maanden.";
                    }
                    else if(eindDatumAantalMaanden-beginDatumAantalMaanden==0){
                        $scope.searchIsInvalid=(eindDatumDag< beginDatumDag);
                        if($scope.searchIsInvalid)
                            $scope.errorMsg="Het is niet mogelijk een einddatum te selecteren die voor de begindatum ligt.";
                    }
                    else if(eindDatumAantalMaanden-beginDatumAantalMaanden<0){
                        $scope.searchIsInvalid=true;
                        if($scope.searchIsInvalid)
                            $scope.errorMsg="Het is niet mogelijk een einddatum te selecteren die voor de begindatum ligt.";
                    }
                    else
                    {
                        $scope.searchIsInvalid=false;
                    }
                }
            }
            else if(eindDatumAantalMaanden-beginDatumAantalMaanden>2){
                $scope.searchIsInvalid=true;
                if($scope.searchIsInvalid)
                    $scope.errorMsg="Zoekperiode is te lang. De periode voor zoekopdrachten is maximaal twee maanden.";
            }
            else if(eindDatumAantalMaanden-beginDatumAantalMaanden==2){
                $scope.searchIsInvalid=(eindDatumDag>beginDatumDag);
                if($scope.searchIsInvalid)
                    $scope.errorMsg="Zoekperiode is te lang. De periode voor zoekopdrachten is maximaal twee maanden.";
            }
            else if(eindDatumAantalMaanden-beginDatumAantalMaanden==0){
                $scope.searchIsInvalid=(eindDatumDag< beginDatumDag);
                if($scope.searchIsInvalid)
                    $scope.errorMsg="Het is niet mogelijk een einddatum te selecteren die voor de begindatum ligt.";
            }
            else if(eindDatumAantalMaanden-beginDatumAantalMaanden<0){
                $scope.searchIsInvalid=true;
                if($scope.searchIsInvalid)
                    $scope.errorMsg="Het is niet mogelijk een einddatum te selecteren die voor de begindatum ligt.";
            }
            else
            {
                $scope.searchIsInvalid=false;
            }

            $scope.$digest;
            if(!$scope.$$phase) {
                $scope.$apply();
            }
        }
    }
]);

function getLogboek(LogboekServiceGet, $scope,beginTime,endTime,applicationCache,$state,$log) {
    $scope.buttonDisabledZoeken=true;
    $scope.buttonDisabledDownload=true;

    var promise = LogboekServiceGet.getLogboek(beginTime, endTime, $scope.traject.systemId);
    promise.then(function (data) {
        $log.debug(data);

        if(data==undefined || data.length==0){
            $scope.errorMsg="Geen zoekresultaten gevonden voor de ingevoerde datums.";
            $scope.noResults=true;
        }

        if($scope.startingup)
        {
            if(data.length>=10)
                data=data.slice(0,10);
        }

        $scope.logboek = data;
        $scope.buttonDisabledZoeken=false;
        $scope.buttonDisabledDownload=false;

    }, function (data) {
        $log.debug(data);
        $scope.buttonDisabledZoeken=false;
        $scope.buttonDisabledDownload=true;
        applicationCache.put("error",data);
        $state.go("error");
    });
}
