app.controller('onderhoudenCertificatenlocatieController', ['$scope', 'loginService', 'ApplicationCache', 'RoleService', '$stateParams', '$state', 'CertificeringService', '$log', 'isSystemEditable', 'modalService',
    function ($scope, loginService, applicationCache, roleService, $stateParams, $state, certificeringService, $log, isSystemEditable, modalService) {
        $scope.isbtnDisabled = false;
        $scope.isbtnDisabledComponents = false;

        $scope.showDetails = false;
        $scope.showDetailsComponents = false;

        $scope.isAddnew = false;
        $scope.isAddnewComponent = false;
        $scope.errors = [];

        $scope.traject = applicationCache.get('traject');
        $scope.systemId = $scope.traject.systemId;
        $scope.systemName = $scope.traject.systemName;

        var promise = certificeringService.getLocatieCertificering($scope.traject.systemId);
        promise.then(function (data) {
            $log.debug(data);
            $scope.certificaten = data;
            $log.debug($scope.certificaten);
        }, function (reason) {
            $log.debug(reason);
        });

        $scope.isEditDisabledForUser = function () {
            return !roleService.isInRoleTechnischBeheer() || isSystemEditable == false;
        };

        $scope.disableLinksComponents = function () {
            $scope.isbtnDisabledComponents = true;
        };

        $scope.enableLinksComponents = function () {
            $scope.isbtnDisabledComponents = false;
        };

        $scope.disableLinks = function () {
            $scope.isbtnDisabled = true;
        };

        $scope.enableLinks = function () {
            $scope.isbtnDisabled = false;
        };

        $scope.doShowDetails = function () {
            $scope.showDetails = true;
        };

        $scope.doShowDetailsComponents = function () {
            $scope.showDetailsComponents = true;
        };

        $scope.hideDetails = function () {
            $scope.showDetails = false;
        };

        $scope.hideDetailsComponents = function () {
            $scope.showDetailsComponents = false;
        };

        $scope.getChecksum = function (checksum) {
            return checksum.chunk(50).join('\n');
        };

        $scope.getDutchDateTimeFromEpochString = function (dateString) {
            return getDutchDateTimeFromEpochString(dateString);
        };

        $scope.verwijderComponent = function (index) {
            var foundIndex = -1;
            for (var i = 0; i < $scope.locationCertificate.components.length; i++) {
                if ($scope.locationCertificate.components[i].index == index) {
                    foundIndex = i;
                }
            }

            if (foundIndex > -1) {
                $scope.locationCertificate.components.splice(foundIndex, 1);
            }

            $scope.getSeal();
        };

        $scope.verwijder = function (locationCertificateId) {
            modalService.showModal({}).then(function (result) {
                $log.debug(result);

                var promiseDelete = certificeringService.deleteLocatieCertificering($scope.traject.systemId, locationCertificateId);
                promiseDelete.then(function (data) {
                    if (data.success) {
                        var promiseAll = certificeringService.getLocatieCertificering($scope.traject.systemId);
                        promiseAll.then(function (data) {
                            $log.debug(data);
                            $scope.certificaten = data;
                            $log.debug($scope.certificaten);
                        }, function (reason) {
                            $log.debug(reason);
                        });
                    }
                    else {
                        if (data.fields !== undefined && data.fields.length > 0) {
                            $scope.errors = data.fields;
                            $log.debug($scope.errors);
                        }
                        else {
                            var x = {field: "", error: data.description};
                            $log.debug(x);
                            $scope.errors = [];
                            $scope.errors.push(x);
                            $log.debug($scope.errors);
                        }
                    }
                }, function (reason) {
                    $log.debug(reason);
                });
            });
        };

        $scope.addnewComponent = function () {
            $scope.doShowDetailsComponents();
            $scope.disableLinksComponents();

            $scope.showDetailsComponents = true;
            $scope.isAddnewComponent = true;
            $scope.errors = [];
        };

        $scope.addnew = function () {
            $scope.showDetails = true;
            $scope.isAddnew = true;
            $scope.disableLinks();
            $scope.errors = [];

            $scope.locationCertificate = {
                id: undefined,
                inspectionDate: undefined,
                inspectionDateTime: undefined,
                validTime: undefined,
                validTimeTime: undefined,
                components: []
            };
        };

        $scope.wijzigenComponent = function (index) {
            var reference = $scope.locationCertificate.components.find(function (e) {
                return e.index == index
            });

            // Create a copy otherwise the list binding will also show the changes immediately in the GUI.
            $scope.component = {index: reference.index, name: reference.name, checksum: reference.checksum};
            $scope.doShowDetailsComponents();
            $scope.disableLinksComponents();
        };

        $scope.wijzigen = function (locationCertificateId) {
            var promise = certificeringService.getLocatieCertificeringById($scope.traject.systemId, locationCertificateId);
            promise.then(function (data) {
                $scope.errors = [];
                $scope.locationCertificate = {
                    id: data.certificate.id,
                    inspectionDate: getDutchDateFromEpochString(data.certificate.inspectionDate),
                    inspectionDateTime: getDutchTimeFromEpochString(data.certificate.inspectionDate),
                    validTime: getDutchDateFromEpochString(data.certificate.validTime),
                    validTimeTime: getDutchTimeFromEpochString(data.certificate.validTime),
                    components: []
                };

                if (data.certificate.components != undefined) {
                    for (var i = 0; i < data.certificate.components.length; i++) {
                        $scope.locationCertificate.components.push({
                                index: i,
                                name: data.certificate.components[i].name,
                                checksum: data.certificate.components[i].checksum
                            }
                        )
                    }
                }

                $scope.getSeal();

                $scope.fetchedLocationCertificate = data;
                $scope.component = null;
                $log.debug(data);
                $scope.doShowDetails();
                $scope.disableLinks();
            }, function (data) {
                applicationCache.put("error", data);
                $state.go("error");
            });
        };

        $scope.bewaar = function () {
            savelocationCertificate($scope, certificeringService, $log);
        };

        $scope.bewaarComponent = function () {
            if ($scope.isAddnewComponent) {
                $scope.component.index = $scope.locationCertificate.components.length + 1;
                $scope.locationCertificate.components.push({index: $scope.component.index, name: $scope.component.name, checksum: $scope.component.checksum});
            }
            else {
                var reference = $scope.locationCertificate.components.find(function (e) {
                    return e.index == $scope.component.index
                });

                reference.index = $scope.component.index;
                reference.name = $scope.component.name;
                reference.checksum = $scope.component.checksum;
            }

            $scope.component = null;
            $scope.hideDetailsComponents();
            $scope.enableLinksComponents();
            $scope.isAddnewComponent = false;

            $scope.getSeal();
        };

        $scope.annuleer = function () {
            $scope.locationCertificate = null;
            $scope.hideDetails();
            $scope.enableLinks();
            $scope.isAddnew = false;
        };

        $scope.annuleerComponent = function () {
            $scope.component = null;
            $scope.hideDetailsComponents();
            $scope.enableLinksComponents();
            $scope.isAddnewComponent = false;
        };

        $scope.getSeal=function(){
            var promise = certificeringService.calculateSeal($scope.locationCertificate.components);
            promise.then(function (data) {
                $scope.errors = [];
                $scope.configurationSeal=data.seal
            }, function (data) {
                applicationCache.put("error", data);
                $state.go("error");
            });
        }
    }
]);

function savelocationCertificate($scope, certificeringService, $log) {
    var model = null;
    var beginHour = $scope.locationCertificate.inspectionDateTime.substring(0, 2);
    var beginMinute = $scope.locationCertificate.inspectionDateTime.substring(3, 5);

    var endHour = $scope.locationCertificate.validTimeTime.substring(0, 2);
    var endMinute = $scope.locationCertificate.validTimeTime.substring(3, 5);

    var beginDay = $scope.locationCertificate.inspectionDate.substring(0, 2);
    var beginMonth = $scope.locationCertificate.inspectionDate.substring(3, 5);
    var beginYear = $scope.locationCertificate.inspectionDate.substring(6, 10);

    var endDay = $scope.locationCertificate.validTime.substring(0, 2);
    var endMonth = $scope.locationCertificate.validTime.substring(3, 5);
    var endYear = $scope.locationCertificate.validTime.substring(6, 10);

    var begin = new Date(beginYear, beginMonth - 1, beginDay, beginHour, beginMinute);
    var end = new Date(endYear, endMonth - 1, endDay, endHour, endMinute);

    var epochBegin = begin.getTime();
    var epochEnd = end.getTime();

    var promise;
    if ($scope.isAddnew) {
        $log.debug($scope.traject);

        model =
        {
            "systemId": $scope.systemId,
            "certificate": {
                "id": $scope.locationCertificate.id,
                "inspectionDate": epochBegin,
                "validTime": epochEnd,
                "components": $scope.locationCertificate.components
            }
        };

        promise = certificeringService.postLocatieCertificering($scope.systemId, model);
    }
    else {
        $scope.fetchedLocationCertificate.certificate.inspectionDate = epochBegin;
        $scope.fetchedLocationCertificate.certificate.validTime = epochEnd;
        $scope.fetchedLocationCertificate.certificate.id = $scope.locationCertificate.id;
        $scope.fetchedLocationCertificate.certificate.checksum = $scope.locationCertificate.checksum;
        $scope.fetchedLocationCertificate.certificate.components = $scope.locationCertificate.components;
        model = $scope.fetchedLocationCertificate;

        promise = certificeringService.putLocatieCertificering($scope.systemId, model.id, model);
    }

    promise.then(function (data) {
        if (data.success) {
            var allPromise = certificeringService.getLocatieCertificering($scope.traject.systemId);
            allPromise.then(function (data) {
                $log.debug(data);
                $scope.certificaten = data;
                $scope.locationCertificate = null;

                $scope.hideDetails();
                $scope.enableLinks();

                $scope.isAddnew = false;
            }, function (reason) {
                $log.debug(reason);
            });
        }
        else {
            if (data.fields !== undefined && data.fields.length > 0) {
                $scope.errors = data.fields;
                $log.debug($scope.errors);
            }
            else {
                var x = {field: "", error: data.description};
                $log.debug(x);
                $scope.errors = [];
                $scope.errors.push(x);
                $log.debug($scope.errors);
            }
        }
    }, function (reason) {
        $log.debug(reason);
    });
}


