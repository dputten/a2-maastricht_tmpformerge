/**
 * Created by CSC on 10/29/14.
 */
app.controller('onderhoudenTrajectcontrolesysteemController',
    ['$scope','loginService','ApplicationCache','RoleService','trajectService','SectionsService',
        '$state','$log','isSystemEditable','InstellingenService','OnderhoudenGebruikersService','$rootScope',
    function( $scope,loginService,applicationCache,roleService,trajectService,sectionsService,$state,$log,
              isSystemEditable,instellingenService,OnderhoudenGebruikersService,$rootScope) {
        $scope.isbtnDisabled = false;
        $scope.showDetails = false;
        $scope.isbtnDisabled = false;
        $scope.showDetails = false;
        $scope.isAddnew = false;
        $scope.errors=[];
        $scope.traject = applicationCache.get('traject');
        $scope.systemId=$scope.traject.systemId;

        $scope.isEditDisabledForUser=function(){ return !roleService.isInRoleTechnischBeheer() || isSystemEditable==false;};

        $scope.getSystems=function()
        {
            var promise = OnderhoudenGebruikersService.getAllSystemsSmall();
            promise.then(function (data) {
                $scope.systems = data;
            }, function (reason) {
                $log.debug(reason);
            });
        };

        $scope.getSystems();

        $scope.disableLinks = function() {
            $scope.isbtnDisabled = true;
        };

        $scope.enableLinks = function() {
            $scope.isbtnDisabled = false;
        };

        $scope.doShowDetails = function(){
            $scope.showDetails = true;
        };

        $scope.hideDetails = function(){
            $scope.showDetails = false;
        };

        $scope.annuleer=function()
        {
            $scope.hideDetails();
            $scope.enableLinks();
        };

        $scope.bewaar=function(){
            $log.debug($scope.system);

            var promise= instellingenService.putSystemTechnical($scope.systemId, $scope.system);
            promise.then(function (data) {
                if(data.success){
                    $scope.hideDetails();
                    $scope.enableLinks();
                    $scope.getSystems();
                    $scope.setCache();
                    $rootScope.trajectTitle= $scope.system.system.data.title;
                }
                else
                {
                    if(data.fields!==undefined && data.fields.length>0)
                    {
                        $scope.errors=data.fields;
                        $log.debug($scope.errors);
                    }
                    else
                    {
                        var x={field:"",error:data.description};
                        $log.debug(x);
                        $scope.errors=[];
                        $scope.errors.push(x);
                        $log.debug($scope.errors);
                    }
                }
            }, function (reason) {
                $log.debug(reason);
            });

        };

        $scope.wijzigen=function(systemId){
            var promise= instellingenService.getSystem(systemId);
            promise.then(function (data) {
                $scope.doShowDetails();
                $scope.disableLinks();
                $scope.system=data;

            }, function (reason) {
                $log.debug(reason);
            });

            $scope.errors=[];
        };

        $scope.setCache=function(){
            var promise=trajectService.getTraject1();
            promise.then(function (data) {

                var traject;
                var tabs=[];
                var index = 0;

                for (var i = 0; i < data.length; i++) {
                    traject=({
                        title: data[i].system.data.title,
                        id: index,secties:[],
                        systemId:data[i].system.data.id,
                        gantries:[],
                        corridors:[]});

                    // Maak het traject beschikbaar voor het menu.
                    tabs.push({title: "Traject", id: index,systemId:data[i].system.data.id,coridorId:-1})

                    index++;
                    // SECTIES
                    for (var ii = 0; ii < data[i].sections.length; ii++) {
                        $log.debug("lengte=" + data[i].sections[ii].name + index);

                        traject.secties.push({title: data[i].sections[ii].name, id: index,
                            description: data[i].sections[ii].description,
                            sectionId: data[i].sections[ii].id});
                    }
                    // GANTRIES
                    for (var ii = 0; ii < data[i].gantries.length; ii++) {
                        traject.gantries.push({id: data[i].gantries[ii].id, name: data[i].gantries[ii].name, hectometer: data[i].gantries[ii].hectometer});
                    }

                    // CORRIDORS
                    for (var ii = 0; ii < data[i].corridors.length; ii++) {
                        traject.corridors.push({id: data[i].corridors[ii].id,
                            tabId: index,
                            name: data[i].corridors[ii].name,
                            allSectionIds: data[i].corridors[ii].allSectionIds
                        });
                        // Maak de zookeeper corridor beschikbaar voor het menu.
                        tabs.push({title: data[i].corridors[ii].name, id: index,systemId:-1,corridorId:data[i].corridors[ii].id})

                        index++;
                    }

                    applicationCache.put('traject', traject);
                    applicationCache.put('tabs', tabs);

                    break;//alleen het eerste traject is nodig/
                }
            }, function (reason) {
                $log.debug("in de error terug naar login " + reason);
            });
        }
    }
]);
