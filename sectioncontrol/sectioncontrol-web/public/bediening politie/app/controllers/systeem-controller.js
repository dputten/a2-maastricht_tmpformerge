/**
 * Created by CSC on 10/29/14.
 */
app.controller('systeemcontroller', ['$scope','loginService','ApplicationCache','RoleService','$stateParams', 'getLinkSystemResources','$state',
    function($scope,loginService,applicationCache,roleService,$stateParams, getLinkSystemResources,$state) {
        $scope.title=$stateParams.title;
        $scope.getLinkSystemResources=getLinkSystemResources;



        $scope.isInRolePolitie=function(){
            return roleService.isInRolePolitie();
        };
        $scope.isInRoleTechnischBeheer=function(){
            return roleService.isInRoleTechnischBeheer();
        };
        $scope.isInRoleTactischBeheer=function(){
            return roleService.isInRoleTactischBeheer();
        };

        $scope.getSystemResources=function()
        {
            return $scope.getLinkSystemResources.replace(/"/g,"");
        };

        if($scope.isInRolePolitie())
        {
            $state.go("handhaving");
        }
    }
]);