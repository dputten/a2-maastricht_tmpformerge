/**
 * Created by CSC on 10/29/14.
 */
app.controller('onderhoudenrijbanenController', ['$scope','loginService','ApplicationCache','RoleService','$stateParams', '$state',
    'OnderhoudenLanesService','$log','PassagepuntenService','isSystemEditable','modalService',
    function($scope,loginService,applicationCache,roleService,$stateParams, $state,onderhoudenLanesService,$log,
             passagepuntenService,isSystemEditable,modalService) {
        $scope.isbtnDisabled = false;
        $scope.showDetails = false;
        $scope.isAddnew = false;
        $scope.errors=[];

        $scope.traject = applicationCache.get('traject');
        $scope.systemId=$scope.traject.systemId;

        $scope.isEditDisabledForUser=function(){ return !roleService.isInRoleTechnischBeheer() || isSystemEditable==false;};

        var promise = onderhoudenLanesService.getAllLanes();
        promise.then(function (data) {
            $scope.rijbanen = data;
            $log.debug( data);

        }, function (reason) {
            $log.debug(reason);
        });

        var passagepuntenPromise = passagepuntenService.getGantries($scope.systemId);
        passagepuntenPromise.then(function (data) {
            $log.debug(data);
            $scope.gantries = data;
        }, function (reason) {
            $log.debug(reason);
        });

        $scope.disableLinks = function() {
            $scope.isbtnDisabled = true;
        };

        $scope.enableLinks = function() {
            $scope.isbtnDisabled = false;
        };

        $scope.disableButtons = function() {
            $scope.isbtnDisabled2 = true;
        };

        $scope.enableButtons = function() {
            $scope.isbtnDisabled2 = false;
        };

        $scope.doShowDetails = function(){
            $scope.showDetails = true;
        };

        $scope.hideDetails = function(){
            $scope.showDetails = false;
        };

        $scope.annuleer=function()
        {
            $scope.lane=null;
            $scope.gantryId = null;
            $scope.hideDetails();
            $scope.enableLinks();
        };

        $scope.addnew = function(){
            $scope.showDetails = true;
            $scope.isAddnew = true;
            $scope.disableLinks();
            $scope.errors=[];
        };

        $scope.bewaar=function(){
            saveRijbaan($scope,onderhoudenLanesService,$log);
        };

        $scope.wijzigen=function(gantryId,laneId){
            $scope.doShowDetails();
            $scope.disableLinks();
            $scope.isAddnew = false;
            $scope.errors=[];

            var promise = onderhoudenLanesService.getLane($scope.systemId,gantryId,laneId);
            promise.then(function (data) {
                $scope.gantryId = gantryId;
                $scope.laneId = laneId;
                $scope.lane = data;
                $log.debug( data);
            }, function (reason) {
                $log.debug(reason);
            });

        };

        $scope.verwijder=function(systemId,gantryId,laneId)
        {
            modalService.showModal({}).then(function (result) {
                $log.debug(result);
            var promiseGet = onderhoudenLanesService.getLane($scope.systemId,gantryId,laneId);
            promiseGet.then(function (data) {
                var promise = onderhoudenLanesService.deleteLane(systemId,gantryId,laneId,data.version);
                promise.then(function (data) {
                    if(data.success){
                        var promiseGetAll = onderhoudenLanesService.getAllLanes();
                        promiseGetAll.then(function (data) {
                            $scope.rijbanen = data;
                            $scope.lane=null;
                            $scope.gantryId = null;
                            $log.debug( data);

                        }, function (reason) {
                            $log.debug(reason);
                        });
                    }
                    else
                    {
                        if(data.fields!==undefined && data.fields.length>0)
                        {
                            $scope.errors=data.fields;
                            $log.debug($scope.errors);
                        }
                        else
                        {
                            var x={field:"",error:data.description};
                            $log.debug(x);
                            $scope.errors=[];
                            $scope.errors.push(x);
                            $log.debug($scope.errors);
                        }
                    }
                }, function (reason) {
                    $log.debug(reason);
                });
            }, function (reason) {
                $log.debug(reason);
            });

            });

        }
    }
]);

function saveRijbaan($scope,onderhoudenLanesService,$log)
{
    var model=$scope.lane;
    $log.debug(model);

    var promise;
    if($scope.isAddnew ){
        $log.debug($scope.systemId);
        model.version=0;
        model.sensor.id="0";
         promise = onderhoudenLanesService.postLane($scope.systemId, $scope.gantryId,$scope.laneId,model);
    }
    else {
        promise = onderhoudenLanesService.putLane($scope.systemId,$scope.gantryId,$scope.laneId, model);
    }

    promise.then(function (data) {
        if(data.success){
            $scope.hideDetails();
            $scope.enableLinks();
            $scope.lane=null;
            $scope.gantryId = null;

            var promise = onderhoudenLanesService.getAllLanes();
            promise.then(function (data) {
                $scope.rijbanen = data;
                $log.debug( data);
            }, function (reason) {
                $log.debug(reason);
            });
        }
        else
        {
            if(data.fields!==undefined && data.fields.length>0)
            {
                $scope.errors=data.fields;
                $log.debug($scope.errors);
            }
            else
            {
                var x={field:"",error:data.description};
                $log.debug(x);
                $scope.errors=[];
                $scope.errors.push(x);
                $log.debug($scope.errors);
            }
        }
    }, function (reason) {
        $log.debug(reason);
    });
}
