/**
 * Created by CSC on 10/29/14.
 */
app.controller('topheadercontroller', ['$scope','loginService','ApplicationCache','RoleService','$state','AlarmenSoringenService','$timeout','$log','$rootScope',
    function($scope,loginService,applicationCache,roleService,$state,alarmenSoringenService,$timeout,$log,$rootScope) {

        var promise;
        $scope.username=loginService.getUserName();
        $scope.$state = $state;
        $scope.traject=applicationCache.get('traject');
        $scope.trajectTitle= $scope.traject.title;
        $rootScope.trajectTitle= $scope.traject.title;
        $scope.nrOfErrors="";

        $scope.isInRolePolitie=function(){return roleService.isInRolePolitie();};

        $rootScope.$watch('trajectTitle', function () {
            $scope.trajectTitle= $rootScope.trajectTitle;
        });

        $scope.$on('$destroy', function(){
            if($scope.isInRolePolitie()==false) {
                $log.debug("$destroy");
                $timeout.cancel(promise);
            }
        });

        $scope.$on('$locationChangeStart', function(){
            if($scope.isInRolePolitie()==false) {
                $timeout.cancel(promise);
            }
         });

        (function tick() {
            if($scope.isInRolePolitie()==false) {
                var promise = alarmenSoringenService.numberOfErrors($scope.traject.systemId);
                promise.then(function (data) {
                    $scope.errors = null;
                    if (data != undefined && data != "")
                        $scope.nrOfErrors = data;
                    else
                        $scope.nrOfErrors = 0;

                    promise = $timeout(tick, 3000);
                }, function () {
                    promise = $timeout(tick, 3000);
                });
            }
        })();
    }
]);