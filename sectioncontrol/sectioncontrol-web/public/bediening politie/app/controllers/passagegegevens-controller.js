app.controller('passagegegevensController', ['$scope', '$stateParams', '$state','ApplicationCache','RoleService',
    'PassagegegevensService','config','$log','$window','$timeout','$interval',
    function($scope, $stateParams, $state,
             applicationCache,
             roleService,
             passagegegevensService,
             config,
             $log,
             $window,
             $timeout,
             $interval) {
        $log.debug("In passagegegevensController");

        $scope.errors=[];
        $scope.hasResult=true;

        var traject=applicationCache.get('traject');
        var now = new Date();// is nu utc
        var localDate;

        localDate       = new Date(now.getFullYear(), now.getMonth(), now.getDate(),0,0,0);
        $log.debug(localDate);
        $scope.passagegegevensBeginDatum= new Date(now.getFullYear(), now.getMonth(), now.getDate(),0,0,0);
        localDate       = new Date(now.getFullYear(), now.getMonth(), now.getDate(),23,59,59);
        $log.debug(localDate);
        $scope.passagegegevensEindDatum = new Date(now.getFullYear(), now.getMonth(), now.getDate(),23,59,59);

        $scope.buttonDisabledPassagegegevens=false;
        $scope.gantries=traject.gantries;

        function getCookieValue(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');

            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1);
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }

        $scope.guid="";
        var timer;

        $scope.cookieChecker = function () {
            timer = $interval(function () {
                var cookie = getCookieValue("id");
                if (cookie != "") {
                    if (getCookieValue("id") == $scope.guid) {
                        document.cookie = "id=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
                        $scope.buttonDisabledPassagegegevens = false;
                        $scope.guid="";
                        if(angular.isDefined(timer))
                        {
                            $log.debug("$destroy");
                            $interval.cancel(timer);
                            timer=undefined;
                        }
                    }
                }
            }, 100);
        };

        $scope.$on('$destroy', function(){

            if(angular.isDefined(timer))
            {
                $log.debug("$destroy");
                $interval.cancel(timer);
                timer=undefined;
            }
        });

        $scope.$watchCollection('[passagegegevensBeginDatum,passagegegevensEindDatum,begintijd,eindtijd,metFoto]', function(newValues, oldValues) {
            $log.debug("In de watch");
            $log.debug(newValues);
            $log.debug(oldValues);
            $scope.errors=[];
            $scope.error=[];
            $scope.hasError=false;

            var beginHour="00";
            var beginMinute="00";
            var endHour="00";
            var endMinute="00";

            if(newValues[4]==undefined)newValues[4]=false;

            if(newValues[2]!=undefined)
            {
                beginHour=newValues[2].substring(0,2);
                beginMinute=newValues[2].substring(3,5);
            }
            if(newValues[3]!=undefined) {
                endHour = newValues[3].substring(0, 2);
                endMinute = newValues[3].substring(3, 5);
            }

            var begin=new Date(newValues[0].getFullYear(),newValues[0].getMonth(),newValues[0].getDate(),beginHour,beginMinute);
            var end=new Date(newValues[1].getFullYear(),newValues[1].getMonth(),newValues[1].getDate(),endHour,endMinute);

            var epochBegin=begin.getTime()/1000.0;
            var epochEnd=end.getTime()/1000.0;

            if(epochEnd-epochBegin<0)
            {
                $scope.hasError=true;
                $scope.errorMsg="Het is niet mogelijk een einddatum te selecteren die voor de begindatum ligt.";
                $scope.error.push("Geen geldige begintijd ingevoerd. Vul een tijdstip in volgens het formaat uu:mm");
                $scope.buttonDisabledPassagegegevens=true;
            }
            else if(newValues[4] && epochEnd-epochBegin>1800)
            {
                $scope.hasError=true;
                $scope.errorMsg="Tijdvenster is te lang. Voor zoekopdrachten in combinatie met foto kan een tijdvenster van maximaal 30 minuten gebruikt worden.";
                $scope.error.push("Tijdvenster is te lang. Voor zoekopdrachten in combinatie met foto kan een tijdvenster van maximaal 30 minuten gebruikt worden.");

                $scope.buttonDisabledPassagegegevens=true;
            }
            else if(!newValues[4] && epochEnd-epochBegin>86400)
            {
                $scope.hasError=true;
                $scope.errorMsg="Tijdvenster is te lang. Voor zoekopdrachten zonder foto kan een tijdvenster van maximaal 24 uur gebruikt worden.";
                $scope.error.push("Tijdvenster is te lang. Voor zoekopdrachten zonder foto kan een tijdvenster van maximaal 24 uur gebruikt worden.");

                $scope.buttonDisabledPassagegegevens=true;
            }
            else
            {
                $scope.hasError=false;
                $scope.errorMsg="";
                $scope.buttonDisabledPassagegegevens=false;
            }

            if(!$scope.$$phase) {
                $scope.$apply();
            }

        });

        $scope.maxEndDate=new Date(new Date($scope.passagegegevensBeginDatum).setMonth($scope.passagegegevensBeginDatum.getMonth()+1));
        $scope.minEndDate=$scope.passagegegevensBeginDatum;

        $scope.isEditDisabledForUser=function(){
            return !roleService.isInRolePolitie();
        };

        function getSearchData() {
            var beginHour=$scope.begintijd.substring(0,2);
            var beginMinute=$scope.begintijd.substring(3,5);
            var endHour=$scope.eindtijd.substring(0,2);
            var endMinute=$scope.eindtijd.substring(3,5);
            var begin=new Date($scope.passagegegevensBeginDatum.getFullYear(),$scope.passagegegevensBeginDatum.getMonth(),$scope.passagegegevensBeginDatum.getDate(),beginHour,beginMinute);
            var end=new Date($scope.passagegegevensEindDatum.getFullYear(),$scope.passagegegevensEindDatum.getMonth(),$scope.passagegegevensEindDatum.getDate(),endHour,endMinute);

            var epochBegin=begin.getTime()/1000.0;
            var epochEnd=end.getTime()/1000.0;

            var searchData={systemId:traject.systemId,
                gantryId:$scope.Passagepunt.id,
                fromEpoch:epochBegin,
                toEpoch:epochEnd,
                withPhotos:$scope.metFoto==undefined || $scope.metFoto==false?0:1,
                kenteken:$scope.kenteken==undefined?"": $scope.kenteken};

            return searchData;
        }

        $scope.getAdresForDownload=function(){
            if($scope.Passagepunt==undefined)return;

            var searchData = getSearchData();
            $scope.guid=guid();
            var downloadUrl= config.HTTP_DOWNLOAD_PASSAGEGEGEVENS.format(config.HTTP_BASE_URL,searchData.systemId,searchData.gantryId, searchData.fromEpoch,searchData.toEpoch,searchData.withPhotos,searchData.kenteken,$scope.guid);

            return downloadUrl;
        };

        $scope.downloadPassagegegevens=function(){
            if ($scope.Passagepunt==undefined) return;
            $scope.buttonDisabledPassagegegevens = true;

            var searchData = getSearchData();

            if (searchData.withPhotos) {
                // First download images
                var downloadImagesPromise = passagegegevensService.downloadImagesForPassageGegevens(searchData)
                downloadImagesPromise.then(function(data) {
                    if (data.refId) {
                        $log.debug("Download was needed, now polling until done")
                        $scope.hasResult = true;
                        startPollingForDownloadStatus(data.refId, searchData);
                    } else { // No need to download images
                        // Not the most efficient flow, could be improved by having the image download check also check
                        // and return if the search yielded results in the first place
                        $log.debug("No download for images was needed, now checking if there is data available");
                        checkIfThereIsDataAvailableAndDownload(searchData);
                    }
                }, function(reason) {
                    $log.debug("Downloading images failed...")
                    $log.debug(reason)
                });
            } else {
                $log.debug("Performing search without images")
                checkIfThereIsDataAvailableAndDownload(searchData);
            }

            function startPollingForDownloadStatus(refId, searchData) {
                var timer;
                var intervalDuration = 1000; // in millis

                function done() {
                    $interval.cancel(timer);
                    checkIfThereIsDataAvailableAndDownload(searchData);
                }

                function error() {
                    $interval.cancel(timer);
                    $timeout(function () {
                        $log.debug('Something went wrong during polling for download status. Aborting.');
                    }, 50);
                }

                function httpError(error) {
                    $interval.cancel(timer);
                    $timeout(function () {
                        $log.debug("HTTP error when checking for download status: " + error);
                    }, 50);
                }

                timer = $interval(function () {
                    checkDownloadStatus(refId, done, error, httpError)
                }, intervalDuration);
            }

            function checkDownloadStatus(refId, doneFunc, errorFunc, httpErrorFunc) {
                $log.debug("Checking download status for refId: " + refId);
                var promise = passagegegevensService.checkDownloadStatusForImagesForPassageGegevens(refId);
                promise.then(function(response) {
                    switch (response.status) {
                        case 'IN_PROGRESS':
                            // Downloading of images still in progress, keep polling
                            break;
                        case 'COMPLETED':
                            doneFunc();
                            break;
                        case 'ERROR':
                            errorFunc();
                            break;
                    }
                }, function(error) {
                    httpErrorFunc(error);
                });
            }

            function checkIfThereIsDataAvailableAndDownload(searchData) {
                $log.debug("Checking if there is data available for download");
                var passagepuntenPromise = passagegegevensService.passagegegevensAanwezig(searchData);
                passagepuntenPromise.then(function (data) {
                    if(data.result) {
                        $scope.hasResult = true;
                        $scope.hasResultMsg = "";
                        $scope.cookieChecker();
                        $window.location.href = $scope.getAdresForDownload();
                    } else {
                        $scope.hasResult = false;
                        if(searchData.kenteken == "") {
                            $scope.hasResultMsg = "Geen gegevens gevonden.";
                        } else {
                            $scope.hasResultMsg = "Kenteken niet gevonden.";
                        }
                        $scope.buttonDisabledPassagegegevens = false;
                    }
                }, function (error) {
                    $log.debug('Something went wrong checking if passagegegevens are available: ' + error);
                });
            }
        };
    }
]);
