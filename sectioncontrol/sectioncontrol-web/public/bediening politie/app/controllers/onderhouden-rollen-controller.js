/**
 * Created by CSC on 10/29/14.
 */
app.controller('onderhoudenRollenController', ['$scope', 'loginService', 'ApplicationCache', 'RoleService', '$stateParams', '$state', 'OnderhoudenRollenService','$log',
    function($scope, loginService, applicationCache, roleService, $stateParams, $state, onderhoudenRollenService,$log) {
        $log.debug("IN onderhoudenRollenController");

        $scope.isbtnDisabled = false;
        $scope.showDetails = false;

        var promise = onderhoudenRollenService.getAllRoles();
        promise.then(function(data) {
            $scope.rollen = data;
            $log.debug("roles");
            $log.debug(data);
        }, function(data) {
            applicationCache.put("error", data);
            $state.go("error");
        });

        var actionPromise = onderhoudenRollenService.getAllActions();
        actionPromise.then(function(data) {
            $scope.permittedActions = data;
            $log.debug("actions");
            $log.debug(data);
        }, function(data) {
            applicationCache.put("error", data);
            $state.go("error");
        });

        $scope.disableLinks = function() {
            $scope.isbtnDisabled = true;
        };

        $scope.enableLinks = function() {
            $scope.isbtnDisabled = false;
        };

        $scope.doShowDetails = function() {
            $scope.showDetails = true;
        };

        $scope.hideDetails = function() {
            $scope.showDetails = false;
        };

        $scope.wijzigen = function(id) {
            $scope.doShowDetails();
            $scope.disableLinks();

            var promise = onderhoudenRollenService.getAllActions();
            promise.then(function(data) {
                $scope.permittedActions = data;
                $log.debug("actions");
                $log.debug(data);

                var promise1 = onderhoudenRollenService.getRole(id);
                promise1.then(function(data) {
                    $scope.rol = data;
                    $log.debug(data);
                }, function(reason) {
                    $log.debug(reason);
                });
            }, function(reason) {
                $log.debug(reason);
            });
        };

        $scope.bewaar = function() {

            var permittedActions = [];
            $log.debug(permittedActions);

            var updateModel = {
                id: $scope.rol.id,
                name: $scope.rol.name,
                description: $scope.rol.description,
                version: $scope.rol.version,
                actionIds: $scope.rol.actionIds
            };

            $log.debug(updateModel);

            var promise = onderhoudenRollenService.saveRole(updateModel);
            promise.then(function() {
                $scope.hideDetails();
                $scope.enableLinks();
            }, function(data) {
                applicationCache.put("error", data);
                $state.go("error");
            });

        };

        $scope.isInRolePolitie1 = function() {
            return roleService.isInRolePolitie();
        }
    }
]);
