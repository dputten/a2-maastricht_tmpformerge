/**
 * Created by CSC on 10/6/14.
 */
app.controller('errorController', ['$scope', '$stateParams', '$state','ApplicationCache','$log',
    function($scope, $stateParams, $state,applicationCache,$log) {
        $scope.error= "Er heeft zich een onverwachte fout voorgedaan, probeer het opnieuw.";
        $log.debug(applicationCache.get("error"));
      }
]);