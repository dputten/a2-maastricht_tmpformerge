/**
 * Created by CSC on 10/29/14.
 */
app.controller('onderhoudenTechnischController', ['$scope','loginService','ApplicationCache','RoleService','$stateParams', '$state',
    function($scope,loginService,applicationCache,roleService,$stateParams, $state) {
        $scope.isInRolePolitie1=function(){
            return roleService.isInRolePolitie();
        }
    }
]);