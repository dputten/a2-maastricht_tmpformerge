/**
 * Created by CSC on 10/29/14.
 */
app.controller('onderhoudenVerdragslandenController', ['$scope', 'loginService', 'ApplicationCache', 'RoleService',
    'VerdragslandenService', '$stateParams', '$state','$log','isSystemEditable',
    function($scope, loginService, applicationCache, roleService, verdragslandenService, $stateParams,$state,$log,isSystemEditable) {
        $log.debug("IN onderhoudenVerdragslandenController");

        $scope.isLinkDisabled = false;
        $scope.showDetails = false;

        var promise = verdragslandenService.getAllVerdragslanden();
        promise.then(function(data) {
            $scope.verdragslanden = data;
            $log.debug("verdragslanden");
            $log.debug(data);
        }, function(reason) {
            $log.debug(reason);
        });

        $scope.isEditDisabledForUser=function(){ return !roleService.isInRoleTactischBeheer() || isSystemEditable==false;};

        $scope.isInRolePolitie1 = function() {
            return roleService.isInRolePolitie();
        };

        $scope.disableLinks = function() {
            $scope.isLinkDisabled = true;
        };

        $scope.enableLinks = function() {
            $scope.isLinkDisabled = false;
        };

        $scope.doShowDetails = function() {
            $scope.showDetails = true;
        };

        $scope.bewaar = function() {

            var somethingChanged = false;

            var result = $scope.classifyCountries.mobiCountries.find(function(e) {
                return e == $scope.verdragsland.isoCode
            });

            // Moet actief worden.
            if (result == undefined && $scope.verdragsland.actief) {
                $scope.classifyCountries.mobiCountries.push($scope.verdragsland.isoCode);
                somethingChanged = true;
            } else {
                // Is actief maar moet niet actief worden.
                if ($scope.verdragsland.actief == false) {
                    var index = $scope.classifyCountries.mobiCountries.indexOf($scope.verdragsland.isoCode);
                    if (index > -1) {
                        $scope.classifyCountries.mobiCountries.splice(index, 1);
                        somethingChanged = true;
                    }
                }
            }

            if (somethingChanged) {
                var promise = verdragslandenService.putClassifyLanden($scope.classifyCountries);
                promise.then(function(data) {

                    if (data.success) {

                        var promiseGet = verdragslandenService.getAllVerdragslanden();
                        promiseGet.then(function(data) {
                            $scope.verdragslanden = data;
                            $log.debug(data);
                        }, function(reason) {
                            $log.debug(reason);
                        });

                        $scope.hideDetails();
                        $scope.enableLinks();
                    } else {
                        if (data.fields !== undefined && data.fields.length > 0) {
                            $scope.errors = data.fields;
                            $log.debug($scope.errors);
                        } else {
                            var x = {
                                field: "",
                                error: data.description
                            };
                            $log.debug(x);
                            $scope.errors = [];
                            $scope.errors.push(x);
                            $log.debug($scope.errors);
                        }
                    }
                }, function(reason) {
                    $log.debug(reason);
                });
            }
            else
            {
                $scope.hideDetails();
                $scope.enableLinks();
            }

            $log.debug($scope.classifyCountries);

        };

        $scope.wijzig = function(isoCode) {
            $scope.doShowDetails();
            $scope.disableLinks();

            var promise = verdragslandenService.getClassifyLanden();
            promise.then(function(data) {
                $scope.classifyCountries = data;
                var verdragsland = {
                    isoCode: isoCode,
                    actief: false
                };

                var result = data.mobiCountries.find(function(e) {
                    return e == isoCode
                });

                verdragsland.actief = result != undefined;

                $scope.verdragsland = verdragsland;
                $log.debug(data);
            }, function(reason) {
                $log.debug(reason);
            });

        };

        $scope.hideDetails = function() {
            $scope.showDetails = false;
        };

        $scope.isInRolePolitie1 = function() {
            return roleService.isInRolePolitie();

        };
    }
]);