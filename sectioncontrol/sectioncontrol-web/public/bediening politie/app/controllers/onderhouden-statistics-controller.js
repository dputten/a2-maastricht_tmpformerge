/**
 * Created by CSC on 10/29/14.
 */
app.controller('onderhoudenStatisticsController', ['$scope','loginService','ApplicationCache','RoleService','trajectService','StatisticsService','$state','$log','config','statistics',
    function($scope,loginService,applicationCache,roleService,trajectService,statisticsService,$state,$log,config,statistics) {
        $scope.isbtnDisabled = false;
        $scope.showDetails = false;

        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.maxSize = 10;
        $scope.itemsPerPage = 10;
        $scope.totalItems=0;

        $log.debug(statistics);
        $scope.statistics=statistics;
        $scope.totalItems=statistics.length;

        var begin = (($scope.currentPage - 1) * $scope.itemsPerPage)
            , end = begin + $scope.itemsPerPage;
        $scope.filteredStatistics = $scope.statistics.slice(begin, end);
        $scope.traject = applicationCache.get('traject');

        $scope.adresStatistiek=function(statisticId){
            return config.HTTP_BASE_URL+ config.HTTP_GET_DOWNLOAD_STATISTIC_TRAJECT_URL.format($scope.traject.systemId,statisticId)
        };

        $scope.pageChanged = function() {
            $log.log('Page changed to: ' + $scope.currentPage);
            $scope.sliceData();
            $log.debug($scope.totalItems);
            $log.debug($scope.currentPage );
            $log.debug($scope.maxSize );
            $log.debug($scope.itemsPerPage);
        };

        $scope.sliceData=function(){
            var begin = (($scope.currentPage - 1) * $scope.itemsPerPage)
                , end = begin + $scope.itemsPerPage;
            $scope.filteredStatistics = $scope.statistics.slice(begin, end);
        };

        $scope.disableLinks = function() {
            $scope.isbtnDisabled = true;
        };

        $scope.enableLinks = function() {
            $scope.isbtnDisabled = false;
        };

        $scope.doShowDetails = function(){
            $scope.showDetails = true;
        };

        $scope.hideDetails = function(){
            $scope.showDetails = false;
        };

        $scope.isInRolePolitie1=function(){
            return roleService.isInRolePolitie();
        };

        $scope.wijzigen=function(reportId){
            $scope.doShowDetails();
            $scope.disableLinks();

            var promise = statisticsService.getStatisticsReportForSystem($scope.traject.systemId,reportId);
            promise.then(function (data) {
                $scope.statisticsReport = fillData(data);
            }, function (reason) {
                $log.debug(reason);
            });

        };
    }
]);

//function fillData(corridors,$log){
function fillData(xml){
    var data = [];

    var corridors = $.parseXML(xml);
    $(corridors).find('HH-traject').each(function(counter){
        var corridor = $(this);
        var id = corridor.attr('id');
        var index = corridor.attr('index');

        corridor.find('TC-statistiek').each(function(){
            var stats = $(this);
            stats.find('passages').each(function(){
                var lanes = $(this);
                var total_limit_in = lanes.attr('totaal-limietoverschrijdingen-in');
                var total_limit_out = lanes.attr('totaal-limietoverschrijdingen-uit');
                var total_in = lanes.attr('totaal-in');
                var total_out = lanes.attr('totaal-uit');

                if(total_limit_in != undefined)
                    data.push({a:index, b:1, c:id, d:"totaal", e:"Limietoverschrijdingen In",f:total_limit_in});
                if(total_limit_out != undefined)
                    data.push({a:index, b:1, c:id, d:"totaal", e:"Limietoverschrijdingen Uit", f:total_limit_out});
                if(total_in != undefined)
                    data.push({a:index, b:1, c:id, d:"totaal", e:"Totaal In", f:total_in});
                if(total_out != undefined)
                    data.push({a:index, b:1, c:id, d:"totaal",e: "Totaal Uit", f:total_out});

                lanes.find('strook').each(function(){
                    var lane = $(this);
                    var gantry = lane.attr('passage');
                    var location = lane.attr('locatie');
                    var total = lane.text();
                    var key = gantry + ":" + location;

                    if(gantry != undefined && location != undefined )
                        data.push({a:index, b:2, c:id, d:"passage", e:key, f:total});
                });

                var average_speed =  stats.find('snelheden > gemiddeld');
                if(average_speed != undefined ){
                    var average_speed_unit = average_speed.attr('eenheid');
                    var average_speed_speed = average_speed.text();
                    var key = "gemiddeld" + " (" + average_speed_unit + ")";

                    data.push({a:index, b:3, c:id, d:"snelheid", e:key, f:average_speed_speed});
                }


                var max_speed = stats.find('snelheden > max');
                if(max_speed != undefined){
                    var max_speed_unit = max_speed.attr('eenheid');
                    var max_speed_speed = max_speed.text();
                    var key = "max" + " (" + max_speed_unit + ")";

                    data.push({a:index, b:4, c:id, d:"snelheid", e:key, f:max_speed_speed});
                }

//                var violations_total =  stats.find('overtredingen > overtredingen-totaal').text();
                var violations_hand =  stats.find('overtredingen > hand').text();
                var violations_car =  stats.find('overtredingen > auto').text();
                var violations_mobi =  stats.find('overtredingen > mobi').text();
                var violations_hand_conform_spec =  stats.find('overtredingen > hand-conform-specificatie').text();
                var violations_mobi_not_processed =  stats.find('overtredingen > mobi-niet-verwerken').text();
                var violations_mtm_ratio =  stats.find('overtredingen > MTMratio').text();
                var violations_mtm_pardon =  stats.find('overtredingen > MTM-pardon').text();
                var violations_double_violations_pardon =  stats.find('overtredingen > Dubbele-overtredingen-pardon').text();
                var violations_other_pardon =  stats.find('overtredingen > Overig-pardon').text();

                if(violations_hand != undefined)
                    data.push({a:index, b:5, c:id, d:"Overtredingen", e:"hand", f:violations_hand});
                if(violations_car != undefined)
                    data.push({a:index, b:5, c:id, d:"Overtredingen", e:"auto", f:violations_car});
                if(violations_mobi != undefined)
                    data.push({a:index, b:5, c:id, d:"Overtredingen", e:"mobi", f:violations_mobi});
                if(violations_hand_conform_spec != undefined)
                    data.push({a:index, b:5, c:id, d:"Overtredingen", e:"hand conform specificatie", f:violations_hand_conform_spec});
                if(violations_mobi_not_processed != undefined)
                    data.push({a:index, b:5, c:id, d:"Overtredingen", e:"mobi niet verwerken", f:violations_mobi_not_processed});
                if(violations_mtm_ratio != undefined)
                    data.push({a:index, b:5, c:id, d:"Overtredingen", e:"MTM ratio", f:violations_mtm_ratio});
                if(violations_mtm_pardon != undefined)
                    data.push({a:index, b:5, c:id, d:"Overtredingen", e:"MTM pardon", f:violations_mtm_pardon});
                if(violations_double_violations_pardon != undefined)
                    data.push({a:index, b:5, c:id, d:"Overtredingen", e:"Dubbele overtredingen pardon", f:violations_double_violations_pardon});
                if(violations_other_pardon != undefined)
                    data.push({a:index, b:5, c:id, d:"Overtredingen", e:"Overig pardon", f:violations_other_pardon});

                var performance_match_ratio =  stats.find('performance > matchratio').text();
                var performance_violations_ratio =  stats.find('performance > overtredingenratio').text();
                var performance_auto_ratio =  stats.find('performance > autoratio').text();
                var performance_auto_ratio_net =  stats.find('performance > autoratio-netto').text();
                var performance_hh_ratio =  stats.find('performance > handhaafratio').text();
                var performance_faults_ratio =  stats.find('performance > storingenratio').text();
                var performance_av_t_ratio =  stats.find('performance > av-t-ratio').text();
                var performance_avl_f_ratio =  stats.find('performance > avl-f-ratio').text();

                if(performance_match_ratio != undefined)
                    data.push({a:index, b:6, c:id, d:"Performance", e:"match ratio", f:performance_match_ratio});
                if(performance_violations_ratio != undefined)
                    data.push({a:index, b:6, c:id, d:"Performance", e:"overtredingen ratio", f:performance_violations_ratio});
                if(performance_auto_ratio != undefined)
                    data.push({a:index, b:6, c:id, d:"Performance", e:"auto ratio", f:performance_auto_ratio});
                if(performance_auto_ratio_net != undefined)
                    data.push({a:index, b:6, c:id, d:"Performance", e:"auto ratio netto", f:performance_auto_ratio_net});
                if(performance_hh_ratio != undefined)
                    data.push({a:index, b:6, c:id, d:"Performance", e:"handhaafratio", f:performance_hh_ratio});
                if(performance_faults_ratio != undefined)
                    data.push({a:index, b:6, c:id, d:"Performance", e:"storingenratio", f:performance_faults_ratio});
                if(performance_av_t_ratio != undefined)
                    data.push({a:index, b:6, c:id, d:"Performance", e:"av-t-ratio", f:performance_av_t_ratio});
                if(performance_avl_f_ratio != undefined)
                    data.push({a:index, b:6, c:id, d:"Performance", e:"avl-f-ratio", f:performance_avl_f_ratio});

                var matches =  stats.find('matches').text();
                if(matches != undefined)
                    data.push({a:index, b:7, c:id, d:"Matches", e:"matches", f:matches});

            });
        });

        data.push([index, 8, "&nbsp;","&nbsp;","&nbsp;","&nbsp;"]);

    });
    return data;
}
