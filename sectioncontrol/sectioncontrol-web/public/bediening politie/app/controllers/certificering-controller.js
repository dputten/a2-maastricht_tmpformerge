/**
 * Created by CSC on 10/6/14.
 */
app.controller('certificeringController', ['$scope', '$stateParams', '$state','CertificeringService','ApplicationCache',
    function($scope, $stateParams, $state,certificeringService,applicationCache) {
        var traject=applicationCache.get('traject');
        $scope.certificaten=certificeringService.getCertificeringForTraject(traject.systemId);
    }
]);