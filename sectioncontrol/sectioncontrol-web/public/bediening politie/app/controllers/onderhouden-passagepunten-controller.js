/**
 * Created by CSC on 12/5/14.
 */
/**
 * Created by CSC on 10/29/14.
 */
app.controller('onderhoudenPassagepuntenController', ['$scope','loginService','ApplicationCache','RoleService','$stateParams', 
    '$state','trajectService','PassagepuntenService','$log','isSystemEditable','$modal','modalService',
    function($scope,loginService,applicationCache,roleService,$stateParams, $state,trajectService,passagepuntenService,$log,isSystemEditable,$modal,modalService) {
        $scope.isbtnDisabled = false;
        $scope.showDetails = false;
        $scope.isAddnew = false;
        $scope.errors=[];
        $scope.traject = applicationCache.get('traject');
        $scope.systemId=$scope.traject.systemId;

        $scope.isEditDisabledForUser=function(){
            return !roleService.isInRoleTechnischBeheer() || isSystemEditable==false;};

        var passagepuntenPromise = passagepuntenService.getGantries($scope.systemId);
        passagepuntenPromise.then(function (data) {
            $scope.gantries = data;
        }, function (reason) {
            $log.debug(reason);
        });

        $scope.disableLinks = function() {
            $scope.isbtnDisabled = true;
        };

        $scope.enableLinks = function() {
            $scope.isbtnDisabled = false;
        };

        $scope.disableButtons = function() {
            $scope.isbtnDisabled2 = true;
        };

        $scope.enableButtons = function() {
            $scope.isbtnDisabled2 = false;
        };

        $scope.doShowDetails = function(){
            $scope.showDetails = true;
        };

        $scope.hideDetails = function(){
            $scope.showDetails = false;
        };

        $scope.isInRolePolitie1=function(){
            return roleService.isInRolePolitie();
        };

        $scope.bewaar=function(gantryId){
            saveGantry($scope,passagepuntenService,$scope.systemId,gantryId,$log);
        };

        $scope.wijzigen=function(systemId,gantryId){
            var promise = passagepuntenService.getGantry(systemId,gantryId);
            promise.then(function (data) {
                $scope.gantry = data;
                $scope.errors=[];
                $log.debug( data);
                $scope.doShowDetails();
                $scope.disableLinks();
            }, function (data) {
                applicationCache.put("error",data);
                $state.go("error");
            });
        };

        $scope.annuleer=function()
        {
            $scope.gantry = null;
            $scope.hideDetails();
            $scope.enableLinks();
        };

        $scope.deleteGantry=function(systemId,gantryId,version)
        {
            var modalOptions = {
            closeButtonText: 'Nee',
            actionButtonText: 'Ja',
            headerText: 'Verwijderen?',
            bodyText: 'Weet u zeker dat u de geselecteerde gegevens wilt verwijderen? De gekoppelde rijstroken worden ook verwijderd.'
            };

            modalService.showModal({}, modalOptions).then(function (result) {
                var promise = passagepuntenService.deleteGantry(systemId, gantryId, version);
                promise.then(function (data) {
                    if (data.success) {
                        var passagepuntenPromise = passagepuntenService.getGantries($scope.systemId);
                        passagepuntenPromise.then(function (data) {
                            $scope.gantries = data;
                        }, function (reason) {
                            $log.debug(reason);
                        });
                    }
                    else {
                        if (data.fields !== undefined && data.fields.length > 0) {
                            $scope.errors = data.fields;
                            $log.debug($scope.errors);
                        }
                        else {
                            var x = {field: "", error: data.description};
                            $log.debug(x);
                            $scope.errors = [];
                            $scope.errors.push(x);
                            $log.debug($scope.errors);
                        }
                    }
                }, function (reason) {
                    $log.debug(reason);
                });
            });
        };

        $scope.addnew = function(){
            $scope.showDetails = true;
            $scope.isAddnew = true;
            $scope.disableLinks();
            $scope.errors=[];
            $scope.gantry={
                id:"",
                systemId:"",
                name:"",
                hectometer:"",
                longitude:"",
                latitude:""
            };

        };
    }
]);

function saveGantry($scope,passagepuntenService,systemId,gantryId,$log)
{
    $log.debug($scope.gantry);
    var promise;
    if($scope.isAddnew ){
        promise = passagepuntenService.postGantry($scope.systemId ,$scope.gantry);
    }
    else {
        promise = passagepuntenService.putGantry(systemId,gantryId, $scope.gantry);
    }

    promise.then(function (data) {
        if(data.success){
            var passagepuntenPromise = passagepuntenService.getGantries(systemId);
            passagepuntenPromise.then(function (data) {
                $scope.gantries = data;
                $scope.gantry=null;
                $scope.isAddnew = false;
                $scope.hideDetails();
                $scope.enableLinks();
            }, function (reason) {
                $log.debug(reason);
            });
        }
        else
        {
            if(data.fields!==undefined && data.fields.length>0)
            {
                $scope.errors=data.fields;
                $log.debug($scope.errors);
            }
            else
            {
                var x={field:"",error:data.description};
                $log.debug(x);
                $scope.errors=[];
                $scope.errors.push(x);
                $log.debug($scope.errors);
            }
        }
    }, function (reason) {
        $log.debug(reason);
    });
}


