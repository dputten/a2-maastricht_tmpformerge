//Todo:JE-> als de rol van de gebruiker wijzigt dan deze opnieuw op de cache zetten

app.controller('onderhoudenGebruikersController', ['$scope','loginService','ApplicationCache',
    'RoleService','$stateParams', '$state','OnderhoudenGebruikersService','$log',
    function($scope,loginService,applicationCache,roleService,$stateParams, $state,onderhoudenGebruikersService,$log) {
        $log.debug("IN onderhoudenGebruikersController ");

        $scope.mySwitch=false;
        $scope.isAddnew = false;
        $scope.isbtnDisabled = false;
        $scope.showDetails = false;
        $scope.errors=[];
        $scope.loggedInUser=applicationCache.get('gebruiker');
        $scope.traject = applicationCache.get('traject');
        $scope.systemId=$scope.traject.systemId;

        getAllGebruikers($scope,onderhoudenGebruikersService,$log);
        getAllRoles1($scope,onderhoudenGebruikersService,$log);

        $scope.disableLinks = function() {$scope.isbtnDisabled = true;};

        $scope.isEditDisabledForUser=function(){
            return roleService.isInRoleTactischBeheer()==false;
        };

        $scope.showDeleteButton=function(id){
            return roleService.isInRoleTactischBeheer() && $scope.loggedInUser.id!=id;
        };

        $scope.$watch('mySwitch', function(newValue, oldValue) {
            if(newValue==false && $scope.user!=undefined)
                $scope.user.password="";

            if(!$scope.$$phase) {
                $scope.$apply();
            }
        });

        $scope.$watch('user.password', function(newValue, oldValue) {
            $scope.$digest;
            if(!$scope.$$phase) {
                $scope.$apply();
            }
        });

        $scope.$watch('user.username', function(newValue, oldValue) {
            if(!$scope.$$phase) {
                $scope.$apply();
            }
        });

        $scope.roleIsPolitie=function(){
            if($scope.user!=undefined) {
                var role = $scope.roles.find(function (e) {
                    return e.id == $scope.user.roleId
                });

                if (role != undefined && role.name == "Politie") {
                    return true;
                }
            }

            return false;
        };

        $scope.enableLinks = function() {
            $scope.isbtnDisabled = false;
        };

        $scope.annuleren = function() {
            $scope.user=null;
            $scope.isAddnew=false;
            $scope.hideDetailsGebruikers();
            $scope.enableLinks();
            $scope.errors=[];
            $scope.mySwitch=false;

        };

        $scope.doShowDetailsUser = function(id){
            $log.debug("ID" + id);
            $scope.disableLinks();
            getGebruiker($scope,onderhoudenGebruikersService,id,$log);

        };

        $scope.addnew = function(){
            $scope.mySwitch=true;
            $scope.showDetails = true;
            $scope.isAddnew = true;
            $scope.disableLinks();
            $scope.user=null;
            $scope.errors=[];
        };

        $scope.deleteGebruiker = function(id){
            $scope.disableLinks();
            deleteGebruiker($scope,onderhoudenGebruikersService,id,$log);

        };

        $scope.saveGebruiker = function(){
            $log.debug($scope.user);
            saveGebruiker($scope,onderhoudenGebruikersService,$log);
        };

        $scope.hideDetailsGebruikers = function(){
            $scope.showDetails = false;
        };


        $scope.isInRolePolitie1=function(){
            $log.debug(roleService.isInRolePolitie());
            return roleService.isInRolePolitie();
        }
    }
]);

function saveGebruiker($scope,onderhoudenGebruikersService,$log)
{
    var reportingOfficerId;
    if($scope.user.reportingOfficerId==undefined || $scope.user.reportingOfficerId=="" || $scope.user.reportingOfficerId.trim=="")
        reportingOfficerId="";
    else
        reportingOfficerId=$scope.user.reportingOfficerId;

    var model={
        id:$scope.user.id==undefined?"":$scope.user.id,
        reportingOfficerId:reportingOfficerId,
        name:$scope.user.name,
        email:$scope.user.email,
        phone:$scope.user.phone,
        ipAddress:$scope.user.ipAddress==undefined?"":$scope.user.ipAddress,
        username:$scope.user.username,
        password:$scope.user.password,
        roleId:$scope.user.roleId,
        sectionIds:[$scope.systemId],
        changePassword:$scope.mySwitch,
        version:$scope.user.version==undefined?0:$scope.user.version};

    var promise;
    if(!$scope.isAddnew )
        promise = onderhoudenGebruikersService.putGebruiker(model);
    else {
        promise = onderhoudenGebruikersService.postGebruiker(model);
    }

    promise.then(function (data) {
        if(data.success){
            $scope.user=null;
            $scope.isAddnew=false;
            $scope.hideDetailsGebruikers();
            $scope.enableLinks();
            $scope.errors=null;
            $scope.mySwitch=false;

            var promiseGetAll = onderhoudenGebruikersService.getAllGebruikers();
            promiseGetAll.then(function (data) {
                $scope.users = data;

            }, function (reason) {
                $log.debug(reason);
            });
        }
        else
        {
            if(data.fields!==undefined && data.fields.length>0)
            {
                $scope.errors=data.fields;
                $log.debug($scope.errors);
            }
            else
            {
                var x={field:"",error:data.description};
                $log.debug(x);
                $scope.errors=[];
                $scope.errors.push(x);
                $log.debug($scope.errors);
            }
        }
    }, function (reason) {
        $log.debug(reason);
    });
}

//https://egghead.io/lessons/angularjs-chained-promises
function deleteGebruiker($scope,onderhoudenGebruikersService,id,$log){
    var promiseGebruiker = onderhoudenGebruikersService.getGebruiker(id);
    promiseGebruiker.then(function (data) {
        var user = data;
        var promiseDelete = onderhoudenGebruikersService.deleteGebruiker(user.id,user.version);
        promiseDelete.then(function (data) {
            if(data.success) {
                $scope.isAddnew = false;
                $scope.hideDetailsGebruikers();
                $scope.enableLinks();
                $scope.errors=[];
                var promiseGetAll = onderhoudenGebruikersService.getAllGebruikers();
                promiseGetAll.then(function (data) {
                    $scope.users = data;

                }, function (reason) {
                    $log.debug(reason);
                });
            }
        }, function (reason) {
            $log.debug(reason);
        });


    }, function (reason) {
        $log.debug(reason);
    });
}

function getGebruiker($scope,onderhoudenGebruikersService,id,$log){
    var promise = onderhoudenGebruikersService.getGebruiker(id);
    promise.then(function (data) {
        $scope.user = null;
        $scope.user = data;
        $scope.user.changePassword=false;
        $scope.showDetails = true;
        $scope.errors=[];
        $log.debug( "rol");
        $log.debug( $scope.user.roleId);
        $log.debug(data);
        $log.debug($scope.roles);
        $log.debug($scope.systems);
    }, function (reason) {
        $log.debug(reason);
    });
}

function getAllGebruikers($scope,onderhoudenGebruikersService,$log){
    var promise = onderhoudenGebruikersService.getAllGebruikers();
    promise.then(function (data) {
        $scope.users = data;
    }, function (reason) {
        $log.debug(reason);
    });
}

function getAllRoles1($scope,onderhoudenGebruikersService,$log){
    var promise = onderhoudenGebruikersService.getAllRoles();
    promise.then(function (data) {
        $scope.roles = data;
    }, function (reason) {
        $log.debug(reason);
    });
}
