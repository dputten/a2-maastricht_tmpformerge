app.controller('onderhoudenCorridorsController',
    ['$scope','loginService','ApplicationCache','RoleService',
        'HandhavingService','InstellingenService','$state','$log','PassagepuntenService','isSystemEditable','InstellingenServiceSectieSoortWeg',
    function($scope,loginService,applicationCache,roleService,handhavingService,
             instellingenService,$state,$log,passagepuntenService,isSystemEditable,InstellingenServiceSectieSoortWeg) {
        $scope.isbtnDisabled = false;
        $scope.showDetails = false;
        $scope.isAddnew = false;
        $scope.traject = applicationCache.get('traject');
        $scope.systemId = $scope.traject.systemId;
        $scope.showSectieErrors = false;

        $scope.disableLinks = function() {
            $scope.isbtnDisabled = true;
        };

        $scope.enableLinks = function() {
            $scope.isbtnDisabled = false;
        };

        $scope.doShowDetails = function() {
            $scope.showDetails = true;
        };

        $scope.hideDetails = function() {
            $scope.showDetails = false;
        };

        $scope.isEditDisabledForUser = function() { return true; };

        $scope.wijzigen = function(corridorId){
            $scope.getCorridor(corridorId);
        };

        $scope.annuleer = function() {
            $scope.hideDetails();
            $scope.enableLinks();
        };

        $scope.getCorridor=function(corridorId){
            var promiseCorridor = instellingenService.getCorridor(
                $scope.traject.systemId,
                corridorId);

            promiseCorridor.then(function (data) {

                $scope.corridor = data;
                // Haal de passagepunten in op. Sectie bevat alleen het id en we willen de hmp hebben om te tonen.
                var promisePassagepunten = instellingenService.getStartGantryAndEndGantryForCorridor($scope.traject.systemId, corridorId);
                promisePassagepunten.then(function (data) {

                    var passagepuntenIn=[];
                    var passagepuntenUit=[];

                    passagepuntenIn.push({gantryId:data[0].gantryId,hectometer:data[0].hectometer, sectionId:data[0].sectionId});
                    passagepuntenUit.push({gantryId:data[1].gantryId,hectometer:data[1].hectometer, sectionId:data[1].sectionId});

                    $scope.hmpPassagepuntenIn=passagepuntenIn;
                    $scope.hmpPassagepuntenUit=passagepuntenUit;
                    $scope.soortenWeg= InstellingenServiceSectieSoortWeg.getSoortenWeg();
                    $scope.sectie={
                         startSectionId: $scope.corridor.data.startSectionId,
                         endSectionId: $scope.corridor.data.endSectionId,
                         corridorId: corridorId,
                         version: $scope.corridor.version,
                         naam: $scope.corridor.data.name,
                         hmpPassagepuntIn: passagepuntenIn[0],
                         hmpPassagepuntUit: passagepuntenUit[0],
                         locatieBegin: $scope.corridor.data.info.locationLine1,
                         locatieEind: $scope.corridor.data.info.locationLine2,
                         richtingVan: $scope.corridor.data.direction.directionFrom,
                         richtingNaar: $scope.corridor.data.direction.directionTo,
                         codePleegPlaats: pad($scope.corridor.data.info.locationCode.toString(), 5),
                         projectCode: $scope.corridor.data.dutyType + $scope.corridor.data.deploymentCode,
                         radarCode: parseInt($scope.corridor.data.radarCode, 10),
                         codeTekst: $scope.corridor.data.codeText,
                         wegCode: pad($scope.corridor.data.roadCode.toString(), 4),
                         binnenBebouwdeKom: $scope.corridor.data.isInsideUrbanArea,
                         soortWeg: $scope.soortenWeg.find(function (e) {
                             return e.id == $scope.corridor.data.roadType;
                         })};

                    $scope.doShowDetails();
                    $scope.disableLinks();

                }, function (reason) {
                 $log.debug(reason);
             });
            }, function (data) {
                $log.debug(data);
                applicationCache.put("error",data);
                $state.go("error");
            });
        };

        $scope.getPossiblePassagepunten=function(){
            var passagepuntenUit=[];
            if($scope.sectie.hmpPassagepuntIn!=undefined) {
                var promisePassagepuntenUit = instellingenService.getBeschikbarePassagepuntenUit($scope.traject.systemId, $scope.sectie.hmpPassagepuntIn.sectionId);
                promisePassagepuntenUit.then(function (data) {
                    for (var i = 0; i < data.length; i++) {
                        passagepuntenUit.push({gantryId: data[i].gantryId, hectometer: data[i].hectometer, sectionId: data[i].sectionId})
                    }
                    $scope.hmpPassagepuntenUit = passagepuntenUit;

                }, function (reason) {
                    $log.debug(reason);
                });
            } else {
                $scope.hmpPassagepuntenUit = passagepuntenUit;
            }
        };

        $scope.bewaar = function() {

            var promise =  instellingenService.putCorridor($scope.traject.systemId,$scope.sectie );
            promise.then(function() {
                for (var i = 0; i < $scope.traject.corridors.length; i++) {
                    if($scope.traject.corridors[i].id==$scope.sectie.corridorId) {
                        $scope.traject.corridors[i].name=$scope.sectie.naam;
                    }
                }
                applicationCache.put('traject', $scope.traject);

                var tabs=applicationCache.get('tabs');
                var result= tabs.find(function(e){ return e.corridorId == $scope.sectie.corridorId; });
                if(result!=undefined){
                    if(result.title!=$scope.sectie.naam) {
                        result.title=$scope.sectie.naam;
                        applicationCache.remove('tabs');
                        applicationCache.put('tabs',tabs)
                    }
                }

                $log.debug($scope.traject);

                if(!$scope.$$phase) {
                    $scope.$apply();
                }
                $scope.hideDetails();
                $scope.enableLinks();

            }, function (reason) {
                $log.debug(reason);
                if(!$scope.$$phase) {
                    $scope.$apply();
                }
                applicationCache.put("error",reason);
                $state.go("error");
            });
        };
    }
]);

