



app.run(function ($rootScope, $location,AuthenticationService, RoleService, SessionService) {

    var Johan = function (source,str){
        console.log(source);
        console.log(str);
        return source.indexOf(str) == 0;
    };
  'use strict';

  // enumerate routes that don't need authentication
  var routesThatDontRequireAuth = ['/login'];
  var routesForAdmin = ['/admin','/handhaving'];

  // check if current location matches route  
  var routeClean = function (route) {
      console.log("in "  );
    return _.find(routesThatDontRequireAuth,
      function (noAuthRoute) {
        //return _.str.startsWith(route, noAuthRoute);
          if( Johan(route,noAuthRoute))
            console.log("een route zonder authenticatie is gevonden:"  );
          return  Johan(route,noAuthRoute);
          //return route.startsWith(noAuthRoute);
      });
  };

  // check if current location matches route  
  var routeAdmin = function (route) {
    return _.find(routesForAdmin,
      function (noAuthRoute) {
        //return _.str.startsWith(route, noAuthRoute);
          if(Johan(route,noAuthRoute))
              console.log("een route met authenticatie en admin is gevonden:" );
        //return route.startsWith(noAuthRoute);
          return  Johan(route,noAuthRoute);
      });
  };

  $rootScope.$on('$stateChangeStart', function (ev, to, toParams, from, fromParams) {
    // if route requires auth and user is not logged in
    // gaan we naar een locatie waar authenticatie voor nodig is.
    if (!routeClean($location.url()) && !AuthenticationService.isLoggedIn()) {
      // redirect back to login
      ev.preventDefault();
      $location.path('/login');

    }
    else if (routeAdmin($location.url()) && !RoleService.validateRoleAdmin(SessionService.currentUser)) {
      // redirect back to login
      ev.preventDefault();
      $location.path('/error');

    }
      else{
        ev.preventDefault();
        $location.path('/login');


    }

  });
});
