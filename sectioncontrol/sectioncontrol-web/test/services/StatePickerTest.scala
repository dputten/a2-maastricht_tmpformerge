package services

/**
 * Created by jeijlders on 4/9/15.
 */

import models.web.{CorridorStateContainer, SystemStateType}
import models.web.SystemStateType._
import org.specs2.mutable._

class StatePickerTest extends Specification {
  "A StatePicker" should {
    """have a getState method that alsways returns the combined result (EnforceOn) if one of the corridor states
      contains EnforceOn or EnforceOff or EnforceDegraded with reductionfactor<100""" in {

      var coridorStates = List(
        CorridorStateContainer(SystemStateType.EnforceOn, 0, 0),
        CorridorStateContainer(SystemStateType.EnforceOff, 0, 0),
        CorridorStateContainer(SystemStateType.EnforceDegraded, 0, 0))
      val statePicker=new StatePicker(new StateMapper())
      // EnforceOn
      statePicker.getState(coridorStates).get must be_==(SystemStateType.EnforceOn)

      coridorStates=List(CorridorStateContainer(SystemStateType.EnforceOn,0,0),CorridorStateContainer(SystemStateType.StandBy,0,0))
      statePicker.getState(coridorStates).get must be_==(SystemStateType.EnforceOn)

      coridorStates=List(CorridorStateContainer(SystemStateType.EnforceOn,0,0),CorridorStateContainer(SystemStateType.Off,0,0))
      statePicker.getState(coridorStates).get must be_==(SystemStateType.EnforceOn)

      coridorStates=List(CorridorStateContainer(SystemStateType.EnforceOn,0,0),CorridorStateContainer(SystemStateType.Maintenance,0,0))
      statePicker.getState(coridorStates).get must be_==(SystemStateType.EnforceOn)

      coridorStates=List(CorridorStateContainer(SystemStateType.EnforceOn,0,0),CorridorStateContainer(SystemStateType.Failure,0,0))
      statePicker.getState(coridorStates).get must be_==(SystemStateType.EnforceOn)

      // EnforceOff
      coridorStates=List(CorridorStateContainer(SystemStateType.EnforceOff,0,0),CorridorStateContainer(SystemStateType.StandBy,0,0))
      statePicker.getState(coridorStates).get must be_==(SystemStateType.EnforceOn)

      coridorStates=List(CorridorStateContainer(SystemStateType.EnforceOff,0,0),CorridorStateContainer(SystemStateType.Off,0,0))
      statePicker.getState(coridorStates).get must be_==(SystemStateType.EnforceOn)

      coridorStates=List(CorridorStateContainer(SystemStateType.EnforceOff,0,0),CorridorStateContainer(SystemStateType.Maintenance,0,0))
      statePicker.getState(coridorStates).get must be_==(SystemStateType.EnforceOn)

      coridorStates=List(CorridorStateContainer(SystemStateType.EnforceOff,0,0),CorridorStateContainer(SystemStateType.Failure,0,0))
      statePicker.getState(coridorStates).get must be_==(SystemStateType.EnforceOn)

      // EnforceDegraded
      coridorStates=List(CorridorStateContainer(SystemStateType.EnforceDegraded,0,0),CorridorStateContainer(SystemStateType.StandBy,0,0))
      statePicker.getState(coridorStates).get must be_==(SystemStateType.EnforceOn)

      coridorStates=List(CorridorStateContainer(SystemStateType.EnforceDegraded,0,0),CorridorStateContainer(SystemStateType.Off,0,0))
      statePicker.getState(coridorStates).get must be_==(SystemStateType.EnforceOn)

      coridorStates=List(CorridorStateContainer(SystemStateType.EnforceDegraded,0,0),CorridorStateContainer(SystemStateType.Maintenance,0,0))
      statePicker.getState(coridorStates).get must be_==(SystemStateType.EnforceOn)

      coridorStates=List(CorridorStateContainer(SystemStateType.EnforceDegraded,0,0),CorridorStateContainer(SystemStateType.Failure,0,0))
      statePicker.getState(coridorStates).get must be_==(SystemStateType.EnforceOn)
    }
  }

  "A StatePicker" should {
    """have a getState method that alsways returns the combined result (Off) if all of the corridor states
      contains Off""" in {
      var coridorStates =List(CorridorStateContainer(SystemStateType.Off,0,0),CorridorStateContainer(SystemStateType.Off,0,0))
      val statePicker=new StatePicker(new StateMapper())
      statePicker.getState(coridorStates).get must be_==(SystemStateType.Off)

      coridorStates= List(CorridorStateContainer(SystemStateType.Off,0,0),CorridorStateContainer(SystemStateType.Maintenance,0,0))
      statePicker.getState(coridorStates).get must not be_==(SystemStateType.Off)
    }
  }

  "A StatePicker" should {
    """have a getState method that alsways returns the combined result (Maintenance) if all of the corridor states
      contains Off, Maintenance or Failure""" in {
      val coridorStates =List(CorridorStateContainer(SystemStateType.Off,0,0),CorridorStateContainer(SystemStateType.Maintenance,0,0),CorridorStateContainer(SystemStateType.Failure,0,0))
      val statePicker=new StatePicker(new StateMapper())
      statePicker.getState(coridorStates).get must be_==(SystemStateType.Maintenance)
    }
  }

  "A StatePicker" should {
    """have a getState method that alsways returns the combined result (StandBy) if all of the corridor states
      are StandBy OR not all corridors Off, Failure, Maintenance""" in {
      var coridorStates=List(CorridorStateContainer(SystemStateType.StandBy,0,0),CorridorStateContainer(SystemStateType.StandBy,0,0),CorridorStateContainer(SystemStateType.StandBy,0,0))
      val statePicker=new StatePicker(new StateMapper())
      statePicker.getState(coridorStates).get must be_==(SystemStateType.StandBy)

      coridorStates=List(CorridorStateContainer(SystemStateType.StandBy,0,0),CorridorStateContainer(SystemStateType.Maintenance,0,0))
      statePicker.getState(coridorStates).get must  be_==(SystemStateType.StandBy)
    }
  }

  "A StatePicker" should {
    """have a mapToState that returns a reduced state""" in {
      var statePicker=new StatePicker(new StateMapper())
      statePicker.mapToState(CorridorStateContainer(SystemStateType.StandBy,0,0)) must be_==(SystemStateType.StandBy)
      statePicker.mapToState(CorridorStateContainer(SystemStateType.Maintenance,0,0)) must be_==(SystemStateType.Maintenance)
      statePicker.mapToState(CorridorStateContainer(SystemStateType.Off,0,0)) must be_==(SystemStateType.Off)
      statePicker.mapToState(CorridorStateContainer(SystemStateType.EnforceOn,0,0)) must be_==(SystemStateType.EnforceOn)
      statePicker.mapToState(CorridorStateContainer(SystemStateType.EnforceOff,0,0)) must be_==(SystemStateType.EnforceOn)
      statePicker.mapToState(CorridorStateContainer(SystemStateType.EnforceDegraded,99,0)) must be_==(SystemStateType.EnforceOn)

      statePicker=new StatePicker(new StateMapper())
      statePicker.mapToState(CorridorStateContainer(SystemStateType.EnforceDegraded,100,0)) must be_==(SystemStateType.Failure)
    }
  }
}
