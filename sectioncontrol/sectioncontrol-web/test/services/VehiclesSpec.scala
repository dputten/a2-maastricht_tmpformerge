package services

import csc.imageretriever.ImageKey
import csc.sectioncontrol.messages.VehicleImageType.Overview
import csc.sectioncontrol.messages.{Lane, VehicleMetadata}
import csc.sectioncontrol.storage.HBaseTableDefinitions._
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import services.Vehicles.HbaseImageKey

class VehiclesSpec extends WordSpec with MustMatchers {
  val IMAGE_NOT_DOWNLOADED = 0
  val IMAGE_AVAILABLE = 1
  
  val sut = Vehicles

  val systemId = "A4"
  val gantryId = "Gantry"
  val from = 1L
  val to = 2L
  val license = ""

  "VehiclesService" must {
    "provide a list of image keys for vehicles with not yet downloaded images" in {
      def vehicleMetadataProvider(systemId: String, gantryId: String, from: Long, to: Long,license:String): Seq[VehicleMetadata] = {
        List(vehicleWithImage, vehicleWithoutImage)
      }

      def imageProvider(imageKey: HbaseImageKey): Option[vehicleImageRecordType] = {
        imageKey match {
          case (_, IMAGE_NOT_DOWNLOADED, _) => None
          case (_, IMAGE_AVAILABLE, _)      => Some(Array[Byte]())
        }
      }

      val result = sut.getImageKeysForVehiclesWithMissingImages(systemId, gantryId, from, to, license,vehicleMetadataProvider, imageProvider)

      result must be (List(ImageKey("", IMAGE_NOT_DOWNLOADED, Overview, "A4", "Gantry", "withoutImageLane")))
    }
  }


  def vehicleWithImage = VehicleMetadata(
    lane = Lane("dummy", "withImageLane", "dummy", "dummy", 0, 0),
    eventId = "dummy",
    eventTimestamp = IMAGE_AVAILABLE,
    eventTimestampStr = None,
    NMICertificate = "dummy",
    applChecksum = "dummy",
    serialNr = "dummy"
  )

  def vehicleWithoutImage = vehicleWithImage.copy(
    eventTimestamp = IMAGE_NOT_DOWNLOADED,
    lane = vehicleWithImage.lane.copy(name = "withoutImageLane")
  )

}