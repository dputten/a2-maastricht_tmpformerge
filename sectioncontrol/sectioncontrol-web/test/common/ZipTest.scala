package common

import java.io.{File, FileOutputStream}
import java.util.zip.{ZipEntry, ZipFile, ZipOutputStream}

import akka.actor.ActorSystem
import akka.testkit.TestKit
import models.web.FileItem
import org.scalatest.matchers.MustMatchers
import org.scalatest.{BeforeAndAfterEach, BeforeAndAfterAll, WordSpec}
import scala.collection.mutable.ListBuffer

class ZipTest extends TestKit(ActorSystem("testSystem")) with WordSpec with MustMatchers with Config with BeforeAndAfterAll with FileHelper with BeforeAndAfterEach{
  val resourceDir = new File(getClass.getClassLoader.getResource("").getPath)
  val testZipDir = new File(resourceDir,  "testZip")
  testZipDir.mkdir()

  def startWithCleanState() = {
    deleteFiles(testZipDir)
    getNumberOfFiles(testZipDir) must be(0)
  }

  "Zip" must {
    "zip the fileItems to a file on the filesystem" in {
      startWithCleanState()

      val fileName = "/test.zip"
      val fileSize = 20000
      val nrOfItems = 20

      val bytes = new Array[Byte](fileSize)
      var fileItems = new ListBuffer[FileItem]()

      // add nrOfItems
      for (a <- 1 to nrOfItems) {
        val fileItem = new FileItem("test" + a.toString, bytes)
        fileItems += fileItem
      }

      // delete
      new File(resourceDir.toString + fileName).delete()

      // compress
      val zip = new Zip()
      zip.compress(resourceDir.toString, fileName, fileItems)

      // check if the zip exists
      new File(resourceDir.toString + fileName).exists() must be(true)

      // check the nr of entries in the zip
      val zipFile = new ZipFile(resourceDir.toString + fileName)
      zipFile.size() must be(nrOfItems)

      // check one entry name and size
      val zipEntry = zipFile.entries().nextElement()
      zipEntry.getName must be("test1")
      zipEntry.getSize must be(20000)

      deleteFiles(testZipDir)
    }

    // test to create a zip file larger than 2gb because in the previous code this was a problem because of
    // the max siz eof byte array[Int]. We were bounded to the max int size
    // test is ignored because it uses a large local file. Just to prove it works. Please don't delete
    "zip a large amount of data >2gb" ignore {

      val source = scala.io.Source.fromFile("/home/jeijlders/Downloads/WebStorm-10.0.4.tar.gz", "ISO-8859-1")
      val byteArray = source.map(_.toByte).toArray
      source.close()

      val fos: FileOutputStream = new FileOutputStream("/home/jeijlders/Downloads/test.zip")
      val zos: ZipOutputStream = new ZipOutputStream(fos)

      // eerst een zip file maken
      // deze daarna inladen met file en teruggeven. Dan kan de lengte gebruikt worden in de simpleresult

      for (a <- 1 to 20) {
        val fileItem = new FileItem("test1.xxx", byteArray)
        val ze: ZipEntry = new ZipEntry(fileItem.fileName + a.toString)
        zos.putNextEntry(ze)
        zos.write(fileItem.data)
        zos.closeEntry()
      }
      zos.close()
      fos.close()
    }
  }
}
