package common

import java.io.File

import akka.actor.{ActorSystem, Props}
import akka.testkit.TestKit
import common.actors.{DownloadPassagegegevensFileCleanerActor, FileCleaner, Wakeup}
import org.scalatest.matchers.MustMatchers
import org.scalatest.{BeforeAndAfterAll, WordSpec}

class FileCleanerTest extends TestKit(ActorSystem("testSystem")) with WordSpec with MustMatchers with Config with BeforeAndAfterAll with FileHelper {
  val resourceDir = new File(getClass.getClassLoader.getResource("").getPath)

  val testFilesDir = new File(resourceDir,  "testFiles")
  testFilesDir.mkdir()
  val fileCleanerActor = system.actorOf(Props(new DownloadPassagegegevensFileCleanerActor(new FileCleaner(testFilesDir.toString, retentionInSeconds = 1))))

  def startWithCleanState() = {
    deleteFiles(testFilesDir)
    getNumberOfFiles(testFilesDir) must be(0)
  }

  "FileCleaner actor" must {
    "clean the files for which the retention period has expired" in {
      startWithCleanState()
      createFile(testFilesDir.toString, "/test1.txt")
      createFile(testFilesDir.toString, "/test2.txt")
      getNumberOfFiles(testFilesDir) must be(2)

      // wait 2 seconds so the third file has a slightly later creation date
      Thread.sleep(2000)

      createFile(testFilesDir.toString, "/test3.txt")
      getNumberOfFiles(testFilesDir) must be(3)

      fileCleanerActor ! Wakeup
      // Wait because the actor needs to be created and runs asynchronous.
      // Otherwise the test will continue and the result wil not be there yet
      Thread.sleep(3000)
      getNumberOfFiles(testFilesDir) must be(1)
      deleteFiles(testFilesDir)
    }
  }

  "FileCleaner" must {
    "must clear the files that not fall within the interval" in {
      startWithCleanState()

      createFile(testFilesDir.toString, "/test1.txt")
      createFile(testFilesDir.toString, "/test2.txt")
      getNumberOfFiles(testFilesDir) must be(2)

      // Wait 2 seconds so the third file has a slightly later creation date
      Thread.sleep(2000)
      createFile(testFilesDir.toString, "/test3.txt")
      getNumberOfFiles(testFilesDir) must be(3)

      val fileCleaner = new FileCleaner(testFilesDir.toString, 1)
      fileCleaner.clean
      getNumberOfFiles(testFilesDir) must be(1)
      deleteFiles(testFilesDir)
    }
  }
}
