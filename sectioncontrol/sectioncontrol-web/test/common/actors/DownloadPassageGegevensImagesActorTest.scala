import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import common.actors._
import csc.imageretriever.BatchProgressResponse
import org.scalatest.matchers.ShouldMatchers
import org.scalatest.{BeforeAndAfterAll, WordSpec}

class DownloadPassageGegevensImagesActorTest extends TestKit(ActorSystem("testSystem")) with WordSpec with BeforeAndAfterAll with ShouldMatchers with ImplicitSender {

  override protected def afterAll(): scala.Unit = {
    system.shutdown()
  }

  val systemId = "A4"
  val gantryId = "Gantry"

  val imageReceiver = TestProbe()
  val sut = system.actorOf(Props(new DownloadPassageGegevensImagesActor(imageReceiver.ref)))

  "A DownloadPassageGegevensImageActor" should {
    "answer clients requesting for status of completed download" in {
      val refId = startDownload()
      val completedResponse = BatchProgressResponse(refId = refId, total = 3, count = 3, errorCount = 0)

      imageReceiver.send(sut, completedResponse)
      sut ! GetDownloadStatus(refId)

      expectMsg(DownloadCompleted(refId))
    }

    "answer clients requesting for status of download in progress" in {
      val refId = startDownload()
      val inProgressResponse = BatchProgressResponse(refId = refId, total = 3, count = 2, errorCount = 0)

      imageReceiver.send(sut, inProgressResponse)
      sut ! GetDownloadStatus(refId)

      expectMsg(DownloadInProgress(refId))
    }

    def startDownload(): String = {
      sut ! StartDownloadingImagesFromGantry(systemId, gantryId, List())
      expectMsgPF() {
        case DownloadInProgress(refId) => refId
        case _ => fail("Expected confirmation that download has started")
      }
    }

  }

}
