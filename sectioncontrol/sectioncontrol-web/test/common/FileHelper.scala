package common

import java.io.{File, PrintWriter}

trait FileHelper {
  def createFile(filePath: String, fileName: String) {
    val writer = new PrintWriter(new File(filePath + fileName))
    writer.write("Hello Scala")
    writer.close()
  }

  def deleteFiles(file: File) {
    if (file.isDirectory)
      Option(file.listFiles).map(_.toList).getOrElse(Nil).foreach(f => f.delete)
  }

  def getNumberOfFiles(file: File): Int = {
    if (file.isDirectory)
      Option(file.listFiles).map(_.toList).getOrElse(Nil).length
    else
      0
  }
}
