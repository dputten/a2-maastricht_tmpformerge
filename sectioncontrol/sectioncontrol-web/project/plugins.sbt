// Comment to get more information during initialization

logLevel := Level.Warn

resolvers += "CSC Repo" at "http://20.32.143.30:8082/nexus/content/groups/public/"

// Use the Play sbt plugin for Play projects

addSbtPlugin("play" % "sbt-plugin" % "2.0.8")

resolvers += Resolver.url("artifactory", url("http://cscappamd911.nl.emea.csc.com:8082/nexus/content/groups/ivy-release/"))(Resolver.ivyStylePatterns)
