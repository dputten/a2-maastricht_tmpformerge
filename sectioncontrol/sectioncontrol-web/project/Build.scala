import sbt.Keys._
import sbt.PlayProject._
import sbt._

object ApplicationBuild extends Build {

  object V {
    val app       = "6.1.0.1"
    val cscConfig = "1.3.1"
  }

  val appDependencies = Seq(
    "com.csc.traffic"    %% "dconfig"                       % V.cscConfig,
    "com.csc.traffic"    %% "dlog"                          % V.cscConfig,
    "com.csc.traffic"    %% "curator-utils"                 % V.cscConfig,
    "com.csc.traffic"    %% "sectioncontrol-storage"        % V.app,
    "com.csc.traffic"    %% "sectioncontrol-messages"       % V.app,
    "com.csc.traffic"    %% "sectioncontrol-decorate"       % V.app,
    "com.csc.traffic"    %% "sectioncontrol-imageretriever" % V.app,
    "com.csc.traffic"    %% "sectioncontrol-certify"        % V.app,
    "net.liftweb"        %% "lift-json"                     % "2.4",
    "org.apache.commons"  % "commons-lang3"                 % "3.1",
    "org.scalatest"      %% "scalatest"                     % "1.6.1"                % "test",
    "com.typesafe.akka"   % "akka-testkit"                  % "2.0.1-CAMEL-SNAPSHOT" % "test"
  ) ++ CDH5.deps

  val main = PlayProject(
    name = "sectioncontrol-web",
    applicationVersion = V.app,
    dependencies = appDependencies,
    mainLang = SCALA
  ).settings(
    doc in Compile <<= target.map(_ / "none"),
    testOptions in Test := Nil, // Needed to enable ScalaTest, why o why??
    resolvers += Resolver.file("Local repo", file(Path.userHome.absolutePath + "/.ivy2/local"))(Resolver.ivyStylePatterns),
    resolvers += "CSC Repo" at "http://cscappamd911.nl.emea.csc.com:8082/nexus/content/groups/group_sectioncontrol/"
  )

}

