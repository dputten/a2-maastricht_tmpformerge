/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package scala.csc.registrationreceiver.test

import csc.akkautils.DirectLogging
import csc.amqp.JsonSerializers
import csc.curator.utils.CuratorToolsImpl
import csc.hbase.utils.HBaseTableKeeper
import csc.hbase.utils.testframework.HBaseTestFramework
import csc.registrationreceiver.protocol.VehicleRegistrationKeepalive
import csc.registrationreceiver.test.SampleData
import csc.registrationreceiver.{HBaseFunctionsImpl, Boot, HBaseFunctions}
import csc.sectioncontrol.messages._
import csc.sectioncontrol.storage.HBaseTableDefinitions._
import csc.sectioncontrol.storagelayer.hbasetables.RowKeyDistributorFactory
import org.apache.curator.framework.{CuratorFramework, CuratorFrameworkFactory}
import org.apache.curator.retry.ExponentialBackoffRetry
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.util.Bytes
import org.scalatest.matchers.MustMatchers
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, WordSpec}

/**
 * @author csomogyi
 */
class HBaseFunctionsTest extends WordSpec with MustMatchers with BeforeAndAfterEach with BeforeAndAfterAll
  with HBaseTestFramework with SampleData with DirectLogging with JsonSerializers {

  implicit val formats = Boot.jsonFormats
  var functions: HBaseFunctions = _
  var curator: CuratorFramework = _
  var tableKeeper = new HBaseTableKeeper{}

  override protected def beforeAll(configMap: Map[String, Any]): Unit = {
    super.beforeAll(configMap)
    val retryPolicy = new ExponentialBackoffRetry(100, 10)
    val connectString = "localhost:%s".format(testUtil.getZkCluster.getClientPort)
    curator = CuratorFrameworkFactory.newClient(connectString, retryPolicy)
    curator.start()

    val storage = new CuratorToolsImpl(Some(curator), log)
    RowKeyDistributorFactory.init(storage)
  }

  override protected def afterAll(configMap: Map[String, Any]): Unit = {
    tableKeeper.closeTables()
    curator.close()
    super.afterAll(configMap)
  }

  override def beforeEach(): Unit = {
    functions = new HBaseFunctionsImpl(hbaseConfig, tableKeeper, formats)
  }

  "HBaseFunctions" must {
    "save vehicle metadata and license information" in {
      functions.saveMetadata(sampleVehicleMetadata)

      val cf = vehicleRecordColumnFamily.getBytes
      val table = new HTable(hbaseConfig, vehicleRecordTableName)
      val scanner = table.getScanner(cf)
      val result = scanner.next()
      val metadataBytes = result.getValue(cf, vehicleRecordColumnName.getBytes)
      val licenseBytes = result.getValue(cf, vehicleRecordLicenseColumnName.getBytes)
      val metadata = fromJson[VehicleMetadata](Bytes.toString(metadataBytes)).right.get
      assert(metadata === sampleVehicleMetadata)
      val license = Bytes.toString(licenseBytes)
      sampleVehicleMetadata.license.foreach(lic ⇒ assert(license === lic.value))
      val empty = scanner.next()
      assert(empty === null)
    }
    "save keepalive in processing table" in {
      val lane = Lane("A2-3-lane1", "lane1", "3", "A2", 5.0, 45.0)
      val keepalive = VehicleRegistrationKeepalive(lane, System.currentTimeMillis())
      val component = "RegistrationReceiver@" + lane.laneId
      val record = GantryTimeProcessingRecord.create(lane, keepalive.processedTime)
      val distributor = RowKeyDistributorFactory.getDistributor(processingTableName)

      functions.saveKeepalive(keepalive)

      val cf = processingColumnFamily.getBytes
      val table = new HTable(hbaseConfig, processingTableName)
      val scanner = table.getScanner(cf)
      val result = scanner.next()
      val key = result.getRow()
      val componentBytes = result.getValue(cf, processingComponentColumnName.getBytes)
      val stateBytes = result.getValue(cf, processingStateColumnName.getBytes)
      val timeBytes = result.getValue(cf, processingTimeColumnName.getBytes)
      val recordBytes = result.getValue(cf, processingRecordColumnName.getBytes)
      val origKey = "%s-%s-%s".format(lane.system, lane.gantry, lane.name)
      assert(key === distributor.getDistributedKey(origKey.getBytes))
      assert(Bytes.toString(componentBytes) === GantryTimeProcessingRecord.ComponentName)
      assert(Bytes.toString(stateBytes) === GantryTimeProcessingRecord.State)
      assert(Bytes.toLong(timeBytes) === keepalive.processedTime)
      assert(fromJson[ProcessingRecord](Bytes.toString(recordBytes)).right.get === record)
    }
  }
}
