/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package scala.csc.registrationreceiver.test

import akka.util.duration._
import com.typesafe.config.ConfigFactory
import csc.registrationreceiver.Configuration
import org.scalatest.matchers.MustMatchers
import org.scalatest.{BeforeAndAfterAll, WordSpec}

/**
 * @author csomogyi
 */
class ConfigurationTest extends WordSpec with MustMatchers with BeforeAndAfterAll {
  "configuration" must {
    "load the configuration upon specifying system property" in {
      val config = Configuration.load(ConfigFactory.load("registrationReceiver-configtest"))
      config.amqpConnection.actorName must be("registrationreceiver-actor")
      config.amqpConnection.amqpUri must be("amqp://localhost:5672/")
      config.amqpConnection.reconnectDelay must be(5000 milliseconds)

      config.amqpServices.consumerQueue must be("registrationreceiver_req")
      config.amqpServices.serversCount must be(5)

      config.hBase.servers must be("Host123:2181")

      config.zookeeper.servers must be("localhost:2181")
      config.zookeeper.sessionTimeout must be(1000 milliseconds)
      config.zookeeper.retries must be(100)
    }

  }

  override protected def afterAll(): Unit = {
    System.clearProperty("registrationReceiver.conf")
  }
}
