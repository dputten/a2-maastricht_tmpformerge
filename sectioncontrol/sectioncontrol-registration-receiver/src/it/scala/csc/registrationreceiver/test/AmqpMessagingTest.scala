/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.registrationreceiver.test

import java.util.concurrent.{CountDownLatch, TimeUnit}

import akka.actor.ActorSystem
import akka.testkit.TestKit
import com.rabbitmq.client.{Channel, Connection, ConnectionFactory}
import csc.amqp.AmqpSerializers
import csc.curator.utils.{Curator, Versioned}
import csc.registrationreceiver.Configuration.ZookeeperConfiguration
import csc.registrationreceiver.protocol.{VehicleRegistrationCertificate, VehicleRegistrationKeepalive}
import csc.registrationreceiver.{Boot, Configuration, HBaseFunctions}
import csc.sectioncontrol.messages.{Lane, ProcessingRecord, VehicleMetadata}
import csc.sectioncontrol.messages.certificates.{ActiveCertificate, ComponentCertificate}
import com.typesafe.config.ConfigFactory
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, WordSpec}
import org.scalatest.matchers.MustMatchers
import csc.akkautils.DirectLogging
import org.apache.hadoop.conf

/**
 * @author csomogyi
 */
class AmqpMessagingTest extends TestKit(ActorSystem("amqpClient")) with AmqpSerializers
  with WordSpec with MustMatchers with BeforeAndAfterAll with BeforeAndAfterEach with SampleData with DirectLogging {

  override implicit val formats = Boot.jsonFormats
  override val ApplicationId = "test"

  var config: Configuration = _

  override protected def beforeAll(): Unit = {
    config = Configuration.load(ConfigFactory.load("registrationReceiver-test"))
  }

  override protected def afterAll(): Unit = {
    System.clearProperty("registrationReceiver.conf")
    system.shutdown()
  }

  val curator = new CuratorMock {
    override def set[T <: AnyRef](path: String, value: T, storedVersion: Int): Unit = {
      super.set(path,value,storedVersion)
      latch.countDown()
    }
  }

  var connection: Connection = _
  var channel: Channel = _
  var boot: Boot = _
  var savedMetadata: Option[VehicleMetadata] = None
  var savedKeepalive: Option[VehicleRegistrationKeepalive] = None
  var latch = new CountDownLatch(1)

  override def beforeEach(): Unit = {
    val cf = new ConnectionFactory()
    cf.setUri(config.amqpConnection.amqpUri)
    connection = cf.newConnection()
    channel = connection.createChannel()
    channel.queueDelete(config.amqpServices.consumerQueue)
    channel.queueDeclare(config.amqpServices.consumerQueue, false, false, true, null)

    latch = new CountDownLatch(1)
    boot = new Boot {

      override def loadConfiguration(): Configuration = config

      override protected def createHBaseFunctions(config: conf.Configuration): HBaseFunctions = {
        new HBaseFunctions {

          override def saveMetadata(metadata: VehicleMetadata): Option[String] = {
            savedMetadata = Some(metadata)
            latch.countDown()
            None
          }

          override def saveKeepalive(keepalive: VehicleRegistrationKeepalive): Option[String] = {
            savedKeepalive = Some(keepalive)
            latch.countDown()
            None
          }

          override protected def saveKeepalive(lane: Lane, record: ProcessingRecord): Option[String] = throw new UnsupportedOperationException

          override def laneProcessingTimes(): Map[String, Long] = {
            Map("A2-3-lane1" -> 1000)
          }
        }
      }

      override protected def startCurator(config: ZookeeperConfiguration): Curator = curator

      override protected def shutdownCurator(): Unit = {}
    }
    boot.startup()
  }

  override def afterEach(): Unit = {
    channel.close()
    connection.close()
    boot.shutdown()
    boot.actorSystem.shutdown()
  }

  "Registration Receiver" must {
    "consume VehicleMetadata when no error in HBaseFunctions" in {
      channel.queuePurge(config.amqpServices.consumerQueue)
      savedMetadata = None
      val (body, properties) = toPublishPayload(sampleVehicleMetadata).right.get
      channel.basicPublish("", config.amqpServices.consumerQueue, properties, body)
      latch.await(10,TimeUnit.SECONDS)
      assert(savedMetadata === Some(sampleVehicleMetadata))
    }
    "consume VehicleRegistrationKeepalive when no error in HBaseFunctions" in {
      channel.queuePurge(config.amqpServices.consumerQueue)
      savedKeepalive = None
      val lane = Lane("A2-3-lane1", "lane1", "3", "A2", 5.0, 45.0)
      val keepalive = VehicleRegistrationKeepalive(lane, System.currentTimeMillis())
      val (body, properties) = toPublishPayload(keepalive).right.get
      channel.basicPublish("", config.amqpServices.consumerQueue, properties, body)
      latch.await(10,TimeUnit.SECONDS)
      assert(savedKeepalive === Some(keepalive))
    }
    "save ActiveCertificate when receiving VehicleRegistrationCertificate" in {
      curator.clear()

      channel.queuePurge(config.amqpServices.consumerQueue)

      val keepalive = VehicleRegistrationCertificate("alma", "gantry2",
        ActiveCertificate(System.currentTimeMillis(), ComponentCertificate("name1", "checksum2")))
      val (body, properties) = toPublishPayload(keepalive).right.get
      channel.basicPublish("", config.amqpServices.consumerQueue, properties, body)
      //should not change
      latch.await(2,TimeUnit.SECONDS)
      assert(curator.getVersioned[ActiveCertificate]("/ctes/systems/alma/activeCertificates/name1") ===
        Some(Versioned[AnyRef](keepalive.activeCertificate, 1)))

      latch = new CountDownLatch(1)
      channel.basicPublish("", config.amqpServices.consumerQueue, properties, body)
      //should not change
      latch.await(2,TimeUnit.SECONDS)

      assert(curator.getVersioned[ActiveCertificate]("/ctes/systems/alma/activeCertificates/name1") ===
        Some(Versioned[AnyRef](keepalive.activeCertificate, 1)))

      latch = new CountDownLatch(1)
      val keepAlive2 = VehicleRegistrationCertificate("alma", "gantry2",
        ActiveCertificate(System.currentTimeMillis(), ComponentCertificate("name1", "checksum4")))
      val (body2, properties2) = toPublishPayload(keepAlive2).right.get
      channel.basicPublish("", config.amqpServices.consumerQueue, properties2, body2)

      latch.await(10,TimeUnit.SECONDS)
      assert(curator.getVersioned[ActiveCertificate]("/ctes/systems/alma/activeCertificates/name1") ===
        Some(Versioned[AnyRef](keepAlive2.activeCertificate, 2)))
    }
  }
}
