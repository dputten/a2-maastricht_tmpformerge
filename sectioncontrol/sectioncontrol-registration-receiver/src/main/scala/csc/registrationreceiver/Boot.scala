/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.registrationreceiver

import akka.actor.{ Props, ActorSystem }
import akka.kernel.Bootable
import akka.routing.RoundRobinRouter
import com.github.sstone.amqp.Amqp.ChannelParameters
import com.github.sstone.amqp.{ Amqp, RabbitMQConnection }
import com.typesafe.config.ConfigFactory
import csc.akkautils.DirectLogging
import csc.curator.utils.{ Curator, CuratorToolsImpl }
import csc.hbase.utils.HBaseTableKeeper
import csc.json.lift.EnumerationSerializer
import csc.registrationreceiver.Configuration.ZookeeperConfiguration
import csc.registrationreceiver.actors.{ KeepalivePersisterActor, RegistrationReceiverActor }
import csc.sectioncontrol.messages.VehicleImageType
import csc.sectioncontrol.sign.certificate.CertificateService
import csc.sectioncontrol.storagelayer.StorageFactory
import csc.sectioncontrol.storagelayer.hbasetables.{ SerializerFormats, RowKeyDistributorFactory }
import net.liftweb.json.DefaultFormats
import org.apache.curator.framework.CuratorFramework
import org.apache.curator.retry.ExponentialBackoffRetry
import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.hadoop.conf

/**
 * Boots the registration receiver module. Connects to AMQP / RabbitMQ and the Curator then starts the [[RegistrationReceiverActor]]
 *
 * @author csomogyi
 */
class Boot extends Bootable with DirectLogging {
  implicit val actorSystem = ActorSystem("RegistrationReceiver", ConfigFactory.load("registrationReceiver"))

  def loadConfiguration(): Configuration = {
    Configuration.load(ConfigFactory.load("registrationReceiver"))
  }
  var curatorFramework: Option[CuratorFramework] = None
  var tableKeeper: Option[HBaseTableKeeper] = None

  def startup() = {
    log.info("Starting Registration Receiver")
    val config = loadConfiguration()
    log.info("Configuration loaded")
    val amqpConfig = config.amqpConnection
    val connection = RabbitMQConnection.create(amqpConfig.actorName, amqpConfig.amqpUri, amqpConfig.reconnectDelay)
    log.info("RabbitMQ connection created")
    val zookeeperClient = startCurator(config.zookeeper)
    log.info("Curator started")

    val hBaseConfig = HBaseTableKeeper.createCachedConfig(config.hBase.servers)

    val keepalivePersister = actorSystem.actorOf(KeepalivePersisterActor.props(createHBaseFunctions(hBaseConfig)))

    val listener = actorSystem.actorOf(Props(new RegistrationReceiverActor(createHBaseFunctions(hBaseConfig), keepalivePersister,
      zookeeperClient, CertificateService, Boot.jsonFormats))
      .withRouter(RoundRobinRouter(nrOfInstances = config.amqpServices.serversCount)))
    log.info("Listener actor pool ({}) created", config.amqpServices.serversCount)
    // with ChannelParameters we set the number of messages being fetched without Ack (prefetch count)
    val consumer = connection.createSimpleConsumer(config.amqpServices.consumerQueue, listener,
      Some(ChannelParameters(config.amqpServices.serversCount)), false)
    log.info("Consumer created")
    Amqp.waitForConnection(actorSystem, consumer).await()
    log.info("Consumer connected")
  }

  protected def startCurator(config: ZookeeperConfiguration): Curator = {
    val retryPolicy = new ExponentialBackoffRetry(config.sessionTimeout.toMillis.toInt, config.retries)
    curatorFramework = StorageFactory.newClient(config.servers, retryPolicy)
    curatorFramework.foreach(_.start())
    val zookeeperClient = new CuratorToolsImpl(curatorFramework, log, Boot.jsonFormats)
    RowKeyDistributorFactory.init(zookeeperClient)
    zookeeperClient
  }

  protected def createHBaseFunctions(hBaseConfig: conf.Configuration): HBaseFunctions = {
    if (tableKeeper == None) tableKeeper = Some(new HBaseTableKeeper {})
    new HBaseFunctionsImpl(hBaseConfig, tableKeeper.get, Boot.jsonFormats)
  }

  def shutdown() = {
    log.info("Shutting down Registration Receiver")
    shutdownCurator()
    tableKeeper.foreach(_.closeTables())
    log.info("Curator stopped")
  }

  protected def shutdownCurator(): Unit = {
    curatorFramework.foreach(_.close())
    curatorFramework = None
  }
}

object Boot {
  // TODO fix EnumerationSerializer so that we can safely use SerializerFormats.formats
  // at the moment due to naming conflicts it cannot be used in this project
  val jsonFormats = DefaultFormats + new EnumerationSerializer(VehicleImageType)
  //val jsonFormats = SerializerFormats.formats
}
