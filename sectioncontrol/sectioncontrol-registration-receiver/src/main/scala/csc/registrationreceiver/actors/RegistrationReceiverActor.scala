package csc.registrationreceiver.actors

import java.util.concurrent.TimeUnit

import akka.pattern.ask
import akka.actor.{ ActorRef, ActorLogging, Actor }
import akka.util.Timeout
import csc.amqp.AmqpSerializers
import csc.curator.utils.Curator
import csc.sectioncontrol.messages.certificates.ActiveCertificate
import csc.sectioncontrol.sign.certificate.CertificateService
import com.github.sstone.amqp.Amqp.{ Ok, Reject, Ack, Delivery }
import csc.registrationreceiver.HBaseFunctions
import csc.registrationreceiver.protocol.{ VehicleRegistrationCertificate, VehicleRegistrationKeepalive }
import csc.sectioncontrol.messages.VehicleMetadata
import net.liftweb.json.Formats
import scala.collection.mutable.Map

/**
 * Actor for handling incoming messages. Three types of messages are handled:
 * - [[VehicleMetadata]]
 * - [[VehicleRegistrationCertificate]]
 * - [[VehicleRegistrationKeepalive]]
 *
 * These are all fire-n-forget messages, no response is sent back. As they are fire-n-forget they will be rejected upon
 * failure of processing.
 *
 * @author csomogyi
 */
class RegistrationReceiverActor(hBaseFunctions: HBaseFunctions,
                                keepalivePersister: ActorRef,
                                curator: Curator,
                                certificateService: CertificateService,
                                jsonFormats: Formats) extends Actor
  with ActorLogging with AmqpSerializers {

  override implicit val formats = jsonFormats
  override val ApplicationId = "RegistrationReceiver"

  implicit val timeout = Timeout(1, TimeUnit.SECONDS)

  type SystemId = String
  val certificateCache: Map[SystemId, ActiveCertificate] = Map.empty

  object VehicleMetadataExtractor extends MessageExtractor[VehicleMetadata]

  object VehicleRegistrationKeepaliveExtractor extends MessageExtractor[VehicleRegistrationKeepalive]

  object VehicleRegistrationCertificateExtractor extends MessageExtractor[VehicleRegistrationCertificate]

  override protected def receive: Receive = {
    case delivery @ VehicleMetadataExtractor(message) ⇒ {
      // TODO: Fix to make forward compatible with cts-sensor version of VehicleMetaData
      val augmentedMessage = message.licensePlateData match {
        case None ⇒ message
        case Some(lpd) ⇒ message.copy(license = lpd.license,
          rawLicense = lpd.rawLicense,
          licenseType = lpd.licenseType,
          recognizedBy = lpd.recognizedBy,
          country = lpd.country)
      }

      val consumer = sender // this will be needed in a later version of Akka
      val deliveryTag: Long = delivery.envelope.getDeliveryTag
      hBaseFunctions.saveMetadata(augmentedMessage) match {
        case None ⇒ {
          log.info("metadata processed successfully: {}", augmentedMessage)
          consumer ! Ack(deliveryTag)
        }
        case Some(error) ⇒ {
          log.error("Failed to process metadata ({}): {}", augmentedMessage.eventId, error)
          consumer ! Reject(deliveryTag)
        }
      }
    }
    case delivery @ VehicleRegistrationKeepaliveExtractor(message) ⇒ {
      val consumer = sender // this will be needed in a later version of Akka
      val deliveryTag: Long = delivery.envelope.getDeliveryTag
      handleKeepalive(message, deliveryTag, consumer)
    }
    case delivery @ VehicleRegistrationCertificateExtractor(message) ⇒ {
      val consumer = sender // this will be needed in a later version of Akka
      val deliveryTag: Long = delivery.envelope.getDeliveryTag
      certificateCache.get(message.systemId) match {
        case Some(certificate) if (certificate == message.activeCertificate) ⇒ {
          log.info("No certificate update for system {}", message.systemId)
          consumer ! Ack(deliveryTag)
        }
        case _ ⇒ {
          try {
            certificateService.saveActiveCertificate(curator, message.systemId, message.activeCertificate)
            log.info("Updated certificate for system {}", message.systemId)
            certificateCache.put(message.systemId, message.activeCertificate)
            consumer ! Ack(deliveryTag)
          } catch {
            case e: Exception ⇒ {
              log.error(e, "Error while saving data in Curator")
              consumer ! Reject(deliveryTag)
            }
          }
        }
      }
    }
    case delivery: Delivery ⇒ {
      val consumer = sender // this will be needed in a later version of Akka
      val deliveryTag: Long = delivery.envelope.getDeliveryTag
      log.error("Rejecting message with deliveryTag {} - cannot determine message type", deliveryTag)
      consumer ! Reject(deliveryTag)
    }
    case ok: Ok ⇒ log.debug("Ok from AMQP")
    case unhandled: Any ⇒ {
      log.warning("Unhandled message: {}", unhandled)
    }
  }

  def handleKeepalive(message: VehicleRegistrationKeepalive, deliveryTag: Long, consumer: ActorRef): Unit = {
    val future = keepalivePersister ? message
    future.onSuccess {
      case None ⇒ {
        log.info("keepalive processed successfully: {}", message)
        consumer ! Ack(deliveryTag)
      }
      case Some(error) ⇒ {
        log.error("Failed to process keepalive ({}): {}", message.lane, error)
        consumer ! Reject(deliveryTag)
      }
    }.onFailure {
      case e: Throwable ⇒ {
        log.error("Failed to process keepalive ({}): {}", message.lane, e.getMessage)
        consumer ! Reject(deliveryTag)
      }
    }
  }
}
