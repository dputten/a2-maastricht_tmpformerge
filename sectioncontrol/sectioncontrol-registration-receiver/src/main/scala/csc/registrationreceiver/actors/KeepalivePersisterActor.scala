package csc.registrationreceiver.actors

import akka.actor.{ Props, ActorLogging, Actor }
import csc.registrationreceiver.HBaseFunctions
import csc.registrationreceiver.protocol.VehicleRegistrationKeepalive

/**
 * Created by carlos on 14.10.15.
 */
object KeepalivePersisterActor {

  type State = Map[String, Long] //laneId -> last processedTime

  def props(hBaseFunctions: HBaseFunctions) = Props(new KeepalivePersisterActor(hBaseFunctions))

}

import KeepalivePersisterActor._

class KeepalivePersisterActor(hBaseFunctions: HBaseFunctions) extends Actor with ActorLogging {

  override protected def receive: Receive = {
    case msg: InitialState ⇒ context.become(receiveWithState(msg.state))
  }

  def receiveWithState(state: State): Receive = {
    case msg: VehicleRegistrationKeepalive ⇒ {
      val client = sender
      val key = msg.lane.laneId
      val result = persist(msg, state.get(key))
      client ! result.error
      if (result.isPersisted) context.become(receiveWithState(state.updated(key, msg.processedTime)))
    }
  }

  def persist(msg: VehicleRegistrationKeepalive, current: Option[Long]): PersistResult = current match {
    case None ⇒ PersistResult(hBaseFunctions.saveKeepalive(msg), true)
    case Some(value) ⇒ scala.math.signum(msg.processedTime - value) match {
      case -1 ⇒
        log.warning("Ignoring message with older timestamp: " + msg)
        PersistResult(None, false)
      case 0 ⇒ PersistResult(None, false) //same timestamp: no point in saving
      case _ ⇒ PersistResult(hBaseFunctions.saveKeepalive(msg), true)
    }
  }

  override def preStart(): Unit = {
    super.preStart()
    val state = hBaseFunctions.laneProcessingTimes()
    self ! InitialState(state)
  }

}

case class InitialState(state: State)

case class PersistResult(error: Option[String], done: Boolean) {
  def isPersisted = error.isEmpty && done
}
