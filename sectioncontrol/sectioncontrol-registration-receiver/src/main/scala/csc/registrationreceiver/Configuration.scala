/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.registrationreceiver

import java.io.File
import akka.util.Duration
import akka.util.duration._
import com.typesafe.config.{ ConfigFactory, Config }
import csc.akkautils.DirectLogging

/**
 * Configuration class for registration receiver
 *
 * @author csomogyi
 */
class Configuration(config: Config) {
  import Configuration._
  private val root = config.getConfig("registration-receiver")

  val amqpConnection = new AmqpConfiguration(root.getConfig("amqp-connection"))
  val amqpServices = new AmqpServicesConfiguration(root.getConfig("amqp-services"))
  val hBase = new HBaseConfiguration(root.getConfig("hbase"))
  val zookeeper = new ZookeeperConfiguration(root.getConfig("zookeeper"))
}

object Configuration extends DirectLogging {
  class AmqpConfiguration(config: Config) {
    val actorName = config.getString("actor-name")
    val amqpUri = config.getString("amqp-uri")
    val reconnectDelay = config.getMilliseconds("reconnect-delay").toLong.millis
  }

  class AmqpServicesConfiguration(val consumerQueue: String, val serversCount: Int) {
    def this(config: Config) = this(config.getString("consumer-queue"), config.getInt("servers-count"))
  }

  class HBaseConfiguration(val servers: String) {
    def this(config: Config) = this(config.getString("servers"))
  }

  class ZookeeperConfiguration(val servers: String, val sessionTimeout: Duration, val retries: Int) {
    def this(config: Config) = this(config.getString("servers"),
      config.getMilliseconds("session-timeout").toLong.millis,
      config.getInt("retries"))
  }

  private val ConfigFileName = "registrationReceiver.conf"

  def load(config: Config): Configuration = {
    new Configuration(config)
  }
}
