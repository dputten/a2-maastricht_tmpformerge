package csc.registrationreceiver

import csc.sectioncontrol.messages.ProcessingState

/**
 * @author csomogyi
 */
object RegistrationProcessingMessages {
  val ComponentName = "RegistrationReceiver"
}
