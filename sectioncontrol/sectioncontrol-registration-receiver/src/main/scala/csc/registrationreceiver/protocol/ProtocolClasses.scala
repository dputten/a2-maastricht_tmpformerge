/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.registrationreceiver.protocol

import csc.sectioncontrol.messages.Lane
import csc.sectioncontrol.messages.certificates.ActiveCertificate

/**
 * Message class for vehicle registration keepalives. For each lane we maintain the last processed time and a
 * delay we calculate from the difference the of registration creation time and the time when it arrives in the
 * registration completion component (`StoreVehicleRegistrationAmqp`). We produce an exponential moving average
 * of the delays (see Wikipedia) and using this delay and a constant the last processed time is corrected. This time
 * is sent periodically using the keepalive message.
 *
 * The keepalive is saved by the registration receiver in the database (in the processing table). This will be later
 * used for correcting the end time of processing (matching) window.
 *
 * @author csomogyi
 */
case class VehicleRegistrationKeepalive(lane: Lane, processedTime: Long)

/**
 * Message used to update the component certificate related to the vehicle registration in the Curator / Zookeeper.
 *
 * @param systemId the system id of the current vehicle registration
 * @param gantryId the gantry id of the current vehicle registration
 * @param activeCertificate the certificate created by the vehicle registration
 */
case class VehicleRegistrationCertificate(systemId: String, gantryId: String, activeCertificate: ActiveCertificate)