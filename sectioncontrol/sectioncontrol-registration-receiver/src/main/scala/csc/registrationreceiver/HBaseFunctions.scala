
package csc.registrationreceiver

import csc.amqp.JsonSerializers
import csc.hbase.utils.HBaseTableKeeper
import csc.registrationreceiver.protocol.VehicleRegistrationKeepalive
import net.liftweb.json.Formats

import csc.akkautils.DirectLogging
import csc.sectioncontrol.messages._
import csc.sectioncontrol.storagelayer.hbasetables.{ ProcessingTable, VehicleTable }
import org.apache.hadoop.conf.{ Configuration ⇒ HadoopConfiguration }

/**
 * Utility class for performing HBase related functions
 *
 * @author csomogyi
 */
trait HBaseFunctions {
  def saveMetadata(metadata: VehicleMetadata): Option[String]
  def saveKeepalive(keepalive: VehicleRegistrationKeepalive): Option[String]
  protected def saveKeepalive(lane: Lane, record: ProcessingRecord): Option[String]
  def laneProcessingTimes(): Map[String, Long]
}

class HBaseFunctionsImpl(config: HadoopConfiguration, tableKeeper: HBaseTableKeeper, jsonFormats: Formats)
  extends DirectLogging with JsonSerializers with HBaseFunctions {

  override implicit val formats = jsonFormats

  val vehicleTableWriter = VehicleTable.makeWriter(config, tableKeeper)
  val processingTableWriter = ProcessingTable.makeWriter(config, tableKeeper)
  val processingTableReader = ProcessingTable.makeReader(config, tableKeeper)

  /**
   * Saves the vehicle metadata in the vehicle table
   *
   * @param metadata
   * @return
   */
  def saveMetadata(metadata: VehicleMetadata): Option[String] = {
    toJson[VehicleMetadata](metadata) match {
      case Right(json) ⇒ {
        vehicleTableWriter.writeMetadata(metadata)
        None
      }
      case Left(error) ⇒ {
        log.error("Failed to serialize meta data to JSON - {}", error)
        Some("JSON serialization error - " + error)
      }
    }
  }

  /**
   * Saves the keepalive in the processing table
   *
   * @param keepalive
   * @return
   */
  def saveKeepalive(keepalive: VehicleRegistrationKeepalive): Option[String] = {
    val lane = keepalive.lane.laneId
    val componentName = RegistrationProcessingMessages.ComponentName + "@" + lane
    val record = GantryTimeProcessingRecord.create(keepalive.lane, keepalive.processedTime)
    saveKeepalive(keepalive.lane, record)
  }

  protected def saveKeepalive(lane: Lane, record: ProcessingRecord): Option[String] = {
    processingTableWriter.writeGantryTimeRecord(lane, record)
    None
  }

  /**
   * @return map of laneId to its currently stored processing time
   */
  def laneProcessingTimes(): Map[String, Long] = {
    val records = processingTableReader.getAllGantryTimes()
    records map { r ⇒ r.history(0).notes -> r.time } toMap
  }
}
