/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.registrationreceiver.test

import csc.sectioncontrol.messages._

import scala.collection.mutable.ListBuffer

/**
 * @author csomogyi
 */
trait SampleData {
  val sampleImages: List[VehicleImage] = {
    val images: ListBuffer[VehicleImage] = ListBuffer()

    images += new VehicleImage(
      timestamp = 12340010,
      offset = 0,
      uri = "/path/to/overview.jpg",
      imageType = VehicleImageType.Overview,
      checksum = "overview sha",
      timeYellow = Some(123),
      timeRed = Some(789))

    images += new VehicleImage(
      timestamp = 12340010,
      offset = 0,
      uri = "/path/to/overviewMasked.jpg",
      imageType = VehicleImageType.OverviewMasked,
      checksum = "overviewMasked sha",
      timeYellow = Some(123),
      timeRed = Some(789))

    images += new VehicleImage(
      timestamp = 12340010,
      offset = 0,
      uri = "/path/to/overviewRedLight.jpg",
      imageType = VehicleImageType.OverviewRedLight,
      checksum = "overviewRedLight sha",
      timeYellow = Some(123),
      timeRed = Some(789))

    images += new VehicleImage(
      timestamp = 12340010,
      offset = 0,
      uri = "/path/to/overviewSpeed.jpg",
      imageType = VehicleImageType.OverviewSpeed,
      checksum = "overviewSpeed sha",
      timeYellow = Some(123),
      timeRed = Some(789))

    images += new VehicleImage(
      timestamp = 12340010,
      offset = 0,
      uri = "/path/to/redLight.jpg",
      imageType = VehicleImageType.RedLight,
      checksum = "redLight sha")

    images += new VehicleImage(
      timestamp = 12340010,
      offset = 0,
      uri = "/path/to/measureMethod2Speed.jpg",
      imageType = VehicleImageType.MeasureMethod2Speed,
      checksum = "measureMethod2Speed sha",
      timeRed = Some(789))

    images.toList
  }

  val sampleVehicleMetadata: VehicleMetadata = {

    val lane = new Lane(
      laneId = "the lane",
      name = "lane name",
      gantry = "gantry",
      system = "system",
      sensorGPS_longitude = 23.44,
      sensorGPS_latitude = 165.33)

    new VehicleMetadata(
      lane = lane,
      eventId = "anId",
      eventTimestamp = 12340000,
      eventTimestampStr = Some("an event timestamp string"),
      license = Some(new ValueWithConfidence[String]("license")),
      rawLicense = Some("licence"),
      length = Some(new ValueWithConfidence_Float(2.5F)),
      category = Some(new ValueWithConfidence[String]("category")),
      speed = Some(new ValueWithConfidence_Float(120.00F, 45)),
      country = Some(new ValueWithConfidence[String]("country", 80)),
      images = sampleImages,
      NMICertificate = "nmiCertificate",
      applChecksum = "appChecksum",
      serialNr = "SN1234")
  }

  val sampleVehicleMetadataJson: String = {
    val lane =
      """{ "laneId": "the lane", "name": "lane name", "gantry": "gantry", "system": "system",""" +
        """ "sensorGPS_longitude": 23.44, "sensorGPS_latitude": 165.33 }"""
    val eventId = "\"anId\""
    val eventTimestamp = "12340000"
    val eventTimestampStr = "\"an event timestamp string\""
    val license = """{ "value": "license", "confidence": 0 }"""
    val rawLicense = "\"licence\""
    val length = """{ "value": 2.5, "confidence": 0 }"""
    val category = """{ "value": "category", "confidence": 0 }"""
    val speed = """{ "value": 120.0, "confidence": 45 }"""
    val country = """{ "value": "country", "confidence": 80 }"""

    def renderImage(buf: StringBuilder, image: VehicleImage): Unit = {
      buf ++= """{ "timestamp": %d, "offset": %d, "uri": "%s", "imageType": "VehicleImageType$%s", "checksum": "%s", """.format(
        image.timestamp, image.offset, image.uri, image.imageType.toString, image.checksum)
      if (image.timeYellow.isDefined)
        buf ++= """, "timeYellow": %d """.format(image.timeYellow.get)
      if (image.timeRed.isDefined)
        buf ++= """, "timeRed": %d """.format(image.timeRed.get)
      buf ++= "}"
    }

    val images = {
      val buf = new StringBuilder
      buf ++= "["
      renderImage(buf, sampleImages.head)
      for (image ← sampleImages.tail) {
        buf ++ ","
        renderImage(buf, image)
      }
      buf ++= "]"
      buf.toString()
    }

    val NMICertificate = "\"nmiCertificate\""
    val applChecksum = "\"appChecksum\""
    val serialNr = "\"SN1234\""
    val envelope = """{ "lane": %s, "eventId": %s, "eventTimestamp": %s, "eventTimestampStr": %s, "license": %s, "rawLicense": %s, """ +
      """ "length": %s, "category": %s, "speed": %s, "country": %s, "images": %s, "NMICertificate": %s, "applChecksum": %s, "serialNr": %s }"""
    envelope.format(lane, eventId, eventTimestamp, eventTimestampStr, license, rawLicense, length, category, speed,
      country, images, NMICertificate, applChecksum, serialNr)
  }
}
