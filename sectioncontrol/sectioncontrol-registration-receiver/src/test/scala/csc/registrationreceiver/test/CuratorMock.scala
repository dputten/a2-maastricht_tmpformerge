package csc.registrationreceiver.test

import csc.config.Path
import csc.curator.utils.{ Versioned, Curator }
import csc.json.lift.LiftSerialization
import org.apache.curator.framework.CuratorFramework
import org.apache.zookeeper.data.Stat
import scala.Predef
import scala.collection.mutable.Map

/**
 * @author csomogyi
 */
class CuratorMock extends Curator {
  val map: Map[String, Versioned[AnyRef]] = Map()

  def clear() = map.empty

  override def curator: Option[CuratorFramework] = throw new UnsupportedOperationException()

  override def set[T <: AnyRef](path: String, value: T, storedVersion: Int): Unit = {
    val versioned = getVersioned[AnyRef](path)
    val version = versioned match {
      case Some(v) ⇒ v.version + 1
      case None    ⇒ 1
    }
    map.put(path, Versioned[AnyRef](value, version))
  }

  override def set[T <: AnyRef](path: Path, value: T, storedVersion: Int): Unit = throw new UnsupportedOperationException()

  override def get[T <: AnyRef](path: String)(implicit evidence$4: Manifest[T]): Option[T] = throw new UnsupportedOperationException()

  override def get[T <: AnyRef](path: Path)(implicit evidence$5: Manifest[T]): Option[T] = throw new UnsupportedOperationException()

  override def get[T <: AnyRef](pathList: Seq[Path])(implicit evidence$6: Manifest[T]): Option[T] = throw new UnsupportedOperationException()

  override def get[T <: AnyRef](pathList: Seq[Path], default: T)(implicit evidence$7: Manifest[T]): Option[T] = throw new UnsupportedOperationException()

  override def get[T <: AnyRef](pathList: Seq[Path], defaults: Predef.Map[String, Any])(implicit evidence$8: Manifest[T]): Option[T] = throw new UnsupportedOperationException()

  override def appendEvent[T <: AnyRef](path: String, value: T): Unit = throw new UnsupportedOperationException()

  override def appendEvent[T <: AnyRef](path: String, value: T, retries: Int): Unit = throw new UnsupportedOperationException()

  override def appendEvent[T <: AnyRef](path: Path, value: T): Unit = throw new UnsupportedOperationException()

  override def appendEvent[T <: AnyRef](path: Path, value: T, retries: Int): Unit = throw new UnsupportedOperationException()

  override def put[T <: AnyRef](path: String, value: T, ephemeral: Boolean = false): Unit = map.put(path, Versioned[AnyRef](value, 1))

  override def put[T <: AnyRef](path: Path, value: T): Unit = throw new UnsupportedOperationException()

  override def serialization: LiftSerialization = throw new UnsupportedOperationException()

  override def delete(path: String): Unit = throw new UnsupportedOperationException()

  override def delete(path: Path): Unit = throw new UnsupportedOperationException()

  override def getVersioned[T <: AnyRef](path: String)(implicit evidence$9: Manifest[T]): Option[Versioned[T]] =
    map.get(path).map(v ⇒ Versioned[T](v.data.asInstanceOf[T], v.version))

  override def getVersioned[T <: AnyRef](path: Path)(implicit evidence$10: Manifest[T]): Option[Versioned[T]] = throw new UnsupportedOperationException()

  override def getVersioned[T <: AnyRef](pathList: Seq[Path], default: T)(implicit evidence$11: Manifest[T]): Option[Versioned[T]] = throw new UnsupportedOperationException()

  override def getChildNames(parent: String): Seq[String] = throw new UnsupportedOperationException()

  override def getChildNames(parent: Path): Seq[String] = throw new UnsupportedOperationException()

  override def createEmptyPath(path: String): Unit = throw new UnsupportedOperationException()

  override def createEmptyPath(path: Path): Unit = throw new UnsupportedOperationException()

  override def safeDelete(path: String): Unit = throw new UnsupportedOperationException()

  override def safeDelete(path: Path): Unit = throw new UnsupportedOperationException()

  override def getChildren(parent: String): Seq[String] = throw new UnsupportedOperationException()

  override def getChildren(parent: Path): Seq[Path] = throw new UnsupportedOperationException()

  override def deleteRecursive(path: String): Unit = throw new UnsupportedOperationException()

  override def deleteRecursive(path: Path): Unit = throw new UnsupportedOperationException()

  override def exists(path: String): Boolean = throw new UnsupportedOperationException()

  override def exists(path: Path): Boolean = throw new UnsupportedOperationException()

  override def stat(path: String): Option[Stat] = throw new UnsupportedOperationException()

  override def stat(path: Path): Option[Stat] = throw new UnsupportedOperationException()
}
