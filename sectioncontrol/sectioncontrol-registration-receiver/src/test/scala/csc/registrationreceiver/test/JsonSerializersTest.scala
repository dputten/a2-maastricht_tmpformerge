/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.registrationreceiver.test

import csc.amqp.JsonSerializers
import csc.registrationreceiver.Boot
import csc.registrationreceiver.protocol.{ VehicleRegistrationCertificate, VehicleRegistrationKeepalive }
import csc.sectioncontrol.messages.VehicleMetadata
import csc.sectioncontrol.messages.certificates.{ ActiveCertificate, ComponentCertificate }
import net.liftweb.json._
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

/**
 * @author csomogyi
 */
class JsonSerializersTest extends WordSpec with MustMatchers with JsonSerializers with SampleData {
  override implicit val formats = Boot.jsonFormats

  "JsonSerializers" must {
    "serialize VehicleMetadata" in {
      val payload = toJson(sampleVehicleMetadata)
      assert(payload.isRight, if (payload.isLeft) payload.left.get else "No serialization error")
      val json = parse(payload.right.get)
      var reference = parse(sampleVehicleMetadataJson)
      assert(json === reference)
    }
    "serialize VehicleRegistrationCertificate" in {
      val payload = toJson(VehicleRegistrationCertificate("one", "two",
        ActiveCertificate(123456789012L, ComponentCertificate("name1", "checksum2"))))
      assert(payload.isRight, if (payload.isLeft) payload.left.get else "No serialization error")
      val json = parse(payload.right.get)
      assert(json \\ "systemId" === JString("one"))
      assert(json \\ "gantryId" === JString("two"))
      assert(json \\ "activeCertificate" \\ "time" === JInt(123456789012L))
      assert(json \\ "activeCertificate" \\ "componentCertificate" \\ "name" === JString("name1"))
      assert(json \\ "activeCertificate" \\ "componentCertificate" \\ "checksum" === JString("checksum2"))
    }
    "deserialize VehicleMetadata" in {
      val message = fromJson[VehicleMetadata](sampleVehicleMetadataJson)
      assert(message.right.get === sampleVehicleMetadata)
    }
    "deserialize VehicleRegistrationCertificate" in {
      val payload =
        """{ "systemId": "one", "gantryId": "two",
          |"activeCertificate": { "time": 123456789012,
          |"componentCertificate": { "name": "name1", "checksum": "checksum2" } } }""".stripMargin
      val message = fromJson[VehicleRegistrationCertificate](payload)
      assert(message.isRight, if (message.isLeft) message.left.get else "No deserialization error")
      assert(message.right.get ===
        VehicleRegistrationCertificate("one", "two", ActiveCertificate(123456789012L, ComponentCertificate("name1", "checksum2"))))
    }
  }
}
