/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.registrationreceiver.test

import com.github.sstone.amqp.Amqp.Delivery
import com.rabbitmq.client.AMQP
import csc.amqp.AmqpSerializers
import csc.registrationreceiver.Boot
import csc.registrationreceiver.protocol.VehicleRegistrationCertificate
import csc.sectioncontrol.messages.VehicleMetadata
import csc.sectioncontrol.messages.certificates.{ ActiveCertificate, ComponentCertificate }
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.akkautils.DirectLogging

/**
 * @author csomogyi
 */
class AmqpSerializersTest extends WordSpec with MustMatchers with AmqpSerializers with SampleData with DirectLogging {
  override implicit val formats = Boot.jsonFormats
  override val ApplicationId = "test"

  "AmqpSerializers" must {
    "serialize and de-serialize VehicleMetadata" in {
      val payload = toPublishPayload(sampleVehicleMetadata)
      assert(payload.isRight, if (payload.isLeft) payload.left.get else "No serialization error")
      val (body, properties) = payload.right.get
      assert(properties.getContentType === "application/json; charset=UTF-8")
      assert(properties.getContentEncoding === "gzip")
      assert(properties.getAppId === "test")
      assert(properties.getType === "VehicleMetadata")

      val builder = buildProperties(manifest[VehicleMetadata].erasure.getSimpleName)
      val delivery = Delivery("consumerTag", null, builder.build(), body)
      val result = toMessage[VehicleMetadata](delivery)
      result.isRight must be(true)
      result.right.get must be(sampleVehicleMetadata)
    }
    "serialize and de-serialize VehicleRegistrationCertificate" in {
      val message = VehicleRegistrationCertificate("one", "two", ActiveCertificate(123456789012L, ComponentCertificate("name1", "checksum2")))
      val payload = toPublishPayload(message)
      assert(payload.isRight, if (payload.isLeft) payload.left.get else "No serialization error")
      val (body, properties) = payload.right.get
      assert(properties.getContentType === "application/json; charset=UTF-8")
      assert(properties.getContentEncoding === "gzip")
      assert(properties.getAppId === "test")
      assert(properties.getType === "VehicleRegistrationCertificate")

      val builder = buildProperties(manifest[VehicleRegistrationCertificate].erasure.getSimpleName)
      val delivery = Delivery("consumerTag", null, builder.build(), body)
      val result = toMessage[VehicleRegistrationCertificate](delivery)
      result.isRight must be(true)
      result.right.get must be(message)
    }

    "de-serialize VehicleMetadata not conform rfc-2616" in {
      val builder = new AMQP.BasicProperties.Builder()
      val properties = builder.`type`("VehicleMetadata").contentType("application/json").contentEncoding("UTF-8").build()
      val delivery = Delivery("", null, properties, sampleVehicleMetadataJson.getBytes("UTF-8"))
      val message = toMessage[VehicleMetadata](delivery)
      assert(message.isRight, if (message.isLeft) message.left.get else "No serialization error")
      assert(message.right.get === sampleVehicleMetadata)
    }
    "de-serialize VehicleRegistrationCertificate not conform rfc-2616" in {
      val original = VehicleRegistrationCertificate("one", "two", ActiveCertificate(123456789012L, ComponentCertificate("name1", "checksum2")))
      val payload = toJson[VehicleRegistrationCertificate](original).right.get
      val builder = new AMQP.BasicProperties.Builder()
      val properties = builder.`type`("VehicleRegistrationCertificate").contentType("application/json").contentEncoding("UTF-8").build()
      val delivery = Delivery("", null, properties, payload.getBytes("UTF-8"))
      val message = toMessage[VehicleRegistrationCertificate](delivery)
      assert(message.isRight, if (message.isLeft) message.left.get else "No serialization error")
      assert(message.right.get === original)
    }
  }
}
