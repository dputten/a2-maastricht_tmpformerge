package csc.registrationreceiver.actors

import java.util.concurrent.TimeUnit

import akka.actor.{ ActorRef, ActorSystem }
import akka.dispatch.Await
import akka.pattern.ask
import akka.testkit.{ TestActorRef, TestKit }
import akka.util.Timeout
import csc.akkautils.DirectLogging
import csc.registrationreceiver.HBaseFunctions
import csc.registrationreceiver.protocol.VehicleRegistrationKeepalive
import csc.sectioncontrol.messages.{ ProcessingRecord, Lane, VehicleMetadata }
import org.scalatest.{ BeforeAndAfterEach, BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers

/**
 * Created by carlos on 15.10.15.
 */
class KeepalivePersisterActorTest
  extends TestKit(ActorSystem("keepalivePersisterTest"))
  with WordSpec
  with MustMatchers
  with BeforeAndAfterAll
  with BeforeAndAfterEach
  with DirectLogging {

  implicit val timeout: Timeout = Timeout(1, TimeUnit.SECONDS)

  var hbaseFunctions: MockHBaseFunctions = null
  var actor: ActorRef = null

  override protected def afterAll(): Unit = {
    super.afterAll()
    system.shutdown()
  }

  override protected def beforeEach(): Unit = {
    hbaseFunctions = new MockHBaseFunctions
    actor = TestActorRef(new KeepalivePersisterActor(hbaseFunctions))
  }

  import SampleData._

  "KeepalivePersisterActor" must {
    "Loads correctly the initial state" in {
      //there's no way to check the initial state (inside the actor)
      //so we try an older time (so it does not accept) and check the persisted in the end
      assertPersisted(initialLaneId, initialLaneTime) //baseline check
      val msg = createKeepalive(initialLaneId, initialLaneTime - 1) //sending an older time
      val res = sendAndGetResult(msg)
      res must be(None)
      assertPersisted(initialLaneId, initialLaneTime) //we still have the initial time
    }

    "Persist and reply to a valid keepalive" in {
      val msg = createKeepalive("laneId", System.currentTimeMillis())
      val res = sendAndGetResult(msg)
      res must be(None)
      assertPersisted(msg)
    }

    "Ignore and reply to an older keepalive" in {
      val laneId = "laneId"
      val msg1 = createKeepalive(laneId, 100)
      val res1 = sendAndGetResult(msg1)
      res1 must be(None)
      assertPersisted(msg1)

      val msg2 = createKeepalive(laneId, 99) //sending an older time
      val res2 = sendAndGetResult(msg2)
      res2 must be(None)
      assertPersisted(msg1) //we still have the highest time
    }

    "Reply back on some persistence error" in {
      val msg = createKeepalive(errorLaneId, 100)
      val res = sendAndGetResult(msg)
      res must be(Some(persistenceErrorMessage))
    }
  }

  def sendAndGetResult(msg: VehicleRegistrationKeepalive): Option[String] = {
    val future = actor ? msg
    Await.result(future, timeout.duration).asInstanceOf[Option[String]]
  }

  def assertPersisted(laneId: String, time: Long): Unit =
    hbaseFunctions.getProcessedTime(laneId) must be(Some(time))

  def assertPersisted(record: VehicleRegistrationKeepalive): Unit =
    assertPersisted(record.lane.laneId, record.processedTime)

}