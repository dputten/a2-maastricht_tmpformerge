package csc.registrationreceiver.actors

import csc.curator.utils.Curator
import csc.registrationreceiver.HBaseFunctions
import csc.registrationreceiver.protocol.VehicleRegistrationKeepalive
import csc.sectioncontrol.messages.certificates.{ ActiveCertificate, ComponentCertificate, MeasurementMethodInstallationType }
import csc.sectioncontrol.messages.{ ProcessingRecord, Lane, VehicleMetadata }
import csc.sectioncontrol.sign.certificate.CertificateService
import csc.util.test.ObjectBuilder

/**
 * Created by carlos on 16.10.15.
 */
object SampleData extends ObjectBuilder {

  val errorLaneId = "errorLane"
  val initialLaneId = "initialLane"
  val persistenceErrorMessage = "some dummy persistence error"
  val initialLaneTime = 10

  lazy val activeCertificate = create[ActiveCertificate]

  lazy val keepalivePersisterBehavior: PartialFunction[Any, Any] = {
    case msg: VehicleRegistrationKeepalive ⇒ msg.lane.laneId match {
      case lane if lane == errorLaneId ⇒ Some(persistenceErrorMessage)
      case _                           ⇒ None
    }
  }

  def createKeepalive(laneId: String, time: Long = System.currentTimeMillis()): VehicleRegistrationKeepalive = {
    val lane = Lane(laneId, laneId, "system", "gantry", 0, 0, None)
    VehicleRegistrationKeepalive(lane, time)
  }
}

class MockHBaseFunctions extends HBaseFunctions {

  import SampleData._

  var map: Map[String, Long] = Map(initialLaneId -> initialLaneTime)

  override def saveMetadata(metadata: VehicleMetadata): Option[String] = None

  override def laneProcessingTimes(): Map[String, Long] = map

  override protected def saveKeepalive(lane: Lane, record: ProcessingRecord): Option[String] = {
    if (lane.laneId == errorLaneId) {
      Some(persistenceErrorMessage)
    } else {
      map = map.updated(lane.laneId, record.time)
      None
    }
  }
  def saveKeepalive(keepalive: VehicleRegistrationKeepalive): Option[String] = {
    if (keepalive.lane.laneId == errorLaneId) {
      Some(persistenceErrorMessage)
    } else {
      map = map.updated(keepalive.lane.laneId, keepalive.processedTime)
      None
    }
  }

  def getProcessedTime(laneId: String): Option[Long] = map.get(laneId)

}

class MockCertificateService extends CertificateService {

  private var activeCertificatesMap: Map[String, ActiveCertificate] = Map.empty

  override def saveActiveCertificate(curator: Curator, systemId: String, certificate: ActiveCertificate): Unit = {
    activeCertificatesMap = activeCertificatesMap.updated(systemId, certificate)
  }

  def certificates: Map[String, ActiveCertificate] = activeCertificatesMap

  def getActiveConfigurationCertificate(curator: Curator, systemId: String, time: Long = System.currentTimeMillis()): ComponentCertificate = null

  def getActiveSoftwareCertificate(curator: Curator, systemId: String, time: Long = System.currentTimeMillis()): ComponentCertificate = null

  def getCurrentCertificate(curator: Curator, systemId: String, time: Long): Option[MeasurementMethodInstallationType] = None
}

