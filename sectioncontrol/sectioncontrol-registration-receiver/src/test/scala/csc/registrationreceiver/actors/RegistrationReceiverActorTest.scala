package csc.registrationreceiver.actors

import java.util.concurrent.TimeUnit

import akka.dispatch.Await
import akka.pattern.ask
import akka.actor.{ ActorRef, Props, ActorSystem }
import akka.testkit.{ ImplicitSender, TestActorRef, TestKit }
import akka.util.Timeout
import csc.amqp.test.{ Rejected, Acked, Outcome, AmqpClientActor }
import csc.util.test._
import csc.akkautils.DirectLogging
import csc.registrationreceiver.Boot
import csc.registrationreceiver.protocol.{ VehicleRegistrationCertificate, VehicleRegistrationKeepalive }
import org.scalatest.{ BeforeAndAfterEach, BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers

/**
 * Created by carlos on 16.10.15.
 */
class RegistrationReceiverActorTest
  extends TestKit(ActorSystem("registrationReceiverTest"))
  with WordSpec
  with MustMatchers
  with BeforeAndAfterAll
  with BeforeAndAfterEach
  with DirectLogging
  with ImplicitSender {

  import SampleData._

  val jsonFormats = Boot.jsonFormats
  implicit val timeout: Timeout = Timeout(1, TimeUnit.SECONDS)

  def testSelf: ActorRef = self

  var hbaseFunctions: MockHBaseFunctions = null
  var certificateService: MockCertificateService = null
  var keepalivePersister: ActorRef = null
  var actor: ActorRef = null
  var client: ActorRef = null

  override protected def beforeEach(): Unit = {
    super.beforeEach()
    hbaseFunctions = new MockHBaseFunctions
    certificateService = new MockCertificateService
    //we mock keepalivePersister behaviour, and collect whatever it receives
    keepalivePersister = system.actorOf(MockActor.props(keepalivePersisterBehavior, Some(testSelf)))
    actor = TestActorRef(new RegistrationReceiverActor(hbaseFunctions, keepalivePersister, null, certificateService, jsonFormats))
    client = system.actorOf(Props(new AmqpClientActor(actor, jsonFormats)))
  }

  override protected def afterAll(): Unit = {
    super.afterAll()
    system.shutdown()
  }

  import SampleData._

  "RegistrationReceiverActor" must {
    "handle and ack a valid keepalive message" in {

      val msg = createKeepalive("laneId")
      val outcome = sendMessage(msg)
      outcome must be(Acked)
      expectMsg(msg) //checking keepalivePersister received the message
    }
    "handle and reject a keepalive message that fails to persist" in {

      val msg = createKeepalive(errorLaneId)
      val outcome = sendMessage(msg)
      outcome must be(Rejected)
      expectMsg(msg) //checking keepalivePersister received the message
    }

    "handle and ack a VehicleRegistrationCertificate message" in {

      val msg = VehicleRegistrationCertificate("systemId", "gantryId", activeCertificate)
      certificateService.certificates.get(msg.systemId) must be(None) //baseline check
      val outcome = sendMessage(msg)
      outcome must be(Acked)
      certificateService.certificates.get(msg.systemId) must be(Some(activeCertificate))
    }
  }

  def sendMessage(msg: AnyRef): Outcome = {
    val future = client ? msg
    Await.result(future, timeout.duration).asInstanceOf[Outcome]
  }
}
