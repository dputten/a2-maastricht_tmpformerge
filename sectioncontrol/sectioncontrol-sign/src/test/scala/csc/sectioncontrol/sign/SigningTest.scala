/*
* Copyright (C) 2012 CSC. <http://www.csc.com>
*/
package csc.sectioncontrol.sign

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import java.io.File

class SigningTest extends WordSpec with MustMatchers {
  "Signing" must {
    val folder = System.getProperty("user.dir")
    val file = new File(folder, "sectioncontrol-sign/src/test/resources/data.txt")
    val sha = Signing.digest(file.getAbsolutePath())

    "generate proper signature" in {
      sha must be("4b1eba94bf972ffb50f278868cd774")
    }
    "generate 31 character checksum" in {
      val json = """{"dateTime":1373407360041,"detector":1,"direction":"Outgoing","speed":40.0,"length":5.300000190734863,"loop1RiseTime":0,"loop1FallTime":615,"loop2RiseTime":228,"loop2FallTime":841,"yellowTime2":0,"redTime2":0,"dateTime1":1373407359399,"yellowTime1":0,"redTime1":0,"dateTime3":1373407360381,"yellowTime3":0,"redTime3":0,"valid":true}"""
      val sign = "45807435e5c47f4cfad83f85bbd3948" //31 characters
      Signing.calculateHash(Some(json.getBytes)) must be(sign)
    }
  }
}
