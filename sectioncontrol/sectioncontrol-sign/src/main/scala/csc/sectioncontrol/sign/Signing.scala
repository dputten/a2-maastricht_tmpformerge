/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.sign

import java.security.MessageDigest
import sun.misc.BASE64Encoder
import java.math.BigInteger
import java.io.FileInputStream

/**
 * Support object for Signing, encoding and creating digests
 */
object Signing {

  val hashAlgorithm = "MD5" //SHA-256

  /**
   * Create a hash code for given file
   * @param fileName The name of the file
   * @param algorithm the algorithm used to create the hashcode
   */
  def digest(fileName: String, algorithm: String = hashAlgorithm): String = {
    val md = MessageDigest.getInstance(algorithm);
    val input = new FileInputStream(fileName)
    try {
      val buffer = new Array[Byte](1024)
      Stream.continually(input.read(buffer))
        .takeWhile(_ != -1)
        .foreach(md.update(buffer, 0, _))
      getHex(md)
    } finally {
      input.close()
    }
  }

  def calculateHash(data: Option[Array[Byte]], algorithm: String = hashAlgorithm): String = {
    val md = MessageDigest.getInstance(algorithm);
    data.foreach(md.update(_))
    getHex(md)
  }

  private def getHex(md: MessageDigest): String = {
    val number = new BigInteger(1, md.digest)
    number.toString(16)
  }

  /**
   * Encode a string using base64
   */
  def encode(value: String) = new BASE64Encoder().encode(value.getBytes("UTF-8"))
}