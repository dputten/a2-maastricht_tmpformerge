/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.sign

import java.io.File
import java.net.URLDecoder

case class SoftwareChecksum(fileName: String, checksum: String)

/**
 * used to create a checksum of a given file and/or it's own jar file
 */
object SoftwareChecksum {

  /**
   * used to create a checksum of class'es own jar file
   */
  def createHashFromRuntimeClass(obj: AnyRef): Option[SoftwareChecksum] = {
    createHashFromRuntimeClass(obj.getClass)
  }

  def createHashFromRuntimeClass(cl: Class[_]): Option[SoftwareChecksum] = {
    createChecksum(file = new File(cl.getProtectionDomain.getCodeSource.getLocation.getPath))
  }

  def createHashFromFile(file: File): Option[SoftwareChecksum] = {
    createChecksum(file)
  }

  /**
   * used to create a checksum of a given file
   */
  private def createChecksum(file: File): Option[SoftwareChecksum] = {
    if (file.isDirectory) {
      None
    } else if (!file.exists) {
      None
    } else {
      val path = file.getAbsolutePath
      val decodedPath = URLDecoder.decode(path, "UTF-8")

      val hash = Signing.digest(decodedPath)

      Some(SoftwareChecksum(file.getName, hash))
    }
  }
}