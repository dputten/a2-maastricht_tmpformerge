/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.rdwregistry

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

class RdwDataTest extends WordSpec with MustMatchers {
  "RdwData 0001ES0100000000242000000000000000M1" must {
    val rdwData = RdwData.parse("0001ES0100000000242000000000000000M1")
    "be parsed properly" in {
      rdwData.plate must be("0001ES")
      rdwData.vehicleClass must be(1)
      rdwData.deviceCode must be(0)
      rdwData.maxMass must be(0)
      rdwData.wheelBase must be(242)
      rdwData.length must be(0)
      rdwData.width must be(0)
      rdwData.maxSpeed must be(0)
      rdwData.diffMaxSpeed must be(0)
      rdwData.dublicateCode must be(0)
      rdwData.eegCategory must be("M1")
    }
  }

  "RdwData 0003ZB0353115000358000023700000001N2" must {
    val rdwData = RdwData.parse("0003ZB0353115000358000023700000001N2")
    "be parsed properly" in {
      rdwData.plate must be("0003ZB")
      rdwData.vehicleClass must be(03)
      rdwData.deviceCode must be(53)
      rdwData.maxMass must be(11500)
      rdwData.wheelBase must be(358)
      rdwData.length must be(0)
      rdwData.width must be(237)
      rdwData.maxSpeed must be(0)
      rdwData.diffMaxSpeed must be(0)
      rdwData.dublicateCode must be(1)
      rdwData.eegCategory must be("N2")
    }
  }
}
