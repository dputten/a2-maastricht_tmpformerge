/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.rdwregistry

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

class RdwRegistryParserTest extends WordSpec with MustMatchers {
  "RDW parser" must {
    val input = getClass.getResourceAsStream("/rdwdata.txt")
    val iterator = RdwRegistryParser.parseLines(input)
    "find 35 lines in text file" in {
      iterator.size must be(35)
    }
  }

  "RDW parser" must {
    val input = getClass.getResourceAsStream("/FGV#2.txt")
    val iterator = RdwRegistryParser.parseLines(input)
    "find 99 lines in the real file" in {
      iterator.size must be(99)
    }
  }
}
