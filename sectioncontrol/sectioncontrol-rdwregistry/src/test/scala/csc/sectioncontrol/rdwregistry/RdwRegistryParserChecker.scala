/*
* Copyright (C) 2012 CSC. <http://www.csc.com>
*/
package csc.sectioncontrol.rdwregistry

import java.io.BufferedOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import net.liftweb.json.{ DefaultFormats, Serialization }

/**
 * A utility to identify and report inconsistencies in the RDW input file
 */
object RdwRegistryParserChecker extends App {
  val source = new File("/home/mhazewinkel/simdata/20111214_RDW_FLITSNET_bvom.txt")
  val input = new FileInputStream(source)
  val output = new BufferedOutputStream(new FileOutputStream(new File(source.getParentFile(), "OUT.txt")))
  val iterator = RdwRegistryParser.parseLines(input)
  val noLength: scala.collection.mutable.Set[RdwData] = scala.collection.mutable.Set()
  val cat: scala.collection.mutable.Set[RdwData] = scala.collection.mutable.Set()
  val allCategories: scala.collection.mutable.Set[String] = scala.collection.mutable.Set()
  val strangeCategory: scala.collection.mutable.Set[RdwData] = scala.collection.mutable.Set()
  val oWithoutMass: scala.collection.mutable.Set[RdwData] = scala.collection.mutable.Set()
  for (data ← iterator) {
    if (data.length == 0 && data.wheelBase == 0) noLength.add(data)
    if (data.eegCategory.trim.isEmpty() || data.eegCategory.trim.size != 2) cat.add(data)
    allCategories.add(data.eegCategory)
    if (data.eegCategory.startsWith("E") || data.eegCategory.matches("\\d\\d")) strangeCategory.add(data)
    if ((data.eegCategory.startsWith("A") || data.eegCategory.startsWith("O") && (data.maxMass == 0))) oWithoutMass.add(data)

    if (false) {
      implicit val formats = DefaultFormats
      val json = Serialization.write(data)
      val outString = "key=%s, json=%s\n".format(data.plate, json)
      output.write(outString.getBytes("UTF-8"))
    }
    output.close
    println("No Len set size: " + noLength.size)
    val noOld = noLength.iterator.filter(data ⇒ {
      !data.plate.matches("""\D\D\d\d\D\D""") && (data.eegCategory.startsWith("M") || data.eegCategory.startsWith("N"))
    }).toList

    println("No old size: " + noOld.size)

    println("New ones: " + noOld.filter(data ⇒ data.plate.matches("""\d\d\D\D\D\d""")))

    println("Categories: " + allCategories.toList.sortWith {
      case (e1, e2) ⇒ e1 < e2
    })

    println("Strange Category: " + strangeCategory)

    println("O without mass: " + oWithoutMass)
  }
}

