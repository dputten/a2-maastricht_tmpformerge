/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.rdwregistry

trait RdwRegistry {
  /**
   * Get RdwData for the specified NL license plate
   * @param plate - license plate (kentekenen), for instance AB1234
   * @return - RdwData or None if nothing is found
   */
  def getData(plate: String): Option[RdwData]
}

case class RdwData(
  plate: String, // kenteken
  vehicleClass: Int, // Voertuigclassificatie
  deviceCode: Int, // Inrichtingscode
  maxMass: Int, // Maximum massa voertuig, kg
  wheelBase: Int, // Wielbasis, cm
  length: Int, // Lengte voertuig, cm
  width: Int, // Breedte voertuig, cm
  maxSpeed: Int, // Maximum contructiesnelheid, km/h
  diffMaxSpeed: Int, // Afwijkende maximale snelheid, km/h
  dublicateCode: Int, // Kentekenplaat duplicaatcode actueel
  eegCategory: String // EEG/ECE Voertuigcategorie
  ) {
  // req 42 Wanneer het RDW-kentekenregister voor een kenteken geen of foutiev gegevens levert, levert dient de voertuigklasse onbepaald te blijven
  require(plate != null, "Plate is empty")
  require(plate.matches("[A-Z0-9]{6}"), "Plate don't have only chars and numbers [" + plate + "]")
  require(eegCategory != null, "Category is empty")

  /**
   * Wheel base in meters
   */
  lazy val wheelBaseM: Float = wheelBase.toFloat / 100

  /**
   * Length in meters
   */
  lazy val lengthM: Float = length.toFloat / 100

  def hasEmptyFields: Boolean = {
    if (deviceCode == 0 || //valid devicecodes 1..78 and 80..91
      deviceCode == 79 ||
      deviceCode >= 92 ||
      maxMass == 0 ||
      eegCategory == "00" ||
      eegCategory.trim.isEmpty) {
      //data empty
      true
    } else {
      false
    }
  }
}

object RdwData {
  def parse(line: String): RdwData = {
    require(line != null, "Empty line")
    require(line.size == 36, "Line doesn't have the length 36 [" + line + "]")
    val licensePlate = line.substring(0, 6) // kenteken
    val vehicleClass = line.substring(6, 8) // Voertuigclassificatie
    val deviceCode = line.substring(8, 10) // Inrichtingscode
    val maxMass = line.substring(10, 15) // Maximum massa voertuig
    val wheelBase = line.substring(15, 19) // Wielbasis
    val length = line.substring(19, 23) // Lengte voertuig
    val width = line.substring(23, 26) // Breedte voertuig
    val maxSpeed = line.substring(26, 29) // Maximum contructiesnelheid
    val diffMaxSpeed = line.substring(29, 32) // Afwijkende maximale snelheid, km/h
    val dublicateCode = line.substring(32, 34) // Kentekenplaat duplicaatcode actueel
    val eegCategory = line.substring(34) // EEG/ECE Voertuigcategorie
    new RdwData(licensePlate, vehicleClass.toInt, deviceCode.toInt, maxMass.toInt, wheelBase.toInt,
      length.toInt, width.toInt, maxSpeed.toInt, diffMaxSpeed.toInt, dublicateCode.toInt, eegCategory)
  }

  def apply(plate: String, // kenteken
            vehicleClass: Int, // Voertuigclassificatie
            deviceCode: Int, // Inrichtingscode
            maxMass: Int, // Maximum massa voertuig, kg
            wheelBase: Float, // Wielbasis, m
            length: Float, // Lengte voertuig, m
            width: Int, // Breedte voertuig, cm
            maxSpeed: Int, // Maximum contructiesnelheid, km/h
            diffMaxSpeed: Int, // Afwijkende maximale snelheid, km/h
            //dublicateCode: Int, // Kentekenplaat duplicaatcode actueel
            eegCategory: String // EEG/ECE Voertuigcategorie
            ): RdwData = {
    new RdwData(plate, vehicleClass, deviceCode, maxMass,
      (wheelBase * 100).toInt, (length * 100).toInt, 0, maxSpeed, diffMaxSpeed, 0, eegCategory)
  }
}

