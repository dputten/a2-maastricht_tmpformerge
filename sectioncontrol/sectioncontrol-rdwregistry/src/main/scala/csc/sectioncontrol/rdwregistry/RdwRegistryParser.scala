/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.rdwregistry

import org.apache.hadoop.hbase.client.Put
import java.io.InputStream
import java.io.BufferedReader
import java.io.InputStreamReader
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.util.Bytes
import net.liftweb.json.{ DefaultFormats, Serialization }
import org.slf4j.LoggerFactory

/**
 * Parser of the RDW data from the provided extract of the RDW database.
 * @param input text data with one RDW data per line as described in the specification
 * @param table the destination table
 */
class RdwRegistryParser(input: InputStream, table: HTable) {
  val log = LoggerFactory.getLogger(classOf[RdwRegistryParser])

  def parseAll(): Int = {
    table.setAutoFlush(false, true)
    val result = RdwRegistryParser.parseLines(input).foldLeft[Int](0) {
      case (counter, data) ⇒ {
        if (counter % 20000 == 0) log.info("*")
        if (counter % 1000000 == 0) log.info(": %s".format(counter))
        //the general HBase Writer is not used because it has different interface
        RdwRegistryParser.put(data, table)
        counter + 1
      }
    }
    log.info("Final number: %s".format(result))
    table.flushCommits()
    result
  }
}

object RdwRegistryParser {
  def parseLines(input: InputStream): Iterator[RdwData] = {
    val reader = new BufferedReader(new InputStreamReader(input, "UTF-8"))
    for (
      line ← Iterator.continually(reader.readLine()).takeWhile(_ ne null).filterNot(
        line ⇒ line.trim.isEmpty() || line.startsWith("#"))
    ) yield RdwData.parse(line)
  }

  def put(data: RdwData, table: HTable) {
    implicit val formats = DefaultFormats
    val key = data.plate
    val json = Serialization.write(data)
    val put = new Put(key.getBytes("UTF-8"))
    implicit def string2ByteArray(s: String): Array[Byte] = Bytes.toBytes(s)
    put.add(HBaseRdwRegistry.FAMILY, HBaseRdwRegistry.COLUMN, json.getBytes("UTF-8"))
    table.put(put)
  }
}

