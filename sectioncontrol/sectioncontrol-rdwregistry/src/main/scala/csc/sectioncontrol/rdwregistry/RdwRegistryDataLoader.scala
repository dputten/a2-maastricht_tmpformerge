/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.rdwregistry

import java.io.File
import java.io.FileInputStream
import org.apache.hadoop.hbase.client.HTable
import csc.hbase.utils.HBaseAdminTool
import csc.hbase.utils.HBaseTableKeeper
import org.apache.hadoop.hbase.HBaseConfiguration

/**
 * Loader tool for RDW Registry data
 *
 * 1) Can be launched via 'sbt run-main csc.sectioncontrol.rdwregistry.RdwRegistryDataLoader args'
 * 2) Can be executed by a command line tool starting csc.sectioncontrol.rdwregistry.RdwRegistryDataLoader
 * 3) Can be called from another program if no file but only InputStream is available (HTTP connection)
 *
 * @author Maarten Hazewinkel
 */
object RdwRegistryDataLoader extends App {
  if (args.size < 1) {
    println("Need at least the <inputfile> argument. Use argument --help for instructions.")
    sys.exit()
  }
  val fileName = args(0)
  if (fileName == "--help") {
    println("Arguments: <inputfile> [<zookeeper-servers>]")
    println("<inputfile> is required and is where the data is read from")
    println("<zookeeper-servers> is optional and specifies the zookeeper quorum to use for connecting to HBase. Default = localhost:2181")
    sys.exit()
  }

  val tableName = HBaseRdwRegistry.TABLE
  val zkServers = if (args.size >= 2) args(1) else "localhost:2181"

  val source = new File(fileName)
  val input = new FileInputStream(source)

  println("Loading data from " + fileName + " into HBase table " + tableName + " via zookeeper connection to " + zkServers)

  val loader = new RdwRegistryDataLoader(input, tableName, zkServers)
  val count = loader.parseAll

  println("Loaded %s RDW records.".format(count))
  input.close()
  sys.exit()
}

class RdwRegistryDataLoader(input: java.io.InputStream, tableName: String, zkServers: String) {
  val hbaseConfig = HBaseTableKeeper.createCachedConfig(zkServers)

  HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, HBaseRdwRegistry.FAMILY)
  val table = new HTable(hbaseConfig, tableName)

  val writer = new RdwRegistryParser(input, table)

  def parseAll: Int = writer.parseAll()
}
