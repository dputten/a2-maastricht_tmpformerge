/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.rdwregistry

import org.apache.hadoop.hbase.client._
import csc.hbase.utils.{ NoDistributionOfRowKey, JsonHBaseReader }
import org.apache.hadoop.hbase.util.Bytes

/**
 * RDW data storage.
 */
class HBaseRdwRegistry(val hbaseReaderTable: HTable) extends RdwRegistry with JsonHBaseReader[String, RdwData] {

  val rowKeyDistributer = new NoDistributionOfRowKey()
  /**
   * Get RDW record from HBase
   * @param plate - a Dutch license plate (6 chars, like '12AB34')
   */
  def getData(plate: String): Option[RdwData] = readRow(plate)

  def hbaseReaderDataColumnFamily = HBaseRdwRegistry.FAMILY

  def hbaseReaderDataColumnName = HBaseRdwRegistry.COLUMN

  def makeReaderKey(keyValue: String) = Bytes.toBytes(keyValue)
}
object HBaseRdwRegistry {
  val TABLE = "rdw"
  val FAMILY = "cf"
  val COLUMN = "json"
}

