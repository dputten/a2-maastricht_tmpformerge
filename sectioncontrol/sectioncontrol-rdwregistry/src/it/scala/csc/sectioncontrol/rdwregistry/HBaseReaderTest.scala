/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.rdwregistry

import csc.hbase.utils.testframework.HBaseTestFramework
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.util.Bytes
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

/**
 * Prove that the HBase Reader can read the data written by the RdwRegistryParser
 */
class HBaseReaderTest extends WordSpec with MustMatchers with HBaseTestFramework {

  var testTable: Option[HTable] = None

  override protected def beforeAll() {
    super.beforeAll()
    implicit def string2ByteArray(s: String): Array[Byte] = Bytes.toBytes(s)
    testTable = Some(testUtil.createTable(HBaseRdwRegistry.TABLE, HBaseRdwRegistry.FAMILY))
  }

  "HBaseReader" must {
    "Read nothing from empty table" in {
      new HBaseRdwRegistry(testTable.get).getData("123456") must be('empty)
    }

    "read a single value" in {
      val table = testTable.get
      val rdwData = RdwData.parse("0001ES0100000000242000000000000000M1")
      RdwRegistryParser.put(rdwData, table)
      table.flushCommits()
      val data = new HBaseRdwRegistry(testTable.get).getData("0001ES")
      data.get must be(rdwData)
    }

    "write from input stream" in {
      val table = testTable.get
      val input = getClass.getResourceAsStream("/rdwdata.txt")
      val writer = new RdwRegistryParser(input, table)
      val number = writer.parseAll()
      number must be(35)
      input.close()
      val data = new HBaseRdwRegistry(testTable.get).getData("0003ZB")
      data.get.eegCategory must be("N2")
    }

    "read no non-existing value" in {
      val table = testTable.get
      val input = getClass.getResourceAsStream("/rdwdata.txt")
      val writer = new RdwRegistryParser(input, table)
      writer.parseAll()
      input.close()
      val data = new HBaseRdwRegistry(testTable.get).getData("WWWWWW")
      data must be('empty)
    }
  }

  override protected def afterAll() {
    testTable.foreach(_.close())
    super.afterAll()
  }
}

