/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management

import java.util.Date

import akka.actor.ActorSystem
import akka.testkit.{TestKit, TestProbe}
import akka.util.duration._
import com.github.sstone.amqp.Amqp.{Ack, Delivery, Publish, QueueParameters, _}
import com.github.sstone.amqp.{Amqp, RabbitMQConnection}
import com.rabbitmq.client.AMQP
import csc.akkautils.DirectLogging
import org.scalatest.matchers.MustMatchers
import org.scalatest.{BeforeAndAfterAll, WordSpec}

class RabbitMQTest extends TestKit(ActorSystem("RabbitMQTest")) with WordSpec with MustMatchers
  with BeforeAndAfterAll with DirectLogging {

  override protected def afterAll() {
    super.afterAll()
    system.shutdown()
  }

  /**
   * These tests need a RabbitMQ server to be installed locally
   */
  "RabbitMQ" ignore {
    "Send and receive" in {

      // create an AMQP connection
      val conn = new RabbitMQConnection(
        host = "localhost",
        name = "Connection",
        port = 5672,
        vhost = "/",
        user = "guest",
        password = "guest")

      // create a "channel owner" on this connection
      val producer = conn.createChannelOwner()

      val listener = TestProbe()

      // create a consumer that will route incoming AMQP messages to our listener
      val queueParams = QueueParameters("my_queue", passive = false, durable = false, exclusive = false, autodelete = true)
      val consumer = conn.createConsumer(Amqp.StandardExchanges.amqTopic, queueParams, "*.*", listener.ref, None)

      // wait till everyone is actually connected to the broker
      Amqp.waitForConnection(system, consumer, producer).await()

      // send a message
      val sendMsg = "yo!!"
      /*
                content_type     = <<"text/plain">>,
                content_encoding = <<"UTF-8">>,
                delivery_mode    = 2,
                priority         = 1,
                correlation_id   = <<"123">>,
                reply_to         = <<"something">>,
                expiration       = <<"my-expiration">>,
                message_id       = <<"M123">>,
                timestamp        = 123456,
                type             = <<"freshly-squeezed">>,
                user_id          = <<"joe">>,
                app_id           = <<"joe's app">>,
                headers          = [{<<"str">>, longstr, <<"foo">>},
                                    {<<"int">>, longstr, <<"123">>}]
       */
      val properties = new AMQP.BasicProperties.Builder()
      properties.contentType("text/plain")
      properties.contentEncoding("UTF-8")
      properties.appId("EG100")
      properties.timestamp(new Date())
      properties.`type`("blaad")
      producer ! Publish("amq.topic", "EG100.3215", sendMsg.getBytes, properties = Some(properties.build()), mandatory = true, immediate = false)

      val recvMsg = listener.expectMsgType[Delivery](10 seconds)
      listener.send(listener.lastSender, Ack(recvMsg.envelope.getDeliveryTag))
      //Delivery(consumerTag, envelope, properties, body)
      log.debug("consumerTag:" + recvMsg.consumerTag)
      log.debug("envelope.getRoutingKey:" + recvMsg.envelope.getRoutingKey)
      log.debug("envelope.isRedeliver:" + recvMsg.envelope.isRedeliver)
      log.debug("envelope.getDeliveryTag:" + recvMsg.envelope.getDeliveryTag)
      log.debug("envelope.getExchange:" + recvMsg.envelope.getExchange)
      log.debug("properties:" + recvMsg.properties)
      log.debug("body:" + new String(recvMsg.body))
      new String(recvMsg.body) must be(sendMsg)
      conn.stop
    }
    "Send and receive late" in {
      //needs an exchange EG100
      //and a queue EG100.TEST

      // create an AMQP connection
      val conn = new RabbitMQConnection(
        host = "localhost",
        name = "Connection2",
        port = 5672,
        vhost = "/",
        user = "guest",
        password = "guest")

      // create a "channel owner" on this connection
      val producer = conn.createChannelOwner()

      val listener = TestProbe()

      // wait till everyone is actually connected to the broker
      Amqp.waitForConnection(system, producer).await()

      // send a message
      val sendMsg = "yo!!"
      val properties = new AMQP.BasicProperties.Builder()
      properties.contentType("text/plain")
      properties.contentEncoding("UTF-8")
      properties.appId("EG100")
      properties.timestamp(new Date())
      properties.`type`("blaad")
      producer ! Publish("EG100", "EG33.1", sendMsg.getBytes, properties = Some(properties.build()), mandatory = true, immediate = false)

      // create a consumer that will route incoming AMQP messages to our listener
      val queueParams = QueueParameters("EG100.TEST", passive = true, durable = true, exclusive = false, autodelete = true)
      val consumer = conn.createConsumer(
        ExchangeParameters(name = "EG100", passive = true, exchangeType = "topic"),
        queueParams,
        "*.*",
        listener.ref,
        None)
      Amqp.waitForConnection(system, consumer).await()

      val recvMsg = listener.expectMsgType[Delivery](10 seconds)
      listener.send(listener.lastSender, Ack(recvMsg.envelope.getDeliveryTag))
      //Delivery(consumerTag, envelope, properties, body)
      log.debug("consumerTag:" + recvMsg.consumerTag)
      log.debug("envelope.getRoutingKey:" + recvMsg.envelope.getRoutingKey)
      log.debug("envelope.isRedeliver:" + recvMsg.envelope.isRedeliver)
      log.debug("envelope.getDeliveryTag:" + recvMsg.envelope.getDeliveryTag)
      log.debug("envelope.getExchange:" + recvMsg.envelope.getExchange)
      log.debug("properties:" + recvMsg.properties)
      log.debug("body:" + new String(recvMsg.body))
      new String(recvMsg.body) must be(sendMsg)
      conn.stop
    }
  }
}