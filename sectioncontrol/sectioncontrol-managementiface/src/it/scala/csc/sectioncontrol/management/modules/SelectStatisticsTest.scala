/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.modules

import java.util.Date

import akka.actor.{ActorSystem, Props}
import akka.testkit.{TestActorRef, TestKit, TestProbe}
import akka.util.duration._
import csc.akkautils.DirectLogging
import csc.config.Path
import csc.curator.utils.{Curator, CuratorToolsImpl}
import csc.hbase.utils.testframework.HBaseTestFramework
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.common.DayUtility
import csc.sectioncontrol.management.{ModuleIds, StatisticsMessage, Wakeup}
import csc.sectioncontrol.messages.matcher.MatcherCorridorConfig
import csc.sectioncontrol.messages.{EsaProviderType, StatsDuration, VehicleCode, VehicleImageType}
import csc.sectioncontrol.storage.HBaseTableDefinitions._
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storagelayer.ServiceStatisticsRepositoryAccess
import csc.sectioncontrol.storagelayer.servicestatistics.ServiceStatisticsHBaseRepositoryAccess
import net.liftweb.json.Formats
import org.apache.curator.framework.CuratorFrameworkFactory
import org.apache.curator.retry.ExponentialBackoffRetry
import org.scalatest.matchers.MustMatchers
import org.scalatest.{BeforeAndAfterEach, WordSpec}

class SelectStatisticsTest
  extends TestKit(ActorSystem("SelectStatisticsTest")) with WordSpec with MustMatchers
  with HBaseTestFramework with DirectLogging with BeforeAndAfterEach {
  var storage: Curator = _
  val moduleId = ModuleIds.statistics
  var service: Option[ServiceStatisticsRepositoryAccess] = None

  val systemId = "EG100"
  val retryPolicy = new ExponentialBackoffRetry(100, 10)

  override def beforeAll() {
    super.beforeAll()
    service = Some(new ServiceStatisticsHBaseRepositoryAccess {
      protected def hbaseConfig = testUtil.getConfiguration()
      protected def actorSystem = system
    })
    val retryPolicy = new ExponentialBackoffRetry(100, 10)
    val connectString = "localhost:%s".format(testUtil.getZkCluster.getClientPort)
    val curator = CuratorFrameworkFactory.newClient(connectString, retryPolicy)
    curator.start()

    storage = new CuratorToolsImpl(Some(curator), log) {
      override protected def extendFormats(defaultFormats: Formats) = defaultFormats + new EnumerationSerializer(
        ZkWheelbaseType,
        VehicleCode,
        ZkIndicationType,
        ServiceType,
        ZkCaseFileType,
        EsaProviderType,
        VehicleImageType,
        ConfigType)
    }

  }

  override def beforeEach() {
    super.beforeEach()
    //create tree corridors
    val pshtm = ZkPSHTMIdentification(useYear = true,
      useMonth = true,
      useDay = true,
      useReportingOfficerId = false,
      useNumberOfTheDay = false,
      useLocationCode = false,
      useHHMCode = false,
      useTypeViolation = false,
      useFixedCharacter = false,
      fixedCharacter = "YMD")

    val red = new ZkCorridor(id = "red",
      name = "red",
      roadType = 1,
      serviceType = ServiceType.RedLight,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = true,
      dynamaxMqId = "1",
      approvedSpeeds = Seq(1, 2),
      pshtm = pshtm,
      info = new ZkCorridorInfo(
        corridorId = 1,
        locationCode = "1",
        reportId = "TCS A2 Links  39.7 - 54.9",
        reportHtId = "A020011",
        reportPerformance = true,
        locationLine1 = "cor 1"),
      startSectionId = "1",
      endSectionId = "1",
      allSectionIds = Seq("1"),
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 402,
      roadCode = 2,
      dutyType = "KA2",
      deploymentCode = "03",
      codeText = "")
    storage.put(Paths.Corridors.getConfigPath(systemId, "red"), red)
    val speed = new ZkCorridor(id = "speed",
      name = "speed",
      roadType = 1,
      serviceType = ServiceType.SpeedFixed,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = true,
      dynamaxMqId = "1",
      approvedSpeeds = Seq(1, 2),
      pshtm = pshtm,
      info = new ZkCorridorInfo(
        corridorId = 2,
        locationCode = "1",
        reportId = "TCS A2 Links  39.7 - 54.9",
        reportHtId = "A020011",
        reportPerformance = true,
        locationLine1 = "cor 2"),
      startSectionId = "1",
      endSectionId = "1",
      allSectionIds = Seq("1"),
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 402,
      roadCode = 2,
      dutyType = "KA2",
      deploymentCode = "03",
      codeText = "")
    storage.put(Paths.Corridors.getConfigPath(systemId, "speed"), speed)
    val sec = new ZkCorridor(id = "sec",
      name = "sec",
      roadType = 1,
      serviceType = ServiceType.SectionControl,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = true,
      dynamaxMqId = "1",
      approvedSpeeds = Seq(1, 2),
      pshtm = pshtm,
      info = new ZkCorridorInfo(
        corridorId = 3,
        locationCode = "1",
        reportId = "TCS A2 Links  39.7 - 54.9",
        reportHtId = "A020011",
        reportPerformance = true,
        locationLine1 = "cor 3"),
      startSectionId = "1",
      endSectionId = "1",
      allSectionIds = Seq("1"),
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 402,
      roadCode = 2,
      dutyType = "KA2",
      deploymentCode = "03",
      codeText = "")
    storage.put(Paths.Corridors.getConfigPath(systemId, "sec"), sec)
  }

  override def afterEach() {
    testUtil.truncateTable(serviceStatisticsTableName.getBytes)
    storage.deleteRecursive("/ctes")
  }

  override protected def afterAll() {
    storage.curator.foreach(_.close)
    super.afterAll()
    system.shutdown()
  }

  "SelectStatistics" must {
    "Send no messages when empty" in {
      val producer = TestProbe()
      val select = TestActorRef(new SelectStatistics(storage, producer.ref, testUtil.getConfiguration))
      select ! Wakeup
      producer.expectNoMsg(2 seconds)
    }
    "Send 1 message when old state exists" in {
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      var time = DayUtility.substractOneDay(startDay) //start yesterday
      time = DayUtility.substractOneDay(time) //start day before

      val stat = new SpeedFixedStatistics(period = new StatisticsPeriod(stats = StatsDuration(time, DayUtility.fileExportTimeZone), end = time, begin = (time - 10.minutes.toMillis)),
        nrRegistrations = 50,
        nrSpeedMeasurement = 45,
        averageSpeed = 50,
        highestSpeed = 60,
        laneStats = List())
      service.foreach(_.save(systemId, stat))
      //get stats used to be sure that the save has finished
      //This works because the getSpeedFixedStatistics sends a message after the save and waits for a response
      service.foreach(_.getSpeedFixedStatistics(systemId, StatsDuration(time, DayUtility.fileExportTimeZone)))

      //start test
      val producer = TestProbe()
      val select = system.actorOf(Props(new SelectStatistics(storage, producer.ref, testUtil.getConfiguration)))

      select ! Wakeup
      producer.expectMsg(10.minutes, new StatisticsMessage(systemId = systemId, time = new Date(time), corridorId = "speed", speed = Some(stat)))
      val path = Path(Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId)) / ServiceType.SpeedFixed.toString
      //wait for zookeeper to update
      Thread.sleep(100)
      val completion = storage.get[ZkModuleCompletionStatus](path)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(time)
    }
    "Send 2 message when 2 old state exists" in {
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val stat = new SpeedFixedStatistics(period = new StatisticsPeriod(stats = StatsDuration(time, DayUtility.fileExportTimeZone), end = time, begin = (time - 10.minutes.toMillis)),
        nrRegistrations = 50,
        nrSpeedMeasurement = 45,
        averageSpeed = 50,
        highestSpeed = 60,
        laneStats = List())
      service.foreach(_.save(systemId, stat))

      val stat2 = new SpeedFixedStatistics(period = new StatisticsPeriod(stats = StatsDuration(time, DayUtility.fileExportTimeZone), end = timeYesterday, begin = (timeYesterday - 10.minutes.toMillis)),
        nrRegistrations = 55,
        nrSpeedMeasurement = 55,
        averageSpeed = 55,
        highestSpeed = 65,
        laneStats = List())
      service.foreach(_.save(systemId, stat2))
      //wait for store
      service.foreach(_.getSpeedFixedStatistics(systemId, StatsDuration(time, DayUtility.fileExportTimeZone)))

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectStatistics(storage, producer.ref, testUtil.getConfiguration))
      select ! Wakeup
      producer.expectMsg(new StatisticsMessage(systemId = systemId, time = new Date(time), corridorId = "speed", speed = Some(stat)))
      producer.expectMsg(new StatisticsMessage(systemId = systemId, time = new Date(timeYesterday), corridorId = "speed", speed = Some(stat2)))

      //wait for zookeeper to update
      Thread.sleep(100)

      val path = Path(Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId)) / ServiceType.SpeedFixed.toString

      val completion = storage.get[ZkModuleCompletionStatus](path)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(timeYesterday)
    }
    "not send message after update" in {
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val stat = new SpeedFixedStatistics(period = new StatisticsPeriod(stats = StatsDuration(time, DayUtility.fileExportTimeZone), end = time, begin = (time - 10.minutes.toMillis)),
        nrRegistrations = 50,
        nrSpeedMeasurement = 45,
        averageSpeed = 50,
        highestSpeed = 60,
        laneStats = List())
      service.foreach(_.save(systemId, stat))
      //wait for store
      service.foreach(_.getSpeedFixedStatistics(systemId, StatsDuration(time, DayUtility.fileExportTimeZone)))

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectStatistics(storage, producer.ref, testUtil.getConfiguration))
      select ! Wakeup
      producer.expectMsg(new StatisticsMessage(systemId = systemId, time = new Date(time), corridorId = "speed", speed = Some(stat)))

      select ! Wakeup
      producer.expectNoMsg(1 second)
    }
    "Send message after update" in {
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val stat = new SpeedFixedStatistics(period = new StatisticsPeriod(stats = StatsDuration(time, DayUtility.fileExportTimeZone), end = time, begin = (time - 10.minutes.toMillis)),
        nrRegistrations = 50,
        nrSpeedMeasurement = 45,
        averageSpeed = 50,
        highestSpeed = 60,
        laneStats = List())
      service.foreach(_.save(systemId, stat))
      //wait for store
      service.foreach(_.getSpeedFixedStatistics(systemId, StatsDuration(time, DayUtility.fileExportTimeZone)))

      val stat2 = new SpeedFixedStatistics(period = new StatisticsPeriod(stats = StatsDuration(time, DayUtility.fileExportTimeZone), end = timeYesterday, begin = (timeYesterday - 10.minutes.toMillis)),
        nrRegistrations = 55,
        nrSpeedMeasurement = 55,
        averageSpeed = 55,
        highestSpeed = 65,
        laneStats = List())

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectStatistics(storage, producer.ref, testUtil.getConfiguration))
      select ! Wakeup
      producer.expectMsg(new StatisticsMessage(systemId = systemId, time = new Date(time), corridorId = "speed", speed = Some(stat)))
      producer.expectNoMsg(1 second)

      service.foreach(_.save(systemId, stat2))
      //wait for store
      service.foreach(_.getSpeedFixedStatistics(systemId, StatsDuration(time, DayUtility.fileExportTimeZone)))

      select ! Wakeup
      producer.expectMsg(new StatisticsMessage(systemId = systemId, time = new Date(timeYesterday), corridorId = "speed", speed = Some(stat2)))
    }
    "Send message after update for all types of statistics" in {
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val stat = new SpeedFixedStatistics(period = new StatisticsPeriod(stats = StatsDuration(time, DayUtility.fileExportTimeZone), end = time, begin = (time - 10.minutes.toMillis)),
        nrRegistrations = 50,
        nrSpeedMeasurement = 45,
        averageSpeed = 50,
        highestSpeed = 60,
        laneStats = List())
      service.foreach(_.save(systemId, stat))

      val stat2 = new SpeedFixedStatistics(period = new StatisticsPeriod(stats = StatsDuration(time, DayUtility.fileExportTimeZone), end = timeYesterday, begin = (timeYesterday - 10.minutes.toMillis)),
        nrRegistrations = 55,
        nrSpeedMeasurement = 55,
        averageSpeed = 55,
        highestSpeed = 65,
        laneStats = List())

      val statRed = new RedLightStatistics(period = new StatisticsPeriod(stats = StatsDuration(time, DayUtility.fileExportTimeZone), end = time + 1.hour.toMillis, begin = time + 50.minutes.toMillis),
        nrRegistrations = 50,
        nrWithRedLight = 1,
        averageRedLight = 1,
        highestRedLight = 1,
        laneStats = List())
      service.foreach(_.save(systemId, statRed))
      val statRed2 = new RedLightStatistics(period = new StatisticsPeriod(stats = StatsDuration(time, DayUtility.fileExportTimeZone), end = time + 90.minutes.toMillis, begin = time + 1.hour.toMillis),
        nrRegistrations = 55,
        nrWithRedLight = 3,
        averageRedLight = 1,
        highestRedLight = 2,
        laneStats = List())
      val statSec = new SectionControlStatistics(period = new StatisticsPeriod(stats = StatsDuration(time, DayUtility.fileExportTimeZone), end = time + 1.hour.toMillis, begin = time + 50.minutes.toMillis),
        config = new MatcherCorridorConfig(systemId = systemId, zkCorridorId = "sec", id = 3, entry = "entry", exit = "exit", distance = 5000, deltaForDoubleShoot = 1000, followDistance = 1800000),
        input = 100,
        matched = 95,
        unmatched = 4,
        doubles = 1,
        averageSpeed = 100,
        highestSpeed = 130,
        laneStats = List())
      service.foreach(_.save(systemId, statSec))
      //wait for store
      service.foreach(_.getSpeedFixedStatistics(systemId, StatsDuration(time, DayUtility.fileExportTimeZone)))

      val statSec2 = new SectionControlStatistics(period = new StatisticsPeriod(stats = StatsDuration(time, DayUtility.fileExportTimeZone), end = timeYesterday, begin = (timeYesterday - 10.minutes.toMillis)),
        config = new MatcherCorridorConfig(systemId = systemId, zkCorridorId = "sec", id = 3, entry = "entry", exit = "exit", distance = 5000, deltaForDoubleShoot = 1000, followDistance = 1800000),
        input = 200,
        matched = 190,
        unmatched = 8,
        doubles = 2,
        averageSpeed = 110,
        highestSpeed = 135,
        laneStats = List())

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectStatistics(storage, producer.ref, testUtil.getConfiguration))
      select ! Wakeup
      val msg1 = producer.expectMsgType[StatisticsMessage](10000.millis)
      val msg2 = producer.expectMsgType[StatisticsMessage](1000.millis)
      val msg3 = producer.expectMsgType[StatisticsMessage](1000.millis)

      Seq(msg1, msg2, msg3).foreach {
        case StatisticsMessage(sysId, endtime, corrId, None, None, Some(speed)) ⇒ {
          sysId must be(systemId)
          endtime.getTime must be(stat.period.end)
          speed must be(stat)
          corrId must be("speed")
        }
        case StatisticsMessage(sysId, endtime, corrId, None, Some(red), None) ⇒ {
          sysId must be(systemId)
          endtime.getTime must be(statRed.period.end)
          red must be(statRed)
          corrId must be("red")
        }
        case StatisticsMessage(sysId, endtime, corrId, Some(sec), None, None) ⇒ {
          sysId must be(systemId)
          endtime.getTime must be(statSec.period.end)
          sec must be(statSec)
          corrId must be("sec")
        }
        case msg: AnyRef ⇒ fail("Unexpected message %s".format(msg))
      }
      producer.expectNoMsg(1 second)

      service.foreach(_.save(systemId, stat2))
      service.foreach(_.save(systemId, statRed2))
      service.foreach(_.save(systemId, statSec2))
      //wait for store
      service.foreach(_.getSpeedFixedStatistics(systemId, StatsDuration(time, DayUtility.fileExportTimeZone)))
      select ! Wakeup
      val msg21 = producer.expectMsgType[StatisticsMessage](100000.millis)
      val msg22 = producer.expectMsgType[StatisticsMessage](1000.millis)
      val msg23 = producer.expectMsgType[StatisticsMessage](1000.millis)

      Seq(msg21, msg22, msg23).foreach {
        case StatisticsMessage(sysId, endtime, corrId, None, None, Some(speed)) ⇒ {
          sysId must be(systemId)
          endtime.getTime must be(stat2.period.end)
          speed must be(stat2)
          corrId must be("speed")
        }
        case StatisticsMessage(sysId, endtime, corrId, None, Some(red), None) ⇒ {
          sysId must be(systemId)
          endtime.getTime must be(statRed2.period.end)
          red must be(statRed2)
          corrId must be("red")
        }
        case StatisticsMessage(sysId, endtime, corrId, Some(sec), None, None) ⇒ {
          sysId must be(systemId)
          endtime.getTime must be(statSec2.period.end)
          sec must be(statSec2)
          corrId must be("sec")
        }
        case msg: AnyRef ⇒ fail("Unexpected message %s".format(msg))
      }
      producer.expectNoMsg(1 second)
      val path = Path(Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId)) / ServiceType.SpeedFixed.toString
      val completion = storage.get[ZkModuleCompletionStatus](path)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(stat2.period.end)
      val pathRed = Path(Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId)) / ServiceType.RedLight.toString
      val completionRed = storage.get[ZkModuleCompletionStatus](pathRed)
      completionRed.get.id must be(moduleId)
      completionRed.get.lastUpdate must be(statRed2.period.end)
      val pathSec = Path(Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId)) / ServiceType.SectionControl.toString
      val completionSec = storage.get[ZkModuleCompletionStatus](pathSec)
      completionSec.get.id must be(moduleId)
      completionSec.get.lastUpdate must be(statSec2.period.end)

    }
  }
}

