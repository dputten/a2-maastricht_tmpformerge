/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management

import akka.actor.{ActorSystem, Props}
import akka.testkit.{TestKit, TestProbe}
import csc.akkautils.DirectLogging
import csc.config.Path
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.common.DayUtility
import csc.sectioncontrol.messages.{EsaProviderType, StatsDuration, VehicleCode, VehicleImageType}
import csc.sectioncontrol.reports.ZkDayStatistics
import csc.sectioncontrol.storage._
import net.liftweb.json.Formats
import org.scalatest.matchers.MustMatchers
import org.scalatest.{BeforeAndAfterAll, WordSpec}
//import com.github.sstone.amqp.Amqp._
import java.io.ByteArrayOutputStream
import java.util.zip.{Deflater, GZIPOutputStream}

import akka.util.duration._
import com.github.sstone.amqp.Amqp.{Ack, Delivery, QueueParameters}
import com.github.sstone.amqp.{Amqp, RabbitMQConnection}
import csc.curator.CuratorTestServer
import csc.curator.utils.{Curator, CuratorToolsImpl}
import csc.sectioncontrol.management.xmlmessages.CheckXML
import csc.sectioncontrol.storage.{ZkApplicationInfo, ZkMaintenanceConfig, ZkMaintenanceModule, ZkRabbitMQConnection}
import org.apache.hadoop.hbase.HBaseConfiguration
import resource._

class ManagementStartActorTest extends TestKit(ActorSystem("RabbitMQTest")) with WordSpec
  with MustMatchers with CuratorTestServer with DirectLogging with BeforeAndAfterAll {

  override protected def afterAll() {
    super.afterAll()
    system.shutdown()
  }

  var storage: Curator = _

  override def beforeEach() {
    super.beforeEach()
    storage = new CuratorToolsImpl(clientScope, log) {
      override protected def extendFormats(defaultFormats: Formats) = defaultFormats + new EnumerationSerializer(
        ZkWheelbaseType,
        VehicleCode,
        ZkIndicationType,
        ServiceType,
        ZkCaseFileType,
        EsaProviderType,
        VehicleImageType,
        ConfigType)
    }
  }

  /**
   * These tests need a RabbitMQ server to be installed locally
   */
  "ManagementStartActor" ignore {
    "start and trigger the dailyReport" in {
      //create Daily report data
      val systemId = "EG100"
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      var time = DayUtility.substractOneDay(startDay) //start yesterday
      time = DayUtility.substractOneDay(time) //start day before
      val key = StatsDuration(time, DayUtility.fileExportTimeZone)
      val reportData = compress("<report/>".getBytes()).map(_.toInt).toList
      val report = new ZkDayStatistics(id = "test", day = time, createDate = time, data = reportData, fileName = "dagraportage.xml")
      val reportPath = Path(Paths.Systems.getReportPath(systemId, key))
      storage.put(reportPath, report)
      //create config
      val cfgPath = Paths.MaintenanceInterface.getConfigPath()
      val cfg = new ZkMaintenanceConfig(
        applInfo = new ZkApplicationInfo("EG100", "Flitspalen", "2.0.0.11"),
        mq = new ZkRabbitMQConnection(exchangeName = Some("amq.topic")), //use defaults
        modules = Seq(new ZkMaintenanceModule(ModuleIds.dailyReport, true)),
        frequency = 60000L, blackList = Seq())
      storage.put(cfgPath, cfg)

      //create consumer
      val conn = new RabbitMQConnection(name = "ConsumerConnection")
      val listener = TestProbe()
      // create a consumer that will route incoming AMQP messages to our listener
      val queueParams = QueueParameters("MNGT", passive = false, durable = false, exclusive = false, autodelete = true)
      val consumer = conn.createConsumer(
        Amqp.StandardExchanges.amqTopic,
        queueParams, "*.*.*", listener.ref, None)

      // wait till everyone is actually connected to the broker
      Amqp.waitForConnection(system, consumer).await()

      //start Actor
      val hbaseConfig = HBaseConfiguration.create()

      val actor = system.actorOf(Props(new ManagementStartActor(storage, hbaseConfig)))

      val recvMsg = listener.expectMsgType[Delivery](10 seconds)
      listener.send(listener.lastSender, Ack(recvMsg.envelope.getDeliveryTag))
      //Delivery(consumerTag, envelope, properties, body)
      //test if the message is a valid management interface XML message

      CheckXML.checkMessage(recvMsg.body)

      system.stop(consumer)
      system.stop(actor)
      conn.stop
    }
  }

  def compress(data: Array[Byte]): Array[Byte] = {
    managed(new ByteArrayOutputStream).acquireAndGet { os ⇒
      managed(new GZIPOutputStream(os, Deflater.BEST_COMPRESSION)).acquireAndGet(_.write(data))
      os.toByteArray
    }
  }

}