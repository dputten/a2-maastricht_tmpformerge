/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management

import java.util.Date

import akka.actor.{ActorSystem, Props}
import akka.testkit.{TestKit, TestProbe}
import akka.util.duration._
import com.github.sstone.amqp.Amqp.Publish
import csc.sectioncontrol.management.xmlmessages.XMLMessageBuilder
import csc.sectioncontrol.storage.ZkApplicationInfo
import org.scalatest.matchers.MustMatchers
import org.scalatest.{BeforeAndAfterAll, WordSpec}

class AMQPMessageProducerTest extends TestKit(ActorSystem("AMQPMessageProducerTest")) with WordSpec with MustMatchers with BeforeAndAfterAll {

  override protected def afterAll() {
    super.afterAll()
    system.shutdown()
  }
  "AMQPMessageProducer" must {
    "send message to channel" in {
      val appInfo = new ZkApplicationInfo(id = "EG100", systemType = "Flitpaal", version = "v2.0.0.11")
      val exchangeName = "exchangeName"
      val keyTemplate = "EG100.%s.%s"
      val channelOwner = TestProbe()
      val producer = system.actorOf(Props(
        new AMQPMessageProducer(application = appInfo,
          messageBuilder = new MessageBuilder {
            def createMessage(msg: MaintenanceMessage): Array[Byte] = {
              msg.toString().getBytes()
            }
          },
          exchange = exchangeName,
          keyTemplate = keyTemplate,
          channelOwner = channelOwner.ref)))
      val msg = new TestMessage("3215", new Date(), "test1")
      producer ! msg

      val publishMsg = channelOwner.expectMsgType[Publish](1 second)
      publishMsg.exchange must be(exchangeName)
      publishMsg.key must be("EG100.3215.test")
      publishMsg.properties.get.getContentType must be("text/xml")
      publishMsg.properties.get.getContentEncoding must be("ISO-8859-1")
      publishMsg.properties.get.getAppId must be(appInfo.id)
      publishMsg.properties.get.getTimestamp must be(msg.time)
      new String(publishMsg.body) must be(msg.toString)
    }
    "send message to channel with only systemID in key" in {
      val appInfo = new ZkApplicationInfo(id = "EG100", systemType = "Flitpaal", version = "v2.0.0.11")
      val exchangeName = "exchangeName"
      val keyTemplate = "EG100.%s"
      val channelOwner = TestProbe()
      val producer = system.actorOf(Props(
        new AMQPMessageProducer(application = appInfo,
          messageBuilder = new MessageBuilder {
            def createMessage(msg: MaintenanceMessage): Array[Byte] = {
              msg.toString().getBytes()
            }
          },
          exchange = exchangeName,
          keyTemplate = keyTemplate,
          channelOwner = channelOwner.ref)))
      val msg = new TestMessage("3215", new Date(), "test1")
      producer ! msg

      val publishMsg = channelOwner.expectMsgType[Publish](1 second)
      publishMsg.exchange must be(exchangeName)
      publishMsg.key must be("EG100.3215")
      publishMsg.properties.get.getContentType must be("text/xml")
      publishMsg.properties.get.getContentEncoding must be("ISO-8859-1")
      publishMsg.properties.get.getAppId must be(appInfo.id)
      publishMsg.properties.get.getTimestamp must be(msg.time)
      new String(publishMsg.body) must be(msg.toString)
    }
    "send message to channel without systemID in key" in {
      val appInfo = new ZkApplicationInfo(id = "EG100", systemType = "Flitpaal", version = "v2.0.0.11")
      val exchangeName = "exchangeName"
      val keyTemplate = "EG100"
      val channelOwner = TestProbe()
      val producer = system.actorOf(Props(
        new AMQPMessageProducer(application = appInfo,
          messageBuilder = new MessageBuilder {
            def createMessage(msg: MaintenanceMessage): Array[Byte] = {
              msg.toString().getBytes()
            }
          },
          exchange = exchangeName,
          keyTemplate = keyTemplate,
          channelOwner = channelOwner.ref)))
      val msg = new TestMessage("3215", new Date(), "test1")
      producer ! msg

      val publishMsg = channelOwner.expectMsgType[Publish](1 second)
      publishMsg.exchange must be(exchangeName)
      publishMsg.key must be("EG100")
      publishMsg.properties.get.getContentType must be("text/xml")
      publishMsg.properties.get.getContentEncoding must be("ISO-8859-1")
      publishMsg.properties.get.getAppId must be(appInfo.id)
      publishMsg.properties.get.getTimestamp must be(msg.time)
      new String(publishMsg.body) must be(msg.toString)
    }
    "send message to channel with wrong keytemplate" in {
      val appInfo = new ZkApplicationInfo(id = "EG100", systemType = "Flitpaal", version = "v2.0.0.11")
      val exchangeName = "exchangeName"
      val keyTemplate = "EG100.%s.%s.%"
      val channelOwner = TestProbe()
      val producer = system.actorOf(Props(
        new AMQPMessageProducer(application = appInfo,
          messageBuilder = new MessageBuilder {
            def createMessage(msg: MaintenanceMessage): Array[Byte] = {
              msg.toString().getBytes()
            }
          },
          exchange = exchangeName,
          keyTemplate = keyTemplate,
          channelOwner = channelOwner.ref)))
      val msg = new TestMessage("3215", new Date(), "test1")
      producer ! msg

      val publishMsg = channelOwner.expectMsgType[Publish](1 second)
      publishMsg.exchange must be(exchangeName)
      publishMsg.key must be("EG100.%s.%s.%")
      publishMsg.properties.get.getContentType must be("text/xml")
      publishMsg.properties.get.getContentEncoding must be("ISO-8859-1")
      publishMsg.properties.get.getAppId must be(appInfo.id)
      publishMsg.properties.get.getTimestamp must be(msg.time)
      new String(publishMsg.body) must be(msg.toString)
    }
    "send message to channel with wrong keytemplate2" in {
      val appInfo = new ZkApplicationInfo(id = "EG100", systemType = "Flitpaal", version = "v2.0.0.11")
      val exchangeName = "exchangeName"
      val keyTemplate = "EG100.%.%"
      val channelOwner = TestProbe()
      val producer = system.actorOf(Props(
        new AMQPMessageProducer(application = appInfo,
          messageBuilder = new MessageBuilder {
            def createMessage(msg: MaintenanceMessage): Array[Byte] = {
              msg.toString().getBytes()
            }
          },
          exchange = exchangeName,
          keyTemplate = keyTemplate,
          channelOwner = channelOwner.ref)))
      val msg = new TestMessage("3215", new Date(), "test1")
      producer ! msg

      channelOwner.expectNoMsg(2 seconds)
    }
    "fail to send unknown message and log error" in {
      val appInfo = new ZkApplicationInfo(id = "EG100", systemType = "Flitpaal", version = "v2.0.0.11")
      val exchangeName = "exchangeName"
      val keyTemplate = "EG100.%s.%s"
      val channelOwner = TestProbe()
      val producer = system.actorOf(Props(
        new AMQPMessageProducer(application = appInfo,
          messageBuilder = new XMLMessageBuilder(appInfo),
          exchange = exchangeName,
          keyTemplate = keyTemplate,
          channelOwner = channelOwner.ref)))
      val msg = new TestMessage("3215", new Date(), "test2")
      producer ! msg

      channelOwner.expectNoMsg(2 seconds)
    }
  }

}

case class TestMessage(systemId: String, time: Date, reference: String) extends MaintenanceMessage {
  def getMessageType(): String = "test"
}

