/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.modules

import java.util.Date

import akka.actor.ActorSystem
import akka.testkit.{TestActorRef, TestKit, TestProbe}
import akka.util.duration._
import csc.akkautils.DirectLogging
import csc.config.Path
import csc.curator.CuratorTestServer
import csc.curator.utils.{Curator, CuratorToolsImpl}
import csc.hbase.utils.HBaseTableKeeper
import csc.hbase.utils.testframework.HBaseTestFramework
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.common.DayUtility
import csc.sectioncontrol.management._
import csc.sectioncontrol.messages.{Lane, ValueWithConfidence, ValueWithConfidence_Float, VehicleMetadata, _}
import csc.sectioncontrol.storage.HBaseTableDefinitions._
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storagelayer.hbasetables.VehicleTable
import net.liftweb.json.DefaultFormats
import org.apache.hadoop.hbase.client.HTable
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

class SelectRegistrationTest extends TestKit(ActorSystem("SelectRegistrationTest")) with WordSpec with MustMatchers
  with HBaseTestFramework with DirectLogging with CuratorTestServer {

  private var storage: Curator = _
  var table: HTable = _
  val moduleId = ModuleIds.registration
  val tableKeeper = new HBaseTableKeeper {}

  override def beforeEach() {
    val formats = DefaultFormats + new EnumerationSerializer(
      ZkWheelbaseType,
      VehicleCode,
      ZkIndicationType,
      ServiceType,
      ZkCaseFileType,
      EsaProviderType,
      VehicleImageType,
      ConfigType)

    super.beforeEach()
    storage = new CuratorToolsImpl(clientScope, log, formats)
  }

  override def afterEach() {
    truncateTable(table)
    super.afterEach()
  }

  override def beforeAll() {
    super.beforeAll()
    table = testUtil.createTable(vehicleRecordTableName.getBytes, vehicleRecordColumnFamily.getBytes())
  }

  override protected def afterAll() {
    tableKeeper.closeTables()
    table.close()
    system.shutdown()
    super.afterAll()
  }

  val systemId = "3215"

  "SelectCalibration" must {
    "Send no messages when empty" in {

      storage.createEmptyPath(Paths.Systems.getSystemPath(systemId))
      storage.createEmptyPath(Paths.Gantries.getLanesConfigPath(systemId, "1", "RD"))

      val producer = TestProbe()
      val select = TestActorRef(new SelectRegistrations(storage, producer.ref, testUtil.getConfiguration))
      select ! Wakeup
      producer.expectNoMsg(2 seconds)
    }
    "Send 1 message when old state exists" in {
      storage.createEmptyPath(Paths.Systems.getSystemPath(systemId))
      storage.createEmptyPath(Paths.Gantries.getLanesConfigPath(systemId, "1", "RD"))
      val writer = VehicleTable.makeWriter(testUtil.getConfiguration, tableKeeper)

      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      var time = DayUtility.substractOneDay(startDay) //start yesterday
      time = DayUtility.substractOneDay(time) //start day before

      val testResult = new VehicleMetadata(
        lane = new Lane(laneId = "3215-1-RD",
          name = "RD",
          gantry = "1",
          system = "3215",
          sensorGPS_longitude = 52.4,
          sensorGPS_latitude = 5.2),
        eventId = "3215-1-RD-" + time,
        eventTimestamp = time,
        eventTimestampStr = None,
        license = Some(ValueWithConfidence[String]("12XXX3", 60)),
        category = Some(ValueWithConfidence[String]("Car", 77)),
        speed = Some(ValueWithConfidence_Float(80, 80)),
        country = Some(ValueWithConfidence[String]("NL", 40)),
        images = Seq(),
        NMICertificate = "",
        applChecksum = "",
        serialNr = "SN1234")

      writer.writeRow(testResult, testResult)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectRegistrations(storage, producer.ref, testUtil.getConfiguration))

      select ! Wakeup
      producer.expectMsg(new RegistrationMessage(systemId = systemId, time = new Date(time),
        laneId = testResult.lane.laneId,
        speed = testResult.speed,
        license = testResult.license,
        country = testResult.country,
        category = testResult.category))

      val path = Path(Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId)) / testResult.lane.laneId
      val completion = storage.get[ZkModuleCompletionStatus](path)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(time)

    }
    "Send 2 message when 2 old state exists" in {
      //create Daily report
      storage.createEmptyPath(Paths.Systems.getSystemPath(systemId))
      storage.createEmptyPath(Paths.Gantries.getLanesConfigPath(systemId, "1", "RD"))
      val writer = VehicleTable.makeWriter(testUtil.getConfiguration, tableKeeper)
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val testResult = new VehicleMetadata(
        lane = new Lane(laneId = "3215-1-RD",
          name = "RD",
          gantry = "1",
          system = "3215",
          sensorGPS_longitude = 52.4,
          sensorGPS_latitude = 5.2),
        eventId = "3215-1-RD-" + time,
        eventTimestamp = time,
        eventTimestampStr = None,
        license = Some(ValueWithConfidence[String]("12XXX3", 60)),
        category = Some(ValueWithConfidence[String]("Car", 77)),
        speed = Some(ValueWithConfidence_Float(80, 80)),
        country = Some(ValueWithConfidence[String]("NL", 40)),
        images = Seq(),
        NMICertificate = "",
        applChecksum = "",
        serialNr = "SN1234")
      writer.writeRow(testResult, testResult)

      val testResult2 = new VehicleMetadata(
        lane = new Lane(laneId = "3215-1-RD",
          name = "RD",
          gantry = "1",
          system = "3215",
          sensorGPS_longitude = 52.4,
          sensorGPS_latitude = 5.2),
        eventId = "3215-1-RD-" + timeYesterday,
        eventTimestamp = timeYesterday,
        eventTimestampStr = None,
        license = Some(ValueWithConfidence[String]("12XX34", 65)),
        category = Some(ValueWithConfidence[String]("CarTrailer", 70)),
        speed = Some(ValueWithConfidence_Float(85, 85)),
        country = Some(ValueWithConfidence[String]("DE", 45)),
        images = Seq(),
        NMICertificate = "",
        applChecksum = "",
        serialNr = "SN1234")
      writer.writeRow(testResult2, testResult2)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectRegistrations(storage, producer.ref, testUtil.getConfiguration))
      select ! Wakeup
      producer.expectMsg(new RegistrationMessage(systemId = systemId, time = new Date(time),
        laneId = testResult.lane.laneId,
        speed = testResult.speed,
        license = testResult.license,
        country = testResult.country,
        category = testResult.category))
      producer.expectMsg(new RegistrationMessage(systemId = systemId, time = new Date(timeYesterday),
        laneId = testResult2.lane.laneId,
        speed = testResult2.speed,
        license = testResult2.license,
        country = testResult2.country,
        category = testResult2.category))

      val path = Path(Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId)) / testResult.lane.laneId
      val completion = storage.get[ZkModuleCompletionStatus](path)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(timeYesterday)
    }
    "not send message after update" in {
      //create Daily report
      storage.createEmptyPath(Paths.Systems.getSystemPath(systemId))
      storage.createEmptyPath(Paths.Gantries.getLanesConfigPath(systemId, "1", "RD"))
      val writer = VehicleTable.makeWriter(testUtil.getConfiguration, tableKeeper)
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val testResult = new VehicleMetadata(
        lane = new Lane(laneId = "3215-1-RD",
          name = "RD",
          gantry = "1",
          system = "3215",
          sensorGPS_longitude = 52.4,
          sensorGPS_latitude = 5.2),
        eventId = "3215-1-RD-" + time,
        eventTimestamp = time,
        eventTimestampStr = None,
        license = Some(ValueWithConfidence[String]("12XXX3", 60)),
        category = Some(ValueWithConfidence[String]("Car", 77)),
        speed = Some(ValueWithConfidence_Float(80, 80)),
        country = Some(ValueWithConfidence[String]("NL", 40)),
        images = Seq(),
        NMICertificate = "",
        applChecksum = "",
        serialNr = "SN1234")
      writer.writeRow(testResult, testResult)
      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectRegistrations(storage, producer.ref, testUtil.getConfiguration))
      select ! Wakeup
      producer.expectMsg(new RegistrationMessage(systemId = systemId, time = new Date(time),
        laneId = testResult.lane.laneId,
        speed = testResult.speed,
        license = testResult.license,
        country = testResult.country,
        category = testResult.category))
      select ! Wakeup
      producer.expectNoMsg(1 second)
    }
    "Send message after update" in {
      //create Daily report
      storage.createEmptyPath(Paths.Systems.getSystemPath(systemId))
      storage.createEmptyPath(Paths.Gantries.getLanesConfigPath(systemId, "1", "RD"))
      val writer = VehicleTable.makeWriter(testUtil.getConfiguration, tableKeeper)
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val testResult = new VehicleMetadata(
        lane = new Lane(laneId = "3215-1-RD",
          name = "RD",
          gantry = "1",
          system = "3215",
          sensorGPS_longitude = 52.4,
          sensorGPS_latitude = 5.2),
        eventId = "3215-1-RD-" + time,
        eventTimestamp = time,
        eventTimestampStr = None,
        license = Some(ValueWithConfidence[String]("12XXX3", 60)),
        category = Some(ValueWithConfidence[String]("Car", 77)),
        speed = Some(ValueWithConfidence_Float(80, 80)),
        country = Some(ValueWithConfidence[String]("NL", 40)),
        images = Seq(),
        NMICertificate = "",
        applChecksum = "",
        serialNr = "SN1234")
      writer.writeRow(testResult, testResult)

      val testResult2 = new VehicleMetadata(
        lane = new Lane(laneId = "3215-1-RD",
          name = "RD",
          gantry = "1",
          system = "3215",
          sensorGPS_longitude = 52.4,
          sensorGPS_latitude = 5.2),
        eventId = "3215-1-RD-" + timeYesterday,
        eventTimestamp = timeYesterday,
        eventTimestampStr = None,
        license = Some(ValueWithConfidence[String]("12XX34", 65)),
        category = Some(ValueWithConfidence[String]("CarTrailer", 70)),
        speed = Some(ValueWithConfidence_Float(85, 85)),
        country = Some(ValueWithConfidence[String]("DE", 45)),
        images = Seq(),
        NMICertificate = "",
        applChecksum = "",
        serialNr = "SN1234")

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectRegistrations(storage, producer.ref, testUtil.getConfiguration))
      select ! Wakeup
      producer.expectMsg(new RegistrationMessage(systemId = systemId, time = new Date(time),
        laneId = testResult.lane.laneId,
        speed = testResult.speed,
        license = testResult.license,
        country = testResult.country,
        category = testResult.category))
      producer.expectNoMsg(1 second)

      writer.writeRow(testResult2, testResult2)
      select ! Wakeup
      producer.expectMsg(new RegistrationMessage(systemId = systemId, time = new Date(timeYesterday),
        laneId = testResult2.lane.laneId,
        speed = testResult2.speed,
        license = testResult2.license,
        country = testResult2.country,
        category = testResult2.category))
    }
    "Send message after update for all two lanes on multiple gantries" in {
      storage.createEmptyPath(Paths.Systems.getSystemPath(systemId))
      storage.createEmptyPath(Paths.Gantries.getLanesConfigPath(systemId, "1", "RD"))
      storage.createEmptyPath(Paths.Gantries.getLanesConfigPath(systemId, "2", "RA"))
      val writer = VehicleTable.makeWriter(testUtil.getConfiguration, tableKeeper)
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val testResult = new VehicleMetadata(
        lane = new Lane(laneId = "3215-1-RD",
          name = "RD",
          gantry = "1",
          system = "3215",
          sensorGPS_longitude = 52.4,
          sensorGPS_latitude = 5.2),
        eventId = "3215-1-RD-" + time,
        eventTimestamp = time,
        eventTimestampStr = None,
        license = Some(ValueWithConfidence[String]("12XXX3", 60)),
        category = Some(ValueWithConfidence[String]("Car", 77)),
        speed = Some(ValueWithConfidence_Float(80, 80)),
        country = Some(ValueWithConfidence[String]("NL", 40)),
        images = Seq(),
        NMICertificate = "",
        applChecksum = "",
        serialNr = "SN1234")
      writer.writeRow(testResult, testResult)

      val testResult2 = new VehicleMetadata(
        lane = new Lane(laneId = "3215-1-RD",
          name = "RD",
          gantry = "2",
          system = "3215",
          sensorGPS_longitude = 52.4,
          sensorGPS_latitude = 5.2),
        eventId = "3215-1-RD-" + timeYesterday,
        eventTimestamp = timeYesterday,
        eventTimestampStr = None,
        license = Some(ValueWithConfidence[String]("12XX34", 65)),
        category = Some(ValueWithConfidence[String]("CarTrailer", 70)),
        speed = Some(ValueWithConfidence_Float(85, 85)),
        country = Some(ValueWithConfidence[String]("DE", 45)),
        images = Seq(),
        NMICertificate = "",
        applChecksum = "",
        serialNr = "SN1234")

      val testResultL1 = new VehicleMetadata(
        lane = new Lane(laneId = "3215-2-RA",
          name = "RA",
          gantry = "2",
          system = "3215",
          sensorGPS_longitude = 52.4,
          sensorGPS_latitude = 5.2),
        eventId = "3215-2-RA-" + (time + 2.hours.toMillis),
        eventTimestamp = (time + 2.hours.toMillis),
        eventTimestampStr = None,
        license = Some(ValueWithConfidence[String]("12YYY3", 60)),
        category = Some(ValueWithConfidence[String]("Car", 77)),
        speed = Some(ValueWithConfidence_Float(80, 80)),
        country = Some(ValueWithConfidence[String]("NL", 40)),
        images = Seq(),
        NMICertificate = "",
        applChecksum = "",
        serialNr = "SN1234")
      writer.writeRow(testResultL1, testResultL1)

      val testResultL2 = new VehicleMetadata(
        lane = new Lane(laneId = "3215-2-RA",
          name = "RA",
          gantry = "2",
          system = "3215",
          sensorGPS_longitude = 52.4,
          sensorGPS_latitude = 5.2),
        eventId = "3215-2-RA-" + (timeYesterday - 2.hours.toMillis),
        eventTimestamp = (timeYesterday - 2.hours.toMillis),
        eventTimestampStr = None,
        license = Some(ValueWithConfidence[String]("12YY34", 65)),
        category = Some(ValueWithConfidence[String]("CarTrailer", 70)),
        speed = Some(ValueWithConfidence_Float(85, 85)),
        country = Some(ValueWithConfidence[String]("DE", 45)),
        images = Seq(),
        NMICertificate = "",
        applChecksum = "",
        serialNr = "SN1234")

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectRegistrations(storage, producer.ref, testUtil.getConfiguration))
      select ! Wakeup

      producer.expectMsg(new RegistrationMessage(systemId = systemId,
        time = new Date(testResultL1.eventTimestamp),
        laneId = testResultL1.lane.laneId,
        speed = testResultL1.speed,
        license = testResultL1.license,
        country = testResultL1.country,
        category = testResultL1.category))
      producer.expectMsg(new RegistrationMessage(systemId = systemId, time = new Date(testResult.eventTimestamp),
        laneId = testResult.lane.laneId,
        speed = testResult.speed,
        license = testResult.license,
        country = testResult.country,
        category = testResult.category))
      producer.expectNoMsg(1 second)

      writer.writeRow(testResult2, testResult2)
      writer.writeRow(testResultL2, testResultL2)

      //wait for store
      select ! Wakeup

      producer.expectMsg(new RegistrationMessage(systemId = systemId, time = new Date(testResultL2.eventTimestamp),
        laneId = testResultL2.lane.laneId,
        speed = testResultL2.speed,
        license = testResultL2.license,
        country = testResultL2.country,
        category = testResultL2.category))
      producer.expectMsg(new RegistrationMessage(systemId = systemId, time = new Date(testResult2.eventTimestamp),
        laneId = testResult2.lane.laneId,
        speed = testResult2.speed,
        license = testResult2.license,
        country = testResult2.country,
        category = testResult2.category))

      producer.expectNoMsg(1 second)
      val path = Path(Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId)) / testResult2.lane.laneId
      val completion = storage.get[ZkModuleCompletionStatus](path)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(testResult2.eventTimestamp)
      val pathRed = Path(Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId)) / testResultL2.lane.laneId
      val completionRed = storage.get[ZkModuleCompletionStatus](pathRed)
      completionRed.get.id must be(moduleId)
      completionRed.get.lastUpdate must be(testResultL2.eventTimestamp)
    }

  }
}

