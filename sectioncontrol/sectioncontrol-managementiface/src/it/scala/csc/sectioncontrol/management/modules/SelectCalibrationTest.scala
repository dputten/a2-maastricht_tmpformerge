/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.modules

import java.util.Date

import akka.actor.ActorSystem
import akka.testkit.{TestActorRef, TestKit, TestProbe}
import akka.util.duration._
import csc.akkautils.DirectLogging
import csc.config.Path
import csc.curator.utils.{Curator, CuratorToolsImpl}
import csc.hbase.utils.HBaseTableKeeper
import csc.hbase.utils.testframework.HBaseTestFramework
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.common.DayUtility
import csc.sectioncontrol.management.{CalibrateMessage, ModuleIds, Wakeup}
import csc.sectioncontrol.messages.{EsaProviderType, VehicleCode, VehicleImageType}
import csc.sectioncontrol.storage.HBaseTableDefinitions._
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storagelayer.corridor.CorridorService
import csc.sectioncontrol.storagelayer.hbasetables.SelfTestResultTable
import net.liftweb.json.Formats
import org.apache.curator.framework.CuratorFrameworkFactory
import org.apache.curator.retry.ExponentialBackoffRetry
import org.apache.hadoop.hbase.client.HTable
import org.scalatest.matchers.MustMatchers
import org.scalatest.{BeforeAndAfterEach, WordSpec}

class SelectCalibrationTest
  extends TestKit(ActorSystem("SelectStateTest")) with WordSpec with MustMatchers
  with HBaseTestFramework with DirectLogging with BeforeAndAfterEach {

  var storage: Curator = _
  var table: HTable = _
  val moduleId = ModuleIds.calibration
  val tableKeeper = new HBaseTableKeeper() {}

  override def beforeEach() {
    super.beforeEach()
  }

  override def afterEach() {
    truncateTable(table)
    storage.deleteRecursive("/ctes")
  }

  override def beforeAll() {
    super.beforeAll()
    val retryPolicy = new ExponentialBackoffRetry(100, 10)
    val connectString = "localhost:%s".format(testUtil.getZkCluster.getClientPort)
    val curator = CuratorFrameworkFactory.newClient(connectString, retryPolicy)
    curator.start()

    storage = new CuratorToolsImpl(Some(curator), log) {
      override protected def extendFormats(defaultFormats: Formats) = defaultFormats + new EnumerationSerializer(
        ZkWheelbaseType,
        VehicleCode,
        ZkIndicationType,
        ServiceType,
        ZkCaseFileType,
        EsaProviderType,
        VehicleImageType,
        ConfigType)
    }
    table = testUtil.createTable(selfTestResultTableName.getBytes, selfTestResultColumnFamily.getBytes())
  }
  override protected def afterAll() {
    tableKeeper.closeTables()
    table.close()
    storage.curator.foreach(_.close)
    system.shutdown()
    super.afterAll()
  }

  "SelectCalibration" must {
    "Send no messages when empty" in {
      val systemId = "EG100"
      storage.createEmptyPath(Paths.Systems.getSystemPath(systemId))
      val producer = TestProbe()
      val select = TestActorRef(new SelectCalibration(storage, producer.ref, testUtil.getConfiguration))
      select ! Wakeup
      producer.expectNoMsg(2 seconds)
    }
    "Send 1 message when old state exists" in {
      val systemId = "EG100"
      val corridorId = "S1"
      storage.createEmptyPath(Paths.Systems.getSystemPath(systemId))
      createCorridor(systemId, corridorId)
      val writer = SelfTestResultTable.makeWriter(testUtil.getConfiguration, tableKeeper, systemId, corridorId)

      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      var time = DayUtility.substractOneDay(startDay) //start yesterday
      time = DayUtility.substractOneDay(time) //start day before

      val testResult = new SelfTestResult(systemId = systemId,
        corridorId = corridorId,
        time = time,
        success = true,
        reportingOfficerCode = "XX1234",
        nrCamerasTested = 1,
        images = Seq(),
        errors = Seq(),
        serialNr = "SN1234")
      writer.writeRow(testResult.time, testResult)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectCalibration(storage, producer.ref, testUtil.getConfiguration))

      select ! Wakeup
      producer.expectMsg(new CalibrateMessage(systemId = systemId, corridorId = corridorId, time = new Date(time), success = testResult.success))

      val path = Path(Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId)) / corridorId
      val completion = storage.get[ZkModuleCompletionStatus](path)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(time)

    }
    "Send 2 message when 2 old state exists" in {
      //create Daily report
      val systemId = "EG100"
      val corridorId = "S1"
      storage.createEmptyPath(Paths.Systems.getSystemPath(systemId))
      createCorridor(systemId, corridorId)
      val writer = SelfTestResultTable.makeWriter(testUtil.getConfiguration, tableKeeper, systemId, corridorId)
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val testResult = new SelfTestResult(systemId = systemId,
        corridorId = corridorId,
        time = time,
        success = true,
        reportingOfficerCode = "XX1234",
        nrCamerasTested = 1,
        images = Seq(),
        errors = Seq(),
        serialNr = "SN1234")
      writer.writeRow(testResult.time, testResult)

      val testResult2 = new SelfTestResult(systemId = systemId,
        corridorId = corridorId,
        time = timeYesterday,
        success = false,
        reportingOfficerCode = "XX1234",
        nrCamerasTested = 1,
        images = Seq(),
        errors = Seq(),
        serialNr = "SN1234")
      writer.writeRow(testResult2.time, testResult2)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectCalibration(storage, producer.ref, testUtil.getConfiguration))
      select ! Wakeup
      producer.expectMsg(new CalibrateMessage(systemId = systemId, corridorId = corridorId, time = new Date(time), success = testResult.success))
      producer.expectMsg(new CalibrateMessage(systemId = systemId, corridorId = corridorId, time = new Date(timeYesterday), success = testResult2.success))

      val path = Path(Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId)) / corridorId
      val completion = storage.get[ZkModuleCompletionStatus](path)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(timeYesterday)
    }
    "not send message after update" in {
      //create Daily report
      val systemId = "EG100"
      val corridorId = "S1"
      storage.createEmptyPath(Paths.Systems.getSystemPath(systemId))
      createCorridor(systemId, corridorId)
      val writer = SelfTestResultTable.makeWriter(testUtil.getConfiguration, tableKeeper, systemId, corridorId)
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val testResult = new SelfTestResult(systemId = systemId,
        corridorId = corridorId,
        time = time,
        success = true,
        reportingOfficerCode = "XX1234",
        nrCamerasTested = 1,
        images = Seq(),
        errors = Seq(),
        serialNr = "SN1234")
      writer.writeRow(testResult.time, testResult)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectCalibration(storage, producer.ref, testUtil.getConfiguration))
      select ! Wakeup
      producer.expectMsg(new CalibrateMessage(systemId = systemId, corridorId = corridorId, time = new Date(time), success = testResult.success))

      select ! Wakeup
      producer.expectNoMsg(1 second)
    }
    "Send message after update" in {
      //create Daily report
      val systemId = "EG100"
      val corridorId = "S1"
      storage.createEmptyPath(Paths.Systems.getSystemPath(systemId))
      createCorridor(systemId, corridorId)
      val writer = SelfTestResultTable.makeWriter(testUtil.getConfiguration, tableKeeper, systemId, corridorId)
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val testResult = new SelfTestResult(systemId = systemId,
        corridorId = corridorId,
        time = time,
        success = true,
        reportingOfficerCode = "XX1234",
        nrCamerasTested = 1,
        images = Seq(),
        errors = Seq(),
        serialNr = "SN1234")
      writer.writeRow(testResult.time, testResult)

      val testResult2 = new SelfTestResult(systemId = systemId,
        corridorId = corridorId,
        time = timeYesterday,
        success = false,
        reportingOfficerCode = "XX1234",
        nrCamerasTested = 1,
        images = Seq(),
        errors = Seq(),
        serialNr = "SN1234")

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectCalibration(storage, producer.ref, testUtil.getConfiguration))
      select ! Wakeup
      producer.expectMsg(new CalibrateMessage(systemId = systemId, corridorId = corridorId, time = new Date(time), success = testResult.success))
      producer.expectNoMsg(1 second)

      writer.writeRow(testResult2.time, testResult2)
      select ! Wakeup
      producer.expectMsg(new CalibrateMessage(systemId = systemId, corridorId = corridorId, time = new Date(timeYesterday), success = testResult2.success))
    }
  }

  def createCorridor(systemId: String, corridorId: String) {
    val corridor = ZkCorridor(id = corridorId,
      name = "S1-name",
      roadType = 0,
      serviceType = ServiceType.SectionControl,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = false,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = ZkPSHTMIdentification(useYear = false,
        useMonth = true,
        useDay = false,
        useReportingOfficerId = false,
        useNumberOfTheDay = true,
        useLocationCode = false,
        useHHMCode = false,
        useTypeViolation = false,
        useFixedCharacter = false,
        fixedCharacter = "T"),
      info = ZkCorridorInfo(corridorId = 1,
        locationCode = "2",
        reportId = "",
        reportHtId = "",
        reportPerformance = true,
        locationLine1 = ""),
      startSectionId = "sec1",
      endSectionId = "sec1",
      allSectionIds = Seq("sec1"),
      direction = ZkDirection(directionFrom = "",
        directionTo = ""),
      radarCode = 4,
      roadCode = 0,
      dutyType = "",
      deploymentCode = "",
      codeText = "")
    CorridorService.createCorridor(storage, systemId, corridor)
  }
}
