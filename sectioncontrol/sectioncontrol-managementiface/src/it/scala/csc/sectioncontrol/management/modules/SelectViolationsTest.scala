/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.modules

import java.util.Date

import akka.actor.ActorSystem
import akka.testkit.{TestActorRef, TestKit, TestProbe}
import akka.util.duration._
import csc.akkautils.DirectLogging
import csc.config.Path
import csc.curator.utils.{Curator, CuratorToolsImpl}
import csc.hbase.utils.HBaseTableKeeper
import csc.hbase.utils.testframework.HBaseTestFramework
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.common.DayUtility
import csc.sectioncontrol.management.{ModuleIds, ViolationMessage, Wakeup}
import csc.sectioncontrol.messages.{Lane, ValueWithConfidence, ValueWithConfidence_Float, _}
import csc.sectioncontrol.storage.HBaseTableDefinitions._
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storagelayer.hbasetables.ProcessedViolationTable
import net.liftweb.json.{DefaultFormats, Formats}
import org.apache.curator.framework.CuratorFrameworkFactory
import org.apache.curator.retry.ExponentialBackoffRetry
import org.apache.hadoop.hbase.client.HTable
import org.scalatest.matchers.MustMatchers
import org.scalatest.{BeforeAndAfterEach, WordSpec}

class SelectViolationsTest extends TestKit(ActorSystem("SelectViolationsTest")) with WordSpec with MustMatchers
  with HBaseTestFramework with DirectLogging with BeforeAndAfterEach {

  var storage: Curator = _
  var table: HTable = _
  val moduleId = ModuleIds.violation
  val tableKeeper = new HBaseTableKeeper {}

  override def beforeEach() {
    super.beforeEach()
  }

  override def afterEach() {
    truncateTable(table)
    storage.deleteRecursive("/ctes")
  }

  override def beforeAll() {
    super.beforeAll()
    val retryPolicy = new ExponentialBackoffRetry(100, 10)
    val connectString = "localhost:%s".format(testUtil.getZkCluster.getClientPort)
    val curator = CuratorFrameworkFactory.newClient(connectString, retryPolicy)
    curator.start()

    storage = new CuratorToolsImpl(Some(curator), log) {
      override protected def extendFormats(defaultFormats: Formats) = DefaultFormats + new EnumerationSerializer(
        ProcessingIndicator,
        VehicleCode,
        VehicleCategory,
        ZkWheelbaseType,
        ZkIndicationType,
        ServiceType,
        ZkCaseFileType,
        EsaProviderType,
        VehicleImageType,
        ConfigType)
    }
    table = testUtil.createTable(processedViolationTableName.getBytes, processedViolationColumnFamily.getBytes())
  }
  override protected def afterAll() {
    tableKeeper.closeTables()
    storage.curator.foreach(_.close)
    table.close()
    system.shutdown()
    super.afterAll()
  }
  val systemId = "3215"

  "SelectCalibration" must {
    "Send no messages when empty" in {
      storage.createEmptyPath(Paths.Systems.getSystemPath(systemId))

      val producer = TestProbe()
      val select = TestActorRef(new SelectViolations(storage, producer.ref, testUtil.getConfiguration))
      select ! Wakeup
      producer.expectNoMsg(2 seconds)
    }
    "Send 1 message when old state exists" in {
      storage.createEmptyPath(Paths.Systems.getSystemPath(systemId))
      val writer = ProcessedViolationTable.makeWriter(testUtil.getConfiguration, tableKeeper)

      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      var time = DayUtility.substractOneDay(startDay) //start yesterday
      time = DayUtility.substractOneDay(time) //start day before

      val testResult = createViolation1(time)
      writer.writeRow(testResult, testResult)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectViolations(storage, producer.ref, testUtil.getConfiguration))

      select ! Wakeup
      producer.expectMsg(new ViolationMessage(systemId = systemId, time = new Date(time), violation = testResult))

      val path = Path(Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId))
      val completion = storage.get[ZkModuleCompletionStatus](path)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(time)

    }
    "Send 2 message when 2 old state exists" in {
      //create Daily report
      storage.createEmptyPath(Paths.Systems.getSystemPath(systemId))
      val writer = ProcessedViolationTable.makeWriter(testUtil.getConfiguration, tableKeeper)
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val testResult = createViolation1(time)
      writer.writeRow(testResult, testResult)

      val testResult2 = createViolation2(timeYesterday)
      writer.writeRow(testResult2, testResult2)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectViolations(storage, producer.ref, testUtil.getConfiguration))
      select ! Wakeup
      producer.expectMsg(new ViolationMessage(systemId = systemId, time = new Date(time), violation = testResult))
      producer.expectMsg(new ViolationMessage(systemId = systemId, time = new Date(timeYesterday), violation = testResult2))

      val path = Path(Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId))
      val completion = storage.get[ZkModuleCompletionStatus](path)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(timeYesterday)
    }
    "not send message after update" in {
      //create Daily report
      storage.createEmptyPath(Paths.Systems.getSystemPath(systemId))
      val writer = ProcessedViolationTable.makeWriter(testUtil.getConfiguration, tableKeeper)
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val testResult = createViolation1(time)
      writer.writeRow(testResult, testResult)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectViolations(storage, producer.ref, testUtil.getConfiguration))
      select ! Wakeup
      producer.expectMsg(new ViolationMessage(systemId = systemId, time = new Date(time), violation = testResult))
      select ! Wakeup
      producer.expectNoMsg(1 second)
    }
    "Send message after update" in {
      //create Daily report
      storage.createEmptyPath(Paths.Systems.getSystemPath(systemId))
      val writer = ProcessedViolationTable.makeWriter(testUtil.getConfiguration, tableKeeper)
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val testResult = createViolation1(time)
      writer.writeRow(testResult, testResult)

      val testResult2 = createViolation2(timeYesterday)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectViolations(storage, producer.ref, testUtil.getConfiguration))
      select ! Wakeup
      producer.expectMsg(new ViolationMessage(systemId = systemId, time = new Date(time), violation = testResult))
      producer.expectNoMsg(1 second)

      writer.writeRow(testResult2, testResult2)
      select ! Wakeup
      producer.expectMsg(new ViolationMessage(systemId = systemId, time = new Date(timeYesterday), violation = testResult2))

      val path = Path(Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId))
      val completion = storage.get[ZkModuleCompletionStatus](path)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(timeYesterday)
    }
  }

  def createViolation1(time: Long): ProcessedViolationRecord = {
    val lane = Lane(laneId = "3215-1-RD1",
      name = "RD1",
      gantry = "1",
      system = systemId,
      sensorGPS_longitude = 52.397026,
      sensorGPS_latitude = 4.626074)

    val pass: VehicleMetadata = VehicleMetadata(lane = lane,
      eventId = "3203-1-RD1-" + time,
      eventTimestamp = time,
      eventTimestampStr = None, //Only used for testing
      license = Some(ValueWithConfidence[String]("5SXT66", 71)),
      rawLicense = Some("5_SXT_66"),
      length = Some(ValueWithConfidence_Float(5.4F, 95)),
      category = None,
      speed = Some(ValueWithConfidence_Float(102, 95)),
      country = Some(ValueWithConfidence[String]("NL", 83)),
      images = Seq(),
      NMICertificate = "TP8372",
      applChecksum = "",
      serialNr = "PCB060976")
    val speed = VehicleSpeedRecord(id = "3215-1-RD1-4141604954084964-1",
      entry = pass,
      exit = None,
      corridorId = 1,
      speed = 102,
      measurementSHA = "6c7ecf4a")
    val cl = VehicleClassifiedRecord(id = "3215-1-RD1-4141604954084964-1",
      speedRecord = speed,
      code = Some(VehicleCode.PA),
      indicator = ProcessingIndicator.Automatic, mil = false, invalidRdwData = false,
      validLicensePlate = None, manualAccordingToSpec = false,
      reason = IndicatorReason(0), alternativeClassification = None, dambord = None)

    val vio = VehicleViolationRecord(id = "3215-1-RD1-4141604954084964-1",
      classifiedRecord = cl,
      enforcedSpeed = 70,
      dynamicSpeedLimit = None,
      recognizeResults = List())
    ProcessedViolationRecord(id = "3215-1-RD1-4141604954084964-1",
      violationRecord = vio,
      registered = true,
      pardoned = false,
      pardonReason = None,
      violationJarSha = "6c7ecf4a")
  }
  def createViolation2(time: Long): ProcessedViolationRecord = {
    val pass: VehicleMetadata = new VehicleMetadata(lane = Lane(laneId = "3215-2-RD1",
      name = "RD1",
      gantry = "2",
      system = systemId,
      sensorGPS_longitude = 52.397026,
      sensorGPS_latitude = 4.626074),
      eventId = "3203-2-RD1-" + time,
      eventTimestamp = time,
      eventTimestampStr = None, //Only used for testing
      license = Some(ValueWithConfidence[String]("5XXX66", 71)),
      rawLicense = Some("5_XXX_66"),
      length = Some(ValueWithConfidence_Float(4.5F, 95)),
      category = None,
      speed = Some(ValueWithConfidence_Float(90, 95)),
      country = Some(ValueWithConfidence[String]("DE", 83)),
      images = Seq(),
      NMICertificate = "TP8372",
      applChecksum = "",
      serialNr = "PCB060976")
    val speed = VehicleSpeedRecord(id = "3215-2-RD1-4141604954084964-1",
      entry = pass,
      exit = None,
      corridorId = 1,
      speed = 102,
      measurementSHA = "6c7ecf4a")
    val cl = VehicleClassifiedRecord(id = "3215-2-RD1-4141604954084964-1",
      speedRecord = speed,
      code = Some(VehicleCode.PA),
      indicator = ProcessingIndicator.Manual, mil = false, invalidRdwData = false,
      validLicensePlate = None, manualAccordingToSpec = true,
      reason = IndicatorReason(0), alternativeClassification = None, dambord = None)

    val vio = VehicleViolationRecord(id = "3215-2-RD1-4141604954084964-1",
      classifiedRecord = cl,
      enforcedSpeed = 70,
      dynamicSpeedLimit = None,
      recognizeResults = List())
    ProcessedViolationRecord(id = "3215-1-RD1-4141604954084964-1",
      violationRecord = vio,
      registered = true,
      pardoned = false,
      pardonReason = None,
      violationJarSha = "6c7ecf4a")
  }
}
