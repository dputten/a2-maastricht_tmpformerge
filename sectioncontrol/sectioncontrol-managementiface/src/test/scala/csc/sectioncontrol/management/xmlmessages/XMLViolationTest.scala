/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.xmlmessages

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import java.util.Date
import csc.sectioncontrol.management._
import csc.sectioncontrol.storage.ZkApplicationInfo
import csc.sectioncontrol.messages._
import csc.sectioncontrol.storage.ZkApplicationInfo
import scala.Some
import csc.sectioncontrol.storage.ZkApplicationInfo
import scala.Some
import csc.sectioncontrol.storage.ZkApplicationInfo
import scala.Some
import csc.sectioncontrol.storage.ZkApplicationInfo
import scala.Some
import csc.sectioncontrol.storage.ZkApplicationInfo
import scala.Some
import csc.sectioncontrol.storage.ZkApplicationInfo
import scala.Some
import csc.sectioncontrol.messages.ValueWithConfidence
import csc.sectioncontrol.storage.ZkApplicationInfo
import csc.sectioncontrol.messages.VehicleMetadata
import csc.sectioncontrol.messages.ValueWithConfidence_Float
import scala.Some
import csc.sectioncontrol.messages.IndicatorReason
import csc.sectioncontrol.messages.VehicleSpeedRecord
import csc.sectioncontrol.messages.Lane
import csc.sectioncontrol.messages.ProcessedViolationRecord
import csc.sectioncontrol.common.DayUtility
import csc.sectioncontrol.messages.ValueWithConfidence
import csc.sectioncontrol.messages.VehicleViolationRecord
import csc.sectioncontrol.storage.ZkApplicationInfo
import csc.sectioncontrol.messages.VehicleMetadata
import csc.sectioncontrol.messages.ValueWithConfidence_Float
import scala.Some
import csc.sectioncontrol.management.RegistrationMessage
import csc.sectioncontrol.messages.IndicatorReason
import csc.sectioncontrol.messages.VehicleSpeedRecord
import csc.sectioncontrol.messages.VehicleImage
import csc.sectioncontrol.messages.Lane
import csc.sectioncontrol.messages.ProcessedViolationRecord
import org.slf4j.LoggerFactory

class XMLViolationTest extends WordSpec with MustMatchers {
  private val log = LoggerFactory.getLogger(this.getClass.getName)

  val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
  var time = DayUtility.substractOneDay(startDay) //start yesterday
  time = DayUtility.substractOneDay(time) //start day before

  "XMLRegistration" must {
    "generate correct XML for auto violation" in {
      val systemId = "3215"

      val msg = ViolationMessage(
        systemId, new Date(time),
        createFullAutoViolation(ind = ProcessingIndicator.Automatic, accordingToSpec = false, reason = 0))
      val builder = new XMLMessageBuilder(ZkApplicationInfo(id = "EG033", systemType = "SectionControl", version = "1.0.1.11"))

      val result = builder.createMessage(msg)
      log.info(new String(result))
      CheckXML.checkMessage(result)
    }
    "generate correct XML for mobi violation" in {
      val systemId = "3215"

      val msg = ViolationMessage(
        systemId, new Date(time),
        createFullAutoViolation(ind = ProcessingIndicator.Mobi, accordingToSpec = false, reason = 0))
      val builder = new XMLMessageBuilder(ZkApplicationInfo(id = "EG033", systemType = "SectionControl", version = "1.0.1.11"))

      val result = builder.createMessage(msg)
      log.info(new String(result))
      CheckXML.checkMessage(result)
    }
    "generate correct XML for manual accourding spec violation" in {
      val systemId = "3215"

      val msg = ViolationMessage(
        systemId, new Date(time),
        createFullAutoViolation(ind = ProcessingIndicator.Manual, accordingToSpec = true, reason = 2))
      val builder = new XMLMessageBuilder(ZkApplicationInfo(id = "EG033", systemType = "SectionControl", version = "1.0.1.11"))

      val result = builder.createMessage(msg)
      log.info(new String(result))
      CheckXML.checkMessage(result)
    }
    "generate correct XML for manual violation" in {
      val systemId = "3215"

      val msg = ViolationMessage(
        systemId, new Date(time),
        createFullAutoViolation(ind = ProcessingIndicator.Manual, accordingToSpec = false, reason = 7))
      val builder = new XMLMessageBuilder(ZkApplicationInfo(id = "EG033", systemType = "SectionControl", version = "1.0.1.11"))

      val result = builder.createMessage(msg)
      log.info(new String(result))
      CheckXML.checkMessage(result)
    }
    "generate correct XML for manual empty code violation" in {
      val systemId = "3215"

      val msg = ViolationMessage(systemId, new Date(time), createAlternateSpeedViolation)
      val builder = new XMLMessageBuilder(ZkApplicationInfo(id = "EG033", systemType = "SectionControl", version = "1.0.1.11"))

      val result = builder.createMessage(msg)
      log.info(new String(result))
      CheckXML.checkMessage(result)
    }

    "generate correct XML for redlight violation" in {
      val systemId = "3215"

      val msg = ViolationMessage(systemId, new Date(time), createFullRedViolation())
      val builder = new XMLMessageBuilder(ZkApplicationInfo(id = "EG033", systemType = "SectionControl", version = "1.0.1.11"))

      val result = builder.createMessage(msg)
      log.info(new String(result))
      CheckXML.checkMessage(result)
    }
    "generate correct XML for pardonned violation" in {
      val systemId = "3215"

      val msg = ViolationMessage(systemId, new Date(time), createPardonViolation())
      val builder = new XMLMessageBuilder(ZkApplicationInfo(id = "EG033", systemType = "SectionControl", version = "1.0.1.11"))

      val result = builder.createMessage(msg)
      log.info(new String(result))
      CheckXML.checkMessage(result)
    }
  }

  def createFullAutoViolation(ind: ProcessingIndicator.Value, accordingToSpec: Boolean, reason: Int): ProcessedViolationRecord = {
    val lane = Lane(laneId = "3215-1-RD1",
      name = "RD1",
      gantry = "1",
      system = "3215",
      sensorGPS_longitude = 52.397026,
      sensorGPS_latitude = 4.626074)

    val pass: VehicleMetadata = VehicleMetadata(lane = lane,
      eventId = "3203-1-RD1-" + time,
      eventTimestamp = time,
      eventTimestampStr = None, //Only used for testing
      license = Some(ValueWithConfidence[String]("5SXT66", 71)),
      rawLicense = Some("5_SXT_66"),
      length = Some(ValueWithConfidence_Float(5.4F, 95)),
      category = None,
      speed = Some(ValueWithConfidence_Float(102, 95)),
      country = Some(ValueWithConfidence[String]("NL", 83)),
      images = Seq(),
      NMICertificate = "TP8372",
      applChecksum = "",
      serialNr = "PCB060976")
    val speed = VehicleSpeedRecord(id = "3215-1-RD1-4141604954084964-1",
      entry = pass,
      exit = None,
      corridorId = 1,
      speed = 102,
      measurementSHA = "6c7ecf4a")
    val cl = VehicleClassifiedRecord(id = "3215-1-RD1-4141604954084964-1",
      speedRecord = speed,
      code = Some(VehicleCode.PA),
      indicator = ind, mil = false, invalidRdwData = false,
      validLicensePlate = None, manualAccordingToSpec = accordingToSpec,
      reason = IndicatorReason(reason), alternativeClassification = None, dambord = None)

    val vio = VehicleViolationRecord(id = "3215-1-RD1-4141604954084964-1",
      classifiedRecord = cl,
      enforcedSpeed = 70,
      dynamicSpeedLimit = None,
      recognizeResults = List())
    ProcessedViolationRecord(id = "3215-1-RD1-4141604954084964-1",
      violationRecord = vio,
      registered = true,
      pardoned = false,
      pardonReason = None,
      violationJarSha = "6c7ecf4a")
  }

  def createFullRedViolation(): ProcessedViolationRecord = {
    val lane = Lane(laneId = "3215-1-RD1",
      name = "RD1",
      gantry = "1",
      system = "3215",
      sensorGPS_longitude = 52.397026,
      sensorGPS_latitude = 4.626074)

    val pass: VehicleMetadata = VehicleMetadata(lane = lane,
      eventId = "3203-1-RD1-" + time,
      eventTimestamp = time,
      eventTimestampStr = None, //Only used for testing
      license = Some(ValueWithConfidence[String]("5SXT66", 71)),
      rawLicense = Some("5_SXT_66"),
      length = Some(ValueWithConfidence_Float(5.4F, 95)),
      category = None,
      speed = Some(ValueWithConfidence_Float(102, 95)),
      country = Some(ValueWithConfidence[String]("NL", 83)),
      images = Seq(VehicleImage(timestamp = time, offset = 0, uri = "", imageType = VehicleImageType.RedLight, checksum = "", timeYellow = Some(65000), timeRed = Some(1))),
      NMICertificate = "TP8372",
      applChecksum = "",
      serialNr = "PCB060976")
    val speed = VehicleSpeedRecord(id = "3215-1-RD1-4141604954084964-1",
      entry = pass,
      exit = None,
      corridorId = 1,
      speed = 102,
      measurementSHA = "6c7ecf4a")
    val cl = VehicleClassifiedRecord(id = "3215-1-RD1-4141604954084964-1",
      speedRecord = speed,
      code = Some(VehicleCode.PA),
      indicator = ProcessingIndicator.Automatic, mil = false, invalidRdwData = false,
      validLicensePlate = None, manualAccordingToSpec = false,
      reason = IndicatorReason(0), alternativeClassification = None, dambord = None)

    val vio = VehicleViolationRecord(id = "3215-1-RD1-4141604954084964-1",
      classifiedRecord = cl,
      enforcedSpeed = 70,
      dynamicSpeedLimit = None,
      recognizeResults = List())
    ProcessedViolationRecord(id = "3215-1-RD1-4141604954084964-1",
      violationRecord = vio,
      registered = true,
      pardoned = false,
      pardonReason = None,
      violationJarSha = "6c7ecf4a")
  }

  def createAlternateSpeedViolation(): ProcessedViolationRecord = {
    val lane = Lane(laneId = "3215-1-RD1",
      name = "RD1",
      gantry = "1",
      system = "3215",
      sensorGPS_longitude = 52.397026,
      sensorGPS_latitude = 4.626074)

    val pass: VehicleMetadata = VehicleMetadata(lane = lane,
      eventId = "3203-1-RD1-" + time,
      eventTimestamp = time,
      eventTimestampStr = None, //Only used for testing
      license = Some(ValueWithConfidence[String]("5SXT66", 71)),
      rawLicense = Some("5_SXT_66"),
      length = Some(ValueWithConfidence_Float(5.4F, 95)),
      category = None,
      speed = Some(ValueWithConfidence_Float(102, 95)),
      country = Some(ValueWithConfidence[String]("NL", 83)),
      images = Seq(VehicleImage(timestamp = time, offset = 0, uri = "", imageType = VehicleImageType.RedLight, checksum = "", timeYellow = Some(65000), timeRed = Some(1))),
      NMICertificate = "TP8372",
      applChecksum = "",
      serialNr = "PCB060976")
    val speed = VehicleSpeedRecord(id = "3215-1-RD1-4141604954084964-1",
      entry = pass,
      exit = None,
      corridorId = 1,
      speed = 102,
      measurementSHA = "6c7ecf4a")
    val cl = VehicleClassifiedRecord(id = "3215-1-RD1-4141604954084964-1",
      speedRecord = speed,
      code = None,
      indicator = ProcessingIndicator.Manual, mil = false, invalidRdwData = false,
      validLicensePlate = None, manualAccordingToSpec = false,
      reason = IndicatorReason(0), alternativeClassification = Some(VehicleCode.PA), dambord = None)

    val vio = VehicleViolationRecord(id = "3215-1-RD1-4141604954084964-1",
      classifiedRecord = cl,
      enforcedSpeed = 70,
      dynamicSpeedLimit = None,
      recognizeResults = List())
    ProcessedViolationRecord(id = "3215-1-RD1-4141604954084964-1",
      violationRecord = vio,
      registered = true,
      pardoned = false,
      pardonReason = None,
      violationJarSha = "6c7ecf4a")
  }

  def createPardonViolation(): ProcessedViolationRecord = {
    val lane = Lane(laneId = "3215-1-RD1",
      name = "RD1",
      gantry = "1",
      system = "3215",
      sensorGPS_longitude = 52.397026,
      sensorGPS_latitude = 4.626074)

    val pass: VehicleMetadata = VehicleMetadata(lane = lane,
      eventId = "3203-1-RD1-" + time,
      eventTimestamp = time,
      eventTimestampStr = None, //Only used for testing
      license = Some(ValueWithConfidence[String]("5SXT66", 71)),
      rawLicense = Some("5_SXT_66"),
      length = Some(ValueWithConfidence_Float(5.4F, 95)),
      category = None,
      speed = Some(ValueWithConfidence_Float(102, 95)),
      country = Some(ValueWithConfidence[String]("NL", 83)),
      images = Seq(),
      NMICertificate = "TP8372",
      applChecksum = "",
      serialNr = "PCB060976")
    val speed = VehicleSpeedRecord(id = "3215-1-RD1-4141604954084964-1",
      entry = pass,
      exit = None,
      corridorId = 1,
      speed = 102,
      measurementSHA = "6c7ecf4a")
    val cl = VehicleClassifiedRecord(id = "3215-1-RD1-4141604954084964-1",
      speedRecord = speed,
      code = Some(VehicleCode.PA),
      indicator = ProcessingIndicator.Automatic, mil = false, invalidRdwData = false,
      validLicensePlate = None, manualAccordingToSpec = false,
      reason = IndicatorReason(0), alternativeClassification = None, dambord = None)

    val vio = VehicleViolationRecord(id = "3215-1-RD1-4141604954084964-1",
      classifiedRecord = cl,
      enforcedSpeed = 70,
      dynamicSpeedLimit = None,
      recognizeResults = List())
    ProcessedViolationRecord(id = "3215-1-RD1-4141604954084964-1",
      violationRecord = vio,
      registered = true,
      pardoned = true,
      pardonReason = Some("Callibration error"),
      violationJarSha = "6c7ecf4a")
  }

}