/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.modules

import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import akka.actor.ActorSystem
import csc.curator.utils.{ CuratorToolsImpl, Curator }
import net.liftweb.json.DefaultFormats
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.storage._
import csc.sectioncontrol.messages.{ EsaProviderType, VehicleImageType, VehicleCode }
import csc.akkautils.DirectLogging
import akka.testkit.{ TestActorRef, TestProbe, TestKit }
import csc.sectioncontrol.management.{ AlertMessage, ModuleIds, Wakeup }
import akka.util.duration._
import csc.sectioncontrol.common.DayUtility
import csc.config.Path
import java.util.Date
import csc.curator.CuratorTestServer
import csc.sectioncontrol.storagelayer.alerts.AlertService
import csc.sectioncontrol.storagelayer.corridor.CorridorService

class SelectAlertsTest
  extends TestKit(ActorSystem("SelectAlertsTest")) with WordSpec with MustMatchers
  with CuratorTestServer with DirectLogging with BeforeAndAfterAll {

  override protected def afterAll() {
    super.afterAll()
    system.shutdown()
  }

  def createCorridor(systemId: String, corridorId: String) {
    val corridor = ZkCorridor(id = corridorId,
      name = "S1-name",
      roadType = 0,
      serviceType = ServiceType.SectionControl,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = false,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = ZkPSHTMIdentification(useYear = false,
        useMonth = true,
        useDay = false,
        useReportingOfficerId = false,
        useNumberOfTheDay = true,
        useLocationCode = false,
        useHHMCode = false,
        useTypeViolation = false,
        useFixedCharacter = false,
        fixedCharacter = "T"),
      info = ZkCorridorInfo(corridorId = 1,
        locationCode = "2",
        reportId = "",
        reportHtId = "",
        reportPerformance = true,
        locationLine1 = ""),
      startSectionId = "sec1",
      endSectionId = "sec1",
      allSectionIds = Seq("sec1"),
      direction = ZkDirection(directionFrom = "",
        directionTo = ""),
      radarCode = 4,
      roadCode = 0,
      dutyType = "",
      deploymentCode = "",
      codeText = "")
    CorridorService.createCorridor(storage, systemId, corridor)
  }

  private var storage: Curator = _
  val moduleId = ModuleIds.alert

  private def createCorridor(): ZkCorridor = {
    ZkCorridor(id = "S1",
      name = "S1-name",
      roadType = 0,
      serviceType = ServiceType.SectionControl,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = false,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = ZkPSHTMIdentification(useYear = false,
        useMonth = true,
        useDay = false,
        useReportingOfficerId = false,
        useNumberOfTheDay = true,
        useLocationCode = false,
        useHHMCode = false,
        useTypeViolation = false,
        useFixedCharacter = false,
        fixedCharacter = "T"),
      info = ZkCorridorInfo(corridorId = 1,
        locationCode = "2",
        reportId = "",
        reportHtId = "",
        reportPerformance = true,
        locationLine1 = ""),
      startSectionId = "sec1",
      endSectionId = "sec1",
      allSectionIds = Seq("sec1"),
      direction = ZkDirection(directionFrom = "",
        directionTo = ""),
      radarCode = 4,
      roadCode = 0,
      dutyType = "",
      deploymentCode = "",
      codeText = "")
  }

  override def beforeEach() {
    super.beforeEach()

    val formats = DefaultFormats + new EnumerationSerializer(
      ZkWheelbaseType,
      VehicleCode,
      ZkIndicationType,
      ServiceType,
      ZkCaseFileType,
      EsaProviderType,
      VehicleImageType,
      ConfigType)
    storage = new CuratorToolsImpl(clientScope, log, formats)

    val corridor = createCorridor()
    CorridorService.createCorridor(storage, "EG100", corridor)
    storage.put(Path("/ctes") / "users" / "testUser", new ZkUser("testUser", "testReportingOfficer", "", "", "", "", "", "", "", List()))
    storage.put(Path("/ctes") / "users" / "testUser1", new ZkUser("testUser1", "testReportingOfficer", "", "", "", "", "", "", "", List()))
  }

  "Selectalerts" must {
    "Send no messages when empty" in {
      val producer = TestProbe()
      val select = TestActorRef(new SelectAlerts(storage, producer.ref))
      select ! Wakeup
      producer.expectNoMsg(2 seconds)
    }
    "Send 1 message when old alert exists" in {
      //create Daily report
      val systemId = "EG100"
      val corridorId = "S1"

      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      var time = DayUtility.substractOneDay(startDay) //start yesterday
      time = DayUtility.substractOneDay(time) //start day before

      val alert = new ZkAlert(path = "3215-1-camera", alertType = "", message = "Connect error", timestamp = time,
        configType = ConfigType.Lane, reductionFactor = 0.5F)
      AlertService.create(systemId, corridorId, alert, storage)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectAlerts(storage, producer.ref))

      select ! Wakeup

      producer.expectMsg(new AlertMessage(systemId = systemId, corridorId = corridorId, alertType = alert.alertType, time = new Date(time),
        path = alert.path, message = alert.message, configType = alert.configType.toString,
        reductionFactor = alert.reductionFactor, startTime = None))

      val path = Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId)
      val completion = storage.get[ZkModuleCompletionStatus](path + "/" + corridorId)
      //println("completion")
      //println(completion)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(time)
    }
    "Send 2 message when 2 old state exists" in {
      //create Daily report
      val systemId = "EG100"
      val corridorId = "S1"
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val alert = new ZkAlert(path = "3215-1-camera", alertType = "", message = "Connect error", timestamp = time,
        configType = ConfigType.Lane, reductionFactor = 0.5F)
      AlertService.create(systemId, corridorId, alert, storage)

      val alert2 = new ZkAlert(path = "3215-2-camera", alertType = "", message = "FTP error", timestamp = timeYesterday, configType = ConfigType.Lane, reductionFactor = 0.25F)
      AlertService.create(systemId, corridorId, alert2, storage)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectAlerts(storage, producer.ref))
      select ! Wakeup
      producer.expectMsg(new AlertMessage(systemId = systemId, corridorId = corridorId, alertType = alert.alertType, time = new Date(time),
        path = alert.path, message = alert.message, configType = alert.configType.toString,
        reductionFactor = alert.reductionFactor, startTime = None))
      producer.expectMsg(new AlertMessage(systemId = systemId, corridorId = corridorId, alertType = alert2.alertType, time = new Date(timeYesterday),
        path = alert2.path, message = alert2.message, configType = alert2.configType.toString,
        reductionFactor = alert2.reductionFactor, startTime = None))

      val path = Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId)
      val completion = storage.get[ZkModuleCompletionStatus](path + "/" + corridorId)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(timeYesterday)

    }
    "not send message after update" in {
      //create Daily report
      val systemId = "EG100"
      val corridorId = "S1"
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before
      val alert = new ZkAlert(path = "3215-1-camera", alertType = "",
        message = "Connect error", timestamp = time, configType = ConfigType.Lane, reductionFactor = 0.5F)
      AlertService.create(systemId, corridorId, alert, storage)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectAlerts(storage, producer.ref))
      select ! Wakeup

      producer.expectMsg(new AlertMessage(systemId = systemId, corridorId = corridorId, alertType = alert.alertType, time = new Date(time),
        path = alert.path, message = alert.message, configType = alert.configType.toString,
        reductionFactor = alert.reductionFactor, startTime = None))

      select ! Wakeup
      producer.expectNoMsg(1 second)
    }
    "Send message after update" in {
      //create Daily report
      val systemId = "EG100"
      val corridorId = "S1"
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val alert = new ZkAlert(path = "3215-1-camera", alertType = "", message = "Connect error", timestamp = time, configType = ConfigType.Lane, reductionFactor = 0.5F)
      AlertService.create(systemId, corridorId, alert, storage)

      val alert2 = new ZkAlert(path = "3215-2-camera", alertType = "", message = "FTP error", timestamp = timeYesterday, configType = ConfigType.Lane, reductionFactor = 0.25F)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectAlerts(storage, producer.ref))
      select ! Wakeup
      producer.expectMsg(new AlertMessage(systemId = systemId, corridorId = corridorId, alertType = alert.alertType, time = new Date(time),
        path = alert.path, message = alert.message, configType = alert.configType.toString,
        reductionFactor = alert.reductionFactor, startTime = None))

      producer.expectNoMsg(1 second)

      AlertService.create(systemId, corridorId, alert2, storage)

      select ! Wakeup
      producer.expectMsg(new AlertMessage(systemId = systemId, corridorId = corridorId, alertType = alert2.alertType, time = new Date(timeYesterday),
        path = alert2.path, message = alert2.message, configType = alert2.configType.toString,
        reductionFactor = alert2.reductionFactor, startTime = None))
    }
    "Send 1 message when old history exists" in {
      //create Daily report
      val systemId = "EG100"
      val corridorId = "S1"

      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val alert = new ZkAlertHistory(path = "3215-1-camera", alertType = "", message = "Connect error", startTime = time,
        configType = ConfigType.Lane.toString, endTime = timeYesterday, reductionFactor = 0.5F)
      AlertService.addToHistory(systemId, corridorId, alert, storage)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectAlerts(storage, producer.ref))

      select ! Wakeup
      producer.expectMsg(new AlertMessage(systemId = systemId, corridorId = corridorId, alertType = alert.alertType, time = new Date(timeYesterday),
        path = alert.path, message = alert.message, configType = alert.configType.toString,
        reductionFactor = alert.reductionFactor, startTime = Some(new Date(time))))

      val path = Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId)
      val completion = storage.get[ZkModuleCompletionStatus](path + "/" + corridorId)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(timeYesterday)
    }
    "Send 2 message when 2 old history exists" in {
      //create Daily report
      val systemId = "EG100"
      val corridorId = "S1"
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val alert = new ZkAlertHistory(path = "3215-1-camera", alertType = "", message = "Connect error", startTime = time,
        configType = ConfigType.Lane.toString, endTime = timeYesterday, reductionFactor = 0.5F)
      AlertService.addToHistory(systemId, corridorId, alert, storage)

      val alert2 = new ZkAlertHistory(path = "3215-2-camera", alertType = "", message = "FTP error", startTime = timeYesterday,
        configType = ConfigType.Lane.toString, endTime = startDay, reductionFactor = 0.25F)
      AlertService.addToHistory(systemId, corridorId, alert2, storage)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectAlerts(storage, producer.ref))
      select ! Wakeup
      producer.expectMsg(new AlertMessage(systemId = systemId, corridorId = corridorId, alertType = alert.alertType, time = new Date(timeYesterday),
        path = alert.path, message = alert.message, configType = alert.configType.toString,
        reductionFactor = alert.reductionFactor, startTime = Some(new Date(time))))
      producer.expectMsg(new AlertMessage(systemId = systemId, corridorId = corridorId, alertType = alert2.alertType, time = new Date(startDay),
        path = alert2.path, message = alert2.message, configType = alert2.configType,
        reductionFactor = alert2.reductionFactor, startTime = Some(new Date(timeYesterday))))

      val path = Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId)
      val completion = storage.get[ZkModuleCompletionStatus](path + "/" + corridorId)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(startDay)

    }
    "not send message after update for history" in {
      //create Daily report
      val systemId = "EG100"
      val corridorId = "S1"
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before
      val alert = new ZkAlertHistory(path = "3215-1-camera", alertType = "", message = "Connect error", startTime = time,
        configType = ConfigType.Lane.toString, endTime = timeYesterday, reductionFactor = 0.5F)

      AlertService.addToHistory(systemId, corridorId, alert, storage)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectAlerts(storage, producer.ref))
      select ! Wakeup
      producer.expectMsg(new AlertMessage(systemId = systemId, corridorId = corridorId, alertType = alert.alertType, time = new Date(timeYesterday),
        path = alert.path, message = alert.message, configType = alert.configType.toString,
        reductionFactor = alert.reductionFactor, startTime = Some(new Date(time))))

      select ! Wakeup
      producer.expectNoMsg(1 second)
    }
    "Send message after update for history" in {
      //create Daily report
      val systemId = "EG100"
      val corridorId = "S1"
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val alert = new ZkAlertHistory(path = "3215-1-camera", alertType = "", message = "Connect error", startTime = time,
        configType = ConfigType.Lane.toString, endTime = timeYesterday, reductionFactor = 0.5F)
      AlertService.addToHistory(systemId, corridorId, alert, storage)

      val alert2 = new ZkAlertHistory(path = "3215-2-camera", alertType = "", message = "FTP error",
        startTime = timeYesterday, configType = ConfigType.Lane.toString, endTime = startDay, reductionFactor = 0.25F)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectAlerts(storage, producer.ref))
      select ! Wakeup
      producer.expectMsg(new AlertMessage(systemId = systemId, corridorId = corridorId, alertType = alert.alertType, time = new Date(timeYesterday),
        path = alert.path, message = alert.message, configType = alert.configType.toString,
        reductionFactor = alert.reductionFactor, startTime = Some(new Date(time))))
      producer.expectNoMsg(1 second)

      AlertService.addToHistory(systemId, corridorId, alert2, storage)

      select ! Wakeup
      producer.expectMsg(new AlertMessage(systemId = systemId, corridorId = corridorId, alertType = alert2.alertType, time = new Date(startDay),
        path = alert2.path, message = alert2.message, configType = alert2.configType,
        reductionFactor = alert2.reductionFactor, startTime = Some(new Date(timeYesterday))))
    }
    "Send 2 message when old history and current exists" in {
      //create Daily report
      val systemId = "EG100"
      val corridorId = "S1"

      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val alert = new ZkAlertHistory(path = "3215-1-camera", alertType = "", message = "Connect error", startTime = time,
        configType = ConfigType.Lane.toString, endTime = timeYesterday, reductionFactor = 0.5F)

      AlertService.addToHistory(systemId, corridorId, alert, storage)

      val curalert = new ZkAlert(path = "3216-1-camera", alertType = "", message = "FTP error", timestamp = time, configType = ConfigType.Lane, reductionFactor = 0.25F)
      AlertService.create(systemId, corridorId, curalert, storage)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectAlerts(storage, producer.ref))

      select ! Wakeup
      producer.expectMsg(new AlertMessage(systemId = systemId, corridorId = corridorId, alertType = curalert.alertType, time = new Date(time),
        path = curalert.path, message = curalert.message, configType = curalert.configType.toString.toString,
        reductionFactor = curalert.reductionFactor, startTime = None))
      producer.expectMsg(new AlertMessage(systemId = systemId, corridorId = corridorId, alertType = alert.alertType, time = new Date(timeYesterday),
        path = alert.path, message = alert.message, configType = alert.configType.toString,
        reductionFactor = alert.reductionFactor, startTime = Some(new Date(time))))

      val path = Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId)
      val completion = storage.get[ZkModuleCompletionStatus](path + "/" + corridorId)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(timeYesterday)
    }
  }
}