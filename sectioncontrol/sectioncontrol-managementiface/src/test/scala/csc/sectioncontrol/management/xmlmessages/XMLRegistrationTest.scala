/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.xmlmessages

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import java.util.Date
import csc.sectioncontrol.management.{ RegistrationMessage, AlertMessage }
import csc.sectioncontrol.storage.ZkApplicationInfo
import csc.sectioncontrol.messages.{ ValueWithConfidence_Float, ValueWithConfidence }
import org.slf4j.LoggerFactory

class XMLRegistrationTest extends WordSpec with MustMatchers {
  private val log = LoggerFactory.getLogger(this.getClass.getName)

  "XMLRegistration" must {
    "generate correct XML for full filled Registration" in {
      val systemId = "3215"

      val msg = new RegistrationMessage(systemId = systemId, time = new Date(),
        laneId = systemId + "-1-RD",
        speed = Some(new ValueWithConfidence_Float(80)),
        license = Some(new ValueWithConfidence[String]("12XXX3", 50)),
        country = Some(new ValueWithConfidence[String]("NL", 55)),
        category = Some(new ValueWithConfidence[String]("Car", 60)))

      val builder = new XMLMessageBuilder(ZkApplicationInfo(id = "EG033", systemType = "SectionControl", version = "1.0.1.11"))

      val result = builder.createMessage(msg)
      log.info(new String(result))
      CheckXML.checkMessage(result)
    }
    "generate correct XML for empty Registration" in {
      val systemId = "3215"

      val msg = new RegistrationMessage(systemId = systemId, time = new Date(),
        laneId = systemId + "-1-RD",
        speed = None,
        license = None,
        country = None,
        category = None)

      val builder = new XMLMessageBuilder(ZkApplicationInfo(id = "EG033", systemType = "SectionControl", version = "1.0.1.11"))

      val result = builder.createMessage(msg)
      log.info(new String(result))
      CheckXML.checkMessage(result)
    }
  }

}