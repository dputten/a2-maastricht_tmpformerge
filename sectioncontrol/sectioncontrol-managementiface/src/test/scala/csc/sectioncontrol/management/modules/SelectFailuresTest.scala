/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.modules

import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import akka.actor.ActorSystem
import csc.curator.utils.{ CuratorToolsImpl, Curator }
import net.liftweb.json.{ DefaultFormats, Formats }
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.storage._
import csc.sectioncontrol.messages.{ EsaProviderType, VehicleImageType, VehicleCode }
import csc.akkautils.DirectLogging
import akka.testkit.{ TestActorRef, TestProbe, TestKit }
import csc.sectioncontrol.management.{ FailureMessage, ModuleIds, Wakeup }
import akka.util.duration._
import csc.sectioncontrol.common.DayUtility
import csc.config.Path
import java.util.Date
import csc.curator.CuratorTestServer
import csc.sectioncontrol.storagelayer.state.FailureService

class SelectFailuresTest
  extends TestKit(ActorSystem("SelectFailuresTest")) with WordSpec with MustMatchers
  with CuratorTestServer with DirectLogging with BeforeAndAfterAll {

  override protected def afterAll() {
    super.afterAll()
    system.shutdown()
  }

  var storage: Curator = _
  val moduleId = ModuleIds.failure

  override def beforeEach() {
    super.beforeEach()
    val formats = DefaultFormats + new EnumerationSerializer(
      ZkWheelbaseType,
      VehicleCode,
      ZkIndicationType,
      ServiceType,
      ZkCaseFileType,
      EsaProviderType,
      VehicleImageType,
      ConfigType)
    storage = new CuratorToolsImpl(clientScope, log, formats)

    storage.createEmptyPath(Path("/destroy"))
    storage.createEmptyPath(Path("/ctes") / "users")
    storage.createEmptyPath(Path("/ctes") / "configuration")
    storage.put(Path("/ctes") / "users" / "testUser", new ZkUser("testUser", "testReportingOfficer", "", "", "", "", "", "", "", List()))
    storage.put(Path("/ctes") / "users" / "testUser1", new ZkUser("testUser1", "testReportingOfficer", "", "", "", "", "", "", "", List()))

    val config1Path = Path(Paths.Corridors.getConfigPath("EG100", "S1"))

    storage.createEmptyPath(config1Path)
    storage.set(config1Path, new ZkCorridor(id = "S1",
      name = "first corridor",
      roadType = 0,
      serviceType = ServiceType.RedLight,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = true,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = null,
      info = new ZkCorridorInfo(corridorId = 1,
        locationCode = "1",
        reportId = "TCS A2 Links  39.7 - 54.9",
        reportHtId = "A020011",
        reportPerformance = true,
        locationLine1 = "loc 1"),
      startSectionId = "1",
      endSectionId = "1",
      allSectionIds = Seq("1"),
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 402,
      roadCode = 2,
      dutyType = "KA2",
      deploymentCode = "03",
      codeText = ""), 0)
    val rootPathToServices = Path(Paths.Corridors.getServicesPath("EG100", "S1"))
    val pathToService = rootPathToServices / ServiceType.SectionControl.toString
    storage.createEmptyPath(pathToService)
  }

  "SelectFailures" must {
    "Send no messages when empty" in {
      val producer = TestProbe()
      val select = TestActorRef(new SelectFailures(storage, producer.ref))
      select ! Wakeup
      producer.expectNoMsg(2 seconds)
    }
    "Send 1 message when old failure exists" in {
      //create Daily report
      val systemId = "EG100"
      val corridorId = "S1"

      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      var time = DayUtility.substractOneDay(startDay) //start yesterday
      time = DayUtility.substractOneDay(time) //start day before

      val failure = new ZkFailure(path = "3215-1-camera", failureType = "", message = "Connect error", timestamp = time, configType = ConfigType.Lane, confirmed = true)
      FailureService.create(systemId, corridorId, failure, storage)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectFailures(storage, producer.ref))

      select ! Wakeup
      producer.expectMsg(new FailureMessage(systemId = systemId, corridorId = corridorId, failureType = failure.failureType, time = new Date(time),
        path = failure.path, message = failure.message, configType = failure.configType.toString,
        startTime = None))

      val path = Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId)
      val completion = storage.get[ZkModuleCompletionStatus](path + "/" + corridorId)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(time)
    }
    "Send 2 message when 2 old state exists" in {
      //create Daily report
      val systemId = "EG100"
      val corridorId = "S1"
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val failure = new ZkFailure(path = "3215-1-camera", failureType = "", message = "Connect error", timestamp = time, configType = ConfigType.Lane, confirmed = true)
      FailureService.create(systemId, corridorId, failure, storage)

      val failure2 = new ZkFailure(path = "3215-2-camera", failureType = "", message = "FTP error", timestamp = timeYesterday, configType = ConfigType.Lane, confirmed = false)
      FailureService.create(systemId, corridorId, failure2, storage)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectFailures(storage, producer.ref))
      select ! Wakeup
      producer.expectMsg(new FailureMessage(systemId = systemId, corridorId = corridorId, failureType = failure.failureType, time = new Date(time),
        path = failure.path, message = failure.message, configType = failure.configType.toString,
        startTime = None))
      producer.expectMsg(new FailureMessage(systemId = systemId, corridorId = corridorId, failureType = failure2.failureType, time = new Date(timeYesterday),
        path = failure2.path, message = failure2.message, configType = failure2.configType.toString,
        startTime = None))

      val path = Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId)
      val completion = storage.get[ZkModuleCompletionStatus](path + "/" + corridorId)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(timeYesterday)

    }
    "not send message after update" in {
      //create Daily report
      val systemId = "EG100"
      val corridorId = "S1"
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before
      val failure = new ZkFailure(path = "3215-1-camera", failureType = "", message = "Connect error", timestamp = time, configType = ConfigType.Lane, confirmed = true)
      FailureService.create(systemId, corridorId, failure, storage)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectFailures(storage, producer.ref))
      select ! Wakeup
      producer.expectMsg(new FailureMessage(systemId = systemId, corridorId = corridorId, failureType = failure.failureType, time = new Date(time),
        path = failure.path, message = failure.message, configType = failure.configType.toString,
        startTime = None))

      select ! Wakeup
      producer.expectNoMsg(1 second)
    }
    "Send message after update" in {
      //create Daily report
      val systemId = "EG100"
      val corridorId = "S1"
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val failure = new ZkFailure(path = "3215-1-camera", failureType = "", message = "Connect error", timestamp = time, configType = ConfigType.Lane, confirmed = true)
      FailureService.create(systemId, corridorId, failure, storage)

      val failure2 = new ZkFailure(path = "3215-2-camera", failureType = "", message = "FTP error", timestamp = timeYesterday, configType = ConfigType.Lane, confirmed = false)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectFailures(storage, producer.ref))
      select ! Wakeup
      producer.expectMsg(new FailureMessage(systemId = systemId, corridorId = corridorId, failureType = failure.failureType, time = new Date(time),
        path = failure.path, message = failure.message, configType = failure.configType.toString,
        startTime = None))
      producer.expectNoMsg(1 second)

      FailureService.create(systemId, corridorId, failure2, storage)
      select ! Wakeup
      producer.expectMsg(new FailureMessage(systemId = systemId, corridorId = corridorId, failureType = failure2.failureType, time = new Date(timeYesterday),
        path = failure2.path, message = failure2.message, configType = failure2.configType.toString,
        startTime = None))
    }

    "Send 1 message when old history exists" in {
      //create Daily report
      val systemId = "EG100"
      val corridorId = "S1"

      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val failure = new ZkFailureHistory(path = "3215-1-camera", failureType = "", message = "Connect error", startTime = time, configType = "Lane", endTime = timeYesterday)
      FailureService.addToHistory(systemId, corridorId, failure, storage)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectFailures(storage, producer.ref))

      select ! Wakeup
      producer.expectMsg(new FailureMessage(systemId = systemId, corridorId = corridorId, failureType = failure.failureType, time = new Date(timeYesterday),
        path = failure.path, message = failure.message, configType = failure.configType,
        startTime = Some(new Date(time))))

      val path = Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId)
      val completion = storage.get[ZkModuleCompletionStatus](path + "/" + corridorId)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(timeYesterday)
    }

    "Send 2 message when 2 old history exists" in {
      //create Daily report
      val systemId = "EG100"
      val corridorId = "S1"
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val failure = new ZkFailureHistory(path = "3215-1-camera", failureType = "", message = "Connect error", startTime = time, configType = "Lane", endTime = timeYesterday)
      FailureService.addToHistory(systemId, corridorId, failure, storage)

      val failure2 = new ZkFailureHistory(path = "3215-2-camera", failureType = "", message = "FTP error", startTime = timeYesterday, configType = "Lane", endTime = startDay)
      FailureService.addToHistory(systemId, corridorId, failure2, storage)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectFailures(storage, producer.ref))
      select ! Wakeup
      producer.expectMsg(new FailureMessage(systemId = systemId, corridorId = corridorId, failureType = failure.failureType, time = new Date(timeYesterday),
        path = failure.path, message = failure.message, configType = failure.configType,
        startTime = Some(new Date(time))))
      producer.expectMsg(new FailureMessage(systemId = systemId, corridorId = corridorId, failureType = failure2.failureType, time = new Date(startDay),
        path = failure2.path, message = failure2.message, configType = failure2.configType,
        startTime = Some(new Date(timeYesterday))))

      val path = Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId)
      val completion = storage.get[ZkModuleCompletionStatus](path + "/" + corridorId)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(startDay)

    }
    "not send message after update for history" in {
      //create Daily report
      val systemId = "EG100"
      val corridorId = "S1"
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before
      val failure = new ZkFailureHistory(path = "3215-1-camera", failureType = "", message = "Connect error", startTime = time, configType = "Lane", endTime = timeYesterday)
      FailureService.addToHistory(systemId, corridorId, failure, storage)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectFailures(storage, producer.ref))
      select ! Wakeup
      producer.expectMsg(new FailureMessage(systemId = systemId, corridorId = corridorId, failureType = failure.failureType, time = new Date(timeYesterday),
        path = failure.path, message = failure.message, configType = failure.configType,
        startTime = Some(new Date(time))))

      select ! Wakeup
      producer.expectNoMsg(1 second)
    }
    "Send message after update for history" in {
      //create Daily report
      val systemId = "EG100"
      val corridorId = "S1"
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val failure = new ZkFailureHistory(path = "3215-1-camera", failureType = "", message = "Connect error", startTime = time, configType = "Lane", endTime = timeYesterday)
      FailureService.addToHistory(systemId, corridorId, failure, storage)

      val failure2 = new ZkFailureHistory(path = "3215-2-camera", failureType = "", message = "FTP error", startTime = timeYesterday, configType = "Lane", endTime = startDay)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectFailures(storage, producer.ref))
      select ! Wakeup
      producer.expectMsg(new FailureMessage(systemId = systemId, corridorId = corridorId, failureType = failure.failureType, time = new Date(timeYesterday),
        path = failure.path, message = failure.message, configType = failure.configType,
        startTime = Some(new Date(time))))
      producer.expectNoMsg(1 second)

      FailureService.addToHistory(systemId, corridorId, failure2, storage)
      select ! Wakeup
      producer.expectMsg(new FailureMessage(systemId = systemId, corridorId = corridorId, failureType = failure2.failureType, time = new Date(startDay),
        path = failure2.path, message = failure2.message, configType = failure2.configType,
        startTime = Some(new Date(timeYesterday))))
    }
    "Send 2 message when old history and current exists" in {
      //create Daily report
      val systemId = "EG100"
      val corridorId = "S1"

      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val failure = new ZkFailureHistory(path = "3215-1-camera", failureType = "", message = "Connect error", startTime = time, configType = "Lane", endTime = timeYesterday)

      FailureService.addToHistory(systemId, corridorId, failure, storage)

      val curFailure = new ZkFailure(path = "3216-1-camera", failureType = "", message = "FTP error", timestamp = time,
        configType = ConfigType.Lane, confirmed = true)
      FailureService.create(systemId, corridorId, curFailure, storage)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectFailures(storage, producer.ref))

      select ! Wakeup
      producer.expectMsg(new FailureMessage(systemId = systemId, corridorId = corridorId, failureType = curFailure.failureType, time = new Date(time),
        path = curFailure.path, message = curFailure.message, configType = curFailure.configType.toString,
        startTime = None))
      producer.expectMsg(new FailureMessage(systemId = systemId, corridorId = corridorId, failureType = failure.failureType, time = new Date(timeYesterday),
        path = failure.path, message = failure.message, configType = failure.configType,
        startTime = Some(new Date(time))))

      val path = Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId)
      val completion = storage.get[ZkModuleCompletionStatus](path + "/" + corridorId)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(timeYesterday)
    }
  }
}