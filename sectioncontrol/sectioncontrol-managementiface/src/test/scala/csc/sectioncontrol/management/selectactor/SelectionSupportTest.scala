/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.selectactor

import akka.testkit.{ TestProbe, TestKit }
import akka.actor.ActorSystem
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.akkautils.DirectLogging
import csc.sectioncontrol.management.ModuleIds
import net.liftweb.json.Formats
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.storage._
import csc.sectioncontrol.messages.{ EsaProviderType, VehicleImageType, VehicleCode }
import csc.curator.CuratorTestServer
import csc.curator.utils.{ CuratorToolsImpl, Curator }

class SelectionSupportTest extends TestKit(ActorSystem("SelectionSupportTest")) with WordSpec with MustMatchers
  with CuratorTestServer with DirectLogging {

  var storage: Curator = _

  override def beforeEach() {
    super.beforeEach()
    storage = new CuratorToolsImpl(clientScope, log) {
      override protected def extendFormats(defaultFormats: Formats) = defaultFormats + new EnumerationSerializer(
        ZkWheelbaseType,
        VehicleCode,
        ZkIndicationType,
        ServiceType,
        ZkCaseFileType,
        EsaProviderType,
        VehicleImageType,
        ConfigType)
    }
  }

  "SelectionSupport.getSystems" must {
    "return all systems" in {
      storage.createEmptyPath(Paths.Systems.getSystemPath("3200"))
      storage.createEmptyPath(Paths.Systems.getSystemPath("3201"))
      storage.createEmptyPath(Paths.Systems.getSystemPath("3202"))

      val cfgPath = Paths.MaintenanceInterface.getConfigPath()
      val cfg = new ZkMaintenanceConfig(
        applInfo = new ZkApplicationInfo("EG100", "Flitspalen", "2.0.0.11"),
        mq = new ZkRabbitMQConnection(exchangeName = Some("amq.topic")), //use defaults
        modules = Seq(new ZkMaintenanceModule(ModuleIds.dailyReport, true)),
        frequency = 60000L, blackList = Seq())
      storage.put(cfgPath, cfg)
      val producerProbe = TestProbe()
      val select = new SelectionSupport {
        val moduleId = "alert"
        val producer = producerProbe.ref
        val curator = storage
      }
      val systemIds = select.getSystems()
      systemIds.size must be(3)
      systemIds must contain("3200")
      systemIds must contain("3201")
      systemIds must contain("3202")
    }
    "filter global systems" in {
      storage.createEmptyPath(Paths.Systems.getSystemPath("3200"))
      storage.createEmptyPath(Paths.Systems.getSystemPath("3201"))
      storage.createEmptyPath(Paths.Systems.getSystemPath("3202"))

      val cfgPath = Paths.MaintenanceInterface.getConfigPath()
      val cfg = new ZkMaintenanceConfig(
        applInfo = new ZkApplicationInfo("EG100", "Flitspalen", "2.0.0.11"),
        mq = new ZkRabbitMQConnection(exchangeName = Some("amq.topic")), //use defaults
        modules = Seq(new ZkMaintenanceModule(ModuleIds.dailyReport, true)),
        frequency = 60000L, blackList = Seq(ZkBlackListEntry("3200", "")))
      storage.put(cfgPath, cfg)
      val producerProbe = TestProbe()
      val select = new SelectionSupport {
        val moduleId = "alert"
        val producer = producerProbe.ref
        val curator = storage
      }
      val systemIds = select.getSystems()
      systemIds.size must be(2)
      systemIds must contain("3201")
      systemIds must contain("3202")
    }
    "filter module systems" in {
      storage.createEmptyPath(Paths.Systems.getSystemPath("3200"))
      storage.createEmptyPath(Paths.Systems.getSystemPath("3201"))
      storage.createEmptyPath(Paths.Systems.getSystemPath("3202"))

      val cfgPath = Paths.MaintenanceInterface.getConfigPath()
      val cfg = new ZkMaintenanceConfig(
        applInfo = new ZkApplicationInfo("EG100", "Flitspalen", "2.0.0.11"),
        mq = new ZkRabbitMQConnection(exchangeName = Some("amq.topic")), //use defaults
        modules = Seq(new ZkMaintenanceModule(ModuleIds.dailyReport, true)),
        frequency = 60000L, blackList = Seq(
          ZkBlackListEntry("3200", "alert"),
          ZkBlackListEntry("3201", "module")))
      storage.put(cfgPath, cfg)
      val producerProbe = TestProbe()
      val select = new SelectionSupport {
        val moduleId = "alert"
        val producer = producerProbe.ref
        val curator = storage
      }
      val systemIds = select.getSystems()
      systemIds.size must be(2)
      systemIds must contain("3201")
      systemIds must contain("3202")
    }
  }
}