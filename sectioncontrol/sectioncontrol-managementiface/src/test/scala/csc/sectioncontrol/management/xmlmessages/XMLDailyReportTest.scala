/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.xmlmessages

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import org.apache.commons.io.FileUtils
import java.io.File
import csc.sectioncontrol.reports.ZkDayStatistics
import java.util.Date
import csc.sectioncontrol.management.DailyReportMessage
import csc.sectioncontrol.storage.ZkApplicationInfo
import org.slf4j.LoggerFactory

class XMLDailyReportTest extends WordSpec with MustMatchers {
  private val log = LoggerFactory.getLogger(this.getClass.getName)

  "XMLDailyReport" must {
    "generate correct XML" in {
      val url = this.getClass.getClassLoader.getResource("dailyreport.gz")
      val bytes = FileUtils.readFileToByteArray(new File(url.getPath))
      val now = System.currentTimeMillis()
      val stat = ZkDayStatistics(id = "", day = now, createDate = now, data = bytes, fileName = "test.xml")
      val msg = DailyReportMessage(systemId = "A2", time = new Date(), report = stat)

      val builder = new XMLMessageBuilder(ZkApplicationInfo(id = "EG033", systemType = "SectionControl", version = "1.0.1.11"))

      val result = builder.createMessage(msg)
      log.info(new String(result))
      CheckXML.checkMessage(result)
    }
  }

}