/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.xmlmessages

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import akka.util.duration._

class XMLDataTypesTest extends WordSpec with MustMatchers {
  "XMLDataTypes.createDuration" must {
    "generate correct XML value for 1 msec" in {
      XMLDataTypes.createDuration(1.millis) must be("PT0.001S")
    }
    "generate correct XML value for 1 sec" in {
      XMLDataTypes.createDuration(1.second) must be("PT1.000S")
    }
    "generate correct XML value for 60 sec" in {
      XMLDataTypes.createDuration(1.minute) must be("PT1M")
    }
    "generate correct XML value for 61 sec" in {
      XMLDataTypes.createDuration(61.second) must be("PT1M1.000S")
    }
    "generate correct XML value for 1 hour" in {
      XMLDataTypes.createDuration(1.hour) must be("PT1H")
    }
    "generate correct XML value for 61 minutes" in {
      XMLDataTypes.createDuration(61.minutes) must be("PT1H1M")
    }
    "generate correct XML value for 3 days" in {
      XMLDataTypes.createDuration(3.days) must be("P3D")
    }
    "generate correct XML value for all parts" in {
      val time = 3.days.toMillis + 12.hours.toMillis + 50.minutes.toMillis + 40123
      XMLDataTypes.createDuration(time.millis) must be("P3DT12H50M40.123S")
    }
  }

}