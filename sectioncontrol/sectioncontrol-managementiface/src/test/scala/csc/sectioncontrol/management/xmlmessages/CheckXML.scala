/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.xmlmessages

import javax.xml.validation.{ Schema, SchemaFactory }
import javax.xml.XMLConstants
import javax.xml.transform.stream.StreamSource
import java.io.ByteArrayInputStream
import xml.parsing.NoBindingFactoryAdapter
import org.xml.sax.InputSource
import javax.xml.parsers.{ SAXParserFactory, SAXParser }
import xml.{ Node, Elem, TopScope }

object CheckXML {

  def checkMessage(xml: Array[Byte]): Node = {
    val factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
    val xsdStream = getClass.getResourceAsStream("/beheer-v1.0.xsd")
    val schema = factory.newSchema(new StreamSource(xsdStream))
    val source = new ByteArrayInputStream(xml)
    new SchemaAwareFactoryAdapter(schema).load(source)
  }

  class SchemaAwareFactoryAdapter(schema: Schema) extends NoBindingFactoryAdapter {
    override def loadXML(source: InputSource, parser: SAXParser) = {
      val reader = parser.getXMLReader()
      val handler = schema.newValidatorHandler()
      handler.setContentHandler(this)
      reader.setContentHandler(handler)

      scopeStack.push(TopScope)
      reader.parse(source)
      scopeStack.pop
      rootElem.asInstanceOf[Elem]
    }

    override def parser: SAXParser = {
      val factory = SAXParserFactory.newInstance()
      factory.setNamespaceAware(true)
      factory.setFeature("http://xml.org/sax/features/namespace-prefixes", true)
      factory.newSAXParser()
    }
  }
}

