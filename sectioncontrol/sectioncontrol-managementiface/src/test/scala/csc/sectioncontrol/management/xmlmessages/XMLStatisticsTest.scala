/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.xmlmessages

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import java.util.Date
import csc.sectioncontrol.storage._
import csc.sectioncontrol.messages.StatsDuration
import csc.sectioncontrol.common.DayUtility
import csc.sectioncontrol.management.StatisticsMessage
import csc.sectioncontrol.messages.Lane
//import csc.sectioncontrol.storage.ZkApplicationInfo
//import csc.sectioncontrol.storage.SpeedFixedStatistics
//import csc.sectioncontrol.storage.LaneInfo
//import csc.sectioncontrol.storage.StatisticsPeriod
import scala.Some
import csc.sectioncontrol.messages.matcher.MatcherCorridorConfig
import org.slf4j.LoggerFactory

class XMLStatisticsTest extends WordSpec with MustMatchers {
  private val log = LoggerFactory.getLogger(this.getClass.getName)

  val lane1 = new Lane(laneId = "3210-1-RD",
    name = "RD",
    gantry = "1",
    system = "3210",
    sensorGPS_longitude = 52.4,
    sensorGPS_latitude = 5.2)
  val lane2 = new Lane(laneId = "3210-1-RA",
    name = "RA",
    gantry = "1",
    system = "3210",
    sensorGPS_longitude = 52.4,
    sensorGPS_latitude = 5.2)
  "XMLStatistics" must {
    "generate correct XML for speed" in {
      val systemId = "3215"
      val now = System.currentTimeMillis()
      val msg = new StatisticsMessage(systemId = systemId, time = new Date(), corridorId = "speed", speed = Some(
        new SpeedFixedStatistics(period = StatisticsPeriod(StatsDuration(now, DayUtility.fileExportTimeZone), now - 300000, now),
          nrRegistrations = 20,
          nrSpeedMeasurement = 18,
          averageSpeed = 50.5,
          highestSpeed = 60,
          laneStats = List(
            new LaneInfo(lane1, 15),
            new LaneInfo(lane2, 5)))))

      val builder = new XMLMessageBuilder(ZkApplicationInfo(id = "EG033", systemType = "SectionControl", version = "1.0.1.11"))

      val result = builder.createMessage(msg)
      log.info(new String(result))
      CheckXML.checkMessage(result)
    }
    "generate correct XML for redlight" in {
      val systemId = "3215"
      val now = System.currentTimeMillis()
      val msg = new StatisticsMessage(systemId = systemId, time = new Date(), corridorId = "red", redLight = Some(
        new RedLightStatistics(period = StatisticsPeriod(StatsDuration(now, DayUtility.fileExportTimeZone), now - 300000, now),
          nrRegistrations = 20,
          nrWithRedLight = 18,
          averageRedLight = 50.5,
          highestRedLight = 60,
          laneStats = List(
            new LaneInfo(lane1, 15),
            new LaneInfo(lane2, 5)))))

      val builder = new XMLMessageBuilder(ZkApplicationInfo(id = "EG033", systemType = "SectionControl", version = "1.0.1.11"))

      val result = builder.createMessage(msg)
      log.info(new String(result))
      CheckXML.checkMessage(result)
    }
    "generate correct XML for section" in {
      val systemId = "3215"
      val now = System.currentTimeMillis()
      val msg = new StatisticsMessage(systemId = systemId, time = new Date(), corridorId = "sec", section = Some(
        new SectionControlStatistics(period = StatisticsPeriod(StatsDuration(now, DayUtility.fileExportTimeZone), now - 300000, now),
          config = MatcherCorridorConfig(systemId = systemId, zkCorridorId = "1", id = 1, entry = "", exit = "", distance = 0, deltaForDoubleShoot = 100, followDistance = 1800000),
          input = 20,
          matched = 18,
          unmatched = 1,
          doubles = 1,
          averageSpeed = 50.5,
          highestSpeed = 60,
          laneStats = List(
            new LaneInfo(lane1, 15),
            new LaneInfo(lane2, 5)))))

      val builder = new XMLMessageBuilder(ZkApplicationInfo(id = "EG033", systemType = "SectionControl", version = "1.0.1.11"))

      val result = builder.createMessage(msg)
      log.info(new String(result))
      CheckXML.checkMessage(result)
    }
  }
  "generate correct empty XML" in {
    val systemId = "3215"
    val msg = new StatisticsMessage(systemId = systemId, time = new Date(), corridorId = "test")

    val builder = new XMLMessageBuilder(ZkApplicationInfo(id = "EG033", systemType = "SectionControl", version = "1.0.1.11"))

    val result = builder.createMessage(msg)
    log.info(new String(result))
    CheckXML.checkMessage(result)
  }

}