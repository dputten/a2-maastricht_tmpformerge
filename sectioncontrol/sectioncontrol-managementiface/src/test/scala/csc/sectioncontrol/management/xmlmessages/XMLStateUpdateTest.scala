/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.xmlmessages

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import java.util.Date
import csc.sectioncontrol.management.StateUpdateMessage
import csc.sectioncontrol.storage.ZkApplicationInfo
import org.slf4j.LoggerFactory

class XMLStateUpdateTest extends WordSpec with MustMatchers {
  private val log = LoggerFactory.getLogger(this.getClass.getName)

  "XMLStateUpdate" must {
    "generate correct XML" in {
      val systemId = "3215"
      val corridorId = "red"

      val msg = new StateUpdateMessage(systemId = systemId, corridorId = corridorId, time = new Date(), oldState = "StandBy", newState = "EnforceOn", reason = "Gebruiker [ctcs] met verbalisantcode [AA0000] heeft voor systeem [3254] een status verandering van [Off] naar [Standby] aangevraagd")

      val builder = new XMLMessageBuilder(ZkApplicationInfo(id = "EG033", systemType = "SectionControl", version = "1.0.1.11"))

      val result = builder.createMessage(msg)
      log.info(new String(result))
      CheckXML.checkMessage(result)
    }
  }

}