/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.modules

import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import akka.actor.ActorSystem
import net.liftweb.json.Formats
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.storage._
import csc.sectioncontrol.messages.{ EsaProviderType, VehicleImageType, VehicleCode }
import csc.akkautils.DirectLogging
import akka.testkit.{ TestActorRef, TestProbe, TestKit }
import csc.sectioncontrol.management.{ StateUpdateMessage, ModuleIds, Wakeup }
import akka.util.duration._
import csc.sectioncontrol.common.DayUtility
import csc.config.Path
import java.util.Date
import csc.curator.CuratorTestServer
import csc.curator.utils.{ CuratorToolsImpl, Curator }
import csc.sectioncontrol.storagelayer.state.StateService

class SelectStateTest
  extends TestKit(ActorSystem("SelectStateTest")) with WordSpec with MustMatchers
  with CuratorTestServer with DirectLogging with BeforeAndAfterAll {

  override protected def afterAll() {
    super.afterAll()
    system.shutdown()
  }

  var storage: Curator = _
  val moduleId = ModuleIds.statusUpdate

  override def beforeEach() {
    super.beforeEach()
    storage = new CuratorToolsImpl(clientScope, log) {
      override protected def extendFormats(defaultFormats: Formats) = defaultFormats + new EnumerationSerializer(
        ZkWheelbaseType,
        VehicleCode,
        ZkIndicationType,
        ServiceType,
        ZkCaseFileType,
        EsaProviderType,
        VehicleImageType,
        ConfigType)
    }
  }

  "SelectState" must {
    "Send no messages when empty" in {
      val systemId = "EG100"
      storage.createEmptyPath(Paths.Systems.getSystemPath(systemId))
      val producer = TestProbe()
      val select = TestActorRef(new SelectState(storage, producer.ref))
      select ! Wakeup
      producer.expectNoMsg(2 seconds)
    }
    "Send 1 message when old state exists" in {
      //create Daily report
      val systemId = "EG100"
      val corridorId = "S1"

      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      var time = DayUtility.substractOneDay(startDay) //start yesterday
      time = DayUtility.substractOneDay(time) //start day before

      val state = new ZkState(state = "EnforceOn", timestamp = time, userId = "e7f09abf-b2f2-414f-b35e-ac82ac8a5b22", reason = "Calibratie geslaagd, Stand-by -> Handhaven aan")
      StateService.create(systemId, corridorId, state, storage)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectState(storage, producer.ref))

      select ! Wakeup
      producer.expectMsg(new StateUpdateMessage(systemId = systemId, corridorId = corridorId, time = new Date(time), oldState = "Unknown", newState = "EnforceOn", reason = state.reason))
      //producer.expectMsg(new StateUpdateMessage(systemId = systemId, corridorId = corridorId, time = new Date(time), oldState = "StandBy", newState = "EnforceOn", reason = state.reason))

      val path = Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId)
      val completion = storage.get[ZkModuleCompletionStatus](path + "/" + corridorId)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(time)
    }
    "Send 2 message when 2 old state exists" in {
      //create Daily report
      val systemId = "EG100"
      val corridorId = "S1"
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val state = new ZkState(state = "EnforceOn", timestamp = time, userId = "e7f09abf-b2f2-414f-b35e-ac82ac8a5b22", reason = "Calibratie geslaagd, Stand-by -> Handhaven aan")
      StateService.create(systemId, corridorId, state, storage)

      val state2 = new ZkState(state = "StandBy", timestamp = timeYesterday, userId = "e7f09abf-b2f2-414f-b35e-ac82ac8a5b22", reason = "Gebruiker [ctcs] met verbalisantcode [AA0000] heeft voor systeem [3254] een status verandering van [Off] naar [Standby] aangevraagd")
      val versioned = StateService.getVersioned(systemId, corridorId, storage)
      StateService.update(systemId, corridorId, state2, versioned.get.version, storage)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectState(storage, producer.ref))
      select ! Wakeup
      producer.expectMsg(new StateUpdateMessage(systemId = systemId, corridorId = corridorId, time = new Date(time), oldState = "Unknown", newState = "EnforceOn", reason = state.reason))
      //producer.expectMsg(new StateUpdateMessage(systemId = systemId, corridorId = corridorId, time = new Date(time), oldState = "StandBy", newState = "EnforceOn", reason = state.reason))
      producer.expectMsg(new StateUpdateMessage(systemId = systemId, corridorId = corridorId, time = new Date(timeYesterday), oldState = "EnforceOn", newState = "StandBy", reason = state2.reason))

      val path = Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId)
      val completion = storage.get[ZkModuleCompletionStatus](path + "/" + corridorId)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(timeYesterday)

    }
    "not send message after update" in {
      //create Daily report
      val systemId = "EG100"
      val corridorId = "S1"
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before
      val state = new ZkState(state = "EnforceOn", timestamp = time, userId = "e7f09abf-b2f2-414f-b35e-ac82ac8a5b22", reason = "Calibratie geslaagd, Stand-by -> Handhaven aan")
      StateService.create(systemId, corridorId, state, storage)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectState(storage, producer.ref))
      select ! Wakeup
      producer.expectMsg(new StateUpdateMessage(systemId = systemId, corridorId = corridorId, time = new Date(time), oldState = "Unknown", newState = "EnforceOn", reason = state.reason))
      //producer.expectMsg(new StateUpdateMessage(systemId = systemId, corridorId = corridorId, time = new Date(time), oldState = "StandBy", newState = "EnforceOn", reason = state.reason))

      select ! Wakeup
      producer.expectNoMsg(1 second)
    }
    "Send message after update" in {
      //create Daily report
      val systemId = "EG100"
      val corridorId = "S1"
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val state = new ZkState(state = "EnforceOn", timestamp = time, userId = "e7f09abf-b2f2-414f-b35e-ac82ac8a5b22", reason = "Calibratie geslaagd, Stand-by -> Handhaven aan")
      StateService.create(systemId, corridorId, state, storage)

      val state2 = new ZkState(state = "StandBy", timestamp = timeYesterday, userId = "e7f09abf-b2f2-414f-b35e-ac82ac8a5b22", reason = "Gebruiker [ctcs] met verbalisantcode [AA0000] heeft voor systeem [3254] een status verandering van [Off] naar [Standby] aangevraagd")

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectState(storage, producer.ref))
      select ! Wakeup
      producer.expectMsg(new StateUpdateMessage(systemId = systemId, corridorId = corridorId, time = new Date(time), oldState = "Unknown", newState = "EnforceOn", reason = state.reason))
      //producer.expectMsg(new StateUpdateMessage(systemId = systemId, corridorId = corridorId, time = new Date(time), oldState = "StandBy", newState = "EnforceOn", reason = state.reason))
      producer.expectNoMsg(1 second)

      val versioned = StateService.getVersioned(systemId, corridorId, storage)
      StateService.update(systemId, corridorId, state2, versioned.get.version, storage)
      select ! Wakeup
      producer.expectMsg(new StateUpdateMessage(systemId = systemId, corridorId = corridorId, time = new Date(timeYesterday), oldState = "EnforceOn", newState = "StandBy", reason = state2.reason))
    }
  }
}