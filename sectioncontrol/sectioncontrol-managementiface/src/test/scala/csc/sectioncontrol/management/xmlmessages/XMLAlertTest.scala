/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.xmlmessages

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import java.util.Date
import csc.sectioncontrol.management.{ AlertMessage, FailureMessage }
import csc.sectioncontrol.storage.ZkApplicationInfo
import org.slf4j.LoggerFactory

class XMLAlertTest extends WordSpec with MustMatchers {
  private val log = LoggerFactory.getLogger(this.getClass.getName)

  "XMLAlert" must {
    "generate correct XML for alert" in {
      val systemId = "3215"
      val corridorId = "S1"

      val msg = new AlertMessage(systemId = systemId, corridorId = corridorId, alertType = "NTP", time = new Date(),
        path = "3214-1-Camera", message = "FTP Error", configType = "Lane", startTime = None,
        reductionFactor = 0.5F)

      val builder = new XMLMessageBuilder(ZkApplicationInfo(id = "EG033", systemType = "SectionControl", version = "1.0.1.11"))

      val result = builder.createMessage(msg)
      log.info(new String(result))
      CheckXML.checkMessage(result)
    }
    "generate correct XML for solved alert" in {
      val systemId = "3215"
      val corridorId = "S1"

      val msg = new AlertMessage(systemId = systemId, corridorId = corridorId, alertType = "NTP", time = new Date(),
        path = "3214-1-Camera", message = "FTP Error", configType = "Lane", startTime = Some(new Date()),
        reductionFactor = 0.5F)

      val builder = new XMLMessageBuilder(ZkApplicationInfo(id = "EG033", systemType = "SectionControl", version = "1.0.1.11"))

      val result = builder.createMessage(msg)
      log.info(new String(result))
      CheckXML.checkMessage(result)
    }
  }

}