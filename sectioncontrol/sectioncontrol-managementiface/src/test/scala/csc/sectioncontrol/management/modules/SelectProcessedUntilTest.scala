/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.modules

import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import akka.actor.ActorSystem
import net.liftweb.json.Formats
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.storage._
import csc.sectioncontrol.messages.{ EsaProviderType, VehicleImageType, VehicleCode }
import csc.akkautils.DirectLogging
import akka.testkit.{ TestActorRef, TestProbe, TestKit }
import csc.sectioncontrol.management._
import akka.util.duration._
import csc.sectioncontrol.common.DayUtility
import java.util.Date
import csc.sectioncontrol.management.ProcessedUntilMessage
import csc.sectioncontrol.classify.nl.ClassificationCompletionStatus
import csc.config.Path
import java.text.SimpleDateFormat
import csc.curator.CuratorTestServer
import csc.curator.utils.CuratorToolsImpl

class SelectProcessedUntilTest
  extends TestKit(ActorSystem("SelectProcessedUntilTest")) with WordSpec with MustMatchers
  with CuratorTestServer with DirectLogging with BeforeAndAfterAll {

  override protected def afterAll() {
    super.afterAll()
    system.shutdown()
  }

  var storage: CuratorToolsImpl = _
  val moduleId = ModuleIds.processUntil

  override def beforeEach() {
    super.beforeEach()
    storage = new CuratorToolsImpl(clientScope, log) {
      override protected def extendFormats(defaultFormats: Formats) = defaultFormats + new EnumerationSerializer(
        ZkWheelbaseType,
        VehicleCode,
        ZkIndicationType,
        ServiceType,
        ZkCaseFileType,
        EsaProviderType,
        VehicleImageType,
        ConfigType)
    }
  }
  val systemId = "EG100"

  "SelectPreselector" must {
    "Send no messages when empty" in {
      storage.createEmptyPath(Paths.Systems.getSystemPath(systemId))
      val producer = TestProbe()
      val select = TestActorRef(new SelectProcessedUntil(storage, producer.ref))
      select ! Wakeup
      producer.expectNoMsg(2 seconds)
    }
    "Send 1 message when matcher status exists" in {
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      var time = DayUtility.substractOneDay(startDay) //start yesterday
      time = DayUtility.substractOneDay(time) //start day before
      val lane1 = CompletionLaneStatus(systemId = systemId, gantryId = "1", laneId = "lane1",
        lastUpdateTime = time, completeUntil = time - 1.hour.toMillis, lastVehicle = time - 1.hour.toMillis)
      val lane2 = CompletionLaneStatus(systemId = systemId, gantryId = "1", laneId = "lane1",
        lastUpdateTime = time - 5.minutes.toMillis, completeUntil = time - 30.minutes.toMillis, lastVehicle = time - 30.minutes.toMillis)
      val statMap = Map("lane1" -> lane1,
        "lane2" -> lane2)
      val stat = new SystemLaneCompletionStatus(statMap)
      val pathMatch = Paths.Systems.getPreSelectorCompletionStatusPath(systemId)
      storage.put(pathMatch, stat)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectProcessedUntil(storage, producer.ref))

      select ! Wakeup
      producer.expectMsg(new ProcessedUntilMessage(
        systemId = systemId, time = new Date(time),
        process = "PreSelector",
        completionTime = new Date(time - 1.hour.toMillis),
        detailType = ProcessedUntilMessage.detailLane,
        details = Seq(ProcessDetails(detail = "lane1", lastUpdated = new Date(lane1.lastUpdateTime), completionTime = new Date(lane1.completeUntil)),
          ProcessDetails(detail = "lane2", lastUpdated = new Date(lane2.lastUpdateTime), completionTime = new Date(lane2.completeUntil)))))

      val path = Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId) + "/PreSelector"
      val completion = storage.get[ZkModuleCompletionStatus](path)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(time)
    }
    "not send message after wakeup" in {
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before
      val lane1 = CompletionLaneStatus(systemId = systemId, gantryId = "1", laneId = "lane1",
        lastUpdateTime = time, completeUntil = time - 1.hour.toMillis, lastVehicle = time - 1.hour.toMillis)
      val lane2 = CompletionLaneStatus(systemId = systemId, gantryId = "1", laneId = "lane1",
        lastUpdateTime = time - 5.minutes.toMillis, completeUntil = time - 30.minutes.toMillis, lastVehicle = time - 30.minutes.toMillis)
      val statMap = Map("lane1" -> lane1,
        "lane2" -> lane2)
      val stat = new SystemLaneCompletionStatus(statMap)
      val pathMatch = Paths.Systems.getPreSelectorCompletionStatusPath(systemId)
      storage.put(pathMatch, stat)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectProcessedUntil(storage, producer.ref))
      select ! Wakeup
      producer.expectMsg(new ProcessedUntilMessage(
        systemId = systemId, time = new Date(time),
        process = "PreSelector",
        completionTime = new Date(time - 1.hour.toMillis),
        detailType = ProcessedUntilMessage.detailLane,
        details = Seq(ProcessDetails(detail = "lane1", lastUpdated = new Date(lane1.lastUpdateTime), completionTime = new Date(lane1.completeUntil)),
          ProcessDetails(detail = "lane2", lastUpdated = new Date(lane2.lastUpdateTime), completionTime = new Date(lane2.completeUntil)))))

      select ! Wakeup
      producer.expectNoMsg(1 second)
    }
    "Send message after update" in {
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val lane1 = CompletionLaneStatus(systemId = systemId, gantryId = "1", laneId = "lane1",
        lastUpdateTime = time, completeUntil = time - 1.hour.toMillis, lastVehicle = time - 1.hour.toMillis)
      val lane2 = CompletionLaneStatus(systemId = systemId, gantryId = "1", laneId = "lane1",
        lastUpdateTime = time - 5.minutes.toMillis, completeUntil = time - 30.minutes.toMillis, lastVehicle = time - 30.minutes.toMillis)
      val statMap = Map("lane1" -> lane1,
        "lane2" -> lane2)
      val stat = new SystemLaneCompletionStatus(statMap)

      val pathMatch = Paths.Systems.getPreSelectorCompletionStatusPath(systemId)
      storage.put(pathMatch, stat)

      val cor21 = CompletionCorridorStatus(lastUpdateTime = time + 1.hour.toMillis, completeUntil = time)
      val cor22 = CompletionCorridorStatus(lastUpdateTime = time - 5.minutes.toMillis, completeUntil = time - 30.minutes.toMillis)
      val lane21 = CompletionLaneStatus(systemId = systemId, gantryId = "1", laneId = "lane1",
        lastUpdateTime = time + 1.hour.toMillis, completeUntil = time, lastVehicle = time)
      val lane22 = CompletionLaneStatus(systemId = systemId, gantryId = "1", laneId = "lane1",
        lastUpdateTime = time - 5.minutes.toMillis, completeUntil = time - 30.minutes.toMillis, lastVehicle = time - 30.minutes.toMillis)
      val statMap2 = Map("lane1" -> lane21,
        "lane2" -> lane22)
      val stat2 = new SystemLaneCompletionStatus(statMap2)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectProcessedUntil(storage, producer.ref))
      select ! Wakeup
      producer.expectMsg(new ProcessedUntilMessage(
        systemId = systemId, time = new Date(time),
        process = "PreSelector",
        completionTime = new Date(time - 1.hour.toMillis),
        detailType = ProcessedUntilMessage.detailLane,
        details = Seq(ProcessDetails(detail = "lane1", lastUpdated = new Date(lane1.lastUpdateTime), completionTime = new Date(lane1.completeUntil)),
          ProcessDetails(detail = "lane2", lastUpdated = new Date(lane2.lastUpdateTime), completionTime = new Date(lane2.completeUntil)))))
      producer.expectNoMsg(1 second)

      storage.set(pathMatch, stat2, 0)
      select ! Wakeup
      producer.expectMsg(new ProcessedUntilMessage(
        systemId = systemId, time = new Date(cor21.lastUpdateTime),
        process = "PreSelector",
        completionTime = new Date(cor22.completeUntil),
        detailType = ProcessedUntilMessage.detailLane,
        details = Seq(ProcessDetails(detail = "lane1", lastUpdated = new Date(lane21.lastUpdateTime), completionTime = new Date(lane21.completeUntil)),
          ProcessDetails(detail = "lane2", lastUpdated = new Date(lane22.lastUpdateTime), completionTime = new Date(lane22.completeUntil)))))
    }
  }
  "SelectMatches" must {
    "Send no messages when empty" in {
      storage.createEmptyPath(Paths.Systems.getSystemPath(systemId))
      val producer = TestProbe()
      val select = TestActorRef(new SelectProcessedUntil(storage, producer.ref))
      select ! Wakeup
      producer.expectNoMsg(2 seconds)
    }
    "Send 1 message when matcher status exists" in {
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      var time = DayUtility.substractOneDay(startDay) //start yesterday
      time = DayUtility.substractOneDay(time) //start day before
      val cor1 = CompletionCorridorStatus(lastUpdateTime = time, completeUntil = time - 1.hour.toMillis)
      val cor2 = CompletionCorridorStatus(lastUpdateTime = time - 5.minutes.toMillis, completeUntil = time - 30.minutes.toMillis)
      val statMap = Map("cor1" -> cor1,
        "cor2" -> cor2)
      val stat = new SystemCorridorCompletionStatus(statMap)
      val pathMatch = Paths.Systems.getMatchCompletionStatusPath(systemId)
      storage.put(pathMatch, stat)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectProcessedUntil(storage, producer.ref))

      select ! Wakeup
      producer.expectMsg(new ProcessedUntilMessage(
        systemId = systemId, time = new Date(time),
        process = "Matcher",
        completionTime = new Date(time - 1.hour.toMillis),
        detailType = ProcessedUntilMessage.detailCorridor,
        details = Seq(ProcessDetails(detail = "cor1", lastUpdated = new Date(cor1.lastUpdateTime), completionTime = new Date(cor1.completeUntil)),
          ProcessDetails(detail = "cor2", lastUpdated = new Date(cor2.lastUpdateTime), completionTime = new Date(cor2.completeUntil)))))

      val path = Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId) + "/Matcher"
      val completion = storage.get[ZkModuleCompletionStatus](path)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(time)
    }
    "not send message after wakeup" in {
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before
      val cor1 = CompletionCorridorStatus(lastUpdateTime = time, completeUntil = time - 1.hour.toMillis)
      val cor2 = CompletionCorridorStatus(lastUpdateTime = time - 5.minutes.toMillis, completeUntil = time - 30.minutes.toMillis)
      val statMap = Map("cor1" -> cor1,
        "cor2" -> cor2)
      val stat = new SystemCorridorCompletionStatus(statMap)
      val pathMatch = Paths.Systems.getMatchCompletionStatusPath(systemId)
      storage.put(pathMatch, stat)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectProcessedUntil(storage, producer.ref))
      select ! Wakeup
      producer.expectMsg(new ProcessedUntilMessage(
        systemId = systemId, time = new Date(time),
        process = "Matcher",
        completionTime = new Date(time - 1.hour.toMillis),
        detailType = ProcessedUntilMessage.detailCorridor,
        details = Seq(ProcessDetails(detail = "cor1", lastUpdated = new Date(cor1.lastUpdateTime), completionTime = new Date(cor1.completeUntil)),
          ProcessDetails(detail = "cor2", lastUpdated = new Date(cor2.lastUpdateTime), completionTime = new Date(cor2.completeUntil)))))

      select ! Wakeup
      producer.expectNoMsg(1 second)
    }
    "Send message after update" in {
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val cor1 = CompletionCorridorStatus(lastUpdateTime = time, completeUntil = time - 1.hour.toMillis)
      val cor2 = CompletionCorridorStatus(lastUpdateTime = time - 5.minutes.toMillis, completeUntil = time - 30.minutes.toMillis)
      val statMap = Map("cor1" -> cor1,
        "cor2" -> cor2)
      val stat = new SystemCorridorCompletionStatus(statMap)
      val pathMatch = Paths.Systems.getMatchCompletionStatusPath(systemId)
      storage.put(pathMatch, stat)

      val cor21 = CompletionCorridorStatus(lastUpdateTime = time + 1.hour.toMillis, completeUntil = time)
      val cor22 = CompletionCorridorStatus(lastUpdateTime = time - 5.minutes.toMillis, completeUntil = time - 30.minutes.toMillis)
      val statMap2 = Map("cor1" -> cor21,
        "cor2" -> cor22)
      val stat2 = new SystemCorridorCompletionStatus(statMap2)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectProcessedUntil(storage, producer.ref))
      select ! Wakeup
      producer.expectMsg(new ProcessedUntilMessage(
        systemId = systemId, time = new Date(time),
        process = "Matcher",
        completionTime = new Date(time - 1.hour.toMillis),
        detailType = ProcessedUntilMessage.detailCorridor,
        details = Seq(ProcessDetails(detail = "cor1", lastUpdated = new Date(cor1.lastUpdateTime), completionTime = new Date(cor1.completeUntil)),
          ProcessDetails(detail = "cor2", lastUpdated = new Date(cor2.lastUpdateTime), completionTime = new Date(cor2.completeUntil)))))
      producer.expectNoMsg(1 second)

      storage.set(pathMatch, stat2, 0)
      select ! Wakeup
      producer.expectMsg(new ProcessedUntilMessage(
        systemId = systemId, time = new Date(cor21.lastUpdateTime),
        process = "Matcher",
        completionTime = new Date(cor22.completeUntil),
        detailType = ProcessedUntilMessage.detailCorridor,
        details = Seq(ProcessDetails(detail = "cor1", lastUpdated = new Date(cor21.lastUpdateTime), completionTime = new Date(cor21.completeUntil)),
          ProcessDetails(detail = "cor2", lastUpdated = new Date(cor22.lastUpdateTime), completionTime = new Date(cor22.completeUntil)))))
    }
  }
  "SelectClassify" must {
    "Send no messages when empty" in {
      storage.createEmptyPath(Paths.Systems.getSystemPath(systemId))
      val producer = TestProbe()
      val select = TestActorRef(new SelectProcessedUntil(storage, producer.ref))
      select ! Wakeup
      producer.expectNoMsg(2 seconds)
    }
    "Send 1 message when classify status exists" in {
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      var time = DayUtility.substractOneDay(startDay) //start yesterday
      time = DayUtility.substractOneDay(time) //start day before

      val stat = new ClassificationCompletionStatus(lastUpdateTime = time, completeUntil = time - 1.hour.toMillis)
      val pathClass = Paths.Systems.getClassificationCompletionStatusPath(systemId)
      storage.put(pathClass, stat)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectProcessedUntil(storage, producer.ref))

      select ! Wakeup
      producer.expectMsg(new ProcessedUntilMessage(
        systemId = systemId, time = new Date(stat.lastUpdateTime),
        process = "Classify",
        completionTime = new Date(stat.completeUntil),
        detailType = ProcessedUntilMessage.detailSystem,
        details = Seq()))

      val path = Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId) + "/Classify"
      val completion = storage.get[ZkModuleCompletionStatus](path)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(time)
    }
    "not send message after wakeup" in {
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val stat = new ClassificationCompletionStatus(lastUpdateTime = time, completeUntil = time - 1.hour.toMillis)
      val pathClass = Paths.Systems.getClassificationCompletionStatusPath(systemId)
      storage.put(pathClass, stat)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectProcessedUntil(storage, producer.ref))
      select ! Wakeup
      producer.expectMsg(new ProcessedUntilMessage(
        systemId = systemId, time = new Date(stat.lastUpdateTime),
        process = "Classify",
        completionTime = new Date(stat.completeUntil),
        detailType = ProcessedUntilMessage.detailSystem,
        details = Seq()))

      select ! Wakeup
      producer.expectNoMsg(1 second)
    }
    "Send message after update" in {
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val stat = new ClassificationCompletionStatus(lastUpdateTime = time, completeUntil = time - 1.hour.toMillis)
      val pathClass = Paths.Systems.getClassificationCompletionStatusPath(systemId)
      storage.put(pathClass, stat)

      val stat2 = new ClassificationCompletionStatus(lastUpdateTime = time + 1.hours.toMillis, completeUntil = time)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectProcessedUntil(storage, producer.ref))
      select ! Wakeup
      producer.expectMsg(new ProcessedUntilMessage(
        systemId = systemId, time = new Date(stat.lastUpdateTime),
        process = "Classify",
        completionTime = new Date(stat.completeUntil),
        detailType = ProcessedUntilMessage.detailSystem,
        details = Seq()))
      producer.expectNoMsg(1 second)

      storage.set(pathClass, stat2, 0)
      select ! Wakeup
      producer.expectMsg(new ProcessedUntilMessage(
        systemId = systemId, time = new Date(stat2.lastUpdateTime),
        process = "Classify",
        completionTime = new Date(stat2.completeUntil),
        detailType = ProcessedUntilMessage.detailSystem,
        details = Seq()))
    }
  }
  "SelectEnforce" must {
    val df = new SimpleDateFormat("yyyy-MM-dd")
    "Send no messages when empty status exists" in {
      storage.createEmptyPath(Paths.Systems.getSystemPath(systemId))

      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      var time = DayUtility.substractOneDay(startDay) //start yesterday
      time = DayUtility.substractOneDay(time) //start day before

      val stat = new ZkEnforcementStatus(mtmDataProcessed = false,
        mtmDataProcessingTime = time,
        qualificationComplete = false,
        registrationComplete = false)
      val enfPath = Path(Paths.Systems.getSystemJobsPath(systemId)) / "enforceStatus-nl"
      storage.put((enfPath / df.format(new Date(time))).toString(), stat)

      val producer = TestProbe()
      val select = TestActorRef(new SelectProcessedUntil(storage, producer.ref))
      select ! Wakeup
      producer.expectNoMsg(2 seconds)
    }
    "Send 1 message when mtm true exists" in {
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val stat = new ZkEnforcementStatus(mtmDataProcessed = true,
        mtmDataProcessingTime = time,
        qualificationComplete = false,
        registrationComplete = false)
      val enfPath = Path(Paths.Systems.getSystemJobsPath(systemId)) / "enforceStatus-nl"
      storage.put((enfPath / df.format(new Date(time))).toString(), stat)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectProcessedUntil(storage, producer.ref))

      select ! Wakeup
      producer.expectMsg(new ProcessedUntilMessage(
        systemId = systemId, time = new Date(timeYesterday),
        process = "MTM",
        completionTime = new Date(timeYesterday),
        detailType = ProcessedUntilMessage.detailSystem,
        details = Seq()))

      val path = Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId) + "/MTM"
      val completion = storage.get[ZkModuleCompletionStatus](path)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(timeYesterday)
    }
    "not send message after wakeup" in {
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before
      val stat = new ZkEnforcementStatus(mtmDataProcessed = true,
        mtmDataProcessingTime = time,
        qualificationComplete = false,
        registrationComplete = false)
      val enfPath = Path(Paths.Systems.getSystemJobsPath(systemId)) / "enforceStatus-nl"
      storage.put((enfPath / df.format(new Date(time))).toString(), stat)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectProcessedUntil(storage, producer.ref))
      select ! Wakeup
      producer.expectMsg(new ProcessedUntilMessage(
        systemId = systemId, time = new Date(timeYesterday),
        process = "MTM",
        completionTime = new Date(timeYesterday),
        detailType = ProcessedUntilMessage.detailSystem,
        details = Seq()))

      select ! Wakeup
      producer.expectNoMsg(1 second)
    }
    "not send message after update with false" in {
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before
      val stat = new ZkEnforcementStatus(mtmDataProcessed = true,
        mtmDataProcessingTime = time,
        qualificationComplete = false,
        registrationComplete = false)
      val enfPath = Path(Paths.Systems.getSystemJobsPath(systemId)) / "enforceStatus-nl"
      storage.put((enfPath / df.format(new Date(time))).toString(), stat)
      val stat2 = new ZkEnforcementStatus(mtmDataProcessed = false,
        mtmDataProcessingTime = timeYesterday,
        qualificationComplete = false,
        registrationComplete = false)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectProcessedUntil(storage, producer.ref))
      select ! Wakeup
      producer.expectMsg(new ProcessedUntilMessage(
        systemId = systemId, time = new Date(timeYesterday),
        process = "MTM",
        completionTime = new Date(timeYesterday),
        detailType = ProcessedUntilMessage.detailSystem,
        details = Seq()))

      storage.put((enfPath / df.format(new Date(timeYesterday))).toString(), stat2)

      select ! Wakeup
      producer.expectNoMsg(1 second)
    }
    "Send message after update" in {
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val stat = new ZkEnforcementStatus(mtmDataProcessed = true,
        mtmDataProcessingTime = time,
        qualificationComplete = false,
        registrationComplete = false)
      val enfPath = Path(Paths.Systems.getSystemJobsPath(systemId)) / "enforceStatus-nl"
      storage.put((enfPath / df.format(new Date(time))).toString(), stat)
      val stat2 = new ZkEnforcementStatus(mtmDataProcessed = true,
        mtmDataProcessingTime = timeYesterday,
        qualificationComplete = false,
        registrationComplete = false)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectProcessedUntil(storage, producer.ref))
      select ! Wakeup
      producer.expectMsg(new ProcessedUntilMessage(
        systemId = systemId, time = new Date(timeYesterday),
        process = "MTM",
        completionTime = new Date(timeYesterday),
        detailType = ProcessedUntilMessage.detailSystem,
        details = Seq()))
      producer.expectNoMsg(1 second)

      storage.put((enfPath / df.format(new Date(timeYesterday))).toString(), stat2)
      select ! Wakeup
      producer.expectMsg(new ProcessedUntilMessage(
        systemId = systemId, time = new Date(startDay),
        process = "MTM",
        completionTime = new Date(startDay),
        detailType = ProcessedUntilMessage.detailSystem,
        details = Seq()))
      val path = Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId) + "/MTM"
      val completion = storage.get[ZkModuleCompletionStatus](path)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(startDay)

    }
    "Send message after update qualify" in {
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val stat = new ZkEnforcementStatus(mtmDataProcessed = false,
        mtmDataProcessingTime = time,
        qualificationComplete = true,
        registrationComplete = false)
      val enfPath = Path(Paths.Systems.getSystemJobsPath(systemId)) / "enforceStatus-nl"
      storage.put((enfPath / df.format(new Date(time))).toString(), stat)
      val stat2 = new ZkEnforcementStatus(mtmDataProcessed = false,
        mtmDataProcessingTime = timeYesterday,
        qualificationComplete = true,
        registrationComplete = false)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectProcessedUntil(storage, producer.ref))
      select ! Wakeup
      producer.expectMsg(new ProcessedUntilMessage(
        systemId = systemId, time = new Date(timeYesterday),
        process = "Qualify",
        completionTime = new Date(timeYesterday),
        detailType = ProcessedUntilMessage.detailSystem,
        details = Seq()))
      producer.expectNoMsg(1 second)

      storage.put((enfPath / df.format(new Date(timeYesterday))).toString(), stat2)
      select ! Wakeup
      producer.expectMsg(new ProcessedUntilMessage(
        systemId = systemId, time = new Date(startDay),
        process = "Qualify",
        completionTime = new Date(startDay),
        detailType = ProcessedUntilMessage.detailSystem,
        details = Seq()))
      val path = Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId) + "/Qualify"
      val completion = storage.get[ZkModuleCompletionStatus](path)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(startDay)

    }
    "Send message after update registration" in {
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before

      val stat = new ZkEnforcementStatus(mtmDataProcessed = false,
        mtmDataProcessingTime = time,
        qualificationComplete = false,
        registrationComplete = true)
      val enfPath = Path(Paths.Systems.getSystemJobsPath(systemId)) / "enforceStatus-nl"
      storage.put((enfPath / df.format(new Date(time))).toString(), stat)
      val stat2 = new ZkEnforcementStatus(mtmDataProcessed = false,
        mtmDataProcessingTime = timeYesterday,
        qualificationComplete = false,
        registrationComplete = true)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectProcessedUntil(storage, producer.ref))
      select ! Wakeup
      producer.expectMsg(new ProcessedUntilMessage(
        systemId = systemId, time = new Date(timeYesterday),
        process = "Registration",
        completionTime = new Date(timeYesterday),
        detailType = ProcessedUntilMessage.detailSystem,
        details = Seq()))
      producer.expectNoMsg(1 second)

      storage.put((enfPath / df.format(new Date(timeYesterday))).toString(), stat2)
      select ! Wakeup
      producer.expectMsg(new ProcessedUntilMessage(
        systemId = systemId, time = new Date(startDay),
        process = "Registration",
        completionTime = new Date(startDay),
        detailType = ProcessedUntilMessage.detailSystem,
        details = Seq()))
      val path = Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId) + "/Registration"
      val completion = storage.get[ZkModuleCompletionStatus](path)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(startDay)

    }
  }

}