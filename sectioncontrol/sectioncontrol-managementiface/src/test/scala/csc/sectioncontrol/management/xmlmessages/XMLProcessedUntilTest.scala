/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.xmlmessages

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import java.util.Date
import csc.sectioncontrol.management.{ ProcessDetails, ProcessedUntilMessage }
import csc.sectioncontrol.storage.ZkApplicationInfo
import org.slf4j.LoggerFactory

class XMLProcessedUntilTest extends WordSpec with MustMatchers {
  private val log = LoggerFactory.getLogger(this.getClass.getName)

  "XMLProcessedUntil" must {
    "generate correct XML for system" in {
      val systemId = "3215"

      val msg = new ProcessedUntilMessage(systemId = systemId, time = new Date(),
        process = "Classify", completionTime = new Date(), detailType = ProcessedUntilMessage.detailSystem, details = Seq(
          ProcessDetails("lane123", new Date(), new Date()),
          ProcessDetails("lane124", new Date(), new Date())))

      val builder = new XMLMessageBuilder(ZkApplicationInfo(id = "EG033", systemType = "SectionControl", version = "1.0.1.11"))

      val result = builder.createMessage(msg)
      log.info(new String(result))
      CheckXML.checkMessage(result)
    }
    "generate correct XML for system with details" in {
      val systemId = "3215"

      val msg = new ProcessedUntilMessage(systemId = systemId, time = new Date(),
        process = "Classify", completionTime = new Date(), detailType = ProcessedUntilMessage.detailSystem, details = Seq())

      val builder = new XMLMessageBuilder(ZkApplicationInfo(id = "EG033", systemType = "SectionControl", version = "1.0.1.11"))

      val result = builder.createMessage(msg)
      log.info(new String(result))
      CheckXML.checkMessage(result)
    }
    "generate correct XML for lane" in {
      val systemId = "3215"

      val msg = new ProcessedUntilMessage(systemId = systemId, time = new Date(),
        process = "Matcher", completionTime = new Date(), detailType = ProcessedUntilMessage.detailLane, details = Seq(
          ProcessDetails("lane123", new Date(), new Date()),
          ProcessDetails("lane124", new Date(), new Date())))

      val builder = new XMLMessageBuilder(ZkApplicationInfo(id = "EG033", systemType = "SectionControl", version = "1.0.1.11"))

      val result = builder.createMessage(msg)
      log.info(new String(result))
      CheckXML.checkMessage(result)
    }
    "generate correct XML for lane without details" in {
      val systemId = "3215"

      val msg = new ProcessedUntilMessage(systemId = systemId, time = new Date(),
        process = "Matcher", completionTime = new Date(), detailType = ProcessedUntilMessage.detailLane, details = Seq())

      val builder = new XMLMessageBuilder(ZkApplicationInfo(id = "EG033", systemType = "SectionControl", version = "1.0.1.11"))

      val result = builder.createMessage(msg)
      log.info(new String(result))
      CheckXML.checkMessage(result)
    }
    "generate correct XML for corridor" in {
      val systemId = "3215"

      val msg = new ProcessedUntilMessage(systemId = systemId, time = new Date(),
        process = "Matcher", completionTime = new Date(), detailType = ProcessedUntilMessage.detailCorridor, details = Seq(
          ProcessDetails("Cor123", new Date(), new Date()),
          ProcessDetails("Cor124", new Date(), new Date())))

      val builder = new XMLMessageBuilder(ZkApplicationInfo(id = "EG033", systemType = "SectionControl", version = "1.0.1.11"))

      val result = builder.createMessage(msg)
      log.info(new String(result))
      CheckXML.checkMessage(result)
    }
  }

}