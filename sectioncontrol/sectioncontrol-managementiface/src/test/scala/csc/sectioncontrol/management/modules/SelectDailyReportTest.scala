/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.modules

import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import akka.actor.ActorSystem
import csc.curator.utils.{ CuratorToolsImpl, Curator }
import net.liftweb.json.Formats
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.storage._
import csc.sectioncontrol.messages.{ EsaProviderType, StatsDuration, VehicleImageType, VehicleCode }
import csc.akkautils.DirectLogging
import akka.testkit.{ TestActorRef, TestProbe, TestKit }
import csc.sectioncontrol.management.{ ModuleIds, DailyReportMessage, Wakeup }
import akka.util.duration._
import csc.sectioncontrol.common.DayUtility
import csc.config.Path
import csc.sectioncontrol.reports.ZkDayStatistics
import java.util.Date
import csc.curator.CuratorTestServer

class SelectDailyReportTest
  extends TestKit(ActorSystem("SelectDailyReportTest")) with WordSpec with MustMatchers
  with CuratorTestServer with DirectLogging with BeforeAndAfterAll {

  override protected def afterAll() {
    super.afterAll()
    system.shutdown()
  }

  var storage: Curator = _
  val moduleId = ModuleIds.dailyReport

  override def beforeEach() {
    super.beforeEach()
    storage = new CuratorToolsImpl(clientScope, log) {
      override protected def extendFormats(defaultFormats: Formats) = defaultFormats + new EnumerationSerializer(
        ZkWheelbaseType,
        VehicleCode,
        ZkIndicationType,
        ServiceType,
        ZkCaseFileType,
        EsaProviderType,
        VehicleImageType,
        ConfigType)
    }
  }

  "SelectDailyReport" must {
    "Send no messages when empty" in {
      val systemId = "EG100"
      storage.createEmptyPath(Paths.Systems.getSystemPath(systemId))
      val producer = TestProbe()
      val select = TestActorRef(new SelectDailyReport(storage, producer.ref))
      select ! Wakeup
      producer.expectNoMsg(2 seconds)
    }
    "Send 1 message when old rapportage exists" in {
      //create Daily report
      val systemId = "EG100"
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      var time = DayUtility.substractOneDay(startDay) //start yesterday
      time = DayUtility.substractOneDay(time) //start day before
      val key = StatsDuration(time, DayUtility.fileExportTimeZone)
      val reportData = "report".getBytes().map(_.toInt).toList
      val report = new ZkDayStatistics(id = "test", day = time, createDate = time, data = reportData, fileName = "dagraportage.xml")
      val reportPath = Path(Paths.Systems.getReportPath(systemId, key))
      storage.put(reportPath, report)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectDailyReport(storage, producer.ref))

      select ! Wakeup
      producer.expectMsg(new DailyReportMessage(systemId = systemId, time = new Date(time), report = report))

      val path = Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId)
      val completion = storage.get[ZkModuleCompletionStatus](path)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(time)
    }
    "Send 2 message when 2 old rapportage exists" in {
      //create Daily report
      val systemId = "EG100"
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before
      val key = StatsDuration(time, DayUtility.fileExportTimeZone)
      val reportData = "report".getBytes().map(_.toInt).toList
      val report = new ZkDayStatistics(id = "test", day = time, createDate = time, data = reportData, fileName = "dagraportage.xml")
      val reportPath = Path(Paths.Systems.getReportPath(systemId, key))
      storage.put(reportPath, report)
      val key2 = StatsDuration(timeYesterday, DayUtility.fileExportTimeZone)
      val reportData2 = "report2".getBytes().map(_.toInt).toList
      val report2 = new ZkDayStatistics(id = "test", day = timeYesterday, createDate = timeYesterday, data = reportData2, fileName = "dagraportage2.xml")
      val reportPath2 = Path(Paths.Systems.getReportPath(systemId, key2))
      storage.put(reportPath2, report2)

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectDailyReport(storage, producer.ref))
      select ! Wakeup
      producer.expectMsg(new DailyReportMessage(systemId = systemId, time = new Date(time), report = report))
      producer.expectMsg(new DailyReportMessage(systemId = systemId, time = new Date(timeYesterday), report = report2))

      val path = Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId)
      val completion = storage.get[ZkModuleCompletionStatus](path)
      completion.get.id must be(moduleId)
      completion.get.lastUpdate must be(timeYesterday)

    }
    "not send message after update" in {
      //create Daily report
      val systemId = "EG100"
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before
      val key = StatsDuration(time, DayUtility.fileExportTimeZone)
      val reportData = "report".getBytes().map(_.toInt).toList
      val report = new ZkDayStatistics(id = "test", day = time, createDate = time, data = reportData, fileName = "dagraportage.xml")
      val reportPath = Path(Paths.Systems.getReportPath(systemId, key))
      storage.put(reportPath, report)

      //start test
      val producer = TestProbe()
      //val select = system.actorOf(Props(new SelectDailyReport(storage, producer.ref)))
      val select = TestActorRef(new SelectDailyReport(storage, producer.ref))
      select ! Wakeup
      producer.expectMsg(30 minute, new DailyReportMessage(systemId = systemId, time = new Date(time), report = report))

      select ! Wakeup
      producer.expectNoMsg(1 second)
    }
    "Send message after update" in {
      //create Daily report
      val systemId = "EG100"
      val startDay = DayUtility.getStartDay(System.currentTimeMillis()) //start today
      val timeYesterday = DayUtility.substractOneDay(startDay) //start yesterday
      val time = DayUtility.substractOneDay(timeYesterday) //start day before
      val key = StatsDuration(time, DayUtility.fileExportTimeZone)
      val reportData = "report".getBytes().map(_.toInt).toList
      val report = new ZkDayStatistics(id = "test", day = time, createDate = time, data = reportData, fileName = "dagraportage.xml")
      val reportPath = Path(Paths.Systems.getReportPath(systemId, key))
      storage.put(reportPath, report)
      val key2 = StatsDuration(timeYesterday, DayUtility.fileExportTimeZone)
      val reportData2 = "report2".getBytes().map(_.toInt).toList
      val report2 = new ZkDayStatistics(id = "test", day = timeYesterday, createDate = timeYesterday, data = reportData2, fileName = "dagraportage2.xml")
      val reportPath2 = Path(Paths.Systems.getReportPath(systemId, key2))

      //start test
      val producer = TestProbe()
      val select = TestActorRef(new SelectDailyReport(storage, producer.ref))
      select ! Wakeup
      producer.expectMsg(new DailyReportMessage(systemId = systemId, time = new Date(time), report = report))
      producer.expectNoMsg(1 second)

      storage.put(reportPath2, report2)
      select ! Wakeup
      producer.expectMsg(new DailyReportMessage(systemId = systemId, time = new Date(timeYesterday), report = report2))
    }
  }
}