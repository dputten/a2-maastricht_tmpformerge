/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.xmlmessages

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import java.util.Date
import csc.sectioncontrol.management.{ CalibrateMessage, StateUpdateMessage }
import csc.sectioncontrol.storage.ZkApplicationInfo
import org.slf4j.LoggerFactory

class XMLCalibrationTest extends WordSpec with MustMatchers {
  private val log = LoggerFactory.getLogger(this.getClass.getName)

  "XMLCalibration" must {
    "generate correct XML" in {
      val systemId = "3215"
      val corridorId = "S1"
      val msg = new CalibrateMessage(systemId = systemId, corridorId = corridorId, time = new Date(), success = true)

      val builder = new XMLMessageBuilder(ZkApplicationInfo(id = "EG033", systemType = "SectionControl", version = "1.0.1.11"))

      val result = builder.createMessage(msg)
      log.info(new String(result))
      CheckXML.checkMessage(result)
    }
  }

}