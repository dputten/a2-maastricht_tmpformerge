/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.xmlmessages

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import java.util.Date
import csc.sectioncontrol.management.{ FailureMessage, StateUpdateMessage }
import csc.sectioncontrol.storage.ZkApplicationInfo
import org.slf4j.LoggerFactory

class XMLFailureTest extends WordSpec with MustMatchers {
  private val log = LoggerFactory.getLogger(this.getClass.getName)

  "XMLFailure" must {
    "generate correct XML for failure" in {
      val systemId = "3215"
      val corridorId = "S1"

      val msg = new FailureMessage(systemId = systemId, corridorId = corridorId, failureType = "NTP", time = new Date(),
        path = "3214-1-Camera", message = "FTP Error", configType = "Lane", startTime = None)

      val builder = new XMLMessageBuilder(ZkApplicationInfo(id = "EG033", systemType = "SectionControl", version = "1.0.1.11"))

      val result = builder.createMessage(msg)
      log.info(new String(result))
      CheckXML.checkMessage(result)
    }
    "generate correct XML for solved failure" in {
      val systemId = "3215"
      val corridorId = "S1"

      val msg = new FailureMessage(systemId = systemId, corridorId = corridorId, failureType = "NTP", time = new Date(),
        path = "3214-1-Camera", message = "FTP Error", configType = "Lane", startTime = Some(new Date()))

      val builder = new XMLMessageBuilder(ZkApplicationInfo(id = "EG033", systemType = "SectionControl", version = "1.0.1.11"))

      val result = builder.createMessage(msg)
      log.info(new String(result))
      CheckXML.checkMessage(result)
    }
  }

}