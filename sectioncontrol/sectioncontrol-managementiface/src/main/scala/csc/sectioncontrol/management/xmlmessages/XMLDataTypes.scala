/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.xmlmessages

import java.text.SimpleDateFormat
import java.util.{ TimeZone, Date }
import akka.util.Duration
import akka.util.duration._

object XMLDataTypes {
  val dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
  dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))

  def createDateTime(date: Date): String = {
    dateFormat.format(date)
  }
  def createDateTime(date: Long): String = {
    createDateTime(new Date(date))
  }

  def createFloat(f: Float): String = {
    "%.3f".format(f)
  }
  def createDouble(f: Double): String = {
    "%.3f".format(f)
  }

  def createDuration(time: Duration): String = {
    var timeLeft = time
    var duration = "P"
    if (timeLeft.toDays > 0) {
      duration += timeLeft.toDays + "D"
      timeLeft -= timeLeft.toDays.days
    }
    var timeStr = "T"
    if (timeLeft.toHours > 0) {
      timeStr += timeLeft.toHours + "H"
      timeLeft -= timeLeft.toHours.hours
    }
    if (timeLeft.toMinutes > 0) {
      timeStr += timeLeft.toMinutes + "M"
      timeLeft -= timeLeft.toMinutes.minutes
    }
    if (timeLeft.toMillis > 0) {
      val seconds = timeLeft.toMillis.toFloat / 1000F
      timeStr += createFloat(seconds) + "S"
    }
    if (timeStr == "T") {
      duration
    } else {
      duration + timeStr
    }
  }

  def createStatusType(state: String): String = {
    state.toUpperCase match {
      case "OFF"             ⇒ "OFF"
      case "MAINTENANCE"     ⇒ "MAINTENANCE"
      case "STANDBY"         ⇒ "STANDBY"
      case "FAILURE"         ⇒ "ALARM"
      case "ENFORCEON"       ⇒ "ENFORCE"
      case "ENFORCEOFF"      ⇒ "ENFORCE"
      case "ENFORCEDEGRADED" ⇒ "ENFORCE"
      case _                 ⇒ "UNKNOWN"
    }
  }
}