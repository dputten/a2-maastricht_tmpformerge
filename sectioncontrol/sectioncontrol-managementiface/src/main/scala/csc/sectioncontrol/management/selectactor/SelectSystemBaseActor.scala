/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.selectactor

import akka.actor.{ ActorLogging, Actor }
import csc.sectioncontrol.management.{ MaintenanceMessage, Wakeup }

abstract class SelectSystemBaseActor() extends Actor with SelectionSupport with ActorLogging {

  override def preRestart(reason: Throwable, message: Option[Any]) {
    super.preRestart(reason, message)
    log.error("Restart while processing message %s".format(message), reason)
  }

  def receive = {
    case Wakeup ⇒ {
      val systems = getSystems()
      systems.foreach(systemId ⇒ {
        //get Last time
        val lastTime = getLastUpdate(systemId)
        //do check
        val results = doCheck(systemId, lastTime)
        //send results
        sendMessage(results)
      })
    }
  }

  def doCheck(systemId: String, startTime: Long): Seq[MaintenanceMessage]

}

