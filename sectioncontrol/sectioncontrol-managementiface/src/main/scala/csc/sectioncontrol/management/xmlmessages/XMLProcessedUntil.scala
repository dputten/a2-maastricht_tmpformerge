/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.xmlmessages

import xml.Elem
import csc.sectioncontrol.management.ProcessedUntilMessage

object XMLProcessedUntil {
  def createXMLElement(msg: ProcessedUntilMessage): Elem = {

    <procesVoortgang>
      <proces>{ msg.process }</proces>
      <tijd>{ XMLDataTypes.createDateTime(msg.completionTime) }</tijd>
      <aangepast>{ XMLDataTypes.createDateTime(msg.time) }</aangepast>
      {
        if (!msg.details.isEmpty && (msg.detailType == ProcessedUntilMessage.detailLane || msg.detailType == ProcessedUntilMessage.detailCorridor)) {
          <details>
            {
              if (msg.detailType == ProcessedUntilMessage.detailLane) {
                msg.details.map(lane ⇒ {
                  <lane>
                    <laneId>{ lane.detail }</laneId>
                    <tijd>{ XMLDataTypes.createDateTime(lane.completionTime) }</tijd>
                    <aangepast>{ XMLDataTypes.createDateTime(lane.lastUpdated) }</aangepast>
                  </lane>
                })
              } else {
                msg.details.map(cor ⇒ {
                  <corridor>
                    <corridorId>{ cor.detail }</corridorId>
                    <tijd>{ XMLDataTypes.createDateTime(cor.completionTime) }</tijd>
                    <aangepast>{ XMLDataTypes.createDateTime(cor.lastUpdated) }</aangepast>
                  </corridor>
                })
              }
            }
          </details>
        }
      }
    </procesVoortgang>
  }

}