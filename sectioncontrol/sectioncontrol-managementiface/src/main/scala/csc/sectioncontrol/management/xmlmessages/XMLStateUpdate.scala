/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.xmlmessages

import xml.Elem
import csc.sectioncontrol.management.StateUpdateMessage

object XMLStateUpdate {
  def createXMLElement(msg: StateUpdateMessage): Elem = {

    <statusovergang>
      <corridorId>{ msg.corridorId }</corridorId>
      <tijd>{ XMLDataTypes.createDateTime(msg.time) }</tijd>
      <statusOud>{ XMLDataTypes.createStatusType(msg.oldState) }</statusOud>
      <statusNieuw>{ XMLDataTypes.createStatusType(msg.newState) }</statusNieuw>
      <trigger>{ msg.reason }</trigger>
    </statusovergang>
  }

}