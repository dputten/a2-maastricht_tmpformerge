/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.modules

import java.util.Date

import akka.actor.ActorRef

import org.apache.hadoop.conf.Configuration

import csc.curator.utils.Curator
import csc.hbase.utils.HBaseTableKeeper

import csc.sectioncontrol.management._
import selectactor.{ SelectCorridorBaseActor, SelectionTable, SelectSystemBaseActor }
import csc.sectioncontrol.storagelayer.hbasetables.SelfTestResultTable

class SelectCalibration(val curator: Curator, val producer: ActorRef, hbaseCfg: Configuration) extends SelectCorridorBaseActor
  with SelectionTable[SelfTestResultTable.Reader] {
  val moduleId = ModuleIds.calibration

  override def postStop() {
    closeTables()
    super.postStop()
  }

  def doCheck(systemId: String, corridorId: String, startTime: Long): Seq[MaintenanceMessage] = {
    val now = System.currentTimeMillis()
    val calibrations = getReader(systemId).readRows((corridorId, startTime + 1), (corridorId, now))

    calibrations.map(cal ⇒ {
      CalibrateMessage(systemId, corridorId, new Date(cal.time), cal.success)
    })
  }

  def makeReader(tablekeeper: HBaseTableKeeper, systemId: String): SelfTestResultTable.Reader = {
    SelfTestResultTable.makeReader(hbaseCfg, tablekeeper, systemId)
  }

}
