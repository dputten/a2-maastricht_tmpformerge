/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.selectactor

import csc.hbase.utils.HBaseTableKeeper

trait SelectionTable[ReaderType <: AnyRef] {
  var readerCache = Map[String, ReaderType]()
  var tables = new HBaseTableKeeper {}

  def closeTables() {
    tables.closeTables()
    readerCache.empty
  }

  def getReader(systemId: String): ReaderType = {
    readerCache.get(systemId).getOrElse {
      val reader = makeReader(tables, systemId)
      readerCache += systemId -> reader
      reader
    }
  }
  def makeReader(tableKeeper: HBaseTableKeeper, systemId: String): ReaderType

}