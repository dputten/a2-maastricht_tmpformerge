/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.xmlmessages

import xml.Elem
import csc.sectioncontrol.management.StatisticsMessage
import csc.sectioncontrol.storage.{ LaneInfo, StatisticsPeriod }

object XMLStatistics {
  def createXMLElement(msg: StatisticsMessage): Elem = {
    getPeriod(msg).map(period ⇒ {
      <statistics>
        <corridorId>{ msg.corridorId }</corridorId>
        <period>
          <start>{ XMLDataTypes.createDateTime(period.begin) }</start>
          <end>{ XMLDataTypes.createDateTime(period.end) }</end>
        </period>
        {
          getDetails(msg) match {
            case Some(p)⇒ p
            case None⇒
          }
        }
        <laneInfo>
          {
            val lanes = getLanes(msg)
            lanes.map(lane ⇒ {
              <lane>
                <laneId>{ lane.lane.laneId }</laneId>
                <count>{ lane.count }</count>
              </lane>
            })
          }
        </laneInfo>
      </statistics>
    }).getOrElse(<statistics>
                   <corridorId>{ msg.corridorId }</corridorId>
                   <period>
                     <start>{ XMLDataTypes.createDateTime(msg.time) }</start>
                     <end>{ XMLDataTypes.createDateTime(msg.time) }</end>
                   </period>
                   <laneInfo></laneInfo>
                 </statistics>)
  }

  def getPeriod(msg: StatisticsMessage): Option[StatisticsPeriod] = {
    msg match {
      case StatisticsMessage(_, _, _, None, None, Some(stat)) ⇒ Some(stat.period)
      case StatisticsMessage(_, _, _, None, Some(stat), None) ⇒ Some(stat.period)
      case StatisticsMessage(_, _, _, Some(stat), None, None) ⇒ Some(stat.period)
      case _ ⇒ None
    }
  }
  def getLanes(msg: StatisticsMessage): Seq[LaneInfo] = {
    msg match {
      case StatisticsMessage(_, _, _, None, None, Some(stat)) ⇒ stat.laneStats
      case StatisticsMessage(_, _, _, None, Some(stat), None) ⇒ stat.laneStats
      case StatisticsMessage(_, _, _, Some(stat), None, None) ⇒ stat.laneStats
      case _ ⇒ Seq()
    }
  }
  def getDetails(msg: StatisticsMessage): Option[Elem] = {
    msg match {
      case StatisticsMessage(_, _, _, None, None, Some(speed)) ⇒ {
        Some(
          <speed>
            <nrRegistrations>{ speed.nrRegistrations }</nrRegistrations>
            <nrWithMeasurement>{ speed.nrSpeedMeasurement }</nrWithMeasurement>
            <average>{ XMLDataTypes.createDouble(speed.averageSpeed) }</average>
            <max>{ speed.highestSpeed }</max>
          </speed>)
      }
      case StatisticsMessage(systemId, _, _, None, Some(red), None) ⇒ {
        Some(
          <redLight>
            <nrRegistrations>{ red.nrRegistrations }</nrRegistrations>
            <nrWithMeasurement>{ red.nrWithRedLight }</nrWithMeasurement>
            <average>{ XMLDataTypes.createDouble(red.averageRedLight) }</average>
            <max>{ red.highestRedLight }</max>
          </redLight>)
      }
      case StatisticsMessage(systemId, _, _, Some(sec), None, None) ⇒ {
        Some(
          <section>
            <nrRegistrations>{ sec.input }</nrRegistrations>
            <matched>{ sec.matched }</matched>
            <unmatched>{ sec.unmatched }</unmatched>
            <doubles>{ sec.doubles }</doubles>
            <average>{ XMLDataTypes.createDouble(sec.averageSpeed) }</average>
            <max>{ sec.highestSpeed }</max>
          </section>)
      }
      case _ ⇒ None
    }
  }
}