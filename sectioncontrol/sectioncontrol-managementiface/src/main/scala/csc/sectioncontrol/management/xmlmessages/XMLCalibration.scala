/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.xmlmessages

import xml.Elem
import csc.sectioncontrol.management.{ CalibrateMessage, StateUpdateMessage }

object XMLCalibration {
  def createXMLElement(msg: CalibrateMessage): Elem = {

    <calibratieTest>
      <corridorId>{ msg.corridorId }</corridorId>
      <tijd>{ XMLDataTypes.createDateTime(msg.time) }</tijd>
      <geslaagd>{ msg.success }</geslaagd>
    </calibratieTest>
  }

}