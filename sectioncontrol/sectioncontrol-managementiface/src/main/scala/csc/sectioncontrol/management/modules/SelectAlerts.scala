/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.modules

import java.util.Date

import akka.actor.ActorRef

import csc.curator.utils.Curator

import csc.sectioncontrol.management._
import csc.sectioncontrol.storage.{ ZkAlert, ZkAlertHistory, Paths }
import selectactor.{ SelectCorridorBaseActor, SelectSystemBaseActor }
import csc.sectioncontrol.storagelayer.alerts.AlertService

class SelectAlerts(val curator: Curator, val producer: ActorRef) extends SelectCorridorBaseActor {
  val moduleId = ModuleIds.alert

  def doCheck(systemId: String, corridorId: String, startTime: Long): Seq[MaintenanceMessage] = {
    val alerts = AlertService.getAlertLogHistory(curator, systemId, corridorId, startTime, System.currentTimeMillis())

    alerts.map(hist ⇒ {
      if (hist.endTime == AlertService.endOfTime) {
        //current Alert
        //his start na gewone startTime
        AlertMessage(systemId = systemId,
          corridorId = corridorId,
          alertType = hist.alertType,
          time = new Date(hist.startTime),
          path = hist.path,
          message = hist.message,
          configType = hist.configType.toString,
          reductionFactor = hist.reductionFactor,
          startTime = None)
      } else {
        //history alert
        AlertMessage(systemId = systemId,
          corridorId = corridorId,
          alertType = hist.alertType,
          time = new Date(hist.endTime),
          path = hist.path,
          message = hist.message,
          configType = hist.configType,
          reductionFactor = hist.reductionFactor,
          startTime = Some(new Date(hist.startTime)))
      }
    })
  }
}
