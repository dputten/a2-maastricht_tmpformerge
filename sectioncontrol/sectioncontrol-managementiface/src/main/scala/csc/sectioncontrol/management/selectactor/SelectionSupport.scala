/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.selectactor

import akka.actor.ActorRef

import org.joda.time.DateTime

import csc.config.Path
import csc.curator.utils.Curator

import csc.sectioncontrol.storage.{ ZkMaintenanceConfig, ZkModuleCompletionStatus, Paths }
import csc.sectioncontrol.management.MaintenanceMessage
import csc.sectioncontrol.common.DayUtility

trait SelectionSupport {
  val curator: Curator
  val producer: ActorRef
  val moduleId: String

  def getSystems(): Seq[String] = {
    val systems = curator.getChildNames(Paths.Systems.getDefaultPath)
    val path = Paths.MaintenanceInterface.getConfigPath()
    curator.get[ZkMaintenanceConfig](path) match {
      case Some(cfg) ⇒ {
        val black = cfg.blackList
        systems.filterNot(sys ⇒
          black.exists(bl ⇒
            bl.systemId == sys && (bl.moduleId.isEmpty || bl.moduleId == moduleId)))
      }
      case None ⇒ systems
    }
  }

  def getCorridors(systemId: String): Seq[String] = {
    curator.getChildNames(Paths.Corridors.getDefaultPath(systemId))
  }

  case class LaneInfo(systemId: String, gantryId: String, laneId: String, uniqueLaneId: String)
  def getLanesIds(systemId: String): Seq[LaneInfo] = {
    val gantries = curator.getChildNames(Paths.Gantries.getDefaultPath(systemId))
    gantries.flatMap(gantryId ⇒ {
      val lanes = curator.getChildNames(Paths.Gantries.getLanesPath(systemId, gantryId))
      lanes.map(laneId ⇒ LaneInfo(systemId, gantryId, laneId, "%s-%s-%s".format(systemId, gantryId, laneId)))
    })
  }

  def sendMessage(msgs: Seq[MaintenanceMessage]) {
    val grouped = msgs.groupBy(_.systemId)
    grouped.foreach {
      case (systemId, msgList) ⇒ {
        val sorted = msgList.sortBy(_.time.getTime)
        //send message
        sorted.foreach(producer ! _)
        //update completion
        sorted.lastOption.foreach(msg ⇒ {
          val path = Path(Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId))
          val completion = ZkModuleCompletionStatus(moduleId, msg.time.getTime)
          updateModuleCompletion(path, completion)
        })
      }
    }
  }

  def sendMessage(msgs: Seq[MaintenanceMessage], detailKey: String) {
    val grouped = msgs.groupBy(_.systemId)
    grouped.foreach {
      case (systemId, msgList) ⇒ {
        val sorted = msgList.sortBy(_.time.getTime)
        //send message
        sorted.foreach(msg ⇒ producer ! msg)
        //update completion
        sorted.lastOption.foreach(msg ⇒ {
          val path = Path(Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId)) / detailKey
          val completion = ZkModuleCompletionStatus(moduleId, msg.time.getTime)
          updateModuleCompletion(path, completion)
        })
      }
    }
  }
  def updateModuleCompletion(path: Path, completion: ZkModuleCompletionStatus) {
    val versionOpt = curator.getVersioned[ZkModuleCompletionStatus](path)
    val version = versionOpt.map(_.version).getOrElse(0)
    if (version == 0 && !curator.exists(path))
      curator.createEmptyPath(path)
    curator.set(path, completion, version)
  }

  def getLastUpdate(systemId: String): Long = {
    val path = Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId)
    val completion = curator.get[ZkModuleCompletionStatus](path)
    completion.map(_.lastUpdate).getOrElse(new DateTime(System.currentTimeMillis()).minusMonths(1).getMillis)
  }

  def getLastUpdate(systemId: String, detailKey: String): Long = {
    val path = Path(Paths.MaintenanceInterface.getModuleCompletionStatusPath(systemId, moduleId)) / detailKey
    val completion = curator.get[ZkModuleCompletionStatus](path)
    completion.map(_.lastUpdate).getOrElse(new DateTime(System.currentTimeMillis()).minusMonths(1).getMillis)
  }

  def getDayStartsAfter(startTime: Long): Seq[Long] = {
    val now = System.currentTimeMillis()
    val startDay = DayUtility.getStartDay(startTime)

    var time = DayUtility.addOneDay(startDay)
    var results = Seq[Long]()
    while (time <= now) {
      results = results :+ time
      time = DayUtility.addOneDay(time)
    }
    results
  }

  def getChangesFromAppendingNodes[ZkClass <: AnyRef: Manifest](
    zkParentPath: String,
    startTime: Long,
    getTime: (ZkClass) ⇒ Long,
    createMessage: (ZkClass, Option[ZkClass]) ⇒ MaintenanceMessage): Seq[MaintenanceMessage] = {

    var allChildren = curator.getChildren(zkParentPath).flatMap(curator.get[ZkClass](_))
    //get all changes in order of newest first
    var children = allChildren.sortBy(getTime(_)).reverse
    //init pre condition for while loop
    var resultList = Seq[MaintenanceMessage]()
    if (!children.isEmpty) {
      var stateTooOld = false
      while (!children.isEmpty && !stateTooOld) {
        var last = children.head
        //update condition
        children = children.tail
        val next = children.headOption
        //process current state
        if (getTime(last) > startTime) {
          //found new state
          //export to interface
          resultList = resultList :+ createMessage(last, next)
        } else {
          //state is too old where done
          stateTooOld = true
        }
      }
    }
    //return new states
    resultList
  }

}