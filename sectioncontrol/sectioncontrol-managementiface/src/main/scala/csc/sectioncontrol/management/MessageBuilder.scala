/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management

trait MessageBuilder {
  def createMessage(msg: MaintenanceMessage): Array[Byte]
}

