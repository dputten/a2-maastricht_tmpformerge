/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.modules

import java.util.Date

import akka.actor.ActorRef

import csc.curator.utils.Curator
import csc.sectioncontrol.management._
import csc.sectioncontrol.storage.{ ZkState, Paths }
import selectactor.{ SelectCorridorBaseActor, SelectSystemBaseActor }
import csc.sectioncontrol.storagelayer.state.StateService

//case class ZkState(state: String, timestamp: Long, userId: String, reason: String)

case class ZkUser(id: String,
                  reportingOfficerId: String,
                  phone: String,
                  ipAddress: String,
                  name: String,
                  username: String,
                  email: String,
                  passwordHash: String,
                  roleId: String,
                  sectionIds: List[String])

class SelectState(val curator: Curator, val producer: ActorRef) extends SelectCorridorBaseActor {
  val moduleId = ModuleIds.statusUpdate

  def doCheck(systemId: String, corridorId: String, startTime: Long): Seq[MaintenanceMessage] = {
    //get all present states to be able to determine the previous state
    val states = StateService
      .getStateLogHistory(systemId, corridorId, 0L, System.currentTimeMillis(), curator, true)
    //.filter(p ⇒ !(p.state == ZkState.standBy && p.timestamp == 0))

    //sort newest states first
    var sorted = states.sortBy(_.timestamp).reverse

    var resultList = Seq[MaintenanceMessage]()
    if (!sorted.isEmpty) {
      var stateTooOld = false
      var state = sorted.head

      while (!sorted.isEmpty && !stateTooOld) {
        //update condition
        sorted = sorted.tail
        val next = sorted.headOption
        //process current state
        if (state.timestamp > startTime) {
          //found new state
          //export to interface
          resultList = resultList :+
            StateUpdateMessage(systemId = systemId,
              corridorId = corridorId,
              time = new Date(state.timestamp),
              oldState = next.map(_.state).getOrElse("Unknown"),
              newState = state.state,
              reason = state.reason)
        } else {
          //state is too old where done
          stateTooOld = true
        }
        if (next.isDefined)
          state = next.get
      }
    }
    //return new states
    resultList
  }
}
