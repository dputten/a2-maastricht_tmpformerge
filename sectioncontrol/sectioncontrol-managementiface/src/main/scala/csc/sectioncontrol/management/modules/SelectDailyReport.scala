/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.modules

import java.util.Date

import akka.actor.ActorRef

import csc.curator.utils.Curator
import csc.config.Path

import csc.sectioncontrol.management.selectactor.SelectSystemBaseActor
import csc.sectioncontrol.management.{ ModuleIds, DailyReportMessage, MaintenanceMessage }
import csc.sectioncontrol.messages.StatsDuration
import csc.sectioncontrol.common.DayUtility
import csc.sectioncontrol.storage.Paths
import csc.sectioncontrol.reports.ZkDayStatistics

class SelectDailyReport(val curator: Curator, val producer: ActorRef) extends SelectSystemBaseActor {
  val moduleId = ModuleIds.dailyReport
  def doCheck(systemId: String, startTime: Long): Seq[MaintenanceMessage] = {
    val startDays = getDayStartsAfter(startTime)

    startDays.foldLeft(Seq[MaintenanceMessage]()) {
      case (resultList, startDay) ⇒ {
        val key = StatsDuration(startDay, DayUtility.fileExportTimeZone)
        val reportPath = Path(Paths.Systems.getReportPath(systemId, key))
        if (curator.exists(reportPath)) {
          val report = curator.get[ZkDayStatistics](reportPath)
          resultList ++ report.map(r ⇒ DailyReportMessage(systemId = systemId, time = new Date(startDay), report = r))
        } else {
          resultList
        }
      }
    }
  }

}
