/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.modules

import java.text.SimpleDateFormat
import java.util.Date

import akka.actor.{ Actor, ActorLogging, ActorRef }

import csc.config.Path
import csc.curator.utils.{ Curator, CuratorToolsImpl }

import csc.sectioncontrol.management._
import csc.sectioncontrol.common.DayUtility
import csc.sectioncontrol.storage.{ SystemCorridorCompletionStatus, CompletionCorridorStatus, SystemLaneCompletionStatus, Paths }
import csc.sectioncontrol.management.ProcessDetails
import csc.sectioncontrol.classify.nl.ClassificationCompletionStatus
import selectactor.SelectionSupport

class SelectProcessedUntil(val curator: Curator, val producer: ActorRef) extends Actor with ActorLogging with SelectionSupport {
  val moduleId = ModuleIds.processUntil

  val df = new SimpleDateFormat("yyyy-MM-dd")
  df.setTimeZone(DayUtility.fileExportTimeZone)

  val preselectorStr = "PreSelector"
  val matcherStr = "Matcher"
  val classifyStr = "Classify"
  val mtmStr = "MTM"
  val qualifyStr = "Qualify"
  val registrationStr = "Registration"

  override def preRestart(reason: Throwable, message: Option[Any]) {
    super.preRestart(reason, message)
    log.error("Restart while processing message %s".format(message), reason)
  }

  override def receive = {
    case Wakeup ⇒ {
      val systems = getSystems()
      systems.foreach(systemId ⇒ {
        val msgs = getPreselectorData(systemId) ++ getMatcherData(systemId) ++ getClassifyData(systemId) ++ getEnforceProcesses(systemId)
        msgs.foreach(m ⇒ {
          val start = getLastUpdate(systemId, m.process)
          if (m.time.getTime > start) {
            sendMessage(Seq(m), m.process)
          }
        })
      })
    }
  }

  def getPreselectorData(systemId: String): Option[ProcessedUntilMessage] = {
    //get preselector data
    val currentTime = System.currentTimeMillis()
    val status = curator.get[SystemLaneCompletionStatus](Paths.Systems.getPreSelectorCompletionStatusPath(systemId))

    status.map(stat ⇒ {
      val details = stat.lanes.map {
        case (laneId, status) ⇒ {
          new ProcessDetails(laneId, new Date(status.lastUpdateTime), new Date(status.completeUntil))
        }
      }
      val currentTime = System.currentTimeMillis()
      val (lastUpdated: Long, processedUntil: Long) = stat.lanes.foldLeft(0L, currentTime) {
        case ((last, until), comp) ⇒ {
          (math.max(last, comp._2.lastUpdateTime), math.min(until, comp._2.completeUntil))
        }
      }
      new ProcessedUntilMessage(systemId, new Date(lastUpdated), preselectorStr, new Date(processedUntil), ProcessedUntilMessage.detailLane, details.toSeq)
    })
  }

  def getMatcherData(systemId: String): Option[ProcessedUntilMessage] = {
    //get matcher data
    val status = curator.get[SystemCorridorCompletionStatus](Paths.Systems.getMatchCompletionStatusPath(systemId))
    status.map(stat ⇒ {
      val details = stat.corridors.map {
        case (corridorId, status) ⇒ {
          new ProcessDetails(corridorId, new Date(status.lastUpdateTime), new Date(status.completeUntil))
        }
      }
      val currentTime = System.currentTimeMillis()
      val (lastUpdated: Long, processedUntil: Long) = stat.corridors.foldLeft(0L, currentTime) {
        case ((last, until), comp) ⇒ {
          (math.max(last, comp._2.lastUpdateTime), math.min(until, comp._2.completeUntil))
        }
      }
      new ProcessedUntilMessage(systemId, new Date(lastUpdated), matcherStr, new Date(processedUntil), ProcessedUntilMessage.detailCorridor, details.toSeq)
    })
  }

  def getClassifyData(systemId: String): Option[ProcessedUntilMessage] = {
    //get classification data
    val statusClassify = curator.get[ClassificationCompletionStatus](Paths.Systems.getClassificationCompletionStatusPath(systemId))
    statusClassify.map(stat ⇒ {
      new ProcessedUntilMessage(systemId, new Date(stat.lastUpdateTime), classifyStr, new Date(stat.completeUntil), ProcessedUntilMessage.detailSystem, Seq())
    })
  }

  def getEnforceProcesses(systemId: String): Seq[ProcessedUntilMessage] = {
    //get classification data
    val path = Path(Paths.Systems.getSystemJobsPath(systemId)) / "enforceStatus-nl"
    val dates = curator.getChildNames(path.toString).sortWith((str1, str2) ⇒ str1 > str2)
    var mtm: Option[String] = None
    var qualify: Option[String] = None
    var registrate: Option[String] = None
    var index = 0
    while ((index < dates.size) && (mtm.isEmpty || qualify.isEmpty || registrate.isEmpty)) {
      val dayStr = dates(index)
      val day = curator.get[ZkEnforcementStatus]((path / dayStr).toString)
      day.foreach(stat ⇒ {
        if (stat.mtmDataProcessed && mtm.isEmpty) {
          mtm = Some(dayStr)
        }
        if (stat.qualificationComplete && qualify.isEmpty) {
          qualify = Some(dayStr)
        }
        if (stat.registrationComplete && registrate.isEmpty) {
          registrate = Some(dayStr)
        }
      })
      index += 1
    }
    val mtmStat = mtm.map(day ⇒ {
      val time = getTimeFromDay(day)
      new ProcessedUntilMessage(systemId, time, mtmStr, time, ProcessedUntilMessage.detailSystem, Seq())

    })
    val qualifyStat = qualify.map(day ⇒ {
      val time = getTimeFromDay(day)
      new ProcessedUntilMessage(systemId, time, qualifyStr, time, ProcessedUntilMessage.detailSystem, Seq())

    })
    val registrateStat = registrate.map(day ⇒ {
      val time = getTimeFromDay(day)
      new ProcessedUntilMessage(systemId, time, registrationStr, time, ProcessedUntilMessage.detailSystem, Seq())
    })
    (mtmStat ++ qualifyStat ++ registrateStat).toSeq
  }

  def getTimeFromDay(day: String): Date = {
    val time = df.parse(day)
    val endDayTime = DayUtility.getEndDay(time.getTime, DayUtility.fileExportTimeZone)
    new Date(endDayTime)
  }
}

case class ZkEnforcementStatus(mtmDataProcessed: Boolean = false,
                               mtmDataProcessingTime: Long = 0,
                               qualificationComplete: Boolean = false,
                               registrationComplete: Boolean = false)
