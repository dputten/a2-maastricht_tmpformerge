/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.modules

import java.util.Date

import akka.actor.{ ActorLogging, ActorRef }

import org.apache.hadoop.conf.Configuration
import csc.curator.utils.Curator
import csc.hbase.utils.HBaseTableKeeper
import csc.sectioncontrol.storage._
import csc.sectioncontrol.management.selectactor.{ SelectSystemBaseActor, SelectionTable }
import csc.sectioncontrol.management._
import csc.sectioncontrol.storagelayer.hbasetables.ProcessedViolationTable

class SelectViolations(val curator: Curator, val producer: ActorRef, hbaseConfig: Configuration) extends SelectSystemBaseActor
  with ActorLogging with SelectionTable[ProcessedViolationTable.Reader] {

  val moduleId = ModuleIds.violation

  override def preRestart(reason: Throwable, message: Option[Any]) {
    super.preRestart(reason, message)
    log.error("Restart while processing message %s".format(message), reason)
  }
  override def postStop() {
    closeTables()
    super.postStop()
  }

  def doCheck(systemId: String, startTime: Long): Seq[MaintenanceMessage] = {
    val now = System.currentTimeMillis()
    val violations = getReader("").readRows(KeyWithTimeIdAndPostFix(startTime + 1, systemId, None), KeyWithTimeIdAndPostFix(now, systemId, None))
    violations.map(vio ⇒ {
      ViolationMessage(systemId, new Date(vio.violationRecord.classifiedRecord.time), vio)
    })
  }

  def makeReader(table: HBaseTableKeeper, systemId: String): ProcessedViolationTable.Reader = {
    ProcessedViolationTable.makeReader(hbaseConfig, table)
  }

}