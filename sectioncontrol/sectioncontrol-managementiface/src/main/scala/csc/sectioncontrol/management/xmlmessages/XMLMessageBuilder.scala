/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.xmlmessages

import csc.sectioncontrol.management._
import xml.{ PrettyPrinter, Elem }
import csc.sectioncontrol.storage.ZkApplicationInfo
import csc.sectioncontrol.management.StateUpdateMessage
import csc.sectioncontrol.management.DailyReportMessage
import csc.sectioncontrol.storage.ZkApplicationInfo

class XMLMessageBuilder(application: ⇒ ZkApplicationInfo)
  extends MessageBuilder {

  def createMessage(msg: MaintenanceMessage): Array[Byte] = {
    val xml = <Beheerbericht xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="beheer-v1.0.xsd">
                <bron>
                  <AppId>{ application.id }</AppId>
                  <SysteemId>{ msg.systemId }</SysteemId>
                  <type>{ application.systemType }</type>
                  <versie>{ application.version }</versie>
                </bron>
                { transformMessage(msg) }
              </Beheerbericht>

    prettyFormat(xml).getBytes("ISO-8859-1")
  }

  private def prettyFormat(xml: Elem): String = {
    val printer = new PrettyPrinter(width = 920, step = 4)
    val firstLines =
      """<?xml version="1.0" encoding="ISO-8859-1"?>
        |""".stripMargin
    val buffer = new StringBuilder(firstLines)
    printer.format(xml, buffer)
    buffer.toString
  }

  def transformMessage(msg: MaintenanceMessage): Elem = {
    msg match {
      case report: DailyReportMessage      ⇒ XMLDailyReport.createXMLElement(report)
      case stateUpdate: StateUpdateMessage ⇒ XMLStateUpdate.createXMLElement(stateUpdate)
      case calibrate: CalibrateMessage     ⇒ XMLCalibration.createXMLElement(calibrate)
      case failure: FailureMessage         ⇒ XMLFailure.createXMLElement(failure)
      case alert: AlertMessage             ⇒ XMLAlert.createXMLElement(alert)
      case stat: StatisticsMessage         ⇒ XMLStatistics.createXMLElement(stat)
      case reg: RegistrationMessage        ⇒ XMLRegistration.createXMLElement(reg)
      case vio: ViolationMessage           ⇒ XMLViolation.createXMLElement(vio)
      case state: ProcessedUntilMessage    ⇒ XMLProcessedUntil.createXMLElement(state)
      case msg: AnyRef                     ⇒ throw new IllegalArgumentException("XMLMessage: Unsuported message class %s: value:%s".format(msg.getClass().getName, msg.toString))
    }
  }
}