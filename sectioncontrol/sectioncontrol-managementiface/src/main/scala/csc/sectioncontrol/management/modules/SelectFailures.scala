/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.modules

import java.util.Date

import akka.actor.ActorRef

import csc.curator.utils.Curator
import csc.sectioncontrol.management._
import csc.sectioncontrol.storage.{ ZkFailure, ZkFailureHistory, Paths }
import selectactor.{ SelectCorridorBaseActor, SelectSystemBaseActor }
import csc.sectioncontrol.storagelayer.state.FailureService

class SelectFailures(val curator: Curator, val producer: ActorRef) extends SelectCorridorBaseActor {
  val moduleId = ModuleIds.failure

  def doCheck(systemId: String, corridorId: String, startTime: Long): Seq[MaintenanceMessage] = {
    val alerts = FailureService.getFailureHistory(systemId, corridorId, startTime, System.currentTimeMillis(), curator)
    alerts.map(hist ⇒ {
      if (hist.endTime == FailureService.endOfTime) {
        //current Alert
        FailureMessage(systemId = systemId,
          corridorId = corridorId,
          failureType = hist.failureType,
          time = new Date(hist.startTime),
          path = hist.path,
          message = hist.message,
          configType = hist.configType.toString,
          startTime = None)
      } else {
        //history alert
        FailureMessage(systemId = systemId,
          corridorId = corridorId,
          failureType = hist.failureType,
          time = new Date(hist.endTime),
          path = hist.path,
          message = hist.message,
          configType = hist.configType,
          startTime = Some(new Date(hist.startTime)))
      }
    })
  }
}
