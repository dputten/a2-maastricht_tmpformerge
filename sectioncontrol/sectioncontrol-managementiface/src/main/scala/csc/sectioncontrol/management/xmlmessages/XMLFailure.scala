/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.xmlmessages

import xml.Elem
import csc.sectioncontrol.management.{ FailureMessage, StateUpdateMessage }

object XMLFailure {
  def createXMLElement(msg: FailureMessage): Elem = {

    <alarm>
      <corridorId>{ msg.corridorId }</corridorId>
      <component>{ msg.path }</component>
      <failureType>{ msg.failureType }</failureType>
      {
        msg.startTime match {
          case Some(start) ⇒ {
            <tijd>{ XMLDataTypes.createDateTime(start) }</tijd>
            <opgelost>{ XMLDataTypes.createDateTime(msg.time) }</opgelost>
          }
          case None ⇒ {
            <tijd>{ XMLDataTypes.createDateTime(msg.time) }</tijd>
          }
        }
      }
      <type>{ msg.configType }</type>
      <opmerking>{ msg.message }</opmerking>
    </alarm>
  }

}