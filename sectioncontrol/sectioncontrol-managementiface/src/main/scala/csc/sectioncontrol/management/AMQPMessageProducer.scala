/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management

import akka.actor.{ ActorLogging, Actor, ActorRef }
import com.rabbitmq.client.AMQP
import com.github.sstone.amqp.Amqp.Publish
import csc.sectioncontrol.storage.ZkApplicationInfo

class AMQPMessageProducer(application: ZkApplicationInfo,
                          messageBuilder: MessageBuilder,
                          exchange: ⇒ String,
                          keyTemplate: ⇒ String,
                          channelOwner: ActorRef) extends Actor with ActorLogging {

  override def preRestart(reason: Throwable, message: Option[Any]) {
    super.preRestart(reason, message)
    log.info("Restart producer while processing %s".format(message), reason)
  }

  def receive = {
    case msg: MaintenanceMessage ⇒ {
      val properties = new AMQP.BasicProperties.Builder()
      properties.contentType("text/xml")
      properties.contentEncoding("ISO-8859-1")
      properties.appId(application.id)
      properties.timestamp(msg.time)
      val msgKey = createKey(msg)
      val mqMsg = messageBuilder.createMessage(msg)
      channelOwner ! Publish(exchange, msgKey, mqMsg, properties = Some(properties.build()), mandatory = true, immediate = false)
    }
  }
  private def createKey(msg: MaintenanceMessage): String = {
    keyTemplate.count(ch ⇒ '%' == ch) match {
      case 0 ⇒ keyTemplate
      case 1 ⇒ keyTemplate.format(msg.systemId)
      case 2 ⇒ keyTemplate.format(msg.systemId, msg.getMessageType())
      case nr ⇒ {
        log.warning("Unexpected number of %% (%d>2) in message key template [%s]".format(nr, keyTemplate))
        keyTemplate
      }
    }
  }

}