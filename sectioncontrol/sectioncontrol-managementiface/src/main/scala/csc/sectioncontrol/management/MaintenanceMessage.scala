/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management

import java.util.Date
import csc.sectioncontrol.reports.ZkDayStatistics
import csc.sectioncontrol.messages.matcher.MatcherCorridorConfig
import csc.sectioncontrol.storage.{ SectionControlStatistics, RedLightStatistics, SpeedFixedStatistics }
import csc.sectioncontrol.messages.{ ProcessedViolationRecord, VehicleViolationRecord, ValueWithConfidence_Float, ValueWithConfidence }

trait MaintenanceMessage {
  val systemId: String
  val time: Date
  def getMessageType(): String
}

case class DailyReportMessage(systemId: String, time: Date, report: ZkDayStatistics) extends MaintenanceMessage {
  def getMessageType(): String = "report"
}

case class StateUpdateMessage(systemId: String, corridorId: String, time: Date, oldState: String, newState: String, reason: String) extends MaintenanceMessage {
  def getMessageType(): String = "state"
}

case class CalibrateMessage(systemId: String, corridorId: String, time: Date, success: Boolean) extends MaintenanceMessage {
  def getMessageType(): String = "calibrate"
}

case class FailureMessage(systemId: String, corridorId: String, failureType: String, time: Date, path: String, message: String, configType: String, startTime: Option[Date]) extends MaintenanceMessage {
  def getMessageType(): String = "failure"
}

case class AlertMessage(systemId: String, corridorId: String, alertType: String, time: Date, path: String, message: String, configType: String, reductionFactor: Float, startTime: Option[Date]) extends MaintenanceMessage {
  def getMessageType(): String = "alert"
}

case class StatisticsMessage(systemId: String, time: Date, corridorId: String,
                             section: Option[SectionControlStatistics] = None,
                             redLight: Option[RedLightStatistics] = None,
                             speed: Option[SpeedFixedStatistics] = None) extends MaintenanceMessage {
  def getMessageType(): String = "statistics"
}

case class RegistrationMessage(systemId: String, time: Date, laneId: String,
                               speed: Option[ValueWithConfidence_Float],
                               license: Option[ValueWithConfidence[String]],
                               country: Option[ValueWithConfidence[String]],
                               category: Option[ValueWithConfidence[String]]) extends MaintenanceMessage {
  def getMessageType(): String = "registration"
}

case class ViolationMessage(systemId: String, time: Date, violation: ProcessedViolationRecord) extends MaintenanceMessage {
  def getMessageType(): String = "violation"
}

case class ProcessDetails(detail: String, lastUpdated: Date, completionTime: Date)
case class ProcessedUntilMessage(systemId: String, time: Date, process: String, completionTime: Date, detailType: String, details: Seq[ProcessDetails]) extends MaintenanceMessage {
  def getMessageType(): String = "processUntil"
}

object ProcessedUntilMessage {
  val detailSystem = "system"
  val detailCorridor = "corridor"
  val detailLane = "lane"
}

