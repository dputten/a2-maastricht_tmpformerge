/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management

import akka.actor.Props

import org.apache.curator.retry.ExponentialBackoffRetry
import org.apache.curator.framework.CuratorFramework
import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.client.HConnectionManager

import net.liftweb.json.Formats

import csc.akkautils.GenericBoot
import csc.json.lift.EnumerationSerializer
import csc.curator.utils.CuratorToolsImpl

import csc.sectioncontrol.storagelayer.StorageFactory
import csc.sectioncontrol.messages.{ EsaProviderType, VehicleImageType, VehicleCode, VehicleCategory }
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storagelayer.hbasetables.RowKeyDistributorFactory

class Boot extends GenericBoot {
  var curator: Option[CuratorFramework] = None
  var hbaseConf: Option[Configuration] = None

  /**
   * Defines the name of the component being booted, and also the name of the actor
   * system that will be started.
   * This is also used as the base of the name of the configuration file to read by
   * appending ".conf" to this name.
   */
  def componentName = "sectioncontrol-maintenance"

  /**
   * Implemented by subclasses to perform an orderly shutdown of the actors in the
   * actor system.
   */
  def shutdownActors() {
    hbaseConf.foreach(hbase ⇒ HConnectionManager.deleteConnection(hbase))
    hbaseConf = None
    curator.foreach(zk ⇒ zk.close())
    curator = None
  }

  /**
   * Implemented by subclasses to start up any required actors in the actor system.
   */
  def startupActors() {
    val configuration = actorSystem.settings.config
    val zkServerQuorum = configuration.getString("sectioncontrol.maintenance.zookeeper.zkServers")
    val zkSessionTimeout = configuration.getInt("sectioncontrol.maintenance.zookeeper.sessionTimeout")
    val zkRetries = configuration.getInt("sectioncontrol.maintenance.zookeeper.retries")
    curator = StorageFactory.newClient(zkServerQuorum, new ExponentialBackoffRetry(zkSessionTimeout, zkRetries))
    curator.foreach(_.start())
    val zookeeperClient = new CuratorToolsImpl(curator, log) {
      override def extendFormats(defaultFormats: Formats): Formats = {
        super.extendFormats(defaultFormats) +
          new EnumerationSerializer(VehicleCategory,
            ZkWheelbaseType,
            VehicleCode,
            ZkIndicationType,
            ServiceType,
            ZkCaseFileType,
            EsaProviderType,
            VehicleImageType,
            DayReportVersion,
            ConfigType)
      }
    }
    RowKeyDistributorFactory.init(zookeeperClient)
    val zkHBaseServerQuorum = configuration.getString("sectioncontrol.maintenance.zookeeper.hbase.zkServers")
    val config = HBaseConfiguration.create()
    config.set("hbase.zookeeper.quorum", zkHBaseServerQuorum)
    hbaseConf = Some(config)

    actorSystem.actorOf(Props(new ManagementStartActor(zookeeperClient, config)), name = "ManagementActor")

  }
}