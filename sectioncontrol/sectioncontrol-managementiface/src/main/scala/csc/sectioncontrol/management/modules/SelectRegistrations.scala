/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.modules

import java.util.Date

import akka.actor.{ ActorLogging, Actor, ActorRef }

import org.apache.hadoop.conf.Configuration

import csc.curator.utils.Curator
import csc.hbase.utils.HBaseTableKeeper

import csc.sectioncontrol.management.selectactor.{ SelectionTable, SelectionSupport }
import csc.sectioncontrol.management._
import csc.sectioncontrol.storagelayer.hbasetables.VehicleTable

class SelectRegistrations(val curator: Curator, val producer: ActorRef, hbaseConfig: Configuration) extends Actor
  with ActorLogging with SelectionSupport with SelectionTable[VehicleTable.Reader] {

  val moduleId = ModuleIds.registration

  override def preRestart(reason: Throwable, message: Option[Any]) {
    super.preRestart(reason, message)
    log.error("Restart while processing message %s".format(message), reason)
  }
  override def postStop() {
    closeTables()
    super.postStop()
  }

  override def receive = {
    case Wakeup ⇒ {
      val systems = getSystems()
      systems.foreach(systemId ⇒ {
        //get all laneIds
        val lanes = getLanesIds(systemId)
        //get all start times
        val startTimes = lanes.map(lane ⇒ (lane, getLastUpdate(systemId, lane.uniqueLaneId))).toMap
        val gantries = lanes.map(_.gantryId).distinct

        //get all registrations of a system with the minimal startTime
        val now = System.currentTimeMillis()
        //selection can only be done per gantry
        val registrations = gantries.flatMap(gantryId ⇒ {
          val startTime = startTimes.filter(_._1.gantryId == gantryId).values.min
          val keyPrefix = "%s-%s".format(systemId, gantryId)
          getReader("").readRows((keyPrefix, startTime + 1), (keyPrefix, now))
        })
        //group the registrations by laneId and process them per lane
        val registrationsByLane = registrations.groupBy(_.lane.laneId)
        registrationsByLane.map {
          case (laneId, regList) ⇒ {
            //get the startTime for this lane
            val laneStartTime = startTimes.find(_._1.uniqueLaneId == laneId).map(_._2).getOrElse(0L)
            //remove the old registrations
            val newRegistrations = regList.filter(_.eventTimestamp > laneStartTime)
            //translate the registrations into the RegistrationMessage
            val messages = newRegistrations.map(reg ⇒
              RegistrationMessage(systemId = systemId,
                time = new Date(reg.eventTimestamp),
                laneId = laneId,
                speed = reg.speed,
                license = reg.license,
                country = reg.country,
                category = reg.category))
            sendMessage(messages, laneId)
          }
        }
      })
    }
  }

  def makeReader(table: HBaseTableKeeper, systemId: String): VehicleTable.Reader = {
    VehicleTable.makeReader(hbaseConfig, table)
  }

}