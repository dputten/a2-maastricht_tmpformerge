/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.xmlmessages

import xml.Elem
import csc.sectioncontrol.management.ViolationMessage
import csc.sectioncontrol.messages.{ VehicleImageType, ProcessingIndicator, VehicleMetadata }
import akka.util.duration._

object XMLViolation {
  def createXMLElement(msg: ViolationMessage): Elem = {
    val reg = getRegistration(msg)
    val classify = msg.violation.violationRecord.classifiedRecord
    val firstImage = reg.getImage(VehicleImageType.RedLight)

    <overzichtZaakbestand>
      <locatie>{ reg.lane.laneId }</locatie>
      <tijd>{ XMLDataTypes.createDateTime(msg.violation.violationRecord.classifiedRecord.time) }</tijd>
      {
        if (msg.violation.pardoned) {
          <zaakbestand>pardon</zaakbestand>
        } else {
          classify.indicator match {
            case ProcessingIndicator.Automatic                              ⇒ <zaakbestand>auto</zaakbestand>
            case ProcessingIndicator.Mobi                                   ⇒ <zaakbestand>mobi</zaakbestand>
            case ProcessingIndicator.Manual if (classify.manualAccordingToSpec)⇒ <zaakbestand>handVolgensSpec</zaakbestand>
            case ProcessingIndicator.Manual                                 ⇒ <zaakbestand>hand</zaakbestand>
          }
        }
      }
      {
        if (msg.violation.pardoned) {
          <reden>{ msg.violation.pardonReason.getOrElse("") }</reden>
        } else {
          val reason = classify.reason
          var value = ""
          if (reason.isUnreliableClassification()) {
            value += ":Classification"
          }
          if (reason.isUnreliableLicense()) {
            value += ":License"

          }
          if (reason.isUnreliableCountry()) {
            value += ":Country"
          }
          if (!value.isEmpty()) {
            value = "Unreliable" + value
          }
          <reden>{ value }</reden>
        }

      }
      {
        reg.license match {
          case Some(lic)⇒ <kenteken betrouwbaarheid={ lic.confidence.toString }>{ lic.value }</kenteken>
          case None  ⇒
        }
      }
      {
        reg.country match {
          case Some(cnt)⇒ <land betrouwbaarheid={ cnt.confidence.toString }>{ cnt.value }</land>
          case None  ⇒
        }
      }
      {
        classify.code match {
          case Some(code)⇒ <categorieCode>{ code.toString }</categorieCode>
          case None   ⇒
        }
      }
      {
        classify.alternativeClassification match {
          case Some(code)⇒ <snelheidCategorieCode>{ code.toString }</snelheidCategorieCode>
          case None   ⇒
        }
      }
      <snelheid>{ classify.speedRecord.speed }</snelheid>
      <handhaafSnelheid>{ msg.violation.violationRecord.enforcedSpeed }</handhaafSnelheid>
      {
        firstImage.flatMap(_.timeRed) match {
          case Some(t)⇒ <roodTijd>{ XMLDataTypes.createDuration(t.millis) }</roodTijd>
          case None⇒
        }
      }
      {
        firstImage.flatMap(_.timeYellow) match {
          case Some(t)⇒ <geelTijd>{ XMLDataTypes.createDuration(t.millis) }</geelTijd>
          case None⇒
        }
      }
    </overzichtZaakbestand>
  }
  def getRegistration(msg: ViolationMessage): VehicleMetadata = {
    val entry = msg.violation.violationRecord.classifiedRecord.speedRecord.entry
    val exit = msg.violation.violationRecord.classifiedRecord.speedRecord.exit
    exit.getOrElse(entry)
  }
}