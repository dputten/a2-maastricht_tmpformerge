/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.modules

import java.util.Date

import akka.actor.{ ActorLogging, Actor, Props, ActorRef }

import org.apache.hadoop.conf.Configuration

import csc.curator.utils.Curator

import csc.sectioncontrol.management.selectactor.SelectionSupport
import csc.sectioncontrol.management.{ StatisticsMessage, Wakeup, ModuleIds }
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storagelayer.servicestatistics.ServiceStatisticsHBaseRepositoryActor
import csc.sectioncontrol.storagelayer.servicestatistics.ServiceStatisticsHBaseRepositoryActor._
import csc.sectioncontrol.storage.ZkCorridor

class SelectStatistics(val curator: Curator, val producer: ActorRef, hbaseConfig: Configuration) extends Actor with ActorLogging with SelectionSupport {
  val moduleId = ModuleIds.statistics
  var corridorMapping = Map[ServiceType.Value, String]()

  private lazy val repository = context.actorOf(Props(new ServiceStatisticsHBaseRepositoryActor(hbaseConfig)))

  override def preRestart(reason: Throwable, message: Option[Any]) {
    super.preRestart(reason, message)
    log.error("Restart while processing message %s".format(message), reason)
  }

  override def receive = {
    case Wakeup ⇒ {
      val systems = getSystems()
      systems.foreach(systemId ⇒ {
        //get corridors
        val corridors = getCorridors(systemId)
        val corridorTypes = corridors.flatMap(corridorId ⇒ getCorridorType(systemId, corridorId).map((_, corridorId))).toMap
        corridorMapping = corridorTypes

        corridorTypes.keySet.foreach(corType ⇒ {
          val startTime = getLastUpdate(systemId, corType.toString)
          corType match {
            case ServiceType.RedLight       ⇒ repository ! GetServiceStatisticsPeriod(systemId, startTime + 1, Long.MaxValue, RedLightStatisticsType)
            case ServiceType.SpeedFixed     ⇒ repository ! GetServiceStatisticsPeriod(systemId, startTime + 1, Long.MaxValue, SpeedFixedStatisticsType)
            case ServiceType.SectionControl ⇒ repository ! GetServiceStatisticsPeriod(systemId, startTime + 1, Long.MaxValue, SectionControlStatisticsType)
            case _                          ⇒ //other types are not supported
          }
        })
      })
    }
    case result: SpeedFixedStatisticsResultPeriod ⇒ {
      val corridorId = corridorMapping.get(ServiceType.SpeedFixed).getOrElse("")
      val statistics = result.statistics.map(stat ⇒ StatisticsMessage(result.systemId, new Date(stat.period.end), corridorId = corridorId, speed = Some(stat)))
      sendMessage(statistics, ServiceType.SpeedFixed.toString)
    }
    case result: RedLightStatisticsResultPeriod ⇒ {
      val corridorId = corridorMapping.get(ServiceType.RedLight).getOrElse("")
      val statistics = result.statistics.map(stat ⇒ StatisticsMessage(result.systemId, new Date(stat.period.end), corridorId = corridorId, redLight = Some(stat)))
      sendMessage(statistics, ServiceType.RedLight.toString)
    }
    case result: SectionControlStatisticsResultPeriod ⇒ {
      val corridorId = corridorMapping.get(ServiceType.SectionControl).getOrElse("")
      val statistics = result.statistics.map(stat ⇒ StatisticsMessage(result.systemId, new Date(stat.period.end), corridorId = corridorId, section = Some(stat)))
      sendMessage(statistics, ServiceType.SectionControl.toString)
    }
  }

  def getCorridorType(systemId: String, corridorId: String): Option[ServiceType.Value] = {
    val corridorOpt = curator.get[ZkCorridor](Paths.Corridors.getConfigPath(systemId, corridorId))
    corridorOpt.map(_.serviceType)
  }

}