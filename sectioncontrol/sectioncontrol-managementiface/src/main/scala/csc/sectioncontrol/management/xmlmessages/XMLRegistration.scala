/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.xmlmessages

import xml.Elem
import csc.sectioncontrol.management.{ RegistrationMessage, AlertMessage }

object XMLRegistration {
  def createXMLElement(msg: RegistrationMessage): Elem = {
    <passage>
      <locatie>{ msg.laneId }</locatie>
      <tijd>{ XMLDataTypes.createDateTime(msg.time) }</tijd>
      {
        msg.license match {
          case Some(lic)⇒ <kenteken betrouwbaarheid={ lic.confidence.toString }>{ lic.value }</kenteken>
          case None  ⇒
        }
      }
      {
        msg.country match {
          case Some(cnt)⇒ <land betrouwbaarheid={ cnt.confidence.toString }>{ cnt.value }</land>
          case None  ⇒
        }
      }
      {
        msg.category match {
          case Some(cat)⇒ <categorie betrouwbaarheid={ cat.confidence.toString }>{ cat.value }</categorie>
          case None  ⇒
        }
      }
      {
        msg.speed match {
          case Some(speed)⇒ <snelheid>{ XMLDataTypes.createFloat(speed.value) }</snelheid>
          case None    ⇒
        }
      }
    </passage>
  }

}
