/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.xmlmessages

import xml.Elem
import csc.sectioncontrol.management.{ AlertMessage, FailureMessage }

object XMLAlert {
  def createXMLElement(msg: AlertMessage): Elem = {

    <storing>
      <corridorId>{ msg.corridorId }</corridorId>
      <component>{ msg.path }</component>
      <alertType>{ msg.alertType }</alertType>
      {
        msg.startTime match {
          case Some(start) ⇒ {
            <tijd>{ XMLDataTypes.createDateTime(start) }</tijd>
            <opgelost>{ XMLDataTypes.createDateTime(msg.time) }</opgelost>
          }
          case None ⇒ {
            <tijd>{ XMLDataTypes.createDateTime(msg.time) }</tijd>
          }
        }
      }
      <type>{ msg.configType }</type>
      <opmerking>{ msg.message }</opmerking>
      <reductiefactor>{ XMLDataTypes.createFloat(msg.reductionFactor) }</reductiefactor>
    </storing>
  }

}