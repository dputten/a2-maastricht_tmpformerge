/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management

import scala.Some
import xmlmessages.XMLMessageBuilder

import akka.actor._
import akka.util.duration._

import org.apache.hadoop.conf.Configuration

import com.github.sstone.amqp.{ Amqp, RabbitMQConnection }

import csc.curator.utils.Curator
import modules._

import csc.sectioncontrol.storage.Paths
import csc.sectioncontrol.storage.ZkApplicationInfo
import csc.sectioncontrol.storage.ZkMaintenanceConfig

object ModuleIds {
  val dailyReport = "DailyReport"
  val statusUpdate = "StatusUpdate"
  val calibration = "Calibration"
  val failure = "Failure"
  val alert = "Alert"
  val statistics = "Statistics"
  val registration = "Registration"
  val violation = "Violation"
  val processUntil = "ProcessUntil"
}

case object Wakeup

class ManagementStartActor(curator: Curator, hbaseCfg: Configuration) extends Actor with ActorLogging {

  val producerKey = "producer"
  val channelOwnerKey = "channelOwner"
  val modulePrefix = "Module_"
  var actorRefs = Map[String, ActorRef]()

  var timer: Option[Cancellable] = None
  var mqConnection: Option[RabbitMQConnection] = None

  override def preStart() {
    super.preStart()
    //start MQ Interface Actors
    // create an AMQP connection
    val cfg = getConfiguration(curator)
    implicit val system = context.system
    val conn = new RabbitMQConnection(
      host = cfg.flatMap(_.mq.host).getOrElse("localhost"),
      name = cfg.flatMap(_.mq.name).getOrElse("Connection"),
      port = cfg.flatMap(_.mq.port).getOrElse(5672),
      vhost = cfg.flatMap(_.mq.vhost).getOrElse("/"),
      user = cfg.flatMap(_.mq.user).getOrElse("guest"),
      password = cfg.flatMap(_.mq.password).getOrElse("guest"))
    mqConnection = Some(conn)
    // create a "channel owner" on this connection
    val channelOwner = conn.createChannelOwner()
    actorRefs += channelOwnerKey -> channelOwner
    val appInfo = cfg.map(_.applInfo).getOrElse(ZkApplicationInfo("Unknown", "Unknown", "Unknown"))
    val producer = context.actorOf(Props(
      new AMQPMessageProducer(application = appInfo,
        messageBuilder = new XMLMessageBuilder(appInfo),
        exchange = cfg.flatMap(_.mq.exchangeName).getOrElse("MNGT"),
        keyTemplate = cfg.flatMap(_.mq.msgKeyTemplate).getOrElse("MNGT.%s.%s"),
        channelOwner = channelOwner)))

    actorRefs += producerKey -> producer

    val schedule = context.system.scheduler.schedule(1.second, cfg.map(_.frequency.millis).getOrElse(1.minute), self, Wakeup)
    timer = Some(schedule)
  }

  override def postStop() {
    super.postStop()
    //stop Actors
    actorRefs.values.foreach { context.stop(_) }
    actorRefs = Map[String, ActorRef]()
    //stop the schedule
    timer.foreach(_.cancel())
    //stop the mqConnection
    mqConnection.foreach(_.stop)
  }

  def receive = {
    case Wakeup ⇒ {
      //read config
      getConfiguration(curator) match {
        case Some(cfg) ⇒ {
          var modules = getModules()
          //start modules
          cfg.modules.foreach(mod ⇒ {
            val moduleKey = createModuleKey(mod.id)
            if (mod.enable) {
              val moduleRef = modules.get(moduleKey)
              if (moduleRef.isEmpty) {
                //start module and store the reference
                startModule(mod.id)
              } else {
                //module already started just remove reference from modules
                modules -= moduleKey
              }
            }
          })
          //all modules which has to be started are started
          //all modules still present in modules have to be stopped
          modules.keys.foreach(stopModule(_))

          //Send Wakeup to all modules
          val activeModules = getModules().values
          activeModules.foreach(_ ! Wakeup)
        }
        case None ⇒ //skip processing
      }
    }
  }

  /**
   * Get the configuration for the managementInterface
   * @param curator The curator
   * @return the configuration when found
   */
  private def getConfiguration(curator: Curator): Option[ZkMaintenanceConfig] = {
    val path = Paths.MaintenanceInterface.getConfigPath()
    curator.get[ZkMaintenanceConfig](path)
  }

  /**
   * Create a moduleKey for the actorRefs map
   * @param moduleId the moduleId
   * @return the key used in the actorRefs map
   */
  private def createModuleKey(moduleId: String): String = {
    modulePrefix + moduleId
  }

  /**
   * Get all modules from the actorRefs map
   * @return
   */
  private def getModules(): Map[String, ActorRef] = {
    actorRefs.filter { case (key, ref) ⇒ key.startsWith(modulePrefix) }
  }

  /**
   * Start a module using the moduleId and add it to the actorRefs
   * @param moduleId the moduleID of the module that has to be started
   */
  private def startModule(moduleId: String) {
    val producer = actorRefs.get(producerKey)
    if (producer.isEmpty) {
      //should not happen. throw exception causing this actor to restart and create
      //the producer again
      throw new IllegalStateException("Producer isn't available")
    }
    moduleId match {
      case ModuleIds.dailyReport ⇒ {
        actorRefs += createModuleKey(moduleId) -> context.actorOf(
          Props(new SelectDailyReport(curator, producer.get)),
          moduleId)
      }
      case ModuleIds.statusUpdate ⇒ {
        actorRefs += createModuleKey(moduleId) -> context.actorOf(
          Props(new SelectState(curator, producer.get)),
          moduleId)
      }
      case ModuleIds.calibration ⇒ {
        actorRefs += createModuleKey(moduleId) -> context.actorOf(
          Props(new SelectCalibration(curator, producer.get, hbaseCfg)),
          moduleId)
      }
      case ModuleIds.failure ⇒ {
        actorRefs += createModuleKey(moduleId) -> context.actorOf(
          Props(new SelectFailures(curator, producer.get)),
          moduleId)
      }
      case ModuleIds.alert ⇒ {
        actorRefs += createModuleKey(moduleId) -> context.actorOf(
          Props(new SelectAlerts(curator, producer.get)),
          moduleId)
      }
      case ModuleIds.statistics ⇒ {
        actorRefs += createModuleKey(moduleId) -> context.actorOf(
          Props(new SelectStatistics(curator, producer.get, hbaseCfg)),
          moduleId)
      }
      case ModuleIds.registration ⇒ {
        actorRefs += createModuleKey(moduleId) -> context.actorOf(
          Props(new SelectRegistrations(curator, producer.get, hbaseCfg)),
          moduleId)
      }
      case ModuleIds.violation ⇒ {
        actorRefs += createModuleKey(moduleId) -> context.actorOf(
          Props(new SelectViolations(curator, producer.get, hbaseCfg)),
          moduleId)
      }
      case ModuleIds.processUntil ⇒ {
        actorRefs += createModuleKey(moduleId) -> context.actorOf(
          Props(new SelectProcessedUntil(curator, producer.get)),
          moduleId)
      }
      case _ ⇒ log.warning("Couldn't start an unknown moduleId: " + moduleId)
    }
  }

  /**
   * Stop a module using the moduleKey
   * and remove it form the actorRefs
   * @param moduleKey The module key
   */
  private def stopModule(moduleKey: String) {
    val refOpt = actorRefs.get(moduleKey)
    if (refOpt.isDefined) {
      val ref = refOpt.get
      if (ref.path.name.startsWith(modulePrefix)) {
        context.stop(ref)
      }
      actorRefs -= moduleKey
    }
  }
}

