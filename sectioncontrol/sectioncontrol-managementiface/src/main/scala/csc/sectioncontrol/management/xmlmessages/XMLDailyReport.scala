/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.management.xmlmessages

import resource._
import xml.{ XML, Elem }
import csc.sectioncontrol.management.DailyReportMessage
import java.util.zip.GZIPInputStream
import java.io.ByteArrayInputStream

object XMLDailyReport {

  def createXMLElement(msg: DailyReportMessage): Elem = {
    val reportData = msg.report.data.map(_.toByte).toArray

    <dagrapportage>{ decompress(reportData) }</dagrapportage>
  }

  /**
   * decompress a byte array using GZip.
   * Warning: This will only work if memory is larger than the given data and the decompress data combined
   * @param data to be decompressed
   * @return decompressed version of data
   */
  def decompress(data: Array[Byte]): Elem = {
    managed(new GZIPInputStream(new ByteArrayInputStream(data))) acquireAndGet {
      gzip ⇒ XML.load(gzip)
    }
  }

}