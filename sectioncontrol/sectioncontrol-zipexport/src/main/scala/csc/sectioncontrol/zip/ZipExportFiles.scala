/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.zip

import java.io.{ FileFilter, File }
import org.apache.commons.io.FileUtils

object ZipExportFiles {

  /**
   * Zip the specified directories and place them into the export directory.
   * When the zip file already exists the fileName gets a post fix .Nr
   * @param dirs  The directories which have to be compressed
   * @param zipFileName the requested zip filename
   * @param splitLength the length of the zipfile parts
   * @param exportDir the export directory
   * @param workingDir A temporal working directory
   */
  def processZip(dirs: Seq[File], zipFileName: String, splitLength: Option[Long], exportDir: File, workingDir: File) {
    if (dirs.size > 0) {

      val tmpFileName = "tmp." + zipFileName
      val tmpWorkingDir = tmpFile(workingDir, tmpFileName)

      // if we had a failure, we might have new files since this restart
      val newDirsSinceLastRun = newDirsSinceLastFailedRun(tmpWorkingDir, dirs)

      zipDir(newDirsSinceLastRun, dirs, tmpWorkingDir)

      //zip dirs
      val tmpZipFile = new File(workingDir, zipFileName)
      zipDirectory(tmpWorkingDir, tmpZipFile, splitLength)

      //move zipFiles to export Dir
      moveZipFiles(tmpZipFile, exportDir)
      //remove tempDir
      FileUtils.deleteDirectory(tmpWorkingDir)

    }
  }

  /**
   * this method receives the original files list, and a collection that denotes the difference between the original files list and
   * files that might have been in the working dir. In case of a failure the working dir might be there with some files.
   *  1. In case of no working dir, we proceed if it is a normal run.
   *  2. In case of of working dir with some files that are now new, only the new ones are added to the working dir
   *  3. In case of the working dir existed with the files, and no new files, skip creation and copying/moving
   *
   *  As the working dir is the place from where the rest of the zip-export is done, here we can make above decisions.
   *  It also works because the newDirsSinceLastFailedRun method called earlier guarantees us that if no working dir exists, we can safely use the case 1 above.
   *
   *  Note: this method takes care of the actual creation of the workingDir by means of the FileUtils method of apache which is called in the createZipStructure method.
   *  If the destination folder does not exist, the destination will be created.
   *
   * @param newDirsSinceLastRun
   * @param dirs
   * @param tmpWorkingDir
   */

  private def zipDir(newDirsSinceLastRun: Seq[File], dirs: Seq[File], tmpWorkingDir: File) = {

    val NO_NEW_DIRECTORIES_AVAILABLE = true
    val NEW_DIRECTORIES_AVAILABLE = !NO_NEW_DIRECTORIES_AVAILABLE
    val TMP_WORKING_DIR_AVAILABLE = true
    val NO_TMP_WORKING_DIR = !TMP_WORKING_DIR_AVAILABLE

    (newDirsSinceLastRun.isEmpty, tmpWorkingDir.exists()) match {
      case (NO_NEW_DIRECTORIES_AVAILABLE, NO_TMP_WORKING_DIR)        ⇒ createZipStructure(dirs, tmpWorkingDir)
      case (NEW_DIRECTORIES_AVAILABLE, _)                            ⇒ createZipStructure(newDirsSinceLastRun, tmpWorkingDir)
      case (NO_NEW_DIRECTORIES_AVAILABLE, TMP_WORKING_DIR_AVAILABLE) ⇒ // former run crashed, tmp folder still has all files
    }
  }

  private def newDirsSinceLastFailedRun(tmpWorkingDir: File, dirs: Seq[File]): Seq[File] = {
    tmpWorkingDir.exists() match {
      case true  ⇒ dirs.filter(dir ⇒ !(tmpWorkingDir.listFiles() map (_.getName)).contains(dir.getName))
      case false ⇒ Seq.empty[File]
    }
  }

  private def tmpFile(file: File, tmpDirName: String) = new File(file, tmpDirName)

  /**
   * move the registration directories to the temporaly directory
   * @param dirs  The registration directories
   * @param zipDir The working directory
   * @return The new Directory where the registration files are moved to
   */
  def createZipStructure(dirs: Seq[File], zipDir: File) {

    dirs.foreach(dir ⇒ {
      FileUtils.moveToDirectory(dir, zipDir, true)
    })

  }

  /**
   * Zip the directory. When the zipfile already exists the zip file
   * is removed including all the partial files
   * @param zipDir  The zipDirectory
   * @param zipFile The file name
   * @param splitLength The split length of the partial zip files
   */
  def zipDirectory(zipDir: File, zipFile: File, splitLength: Option[Long]) {
    if (zipFile.exists()) {
      //remove old zipfiles
      getZipFiles(zipFile).foreach(_.delete())
    }
    ZipUtility.compressFiles(zipDir.listFiles(), zipFile.getAbsolutePath, splitLength)
  }

  /**
   * Find all the partial files of the zipfile.
   * @param zipFile
   * @return
   */
  def getZipFiles(zipFile: File): Seq[File] = {
    //find all parts of the zip file
    val name = zipFile.getName
    val pos = name.lastIndexOf(".")
    val prefix = if (pos >= 0) {
      name.substring(0, pos)
    } else {
      name
    }
    val filter = new FileFilter() {
      def accept(pathname: File): Boolean = {
        val name = pathname.getName
        val pos = name.lastIndexOf(".")
        val pathPrefix = if (pos >= 0) {
          name.substring(0, pos)
        } else {
          name
        }
        pathPrefix == prefix
      }
    }
    zipFile.getParentFile.listFiles(filter)
  }

  /**
   * Move the zip file including all the partial zipfiles.
   * And move first the partial files and at last .zip file itself
   * @param tmpZipFile The zipfile
   * @param exportDir The export Directory
   */
  def moveZipFiles(tmpZipFile: File, exportDir: File) {
    if (!exportDir.exists()) {
      exportDir.mkdirs()
    }
    val zipFileParts = getZipFiles(tmpZipFile)
    val name = tmpZipFile.getName
    //create another base name when the zip file already exists
    val baseName = createBaseName(name, exportDir)

    //move only the zip parts
    zipFileParts.foreach(part ⇒ {
      if (part.getName != name && part.isFile) {
        val newName = new File(exportDir, createFileName(baseName, part))
        FileUtils.moveFile(part, newName)
      }
    })

    //move the zip file indicating that all zipfiles are present
    val newName = new File(exportDir, createFileName(baseName, tmpZipFile))
    FileUtils.moveFile(tmpZipFile, newName)
  }

  /**
   * Check if the zipfile already exists and create a new baseFilename when
   * the zipfile already exists
   * @param tmpZipFile the requested fileName
   * @param exportDir The export directory
   * @return None when the zip file doesn't exist yet and otherwise the new baseFile name
   */
  def createBaseName(tmpZipFile: String, exportDir: File): Option[String] = {
    var expZip = new File(exportDir, tmpZipFile)
    if (!expZip.exists())
      return None

    //zipfile already exists
    val pos = tmpZipFile.lastIndexOf(".")
    val prefix = if (pos >= 0) {
      tmpZipFile.substring(0, pos)
    } else {
      tmpZipFile
    }

    val postfix = if (pos >= 0) {
      tmpZipFile.substring(pos)
    } else {
      ""
    }
    var nr = 0
    do {
      nr += 1
      expZip = new File(exportDir, "%s.%d%s".format(prefix, nr, postfix))
    } while (expZip.exists())
    Some("%s.%d".format(prefix, nr))
  }

  /**
   * Create a new file name using the baseName
   * @param baseName new basename
   * @param zipPart the original file
   * @return the new filename
   */
  def createFileName(baseName: Option[String], zipPart: File): String = {
    baseName match {
      case None ⇒ zipPart.getName
      case Some(base) ⇒ {
        val name = zipPart.getName
        val pos = name.lastIndexOf(".")
        if (pos >= 0) {
          base + name.substring(pos)
        } else {
          base
        }
      }
    }
  }
}