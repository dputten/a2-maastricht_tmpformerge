/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.zip

import net.lingala.zip4j.model.ZipParameters
import net.lingala.zip4j.util.Zip4jConstants
import scala.collection.JavaConversions._
import java.io._
import net.lingala.zip4j.io.ZipOutputStream
import net.lingala.zip4j.io.SplitOutputStream
import akka.actor.LoggingFSM
import csc.akkautils.DirectLogging
import java.util.zip.ZipException

object ZipUtility extends DirectLogging {
  private final val BUFFER_SIZE = 4096
  private final val MIN_SPLIT_SIZE = 65536L

  /**
   * Compresses a collection of files to a destination zip file
   * @param listFiles A collection of files and directories
   * @param destZipFile The path of the destination zip file
   * @throws FileNotFoundException
   * @throws IOException
   */
  def compressFiles(listFiles: Seq[File], destZipFile: String, splitLen: Option[Long] = None, deflateLevel: Int = Zip4jConstants.DEFLATE_LEVEL_NORMAL) {
    val zipParams = new ZipParameters()
    zipParams.setCompressionMethod(Zip4jConstants.COMP_DEFLATE)
    zipParams.setCompressionLevel(deflateLevel)

    val zos: ZipOutputStream = if (splitLen.isEmpty) {
      new ZipOutputStream(new FileOutputStream(destZipFile))
    } else {
      // Create a split file by setting splitArchive parameter to true
      // and specifying the splitLength. SplitLenth has to be greater than
      // 65536 bytes
      val len = math.max(splitLen.getOrElse(MIN_SPLIT_SIZE), MIN_SPLIT_SIZE)
      val splitOutputStream = new SplitOutputStream(new File(destZipFile), len)
      new ZipOutputStream(splitOutputStream)
    }

    for (file ← listFiles) {
      if (file.isDirectory) {
        addFolderToZip(file, file.getName, zos, zipParams)
      } else {
        addFileToZip(file, zos, zipParams)
      }
    }
    zos.finish()

    zos.flush
    zos.close
  }

  /**
   * Adds a directory to the current zip output stream
   * @param folder the directory to be  added
   * @param parentFolder the path of parent directory
   * @param zos the current zip output stream
   * @throws FileNotFoundException
   * @throws IOException
   */
  private def addFolderToZip(folder: File, parentFolder: String, zos: ZipOutputStream, zipParams: ZipParameters) {
    for (file ← folder.listFiles) {
      if (file.isDirectory) {
        addFolderToZip(file, parentFolder + "/" + file.getName, zos, zipParams)
      } else {
        val param = zipParams.clone().asInstanceOf[ZipParameters]
        param.setRootFolderInZip(parentFolder)

        addFileToZip(file, zos, param)
      }
    }
  }

  def bufferedStream(file: File): BufferedInputStream = new BufferedInputStream(new FileInputStream(file))

  /**
   * Adds a file to the current zip output stream
   * @param file the file to be added
   * @param zos the current zip output stream
   * @throws FileNotFoundException
   * @throws IOException
   */
  private def addFileToZip(file: File, zos: ZipOutputStream, zipParams: ZipParameters) {
    zos.putNextEntry(file, zipParams)

    try {

      val bis = bufferedStream(file)
      var bytesRead: Long = 0
      val bytesIn: Array[Byte] = new Array[Byte](BUFFER_SIZE)
      var read: Int = 0

      try {
        while ((({
          read = bis.read(bytesIn);
          read
        })) != -1) {
          zos.write(bytesIn, 0, read)
          bytesRead += read
        }
      } finally {
        bis.close()
        zos.closeEntry
      }
    } catch {
      case ex: Exception ⇒
        log.error(ex, "Adding to zip file failed")
        // Agreed to rethrow the exception to keep the original flow. Issue was closing the connections.
        throw ex
    }
  }
}