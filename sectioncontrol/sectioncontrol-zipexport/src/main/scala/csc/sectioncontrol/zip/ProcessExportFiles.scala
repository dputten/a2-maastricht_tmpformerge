/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.zip

import java.io.File
import java.text.SimpleDateFormat

import org.joda.time.DateTime

import akka.event.LoggingAdapter

import csc.curator.utils.Curator
import csc.config.Path

import csc.sectioncontrol.common.DayUtility
import csc.sectioncontrol.enforce.nl.run.RegisterViolationsJobConfig
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storage.EnforcementStatus
import csc.sectioncontrol.storage.ZkExportZipConfig
import csc.sectioncontrol.storage.ZkSystem
import java.util.Calendar

class ProcessExportFiles(curator: Curator, logger: Option[LoggingAdapter] = None) {
  var lastCheck = Int.MaxValue
  var registration: Option[FindRegistrations] = None

  def getFormatDay(now: Long): String = {
    val df = new SimpleDateFormat("yyyyMMdd")
    df.format(now)
  }

  def doCheck(now: Long): Seq[ZipRegistration] = {
    logger.foreach(_.debug("Start ZipExport check"))
    val config = getConfig()
    val nrMinutesAfterMidnight = new DateTime(now).getMinuteOfDay

    logger.foreach(_.debug("Check final=%d, now=%d, last=%d".format(config.finalExportTime, nrMinutesAfterMidnight, lastCheck)))

    val yesterday = DayUtility.substractOneDay(now)
    val finishedList = if (nrMinutesAfterMidnight < config.finalExportTime) {
      //within window
      if (lastCheck > nrMinutesAfterMidnight) {
        //process new Day
        logger.foreach(_.debug("Start new day"))
        registration = Some(createNewFindRegistrations(yesterday))
      }
      registration.map(_.findFinishedSystems(checkSystemReady)).getOrElse(Seq())
    } else if (lastCheck < config.finalExportTime) {
      //timer expired export all we have
      logger.foreach(_.debug("finalExport time reached"))
      val list = registration.map(_.findFinishedSystems((systemId: String, currentDay: Long) ⇒ true)).getOrElse(Seq())
      //and reset
      registration = None
      list ++ createDailyReport()
    } else {
      Seq()
    }
    lastCheck = nrMinutesAfterMidnight
    val registrationFiles = if (!finishedList.isEmpty) {
      val exportDir = new File(config.ExportDir)
      val workingDir = new File(config.workingDir)
      val currentDay = getFormatDay(yesterday)
      finishedList.map(fin ⇒ {
        //TTTTnnnn_JJJJMMDD.zip
        val zipFile = "%s_%s.zip".format(fin.corId, currentDay)
        new ZipRegistration(dirs = fin.dirs, zipFileName = zipFile, splitLength = Some(config.splitSize), exportDir = exportDir, workingDir = workingDir)
      })
    } else {
      Seq()
    }
    if (registrationFiles.isEmpty) {
      logger.foreach(_.info("No available exports"))
    } else {
      logger.foreach(_.info("Request to create zip exports %s".format(registrationFiles)))
    }
    registrationFiles
  }

  def createDailyReport(): Option[FinishedSystem] = {
    val configPath = Path(Paths.Configuration.getReportsConfigPath)
    val reportConfig = curator.get[ZkDayReportConfig](configPath)
    reportConfig.flatMap(_.exportDirectory).flatMap(exportDirectory ⇒ {
      val dir = new File(exportDirectory)
      val dayReports = dir.listFiles()
      if (!dayReports.isEmpty) {
        Some(new FinishedSystem("dailyReport", "dagrapportage", Seq(dir)))
      } else
        None
    })
  }

  def checkSystemReady(systemId: String, currentDay: Long): Boolean = {
    val path = Paths.Systems.getEnforceStatusPath(systemId, currentDay)
    val status = curator.get[EnforcementStatus](path)
    status.exists(_.registrationComplete)
  }

  def createNewFindRegistrations(currentDay: Long): FindRegistrations = {
    val systems = curator.getChildNames(Paths.Systems.getDefaultPath)

    //create prefix map
    val prefixMap = systems.flatMap(systemId ⇒ {
      val path = Paths.Systems.getConfigPath(systemId)
      val systemConfig = curator.get[ZkSystem](path)
      systemConfig.map(cfg ⇒ (cfg.violationPrefixId, systemId))
    })
    //create exportDirs
    val exportDirs = systems.flatMap(systemId ⇒ {
      val folderConfigPath = Paths.Systems.getRegisterViolationsJobConfigPath(systemId)
      val config = curator.get[RegisterViolationsJobConfig](folderConfigPath)
      config.map(cfg ⇒ new File(cfg.exportDirectoryPath))
    }).toSet

    new FindRegistrations(currentDay = currentDay, prefixMap = prefixMap, exportDirs = exportDirs)
  }

  def getConfig(): ZkExportZipConfig = {
    val path = Paths.Configuration.getExportZipConfigPath
    curator.get[ZkExportZipConfig](path).getOrElse(
      new ZkExportZipConfig(finalExportTime = 300, //5 hours in minutes
        pollFrequency = 300000, //5 minuten in msec
        nrWorkers = 20,
        splitSize = 10485760, //10MB
        workingDir = "/tmp/exportZip",
        ExportDir = "/app/politie",
        processDate = None))
  }

}