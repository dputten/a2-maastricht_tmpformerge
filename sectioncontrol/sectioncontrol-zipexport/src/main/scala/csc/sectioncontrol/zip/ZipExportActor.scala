/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.zip

import akka.util.duration._
import akka.routing.SmallestMailboxRouter
import akka.actor.{ ActorLogging, Props, ActorRef, Actor }

import csc.curator.utils.Curator
import csc.sectioncontrol.storage.ZkExportZipConfig

object WakeUp

class ZipExportActor(curator: Curator) extends Actor with ActorLogging {
  val processor = new ProcessExportFiles(curator, Some(log))
  var zipActor: ActorRef = _

  override def preStart() {
    super.preStart()
    val config: ZkExportZipConfig = processor.getConfig()
    val polDuration = config.pollFrequency.millis
    context.system.scheduler.schedule(1.second, polDuration, self, WakeUp)
    //create routes ourself so this actor is the supervisor
    //restart only one routee and not all
    var routees = Seq[ActorRef]()
    for (nr ← 0 until config.nrWorkers) {
      routees = routees :+ context.actorOf(Props[ZipFilesActor], "zipworker%d".format(nr))
    }
    zipActor = context.actorOf(Props().withRouter(
      SmallestMailboxRouter(routees = routees)), "routerzip")
    log.info("Started ZipExportActor with config %s".format(config))
  }

  def receive = {
    case WakeUp ⇒ {
      val config = processor.getConfig()
      val processDate = config.processDate match {
        case Some(date) if date == 0 ⇒ System.currentTimeMillis()
        case None                    ⇒ System.currentTimeMillis()
        case Some(date)              ⇒ date
      }
      val zipRequests = processor.doCheck(processDate)
      zipRequests.foreach(zipActor ! _)
    }
  }
}
