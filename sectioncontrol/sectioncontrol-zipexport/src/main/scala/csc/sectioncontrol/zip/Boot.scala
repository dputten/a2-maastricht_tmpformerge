/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.zip

import akka.actor.Props

import org.apache.curator.framework.CuratorFramework
import org.apache.curator.retry.ExponentialBackoffRetry

import net.liftweb.json.Formats

import csc.akkautils.GenericBoot
import csc.curator.utils.CuratorToolsImpl
import csc.json.lift.EnumerationSerializer

import csc.sectioncontrol.storagelayer.StorageFactory
import csc.sectioncontrol.storage._
import csc.sectioncontrol.messages.{ EsaProviderType, VehicleImageType, VehicleCategory, VehicleCode }

/**
 * Start the zipExport
 */
class Boot extends GenericBoot {

  def componentName = "sectioncontrol-zipexport"
  var curator: Option[CuratorFramework] = None

  /**
   * Implemented by subclasses to start up any required actors in the actor system.
   */
  def startupActors() {
    val configuration = actorSystem.settings.config
    val zkServerQuorum = configuration.getString("sectioncontrol.zipexport.zookeeper.zkServers")
    val zkSessionTimeout = configuration.getInt("sectioncontrol.zipexport.zookeeper.sessionTimeout")
    val zkRetries = configuration.getInt("sectioncontrol.zipexport.zookeeper.retries")

    curator = StorageFactory.newClient(zkServerQuorum, new ExponentialBackoffRetry(zkSessionTimeout, zkRetries))
    curator.foreach(_.start())
    val zookeeperClient = new CuratorToolsImpl(curator, log) {
      override def extendFormats(defaultFormats: Formats): Formats = {
        super.extendFormats(defaultFormats) +
          new EnumerationSerializer(VehicleCategory,
            ZkWheelbaseType,
            VehicleCode,
            ZkIndicationType,
            ServiceType,
            ZkCaseFileType,
            EsaProviderType,
            VehicleImageType,
            DayReportVersion,
            ConfigType)
      }
    }
    actorSystem.actorOf(Props(new ZipExportActor(zookeeperClient)), name = "zipActor")
  }

  /**
   * Implemented by subclasses to perform an orderly shutdown of the actors in the
   * actor system.
   */
  def shutdownActors() {
    curator.foreach(zk ⇒ zk.close())
    curator = None
  }
}
