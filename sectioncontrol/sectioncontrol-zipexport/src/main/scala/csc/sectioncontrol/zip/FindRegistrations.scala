/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.zip

import java.io.File
import csc.akkautils.DirectLogging

case class FinishedSystem(systemId: String, corId: String, dirs: Seq[File])

/**
 *
 * @param currentDay the current day in epoch time
 * @param prefixMap mapping between prefixes and systems(prefix, systemId)
 * @param exportDirs list of export directories
 */
class FindRegistrations(currentDay: Long, prefixMap: Seq[(String, String)], exportDirs: Set[File]) extends DirectLogging {
  /**
   * Find all finished systems with there directories
   *
   * @param checkSystemReady function to be able to check if the system export is ready
   * @return boolean is the system export finished
   */
  def findFinishedSystems(checkSystemReady: (String, Long) ⇒ Boolean): Seq[FinishedSystem] = {
    val exportDirs = findRegistrations() // dagrapportages and bv a0020801 directories
    log.debug("exportDirs:" + exportDirs)
    val systemsIds = exportDirs.flatMap(dir ⇒ {
      val prefix = prefixMap.find { case (violationPrefixId, _) ⇒ dir.getName.startsWith(violationPrefixId.toLowerCase) }
      //tuple._2 systemId
      val result = prefix.map(tuple ⇒ tuple._2)
      result
    })

    log.debug("systemsIds:" + systemsIds)
    //check systems
    val readySystems = systemsIds.foldLeft(Seq[String]()) { (list, systemId) ⇒
      {
        if (checkSystemReady(systemId, currentDay)) {
          list :+ systemId
        } else {
          list
        }
      }
    }
    log.debug("readySystems:" + readySystems)

    createFinishedSystems(readySystems, exportDirs)
  }

  /**
   * create the FinishedSystem objects from the system list and the list of directories
   *
   * @param systems list of ready systems
   * @param exportDirs list of found directories
   * @return list of FinishedSystem
   */
  def createFinishedSystems(systems: Seq[String], exportDirs: Set[File]): Seq[FinishedSystem] = {
    //iterate over all systems which has to be exported
    val finishedSystems = systems.foldLeft(Seq[FinishedSystem]()) { (list, systemId) ⇒
      {
        //find prefix of current systems
        val violationPrefixIds = prefixMap.filter { case (_, sysId) ⇒ sysId == systemId }.map(tuple ⇒ tuple._1)
        val corridors = violationPrefixIds.flatMap(violationPrefixId ⇒ {
          // sysDir = all dirs for given system, These can be multiple corridors or multiple speeds
          // Skip the zaakbestand dir
          val sysDirs = exportDirs.filter(file ⇒ file.getName.startsWith(violationPrefixId))

          val groupedByCorridor = sysDirs.groupBy(_.getName.substring(0, 8))
          log.debug("directories groupedByCorridor:" + groupedByCorridor)
          groupedByCorridor.map {
            case (id, corDirs) ⇒
              // id is used in the zipexport as the name for the zip.
              // but can contain (partly) NoConfig so translate it to a expected format
              val zipname = if (id.endsWith("No")) {
                // dir name was a01201NoConfig80 id became a01201No transfer to results in: a0120001
                id.substring(0, 4) + "00" + id.substring(4, 6)
              } else {
                id
              }
              new FinishedSystem(systemId, zipname, corDirs.toSeq)
          }
        })
        list ++ corridors
      }
    }

    log.debug("finishedSystems:" + finishedSystems)

    //remove systems without any directories
    finishedSystems.filter(!_.dirs.isEmpty)
  }

  /**
   * Get a map of corridor ExportNames with the containing days in it
   *
   * @return map of corridor export names with a list of containing days
   */
  private def findRegistrations(): Set[File] = {
    exportDirs.flatMap(dir ⇒ {
      val children = dir.listFiles()
      if (children != null) {
        children.filter(_.isDirectory)
      } else {
        Set()
      }
    })
  }
}