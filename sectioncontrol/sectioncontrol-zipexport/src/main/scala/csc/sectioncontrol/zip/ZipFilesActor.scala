/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.zip

import java.io.File
import akka.actor.{ ActorLogging, Actor }

/**
 * Request to zip registration files
 * @param dirs
 * @param zipFileName
 * @param splitLength
 * @param exportDir
 * @param workingDir
 */
case class ZipRegistration(dirs: Seq[File], zipFileName: String, splitLength: Option[Long], exportDir: File, workingDir: File)

/**
 * Indicate a failure while zipping registrations
 * @param reason the failure
 * @param request the request
 */
case class ZipFileFailure(reason: Throwable, request: ZipRegistration)

/**
 * Actor to zip registration files
 */
class ZipFilesActor extends Actor with ActorLogging {

  override def preRestart(reason: Throwable, message: Option[Any]) {
    handleError(reason, message)
    super.preRestart(reason, message)
  }

  def receive = {
    case zip: ZipRegistration ⇒ {
      ZipExportFiles.processZip(zip.dirs, zip.zipFileName, zip.splitLength, zip.exportDir, zip.workingDir)
      log.info("Created %s".format(zip.zipFileName))
    }
  }

  /**
   * Log the error and try to send a failure to the sender.
   * @param reason the failure
   * @param message the requested message
   */
  private def handleError(reason: Throwable, message: Option[Any]) {
    if (message.isDefined) {
      message.get match {
        case zip: ZipRegistration ⇒ {
          log.error("Failed to create zipfile %s: %s".format(zip.zipFileName, reason.getMessage))
          sender ! ZipFileFailure(reason, zip)
        }
        case other ⇒ {
          log.error("Unknown message: %s failed: %s".format(message.getClass.getName, reason.getMessage))
        }
      }
    } else {
      log.error("Failed to process: %s".format(reason.getMessage))
    }
  }
}