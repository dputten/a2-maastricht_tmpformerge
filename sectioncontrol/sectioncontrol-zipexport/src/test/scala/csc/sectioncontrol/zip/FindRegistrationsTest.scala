/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.zip

import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers
import java.io.File
import org.apache.commons.io.FileUtils

class FindRegistrationsTest extends WordSpec with MustMatchers with BeforeAndAfterAll {
  val resourceDir = new File(getClass.getClassLoader.getResource("logback-test.xml").getPath).getParentFile
  val rootDir = new File(resourceDir, "reg")
  rootDir.mkdirs()

  override protected def afterAll() {
    super.afterAll()
    FileUtils.deleteQuietly(rootDir)
  }

  "FindRegistration.createFinishedSystems" must {
    "create list of finished registrations" in {
      val file1 = new File("a0120801")
      val file2 = new File("a0121001")
      val file3 = new File("a0131002")

      val prefixMap = Seq(("a012", "3210"), ("a0131002", "3215"))
      val systemIds = prefixMap.map(_._2).distinct

      val dirs = Set(file1, file2, file3)
      val now = System.currentTimeMillis()

      val findRegistrations = new FindRegistrations(currentDay = now, prefixMap = prefixMap, exportDirs = dirs)
      val registrations = findRegistrations.createFinishedSystems(systemIds, dirs)

      registrations.size must be(3)
      registrations.exists { case fin ⇒ fin.corId == "a0120801" && fin.dirs.contains(file1) } must be(true)
      registrations.exists { case fin ⇒ fin.corId == "a0121001" && fin.dirs.contains(file2) } must be(true)
      registrations.exists { case fin ⇒ fin.corId == "a0131002" && fin.dirs.contains(file3) } must be(true)
    }
    "create list with combined .XXX registrations" in {
      val file1 = new File("a0128001")
      val file2 = new File("a0128001.1")

      val prefixMap = Seq(("a012", "3210"), ("a0131002", "3215"))
      val systemIds = prefixMap.map(_._2).distinct

      val dirs = Set(file1, file2)
      val now = System.currentTimeMillis()

      val findRegistrations = new FindRegistrations(currentDay = now, prefixMap = prefixMap, exportDirs = dirs)
      val registrations = findRegistrations.createFinishedSystems(systemIds, dirs)

      registrations.size must be(1)
      registrations.exists { case fin ⇒ fin.corId == "a0128001" && fin.dirs.contains(file1) && fin.dirs.contains(file2) } must be(true)
    }

    "translate NoConfig directories" in {
      val file1 = new File("a01201NoConfig100") // violationprefix, corridor.info.id, NoConfig, speed

      val prefixMap = Seq(("a012", "3210"), ("a0131002", "3215"))
      val systemIds = prefixMap.map(_._2).distinct
      val dirs = Set(file1)
      val now = System.currentTimeMillis()

      val findRegistrations = new FindRegistrations(currentDay = now, prefixMap = prefixMap, exportDirs = dirs)
      val registrations = findRegistrations.createFinishedSystems(systemIds, dirs)
      registrations.size must be(1)

      registrations.exists { case fin ⇒ fin.corId == "a0120001" && fin.dirs.contains(file1) } must be(true)
    }
  }

  "FindRegistration" must {
    "create list of finished registrations" in {
      val resourceRegisterDir = new File(resourceDir, "registration")
      val sourceDir = new File(rootDir, "test1.in")
      sourceDir.mkdirs()
      FileUtils.copyDirectory(resourceRegisterDir, sourceDir)

      val sourceDir2 = new File(rootDir, "test1.in2")
      sourceDir2.mkdirs()
      val dir1 = new File(sourceDir2, "32150001")
      dir1.mkdirs()
      val dir2 = new File(sourceDir2, "32100002")
      dir2.mkdirs()
      val prefixMap = Seq(("3210", "sys1"), ("3215", "sys2"))
      val exportDirs = Set(sourceDir, sourceDir2)
      val now = System.currentTimeMillis()
      val testClass = new FindRegistrations(currentDay = now, prefixMap = prefixMap, exportDirs = exportDirs)

      def isNotReady(systemId: String, currentDay: Long): Boolean = { false }
      testClass.findFinishedSystems(isNotReady) must be(Seq())
      def isReady(systemId: String, currentDay: Long): Boolean = { true }
      val finishedList = testClass.findFinishedSystems(isReady)
      finishedList.size must be(3)
      var record1 = false
      var record2 = false
      var record3 = false
      finishedList.foreach(fin ⇒ fin.systemId match {
        case "sys1" ⇒
          fin.corId match {
            case "32100001" ⇒
              fin.dirs.size must be(2)
              fin.dirs.head.getName must (be("32100001.1") or be("32100001"))
              fin.dirs.last.getName must (be("32100001.1") or be("32100001"))
              record1 must be(false)
              record1 = true
            case "32100002" ⇒
              fin.dirs.size must be(1)
              fin.dirs.head.getName must be("32100002")
              record2 must be(false)
              record2 = true
            case other ⇒ fail("Unexpected corID %s".format(fin.corId))
          }
        case "sys2" ⇒
          fin.corId must be("32150001")
          fin.dirs.size must be(1)
          fin.dirs.head.getName must be("32150001")
          record3 must be(false)
          record3 = true
        case other ⇒ fail("Unexpected system %s".format(fin.systemId))
      })
    }
  }

}