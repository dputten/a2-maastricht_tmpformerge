/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.zip

import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers
import java.io.File
import org.apache.commons.io.FileUtils
import net.liftweb.json.DefaultFormats
import csc.akkautils.DirectLogging
import csc.sectioncontrol.storage._
import java.text.SimpleDateFormat
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.messages.{ EsaProviderType, VehicleImageType, VehicleCode, VehicleCategory }
import csc.sectioncontrol.storage.EnforcementStatus
import csc.sectioncontrol.storage.ZkExportZipConfig
import csc.sectioncontrol.enforce.nl.run.RegisterViolationsJobConfig
import csc.sectioncontrol.storage.ZkSystem
import csc.curator.CuratorTestServer
import csc.curator.utils.{ CuratorToolsImpl, Curator }

class ProcessExportsFilesTest extends WordSpec with MustMatchers with CuratorTestServer
  with DirectLogging with BeforeAndAfterAll {
  val resourceDir = new File(getClass.getClassLoader.getResource("logback-test.xml").getPath).getParentFile
  val rootDir = new File(resourceDir, "proc")
  rootDir.mkdirs()
  var curator: Curator = _

  val system1 = "sys1"
  val system1Prefix = "3210"
  val sourceIn1 = new File(rootDir, "test.in1")

  val system2 = "sys2"
  val system2Prefix = "3215"
  val sourceIn2 = new File(rootDir, "test.in2")

  val dayReport = new File(rootDir, "dagrapportages")

  val dateFormat = new SimpleDateFormat("ddMMyyyy HH:mm:ss.SSS")
  val daybefore_0100 = dateFormat.parse("01072013 01:00:00.000").getTime
  val date_0100 = dateFormat.parse("02072013 01:00:00.000").getTime
  val date_0600 = dateFormat.parse("02072013 06:00:00.000").getTime
  val date_2359 = dateFormat.parse("02072013 23:59:59.999").getTime

  val config = new ZkExportZipConfig(finalExportTime = 300, //5 hours in seconds
    pollFrequency = 300000, //5 minuten in msec
    nrWorkers = 20,
    splitSize = 10485760, //10MB
    workingDir = "/tmp/exportZip",
    ExportDir = "/app/politie")

  override def beforeAll() {
    super.beforeAll()
    createDirs()
  }

  override def beforeEach() {
    super.beforeEach()
    val formats = DefaultFormats + new EnumerationSerializer(
      VehicleCategory,
      ZkWheelbaseType,
      VehicleCode,
      ZkIndicationType,
      ServiceType,
      ZkCaseFileType,
      EsaProviderType,
      VehicleImageType,
      DayReportVersion,
      ConfigType)

    curator = new CuratorToolsImpl(clientScope, log, formats)

    fillZookeeper()

  }

  override def afterEach() {
    super.afterEach()
  }

  override def afterAll() {
    super.afterAll()
    FileUtils.deleteQuietly(rootDir)
  }

  "ProcessRegistrations" must {
    "do nothing when out of window" in {
      val testClass = new ProcessExportFiles(curator)
      testClass.doCheck(date_2359) must be(Seq())
    }
    "create systems" in {
      val processExportFiles = new ProcessExportFiles(curator)
      //system 1 is ready system2 is NOT

      val zipRegistrations = processExportFiles.doCheck(date_0100)
      zipRegistrations.size must be(2)

      val zipRegistrationHead = zipRegistrations.head
      zipRegistrationHead.exportDir.getAbsolutePath must be(config.ExportDir)
      zipRegistrationHead.workingDir.getAbsolutePath must be(config.workingDir)
      zipRegistrationHead.splitLength.get must be(config.splitSize)

      zipRegistrationHead.zipFileName must be("32100001_20130701.zip")
      zipRegistrationHead.dirs.size must be(2)
      zipRegistrationHead.dirs.head.getName must (be("32100001") or be("32100001.1"))
      zipRegistrationHead.dirs.last.getName must (be("32100001") or be("32100001.1"))

      val zipRegistrationLast = zipRegistrations.last
      zipRegistrationLast.exportDir.getAbsolutePath must be(config.ExportDir)
      zipRegistrationLast.workingDir.getAbsolutePath must be(config.workingDir)
      zipRegistrationLast.splitLength.get must be(config.splitSize)
      zipRegistrationLast.zipFileName must be("32100002_20130701.zip")
      zipRegistrationLast.dirs.size must be(1)
      zipRegistrationLast.dirs.head.getName must be("32100002")

      //check is unfinished functionality works
      val unfinishedRequests = processExportFiles.doCheck(date_0600)
      unfinishedRequests.size must be(4)
      val sys2Request = unfinishedRequests(2)
      sys2Request.exportDir.getAbsolutePath must be(config.ExportDir)
      sys2Request.workingDir.getAbsolutePath must be(config.workingDir)
      sys2Request.splitLength.get must be(config.splitSize)
      sys2Request.zipFileName must be("32150001_20130701.zip")
      sys2Request.dirs.size must be(1)
      sys2Request.dirs.head.getName must be("32150001")

      val sys2Request2 = unfinishedRequests(3)
      sys2Request2.exportDir.getAbsolutePath must be(config.ExportDir)
      sys2Request2.workingDir.getAbsolutePath must be(config.workingDir)
      sys2Request2.splitLength.get must be(config.splitSize)
      sys2Request2.zipFileName must be("dagrapportage_20130701.zip")
      sys2Request2.dirs.size must be(1)
      sys2Request2.dirs.head.getName must be("dagrapportages")
    }
  }

  def createDirs() {
    val resourceRegisterDir = new File(resourceDir, "registration")
    sourceIn1.mkdirs()
    FileUtils.copyDirectory(resourceRegisterDir, sourceIn1)

    sourceIn2.mkdirs()
    val dir1 = new File(sourceIn2, "32150001")
    dir1.mkdirs()
    val dir2 = new File(sourceIn2, "32100002")
    dir2.mkdirs()

    dayReport.mkdirs()
    new File(dayReport, "32100002_20130701.xml").createNewFile()
    new File(dayReport, "32100001_20130701.xml").createNewFile()
  }

  def fillZookeeper() {
    //create config
    val cfgPath = Paths.Configuration.getExportZipConfigPath
    curator.put(cfgPath, config)

    //create finished enforce Status
    val status = new EnforcementStatus(true, daybefore_0100, true, true)
    val pathStatus = Paths.Systems.getEnforceStatusPath(system1, daybefore_0100)
    curator.put(pathStatus, status)
    val pathStatus2 = Paths.Systems.getEnforceStatusPath(system2, daybefore_0100)
    curator.put(pathStatus2, status.copy(registrationComplete = false))

    //create exportDirs
    val regConfig = new RegisterViolationsJobConfig(exportDirectoryPath = sourceIn1.getAbsolutePath)
    val folderConfigPath = Paths.Systems.getRegisterViolationsJobConfigPath(system1)
    curator.put(folderConfigPath, regConfig)

    val regConfig2 = new RegisterViolationsJobConfig(exportDirectoryPath = sourceIn2.getAbsolutePath)
    val folderConfigPath2 = Paths.Systems.getRegisterViolationsJobConfigPath(system2)
    curator.put(folderConfigPath2, regConfig2)

    //create corridor prefix
    val sysCfg = new ZkSystem(id = system1,
      name = system1,
      title = system1,
      mtmRouteId = "mtmRoute",
      violationPrefixId = system1Prefix,
      location = ZkSystemLocation(
        description = "",
        region = "",
        viewingDirection = Some(""),
        roadNumber = "",
        roadPart = "",
        systemLocation = "Amsterdam"),
      serialNumber = SerialNumber(""),
      orderNumber = 1,
      maxSpeed = 250,
      compressionFactor = 80,
      retentionTimes = ZkSystemRetentionTimes(
        nrDaysKeepTrafficData = 1,
        nrDaysKeepViolations = 2,
        nrDaysRemindCaseFiles = 1),
      pardonTimes = ZkSystemPardonTimes(pardonTimeEsa_secs = 5000, pardonTimeTechnical_secs = 5000))
    val sysPath1 = Paths.Systems.getConfigPath(system1)
    curator.put(sysPath1, sysCfg)

    val sysCfg2 = sysCfg.copy(id = system2,
      name = system2,
      violationPrefixId = system2Prefix)
    val sysPath2 = Paths.Systems.getConfigPath(system2)
    curator.put(sysPath2, sysCfg2)

    //create DailyReport config
    val report = new ZkDayReportConfig(subject = "",
      to = "",
      dateFormat = "",
      properties = Map(),
      exportDirectory = Some(dayReport.getAbsolutePath))
    val reportPath = Paths.Configuration.getReportsConfigPath
    curator.put(reportPath, report)

  }
}