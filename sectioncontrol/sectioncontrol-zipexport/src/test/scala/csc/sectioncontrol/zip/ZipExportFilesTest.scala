/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.zip

import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers
import java.io.File
import org.apache.commons.io.FileUtils

class ZipExportFilesTest extends WordSpec with MustMatchers with BeforeAndAfterAll {
  val resourceDir = new File(getClass.getClassLoader.getResource("logback-test.xml").getPath).getParentFile
  val rootDir = new File(resourceDir, "zip")
  rootDir.mkdirs()

  override protected def afterAll() {
    super.afterAll()
    FileUtils.deleteQuietly(rootDir)
  }

  "createFileName" must {
    "create correct zipfile name when no basename" in {
      val zipPart = new File("321500001.zip")
      ZipExportFiles.createFileName(baseName = None, zipPart = zipPart) must be(zipPart.getName)
      val zipPart2 = new File("321500001.z01")
      ZipExportFiles.createFileName(baseName = None, zipPart = zipPart2) must be(zipPart2.getName)
    }
    "create correct zipfile name when using basename" in {
      val baseName = "321500001.1"
      val zipPart = new File("321500001.zip")
      ZipExportFiles.createFileName(baseName = Some(baseName), zipPart = zipPart) must be("321500001.1.zip")
      val zipPart2 = new File("321500001.z01")
      ZipExportFiles.createFileName(baseName = Some(baseName), zipPart = zipPart2) must be("321500001.1.z01")
    }
  }
  "createBaseName" must {
    "create correct baseName when no files exists" in {
      val exportDir = new File(rootDir, "test1")
      exportDir.mkdirs()
      ZipExportFiles.createBaseName(tmpZipFile = "321500001.zip", exportDir = exportDir) must be(None)
    }
    "create correct baseName when zip file exists" in {
      val exportDir = new File(rootDir, "test2")
      exportDir.mkdirs()
      val zipFileName = "321500001.zip"
      val zipfile = new File(exportDir, zipFileName)
      zipfile.createNewFile() must be(true)
      ZipExportFiles.createBaseName(tmpZipFile = zipFileName, exportDir = exportDir) must be(Some("321500001.1"))
    }
    "create correct baseName when multiple zip file exists" in {
      val exportDir = new File(rootDir, "test3")
      exportDir.mkdirs()
      val zipFileName = "321500001.zip"
      val zipfile = new File(exportDir, zipFileName)
      zipfile.createNewFile() must be(true)
      val zipfile2 = new File(exportDir, "321500001.1.zip")
      zipfile2.createNewFile() must be(true)
      val zipfile3 = new File(exportDir, "321500001.2.zip")
      zipfile3.createNewFile() must be(true)
      ZipExportFiles.createBaseName(tmpZipFile = zipFileName, exportDir = exportDir) must be(Some("321500001.3"))
    }
  }
  "moveZipFiles" must {
    "move all zip parts" in {
      val sourceDir = new File(rootDir, "test4.in")
      sourceDir.mkdirs()
      val exportDir = new File(rootDir, "test4.out")
      exportDir.mkdirs()
      val zipFileName = "321500001.zip"
      val zipfile = new File(sourceDir, zipFileName)
      zipfile.createNewFile() must be(true)
      val zipfile2 = new File(sourceDir, "321500001.z01")
      zipfile2.createNewFile() must be(true)
      val zipfile3 = new File(sourceDir, "321500001.z02")
      zipfile3.createNewFile() must be(true)
      val zipfile4 = new File(sourceDir, "321500001.1.zip")
      zipfile4.createNewFile() must be(true)

      ZipExportFiles.moveZipFiles(tmpZipFile = zipfile, exportDir = exportDir)
      val srcList = sourceDir.listFiles()
      srcList.length must be(1)
      srcList(0) must be(zipfile4)

      val expList = exportDir.listFiles()
      expList.length must be(3)
      val names = expList.map(_.getName())
      names.contains(zipfile.getName) must be(true)
      names.contains(zipfile2.getName) must be(true)
      names.contains(zipfile3.getName) must be(true)
    }
  }
  "processZip" must {
    "createZip in ExportDir when using directories" in {
      val resourceRegisterDir = new File(resourceDir, "registration")
      val sourceDir = new File(rootDir, "test5.in")
      sourceDir.mkdirs()
      FileUtils.copyDirectory(resourceRegisterDir, sourceDir)
      val workingDir = new File(rootDir, "test5.work")
      val exportDir = new File(rootDir, "test5.out")

      sourceDir.exists() must be(true)
      ZipExportFiles.processZip(
        dirs = sourceDir.listFiles(),
        zipFileName = "32100001.zip",
        splitLength = Some(66000),
        exportDir = exportDir,
        workingDir = workingDir)

      val list = exportDir.listFiles()
      list.size must be(1)
      list(0).getName must be("32100001.zip")

    }
    "createZip in ExportDir when using files" in {
      val sourceDir = new File(rootDir, "test6.in")
      sourceDir.mkdirs()
      new File(sourceDir, "file1.xml").createNewFile()
      new File(sourceDir, "file2.xml").createNewFile()
      new File(sourceDir, "file3.xml").createNewFile()
      val workingDir = new File(rootDir, "test6.work")
      val exportDir = new File(rootDir, "test6.out")

      sourceDir.exists() must be(true)
      ZipExportFiles.processZip(
        dirs = sourceDir.listFiles(),
        zipFileName = "dagrapport.zip",
        splitLength = Some(66000),
        exportDir = exportDir,
        workingDir = workingDir)

      val list = exportDir.listFiles()
      list.size must be(1)
      list(0).getName must be("dagrapport.zip")

    }
  }

}