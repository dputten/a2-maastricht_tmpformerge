package csc.rabbitmq

import java.util.concurrent.TimeUnit

import akka.actor.{ ActorRef, ActorSystem }
import com.github.sstone.amqp.Amqp._
import com.github.sstone.amqp.RabbitMQConnection
import csc.akkautils.DirectLogging
import csc.amqp.AmqpConnectionConfig

/**
 * RabbitMQ utilities. Can be used to start producers and consumers.
 */
object RabbitMQ extends DirectLogging {

  /**
   * Connection timeout in seconds
   */
  val CONNECTION_TIMEOUT = 5

  /**
   * Cached connection
   */
  var connection: Option[RabbitMQConnection] = None

  /**
   * Starts a RabbitMQ consumer
   *
   * @param config      the amqp connection configuration
   * @param queueName   the queue the consumers listens on
   * @param listener    actor that does the listening
   * @param actorSystem needed to start the listening actor
   *
   * @return the RabbitMQ consumer
   */
  def startConsumer(config: AmqpConnectionConfig, queueName: String, listener: ActorRef)(implicit actorSystem: ActorSystem) = {
    log.debug("Trying to start RabbitMQ consumer...")
    val conn = createRabbitMQConnection(config)

    val consumer = conn.createSimpleConsumer(queueName, listener, Some(ChannelParameters(100)), false)

    waitForConnection(actorSystem, consumer).await(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
    log.debug("RabbitMQ consumer started.")

    consumer
  }

  /**
   * Starts a RabbitMQ producer
   *
   * @param config      the amqp connection configuration
   * @param actorSystem required by RabbitMQ to start the channel owner
   * @return the RabbitMQ producer
   */
  def startProducer(config: AmqpConnectionConfig)(implicit actorSystem: ActorSystem): ActorRef = {
    log.debug("Trying to start RabbitMQ producer...")
    val conn = createRabbitMQConnection(config)

    // Create producer and wait until it's connected to the broker
    val producer = conn.createChannelOwner()

    waitForConnection(actorSystem, producer).await(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
    log.debug("RabbitMQ producer started.")

    producer
  }

  /**
   * Creates a RabbitMQ connection
   *
   * @param config         the amqp configuration
   * @param system         (implicit) actor system required by RabbitMQ
   * @return               a RabbitMQ connection
   */
  private def createRabbitMQConnection(config: AmqpConnectionConfig)(implicit system: ActorSystem) = {
    connection.getOrElse {
      log.debug("Creating new connection using AMQP connection configuration {}", config)

      val conn = RabbitMQConnection.create(config.actorName, config.amqpUri, config.reconnectDelay)
      require(conn != null)

      log.debug("Connection to RabbitMQ established.")
      connection = Some(conn)

      conn
    }
  }

}
