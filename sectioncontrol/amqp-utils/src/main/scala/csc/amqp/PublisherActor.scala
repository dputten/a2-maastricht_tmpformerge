/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.amqp

import akka.actor._
import akka.pattern.ask
import akka.util.Timeout
import com.github.sstone.amqp.Amqp.{ Ok, Publish }
import com.rabbitmq.client.AMQP
import akka.event.LoggingReceive

/**
 * Base actor class for sending AMQP messages with logging. It has two helper functions which can be used to send
 * a message through the `producer` passed in the ''constructor''. Using [[akka.pattern.ask]] it will wait for the
 * outcome of message publishing and log error if any
 *
 * @author csomogyi
 */
abstract class PublisherActor extends Actor with ActorLogging with AmqpSerializers {
  val producerTimeout = Timeout(5000) // 5 seconds should be enough to the producer

  val producer: ActorRef
  val producerEndpoint: String
  def producerRouteKey(msg: AnyRef): Option[String]
  def messageToSend(msg: AnyRef): Option[AnyRef] = Some(msg)

  def receive: Receive = LoggingReceive {
    case msg: AnyRef ⇒
      val routeKey = producerRouteKey(msg)
      val responseOpt = messageToSend(msg)
      if (routeKey.isDefined && responseOpt.isDefined) {
        val response = responseOpt.get
        val payload = toPublishPayload[response.type](response.asInstanceOf[response.type])
        val responseName = response.getClass.getName
        publishOrError(producerEndpoint, routeKey.get, payload, responseName)
        log.debug("%s send".format(response.getClass.getName))
      }
  }

  /**
   * Publishes a message via the producer
   *
   * @param producerEndpoint the endpoint / exchange parameter of the message publishing
   * @param producerRouteKey the routing key parameter of the message publishing
   * @param body the body part of the message to be published
   * @param properties the properties part of the message to be published
   */
  def publish(producerEndpoint: String,
              producerRouteKey: String,
              body: Array[Byte],
              properties: Option[AMQP.BasicProperties] = None): Unit = {

    val publish = Publish(producerEndpoint, producerRouteKey, body, properties)
    implicit val timeout = producerTimeout
    producer ? publish onComplete {
      case Right(response) ⇒ {
        response match {
          case Ok(_, _) ⇒ log.debug("Message sent successfully: {}", publish)
          case msg      ⇒ log.error("Message not published: {}", msg)
        }
      }
      case Left(e) ⇒ {
        log.error(e, "Failed to publish message - producer timeout")
      }
    }
  }

  /**
   * Expects a `toPublishPayload()` result from the serializer which can be Either payload or error String.
   * Upon error it logs an error with the message name specified in responseName field
   *
   * @param producerEndpoint the endpoint / exchange parameter of the message publishing
   * @param producerRouteKey the route key parameter of the message publishing
   * @param payload the payload to be sent or log error upon error
   * @param responseName the name of the response to be logged upon error
   */
  def publishOrError(producerEndpoint: String,
                     producerRouteKey: String,
                     payload: AmqpSerializers.PayloadType,
                     responseName: String): Unit = {
    payload match {
      case Right((body, properties)) ⇒ publish(producerEndpoint, producerRouteKey, body, Some(properties))
      case Left(error)               ⇒ log.error("Cannot send {} / serialization error - {}", responseName, error)
    }
  }
}
