package csc.amqp.test

import java.util.Date
import java.util.concurrent.TimeUnit

import akka.actor.{ Actor, ActorRef }
import akka.pattern.ask
import akka.util.Timeout
import com.github.sstone.amqp.Amqp.{ Delivery, Ack, Reject, Publish }
import com.rabbitmq.client.{ AMQP, Envelope }
import csc.amqp.JsonSerializers
import net.liftweb.json.Formats

/**
 * This utility actor behaves as a client to any actor (the target) receiving AMQP deliveries and mocks the whole queue and wiring.
 * To use it, just use the ask pattern on this actor sending the messages you want wrapped in deliveries to the target actor.
 * jsonFormats must be able to convert all messages to json
 * The response to ask is an Outcome, which can be Acked, Rejected or Failure, depending if the target actor sent an Ack,
 * a Reject or something failed in between.
 *
 * Created by carlos on 16.10.15.
 */
class AmqpClientActor(target: ActorRef, jsonFormats: Formats) extends Actor with JsonSerializers {

  val encoding = "UTF-8"

  implicit val timeout: Timeout = Timeout(1, TimeUnit.SECONDS)

  override implicit val formats = jsonFormats

  override protected def receive: Receive = receiveWithState(State(0))

  def receiveWithState(state: State): Receive = {
    case msg: Publish ⇒ //TODO csilva: implement the whole response logic
    case msg: AnyRef ⇒
      val tag = state.lastTag + 1
      handleMessage(msg, tag)
      context.become(receiveWithState(State(tag)))
  }

  def handleMessage(msg: AnyRef, tag: Long): Unit = {
    val client = sender

    createDelivery(msg, tag) match {
      case Right(delivery) ⇒ {
        val future = target ? delivery
        future onSuccess {
          case msg: Ack    ⇒ client ! Acked
          case msg: Reject ⇒ client ! Rejected
        } onFailure {
          case error: Throwable ⇒ client ! Failure(error.getMessage)
        }
      }
      case Left(error) ⇒ client ! Failure(error)
    }
  }

  def createDelivery(msg: AnyRef, tag: Long): Either[String, Delivery] = {
    try {
      val envelope: Envelope = new Envelope(tag, false, "exchange", "routingKey")
      val properties = buildProperties(msg.getClass.getSimpleName).build()
      toJson(msg) match {
        case Right(json) ⇒ Right(Delivery(tag.toString, envelope, properties, json.getBytes(encoding)))
        case Left(error) ⇒ Left(error)
      }

    } catch {
      case ex: Throwable ⇒ Left(ex.getMessage)
    }
  }

  def buildProperties(messageType: String): AMQP.BasicProperties.Builder = {
    new AMQP.BasicProperties.Builder().appId("applicationId")
      .contentType("application/json").contentEncoding(encoding)
      .timestamp(new Date()).`type`(messageType)
  }
}

case class State(lastTag: Long)

trait Outcome
case class Failure(error: String) extends Outcome
object Acked extends Outcome
object Rejected extends Outcome
