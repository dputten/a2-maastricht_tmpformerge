package csc.amqp

import akka.actor.{ Actor, ActorLogging, ActorRef }
import com.github.sstone.amqp.Amqp.{ Ok, Ack, Delivery }
import csc.amqp.AmqpSerializers.Deserializer

/**
 * Created by carlos on 11.08.15.
 */
abstract class GenericConsumer(handler: ActorRef) extends Actor with ActorLogging with AmqpSerializers {

  def deserializers: Map[String, Deserializer]

  override protected def receive: Receive = {
    case delivery: Delivery ⇒ handleDelivery(delivery)
    case _: Ok              ⇒ //silent OK handling
    case other              ⇒ log.warning("Unhandled message: {}", other)
  }

  def handleDelivery(delivery: Delivery): Unit = {
    log.debug("Received Delivery")
    val consumer = sender
    val msgType = extractMessageType(delivery)
    var message: Option[AnyRef] = None
    var error: Option[String] = None

    msgType match {
      case Some(tp) ⇒ deserializers.get(tp) match {
        case Some(deserializer) ⇒ deserializer(delivery) match {
          case Right(msg) ⇒ message = Some(msg)
          case Left(msg)  ⇒ error = Some(msg)
        }
        case None ⇒ error = Some("Don't know how to deserialize message " + msgType)
      }
      case None ⇒ error = Some("Don't know type of message")
    }

    message map { m ⇒
      log.debug("message extracted: {}", m)
      dispatch(m) //dispatch the message
    }

    error.map {
      //log the error
      log.error("Cannot decode message {} with tag '{}' - {}", msgType, delivery.envelope.getDeliveryTag, _)
    }

    consumer ! Ack(delivery.envelope.getDeliveryTag) //notify the sender
  }

  /**
   * Dispatches the message. Default implementation sends the message to the handler.
   * Override if add specific behaviour.
   * @param message message to be dispatched
   */
  def dispatch(message: AnyRef) = handler ! message

}

