/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.amqp

import java.nio.charset.Charset
import java.util.Date

import com.github.sstone.amqp.Amqp.Delivery
import com.rabbitmq.client.AMQP
import org.slf4j.LoggerFactory
import akka.event.LoggingAdapter
import java.util
import scala.collection.JavaConversions._

/**
 * Trait for serializing (`toPublishPayload`) and deserializing (`toMessage`) AMQP messages. [[csc.amqp.AmqpSerializers.ApplicationId]]
 * must be overridden. It's used to specify the app-id of the outgoing message.
 *
 * The message format:
 * - '''content-type''' is or should be set to `application/json`
 * - the '''content-encoding''' is `UTF-8` by default but it can be over-defined by overriding [[csc.amqp.AmqpSerializers.DefaultEncoding]]
 *   otherwise '''content-encoding''' field is respected during deserialization
 * - '''application id''' is set to [[csc.amqp.AmqpSerializers.ApplicationId]]
 * - '''timestamp''' is set to the current time on sending
 * - '''type''' must be the simple name of the class being serialized
 * - the '''payload / body''' is a string containing the JSON serialization of the class object; the string is encoded into
 *   byte array using the ''content-encoding''
 *
 * @author csomogyi
 */
trait AmqpSerializers extends JsonSerializers with GZIPCompressor {
  val log: LoggingAdapter
  val ApplicationId: String

  val DefaultEncodingStr = "gzip"
  val DefaultMimeTypeStr = "application/json"
  val DefaultCharsetStr = "UTF-8"
  val DefaultContentTypeStr = DefaultMimeTypeStr + "; charset=" + DefaultCharsetStr

  import AmqpSerializers.PayloadType

  /**
   * Creates a publish message payload from the specified type or error message upon failure of serialization
   *
   * @param response
   * @tparam T
   * @return
   */
  def toPublishPayload[T <: AnyRef: Manifest](response: T): PayloadType = {
    val builder = buildProperties(manifest[T].erasure.getSimpleName)
    toJson[T](response) match {
      case Left(error) ⇒ Left(error)
      // do compression here
      case Right(json) ⇒
        val jsonBytes = json.getBytes(DefaultCharsetStr)
        val compressedBytes = compress(jsonBytes)
        log.debug("gzip compressed %d bytes to %d bytes (%.2f)".format(jsonBytes.size, compressedBytes.size,
          compressedBytes.size / jsonBytes.size.toFloat))
        Right(compressedBytes, builder.build())
    }
  }

  /**
   * Deserializes AMQP Delivery into message or returns error message upon failure
   *
   * @param delivery the AMQP delivery
   * @tparam T expected type of the message
   * @return
   */
  def toMessage[T <: AnyRef: Manifest](delivery: Delivery): Either[String, T] = {
    val actualMessageType = extractMessageType(delivery)
    val expectedMessageType = Some(manifest[T].erasure.getSimpleName)
    val (someMimeType, someCharsetStr) = extractMimetypeAndCharset(delivery)
    val someCharset = makeCharset(someCharsetStr)
    val someEncoding = extractEncoding(delivery)

    if (actualMessageType != expectedMessageType)
      Left("Invalid message type. Expected: '%s', actual: '%s'".format(expectedMessageType, actualMessageType))
    else if (someMimeType != Some(DefaultMimeTypeStr))
      Left("Invalid mime type. Expected '%s', actual: '%s'".format(DefaultMimeTypeStr, someMimeType))
    else if (someCharset == None)
      Left("Invalid character set: '%s'".format(someCharsetStr))
    else if (someEncoding != None && someEncoding != Some(DefaultEncodingStr))
      Left("Invalid content-encoding. Expected '%s' or None, actual: '%s'".format(Some(DefaultEncodingStr), someEncoding))
    else {
      val body = if (someEncoding == Some(DefaultEncodingStr)) decompress(delivery.body) else delivery.body
      fromJson[T](new String(body, someCharset.get))
    }
  }

  /**
   * Abstract class used to create the message extractors
   * @param t manifest of generic class
   * @tparam T tne generic class
   */
  abstract class MessageExtractor[T <: AnyRef](implicit t: reflect.Manifest[T]) {
    def unapply(delivery: Delivery): Option[T] = {
      val className = t.erasure.getSimpleName
      delivery.properties.getType match {
        case `className` ⇒ {
          toMessage[T](delivery) match {
            case Right(message) ⇒ Some(message)
            case Left(error) ⇒ {
              log.error("Cannot decode request ({}): {}", delivery.envelope.getDeliveryTag, error)
              None
            }
          }
        }
        case other ⇒ None
      }
    }
  }

  /**
   * Builds standard properties. ApplicationId will be assigned as application ID and Content-Type and Encoding is set
   * to "application/json" and DefaultEncoding respectively.
   *
   * @param messageType
   * @return
   */
  protected def buildProperties(messageType: String): AMQP.BasicProperties.Builder = {
    new AMQP.BasicProperties.Builder().appId(ApplicationId)
      .contentType(DefaultContentTypeStr)
      .contentEncoding(DefaultEncodingStr)
      .timestamp(new Date()).`type`(messageType)
  }

  protected def stringAsOption(value: String) = if (value != null && !value.isEmpty) Some(value) else None
  protected def safeString(value: String) = if (value != null && !value.isEmpty) value else ""

  /**
   * Extract the mimetype and the charset from the given delivery
   * @param delivery the given AMQP delivery
   * @return tuple (Option[String], Option[String]) (Option[MimetypeStr], Option[charsetStr])
   */
  protected def extractMimetypeAndCharset(delivery: Delivery): (Option[String], Option[String]) = {
    val properties = delivery.properties
    val contentEncoding = safeString(properties.getContentEncoding)
    val contentType = safeString(properties.getContentType)

    // TODO allow for empty contentEncoding as per rfc-2616
    if (contentEncoding == DefaultEncodingStr) { // "gzip"
      // new stuff conforms to rfc2616 section 3.5 and ALWAYS uses "gzip" as content-encoding

      val MimetypeAndCharsetPattern = "([^;]+)\\s*;\\s*charset=(.*)".r // extract mimetype and charset
      val MimetypePattern = "([^;]*)".r // extract mimetype. 1 or more characters excluding semicolon

      contentType match {
        case MimetypeAndCharsetPattern(mimeTypeStr1, charsetStr1) ⇒ (Some(mimeTypeStr1), Some(charsetStr1))
        case MimetypePattern(mimeTypeStr2) ⇒ (Some(mimeTypeStr2), None)
        case other ⇒ (None, None)
      }
    } else {
      // TODO 20151204 JWL: remove because this abuses content-encoding to store charset
      (stringAsOption(contentType), stringAsOption(contentEncoding))
    }
  }

  protected def extractEncoding(delivery: Delivery): Option[String] = {
    // In previous versions content-encoding was abused to hold the character set. According to
    // rfc2616 the character set is part of the content-type as in "application/json; charset=UTF-8"
    // There currently are 3 possible values for for content-encoding: empty, "gzip" or the character set
    // Note that we only allow gzip encoding
    val contentEncoding = safeString(delivery.properties.getContentEncoding)
    if (contentEncoding == DefaultEncodingStr) Some(DefaultEncodingStr) else None
  }

  protected def extractMessageType(delivery: Delivery): Option[String] = {
    stringAsOption(delivery.properties.getType)
  }

  protected def makeCharset(someCharsetStr: Option[String]): Option[Charset] = {
    val charsetStr = someCharsetStr.getOrElse(DefaultCharsetStr)
    try {
      Some(Charset.forName(charsetStr))
    } catch {
      case e: Exception ⇒ None
    }
  }

}

object AmqpSerializers {
  type PayloadType = Either[String, (Array[Byte], AMQP.BasicProperties)]
  type Deserializer = Delivery ⇒ Either[String, AnyRef] //matches any call to toMessage
}