/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.amqp

import net.liftweb.json.{ Formats, Serialization }

/**
 * Generic JSON serializer / deserializer using Lifweb JSON library.
 *
 * [[JsonSerializers.formats]] must be overridden when using this trait.
 *
 * @author csomogyi
 */
trait JsonSerializers {
  implicit val formats: Formats

  /**
   * Converts to JSON string or returns serialization error message
   *
   * @param message
   * @tparam T
   * @return
   */
  def toJson[T <: AnyRef: Manifest](message: T): Either[String, String] = {
    try {
      Right(Serialization.write[T](message))
    } catch {
      case e: Exception ⇒ Left("Error during JSON serialization - " + e.getMessage)
    }
  }

  /**
   * Converts to message from JSON payload or returns deserialization error message
   *
   * @param payload
   * @tparam T
   * @return
   */
  def fromJson[T <: AnyRef: Manifest](payload: String): Either[String, T] = {
    try {
      Right(Serialization.read[T](payload))
    } catch {
      case e: Exception ⇒ Left("Error during JSON de-serialization - " + e.getMessage)
    }
  }
}
