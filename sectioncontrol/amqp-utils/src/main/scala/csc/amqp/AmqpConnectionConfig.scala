package csc.amqp

import akka.util.Duration
import akka.util.duration._
import com.typesafe.config.Config

case class AmqpConnectionConfig(actorName: String, amqpUri: String, reconnectDelay: Duration)

object AmqpConnectionConfig {

  def apply(configPath: String, config: Config) = new AmqpConnectionConfig(
    config.getString(configPath + ".actorName"),
    config.getString(configPath + ".amqpUri"),
    config.getInt(configPath + ".reconnectDelay_ms") millis)

}
