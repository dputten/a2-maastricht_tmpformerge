package csc.amqp

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 12/5/15.
 */

class GZIPCompressorTest extends GZIPCompressor with WordSpec with MustMatchers {
  "GZipCompressor" must {

    "properly zip and unzip messages" in {
      val testString = "this is a test using the gzip algorithm. It zips and unzips a message"
      val compressed = compress(testString.getBytes())
      val uncompressed = decompress(compressed)
      val result = new String(uncompressed)
      result must be(testString)
    }
  }
}
