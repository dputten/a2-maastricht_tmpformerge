package csc.amqp

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import com.rabbitmq.client.Envelope
import com.github.sstone.amqp.Amqp.Delivery
import net.liftweb.json.DefaultFormats
import csc.akkautils.DirectLoggingAdapter

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 12/5/15.
 */

case class SerializerTest(string1: String, string2: String, int1: Int, int2: Int)

class AmqpSerializersTest extends AmqpSerializers with WordSpec with MustMatchers {
  val ApplicationId = "AmqpSerializeTest"
  val formats = DefaultFormats
  val log = new DirectLoggingAdapter(this.getClass.getName)

  "AmqpSerializers" must {

    "properly serialize and deserialize messages" in {
      // Make a test message and serialize it
      val original = SerializerTest("aap", "noot", 11, 13)
      val payload = toPublishPayload[SerializerTest](original)

      // assert serialization was successful
      payload.isRight must be(true)
      val (buffer, amqpProperties) = payload.right.get
      amqpProperties.getContentType must be(DefaultContentTypeStr)
      amqpProperties.getContentEncoding must be(DefaultEncodingStr)

      // Now deserialize the result
      val envelope = new Envelope(1, true, "exchange", "routing.key")
      val builder = buildProperties(manifest[SerializerTest].erasure.getSimpleName)
      val delivery = Delivery("consumerTag", envelope, builder.build(), buffer)
      val result = toMessage[SerializerTest](delivery)

      // assert de-serialization was successful
      result.isRight must be(true)
      result.right.get must be(original)
    }
  }
}
