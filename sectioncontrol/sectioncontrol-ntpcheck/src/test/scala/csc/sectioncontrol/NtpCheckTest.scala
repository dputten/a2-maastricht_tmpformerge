package csc.sectioncontrol

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import collection.mutable.ListBuffer
import java.util.Date

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
class NtpCheckTest extends WordSpec with MustMatchers {
  val ntp = new NtpCheck("", 4, 2)
  val output = new ListBuffer[String]
  val now = new Date()
  "NtpCheck checkOutput" must {
    "validate correct input" in {
      val output = Seq(
        "     remote           refid      st t when poll reach   delay   offset  jitter",
        "==============================================================================",
        "*10.109.22.14    .PPSa.           1 u  511 1024  377    0.197   -0.623   0.213")

      ntp.checkOutput(now, output, 2) must be(Seq[String]())
    }
    "validate correct input with newlines" in {
      val output = Seq(
        "     remote           refid      st t when poll reach   delay   offset  jitter\n",
        "==============================================================================\n",
        "*10.109.22.14    .PPSa.           1 u  511 1024  377    0.197   -0.623   0.213\n")
      ntp.checkOutput(now, output, 2) must be(Seq[String]())
    }
    "validate multiple input" in {
      val output = Seq(
        "     remote           refid      st t when poll reach   delay   offset  jitter",
        "==============================================================================",
        "*10.109.22.14    .PPSa.           1 u  511 1024  377    0.197   -0.623   0.213",
        "+10.109.22.15    .PPSa.           1 u  511 1024  377    0.197   -0.623   0.213")
      ntp.checkOutput(now, output, 2) must be(Seq[String]())
    }
    "validate multiple input syn problem" in {
      val output = Seq(
        "     remote           refid      st t when poll reach   delay   offset  jitter",
        "==============================================================================",
        "*10.109.22.14    .PPSa.           1 u  511 1024  360    0.197   -0.623   0.213",
        "+10.109.22.15    .PPSa.           1 u  511 1024  377    0.197   -0.623   0.213")
      val err = ntp.checkOutput(now, output, 2)
      err must have size (1)
      val event = err.head
      event.componentId must be("NTP-10.109.22.14")
      event.timestamp must be(now.getTime)
      event.reason must startWith("Synchronisatie faalt")
    }
    "validate multiple input stratum problem" in {
      val output = Seq(
        "     remote           refid      st t when poll reach   delay   offset  jitter",
        "==============================================================================",
        "*10.109.22.14    .PPSa.           6 u  511 1024  377    0.197   -0.623   0.213",
        "+10.109.22.15    .PPSa.           1 u  511 1024  377    0.197   -0.623   0.213")
      val err = ntp.checkOutput(now, output, 2)
      err must have size (1)
      val event = err.head
      event.componentId must be("NTP-10.109.22.14")
      event.timestamp must be(now.getTime)
      event.reason must startWith("Stratum=")
    }
    "validate Multiple errors" in {
      val output = Seq(
        "     remote           refid      st t when poll reach   delay   offset  jitter",
        "==============================================================================",
        "*LOCAL    .PPSa.                  12 u  511 1024  377    0.197   -0.623   0.213",
        "+10.109.22.15    .PPSa.           1 u  511 1024  360    0.197   -0.623   0.213")
      val err = ntp.checkOutput(now, output, 2)
      err must have size (2)
      val event = err.head
      event.componentId must be("NTP-LOCAL")
      event.timestamp must be(now.getTime)
      event.reason must startWith("Stratum=")
      val event2 = err.last
      event2.componentId must be("NTP-10.109.22.15")
      event2.timestamp must be(now.getTime)
      event2.reason must startWith("Synchronisatie faalt")
    }
    "validate Init" in {
      val output = Seq(
        "     remote           refid      st t when poll reach   delay   offset  jitter",
        "==============================================================================",
        " 10.4.0.154      .INIT.          16 u    - 1024    0    0.000    0.000   0.000",
        " 10.4.0.155      .INIT.          16 u    - 1024    0    0.000    0.000   0.000")
      val err = ntp.checkOutput(now, output, 2)
      err must have size (2)
      val event = err.head
      event.componentId must be("NTP-10.4.0.154")
      event.timestamp must be(now.getTime)
      event.reason must startWith("Synchronisatie faalt")
      val event2 = err.last
      event2.componentId must be("NTP-10.4.0.155")
      event2.timestamp must be(now.getTime)
      event2.reason must startWith("Synchronisatie faalt")
    }
    "validate Production sync failure" in {
      val output = Seq(
        "     remote           refid      st t when poll reach   delay   offset  jitter",
        "==============================================================================",
        "*10.109.22.14    .PPSa.           1 u  91m 1024  340    0.210   -0.648   0.641")
      val err = ntp.checkOutput(now, output, 2)
      err must have size (1)
      val event = err.head
      event.componentId must be("NTP-10.109.22.14")
      event.timestamp must be(now.getTime)
      event.reason must startWith("Synchronisatie faalt")
    }
  }
  "Calulate Mask" must {
    "handle 0 failures" in {
      ntp.calcReachMask(0) must be(0)
    }
    "handle 4 failures" in {
      ntp.calcReachMask(4) must be(15)
    }
    "handle 1 failures" in {
      ntp.calcReachMask(1) must be(1)
    }

  }

  // Selftest to check if the inner or outer exception boils up.
  "expect exeptions" must {
    "createOuterException must produce IndexOutOfBoundsException" in {
      evaluating { createOuterException() } must produce[IndexOutOfBoundsException]
    }

    "createInnerException must produce IllegalArgumentException" in {
      evaluating { createInnerException() } must produce[IllegalArgumentException]
    }

    "createInnerAndOuterException must produce IllegalArgumentException" in {
      evaluating { createInnerAndOuterException() } must produce[IllegalArgumentException]
    }

    "the order of closing and destroy of RuntimeGetRuntimeExecDestroy should not matter (first close)" in {
      RuntimeGetRuntimeExecDestroyFirstClose
    }

    "the order of closing and destroy of RuntimeGetRuntimeExecDestroy should not matter (first destroy)" in {
      RuntimeGetRuntimeExecDestroyFirstDestroy
    }

    "exception is thrown" in {
      evaluating { JustToTestFinally() } must produce[IllegalArgumentException]

    }
  }

  def RuntimeGetRuntimeExecDestroyFirstClose {
    var process: Process = null
    process = Runtime.getRuntime.exec("ls")

    process.getOutputStream.close()
    process.getErrorStream.close()
    process.getOutputStream.close()

    process.destroy()

  }

  def RuntimeGetRuntimeExecDestroyFirstDestroy {
    var process: Process = null
    process = Runtime.getRuntime.exec("ls")

    process.destroy()

    process.getOutputStream.close()
    process.getErrorStream.close()
    process.getOutputStream.close()
  }

  def createInnerAndOuterException() {
    var myVar: String = null
    var myVar1: String = null
    var myVar2: String = null

    try {
      myVar = "a"
      throw new IndexOutOfBoundsException()
    } catch {
      case e: Exception ⇒
        throw e
    } finally {
      if (myVar != null) {
        var exception: Exception = null

        try { throw new IllegalArgumentException() }
        catch {
          case e: Exception ⇒
            myVar1 = "b"
            exception = e
          // throwing the IndexOutOfBoundsException will mess up the whole try/catch/finally
          // finally is for cleaning all recources so all should be done
          //throw new IndexOutOfBoundsException
        }

        myVar2 = myVar + myVar1
        myVar2 must be("ab")

        if (exception != null)
          throw exception
      }
    }

  }

  def createOuterException() {
    var myVar: String = null
    var myVar1: String = null
    var myVar2: String = null

    try {
      myVar = "a"
      throw new IndexOutOfBoundsException()
    } catch {
      case e: Exception ⇒
        throw e
    } finally {
      if (myVar != null) {
        var exception: Exception = null

        try { myVar1 = "b" }
        catch {
          case e: Exception ⇒
            exception = e
        }

        myVar2 = myVar + myVar1
        myVar2 must be("ab")

        if (exception != null)
          throw exception
      }
    }

  }

  def createInnerException() {
    var myVar: String = null
    var myVar1: String = null
    var myVar2: String = null

    try {
      myVar = "a"
    } catch {
      case e: Exception ⇒
        throw e
    } finally {
      if (myVar != null) {
        var exception: Exception = null

        try { throw new IllegalArgumentException() }
        catch {
          case e: Exception ⇒
            myVar1 = "b"
            exception = e
        }

        myVar2 = myVar + myVar1
        myVar2 must be("ab")

        if (exception != null)
          throw exception
      }
    }
  }

  def JustToTestFinally() {
    var myVar: String = null

    try {
      myVar = "a"
      throw new IllegalArgumentException
    } catch {
      case e: Exception ⇒
        throw e
    } finally {
      myVar = "ab"
      myVar must be("ab")
    }
  }
}