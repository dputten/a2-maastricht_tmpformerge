package csc.sectioncontrol

import collection.mutable.ListBuffer

import java.util.Date

import akka.util.Duration
import akka.actor.{ Cancellable, ActorLogging, Actor }

import messages.SystemEvent
import csc.sectioncontrol.storage.Paths
import csc.config.Path
import csc.curator.utils.Curator

case class Timer()
/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
/**
 * Check every timer for the ntp synchronisation
 * But only send once an hour the found SystemsEvents to prevent the logs flooding of the same message
 * @param ntpqPath
 * @param maxNrFailures
 * @param maxStratum
 * @param timer
 */
class CheckNTPActor(curator: Curator, ntpqPath: String, maxNrFailures: Int, maxStratum: Int, timer: Duration, reportWindow: Duration)
  extends Actor
  with ActorLogging {

  log.info("NTP check Actor Starting %s".format(self.path))
  var schedule: Option[Cancellable] = None
  val ntpCheck = new NtpCheck(ntpqPath, maxNrFailures, maxStratum)
  var eventsSent = new ListBuffer[SystemEvent]()

  override def preStart() {
    schedule = Some(context.system.scheduler.schedule(timer, timer, self, new Timer()))
  }

  override def postStop() {
    schedule.foreach(_.cancel())
  }

  def receive = {
    case t: Timer ⇒ {
      val errors = ntpCheck.check(new Date())
      errors.foreach(event ⇒ {
        eventsSent.find(ev ⇒
          ev.eventType == event.eventType &&
            ev.componentId == event.componentId) match {
          case Some(oldEvent) ⇒ {
            if (oldEvent.timestamp + reportWindow.toMillis < event.timestamp) {
              //remove old event and add new
              eventsSent -= oldEvent
              eventsSent += event
              sendEventToAllZookeeperNodes(event)
            } else {
              log.info("Ignore event %s".format(event))
            }
          }
          case None ⇒ {
            eventsSent += event
            sendEventToAllZookeeperNodes(event)
          }
        }

      })

      if (errors.isEmpty) {
        log.info("NTP check Done")
      } else {
        log.info("NTP check found %d problems".format(errors.size))
      }
    }
  }

  def sendEventToAllZookeeperNodes(event: SystemEvent) {
    val systemsIds = curator.getChildNames(Paths.Systems.getDefaultPath)
    systemsIds.foreach(id ⇒ curator.appendEvent(Path(Paths.Systems.getLogEventsPath(id)) / "qn-", event.copy(systemId = id)))
  }
}