package csc.sectioncontrol

import collection.mutable.ListBuffer
import java.util.Date
import csc.akkautils.DirectLogging
import messages.SystemEvent
import java.io.{ Closeable, IOException, BufferedReader, InputStreamReader }

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
/**
 * Test if NTP synchronization works for at least the last 4 tries
 * and create the Alarm accordingly
 * returns RET_SUCCESS 	when NTP synchronization test could be performed. Alarm could
 * 						be created or removed depending on the test result
 * 		   RET_FAILED 	when problem with performing the test. Always raise an Alarm
 *
 * @param ntpqPath
 */
class NtpCheck(ntpqPath: String, maxNrFailures: Int, maxStratum: Int) extends DirectLogging {
  val ntpCommand = "ntpq -p"
  val reachMask = calcReachMask(maxNrFailures)
  val NTP_SYNC = "NTP-SYNC"
  val NTP_STRATUM = "NTP-STRATUM"

  def calcReachMask(maxNrFailures: Int): Int = {
    var mask = ~0 //all 1
    mask = mask << maxNrFailures //last bits filled with zero
    ~mask //invert all bits 0-> 1 and 1 -> 0 => last bits filled with 1
  }

  def check(now: Date): Seq[SystemEvent] = {
    val cmd = if (!ntpqPath.isEmpty && !ntpqPath.endsWith("/")) {
      ntpqPath + "/" + ntpCommand
    } else {
      ntpqPath + ntpCommand
    }
    val lines = executeNTPQ(cmd)
    checkOutput(now, lines, maxStratum)
  }

  def checkOutput(now: Date, lines: Seq[String], maxStratum: Int): Seq[SystemEvent] = {
    /* output example
remote           refid      st t when poll reach   delay   offset  jitter
==============================================================================
*10.109.22.14    .PPSa.           1 u  511 1024  377    0.197   -0.623   0.213
*/
    val reg = """(.)(\S+)\s+(\S+)\s+(\d+)\s+\S\s+[\S]+\s+\d+\s+([0-7]+)\s+[-\d.]+\s+[-\d.]+\s+[-\d.]+""".r
    val header = """\s+remote[\s]+refid[\s]+st[\s]+t[\s]+when[\s]+poll[\s]+reach[\s]+delay[\s]+offset[\s]+jitter""".r
    val headerLine = """=+""".r

    val errors = new ListBuffer[SystemEvent]
    for (line ← lines) {
      line match {
        case reg(tallyCode, remote, refId, stratum, reach) ⇒ {
          //check for sync
          val reachInt = Integer.parseInt(reach, 8)
          if ((reachMask & reachInt) == 0) {
            //synch failed for the last 4 times
            errors += SystemEvent(componentId = "NTP-%s".format(remote),
              timestamp = now.getTime,
              systemId = "will be overwritten",
              path = None,
              eventType = NTP_SYNC,
              userId = "system",
              reason = Some("Synchronisatie faalt reach=[%s] voor %s".format(reach, remote)))
          } else if (stratum.toInt > maxStratum) {
            errors += SystemEvent(componentId = "NTP-%s".format(remote),
              timestamp = now.getTime,
              systemId = "will be overwritten",
              path = None,
              eventType = NTP_STRATUM,
              userId = "system",
              reason = Some("Stratum=[%s] is te hoog voor %s".format(stratum, remote)))
          }
        }
        case header()     ⇒ //nothing to do
        case headerLine() ⇒ //nothing to do
        case other ⇒ {
          log.warning("Parsing Failed for line [%s]".format(line))
        }
      }
    }
    errors
  }

  def executeNTPQ(ntpq: String): Seq[String] = {
    var results: BufferedReader = null
    var process: Process = null
    var output: ListBuffer[String] = null
    // variable to keep the first exception and prevent other exceptions to hide it.
    var cachedException: Exception = null

    try {
      process = Runtime.getRuntime.exec(ntpq)
      val result = process.waitFor()
      if (result != 0) {
        log.warning("Process [%s] returned %d".format(process, result))
      }

      output = new ListBuffer[String]
      results = new BufferedReader(new InputStreamReader(process.getInputStream))

      var line = results.readLine()
      while (line != null) {
        output += line
        line = results.readLine()
      }
    } catch {
      case e: Exception ⇒
        log.error(e, "method executeNTPQ failed.")
        cachedException = e
    } finally {
      if (results != null) {
        cachedException = closeCloseable(results, "BufferedReader cannot be closed.", cachedException)
      }

      if (process != null) {
        cachedException = closeCloseable(process.getInputStream, "Process [%s]. Process.getInputStream cannot be closed.".format(process), cachedException)
        cachedException = closeCloseable(process.getOutputStream, "Process [%s]. Process.getOutputStream cannot be closed.".format(process), cachedException)
        cachedException = closeCloseable(process.getErrorStream, "Process [%s]. Process.getErrorStream cannot be closed.".format(process), cachedException)

        process.destroy()
      }

      if (cachedException != null)
        throw cachedException
    }

    output
  }

  def closeCloseable(closable: Closeable, logMsg: String, cachedException: Exception): Exception = {
    var result: Exception = cachedException
    try {
      if (closable != null)
        closable.close()
    } catch {
      case e: Exception ⇒
        log.error(e, logMsg)
        if (cachedException == null)
          result = e
    }

    result
  }
}