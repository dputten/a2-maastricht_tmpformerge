package csc.input_events.snmp

import akka.actor.{ Props, ActorRef, ActorSystem }
import akka.testkit.{ TestKit, TestProbe }
import akka.util.Duration
import akka.util.duration._
import csc.input_events.config.{ SwitchConfig, BounceEliminatorConfig }
import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import akka.camel.CamelExtension
import csc.input_events.{ SyslogMessage, BounceEliminator }

class BounceEliminatorSnmpTest extends TestKit(ActorSystem("BounceEliminatorSnmpTest"))
  with WordSpec with MustMatchers with BeforeAndAfterAll {

  override protected def afterAll() {
    super.afterAll()
    system.shutdown()
  }

  "BounceEliminator" must {
    "not filter messages if time between them is larger than the specified delay" in {
      val regexStrings = Set(
        "<\\d+>\\S* 127\\.0\\.0\\.2 SpecificTrap:1 VariableBindings:\\[1.3.6.1.4.1.8691.10.1210.20.2 = 0\\]",
        "<\\d+>\\S* 127\\.0\\.0\\.2 SpecificTrap:1 VariableBindings:\\[1.3.6.1.4.1.8691.10.1210.20.2 = 1\\]")

      val config = BounceEliminatorConfig(
        "BounceEliminator",
        "mina:udp://localhost:12400?sync=false",
        Set(
          SwitchConfig("switch1", 200 millis, regexStrings)))

      val msgProbe = TestProbe()
      val testActor = system.actorOf(Props(new BounceEliminator(config, Set(msgProbe.ref))))
      CamelExtension.get(system).awaitActivation(testActor, 10 seconds)

      val timing = List(100.millis, 700.millis, 1300.millis, 1900.millis)

      val messages = List(
        SyslogMessage(Some("127.0.0.2"), "<1> 127.0.0.2 SpecificTrap:1 VariableBindings:[1.3.6.1.4.1.8691.10.1210.20.2 = 0]", 1),
        SyslogMessage(Some("127.0.0.2"), "<2> 127.0.0.2 SpecificTrap:1 VariableBindings:[1.3.6.1.4.1.8691.10.1210.20.2 = 1]", 2),
        SyslogMessage(Some("127.0.0.2"), "<3> 127.0.0.2 SpecificTrap:1 VariableBindings:[1.3.6.1.4.1.8691.10.1210.20.2 = 0]", 3),
        SyslogMessage(Some("127.0.0.2"), "<4> 127.0.0.2 SpecificTrap:1 VariableBindings:[1.3.6.1.4.1.8691.10.1210.20.2 = 1]", 4))

      scheduleMessages(timing.zip(messages), testActor)
      val fourMessages = msgProbe.receiveN(4, 10.seconds)

      fourMessages.map { m: Any ⇒ m.asInstanceOf[SyslogMessage] } must be(messages)
      msgProbe.msgAvailable must be(false)

      system.stop(testActor)
      CamelExtension.get(system).awaitDeactivation(testActor, 10 seconds)
    }

    "send only last message of a sequence of messages if the time between them is shorter than the specified delay" in {
      val regexStrings = Set(
        "<\\d+>\\S* 127\\.0\\.0\\.2 SpecificTrap:1 VariableBindings:\\[1.3.6.1.4.1.8691.10.1210.20.2 = 0\\]",
        "<\\d+>\\S* 127\\.0\\.0\\.2 SpecificTrap:1 VariableBindings:\\[1.3.6.1.4.1.8691.10.1210.20.2 = 1\\]")

      val config = BounceEliminatorConfig(
        "BounceEliminator",
        "mina:udp://localhost:12400?sync=false",
        Set(
          SwitchConfig("switch1", 200 millis, regexStrings)))

      val msgProbe = TestProbe()
      val testActor = system.actorOf(Props(new BounceEliminator(config, Set(msgProbe.ref))))
      CamelExtension.get(system).awaitActivation(testActor, 10 seconds)
      val timing = List(10.millis, 50.millis, 90.millis, 130.millis, 170.millis)

      val messages = List(
        SyslogMessage(Some("127.0.0.2"), "<1> 127.0.0.2 SpecificTrap:1 VariableBindings:[1.3.6.1.4.1.8691.10.1210.20.2 = 0]", 1000),
        SyslogMessage(Some("127.0.0.2"), "<2> 127.0.0.2 SpecificTrap:1 VariableBindings:[1.3.6.1.4.1.8691.10.1210.20.2 = 1]", 1001),
        SyslogMessage(Some("127.0.0.2"), "<3> 127.0.0.2 SpecificTrap:1 VariableBindings:[1.3.6.1.4.1.8691.10.1210.20.2 = 0]", 1002),
        SyslogMessage(Some("127.0.0.2"), "<12> 127.0.0.2 SpecificTrap:1 VariableBindings:[1.3.6.1.4.1.8691.10.1210.20.2 = 1]", 1003))

      scheduleMessages(timing.zip(messages), testActor)

      val oneMessage = msgProbe.receiveN(1, 2.seconds)
      msgProbe.msgAvailable must be(false)
      val message = oneMessage(0).asInstanceOf[SyslogMessage]
      message must be(messages(3))

      system.stop(testActor)
      CamelExtension.get(system).awaitDeactivation(testActor, 10 seconds)
    }

    "only pass on messages that match the regular expressions" in {
      val regexStrings = Set(
        "<\\d+>\\S* 127\\.0\\.0\\.2 SpecificTrap:1 VariableBindings:\\[1.3.6.1.4.1.8691.10.1210.20.2 = 0\\]",
        "<\\d+>\\S* 127\\.0\\.0\\.2 SpecificTrap:1 VariableBindings:\\[1.3.6.1.4.1.8691.10.1210.20.2 = 1\\]")
      val config = BounceEliminatorConfig(
        "BounceEliminator",
        "mina:udp://localhost:12400?sync=false",
        Set(
          SwitchConfig("switch1", 0 millis, regexStrings)))

      val msgProbe = TestProbe()
      val testActor = system.actorOf(Props(new BounceEliminator(config, Set(msgProbe.ref))))
      CamelExtension.get(system).awaitActivation(testActor, 10 seconds)

      val timing = List(10.millis, 110.millis, 210.millis)
      val messages = List(
        SyslogMessage(Some("127.0.0.2"), "<1> 127.0.0.2 SpecificTrap:1 VariableBindings:[1.3.6.1.4.1.8691.10.1210.20.2 = 1]", 666),
        SyslogMessage(Some("127.0.0.2"), "<2> 127.0.0.2 SpecificTrap:1 VariableBindings:[1.3.6.1.4.1.8691.10.1210.20.2 = 0]", 777),
        SyslogMessage(Some("127.0.0.2"), "This message does not match", 666))

      scheduleMessages(timing.zip(messages), testActor)

      val receivedMessages = msgProbe.receiveN(2, 5.seconds)
      msgProbe.msgAvailable must be(false)
      val message0 = receivedMessages(0).asInstanceOf[SyslogMessage]
      val message1 = receivedMessages(1).asInstanceOf[SyslogMessage]
      message0 must be(messages(0))
      message1 must be(messages(1))

      system.stop(testActor)
      CamelExtension.get(system).awaitDeactivation(testActor, 10 seconds)
    }
  }

  protected def scheduleMessages(msgList: Seq[(Duration, SyslogMessage)], actor: ActorRef): Unit = {
    val s = system.scheduler
    msgList foreach { timedMessage ⇒
      {
        val (duration, syslogMessage) = timedMessage
        s.scheduleOnce(duration, actor, syslogMessage)
      }
    }
  }
}

