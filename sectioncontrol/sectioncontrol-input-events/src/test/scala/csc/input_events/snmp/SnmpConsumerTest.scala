package csc.input_events.snmp

import org.scalatest.{ BeforeAndAfterEach, BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers
import csc.snmp._
import com.typesafe.config.ConfigFactory
import akka.testkit.{ TestKit, TestProbe }
import akka.util.duration._
import akka.actor.{ Props, ActorSystem }
import csc.input_events.config._
import csc.input_events.mock.HttpUriClientFactoryMock
import java.net.URI
import csc.input_events.config.RegexWithSystemStateUri
import csc.input_events.client.SchemeFactoryMap
import csc.input_events.config.RegexWithAnyMap
import scala.Some
import csc.snmp.SnmpConfig
import csc.input_events.{ SnmpConsumer, SyslogConsumer }
import com.github.sstone.amqp.Amqp.Publish
import net.liftweb.json.DefaultFormats
import csc.amqp.AmqpSerializers
import com.rabbitmq.client.AMQP.BasicProperties
import csc.sectioncontrol.messages.SystemEvent

class SnmpConsumerTest extends TestKit(ActorSystem("SnmpTrapReceiver")) with WordSpec with MustMatchers with BeforeAndAfterAll with AmqpSerializers with BeforeAndAfterEach {
  override val ApplicationId = "Unit-tests"
  override val log = null
  override implicit val formats = DefaultFormats

  override def afterAll(): Unit = {
    system.shutdown()
    super.afterAll()
  }

  "SnmpConsumerIntegrationTest" must {
    "send a message through snmp and match the message in the syslogConsumer against a regex and set eventType to 'UnauthorizedDoorOpen'" in {
      val schemeFactoryMap = new SchemeFactoryMap(Map("http" -> new HttpUriClientFactoryMock))
      val regexWithMaintenanceUri = Seq(RegexWithSystemStateUri("regex", new URI("http://localhost/enforceOn")))
      // val systemEventTemplate: Map[String, Any] = Map("eventTypeFormat" -> "%sDoorOpen", "key2" -> "value2")
      // val systemEventTemplateClosed: Map[String, Any] = Map("eventTypeFormat" -> "%sDoorClosed", "key2" -> "value2")
      val regexWithMaps = Seq(RegexWithAnyMap(
        "<\\d+>\\S* 0\\.0\\.0\\.0 SpecificTrap:2 VariableBindings:\\[1.3.6.1.4.1.8691.10.1210.20.2 = 1\\]",
        Set("a.b.c"), systemEventTemplate))

      val syslogConsumerConfig = SyslogConsumerConfig(
        Some("Consumer"), "AmqpEndpoint", regexWithMaintenanceUri, regexWithMaps)

      val msgProbe = new TestProbe(system)
      val syslogConsumer = system.actorOf(Props(new SyslogConsumer(syslogConsumerConfig, schemeFactoryMap, msgProbe.ref)))

      val config1 = ConfigFactory.load("reference.conf")
      val snmpConfig = SnmpConfig(config1)

      val snmpConsumerActor = system.actorOf(Props(new SnmpConsumer(syslogConsumer)))

      val now = new NowMock
      now.setNow(1448455101)

      val snmpTrapReceiver = new SnmpTrapReceiver(snmpConfig, snmpConsumerActor, now)
      snmpTrapReceiver.start()

      val snmpTrapGenerator = new SnmpTrapGenerator(snmpConfig)
      snmpTrapGenerator.send()
      snmpTrapReceiver.stop()

      // Actor must:
      // - add timeStamp to the map
      // - add the correct eventType to the map
      // - remove eventTypeFormat from the map
      val msg = msgProbe.receiveOne(3.second).asInstanceOf[Publish]
      msg.exchange must be("AmqpEndpoint")
      msg.key must be("a.b.c")
      val Right(event) = decodeMessage[SystemEvent](msg)
      event.eventType must be("DoorOpen")
    }
  }

  val systemEventTemplate: Map[String, Any] = Map(
    "componentId" -> "InputEvents",
    "timestamp" -> 0L,
    "systemId" -> "A12?",
    "eventTypeFormat" -> "%sDoorOpen",
    "userId" -> "System",
    "reason" -> "kaputt",
    "corridorId" -> "deze corridor")

  val systemEventTemplateClosed: Map[String, Any] = Map(
    "componentId" -> "InputEvents",
    "timestamp" -> 0L,
    "systemId" -> "A12?",
    "eventTypeFormat" -> "%sDoorClosed",
    "userId" -> "System",
    "corridorId" -> "deze corridor")

  private def decodeMessage[T <: AnyRef: Manifest](msg: Publish): Either[String, T] = {
    val props: BasicProperties = msg.properties.get
    val encoding = props.getContentEncoding
    val body = if (encoding == "gzip") decompress(msg.body) else msg.body
    fromJson[T](new String(body, "UTF-8"))
  }
}

class NowMock() extends TimeNow {
  var _now: Long = _
  override def getNow: Long = _now
  override def setNow(now: Long) = {
    _now = now
  }
}

