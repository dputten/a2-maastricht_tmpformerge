package csc.input_events

import java.net.URI

import akka.actor.{ ActorSystem, Props }
import akka.testkit.{ TestKit, TestProbe }
import akka.util.duration._
import csc.input_events.client.SchemeFactoryMap
import csc.input_events.config.{ RegexWithAnyMap, RegexWithSystemStateUri, SyslogConsumerConfig }
import csc.input_events.mock.HttpUriClientFactoryMock
import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import csc.sectioncontrol.messages.SystemEvent
import com.github.sstone.amqp.Amqp.Publish
import csc.amqp.AmqpSerializers
import com.rabbitmq.client.AMQP.BasicProperties
import net.liftweb.json.{ Formats, DefaultFormats }
import csc.json.lift.EnumerationSerializer

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 3/16/15.
 */

class SyslogConsumerTest extends TestKit(ActorSystem("SyslogConsumerTest")) with WordSpec with MustMatchers with AmqpSerializers with BeforeAndAfterAll {
  override val ApplicationId = "Unit-tests"
  override val log = null
  override implicit val formats: Formats = DefaultFormats + new EnumerationSerializer(SystemStateType)

  val systemEventTemplate: Map[String, Any] = Map(
    "componentId" -> "InputEvents",
    "timestamp" -> 0L,
    "systemId" -> "A12?",
    "eventTypeFormat" -> "%sDoorOpen",
    "userId" -> "System",
    "reason" -> "kaputt",
    "corridorId" -> "deze corridor")

  override protected def afterAll() {
    super.afterAll()
    system.shutdown()
  }

  "SyslogConsumer" must {

    "match messages against a regex and ignore or forward and augment the message" in {
      val schemeFactoryMap = new SchemeFactoryMap(Map("http" -> new HttpUriClientFactoryMock))
      val regexWithMaintenanceUri = Seq(RegexWithSystemStateUri("regex", new URI("http://localhost/maintenance")))

      val regexWithMaps = Seq(RegexWithAnyMap("regex", Set("a.b.c"), systemEventTemplate))
      val config = SyslogConsumerConfig(
        Some("Consumer"),
        "AmqpEndpoint",
        regexWithMaintenanceUri,
        regexWithMaps)

      val msgProbe = TestProbe()
      val testActor = system.actorOf(Props(new SyslogConsumer(config, schemeFactoryMap, msgProbe.ref)))

      // Message does not match the regex, actor must ignore the message
      testActor ! "Regex"
      msgProbe.expectNoMsg(1.second)

      // Message matches the regex, actor must augment the template and send message
      testActor ! "regex"

      // Actor must:
      // - add timeStamp to the map
      // - add the correct eventType to the map
      // - remove eventTypeFormat from the map
      val msg: Publish = msgProbe.receiveOne(1.second).asInstanceOf[Publish]
      msg.exchange must be("AmqpEndpoint")
      msg.key must be("a.b.c")
      val Right(event) = decodeMessage[SystemEvent](msg)
      event.componentId must be("InputEvents")
      event.systemId must be("A12?")
      event.eventType must be("AuthorizedDoorOpen")
      event.userId must be("System")
      event.reason must be("kaputt")
      event.corridorId must be(Some("deze corridor"))
      system.stop(testActor)
    }

    "match message against a regex and set eventType to 'UnauthorizedDoorOpen'" in {
      val schemeFactoryMap = new SchemeFactoryMap(Map("http" -> new HttpUriClientFactoryMock))
      val regexWithMaintenanceUri = Seq(RegexWithSystemStateUri("regex", new URI("http://localhost/enforceOn")))
      val regexWithMaps = Seq(RegexWithAnyMap("regex", Set("a.b.c"), systemEventTemplate))
      val config = SyslogConsumerConfig(
        Some("Consumer"),
        "AmqpEndpoint",
        regexWithMaintenanceUri,
        regexWithMaps)

      val msgProbe = TestProbe()
      val testActor = system.actorOf(Props(new SyslogConsumer(config, schemeFactoryMap, msgProbe.ref)))

      // Message matches the regex, actor must augment the template and send message
      testActor ! "regex"

      // Actor must:
      // - add timeStamp to the map
      // - add the correct eventType to the map
      // - remove eventTypeFormat from the map
      val msg = msgProbe.receiveOne(1.second).asInstanceOf[Publish]
      msg.exchange must be("AmqpEndpoint")
      msg.key must be("a.b.c")

      val Right(event) = decodeMessage[SystemEvent](msg)
      event.eventType must be("DoorOpen")

      system.stop(testActor)
    }
  }

  private def decodeMessage[T <: AnyRef: Manifest](msg: Publish): Either[String, T] = {
    val props: BasicProperties = msg.properties.get
    val encoding = props.getContentEncoding
    val body = if (encoding == "gzip") decompress(msg.body) else msg.body
    fromJson[T](new String(body, "UTF-8"))
  }
}
