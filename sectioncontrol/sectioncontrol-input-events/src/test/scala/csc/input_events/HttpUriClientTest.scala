package csc.input_events

import java.net.{ UnknownHostException, URI }

import csc.akkautils.DirectLoggingAdapter
import csc.input_events.client.{ HttpUriClient, SchemeFactoryMap }
import csc.input_events.mock.HttpUriClientFactoryMock
import csc.sectioncontrol.storage.ZkState
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 3/11/15.
 */

class HttpUriClientTest extends WordSpec with MustMatchers {
  val schemeFactoryMap = new SchemeFactoryMap(Map("http" -> new HttpUriClientFactoryMock))
  val log = new DirectLoggingAdapter(this.getClass.getName)

  "HttpUriClient" must {
    "return a Right(SimpleSystemState)" in {
      val uri = new URI("http://www.apple.com/maintenance")
      val clientFactory = schemeFactoryMap.factoryForScheme(uri.getScheme)
      val client = clientFactory.getClientForUri(uri)

      val response1 = client.get[SimpleSystemState](uri)
      response1 must be(Right(SimpleSystemState(SystemStateType.Maintenance)))

      val response2 = client.get[SimpleSystemState](new URI("http://www.apple.com/standBy"))
      response2 must be(Right(SimpleSystemState(SystemStateType.StandBy)))
    }

    "return a Left[UnknownHostException] for a non-existing server" in {
      val uri = new URI("http://non.existing.server/")
      val client = new HttpUriClient(uri)
      val response = client.get[SimpleSystemState](uri)
      response.isLeft must be(true)
    }

    "return a Left[Exception] for existing server that returns wrong content" in {
      val uri = new URI("http://www.apple.com/")
      val client = new HttpUriClient(uri)
      val response = client.get[SimpleSystemState](uri)
      response.isLeft must be(true)
    }
  }
}
