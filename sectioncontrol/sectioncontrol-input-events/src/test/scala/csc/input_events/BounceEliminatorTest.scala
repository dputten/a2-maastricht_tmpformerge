package csc.input_events

import akka.actor.{ ActorRef, ActorSystem, Props }
import akka.testkit.{ TestKit, TestProbe }
import akka.util.Duration
import akka.util.duration._
import csc.input_events.config.{ SwitchConfig, BounceEliminatorConfig }
import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import akka.camel.CamelExtension

class BounceEliminatorTest extends TestKit(ActorSystem("BounceEliminatorTest"))
  with WordSpec with MustMatchers with BeforeAndAfterAll {

  override protected def afterAll() {
    super.afterAll()
    system.shutdown()
  }

  "BounceEliminator" must {
    "not filter messages if time between them is larger than the specified delay" in {
      val regexStrings = Set(
        "<\\d*>\\S* +\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2} 192\\.168\\.2\\.65 INFO:DI 1 transition \\(On -> Off\\)",
        "<\\d*>\\S* +\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2} 192\\.168\\.2\\.65 INFO:DI 1 transition \\(Off -> On\\)")
      val config = BounceEliminatorConfig(
        "BounceEliminator",
        "mina:udp://localhost:12400?sync=false",
        Set(
          SwitchConfig("switch1", 100 millis, regexStrings)))

      val msgProbe = TestProbe()
      val testActor = system.actorOf(Props(new BounceEliminator(config, Set(msgProbe.ref))))
      CamelExtension.get(system).awaitActivation(testActor, 10 seconds)

      val timing = List(100.millis, 700.millis, 1300.millis, 1900.millis)
      val messages = List(
        "<1>Jan 01 01:58:39 192.168.2.65 INFO:DI 1 transition (On -> Off)",
        "<2>Jan 01 01:58:49 192.168.2.65 INFO:DI 1 transition (Off -> On)",
        "<3>Jan 01 01:58:59 192.168.2.65 INFO:DI 1 transition (On -> Off)",
        "<4>Jan 01 01:59:09 192.168.2.65 INFO:DI 1 transition (Off -> On)")
      scheduleMessages(timing.zip(messages), testActor)
      val fourMessages = msgProbe.receiveN(4, 10.seconds)
      fourMessages.map { m: Any ⇒ m.asInstanceOf[SyslogMessage].text } must be(messages)
      msgProbe.msgAvailable must be(false)

      system.stop(testActor)
      CamelExtension.get(system).awaitDeactivation(testActor, 10 seconds)
    }

    "send only last message of a sequence of messages if the time between them is shorter than the specified delay" in {
      val regexStrings = Set(
        "<\\d+>\\S* +\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2} 192\\.168\\.2\\.65 INFO:DI 1 transition \\(On -> Off\\)",
        "<\\d+>\\S* +\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2} 192\\.168\\.2\\.65 INFO:DI 1 transition \\(Off -> On\\)")
      val config = BounceEliminatorConfig(
        "BounceEliminator",
        "mina:udp://localhost:12400?sync=false",
        Set(
          SwitchConfig("switch1", 200 millis, regexStrings)))

      val msgProbe = TestProbe()
      val testActor = system.actorOf(Props(new BounceEliminator(config, Set(msgProbe.ref))))
      CamelExtension.get(system).awaitActivation(testActor, 10 seconds)
      val timing = List(10.millis, 50.millis, 90.millis, 130.millis, 170.millis)
      val messages = List(
        "<1>Jan 01 01:58:39 192.168.2.65 INFO:DI 1 transition (On -> Off)",
        "<2>Jan 01 01:58:39 192.168.2.65 INFO:DI 1 transition (Off -> On)",
        "<3>Jan 01 01:58:39 192.168.2.65 INFO:DI 1 transition (On -> Off)",
        "<12>Jan 01 01:58:39 192.168.2.65 INFO:DI 1 transition (Off -> On)")
      scheduleMessages(timing.zip(messages), testActor)

      //      testActor ! messages(0)
      //      Thread.sleep(1)
      //      testActor ! messages(1)
      //      Thread.sleep(1)
      //      testActor ! messages(2)
      //      Thread.sleep(1)
      //      testActor ! messages(3)

      val oneMessage = msgProbe.receiveN(1, 2.seconds)
      msgProbe.msgAvailable must be(false)
      val message = oneMessage(0).asInstanceOf[SyslogMessage]
      message.text must be("<12>Jan 01 01:58:39 192.168.2.65 INFO:DI 1 transition (Off -> On)")

      system.stop(testActor)
      CamelExtension.get(system).awaitDeactivation(testActor, 1 seconds)
    }

    "only pass on messages that match the regular expressions" in {
      val regexStrings = Set(
        "<\\d+>\\S* +\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2} 192\\.168\\.2\\.65 INFO:DI 1 transition \\(On -> Off\\)",
        "<\\d+>\\S* +\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2} 192\\.168\\.2\\.65 INFO:DI 1 transition \\(Off -> On\\)")
      val config = BounceEliminatorConfig(
        "BounceEliminator",
        "mina:udp://localhost:12400?sync=false",
        Set(
          SwitchConfig("switch1", 0 millis, regexStrings)))

      val msgProbe = TestProbe()
      val testActor = system.actorOf(Props(new BounceEliminator(config, Set(msgProbe.ref))))
      CamelExtension.get(system).awaitActivation(testActor, 10 seconds)
      val timing = List(10.millis, 110.millis, 210.millis)
      val messages = List(
        "<1>Jan 01 01:58:39 192.168.2.65 INFO:DI 1 transition (On -> Off)",
        "<2>Jan 01 01:58:39 192.168.2.65 INFO:DI 1 transition (Off -> On)",
        "This message does not match")
      scheduleMessages(timing.zip(messages), testActor)

      val receivedMessages = msgProbe.receiveN(2, 5.seconds)
      msgProbe.msgAvailable must be(false)
      val message0 = receivedMessages(0).asInstanceOf[SyslogMessage]
      val message1 = receivedMessages(1).asInstanceOf[SyslogMessage]
      message0.text must be("<1>Jan 01 01:58:39 192.168.2.65 INFO:DI 1 transition (On -> Off)")
      message1.text must be("<2>Jan 01 01:58:39 192.168.2.65 INFO:DI 1 transition (Off -> On)")

      system.stop(testActor)
      CamelExtension.get(system).awaitDeactivation(testActor, 10 seconds)
    }
  }

  protected def scheduleMessages(msgList: Seq[(Duration, String)], actor: ActorRef): Unit = {
    val s = system.scheduler
    msgList foreach { timedMessage ⇒
      {
        val (duration, text) = timedMessage
        s.scheduleOnce(duration, actor, text)
      }
    }
  }
}
