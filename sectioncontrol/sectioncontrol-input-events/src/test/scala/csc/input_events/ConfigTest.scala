package csc.input_events

import java.net.URI
import com.typesafe.config.ConfigFactory
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import akka.util.duration._
import csc.input_events.config._

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 3/3/15.
 */

class ConfigTest extends WordSpec with MustMatchers {
  val config = ConfigFactory.load(getClass.getClassLoader, "reference.conf")

  "RegexWithSystemEventTemplate" must {

    "Instantiate from a Config instance" in {
      val configString =
        """
          |regex: "[Aa]ap",
          |routingKeys: ["amqp.routing.key1", "amqp.routing.key2"]
          |systemEventTemplate: {
          |     componentId: "deuralarmen"
          |     eventTypeFormat: "%sDoorOpen"
          |     userId: "System"
          |     reason: "inbrekers"
          |}
        """.stripMargin
      val conf = ConfigFactory.parseString(configString)
      val r = new RegexWithAnyMap(conf)

      val expectedMap: Map[String, Any] = Map(
        "componentId" -> "deuralarmen",
        "eventTypeFormat" -> "%sDoorOpen",
        "userId" -> "System",
        "reason" -> "inbrekers")
      r.regex must be("[Aa]ap")
      r.routingKeys must be(Set("amqp.routing.key1", "amqp.routing.key2"))
      r.template must be(expectedMap)
    }

    "properly match its regular expression" in {
      val map = Map("aap" -> "noot")
      val r = RegexWithAnyMap("[Aa]ap", Set("amqp.routing.key"), map)
      r("aap") must be(Some(r))
      r("Aap") must be(Some(r))
      r("noot") must be(None)
    }
  }

  "SyslogConsumerConfig" must {
    "Instantiate from a Config instance" in {
      val expected = SyslogConsumerConfig(None, "theAmqpEndpoint",
        Seq(RegexWithSystemStateUri("aap", new URI("zk://localhost/ctes/state"))),
        Seq(RegexWithAnyMap("noot", Set("key1.for.amqp", "key2.for.amqp"),
          Map("component" -> "DoorAlarms",
            "eventTypeFormat" -> "%sDoorOpen",
            "userId" -> "System",
            "reason" -> "Iemand zit met zijn vingers aan de deur"))))

      val configString =
        """
          |syslogEndpoint : "theSyslogEndpoint",
          |amqpEndpoint : "theAmqpEndpoint",
          |regexToStateUris: [
          |     {regex: "aap", uri: "zk://localhost/ctes/state"}
          |  ],
          |regexToSystemEvents: [
          |     { regex: "noot",
          |       routingKeys: ["key1.for.amqp", "key2.for.amqp"],
          |       systemEventTemplate: {
          |         component : "DoorAlarms",
          |         eventTypeFormat : "%sDoorOpen",
          |         userId    : "System",
          |         reason    : "Iemand zit met zijn vingers aan de deur"
          |       }
          |     }
          |]
          |
        """.stripMargin
      val config = ConfigFactory.parseString(configString)
      val syslogConsumerConfig = SyslogConsumerConfig("", config)
      syslogConsumerConfig must be(expected)
    }

    "select proper uris" in {
      val uriAap0 = new URI("zk://localhost/ctes/aap0")
      val uriAap1 = new URI("zk://localhost/ctes/aap1")
      val uriNoot = new URI("zk://localhost/ctes/noot")
      val uriNooot = new URI("zk://localhost/ctes/noooot")

      val config = SyslogConsumerConfig(None, "theAmqpEndpoint",
        Seq(RegexWithSystemStateUri("[Aa]ap", uriAap0),
          RegexWithSystemStateUri("[Aa]ap", uriAap1),
          RegexWithSystemStateUri("[Nn]oot", uriNoot),
          RegexWithSystemStateUri("[Nn]o+t", uriNooot)),
        Seq())

      config("aap").map(_.uri) must be(Set(uriAap0, uriAap1))
      config("Noot").map(_.uri) must be(Set(uriNoot, uriNooot))
      config("Nooot").map(_.uri) must be(Set(uriNooot))
    }
  }

  "SwitchConfig" must {
    "Instantiate from a Config object" in {
      val configString =
        """
          |    {
          |      id: "switchId",
          |      delay_ms: 100
          |      regexList: ["[Rr]egex", "[Aa]ap", "[Nn]oot"]
          |    }
          |
        """.stripMargin
      val config = ConfigFactory.parseString(configString)
      val switchConfig = new SwitchConfig(config)
      switchConfig.id must be("switchId")
      switchConfig.delay must be(100 millis)
      switchConfig.regexen must be(Set("[Rr]egex", "[Aa]ap", "[Nn]oot"))
    }
  }

  "BounceEliminatorConfig" must {
    val configString =
      """
        |{
        |  # Name of this actor
        |  actorName: "BounceEliminator"
        |  # Camel mina endpoint used to listen for syslog messages
        |  syslogEndpoint: "mina:udp://localhost:12400?sync=false"
        |
        |  switches: [
        |    {
        |      id: "switch1",
        |      delay_ms: 100
        |      regexList: ["Regex01", "Regex02", "Regex.3"]
        |    },
        |    {
        |      id: "switch2",
        |      delay_ms: 120
        |      regexList: ["Regex11", "Regex12", "Regex.3"]
        |    }
        |
        |  ]
        |}
      """.stripMargin
    val switch1 = SwitchConfig("switch1", 100 millis, Set("Regex01", "Regex02", "Regex.3"))
    val switch2 = SwitchConfig("switch2", 120 millis, Set("Regex11", "Regex12", "Regex.3"))

    "Instantiate from a Config object" in {
      val config = ConfigFactory.parseString(configString)
      val bounceConfig = BounceEliminatorConfig("", config)

      bounceConfig.actorName must be("BounceEliminator")
      bounceConfig.syslogEndpoint must be("mina:udp://localhost:12400?sync=false")
      bounceConfig.switches must be(Set(switch1, switch2))
    }

    "select proper switches matching a message" in {
      val config = ConfigFactory.parseString(configString)
      val bounceConfig = BounceEliminatorConfig("", config)
      bounceConfig("Regex") must be(Set())
      bounceConfig("Regex01") must be(Set(switch1))
      bounceConfig("Regex33") must be(Set(switch1, switch2))
    }
  }
}
