package csc.input_events.mock

import java.net.URI

import csc.input_events.client.{ HttpUriClient, UriClientFactory }
import csc.amqp.JsonSerializers
import csc.input_events.SystemStateType
import csc.input_events.SimpleSystemState
import net.liftweb.json.{ Formats, DefaultFormats }
import csc.json.lift.EnumerationSerializer

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 3/11/15.
 */

class HttpUriClientMock(uri: URI) extends HttpUriClient(uri) {

  override protected def httpGet(uri: URI): String = {
    val state = uri.getPath match {
      case "/enforceDegraded" ⇒ toJson[SimpleSystemState](SimpleSystemState(SystemStateType.EnforceDegraded))
      case "/enforceOff"      ⇒ toJson[SimpleSystemState](SimpleSystemState(SystemStateType.EnforceOff))
      case "/enforceOn"       ⇒ toJson[SimpleSystemState](SimpleSystemState(SystemStateType.EnforceOn))
      case "/maintenance"     ⇒ toJson[SimpleSystemState](SimpleSystemState(SystemStateType.Maintenance))
      case "/off"             ⇒ toJson[SimpleSystemState](SimpleSystemState(SystemStateType.Off))
      case "/standBy"         ⇒ toJson[SimpleSystemState](SimpleSystemState(SystemStateType.StandBy))
      case _                  ⇒ toJson[SimpleSystemState](SimpleSystemState(SystemStateType.Failure))
    }
    state match {
      case Right(json) ⇒ json
      case Left(err)   ⇒ "{\"error\": \"%s\"}".format(err)
    }
  }
}

class HttpUriClientFactoryMock extends UriClientFactory {
  def getClientForUri(uri: URI) = new HttpUriClientMock(uri)
  def shutdown() {}
}
