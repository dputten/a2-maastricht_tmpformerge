package csc.input_events

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 2/16/15.
 */

import java.net.URI

import akka.actor._
import csc.input_events.config.SyslogConsumerConfig
import csc.amqp.{ PublisherActor, JsonSerializers }
import net.liftweb.json.{ DefaultFormats, Formats }
import csc.sectioncontrol.messages.SystemEvent
import scala.Left
import csc.input_events.client.SchemeFactoryMap
import scala.Right
import scala.Some
import csc.json.lift.EnumerationSerializer
import csc.input_events.SystemStateType.SystemStateType

/*
* The different states that a system can be placed in
*/
object SystemStateType extends Enumeration {
  type SystemStateType = Value
  val Off, StandBy, Maintenance, Failure, EnforceOn, EnforceOff, EnforceDegraded = Value
}

case class SimpleSystemState(state: SystemStateType)

class SyslogConsumer(protected val consumerConfig: SyslogConsumerConfig, schemeFactoryMap: SchemeFactoryMap, producer: ActorRef)
  extends Actor with JsonSerializers with ActorLogging {

  override val formats: Formats = DefaultFormats + new EnumerationSerializer(SystemStateType)

  def receive = {
    case msg: SyslogMessage ⇒
      log.debug("SyslogConsumer.receive: " + msg)
      handleSyslogEvent(msg)
    case text: String ⇒ handleSyslogEvent(SyslogMessage(None, text))
  }

  protected def handleSyslogEvent(syslogEvent: SyslogMessage): Unit = {
    log.debug("syslogEvent: " + syslogEvent)

    // get the list of uri's where we must obtain the ZkSystemState resource
    val stateUris: Set[URI] = consumerConfig(syslogEvent.text).map(_.uri)
    log.info("URI's where to obtain SimpleSystemState:" + stateUris)

    // transform Seq[Uri] to Set[Either[Exception,SimpleSystemState]]
    val systemStates: Set[Either[String, SimpleSystemState]] = collectSystemStates(stateUris)
    log.debug("systemStates: " + systemStates)

    // transform Seq[SimpleSystemState] to (authorized, unauthorized, error)
    val (authorized, unauthorized, error) = calculateAuthorization(systemStates)
    log.debug("(authorized, unauthorized, error): " + (authorized, unauthorized, error))

    // check if there's at least one ZkSystemState.Maintenance
    val eventTypePrefix = if ((authorized + unauthorized + error) == 0) ""
    else if (authorized > 0) "Authorized" // authorized because at least 1 system in maintenance
    else "" // Unauthorized
    sendEvents(eventTypePrefix, syslogEvent.text)
  }

  protected def calculateAuthorization(states: Set[Either[String, SimpleSystemState]]): (Int, Int, Int) = {
    val initialAccu = (0, 0, 0)
    states.foldLeft(initialAccu)({
      (accu, inquireResult) ⇒
        {
          val (a, u, e) = accu
          inquireResult match {
            case Right(SimpleSystemState(SystemStateType.Maintenance)) ⇒ (a + 1, u, e)
            case Left(exception) ⇒ (a, u, e + 1)
            case other ⇒ (a, u + 1, e)
          }
        }
    })
  }

  protected def collectSystemStates(stateUris: Set[URI]): Set[Either[String, SimpleSystemState]] = {
    stateUris.map((u) ⇒ {
      val clientFactory = schemeFactoryMap.factoryForScheme(u.getScheme)
      val client = clientFactory.getClientForUri(u)
      val result = client.get[SimpleSystemState](u)
      log.info("SystemState[" + u + "]=" + result)
      result
    })
  }

  protected def sendEvents(eventTypePrefix: String, event: String): Unit = {
    log.debug("sendEvents->event: " + event)
    val matchingEventMaps = consumerConfig.regexWithAnyMapsMatching(event)
    log.debug("matchingEventMaps: " + matchingEventMaps)
    matchingEventMaps.foreach((regexWithMap) ⇒ {

      val finalMap = templateToFinalMap(regexWithMap.template, eventTypePrefix)
      log.info("final Map[String, Any] for systemEvent: {}", finalMap)
      val eitherSystemEvent = mapToSystemEvent(finalMap)
      eitherSystemEvent match {
        case Left(deserializeErrorStr) ⇒ log.error(deserializeErrorStr)
        case Right(systemEvent) ⇒ {
          regexWithMap.routingKeys foreach {
            routingKey ⇒
              {
                val publisher = context.system.actorOf(publisherActorProps(producer, consumerConfig.amqpEndpoint, routingKey))
                log.info("Sending " + systemEvent + " to " + producer)
                publisher ! systemEvent
                publisher ! PoisonPill
              }
          }
        }
      }
    })
  }

  protected def templateToFinalMap(template: Map[String, Any], eventTypePrefix: String): Map[String, Any] = {
    val map = template.get("eventTypeFormat") match {
      case Some(formatString) ⇒ template + ("eventType" -> formatString.asInstanceOf[String].format(eventTypePrefix))
      case None               ⇒ template
    }
    map - "eventTypeFormat" + ("timestamp" -> System.currentTimeMillis())
  }

  /**
   * convert a Map[String, Any] to a SystemEvent.
   * @param map the map to convert
   * @return Either a string containing the error of the SystemEvent instance
   */
  protected def mapToSystemEvent(map: Map[String, Any]): Either[String, SystemEvent] = {
    toJson[Map[String, Any]](map) match {
      case Left(serializeErrorStr) ⇒ Left(serializeErrorStr)
      case Right(jsonString) ⇒ {
        fromJson[SystemEvent](jsonString) match {
          case Left(deserializeErrorStr) ⇒ Left(deserializeErrorStr)
          case result                    ⇒ result
        }
      }
    }
  }

  private def publisherActorProps(theProducer: ActorRef, endPoint: String, routeKey: String): Props = {
    Props(new PublisherActor() {
      override implicit val formats: Formats = DefaultFormats
      override val ApplicationId = "InputEvents"
      override val producer = theProducer
      override val producerEndpoint = endPoint

      override def producerRouteKey(msg: AnyRef) = Option(routeKey)
    })
  }
}
