package csc.input_events

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 3/17/15.
 */

case class SyslogMessage(remoteAddress: Option[String], text: String, timestamp: Long = System.currentTimeMillis()) {
  val regex = "<(\\d+)>.*".r
  def sequenceNumber: Int = {
    text match {
      case regex(numberString) ⇒ numberString.toInt
    }
  }
}

object SyslogMessage {
  implicit def reverseOrderingBySequenceNumber[A <: SyslogMessage]: Ordering[A] =
    Ordering.by { s ⇒ -s.sequenceNumber }

}
