package csc.input_events.client

import java.net.URI

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 2/22/15.
 */

trait UriClient {
  def get[T <: AnyRef: Manifest](uri: URI): Either[String, T]
}

trait UriClientFactory {
  def getClientForUri(uri: URI): UriClient
  def shutdown()
}

case class SchemeFactoryMap(protected val schemeMap: Map[String, UriClientFactory]) {
  def factoryForScheme(scheme: String) = schemeMap(scheme)
  def factoryForUri(uri: URI) = factoryForScheme(uri.getScheme)

  def factoryOptionForScheme(scheme: String) = schemeMap.get(scheme)
  def factoryOptionForUri(uri: URI) = factoryOptionForScheme(uri.getScheme)

  def shutdown() {
    schemeMap.values.foreach(_.shutdown())
  }
}
