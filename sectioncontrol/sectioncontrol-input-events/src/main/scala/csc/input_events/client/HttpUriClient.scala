package csc.input_events.client

import java.net.URI

import net.liftweb.json.{ Formats, DefaultFormats }

import scala.io.Source
import csc.amqp.JsonSerializers
import csc.json.lift.EnumerationSerializer
import csc.input_events.SystemStateType

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 3/11/15.
 */

class HttpUriClient(protected val uri: URI) extends UriClient with JsonSerializers {
  override val formats: Formats = DefaultFormats + new EnumerationSerializer(SystemStateType)

  def get[T <: AnyRef: Manifest](uri: URI): Either[String, T] = {
    try {
      fromJson[T](httpGet(uri))
    } catch {
      case e: Exception ⇒ Left(e.getMessage)
    }
  }

  protected def httpGet(uri: URI): String = {
    val source = Source.fromURL(uri.toURL)
    val content = try {
      source.mkString
    } finally {
      source.close()
    }
    content
  }
}

class HttpUriClientFactory extends UriClientFactory {
  def getClientForUri(uri: URI): UriClient = new HttpUriClient(uri)

  def shutdown() {}
}
