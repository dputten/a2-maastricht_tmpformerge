package csc.input_events.client

import java.net.URI

import akka.event.LoggingAdapter
import csc.akkautils.DirectLoggingAdapter
import csc.curator.utils.{ Curator, CuratorToolsImpl }
import net.liftweb.json._
import org.apache.curator.RetryPolicy
import org.apache.curator.framework.CuratorFrameworkFactory
import org.apache.curator.retry.RetryUntilElapsed

import scala.collection.mutable

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 2/22/15.
 */

class ZkUriClient(protected val uri: URI) extends UriClient {
  require(uri != null)
  require(uri.getUserInfo == null)

  protected val curator: Curator = createCuratorWithLogger(new DirectLoggingAdapter(this.getClass.getName), zkServers = uri.getAuthority)

  def get[T <: AnyRef: Manifest](uri: URI): Either[String, T] = {
    try {
      val result = curator.get[T](uri.getPath).get
      Right(result)
    } catch {
      case e: Exception ⇒ Left(e.getMessage)
    }
  }

  protected def createCuratorWithLogger(logger: LoggingAdapter, retryPolicy: RetryPolicy = new RetryUntilElapsed(1000 * 60 * 60 * 24 * 3, 5000), zkServers: String = "localhost:2181", extraFormats: Formats = DefaultFormats): Curator = {

    val curator = CuratorFrameworkFactory.newClient(zkServers, retryPolicy)

    curator.start()

    new CuratorToolsImpl(Some(curator), logger, extraFormats) {
      override protected def extendFormats(defaultFormats: Formats) = formats
    }
  }
  def shutdown() {
    curator.curator.foreach(_.close())
  }
}

class ZkUriClientFactory extends UriClientFactory {
  protected val authorityMap = new mutable.HashMap[String, ZkUriClient]

  def getClientForUri(uri: URI): UriClient = {
    authorityMap.get(uri.getAuthority) match {
      case None ⇒
        val client = new ZkUriClient(uri)
        authorityMap(uri.getAuthority) = client
        client
      case Some(client) ⇒ client
    }
  }

  def shutdown() {
    authorityMap.values.foreach(_.shutdown())
    authorityMap.clear()
  }
}