package csc.input_events

import akka.actor.{ Actor, ActorLogging, ActorRef }
import akka.event.LoggingReceive
import com.github.sstone.amqp.Amqp.Publish
import com.rabbitmq.client.AMQP.BasicProperties
import net.liftweb.json._
/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 3/2/15.
 */

case class AmqpMessage(endpoint: String, routingKey: String, payload: Map[String, Any])

class AmqpEventProducer(protected val producer: ActorRef)
  extends Actor with ActorLogging {
  implicit def formats: DefaultFormats.type = DefaultFormats

  def receive = LoggingReceive {
    case AmqpMessage(endpoint, routingKey, payload) ⇒ {
      val jsonString = Serialization.write(payload)
      send(endpoint, routingKey, jsonString)
    }
  }

  protected def send(endpoint: String, routingKey: String, data: String): Unit =
    send(endpoint, routingKey, data.getBytes)

  protected def send(endpoint: String, routingKey: String, buf: Array[Byte]): Unit = {
    val headers = Some(new BasicProperties.Builder().
      deliveryMode(2 /*AMQP.BasicProperties.PERSISTANT_BASIC*/ ).build())
    val msg = Publish(endpoint, routingKey, buf, properties = headers, mandatory = true, immediate = false)
    producer ! msg
  }
}
