package csc.input_events

import akka.actor.{ ActorLogging, ActorRef, Cancellable }
import akka.camel.{ CamelMessage, Consumer }
import csc.input_events.config.{ BounceEliminatorConfig, SwitchConfig }

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 3/17/15.
 */

class BounceEliminator(protected val config: BounceEliminatorConfig, protected val receivers: Set[ActorRef])
  extends Consumer with ActorLogging {

  // Maps SwitchConfigs to a timed message
  private var timersForSwitches = Map[SwitchConfig, (Cancellable, Seq[SyslogMessage])]()

  def endpointUri = config.syslogEndpoint

  def receive = {
    case syslogMessage: SyslogMessage ⇒
      messageReceived(syslogMessage)
    case msg: CamelMessage ⇒
      // Received a message (from Syslog). Set timer to eliminate the bouncing of the switch
      // The socket address of the remote machine that send the message.
      val remoteAddress = msg.getHeader("CamelMinaRemoteAddress") match {
        case s: String ⇒ Some(s)
        case _         ⇒ None
      }
      val syslogMessage = SyslogMessage(remoteAddress, msg.bodyAs[String].replaceAll("\\n", ""))

      messageReceived(syslogMessage)

    // Timer on the switch expired. Forward last message from this timer
    // to all receivers
    case s: SwitchConfig ⇒
      switchExpired(s)

    // For testing purposes only
    case msg: String ⇒
      messageReceived(SyslogMessage(Some("localhost"), msg))
  }

  protected def messageReceived(msg: SyslogMessage): Unit = {
    val switches = config(msg.text)
    switches foreach { switch ⇒
      if (switch.delay.toMillis > 0)
        updateSwitch(switch, msg)
      else {
        // If zero delay we will not check for bounces
        receivers foreach { r ⇒ r ! msg }
      }
    }
  }

  protected def updateTimersForMessage(msg: SyslogMessage): Unit = {
    // update all matching timersForSwitches (see BounceEliminatorConfig#apply)
    config(msg.text) foreach ((switch) ⇒ {
      updateSwitch(switch, msg)
    })
  }

  protected def updateSwitch(switch: SwitchConfig, newMessage: SyslogMessage): Unit = {
    timersForSwitches.get(switch) match {
      case None ⇒
        // Switch is not in map, schedule a timer for this switch
        val timer = context.system.scheduler.scheduleOnce(switch.delay, self, switch)
        timersForSwitches = timersForSwitches + (switch -> (timer, Seq(newMessage)))
      case Some((timer, bouncedMessages)) ⇒
        // Switch is in map, reschedule the new message for this Switch
        timer.cancel()

        val newTimer = context.system.scheduler.scheduleOnce(switch.delay, self, switch)
        timersForSwitches = timersForSwitches - switch + (switch -> Tuple2(newTimer, newMessage +: bouncedMessages))
    }
  }

  protected def switchExpired(s: SwitchConfig): Unit = {
    timersForSwitches.get(s) match {
      // Oops, the switch for which a timer expired is not in the map. Ignore
      // since it most likely is caused by a race condition
      case None ⇒
      // Switch has a corresponding entry, send last message to receivers
      case Some((_, messages)) ⇒
        val message = messages.sorted.head
        timersForSwitches = timersForSwitches - s
        receivers.foreach(_ ! message)
    }
  }
}
