package csc.input_events

import akka.actor.{ Actor, ActorRef }
import csc.snmp.TrapMessage
import csc.akkautils.DirectLogging

/*
Class to change the incoming trapMessage so it can be processed by SyslogConsumer and the BounceEliminator
 */
class SnmpConsumer(listener: ActorRef) extends Actor with DirectLogging {
  def receive = {
    case trapMessage: TrapMessage ⇒
      val syslogMessage = SyslogMessage(trapMessage.agentAddress, trapMessage.text, trapMessage.timestamp)
      log.info("trapMessage received: {}, transformed to SysLogMessage: {}", trapMessage, syslogMessage)
      listener ! syslogMessage
  }
}
