package csc.input_events

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 2/16/15.
 */

import akka.actor.{ ActorRef, Props }
import com.github.sstone.amqp.{ Amqp, RabbitMQConnection }
import csc.akkautils.GenericBoot
import csc.input_events.client.{ HttpUriClientFactory, SchemeFactoryMap, ZkUriClientFactory }
import csc.input_events.config.{ BounceEliminatorConfig, SyslogConsumerConfig }
import csc.snmp.{ SystemNow, SnmpConfig, SnmpTrapReceiver }

class Boot extends GenericBoot {
  var snmpReceiver: SnmpTrapReceiver = _

  def componentName = "Input-Events"
  /**
   * Implemented by subclasses to start up any required actors in the actor system.
   */
  def startupActors() {
    implicit val system = actorSystem

    // Create factories for each of the supported schemes
    val schemeFactoryMap = new SchemeFactoryMap(Map(
      "zk" -> new ZkUriClientFactory,
      "http" -> new HttpUriClientFactory))

    val appConfig = actorSystem.settings.config

    val amqpConfig = new AmqpConnectionConfig("amqpConnection", appConfig)
    val rmqConnection = RabbitMQConnection.create(amqpConfig.actorName, amqpConfig.amqpUri,
      amqpConfig.reconnectDelay)
    require(rmqConnection != null)

    // Create producer and wait until it's connected to the broker
    val producer = rmqConnection.createChannelOwner()
    Amqp.waitForConnection(actorSystem, producer).await()

    val syslogConsumerConfig = SyslogConsumerConfig("syslogConsumer", appConfig)
    val syslogConsumer = actorSystem.actorOf(Props(
      new SyslogConsumer(syslogConsumerConfig, schemeFactoryMap, producer)),
      syslogConsumerConfig.actorName.getOrElse("SyslogConsumer"))

    val bounceEliminatorConfig = BounceEliminatorConfig("bounceEliminator", appConfig)
    val bounceEliminator = actorSystem.actorOf(Props(new BounceEliminator(bounceEliminatorConfig, Set(syslogConsumer))))

    // ip-address read from config
    // port read from config
    val snmpConsumer: ActorRef = actorSystem.actorOf(Props(new SnmpConsumer(bounceEliminator)))
    val snmpConfig = SnmpConfig(appConfig)
    snmpReceiver = new SnmpTrapReceiver(snmpConfig, snmpConsumer, new SystemNow())
    snmpReceiver.start()
  }

  def shutdownActors() {
    snmpReceiver.stop()
  }
}