package csc.input_events.config

import com.typesafe.config.Config
import scala.collection.JavaConversions._

case class SyslogConsumerConfig(actorName: Option[String],
                                amqpEndpoint: String,
                                regexToStateUris: Seq[RegexWithSystemStateUri],
                                regexToMaps: Seq[RegexWithAnyMap]) {

  /**
   * Collect the set of RegexWithSystemStateUri's where the event matches the regex
   * @param s the String to match against the regex
   * @return Set[RegexWithSystemStateUri] matching the event
   */
  def apply(s: String): Set[RegexWithSystemStateUri] = {
    regexToStateUris.map((r: RegexWithSystemStateUri) ⇒ r(s)).flatMap((x) ⇒ x).toSet
  }

  /**
   * Collect the set of RegexWithAnyMap where the event matches the regex
   * @param event the event to match
   * @return
   */
  def regexWithAnyMapsMatching(event: String): Set[RegexWithAnyMap] = {
    regexToMaps.map((regexToTemplate) ⇒ regexToTemplate(event)).flatMap((x) ⇒ x).toSet
  }
}

object SyslogConsumerConfig {
  def apply(configPath: String, conf: Config) = {
    val path = if (configPath.length > 0) configPath + "." else configPath
    val actorName = if (conf.hasPath(path + "actorName")) Some(conf.getString(path + "actorName")) else None
    val regexToStateSeq: Seq[Config] = conf.getConfigList(path + "regexToStateUris")
    val regexToSystemStateUris = regexToStateSeq.map((c: Config) ⇒ new RegexWithSystemStateUri(c))

    val regexToSystemEventTemplateSeq: Seq[Config] = conf.getConfigList(path + "regexToSystemEvents")
    val regexToSystemEventTemplates = regexToSystemEventTemplateSeq.map((c: Config) ⇒ new RegexWithAnyMap(c))

    new SyslogConsumerConfig(
      actorName,
      conf.getString(path + "amqpEndpoint"),
      regexToSystemStateUris,
      regexToSystemEventTemplates)
  }
}
