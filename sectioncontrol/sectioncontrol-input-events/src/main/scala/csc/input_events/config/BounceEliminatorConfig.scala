package csc.input_events.config

import com.typesafe.config.Config

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 3/17/15.
 */

case class BounceEliminatorConfig(actorName: String, syslogEndpoint: String, switches: Set[SwitchConfig]) {
  /**
   * Apply a String. Collect the switches for which the string matches their regex.
   * See SwitchConfig#apply
   * @param s The string to match against the regex of the individual switches
   * @return The set of switches where s matches the switches regex
   */
  def apply(s: String): Set[SwitchConfig] = switches.filter(_(s))
}

object BounceEliminatorConfig {
  import scala.collection.JavaConversions._

  /**
   * Apply a Config instance => Instantiate a BounceEliminatorConfig using a Config
   * @param conf the config instance to apply
   * @return a BounceEliminatorConfig instance
   */
  def apply(configPath: String, conf: Config) = {
    val path = if (configPath.length > 0) configPath + "." else configPath
    val switchConfigs = conf.getConfigList(path + "switches").map {
      (switchConfig: Config) ⇒ new SwitchConfig(switchConfig)
    }.toSet
    new BounceEliminatorConfig(
      conf.getString(path + "actorName"),
      conf.getString(path + "syslogEndpoint"),
      switchConfigs)
  }
}
