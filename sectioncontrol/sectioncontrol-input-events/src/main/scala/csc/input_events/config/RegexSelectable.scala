package csc.input_events.config

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 3/11/15.
 */

/**
 * An containing a regex so it can be selected by strings matching the regex
 * @tparam T the type of RegexSelectable
 */
trait RegexSelectable[T] {
  def regex: String
  def asOption: Option[T]

  /**
   * Apply a String. If the String matches the regex return
   * a Some(this) otherwise return None
   * @param s
   * @return
   */
  def apply(s: String): Option[T] =
    if (s.matches(regex)) this.asOption else None
}
