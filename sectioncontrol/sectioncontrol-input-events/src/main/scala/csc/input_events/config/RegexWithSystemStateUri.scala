package csc.input_events.config

import java.net.URI
import com.typesafe.config.Config

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 2/16/15.
 */

/**
 * RegexWithSystemStateUri. (Input) Events received via syslog are matched
 * against a list of RegexWithSystemStateUri.
 * @param regex If a message matches the regex, uri must be used
 *              to obtain the system state.
 * @param uri the address to be used to determine the system state.
 *            What this actually represents is implementation-specific.
 *            Could be a http-uri to post a get-request, a zookeeper-node
 *            holding system-state, etc...
 */
case class RegexWithSystemStateUri(regex: String, uri: URI)
  extends RegexSelectable[RegexWithSystemStateUri] {

  def this(regex: String, uriString: String) = this(regex, new URI(uriString))
  def this(c: Config) = this(c.getString("regex"), new URI(c.getString("uri")))

  def asOption = Some(this)
}
