package csc.input_events.config

import scala.collection.JavaConversions._
import com.typesafe.config.Config
import akka.util.Duration
import akka.util.duration._
/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 3/17/15.
 */

case class SwitchConfig(id: String, delay: Duration, regexen: Set[String]) {
  def this(id: String, delay_ms: Long, regexList: Seq[String]) = this(id, delay_ms.millis, regexList.toSet)
  def this(conf: Config) = this(conf.getString("id"), conf.getLong("delay_ms"), conf.getStringList("regexList"))
  def apply(s: String): Boolean = regexen.exists((regex: String) ⇒ s matches regex)
}
