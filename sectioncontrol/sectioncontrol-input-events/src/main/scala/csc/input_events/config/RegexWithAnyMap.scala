package csc.input_events.config

import com.typesafe.config.Config
import scala.collection.JavaConversions._

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 3/3/15.
 */

/**
 * This class is used to select templates for a SystemEvent based on a string.
 * It binds the template to the input message via a regular expression. If the input
 * matches the regex, the template is to be used to construct a system event and
 * send it to the eventbus
 * @param regex, a regular expression used for selection purposes.
 * @param routingKeys, the routingKeys used when sending over AMQP
 * @param template the template for constructing a SystemEvents instance.
 */
case class RegexWithAnyMap(regex: String, routingKeys: Set[String], template: Map[String, Any])
  extends RegexSelectable[RegexWithAnyMap] {

  def this(c: Config) = this(c.getString("regex"), c.getStringList("routingKeys").toSet, c.getObject("systemEventTemplate").unwrapped().toMap)

  def asOption: Option[RegexWithAnyMap] = Some(this)

  def routingMapWithPrefixAndTimestamp(prefix: String, timestamp: Long): (Set[String], Map[String, Any]) = {
    (routingKeys, complementMap(prefix, timestamp))
  }

  def complementMap(eventPrefix: String, timestamp: Long) = {
    val eventTypeFmt = template("eventTypeFormat").asInstanceOf[String]
    template - "eventTypeFormat" +
      ("eventType" -> eventTypeFmt.format(eventPrefix), "timestamp" -> timestamp)
  }
}
