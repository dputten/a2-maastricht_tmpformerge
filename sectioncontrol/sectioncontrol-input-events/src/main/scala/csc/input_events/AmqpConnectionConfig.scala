package csc.input_events

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 2/23/15.
 */

import com.typesafe.config.Config
import akka.util.duration._
import akka.util.Duration

case class AmqpConnectionConfig(actorName: String, amqpUri: String, reconnectDelay: Duration) {
  def this(configPath: String, config: Config) = {
    this(
      config.getString(configPath + ".actorName"),
      config.getString(configPath + ".amqpUri"),
      config.getInt(configPath + ".reconnectDelay_ms") millis)
  }
}
