package csc.imageretriever

import akka.util.FiniteDuration
import akka.util.duration._
import com.typesafe.config.Config
import csc.imageretriever.ImageRetrieverConfig.{ AmqpConfig, AmqpServicesConfig, HBaseConfig }

/**
 * Created by carlos on 10.08.15.
 */
case class ImageRetrieverConfig(amqpConnection: AmqpConfig,
                                amqpServices: AmqpServicesConfig,
                                hBase: HBaseConfig)

object ImageRetrieverConfig {

  val rootKey = "image-retriever"
  val amqpKey = "amqp-connection"
  val amqpServicesKey = "amqp-services"
  val hBaseKey = "hbase"

  class AmqpConfig(val actorName: String, val amqpUri: String, val reconnectDelay: FiniteDuration) {
    def this(config: Config) = this(
      config.getString("actor-name"),
      config.getString("amqp-uri"),
      config.getMilliseconds("reconnect-delay").toLong.millis)
  }

  class AmqpServicesConfig(val consumerQueue: String, val producerEndpoint: String, val serversCount: Int) {
    def this(config: Config) = this(
      config.getString("consumer-queue"),
      config.getString("producer-endpoint"),
      config.getInt("servers-count"))
  }

  class HBaseConfig(val servers: String) {
    def this(config: Config) = this(config.getString("servers"))
  }

  def apply(config: Config): ImageRetrieverConfig = {
    val root = config.getConfig(rootKey)
    val amqpConnection = new AmqpConfig(root.getConfig(amqpKey))
    val amqpServices = new AmqpServicesConfig(root.getConfig(amqpServicesKey))
    val hBase = new HBaseConfig(root.getConfig(hBaseKey))
    new ImageRetrieverConfig(amqpConnection, amqpServices, hBase)
  }

}
