package csc.imageretriever

import csc.akkautils.DirectLogging
import csc.hbase.utils.HBaseTableKeeper
import csc.sectioncontrol.messages.VehicleImageType
import csc.sectioncontrol.messages.VehicleImageType.VehicleImageType
import csc.sectioncontrol.storagelayer.hbasetables.VehicleImageTable
import net.liftweb.json.Formats
import org.apache.hadoop.conf.{ Configuration ⇒ HadoopConfiguration }
import org.apache.hadoop.hbase.HBaseConfiguration

/**
 * Abstraction over HBase for image operations.
 * Created by carlos on 10.08.15.
 */
trait HBaseFunctions {
  /**
   * Must be called once, before any other method
   */
  def start()

  /**
   * Must be called in the end, for closing resources
   */
  def stop()

  def writeImage(key: ImageKey, data: Array[Byte]): Option[String]

  def readImage(key: ImageKey): Option[Array[Byte]]
}

class HBaseFunctionsImpl(config: HadoopConfiguration, tableKeeper: HBaseTableKeeper, jsonFormats: Formats)
  extends HBaseFunctions
  with DirectLogging {

  var readers: Map[VehicleImageType, VehicleImageTable.Reader] = Map()
  var writers: Map[VehicleImageType, VehicleImageTable.Writer] = Map()

  def start(): Unit = {
    readers = mapOf[VehicleImageTable.Reader](VehicleImageTable.makeReader(config, tableKeeper, _))
    writers = mapOf[VehicleImageTable.Writer](VehicleImageTable.makeWriter(config, tableKeeper, _))
  }

  private def mapOf[T](mapper: VehicleImageType ⇒ T): Map[VehicleImageType, T] =
    VehicleImageType.values map { tp ⇒ tp -> mapper(tp) } toMap

  def stop(): Unit = tableKeeper.closeTables()

  def readerKey(key: ImageKey): (String, Long, String) = {
    val res = ("%s-%s".format(key.systemId, key.gantry), key.time, key.lane)
    log.debug("KEY to search for image = " + res)
    res
  }

  def writerKey(key: ImageKey): (String, String, Long, String) = (key.systemId, key.gantry, key.time, key.lane)

  override def writeImage(key: ImageKey, data: Array[Byte]): Option[String] = {
    val id = writerKey(key)
    writers.get(key.imageType) match {
      case Some(writer) ⇒ try {
        writer.writeRow(id, data)
        None //no error
      } catch {
        case ex: Exception ⇒
          log.error(ex, "Error writing image {}", id)
          Some(ex.getMessage)
      }
      case None ⇒ Some("No writer for %s".format(key.imageType))
    }
  }

  override def readImage(key: ImageKey): Option[Array[Byte]] = readers.get(key.imageType) match {
    case Some(reader) ⇒ reader.readRow(readerKey(key))
    case None         ⇒ None
  }
}

object HBaseFunctions {
  def apply(config: ImageRetrieverConfig, jsonFormats: Formats): HBaseFunctions = {
    val hBaseConfig = HBaseTableKeeper.createCachedConfig(config.hBase.servers)
    val tk = new HBaseTableKeeper {}
    new HBaseFunctionsImpl(hBaseConfig, tk, jsonFormats)
  }
}
