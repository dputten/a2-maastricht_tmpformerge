package csc.imageretriever

import akka.util.duration._
import com.typesafe.config.Config
import csc.akkautils.DirectLogging

/**
 * Created by carlos on 10.08.15.
 */
class Configuration(config: Config) {
  private val root = config.getConfig("image-receiver")

  import Configuration._
  val amqpConnection = new AmqpConfiguration(root.getConfig("amqp-connection"))
  val amqpServices = new AmqpServicesConfiguration(root.getConfig("amqp-services"))
  val hBase = new HBaseConfiguration(root.getConfig("hbase"))
  //val zookeeper = new ZookeeperConfiguration(root.getConfig("zookeeper"))
}

object Configuration extends DirectLogging {
  class AmqpConfiguration(config: Config) {
    val actorName = config.getString("actor-name")
    val amqpUri = config.getString("amqp-uri")
    val reconnectDelay = config.getMilliseconds("reconnect-delay").toLong.millis
  }

  class AmqpServicesConfiguration(val consumerQueue: String, val producerEndpoint: String, val serversCount: Int) {
    def this(config: Config) = this(
      config.getString("consumer-queue"),
      config.getString("producer-endpoint"),
      config.getInt("servers-count"))
  }

  class HBaseConfiguration(val servers: String) {
    def this(config: Config) = this(config.getString("servers"))
  }

  //  class ZookeeperConfiguration(val servers: String, val sessionTimeout: Duration, val retries: Int) {
  //    def this(config: Config) = this(config.getString("servers"),
  //      config.getMilliseconds("session-timeout").toLong.millis,
  //      config.getInt("retries"))
  //  }

}
