/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.imageretriever

import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.messages.VehicleImageType
import net.liftweb.json._

/**
 * @author csomogyi
 */
object JsonSerializers {

  val formats = DefaultFormats + new EnumerationSerializer(VehicleImageType) + new JsonSerializers.ImageGetSerializer()

  private def stringOrNull(value: JValue): String = {
    value match {
      case JNull     ⇒ null
      case x: JValue ⇒ x.values.toString
    }
  }

  private def stringOption(value: JValue): Option[String] = {
    value match {
      case JNull     ⇒ None
      case x: JValue ⇒ Some(x.values.toString)
    }
  }

  private def byteArrayOrNull(value: JValue): Array[Byte] = {
    value match {
      case JNull ⇒ null
      case x: JArray ⇒ {
        x.arr.foreach { x ⇒ if (x.getClass != classOf[JInt]) throw new MappingException("Array contains non-integer") }
        x.arr.map(_.asInstanceOf[JInt]).map(_.values.toByte).toArray
      }
      case _ ⇒ throw new MappingException("Not an array")
    }
  }

  /**
   * Because of the type erasure one cannot create a serializer of Array[Byte]. Therefore we have to create a full
   * serializer for the class having Array of Byte member.
   */
  class ImageGetSerializer extends Serializer[ImageGetResponse] {
    val ImageGetResponseClass = classOf[ImageGetResponse]

    override def deserialize(implicit format: Formats): PartialFunction[(TypeInfo, JValue), ImageGetResponse] = {
      case (TypeInfo(ImageGetResponseClass, _), json) ⇒ {
        val refId = json \\ "refId"
        val key = json \\ "key"
        val creationTime = json \\ "creationTime"
        val image = json \\ "image"
        val checksum = json \\ "checksum"
        val error = json \\ "error"
        ImageGetResponse(stringOrNull(refId), stringOrNull(key), stringOrNull(creationTime).toLong,
          byteArrayOrNull(image), stringOrNull(checksum), stringOption(error))
      }
    }

    override def serialize(implicit format: Formats): PartialFunction[Any, JValue] = {
      case r: ImageGetResponse ⇒ {
        val refId: JValue = if (r.refId != null) JString(r.refId) else JNull
        val key: JValue = if (r.key != null) JString(r.key) else JNull
        val creationTime: JValue = JString(r.creationTime.toString) // Long does not fit into JInt
        val image = if (r.image != null) JArray(r.image.map(_.toInt).map(JInt(_)).toList) else JNull
        val checksum: JValue = if (r.checksum != null) JString(r.checksum) else JNull
        val error: JValue = if (r.error.isDefined) JString(r.error.get) else JNull
        JObject(List(
          JField("refId", refId),
          JField("key", key),
          JField("creationTime", creationTime),
          JField("image", image),
          JField("checksum", checksum),
          JField("error", error)))
      }
    }
  }

}
