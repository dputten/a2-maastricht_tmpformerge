/*
 * Copyright (C) 2015. CSC <http://www.csc.com>
 */

package csc.imageretriever

import java.util.Arrays

trait Referenced {
  def refId: String
}

/**
 * Request message for getting an image. Responded with [[ImageGetResponse]]
 *
 * @author csomogyi
 * @param refId a reference ID specified by the initiator - copied back in the response to provide correlation
 * @param key the key of the image, should contain only alphanumeric and some specials, eg. at-sign (@)
 */
case class ImageGetRequest(refId: String, key: String) extends Referenced

/**
 *
 * Response to a an [[ImageGetRequest]]. If the image is in the store it returns the [[creationTime]], the image
 * content in the field [[image]] and an MD5 [[checksum]].
 *
 * Custom [[equals()]] must have been implemented due to different behavior on [[Array]] equality
 *
 * @param refId ID specified by the caller in the request
 * @param key Key of the requested image
 * @param creationTime the creation time of the image, the precision is seconds although it is expressed in milliseconds
 * @param image The image in a byte array - null on error
 * @param checksum MD5 checksum of the image - null on error
 * @param error error message when the get has failed
 */
case class ImageGetResponse(refId: String, key: String, creationTime: Long,
                            image: Array[Byte], checksum: String, error: Option[String]) {
  override def equals(that: Any): Boolean = that match {
    case ImageGetResponse(refId1, key1, creationTime1, image1, checksum1, error1) ⇒
      refId == refId1 && key == key1 && creationTime == creationTime1 && Arrays.equals(image, image1) &&
        checksum == checksum1 && error == error1
    case _ ⇒ false
  }
}

/**
 * Create a request of batch get of images. It is responded with a series of [[ImageGetResponse]] messages and some
 * [[ImageBatchProgressResponse]] indicating the progress.
 *
 * @param refId arbitrary reference id used by the requestor to distinguish between the different requests
 * @param keys keys of the requested images
 */
case class ImageBatchGetRequest(refId: String, keys: Seq[String]) extends Referenced {
  lazy val requests = keys.map(ImageGetRequest(refId, _))
}

/**
 * A progress message indicating how many successful or failed attempts of image requests are processed in the batch
 * so far.
 *
 * @param refId reference id used by the requestor
 * @param total the total number of images requested
 * @param count the number of images sent back so far
 * @param errorCount the number of images not found or failed to provide so far
 */
case class ImageBatchProgressResponse(refId: String, total: Int, count: Int, errorCount: Int)

case class RoutedRequest(systemId: String, gantry: String, request: Referenced)