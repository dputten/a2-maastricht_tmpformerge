package csc.imageretriever.actor

import akka.actor.{ ActorLogging, Actor, ActorRef }
import csc.imageretriever._

/**
 * Worker actor for dealing with a single GetImageRequest.
 * This actor will stop once the request is fully handled.
 *
 * The flow for this actor is:
 * 1- receive (GetImageRequest)
 * 2- databaseGetReceive
 * 3- storeGetReceive
 * 4- ackDatabaseReceive
 * 5- complete
 *
 * Created by carlos on 18.08.15.
 */
class RetrieveSingleImageWorker(database: ActorRef, imageStore: ActorRef)
  extends Actor with ActorLogging {

  /**
   * Initial receive behaviour, accepting GetImageRequest
   * @return
   */
  override protected def receive: Receive = {
    case msg: GetImageRequest ⇒ {
      val state = State(sender, msg, null)
      database ! msg //check the database
      context.become(databaseGetReceive(state)) //waiting for the database get response
    }
  }

  /**
   * Receive behaviour when expecting GetImageResponse from database
   * @param state
   * @return
   */
  def databaseGetReceive(state: State): Receive = {
    case msg: GetImageResponse ⇒ msg.contents match {
      case Some(_) ⇒ complete(state, msg) //already in database: complete
      case None ⇒ {
        imageStore ! state.request //try the store
        context.become(storeGetReceive(state)) //waiting for the store get response
      }
    }
  }

  /**
   * Receive behaviour when expecting the GetImageResponse from ImageStore
   * @param state
   * @return
   */
  def storeGetReceive(state: State): Receive = {
    case msg: GetImageResponse ⇒ msg.contents match {
      case Some(data) ⇒ {
        database ! StoreImageRequest(msg.refId, state.request.key, data)
        context.become(ackDatabaseReceive(state.copy(response = msg))) //waiting for database save confirmation
      }
      case None ⇒ complete(state, msg) //not found: complete
    }
  }

  /**
   * Receive behaviour when expecting the StoreImageResponse (save confirmation from ImageStore)
   * @param state
   * @return
   */
  def ackDatabaseReceive(state: State): Receive = {
    case msg: StoreImageResponse ⇒ msg.error match {
      case Some(error) ⇒
        log.warning("Unable to persist image {} in database: {}", state.request.key, error)
        complete(state, GetImageResponse(state.request.refId, msg.key, None, Some(error))) //not saved: complete with error
      case None ⇒ complete(state, state.response) //saved successfully: complete
    }
  }

  /**
   * Completes a single image request: sends the result and stops the actor
   * @param state
   * @param msg
   */
  def complete(state: State, msg: GetImageResponse): Unit = {
    state.client ! msg //reply
    context.stop(self) //stop
  }

  case class State(client: ActorRef, request: GetImageRequest, response: GetImageResponse)

}
