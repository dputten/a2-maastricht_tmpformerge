package csc.imageretriever.actor

import akka.actor.ActorRef
import csc.amqp.AmqpSerializers.Deserializer
import csc.amqp.GenericConsumer
import csc.imageretriever.{ ImageBatchProgressResponse, JsonSerializers, ImageGetResponse }
import net.liftweb.json.Formats

/**
 * Created by carlos on 11.08.15.
 */
class ImageStoreConsumer(handler: ⇒ ActorRef) extends GenericConsumer(handler) {

  override val ApplicationId: String = "ImageStoreConsumer"

  override implicit val formats: Formats = JsonSerializers.formats

  val deserializers: Map[String, Deserializer] = Map(
    "ImageGetResponse" -> toMessage[ImageGetResponse],
    "ImageBatchProgressResponse" -> toMessage[ImageBatchProgressResponse])

}
