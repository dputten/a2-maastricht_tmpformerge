package csc.imageretriever.actor

import akka.actor.{ Actor, ActorLogging }
import akka.dispatch.Future
import csc.imageretriever._

/**
 * Actor that interacts with HBase to retrieve and store images.
 * All the HBase functionality is hidden/abstracted in [[HBaseFunctions]]
 *
 * Created by carlos on 10.08.15.
 */
class HBaseClientActor(functions: HBaseFunctions) extends Actor with ActorLogging {

  implicit val executor = context.dispatcher

  override protected def receive: Receive = {
    case msg: GetImageRequest   ⇒ handleImageRequest(msg)
    case msg: StoreImageRequest ⇒ handleStoreRequest(msg)
  }

  def handleImageRequest(msg: GetImageRequest): Unit = {
    val client = sender
    val someData = functions.readImage(msg.key)
    val response = GetImageResponse(msg.refId, msg.key, someData, None)

    client ! response
    /*
    val client = sender

    Future {
      functions.readImage(msg.key)
    } onComplete { result ⇒
      val response = result match {
        case Right(data) ⇒ GetImageResponse(msg.refId, msg.key, data, None)
        case Left(error) ⇒ GetImageResponse(msg.refId, msg.key, None, Some(error.getMessage))
      }
      client ! response
    }
    */
  }

  def handleStoreRequest(msg: StoreImageRequest): Unit = {
    val client = sender
    val result = functions.writeImage(msg.key, msg.contents)
    val response = StoreImageResponse(msg.refId, msg.key, result)
    client ! response

    /*
    val client = sender
    Future {
      functions.writeImage(msg.key, msg.contents)
    } onComplete { result ⇒
      val response = result match {
        case Right(opt)  ⇒ StoreImageResponse(msg.refId, msg.key, opt)
        case Left(error) ⇒ StoreImageResponse(msg.refId, msg.key, Some(error.getMessage))
      }
      client ! response
    }
    */
  }

  override def preStart(): Unit = {
    functions.start()
  }

  override def postStop(): Unit = {
    functions.stop()
  }

}
