package csc.imageretriever.actor

import akka.actor.{ Actor, ActorLogging, ActorRef }
import csc.imageretriever._

/**
 * Worker actor for dealing with a GetImageBatchRequest.
 * This actor will stop once all the GetImageResponse for the batch are sent to the client
 *
 * There flow for this actor is:
 * 1- receive (GetImageBatchRequest)
 * 2- batchReceive
 * 3- finish when all the image responses have arrived
 *
 * Created by carlos on 10.08.15.
 */
class RetrieveBatchImageWorker(
  database: ActorRef,
  imageStore: ActorRef,
  batchProgressTreshold: Int) extends Actor with ActorLogging {

  /**
   * Initial receive behaviour: accept GetImageBatchRequest
   * @return
   */
  override protected def receive: Receive = {
    case msg: GetImageBatchRequest ⇒ {
      val total = msg.keys.size
      val state = BatchState(sender, BatchProgressResponse(msg.refId, total, 0, 0), Map())

      if (total == 0) {
        checkProgress(state) //nothing to actually do: will stop here
      } else {
        imageStore ! msg
        context.become(batchReceive(state))
      }
    }
  }

  /**
   * Receive behaviour when the batch is not yet complete (some GetImageResponse still to be sent)
   * @param state
   * @return
   */
  def batchReceive(state: BatchState): Receive = {
    case msg: GetImageResponse ⇒ (msg.contents, msg.error) match {
      case (Some(data), None) ⇒ {
        database ! StoreImageRequest(msg.refId, msg.key, data)
        val pendingMap = state.pendingSaveMap.updated(msg.key, msg) //add the key/response to the pending map
        context.become(batchReceive(state.copy(pendingSaveMap = pendingMap)))
      }
      case (_, Some(error)) ⇒ {
        val newProgress = state.progress.copy(errorCount = state.progress.errorCount + 1)
        completeBatchItem(state.copy(progress = newProgress), msg)
      }
      case (None, None) ⇒ {
        val newProgress = state.progress.copy(count = state.progress.count + 1)
        completeBatchItem(state.copy(progress = newProgress), msg)
      }

    }

    case msg: StoreImageResponse    ⇒ completeBatchItem(state, msg)
    case msg: BatchProgressResponse ⇒ //we ignore these, as the real progress to send to client is after database save
  }

  /**
   * Handles the StoreImageResponse, completing a batch item
   * @param state
   * @param msg
   */
  def completeBatchItem(state: BatchState, msg: StoreImageResponse): Unit = {
    var localProgress = state.progress
    var response: Option[GetImageResponse] = None

    msg.error match {
      case Some(error) ⇒ {
        log.warning("Unable to persist image {} in database: {}", msg.key, error)
        localProgress = localProgress.copy(errorCount = localProgress.errorCount + 1) //updates error count
        response = Some(GetImageResponse(msg.refId, msg.key, None, Some(error)))
      } case None ⇒ {
        response = state.pendingSaveMap.get(msg.key)
        if (response.isDefined) {
          localProgress = localProgress.copy(count = localProgress.count + 1) //updates count
        }
      }
    }

    response map { resp ⇒
      val pendingMap = state.pendingSaveMap - msg.key
      completeBatchItem(state.copy(progress = localProgress, pendingSaveMap = pendingMap), resp)
    }
  }

  /**
   * Sends the GetImageResponse to client and checks the progress
   * @param state
   * @param msg
   */
  def completeBatchItem(state: BatchState, msg: GetImageResponse): Unit = {
    state.client ! msg //reply
    checkProgress(state) //checks progress and termination
  }

  /**
   * Checks the progress, sends a BatchProgressResponse (if its the right moment), and might stop the actor, if batch finished
   * @param state
   */
  def checkProgress(state: BatchState): Unit = {
    val progress = state.totalProgress
    val finished = state.progress.total == progress

    if (finished || progress % batchProgressTreshold == 0) {
      state.client ! state.progress
    }

    if (finished) {
      context.stop(self) //stop
    } else {
      context.become(batchReceive(state)) //updates the state on the batch receive
    }
  }

  case class BatchState(client: ActorRef,
                        progress: BatchProgressResponse,
                        pendingSaveMap: Map[ImageKey, GetImageResponse]) {

    def totalProgress = progress.count + progress.errorCount
  }

}
