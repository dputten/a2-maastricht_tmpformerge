package csc.imageretriever.actor

import java.util.UUID
import akka.actor._
import com.github.sstone.amqp.Amqp.ChannelParameters
import com.github.sstone.amqp.{ Amqp, RabbitMQConnection }
import csc.imageretriever._

/**
 * Actor matching 'request sent to' / 'response received from' ImageStore over AMQP.
 * Its also responsible for bridging the protocol messages:
 * -GetImageRequest and GetImageBatchRequest converted to image store RoutedRequest
 * -image store ImageGetResponse converted to GetImageResponse
 * -image store ImageBatchProgressResponse converted to BatchProgressResponse
 *
 * Created by carlos on 11.08.15.
 */
class ImageStoreActor(producer: ActorRef) extends Actor with ActorLogging {
  sealed trait SimpleRequest { def externalRefId: String; def client: ActorRef }
  protected case class Request(externalRefId: String, client: ActorRef) extends SimpleRequest
  protected case class BatchRequest(externalRefId: String, client: ActorRef, uriMap: Map[String, ImageKey]) extends SimpleRequest

  protected case class State(pendingRequests: Map[ImageKey, Seq[Request]] = Map(),
                             // localRefId -> BatchRequest
                             pendingBatchRequests: Map[String, BatchRequest] = Map(),
                             // localRefId -> ImageKey
                             pendingResponses: Map[String, ImageKey] = Map())

  override protected def receive = receiveWithState(State())

  protected def receiveWithState(state: State): Receive = {
    case msg: GetImageRequest            ⇒ context.become(receiveWithState(handleImageRequest(state, msg)))
    case response: ImageGetResponse      ⇒ context.become(receiveWithState(handleImageResponse(state, response)))
    case msg: GetImageBatchRequest       ⇒ context.become(receiveWithState(handleBatchRequest(state, msg)))
    case msg: ImageBatchProgressResponse ⇒ context.become(receiveWithState(handleBatchProgressResponse(state, msg)))
  }

  protected def handleImageRequest(state: State, msg: GetImageRequest): State = {

    def placeNewRequest(state: State, msg: GetImageRequest, client: ActorRef): State = {
      val uuid = UUID.randomUUID().toString
      log.info("Placing new image request {} with uuid={}", msg, uuid)
      val getRequest = ImageGetRequest(uuid, msg.key.uri)
      val newState = nextState(state, uuid, msg.refId, msg.key, client)
      producer ! RoutedRequest(msg.key.systemId, msg.key.gantry, getRequest)
      newState
    }

    def appendRequest(state: State, msg: GetImageRequest, client: ActorRef): State = {
      log.info("Appending image request: {}", msg)
      nextState(state, msg.refId, msg.key, client)
    }

    log.info("handleImageRequest state: {}", state)
    val newState = if (state.pendingRequests.contains(msg.key))
      appendRequest(state, msg, sender)
    else
      placeNewRequest(state, msg, sender)
    log.info("handleImageRequest newState: {}", newState)
    newState
  }

  protected def nextState(state: State, localRefId: String, externalRefId: String, imageKey: ImageKey, client: ActorRef): State = {
    val newPendingResponses = state.pendingResponses + (localRefId -> imageKey)
    val newPendingRequests = state.pendingRequests + (imageKey -> Seq(Request(externalRefId, client)))
    state.copy(pendingRequests = newPendingRequests, pendingResponses = newPendingResponses)
  }

  protected def nextState(state: State, externalRefId: String, imageKey: ImageKey, client: ActorRef): State = {
    val newRequestList = Request(externalRefId, client) +: state.pendingRequests(imageKey)
    val newPendingRequests = state.pendingRequests.updated(imageKey, newRequestList)
    state.copy(pendingRequests = newPendingRequests)
  }

  private def handleImageResponse(state: State, msg: ImageGetResponse): State = {

    def processBatchImageResponse(state: State, msg: ImageGetResponse): State = {
      log.info("Processing batchImageResponse: {}", msg)
      val request = state.pendingBatchRequests(msg.refId)
      val imageKey = request.uriMap(msg.key)
      completeRequest(request, imageKey, msg)
      state
    }

    def processSingleImageResponse(state: State, msg: ImageGetResponse): State = {
      log.info("Process singleImageResponse: {}", msg)
      val imageKey = state.pendingResponses(msg.refId)
      val pendingRequestsForImage = state.pendingRequests(imageKey)
      pendingRequestsForImage.foreach((r) ⇒ completeRequest(r, imageKey, msg))
      val newPendingResponses = state.pendingResponses - msg.refId
      val newPendingRequests = state.pendingRequests - imageKey
      state.copy(pendingRequests = newPendingRequests, pendingResponses = newPendingResponses)
    }

    def completeRequest(request: SimpleRequest, imageKey: ImageKey, response: ImageGetResponse): Unit = {
      val getImageResponse = GetImageResponse(request.externalRefId, imageKey, Option[Array[Byte]](response.image), response.error)
      log.info("Completing request by sending response: {} to {}", getImageResponse, request.client.toString())
      request.client ! getImageResponse
    }

    log.info("handleImageResponse state: {}", state)
    val newState = if (state.pendingBatchRequests.contains(msg.refId)) {
      processBatchImageResponse(state, msg)
    } else if (state.pendingResponses.contains(msg.refId)) {
      processSingleImageResponse(state, msg)
    } else {
      log.warning("Did not find matching request for {}", msg.refId)
      state
    }
    log.info("handleImageResponse new state: {}", newState)
    newState
  }

  private def handleBatchRequest(state: State, msg: GetImageBatchRequest): State = {
    val client = sender
    val refId = msg.refId

    log.info("handleBatchRequest state: {}", state)
    val newState = if (msg.keys.isEmpty) {
      client ! BatchProgressResponse(refId, 0, 0, 0) //nothing to request
      state
    } else {
      val uuid = UUID.randomUUID().toString
      val req = ImageBatchGetRequest(uuid, msg.keys.map(_.uri))
      producer ! RoutedRequest(msg.systemId, msg.gantry, req)
      val emptyUriMap: Map[String, ImageKey] = Map()
      val uriMap = msg.keys.foldLeft(emptyUriMap) { (accu: Map[String, ImageKey], imageKey: ImageKey) ⇒ accu.updated(imageKey.uri, imageKey) }
      val batchRequest = BatchRequest(msg.refId, client, uriMap)
      val newBatchRequests = state.pendingBatchRequests.updated(uuid, batchRequest)
      state.copy(pendingBatchRequests = newBatchRequests)
    }
    log.info("handleBatchRequest new state: {}", newState)
    newState
  }

  private def handleBatchProgressResponse(state: State, msg: ImageBatchProgressResponse): State = {
    log.info("handleBatchProgressResponse state: {}", state)
    val newState = if (state.pendingBatchRequests.contains(msg.refId)) {
      val request = state.pendingBatchRequests(msg.refId)
      val batchProgressResponse = BatchProgressResponse(request.externalRefId, msg.total, msg.count, msg.errorCount)
      log.info("Sending batchProgressResponse: {}", batchProgressResponse)
      request.client ! batchProgressResponse
      if ((msg.total - msg.count - msg.errorCount) != 0) {
        state
      } else {
        log.info("Batchrequest {} completed", msg.refId)
        val newBatchRequests = state.pendingBatchRequests - msg.refId
        state.copy(pendingBatchRequests = newBatchRequests)
      }
    } else {
      log.warning("Did not find corresponding batch request for {}", msg.refId)
      state
    }
    log.info("handleBatchProgressResponse new state: {}", newState)
    newState
  }
}

object ImageStoreActor {

  def apply(actorSystem: ActorSystem, config: ImageRetrieverConfig): ActorRef = {
    val amqpConfig = config.amqpConnection
    val connection = RabbitMQConnection.create(amqpConfig.actorName, amqpConfig.amqpUri, amqpConfig.reconnectDelay)(actorSystem)

    val producer = connection.createChannelOwner()
    Amqp.waitForConnection(actorSystem, producer).await()

    val prod = actorSystem.actorOf(Props(new ImageStoreProducer(producer, config.amqpServices.producerEndpoint)))
    val imageStore = actorSystem.actorOf(Props(new ImageStoreActor(prod)))
    val listener = actorSystem.actorOf(Props(new ImageStoreConsumer(imageStore)))

    val consumer = connection.createSimpleConsumer(config.amqpServices.consumerQueue, listener, Some(ChannelParameters(100)), false)
    Amqp.waitForConnection(actorSystem, consumer).await()

    imageStore
  }
}