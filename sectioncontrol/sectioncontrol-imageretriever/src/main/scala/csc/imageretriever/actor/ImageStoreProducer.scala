package csc.imageretriever.actor

import akka.actor.{ ActorLogging, ActorRef }
import csc.amqp.{ AmqpSerializers, PublisherActor }
import csc.imageretriever.{ JsonSerializers, RoutedRequest }
import net.liftweb.json.Formats

/**
 * Created by carlos on 11.08.15.
 */
class ImageStoreProducer(val producer: ActorRef, val producerEndpoint: String)
  extends PublisherActor
  with ActorLogging
  with AmqpSerializers {

  override val ApplicationId: String = "ImageStoreProducer"

  override implicit val formats: Formats = JsonSerializers.formats

  override def messageToSend(msg: AnyRef): Option[AnyRef] = msg match {
    case RoutedRequest(_, _, msg) ⇒ Some(msg)
    case _                        ⇒ None
  }

  override def producerRouteKey(msg: AnyRef): Option[String] = msg match {
    case RoutedRequest(systemId, gantry, msg) ⇒
      Some(systemId + "." + gantry + "." + msg.getClass.getSimpleName)
    case _ ⇒ None
  }
}
