package csc.imageretriever.actor

import akka.actor._
import csc.imageretriever._

/**
 * Entry point actor for retrieving images.
 * This actor behaves as a receptionist, delegating each request to a new worker actor.
 *
 * Created by carlos on 10.08.15.
 */
class RetrieveImageActor(config: ImageRetrieverConfig,
                         batchProgressTreshold: Int = 10) extends Actor with ActorLogging {

  implicit val actorSystem = context.system
  var databaseActor: ActorRef = null
  var imageStoreActor: ActorRef = null

  override protected def receive: Receive = {
    case msg: GetImageRequest      ⇒ handleImageRequest(msg)
    case msg: GetImageBatchRequest ⇒ handleBatchRequest(msg)
    case unhandled                 ⇒ log.warning("Unhandled message: {}", unhandled)
  }

  def handleImageRequest(msg: GetImageRequest): Unit = {
    //adds a refId (copied from the key.uri) if not defined
    val toSend = if (msg.refId == null) msg.copy(refId = msg.key.uri) else msg
    delegate(toSend, false)
  }

  def handleBatchRequest(msg: GetImageBatchRequest): Unit = {
    //adds a composite refId, if not defined
    val toSend = if (msg.refId == null) msg.copy(refId = buildBatchRefId(msg)) else msg
    delegate(toSend, true)
  }

  def delegate(msg: Any, batch: Boolean): Unit = {
    val client = sender
    //creates a new worker actor, just for this request
    val props = batch match {
      case true  ⇒ Props(new RetrieveBatchImageWorker(databaseActor, imageStoreActor, batchProgressTreshold))
      case false ⇒ Props(new RetrieveSingleImageWorker(databaseActor, imageStoreActor))
    }
    val worker = context.actorOf(props)
    worker.tell(msg, client) //delegate to handler, with original sender

  }

  private def buildBatchRefId(msg: GetImageBatchRequest): String =
    msg.systemId + "-" + msg.gantry + "-" + System.currentTimeMillis() + "-" + msg.hashCode()

  override def preStart(): Unit = {
    super.preStart()
    databaseActor = context.actorOf(Props(new HBaseClientActor(createHBaseFunctions)))
    imageStoreActor = createImageStoreActor
  }

  /**
   * Creates an imageStoreActor, for interacting with a remote ImageStore.
   * All the AMQP wiring is done here, so this is the method to override when mocking AMQP
   * @return image store actor ref
   */
  protected def createImageStoreActor: ActorRef = ImageStoreActor(actorSystem, config)

  /**
   * Creates the HBaseFunctions to be used with HBase client actor.
   * All the HBase setup is done here, so this is the method to override when mocking HBase
   * @return HBaseFunctions
   */
  protected def createHBaseFunctions: HBaseFunctions = HBaseFunctions(config, JsonSerializers.formats)

}

object RetrieveImageActor {

  def props(config: ImageRetrieverConfig) = Props(new RetrieveImageActor(config))

}