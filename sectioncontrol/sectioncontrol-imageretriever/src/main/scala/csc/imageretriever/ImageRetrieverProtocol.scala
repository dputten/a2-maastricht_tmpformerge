package csc.imageretriever

import csc.sectioncontrol.messages.VehicleImageType.VehicleImageType

/**
 * Created by carlos on 10.08.15.
 */

/**
 * Contains the union of all the fields we need to identify an image in either HBase or ImageStore
 * @param uri uri of the image, needed for ImageStore lookup
 * @param time time the photo was taken, needed for HBase lookup
 * @param imageType type of photo, needed for HBase lookup
 * @param systemId systemId, needed for HBase lookup and create AMQP route key
 * @param gantry gantryId, needed for HBase lookup and create AMQP route key
 * @param lane lane, needed for HBase lookup
 */
case class ImageKey(uri: String, time: Long, imageType: VehicleImageType, systemId: String, gantry: String, lane: String)

case class GetImageRequest(refId: String, key: ImageKey)

case class GetImageResponse(refId: String, key: ImageKey, contents: Option[Array[Byte]], error: Option[String])

case class GetImageBatchRequest(refId: String, systemId: String, gantry: String, keys: List[ImageKey])

case class BatchProgressResponse(refId: String, total: Int, count: Int, errorCount: Int) {
  def isCompleted(): Boolean = count + errorCount == total
}

case class StoreImageRequest(refId: String, key: ImageKey, contents: Array[Byte])

case class StoreImageResponse(refId: String, key: ImageKey, error: Option[String])

