package csc.imageretriever

import akka.actor._
import akka.testkit.{ TestProbe, TestKit }
import akka.util.Duration
import java.util.concurrent.TimeUnit
import csc.imageretriever.actor.{ ImageStoreActor, RetrieveImageActor }
import org.scalatest.{ BeforeAndAfterEach, BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers

/**
 * Created by carlos on 12.08.15.
 */
class RetrieveImageActorTest
  extends TestKit(ActorSystem("retrieveImage"))
  with WordSpec
  with MustMatchers
  with BeforeAndAfterAll
  with BeforeAndAfterEach
  with SampleData
  with ImageBatchTest {

  var hbaseFunctions: HBaseFunctionsMock = _
  var actor: ActorRef = _
  val probe: TestProbe = new TestProbe(system)

  "RetrieveImageActor/RetrieveImageWorker" must {
    "return Some(content) getting an image already in HBase" in {
      val msg = GetImageRequest(nextString, hbaseExistingKey)
      probe.send(actor, msg)

      //check response
      val response = probe.expectMsgClass(classOf[GetImageResponse])
      response.error must be(None)
      response.contents.isDefined must be(true)
      response.contents.get.deep must be(hbaseImageContents.deep)
    }

    "return Some(content) getting an image in ImageStore, then saved in HBase" in {
      val msg = GetImageRequest(nextString, storeExistingKey)
      probe.send(actor, msg)

      //check response
      val response = probe.expectMsgClass(Duration(10, TimeUnit.SECONDS), classOf[GetImageResponse])
      response.error must be(None)
      response.contents.isDefined must be(true)
      response.contents.get.deep must be(storeImageContents.deep)

      //check if properly saved in hbase
      hbaseFunctions.savedImageCount must be(1)
      hbaseFunctions.isSaved(msg.key, storeImageContents)
    }

    "return None getting an image that doesn't exist anywhere" in {
      val msg = GetImageRequest(nextString, newImageKey())
      probe.send(actor, msg)

      //check response
      val response = probe.expectMsgClass(classOf[GetImageResponse])
      response.error must be(None)
      response.contents must be(None)
    }

    "return None getting an image that exists in ImageStore, but failed to be saved in HBase" in {
      val msg = GetImageRequest(nextString, errorSavingKey)
      probe.send(actor, msg)

      //check response
      val response = probe.expectMsgClass(Duration(20, TimeUnit.SECONDS), classOf[GetImageResponse])
      response.error must be(Some(dummyErrorMessage))
      response.contents must be(None)
    }

    "adds a refId (copied from uri) to GetImageRequest if not specified" in {
      val msg = GetImageRequest(null, errorSavingKey) //refId not specified
      probe.send(actor, msg)

      val response = probe.expectMsgClass(Duration(20, TimeUnit.SECONDS), classOf[GetImageResponse])
      response.refId must be(msg.key.uri)
    }

    "return an empty BatchProgressResponse for an empty GetImageBatchRequest" in {
      val keys: List[ImageKey] = Nil
      val msg = GetImageBatchRequest(nextString, "system", "gantry", keys) //refId not specified and empty list
      probe.send(actor, msg)

      val response = probe.expectMsgClass(classOf[BatchProgressResponse])
      response.total must be(keys.size)
      response.count must be(0)
      response.errorCount must be(0)
    }

    "return several GetImageResponse and BatchProgressResponses for a batch with a mix of existing, missing and failing image keys" in {
      val keys = batchKeys.toList ++ List(newImageKey(), errorImageKey, errorSavingKey, storeExistingKey)
      implicit val msg = GetImageBatchRequest(nextString, "system", "gantry", keys)
      probe.send(actor, msg)

      checkImageResponse()
      checkImageResponse()
      checkProgress(2)

      checkImageResponse()
      checkImageResponse()
      checkProgress(4)

      checkImageResponse()
      checkImageResponse()
      checkProgress(6)

      checkImageResponse()
      checkImageResponse()
      checkProgress(8)

      checkImageResponse()
      val lastProgress = checkProgress(9)

      lastProgress.count must be(7)
      lastProgress.errorCount must be(2) //1 error saving
    }

  }

  override protected def beforeEach(): Unit = {
    hbaseFunctions = new HBaseFunctionsMock
    actor = system.actorOf(Props(new RetrieveImageActorMock(hbaseFunctions)))
  }

  override protected def afterAll(): Unit = {
    system.shutdown()
  }

}

class RetrieveImageActorMock(hbaseFunctions: HBaseFunctions) extends RetrieveImageActor(null, 2) with SampleData {

  override protected def createImageStoreActor: ActorRef = {
    val producer = actorSystem.actorOf(Props(new ImageStoreMock())) //mock producer and consumer
    actorSystem.actorOf(Props(new ImageStoreActor(producer)))
  }

  override protected def createHBaseFunctions: HBaseFunctions = hbaseFunctions // new HBaseFunctionsMock

}
