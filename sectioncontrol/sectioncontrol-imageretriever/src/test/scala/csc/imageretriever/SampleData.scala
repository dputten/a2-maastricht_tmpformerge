package csc.imageretriever

import csc.sectioncontrol.messages.VehicleImageType

import scala.collection.mutable

/**
 * Created by carlos on 12.08.15.
 */
trait SampleData {

  private var counter: Long = 0

  lazy val dummyErrorMessage = "Dummy Exception"
  lazy val hbaseImageContents: Array[Byte] = "hbaseImageContents".getBytes
  lazy val storeImageContents: Array[Byte] = "otherImageContents".getBytes
  lazy val hbaseExistingKey = newImageKey("hbase")
  lazy val storeExistingKey = newImageKey("storeUri")
  lazy val errorImageKey = newImageKey("error")
  lazy val errorSavingKey = newImageKey("hbaseSaveError")
  lazy val batchKeys = for (i ← Range(0, 5)) yield newImageKey("batch-" + i)

  val hbaseInitialImages: Map[ImageKey, Array[Byte]] = Map(hbaseExistingKey -> hbaseImageContents)
  val storeImages: Map[String, Array[Byte]] = Map(
    storeExistingKey.uri -> storeImageContents,
    errorSavingKey.uri -> storeImageContents) ++ batchKeys.map { k ⇒ (k.uri -> storeImageContents) }.toMap

  def newImageKey(uri: String = nextString): ImageKey =
    ImageKey(uri, nextLong, VehicleImageType.Overview, nextString, nextString, nextString)

  def nextLong: Long = {
    val value = counter + 1
    counter = value
    value
  }

  def nextString: String = "str%s".format(nextLong)

}

class HBaseFunctionsMock extends HBaseFunctions with SampleData {

  var started = false

  private val addedImages: mutable.Map[ImageKey, Array[Byte]] = new mutable.HashMap

  override def start(): Unit = started = true

  override def readImage(key: ImageKey): Option[Array[Byte]] = {
    checkStarted
    hbaseInitialImages.get(key)
  }

  override def writeImage(key: ImageKey, data: Array[Byte]): Option[String] = {
    checkStarted
    if (errorSavingKey.uri == key.uri)
      Some(dummyErrorMessage)
    else {
      addedImages.update(key, data)
      None
    }
  }

  override def stop(): Unit = started = false

  private def checkStarted = if (!started) throw new IllegalStateException("start was not called")

  def savedImageCount: Int = addedImages.size

  def isSaved(key: ImageKey, contents: Array[Byte]): Boolean = {
    val value = addedImages.get(key)
    value.isDefined && value.get.deep == contents.deep
  }

}
