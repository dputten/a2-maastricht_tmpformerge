package csc.imageretriever

import java.util.concurrent.TimeUnit

import akka.actor.{ ActorRef, ActorSystem }
import akka.pattern.ask
import akka.testkit.{ TestActorRef, TestKit }
import akka.util.{ Duration, Timeout }
import csc.imageretriever.actor.HBaseClientActor
import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }
import akka.dispatch.Await

/**
 * Created by carlos on 12.08.15.
 */
class HBaseClientActorTest
  extends TestKit(ActorSystem("hbaseClient"))
  with WordSpec
  with MustMatchers
  with BeforeAndAfterAll
  with BeforeAndAfterEach
  with SampleData {

  implicit val timeout: Timeout = Timeout(1, TimeUnit.SECONDS)

  var hbase: HBaseFunctionsMock = _
  var actor: ActorRef = _

  override protected def beforeEach(): Unit = {
    super.beforeEach()
    hbase = new HBaseFunctionsMock
    actor = TestActorRef(new HBaseClientActor(hbase))
  }

  "HBaseClientActor" must {

    "reply GetImageResponse with Some(contents) getting an existing image" in {
      val msg = GetImageRequest(nextString, hbaseExistingKey)
      val future = actor ? msg
      val result = Await.result(future, Duration(10, TimeUnit.SECONDS)).asInstanceOf[GetImageResponse]
      result.refId must be(msg.refId)
      result.contents.isDefined must be(true)
      result.contents.get.deep must be(hbaseImageContents.deep)
      result.error must be(None)
    }

    "reply GetImageResponse with None getting a non-existing image" in {
      val msg = GetImageRequest(nextString, newImageKey())
      val future = actor ? msg

      val res = Await.result(future, Duration(10, TimeUnit.SECONDS)).asInstanceOf[GetImageResponse]
      res.refId must be(msg.refId)
      res.contents must be(None)
      res.error must be(None)
    }

    "reply StoreImageResponse with None for successful save operation" in {
      val msg = StoreImageRequest(nextString, newImageKey(), hbaseImageContents)
      val future = actor ? msg
      val res = Await.result(future, Duration(10, TimeUnit.SECONDS)).asInstanceOf[StoreImageResponse]

      res.refId must be(msg.refId)
      res.error must be(None)

      hbase.savedImageCount must be(1)
      hbase.isSaved(msg.key, msg.contents)

      val msg2 = StoreImageRequest(nextString, newImageKey(), storeImageContents)
      val future2 = actor ? msg2

      hbase.savedImageCount must be(2)
      hbase.isSaved(msg2.key, msg2.contents)
    }

    "reply StoreImageResponse with Some(error) for failed save operation" in {
      val msg = StoreImageRequest(nextString, errorSavingKey, hbaseImageContents)
      val future = actor ? msg
      val res = Await.result(future, Duration(10, TimeUnit.SECONDS)).asInstanceOf[StoreImageResponse]
      res.refId must be(msg.refId)
      res.error.isDefined must be(true)
      res.error.get must be(dummyErrorMessage)

      hbase.savedImageCount must be(0)
    }
  }

  override protected def afterAll(): Unit = {
    system.shutdown()
  }
}
