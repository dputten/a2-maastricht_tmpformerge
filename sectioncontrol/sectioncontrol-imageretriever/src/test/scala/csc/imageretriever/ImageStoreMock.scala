package csc.imageretriever

import akka.actor.Actor
import akka.dispatch.Future

/**
 * Mocks both AMQP actors (producer and consumer) by answering directly ImageGetResponse to any wrapped ImageGetRequest
 * For ImageBatchGetRequest, it sends a ImageBatchProgressResponse for every 2 sent ImageGetResponse
 *
 * Created by carlos on 12.08.15.
 */
class ImageStoreMock extends Actor with SampleData {

  implicit val executor = context.dispatcher

  override protected def receive: Receive = {
    case RoutedRequest(systemId, gantry, req: ImageGetRequest)      ⇒ sender ! createResponse(req.refId, req.key)
    case RoutedRequest(systemId, gantry, req: ImageBatchGetRequest) ⇒ handleBatchRequest(req)
  }

  def handleBatchRequest(req: ImageBatchGetRequest): Unit = {
    val client = sender
    val total = req.keys.size

    Future {
      var lastProgressSent: Option[Int] = None
      var count = 0
      var errors = 0
      req.keys.foreach { key ⇒
        val imgResponse = createResponse(req.refId, key)
        client ! imgResponse //send the image response

        if (imgResponse.error.isDefined)
          errors = errors + 1
        else
          count = count + 1

        val current = errors + count
        if (current % 2 == 0) {
          //sends the progress every 2
          client ! ImageBatchProgressResponse(req.refId, total, count, errors)
          lastProgressSent = Some(current)
        }
      }

      if (lastProgressSent.isEmpty || lastProgressSent.get < total) {
        client ! ImageBatchProgressResponse(req.refId, total, total, 0) //sends the last batch progress (remainder)
      }
    }

  }

  def createResponse(refId: String, key: String): ImageGetResponse = {
    if (key == errorImageKey.uri) {
      ImageGetResponse(refId, key, 0, null, null, Some(dummyErrorMessage))
    } else storeImages.get(key) match {
      case Some(data) ⇒ ImageGetResponse(refId, key, System.currentTimeMillis(), data, "checksum", None)
      case None       ⇒ ImageGetResponse(refId, key, 0, null, null, None)
    }
  }

}
