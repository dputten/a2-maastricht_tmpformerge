package csc.imageretriever

import java.util.concurrent.TimeUnit

import akka.actor.{ ActorSystem, Props }
import akka.pattern.ask
import akka.testkit.{ ImplicitSender, TestActorRef, TestKit, TestProbe }
import akka.util.{ Duration, Timeout }
import csc.imageretriever.actor.ImageStoreActor
import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }
import akka.dispatch.Await

/**
 * Created by carlos on 12.08.15.
 */
class ImageStoreActorTest
  extends TestKit(ActorSystem("imageStore"))
  with WordSpec
  with MustMatchers
  with BeforeAndAfterAll
  with BeforeAndAfterEach
  with SampleData
  with ImageBatchTest
  with ImplicitSender {

  implicit val timeout: Timeout = Timeout(1, TimeUnit.SECONDS)

  val responseTimeout = Duration(3, "seconds")
  var imageStoreActor: TestActorRef[ImageStoreActor] = _
  var probe = new TestProbe(system)

  override protected def beforeEach(): Unit = {
    super.beforeEach()

    val producer = system.actorOf(Props(new ImageStoreMock())) //mock producer and consumer
    imageStoreActor = TestActorRef(new ImageStoreActor(producer))
  }

  "ImageStoreActor" must {

    "reply GetImageResponse with Some(contents) getting an existing image" in {
      val msg = GetImageRequest(nextString, storeExistingKey)
      val future = imageStoreActor ? msg

      val res = Await.result(future, responseTimeout).asInstanceOf[GetImageResponse]
      res.refId must be(msg.refId)
      res.contents.isDefined must be(true)
      res.contents.get.deep must be(storeImageContents.deep)
      res.error must be(None)
    }

    "send 2 replies when receiving 2 requests with the same refId for the same image" in {
      val key = "Duplicate refId"
      val msg1 = GetImageRequest(key, storeExistingKey)
      val msg2 = GetImageRequest(key, storeExistingKey)

      imageStoreActor ! msg1
      imageStoreActor ! msg2

      val messages = expectMsgAllClassOf(classOf[GetImageResponse], classOf[GetImageResponse])
      messages.size must be(2)
    }

    "send 3 replies when receiving 3 requests with the same refId for the same image" in {
      imageStoreActor = TestActorRef(new ImageStoreActor(probe.ref))
      val key = "Duplicate refId"
      val msg1 = GetImageRequest(key, storeExistingKey)
      val msg2 = GetImageRequest(key, storeExistingKey)
      val msg3 = GetImageRequest(key, storeExistingKey)

      imageStoreActor ! msg1
      imageStoreActor ! msg2
      imageStoreActor ! msg3

      val imageStoreReq: RoutedRequest = probe.expectMsgType[RoutedRequest]
      //The image store should get only one request
      probe.expectNoMsg

      //This should normally be done by the image store
      imageStoreActor ! ImageGetResponse(imageStoreReq.request.refId, key, System.currentTimeMillis(), storeImageContents, "checksum", None)

      //Expect 3 responses
      expectMsgAllClassOf(classOf[GetImageResponse], classOf[GetImageResponse], classOf[GetImageResponse])
      expectNoMsg()
    }

    "reply GetImageResponse with None getting a non-existing image" in {
      val msg = GetImageRequest(nextString, newImageKey())
      val future = imageStoreActor ? msg

      val res = Await.result(future, responseTimeout).asInstanceOf[GetImageResponse]
      res.refId must be(msg.refId)
      res.contents must be(None)
      res.error must be(None)
    }

    "reply GetImageResponse with Some(error) for a failed operation" in {
      val msg = GetImageRequest(nextString, errorImageKey)
      val future = imageStoreActor ? msg

      val res = Await.result(future, responseTimeout).asInstanceOf[GetImageResponse]
      res.refId must be(msg.refId)
      res.contents must be(None)
      res.error must be(Some(dummyErrorMessage))
    }

    "reply with the correct GetImageResponses and BatchProgressResponses for a GetImageBatchRequest" in {
      implicit val msg = GetImageBatchRequest(nextString, "system", "gantry", batchKeys.toList)

      probe.send(imageStoreActor, msg)

      checkImageResponse()
      checkImageResponse()
      checkProgress(2)

      checkImageResponse()
      checkImageResponse()
      checkProgress(4)

      checkImageResponse()
      checkProgress(5)

    }
  }

  override protected def afterAll(): Unit = {
    system.shutdown()
  }

}

trait ImageBatchTest extends WordSpec with MustMatchers with SampleData {

  def probe: TestProbe

  def checkImageResponse()(implicit msg: GetImageBatchRequest): GetImageResponse = {
    val m = probe.expectMsgClass(classOf[GetImageResponse])
    m.refId must be(msg.refId)
    //m.error must be(None)

    //m.contents.isDefined must be(true)
    if (m.contents.isDefined) {
      m.contents.get must be(storeImageContents)
    }
    m
  }

  def checkProgress(expected: Int)(implicit msg: GetImageBatchRequest): BatchProgressResponse = {
    val m = probe.expectMsgClass(classOf[BatchProgressResponse])
    m.refId must be(msg.refId)
    m.total must be(msg.keys.size)
    m.count + m.errorCount must be(expected)
    //m.errorCount must be(0)
    m
  }

}