package com.github.sstone.amqp

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

import com.rabbitmq.client.ConnectionFactory
/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 1/14/15.
 */

class ConnectionFactoryTest extends WordSpec with MustMatchers {
  "ConnectionFactory" must {
    "properly parse a amqp uri" in {
      val cf = new ConnectionFactory()
      val uriString = "amqp://username:password@amqphost.emea.csc.com/"
      cf.setUri(uriString)
      if (cf.getVirtualHost == "") cf.setVirtualHost("/")
      cf.getHost must be("amqphost.emea.csc.com")
      cf.getUsername must be("username")
      cf.getPassword must be("password")
      cf.getPort must be(5672)
      cf.getVirtualHost must be("/")
    }
  }
}
