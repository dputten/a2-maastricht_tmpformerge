#!/bin/bash
export http_proxy=
# set sbt_home to right directory (that is to the repos you just cloned)
base='/media/NL-TC-Projects/git-projects'
sbt_home=${base}/sbt_0.12.3
# You may change/omit the option -Xms256m, it limits the amount of memory java swallows up.
java_opts="-Dsbt.repository.config=$sbt_home/repositories_2.9.1_0.11.2 -Dsbt.override.build.repos=true -Dsbt.boot.properties=$sbt_home/sbt.boot.properties_2.9.1_0.11.2 -Xms368m -Xmx368m"
export java_opts
export JAVA_HOME=${base}/jdk1.6.0_31
export PATH=$PATH:${JAVA_HOME}/bin
$sbt_home/sbt/bin/sbt -verbose "$@"

