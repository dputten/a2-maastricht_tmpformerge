/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.cleanup

import csc.config.Path
import csc.curator.utils.Curator
import csc.akkautils.{ DirectLoggingAdapter, JobSchedule }
import akka.util.duration._
import csc.sectioncontrol.storage._
import csc.sectioncontrol.enforce.nl.run.RegisterViolationsJobConfig
import csc.sectioncontrol.messages.EsaProviderType

/**
 * Utility class to load a basic zookeeper config for testing the Matcher boot.
 * Also loads the necessary configuration for vehicle-registration to run.
 *
 * @author Maarten Hazewinkel
 */
object CleanupSetup extends App {

  val zkStore = Curator.createCuratorWithLogger(this, new DirectLoggingAdapter(this.getClass.getName))

  def saveZk[T <: AnyRef](p: String, v: T) {
    zkStore.put(Path(p), v)
  }

  val systemId = "A2"

  val s = ZkSystem(
    id = systemId,
    name = systemId,
    title = "systemId",
    mtmRouteId = "",
    violationPrefixId = "a002",
    location = ZkSystemLocation(
      description = systemId,
      region = "Utrecht",
      viewingDirection = Some(""),
      roadNumber = "0002",
      roadPart = "Links",
      systemLocation = "Amsterdam"),
    serialNumber = SerialNumber("eg33-1234"),
    orderNumber = 1,
    maxSpeed = 120,
    compressionFactor = 5,
    retentionTimes = ZkSystemRetentionTimes(
      nrDaysKeepTrafficData = 28,
      nrDaysKeepViolations = 7,
      nrDaysRemindCaseFiles = 3),
    pardonTimes = ZkSystemPardonTimes(pardonTimeEsa_secs = 5000, pardonTimeTechnical_secs = 5000),
    esaProviderType = EsaProviderType.NoProvider,
    minimumViolationTimeSeparation = 0)

  saveZk(Paths.Systems.getConfigPath(systemId), s)

  val j = JobSchedule(true, 0, runAtMillisPastMidnight = Some(4.hours.toMillis), runAtMillisInterval = None, timeZone = "Europe/Amsterdam")
  saveZk(Paths.Systems.getSystemJobsPath(systemId) + "/cleanupJob", j)

  val cr = RegisterViolationsJobConfig("/tmp/politie")
  saveZk(Paths.Systems.getSystemJobsPath(systemId) + "/registerViolationsJob/config", cr)
}