/*
* Copyright (C) 2012 CSC. <http://www.csc.com>
*/
package csc.sectioncontrol.cleanup

import org.scalatest.matchers.MustMatchers
import akka.actor.ActorSystem
import akka.testkit.TestActorRef
import akka.dispatch.Await
import org.scalatest.WordSpec
import java.lang.System
import akka.pattern.ask
import akka.util.Timeout
import akka.util.duration._
import csc.akkautils.DirectLoggingAdapter
import csc.curator.CuratorTestServer
import csc.curator.utils.{ CuratorToolsImpl, Curator }

class ZookeeperCleanupTest extends WordSpec with MustMatchers with CuratorTestServer {

  implicit val testSystem = ActorSystem("ZookeeperCleanupTest")
  var testActor: TestActorRef[CleanupZookeeper] = _
  var storage: Curator = _

  override def beforeEach() {
    super.beforeEach()
    storage = new CuratorToolsImpl(clientScope, log)
    testActor = TestActorRef(new CleanupZookeeper(1, storage))
  }

  "CleanupZookeeper" must {
    "clean old record" in {
      val basePath = "/ctes/test/zookeeperCleanup"

      saveZk(basePath + "/val1", ZookeeperCleanTestData("val1"))
      Thread.sleep(100)
      val cutoffTime = System.currentTimeMillis()
      Thread.sleep(100)
      saveZk(basePath + "/val2", ZookeeperCleanTestData("val2"))
      Thread.sleep(100)

      storage.getChildNames(basePath) must have length (2)

      val message = CleanZookeeperPath(basePath, cutoffTime)
      implicit val timeout = Timeout(15 seconds)
      val getFuture = testActor ? message
      val getResult = Await.result(getFuture, timeout.duration).asInstanceOf[CleanedMessage]
      getResult must be(CleanedMessage(true))

      storage.getChildNames(basePath) must have length (1)
      storage.getChildNames(basePath)(0) must be("val2")
    }
  }

  def saveZk[T <: AnyRef](p: String, v: T) {
    if (storage.exists(p))
      storage.deleteRecursive(p)
    storage.put(p, v)
  }

  override def afterEach() {
    try {
      testSystem.shutdown()
    } finally {
      super.afterEach()
    }
  }

  protected def log = new DirectLoggingAdapter(this.getClass.getName)
}

case class ZookeeperCleanTestData(v: String)
