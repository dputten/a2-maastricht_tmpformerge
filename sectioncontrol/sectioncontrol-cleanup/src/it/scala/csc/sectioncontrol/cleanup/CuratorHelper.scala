/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

package csc.sectioncontrol.cleanup

/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

import org.apache.curator.framework.CuratorFrameworkFactory
import org.apache.curator.retry.RetryUntilElapsed
import csc.akkautils.DirectLoggingAdapter
import csc.curator.utils.{ CuratorToolsImpl, Curator }
import org.apache.curator.RetryPolicy
import net.liftweb.json.{ DefaultFormats, Formats }
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.storage._
import csc.sectioncontrol.messages.{ EsaProviderType, VehicleImageType, VehicleCode }

object CuratorHelper {
  private lazy val retryPolicy = new RetryUntilElapsed(1000 * 60 * 60 * 24 * 3, 5000)
  private lazy val formats: Formats = DefaultFormats

  def create(caller: AnyRef, zkServers: String = "localhost:2181", retryPolicy: RetryPolicy = retryPolicy, formats: Formats = formats): Curator = {
    val newClient = CuratorFrameworkFactory.newClient(zkServers, retryPolicy)
    val log = new DirectLoggingAdapter(caller.getClass.getName)
    newClient.start()
    new CuratorToolsImpl(Some(newClient), log) {
      override protected def extendFormats(defaultFormats: Formats) = formats + new EnumerationSerializer(
        ZkWheelbaseType,
        VehicleCode,
        ZkIndicationType,
        ServiceType,
        ZkCaseFileType,
        EsaProviderType,
        VehicleImageType,
        ConfigType)
    }
  }

  def close(curator: Curator) {
    curator.curator.foreach(_.close())
  }
}