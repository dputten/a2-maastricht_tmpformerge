/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.cleanup

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import akka.util.duration._

class CleanupZookeeperTest extends WordSpec with MustMatchers {
  "filterRecords" must {
    "filter old records" in {
      //create list
      val startTime = 1405583000L
      val statsList = Seq(
        ("id1", startTime),
        ("id2", startTime + 1.day.toMillis),
        ("id3", startTime + 2.day.toMillis),
        ("id4", startTime + 3.day.toMillis),
        ("id5", startTime + 4.day.toMillis),
        ("id6", startTime + 5.day.toMillis),
        ("id7", startTime + 6.day.toMillis),
        ("id8", startTime + 7.day.toMillis))
      val result = CleanupZookeeper.filterRecords(statsList, startTime + 4.day.toMillis, 1)
      result.size must be(4)
      val resultList = Seq(statsList(0), statsList(1), statsList(2), statsList(3)).map(_._1)
      result must be(resultList)
    }
    "filter old records, but leave 1 records" in {
      //create list
      val startTime = 1405583000L
      val statsList = Seq(
        ("id1", startTime),
        ("id2", startTime + 1.day.toMillis),
        ("id3", startTime + 2.day.toMillis),
        ("id4", startTime + 3.day.toMillis),
        ("id5", startTime + 4.day.toMillis),
        ("id6", startTime + 5.day.toMillis),
        ("id7", startTime + 6.day.toMillis),
        ("id8", startTime + 7.day.toMillis))
      val result = CleanupZookeeper.filterRecords(statsList, startTime + 8.day.toMillis, 1)
      result.size must be(7)
      val resultList = statsList.init.map(_._1)
      result must be(resultList)
    }
    "filter old records, but leave 4 records" in {
      //create list
      val startTime = 1405583000L
      val statsList = Seq(
        ("id1", startTime),
        ("id2", startTime + 1.day.toMillis),
        ("id3", startTime + 2.day.toMillis),
        ("id4", startTime + 3.day.toMillis),
        ("id5", startTime + 4.day.toMillis),
        ("id6", startTime + 5.day.toMillis),
        ("id7", startTime + 6.day.toMillis),
        ("id8", startTime + 7.day.toMillis))
      val result = CleanupZookeeper.filterRecords(statsList, startTime + 8.day.toMillis, 4)
      result.size must be(4)
      val resultList = Seq(statsList(0), statsList(1), statsList(2), statsList(3)).map(_._1)
      result must be(resultList)
    }
    "return empty list when there are no records at all" in {
      //create list
      val startTime = 1405583000L
      val result = CleanupZookeeper.filterRecords(Seq(), startTime + 8.day.toMillis, 4)
      result.size must be(0)
    }
    "return empty list when nr records is smaller than the minNrRecords" in {
      //create list
      val startTime = 1405583000L
      val statsList = Seq(
        ("id1", startTime),
        ("id2", startTime + 1.day.toMillis),
        ("id3", startTime + 2.day.toMillis),
        ("id4", startTime + 3.day.toMillis),
        ("id5", startTime + 4.day.toMillis),
        ("id6", startTime + 5.day.toMillis),
        ("id7", startTime + 6.day.toMillis),
        ("id8", startTime + 7.day.toMillis))
      val result = CleanupZookeeper.filterRecords(statsList, startTime + 8.day.toMillis, 10)
      result.size must be(0)
    }
    "retrun all when minNrRecords is 0" in {
      //create list
      val startTime = 1405583000L
      val statsList = Seq(
        ("id1", startTime),
        ("id2", startTime + 1.day.toMillis),
        ("id3", startTime + 2.day.toMillis),
        ("id4", startTime + 3.day.toMillis),
        ("id5", startTime + 4.day.toMillis),
        ("id6", startTime + 5.day.toMillis),
        ("id7", startTime + 6.day.toMillis),
        ("id8", startTime + 7.day.toMillis))
      val result = CleanupZookeeper.filterRecords(statsList, startTime + 8.day.toMillis, 0)
      result.size must be(8)
      result must be(statsList.map(_._1))
    }
  }

}