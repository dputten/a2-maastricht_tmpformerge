/*
* Copyright (C) 2012 CSC. <http://www.csc.com>
*/
package csc.sectioncontrol.cleanup

import akka.actor.ActorSystem
import akka.dispatch.Await
import akka.pattern.ask
import akka.testkit.TestActorRef
import akka.util.Timeout
import akka.util.duration._
import com.sematext.hbase.wd.RowKeyDistributorByHashPrefix.OneByteSimpleHash
import com.sematext.hbase.wd.{ DistributedScanner, RowKeyDistributorByHashPrefix }
import csc.hbase.utils.testframework.HBaseTestFramework
import csc.hbase.utils.{ JsonHBaseReader, JsonHBaseWriter }
import net.liftweb.json.DefaultFormats
import org.apache.hadoop.hbase.client._
import org.apache.hadoop.hbase.util.Bytes
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

class HBaseCleanupTest extends WordSpec with MustMatchers with HBaseTestFramework {
  implicit val testSystem = ActorSystem("CleanupTest")
  var testActor: TestActorRef[CleanupTable] = _

  protected var test1Table: Option[HTable] = None
  protected var test2Table: Option[HTable] = None
  protected val table1Name = "test1"
  protected val table2Name = "test2"
  protected val prefix1 = "prefix1"
  protected val prefix2 = "prefix2"

  protected val rowKeyDistributor = new RowKeyDistributorByHashPrefix(new OneByteSimpleHash(10))

  implicit val jsonDefaultFormats = DefaultFormats

  val td1 = TestData("row1", None)
  val td2 = TestData("row2", Some(2))
  val td3 = TestData("row3", Some(64))
  val records = Seq(td1, td2, td3, td1, td2, td3)

  val aMonthAgo = System.currentTimeMillis() - 1000L * 3600L * 24L * 30L // 30 days ago
  val aWeekAgo = System.currentTimeMillis() - 1000L * 3600L * 24L * 7L

  val timestamps = Seq(aMonthAgo, aMonthAgo + 10, aMonthAgo + 20, aWeekAgo, aWeekAgo + 10, aWeekAgo + 20)

  override protected def beforeAll() {
    super.beforeAll()
    implicit def string2ByteArray(s: String): Array[Byte] = Bytes.toBytes(s)
    test1Table = Some(testUtil.createTable(table1Name, "cf"))
    test2Table = Some(testUtil.createTable(table2Name, "cf"))
    testActor = TestActorRef(new CleanupTable("A2", "localhost:" + testUtil.getZkCluster.getClientPort))
  }

  "Cleanup" must {
    "delete records based on timestamp" in {
      HBaseWriterImpl.useTable(test1Table)
      writeRecords(test1Table, prefix1, timestamps, records)
      //
      implicit val timeout = Timeout(15 seconds)
      val getFuture = testActor ? DeleteRowsInTableWithTimestamp(table1Name, aWeekAgo - 1000L)
      val getResult = Await.result(getFuture, timeout.duration).asInstanceOf[CleanedMessage]
      getResult must be(CleanedMessage(true))
      //
      getTableSize(test1Table) must be(3)
      // remove old  data
      testActor ? DeleteRowsInTableWithTimestamp(table1Name, System.currentTimeMillis())
      getTableSize(test1Table) must be(0)
    }

    "delete  records based on prefix" in {
      HBaseWriterImpl.useTable(test2Table)
      writeRecords(test2Table, prefix2, timestamps, records)

      val message = DeleteRowsInTableWithTimestampAndPrefixes(table2Name, aWeekAgo - 1000L, Seq(prefix2))
      implicit val timeout = Timeout(15 seconds)
      val getFuture = testActor ? message
      val getResult = Await.result(getFuture, timeout.duration).asInstanceOf[CleanedMessage]
      getResult must be(CleanedMessage(true))
      //
      getTableSize(test2Table) must be(3)

      val cleanAllFuture = testActor ? DeleteRowsInTableWithTimestamp(table2Name, System.currentTimeMillis())
      val cleanAllResult = Await.result(cleanAllFuture, timeout.duration).asInstanceOf[CleanedMessage]
    }
  }

  private def writeRecords(table: Option[HTable], prefix: String, timeStamps: Seq[Long], data: Seq[TestData]): Unit = {
    timeStamps.zip(records) foreach ((t: Tuple2[Long, TestData]) ⇒ {
      val key = prefix + t._1
      val timestamp = Some(t._1)
      HBaseWriterImpl.writeRow(key, t._2, timestamp)
    })
    test1Table foreach (_.flushCommits())
    Thread.sleep(100)
    getTableSize(table) must be(timeStamps.size)
  }

  private def getTableSize(table: Option[HTable]): Int = {
    val scan = new Scan()
    scan.setTimeRange(0L, System.currentTimeMillis())
    val scanner = DistributedScanner.create(table.get, scan, rowKeyDistributor)

    import collection.JavaConversions._
    val result = scanner.size
    scanner.close()
    result
  }

  override protected def afterAll() {
    try {
      testSystem.shutdown()
      test1Table foreach (_.close())
      test2Table foreach (_.close())
    } finally {
      super.afterAll()
    }
  }
  case class TestData(f1: String, f2: Option[Int])

  object HBaseReaderImpl extends JsonHBaseReader[String, TestData] {
    val rowKeyDistributer = new RowKeyDistributorByHashPrefix(new OneByteSimpleHash(10))
    var someTable: Option[HTable] = None
    def hbaseReaderTable = someTable.get
    val hbaseReaderDataColumnFamily = "cf"
    val hbaseReaderDataColumnName = "cn"
    def useTable(table: Option[HTable]): Unit = someTable = table
    def makeReaderKey(keyValue: String) = Bytes.toBytes(keyValue)
  }

  object HBaseWriterImpl extends JsonHBaseWriter[String, TestData] {
    val rowKeyDistributer = new RowKeyDistributorByHashPrefix(new OneByteSimpleHash(10))
    var someTable: Option[HTable] = None
    def hbaseWriterTable = someTable.get
    val hbaseWriterDataColumnFamily = "cf"
    val hbaseWriterDataColumnName = "cn"
    def useTable(table: Option[HTable]): Unit = someTable = table
    def makeWriterKey(keyValue: String) = Bytes.toBytes(keyValue)
  }

}

