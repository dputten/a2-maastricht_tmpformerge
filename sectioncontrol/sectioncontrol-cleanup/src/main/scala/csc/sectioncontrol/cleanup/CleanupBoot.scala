/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.cleanup

import java.text.SimpleDateFormat
import java.util.Date

import scala.Predef._
import csc.akkautils._

import akka.dispatch.Future
import akka.pattern.ask
import akka.util.duration._
import akka.util.Timeout
import akka.actor._
import akka.actor.SupervisorStrategy.Stop

import net.liftweb.json.DefaultFormats

import csc.sectioncontrol.enforce.nl.run.RegisterViolationsJobConfig
import csc.curator.utils.{ Curator, CuratorActor }
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.storage._
import csc.sectioncontrol.messages.{ EsaProviderType, VehicleImageType, VehicleCode, VehicleCategory }
import csc.sectioncontrol.storagelayer.hbasetables.RowKeyDistributorFactory

/**
 * Boot the cleanup actors.
 */
class CleanupBoot extends GenericBoot {
  import CleanupBoot._

  def componentName = "SectionControl-Cleanup"

  /**
   * Startup the job scheduler that will remove outdated records from HBase.
   *
   * The path where the scheduler looks for time-to-live period is:
   * "/ctes/systems/<id>/jobs/cleanupJob"
   *
   */
  def startupActors() {
    val config = actorSystem.settings.config
    val minNrRecordsConfigPath = "sectioncontrol.cleanup.minNrRecords"
    val zkServerConfigPath = "sectioncontrol.zkServers"
    val hbaseZkServerConfigPath = "sectioncontrol.hbaseZkServers"

    if (config.hasPath(minNrRecordsConfigPath)) {
      minNrRecords = config.getInt(minNrRecordsConfigPath)
    }

    if (config.hasPath(zkServerConfigPath)) {
      // Get the zookeeper server from the config file application.conf     
      val zkServers = config.getString(zkServerConfigPath)
      // Get the zookeeper server from the config file application.conf
      hbaseZkServers = if (config.hasPath(hbaseZkServerConfigPath)) {
        config.getString(hbaseZkServerConfigPath)
      } else {
        // Hbase Zookeeper servers are the same as the zookeeper servers
        zkServers
      }
      val formats = DefaultFormats +
        new EnumerationSerializer(
          VehicleCategory,
          ZkWheelbaseType,
          VehicleCode,
          ZkIndicationType,
          ServiceType,
          ZkCaseFileType,
          EsaProviderType,
          VehicleImageType,
          ConfigType)

      curator = Curator.createCurator(caller = this, system = actorSystem, zkServers = zkServers, formats = formats)

      RowKeyDistributorFactory.init(curator)
      // Create the actor that handles the scheduler task for cleanup
      actorSystem.actorOf(Props(new RunScheduledJobs(Paths.Systems.getSystemJobsPath("*") + "/cleanupJob",
        curator,
        startCleanupJob) with WaitForCompletion),
        "cleanup_JobScheduler")
    } else {
      log.error("Unknown Zookeeper servers. Required key {} not defined in config file", zkServerConfigPath)
    }
  }

  private var curator: Curator = _
  private var hbaseZkServers: String = ""
  private var minNrRecords = 1

  protected def startCleanupJob(zkJobPath: String, runToken: Option[RunToken], runner: ActorRef, curator: Curator) {
    // Create a CleanupJob actor and send the StartCleanup message to it
    runner ! CreateJobActor(() ⇒ new CleanupJob(curator, hbaseZkServers, runToken, zkJobPath, getSystemId(zkJobPath), minNrRecords), StartCleanup)
  }

  def shutdownActors() {
    actorSystem.stop(actorSystem.actorFor("/user/cleanup_JobScheduler"))
  }
}

object CleanupBoot {
  private def getSystemId(jobPath: String) = {
    val index = Paths.Systems.getDefaultPath.split('/').length
    jobPath.split('/')(index)
  }
}

/**
 * Specific details for starting up the cleanup jobs.
 */
class CleanupJob(val curator: Curator, hbaseZkServers: String, runToken: Option[RunToken], zkJobPath: String, systemId: String, minNrRecords: Int)
  extends CuratorActor {

  // Set the runner actor to it's default value
  var runner: ActorRef = _

  protected def receive = {
    case StartCleanup ⇒
      runner = sender
      runCleanup()
  }

  override def supervisorStrategy() = AllForOneStrategy() {
    case e: Exception ⇒
      log.error(e, "Cleanup instance failed. Scheduling rerun for: {}", zkJobPath)
      runner ! JobFailed(true)
      Stop
  }

  /**
   * Start up the cleanup job for this path, creating it if necessary.
   */
  def runCleanup() {
    import CleanupJob._

    /**
     * Define actor function (def). Each time calling this function,
     * you get an actor handling the cleaning of a specific table
     */
    def cleanupRecords = context.actorOf(Props(new CleanupTable(systemId, hbaseZkServers)))

    // Define one actor instance (val)
    val cleanupImages = context.actorOf(Props(new CleanupImages()))
    val cleanupZookeeper = context.actorOf(Props(new CleanupZookeeper(minNrRecords, curator)))

    val cleanupConfig = curator.get[CleanupJobConfig](zkJobPath + "/config").getOrElse(configDefault)

    log.info("Starting cleaning for system {}...", systemId)

    //create list of Futures. If no configuration found, do not add the Future to the list.
    // The Futures are executed in parallel on different Actor instances.
    val tableFutures: Seq[Future[CleanedMessage]] = curator.get[ZkSystem](Paths.Systems.getConfigPath(systemId)) match {
      case None ⇒
        log.error("Cleanup could not find settings for system {}. Use defaults.", systemId)
        Nil
      case Some(config) ⇒ {
        // Get the config values for how long violation and traffic data has saved.
        val (violationTTL: Int, passageTTL: Int) = (config.retentionTimes.nrDaysKeepViolations, config.retentionTimes.nrDaysKeepTrafficData)
        // Calculate the 'starttime' in milliseconds
        val passageCleanUntil = runToken.get.scheduledTime - passageTTL.days.toMillis
        val violationCleanUntil = runToken.get.scheduledTime - violationTTL.days.toMillis
        val statisticsCleanUntil = toYYYYMMDD(passageCleanUntil)

        val gantries = curator.getChildNames(Paths.Gantries.getDefaultPath(systemId))

        // Clean all the tables with the passage information
        val passageFutures = cleanupConfig.passageTables.getOrElse(configDefault.passageTables.get).map { tableName ⇒
          ask(cleanupRecords, DeleteRowsInTableWithTimestampAndPrefixes(tableName, passageCleanUntil, gantries.map(g ⇒ systemId + "-" + g + "-")))(Timeout(1 hours)).mapTo[CleanedMessage]
        }

        // Clean all the tables with the violation information
        val violationFutures = cleanupConfig.violationTables.getOrElse(configDefault.violationTables.get).map { tableName ⇒
          ask(cleanupRecords, DeleteRowsInTableWithTimestampAndPrefixes(tableName, violationCleanUntil, gantries.map(g ⇒ systemId + "-")))(Timeout(1 hours)).mapTo[CleanedMessage]
        }

        val statisticsFutures = cleanupConfig.statisticsTables.getOrElse(configDefault.statisticsTables.get).map { tableName ⇒
          //ask(cleanupRecords, CleanupTableRows(tableName, passageCleanUntil, Seq(Bytes.toBytes(systemId + "-" + statisticsCleanUntil + "-"))))(Timeout(1 hours)).mapTo[CleanedMessage]
          ask(cleanupRecords, DeleteRowsInTableWithTimestampAndPrefixes(tableName, passageCleanUntil, Seq(systemId + "-" + statisticsCleanUntil + "-")))(Timeout(1 hours)).mapTo[CleanedMessage]
        }

        // Clean all other tables
        val otherFutures = cleanupConfig.otherTables.getOrElse(configDefault.otherTables.get).map { tableName ⇒
          ask(cleanupRecords,
            DeleteRowsInTableWithTimestamp(tableName, passageCleanUntil))(Timeout(1 hours)).mapTo[CleanedMessage]
        }

        passageFutures ++ violationFutures ++ statisticsFutures ++ otherFutures
      }
    }

    val imageFutures: List[Future[CleanedMessage]] = curator.get[RegisterViolationsJobConfig](
      Paths.Systems.getSystemJobsPath(systemId) + "/registerViolationsJob/config") match {

        case None ⇒
          log.error("Cleanup could not find settings for registerViolationsJob in system {}.", systemId)
          Nil
        case Some(config) ⇒ {
          val imagesMessage = CleanupImagePathMessage(config.exportDirectoryPath, Set("*"), 10, CalendarTimeUnitType.DAYS, false)
          val imagesFuture = ask(cleanupImages, CleanupImagesMessage(Set(imagesMessage)))(Timeout(1 hours)).mapTo[CleanedMessage]
          List(imagesFuture)
        }
      }

    val corridors = curator.getChildNames(Paths.Corridors.getDefaultPath(systemId))
    val zkPaths = cleanupConfig.zookeeperPaths.getOrElse(configDefault.zookeeperPaths.get).
      map(_.replace("SYSTEMID", systemId)).
      flatMap { path ⇒
        corridors.map { corridor ⇒ path.replace("CORRIDORID", corridor) }
      }.toSet
    val zookeeperCleanUntil = runToken.get.scheduledTime -
      cleanupConfig.zookeeperRetentionDays.getOrElse(configDefault.zookeeperRetentionDays.get).days.toMillis

    val zookeeperFutures = zkPaths.map { zkPath ⇒
      ask(cleanupZookeeper, CleanZookeeperPath(zkPath, zookeeperCleanUntil))(Timeout(1 hours)).mapTo[CleanedMessage]
    }.toSeq

    import context.dispatcher
    //convert list of Futures to Future of lists
    val finalFuture = Future.sequence(tableFutures ++ imageFutures ++ zookeeperFutures)
    finalFuture onComplete {
      case Right(result) ⇒ {
        context.stop(cleanupRecords)
        context.stop(cleanupImages)
        runner ! JobComplete
      }
      case Left(failure) ⇒ {
        context.stop(cleanupRecords)
        context.stop(cleanupImages)
        runner ! JobFailed
      }
    }
  }

  def toYYYYMMDD(time: Long): String = {
    val format = new SimpleDateFormat("yyyyMMdd")
    format.format(new Date(time))
  }

}

object CleanupJob {
  val configDefault = CleanupJobConfig(
    passageTables = Some(Seq("vehicle")),
    violationTables = Some(Seq(
      "processedViolation",
      "speedRecord",
      "unmatchedVehicle",
      "vehicleClassified",
      "violation",
      "GlobalSystemsLog",
      "SelftestImage",
      "SelftestRegistration",
      "SelftestResult")),
    statisticsTables = Some(Seq("ServiceStatistics")),
    otherTables = Some(Seq("DacolianRecognitionCache",
      "DecisionResult",
      "matcherLeftOver",
      "mtmFile")),
    zookeeperPaths = Some(Seq(
      "/ctes/systems/SYSTEMID/jobs/enforceStatus-nl",
      "/ctes/systems/SYSTEMID/states",
      "/ctes/systems/SYSTEMID/statistics",
      "/ctes/systems/SYSTEMID/corridors/CORRIDORID/scheduleHistory",
      "/ctes/systems/SYSTEMID/jobs/registration",
      "/ctes/systems/SYSTEMID/log-history/failures")),
    zookeeperRetentionDays = Some(60))
}

case object StartCleanup

/**
 * Result indicator at the end of cleaning
 */
case class CleanedMessage(success: Boolean)

case class CleanupJobConfig(passageTables: Option[Seq[String]],
                            violationTables: Option[Seq[String]],
                            statisticsTables: Option[Seq[String]],
                            otherTables: Option[Seq[String]],
                            zookeeperPaths: Option[Seq[String]],
                            zookeeperRetentionDays: Option[Int])
