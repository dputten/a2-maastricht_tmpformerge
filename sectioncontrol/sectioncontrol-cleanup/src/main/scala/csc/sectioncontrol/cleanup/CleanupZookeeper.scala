package csc.sectioncontrol.cleanup

import csc.curator.utils.{ Curator, CuratorActor }

class CleanupZookeeper(minNrRecords: Int, protected val curator: Curator) extends CuratorActor {
  protected def receive = {
    case CleanZookeeperPath(path, cleanUntilTime) ⇒
      try {
        log.info("Cleaning zookeeper path {}", path)
        val nodes = curator.getChildren(path)
        getRecordsToDelete(nodes, cleanUntilTime).foreach {
          itemPath ⇒
            log.info("Deleting zookeeper item {}", itemPath)
            curator.safeDelete(itemPath)
        }
        sender ! CleanedMessage(true)
      } catch {
        case e: Exception ⇒
          log.error(e, "Failed to clean zookeeper path {}", path)
          sender ! CleanedMessage(false)
      }
  }

  def getRecordsToDelete(nodes: Seq[String], cleanUntilTime: Long): Seq[String] = {
    val stats = nodes.flatMap { itemPath ⇒
      {
        curator.stat(itemPath) match {
          case Some(stat) ⇒ Some(itemPath, stat.getMtime)
          case _ ⇒ {
            log.info("Cannot read Stat for path {}", itemPath)
            None
          }
        }
      }
    }
    CleanupZookeeper.filterRecords(stats, cleanUntilTime, minNrRecords)
  }

}

/**
 * CleanupZookeeper object to be able to create a unit test for the filter
 */
object CleanupZookeeper {

  def filterRecords(stats: Seq[(String, Long)], cleanUntilTime: Long, minNrRecords: Int): Seq[String] = {
    var sortedStat = stats.sortBy(_._2)
    //remove minNrRecords from this list
    for (i ← 0 until minNrRecords) {
      if (!sortedStat.isEmpty) {
        sortedStat = sortedStat.init
      }
    }
    //select the actual deletion when the record is too old
    sortedStat.filter { case (itemPath, mTime) ⇒ mTime < cleanUntilTime }.map(_._1)
  }
}
/**
 * Clean outdated data
 */
case class CleanZookeeperPath(path: String, cleanUntilTime: Long)
