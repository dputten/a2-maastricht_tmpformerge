/*
* Copyright (C) 2012 CSC. <http://www.csc.com>
*/
package csc.sectioncontrol.cleanup

import akka.actor.{ ActorLogging, Actor }
import com.sematext.hbase.wd.DistributedScanner
import csc.sectioncontrol.storagelayer.hbasetables.RowKeyDistributorFactory
import org.apache.hadoop.hbase.client._
import org.apache.hadoop.hbase.util.Bytes
import csc.hbase.utils.{ HBaseAdminTool, HBaseTableKeeper }
import org.apache.hadoop.hbase.filter.KeyOnlyFilter

/**
 * Clean old data for the specified system
 * @param systemId system ID for the tables to clean
 */
class CleanupTable(systemId: String, hbaseZkServers: String) extends Actor with ActorLogging with HBaseTableKeeper {

  def receive = {
    case init @ DeleteRowsInTableWithTimestamp(tableName, cleanUntilTime) ⇒ {
      val hbaseConfig = createConfig(hbaseZkServers)
      val hbaseAdmin = new HBaseAdmin(hbaseConfig)
      try {
        log.info("Cleaning table {}...", tableName)
        log.info("hbaseConfig: {}", hbaseConfig)
        if (!hbaseAdmin.tableExists(tableName)) {
          log.info("Table {} does not exists", tableName)
        } else {
          val table = new HTable(hbaseConfig, tableName)
          addTable(table)

          val scan = new Scan()
          scan.setTimeRange(0L, cleanUntilTime)
          scan.setFilter(new KeyOnlyFilter())

          val rowKeyDistributorr = RowKeyDistributorFactory.getDistributor(new String(table.getTableName))
          val scanner = DistributedScanner.create(table, scan, rowKeyDistributorr)

          var cleanedCount: Int = 0

          import collection.JavaConversions._
          try {
            for (result ← scanner) {
              val delete = new Delete(result.getRow)
              delete.setTimestamp(cleanUntilTime)
              table.delete(delete)
              cleanedCount += 1
            }
          } finally {
            scanner.close()
          }

          log.info("Finished cleaning table {} for {}", tableName, systemId)
          log.info("Deleted {} rows from table {} in system {}", cleanedCount, tableName, systemId)

          sender ! CleanedMessage(true)
        }
      } catch {
        case e: Exception ⇒
          log.error(e, "Failed to clean table {} for {}", tableName, systemId)
          sender ! CleanedMessage(false)
      } finally {
        hbaseAdmin.close()
        closeTables()
      }
    }

    case DeleteRowsInTableWithTimestampAndPrefixes(tableName, cleanupUntil, keyPrefixes) ⇒ {
      try {
        log.info("Cleaning rows for table [{}]", tableName)

        val rowkeyPrefixes = if (keyPrefixes.isEmpty) Seq[String]() else keyPrefixes
        val table = new HTable(createConfig(hbaseZkServers), tableName)
        addTable(table)

        val cleanupCount = keyPrefixes.map(prefix ⇒ cleanupRows(table, prefix, cleanupUntil))

        log.info("Finished cleaning table {} for system {}", tableName, systemId)
        log.info("Deleted {} rows from table {} in system {}", cleanupCount.sum, tableName, systemId)

        sender ! CleanedMessage(true)
      } catch {
        case e: Exception ⇒
          log.error(e, "Failed to clean table {} for {}", tableName, systemId)
          sender ! CleanedMessage(false)
      } finally {
        closeTables()
      }
    }
  }
  private def cleanupRows(table: HTable, rowKeyPrefix: String, cleanupUntil: Long): Int = {
    val from = rowKeyPrefix + 0L
    val to = rowKeyPrefix + cleanupUntil

    log.debug("cleaning table:[{}] with prefix: [{}], from:[{}], to=[{}]",
      table.getTableDescriptor.getNameAsString, rowKeyPrefix, from, to)

    val scan = new Scan(Bytes.toBytes(from), Bytes.toBytes(to))
    scan.setFilter(new KeyOnlyFilter())
    val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(new String(table.getTableName))
    val scanner = DistributedScanner.create(table, scan, rowKeyDistributer)

    //val scanner: ResultScanner = table.getScanner(scan)
    var cleanedCount: Int = 0

    import collection.JavaConversions._
    try {
      for (result ← scanner) {
        val delete = new Delete(result.getRow)
        table.delete(delete)
        cleanedCount += 1
      }
    } finally {
      scanner.close()
    }

    cleanedCount
  }
}

/**
 * Clean outdated data
 */
case class DeleteRowsInTableWithTimestamp(tableName: String, cleanUntilTime: Long)

//case class CleanupTableRows(tableName: String, cleanupUntil: Long, keyPrefixes: Seq[Array[Byte]])
case class DeleteRowsInTableWithTimestampAndPrefixes(tableName: String, cleanupUntil: Long, keyPrefixes: Seq[String])
