package csc.sectioncontrol.cleanup

import java.io.File
import java.util.{ Date, Calendar }
import akka.actor.{ ActorLogging, Actor }

/**
 * This class is used to delete images that are no longer required. Message CleanupImagesMessage
 * is used (received) that contains which files are to be removed
 */
class CleanupImages extends Actor with ActorLogging {
  def receive = {
    //receive a new message to clean up specific files
    case msg: CleanupImagesMessage ⇒ {
      log.info("Start images cleanup...")
      try {
        msg.directories.foreach {
          directory ⇒
            val dir = new File(directory.path)
            deleteFiles(dir = dir, currentDate = new Date, directory = directory, recursive = directory.recursive)
        }
        sender ! CleanedMessage(true)
      } catch {
        case _: Exception ⇒ sender ! CleanedMessage(false)
      }
      log.info("Finish images cleanup.")
    }
  }

  /**
   * used for actual deleting the files
   */
  private def deleteFiles(dir: File, currentDate: Date, directory: CleanupImagePathMessage, recursive: Boolean) {
    if (dir.exists && dir.isDirectory) {
      val files = dir.listFiles()
      files.foreach {
        file ⇒
          try {
            var isDeleted = false
            if (checkIfFileNeedsDeletion(file, directory, currentDate)) {
              if (file.isFile) {
                if (file.delete) {
                  log.warning("file is deleted path:[%s]".format(file.getAbsolutePath))
                  isDeleted = true
                } else {
                  log.warning("file could not be deleted path:[%s]".format(file.getAbsolutePath))
                }
              }
              if (file.isDirectory) {
                //At this moment if directory match creteria delete directory and all its contents
                //Better is to create new options for directories
                //delete this directory and all its children
                if (deleteDir(file)) {
                  log.warning("directory is deleted path:[%s]".format(file.getAbsolutePath))
                  isDeleted = true
                } else {
                  log.warning("directory could not be deleted path:[%s]".format(file.getAbsolutePath))
                }
              }
            }
            if (!isDeleted && recursive && file.isDirectory) {
              deleteFiles(dir = file, currentDate = currentDate, directory = directory, recursive = recursive)
            }
          } catch {
            case se: SecurityException ⇒ {
              log.warning("not allowed to remove file, path:[%s], message:[%s]".format(file.getAbsolutePath, se.getMessage))
            }
            case e: Exception ⇒ {
              log.error("Unexpected error occured message:[%s]".format(e.getMessage))
            }
          }
      }
    } else {
      log.warning("given path does not exists or is not a directory, path:[%s]".format(dir.getAbsolutePath))
    }
  }

  /**
   * checks to see if a given file must be deleted or not.
   */
  private def checkIfFileNeedsDeletion(file: File, directory: CleanupImagePathMessage, currentDate: Date): Boolean = {
    val dateCalendar = Calendar.getInstance()

    dateCalendar.setTime(currentDate)
    dateCalendar.add(directory.timeUnit.id, -directory.olderThen)

    directory.extensions.foreach(extension ⇒
      if ((file.getName.endsWith(extension) || extension == "*") &&
        new Date(file.lastModified).before(dateCalendar.getTime)) {
        return true
      })

    false
  }

  /**
   * Deletes all files and subdirectories under dir.
   * Returns true if all deletions were successful.
   * If a deletion fails, the method stops attempting to delete and returns false.
   */
  private def deleteDir(dir: File): Boolean = {
    if (dir.isDirectory) {
      val children = dir.list()
      for (fileName ← children) {
        val success = deleteDir(new File(dir, fileName))
        if (!success) {
          return false
        }
      }
    }
    // The directory is now empty so delete it
    dir.delete()
  }
}

//used with cleanupimages to indicate the time difference
object CalendarTimeUnitType extends Enumeration {
  type CalendarTimeUnitType = Value
  val YEAR = Value(Calendar.YEAR)
  val MONTH = Value(Calendar.MONTH)
  val DAYS = Value(Calendar.DATE)
  val HOUR = Value(Calendar.HOUR)
  val MINUTE = Value(Calendar.MINUTE)
  val SECOND = Value(Calendar.SECOND)
}

//message that is sent to cleanup old images
case class CleanupImagesMessage(directories: Set[CleanupImagePathMessage]) {
  require(directories != null, "Missing directories")
  require(!directories.isEmpty, "Empty directories")
}

//message that is sent to cleanup old images
case class CleanupImagePathMessage(path: String, extensions: Set[String], olderThen: Int, timeUnit: CalendarTimeUnitType.Value, recursive: Boolean = true) {
  require(path != null, "Missing path")
  require(!path.isEmpty, "Empty path")

  require(extensions != null, "Missing extensions")
  require(!extensions.isEmpty, "Empty extensions")
}

