#
# Copyright 2011 The Apache Software Foundation
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
# Merge regions in batches
#
# To see usage for this script, run:
#
#  ${HBASE_HOME}/bin/hbase org.jruby.Main online_merge.rb
#
require 'trollop'
include Java
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.hbase.HConstants
import org.apache.hadoop.hbase.HRegionInfo
import org.apache.hadoop.hbase.NotServingRegionException
import org.apache.hadoop.hbase.HTableDescriptor
import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.hadoop.hbase.HServerAddress
import org.apache.hadoop.hbase.regionserver.HRegion
import org.apache.hadoop.hbase.regionserver.Store
import org.apache.hadoop.hbase.regionserver.StoreFile
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.client.HBaseAdmin
import org.apache.hadoop.hbase.client.Delete
import org.apache.hadoop.hbase.client.Put
import org.apache.hadoop.hbase.client.Scan
import org.apache.hadoop.hbase.client.Get
import org.apache.hadoop.hbase.client.Result
import org.apache.hadoop.hbase.util.FSUtils
import org.apache.hadoop.hbase.util.Writables
import org.apache.hadoop.fs.Path
import org.apache.hadoop.fs.FileSystem
import org.apache.commons.logging.LogFactory
import org.apache.hadoop.ipc.RemoteException
import java.util.TreeSet
import java.util.HashMap
import java.util.ArrayList
import java.lang.Byte

# Name of this script
NAME = "online_merge"

require 'trollop'
opts = Trollop::options do
  banner <<-EOS
  This script will merge regions in batches specified by [step].
  The default step is 5, meaning that you will have 5 times less regions.
  This script will make sure that the balancer is disabled while it runs.
  Also the script defaults to do a dry run, you must use -n to merge.
  
  RECOMMENDED
  It\'s better to run this script on a table that\'s not likely to split
  Therefor you should first set the MAX_FILESIZE 4-5 times what it needs to be.
  Another reason for this is that merged regions could very well 
  start splitting again as soon as they open.

  Usage: online_merge.rb table_name [options]
  EOS
  opt :step, "Number of regions to merge at the same time", :default => 5
  opt :normalRun , "Do the merging, it won't by default", :default => false
  opt :startEncodedName, "Region to start merging from", :type => :string
  opt :endEncodedName, "Region to stop the merging at", :type => :string
end

tableName = ARGV[0]
Trollop::die "Missing a table" if tableName.nil?

step = opts[:step]
normalRun = opts[:normalRun]
startEncodedName = opts[:startEncodedName]
endEncodedName = opts[:endEncodedName]

c = HBaseConfiguration.create()

c.set("fs.default.name", c.get(HConstants::HBASE_DIR))
fs = FileSystem.get(c)

LOG = LogFactory.getLog(NAME)

admin = HBaseAdmin.new(c)

admin.balanceSwitch(false)

table = HTable.new(c, tableName)
meta = HTable.new(c, HConstants::META_TABLE_NAME)

scan = Scan.new(HRegionInfo.createRegionName(tableName.to_java_bytes, HConstants::EMPTY_START_ROW, HConstants::ZEROES, false))
scan.setCaching(100)
resultScanner = meta.getScanner(scan)

regions = HashMap.new
results = ArrayList.new
startReached = startEncodedName.nil?
endReached = endEncodedName.nil?
for result in resultScanner
  info = Writables.getHRegionInfo(
            result.getValue(HConstants::CATALOG_FAMILY,
                HConstants::REGIONINFO_QUALIFIER))
  break if info.getTableDesc.getNameAsString != tableName

  if not startReached
    startReached = !startEncodedName.nil? && info.getEncodedName == startEncodedName
    next if not startReached
  end

  if not endReached 
    endReached = !endEncodedName.nil? && info.getEncodedName == endEncodedName
    break if endReached
  end

  server = HServerAddress.new
  value = result.getValue(HConstants::CATALOG_FAMILY,
            HConstants::SERVER_QUALIFIER)

  if not value.nil? && value.length != 0
    server = HServerAddress.new(String.from_java_bytes(value))
  else
    LOG.fatal(String.from_java_bytes(result.getRow) + " has an null server, please fix that row")
    return if normalRun
  end
  results.add(result)
  regions.put(result.getRow, [result, info, server])
end

Trollop::die :startEncodedName, "Never encountered the start encoded name" if !startEncodedName.nil? && !startReached
Trollop::die :endEncodedName, "Never encountered the end encoded name" if !endEncodedName.nil? && !endReached

# Using a TreeSet sorts the regions
# sortedHRIs = TreeSet.new(regions.keySet)

tableDesc = table.getTableDescriptor

rootdir = FSUtils.getRootDir(c)

tableDir = Path.new(rootdir, tableDesc.getNameAsString)

# We'll loop until we run out of regions
while true
  toMerge = Array.new
  expectedDaughters = 0
  skip = false

  # Select the first regions, up to step
  for result in results
    row = result.getRow
    hri = regions.get(result.getRow)[1]
    LOG.info("Processing " + hri.getRegionNameAsString)
    if hri.isSplit
      # We already have a few regions we can merge, let's process those splits later
      # If there was one region already, we'll just skip it
      break if toMerge.length > 1

      skip = true
      daughterA = result.getValue(HConstants::CATALOG_FAMILY, HConstants::SPLITA_QUALIFIER)
      daughterB = result.getValue(HConstants::CATALOG_FAMILY, HConstants::SPLITB_QUALIFIER)
      expectedDaughters += 1 if not daughterA.nil?
      expectedDaughters += 1 if not daughterB.nil?
      toMerge << row
      if expectedDaughters == 0
        LOG.warn("Current region is a parent without daughters, may be cleaned by CatalogJanitor or bug?")
        break
      else
        next
      end
    end
    toMerge << row
    if expectedDaughters > 0
      for family in tableDesc.getFamiliesKeys
        familyPath = Store.getStoreHomedir(tableDir, hri.getEncodedName, family)
        fileStatuses = fs.listStatus(familyPath)
        isRef = false
        for file in fileStatuses
          isRef = StoreFile.isReference(file.getPath)
          if isRef
            expectedDaughters -= 1
            break
          end
        end
      break if isRef
      end 
    end

    break if toMerge.length >= step && !skip
  end

  if skip
    for row in toMerge
      LOG.info("Skipping region because of a split: " + String.from_java_bytes(row))
      results.remove(0)
    end
    next
  end
  
  # Are we done?
  break if toMerge.empty? || toMerge.length == 1

  LOG.info("Merging from " + String.from_java_bytes(toMerge.first) + " to " + String.from_java_bytes(toMerge.last))

  # First close all the regions, the call is async
  for row in toMerge
    begin
      server = regions.get(row)[2]
      if normalRun
        admin.closeRegion(row , server.getHostname + ":" + server.getPort.to_s)
      else
        LOG.info("DRY-RUN Would have closed " + String.from_java_bytes(row) + " on " + server.toString)
      end
    rescue RemoteException => e
      LOG.warn("The region might already be closed? " + String.from_java_bytes(row) + " because " + e )
    end
  end

  connection = admin.getConnection
  # Now verify that they are indeed close, wait if needed
  for row in toMerge
    while true
      hri = regions.get(row)[1]
      server = regions.get(row)[2]
      if connection.getHRegionConnection(server).getOnlineRegions.contains(hri)
        if normalRun
          sleep 1
        else
          LOG.info("DRY-RUN Would have checked if " + String.from_java_bytes(row) + " is still on " + server.toString)
          break
        end
      else
        if normalRun
          break
        else
          LOG.warn("DRY-RUN This region: " + String.from_java_bytes(row) + " wasn't on " + server.toString)
          return
        end
      end
    end
  end

  # Create the new region to cover all the others
  firstHRI = regions.get(toMerge.first)[1]
  lastHRI = regions.get(toMerge.last)[1]
  newHRI = HRegionInfo.new(tableDesc, firstHRI.getStartKey, lastHRI.getEndKey)
  encodedName = newHRI.getEncodedName
  newRegionDir = HRegion.getRegionDir(rootdir, newHRI)

  if (fs.exists(newRegionDir)) 
    LOG.info("The new region's folder already exists")
  end

  LOG.info("About to merge into " + newHRI.getRegionNameAsString)

  # For each family, move all the files one by one
  for family in tableDesc.getFamiliesKeys
    HRegion.makeColumnFamilyDirs(fs, tableDir, newHRI, family)
    newFamilyPath = Store.getStoreHomedir(tableDir, newHRI.getEncodedName, family)
    
    for row in toMerge
      hri = regions.get(row)[1]
      oldFamilyPath = Store.getStoreHomedir(tableDir, hri.getEncodedName, family)
      fileStatuses = fs.listStatus(oldFamilyPath)
      for file in fileStatuses
        next if file.isDir
        LOG.info((normalRun ? "M" : "DRY-RUN Would be m" ) + "oving this store file: " + file.getPath.toString)
        fs.rename(file.getPath, Path.new(newFamilyPath, file.getPath.getName)) if normalRun
      end
      fs.delete(oldFamilyPath, true) if normalRun
    end
  end

  # Add the new region entry in .META.
  put = Put.new(newHRI.getRegionName)
  put.add(HConstants::CATALOG_FAMILY, HConstants::REGIONINFO_QUALIFIER,
        Writables.getBytes(newHRI))
  if normalRun
    meta.put(put)
  else
    LOG.info("DRY-RUN Would have added the new .META. row " + newHRI.getRegionNameAsString)
  end

  # Clean all the old regions
  for row in toMerge
    # This is a dummy call, all it really does is cleaning the master's memory
    hri = regions.get(row)[1]
    if normalRun
      admin.unassign(row, true)
      delete = Delete.new(row)
      meta.delete(delete)
      fs.delete(HRegion.getRegionDir(rootdir, hri), true)
    else
      LOG.info("DRY-RUN Would have cleared " + String.from_java_bytes(row) + " from .META. and deleted its folder")
    end
    results.remove(0)
  end
   
  # Finally, online the new region
  if normalRun
    admin.assign(newHRI.getRegionName, true)
  else
    LOG.info("DRY-RUN Would have assigned " + newHRI.getRegionNameAsString)
  end
end

admin.balanceSwitch(true)
