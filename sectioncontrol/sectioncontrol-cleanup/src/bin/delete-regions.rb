#
# Delete regions
#
# To see usage for this script, run:
# The script has to be run by the hbase user
#  ${HBASE_HOME}/bin/hbase org.jruby.Main delete-regions.rb
#
require 'trollop'
include Java
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.hbase.HConstants
import org.apache.hadoop.hbase.HRegionInfo
import org.apache.hadoop.hbase.NotServingRegionException
import org.apache.hadoop.hbase.HTableDescriptor
import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.hadoop.hbase.HServerAddress
import org.apache.hadoop.hbase.regionserver.HRegion
import org.apache.hadoop.hbase.regionserver.Store
import org.apache.hadoop.hbase.regionserver.StoreFile
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.client.HBaseAdmin
import org.apache.hadoop.hbase.client.Delete
import org.apache.hadoop.hbase.client.Put
import org.apache.hadoop.hbase.client.Scan
import org.apache.hadoop.hbase.client.Get
import org.apache.hadoop.hbase.client.Result
import org.apache.hadoop.hbase.util.FSUtils
import org.apache.hadoop.hbase.util.Writables
import org.apache.hadoop.hbase.util.HBaseFsck
import org.apache.hadoop.fs.Path
import org.apache.hadoop.fs.FileSystem
import org.apache.commons.logging.LogFactory
import org.apache.hadoop.ipc.RemoteException
import java.util.TreeSet
import java.util.HashMap
import java.util.ArrayList
import java.lang.Byte

# Name of this script
NAME = "delete_empty_regions"

def closeRegions(log, admin, normal_run, regions)
    log.info("Start closing regions")
    #close the deletion rows
    for region in regions
        begin
          result = region[0]
          info = region[1]
          server = region[2]
          if normal_run
            admin.closeRegion(result.getRow , server.getHostname + ":" + server.getPort.to_s)
          else
            log.info("DRY-RUN Would have closed " + String.from_java_bytes(result.getRow) + " on " + server.toString)
          end
        rescue RemoteException => e
          log.warn("The region might already be closed? " + String.from_java_bytes(result.row) + " because " + e )
        end
    end

    log.info("Wait for regions to close")
    connection = admin.getConnection
    # Now verify that they are indeed close, wait if needed
    sortedRegions = regions.sort { |x, y| x[2] <=> y[2] }
    currentServer = ""
    currentList = nil

    for region in sortedRegions
       result = region[0]
       hri = region[1]
       server = region[2]
       if currentServer != server.toString
         currentServer = server.toString
         currentList = connection.getHRegionConnection(server).getOnlineRegions
         log.info("Initial Update regionList for server " + server.toString)
       end

       while true
          if currentList.contains(hri)
            if normal_run
              sleep 1
              currentList = connection.getHRegionConnection(server).getOnlineRegions
              log.info("Update regionList for server " + server.toString)
            else
#              log.info("DRY-RUN Would have checked if " + String.from_java_bytes(result.row) + " is still on " + server.toString)
              break
            end
          else
            if normal_run
              break
            else
              log.warn("DRY-RUN This region: " + String.from_java_bytes(result.row) + " wasn't on " + server.toString)
              break
            end
          end
        end
    end
end

def createMergedRegion(log, fs, rootdir, admin, tableDesc, tableDir, meta, normal_run, regions)
    #create the new empty region first, to enable the different processes to query the table
    firstHRI = regions[0][1]
    lastHRI = regions[regions.size-1][1]
    newHRI = HRegionInfo.new(tableDesc, firstHRI.getStartKey, lastHRI.getEndKey)
    if normal_run
        # Create the new region to cover all the others
        encodedName = newHRI.getEncodedName
        newRegionDir = HRegion.getRegionDir(rootdir, newHRI)

        if (fs.exists(newRegionDir))
          log.info("The new region's folder already exists")
        end

        for family in tableDesc.getFamiliesKeys
           HRegion.makeColumnFamilyDirs(fs, tableDir, newHRI, family)
        end
        # Add the new region entry in .META.
        put = Put.new(newHRI.getRegionName)
        put.add(HConstants::CATALOG_FAMILY, HConstants::REGIONINFO_QUALIFIER,
                Writables.getBytes(newHRI))
        meta.put(put)
        # Finally, online the new region
        admin.assign(newHRI.getRegionName, true)
    else
       log.info("DRY-RUN Would have added the new .META. row " + newHRI.getRegionNameAsString)
    end
end

def deleteRegions(log, fs, rootdir, meta, normal_run, regions)
    if normal_run
       log.info("Start deleting regions")
       #delete regions rows from .META. in one batch
       deletions = ArrayList.new
       for region in regions
           result = region[0]
           deletions.add(Delete.new(result.row))
       end
       #do the actual deletion
       meta.delete(deletions)

       #delete regions directories rows from FileSystem
       for region in regions
          info = region[1]
          fs.delete(HRegion.getRegionDir(rootdir, info), true)
       end
    else
       log.info("DRY-RUN Would have deleted regions")
    end
end

require 'trollop'
opts = Trollop::options do
  banner <<-EOS
  This script will delete empty regions in batches specified by [maxDelete]
  and replace it by one empty region.
  The default step is -1, meaning that you remove all empty regions.
  The script has to be run by the hbase user

  Also the script defaults to do a dry run, you must use -n to actually delete
  the regions.

  Usage: ${HBASE_HOME}/bin/hbase org.jruby.Main delete-regions.rb table_name [options]
  EOS
  opt :normalRun , "Do the deletion, it won't by default", :default => false
  opt :startEncodedName, "Region start-key to start deleting from", :type => :string
  opt :endEncodedName, "Region start-key to stop the deleting at", :type => :string
  opt :maxDelete, "Maximum number of regions to be deleted in one block", :default => -1
  opt :nrBlocks, "Maximum number of empty region blocks to be deleted", :default => -1
end

tableName = ARGV[0]
Trollop::die "Missing a table" if tableName.nil?

normalRun = opts[:normalRun]
startEncodedName = opts[:startEncodedName]
endEncodedName = opts[:endEncodedName]
MAX_REGIONS_DELETE = opts[:maxDelete]
MAX_BLOCK_REGIONS = opts[:nrBlocks]

c = HBaseConfiguration.create()

c.set("fs.default.name", c.get(HConstants::HBASE_DIR))
fs = FileSystem.get(c)

LOG = LogFactory.getLog(NAME)

admin = HBaseAdmin.new(c)

admin.balanceSwitch(false)

table = HTable.new(c, tableName)
tableDesc = table.getTableDescriptor
rootdir = FSUtils.getRootDir(c)
tableDir = Path.new(rootdir, tableDesc.getNameAsString)

meta = HTable.new(c, HConstants::META_TABLE_NAME)

scan = Scan.new(HRegionInfo.createRegionName(tableName.to_java_bytes, HConstants::EMPTY_START_ROW, HConstants::ZEROES, false))
scan.setCaching(100)
resultScanner = meta.getScanner(scan)

startReached = startEncodedName.nil?
endReached = endEncodedName.nil?
#did we reached the maximum blocksize and skipping the empty regions until a filled region is found
skipEmpty = false
#list of empty region blocks
regionBlocks = ArrayList.new
#list of empty regions of the current block of regions
currentBlock = ArrayList.new

for result in resultScanner

  #get the info of the region
  info = Writables.getHRegionInfo(
            result.getValue(HConstants::CATALOG_FAMILY,
                HConstants::REGIONINFO_QUALIFIER))
  break if info.getTableDesc.getNameAsString != tableName
  # check if we have need to check the key range
  if not startReached
    startReached = !startEncodedName.nil? &&  String.from_java_bytes(info.getStartKey) == startEncodedName
    next if not startReached
  end

  if not endReached
    endReached = !endEncodedName.nil? &&  String.from_java_bytes(info.getStartKey) == endEncodedName
    break if endReached
  end

  #check the server information
  server = HServerAddress.new
  value = result.getValue(HConstants::CATALOG_FAMILY,
            HConstants::SERVER_QUALIFIER)

  if not value.nil? && value.length != 0
    server = HServerAddress.new(String.from_java_bytes(value))
  else
    LOG.fatal(String.from_java_bytes(result.getRow) + " has an null server, please fix that row")
    return if normalRun
  end

  # For each family, check if the files are empty
  empty = true
  for family in tableDesc.getFamiliesKeys
      oldFamilyPath = Store.getStoreHomedir(tableDir, info.getEncodedName, family)
      fileStatuses = fs.listStatus(oldFamilyPath)
      empty = false if fileStatuses.size > 0
  end

  #add record to current block if it is empty and not already reached the maximum
  if empty && !skipEmpty
    currentBlock.add([result, info, server])
  end

 #stop the empty skipping when the region has data
  if skipEmpty && !empty
    skipEmpty = false
  end

  #check if this is the first region with data after a empty region block
  #which signal the end of the block
  if currentBlock.size > 0 && !empty
    LOG.info("Found block of empty regions size ("+ String(currentBlock.size) +")")
    regionBlocks.add(currentBlock)
    currentBlock = ArrayList.new
  end
  #check if we reached the maximum of regions to be in one block
  if MAX_REGIONS_DELETE > 0 && currentBlock.size >= MAX_REGIONS_DELETE
    LOG.info("Stop criteria reached " + String(MAX_REGIONS_DELETE) + " regions deleted")
    regionBlocks.add(currentBlock)
    currentBlock = ArrayList.new
    skipEmpty = true
  end
  #check if we had reached the maximum number of blocks to be processed
  if MAX_BLOCK_REGIONS > 0 && regionBlocks.size >= MAX_BLOCK_REGIONS
    LOG.info("Stop criteria reached number of empty region blocks " + String(regionBlocks.size))
    break
  end
end

#if the current block is filled add it to the block list.
#should only happen when the block is at the end
if currentBlock.size > 0
    regionBlocks.add(currentBlock)
    LOG.warn("Empty region block found at the end")
end

Trollop::die :startEncodedName, "Never encountered the start encoded name" if !startEncodedName.nil? && !startReached
Trollop::die :endEncodedName, "Never encountered the end encoded name" if !endEncodedName.nil? && !endReached


LOG.info(("Found ")  + String(regionBlocks.size) + " empty region blocks")
if regionBlocks.size == 0
    LOG.info("Stop processing: Found no empty regions.")
    return
end

# regions contains the regions to be deleted
LOG.info("Start processing "+String(regionBlocks.size)+" blocks with empty regions.")
for regions in regionBlocks

    if regions.size == 1
        LOG.info("Block found with only one empty region ["+
            String.from_java_bytes(regions[0][1].getStartKey) +
            ","+
            String.from_java_bytes(regions[0][1].getEndKey)+
            "]")
        next
    end

    createMergedRegion(LOG, fs, rootdir, admin, tableDesc, tableDir, meta, normalRun, regions)

    closeRegions(LOG, admin, normalRun, regions)

    deleteRegions(LOG, fs, rootdir, meta, normalRun, regions)

    LOG.info((normalRun ? "Deleted " : "DRY-RUN Would delete ")  + String(regions.size) + " regions")
end

admin.balanceSwitch(true)

LOG.info("Script is finished.")