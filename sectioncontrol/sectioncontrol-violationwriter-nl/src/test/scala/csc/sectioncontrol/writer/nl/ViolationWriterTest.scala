/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.writer.nl

import java.io.File
import org.scalatest.matchers.MustMatchers
import org.apache.commons.io.{ IOUtils, FileUtils }
import org.scalatest.{ BeforeAndAfterEach, BeforeAndAfterAll, WordSpec }
import csc.sectioncontrol.messages.{ EsaProviderType, VehicleCode, ProcessingIndicator }
import ProcessingIndicator._
import csc.sectioncontrol.messages.certificates.MeasurementMethodType
import csc.sectioncontrol.enforce.common.nl.violations.ViolationGroupParameters
import csc.sectioncontrol.enforce.nl.casefiles.CaseFile
import csc.sectioncontrol.enforce.common.nl.messages.Constants._
import akka.util.duration._
import csc.sectioncontrol.enforce.common.nl.services._
import csc.sectioncontrol.enforce.register.CountryCodes
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storagelayer.system.SystemService
import csc.sectioncontrol.enforce.common.nl.violations.CalibrationTestPhoto
import csc.sectioncontrol.enforce.common.nl.messages.SpeedIndicationSign
import csc.sectioncontrol.storage.SerialNumber
import csc.sectioncontrol.messages.certificates.MeasurementMethodType
import csc.sectioncontrol.enforce.nl.casefiles.HHMVS40
import csc.sectioncontrol.enforce.common.nl.violations.CorridorInfo
import csc.sectioncontrol.enforce.common.nl.services.SectionControl
import csc.sectioncontrol.enforce.common.nl.messages.ViolationData
import scala.Some
import csc.sectioncontrol.enforce.nl.casefiles.HHMVS14
import csc.sectioncontrol.messages.IndicatorReason
import csc.sectioncontrol.enforce.nl.casefiles.TCVS33
import csc.sectioncontrol.enforce.common.nl.violations.CalibrationTestResult
import csc.sectioncontrol.storage.ZkSystem
import csc.sectioncontrol.enforce.common.nl.violations.ViolationGroupParameters
import csc.sectioncontrol.enforce.common.nl.messages.HeaderInfo
import csc.sectioncontrol.enforce.common.nl.services.TargetGroup
import csc.curator.utils.Versioned
import csc.sectioncontrol.messages.certificates.TypeCertificate
import csc.sectioncontrol.enforce.common.nl.services.SpeedFixed
import csc.sectioncontrol.storage.ZkSystemRetentionTimes
import csc.sectioncontrol.storage.ZkSystemLocation
import csc.sectioncontrol.enforce.common.nl.messages.SystemParameters
import csc.sectioncontrol.enforce.common.nl.services.RedLight
import csc.sectioncontrol.messages.certificates.ComponentCertificate
import csc.sectioncontrol.enforce.common.nl.messages._
import csc.sectioncontrol.enforce.register.CountryCodes
import csc.sectioncontrol.storage.{ ExportDirectoryForSpeedLimit, ZkCaseFileConfig, ServiceType }
import csc.sectioncontrol.storagelayer.caseFile.CaseFileConfigService

class SystemServiceMock() extends SystemService {
  def get(systemId: String): Option[ZkSystem] = {
    Some(ZkSystemBuilder.get())
  }

  def getVersioned(systemId: String): Option[Versioned[ZkSystem]] = {
    None
  }

  def update(systemId: String, mailServer: ZkSystem, version: Int) = {

  }

  def delete(systemId: String) = {

  }

  def exists(systemId: String) = {
    true
  }
}

object ZkSystemBuilder {
  def get(): ZkSystem = {
    new ZkSystem(id = "A2",
      name = "A2",
      title = "A2",
      mtmRouteId = "a200l",
      violationPrefixId = "",
      location = ZkSystemLocation(
        description = "",
        viewingDirection = Some(""),
        roadNumber = "",
        roadPart = "",
        region = "",
        systemLocation = ""),
      serialNumber = SerialNumber(""),
      orderNumber = 0,
      maxSpeed = 0,
      compressionFactor = 0,
      retentionTimes = ZkSystemRetentionTimes(
        nrDaysKeepTrafficData = 0,
        nrDaysKeepViolations = 0,
        nrDaysRemindCaseFiles = 0),
      pardonTimes = ZkSystemPardonTimes(pardonTimeEsa_secs = 5000, pardonTimeTechnical_secs = 5000),
      esaProviderType = EsaProviderType.Dynamax,
      pshtmId = Some("A120"))
  }
}
/**
 * @author Maarten Hazewinkel
 */
class ViolationWriterTest extends WordSpec
  with MustMatchers
  with BeforeAndAfterEach
  with BeforeAndAfterAll {

  val baseTestDir = new File(FileUtils.getTempDirectory, "csc.sectioncontrol.writer.nl.ViolationWriterTest")

  val jun_04_2000_21_59 = 960148740000L

  val corridorInfo = CorridorInfo(corridorId = 1,
    corridorEntryLocation = "Entry",
    corridorExitLocation = "Exit",
    corridorLength = 1578,
    roadCode = 12,
    drivingDirectionFrom = Some("Beginning"),
    drivingDirectionTo = Some("End"),
    locationLine1 = "Here",
    locationLine2 = None,
    locationCode = "01234")
  val globalParameters = ViolationGroupParameters(systemId = "HHM1",
    systemName = "HHM1",
    countryCodes = CountryCodes(),
    routeId = "EG33",
    corridorInfo = corridorInfo,
    mtmRouteId = "a002l",
    violationsDate = jun_04_2000_21_59,
    radarCode = 2,
    pshTmIdFormat = "[YY][MM][DD][HHHH]",
    hardwareSoftwareSeals = List(ComponentCertificate("DeelA", "1234"),
      ComponentCertificate("DeelB", "5678")),
    headerInfo = HeaderInfo("Header1", "Header2", new MeasurementMethodType(typeDesignation = "XX123",
      category = "A",
      unitSpeed = "km/h",
      unitRedLight = "s",
      unitLength = "m",
      restrictiveConditions = "",
      displayRange = "20-250 km/h",
      temperatureRange = "temperatuurbereik -10 to 50 graden Celsius",
      permissibleError = "toelatbare fout 3%",
      typeCertificate = new TypeCertificate("eg33-cert", 3,
        List(ComponentCertificate("matcher1", "blah1"),
          ComponentCertificate("vr2", "blah2"))))),
    roadPart = "L",
    pshtmId = "A120")

  val testPhotoBytes: Array[Byte] = IOUtils.toByteArray(getClass.getResourceAsStream("testphoto.jpg"))
  val calibrationPhoto = CalibrationTestPhoto(cameraId = 2,
    time = jun_04_2000_21_59 + 3.seconds.toMillis,
    image = testPhotoBytes)

  val baseViolation = ViolationData(processingIndicator = Automatic,
    countryCodes = CountryCodes(),
    reason = IndicatorReason(),
    exportLicensePlate = "XX00XX",
    vehicleClass = Some(VehicleCode.PA),
    measuredSpeed = 101,
    vehicleCountryCode = Some("NL"),
    reportingOfficerCode = "VC1234",
    pshTmId = "000125EG33",
    indicatedSpeed = SpeedIndicationSign(signIndicator = false),
    corridorEntryTime = jun_04_2000_21_59 + 4.seconds.toMillis + 92L,
    corridorExitTime = jun_04_2000_21_59 + 53.seconds.toMillis,
    violationJarSha = "SHA-1",
    images = Seq())

  override protected def beforeEach() {
    if (baseTestDir.exists()) FileUtils.forceDelete(baseTestDir)
    FileUtils.forceMkdir(baseTestDir)
  }

  val mtmRawText: String = "Raw MTM Data\r\nRaw MTM Data\r\n"

  val mtmInfoText: String = "" +
    "# Locatie\r\n" +
    "# A2 EG033\r\n" +
    "# Datum 2000/06/04\r\n" +
    "# \r\n" +
    "#locatie |tijdstip           |maxsnelheid|\r\n" +
    "|A2 EG033|2000/06/04 00:00:00|onbepaald  |\r\n" +
    "|A2 EG033|2000/06/04 21:59:00|onbepaald  |\r\n" +
    "|A2 EG033|2000/06/04 24:00:00|onbepaald  |\r\n"

  val ctresExpectedTextWithoutHash: String = "" +
    "1,215903,hhm1,000604A120,2,A 1,AB123,1,2,3,4,5,6,7,8,9,10\r\n"
  // The last item checksum is the checksum off everything before it.
  //  val ctresExpectedText: String = "" +
  //    "1,215903,hhm1,000604EG33,2,A 1,AB123,1,2,3,4,5,6,7,8,9,10\r\n" +
  //    "8a4b70a0e2b65866624eca3f7d5a6ad6\r\n"

  private def setupBasicViolationWriter(timeOffset: Long = 0L, caseFile: CaseFile = new HHMVS14(), service: Service = new SectionControl(),
                                        esaProviderType: EsaProviderType.Value = EsaProviderType.Mtm,
                                        headerInfo: HeaderInfo = HeaderInfo("systemName", "serialNumber", createMeasurmentType()),
                                        caseFileConfigService: CaseFileConfigServiceMock = new CaseFileConfigServiceMock) = {
    val writer = ViolationWriter(baseTestDir.getAbsolutePath,
      globalParameters.copy(headerInfo = headerInfo, violationsDate = globalParameters.violationsDate + timeOffset),
      mtmRawText.getBytes(fileExportCharSet),
      mtmInfoText.getBytes(fileExportCharSet),
      caseFile = caseFile,
      service = service,
      esaProviderType = esaProviderType, optSpeedLimit = Some(100), caseFileConfigService = caseFileConfigService)

    writer.add(CalibrationTestResult(testedOk = true,
      time = jun_04_2000_21_59 + 3.seconds.toMillis + timeOffset,
      reportingOfficerCode = "VC1234",
      cameraCount = 2,
      softwareSeals = List(ComponentCertificate("A", "1")),
      serialNumbers = List("AB123"),
      parameters = SystemParameters("1", 2, 3, 4, 5, "6", "7", 8, 9, 10),
      serviceType = ServiceType.SectionControl,
      testPhotos = List(calibrationPhoto,
        calibrationPhoto.copy(cameraId = 4,
          image = testPhotoBytes ++ testPhotoBytes))))
    writer
  }

  def writeSomeViolations(writer: ViolationWriter) {
    writer.add(baseViolation.copy(processingIndicator = Automatic), testPhotoBytes.take(20), Some(testPhotoBytes.take(21)))
    writer.add(baseViolation.copy(processingIndicator = Automatic), testPhotoBytes.take(22), Some(testPhotoBytes.take(23)))
    writer.add(baseViolation.copy(processingIndicator = Automatic), testPhotoBytes.take(24), Some(testPhotoBytes.take(25)))
    writer.add(baseViolation.copy(processingIndicator = Manual), testPhotoBytes.take(26), Some(testPhotoBytes.take(27)))
    writer.add(baseViolation.copy(processingIndicator = Manual), testPhotoBytes.take(28), Some(testPhotoBytes.take(29)))
    writer.add(baseViolation.copy(processingIndicator = Mobi), testPhotoBytes.take(30), Some(testPhotoBytes.take(31)))
  }

  case class WriterOutput(automaticLines: Seq[String], manualLines: Seq[String], mobiLines: Seq[String], root: File, service: Service, caseFile: CaseFile, headerInfo: HeaderInfo) {
    import collection.JavaConversions._

    def exists(path: String): Boolean = new File(root, path).exists
    def printPaths() {
      FileUtils.listFiles(root, null, true).foreach {
        fileName ⇒ println("FILE => " + fileName.getPath)
      }
    }
    def getOutput(path: String): Map[Int, String] = {
      val pathToFile = new File(root, path)
      io.Source.fromFile(pathToFile, fileExportCharSet.name()).getLines().zipWithIndex.toMap.map(_.swap)
    }

    def printOutput(path: String) {
      getOutput(path).foreach {
        line ⇒ println(line._1 + ":" + line._2)
      }
    }
  }

  def writeViolations(service: Service, caseFile: CaseFile, headerInfo: HeaderInfo = HeaderInfo("", "", createMeasurmentType())) = {
    val writer = setupBasicViolationWriter(service = service, caseFile = caseFile, headerInfo = headerInfo)
    writeSomeViolations(writer)
    writer.flush()
    val packDir = new File(new File(baseTestDir, "a0021001"), "20000604")
    val automaticFiles = new File(new File(packDir, "auto"), "files")
    val manualFiles = new File(new File(packDir, "hand"), "files")
    val mobiFiles = new File(new File(packDir, "mobi"), "files")

    //val automaticFiles = new File(new File(packDir, "auto"), "files")

    val automaticLines = io.Source.fromFile(new File(automaticFiles, "zaakauto.txt"), fileExportCharSet.name()).getLines().toList
    val manualLines = io.Source.fromFile(new File(manualFiles, "zaakhand.txt"), fileExportCharSet.name()).getLines().toList
    val mobiLines = io.Source.fromFile(new File(mobiFiles, "zaakmobi.txt"), fileExportCharSet.name()).getLines().toList
    val flgAutomatic = new File(new File(packDir, "auto"), "files")

    WriterOutput(
      automaticLines = automaticLines,
      manualLines = manualLines,
      mobiLines = mobiLines,
      root = packDir,
      service = service,
      caseFile = caseFile,
      headerInfo = headerInfo)
  }

  def defaultHeaderInfo = HeaderInfo(systemName = "systemName", serialNumber = "serialNumber", methodType = createMeasurmentType())

  "A ViolationWriter" must {
    "create the right headers" when {
      "service is 'SectionControl' and caseFile is 'TCVS33'" in {
        val output = writeViolations(new SectionControl(), new TCVS33(), defaultHeaderInfo)
        List(output.automaticLines, output.manualLines, output.mobiLines).foreach {
          lines ⇒
            lines(0) must be("# Trajectcontrolesysteem " + defaultHeaderInfo.systemName)
            lines(1) must be("# Categorie A meetmiddel")
            lines(2) must be("# NMi typegoedkeuringsnummer " + defaultHeaderInfo.methodType.typeCertificate.id)
            lines(3) must be("# Serienummer " + defaultHeaderInfo.serialNumber)
            lines(4) must be("# Snelheden in km/h")
            lines(5) must be("# Trajectlengtes in m")
        }
      }
      "service is 'SectionControl' and caseFile is 'HHMVS14'" in {
        val output = writeViolations(new SectionControl(), new HHMVS14(), defaultHeaderInfo)
        List(output.automaticLines, output.manualLines, output.mobiLines).foreach {
          lines ⇒
            lines(0) must be("# Trajectcontrolesysteem " + defaultHeaderInfo.systemName)
            lines(1) must be("# Categorie A meetmiddel")
            lines(2) must be("# Typegoedkeuringsnummer " + defaultHeaderInfo.methodType.typeCertificate.id)
            lines(3) must be("# Serienummer " + defaultHeaderInfo.serialNumber)
            lines(4) must be("# Interfaceversie v1.4")
            lines(5) must be("# Snelheden in km/h")
            lines(6) must be("# Trajectlengtes in m")
        }
      }
      "service is 'SpeedFixed' and caseFile is 'TCVS33'" in {
        pending
      }
      "service is 'SpeedFixed' and caseFile is 'HHMVS14'" in {
        val output = writeViolations(new SpeedFixed(), new HHMVS14(), defaultHeaderInfo)
        List(output.automaticLines, output.manualLines, output.mobiLines).foreach {
          lines ⇒
            lines(0) must be("# Snelheidscontrole " + defaultHeaderInfo.systemName)
            lines(1) must be("# Categorie A meetmiddel")
            lines(2) must be("# Typegoedkeuringsnummer " + defaultHeaderInfo.methodType.typeCertificate.id)
            lines(3) must be("# Serienummer " + defaultHeaderInfo.serialNumber)
            lines(4) must be("# Interfaceversie v1.4")
            lines(5) must be("# Snelheden in km/h")
        }
      }
      "service is 'SpeedFixed' and caseFile is 'HHMVS40'" in {
        val output = writeViolations(new SpeedFixed(), new HHMVS40(), defaultHeaderInfo)
        List(output.automaticLines, output.manualLines, output.mobiLines).foreach {
          lines ⇒
            lines(0) must be("# Snelheidscontrole " + defaultHeaderInfo.systemName)
            lines(1) must be("# Categorie A meetmiddel")
            lines(2) must be("# Typegoedkeuringsnummer " + defaultHeaderInfo.methodType.typeCertificate.id)
            lines(3) must be("# Serienummer " + defaultHeaderInfo.serialNumber)
            lines(4) must be("# Interfaceversie v1.4")
            lines(5) must be("# Snelheden in km/h")
        }
      }
      "service is 'SpeedMobile' and caseFile is 'TCVS33'" in {
        pending
      }
      "service is 'SpeedMobile' and caseFile is 'HHMVS14'" in {
        pending
      }
      "service is 'SpeedMobile' and caseFile is 'HHMVS40'" in {
        pending
      }
      "service is 'RedLight' and caseFile is 'TCVS33'" in {
        pending
      }
      "servicetype is 'RedLight' and caseFile is 'HHMVS14'" in {
        val output = writeViolations(new RedLight(pardonRedTime = 10, minimumYellowTime = 20, factNumber = "1234"), new HHMVS14(), defaultHeaderInfo)
        List(output.automaticLines, output.manualLines, output.mobiLines).foreach {
          lines ⇒
            lines(0) must be("# Roodlichtcontrole " + defaultHeaderInfo.systemName)
            lines(1) must be("# Categorie A meetmiddel")
            lines(2) must be("# Typegoedkeuringsnummer " + defaultHeaderInfo.methodType.typeCertificate.id)
            lines(3) must be("# Serienummer " + defaultHeaderInfo.serialNumber)
            lines(4) must be("# Interfaceversie v1.4")
            lines(5) must be("# Rood- en geeltijden in s")
        }
      }
      "servicetype is 'RedLight' and caseFile is 'HHMVS40'" in {
        val output = writeViolations(new RedLight(pardonRedTime = 10, minimumYellowTime = 20, factNumber = "1234"), new HHMVS14(), defaultHeaderInfo)
        List(output.automaticLines, output.manualLines, output.mobiLines).foreach {
          lines ⇒
            lines(0) must be("# Roodlichtcontrole " + defaultHeaderInfo.systemName)
            lines(1) must be("# Categorie A meetmiddel")
            lines(2) must be("# Typegoedkeuringsnummer " + defaultHeaderInfo.methodType.typeCertificate.id)
            lines(3) must be("# Serienummer " + defaultHeaderInfo.serialNumber)
            lines(4) must be("# Interfaceversie v1.4")
            lines(5) must be("# Rood- en geeltijden in s")
        }
      }
    }
    "create the correct .flg file (sturingsbestand)" when {
      "servicetype is 'Roodlicht'" in {
        val output = writeViolations(new RedLight(pardonRedTime = 10, minimumYellowTime = 20, factNumber = "1234"),
          new HHMVS14(), defaultHeaderInfo)

        output.service.flgFileName must be(Some("roodlicht.flg"))
        output.exists("auto/files/" + output.service.flgFileName.get) must be(true)
        output.exists("auto/files/" + output.service.flgFileName.get) must be(true)
        output.exists("hand/files/" + output.service.flgFileName.get) must be(true)
        output.exists("mobi/files/" + output.service.flgFileName.get) must be(true)
      }
      "servicetype is 'SectionControl'" in {
        val output = writeViolations(new SectionControl(), new HHMVS14(), defaultHeaderInfo)

        output.service.flgFileName must be(Some("traject.flg"))
        output.exists("auto/files/" + output.service.flgFileName.get) must be(true)
        output.exists("auto/files/" + output.service.flgFileName.get) must be(true)
        output.exists("hand/files/" + output.service.flgFileName.get) must be(true)
        output.exists("mobi/files/" + output.service.flgFileName.get) must be(true)
      }
      "servicetype is 'SpeedFixed'" in {
        val output = writeViolations(new SpeedFixed(), new HHMVS14(), defaultHeaderInfo)

        output.service.flgFileName must be(Some("snelheid.flg"))
        output.exists("auto/files/" + output.service.flgFileName.get) must be(true)
        output.exists("auto/files/" + output.service.flgFileName.get) must be(true)
        output.exists("hand/files/" + output.service.flgFileName.get) must be(true)
        output.exists("mobi/files/" + output.service.flgFileName.get) must be(true)
      }
      "servicetype is 'TargetGroup'" in {
        val output = writeViolations(new TargetGroup(factNumber = "1234"), new HHMVS14(), defaultHeaderInfo)

        output.service.flgFileName must be(Some("doelgroep.flg"))
        output.exists("auto/files/" + output.service.flgFileName.get) must be(true)
        output.exists("auto/files/" + output.service.flgFileName.get) must be(true)
        output.exists("hand/files/" + output.service.flgFileName.get) must be(true)
        output.exists("mobi/files/" + output.service.flgFileName.get) must be(true)
      }
    }
    "create the correct statsaut.txt header" when {
      "casefile format is HHMVS14 or HHMVS40" in {
        val output14 = writeViolations(new SectionControl(), new HHMVS14(), defaultHeaderInfo)
        val output40 = writeViolations(new SectionControl(), new HHMVS40(), defaultHeaderInfo)

        val stats14 = output14.caseFile.createStats(output14.service, 1, 1, 1)
        val stats40 = output40.caseFile.createStats(output40.service, 1, 1, 1)

        output14.exists("hand/files/statsaut.txt") must be(true)
        output40.exists("hand/files/statsaut.txt") must be(true)

        val lines14: Map[Int, String] = output14.getOutput("hand/files/statsaut.txt")
        lines14.size must be(5)
        lines14(0).startsWith(stats14(0)) //Totaal aantal beelden..
        lines14(1).startsWith(stats14(1)) //Nummerbord onleesbaar..
        lines14(2).startsWith(stats14(2)) //Geldige kentekens..
        lines14(3).startsWith(stats14(3)) //Automatisch herk. zaken..
        lines14(4).startsWith(stats14(4)) //Mobiel verwerkte zaken..

        val lines40: Map[Int, String] = output40.getOutput("hand/files/statsaut.txt")
        lines40.size must be(5)
        lines40(0).startsWith(stats40(0)) //Totaal aantal beelden..
        lines40(1).startsWith(stats40(1)) //Nummerbord onleesbaar..
        lines40(2).startsWith(stats40(2)) //Geldige kentekens..
        lines40(3).startsWith(stats40(3)) //Automatisch herk. zaken..
        lines40(4).startsWith(stats40(4)) //Mobiel verwerkte zaken..
      }
      "casefile format is TCVS33" in {
        val output = writeViolations(new SectionControl(), new TCVS33(), defaultHeaderInfo)
        val stats = output.caseFile.createStats(output.service, 1, 1, 1)
        val lines = output.getOutput("hand/files/statsaut.txt")

        lines.size must be(4)
        lines(0).startsWith(stats(0)) //Totaal aantal beelden..
        lines(1).startsWith(stats(1)) //Nummerbord onleesbaar..
        lines(2).startsWith(stats(2)) //Geldige kentekens..
        lines(3).startsWith(stats(3)) //Automatisch herk. zaken..
      }
    }
    "clean up after itself if aborted" in {
      val writer = setupBasicViolationWriter()
      writer.abort()
      baseTestDir.list() must have length 0
    }

    "produce multiple export directories if called multiple times for the same corridor" in {
      val writer1 = setupBasicViolationWriter()
      val writer2 = setupBasicViolationWriter()
      val writer3 = setupBasicViolationWriter()

      //baseTestDir.list().toSet must be(Set("InProgress_a0021001", "InProgress_a0021001.1", "InProgress_a0021001.2"))
      baseTestDir.list().toSet must be(Set("InProgress_a0021001", "InProgress_a0021001.1", "InProgress_a0021001.2"))

      writer2.flush()
      writer3.flush()

      baseTestDir.list().toSet must be(Set("InProgress_a0021001", "a0021001", "a0021001.1"))

      writer1.flush()

      baseTestDir.list().toSet must be(Set("a0021001", "a0021001.1", "a0021001.2"))
    }

    "aggregate multiple exports of different dates" in {
      val writer1 = setupBasicViolationWriter()
      val writer2 = setupBasicViolationWriter(1.day.toMillis)
      val writer3 = setupBasicViolationWriter(2.days.toMillis)

      baseTestDir.list().toSet must be(Set("InProgress_a0021001", "InProgress_a0021001.1", "InProgress_a0021001.2"))

      writer2.flush()
      writer3.flush()

      baseTestDir.list().toSet must be(Set("InProgress_a0021001", "a0021001"))

      writer1.flush()

      baseTestDir.list().toSet must be(Set("a0021001"))

      new File(baseTestDir, "a0021001").list().toSet must be(Set("20000604", "20000605", "20000606"))
    }

    "successfully export over already existing exports" in {
      new File(new File(baseTestDir, "a0021001"), "20000604").mkdirs()
      new File(new File(baseTestDir, "a0021001.1"), "20000604").mkdirs()

      val writer1 = setupBasicViolationWriter()
      writeSomeViolations(writer1)
      val writer2 = setupBasicViolationWriter()
      writeSomeViolations(writer2)

      writer1.flush()
      writer2.flush()

      baseTestDir.list().toSet must be(Set("a0021001", "a0021001.1", "a0021001.2", "a0021001.3"))
    }

    "produce multiple date directories if more than 9999 violations are written" in {
      val writer = setupBasicViolationWriter()
      (1 to 12000).foreach((_) ⇒ writer.add(baseViolation, testPhotoBytes.take(20), Some(testPhotoBytes.take(21))))
      writer.flush() must be('right)

      val exportDir = new File(baseTestDir, "a0021001")
      exportDir.list().toSet must be(Set("20000604", "20000604.1"))

      val packDir1 = new File(exportDir, "20000604")
      val packDir2 = new File(exportDir, "20000604.1")
      val images1 = new File(new File(packDir1, "auto"), "images")
      val images2 = new File(new File(packDir2, "auto"), "images")
      images1.list() must have size (2 * 9999)
      images2.list() must have size (2 * 2001)

      val dataLines1 = io.Source.fromFile(new File(new File(new File(packDir1, "auto"), "files"), "zaakauto.txt"), fileExportCharSet.name()).getLines().toList
      val dataLines2 = io.Source.fromFile(new File(new File(new File(packDir2, "auto"), "files"), "zaakauto.txt"), fileExportCharSet.name()).getLines().toList

      // check line counts. 7 headers lines, x records, 1 hashvalue
      dataLines1 must have size (7 + 9999 + 1)
      dataLines2 must have size (7 + 2001 + 1)

      // check record numbers
      dataLines1(7) must startWith("A 0001 ")
      dataLines1(8) must startWith("A 0002 ")
      dataLines1(10005) must startWith("A 9999 ")
      dataLines2(7) must startWith("A 0001 ")
      dataLines2(8) must startWith("A 0002 ")
      dataLines2(2007) must startWith("A 2001 ")

      // check stats file
      val expectedStatsFile = "" +
        "Totaal aantal beelden    =\t12000\r\n" +
        "Nummerbord onleesbaar    =\t0\r\n" +
        "Geldige kentekens        =\t12000\r\n" +
        "Automatisch herk. zaken  =\t12000\r\n" +
        "Mobiel verwerkte zaken   =\t0"
      FileUtils.readFileToByteArray(new File(new File(new File(packDir1, "hand"), "files"), "statsaut.txt")) must be(expectedStatsFile.getBytes(fileExportCharSet))
      new File(new File(new File(packDir2, "hand"), "files"), "statsaut.txt").exists() must be(false)
    }

    "produce a basic output directory(with MTM file) when called with no violations and EsaProviderType.NoProvider" in {
      val writer = setupBasicViolationWriter(esaProviderType = EsaProviderType.NoProvider)
      baseTestDir.list() must be(Array("InProgress_a0021001"))
      writer.flush() must be('right)
      baseTestDir.list() must be(Array("a0021001"))

      val exportDir = new File(baseTestDir, "a0021001")
      exportDir.list() must be(Array("20000604"))

      val packDir = new File(exportDir, "20000604")
      packDir.list().toSet must be(Set("mtma002l.txt", "calibratietest"))
      val calibrationTestDir = new File(packDir, "calibratietest")
      calibrationTestDir.list().toSet must be(Set("ctres.txt", "ct02215903.jpg", "ct04215903.jpg"))

      FileUtils.readFileToByteArray(new File(packDir, "mtma002l.txt")) must be(mtmInfoText.getBytes(fileExportCharSet))

      val calibrationWriter = new CalibrationWriter(calibrationTestDir)
      val lastfieldhash = calibrationWriter.calculateHash(ctresExpectedTextWithoutHash.getBytes(fileExportCharSet), calibrationTestDir)

      FileUtils.readFileToByteArray(new File(calibrationTestDir, "ctres.txt")) must be(getCtresExpected(calibrationTestDir).getBytes(fileExportCharSet))
      FileUtils.readFileToByteArray(new File(calibrationTestDir, "ct02215903.jpg")) must be(testPhotoBytes)
      FileUtils.readFileToByteArray(new File(calibrationTestDir, "ct04215903.jpg")) must be(testPhotoBytes ++ testPhotoBytes)
    }

    def getCtresExpected(calibrationTestDir: File): String = {
      val calibrationWriter = new CalibrationWriter(calibrationTestDir)
      val lastfieldhash = calibrationWriter.calculateHash(ctresExpectedTextWithoutHash.getBytes(fileExportCharSet), calibrationTestDir)
      ctresExpectedTextWithoutHash + lastfieldhash + "\r\n"
    }

    "produce a basic output directory when called with no violations" in {
      val writer = setupBasicViolationWriter()
      baseTestDir.list() must be(Array("InProgress_a0021001"))
      writer.flush() must be('right)
      baseTestDir.list() must be(Array("a0021001"))
      val exportDir = new File(baseTestDir, "a0021001")
      exportDir.list() must be(Array("20000604"))
      val packDir = new File(exportDir, "20000604")
      packDir.list().toSet must be(Set("mtma002l.txt", "mtmruw.txt", "calibratietest"))
      val calibrationTestDir = new File(packDir, "calibratietest")
      calibrationTestDir.list().toSet must be(Set("ctres.txt", "ct02215903.jpg", "ct04215903.jpg"))

      FileUtils.readFileToByteArray(new File(packDir, "mtmruw.txt")) must be(mtmRawText.getBytes(fileExportCharSet))
      FileUtils.readFileToByteArray(new File(packDir, "mtma002l.txt")) must be(mtmInfoText.getBytes(fileExportCharSet))
      //FileUtils.readFileToByteArray(new File(calibrationTestDir, "ctres.txt")) must be(ctresExpectedText.getBytes(fileExportCharSet))
      FileUtils.readFileToByteArray(new File(calibrationTestDir, "ctres.txt")) must be(getCtresExpected(calibrationTestDir).getBytes(fileExportCharSet))
      FileUtils.readFileToByteArray(new File(calibrationTestDir, "ct02215903.jpg")) must be(testPhotoBytes)
      FileUtils.readFileToByteArray(new File(calibrationTestDir, "ct04215903.jpg")) must be(testPhotoBytes ++ testPhotoBytes)
    }

    "write the correct number of violations to each directory" in {
      val writer = setupBasicViolationWriter()
      writeSomeViolations(writer)
      writer.flush() must be('right)
      //val packDir = new File(new File(baseTestDir, "a0021001"), "20000604")
      val packDir = new File(new File(baseTestDir, "a0021001"), "20000604")
      val automaticFiles = new File(new File(packDir, "auto"), "files")
      val manualFiles = new File(new File(packDir, "hand"), "files")
      val mobiFiles = new File(new File(packDir, "mobi"), "files")
      val automaticLines = io.Source.fromFile(new File(automaticFiles, "zaakauto.txt"), fileExportCharSet.name()).getLines().toList
      val manualLines = io.Source.fromFile(new File(manualFiles, "zaakhand.txt"), fileExportCharSet.name()).getLines().toList
      val mobiLines = io.Source.fromFile(new File(mobiFiles, "zaakmobi.txt"), fileExportCharSet.name()).getLines().toList

      // check .flg files exist
      List(new File(automaticFiles, "traject.flg"),
        new File(manualFiles, "traject.flg"),
        new File(mobiFiles, "traject.flg")).foreach((file) ⇒ {
          file.exists() must be(true)
          file.length must be(0)
        })

      // check line counts. 2 headers lines, x records, 1 hashvalue
      automaticLines must have size (7 + 3 + 1)
      manualLines must have size (7 + 2 + 1)
      mobiLines must have size (7 + 1 + 1)

      // check headers
      List(automaticLines, manualLines, mobiLines).foreach((lines) ⇒ {
        lines(0) must be("# Trajectcontrolesysteem systemName")
        lines(1) must be("# Categorie A meetmiddel")
      })

      // check record numbers
      automaticLines(7) must startWith("A 0001 ")
      automaticLines(8) must startWith("A 0002 ")
      automaticLines(9) must startWith("A 0003 ")
      manualLines(7) must startWith(". 0004 ")
      manualLines(8) must startWith(". 0005 ")
      mobiLines(7) must startWith("V 0006 ")

      // check stats file
      val expectedStatsFile = "" +
        "Totaal aantal beelden    =\t6\r\n" +
        "Nummerbord onleesbaar    =\t2\r\n" +
        "Geldige kentekens        =\t3\r\n" +
        "Automatisch herk. zaken  =\t3\r\n" +
        "Mobiel verwerkte zaken   =\t1"
      FileUtils.readFileToByteArray(new File(manualFiles, "statsaut.txt")) must be(expectedStatsFile.getBytes(fileExportCharSet))
    }

    "write the correct violation images to each directory" in {
      val writer = setupBasicViolationWriter()
      writeSomeViolations(writer)
      writer.flush() must be('right)

      val packDir = new File(new File(baseTestDir, "a0021001"), "20000604")
      val automaticImages = new File(new File(packDir, "auto"), "images")
      val manualImages = new File(new File(packDir, "hand"), "images")
      val mobiImages = new File(new File(packDir, "mobi"), "images")

      // check image counts
      automaticImages.list() must have size 6
      manualImages.list() must have size 4
      mobiImages.list() must have size 2

      new File(automaticImages, "0001ful1.jpg").length must be(20)
      new File(automaticImages, "0001ful2.jpg").length must be(21)
      new File(automaticImages, "0002ful1.jpg").length must be(22)
      new File(automaticImages, "0002ful2.jpg").length must be(23)
      new File(automaticImages, "0003ful1.jpg").length must be(24)
      new File(automaticImages, "0003ful2.jpg").length must be(25)
      new File(manualImages, "0004ful1.jpg").length must be(26)
      new File(manualImages, "0004ful2.jpg").length must be(27)
      new File(manualImages, "0005ful1.jpg").length must be(28)
      new File(manualImages, "0005ful2.jpg").length must be(29)
      new File(mobiImages, "0006ful1.jpg").length must be(30)
      new File(mobiImages, "0006ful2.jpg").length must be(31)
    }

    "when a violation for a different date is added, produce an exception" in {
      val writer = setupBasicViolationWriter()
      evaluating {
        writer.add(baseViolation.copy(corridorEntryTime = baseViolation.corridorEntryTime + 24 * 60 * 60 * 1000,
          corridorExitTime = baseViolation.corridorExitTime + 24 * 60 * 60 * 1000),
          testPhotoBytes,
          Some(testPhotoBytes))
      } must produce[IllegalArgumentException]
    }

    "produce an exception if created with insufficient disk space" in {
      evaluating {
        ViolationWriter(baseTestDir.getAbsolutePath,
          globalParameters,
          mtmRawText.getBytes(fileExportCharSet),
          mtmInfoText.getBytes(fileExportCharSet),
          Long.MaxValue,
          caseFile = new HHMVS14(),
          service = new SectionControl(),
          esaProviderType = EsaProviderType.Mtm, optSpeedLimit = Some(100), caseFileConfigService = null)

      } must produce[IllegalArgumentException]
      baseTestDir.list() must have length 0
    }

    "produce an exception if invalid image data is supplied with a violation" in {
      val writer = setupBasicViolationWriter()
      evaluating { writer.add(baseViolation, testPhotoBytes.reverse, Some(testPhotoBytes)) } must produce[IllegalArgumentException]
      evaluating { writer.add(baseViolation, testPhotoBytes, Some(testPhotoBytes.take(5))) } must produce[IllegalArgumentException]
    }

    "produce an exception if adding a record after flush" in {
      val writer = setupBasicViolationWriter()
      writer.flush()
      evaluating { writer.add(baseViolation, testPhotoBytes, Some(testPhotoBytes)) } must produce[IllegalArgumentException]
      evaluating {
        writer.add(CalibrationTestResult(testedOk = true,
          time = jun_04_2000_21_59 + 103.seconds.toMillis,
          reportingOfficerCode = "VC1234",
          cameraCount = 2,
          softwareSeals = List(ComponentCertificate("A", "1")),
          serialNumbers = List("AB123"),
          parameters = SystemParameters("1", 2, 3, 4, 5, "6", "7", 8, 9, 10),
          serviceType = ServiceType.SectionControl,
          testPhotos = List(calibrationPhoto,
            calibrationPhoto.copy(cameraId = 4,
              image = testPhotoBytes ++ testPhotoBytes))))
      } must produce[IllegalArgumentException]
    }

    "produce an exception if adding a record after abort" in {
      val writer = setupBasicViolationWriter()
      writer.abort()
      evaluating { writer.add(baseViolation, testPhotoBytes, Some(testPhotoBytes)) } must produce[IllegalArgumentException]
      evaluating {
        writer.add(CalibrationTestResult(testedOk = true,
          time = jun_04_2000_21_59 + 103.seconds.toMillis,
          reportingOfficerCode = "VC1234",
          cameraCount = 2,
          softwareSeals = List(ComponentCertificate("A", "1")),
          serialNumbers = List("AB123"),
          parameters = SystemParameters("1", 2, 3, 4, 5, "6", "7", 8, 9, 10),
          serviceType = ServiceType.SectionControl,
          testPhotos = List(calibrationPhoto,
            calibrationPhoto.copy(cameraId = 4,
              image = testPhotoBytes ++ testPhotoBytes))))
      } must produce[IllegalArgumentException]
    }

    "produce an exception if the base directory is not writable" in {
      baseTestDir.setWritable(false)
      evaluating { setupBasicViolationWriter() } must produce[IllegalArgumentException]
      baseTestDir.setWritable(true)
    }
  }

  "ViolationWriter.getBaseName" must {
    "get the correct basename for the directories" in {
      val basicViolationWriter = setupBasicViolationWriterImpl()
      // setupBasicViolationWriterImpl is initiated with speed 100
      basicViolationWriter.getBaseName must be("a0021001")
    }

    "get the correct basename for the directories when no casefiles -> config can be found" in {
      val caseFileConfigService = new CaseFileConfigServiceMock()
      caseFileConfigService.getResult = None
      println(caseFileConfigService.getResult)
      val basicViolationWriter = setupBasicViolationWriterImpl(caseFileConfigService = caseFileConfigService)
      // setupBasicViolationWriterImpl is initiated with speed 100
      basicViolationWriter.getBaseName must be("eg331NoConfig100")
    }

    "get the correct basename for the directories when no correct speeds can be found in the casefiles->config" in {
      val caseFileConfigService = new CaseFileConfigServiceMock()
      caseFileConfigService.getResult = Some(ZkCaseFileConfig("header", List(ExportDirectoryForSpeedLimit(180, "a0021301"), ExportDirectoryForSpeedLimit(110, "a0021101"))))
      //caseFileConfigService.getResult = List(ZkCaseFileConfig(180, "a0021301"), ZkCaseFileConfig(110, "a0021101"))
      println(caseFileConfigService.getResult)
      val basicViolationWriter = setupBasicViolationWriterImpl(caseFileConfigService = caseFileConfigService)
      // setupBasicViolationWriterImpl is initiated with speed 100
      basicViolationWriter.getBaseName must be("eg331NoConfig100")
    }
  }

  private def setupBasicViolationWriterImpl(timeOffset: Long = 0L, caseFile: CaseFile = new HHMVS14(),
                                            service: Service = new SectionControl(),
                                            esaProviderType: EsaProviderType.Value = EsaProviderType.Mtm,
                                            headerInfo: HeaderInfo = HeaderInfo("systemName", "serialNumber", createMeasurmentType()),
                                            caseFileConfigService: CaseFileConfigService = new CaseFileConfigServiceMock()) = {
    val writer = ViolationWriterImpl(baseTestDir.getAbsolutePath,
      globalParameters.copy(headerInfo = headerInfo, violationsDate = globalParameters.violationsDate + timeOffset),
      mtmRawText.getBytes(fileExportCharSet),
      mtmInfoText.getBytes(fileExportCharSet),
      caseFile = caseFile,
      service = service,
      esaProviderType = esaProviderType, optSpeedLimit = Some(100), caseFileConfigService = caseFileConfigService)

    writer.add(CalibrationTestResult(testedOk = true,
      time = jun_04_2000_21_59 + 3.seconds.toMillis + timeOffset,
      reportingOfficerCode = "VC1234",
      cameraCount = 2,
      softwareSeals = List(ComponentCertificate("A", "1")),
      serialNumbers = List("AB123"),
      parameters = SystemParameters("1", 2, 3, 4, 5, "6", "7", 8, 9, 10),
      serviceType = ServiceType.SectionControl,
      testPhotos = List(calibrationPhoto,
        calibrationPhoto.copy(cameraId = 4,
          image = testPhotoBytes ++ testPhotoBytes))))
    writer
  }

  override protected def afterAll() {
    FileUtils.forceDelete(baseTestDir)
  }
  def createMeasurmentType() =
    new MeasurementMethodType(typeDesignation = "XX123",
      category = "A",
      unitSpeed = "km/h",
      unitRedLight = "s",
      unitLength = "m",
      restrictiveConditions = "",
      displayRange = "20-250 km/h",
      temperatureRange = "temperatuurbereik -10 to 50 graden Celsius",
      permissibleError = "toelatbare fout 3%",
      typeCertificate = new TypeCertificate("eg33-cert", 3,
        List(ComponentCertificate("matcher1", "blah1"),
          ComponentCertificate("vr2", "blah2"))))
}

class CaseFileConfigServiceMock extends CaseFileConfigService {
  var getResult: Option[ZkCaseFileConfig] = Some(ZkCaseFileConfig("header", List(ExportDirectoryForSpeedLimit(80, "a0020801"), ExportDirectoryForSpeedLimit(100, "a0021001"))))
  override def exists(systemId: String, corridorId: String): Boolean = false

  override def get(systemId: String, corridorInfoId: Int): Option[ZkCaseFileConfig] = {
    getResult
  }

  override def get(systemId: String, corridorId: String): Option[ZkCaseFileConfig] = None

  override def create(systemId: String, corridorId: String, caseFileConfig: ZkCaseFileConfig): Unit = ()
}
