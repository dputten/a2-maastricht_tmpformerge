package csc.sectioncontrol.writer.nl

import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }
import org.scalatest.matchers.MustMatchers
import java.io.File

/**
 * Created by jeijlders on 5/20/14.
 */
class CalibrationWriterTest extends WordSpec
  with MustMatchers
  with BeforeAndAfterEach
  with BeforeAndAfterAll {
  "getFormattedTime" must {
    "return the correct formatted time" in {
      val calibrationWriter = new CalibrationWriter(new File(""))
      calibrationWriter.getFormattedTime(960148784000L, 960156000000L) must be("215944")
      calibrationWriter.getFormattedTime(960156000000L, 960156000000L) must be("235959")
      calibrationWriter.getFormattedTime(960156000000L, 960242400000L) must be("000000")
    }
  }
}
