/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

package csc.sectioncontrol.writer.nl

import java.text.SimpleDateFormat
import org.apache.commons.io.FileUtils
import java.io.{ FilenameFilter, File }
import csc.sectioncontrol.enforce.violations.Implicits._
import csc.sectioncontrol.enforce.common.nl.messages.Constants._
import collection.mutable.ListBuffer
import csc.sectioncontrol.enforce.common.nl.violations._
import csc.sectioncontrol.enforce.violations.{ GenerateId, DayUtility }

class CalibrationWriter(exportDirectory: File) extends GenerateId {
  private val fileName = "ctres.txt"
  private val calibrationImageCacheName = "CalibrationImageCache"
  private val calibrationImageCache = new File(exportDirectory, calibrationImageCacheName)
  private val calibrationTestResults = ListBuffer[CalibrationTestResult]()

  def writeFile(calibrationTestDir: File, parameters: ViolationGroupParameters): Unit = {
    FileUtils.writeByteArrayToFile(new File(calibrationTestDir, fileName),
      getCalibrationTestData(calibrationTestDir, parameters))
  }

  def addTestResult(calibrationTestResult: CalibrationTestResult): Unit = {
    calibrationTestResults.append(cacheCalibrationPhotos(calibrationTestResult))
  }

  def clearTestResults(): Unit = {
    calibrationTestResults.clear()
  }

  def removeImageCache(): Unit = {
    FileUtils.deleteDirectory(calibrationImageCache)
  }

  private def cacheImage(image: Array[Byte], directory: File): String = {
    val imageId = nextId + ".jpg"
    val imageFile = new File(directory, imageId)
    FileUtils.writeByteArrayToFile(imageFile, image)
    imageId
  }

  private def cacheCalibrationPhotos(calibrationTestResult: CalibrationTestResult): CalibrationTestResult = {
    val cachedPhotos = calibrationTestResult.testPhotos.map {
      case photo: CachedCalibrationImage ⇒ photo
      case photo: CalibrationTestPhoto ⇒
        CachedCalibrationImage(cameraId = photo.cameraId,
          time = photo.time,
          imageFileName = cacheImage(photo.image, calibrationImageCache))
    }
    calibrationTestResult.copy(testPhotos = cachedPhotos)
  }

  def getFormattedTime(time: Long, dayEndTime: Long): String = {
    val timeFormat = new SimpleDateFormat("HHmmss")
    val dayEndTimeFormat = new SimpleDateFormat("kkmmss")

    if (time < dayEndTime) {
      timeFormat(time)
    } else {
      dayEndTimeFormat(time - 1)
    }
  }

  private def getCalibrationTestData(calibrationTestDir: File, parameters: ViolationGroupParameters): Array[Byte] = {
    val (_, dayEndTime) = DayUtility.calculateDayPeriod(parameters.violationsDate)

    def getCtImageName(image: CalibrationImage): String = {
      "ct" + image.cameraId.toString.padLeft(2, '0') + getFormattedTime(image.time, dayEndTime) + ".jpg"
    }

    val fileData = calibrationTestResults.sortBy(_.time).map((testResult) ⇒ {
      testResult.testPhotos.map {
        case image: CachedCalibrationImage ⇒
          FileUtils.moveFile(new File(calibrationImageCache, image.imageFileName),
            new File(calibrationTestDir, getCtImageName(image)))
        case image: CalibrationTestPhoto ⇒
          FileUtils.writeByteArrayToFile(new File(calibrationTestDir, getCtImageName(image)),
            image.image)
      }
      testResult.toDataLine(parameters, dayEndTime - 1000) // Nb: Use CaseFile to determin dayEndTime
    }).mkString(start = "", sep = "\r\n", end = "\r\n").getBytes(fileExportCharSet)

    val hash = (calculateHash(fileData, calibrationTestDir) + "\r\n").getBytes(fileExportCharSet)
    fileData ++ hash
  }

  /**
   * the files of a calibration test must be hashed in the right order. the file names look like
   * ct00041631.jpg 00 is index, 041631 is HHMMss
   * ct00205123.jpg 00 is index, 205123 is HHMMss
   * ct00211725.jpg 00 is index, 211725 is HHMMss
   * ct01041631.jpg 01 is index, 041631 is HHMMss
   * ct03211725.jpg 03 is index, 211725 is HHMMss
   * the hashing start with all files that have the same time. Those files must than me sorted by their index (lowest first)
   * This means the above file names wil be hashed in the following order
   * ct00041631.jpg 00 is index, 041631 is HHMMss
   * ct01041631.jpg 01 is index, 041631 is HHMMss
   * ct00205123.jpg 00 is index, 205123 is HHMMss
   * ct00211725.jpg 00 is index, 211725 is HHMMss
   * ct03211725.jpg 03 is index, 211725 is HHMMss
   * @param recordData
   * @param imageDir
   * @return
   */
  def calculateHash(recordData: Array[Byte], imageDir: File): String = {
    val digest = java.security.MessageDigest.getInstance("MD5")
    digest.update(recordData)
    if (imageDir.exists() && imageDir.isDirectory) {
      val fileNames = imageDir.list(new FilenameFilter() {
        def accept(dir: File, name: String) = name.endsWith(".jpg")
      })
      //Map(41631 -> List("ct00041631.jpg", "ct01041631.jpg"), 205123 -> List("ct00205123.jpg", "ct01205123.jpg"))
      val mappedByTime = fileNames.groupBy[Int](_.substring(4, 10).toInt)
      val sortedAndMappedByTime = mappedByTime.toSeq.sortBy(_._1) //sorted by time (lowest first)
      sortedAndMappedByTime.map {
        case (time, fileNames) ⇒
          //Map(0 -> List("CT00041631"), 1 -> List("CT01041631"))
          val mappedByIndex = fileNames.groupBy[Int](_.substring(2, 4).toInt)
          val sortedAndMappedByIndex = mappedByIndex.toSeq.sortBy(_._1) //lowest first
          sortedAndMappedByIndex.map {
            case (index, fileNames) ⇒
              val fileName = fileNames(0) //there is only one name in list of fileNames
              val fileToHash = new File(imageDir, fileName)
              digest.update(FileUtils.readFileToByteArray(fileToHash))
          }
      }
    }
    new String(org.apache.commons.codec.binary.Hex.encodeHex(digest.digest()))
  }
}

