/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.writer.nl

import annotation.tailrec
import csc.sectioncontrol.enforce.nl.casefiles.CaseFile
import csc.sectioncontrol.enforce.violations.Checks._
import csc.sectioncontrol.messages.{ ProcessingIndicator, EsaProviderType }
import java.io.{ FilenameFilter, PrintWriter, StringWriter, File }
import java.text.SimpleDateFormat
import org.apache.commons.io.output.LockableFileWriter
import org.apache.commons.io.{ FileSystemUtils, FileUtils }
import csc.sectioncontrol.enforce.common.nl.violations.{ ViolationGroupParameters, CalibrationTestResult }
import csc.sectioncontrol.enforce.common.nl.messages.ViolationData
import csc.sectioncontrol.enforce.common.nl.messages.Constants._
import csc.sectioncontrol.enforce.common.nl.services.Service
import collection.mutable.ListBuffer
import csc.sectioncontrol.enforce.violations.Implicits._
import csc.sectioncontrol.enforce.violations.Violations._
import csc.sectioncontrol.enforce.violations.GenerateId
import csc.sectioncontrol.storagelayer.caseFile.CaseFileConfigService
import csc.akkautils.DirectLogging

/**
 * Main interface to write out the violation data for pickup by LPTV.
 * Create an instance per export. Multiple instances using the same baseDir are intended for single-threaded
 * use, and should not be accessed by multiple threads simultaneously. Since the throughput is limited by
 * disk access, it is unlikely that multi-threaded use would provide much of a speedup anyway.
 *
 * To use, create an instance, add all the required data (including MTM change data, Calibration Test results
 * as well as the actual Violations).
 * Then call flush() to write out the final data to disk.
 *
 * @author Maarten Hazewinkel
 */
trait ViolationWriter {
  /**
   * Add a Calibration test result record.
   *
   * @return Right() if ok, Left(errorString) if an error occurred
   */
  def add(calibrationTestResult: CalibrationTestResult): Either[String, Unit]

  /**
   * Add a Violation record.
   *
   * @return Right() if ok, Left(errorString) if an error occurred
   */
  def add(violation: ViolationData, image1: Array[Byte], image2: Option[Array[Byte]]): Either[String, Unit]

  /**
   * Write out all the data and complete the export directory.
   *
   * @return Right(exportDirectory) if ok, Left(errorString) if an error occurred.
   */
  def flush(): Either[String, File]

  /**
   * Abort this export and clean up all temporary directories.
   */
  def abort()
}

/**
 * Case class to hold a Violation with references to the cached images belonging to that Violation.
 *
 * @author Maarten Hazewinkel
 */
private case class CachedViolation(violation: ViolationData,
                                   image1FileName: String,
                                   image2FileName: Option[String]) {
  require(allNotNull(violation, image1FileName), nullValuesErrorMessage)
}

/**
 * Main implementation of the ViolationWriter interface.
 *
 * While the export is in progress, the data is stored in an 'InProgress_????' directory in the base directory.
 * This is to prevent an external observer from starting pickup on an incomplete export.
 * Once the flush() is complete, the in-progress directory will be renamed to the correct name for pickup by
 * LPTV.
 *
 * @param baseDir The base directory into which the export directory will be written.
 * @param parameters Global parameters that pertain to the export as a whole.
 * @param mtmRaw The contents of the raw MTM datafile as received by the sectioncontrol system.
 * @param mtmInfo The processed speed information as used by the sectioncontrol system.
 * @param requireFreeSpaceKb The amount of space that should be free on the export device. Defaults to 1000 kb.
 *
 * @author Maarten Hazewinkel
 */
class ViolationWriterImpl(val baseDir: String,
                          val parameters: ViolationGroupParameters,
                          val mtmRaw: Array[Byte], // expected to be less than a megabyte. Can be kept in memory.
                          val mtmInfo: Array[Byte], // expected to be less than a megabyte. Can be kept in memory.
                          val requireFreeSpaceKb: Long,
                          val caseFile: CaseFile,
                          val service: Service,
                          val esaProvider: EsaProviderType.Value,
                          val optSpeedLimit: Option[Int],
                          val caseFileConfigService: CaseFileConfigService) extends ViolationWriter with GenerateId with DirectLogging {

  require(allNotNull(baseDir, parameters, mtmRaw, mtmInfo), nullValuesErrorMessage)

  private val inProgressPrefix = "InProgress_"
  private val violationImageCacheName = "ViolationImageCache"
  private object Status extends Enumeration {
    val InProgress, Completed, Aborted = Value
  }
  private var status: Status.Value = Status.InProgress
  private val baseDirectory = new File(baseDir)

  checkBaseDirectory()

  private val exportDirectory = createInProgressExportDirectory()
  private val violationImageCache = new File(exportDirectory, violationImageCacheName)
  private val violations = ListBuffer[CachedViolation]()
  private val calibrationWriter = new CalibrationWriter(exportDirectory)

  /**
   * Add a Calibration test result record.
   *
   * @return Right() if ok, Left(errorString) if an error occurred
   */
  def add(calibrationTestResult: CalibrationTestResult): Either[String, Unit] = {
    require(calibrationTestResult != null, nullValuesErrorMessage)
    require(status == Status.InProgress, "cannot add new data to a writer that is no longer open")
    try {
      calibrationWriter.addTestResult(calibrationTestResult)
      Right()
    } catch {
      case e: Exception ⇒ abortWithException(e)
    }
  }

  /**
   * Add a Violation record.
   *
   * @return Right() if ok, Left(errorString) if an error occurred
   */
  def add(violation: ViolationData, image1: Array[Byte], image2: Option[Array[Byte]]): Either[String, Unit] = {
    require(violation != null, "can not add a violation when ViolationData is null")
    require(image1 != null, "first image must never be null")

    if (service.requireSecondImage) {
      require(image2.isDefined, service + " requires a second image")
      require(isJFIF(image2.get), "second image must be a jpeg image")
    }

    require(isJFIF(image1), "image1 must be a jpeg image")
    require(isSameDate(violation.time, parameters.violationsDate), "violations must be for the date set in the violation group parameters")
    require(status == Status.InProgress, "cannot add new data to a writer that is no longer open")

    try {
      val imageFileName1: String = cacheImage(image1, violationImageCache)
      val imageFileName2: Option[String] = image2.map(i ⇒ Some(cacheImage(i, violationImageCache))).getOrElse(None)
      violations.append(CachedViolation(violation, imageFileName1, imageFileName2))
      Right()
    } catch {
      case e: Exception ⇒ abortWithException(e)
    }
  }

  /**
   * Write out all the data and complete the export directory.
   *
   * @return Right(exportDirectory) if ok, Left(errorString) if an error occurred.
   */
  def flush(): Either[String, File] = {
    require(status == Status.InProgress, "cannot flush a writer that is no longer open")
    import ProcessingIndicator._

    try {
      // Sort violations by date, group into sets of 9999, and add an index to each group
      violations.sortBy(_.violation.time).grouped(9999).zipWithIndex.foreach {
        case (violationsBlock, blockIndex) ⇒
          val blockDirectory = new File(exportDirectory, getBlockDirectoryName(blockIndex))

          // number all records
          val numberedViolations = violationsBlock.sortBy(cv ⇒ (cv.violation.processingIndicator, cv.violation.time)).zip(1 to 9999)

          // Within each block group by ProcessingIndicator
          val violationTypeBlocks = numberedViolations.groupBy(_._1.violation.processingIndicator)
          ProcessingIndicator.values.foreach { violationType ⇒
            {
              val noViolationsForType = violationTypeBlocks.get(violationType).isEmpty

              // Create directory structure for ProcessingType
              val (dataFile, imageDir, typeDir) = makeDataDirectory(blockDirectory, violationType, service)
              // process records if present
              violationTypeBlocks.get(violationType).foreach { violationTypeBlock ⇒
                writeViolationTypeBlock(violationTypeBlock, dataFile, imageDir)
              }

              //create the data file (zaakauto.txt, zaakhand.txt etc)
              //even if there are no violations
              if (noViolationsForType) {
                FileUtils.touch(dataFile)
              }

              // Delete mobi directory if no entries are present
              if (violationType == Mobi && noViolationsForType) {
                FileUtils.deleteDirectory(typeDir)
              }
            }
          }
      }

      writeSupportingFiles()
      FileUtils.deleteDirectory(violationImageCache)
      calibrationWriter.removeImageCache()

      val finalDirectory = new File(baseDirectory, getFinalDirectoryName)
      if (moveToFinalDirectory(exportDirectory, finalDirectory)) {
        status = Status.Completed
        Right(finalDirectory)
      } else {
        throw new Exception("Moving contents of export directory '" + exportDirectory + "' to '" + finalDirectory + "' failed")
      }
    } catch {
      case e: Exception ⇒
        abortWithException(e)
    }
  }

  /**
   * Abort this export and clean up all temporary directories.
   */
  def abort() {
    if (status == Status.InProgress) {
      calibrationWriter.clearTestResults()
      violations.clear()
      FileUtils.deleteDirectory(exportDirectory)
      status = Status.Aborted
    }
  }

  /**
   * Overridden to call abort() if this writer is still open.
   */
  override def finalize() { if (status == Status.InProgress) abort() }

  private def abortWithException[T](e: Exception): Left[String, T] = {
    val catcher: StringWriter = new StringWriter()
    val writer = new PrintWriter(catcher)
    try {
      e.printStackTrace(writer)
      try {
        abort()
      } catch {
        case abortException: Exception ⇒ abortException.printStackTrace(writer)
      }
    } finally {
      writer.close()
    }
    Left(catcher.toString)
  }

  private def checkBaseDirectory() {
    require(baseDirectory.exists(), baseDir + " does not exist")
    require(baseDirectory.isDirectory, baseDir + " is not a directory")
    require(baseDirectory.canWrite, baseDir + " is not writable")
    require(FileSystemUtils.freeSpaceKb(baseDir) >= requireFreeSpaceKb, baseDir + " does not have enough free space")
  }

  def getBaseName: String = {
    val caseFileConfig = caseFileConfigService.get(parameters.systemId, parameters.corridorInfo.corridorId)
    log.debug("zkCaseFileConfig:" + caseFileConfig)
    log.debug("parameters:" + parameters)

    caseFileConfig match {
      case Some(_caseFileConfig) ⇒
        val exportDirectoryForSpeedLimits = _caseFileConfig.exportDirectoryForSpeedLimits.find { config ⇒ Some(config.speedlimit) == optSpeedLimit }
        exportDirectoryForSpeedLimits match {
          case Some(_exportDirectoryForSpeedLimits) ⇒ _exportDirectoryForSpeedLimits.exportDirectory
          case _                                    ⇒ getNoConfigName
        }
      case _ ⇒ getNoConfigName
    }
  }

  def getNoConfigName: String = {
    val noConfig = "NoConfig"
    val speedString = if (optSpeedLimit == None) "NoSpeedSign" else optSpeedLimit.get.toString
    parameters.routeId.toLowerCase + parameters.corridorInfo.corridorId.toString + noConfig + speedString
  }

  private def getFinalDirectoryName: String = {
    @tailrec
    def makeFinalDirectoryName(extension: Int): String = {
      val baseName = getBaseName
      log.debug("makeFinalDirectoryName.baseName:" + baseName)

      val fullName = if (extension == 0) baseName else baseName + '.' + extension
      if (!new File(new File(baseDirectory, fullName), getBlockDirectoryName(0)).exists()) {
        fullName
      } else {
        makeFinalDirectoryName(extension + 1)
      }
    }

    val lockFile = new File(baseDirectory, "ViolationWriter_DirectoryLock")
    repeatAttemptWithDelays() { new LockableFileWriter(lockFile) } match {
      case Left(exception) ⇒ throw new IllegalStateException("Could not obtain lock on directory " + baseDir, exception)
      case Right(lockFileWriter) ⇒
        try {
          makeFinalDirectoryName(0)
        } finally {
          lockFileWriter.close()
          lockFile.delete()
        }
    }
  }

  @tailrec
  private def getInProgressDirectoryName(extension: Int = 0): String = {
    val baseName = inProgressPrefix + getBaseName
    log.debug("getInProgressDirectoryName.baseName:" + baseName)

    val fullName = if (extension == 0) baseName else baseName + '.' + extension
    if (!new File(baseDirectory, fullName).exists()) {
      fullName
    } else {
      getInProgressDirectoryName(extension + 1)
    }
  }

  private def getBlockDirectoryName(index: Int): String = {
    val formatter = new SimpleDateFormat("yyyyMMdd")
    val baseName = formatter(parameters.violationsDate)
    if (index == 0) baseName else baseName + "." + index
  }

  private def createInProgressExportDirectory(): File = {
    val lockFile = new File(baseDirectory, "ViolationWriter_DirectoryLock")
    repeatAttemptWithDelays() { new LockableFileWriter(lockFile) } match {
      case Left(exception) ⇒ throw new IllegalStateException("Could not obtain lock on directory " + baseDir, exception)
      case Right(lockFileWriter) ⇒
        try {
          val inProgressDirectoryName = getInProgressDirectoryName()
          val inProgressDirectory = new File(baseDirectory, inProgressDirectoryName)
          val dateDirectory = new File(inProgressDirectory, getBlockDirectoryName(0))
          if (!dateDirectory.mkdirs()) throw new IllegalStateException("Could not create export directory " + inProgressDirectoryName)
          inProgressDirectory
        } finally {
          lockFileWriter.close()
          lockFile.delete()
        }
    }
  }

  private def moveToFinalDirectory(exportDir: File, finalDir: File): Boolean = {
    if (!finalDir.exists()) {
      finalDir.mkdirs()
    }
    val files = exportDir.listFiles()
    val moveResults = files.map { src ⇒
      val dest = new File(finalDir, src.getName)
      if (dest.exists()) {
        false
      } else {
        src.renameTo(dest)
      }
    }
    if (moveResults.contains(false)) {
      false
    } else {
      if (exportDir.list().size == 0) {
        exportDir.delete()
      } else {
        true
      }
    }
  }

  private def cacheImage(image: Array[Byte], directory: File): String = {
    val imageId = nextId + ".jpg"
    val imageFile = new File(directory, imageId)
    FileUtils.writeByteArrayToFile(imageFile, image)
    imageId
  }

  /**
   * Creates the default folders like auto/file , auto/images etc
   * @param blockDir base folder path where to create the folder in
   * @param violationType the type of violation Automatic, Manual etc
   * @return
   */
  private def makeDataDirectory(blockDir: File, violationType: ProcessingIndicator.Value, service: Service): (File, File, File) = {
    import ProcessingIndicator._

    val name = violationType match {
      case Automatic ⇒ "auto"
      case Manual    ⇒ "hand"
      case Mobi      ⇒ "mobi"
    }

    val caseFile = violationType match {
      case Automatic ⇒ "zaakauto.txt"
      case Manual    ⇒ "zaakhand.txt"
      case Mobi      ⇒ "zaakmobi.txt"
    }

    val typeDir: File = new File(blockDir, name)
    val dataDir = new File(typeDir, "files")
    val imageDir = new File(typeDir, "images")
    val dataFile = new File(dataDir, caseFile)

    FileUtils.forceMkdir(dataDir)
    FileUtils.forceMkdir(imageDir)
    service.flgFileName.map(flg ⇒ FileUtils.touch(new File(dataDir, flg)))

    (dataFile, imageDir, typeDir)
  }

  private def writeViolationTypeBlock(violationTypeBlock: Seq[(CachedViolation, Int)],
                                      dataFile: File,
                                      imageDir: File) {
    // add record numbers to (re)sorted violations
    val violationRecordBlock = violationTypeBlock.sortBy(_._1.violation.time)
    // map each violation to a line of data and move image files into place
    val recordData = violationRecordBlock.map((violationRecord) ⇒ {
      val recordNum: String = violationRecord._2.toString.padLeft(4, '0')
      FileUtils.moveFile(new File(violationImageCache, violationRecord._1.image1FileName),
        new File(imageDir, recordNum + "ful1.jpg"))

      //only write second image to file if available
      val secondImageFileName: Option[String] = violationRecord._1.image2FileName
      secondImageFileName.map {
        imageFileName ⇒
          FileUtils.moveFile(new File(violationImageCache, imageFileName), new File(imageDir, recordNum + "ful2.jpg"))
      }

      violationRecord._1.violation.toDataLine(recordNum, parameters, caseFile, service)

    }).toList

    if (recordData.size == 0) {
      FileUtils.touch(dataFile)
    } else {
      // calculate hash and write data

      val fileData = (caseFile.createHeader(service, parameters.headerInfo) ++ recordData).mkString("\r\n").getBytes(fileExportCharSet)
      val hash = ("\r\n" + calculateMD5Hash(fileData, imageDir) + "\r\n").getBytes(fileExportCharSet)
      FileUtils.writeByteArrayToFile(dataFile, fileData ++ hash)
    }
  }

  /**
   * write the following support files;
   * - dynamax.txt
   * - mtmruw.txt
   * - mtmn999n.txt (n999n part will be replaced by /ctes/systems/../config/mtmRouteId value in zookeeper
   */
  def writeSupportingFiles() {
    import EsaProviderType._

    val blockDirectoryName = getBlockDirectoryName(0)
    esaProvider match {
      case Dynamax ⇒
        FileUtils.writeByteArrayToFile(new File(new File(exportDirectory, blockDirectoryName), "dynamax.txt"), mtmInfo)
      case (Mtm | TubesProvider) ⇒
        FileUtils.writeByteArrayToFile(new File(new File(exportDirectory, blockDirectoryName), "mtmruw.txt"), mtmRaw)
        writeMtmInfoToFile(blockDirectoryName)
      case NoProvider ⇒
        writeMtmInfoToFile(blockDirectoryName)
    }

    if (violations.size > 0)
      FileUtils.writeByteArrayToFile(new File(new File(new File(new File(exportDirectory, blockDirectoryName), "hand"), "files"), "statsaut.txt"),
        getStatsData)
    val calibrationTestDir: File = new File(new File(exportDirectory, blockDirectoryName), "calibratietest")

    calibrationWriter.writeFile(calibrationTestDir, parameters)
  }

  def writeMtmInfoToFile(blockDirectoryName: String) {
    FileUtils.writeByteArrayToFile(new File(new File(exportDirectory, blockDirectoryName), "mtm" + parameters.mtmRouteId + ".txt"), mtmInfo)
  }

  private def calculateMD5Hash(recordData: Array[Byte], imageDir: File): String = {
    val digest = java.security.MessageDigest.getInstance("MD5")
    digest.update(recordData)
    if (imageDir.exists() && imageDir.isDirectory) {
      imageDir.list(new FilenameFilter() {
        def accept(dir: File, name: String) = name.endsWith(".jpg")
      }).sorted.foreach { name ⇒
        digest.update(FileUtils.readFileToByteArray(new File(imageDir, name)))
      }
    }
    new String(org.apache.commons.codec.binary.Hex.encodeHex(digest.digest()))
  }

  private def getStatsData: Array[Byte] = {
    import ProcessingIndicator._

    val groupedViolations = violations.groupBy(_.violation.processingIndicator)
    val totalAutomatic = groupedViolations.getOrElse(Automatic, Nil).size
    val totalManual = groupedViolations.getOrElse(Manual, Nil).size
    val totalMobi = groupedViolations.getOrElse(Mobi, Nil).size

    caseFile
      .createStats(service, totalAutomatic, totalManual, totalMobi)
      .mkString("\r\n").getBytes(fileExportCharSet)
  }
}

object ViolationWriter {
  /**
   * Create a new ViolationWriter instance.
   *
   * @param baseDir The base directory into which the export directory will be written.
   * @param parameters Global parameters that pertain to the export as a whole.
   * @param mtmRaw The contents of the raw MTM datafile as received by the sectioncontrol system.
   * @param mtmInfo The processed speed information as used by the sectioncontrol system.
   * @param requireFreeSpaceKb The amount of space that should be free on the export device. Defaults to 1000 kb.
   * @param caseFile The file format (HHM1.4 , 4.0 etc
   * @param service The type of service (SectionControl, RedLight etc)
   * @param esaProviderType The type of esa provider (Mtm, Dynamax etc)
   */
  def apply(baseDir: String,
            parameters: ViolationGroupParameters,
            mtmRaw: Array[Byte],
            mtmInfo: Array[Byte],
            requireFreeSpaceKb: Long = 1000,
            caseFile: CaseFile,
            service: Service,
            esaProviderType: EsaProviderType.Value,
            optSpeedLimit: Option[Int],
            caseFileConfigService: CaseFileConfigService): ViolationWriter = {

    new ViolationWriterImpl(baseDir, parameters, mtmRaw, mtmInfo, requireFreeSpaceKb, caseFile, service, esaProviderType, optSpeedLimit, caseFileConfigService)
  }
}

object ViolationWriterImpl {
  /**
   * Create a new ViolationWriter instance.
   *
   * @param baseDir The base directory into which the export directory will be written.
   * @param parameters Global parameters that pertain to the export as a whole.
   * @param mtmRaw The contents of the raw MTM datafile as received by the sectioncontrol system.
   * @param mtmInfo The processed speed information as used by the sectioncontrol system.
   * @param requireFreeSpaceKb The amount of space that should be free on the export device. Defaults to 1000 kb.
   * @param caseFile The file format (HHM1.4 , 4.0 etc
   * @param service The type of service (SectionControl, RedLight etc)
   * @param esaProviderType The type of esa provider (Mtm, Dynamax etc)
   */
  def apply(baseDir: String,
            parameters: ViolationGroupParameters,
            mtmRaw: Array[Byte],
            mtmInfo: Array[Byte],
            requireFreeSpaceKb: Long = 1000,
            caseFile: CaseFile,
            service: Service,
            esaProviderType: EsaProviderType.Value,
            optSpeedLimit: Option[Int],
            caseFileConfigService: CaseFileConfigService): ViolationWriterImpl = {

    new ViolationWriterImpl(baseDir, parameters, mtmRaw, mtmInfo, requireFreeSpaceKb, caseFile, service, esaProviderType, optSpeedLimit, caseFileConfigService)
  }
}