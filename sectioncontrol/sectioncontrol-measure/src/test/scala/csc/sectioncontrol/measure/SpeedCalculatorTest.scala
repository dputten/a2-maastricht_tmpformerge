/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.measure

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

class SpeedCalculatorTest extends WordSpec with MustMatchers {
  "SpeedCalculator" must {
    val speedCalc = new SpeedCalculator()

    "calculate 100 km/hour speed" in {
      val oneHour = 3600 * 1000
      val oneKilometer = 100000
      val speed = speedCalc.calculate(1000, 1000 + oneHour, oneKilometer)
      speed must be(100)
    }
    "round to 100 km/hour speed" in {
      val oneHour = 3600 * 1000
      val length = 100499
      val speed = speedCalc.calculate(1000, 1000 + oneHour, length)
      speed must be(100)
    }
    "round to 101 km/hour speed" in {
      val oneHour = 3600 * 1000
      val length = 100500
      val speed = speedCalc.calculate(1000, 1000 + oneHour, length)
      speed must be(101)
    }
  }
}
