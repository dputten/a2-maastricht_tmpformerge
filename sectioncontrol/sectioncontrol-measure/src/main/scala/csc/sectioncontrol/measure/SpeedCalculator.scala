/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.measure

import csc.sectioncontrol.sign.SoftwareChecksum

/**
 * //TODO 20120519 RR->AS: the 'conversion' of CEST to UTC must be certified as well. (It is a strange requirement from the past)
 * //TODO 20120519 RR->AS: we need to discuss how to proceed.
 * Speed calculation
 */
class SpeedCalculator {

  /**
   * Calculate the speed according to req 35 and req 37
   * @param start - timestamp in milliseconds when the entry passed, UTC time
   * @param finish - timestamp in milliseconds when the exit passed, UTC time
   * @param distance - distance in meters
   * @return speed in km/h rounded to the lower value
   */
  def calculate(start: Long, finish: Long, distance: Int): Int = {
    val duration = finish - start
    // req 35: km/h
    val speed = distance.toDouble * 3600 / duration.toDouble
    // req 37: Een trajectcontrolesysteem moet de berekende trajectsnelheid afkappen op
    // hele km/h. Hiermee wordt bedoeld dat de berekende snelheid naar beneden
    // afgerond moet worden op hele km / h
    // Req. 37 wordt gewijzigd in: "Een trajectcontrolesysteem moet de berekende trajectsnelheid afronden volgens de normale afrondingsregels op hele km/h."
    scala.math.round(speed).toInt
  }

  lazy val sha = SoftwareChecksum.createHashFromRuntimeClass(this).getOrElse(SoftwareChecksum("", ""))
}
