/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.akkautils

/**
 * Object introduced to be able to create RunToken in unit-tests
 */
object CreateRunToken {
  def instance(
    scheduledTime: Long,
    startTime: Long,
    timezone: String): RunToken = {
    RunToken(scheduledTime, startTime, timezone)
  }
}