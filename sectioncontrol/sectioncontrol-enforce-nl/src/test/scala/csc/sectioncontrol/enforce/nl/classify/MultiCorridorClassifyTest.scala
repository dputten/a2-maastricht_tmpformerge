/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.nl.classify

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.messages._
import csc.sectioncontrol.messages.IndicatorReason
import csc.sectioncontrol.storage.MultiCorridorClassifyConfig

class MultiCorridorClassifyTest extends WordSpec with MustMatchers {
  val config = new MultiCorridorClassifyConfig(enable = true,
    minReliableCar = 70,
    minimumViolationTimeSeparation = 1800000L)
  "groupByTime" must {
    "group 3 corridors" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Car", 90)))

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 10 * 60 * 1000,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 2,
        category = Some(new ValueWithConfidence[String]("Car", 90)))
      val record3 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 20 * 60 * 1000,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 3,
        category = Some(new ValueWithConfidence[String]("Car", 90)))
      val listOfGroups = classify.groupByTime(Seq(record1, record2, record3))
      listOfGroups.size must be(1)
      val head = listOfGroups.head
      head.size must be(3)
      head must contain(record1)
      head must contain(record2)
      head must contain(record3)
    }
    "group 3 the same corridors" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Car", 90)))

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 10 * 60 * 1000,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Car", 90)))
      val record3 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 20 * 60 * 1000,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Car", 90)))
      val listOfGroups = classify.groupByTime(Seq(record1, record2, record3))
      listOfGroups.size must be(3)
      listOfGroups(0).size must be(1)
      listOfGroups(0) must contain(record1)
      listOfGroups(1).size must be(1)
      listOfGroups(1) must contain(record2)
      listOfGroups(2).size must be(1)
      listOfGroups(2) must contain(record3)
    }
    "group 3 corridors but to long time spaces" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Car", 90)))

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 30 * 60 * 1000 + 1,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 2,
        category = Some(new ValueWithConfidence[String]("Car", 90)))
      val record3 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 60 * 60 * 1000 + 2,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 3,
        category = Some(new ValueWithConfidence[String]("Car", 90)))
      val listOfGroups = classify.groupByTime(Seq(record1, record2, record3))
      listOfGroups.size must be(3)
      listOfGroups(0).size must be(1)
      listOfGroups(0) must contain(record1)
      listOfGroups(1).size must be(1)
      listOfGroups(1) must contain(record2)
      listOfGroups(2).size must be(1)
      listOfGroups(2) must contain(record3)
    }
    "group 3 corridors multiple times" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Car", 90)))

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 10 * 60 * 1000,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 2,
        category = Some(new ValueWithConfidence[String]("Car", 90)))
      val record3 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 20 * 60 * 1000,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 3,
        category = Some(new ValueWithConfidence[String]("Car", 90)))
      val record4 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 30 * 60 * 1000,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Car", 90)))

      val record5 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 61 * 60 * 1000,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 2,
        category = Some(new ValueWithConfidence[String]("Car", 90)))
      val record6 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 80 * 60 * 1000,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 3,
        category = Some(new ValueWithConfidence[String]("Car", 90)))
      val listOfGroups = classify.groupByTime(Seq(record4, record1, record5, record2, record6, record3))
      listOfGroups.size must be(3)
      val head = listOfGroups(0)
      head.size must be(3)
      head must contain(record1)
      head must contain(record2)
      head must contain(record3)
      listOfGroups(1).size must be(1)
      listOfGroups(1) must contain(record4)
      val last = listOfGroups(2)
      last.size must be(2)
      last must contain(record5)
      last must contain(record6)
    }
  }
  "findReliableClassification" must {
    "find one reliable Car" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("CarTrailer", 90)))

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 10 * 60 * 1000,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Car", 60)))
      val record3 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 20 * 60 * 1000,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Car", 70)))
      val result = classify.findReliableClassification(Seq(record1, record2, record3))
      result.isDefined must be(true)
      result.get must be(record3)
    }
    "find one reliable Van" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("CarTrailer", 90)))

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 10 * 60 * 1000,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Car", 60)))
      val record3 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 20 * 60 * 1000,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Van", 70)))
      val result = classify.findReliableClassification(Seq(record1, record2, record3))
      result.isDefined must be(true)
      result.get must be(record3)
    }
    "fail to find one reliable classification" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("CarTrailer", 90)))

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 10 * 60 * 1000,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Car", 60)))
      val record3 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 20 * 60 * 1000,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Van", 60)))
      val result = classify.findReliableClassification(Seq(record1, record2, record3))
      result.isDefined must be(false)
    }
  }
  "findSameClassification" must {
    "find 2 Cars" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("CarTrailer", 90)))

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 10 * 60 * 1000,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Car", 60)))
      val record3 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 20 * 60 * 1000,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Car", 0)))
      val result = classify.findSameClassification(Seq(record1, record2, record3))
      result.size must be(2)
      result must contain(record2)
      result must contain(record3)

    }
    "find 2 Vans" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("CarTrailer", 90)))

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 10 * 60 * 1000,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Van", 60)))
      val record3 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 20 * 60 * 1000,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Van", 0)))
      val result = classify.findSameClassification(Seq(record1, record2, record3))
      result.size must be(2)
      result must contain(record2)
      result must contain(record3)
    }
    "find 1 Car and 1 Van" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("CarTrailer", 90)))

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 10 * 60 * 1000,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Van", 60)))
      val record3 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 20 * 60 * 1000,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Car", 0)))
      val result = classify.findSameClassification(Seq(record1, record2, record3))
      result.size must be(2)
      result must contain(record2)
      result must contain(record3)
    }
    "fail to find a double record" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("CarTrailer", 90)))

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 10 * 60 * 1000,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Van", 60)))
      val record3 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 20 * 60 * 1000,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Truck", 0)))
      val result = classify.findSameClassification(Seq(record1, record2, record3))
      result.size must be(0)
    }
  }

  "correct2Classification" must {
    "correct first None with car" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = None)

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 10 * 60 * 1000,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Automatic,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Car", 10)))

      val result = classify.correct2Classification(record1, record2)
      result.size must be(2)
      result must contain(record2)
      val expected = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected)
    }
    "correct first Unknown with car" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Unknown", 10)))

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 10 * 60 * 1000,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Automatic,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Car", 10)))

      val result = classify.correct2Classification(record1, record2)
      result.size must be(2)
      result must contain(record2)
      val expected = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected)
    }
    "correct second None with car" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = None)

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 10 * 60 * 1000,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Automatic,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Car", 10)))

      val result = classify.correct2Classification(record2, record1)
      result.size must be(2)
      result must contain(record2)
      val expected = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected)
    }
    "dont correct first None with car" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = None)

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 10 * 60 * 1000,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Automatic,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Car", 0)))

      val result = classify.correct2Classification(record1, record2)
      result.size must be(2)
      result must contain(record1)
      result must contain(record2)
    }
  }
  "correctClassification" must {
    "with empty list" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val result = classify.correctClassification(Seq())
      result.size must be(0)
    }
    "with one record list" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = None)

      val result = classify.correctClassification(Seq(record1))
      result.size must be(1)
      result must contain(record1)
    }
    "with two records in list but none can be updated" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = None)

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 10 * 60 * 1000,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("CarTrailer", 0)))

      val result = classify.correctClassification(Seq(record1, record2))
      result.size must be(2)
      result must contain(record1)
      result must contain(record2)

    }
    "correct with two records in list with car" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = None)

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 10 * 60 * 1000,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Automatic,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Car", 10)))

      val result = classify.correctClassification(Seq(record1, record2))
      result.size must be(2)
      result must contain(record2)
      val expected = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected)
    }
    "dont correct with three records in list" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = None)

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 10 * 60 * 1000,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Automatic,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Car", 10)))

      val record3 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 20 * 60 * 1000,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Automatic,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = None)

      val result = classify.correctClassification(Seq(record1, record2, record3))
      result.size must be(3)
      result must contain(record1)
      result must contain(record2)
      result must contain(record3)
    }
    "correct with three records in list and one overrule" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = None)

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 10 * 60 * 1000,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Automatic,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Car", 70)))

      val record3 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 20 * 60 * 1000,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Automatic,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = None)

      val result = classify.correctClassification(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record2)
      val expected = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected)

      val expected2 = record3.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected2)
    }
    "correct with three records in list and double overrule" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = None)

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 10 * 60 * 1000,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Automatic,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Car", 10)))

      val record3 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 20 * 60 * 1000,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Automatic,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Car", 0)))

      val result = classify.correctClassification(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record2)
      result must contain(record3)
      val expected = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected)

    }
    "dont correct with three records in list and double overrule, but wrong dambord" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("A"),
        corridorId = 1,
        category = None)

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 10 * 60 * 1000,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Automatic,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Car", 10)))

      val record3 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 20 * 60 * 1000,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Automatic,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Car", 0)))

      val result = classify.correctClassification(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record1)
      result must contain(record2)
      result must contain(record3)
    }
  }
  "checkClassifications" must {
    "process 3 with same license" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = None,
        license = Some(new ValueWithConfidence[String]("12abc3", 70)))

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 10 * 60 * 1000,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Automatic,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 2,
        category = Some(new ValueWithConfidence[String]("Car", 70)),
        license = Some(new ValueWithConfidence[String]("12abc3", 70)))

      val record3 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 20 * 60 * 1000,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Automatic,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 3,
        category = None,
        license = Some(new ValueWithConfidence[String]("12abc3", 70)))

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record2)
      val expected = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected)

      val expected2 = record3.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected2)
    }
    "process 2 with same license" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("CarTrailer", 70)),
        license = Some(new ValueWithConfidence[String]("12abc3", 70)))

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 10 * 60 * 1000,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Automatic,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 2,
        category = Some(new ValueWithConfidence[String]("Car", 70)),
        license = Some(new ValueWithConfidence[String]("12abc3", 70)))

      val result = classify.checkClassifications(Seq(record1, record2))
      result.size must be(2)

      result must contain(record2)
      val expected = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected)
    }
    "partly process 3 with diferent licenses" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = None,
        license = Some(new ValueWithConfidence[String]("12abc4", 70)))

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 10 * 60 * 1000,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Automatic,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 2,
        category = Some(new ValueWithConfidence[String]("Car", 70)),
        license = Some(new ValueWithConfidence[String]("12abc3", 70)))

      val record3 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 20 * 60 * 1000,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Automatic,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 3,
        category = None,
        license = Some(new ValueWithConfidence[String]("12abc3", 70)))

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record1)
      result must contain(record2)

      val expected2 = record3.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected2)
    }
    "dont process 3 with diferent licenses and double registration" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = None,
        license = Some(new ValueWithConfidence[String]("12abc4", 70)))

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 10 * 60 * 1000,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Automatic,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 2,
        category = Some(new ValueWithConfidence[String]("Car", 70)),
        license = Some(new ValueWithConfidence[String]("12abc3", 70)))

      val record3 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 20 * 60 * 1000,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Automatic,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 2,
        category = None,
        license = Some(new ValueWithConfidence[String]("12abc3", 70)))

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record1)
      result must contain(record2)
      result must contain(record3)
    }
    "dont process 3 with missing dambord info" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.AS),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = None,
        corridorId = 1,
        category = None,
        license = Some(new ValueWithConfidence[String]("12abc3", 70)))

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 10 * 60 * 1000,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Automatic,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 2,
        category = Some(new ValueWithConfidence[String]("Car", 90)),
        license = Some(new ValueWithConfidence[String]("12abc3", 70)))

      val record3 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 20 * 60 * 1000,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Automatic,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 2,
        category = None,
        license = Some(new ValueWithConfidence[String]("12abc3", 70)))

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record1)
      result must contain(record2)
      result must contain(record3)
    }
    "dont set to auto when acourding to spec" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = true,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Car", 0)),
        license = Some(new ValueWithConfidence[String]("12abc3", 70)))

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 10 * 60 * 1000,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = true,
        dambord = Some("P"),
        corridorId = 2,
        category = Some(new ValueWithConfidence[String]("Car", 0)),
        license = Some(new ValueWithConfidence[String]("12abc3", 70)))

      val record3 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 20 * 60 * 1000,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 3,
        category = None,
        license = Some(new ValueWithConfidence[String]("12abc3", 90)))

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record1)
      result must contain(record2)
      val expected3 = record3.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = IndicatorReason(0))

      result must contain(expected3)
    }
    "dont set to auto when country or license is unreliable" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Car", 0)),
        license = Some(new ValueWithConfidence[String]("12abc3", 70)),
        reason = IndicatorReason(7))

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 10 * 60 * 1000,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 2,
        category = Some(new ValueWithConfidence[String]("Car", 0)),
        license = Some(new ValueWithConfidence[String]("12abc3", 70)),
        reason = IndicatorReason(7))

      val record3 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 20 * 60 * 1000,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 3,
        category = None,
        license = Some(new ValueWithConfidence[String]("12abc3", 90)),
        reason = IndicatorReason(7))

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record1)
      result must contain(record2)
      val expected3 = record3.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)

      result must contain(expected3)
    }
    "set to auto when at least country or license is reliable" in {
      val startTime = System.currentTimeMillis()
      val classify = new MultiCorridorClassify(config)

      val record1 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 1,
        category = Some(new ValueWithConfidence[String]("Car", 0)),
        license = Some(new ValueWithConfidence[String]("12abc3", 70)),
        reason = IndicatorReason(2))

      val record2 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 10 * 60 * 1000,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 2,
        category = Some(new ValueWithConfidence[String]("Car", 0)),
        license = Some(new ValueWithConfidence[String]("12abc3", 70)),
        reason = IndicatorReason(7))

      val record3 = CreateClassifiedRecords.createVehicleClassifiedRecord(time = startTime + 20 * 60 * 1000,
        code = Some(VehicleCode.PA),
        indicator = ProcessingIndicator.Manual,
        manualAccordingToSpec = false,
        dambord = Some("P"),
        corridorId = 3,
        category = None,
        license = Some(new ValueWithConfidence[String]("12abc3", 90)),
        reason = IndicatorReason(7))

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(indicator = ProcessingIndicator.Automatic,
        reason = IndicatorReason(0))
      result must contain(expected1)
      val expected2 = record2.copy(indicator = ProcessingIndicator.Automatic,
        reason = IndicatorReason(0))
      result must contain(expected2)
      val expected3 = record3.copy(code = record2.code,
        indicator = ProcessingIndicator.Automatic,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = IndicatorReason(0))
      result must contain(expected3)
    }
  }

}