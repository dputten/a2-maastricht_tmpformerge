/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.nl.classify

import csc.sectioncontrol.messages._
import csc.sectioncontrol.storage.MultiCorridorClassifyConfig
import akka.util.duration._
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

class MultiCorridorClassifyVanTest extends WordSpec with MustMatchers {
  val config = new MultiCorridorClassifyConfig(enable = true,
    minReliableCar = 10,
    minimumViolationTimeSeparation = 1800000L)
  val startTime = System.currentTimeMillis()
  val classify = new MultiCorridorClassify(config)

  "MultiClassify with 3 corridors" must {
    "unchange when 1,1,1" in {
      val record1 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB")
      val record3 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record1)
      result must contain(record2)
      result must contain(record3)
    }
    "unchange when 1,1,2" in {
      val record1 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB")
      val record3 = CreateClassifiedRecords.create2Empty(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record1)
      result must contain(record2)
      result must contain(record3)
    }
    "change both 1's when 1,1,4" in {
      val record1 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB")
      val record3 = CreateClassifiedRecords.create4Conf70(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected1)

      val expected2 = record2.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected2)

      result must contain(record3)
    }
    "change both 1's when 1,1,5" in {
      val record1 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB")
      val record3 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected1)

      val expected2 = record2.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected2)

      result must contain(record3)
    }

    "Unchanged when 1,1,6" in {
      val record1 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB")
      val record3 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record1)
      result must contain(record2)
      result must contain(record3)
    }

    "change all when 1,1,7" in {
      val record1 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB")
      val record3 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record3.code,
        indicator = ProcessingIndicator.Manual,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected1)

      val expected2 = record2.copy(code = record3.code,
        indicator = ProcessingIndicator.Manual,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected2)

      val expected3 = record3.copy(indicator = ProcessingIndicator.Manual)
      result must contain(expected3)
    }
    "unchange when 1,2,2" in {
      val record1 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create2Empty(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB")
      val record3 = CreateClassifiedRecords.create2Empty(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record1)
      result must contain(record2)
      result must contain(record3)
    }

    "change both 1 and 2 when 1,2,4" in {
      val record1 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create2Empty(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB")
      val record3 = CreateClassifiedRecords.create4Conf70(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected1)

      val expected2 = record2.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected2)

      result must contain(record3)
    }
    "change both 1 and 2 when 1,2,5" in {
      val record1 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create2Empty(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB")
      val record3 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected1)

      val expected2 = record2.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected2)

      result must contain(record3)
    }
    "unchange when 1,2,6" in {
      val record1 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create2Empty(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB")
      val record3 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected1)

      val expected2 = record2.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected2)
      result must contain(record3)
    }
    "change both 1 and 2 when 1,2,7" in {
      val record1 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create2Empty(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB")
      val record3 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record3.code,
        indicator = ProcessingIndicator.Manual,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected1)

      val expected2 = record2.copy(code = record3.code,
        indicator = ProcessingIndicator.Manual,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected2)

      val expected3 = record3.copy(indicator = ProcessingIndicator.Manual)
      result must contain(expected3)
    }
    "change 1 when 1,4,4" in {
      val record1 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create4Conf70(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create4Conf70(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected1)

      result must contain(record2)
      result must contain(record3)
    }
    "change 1 when 1,4,5" in {
      val record1 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create4Conf70(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected1)

      result must contain(record2)
      result must contain(record3)
    }
    "change 1 and 6 when 1,4,6" in {
      val record1 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create4Conf70(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected1)

      result must contain(record2)
      val expected3 = record3.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected3)
    }
    "change 1 when 1,4,7" in {
      val record1 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create4Conf70(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected1)

      result must contain(record2)
      result must contain(record3)
    }
    "change 1 when 1,5,5" in {
      val record1 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected1)

      result must contain(record2)
      result must contain(record3)
    }
    "change 1 when 1,5,6" in {
      val record1 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected1)

      result must contain(record2)
      val expected3 = record3.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected3)
    }
    "change 1 when 1,5,7" in {
      val record1 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected1)

      result must contain(record2)
      result must contain(record3)
    }
    "change 1 when 1,6,6" in {
      val record1 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected1)

      result must contain(record2)
      result must contain(record3)
    }
    "change 1 when 1,6,7" in {
      val record1 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected1)

      val expected2 = record2.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected2)
      result must contain(record3)
    }
    "change 1 when 1,7,7" in {
      val record1 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected1)
      result must contain(record2)
      result must contain(record3)
    }
    "unchange when 2,2,2" in {
      val record1 = CreateClassifiedRecords.create2Empty(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create2Empty(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB")
      val record3 = CreateClassifiedRecords.create2Empty(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record1)
      result must contain(record2)
      result must contain(record3)
    }

    "change both 1 and 2 when 2,2,4" in {
      val record1 = CreateClassifiedRecords.create2Empty(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create2Empty(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB")
      val record3 = CreateClassifiedRecords.create4Conf70(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected1)

      val expected2 = record2.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected2)

      result must contain(record3)
    }
    "change both 1 and 2 when 2,2,5" in {
      val record1 = CreateClassifiedRecords.create2Empty(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create2Empty(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB")
      val record3 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected1)

      val expected2 = record2.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected2)

      result must contain(record3)
    }
    "update both 2 when 2,2,6" in {
      val record1 = CreateClassifiedRecords.create2Empty(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create2Empty(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB")
      val record3 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected1)

      val expected2 = record2.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)

      result must contain(record3)
    }
    "change both 2 when 2,2,7" in {
      val record1 = CreateClassifiedRecords.create2Empty(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create2Empty(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB")
      val record3 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected1)

      val expected2 = record2.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected2)

      result must contain(record3)
    }
    "change 2 when 2,4,4" in {
      val record1 = CreateClassifiedRecords.create2Empty(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create4Conf70(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create4Conf70(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected1)

      result must contain(record2)
      result must contain(record3)
    }
    "change 2 when 2,4,5" in {
      val record1 = CreateClassifiedRecords.create2Empty(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create4Conf70(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected1)

      result must contain(record2)
      result must contain(record3)
    }
    "change 2 and 6 when 2,4,6" in {
      val record1 = CreateClassifiedRecords.create2Empty(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create4Conf70(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected1)

      result must contain(record2)
      val expected3 = record3.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected3)
    }
    "change 2 when 2,4,7" in {
      val record1 = CreateClassifiedRecords.create2Empty(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create4Conf70(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected1)

      result must contain(record2)
      result must contain(record3)
    }
    "change 2 when 2,5,5" in {
      val record1 = CreateClassifiedRecords.create2Empty(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected1)

      result must contain(record2)
      result must contain(record3)
    }
    "change 2 and 6 when 2,5,6" in {
      val record1 = CreateClassifiedRecords.create2Empty(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected1)

      result must contain(record2)
      val expected3 = record3.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected3)
    }
    "change 2 when 2,5,7" in {
      val record1 = CreateClassifiedRecords.create2Empty(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected1)

      result must contain(record2)
      result must contain(record3)
    }
    "change all to auto when 2,6,6" in {
      val record1 = CreateClassifiedRecords.create2Empty(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record2.code,
        indicator = ProcessingIndicator.Automatic,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected1)

      val expected2 = record2.copy(indicator = ProcessingIndicator.Automatic)
      result must contain(expected2)
      val expected3 = record3.copy(indicator = ProcessingIndicator.Automatic)
      result must contain(expected3)
    }
    "change 2 and 6 when 2,6,7" in {
      val record1 = CreateClassifiedRecords.create2Empty(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected1)

      val expected2 = record2.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected2)
      result must contain(record3)
    }
    "change 2 when 2,7,7" in {
      val record1 = CreateClassifiedRecords.create2Empty(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected1)
      result must contain(record2)
      result must contain(record3)
    }
    "no changes when 4,4,4" in {
      val record1 = CreateClassifiedRecords.create4Conf70(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create4Conf70(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create4Conf70(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record1)
      result must contain(record2)
      result must contain(record3)
    }
    "no changes when 4,4,5" in {
      val record1 = CreateClassifiedRecords.create4Conf70(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create4Conf70(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record1)
      result must contain(record2)
      result must contain(record3)
    }
    "change6 when 4,4,6" in {
      val record1 = CreateClassifiedRecords.create4Conf70(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create4Conf70(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record1)
      result must contain(record2)
      val expected3 = record3.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected3)
    }
    "no changes when 4,4,7" in {
      val record1 = CreateClassifiedRecords.create4Conf70(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create4Conf70(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record1)
      result must contain(record2)
      result must contain(record3)
    }
    "no changes when 4,5,5" in {
      val record1 = CreateClassifiedRecords.create4Conf70(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record1)
      result must contain(record2)
      result must contain(record3)
    }
    "change 6 when 4,5,6" in {
      val record1 = CreateClassifiedRecords.create4Conf70(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record1)
      result must contain(record2)
      val expected3 = record3.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected3)
    }
    "no changes when 4,5,7" in {
      val record1 = CreateClassifiedRecords.create4Conf70(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record1)
      result must contain(record2)
      result must contain(record3)
    }
    "change both 6 when 4,6,6" in {
      val record1 = CreateClassifiedRecords.create4Conf70(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record1)

      val expected2 = record2.copy(code = record1.code,
        indicator = record1.indicator,
        alternativeClassification = record1.alternativeClassification,
        manualAccordingToSpec = record1.manualAccordingToSpec,
        reason = record1.reason)
      result must contain(expected2)
      val expected3 = record3.copy(code = record1.code,
        indicator = record1.indicator,
        alternativeClassification = record1.alternativeClassification,
        manualAccordingToSpec = record1.manualAccordingToSpec,
        reason = record1.reason)
      result must contain(expected3)
    }
    "change 6 when 4,6,7" in {
      val record1 = CreateClassifiedRecords.create4Conf70(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record1)

      val expected2 = record2.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected2)
      result must contain(record3)
    }
    "no changes when 4,7,7" in {
      val record1 = CreateClassifiedRecords.create4Conf70(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record1)
      result must contain(record2)
      result must contain(record3)
    }

    "no changes when 5,5,5" in {
      val record1 = CreateClassifiedRecords.create5Conf10(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record1)
      result must contain(record2)
      result must contain(record3)
    }
    "change 6 when 5,5,6" in {
      val record1 = CreateClassifiedRecords.create5Conf10(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record1)
      result must contain(record2)
      val expected3 = record3.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected3)
    }
    "no changes when 5,5,7" in {
      val record1 = CreateClassifiedRecords.create5Conf10(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record1)
      result must contain(record2)
      result must contain(record3)
    }
    "change both 6 when 5,6,6" in {
      val record1 = CreateClassifiedRecords.create5Conf10(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record1)

      val expected2 = record2.copy(code = record1.code,
        indicator = record1.indicator,
        alternativeClassification = record1.alternativeClassification,
        manualAccordingToSpec = record1.manualAccordingToSpec,
        reason = record1.reason)
      result must contain(expected2)
      val expected3 = record3.copy(code = record1.code,
        indicator = record1.indicator,
        alternativeClassification = record1.alternativeClassification,
        manualAccordingToSpec = record1.manualAccordingToSpec,
        reason = record1.reason)
      result must contain(expected3)
    }
    "change 6 when 5,6,7" in {
      val record1 = CreateClassifiedRecords.create5Conf10(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record1)

      val expected2 = record2.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected2)
      result must contain(record3)
    }
    "no changes when 5,7,7" in {
      val record1 = CreateClassifiedRecords.create5Conf10(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record1)
      result must contain(record2)
      result must contain(record3)
    }
    "change all when 6,6,6" in {
      val record1 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(indicator = ProcessingIndicator.Automatic)
      result must contain(expected1)
      val expected2 = record2.copy(indicator = ProcessingIndicator.Automatic)
      result must contain(expected2)
      val expected3 = record3.copy(indicator = ProcessingIndicator.Automatic)
      result must contain(expected3)
    }
    "change both 6 when 6,6,7" in {
      val record1 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected1)
      val expected2 = record2.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected2)
      result must contain(record3)
    }
    "change 6 when 6,7,7" in {
      val record1 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      val expected1 = record1.copy(code = record3.code,
        indicator = record3.indicator,
        alternativeClassification = record3.alternativeClassification,
        manualAccordingToSpec = record3.manualAccordingToSpec,
        reason = record3.reason)
      result must contain(expected1)
      result must contain(record2)
      result must contain(record3)
    }
    "no changes when 7,7,7" in {
      val record1 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")
      val record3 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 20.minutes.toMillis,
        corridorId = 3,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2, record3))
      result.size must be(3)

      result must contain(record1)
      result must contain(record2)
      result must contain(record3)
    }
  }

  "MultiClassify with 2 corridors" must {
    "unchange when 1,1" in {
      val record1 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB")

      val result = classify.checkClassifications(Seq(record1, record2))
      result.size must be(2)

      result must contain(record1)
      result must contain(record2)
    }
    "unchange when 1,2" in {
      val record1 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create2Empty(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB")

      val result = classify.checkClassifications(Seq(record1, record2))
      result.size must be(2)

      result must contain(record1)
      result must contain(record2)
    }
    "update 1 when 1,4" in {
      val record1 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create4Conf70(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2))
      result.size must be(2)

      val expected1 = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected1)
      result must contain(record2)
    }
    "update 1 when 1,5" in {
      val record1 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2))
      result.size must be(2)

      val expected1 = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected1)
      result must contain(record2)
    }
    "update 1 when 1,6" in {
      val record1 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2))
      result.size must be(2)

      val expected1 = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected1)
      result must contain(record2)
    }
    "update 1 when 1,7" in {
      val record1 = CreateClassifiedRecords.create1NoCarVan(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2))
      result.size must be(2)

      val expected1 = record1.copy(code = record2.code,
        indicator = ProcessingIndicator.Manual,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected1)
      val expected2 = record2.copy(indicator = ProcessingIndicator.Manual)
      result must contain(expected2)
    }

    "unchange when 2,2" in {
      val record1 = CreateClassifiedRecords.create2Empty(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create2Empty(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB")

      val result = classify.checkClassifications(Seq(record1, record2))
      result.size must be(2)

      result must contain(record1)
      result must contain(record2)
    }
    "update 2 when 2,4" in {
      val record1 = CreateClassifiedRecords.create2Empty(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create4Conf70(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2))
      result.size must be(2)

      val expected1 = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected1)
      result must contain(record2)
    }
    "update 2 when 2,5" in {
      val record1 = CreateClassifiedRecords.create2Empty(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2))
      result.size must be(2)

      val expected1 = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected1)
      result must contain(record2)
    }
    "update 2 when 2,6" in {
      val record1 = CreateClassifiedRecords.create2Empty(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2))
      result.size must be(2)

      val expected1 = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected1)
      result must contain(record2)
    }
    "update 2 when 2,7" in {
      val record1 = CreateClassifiedRecords.create2Empty(
        time = startTime,
        corridorId = 1,
        dambord = "LB")
      val record2 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2))
      result.size must be(2)

      val expected1 = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected1)
      result must contain(record2)
    }
    "no changes when 4,4" in {
      val record1 = CreateClassifiedRecords.create4Conf70(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create4Conf70(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2))
      result.size must be(2)

      result must contain(record1)
      result must contain(record2)
    }
    "nochanges when 4,5" in {
      val record1 = CreateClassifiedRecords.create4Conf70(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2))
      result.size must be(2)

      result must contain(record1)
      result must contain(record2)
    }
    "update 6 when 4,6" in {
      val record1 = CreateClassifiedRecords.create4Conf70(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2))
      result.size must be(2)

      result must contain(record1)
      val expected2 = record2.copy(code = record1.code,
        indicator = record1.indicator,
        alternativeClassification = record1.alternativeClassification,
        manualAccordingToSpec = record1.manualAccordingToSpec,
        reason = record1.reason)
      result must contain(expected2)
    }
    "no changes when 4,7" in {
      val record1 = CreateClassifiedRecords.create4Conf70(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2))
      result.size must be(2)

      result must contain(record1)
      result must contain(record2)
    }
    "nochanges when 5,5" in {
      val record1 = CreateClassifiedRecords.create5Conf10(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create5Conf10(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2))
      result.size must be(2)

      result must contain(record1)
      result must contain(record2)
    }
    "update 6 when 5,6" in {
      val record1 = CreateClassifiedRecords.create5Conf10(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2))
      result.size must be(2)

      result must contain(record1)
      val expected2 = record2.copy(code = record1.code,
        indicator = record1.indicator,
        alternativeClassification = record1.alternativeClassification,
        manualAccordingToSpec = record1.manualAccordingToSpec,
        reason = record1.reason)
      result must contain(expected2)
    }
    "no changes when 5,7" in {
      val record1 = CreateClassifiedRecords.create5Conf10(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2))
      result.size must be(2)

      result must contain(record1)
      result must contain(record2)
    }
    "update both 6 when 6,6" in {
      val record1 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2))
      result.size must be(2)

      val expected1 = record1.copy(indicator = ProcessingIndicator.Automatic)
      result must contain(expected1)
      val expected2 = record2.copy(indicator = ProcessingIndicator.Automatic)
      result must contain(expected2)
    }
    "update 6 when 6,7" in {
      val record1 = CreateClassifiedRecords.create6Conf0_LengthNOK(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2))
      result.size must be(2)

      val expected1 = record1.copy(code = record2.code,
        indicator = record2.indicator,
        alternativeClassification = record2.alternativeClassification,
        manualAccordingToSpec = record2.manualAccordingToSpec,
        reason = record2.reason)
      result must contain(expected1)
      result must contain(record2)
    }
    "no changes when 7,7" in {
      val record1 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime,
        corridorId = 1,
        dambord = "LB",
        category = "Van")
      val record2 = CreateClassifiedRecords.create7Conf0_LengthOK(
        time = startTime + 10.minutes.toMillis,
        corridorId = 2,
        dambord = "LB",
        category = "Van")

      val result = classify.checkClassifications(Seq(record1, record2))
      result.size must be(2)

      result must contain(record1)
      result must contain(record2)
    }

  }

}