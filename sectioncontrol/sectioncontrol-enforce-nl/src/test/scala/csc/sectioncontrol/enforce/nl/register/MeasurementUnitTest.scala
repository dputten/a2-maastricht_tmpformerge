/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.nl.register

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import akka.util.duration._
import csc.sectioncontrol.messages._
import java.util.{ Calendar, GregorianCalendar, Date }
import csc.sectioncontrol.messages.ValueWithConfidence
import csc.sectioncontrol.messages.VehicleViolationRecord
import csc.sectioncontrol.storage.SelfTestResult
import csc.sectioncontrol.messages.VehicleMetadata
import csc.sectioncontrol.messages.IndicatorReason
import csc.sectioncontrol.messages.VehicleSpeedRecord
import csc.sectioncontrol.messages.Lane
import csc.sectioncontrol.enforce.common.nl.violations.{ CorridorData, Violation }
import csc.sectioncontrol.messages.certificates.LocationCertificate
import csc.sectioncontrol.storagelayer.corridor.CorridorIdMapping
import csc.sectioncontrol.enforce.Enumerations
import csc.sectioncontrol.enforce.common.nl.services.{ SectionControl, Service }
import csc.sectioncontrol.enforce.common.nl.violations

class MeasurementUnitTest extends WordSpec with MustMatchers {
  val jan_25_2002 = new GregorianCalendar(2002, Calendar.JANUARY, 25, 0, 0, 0).getTimeInMillis / 1000 * 1000
  val jan_26_2002 = new GregorianCalendar(2002, Calendar.JANUARY, 26, 0, 0, 0).getTimeInMillis / 1000 * 1000

  "createCorrectSelfTestPeriods" must {
    "create 1 period with start and end time" in {
      val startTime = jan_25_2002

      val selfTests = Seq(new SelfTestResult(systemId = "A2",
        corridorId = "S1",
        time = startTime,
        success = true,
        reportingOfficerCode = "A123",
        nrCamerasTested = 1,
        images = Seq(),
        errors = Seq(),
        serialNr = "SN1234"),
        new SelfTestResult(systemId = "A2",
          corridorId = "S1",
          time = startTime + 1.day.toMillis,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234"))
      val result = MeasurementUnit.createCorrectSelfTestPeriods(selfTests)
      result.size must be(1)
      result must contain(new SelfTestPeriod("A2", "SN1234", startTime, startTime + 1.day.toMillis))
    }
    "create 2 periods with start and end time" in {
      val startTime = jan_25_2002

      val selfTests = Seq(
        new SelfTestResult(systemId = "A2",
          corridorId = "S1",
          time = startTime,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234"),
        new SelfTestResult(systemId = "A2",
          corridorId = "S1",
          time = startTime + 12.hours.toMillis,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234"),
        new SelfTestResult(systemId = "A2",
          corridorId = "S1",
          time = startTime + 1.day.toMillis,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234"))
      val result = MeasurementUnit.createCorrectSelfTestPeriods(selfTests)
      result.size must be(2)
      result must contain(new SelfTestPeriod("A2", "SN1234", startTime, startTime + 12.hours.toMillis))
      result must contain(new SelfTestPeriod("A2", "SN1234", startTime + 12.hours.toMillis, startTime + 1.day.toMillis))
    }
    "create 1 period when first serial isn't equal" in {
      val startTime = jan_25_2002

      val selfTests = Seq(
        new SelfTestResult(systemId = "A2",
          corridorId = "S1",
          time = startTime,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN123"),
        new SelfTestResult(systemId = "A2",
          corridorId = "S1",
          time = startTime + 12.hours.toMillis,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234"),
        new SelfTestResult(systemId = "A2",
          corridorId = "S1",
          time = startTime + 1.day.toMillis,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234"))
      val result = MeasurementUnit.createCorrectSelfTestPeriods(selfTests)
      result.size must be(1)
      result must contain(new SelfTestPeriod("A2", "SN1234", startTime + 12.hours.toMillis, startTime + 1.day.toMillis))
    }
    "create 0 periods when having failures" in {
      val startTime = jan_25_2002

      val selfTests = Seq(
        new SelfTestResult(systemId = "A2",
          corridorId = "S1",
          time = startTime,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234"),
        new SelfTestResult(systemId = "A2",
          time = startTime + 12.hours.toMillis,
          corridorId = "S1",
          success = false,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234"),
        new SelfTestResult(systemId = "A2",
          corridorId = "S1",
          time = startTime + 1.day.toMillis,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234"))
      val result = MeasurementUnit.createCorrectSelfTestPeriods(selfTests)
      result.size must be(0)
    }
    "create 0 periods when empty" in {
      val selfTests = Seq()
      val result = MeasurementUnit.createCorrectSelfTestPeriods(selfTests)
      result.size must be(0)
    }
    "create 0 periods when one record" in {
      val startTime = jan_25_2002

      val selfTests = Seq(new SelfTestResult(systemId = "A2",
        corridorId = "S1",
        time = startTime,
        success = true,
        reportingOfficerCode = "A123",
        nrCamerasTested = 1,
        images = Seq(),
        errors = Seq(),
        serialNr = "SN1234"))
      val result = MeasurementUnit.createCorrectSelfTestPeriods(selfTests)
      result.size must be(0)
    }
  }
  "filterWrongUnits" must {
    "pass correct violations" in {
      val startTime = jan_25_2002
      val selfTests = Map(CorridorIdMapping("S1", 1) -> Seq(
        new SelfTestResult(systemId = "A2",
          corridorId = "S1",
          time = startTime,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234"),
        new SelfTestResult(systemId = "A2",
          corridorId = "S1",
          time = startTime + 12.hours.toMillis,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234"),
        new SelfTestResult(systemId = "A2",
          corridorId = "S1",
          time = startTime + 1.day.toMillis,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234")))
      val violations = Seq(
        makeViolation(systemId = "A2", id = "1", entryTime = startTime + 1.hour.toMillis, serialNr = "SN1234"),
        makeViolation(systemId = "A2", id = "2", entryTime = startTime + 13.hour.toMillis, serialNr = "SN1234"))
      val (correct, wrong) = MeasurementUnit.filterWrongUnits(violations, selfTests)
      wrong.size must be(0)
      correct.size must be(2)
      correct must contain(violations(0))
      correct must contain(violations(1))
    }
    "filter violations with multiple corridors" in {
      val startTime = jan_25_2002
      val selfTests = Map(CorridorIdMapping("S1", 1) -> Seq(
        new SelfTestResult(systemId = "A2",
          corridorId = "S1",
          time = startTime,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234"),
        new SelfTestResult(systemId = "A2",
          corridorId = "S1",
          time = startTime + 12.hours.toMillis,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234"),
        new SelfTestResult(systemId = "A12",
          corridorId = "S1",
          time = startTime + 12.hours.toMillis,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234"),
        new SelfTestResult(systemId = "A12",
          corridorId = "S1",
          time = startTime + 1.day.toMillis,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234")))
      val violations = Seq(
        makeViolation(systemId = "A2", id = "1", entryTime = startTime + 1.hour.toMillis, serialNr = "SN1234", corridorId = 1),
        makeViolation(systemId = "A12", id = "2", entryTime = startTime + 13.hour.toMillis, serialNr = "SN1234", corridorId = 2))
      val (correct, wrong) = MeasurementUnit.filterWrongUnits(violations, selfTests)
      wrong.size must be(1)
      wrong must contain(violations(1))
      correct.size must be(1)
      correct must contain(violations(0))
    }
    "pass correct violations with multiple corridors" in {
      val startTime = jan_25_2002
      val selfTests = Map(CorridorIdMapping("S1", 1) -> Seq(
        new SelfTestResult(systemId = "A2",
          corridorId = "S1",
          time = startTime,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234"),
        new SelfTestResult(systemId = "A2",
          corridorId = "S1",
          time = startTime + 12.hours.toMillis,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234")),
        CorridorIdMapping("S2", 2) -> Seq(
          new SelfTestResult(systemId = "A12",
            corridorId = "S2",
            time = startTime + 12.hours.toMillis,
            success = true,
            reportingOfficerCode = "A123",
            nrCamerasTested = 1,
            images = Seq(),
            errors = Seq(),
            serialNr = "SN1234"),
          new SelfTestResult(systemId = "A12",
            corridorId = "S2",
            time = startTime + 1.day.toMillis,
            success = true,
            reportingOfficerCode = "A123",
            nrCamerasTested = 1,
            images = Seq(),
            errors = Seq(),
            serialNr = "SN1234")))
      val violations = Seq(
        makeViolation(systemId = "A2", id = "1", entryTime = startTime + 1.hour.toMillis, serialNr = "SN1234", corridorId = 1),
        makeViolation(systemId = "A12", id = "2", entryTime = startTime + 13.hour.toMillis, serialNr = "SN1234", corridorId = 2))
      val (correct, wrong) = MeasurementUnit.filterWrongUnits(violations, selfTests)
      wrong.size must be(0)
      correct.size must be(2)
      correct must contain(violations(0))
      correct must contain(violations(1))
    }
    "filter violations with one selftest failure multiple corridors" in {
      val startTime = jan_25_2002
      val selfTests = Map(CorridorIdMapping("S1", 1) -> Seq(
        new SelfTestResult(systemId = "A2",
          corridorId = "S1",
          time = startTime,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234"),
        new SelfTestResult(systemId = "A2",
          corridorId = "S1",
          time = startTime + 12.hours.toMillis,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234")),
        CorridorIdMapping("S2", 2) -> Seq(
          new SelfTestResult(systemId = "A12",
            corridorId = "S2",
            time = startTime + 12.hours.toMillis,
            success = false,
            reportingOfficerCode = "A123",
            nrCamerasTested = 1,
            images = Seq(),
            errors = Seq(),
            serialNr = "SN1234"),
          new SelfTestResult(systemId = "A12",
            corridorId = "S2",
            time = startTime + 1.day.toMillis,
            success = true,
            reportingOfficerCode = "A123",
            nrCamerasTested = 1,
            images = Seq(),
            errors = Seq(),
            serialNr = "SN1234")))
      val violations = Seq(
        makeViolation(systemId = "A2", id = "1", entryTime = startTime + 1.hour.toMillis, serialNr = "SN1234", corridorId = 1),
        makeViolation(systemId = "A12", id = "2", entryTime = startTime + 1.hour.toMillis, serialNr = "SN1234", corridorId = 2))
      val (correct, wrong) = MeasurementUnit.filterWrongUnits(violations, selfTests)
      wrong.size must be(1)
      wrong must contain(violations(1))
      correct.size must be(1)
      correct must contain(violations(0))
    }
    "filter wrong violations" in {
      val startTime = jan_25_2002
      val selfTests = Map(CorridorIdMapping("S1", 1) -> Seq(
        new SelfTestResult(systemId = "A2",
          corridorId = "S1",
          time = startTime,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234"),
        new SelfTestResult(systemId = "A2",
          corridorId = "S1",
          time = startTime + 12.hours.toMillis,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234"),
        new SelfTestResult(systemId = "A2",
          corridorId = "S1",
          time = startTime + 1.day.toMillis,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234")))
      val violations = Seq(
        makeViolation(systemId = "A2", id = "1", entryTime = startTime + 1.hour.toMillis, serialNr = "SN1234"),
        makeViolation(systemId = "A2", id = "2", entryTime = startTime + 13.hour.toMillis, serialNr = "SN123"))
      val (correct, wrong) = MeasurementUnit.filterWrongUnits(violations, selfTests)
      wrong.size must be(1)
      correct.size must be(1)
      correct must contain(violations(0))
      wrong must contain(violations(1))
    }
    "filter wrong system violations" in {
      val startTime = jan_25_2002
      val selfTests = Map(CorridorIdMapping("S1", 1) -> Seq(
        new SelfTestResult(systemId = "A2",
          corridorId = "S1",
          time = startTime,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234"),
        new SelfTestResult(systemId = "A2",
          corridorId = "S1",
          time = startTime + 12.hours.toMillis,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234"),
        new SelfTestResult(systemId = "A2",
          corridorId = "S1",
          time = startTime + 1.day.toMillis,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234")))
      val violations = Seq(
        makeViolation(systemId = "A2", id = "1", entryTime = startTime + 1.hour.toMillis, serialNr = "SN1234"),
        makeViolation(systemId = "A12", id = "2", entryTime = startTime + 13.hour.toMillis, serialNr = "SN1234"))
      val (correct, wrong) = MeasurementUnit.filterWrongUnits(violations, selfTests)
      wrong.size must be(1)
      correct.size must be(1)
      correct must contain(violations(0))
      wrong must contain(violations(1))
    }
    "process with empty violations" in {
      val startTime = jan_25_2002
      val selfTests = Map(CorridorIdMapping("S1", 1) -> Seq(
        new SelfTestResult(systemId = "A2",
          corridorId = "S1",
          time = startTime,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234"),
        new SelfTestResult(systemId = "A2",
          corridorId = "S1",
          time = startTime + 12.hours.toMillis,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234"),
        new SelfTestResult(systemId = "A2",
          corridorId = "S1",
          time = startTime + 1.day.toMillis,
          success = true,
          reportingOfficerCode = "A123",
          nrCamerasTested = 1,
          images = Seq(),
          errors = Seq(),
          serialNr = "SN1234")))
      val violations = Seq()

      val (correct, wrong) = MeasurementUnit.filterWrongUnits(violations, selfTests)
      wrong.size must be(0)
      correct.size must be(0)
    }
    "filter empty testresults" in {
      val startTime = jan_25_2002
      val selfTests = Map[CorridorIdMapping, Seq[SelfTestResult]]()
      val violations = Seq(
        makeViolation(systemId = "A2", id = "1", entryTime = startTime + 1.hour.toMillis, serialNr = "SN1234"),
        makeViolation(systemId = "A2", id = "2", entryTime = startTime + 13.hour.toMillis, serialNr = "SN1234"))
      val (correct, wrong) = MeasurementUnit.filterWrongUnits(violations, selfTests)
      wrong.size must be(2)
      correct.size must be(0)
      wrong must contain(violations(0))
      wrong must contain(violations(1))
    }
  }
  "createExclusionPeriods" when {
    "certificate is before period" in {
      val certiticates = Seq(
        new LocationCertificate(id = "Test1", inspectionDate = 1000, validTime = 2000, components = List()))
      val period = new ExclusionPeriod(from = 2000, to = 2100)
      val result = MeasurementUnit.createExclusionPeriods(certiticates, period)
      result.size must be(1)
      result.head must be(period)
    }
    "certificate is after period" in {
      val certiticates = Seq(
        new LocationCertificate(id = "Test1", inspectionDate = 1000, validTime = 2000, components = List()))
      val period = new ExclusionPeriod(from = 100, to = 1000)
      val result = MeasurementUnit.createExclusionPeriods(certiticates, period)
      result.size must be(1)
      result.head must be(period)
    }
    "certificate valid only the start of the period" in {
      val certiticates = Seq(
        new LocationCertificate(id = "Test1", inspectionDate = 1000, validTime = 2000, components = List()))
      val period = new ExclusionPeriod(from = 1500, to = 2500)
      val result = MeasurementUnit.createExclusionPeriods(certiticates, period)
      result.size must be(1)
      result.head.from must be(2000)
      result.head.to must be(2500)
    }
    "certificate valid only the end of the period" in {
      val certiticates = Seq(
        new LocationCertificate(id = "Test1", inspectionDate = 1000, validTime = 2000, components = List()))
      val period = new ExclusionPeriod(from = 500, to = 1500)
      val result = MeasurementUnit.createExclusionPeriods(certiticates, period)
      result.size must be(1)
      result.head.from must be(500)
      result.head.to must be(1000)
    }
    "certificate valid only in the middle of the period" in {
      val certiticates = Seq(
        new LocationCertificate(id = "Test1", inspectionDate = 1000, validTime = 2000, components = List()))
      val period = new ExclusionPeriod(from = 500, to = 2500)
      val result = MeasurementUnit.createExclusionPeriods(certiticates, period)
      result.size must be(2)
      result.head.from must be(500)
      result.head.to must be(1000)
      result.last.from must be(2000)
      result.last.to must be(2500)
    }
    "certificate valid completely of the period" in {
      val certiticates = Seq(
        new LocationCertificate(id = "Test1", inspectionDate = 1000, validTime = 2000, components = List()))
      val period = new ExclusionPeriod(from = 1200, to = 1800)
      val result = MeasurementUnit.createExclusionPeriods(certiticates, period)
      result.size must be(0)
    }
    "two certificate valid half of the period" in {
      val certiticates = Seq(
        new LocationCertificate(id = "Test1", inspectionDate = 1000, validTime = 2000, components = List()),
        new LocationCertificate(id = "Test1", inspectionDate = 2000, validTime = 3000, components = List()))
      val period = new ExclusionPeriod(from = 1000, to = 3000)
      val result = MeasurementUnit.createExclusionPeriods(certiticates, period)
      result.size must be(0)
    }
    "two certificate valid half of the period with a part not valid" in {
      val certiticates = Seq(
        new LocationCertificate(id = "Test1", inspectionDate = 1000, validTime = 1500, components = List()),
        new LocationCertificate(id = "Test1", inspectionDate = 2000, validTime = 3000, components = List()))
      val period = new ExclusionPeriod(from = 1000, to = 3000)
      val result = MeasurementUnit.createExclusionPeriods(certiticates, period)
      result.size must be(1)
      result.head.from must be(1500)
      result.head.to must be(2000)
    }
  }

  def makeViolation(systemId: String, id: String, entryTime: Long, serialNr: String, corridorId: Int = 1): Violation = {
    val vvr = makeVVR(systemId, id, entryTime, entryTime + 5.minutes.toMillis, serialNr)
    val corridorData = violations.CorridorData(corridorId = corridorId,
      entryGantryId = "E1",
      exitGantryId = "X1",
      entryLocationName = "",
      exitLocationName = "",
      length = 8000,
      roadCode = 4,
      radarCode = 4,
      compressionFactor = 20,
      nrDaysKeepViolations = 7,
      dynaMaxScanInterval = 60000,
      pardonTimeEsa = 3,
      locationCode = "0",
      locationLine1 = "",
      service = SectionControl())
    Violation(vvr, None, corridorData, null, null, "", "")
  }

  def makeVVR(systemId: String,
              id: String,
              entryTime: Long,
              exitTime: Long,
              serialNr: String): VehicleViolationRecord = {
    val countryCode = Some("NL")
    val measuredSpeed = 113
    val vehicleClass = Some(VehicleCode.PA)
    val licensePlate = Some("AB12CD")
    val processingIndicator = ProcessingIndicator.Automatic
    val enforcedSpeed = 100
    val reason = IndicatorReason()

    VehicleViolationRecord(id,
      VehicleClassifiedRecord(
        id,
        VehicleSpeedRecord(id,
          VehicleMetadata(Lane("1",
            "lane1",
            "gantry1",
            systemId,
            0.0f,
            0.0f),
            id,
            entryTime,
            Some(new Date(entryTime).toString),
            licensePlate.map(lp ⇒ ValueWithConfidence(lp, 42)),
            country = countryCode.map(cc ⇒ ValueWithConfidence(cc, 42)),
            images = Seq(),
            NMICertificate = "",
            applChecksum = "",
            serialNr = serialNr),
          Some(VehicleMetadata(Lane("1",
            "lane1",
            "gantry2",
            systemId,
            0.0f,
            0.0f),
            id,
            exitTime,
            Some(new Date(exitTime).toString),
            licensePlate.map(lp ⇒ ValueWithConfidence(lp, 42)),
            country = countryCode.map(cc ⇒ ValueWithConfidence(cc, 42)),
            images = Seq(),
            NMICertificate = "",
            applChecksum = "",
            serialNr = serialNr)),
          2002, measuredSpeed, "SHA-1-foo"),
        vehicleClass,
        processingIndicator,
        false,
        false,
        None,
        false,
        reason,
        None,
        None),
      enforcedSpeed,
      None,
      recognizeResults = List())
  }
}