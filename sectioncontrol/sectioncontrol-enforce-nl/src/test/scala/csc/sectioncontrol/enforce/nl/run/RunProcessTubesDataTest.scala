/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.nl.run

import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import csc.config.Path
import akka.util.duration._
import csc.sectioncontrol.storage._
import akka.testkit.TestProbe
import java.io.{ File, FileInputStream, FileOutputStream }
import java.text.SimpleDateFormat
import java.util.{ Date, Calendar }
import csc.sectioncontrol.enforce.speedlog.{ GeneratedExcludeEvents, GenerateExcludeEvents }
import csc.sectioncontrol.enforce.EnforceBoot
import csc.akkautils._
import akka.actor.{ ActorRef, Props, Actor, ActorSystem }
import csc.sectioncontrol.enforce.nl.CuratorHelper
import csc.curator.CuratorTestServer
import csc.curator.utils.Curator
import csc.sectioncontrol.tubes.TubesSetting
import csc.sectioncontrol.messages.EsaProviderType

class RunProcessTubesDataTest extends WordSpec with MustMatchers with CuratorTestServer
  with BeforeAndAfterAll {

  override def beforeEach() {
    super.beforeEach()
    curator = CuratorHelper.create(this, clientScope)
    createConfig
  }

  override def afterEach() {
    curator.curator.foreach(c ⇒ c.close())
  }

  override protected def beforeAll() {
    super.beforeAll()
  }

  override protected def afterAll() {
    testSystem.shutdown()
    super.afterAll()
  }

  "RunProcessTubesDataTest" must {
    "fail with 'JobFailed' when there's no job config present" in {
      val probeRunner = TestProbe()
      val probeStatus = TestProbe()
      runProcessTubesActor ! new Run(
        zkJobPath = "/test",
        runner = probeRunner.ref,
        runToken = CreateRunToken.instance(
          now.getTimeInMillis,
          now.getTimeInMillis,
          "CET"),
        actionDate = yesterday.getTimeInMillis,
        manageEnforceStatus = probeStatus.ref)
      probeRunner.expectMsg(JobFailed)
    }
    "fail with 'JobFailed(true)' when no files are present" in {
      val probeRunner = TestProbe()
      val probeStatus = TestProbe()
      runProcessTubesActor ! new Run(
        zkJobPath = jobPath,
        runner = probeRunner.ref,
        runToken = CreateRunToken.instance(
          now.getTimeInMillis,
          now.getTimeInMillis,
          "CET"),
        actionDate = yesterday.getTimeInMillis,
        manageEnforceStatus = probeStatus.ref)
      probeRunner.expectMsg(20.seconds, JobFailed(retry = true))
    }

    "fail when files are present but too new" in {
      createFile
      val probeRunner = TestProbe()
      val probeStatus = TestProbe()
      val runMsg = new Run(
        zkJobPath = jobPath,
        runner = probeRunner.ref,
        runToken = CreateRunToken.instance(System.currentTimeMillis(),
          System.currentTimeMillis(), "CET"),
        actionDate = yesterday.getTimeInMillis,
        manageEnforceStatus = probeStatus.ref)
      runProcessTubesActor ! runMsg
      probeRunner.expectMsg(5.seconds, JobFailed(true))
      deleteFile
    }

    "succeed when files are present" in {
      createFile
      val probeRunner = TestProbe()
      val probeStatus = TestProbe()
      val runMsg = new Run(
        zkJobPath = jobPath,
        runner = probeRunner.ref,
        runToken = CreateRunToken.instance(System.currentTimeMillis() + 65000,
          System.currentTimeMillis() + 65000, "CET"),
        actionDate = yesterday.getTimeInMillis,
        manageEnforceStatus = probeStatus.ref)
      runProcessTubesActor ! runMsg
      Thread.sleep(25.seconds.toMillis)
      probeRunner.expectMsg(30.seconds, JobComplete)
      probeStatus.expectMsg(30.seconds,
        ProcessingMtmCompleted(runMsg.actionDate, runMsg.runToken.startTime))
      deleteFile
    }
  }

  val systemId = "N62-test"
  val corridorId = "cor1"
  val sectionId = "1"

  implicit val testSystem = ActorSystem(systemId)
  private var curator: Curator = _
  val jobPath = Paths.Systems.getSystemJobsPath(systemId) + "/processMtMDataJob"
  val jobConfigPath = jobPath + "/config"

  /*
  NB: Make sure the file test1.log exists or running the test will result in a
      NullPointer exception without a stacktrace.
   */
  val inputFile = new File(getClass.getClassLoader.getResource("test1.log").getPath)
  val fileDeliveryPath = inputFile.getParent
  val filenamePattern = "'TC-'yyyy-MM-dd'.log'"

  val now = Calendar.getInstance()
  val yesterday = {
    val tmp = Calendar.getInstance()
    tmp.setTime(new Date(now.getTimeInMillis - 1.day.toMillis))
    tmp
  }

  val dateFormat = new SimpleDateFormat(filenamePattern)
  val outputFile = new File(fileDeliveryPath, dateFormat.format(yesterday.getTime))

  lazy val manageEnforceStatus = TestProbe()
  lazy val storeMtMData = TestProbe()

  val speedLog = testSystem.actorOf(Props(new SpeedlogDummy), EnforceBoot.SpeedLogName)
  val excludeLog = testSystem.actorOf(Props(new ExcludelogDummy), EnforceBoot.ExcludeLogName)

  def createFile {
    new FileOutputStream(outputFile) getChannel () transferFrom (
      new FileInputStream(inputFile) getChannel, 0, Long.MaxValue)
  }

  def deleteFile {
    outputFile.delete()
  }

  def getSpeedLog(SystemId: String): ActorRef = {
    speedLog
  }

  def getExcludeLog(SystemId: String): ActorRef = {
    testSystem.deadLetters //isn't really used only to create a message
  }

  def runProcessTubesActor = testSystem.actorOf(Props(new RunProcessTubesData(
    systemId = systemId,
    curator = curator,
    getSpeedLog = getSpeedLog,
    getExcludeLog = getExcludeLog,
    storeMtmData = storeMtMData.ref,
    timeZoneStr = "CET")))

  def createConfig {
    curator.put(Paths.Systems.getConfigPath(systemId), ZkSystem(
      id = systemId,
      name = "Test",
      title = "Test",
      mtmRouteId = "",
      violationPrefixId = "",
      location = ZkSystemLocation(
        description = "",
        region = "",
        viewingDirection = Some(""),
        roadNumber = "",
        roadPart = "",
        systemLocation = ""),
      serialNumber = SerialNumber(""),
      orderNumber = 0,
      maxSpeed = 0,
      compressionFactor = 0,
      retentionTimes = ZkSystemRetentionTimes(
        nrDaysKeepTrafficData = 0,
        nrDaysKeepViolations = 0,
        nrDaysRemindCaseFiles = 0),
      pardonTimes = ZkSystemPardonTimes(pardonTimeEsa_secs = 0, pardonTimeTechnical_secs = 0),
      esaProviderType = EsaProviderType.Dynamax))

    //create 1 corridor
    curator.put(Paths.Corridors.getConfigPath(systemId, corridorId), new ZkCorridor(
      id = corridorId,
      name = "",
      roadType = 0,
      isInsideUrbanArea = false,
      dynamaxMqId = "100",
      approvedSpeeds = Seq(),

      serviceType = ServiceType.SectionControl,

      pshtm = new ZkPSHTMIdentification(useYear = false,
        useMonth = false,
        useDay = false,
        useReportingOfficerId = false,
        useNumberOfTheDay = false,
        useLocationCode = false,
        useHHMCode = false,
        useTypeViolation = false,
        useFixedCharacter = false,
        fixedCharacter = ""),
      info = new ZkCorridorInfo(corridorId = 100,
        locationCode = "0",
        reportId = "TC UT A2 L-1 (100)",
        reportHtId = "a0020011",
        reportPerformance = true,
        locationLine1 = ""),
      startSectionId = sectionId,
      endSectionId = sectionId,
      allSectionIds = Seq(sectionId),
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 402,
      roadCode = 2,
      dutyType = "KA2",
      deploymentCode = "03",
      codeText = ""))

    val cs = List(ZkSchedule("1", ZkIndicationType.None, ZkIndicationType.False, ZkIndicationType.False,
      ZkIndicationType.None, 0, 0, 10, 0, 80, 80, true, None, false),
      ZkSchedule("1", ZkIndicationType.None, ZkIndicationType.False, ZkIndicationType.False,
        ZkIndicationType.None, 10, 0, 24, 0, 100, 100, true, None, false))

    curator.put(Paths.Corridors.getSchedulesPath(systemId, corridorId), cs)

    curator.createEmptyPath(Paths.Corridors.getScheduleHistoryPath(systemId, corridorId))

    curator.put(Paths.Sections.getTubesSettingsPath(systemId, sectionId),
      TubesSetting(corridorId = 1, chartCodes = List("=140+YC016/E9"), messagePatterns = List(".*west.*")))

    curator.put(jobConfigPath, ProcessTubesDataJobConfig(fileDeliveryPath, filenamePattern, 1.minute.toMillis))

  }

  class SpeedlogDummy extends Actor {
    def receive = {
      case msg: GenerateExcludeEvents ⇒ {
        sender ! GeneratedExcludeEvents
      }
    }
  }

  class ExcludelogDummy extends Actor {
    def receive = {
      case msg: GenerateExcludeEvents ⇒ {
        sender ! GeneratedExcludeEvents
      }
    }
  }
}
