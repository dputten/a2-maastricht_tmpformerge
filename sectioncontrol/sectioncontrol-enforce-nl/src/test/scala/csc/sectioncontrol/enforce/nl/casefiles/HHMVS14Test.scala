/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *  
 */

package csc.sectioncontrol.enforce.nl.casefiles

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.enforce.common.nl.services.{ SpeedMobile, RedLight, SpeedFixed, SectionControl }
import csc.sectioncontrol.enforce.common.nl.messages.HeaderInfo
import csc.sectioncontrol.messages.certificates.{ ComponentCertificate, TypeCertificate, MeasurementMethodType }

class HHMVS14Test extends WordSpec with MustMatchers {

  "IRS HHM v1.4 support" must {
    val caseFile = new HHMVS14()
    val service = new SectionControl()
    "indicate that extra info is needed for the output files " in {
      caseFile.showExtraInfo must be(true)
    }
    "name 'stanby' correctly" in {
      caseFile.mtmInfoStandBy must be("stand-by")
    }
    "create the correct case file headers" when {
      val headerInfo = HeaderInfo("systemName", "serialNumber", new MeasurementMethodType(typeDesignation = "XX123",
        category = "A",
        unitSpeed = "km/h",
        unitRedLight = "s",
        unitLength = "m",
        restrictiveConditions = "",
        displayRange = "20-250 km/h",
        temperatureRange = "temperatuurbereik -10 to 50 graden Celsius",
        permissibleError = "toelatbare fout 3%",
        typeCertificate = new TypeCertificate("eg33-cert", 3,
          List(ComponentCertificate("matcher1", "blah1"),
            ComponentCertificate("vr2", "blah2")))))
      "the service is SectionControl" in {
        val header = caseFile.createHeader(new SectionControl(), headerInfo)
        header.size must be(7)
        header(0) must be("# Trajectcontrolesysteem " + headerInfo.systemName)
        header(1) must be("# Categorie A meetmiddel")
        header(2) must be("# Typegoedkeuringsnummer " + headerInfo.methodType.typeCertificate.id)
        header(3) must be("# Serienummer " + headerInfo.serialNumber)
        header(4) must be("# Interfaceversie v1.4")
        header(5) must be("# Snelheden in km/h")
        header(6) must be("# Trajectlengtes in m")
      }
      "the service is SpeedFixed" in {
        val header = caseFile.createHeader(new SpeedFixed(), headerInfo)
        header.size must be(6)
        header(0) must be("# Snelheidscontrole " + headerInfo.systemName)
        header(1) must be("# Categorie A meetmiddel")
        header(2) must be("# Typegoedkeuringsnummer " + headerInfo.methodType.typeCertificate.id)
        header(3) must be("# Serienummer " + headerInfo.serialNumber)
        header(4) must be("# Interfaceversie v1.4")
        header(5) must be("# Snelheden in km/h")
      }
      "the service is RedLight" in {
        val header = caseFile.createHeader(new RedLight(pardonRedTime = 100, minimumYellowTime = 10, factNumber = "1234"), headerInfo)
        header.size must be(6)
        header(0) must be("# Roodlichtcontrole " + headerInfo.systemName)
        header(1) must be("# Categorie A meetmiddel")
        header(2) must be("# Typegoedkeuringsnummer " + headerInfo.methodType.typeCertificate.id)
        header(3) must be("# Serienummer " + headerInfo.serialNumber)
        header(4) must be("# Interfaceversie v1.4")
        header(5) must be("# Rood- en geeltijden in s")
      }
      "the service is something else" in {
        val header = caseFile.createHeader(new SpeedMobile(), headerInfo)
        header.size must be(0)
      }
    }
    "create stats header must be according spec" in {
      val lines = caseFile.createStats(service, 1000, 100, 10).zipWithIndex.toMap.map(_.swap)
      lines.size must be(5)

      /*
        - line out to 25 characters
        - place a '=' character on the 26 character
        - place a tab on the 27 character
        - place a numeric value on the 28 character
       */
      lines(0) must be("Totaal aantal beelden    =\t1110")
      lines(1) must be("Nummerbord onleesbaar    =\t100")
      lines(2) must be("Geldige kentekens        =\t1000")
      lines(3) must be("Automatisch herk. zaken  =\t1000")
      lines(4) must be("Mobiel verwerkte zaken   =\t10")
    }
  }
}
