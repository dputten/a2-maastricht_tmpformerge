/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.nl.imagecheck

import csc.checkimage.{ ImageCheckResult, RecognitionResult, ViolationCheckResult }
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.messages.ValueWithConfidence

class ViolationCheckResultTest extends WordSpec with MustMatchers {

  val contractCountries = Seq("NL", "DE", "BE", "CH", "FR")
  val minLowestLicConfLevel = 50
  val minLicConfLevelRecognizer2 = 70
  val autoThresholdLevel = 55
  val mobiThresholdLevel = 12

  "ViolationCheckResult" must {
    "summerize error when both are OK" in {
      val entry = new ImageCheckResult()
      val exit = new ImageCheckResult()
      val sum = new ViolationCheckResult(Some(entry), Some(exit)).summaryCheck
      sum.getErrors() must be(0)
    }
    "summerize error when only entry has an error" in {
      val entry = new ImageCheckResult(4)
      val exit = new ImageCheckResult()
      val sum = new ViolationCheckResult(Some(entry), Some(exit)).summaryCheck
      sum.getErrors() must be(4)
    }
    "summerize error when only exit has an error" in {
      val entry = new ImageCheckResult(0)
      val exit = new ImageCheckResult(2)
      val sum = new ViolationCheckResult(Some(entry), Some(exit)).summaryCheck
      sum.getErrors() must be(2)
    }
    "summerize error when both has an different error" in {
      val entry = new ImageCheckResult(4)
      val exit = new ImageCheckResult(2)
      val sum = new ViolationCheckResult(Some(entry), Some(exit)).summaryCheck
      sum.getErrors() must be(6)
    }
    "summerize error when both has the same error" in {
      val entry = new ImageCheckResult(2)
      val exit = new ImageCheckResult(2)
      val sum = new ViolationCheckResult(Some(entry), Some(exit)).summaryCheck
      sum.getErrors() must be(2)
    }
    "copy error when no exit" in {
      val entry = new ImageCheckResult(2)
      val sum = new ViolationCheckResult(Some(entry), None).summaryCheck
      sum.getErrors() must be(2)
    }
    "create error when no entry" in {
      val exit = new ImageCheckResult()
      val sum = new ViolationCheckResult(None, Some(exit)).summaryCheck
      sum.getErrors() must be(ImageCheckResult.errorCheck)
    }
    "check licenses of both results when the same" in {
      val entry = new ImageCheckResult(0)
      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("ABCD12", 50)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 50)),
        errorMsg = None))
      val exit = new ImageCheckResult(0)
      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("ABCD12", 40)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 40)),
        errorMsg = None))
      val sum = new ViolationCheckResult(Some(entry), Some(exit)).summaryCheck
      sum.getErrors() must be(0)
    }
    "check licenses of both results when different" in {
      val entry = new ImageCheckResult(0)
      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("ABCD13", 50)),
        rawLicense = Some("AB-CD-13"),
        country = Some(new ValueWithConfidence[String]("NL", 50)),
        errorMsg = None))
      val exit = new ImageCheckResult(0)
      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("ABCD12", 40)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 40)),
        errorMsg = None))
      val sum = new ViolationCheckResult(Some(entry), Some(exit)).summaryCheck
      sum.getErrors() must be(ImageCheckResult.licenseCheck)
    }
    "check licenses of without exit" in {
      val entry = new ImageCheckResult(0)
      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("ABCD12", 50)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 50)),
        errorMsg = None))
      val sum = new ViolationCheckResult(Some(entry), None).summaryCheck
      sum.getErrors() must be(0)
    }
    "check country of both results when different" in {
      val entry = new ImageCheckResult(0)
      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("ABCD12", 50)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 50)),
        errorMsg = None))
      val exit = new ImageCheckResult(0)
      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("ABCD12", 40)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("BE", 40)),
        errorMsg = None))
      val sum = new ViolationCheckResult(Some(entry), Some(exit)).summaryCheck
      sum.getErrors() must be(ImageCheckResult.countryCheck)
    }
    "check country without exit" in {
      val entry = new ImageCheckResult(0)
      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("ABCD12", 50)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 50)),
        errorMsg = None))
      val sum = new ViolationCheckResult(Some(entry), None).summaryCheck
      sum.getErrors() must be(0)
    }
    "check DE format when correct" in {
      val entry = new ImageCheckResult(0)
      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("ABCD12", 50)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("DE", 50)),
        errorMsg = None))
      val exit = new ImageCheckResult(0)
      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("ABCD12", 40)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("DE", 40)),
        errorMsg = None))
      val sum = new ViolationCheckResult(Some(entry), Some(exit)).summaryCheck
      sum.getErrors() must be(0)
    }

    "check DE format when failed" in {
      val entry = new ImageCheckResult(0)
      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("1", 50)),
        rawLicense = Some("1"),
        country = Some(new ValueWithConfidence[String]("DE", 50)),
        errorMsg = None))
      val exit = new ImageCheckResult(0)
      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("1", 40)),
        rawLicense = Some("1"),
        country = Some(new ValueWithConfidence[String]("DE", 40)),
        errorMsg = None))
      val sum = new ViolationCheckResult(Some(entry), Some(exit)).summaryCheck
      sum.getErrors() must be(ImageCheckResult.licenseCheck | ImageCheckResult.licenseFormatCheck)
    }
    "check DE format when no exit" in {
      val entry = new ImageCheckResult(0)
      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("1", 50)),
        rawLicense = Some("1"),
        country = Some(new ValueWithConfidence[String]("DE", 50)),
        errorMsg = None))
      val sum = new ViolationCheckResult(Some(entry), None).summaryCheck
      sum.getErrors() must be(ImageCheckResult.licenseCheck | ImageCheckResult.licenseFormatCheck)
    }

    "check ARH equal Intrada" in {
      val entry = new ImageCheckResult(0)

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 50)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 50)),
        errorMsg = None))

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABCD", 70)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 70)),
        errorMsg = None))

      val exit = new ImageCheckResult(0)

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 40)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 40)),
        errorMsg = None))

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABCD", 40)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 40)),
        errorMsg = None))

      val sum = new ViolationCheckResult(Some(entry), Some(exit)).createSummaryWithIntrada(contractCountries, minLowestLicConfLevel, minLicConfLevelRecognizer2)
      //sum.getErrors() must be(ImageCheckResult.licenseCheck)
      sum.getErrors() must be(0)
    }
    "check ARH equal Intrada (only entry country equal)" in {
      val entry = new ImageCheckResult(0)

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 50)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 50)),
        errorMsg = None))

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABCD", 70)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 70)),
        errorMsg = None))

      val exit = new ImageCheckResult(0)

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 40)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 40)),
        errorMsg = None))

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABCD", 40)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("CH", 40)),
        errorMsg = None))

      val sum = new ViolationCheckResult(Some(entry), Some(exit)).createSummaryWithIntrada(contractCountries, minLowestLicConfLevel, minLicConfLevelRecognizer2)
      //sum.getErrors() must be(ImageCheckResult.licenseCheck)
      sum.getErrors() must be(0)
    }

    "check ARH equal Intrada (only exit country equal)" in {
      val entry = new ImageCheckResult(0)

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 50)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 50)),
        errorMsg = None))

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABCD", 70)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("CH", 70)),
        errorMsg = None))

      val exit = new ImageCheckResult(0)

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 40)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 40)),
        errorMsg = None))

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABCD", 40)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 40)),
        errorMsg = None))

      val sum = new ViolationCheckResult(Some(entry), Some(exit)).createSummaryWithIntrada(contractCountries, minLowestLicConfLevel, minLicConfLevelRecognizer2)
      //sum.getErrors() must be(ImageCheckResult.licenseCheck)
      sum.getErrors() must be(0)
    }

    "check ARH equal Intrada (no country equal)" in {
      val entry = new ImageCheckResult(0)

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 50)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 50)),
        errorMsg = None))

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABCD", 70)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("CH", 70)),
        errorMsg = None))

      val exit = new ImageCheckResult(0)

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 40)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 40)),
        errorMsg = None))

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABCD", 40)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("CH", 40)),
        errorMsg = None))

      val sum = new ViolationCheckResult(Some(entry), Some(exit)).createSummaryWithIntrada(contractCountries, minLowestLicConfLevel, minLicConfLevelRecognizer2)
      //sum.getErrors() must be(ImageCheckResult.licenseCheck)
      sum.getErrors() must be(ImageCheckResult.countryCheck)
    }

    "check ARH not equal Intrada (entry=exit)" in {
      val entry = new ImageCheckResult(0)

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 50)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 50)),
        errorMsg = None))

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABC8", 70)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 70)),
        errorMsg = None))

      val exit = new ImageCheckResult(0)

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 40)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 40)),
        errorMsg = None))

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABC8", 40)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 40)),
        errorMsg = None))

      val sum = new ViolationCheckResult(Some(entry), Some(exit)).createSummaryWithIntrada(contractCountries, minLowestLicConfLevel, minLicConfLevelRecognizer2)
      //sum.getErrors() must be(ImageCheckResult.licenseCheck)
      sum.getErrors() must be(1)
    }

    "check ARH not equal entryIntrada" in {
      val entry = new ImageCheckResult(0)

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 50)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 50)),
        errorMsg = None))

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABC8", 70)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 70)),
        errorMsg = None))

      val exit = new ImageCheckResult(0)

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 40)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 40)),
        errorMsg = None))

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12AB88", 40)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 40)),
        errorMsg = None))

      val sum = new ViolationCheckResult(Some(entry), Some(exit)).createSummaryWithIntrada(contractCountries, minLowestLicConfLevel, minLicConfLevelRecognizer2)
      //sum.getErrors() must be(ImageCheckResult.licenseCheck)
      sum.getErrors() must be(1)
    }

    "check ARH equal entryIntrada but entry ARH.lic.conf <= 50" in {
      val entry = new ImageCheckResult(0)

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 50)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 50)),
        errorMsg = None))

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABCD", 70)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 70)),
        errorMsg = None))

      val exit = new ImageCheckResult(0)

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 51)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 40)),
        errorMsg = None))

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12AB88", 40)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 40)),
        errorMsg = None))

      val sum = new ViolationCheckResult(Some(entry), Some(exit)).createSummaryWithIntrada(contractCountries, minLowestLicConfLevel, minLicConfLevelRecognizer2)
      //sum.getErrors() must be(ImageCheckResult.licenseCheck)
      sum.getErrors() must be(1)
    }

    "check ARH equal entryIntrada but exit ARH.lic.conf <= 50" in {
      val entry = new ImageCheckResult(0)

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 70)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 50)),
        errorMsg = None))

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABCD", 70)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 70)),
        errorMsg = None))

      val exit = new ImageCheckResult(0)

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 50)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 40)),
        errorMsg = None))

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12AB88", 40)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 40)),
        errorMsg = None))

      val sum = new ViolationCheckResult(Some(entry), Some(exit)).createSummaryWithIntrada(contractCountries, minLowestLicConfLevel, minLicConfLevelRecognizer2)
      //sum.getErrors() must be(ImageCheckResult.licenseCheck)
      sum.getErrors() must be(1)
    }

    "check ARH equal entryIntrada but countries not" in {
      val entry = new ImageCheckResult(0)

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 70)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 50)),
        errorMsg = None))

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABCD", 70)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("DE", 70)),
        errorMsg = None))

      val exit = new ImageCheckResult(0)

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 80)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 40)),
        errorMsg = None))

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12AB88", 40)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("DE", 40)),
        errorMsg = None))

      val sum = new ViolationCheckResult(Some(entry), Some(exit)).createSummaryWithIntrada(contractCountries, minLowestLicConfLevel, minLicConfLevelRecognizer2)

      sum.getErrors() must be(ImageCheckResult.countryCheck)
    }

    "check ARH, different IntradaLicense with conf > 70" in {
      val entry = new ImageCheckResult(0)

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 40)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 50)),
        errorMsg = None))

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABC8", 71)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 71)),
        errorMsg = None))

      val exit = new ImageCheckResult(0)

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 40)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 40)),
        errorMsg = None))

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABCD", 80)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 90)),
        errorMsg = None))

      val sum = new ViolationCheckResult(Some(entry), Some(exit)).createSummaryWithIntrada(contractCountries, minLowestLicConfLevel, minLicConfLevelRecognizer2)
      //sum.getErrors() must be(ImageCheckResult.licenseCheck)
      sum.getErrors() must be(ImageCheckResult.errorMatchingCheck)
    }

    "check ARH, exit IntradaLicense with conf > 70" in {
      val entry = new ImageCheckResult(0)

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 80)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 50)),
        errorMsg = None))

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABC8", 11)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 11)),
        errorMsg = None))

      val exit = new ImageCheckResult(0)

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 90)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 40)),
        errorMsg = None))

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABCD", 80)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 90)),
        errorMsg = None))

      val sum = new ViolationCheckResult(Some(entry), Some(exit)).createSummaryWithIntrada(contractCountries, minLowestLicConfLevel, minLicConfLevelRecognizer2)
      //sum.getErrors() must be(ImageCheckResult.licenseCheck)
      sum.getErrors() must be(0)
    }

    "check ARH,IntradaLicense conf < 70, contract country" in {
      val entry = new ImageCheckResult(0)

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 80)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 50)),
        errorMsg = None))

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABC8", 11)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 11)),
        errorMsg = None))

      val exit = new ImageCheckResult(0)

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 90)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 40)),
        errorMsg = None))

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABC9", 12)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 12)),
        errorMsg = None))

      val sum = new ViolationCheckResult(Some(entry), Some(exit)).createSummaryWithIntrada(contractCountries, minLowestLicConfLevel, minLicConfLevelRecognizer2)
      //sum.getErrors() must be(ImageCheckResult.licenseCheck)
      sum.getErrors() must be(1)
    }
    "check ARH,IntradaLicense conf < 70, Mobi country" in {
      val entry = new ImageCheckResult(0)

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 80)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("ES", 50)),
        errorMsg = None))

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABC8", 11)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 11)),
        errorMsg = None))

      val exit = new ImageCheckResult(0)

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 90)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("FR", 40)),
        errorMsg = None))

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABC9", 12)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 12)),
        errorMsg = None))

      val sum = new ViolationCheckResult(Some(entry), Some(exit)).createSummaryWithIntrada(contractCountries, minLowestLicConfLevel, minLicConfLevelRecognizer2)
      //sum.getErrors() must be(ImageCheckResult.licenseCheck)
      sum.getErrors() must be(0)
    }

    "check ARH,IntradaLicense conf < 70, Entry Intrada not above conf level 50" in {
      val entry = new ImageCheckResult(0)

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 50)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("FR", 50)),
        errorMsg = None))

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABCD", 51)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 11)),
        errorMsg = None))

      val exit = new ImageCheckResult(0)

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 40)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("FR", 40)),
        errorMsg = None))

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABC9", 52)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 12)),
        errorMsg = None))

      val sum = new ViolationCheckResult(Some(entry), Some(exit)).createSummaryWithIntrada(contractCountries, minLowestLicConfLevel, minLicConfLevelRecognizer2)
      //sum.getErrors() must be(ImageCheckResult.licenseCheck)
      sum.getErrors() must be(1)
    }

    "check ARH,IntradaLicense conf < 70, Exit Intrada not above conf level 50" in {
      val entry = new ImageCheckResult(0)

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 50)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("FR", 50)),
        errorMsg = None))

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABC9", 51)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 11)),
        errorMsg = None))

      val exit = new ImageCheckResult(0)

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD", 40)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("FR", 40)),
        errorMsg = None))

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABCD", 42)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 12)),
        errorMsg = None))

      val sum = new ViolationCheckResult(Some(entry), Some(exit)).createSummaryWithIntrada(contractCountries, minLowestLicConfLevel, minLicConfLevelRecognizer2)
      //sum.getErrors() must be(ImageCheckResult.licenseCheck)
      sum.getErrors() must be(1)
    }

    "check ARH DE format when no exit" in {
      val entry = new ImageCheckResult(0)
      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD789012", 80)),
        rawLicense = Some("AB-CD-12789012"),
        country = Some(new ValueWithConfidence[String]("DE", 50)),
        errorMsg = None))

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABCD789012", 81)),
        rawLicense = Some("AB-CD-12789012"),
        country = Some(new ValueWithConfidence[String]("DE", 11)),
        errorMsg = None))

      val exit = new ImageCheckResult(0)

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("12ABCD789012", 80)),
        rawLicense = Some("AB-CD-12789012"),
        country = Some(new ValueWithConfidence[String]("DE", 40)),
        errorMsg = None))

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABCD789012", 82)),
        rawLicense = Some("ABCD12789012"),
        country = Some(new ValueWithConfidence[String]("DE", 12)),
        errorMsg = None))
      val sum = new ViolationCheckResult(Some(entry), Some(exit)).createSummaryWithIntrada(contractCountries, minLowestLicConfLevel, minLicConfLevelRecognizer2)
      sum.getErrors() must be(ImageCheckResult.licenseCheck)
    }

    "check ARH wrong read Intrada good read" in {
      val entry = new ImageCheckResult(0)
      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("81WV22", 10)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 50)),
        errorMsg = None))

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("81BVZZ", 81)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 81)),
        errorMsg = None))

      val exit = new ImageCheckResult(0)

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("81WV22", 8)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 40)),
        errorMsg = None))

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("81BVZZ", 92)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 92)),
        errorMsg = None))
      val sum = new ViolationCheckResult(Some(entry), Some(exit)).createSummaryWithIntrada(contractCountries, minLowestLicConfLevel, minLicConfLevelRecognizer2)
      sum.getErrors() must be(ImageCheckResult.licenseCheck)
    }

    "check Intrada2 good read" in {
      val entry = new ImageCheckResult(0)
      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH_ENTRY,
        license = Some(new ValueWithConfidence[String]("81WV22", 10)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 50)),
        errorMsg = None))

      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA2,
        license = Some(new ValueWithConfidence[String]("81WV22", 81)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 81)),
        errorMsg = None))

      val exit = new ImageCheckResult(0)

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH_EXIT,
        license = Some(new ValueWithConfidence[String]("81WV22", 8)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 40)),
        errorMsg = None))

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA2,
        license = Some(new ValueWithConfidence[String]("81WV22", 81)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 81)),
        errorMsg = None))

      val sum = new ViolationCheckResult(Some(entry), Some(exit)).createSummaryWithIntrada2(contractCountries, autoThresholdLevel, mobiThresholdLevel)

      sum.getErrors() must be(0)
    }

    "check Intrada2 mixup" in {
      val entry = new ImageCheckResult(0)
      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH_ENTRY,
        license = Some(new ValueWithConfidence[String]("81WV22", 10)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 50)),
        errorMsg = None))

      /*
        INTRADA2 result is same for entry as exit
        Mixup means een conf level of 1
        */
      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA2,
        license = Some(new ValueWithConfidence[String]("81WV22", 1)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 1)),
        errorMsg = None))

      val exit = new ImageCheckResult(0)

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH_EXIT,
        license = Some(new ValueWithConfidence[String]("81WV22", 8)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 40)),
        errorMsg = None))

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA2,
        license = Some(new ValueWithConfidence[String]("81WV22", 1)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 1)),
        errorMsg = None))

      val sum = new ViolationCheckResult(Some(entry), Some(exit)).createSummaryWithIntrada2(contractCountries, autoThresholdLevel, mobiThresholdLevel)

      sum.getErrors() must be(ImageCheckResult.errorMatchingCheck)
    }

    "check Intrada2 ConflevelBelow10not1" in {
      val entry = new ImageCheckResult(0)
      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH_ENTRY,
        license = Some(new ValueWithConfidence[String]("81WV22", 10)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 50)),
        errorMsg = None))

      /*
        INTRADA2 result is same for entry as exit
        Mixup means een conf level of 1
        */
      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA2,
        license = Some(new ValueWithConfidence[String]("81WV22", 7)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 7)),
        errorMsg = None))

      val exit = new ImageCheckResult(0)

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH_EXIT,
        license = Some(new ValueWithConfidence[String]("81WV22", 8)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 40)),
        errorMsg = None))

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA2,
        license = Some(new ValueWithConfidence[String]("81WV22", 7)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 7)),
        errorMsg = None))

      val sum = new ViolationCheckResult(Some(entry), Some(exit)).createSummaryWithIntrada2(contractCountries, autoThresholdLevel, mobiThresholdLevel)

      sum.getErrors() must be(ImageCheckResult.errorMatchingCheck)
    }

    "check Intrada2 Different License as ARH" in {
      val entry = new ImageCheckResult(0)
      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH_ENTRY,
        license = Some(new ValueWithConfidence[String]("81WV22", 10)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 50)),
        errorMsg = None))

      // Intrada2: Differnet license, low confidence
      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA2,
        license = Some(new ValueWithConfidence[String]("81WV23", 11)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 11)),
        errorMsg = None))

      val exit = new ImageCheckResult(0)

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH_EXIT,
        license = Some(new ValueWithConfidence[String]("81WV22", 8)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 40)),
        errorMsg = None))

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA2,
        license = Some(new ValueWithConfidence[String]("81WV23", 11)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 11)),
        errorMsg = None))

      val sum = new ViolationCheckResult(Some(entry), Some(exit)).createSummaryWithIntrada2(contractCountries, autoThresholdLevel, mobiThresholdLevel)

      sum.getErrors() must be(ImageCheckResult.countryCheck | ImageCheckResult.licenseCheck)
    }

    "check Intrada2 To low confidence" in {
      val entry = new ImageCheckResult(0)
      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH_ENTRY,
        license = Some(new ValueWithConfidence[String]("81WV22", 10)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 50)),
        errorMsg = None))

      // Intrada2: Differnet license, low confidence
      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA2,
        license = Some(new ValueWithConfidence[String]("81WV22", 49)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 49)),
        errorMsg = None))

      val exit = new ImageCheckResult(0)

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH_EXIT,
        license = Some(new ValueWithConfidence[String]("81WV22", 8)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 40)),
        errorMsg = None))

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA2,
        license = Some(new ValueWithConfidence[String]("81WV22", 49)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 49)),
        errorMsg = None))

      val sum = new ViolationCheckResult(Some(entry), Some(exit)).createSummaryWithIntrada2(contractCountries, autoThresholdLevel, mobiThresholdLevel)

      sum.getErrors() must be(ImageCheckResult.countryCheck | ImageCheckResult.licenseCheck)
    }

    "check Intrada2 Good License wrong ARH Country still auto" in {
      val entry = new ImageCheckResult(0)
      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH_ENTRY,
        license = Some(new ValueWithConfidence[String]("81WV22", 10)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 50)),
        errorMsg = None))

      // Intrada2: Differnet license, low confidence
      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA2,
        license = Some(new ValueWithConfidence[String]("81WV22", 70)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 70)),
        errorMsg = None))

      val exit = new ImageCheckResult(0)

      /*
       ARH license results are the same, but the country code not.
       The country code with the highest confidence level is not the same country code as Intrada returns
       Because 'DE' is also a contract country no change to Auto is needed.
       */
      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH_EXIT,
        license = Some(new ValueWithConfidence[String]("81WV22", 8)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("DE", 98)),
        errorMsg = None))

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA2,
        license = Some(new ValueWithConfidence[String]("81WV22", 70)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 70)),
        errorMsg = None))

      val sum = new ViolationCheckResult(Some(entry), Some(exit)).createSummaryWithIntrada2(contractCountries, autoThresholdLevel, mobiThresholdLevel)

      sum.getErrors() must be(ImageCheckResult.changeCountry)
    }

    "check Intrada2 Good License wrong ARH Country still mobi" in {
      val entry = new ImageCheckResult(0)
      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH_ENTRY,
        license = Some(new ValueWithConfidence[String]("81WV22", 10)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("ES", 50)),
        errorMsg = None))

      // Intrada2: Differnet license, low confidence
      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA2,
        license = Some(new ValueWithConfidence[String]("81WV22", 70)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("ES", 70)),
        errorMsg = None))

      val exit = new ImageCheckResult(0)

      /*
       ARH license results are the same, but the country code not.
       The country code with the highest confidence level is not the same country code as Intrada returns
       Because 'DE' is also a contract country no change to Auto is needed.
       */
      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH_EXIT,
        license = Some(new ValueWithConfidence[String]("81WV22", 8)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("IT", 98)),
        errorMsg = None))

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA2,
        license = Some(new ValueWithConfidence[String]("81WV22", 70)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("ES", 70)),
        errorMsg = None))

      val sum = new ViolationCheckResult(Some(entry), Some(exit)).createSummaryWithIntrada2(contractCountries, autoThresholdLevel, mobiThresholdLevel)

      sum.getErrors() must be(ImageCheckResult.changeCountry)
    }

    "check Intrada2 Good License wrong ARH Country Change to Auto" in {
      val entry = new ImageCheckResult(0)
      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH_ENTRY,
        license = Some(new ValueWithConfidence[String]("81WV22", 10)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 50)),
        errorMsg = None))

      // Intrada2: Differnet license, low confidence
      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA2,
        license = Some(new ValueWithConfidence[String]("81WV22", 70)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 70)),
        errorMsg = None))

      val exit = new ImageCheckResult(0)

      /*
       ARH license results are the same, but the country code not.
       The country code with the highest confidence level is not the same country code as Intrada returns
       Because 'ES' is not a contract country and NL is, a change to Auto is needed.
       */
      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH_EXIT,
        license = Some(new ValueWithConfidence[String]("81WV22", 8)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("ES", 98)),
        errorMsg = None))

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA2,
        license = Some(new ValueWithConfidence[String]("81WV22", 70)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 70)),
        errorMsg = None))

      val sum = new ViolationCheckResult(Some(entry), Some(exit)).createSummaryWithIntrada2(contractCountries, autoThresholdLevel, mobiThresholdLevel)

      sum.getErrors() must be(ImageCheckResult.changeCountry | ImageCheckResult.changeToAuto)
    }

    "check Intrada2 Good License wrong ARH Country Change to Mobi" in {
      val entry = new ImageCheckResult(0)
      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH_ENTRY,
        license = Some(new ValueWithConfidence[String]("81WV22", 10)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("ES", 50)),
        errorMsg = None))

      // Intrada2: Differnet license, low confidence
      entry.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA2,
        license = Some(new ValueWithConfidence[String]("81WV22", 70)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("ES", 70)),
        errorMsg = None))

      val exit = new ImageCheckResult(0)

      /*
       ARH license results are the same, but the country code not.
       The country code with the highest confidence level is not the same country code as Intrada returns
       Because 'FR' is a contract country and ES is not, a change to Mobi is needed.
       */
      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.ARH_EXIT,
        license = Some(new ValueWithConfidence[String]("81WV22", 8)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("FR", 98)),
        errorMsg = None))

      exit.addRecognitionResult(new RecognitionResult(recognizerId = RecognitionResult.INTRADA2,
        license = Some(new ValueWithConfidence[String]("81WV22", 70)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("ES", 70)),
        errorMsg = None))

      val sum = new ViolationCheckResult(Some(entry), Some(exit)).createSummaryWithIntrada2(contractCountries, autoThresholdLevel, mobiThresholdLevel)

      sum.getErrors() must be(ImageCheckResult.changeCountry | ImageCheckResult.changeToMobi)
    }
  }
}