/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.nl.pardon

import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers
import csc.curator.CuratorTestServer
import csc.curator.utils.Curator
import akka.util.duration._
import java.util.TimeZone
import csc.sectioncontrol.enforce.nl.register.MockViolation
import csc.sectioncontrol.enforce.common.nl.violations.Violation
import csc.config.Path
import csc.sectioncontrol.storage._
import csc.sectioncontrol.enforce.nl.{ CuratorHelper, DriftEvent, RegistrationTimeUnreliableConfig }
import csc.sectioncontrol.storagelayer.alerts.AlertService
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.messages.{ EsaProviderType, VehicleCode }
import net.liftweb.json.DefaultFormats
import csc.sectioncontrol.enforce.Enumerations.RoadType
import akka.actor.Props
import scala.Some
import csc.sectioncontrol.enforce.nl.RegistrationTimeUnreliableConfig
import csc.sectioncontrol.enforce.nl.DriftEvent
import csc.sectioncontrol.storage.ZkAlert
import csc.sectioncontrol.storage.ZkAlertHistory
import csc.sectioncontrol.enforce.common.nl.violations.Violation
import csc.sectioncontrol.storagelayer.corridor.CorridorService

class PardonRegistrationTimeUnreliableCuratorTest extends WordSpec with MustMatchers with BeforeAndAfterAll with CuratorTestServer {
  val timeZone = TimeZone.getTimeZone("Europe/Amsterdam")
  val startTime = MockViolation.jan25time(0, 0, 0)
  val endTime = startTime + 1.day.toMillis
  val systemId = "N62"
  val corridorId = "S1"
  protected var curator: Curator = _

  override def beforeEach() {
    super.beforeEach()
    val formats = DefaultFormats + new EnumerationSerializer(EsaProviderType, RoadType, VehicleCode,
      ConfigType)
    curator = CuratorHelper.create(this, clientScope, formats)
    //zkClient.get.createPersistent("/ctes/systems/eg33", true)
    val system = ZkSystem(
      id = systemId,
      name = "",
      title = "",
      mtmRouteId = "",
      violationPrefixId = "",
      location = ZkSystemLocation(
        description = "",
        region = "",
        viewingDirection = Some(""),
        roadNumber = "",
        roadPart = "",
        systemLocation = ""),
      serialNumber = SerialNumber(""),
      orderNumber = 0,
      maxSpeed = 0,
      compressionFactor = 0,
      retentionTimes = ZkSystemRetentionTimes(
        nrDaysKeepTrafficData = 0,
        nrDaysKeepViolations = 0,
        nrDaysRemindCaseFiles = 0),
      pardonTimes = ZkSystemPardonTimes(pardonTimeEsa_secs = 5 /*5000*/ , pardonTimeTechnical_secs = 5 /*5000*/ ),
      esaProviderType = EsaProviderType.Mtm)
    curator.put(Path("/ctes/systems/eg33/config"), system)

    val corridor = ZkCorridor(id = "S1",
      name = "S1-name",
      roadType = 0,
      serviceType = ServiceType.SectionControl,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = false,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = ZkPSHTMIdentification(useYear = false,
        useMonth = true,
        useDay = false,
        useReportingOfficerId = false,
        useNumberOfTheDay = true,
        useLocationCode = false,
        useHHMCode = false,
        useTypeViolation = false,
        useFixedCharacter = false,
        fixedCharacter = "T"),
      info = ZkCorridorInfo(corridorId = 1,
        locationCode = "2",
        reportId = "",
        reportHtId = "",
        reportPerformance = true,
        locationLine1 = ""),
      startSectionId = "sec1",
      endSectionId = "sec1",
      allSectionIds = Seq("sec1"),
      direction = ZkDirection(directionFrom = "",
        directionTo = ""),
      radarCode = 4,
      roadCode = 0,
      dutyType = "",
      deploymentCode = "",
      codeText = "")
    CorridorService.createCorridor(curator, system.id, corridor)
    val corridor2 = corridor.copy(id = "S2", startSectionId = "sec2",
      endSectionId = "sec2",
      allSectionIds = Seq("sec2"),
      name = "S2-name",
      info = corridor.info.copy(corridorId = 2))
    CorridorService.createCorridor(curator, system.id, corridor2)

  }

  override def afterEach() {
    super.afterEach()
  }

  "pardon" must {
    "not pardon when no alerts are present" in {
      val violations = createViolations()
      val cfg = RegistrationTimeUnreliableConfig(Seq(DriftEvent(20, "JAI-CAMERA-CONNECTION-ERROR")))
      val result = PardonRegistrationTimeUnreliable.pardon(curator, systemId, startTime, endTime, violations, cfg)
      result.notPardonedViolations must be(violations)
      result.originalPardonViolations.size must be(0)
      result.pardoned.size must be(0)
    }
    "not pardon when other alerts are present" in {
      createHistoryAlerts()
      val violations = createViolations()
      val cfg = RegistrationTimeUnreliableConfig(Seq(DriftEvent(20, "JAI-CAMERA-CONNECTION-ERROR")))
      val result = PardonRegistrationTimeUnreliable.pardon(curator, systemId, startTime, endTime, violations, cfg)
      result.notPardonedViolations must be(violations)
      result.originalPardonViolations.size must be(0)
      result.pardoned.size must be(0)
    }
    "pardon when history alerts are present" in {
      createHistoryAlerts()
      val violations = createViolations()
      val cfg = RegistrationTimeUnreliableConfig(Seq(DriftEvent(20, "NTP_STATUS-STA_UNSYNC")))
      val result = PardonRegistrationTimeUnreliable.pardon(curator, systemId, startTime, endTime, violations, cfg)
      result.notPardonedViolations.size must be(1)
      result.originalPardonViolations.size must be(2)
      result.pardoned.size must be(2)
    }
    "pardon when current alerts are present" in {
      createCurrentAlert()
      val violations = createViolations()
      val cfg = RegistrationTimeUnreliableConfig(Seq(DriftEvent(20, "NTP_STATUS-STA_UNSYNC")))
      val result = PardonRegistrationTimeUnreliable.pardon(curator, systemId, startTime, endTime, violations, cfg)
      result.notPardonedViolations.size must be(2)
      result.originalPardonViolations.size must be(1)
      result.pardoned.size must be(1)
    }
    "pardon when current and history alerts are present" in {
      createCurrentAlert()
      createHistoryAlerts()
      val violations = createViolations()
      val cfg = RegistrationTimeUnreliableConfig(Seq(DriftEvent(20, "NTP_STATUS-STA_UNSYNC")))
      val result = PardonRegistrationTimeUnreliable.pardon(curator, systemId, startTime, endTime, violations, cfg)
      result.notPardonedViolations.size must be(0)
      result.originalPardonViolations.size must be(3)
      result.pardoned.size must be(3)
    }
  }

  def createCurrentAlert() {
    val startTime1 = MockViolation.jan25time(12, 0, 0)
    val alert = new ZkAlert(path = "route1-gantry1-1-camera",
      alertType = "NTP_STATUS-STA_UNSYNC",
      message = "NTP_STATUS-STA_UNSYNC",
      timestamp = startTime1,
      configType = ConfigType.Lane,
      reductionFactor = 0.33F)

    AlertService.create(systemId, corridorId, alert, curator)
  }

  def createHistoryAlerts() {
    val startTime1 = MockViolation.jan25time(8, 0, 0)
    val endTime1 = MockViolation.jan25time(9, 0, 0)
    val hist = new ZkAlertHistory(path = "route1-gantry1-1-camera",
      alertType = "NTP_STATUS-STA_UNSYNC",
      message = "NTP_STATUS-STA_UNSYNC",
      startTime = startTime1,
      endTime = endTime1,
      configType = "Lane",
      reductionFactor = 0.33F)
    AlertService.addToHistory(systemId, corridorId, hist, curator)

    val startTime2 = MockViolation.jan25time(10, 0, 0)
    val endTime2 = MockViolation.jan25time(11, 0, 0)
    val hist2 = new ZkAlertHistory(path = "route1-gantry2-1-camera",
      alertType = "NTP_STATUS-STA_UNSYNC",
      message = "NTP_STATUS-STA_UNSYNC",
      startTime = startTime2,
      endTime = endTime2,
      configType = "Lane",
      reductionFactor = 0.33F)
    AlertService.addToHistory(systemId, corridorId, hist2, curator)
  }
  def createViolations(): Seq[Violation] = {
    val violation1 = MockViolation(entryTime = MockViolation.jan25time(8, 30, 0),
      exitTime = MockViolation.jan25time(8, 35, 0),
      licensePlate = Some("DE34FG"), enforcedSpeed = 100)
    val violation2 = MockViolation(entryTime = MockViolation.jan25time(10, 30, 0),
      exitTime = MockViolation.jan25time(10, 35, 0),
      licensePlate = Some("HI56JK"), enforcedSpeed = 100)
    val violation3 = MockViolation(entryTime = MockViolation.jan25time(12, 30, 0),
      exitTime = MockViolation.jan25time(12, 35, 0),
      licensePlate = Some("LM78NO"), enforcedSpeed = 100)
    Seq(violation1, violation2, violation3)
  }
}