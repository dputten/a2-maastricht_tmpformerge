package csc.sectioncontrol.enforce.nl

import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import csc.sectioncontrol.storage.Paths
import csc.config.Path
import csc.sectioncontrol.messages.StatsDuration
import akka.actor._
import akka.util.duration._
import java.util.TimeZone
import csc.curator.CuratorTestServer
import csc.curator.utils.Curator
import akka.testkit.TestProbe
import csc.sectioncontrol.enforce.DayViolationsStatistics
import csc.sectioncontrol.messages.GenerateReport
import csc.sectioncontrol.enforce.PossibleDefectMSI
import csc.sectioncontrol.enforce.MtmSpeedSetting
import csc.sectioncontrol.enforce.MsiBlankEvent
import csc.sectioncontrol.enforce.MtmSpeedSettings
import csc.sectioncontrol.messages.matcher.MatcherCorridorConfig
import csc.sectioncontrol.enforce.ExportViolationsStatistics

class EnforceEventListenerTest extends WordSpec with MustMatchers with BeforeAndAfterAll with CuratorTestServer {
  implicit val testSystem = ActorSystem("EnforceEventListenerTest")
  val timeZone = TimeZone.getTimeZone("Europe/Amsterdam")
  val config = MatcherCorridorConfig("eg33", "1", 1, "g1", "g2", 2500, 200, 3600000)

  protected var curator: Curator = _

  override def beforeEach() {
    super.beforeEach()
    curator = CuratorHelper.create(this, clientScope)
  }

  "ProcessStatistics" must {
    "save violation statistics" in {
      val reporter = TestProbe()
      val testActor = testSystem.actorOf(Props(new EnforceEventListener("eg33", curator, reporter.ref)))

      val stats = StatsDuration("20120401", timeZone)
      val event1 = DayViolationsStatistics("eg33", StatsDuration("20120401", timeZone), Seq(ExportViolationsStatistics(config.id,
        100, 100, 30, 2, 1, 5, 17, 19, 1)))

      val path = Path(Paths.Systems.getRuntimeViolationsPath("eg33", stats)).toString
      curator.exists(path) must be(false)
      testSystem.eventStream.publish(event1)
      reporter.expectMsg(10 seconds, GenerateReport(event1.systemId, event1.stats))
      val stored = curator.get[DayViolationsStatistics](path)
      stored.isDefined must be(true)
      stored.get must be(event1)
      testSystem.stop(testActor)
    }
    "save empty violation statistics" in {
      val reporter = TestProbe()
      val testActor = testSystem.actorOf(Props(new EnforceEventListener("eg33", curator, reporter.ref)))

      val stats = StatsDuration("20120401", timeZone)
      val event1 = DayViolationsStatistics("eg33", StatsDuration("20120401", timeZone), Seq(ExportViolationsStatistics(config.id,
        100, 0, 0, 0, 0, 0, 0, 0, 0)))

      val path = Path(Paths.Systems.getRuntimeViolationsPath("eg33", stats)).toString
      curator.exists(path) must be(false)
      testSystem.eventStream.publish(event1)
      reporter.expectMsg(10 seconds, GenerateReport(event1.systemId, event1.stats))
      val stored = curator.get[DayViolationsStatistics](path)
      stored.isDefined must be(true)
      stored.get must be(event1)
      testSystem.stop(testActor)
    }
    "update violation statistics" in {
      val reporter = TestProbe()
      val testActor = testSystem.actorOf(Props(new EnforceEventListener("eg33", curator, reporter.ref)))

      val stats = StatsDuration("20120401", timeZone)
      val event1 = DayViolationsStatistics("eg33", StatsDuration("20120401", timeZone), Seq(ExportViolationsStatistics(config.id,
        100, 100, 30, 2, 1, 5, 17, 19, 1)))

      val path = Path(Paths.Systems.getRuntimeViolationsPath("eg33", stats)).toString
      curator.exists(path) must be(false)
      testSystem.eventStream.publish(event1)
      reporter.expectMsg(10 seconds, GenerateReport(event1.systemId, event1.stats))
      val stored = curator.get[DayViolationsStatistics](path)
      stored.isDefined must be(true)
      stored.get must be(event1)
      val event2 = DayViolationsStatistics("eg33", StatsDuration("20120401", timeZone), Seq(ExportViolationsStatistics(config.id,
        100, 101, 31, 3, 2, 6, 18, 20, 2)))
      testSystem.eventStream.publish(event2)
      reporter.expectMsg(10 seconds, GenerateReport(event1.systemId, event1.stats))
      val stored2 = curator.get[DayViolationsStatistics](path)
      stored2.isDefined must be(true)
      stored2.get must be(event2)
      testSystem.stop(testActor)
    }
    "not update empty violation statistics" in {
      val reporter = TestProbe()
      val testActor = testSystem.actorOf(Props(new EnforceEventListener("eg33", curator, reporter.ref)))

      val stats = StatsDuration("20120401", timeZone)
      val event1 = DayViolationsStatistics("eg33", StatsDuration("20120401", timeZone), Seq(ExportViolationsStatistics(config.id,
        100, 100, 30, 2, 1, 5, 17, 19, 1)))

      val path = Path(Paths.Systems.getRuntimeViolationsPath("eg33", stats)).toString
      curator.exists(path) must be(false)
      testSystem.eventStream.publish(event1)
      reporter.expectMsg(10 seconds, GenerateReport(event1.systemId, event1.stats))
      val stored = curator.get[DayViolationsStatistics](path)
      stored.isDefined must be(true)
      stored.get must be(event1)
      val event2 = DayViolationsStatistics("eg33", StatsDuration("20120401", timeZone), Seq(ExportViolationsStatistics(config.id,
        100, 0, 0, 0, 0, 0, 0, 0, 0)))
      testSystem.eventStream.publish(event2)
      reporter.expectMsg(10 seconds, GenerateReport(event1.systemId, event1.stats))
      val stored2 = curator.get[DayViolationsStatistics](path)
      stored2.isDefined must be(true)
      stored2.get must be(event1)
      testSystem.stop(testActor)
    }
    "store generated MTM data" in {
      val reporter = TestProbe()
      val testActor = testSystem.actorOf(Props(new EnforceEventListener("eg33", curator, reporter.ref)))

      val data = List(MtmSpeedSetting(10000000L, "A2-loc1", "100"), MtmSpeedSetting(110000000L, "A2-loc1", "onbepaald"))
      val mtm = MtmSpeedSettings(data, StatsDuration("20120520", timeZone))
      val path = Path(Paths.Systems.getRuntimeMtmInfoPath("eg33", StatsDuration("20120520", timeZone))).toString
      curator.exists(path) must be(false)
      testSystem.eventStream.publish(mtm)
      pathExists(path) must be(true)

      val stored = curator.get[MtmSpeedSettings](path)
      stored.isDefined must be(true)
      stored.get must be(mtm)
      testSystem.stop(testActor)
    }

    "store MsiBlankEvent" in {
      val reporter = TestProbe()
      val testActor = testSystem.actorOf(Props(new EnforceEventListener("eg33", curator, reporter.ref)))

      val blankMsiList = List(PossibleDefectMSI(1, "a", "a"), PossibleDefectMSI(1, "b", "b"))
      val msiBlankEvent = MsiBlankEvent(1, blankMsiList, StatsDuration("20120520", timeZone))

      val path = Path(Paths.Systems.getRuntimeMsiBlanksPath("eg33", StatsDuration("20120520", timeZone)))
      curator.exists(path) must be(false)
      testSystem.eventStream.publish(msiBlankEvent)
      waitForPath(path.toString()) must be(true)
      curator.exists(path) must be(true)

      val stored = curator.get[MsiBlankEvent](path)
      stored.isDefined must be(true)
      stored.get must be(msiBlankEvent)
      testSystem.stop(testActor)
    }
  }

  override protected def afterAll() {
    testSystem.shutdown()
    super.afterAll()
  }

  def pathExists(p: String) = waitForPath(p)

  def waitForPath(p: String, retries: Int = 10, sleep: Int = 50): Boolean = {
    require(retries >= 0)
    val found = curator.exists(p)
    (retries > 0, found) match {
      case (_, true)      ⇒ true
      case (false, false) ⇒ false
      case (true, false) ⇒
        Thread.sleep(sleep)
        waitForPath(p, retries - 1, sleep)
    }
  }
}