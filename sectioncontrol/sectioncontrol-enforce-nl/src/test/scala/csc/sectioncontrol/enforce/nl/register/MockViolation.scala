package csc.sectioncontrol.enforce.nl.register

import java.util.{ Calendar, Date, GregorianCalendar }

import akka.util.duration._
import csc.sectioncontrol.enforce.Enumerations.RoadType
import csc.sectioncontrol.enforce.common.nl.services.SectionControl
import csc.sectioncontrol.enforce.common.nl.violations._
import csc.sectioncontrol.enforce.nl.casefiles.HHMVS40
import csc.sectioncontrol.enforce.register.CountryCodes
import csc.sectioncontrol.enforce.violations.GenerateId
import csc.sectioncontrol.messages.MetadataVersion.MetadataVersion
import csc.sectioncontrol.messages._
import csc.sectioncontrol.messages.certificates._

/**
 * Created by carlos on 03.08.15.
 */
object MockViolation extends GenerateId {
  def jan25time(hour: Int, minute: Int, second: Int = 0) =
    new GregorianCalendar(2002, Calendar.JANUARY, 25, hour, minute, second).getTimeInMillis

  val defaultEntryTime = jan25time(17, 57, 16)
  val defaultExitTime = jan25time(17, 58, 5)
  val defaultScheduleStartTime = jan25time(0, 0)
  val defaultScheduleEndTIme = jan25time(24, 0)

  val goodChecksum = "3c316878d972e367881363c03ffb6c67"
  val defaultSystemId = "EG33"
  val defaultCorridorId = 1

  val entryLane = Lane("route1-gantry1-1", "lane1", "gantry1", "route1", 0.0f, 0.0f)
  val exitLane = Lane("route1-gantry2-1", "lane1", "gantry2", "route1", 0.0f, 0.0f)

  def apply(systemId: String = defaultSystemId,
            routeId: String = "A02L",
            mtmRouteId: String = "a002l",
            mtmLocation: String = "A2 L 96.700",
            hardwareSoftwareSeals: collection.immutable.Seq[ComponentCertificate] = List(ComponentCertificate("DEELA", "12-456-67"),
              ComponentCertificate("DEELB", "9092-351")),
            corridorId: Int = defaultCorridorId,
            entryGantry: String = "A2.123",
            exitGantry: String = "A2.127",
            corridorEntryLocation: String = "95.2L",
            corridorExitLocation: String = "96.7L",
            corridorLength: Double = 1536.8,
            fixedSpeedLimit: Int = 100,
            corridorLocationCode: String = "01234",
            corridorLocation1: String = "here",
            corridorLocation2: Option[String] = None,
            drivingDirectionFrom: Option[String] = None,
            drivingDirectionTo: Option[String] = None,
            enforcedSpeed: Int = 100,
            pshTmId: String = "XYZ1234567",
            entryTime: Long = defaultEntryTime,
            exitTime: Long = defaultExitTime,
            statusCode: Char = '0',
            countryCode: Option[String] = Some("NL"),
            measuredSpeed: Int = 113,
            vehicleClass: Option[VehicleCode.Value] = Some(VehicleCode.PA),
            licensePlate: Option[String] = Some("AB12CD"),
            processingIndicator: ProcessingIndicator.Value = ProcessingIndicator.Automatic,
            reportingOfficerCode: String = "AB4321",
            radarCode: Int = 0,
            roadCode: Int = 1,
            roadType: RoadType.Value = RoadType.Highway,
            insideTown: Boolean = false,
            scheduleStartTime: Long = defaultScheduleStartTime,
            scheduleEndTime: Long = defaultScheduleEndTIme,
            dutyType: Option[String] = None,
            deploymentCode: Option[String] = None,
            systemName: String = "",
            checksum: String = goodChecksum, // "3c316878d972e367881363c03ffb6c67",
            reason: IndicatorReason = IndicatorReason(),
            speedIndicator: SpeedIndicator = SpeedIndicator(signIndicator = true, speedIndicatorType = Some(SpeedIndicatorType.A1)),
            dynamicSpeedLimit: Option[Int] = None,
            serialNr: String = "SN1234",
            version: Option[MetadataVersion] = Some(MetadataVersion.withLicensePlateData)): Violation = {

    val id = nextId.toString
    val entry = createVehicleMetadata(entryLane, entryTime, checksum, serialNr, licensePlate, countryCode, version)
    val exit = createVehicleMetadata(exitLane, exitTime, checksum, serialNr, licensePlate, countryCode, version)

    Violation(VehicleViolationRecord(id,
      VehicleClassifiedRecord(
        id,
        VehicleSpeedRecord(id,
          entry,
          Some(exit),
          2002, measuredSpeed, "SHA-1-foo"),
        vehicleClass,
        processingIndicator,
        false,
        false,
        None,
        false,
        reason,
        None,
        None),
      enforcedSpeed,
      dynamicSpeedLimit,
      recognizeResults = List()),
      None,
      CorridorData(corridorId,
        entryGantry,
        exitGantry,
        corridorEntryLocation,
        corridorExitLocation,
        corridorLength,
        roadCode, radarCode, 7, 7, 60, 60,
        corridorLocationCode,
        corridorLocation1,
        corridorLocation2,
        drivingDirectionFrom,
        drivingDirectionTo,
        service = new SectionControl(),
        caseFile = new HHMVS40()),
      SystemGlobalConfig(CountryCodes(),
        routeId,
        mtmRouteId,
        "L",
        Seq(new MeasurementMethodInstallationType(
          0L,
          new MeasurementMethodType(
            typeDesignation = "XX123",
            category = "A",
            unitSpeed = "km/h",
            unitRedLight = "s",
            unitLength = "m",
            restrictiveConditions = "",
            displayRange = "20-250 km/h",
            temperatureRange = "temperatuurbereik -10 to 50 graden Celsius",
            permissibleError = "toelatbare fout 3%",
            typeCertificate = new TypeCertificate(
              "eg33-cert", 0,
              hardwareSoftwareSeals.toList)))),
        Seq(new LocationCertificate(id = "LocId", inspectionDate = 0L, validTime = Long.MaxValue, components = List())),
        ComponentCertificate("Softwarezegel", "checksum"),
        ComponentCertificate("Configuratiezegel", "checksum"),
        systemName,
        30.minutes.toMillis,
        serialNr,
        Seq("DE", "BE", "CH"), "A004"),
      CorridorSchedule(corridorId = corridorId,
        enforcedSpeedLimit = enforcedSpeed,
        indicatedSpeedLimit = Some(enforcedSpeed),
        pshTmIdFormat = pshTmId,
        startTime = scheduleStartTime,
        endTime = scheduleEndTime,
        textCode = None,
        dutyType = dutyType,
        deploymentCode = deploymentCode,
        speedIndicator = speedIndicator),
      pshTmId,
      reportingOfficerCode)
  }

  def createVehicleMetadata(lane: Lane,
                            time: Long,
                            checksum: String,
                            serialNr: String,
                            licensePlate: Option[String],
                            countryCode: Option[String],
                            version: Option[MetadataVersion]): VehicleMetadata = {

    val lp = licensePlate.map(lp ⇒ ValueWithConfidence(lp, 42))
    val cc = countryCode.map(cc ⇒ ValueWithConfidence(cc, 42))
    val lpd = version.map(v ⇒ LicensePlateData(license = lp, country = cc))

    VehicleMetadata(lane,
      nextId.toString,
      time,
      Some(new Date(time).toString),
      license = lp,
      country = cc,
      images = Seq(new VehicleImage(time, 0L, "", VehicleImageType.Overview, checksum)),
      NMICertificate = "",
      applChecksum = "",
      serialNr = serialNr,
      version = version.map(v ⇒ v.id),
      licensePlateData = lpd)
  }
}
