/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.nl.imagecheck

import csc.checkimage.{ ImageCheckResult, RecognitionResult }
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.messages.ValueWithConfidence

class ImageCheckResultTest extends WordSpec with MustMatchers {

  "ImageCheckResult" must {
    "default without errors" in {
      val result = new ImageCheckResult()
      result.getErrors() must be(0)
    }
    "intitialize with error" in {
      val result = new ImageCheckResult(2)
      result.getErrors() must be(2)
    }
    "add error" in {
      val result = new ImageCheckResult()
      result.setErrorCheck(true)
      result.hasErrorCheckFailed() must be(true)
      result.hasLicenseCheckFailed() must be(false)
      result.hasCountryCheckFailed() must be(false)
      result.getErrors() must be(ImageCheckResult.errorCheck)
    }
    "remove error" in {
      val result = new ImageCheckResult(ImageCheckResult.errorCheck)
      result.hasErrorCheckFailed() must be(true)
      result.setErrorCheck(false)
      result.getErrors() must be(0)
    }
    "add license error" in {
      val result = new ImageCheckResult()
      result.setErrorLicenseCheck(true)
      result.hasErrorCheckFailed() must be(false)
      result.hasLicenseCheckFailed() must be(true)
      result.hasCountryCheckFailed() must be(false)
      result.getErrors() must be(ImageCheckResult.licenseCheck)
    }
    "add country error" in {
      val result = new ImageCheckResult()
      result.setErrorCountryCheck(true)
      result.hasErrorCheckFailed() must be(false)
      result.hasLicenseCheckFailed() must be(false)
      result.hasCountryCheckFailed() must be(true)
      result.getErrors() must be(ImageCheckResult.countryCheck)
    }
    "add multiple errors" in {
      val result = new ImageCheckResult()
      result.setErrorCheck(true)
      result.hasErrorCheckFailed() must be(true)
      result.hasLicenseCheckFailed() must be(false)
      result.hasCountryCheckFailed() must be(false)
      result.hasLicenseFormatCheckFailed() must be(false)
      result.hasCountryChange() must be(false)
      result.hasChangeToMobi() must be(false)
      result.hasChangeToAuto() must be(false)
      result.hasErrorMatchingCheckFailed() must be(false)
      result.getErrors() must be(ImageCheckResult.errorCheck)

      result.setErrorLicenseCheck(true)
      result.hasErrorCheckFailed() must be(true)
      result.hasLicenseCheckFailed() must be(true)
      result.hasCountryCheckFailed() must be(false)
      result.hasLicenseFormatCheckFailed() must be(false)
      result.hasCountryChange() must be(false)
      result.hasChangeToMobi() must be(false)
      result.hasChangeToAuto() must be(false)
      result.hasErrorMatchingCheckFailed() must be(false)
      result.getErrors() must be(ImageCheckResult.errorCheck | ImageCheckResult.licenseCheck)

      result.setErrorCountryCheck(true)
      result.hasErrorCheckFailed() must be(true)
      result.hasLicenseCheckFailed() must be(true)
      result.hasCountryCheckFailed() must be(true)
      result.hasLicenseFormatCheckFailed() must be(false)
      result.hasCountryChange() must be(false)
      result.hasChangeToMobi() must be(false)
      result.hasChangeToAuto() must be(false)
      result.hasErrorMatchingCheckFailed() must be(false)
      result.getErrors() must be(ImageCheckResult.errorCheck | ImageCheckResult.licenseCheck | ImageCheckResult.countryCheck)

      result.setErrorLicenseFormatCheck(true)
      result.hasErrorCheckFailed() must be(true)
      result.hasLicenseCheckFailed() must be(true)
      result.hasCountryCheckFailed() must be(true)
      result.hasLicenseFormatCheckFailed() must be(true)
      result.hasCountryChange() must be(false)
      result.hasChangeToMobi() must be(false)
      result.hasChangeToAuto() must be(false)
      result.hasErrorMatchingCheckFailed() must be(false)
      result.getErrors() must be(ImageCheckResult.errorCheck | ImageCheckResult.licenseCheck | ImageCheckResult.countryCheck | ImageCheckResult.licenseFormatCheck)

      result.setCountryChange(true)
      result.hasErrorCheckFailed() must be(true)
      result.hasLicenseCheckFailed() must be(true)
      result.hasCountryCheckFailed() must be(true)
      result.hasLicenseFormatCheckFailed() must be(true)
      result.hasCountryChange() must be(true)
      result.hasChangeToMobi() must be(false)
      result.hasChangeToAuto() must be(false)
      result.hasErrorMatchingCheckFailed() must be(false)
      result.getErrors() must be(ImageCheckResult.errorCheck | ImageCheckResult.licenseCheck | ImageCheckResult.countryCheck | ImageCheckResult.licenseFormatCheck | ImageCheckResult.changeCountry)

      result.setChangeToMobi(true)
      result.hasErrorCheckFailed() must be(true)
      result.hasLicenseCheckFailed() must be(true)
      result.hasCountryCheckFailed() must be(true)
      result.hasLicenseFormatCheckFailed() must be(true)
      result.hasCountryChange() must be(true)
      result.hasChangeToMobi() must be(true)
      result.hasChangeToAuto() must be(false)
      result.hasErrorMatchingCheckFailed() must be(false)
      result.getErrors() must be(ImageCheckResult.errorCheck | ImageCheckResult.licenseCheck | ImageCheckResult.countryCheck | ImageCheckResult.licenseFormatCheck | ImageCheckResult.changeCountry | ImageCheckResult.changeToMobi)

      result.setChangeToAuto(true)
      result.hasErrorCheckFailed() must be(true)
      result.hasLicenseCheckFailed() must be(true)
      result.hasCountryCheckFailed() must be(true)
      result.hasLicenseFormatCheckFailed() must be(true)
      result.hasCountryChange() must be(true)
      result.hasChangeToMobi() must be(true)
      result.hasChangeToAuto() must be(true)
      result.hasErrorMatchingCheckFailed() must be(false)
      result.getErrors() must be(ImageCheckResult.errorCheck | ImageCheckResult.licenseCheck | ImageCheckResult.countryCheck | ImageCheckResult.licenseFormatCheck | ImageCheckResult.changeCountry | ImageCheckResult.changeToMobi | ImageCheckResult.changeToAuto)

      result.setErrorMatchingCheck(true)
      result.hasErrorCheckFailed() must be(true)
      result.hasLicenseCheckFailed() must be(true)
      result.hasCountryCheckFailed() must be(true)
      result.hasLicenseFormatCheckFailed() must be(true)
      result.hasCountryChange() must be(true)
      result.hasChangeToMobi() must be(true)
      result.hasChangeToAuto() must be(true)
      result.hasErrorMatchingCheckFailed() must be(true)
      result.getErrors() must be(ImageCheckResult.all)
    }
    "remove multiple errors" in {
      val result = new ImageCheckResult(ImageCheckResult.all)

      result.setErrorLicenseFormatCheck(false)
      result.hasErrorCheckFailed() must be(true)
      result.hasLicenseCheckFailed() must be(true)
      result.hasCountryCheckFailed() must be(true)
      result.hasLicenseFormatCheckFailed() must be(false)
      result.hasCountryChange() must be(true)
      result.hasChangeToMobi() must be(true)
      result.hasChangeToAuto() must be(true)
      result.hasErrorMatchingCheckFailed() must be(true)
      result.getErrors() must be(ImageCheckResult.errorCheck | ImageCheckResult.errorMatchingCheck | ImageCheckResult.countryCheck | ImageCheckResult.licenseCheck | ImageCheckResult.changeCountry | ImageCheckResult.changeToMobi | ImageCheckResult.changeToAuto)

      result.setErrorCheck(false)
      result.hasErrorCheckFailed() must be(false)
      result.hasLicenseCheckFailed() must be(true)
      result.hasCountryCheckFailed() must be(true)
      result.hasLicenseFormatCheckFailed() must be(false)
      result.hasCountryChange() must be(true)
      result.hasChangeToMobi() must be(true)
      result.hasChangeToAuto() must be(true)
      result.hasErrorMatchingCheckFailed() must be(true)
      result.getErrors() must be(ImageCheckResult.errorMatchingCheck | ImageCheckResult.countryCheck | ImageCheckResult.licenseCheck | ImageCheckResult.changeCountry | ImageCheckResult.changeToMobi | ImageCheckResult.changeToAuto)

      result.setErrorLicenseCheck(false)
      result.hasErrorCheckFailed() must be(false)
      result.hasLicenseCheckFailed() must be(false)
      result.hasCountryCheckFailed() must be(true)
      result.hasLicenseFormatCheckFailed() must be(false)
      result.hasCountryChange() must be(true)
      result.hasChangeToMobi() must be(true)
      result.hasChangeToAuto() must be(true)
      result.hasErrorMatchingCheckFailed() must be(true)
      result.getErrors() must be(ImageCheckResult.errorMatchingCheck | ImageCheckResult.countryCheck | ImageCheckResult.changeCountry | ImageCheckResult.changeToMobi | ImageCheckResult.changeToAuto)

      result.setErrorCountryCheck(false)
      result.hasErrorCheckFailed() must be(false)
      result.hasLicenseCheckFailed() must be(false)
      result.hasCountryCheckFailed() must be(false)
      result.hasLicenseFormatCheckFailed() must be(false)
      result.hasCountryChange() must be(true)
      result.hasChangeToMobi() must be(true)
      result.hasChangeToAuto() must be(true)
      result.hasErrorMatchingCheckFailed() must be(true)
      result.getErrors() must be(ImageCheckResult.errorMatchingCheck | ImageCheckResult.changeCountry | ImageCheckResult.changeToMobi | ImageCheckResult.changeToAuto)

      result.setCountryChange(false)
      result.hasErrorCheckFailed() must be(false)
      result.hasLicenseCheckFailed() must be(false)
      result.hasCountryCheckFailed() must be(false)
      result.hasLicenseFormatCheckFailed() must be(false)
      result.hasCountryChange() must be(false)
      result.hasChangeToMobi() must be(true)
      result.hasChangeToAuto() must be(true)
      result.hasErrorMatchingCheckFailed() must be(true)
      result.getErrors() must be(ImageCheckResult.errorMatchingCheck | ImageCheckResult.changeToMobi | ImageCheckResult.changeToAuto)

      result.setErrorMatchingCheck(false)
      result.hasErrorCheckFailed() must be(false)
      result.hasLicenseCheckFailed() must be(false)
      result.hasCountryCheckFailed() must be(false)
      result.hasLicenseFormatCheckFailed() must be(false)
      result.hasCountryChange() must be(false)
      result.hasChangeToMobi() must be(true)
      result.hasChangeToAuto() must be(true)
      result.hasErrorMatchingCheckFailed() must be(false)
      result.getErrors() must be(ImageCheckResult.changeToMobi | ImageCheckResult.changeToAuto)

      result.setChangeToMobi(false)
      result.hasErrorCheckFailed() must be(false)
      result.hasLicenseCheckFailed() must be(false)
      result.hasCountryCheckFailed() must be(false)
      result.hasLicenseFormatCheckFailed() must be(false)
      result.hasCountryChange() must be(false)
      result.hasChangeToMobi() must be(false)
      result.hasChangeToAuto() must be(true)
      result.hasErrorMatchingCheckFailed() must be(false)
      result.getErrors() must be(ImageCheckResult.changeToAuto)

      result.setChangeToAuto(false)
      result.hasErrorCheckFailed() must be(false)
      result.hasLicenseCheckFailed() must be(false)
      result.hasCountryCheckFailed() must be(false)
      result.hasLicenseFormatCheckFailed() must be(false)
      result.hasCountryChange() must be(false)
      result.hasChangeToMobi() must be(false)
      result.hasChangeToAuto() must be(false)
      result.hasErrorMatchingCheckFailed() must be(false)
      result.getErrors() must be(0)
    }
    "add results" in {
      val result = new ImageCheckResult(0)
      result.getRecognitionResults().size must be(0)
      val rec1 = new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = Some(new ValueWithConfidence[String]("ABCD12", 50)),
        rawLicense = Some("AB-CD-12"),
        country = Some(new ValueWithConfidence[String]("NL", 50)),
        errorMsg = None)
      result.addRecognitionResult(rec1)
      result.getRecognitionResults().size must be(1)
      result.getRecognitionResults().head must be(rec1)
      val rec2 = new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = Some(new ValueWithConfidence[String]("12ABCD", 50)),
        rawLicense = Some("12-AB-CD"),
        country = Some(new ValueWithConfidence[String]("NL", 50)),
        errorMsg = None)
      result.addRecognitionResult(rec2)
      result.getRecognitionResults().size must be(2)
      result.getRecognitionResults().head must be(rec1)
      result.getRecognitionResults().last must be(rec2)
    }
  }

}