/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.nl.register

import csc.checkimage._
import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import net.liftweb.json.DefaultFormats
import akka.util.duration._
import csc.sectioncontrol.messages._
import csc.sectioncontrol.enforce._
import common.nl.services.SectionControl
import common.nl.violations._
import common.nl.violations.CorridorData
import common.nl.violations.CorridorSchedule
import common.nl.violations.SpeedIndicator
import common.nl.violations.SystemConfigurationData
import common.nl.violations.SystemGlobalConfig
import common.nl.violations.Violation
import nl.{ RegistrationTimeUnreliableConfig, CuratorHelper }
import akka.testkit.{ TestLatch, TestActorRef }
import akka.dispatch.Await
import akka.util.{ Duration, Timeout }
import akka.actor.{ Actor, ActorRef, Props, ActorSystem }
import akka.pattern.ask
import csc.sectioncontrol.enforce.excludelog.ExcludeLog
import csc.json.lift.EnumerationSerializer
import csc.config.Path
import csc.sectioncontrol.storage._
import HBaseTableDefinitions._
import csc.sectioncontrol.storage.Paths
import csc.sectioncontrol.enforce.excludelog.GetExclusions
import java.util.{ Date, Calendar, GregorianCalendar }
import collection.SortedSet
import csc.sectioncontrol.storagelayer.excludelog.Exclusions
import csc.sectioncontrol.enforce.Enumerations.RoadType
import java.text.SimpleDateFormat
import csc.sectioncontrol.common.DayUtility
import csc.curator.CuratorTestServer
import csc.curator.utils.Curator
import csc.akkautils.DirectLogging
import register.CountryCodes
import csc.sectioncontrol.storagelayer.hbasetables._
import java.util.concurrent.TimeoutException
import csc.sectioncontrol.messages.VehicleViolationRecord
import csc.sectioncontrol.messages.certificates.MeasurementMethodType
import csc.sectioncontrol.messages.VehicleClassifiedRecord
import csc.sectioncontrol.enforce.Cleanup
import csc.sectioncontrol.storage.KeyWithTimeIdAndPostFix
import csc.sectioncontrol.enforce.ExportViolationsStatistics
import csc.sectioncontrol.storage.ZkSystemRetentionTimes
import csc.sectioncontrol.storage.ZkSystemLocation
import csc.sectioncontrol.messages.Lane
import csc.sectioncontrol.messages.certificates.ComponentCertificate
import csc.sectioncontrol.messages.certificates.LocationCertificate
import csc.sectioncontrol.storage.SerialNumber
import csc.sectioncontrol.storage.KeyWithTimeAndId
import csc.sectioncontrol.messages.IndicatorReason
import csc.sectioncontrol.messages.certificates.MeasurementMethodInstallation
import csc.sectioncontrol.storage.ZkSystem
import csc.sectioncontrol.messages.ValueWithConfidence
import csc.sectioncontrol.enforce.DayViolationsStatistics
import csc.sectioncontrol.storage.SelfTestResult
import csc.sectioncontrol.messages.certificates.TypeCertificate
import csc.sectioncontrol.messages.VehicleMetadata
import csc.sectioncontrol.messages.VehicleSpeedRecord
import csc.sectioncontrol.messages.certificates.ActiveCertificate
import csc.sectioncontrol.storage.ZkSystemPardonTimes
import csc.sectioncontrol.storagelayer.corridor.{ CorridorService, CorridorIdMapping }
import csc.sectioncontrol.storagelayer.state.StateService
import csc.sectioncontrol.storagelayer.caseFile.{ CaseFileConfigService }
import csc.sectioncontrol.storagelayer.system.SystemService

class RegisterViolationsTest extends WordSpec with MustMatchers with BeforeAndAfterAll
  with CuratorTestServer with DirectLogging {
  implicit val testSystem = ActorSystem("RegisterViolationsTest")
  val format = "yyyy-MM-dd'T'hh:mm:ss"
  def toDate(text: String, format: String = format): Long = (new SimpleDateFormat(format)).parse(text).getTime

  val systemService = new SystemServiceMock()
  var registerViolations: TestActorRef[RegisterViolations] = _
  var storeMtm: TestActorRef[StoreMtmData] = _
  val caseFileConfigMockService = new CaseFileConfigMockService()
  val testDuration: Duration = 5.seconds
  implicit val testTimeout = Timeout(testDuration)

  val jun_04_2000 = new GregorianCalendar(2000, Calendar.JUNE, 4, 0, 0, 0).getTimeInMillis / 1000 * 1000
  val jun_05_2000 = new GregorianCalendar(2000, Calendar.JUNE, 5, 0, 0, 0).getTimeInMillis / 1000 * 1000
  val jan_24_2002 = new GregorianCalendar(2002, Calendar.JANUARY, 24, 0, 0, 0).getTimeInMillis / 1000 * 1000
  val jan_25_2002 = new GregorianCalendar(2002, Calendar.JANUARY, 25, 0, 0, 0).getTimeInMillis / 1000 * 1000
  val jan_26_2002 = new GregorianCalendar(2002, Calendar.JANUARY, 26, 0, 0, 0).getTimeInMillis / 1000 * 1000
  val testData1 = "testData1".getBytes
  val testData2 = "testData2".getBytes
  val testImageBytes = "testImage".getBytes

  val processingLatch = TestLatch(1)

  val violationReader = new MockViolationDataReader
  val processedStore = new MockProcessedViolationStore
  val mtmStore = new MockMTMStore
  val selfTestReader = new MockSelfTestReader
  val speedIndicator = SpeedIndicator(signIndicator = false, speedIndicatorType = None)
  val sysConfig = new SystemConfigurationData(
    SystemGlobalConfig(CountryCodes(),
      "a002",
      "a002l",
      "L",
      Seq(),
      Seq(),
      ComponentCertificate("Softwarezegel", "checksum"),
      ComponentCertificate("Configuratiezegel", "checksum"),
      "",
      30.minutes.toMillis,
      "serial",
      Seq("DE", "BE", "CH"), "A004"),
    Seq[CorridorData](CorridorData(1, "gantry1", "gantry2", "here", "there", 1534.5, 1234, 2, 7, 7, 60, 60, "12345", "Somewhere", service = new SectionControl())),
    Seq[CorridorSchedule](CorridorSchedule(corridorId = 1, enforcedSpeedLimit = 100, indicatedSpeedLimit = None, pshTmIdFormat = "PSHTMID", startTime = jan_25_2002, endTime = jan_26_2002, speedIndicator = speedIndicator)),
    Map(1 -> ReportingOfficers(Map(0L -> "DE0123"))),
    Map[VehicleCode.Value, Int](),
    SortedSet(SpeedMargin(0, 0)))

  var excludeLog: ActorRef = _

  private var curator: Curator = _

  override protected def afterAll() {
    testSystem.shutdown()
    super.afterAll()
  }

  var processViolationsParams: (String, Seq[Violation], Map[Int, Seq[SelfTestResult]], Map[Int, String], Map[Int, String], SystemConfigurationData) = _

  val recognizeImage = testSystem.actorOf(Props(new Actor {
    protected def receive = {
      case ViolationImageCheckRequest(reference, violation) ⇒ {
        sender ! new ViolationImageCheckResponse(reference, violation, new ViolationCheckResult(Some(new ImageCheckResult()), Some(new ImageCheckResult())))
      }
    }
  }))

  override def beforeEach() {
    super.beforeEach()
    val formats = DefaultFormats + new EnumerationSerializer(EsaProviderType, RoadType, VehicleCode,
      ConfigType)
    curator = CuratorHelper.create(this, clientScope, formats)
    excludeLog = testSystem.actorOf(Props(new ExcludeLog("eg33", curator)).withDispatcher("default-dispatcher"))
    //zkClient.get.createPersistent("/ctes/systems/eg33", true)
    val system = ZkSystem(
      id = "eg33",
      name = "",
      title = "",
      mtmRouteId = "",
      violationPrefixId = "",
      location = ZkSystemLocation(
        description = "",
        region = "",
        viewingDirection = Some(""),
        roadNumber = "",
        roadPart = "",
        systemLocation = ""),
      serialNumber = SerialNumber(""),
      orderNumber = 0,
      maxSpeed = 0,
      compressionFactor = 0,
      retentionTimes = ZkSystemRetentionTimes(
        nrDaysKeepTrafficData = 0,
        nrDaysKeepViolations = 0,
        nrDaysRemindCaseFiles = 0),
      pardonTimes = ZkSystemPardonTimes(pardonTimeEsa_secs = 5 /*5000*/ , pardonTimeTechnical_secs = 5 /*5000*/ ),
      esaProviderType = EsaProviderType.Mtm)
    curator.put(Path("/ctes/systems/eg33/config"), system)
    violationReader.setViolations(Seq())

    val corridor = ZkCorridor(id = "S1",
      name = "S1-name",
      roadType = 0,
      serviceType = ServiceType.SectionControl,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = false,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = ZkPSHTMIdentification(useYear = false,
        useMonth = true,
        useDay = false,
        useReportingOfficerId = false,
        useNumberOfTheDay = true,
        useLocationCode = false,
        useHHMCode = false,
        useTypeViolation = false,
        useFixedCharacter = false,
        fixedCharacter = "T"),
      info = ZkCorridorInfo(corridorId = 1,
        locationCode = "2",
        reportId = "",
        reportHtId = "",
        reportPerformance = true,
        locationLine1 = ""),
      startSectionId = "sec1",
      endSectionId = "sec1",
      allSectionIds = Seq("sec1"),
      direction = ZkDirection(directionFrom = "",
        directionTo = ""),
      radarCode = 4,
      roadCode = 0,
      dutyType = "",
      deploymentCode = "",
      codeText = "")
    CorridorService.createCorridor(curator, system.id, corridor)
    val corridor2 = corridor.copy(id = "S2",
      name = "S2-name",
      info = corridor.info.copy(corridorId = 2))
    CorridorService.createCorridor(curator, system.id, corridor2)

    val contractCountries = Seq("NL", "DE", "BE", "CH")
    val minLowestLicConfLevel = 50
    val minLicConfLevelRecognizer2 = 70

    registerViolations = TestActorRef[RegisterViolations](new RegisterViolations(systemId = "eg33",
      inputViolations = violationReader,
      processedViolationStore = processedStore,
      violationImageReaders = Map(VehicleImageType.Overview -> MockImageReader),
      calibrationReportReader = selfTestReader,
      calibrationImageReader = MockSelftestImageReader,
      mtmDataStore = mtmStore,
      excludeLog = excludeLog,
      checkViolationImagesActor = Option(recognizeImage),
      curator = curator,
      timeoutImageResponse = 5.minute,
      recognizerCountries = contractCountries,
      maxCertifiedSpeed = 300,
      recognizeWithARHAgain = true,
      intradaInterfaceToUse = CheckViolation.INTRADA_IFACE_NORMAL,
      minLowestLicConfLevel = minLowestLicConfLevel,
      minLicConfLevelRecognizer2 = minLicConfLevelRecognizer2,
      autoThresholdLevel = 55,
      mobiThresholdLevel = 12,
      pardonRegistrationTime = RegistrationTimeUnreliableConfig(Seq()), caseFileConfigMockService) {
      override def writeViolations(violations: Seq[Violation],
                                   exportBaseDir: String,
                                   sysConfig: SystemConfigurationData,
                                   calibrationReports: Map[Int, Seq[SelfTestResult]],
                                   mtmRaw: Map[Int, Array[Byte]],
                                   mtmInfo: Map[Int, Array[Byte]],
                                   storeProcessed: ActorRef,
                                   esaProvider: EsaProviderType.Value, caseFileConfigMockService: CaseFileConfigService): Boolean = {
        processViolationsParams = (exportBaseDir,
          violations,
          calibrationReports,
          mtmRaw.mapValues(new String(_)),
          mtmInfo.mapValues(new String(_)),
          sysConfig)
        processingLatch.countDown()
        true
      }
    })

    storeMtm = TestActorRef[StoreMtmData](new StoreMtmData("eg33", mtmStore, curator))
  }

  override def afterEach() {
    storeMtm.stop()
    registerViolations.stop()
    testSystem.stop(excludeLog)
    super.afterEach()
  }

  implicit def formats = DefaultFormats + new EnumerationSerializer(RoadType,
    ConfigType)

  def makeCalibrationTestReport(corridorId: String, time: Long, infoId: Int, testOk: Boolean = true) =
    SelfTestResult("eg33", corridorId, time, testOk, "AB1234", infoId, Seq(), Seq(), "SN1234")

  def checkZkPathExists(path: String, mustExist: Boolean = true) { curator.exists(path) must be(mustExist) }

  "StoreMtmData" must {
    "store mtmInfo data when it is supplied" in {
      storeMtm ! LogMTMInfo(jun_04_2000, 1, testData1)
      storeMtm ! LogMTMInfo(jan_25_2002, 3, testData2)
      val path1 = "/ctes/systems/eg33/jobs/registration/2000-06-04/1/mtmInfo"
      checkZkPathExists(path1)
      val someData1 = curator.get[WrappedString](path1)
      someData1.get must be(WrappedString(path1))

      val path2 = "/ctes/systems/eg33/jobs/registration/2002-01-25/3/mtmInfo"
      checkZkPathExists(path2)
      val someData2 = curator.get[WrappedString](path2)
      someData2.get must be(WrappedString(path2))
    }

    "store mtmRaw data when it is supplied" in {
      storeMtm ! LogMTMRaw(jun_04_2000, 1, testData2)
      storeMtm ! LogMTMRaw(jan_25_2002, 3, testData1)
      val path1 = "/ctes/systems/eg33/jobs/registration/2000-06-04/1/mtmRaw"
      checkZkPathExists(path1)
      val s1 = curator.get[WrappedString](path1).get
      s1 must be(WrappedString(path1))

      mtmStore.mtmDataStore.get(path1) must be(Some(testData2))
      val path2 = "/ctes/systems/eg33/jobs/registration/2002-01-25/3/mtmRaw"
      checkZkPathExists(path2)
      val s2 = curator.get[WrappedString](path2).get
      s2 must be(WrappedString(path2))

      mtmStore.mtmDataStore.get(path2) must be(Some(testData1))
    }

    "cleanup old data" in {
      storeMtm ! LogMTMInfo(jun_04_2000, 1, testData1)
      storeMtm ! LogMTMInfo(jan_25_2002, 3, testData2)
      storeMtm ! LogMTMRaw(jun_04_2000, 1, testData2)
      storeMtm ! LogMTMRaw(jan_25_2002, 3, testData1)
      checkZkPathExists("/ctes/systems/eg33/jobs/registration/2000-06-04/1/mtmInfo")
      checkZkPathExists("/ctes/systems/eg33/jobs/registration/2002-01-25/3/mtmInfo")
      checkZkPathExists("/ctes/systems/eg33/jobs/registration/2000-06-04/1/mtmRaw")
      checkZkPathExists("/ctes/systems/eg33/jobs/registration/2002-01-25/3/mtmRaw")
      val dateDiff = (System.currentTimeMillis() - jan_25_2002) / (1000L * 60 * 60 * 24)
      storeMtm ! Cleanup(dateDiff.toInt)
      checkZkPathExists("/ctes/systems/eg33/jobs/registration/2000-06-04/1/mtmInfo", false)
      checkZkPathExists("/ctes/systems/eg33/jobs/registration/2002-01-25/3/mtmInfo")
      checkZkPathExists("/ctes/systems/eg33/jobs/registration/2000-06-04/1/mtmRaw", false)
      checkZkPathExists("/ctes/systems/eg33/jobs/registration/2002-01-25/3/mtmRaw")
    }
  }

  "a RegisterViolations actor" must {
    "read data, generate statistics and not write violations" in {
      val entry1 = new MeasurementMethodInstallation(typeId = "entry1", timeFrom = toDate("2012-01-01T00:00:00"))
      val path = Path(Paths.Systems.getInstallCertificatePath("eg33"))
      curator.put(path / "entry1", entry1)

      val globalType1 = new MeasurementMethodType(typeDesignation = "XX123",
        category = "A",
        unitSpeed = "km/h",
        unitRedLight = "s",
        unitLength = "m",
        restrictiveConditions = "",
        displayRange = "20-250 km/h",
        temperatureRange = "temperatuurbereik -10 to 50 graden Celsius",
        permissibleError = "toelatbare fout 3%",
        typeCertificate = new TypeCertificate("eg33-cert", 0,
          List(ComponentCertificate("part1", "blah1"),
            ComponentCertificate("part2", "blah2"))))

      val pathGlobal = Path(Paths.Configuration.getCertificateMeasurementMethodTypePath(entry1.typeId))
      curator.createEmptyPath(pathGlobal)
      curator.set(pathGlobal, globalType1, 0)

      val activePath = Path(Paths.Systems.getActiveCertificatesPath("eg33"))
      curator.put(activePath / "part1", ActiveCertificate(jan_25_2002, ComponentCertificate("part1", "blah1")))
      curator.put(activePath / "part2", ActiveCertificate(jan_25_2002, ComponentCertificate("part2", "blah2")))

      //add location certificate
      val locationCert = new LocationCertificate(id = "Test1",
        inspectionDate = toDate("2000-01-01T00:00:00"),
        validTime = System.currentTimeMillis() + 15.days.toMillis,
        components = List())
      val pathlocCert = Path(Paths.Systems.getLocationCertificatePath("eg33")) / "cert1"
      curator.createEmptyPath(pathlocCert)
      curator.set(pathlocCert, locationCert, 0)

      val stateStandby = ZkState(ZkState.standBy, jun_04_2000, "DE0123", "unittest")
      StateService.create("eg33", "S1", stateStandby, curator)
      violationReader.setViolations(Seq())
      storeMtm ! LogMTMInfo(jun_04_2000, 1, testData1)
      storeMtm ! LogMTMInfo(jan_25_2002, 3, testData2)
      storeMtm ! LogMTMRaw(jun_04_2000, 1, testData2)
      storeMtm ! LogMTMRaw(jan_25_2002, 3, testData1)
      selfTestReader.setSelfTestResults(Seq(makeCalibrationTestReport("S1", jan_25_2002, 3, true),
        makeCalibrationTestReport("S1", jan_26_2002, 3, true)))

      var statistics: Option[DayViolationsStatistics] = None
      val statisticsLatch = TestLatch(1)
      val statisticsListener = testSystem.actorOf(Props(new Actor {
        protected def receive = {
          case s: DayViolationsStatistics ⇒
            statistics = Some(s)
            statisticsLatch.countDown()
        }
      }))
      testSystem.eventStream.subscribe(statisticsListener, classOf[DayViolationsStatistics])

      val completionResponse = registerViolations ? ExportViolations(
        StatsDuration(jan_25_2002, DayUtility.fileExportTimeZone), jan_25_2002, "/tmp/RegisterViolationsTest", sysConfig)
      evaluating { Await.ready(processingLatch, 10.seconds) } must produce[TimeoutException]

      Await.result(completionResponse, testDuration) must be(ExportViolationsCompleted)

      Await.ready(statisticsLatch, testDuration)
      val expected = DayViolationsStatistics("eg33", StatsDuration(jan_25_2002, DayUtility.fileExportTimeZone), Seq(ExportViolationsStatistics(1, 100, 0, 0, 0, 0, 0, 0, 0, 0)))
      statistics must be(Some(expected))
    }

    "read data and pass it to processViolations" in {
      val entry1 = new MeasurementMethodInstallation(typeId = "entry1", timeFrom = toDate("2000-01-01T00:00:00"))
      val path = Path(Paths.Systems.getInstallCertificatePath("eg33"))
      curator.put(path / "entry1", entry1)

      val globalType1 = new MeasurementMethodType(typeDesignation = "XX123",
        category = "A",
        unitSpeed = "km/h",
        unitRedLight = "s",
        unitLength = "m",
        restrictiveConditions = "",
        displayRange = "20-250 km/h",
        temperatureRange = "temperatuurbereik -10 to 50 graden Celsius",
        permissibleError = "toelatbare fout 3%",
        typeCertificate = new TypeCertificate("eg33-cert", 0,
          List(ComponentCertificate("part1", "blah1"),
            ComponentCertificate("part2", "blah2"))))

      val pathGlobal = Path(Paths.Configuration.getCertificateMeasurementMethodTypePath(entry1.typeId))
      curator.createEmptyPath(pathGlobal)
      curator.set(pathGlobal, globalType1, 0)

      val activePath = Path(Paths.Systems.getActiveCertificatesPath("eg33"))
      curator.put(activePath / "part1", ActiveCertificate(jan_25_2002, ComponentCertificate("part1", "blah1")))
      curator.put(activePath / "part2", ActiveCertificate(jan_25_2002, ComponentCertificate("part2", "blah2")))

      //add location certificate
      val locationCert = new LocationCertificate(id = "Test1",
        inspectionDate = toDate("2000-01-01T00:00:00"),
        validTime = toDate("2015-01-01T00:00:00"),
        components = List())
      val pathlocCert = Path(Paths.Systems.getLocationCertificatePath("eg33")) / "cert1"
      curator.createEmptyPath(pathlocCert)
      curator.set(pathlocCert, locationCert, 0)

      val stateOn = ZkState("EnforceOn", jan_24_2002, "DE0123", "unittest")
      StateService.create("eg33", "S1", stateOn, curator)

      val tmp = makeVVR("test1", jan_25_2002, jan_25_2002 + 1.minute.toMillis)
      val violation1 = tmp.copy(classifiedRecord = tmp.classifiedRecord.copy(speedRecord = tmp.classifiedRecord.speedRecord.copy(corridorId = 1)))

      violationReader.setViolations(Seq(violation1))
      storeMtm ! LogMTMInfo(jun_04_2000, 1, testData1)
      storeMtm ! LogMTMInfo(jan_25_2002, 3, testData2)
      storeMtm ! LogMTMRaw(jun_04_2000, 1, testData2)
      storeMtm ! LogMTMRaw(jan_25_2002, 3, testData1)
      selfTestReader.setSelfTestResults(Seq(makeCalibrationTestReport("S1", jan_25_2002, 3, true),
        makeCalibrationTestReport("S1", jan_26_2002, 3, true)))

      var statistics: Option[DayViolationsStatistics] = None
      val statisticsLatch = TestLatch(1)
      val statisticsListener = testSystem.actorOf(Props(new Actor {
        protected def receive = {
          case s: DayViolationsStatistics ⇒
            statistics = Some(s)
            statisticsLatch.countDown()
        }
      }))
      testSystem.eventStream.subscribe(statisticsListener, classOf[DayViolationsStatistics])

      val completionResponse = registerViolations ? ExportViolations(
        StatsDuration(jan_25_2002, DayUtility.fileExportTimeZone), jan_25_2002, "/tmp/RegisterViolationsTest", sysConfig)
      Await.ready(processingLatch, 10.seconds)

      processViolationsParams must be("/tmp/RegisterViolationsTest",
        List[Violation](Violation(violation1, None, sysConfig.corridors.head, sysConfig.globalConfig, sysConfig.schedules.head, "PSHTMID", "DE0123")),
        Map(1 -> List[SelfTestResult](makeCalibrationTestReport("S1", jan_25_2002, 3, true), makeCalibrationTestReport("S1", jan_26_2002, 3, true)),
          2 -> List()),
        Map(3 -> new String(testData1)),
        Map(3 -> new String(testData2)),
        sysConfig)

      Await.result(completionResponse, testDuration) must be(ExportViolationsCompleted)

      Await.ready(statisticsLatch, testDuration)
      val expected = DayViolationsStatistics("eg33", StatsDuration(jan_25_2002, DayUtility.fileExportTimeZone), Seq(ExportViolationsStatistics(1, 100, 1, 0, 0, 0, 0, 0, 0, 0)))
      statistics must be(Some(expected))
    }

    "pardon violations where speed exceeds maxCertifiedSpeed" in {
      val entry1 = new MeasurementMethodInstallation(typeId = "entry1", timeFrom = toDate("2012-01-01T00:00:00"))
      val path = Path(Paths.Systems.getInstallCertificatePath("eg33")) / "entry1"
      curator.createEmptyPath(path)
      curator.set(path, entry1, 0)

      val globalType1 = new MeasurementMethodType(typeDesignation = "XX123",
        category = "A",
        unitSpeed = "km/h",
        unitRedLight = "s",
        unitLength = "m",
        restrictiveConditions = "",
        displayRange = "20-250 km/h",
        temperatureRange = "temperatuurbereik -10 to 50 graden Celsius",
        permissibleError = "toelatbare fout 3%",
        typeCertificate = new TypeCertificate("eg33-cert", 0,
          List(ComponentCertificate("part1", "blah1"),
            ComponentCertificate("part2", "blah2"))))

      val pathGlobal = Path(Paths.Configuration.getCertificateMeasurementMethodTypePath(entry1.typeId))
      curator.createEmptyPath(pathGlobal)
      curator.set(pathGlobal, globalType1, 0)

      val activePath = Path(Paths.Systems.getActiveCertificatesPath("eg33"))
      curator.put(activePath / "part1", ActiveCertificate(jan_25_2002, ComponentCertificate("part1", "blah1")))
      curator.put(activePath / "part2", ActiveCertificate(jan_25_2002, ComponentCertificate("part2", "blah2")))

      val locationCert = new LocationCertificate(id = "Test1",
        inspectionDate = toDate("2000-01-01T00:00:00"),
        validTime = toDate("2015-01-01T00:00:00"),
        components = List())
      val pathlocCert = Path(Paths.Systems.getLocationCertificatePath("eg33")) / "cert1"
      curator.createEmptyPath(pathlocCert)
      curator.set(pathlocCert, locationCert, 0)

      val stateOn = ZkState("EnforceOn", jun_04_2000, "DE0123", "unittest")
      StateService.create("eg33", "S1", stateOn, curator)

      val tmp = makeVVR("test1", jan_25_2002, jan_25_2002 + 1.minute.toMillis)
      val violation1 = tmp.copy(classifiedRecord = tmp.classifiedRecord.copy(speedRecord = tmp.classifiedRecord.speedRecord.copy(corridorId = 1)))
      val violationToFast = tmp.copy(classifiedRecord = tmp.classifiedRecord.copy(speedRecord = tmp.classifiedRecord.speedRecord.copy(corridorId = 1, speed = 350)))

      violationReader.setViolations(Seq(violation1, violationToFast))
      storeMtm ! LogMTMInfo(jun_04_2000, 1, testData1)
      storeMtm ! LogMTMInfo(jan_25_2002, 3, testData2)
      storeMtm ! LogMTMRaw(jun_04_2000, 1, testData2)
      storeMtm ! LogMTMRaw(jan_25_2002, 3, testData1)
      selfTestReader.setSelfTestResults(Seq(makeCalibrationTestReport("S1", jan_25_2002, 3, true),
        makeCalibrationTestReport("S1", jan_26_2002, 3, true)))

      var statistics: Option[DayViolationsStatistics] = None
      val statisticsLatch = TestLatch(1)
      val statisticsListener = testSystem.actorOf(Props(new Actor {
        protected def receive = {
          case s: DayViolationsStatistics ⇒
            statistics = Some(s)
            statisticsLatch.countDown()
        }
      }))
      testSystem.eventStream.subscribe(statisticsListener, classOf[DayViolationsStatistics])

      val completionResponse = registerViolations ? ExportViolations(
        StatsDuration(jan_25_2002, DayUtility.fileExportTimeZone), jan_25_2002, "/tmp/RegisterViolationsTest", sysConfig)
      Await.ready(processingLatch, 10.seconds)

      processViolationsParams must be("/tmp/RegisterViolationsTest",
        List[Violation](Violation(violation1, None, sysConfig.corridors.head, sysConfig.globalConfig, sysConfig.schedules.head, "PSHTMID", "DE0123")),
        Map(1 -> List[SelfTestResult](makeCalibrationTestReport("S1", jan_25_2002, 3, true), makeCalibrationTestReport("S1", jan_26_2002, 3, true)),
          2 -> List()),
        Map(3 -> new String(testData1)),
        Map(3 -> new String(testData2)),
        sysConfig)

      Await.result(completionResponse, testDuration) must be(ExportViolationsCompleted)

      Await.ready(statisticsLatch, testDuration)
      //pardoned violation must be added to Overigen-pardon
      val expected = DayViolationsStatistics("eg33", StatsDuration(jan_25_2002, DayUtility.fileExportTimeZone), Seq(ExportViolationsStatistics(1, 100, 1, 0, 0, 0, 0, 0, 1, 0)))
      statistics must be(Some(expected))
    }
    "accept a valid combination of typeCertificate and active certificates" in {
      val entry1 = new MeasurementMethodInstallation(typeId = "entry1", timeFrom = toDate("2012-01-01T00:00:00"))
      val path = Path(Paths.Systems.getInstallCertificatePath("eg33")) / "entry1"
      curator.createEmptyPath(path)
      curator.set(path, entry1, 0)

      val globalType1 = new MeasurementMethodType(typeDesignation = "XX123",
        category = "A",
        unitSpeed = "km/h",
        unitRedLight = "s",
        unitLength = "m",
        restrictiveConditions = "",
        displayRange = "20-250 km/h",
        temperatureRange = "temperatuurbereik -10 to 50 graden Celsius",
        permissibleError = "toelatbare fout 3%",
        typeCertificate = new TypeCertificate("eg33-cert", 0,
          List(ComponentCertificate("part1", "blah1"),
            ComponentCertificate("part2", "blah2"))))

      val pathGlobal = Path(Paths.Configuration.getCertificateMeasurementMethodTypePath(entry1.typeId))
      curator.createEmptyPath(pathGlobal)
      curator.set(pathGlobal, globalType1, 0)

      //add location certificate
      val locationCert = new LocationCertificate(id = "Test1",
        inspectionDate = toDate("2000-01-01T00:00:00"),
        validTime = System.currentTimeMillis() + 15.days.toMillis,
        components = List())
      val pathlocCert = Path(Paths.Systems.getLocationCertificatePath("eg33")) / "cert1"
      curator.createEmptyPath(pathlocCert)
      curator.set(pathlocCert, locationCert, 0)

      val activePath = Path(Paths.Systems.getActiveCertificatesPath("eg33"))
      curator.put(activePath / "component1", ActiveCertificate(jan_25_2002, ComponentCertificate("part1", "blah1")))

      //registerViolations.underlyingActor.areAllActiveCertificatesValid must be(true)
      registerViolations.underlyingActor.validateActiveCertificates._1 must be(true)
    }

    "reject an invalid combination of typeCertificate and active certificates" in {
      val entry1 = new MeasurementMethodInstallation(typeId = "entry1", timeFrom = toDate("2012-01-01T00:00:00"))
      val path = Path(Paths.Systems.getInstallCertificatePath("eg33")) / "entry1"
      curator.createEmptyPath(path)
      curator.set(path, entry1, 0)

      val globalType1 = new MeasurementMethodType(typeDesignation = "XX123",
        category = "A",
        unitSpeed = "km/h",
        unitRedLight = "s",
        unitLength = "m",
        restrictiveConditions = "",
        displayRange = "20-250 km/h",
        temperatureRange = "temperatuurbereik -10 to 50 graden Celsius",
        permissibleError = "toelatbare fout 3%",
        typeCertificate = new TypeCertificate("eg33-cert", 0,
          List(ComponentCertificate("part1", "blah1"),
            ComponentCertificate("part2", "blah2"))))

      val pathGlobal = Path(Paths.Configuration.getCertificateMeasurementMethodTypePath(entry1.typeId))
      curator.createEmptyPath(pathGlobal)
      curator.set(pathGlobal, globalType1, 0)

      val activePath = Path(Paths.Systems.getActiveCertificatesPath("eg33"))
      curator.put(activePath / "component1", ActiveCertificate(jan_25_2002, ComponentCertificate("part1", "blah2")))

      registerViolations.underlyingActor.validateActiveCertificates._1 must be(false)
    }

    "reject a combination of typeCertificate and no active certificates" in {
      val entry1 = new MeasurementMethodInstallation(typeId = "entry1", timeFrom = toDate("2012-01-01T00:00:00"))
      val path = Path(Paths.Systems.getInstallCertificatePath("eg33")) / "entry1"
      curator.createEmptyPath(path)
      curator.set(path, entry1, 0)

      val globalType1 = new MeasurementMethodType(typeDesignation = "XX123",
        category = "A",
        unitSpeed = "km/h",
        unitRedLight = "s",
        unitLength = "m",
        restrictiveConditions = "",
        displayRange = "20-250 km/h",
        temperatureRange = "temperatuurbereik -10 to 50 graden Celsius",
        permissibleError = "toelatbare fout 3%",
        typeCertificate = new TypeCertificate("eg33-cert", 0,
          List(ComponentCertificate("part1", "blah1"),
            ComponentCertificate("part2", "blah2"))))

      val pathGlobal = Path(Paths.Configuration.getCertificateMeasurementMethodTypePath(entry1.typeId))
      curator.createEmptyPath(pathGlobal)
      curator.set(pathGlobal, globalType1, 0)

      //registerViolations.underlyingActor.areAllActiveCertificatesValid must be(false)
      registerViolations.underlyingActor.validateActiveCertificates._1 must be(false)
    }

    "reject if no typeCertificate is present" in {
      //registerViolations.underlyingActor.areAllActiveCertificatesValid must be(false)
      registerViolations.underlyingActor.validateActiveCertificates._1 must be(false)
    }

  }

  "A reported failed CalibrationTestReport" must {
    "no self-test must exclude the day" in {
      StateService.create("eg33", "S1", ZkState(ZkState.enforceOn, 1L, "test", "test"), curator)
      val selfTests = Map(CorridorIdMapping("S1", 1) -> Seq[SelfTestResult]())
      registerViolations.underlyingActor.generateSelfTestExclusions(jun_04_2000, selfTests)
      val result = registerViolations.underlyingActor.excludeLog ? GetExclusions(1)
      val exclusions = Await.result(result, testDuration).asInstanceOf[Exclusions]
      val beginTime = jun_04_2000
      exclusions.intersectsInterval(beginTime - 1, beginTime - 1) must be(false)
      exclusions.intersectsInterval(beginTime, beginTime) must be(true)
      exclusions.intersectsInterval(jun_04_2000, jun_04_2000) must be(true)
      exclusions.intersectsInterval(jun_04_2000 + 1, jun_04_2000 + 1) must be(true)
    }

    "exclude the day up to forever" in {
      StateService.create("eg33", "S1", ZkState(ZkState.enforceOn, 1L, "test", "test"), curator)
      val selfTests = Map(CorridorIdMapping("S1", 1) -> Seq(makeCalibrationTestReport("S1", jun_04_2000, 1, false)))
      registerViolations.underlyingActor.generateSelfTestExclusions(jun_04_2000, selfTests)
      val result = registerViolations.underlyingActor.excludeLog ? GetExclusions(1)
      val exclusions = Await.result(result, testDuration).asInstanceOf[Exclusions]
      val beginTime = jun_04_2000
      exclusions.intersectsInterval(beginTime - 1, beginTime - 1) must be(false)
      exclusions.intersectsInterval(beginTime, beginTime) must be(true)
      exclusions.intersectsInterval(jun_04_2000, jun_04_2000) must be(true)
      exclusions.intersectsInterval(jun_04_2000 + 1, jun_04_2000 + 1) must be(true)
    }

    "exclude from last ok test up to forever" in {
      val range = 1.hour.toMillis
      StateService.create("eg33", "S1", ZkState(ZkState.enforceOn, 1L, "test", "test"), curator)
      val selfTests = Map(CorridorIdMapping("S1", 1) -> Seq(makeCalibrationTestReport("S1", jun_04_2000 - range, 1, true),
        makeCalibrationTestReport("S1", jun_04_2000, 1, false)))
      registerViolations.underlyingActor.generateSelfTestExclusions(jun_04_2000, selfTests)
      val result = registerViolations.underlyingActor.excludeLog ? GetExclusions(1)
      val exclusions = Await.result(result, testDuration).asInstanceOf[Exclusions]
      val beginTime = jun_04_2000 - range
      exclusions.intersectsInterval(beginTime - 1, beginTime - 1) must be(false)
      exclusions.intersectsInterval(beginTime, beginTime) must be(true)
      exclusions.intersectsInterval(jun_04_2000, jun_04_2000) must be(true)
      exclusions.intersectsInterval(jun_04_2000 + 1, jun_04_2000 + 1) must be(true)
    }

    "exclude from start of day up to next ok test" in {
      val range = 1.hour.toMillis
      StateService.create("eg33", "S1", ZkState(ZkState.enforceOn, 1L, "test", "test"), curator)
      val selfTests = Map(CorridorIdMapping("S1", 1) -> Seq(makeCalibrationTestReport("S1", jun_04_2000, 1, false),
        makeCalibrationTestReport("S1", jun_04_2000 + range, 1, true),
        makeCalibrationTestReport("S1", jun_05_2000, 1, true)))
      registerViolations.underlyingActor.generateSelfTestExclusions(jun_04_2000, selfTests)
      val result = registerViolations.underlyingActor.excludeLog ? GetExclusions(1)
      val exclusions = Await.result(result, testDuration).asInstanceOf[Exclusions]
      val beginTime = jun_04_2000
      val endTime = jun_04_2000 + range
      exclusions.intersectsInterval(beginTime - 1, beginTime - 1) must be(false)
      exclusions.intersectsInterval(beginTime, beginTime) must be(true)
      exclusions.intersectsInterval(endTime, endTime) must be(true)
      exclusions.intersectsInterval(endTime + 1, endTime + 1) must be(false)
    }

    "exclude from last ok to forever if no end-of-day test" in {
      val range = 1.hour.toMillis
      StateService.create("eg33", "S1", ZkState(ZkState.enforceOn, 1L, "test", "test"), curator)
      val selfTests = Map(CorridorIdMapping("S1", 1) -> Seq(makeCalibrationTestReport("S1", jun_04_2000, 1, false),
        makeCalibrationTestReport("S1", jun_04_2000 + range, 1, true)))
      registerViolations.underlyingActor.generateSelfTestExclusions(jun_04_2000, selfTests)
      val result = registerViolations.underlyingActor.excludeLog ? GetExclusions(1)
      val exclusions = Await.result(result, testDuration).asInstanceOf[Exclusions]
      val beginTime = jun_04_2000
      val endTime = jun_04_2000 + range
      exclusions.intersectsInterval(beginTime - 1, beginTime - 1) must be(false)
      exclusions.intersectsInterval(beginTime, beginTime) must be(true)
      exclusions.intersectsInterval(endTime, endTime) must be(true)
      exclusions.intersectsInterval(endTime + 1, endTime + 1) must be(true)
      exclusions.intersectsInterval(jun_05_2000, jun_05_2000) must be(true)
      exclusions.intersectsInterval(jun_05_2000 + 1, jun_05_2000 + 1) must be(false)
    }

    "exclude from last ok test up to next ok test" in {
      val range = 1.hour.toMillis
      StateService.create("eg33", "S1", ZkState(ZkState.enforceOn, 1L, "test", "test"), curator)
      val selfTests = Map(CorridorIdMapping("S1", 1) -> Seq(makeCalibrationTestReport("S1", jun_04_2000 - range, 1, true),
        makeCalibrationTestReport("S1", jun_04_2000, 1, false),
        makeCalibrationTestReport("S1", jun_04_2000 + range, 1, true),
        makeCalibrationTestReport("S1", jun_05_2000, 1, true)))
      registerViolations.underlyingActor.generateSelfTestExclusions(jun_04_2000, selfTests)
      val result = registerViolations.underlyingActor.excludeLog ? GetExclusions(1)
      val exclusions = Await.result(result, testDuration).asInstanceOf[Exclusions]
      val beginTime = jun_04_2000 - range
      val endTime = jun_04_2000 + range
      exclusions.intersectsInterval(beginTime - 1, beginTime - 1) must be(false)
      exclusions.intersectsInterval(beginTime, beginTime) must be(true)
      exclusions.intersectsInterval(endTime, endTime) must be(true)
      exclusions.intersectsInterval(endTime + 1, endTime + 1) must be(false)
    }

    "exclude from last ok test up to next ok test with other corridors tests" in {
      val range = 1.hour.toMillis
      StateService.create("eg33", "S1", ZkState(ZkState.enforceOn, 1L, "test", "test"), curator)
      StateService.create("eg33", "S2", ZkState(ZkState.enforceOn, 1L, "test", "test"), curator)

      val selfTests = Map(CorridorIdMapping("S1", 1) -> Seq(
        makeCalibrationTestReport("S1", jun_04_2000 - range, 1, true),
        makeCalibrationTestReport("S2", jun_04_2000 - 20.minutes.toMillis, 2, true),
        makeCalibrationTestReport("S1", jun_04_2000, 1, false),
        makeCalibrationTestReport("S2", jun_04_2000 + 20.minutes.toMillis, 2, true),
        makeCalibrationTestReport("S1", jun_04_2000 + range, 1, true),
        makeCalibrationTestReport("S1", jun_05_2000, 1, true)),
        CorridorIdMapping("S2", 2) -> Seq(
          makeCalibrationTestReport("S2", jun_04_2000 - range, 1, true),
          makeCalibrationTestReport("S2", jun_04_2000, 1, false),
          makeCalibrationTestReport("S2", jun_04_2000 + range, 1, true),
          makeCalibrationTestReport("S2", jun_05_2000, 1, true)))

      registerViolations.underlyingActor.generateSelfTestExclusions(jun_04_2000, selfTests)
      var result = registerViolations.underlyingActor.excludeLog ? GetExclusions(1)
      var exclusions = Await.result(result, testDuration).asInstanceOf[Exclusions]
      var beginTime = jun_04_2000 - range
      var endTime = jun_04_2000 + range

      exclusions.intersectsInterval(beginTime - 1, beginTime - 1) must be(false)
      exclusions.intersectsInterval(beginTime, beginTime) must be(true)
      exclusions.intersectsInterval(endTime, endTime) must be(true)
      exclusions.intersectsInterval(endTime + 1, endTime + 1) must be(false)

      //registerViolations.underlyingActor.generateSelfTestExclusions(jun_04_2000, selfTests)
      result = registerViolations.underlyingActor.excludeLog ? GetExclusions(2)
      exclusions = Await.result(result, testDuration).asInstanceOf[Exclusions]
      beginTime = jun_04_2000 - range
      endTime = jun_04_2000 + range

      exclusions.intersectsInterval(beginTime - 1, beginTime - 1) must be(false)
      exclusions.intersectsInterval(beginTime, beginTime) must be(true)
      exclusions.intersectsInterval(endTime, endTime) must be(true)
      exclusions.intersectsInterval(endTime + 1, endTime + 1) must be(false)

    }

    "exclude from last ok test up to next ok test with other corridor tests" in {
      val range = 1.hour.toMillis
      StateService.create("eg33", "S1", ZkState(ZkState.enforceOn, 1L, "test", "test"), curator)
      val selfTests = Map(CorridorIdMapping("S1", 1) -> Seq(
        makeCalibrationTestReport("S1", jun_04_2000 - range, 1, true),
        makeCalibrationTestReport("S2", jun_04_2000 - 20.minutes.toMillis, 2, true),
        makeCalibrationTestReport("S1", jun_04_2000, 1, false),
        makeCalibrationTestReport("S2", jun_04_2000 + 20.minutes.toMillis, 2, true),
        makeCalibrationTestReport("S1", jun_04_2000 + range, 1, true),
        makeCalibrationTestReport("S1", jun_05_2000, 1, true)))

      registerViolations.underlyingActor.generateSelfTestExclusions(jun_04_2000, selfTests)
      val result = registerViolations.underlyingActor.excludeLog ? GetExclusions(1)
      val exclusions = Await.result(result, testDuration).asInstanceOf[Exclusions]
      val beginTime = jun_04_2000 - range
      val endTime = jun_04_2000 + range

      exclusions.intersectsInterval(beginTime - 1, beginTime - 1) must be(false)
      exclusions.intersectsInterval(beginTime, beginTime) must be(true)
      exclusions.intersectsInterval(endTime, endTime) must be(true)
      exclusions.intersectsInterval(endTime + 1, endTime + 1) must be(false)
    }
  }

  "A double CalibrationTestReport" must {
    "be filtered" in {
      val range = 1.hour.toMillis
      StateService.create("eg33", "S1", ZkState(ZkState.enforceOn, 1L, "test", "test"), curator)
      val selftestResults = Seq(
        makeCalibrationTestReport("S1", jun_04_2000 - range, 1, true),
        makeCalibrationTestReport("S1", jun_04_2000 - range + 1, 1, true),

        makeCalibrationTestReport("S1", jun_04_2000, 1, false),
        makeCalibrationTestReport("S1", jun_04_2000 + 1, 1, false),

        makeCalibrationTestReport("S1", jun_04_2000 + range, 1, true),
        makeCalibrationTestReport("S1", jun_04_2000 + range + 999, 1, true),

        makeCalibrationTestReport("S1", jun_05_2000, 1, true))
      selfTestReader.setSelfTestResults(selftestResults)
      val results = registerViolations.underlyingActor.readSelfTestResults(jun_04_2000)

      results.size must be(2)
      val corridorResults = results.get(CorridorIdMapping("S1", 1)).get
      corridorResults.size must be(4)
      corridorResults(0) must be(selftestResults(0))
      corridorResults(1) must be(selftestResults(2))
      corridorResults(2) must be(selftestResults(4))
      corridorResults(3) must be(selftestResults(6))
    }

    "Not be filtered when different corridors" in {
      val range = 1.hour.toMillis
      StateService.create("eg33", "S1", ZkState(ZkState.enforceOn, 1L, "test", "test"), curator)
      val selftestResults = Seq(
        makeCalibrationTestReport("S1", jun_04_2000 - range, 1, true),
        makeCalibrationTestReport("S2", jun_04_2000 - range + 1, 2, true),

        makeCalibrationTestReport("S1", jun_04_2000, 1, false),
        makeCalibrationTestReport("S2", jun_04_2000 + 1, 2, false),

        makeCalibrationTestReport("S1", jun_04_2000 + range, 1, true),
        makeCalibrationTestReport("S1", jun_04_2000 + range + 999, 1, true),

        makeCalibrationTestReport("S1", jun_05_2000, 1, true))
      selfTestReader.setSelfTestResults(selftestResults)
      val results = registerViolations.underlyingActor.readSelfTestResults(jun_04_2000)
      results.size must be(2)
      val corridorResults = results.get(CorridorIdMapping("S1", 1)).get
      corridorResults.size must be(4)
      corridorResults(0) must be(selftestResults(0))
      corridorResults(1) must be(selftestResults(2))
      corridorResults(2) must be(selftestResults(4))
      corridorResults(3) must be(selftestResults(6))

      val corridorResults2 = results.get(CorridorIdMapping("S2", 2)).get
      corridorResults2.size must be(2)
      corridorResults2(0) must be(selftestResults(1))
      corridorResults2(1) must be(selftestResults(3))
    }
    "not fail when empty" in {
      val range = 1.hour.toMillis
      StateService.create("eg33", "S1", ZkState(ZkState.enforceOn, 1L, "test", "test"), curator)
      selfTestReader.setSelfTestResults(Seq())
      val results = registerViolations.underlyingActor.readSelfTestResults(jun_04_2000)
      // There are 2 corridors
      results.size must be(2)

      results.exists(_._1.corridorId == "S1") must be(true)
      results.exists(_._1.corridorId == "S2") must be(true)

      for ((k, v) ← results) v.size must be(0)
    }
    "not fail when containing one record" in {
      val range = 1.hour.toMillis
      StateService.create("eg33", "S1", ZkState(ZkState.enforceOn, 1L, "test", "test"), curator)
      val selftestResults = Seq(makeCalibrationTestReport("S1", jun_04_2000 - range, 1, true))
      selfTestReader.setSelfTestResults(selftestResults)
      val results = registerViolations.underlyingActor.readSelfTestResults(jun_04_2000)

      results.size must be(2)
      val corridorResults = results.get(CorridorIdMapping("S1", 1)).get
      corridorResults.size must be(1)
      corridorResults(0) must be(selftestResults(0))

      val S2 = results.find(_._1.corridorId == "S2")
      S2.get._1.corridorId must be("S2")
      S2.get._2.length must be(0)
    }
  }

  class MockViolationDataReader extends ViolationRecordTable.Reader {
    var violations = Seq[vehicleViolationRecordType]()

    def setViolations(vs: Seq[vehicleViolationRecordType]) { violations = vs }

    def readRow(key: KeyWithTimeAndId)(implicit mt: Manifest[vehicleViolationRecordType]) = None

    def readRows(from: KeyWithTimeAndId, to: KeyWithTimeAndId)(implicit mt: Manifest[vehicleViolationRecordType]) = violations

    def readRowsByTime(from: Long, to: Long)(implicit mt: Manifest[vehicleViolationRecordType]) = null
  }

  class MockProcessedViolationStore extends ProcessedViolationTable.Store {
    def readRow(key: KeyWithTimeIdAndPostFix)(implicit mt: Manifest[processedViolationRecordType]) = None

    def readRows(from: KeyWithTimeIdAndPostFix, to: KeyWithTimeIdAndPostFix)(implicit mt: Manifest[processedViolationRecordType]) = Nil

    def readRowsByTime(from: Long, to: Long)(implicit mt: Manifest[processedViolationRecordType]) = Nil

    def writeRow(key: KeyWithTimeIdAndPostFix, value: processedViolationRecordType, timestamp: Option[Long]) {}

    def writeRows(keysAndValues: Seq[(KeyWithTimeIdAndPostFix, processedViolationRecordType)]) {}

    def writeRowsWithTimestamp(keysAndValues: Seq[(KeyWithTimeIdAndPostFix, processedViolationRecordType, Long)]) {}

    def writeRows(values: Seq[processedViolationRecordType],
                  getKey: (processedViolationRecordType) ⇒ (KeyWithTimeIdAndPostFix, Option[Long])) {}
  }

  class MockMTMStore extends MtmFileTable.Store {
    val mtmDataStore = collection.mutable.Map[String, Array[Byte]]()

    def readRow(key: String)(implicit mt: Manifest[Array[Byte]]) = mtmDataStore.get(key)

    def readRows(from: String, to: String)(implicit mt: Manifest[Array[Byte]]) = Nil

    def readRowsByTime(from: Long, to: Long)(implicit mt: Manifest[Array[Byte]]) = Nil

    def writeRow(key: String, value: Array[Byte], timestamp: Option[Long]) { mtmDataStore.put(key, value) }

    def writeRows(keysAndValues: Seq[(String, Array[Byte])]) {}

    def writeRowsWithTimestamp(keysAndValues: Seq[(String, Array[Byte], Long)]) {}

    def writeRows(values: Seq[Array[Byte]], getKey: (Array[Byte]) ⇒ (String, Option[Long])) {}
  }

  object MockSelftestImageReader extends CalibrationImageTable.Reader {
    def readRow(key: KeyWithTimeAndId)(implicit mt: Manifest[Array[Byte]]) = Some(testImageBytes)
    def readRows(from: KeyWithTimeAndId, to: KeyWithTimeAndId)(implicit mt: Manifest[Array[Byte]]) = null
  }

  object MockImageReader extends VehicleImageTable.Reader {
    def readRow(key: (String, Long, String))(implicit mt: Manifest[Array[Byte]]) = Some(testImageBytes)
    def readRows(from: (String, Long, String), to: (String, Long, String))(implicit mt: Manifest[Array[Byte]]) = null
  }

  class MockSelfTestReader extends SelfTestResultTable.Reader {
    var selfTests = Seq[selfTestResultRecordType]()

    def setSelfTestResults(str: Seq[selfTestResultRecordType]) { selfTests = str }

    def readRow(key: (String, Long))(implicit mt: Manifest[selfTestResultRecordType]) = None

    def readRows(from: (String, Long), to: (String, Long))(implicit mt: Manifest[selfTestResultRecordType]) = selfTests.filter(_.corridorId == from._1)
  }

  def makeVVR(id: String,
              entryTime: Long,
              exitTime: Long,
              countryCode: Option[String] = Some("NL"),
              measuredSpeed: Int = 113,
              vehicleClass: Option[VehicleCode.Value] = Some(VehicleCode.PA),
              licensePlate: Option[String] = Some("AB12CD"),
              processingIndicator: ProcessingIndicator.Value = ProcessingIndicator.Automatic,
              enforcedSpeed: Int = 100,
              reason: IndicatorReason = IndicatorReason()) =
    VehicleViolationRecord(id,
      new VehicleClassifiedRecord(
        VehicleSpeedRecord(id,
          VehicleMetadata(Lane("1",
            "lane1",
            "gantry1",
            "eg33",
            0.0f,
            0.0f),
            id,
            entryTime,
            Some(new Date(entryTime).toString),
            licensePlate.map(lp ⇒ ValueWithConfidence(lp, 42)),
            country = countryCode.map(cc ⇒ ValueWithConfidence(cc, 42)),
            images = Seq(),
            NMICertificate = "",
            applChecksum = "",
            serialNr = "SN1234"),
          Some(VehicleMetadata(Lane("1",
            "lane1",
            "gantry2",
            "eg33",
            0.0f,
            0.0f),
            id,
            exitTime,
            Some(new Date(exitTime).toString),
            licensePlate.map(lp ⇒ ValueWithConfidence(lp, 42)),
            country = countryCode.map(cc ⇒ ValueWithConfidence(cc, 42)),
            images = Seq(),
            NMICertificate = "",
            applChecksum = "",
            serialNr = "SN1234")),
          2002, measuredSpeed, "SHA-1-foo"),
        vehicleClass,
        processingIndicator,
        false,
        false,
        None,
        false,
        reason),
      enforcedSpeed,
      None,
      recognizeResults = List())
}
