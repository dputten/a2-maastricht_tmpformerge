/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.nl.classify

import csc.sectioncontrol.messages._
import csc.sectioncontrol.messages.ValueWithConfidence
import csc.sectioncontrol.messages.VehicleSpeedRecord
import csc.sectioncontrol.messages.VehicleClassifiedRecord
import csc.sectioncontrol.messages.Lane
import csc.sectioncontrol.messages.VehicleMetadata

object CreateClassifiedRecords {
  def create1NoCarVan(time: Long, corridorId: Int, dambord: String): VehicleClassifiedRecord = {
    createVehicleClassifiedRecord(time = time,
      code = Some(VehicleCode.AS),
      indicator = ProcessingIndicator.Manual,
      manualAccordingToSpec = false,
      dambord = Some(dambord),
      corridorId = corridorId,
      category = Some(ValueWithConfidence[String]("Truck", 70)),
      license = Some(ValueWithConfidence[String]("12345", 70)))
  }
  def create2Empty(time: Long, corridorId: Int, dambord: String): VehicleClassifiedRecord = {
    createVehicleClassifiedRecord(time = time,
      code = Some(VehicleCode.AS),
      indicator = ProcessingIndicator.Manual,
      manualAccordingToSpec = false,
      dambord = Some(dambord),
      corridorId = corridorId,
      category = None,
      license = Some(ValueWithConfidence[String]("12345", 70)))
  }

  def create4Conf70(time: Long, corridorId: Int, dambord: String, category: String): VehicleClassifiedRecord = {
    val code = if (category == "Car") {
      VehicleCode.PA
    } else {
      VehicleCode.CA
    }
    createVehicleClassifiedRecord(time = time,
      code = Some(code),
      indicator = ProcessingIndicator.Automatic,
      manualAccordingToSpec = false,
      dambord = Some(dambord),
      corridorId = corridorId,
      category = Some(ValueWithConfidence[String](category, 70)),
      license = Some(ValueWithConfidence[String]("12345", 70)))
  }
  def create5Conf10(time: Long, corridorId: Int, dambord: String, category: String): VehicleClassifiedRecord = {
    val code = if (category == "Car") {
      VehicleCode.PA
    } else {
      VehicleCode.CA
    }
    createVehicleClassifiedRecord(time = time,
      code = Some(code),
      indicator = ProcessingIndicator.Automatic,
      manualAccordingToSpec = false,
      dambord = Some(dambord),
      corridorId = corridorId,
      category = Some(ValueWithConfidence[String](category, 10)),
      license = Some(ValueWithConfidence[String]("12345", 70)))
  }
  def create6Conf0_LengthNOK(time: Long, corridorId: Int, dambord: String, category: String): VehicleClassifiedRecord = {
    val code = if (category == "Car") {
      VehicleCode.PA
    } else {
      VehicleCode.CA
    }
    createVehicleClassifiedRecord(time = time,
      code = Some(code),
      indicator = ProcessingIndicator.Manual,
      manualAccordingToSpec = false,
      dambord = Some(dambord),
      corridorId = corridorId,
      category = Some(ValueWithConfidence[String](category, 0)),
      license = Some(ValueWithConfidence[String]("12345", 70)))
  }
  def create7Conf0_LengthOK(time: Long, corridorId: Int, dambord: String, category: String): VehicleClassifiedRecord = {
    val code = if (category == "Car") {
      VehicleCode.PA
    } else {
      VehicleCode.CA
    }
    createVehicleClassifiedRecord(time = time,
      code = Some(code),
      indicator = ProcessingIndicator.Automatic,
      manualAccordingToSpec = false,
      dambord = Some(dambord),
      corridorId = corridorId,
      category = Some(ValueWithConfidence[String](category, 0)),
      license = Some(ValueWithConfidence[String]("12345", 70)))
  }

  def createVehicleClassifiedRecord(time: Long,
                                    code: Option[VehicleCode.Value],
                                    indicator: ProcessingIndicator.Value,
                                    manualAccordingToSpec: Boolean,
                                    dambord: Option[String],
                                    corridorId: Int,
                                    category: Option[ValueWithConfidence[String]],
                                    license: Option[ValueWithConfidence[String]] = None,
                                    reason: IndicatorReason = IndicatorReason(0)): VehicleClassifiedRecord = {
    val reg = new VehicleMetadata(lane = new Lane("A2-X1-R3", "R3", "X1", "A2", 5.4, 52.4),
      eventId = "A2-X1-R3-123",
      eventTimestamp = time,
      eventTimestampStr = None,
      license = license,
      rawLicense = None,
      length = None,
      category = category,
      speed = None,
      country = None,
      images = Seq(),
      NMICertificate = "",
      applChecksum = "",
      serialNr = "SN1234")
    new VehicleClassifiedRecord(
      id = "id",
      speedRecord = new VehicleSpeedRecord(id = "id",
        entry = reg,
        exit = Some(reg),
        corridorId = corridorId,
        speed = 90,
        measurementSHA = "1234"),
      code = code,
      indicator = indicator,
      mil = false,
      invalidRdwData = false,
      validLicensePlate = None,
      manualAccordingToSpec = manualAccordingToSpec,
      reason = reason,
      alternativeClassification = None,
      dambord = dambord)

  }

}