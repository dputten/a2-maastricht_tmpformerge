/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.nl.imagecheck

import csc.checkimage.{ RecognitionResult, RegistrationImageCheckResponse, RegistrationImageCheckRequest, CheckVehicleRegistration }
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers
import akka.actor.{ Props, ActorSystem }
import java.text.SimpleDateFormat
import akka.testkit.TestProbe
import akka.util.duration._
import java.util.Date
import csc.sectioncontrol.messages.ValueWithConfidence
import csc.sectioncontrol.messages.VehicleMetadata
import scala.Some
import csc.sectioncontrol.messages.Lane
import csc.image.recognizer.client.{ ImageRecognitionResult, ImageRecognitionRequest, Recognizer }

class CheckVehicleRegistrationTest extends WordSpec with MustMatchers with BeforeAndAfterAll {
  implicit val testSystem = ActorSystem("CheckVehicleRegistrationTest")

  val dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS")
  val jan_25_2002 = dateFormat.parse("25-01-2002 00:00:00.000").getTime

  override protected def afterAll() {
    testSystem.shutdown()
    super.afterAll()
  }

  "CheckVehicleRegistration license check" must {

    "send registration to recognizer" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        90,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002)

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Right(vr)))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(0)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(2)
      recognizeResults.head must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults.last must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      testSystem.stop(testActor)
    }

    "fail for supported country when intrada returns a different result" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        90,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002, licensePlate = Some("12ABCD"), countryCode = Some("BE"))

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      val intradaResult = makeVR("test1", jan_25_2002, licensePlate = Some("ABCD12"), countryCode = Some("BE"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Right(intradaResult)))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(1)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(2)
      recognizeResults.head must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults.last must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = intradaResult.license,
        rawLicense = intradaResult.rawLicense,
        country = intradaResult.country,
        errorMsg = None))
      testSystem.stop(testActor)
    }
    "succeed for unsupported country when intrada returns a different result with low confidence" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        90,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002, countryCode = Some("IT"))

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      val intradaResult = makeVR("test1", jan_25_2002, licensePlate = Some("ABCD12"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Right(intradaResult)))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(0)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(2)
      recognizeResults.head must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults.last must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = intradaResult.license,
        rawLicense = intradaResult.rawLicense,
        country = intradaResult.country,
        errorMsg = None))
      testSystem.stop(testActor)
    }
    "fail for unsupported country when intrada returns a different result with high confidence" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        40,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002, countryCode = Some("IT"))

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      val intradaResult = makeVR("test1", jan_25_2002, licensePlate = Some("ABCD12"), countryCode = Some("IT"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Right(intradaResult)))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(1)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(2)
      recognizeResults.head must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults.last must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = intradaResult.license,
        rawLicense = intradaResult.rawLicense,
        country = intradaResult.country,
        errorMsg = None))
      testSystem.stop(testActor)
    }
    "succeed for unsupported country when intrada returns the same result with high confidence" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        90,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002, countryCode = Some("IT"))

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      val intradaResult = vr
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Right(intradaResult)))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(0)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(2)
      recognizeResults.head must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults.last must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = intradaResult.license,
        rawLicense = intradaResult.rawLicense,
        country = intradaResult.country,
        errorMsg = None))
      testSystem.stop(testActor)
    }

    "fail for supported country when intrada fails" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        90,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002, countryCode = Some("CH"))

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Left("No plate found")))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(3)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(2)
      recognizeResults.head must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults.last must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = None,
        rawLicense = None,
        country = None,
        errorMsg = Some("No plate found")))
      testSystem.stop(testActor)
    }
    "succeed for unsupported country when intrada fails" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        90,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002, countryCode = Some("FR"))

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Left("No plate found")))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(0)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(2)
      recognizeResults.head must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults.last must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = None,
        rawLicense = None,
        country = None,
        errorMsg = Some("No plate found")))
      testSystem.stop(testActor)
    }

    "succeed for unknown country at start" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        90,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002, countryCode = None)

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      val intradaResult = makeVR("test1", jan_25_2002, countryCode = Some("NL"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Right(intradaResult)))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(0)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(2)
      recognizeResults.head must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults.last must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = intradaResult.license,
        rawLicense = intradaResult.rawLicense,
        country = intradaResult.country,
        errorMsg = None))
      testSystem.stop(testActor)
    }

    "fail for unknown country by intrada when supported country" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        90,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002, countryCode = Some("CH"))

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      val intradaResult = makeVR("test1", jan_25_2002, countryCode = None)
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Right(intradaResult)))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(2)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(2)
      recognizeResults.head must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults.last must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = intradaResult.license,
        rawLicense = intradaResult.rawLicense,
        country = intradaResult.country,
        errorMsg = None))
      testSystem.stop(testActor)
    }

    "fail if ARH and Intrada produce different (supported) countries" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        90,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002, countryCode = Some("CH"))

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      val intradaResult = makeVR("test1", jan_25_2002, countryCode = Some("DE"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Right(intradaResult)))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(2)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(2)
      recognizeResults.head must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults.last must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = intradaResult.license,
        rawLicense = intradaResult.rawLicense,
        country = intradaResult.country,
        errorMsg = None))
      testSystem.stop(testActor)
    }
    "fail for different country for not supported country with high confidence" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        40,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002, countryCode = Some("IT"))

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      val intradaResult = makeVR("test1", jan_25_2002, countryCode = Some("DE"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Right(intradaResult)))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(2)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(2)
      recognizeResults.head must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults.last must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = intradaResult.license,
        rawLicense = intradaResult.rawLicense,
        country = intradaResult.country,
        errorMsg = None))
      testSystem.stop(testActor)
    }
    "fail for different country for not supported country with low confidence" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        90,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002, Some("IT"))

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      val intradaResult = makeVR("test1", jan_25_2002, countryCode = Some("DE"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Right(intradaResult)))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(0)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(2)
      recognizeResults.head must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults.last must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = intradaResult.license,
        rawLicense = intradaResult.rawLicense,
        country = intradaResult.country,
        errorMsg = None))
      testSystem.stop(testActor)
    }
  }

  "CheckVehicleRegistration SecondPass Rule 1" must {
    "skip Check: when country=BE intrada=BE and licenses are different" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        90,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002, countryCode = Some("BE"))

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      val intradaResult = makeVR("test1", jan_25_2002, licensePlate = Some("ABCD12"), countryCode = Some("BE"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Right(intradaResult)))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(1)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(2)
      recognizeResults.head must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults.last must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = intradaResult.license,
        rawLicense = intradaResult.rawLicense,
        country = intradaResult.country,
        errorMsg = None))
      testSystem.stop(testActor)
    }
    "fail Check when country=BE intrada<>BE and ARH fails" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        90,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002, countryCode = Some("BE"))

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      val intradaResult = makeVR("test1", jan_25_2002, licensePlate = Some("ABCD12"), countryCode = Some("DE"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Right(intradaResult)))
      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.ARH, vr))

      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.ARH,
        inputVehicleMetadata = vr,
        recognitionResult = Left("Plate not found")))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(3)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(3)
      recognizeResults(0) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults(1) must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = intradaResult.license,
        rawLicense = intradaResult.rawLicense,
        country = intradaResult.country,
        errorMsg = None))
      recognizeResults(2) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH2,
        license = None,
        rawLicense = None,
        country = None,
        errorMsg = Some("Plate not found")))
      testSystem.stop(testActor)
    }

    "skip Check when country=BE intrada<>BE and ARH license are different" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        90,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002, countryCode = Some("BE"))

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      val intradaResult = makeVR("test1", jan_25_2002, licensePlate = Some("ABCD12"), countryCode = Some("DE"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Right(intradaResult)))
      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.ARH, vr))

      val arh2Result = makeVR("test1", jan_25_2002, licensePlate = Some("ABCD12"), countryCode = Some("BE"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.ARH,
        inputVehicleMetadata = vr,
        recognitionResult = Right(arh2Result)))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(3)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(3)
      recognizeResults(0) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults(1) must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = intradaResult.license,
        rawLicense = intradaResult.rawLicense,
        country = intradaResult.country,
        errorMsg = None))
      recognizeResults(2) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH2,
        license = arh2Result.license,
        rawLicense = arh2Result.rawLicense,
        country = arh2Result.country,
        errorMsg = None))
      testSystem.stop(testActor)
    }

    "skip Check when country=BE intrada<>BE and ARH license are equal but size is 6" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        90,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002, countryCode = Some("BE"))

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      val intradaResult = makeVR("test1", jan_25_2002, licensePlate = Some("ABCD12"), countryCode = Some("DE"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Right(intradaResult)))
      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.ARH, vr))

      val arh2Result = makeVR("test1", jan_25_2002, countryCode = Some("BE"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.ARH,
        inputVehicleMetadata = vr,
        recognitionResult = Right(arh2Result)))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(3)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(3)
      recognizeResults(0) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults(1) must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = intradaResult.license,
        rawLicense = intradaResult.rawLicense,
        country = intradaResult.country,
        errorMsg = None))
      recognizeResults(2) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH2,
        license = arh2Result.license,
        rawLicense = arh2Result.rawLicense,
        country = arh2Result.country,
        errorMsg = None))
      testSystem.stop(testActor)
    }

    "succeed Check when country=BE intrada<>BE and ARH license are equal and size is 7" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        90,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002, licensePlate = Some("1234567"), countryCode = Some("BE"))

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      val intradaResult = makeVR("test1", jan_25_2002, licensePlate = Some("ABCD12"), countryCode = Some("DE"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Right(intradaResult)))
      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.ARH, vr))

      val arh2Result = makeVR("test1", jan_25_2002, licensePlate = Some("1234567"), countryCode = Some("BE"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.ARH,
        inputVehicleMetadata = vr,
        recognitionResult = Right(arh2Result)))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(0)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(3)
      recognizeResults(0) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults(1) must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = intradaResult.license,
        rawLicense = intradaResult.rawLicense,
        country = intradaResult.country,
        errorMsg = None))
      recognizeResults(2) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH2,
        license = arh2Result.license,
        rawLicense = arh2Result.rawLicense,
        country = arh2Result.country,
        errorMsg = None))
      testSystem.stop(testActor)
    }
  }

  "CheckVehicleRegistration SecondPass Rule 2" must {
    "Succeed when country=DE and ARH license are different" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        90,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002, countryCode = Some("DE"))

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      val intradaResult = makeVR("test1", jan_25_2002, licensePlate = Some("ABCD12"), countryCode = Some("DE"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Right(intradaResult)))
      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.ARH, vr))

      val arh2Result = makeVR("test1", jan_25_2002, licensePlate = Some("ABCD12"), countryCode = Some("DE"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.ARH,
        inputVehicleMetadata = vr,
        recognitionResult = Right(arh2Result)))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(1)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(3)
      recognizeResults(0) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults(1) must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = intradaResult.license,
        rawLicense = intradaResult.rawLicense,
        country = intradaResult.country,
        errorMsg = None))
      recognizeResults(2) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH2,
        license = arh2Result.license,
        rawLicense = arh2Result.rawLicense,
        country = arh2Result.country,
        errorMsg = None))
      testSystem.stop(testActor)
    }

    "fail when country=DE and ARH fails" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        90,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002, countryCode = Some("DE"))

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      val intradaResult = makeVR("test1", jan_25_2002, licensePlate = Some("ABCD12"), countryCode = Some("DE"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Right(intradaResult)))
      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.ARH, vr))

      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.ARH,
        inputVehicleMetadata = vr,
        recognitionResult = Left("Plate not found")))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(1)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(3)
      recognizeResults(0) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults(1) must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = intradaResult.license,
        rawLicense = intradaResult.rawLicense,
        country = intradaResult.country,
        errorMsg = None))
      recognizeResults(2) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH2,
        license = None,
        rawLicense = None,
        country = None,
        errorMsg = Some("Plate not found")))
      testSystem.stop(testActor)
    }

    "Succeed when country=DE and ARH license are equal but confidence < 20" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        90,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002, countryCode = Some("DE"))

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      val intradaResult = makeVR("test1", jan_25_2002, licensePlate = Some("ABCD12"), countryCode = Some("DE"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Right(intradaResult)))
      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.ARH, vr))

      val arh2Result = makeVR("test1", jan_25_2002).copy(country = Some(new ValueWithConfidence[String]("DE", 19)))

      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.ARH,
        inputVehicleMetadata = vr,
        recognitionResult = Right(arh2Result)))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(1)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(3)
      recognizeResults(0) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults(1) must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = intradaResult.license,
        rawLicense = intradaResult.rawLicense,
        country = intradaResult.country,
        errorMsg = None))
      recognizeResults(2) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH2,
        license = arh2Result.license,
        rawLicense = arh2Result.rawLicense,
        country = arh2Result.country,
        errorMsg = None))
      testSystem.stop(testActor)
    }

    "Succeed when country=DE and ARH=DE and license are equal and confidence > 20" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        90,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002, countryCode = Some("DE"))

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      val intradaResult = makeVR("test1", jan_25_2002, licensePlate = Some("ABCD12"), countryCode = Some("DE"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Right(intradaResult)))
      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.ARH, vr))

      val arh2Result = makeVR("test1", jan_25_2002, countryCode = Some("DE"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.ARH,
        inputVehicleMetadata = vr,
        recognitionResult = Right(arh2Result)))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(0)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(3)
      recognizeResults(0) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults(1) must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = intradaResult.license,
        rawLicense = intradaResult.rawLicense,
        country = intradaResult.country,
        errorMsg = None))
      recognizeResults(2) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH2,
        license = arh2Result.license,
        rawLicense = arh2Result.rawLicense,
        country = arh2Result.country,
        errorMsg = None))
      testSystem.stop(testActor)
    }
  }

  "CheckVehicleRegistration SecondPass Rule 3" must {
    "skip Check: when country=FR intrada=NL and license are different" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        40,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002, countryCode = Some("FR"))

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      val intradaResult = makeVR("test1", jan_25_2002, licensePlate = Some("ABCD12"), countryCode = Some("NL"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Right(intradaResult)))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](10000.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(3)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(2)
      recognizeResults.head must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults.last must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = intradaResult.license,
        rawLicense = intradaResult.rawLicense,
        country = intradaResult.country,
        errorMsg = None))
      testSystem.stop(testActor)
    }
    "fail Check when country=FR intrada<>NL and ARH fails" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        40,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002, countryCode = Some("FR"))

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      val intradaResult = makeVR("test1", jan_25_2002, licensePlate = Some("ABCD12"), countryCode = Some("DE"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Right(intradaResult)))
      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.ARH, vr))

      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.ARH,
        inputVehicleMetadata = vr,
        recognitionResult = Left("Plate not found")))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(3)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(3)
      recognizeResults(0) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults(1) must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = intradaResult.license,
        rawLicense = intradaResult.rawLicense,
        country = intradaResult.country,
        errorMsg = None))
      recognizeResults(2) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH2,
        license = None,
        rawLicense = None,
        country = None,
        errorMsg = Some("Plate not found")))
      testSystem.stop(testActor)
    }
    "Check when country=FR intrada<>NL and ARH license are different" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        40,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002, countryCode = Some("FR"))

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      val intradaResult = makeVR("test1", jan_25_2002, licensePlate = Some("ABCD12"), countryCode = Some("FR"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Right(intradaResult)))
      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.ARH, vr))

      val arh2Result = makeVR("test1", jan_25_2002, licensePlate = Some("ABCD12"), countryCode = Some("FR"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.ARH,
        inputVehicleMetadata = vr,
        recognitionResult = Right(arh2Result)))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(1)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(3)
      recognizeResults(0) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults(1) must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = intradaResult.license,
        rawLicense = intradaResult.rawLicense,
        country = intradaResult.country,
        errorMsg = None))
      recognizeResults(2) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH2,
        license = arh2Result.license,
        rawLicense = arh2Result.rawLicense,
        country = arh2Result.country,
        errorMsg = None))
      testSystem.stop(testActor)
    }
    "Check when country=FR intrada<>NL and ARH license are equal but contry is differend" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        40,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002, countryCode = Some("FR"))

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      val intradaResult = makeVR("test1", jan_25_2002, licensePlate = Some("ABCD12"), countryCode = Some("FR"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Right(intradaResult)))
      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.ARH, vr))

      val arh2Result = makeVR("test1", jan_25_2002, countryCode = Some("DE"))

      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.ARH,
        inputVehicleMetadata = vr,
        recognitionResult = Right(arh2Result)))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(1)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(3)
      recognizeResults(0) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults(1) must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = intradaResult.license,
        rawLicense = intradaResult.rawLicense,
        country = intradaResult.country,
        errorMsg = None))
      recognizeResults(2) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH2,
        license = arh2Result.license,
        rawLicense = arh2Result.rawLicense,
        country = arh2Result.country,
        errorMsg = None))
      testSystem.stop(testActor)
    }
    "correct Check country=FR intrada<>NL and ARH license are equal and country=FR" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        40,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002, countryCode = Some("FR"))

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      val intradaResult = makeVR("test1", jan_25_2002, licensePlate = Some("ABCD12"), countryCode = Some("DE"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Right(intradaResult)))
      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.ARH, vr))

      val arh2Result = makeVR("test1", jan_25_2002, countryCode = Some("FR"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.ARH,
        inputVehicleMetadata = vr,
        recognitionResult = Right(arh2Result)))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(0)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(3)
      recognizeResults(0) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults(1) must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = intradaResult.license,
        rawLicense = intradaResult.rawLicense,
        country = intradaResult.country,
        errorMsg = None))
      recognizeResults(2) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH2,
        license = arh2Result.license,
        rawLicense = arh2Result.rawLicense,
        country = arh2Result.country,
        errorMsg = None))
      testSystem.stop(testActor)
    }
  }
  "CheckVehicleRegistration SecondPass Rule 4" must {
    "Check when country=NL and ARH license are different" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        90,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002)

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      val intradaResult = makeVR("test1", jan_25_2002, licensePlate = Some("ABCD12"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Right(intradaResult)))
      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.ARH, vr))

      val arh2Result = makeVR("test1", jan_25_2002, licensePlate = Some("ABCD12"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.ARH,
        inputVehicleMetadata = vr,
        recognitionResult = Right(arh2Result)))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(1)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(3)
      recognizeResults(0) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults(1) must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = intradaResult.license,
        rawLicense = intradaResult.rawLicense,
        country = intradaResult.country,
        errorMsg = None))
      recognizeResults(2) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH2,
        license = arh2Result.license,
        rawLicense = arh2Result.rawLicense,
        country = arh2Result.country,
        errorMsg = None))
      testSystem.stop(testActor)
    }
    "fail Check when country=NL and ARH fails" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        90,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002)

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      val intradaResult = makeVR("test1", jan_25_2002, licensePlate = Some("ABCD12"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Right(intradaResult)))
      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.ARH, vr))

      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.ARH,
        inputVehicleMetadata = vr,
        recognitionResult = Left("Plate not found")))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(1)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(3)
      recognizeResults(0) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults(1) must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = intradaResult.license,
        rawLicense = intradaResult.rawLicense,
        country = intradaResult.country,
        errorMsg = None))
      recognizeResults(2) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH2,
        license = None,
        rawLicense = None,
        country = None,
        errorMsg = Some("Plate not found")))
      testSystem.stop(testActor)
    }
    "Check when country=NL and ARH license are equal but contry is differend" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        90,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002)

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      val intradaResult = makeVR("test1", jan_25_2002, licensePlate = Some("ABCD12"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Right(intradaResult)))
      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.ARH, vr))

      val arh2Result = makeVR("test1", jan_25_2002, countryCode = Some("DE"))

      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.ARH,
        inputVehicleMetadata = vr,
        recognitionResult = Right(arh2Result)))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(1)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(3)
      recognizeResults(0) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults(1) must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = intradaResult.license,
        rawLicense = intradaResult.rawLicense,
        country = intradaResult.country,
        errorMsg = None))
      recognizeResults(2) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH2,
        license = arh2Result.license,
        rawLicense = arh2Result.rawLicense,
        country = arh2Result.country,
        errorMsg = None))
      testSystem.stop(testActor)
    }
    "correct Check country=NL and ARH license are equal and country=NL" in {
      val recognizeImage = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckVehicleRegistration(recognizeImage.ref,
        1.day,
        Seq("NL", "DE", "BE", "CH"),
        90,
        recognizeWithARHAgain = true)))
      val vr = makeVR("test1", jan_25_2002)

      val sender = TestProbe()
      sender.send(testActor, new RegistrationImageCheckRequest(reference = "test1", registration = vr))

      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.INTRADA, vr))

      val intradaResult = makeVR("test1", jan_25_2002, licensePlate = Some("ABCD12"), countryCode = Some("DE"))
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.INTRADA,
        inputVehicleMetadata = vr,
        recognitionResult = Right(intradaResult)))
      recognizeImage.expectMsg(new ImageRecognitionRequest(Recognizer.ARH, vr))

      val arh2Result = makeVR("test1", jan_25_2002)
      recognizeImage.send(testActor, new ImageRecognitionResult(
        recognizer = Recognizer.ARH,
        inputVehicleMetadata = vr,
        recognitionResult = Right(arh2Result)))

      val response = sender.expectMsgType[RegistrationImageCheckResponse](1.second)
      response.reference must be("test1")
      response.registration must be(vr)
      response.result.getErrors() must be(0)
      val recognizeResults = response.result.getRecognitionResults()
      recognizeResults.size must be(3)
      recognizeResults(0) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH1,
        license = vr.license,
        rawLicense = vr.rawLicense,
        country = vr.country,
        errorMsg = None))
      recognizeResults(1) must be(new RecognitionResult(recognizerId = RecognitionResult.INTRADA,
        license = intradaResult.license,
        rawLicense = intradaResult.rawLicense,
        country = intradaResult.country,
        errorMsg = None))
      recognizeResults(2) must be(new RecognitionResult(recognizerId = RecognitionResult.ARH2,
        license = arh2Result.license,
        rawLicense = arh2Result.rawLicense,
        country = arh2Result.country,
        errorMsg = None))
      testSystem.stop(testActor)
    }
  }

  def makeVR(id: String,
             entryTime: Long,
             countryCode: Option[String] = Some("NL"),
             licensePlate: Option[String] = Some("AB12CD")) =
    VehicleMetadata(Lane("1",
      "lane1",
      "gantry1",
      "route1",
      0.0f,
      0.0f),
      id,
      entryTime,
      Some(new Date(entryTime).toString),
      licensePlate.map(lp ⇒ ValueWithConfidence(lp, 42)),
      country = countryCode.map(cc ⇒ ValueWithConfidence(cc, 42)),
      images = Seq(),
      NMICertificate = "",
      applChecksum = "",
      serialNr = "SN1234")
}