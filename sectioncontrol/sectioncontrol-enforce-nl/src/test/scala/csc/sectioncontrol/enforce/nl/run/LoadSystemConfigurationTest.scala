/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.nl.run

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.curator.CuratorTestServer
import csc.sectioncontrol.enforce.nl.CuratorHelper
import csc.curator.utils.Curator
import java.util.{ Calendar, GregorianCalendar, TimeZone }
import csc.sectioncontrol.messages.EsaProviderType
import csc.sectioncontrol.storage
import storage._
import storage.ZkCorridor
import storage.ZkSection
import storage.ZkSystem

class LoadSystemConfigurationTest extends WordSpec with MustMatchers with CuratorTestServer {
  def aug09HourMillis(hour: Int): Long = new GregorianCalendar(2015, Calendar.AUGUST, 9, hour, 0).getTimeInMillis

  private var curator: Curator = _

  val timeZone = TimeZone.getTimeZone("Europe/Amsterdam")
  val aug_09 = aug09HourMillis(0)
  val aug_10 = aug09HourMillis(24)
  val aug_11 = aug09HourMillis(48)
  val systemId = "N62-test"
  val corridorId = "cor1"

  override def beforeEach() {
    super.beforeEach()
    curator = CuratorHelper.create(this, clientScope)
    createConfig()
  }

  override def afterEach() {
    curator.curator.foreach(c ⇒ c.close())
  }

  "getCorridorSchedules" must {
    "return current and historic schedules and current start after backup" in {
      //add one schedule
      val current = createSchedule("nr2", 15, 0, 20, 00, 100)
      val currentPath = Paths.Corridors.getSchedulesPath(systemId, corridorId)
      curator.put(currentPath, List(current))
      //add two historic
      val old = createSchedule("nr1", 7, 0, 15, 0, 130)
      //this backup is active during the backup time
      makeScheduleBackup(systemId, corridorId, List(old), aug09HourMillis(8))
      //this backup is not active during the backup time
      makeScheduleBackup(systemId, corridorId, List(current), aug09HourMillis(12))

      val schedules = LoadSystemConfiguration.getCorridorSchedules(systemId, corridorId, aug_09, aug_10, timeZone, curator)
      //println(schedules)
      schedules.size must be(2)
      //current schedule
      schedules(0).enforcedSpeedLimit must be(100)
      schedules(0).startTime must be(aug09HourMillis(15))
      schedules(0).endTime must be(aug09HourMillis(20))
      //old schedule
      schedules(1).enforcedSpeedLimit must be(130)
      schedules(1).startTime must be(aug09HourMillis(7))
      schedules(1).endTime must be(aug09HourMillis(8))
    }
    "return current and historic schedules and current start before backup" in {
      //add one schedule
      val current = createSchedule("nr2", 10, 00, 20, 00, 100)
      val currentPath = Paths.Corridors.getSchedulesPath(systemId, corridorId)
      curator.put(currentPath, List(current))
      //add two historic
      val old = createSchedule("nr1", 7, 00, 15, 00, 130)
      //this backup is active during the backup time
      makeScheduleBackup(systemId, corridorId, List(old), aug09HourMillis(8))
      //this backup is active during the backup time
      makeScheduleBackup(systemId, corridorId, List(current), aug09HourMillis(12))

      val schedules = LoadSystemConfiguration.getCorridorSchedules(systemId, corridorId, aug_09, aug_10, timeZone, curator)
      //println(schedules)
      schedules.size must be(3)
      //current schedule
      schedules(0).enforcedSpeedLimit must be(100)
      schedules(0).startTime must be(aug09HourMillis(12))
      schedules(0).endTime must be(aug09HourMillis(20))
      //old schedule
      schedules(1).enforcedSpeedLimit must be(130)
      schedules(1).startTime must be(aug09HourMillis(7))
      schedules(1).endTime must be(aug09HourMillis(8))
      //current backup
      schedules(2).enforcedSpeedLimit must be(100)
      schedules(2).startTime must be(aug09HourMillis(10))
      schedules(2).endTime must be(aug09HourMillis(12))
    }
    "return only current when current starts day after backup" in {
      //add one schedule
      val current = createSchedule("nr2", 10, 00, 20, 00, 100)
      val currentPath = Paths.Corridors.getSchedulesPath(systemId, corridorId)
      curator.put(currentPath, List(current))
      //add two historic
      val old = createSchedule("nr1", 7, 00, 15, 00, 130)
      //this backup is active during the backup time
      makeScheduleBackup(systemId, corridorId, List(old), aug09HourMillis(8))
      //this backup is active during the backup time
      makeScheduleBackup(systemId, corridorId, List(current), aug09HourMillis(12))

      val schedules = LoadSystemConfiguration.getCorridorSchedules(systemId, corridorId, aug_10, aug_11, timeZone, curator)
      //println(schedules)
      schedules.size must be(1)
      //current schedule
      schedules(0).enforcedSpeedLimit must be(100)
      schedules(0).startTime must be(aug09HourMillis(10 + 24))
      schedules(0).endTime must be(aug09HourMillis(20 + 24))
    }
  }

  def createConfig() {
    val sys = ZkSystem(id = systemId,
      name = systemId,
      title = "test",
      location = ZkSystemLocation(description = "",
        viewingDirection = None,
        region = "",
        roadNumber = "",
        roadPart = "",
        systemLocation = ""),
      violationPrefixId = "n062",
      orderNumber = 1,
      maxSpeed = 100,
      compressionFactor = 90,
      retentionTimes = ZkSystemRetentionTimes(nrDaysKeepTrafficData = 5,
        nrDaysKeepViolations = 5,
        nrDaysRemindCaseFiles = 2),
      pardonTimes = ZkSystemPardonTimes(),
      esaProviderType = EsaProviderType.NoProvider,
      mtmRouteId = "",
      minimumViolationTimeSeparation = 0,
      serialNumber = SerialNumber("SN1234"))
    curator.put(Paths.Systems.getConfigPath(systemId), sys)
    val corridor = ZkCorridor(id = corridorId,
      name = corridorId,
      roadType = 1,
      serviceType = ServiceType.SectionControl,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = false,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = ZkPSHTMIdentification(useYear = false,
        useMonth = false,
        useDay = false,
        useReportingOfficerId = false,
        useNumberOfTheDay = false,
        useLocationCode = false,
        useHHMCode = false,
        useTypeViolation = false,
        useFixedCharacter = false,
        fixedCharacter = "A",
        elementOrderString = None),
      info = ZkCorridorInfo(corridorId = 10,
        locationCode = "10",
        reportId = "",
        reportHtId = "",
        reportPerformance = true,
        locationLine1 = "",
        locationLine2 = None),
      startSectionId = "S1",
      endSectionId = "S1",
      allSectionIds = Seq("S1"),
      direction = ZkDirection(directionFrom = "",
        directionTo = ""),
      radarCode = 9,
      roadCode = 0,
      dutyType = "K0",
      deploymentCode = "0",
      codeText = "")
    curator.put(Paths.Corridors.getConfigPath(systemId, corridorId), corridor)
    val section = ZkSection(id = "S1",
      systemId = systemId,
      name = "S1",
      length = 8000,
      startGantryId = "E1",
      endGantryId = "X1",
      matrixBoards = Nil,
      msiBlackList = Nil)
    curator.put(Paths.Sections.getConfigPath(systemId, corridor.startSectionId), section)
  }

  def createSchedule(id: String,
                     fromPeriodHour: Int,
                     fromPeriodMinute: Int,
                     toPeriodHour: Int,
                     toPeriodMinute: Int,
                     speedLimit: Int): ZkSchedule = {
    ZkSchedule(id = id,
      indicationRoadworks = ZkIndicationType.False,
      indicationDanger = ZkIndicationType.False,
      indicationActualWork = ZkIndicationType.False,
      invertDrivingDirection = ZkIndicationType.False,
      fromPeriodHour = fromPeriodHour,
      fromPeriodMinute = fromPeriodMinute,
      toPeriodHour = toPeriodHour,
      toPeriodMinute = toPeriodMinute,
      speedLimit = speedLimit,
      signSpeed = 100,
      signIndicator = false,
      speedIndicatorType = None,
      supportMsgBoard = true)
  }

  private def makeScheduleBackup(systemId: String, corridorId: String, schedule: List[ZkSchedule], backupTime: Long) {
    val backupPath = Paths.Corridors.getScheduleHistoryPath(systemId, corridorId) + "/" + backupTime.toString
    curator.put(backupPath, schedule)
  }

}