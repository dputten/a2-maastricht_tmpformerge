/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.nl.qualify

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import java.util.{ Calendar, GregorianCalendar }
import csc.sectioncontrol.enforce.speedlog.{ SpeedEvent, SpeedSettingOverTime }
import csc.sectioncontrol.messages.StatsDuration
import csc.sectioncontrol.enforce._
import common.nl.violations.{ SpeedIndicator, CorridorSchedule }
import nl.casefiles.{ HHMVS14, TCVS33 }
import csc.sectioncontrol.enforce.MtmSpeedSetting
import csc.sectioncontrol.enforce.MtmSpeedSettings
import csc.sectioncontrol.common.DayUtility
import csc.sectioncontrol.enforce.common.nl.casefiles.HHMVS20

/**
 *
 * @author Maarten Hazewinkel
 */
class MtmInfoFormatterTest extends WordSpec with MustMatchers {

  def aug09HourMillis(hour: Int): Long = new GregorianCalendar(2002, Calendar.JANUARY, 25, hour, 0).getTimeInMillis

  val aug_09 = aug09HourMillis(0)
  val aug_10 = aug09HourMillis(24)

  val aug_09_0900 = aug09HourMillis(9)
  val aug_09_1000 = aug09HourMillis(10)
  val aug_09_1100 = aug09HourMillis(11)
  val aug_09_1200 = aug09HourMillis(12)
  val aug_09_1300 = aug09HourMillis(13)
  val aug_09_1500 = aug09HourMillis(15)
  val aug_09_2000 = aug09HourMillis(20)

  val speed100 = Right[SpeedIndicatorStatus.Value, Option[Int]](Some(100))
  val speed90 = Right[SpeedIndicatorStatus.Value, Option[Int]](Some(90))
  val speed50 = Right[SpeedIndicatorStatus.Value, Option[Int]](Some(50))
  val speedInd = Left[SpeedIndicatorStatus.Value, Option[Int]](SpeedIndicatorStatus.Undetermined)
  val speedBl = Right[SpeedIndicatorStatus.Value, Option[Int]](None)
  val speedSb = Left[SpeedIndicatorStatus.Value, Option[Int]](SpeedIndicatorStatus.StandBy)

  val speedData = Map(1 -> SpeedSettingOverTime(1, Seq(MockEvent(aug_09, 1, speedBl, 3),
    MockEvent(aug_09_1200, 1, speedInd),
    MockEvent(aug_09_1500, 1, speed90),
    MockEvent(aug_09_1300, 1, speed100),
    MockEvent(aug_09_1000, 1, speed50))),
    2 -> SpeedSettingOverTime(2, Seq(MockEvent(aug_09, 1, speedBl, 3),
      MockEvent(aug_09_1500, 1, speedBl, 2),
      MockEvent(aug_09_1000, 1, speed90, 2),
      MockEvent(aug_09_2000, 1, speed100, 2),
      MockEvent(aug_09_0900, 1, speed50, 2))),
    3 -> SpeedSettingOverTime(3, Seq(MockEvent(aug_09, 1, speedBl, 3))))
  //4 -> SpeedSettingOverTime(4, Seq(MockEvent(aug_09, 1, speedSb, 3))))

  val speedIndicator = SpeedIndicator(signIndicator = false, speedIndicatorType = None)
  val schedules = Seq(CorridorSchedule(corridorId = 1, enforcedSpeedLimit = 100, indicatedSpeedLimit = None, pshTmIdFormat = "", startTime = aug_09, endTime = aug_10, speedIndicator = speedIndicator),
    CorridorSchedule(corridorId = 2, enforcedSpeedLimit = 120, indicatedSpeedLimit = None, pshTmIdFormat = "", startTime = aug_09, endTime = aug_10, speedIndicator = speedIndicator),
    CorridorSchedule(corridorId = 3, enforcedSpeedLimit = 100, indicatedSpeedLimit = None, pshTmIdFormat = "", startTime = aug_09, endTime = aug_09_1100, speedIndicator = speedIndicator),
    CorridorSchedule(corridorId = 4, enforcedSpeedLimit = 100, indicatedSpeedLimit = None, pshTmIdFormat = "", startTime = aug_09_1200, endTime = aug_10, speedIndicator = speedIndicator))

  val locations = Map(1 -> "test-eg33-1", 2 -> "test-eg33-2", 3 -> "test-eg33-3")

  "MtmInfoFormatter" must {
    "produce a correct onbepaald and end date format without schedule when using IRS TC-VS versie 3.3" in {
      val sd = SpeedSettingOverTime(4, Seq(MockEvent(aug_09, 1, speed100, 3)), Seq())
      val mtmFileText = "# Locatie\r\n" +
        "# test-eg33-3\r\n" +
        "# Datum 2002/01/25\r\n" +
        "#\r\n" +
        "#locatie    |tijdstip           |maxsnelheid|\r\n" +
        "|test-eg33-3|2002/01/25 00:00:00|onbepaald  |\r\n" +
        "|test-eg33-3|2002/01/25 23:59:59|onbepaald  |\r\n"
      new String(MtmInfoFormatter.makeExportFile(locations(3), aug_09, sd, Seq(), TCVS33()), "ISO-8859-1") must be(mtmFileText)
    }
    "produce a correct standby and end date format without schedule when using IRS TC-VS versie 3.3" in {
      val sd = SpeedSettingOverTime(4, Seq(MockEvent(aug_09, 1, speedSb, 3)), Seq())
      val mtmFileText = "# Locatie\r\n" +
        "# test-eg33-3\r\n" +
        "# Datum 2002/01/25\r\n" +
        "#\r\n" +
        "#locatie    |tijdstip           |maxsnelheid|\r\n" +
        "|test-eg33-3|2002/01/25 00:00:00|standby    |\r\n" +
        "|test-eg33-3|2002/01/25 23:59:59|standby    |\r\n"
      new String(MtmInfoFormatter.makeExportFile(locations(3), aug_09, sd, Seq(), TCVS33()), "ISO-8859-1") must be(mtmFileText)
    }

    "produce a correct standby and end date format when using IRS TC-VS versie 3.3" in {
      val sd = SpeedSettingOverTime(4, Seq(MockEvent(aug_09, 1, speedSb, 3)), Seq())
      val mtmFileText = "# Locatie\r\n" +
        "# test-eg33-3\r\n" +
        "# Datum 2002/01/25\r\n" +
        "#\r\n" +
        "#locatie    |tijdstip           |maxsnelheid|\r\n" +
        "|test-eg33-3|2002/01/25 00:00:00|standby    |\r\n" +
        "|test-eg33-3|2002/01/25 23:59:59|standby    |\r\n"
      new String(MtmInfoFormatter.makeExportFile(locations(3), aug_09, sd, schedules.filter(_.corridorId == 2), TCVS33()), "ISO-8859-1") must be(
        mtmFileText)
    }

    "produce a correct standby and end date format when using IRS HHM-VS 2.0" in {
      val sd = SpeedSettingOverTime(4, Seq(MockEvent(aug_09, 1, speedSb, 3)), Seq())
      val mtmFileText = "# Locatie\r\n" +
        "# test-eg33-3\r\n" +
        "# Datum 2002/01/25\r\n" +
        "#\r\n" +
        "#locatie    |tijdstip           |maxsnelheid|\r\n" +
        "|test-eg33-3|2002/01/25 00:00:00|stand-by   |\r\n" +
        "|test-eg33-3|2002/01/25 23:59:59|stand-by   |\r\n"
      new String(MtmInfoFormatter.makeExportFile(locations(3), aug_09, sd, schedules.filter(_.corridorId == 2), HHMVS20()), "ISO-8859-1") must be(
        mtmFileText)
    }

    "produce a correct standby and end date format when using IRS HHM-VS 1.4" in {
      val sd = SpeedSettingOverTime(4, Seq(MockEvent(aug_09, 1, speedSb, 3)), Seq())
      val mtmFileText = "# Locatie\r\n" +
        "# test-eg33-3\r\n" +
        "# Datum 2002/01/25\r\n" +
        "#\r\n" +
        "#locatie    |tijdstip           |maxsnelheid|\r\n" +
        "|test-eg33-3|2002/01/25 00:00:00|stand-by   |\r\n" +
        "|test-eg33-3|2002/01/25 24:00:00|stand-by   |\r\n"
      new String(MtmInfoFormatter.makeExportFile(locations(3), aug_09, sd, schedules.filter(_.corridorId == 2), HHMVS14()), "ISO-8859-1") must be(
        mtmFileText)
    }

    "format an export file" in {
      val mtmFileText = "# Locatie\r\n" +
        "# test-eg33-1\r\n" +
        "# Datum 2002/01/25\r\n" +
        "#\r\n" +
        "#locatie    |tijdstip           |maxsnelheid|\r\n" +
        "|test-eg33-1|2002/01/25 00:00:00|100        |\r\n" +
        "|test-eg33-1|2002/01/25 10:00:00|50         |\r\n" +
        "|test-eg33-1|2002/01/25 12:00:00|onbepaald  |\r\n" +
        "|test-eg33-1|2002/01/25 13:00:00|100        |\r\n" +
        "|test-eg33-1|2002/01/25 15:00:00|90         |\r\n" +
        "|test-eg33-1|2002/01/25 24:00:00|90         |\r\n"
      MtmInfoFormatter.makeExportFile(locations(1), aug_09, speedData(1), schedules.filter(_.corridorId == 1), new HHMVS14()) must be(
        mtmFileText.getBytes("ISO-8859-1"))
    }

    "produce a correct export when speedIndicators are blank" in {
      val mtmFileText = "# Locatie\r\n" +
        "# test-eg33-3\r\n" +
        "# Datum 2002/01/25\r\n" +
        "#\r\n" +
        "#locatie    |tijdstip           |maxsnelheid|\r\n" +
        "|test-eg33-3|2002/01/25 00:00:00|100        |\r\n" +
        "|test-eg33-3|2002/01/25 11:00:00|onbepaald  |\r\n" +
        "|test-eg33-3|2002/01/25 24:00:00|onbepaald  |\r\n"
      new String(MtmInfoFormatter.makeExportFile(locations(3), aug_09, speedData(3), schedules.filter(_.corridorId == 3), HHMVS14()), "ISO-8859-1") must be(
        mtmFileText)
    }

    "produce a correct export when no dynamic speed data is provided" in {
      val mtmFileText = "# Locatie\r\n" +
        "# test-eg33-2\r\n" +
        "# Datum 2002/01/25\r\n" +
        "#\r\n" +
        "#locatie    |tijdstip           |maxsnelheid|\r\n" +
        "|test-eg33-2|2002/01/25 00:00:00|120        |\r\n" +
        "|test-eg33-2|2002/01/25 24:00:00|120        |\r\n"
      new String(MtmInfoFormatter.makeExportFile(locations(2), aug_09, SpeedSettingOverTime(2, Seq()), schedules.filter(_.corridorId == 2), new HHMVS14()), "ISO-8859-1") must be(
        mtmFileText)
    }

    "produce old format" in {
      val mtmFileText = "# Locatie\r\n" +
        "# test-eg33-2\r\n" +
        "# Datum 2002/01/25\r\n" +
        "#\r\n" +
        "#locatie    |tijdstip           |maxsnelheid|\r\n" +
        "|test-eg33-2|2002/01/25 00:00:00|120        |\r\n" +
        "|test-eg33-2|2002/01/25 23:59:59|120        |\r\n"
      val result = new String(MtmInfoFormatter.makeExportFile(locations(2), aug_09, SpeedSettingOverTime(2, Seq(), Seq()),
        schedules.filter(_.corridorId == 2), new TCVS33()), "ISO-8859-1")
      result must be(mtmFileText)
    }
    "produce a correct export when schedule and MTM change at the same end time" in {
      val speeddata = SpeedSettingOverTime(3, Seq(
        MockEvent(aug_09, 1, speed100, 3),
        MockEvent(aug_09_1000, 1, speed90, 3),
        MockEvent(aug_09_1100, 1, speed100, 3),
        MockEvent(aug_09_2000, 1, speed50, 3)))

      val mtmFileText = "# Locatie\r\n" +
        "# test-eg33-3\r\n" +
        "# Datum 2002/01/25\r\n" +
        "#\r\n" +
        "#locatie    |tijdstip           |maxsnelheid|\r\n" +
        "|test-eg33-3|2002/01/25 00:00:00|100        |\r\n" +
        "|test-eg33-3|2002/01/25 10:00:00|90         |\r\n" +
        "|test-eg33-3|2002/01/25 11:00:00|onbepaald  |\r\n" +
        "|test-eg33-3|2002/01/25 24:00:00|onbepaald  |\r\n"
      new String(MtmInfoFormatter.makeExportFile(locations(3), aug_09, speeddata, schedules.filter(_.corridorId == 3), HHMVS14()), "ISO-8859-1") must be(
        mtmFileText)
    }
    "produce a correct export when schedule and MTM change at the same start time" in {
      val speeddata = SpeedSettingOverTime(4, Seq(
        MockEvent(aug_09, 1, speed100, 4),
        MockEvent(aug_09_1000, 1, speed90, 4),
        MockEvent(aug_09_1200, 1, speed100, 4),
        MockEvent(aug_09_2000, 1, speed90, 4),
        MockEvent(aug_10, 1, speed50, 4)))

      val mtmFileText = "# Locatie\r\n" +
        "# test-eg33-3\r\n" +
        "# Datum 2002/01/25\r\n" +
        "#\r\n" +
        "#locatie    |tijdstip           |maxsnelheid|\r\n" +
        "|test-eg33-3|2002/01/25 00:00:00|onbepaald  |\r\n" +
        "|test-eg33-3|2002/01/25 12:00:00|100        |\r\n" +
        "|test-eg33-3|2002/01/25 20:00:00|90         |\r\n" +
        "|test-eg33-3|2002/01/25 24:00:00|onbepaald  |\r\n"
      new String(MtmInfoFormatter.makeExportFile(locations(3), aug_09, speeddata, schedules.filter(_.corridorId == 4), HHMVS14()), "ISO-8859-1") must be(
        mtmFileText)
    }
    "produce a correct export when schedule and MTM change at the same time" in {
      val speeddata = SpeedSettingOverTime(4, Seq(
        MockEvent(aug_09, 1, speed100, 4),
        MockEvent(aug_09_1100, 1, speedInd, 4),
        MockEvent(aug_09_1200, 1, speedBl, 4),
        MockEvent(aug_09_2000, 1, speed100, 4),
        MockEvent(aug_10, 1, speed50, 4)))

      //val cor = CorridorSchedule(4, 100, None, "", aug_09_1100, aug_09_2000)
      val cor = CorridorSchedule(corridorId = 4, enforcedSpeedLimit = 100, indicatedSpeedLimit = None, pshTmIdFormat = "", startTime = aug_09_1100, endTime = aug_09_2000, speedIndicator = speedIndicator)

      val mtmFileText = "# Locatie\r\n" +
        "# test-eg33-3\r\n" +
        "# Datum 2002/01/25\r\n" +
        "#\r\n" +
        "#locatie    |tijdstip           |maxsnelheid|\r\n" +
        "|test-eg33-3|2002/01/25 00:00:00|onbepaald  |\r\n" +
        "|test-eg33-3|2002/01/25 12:00:00|100        |\r\n" +
        "|test-eg33-3|2002/01/25 20:00:00|onbepaald  |\r\n" +
        "|test-eg33-3|2002/01/25 24:00:00|onbepaald  |\r\n"
      new String(MtmInfoFormatter.makeExportFile(locations(3), aug_09, speeddata, Seq(cor), HHMVS14()), "ISO-8859-1") must be(
        mtmFileText)
    }
    "produce a correct export when schedule and MTM Blank" in {
      val speeddata = SpeedSettingOverTime(4, Seq(
        MockEvent(aug_09, 1, speed100, 4),
        MockEvent(aug_09_1200, 1, speedBl, 4),
        MockEvent(aug_09_1300, 1, speed100, 4),
        MockEvent(aug_10, 1, speed50, 4)))

      //val cor = CorridorSchedule(4, 100, None, "", aug_09_1100, aug_09_2000)
      val cor = CorridorSchedule(corridorId = 4, enforcedSpeedLimit = 100, indicatedSpeedLimit = None, pshTmIdFormat = "", startTime = aug_09_1100, endTime = aug_09_2000, speedIndicator = speedIndicator)

      val mtmFileText = "# Locatie\r\n" +
        "# test-eg33-3\r\n" +
        "# Datum 2002/01/25\r\n" +
        "#\r\n" +
        "#locatie    |tijdstip           |maxsnelheid|\r\n" +
        "|test-eg33-3|2002/01/25 00:00:00|onbepaald  |\r\n" +
        "|test-eg33-3|2002/01/25 11:00:00|100        |\r\n" +
        "|test-eg33-3|2002/01/25 20:00:00|onbepaald  |\r\n" +
        "|test-eg33-3|2002/01/25 24:00:00|onbepaald  |\r\n"
      new String(MtmInfoFormatter.makeExportFile(locations(3), aug_09, speeddata, Seq(cor), HHMVS14()), "ISO-8859-1") must be(
        mtmFileText)
    }
    "produce a correct export when two schedules" in {
      val speeddata = SpeedSettingOverTime(4, Seq(
        MockEvent(aug_09, 1, speed100, 4)))

      //val cor1 = CorridorSchedule(4, 100, None, "", aug_09, aug_09_1100)
      //val cor2 = CorridorSchedule(4, 100, None, "", aug_09_1300, aug_09_2000)
      val cor1 = CorridorSchedule(corridorId = 4, enforcedSpeedLimit = 100, indicatedSpeedLimit = None, pshTmIdFormat = "", startTime = aug_09, endTime = aug_09_1100, speedIndicator = speedIndicator)
      val cor2 = CorridorSchedule(corridorId = 4, enforcedSpeedLimit = 100, indicatedSpeedLimit = None, pshTmIdFormat = "", startTime = aug_09_1300, endTime = aug_09_2000, speedIndicator = speedIndicator)

      val mtmFileText = "# Locatie\r\n" +
        "# test-eg33-3\r\n" +
        "# Datum 2002/01/25\r\n" +
        "#\r\n" +
        "#locatie    |tijdstip           |maxsnelheid|\r\n" +
        "|test-eg33-3|2002/01/25 00:00:00|100        |\r\n" +
        "|test-eg33-3|2002/01/25 11:00:00|onbepaald  |\r\n" +
        "|test-eg33-3|2002/01/25 13:00:00|100        |\r\n" +
        "|test-eg33-3|2002/01/25 20:00:00|onbepaald  |\r\n" +
        "|test-eg33-3|2002/01/25 24:00:00|onbepaald  |\r\n"
      new String(MtmInfoFormatter.makeExportFile(locations(3), aug_09, speeddata, Seq(cor1, cor2), HHMVS14()), "ISO-8859-1") must be(
        mtmFileText)
    }
    "produce a correct export when two schedules with different speed" in {
      val speeddata = SpeedSettingOverTime(4, Seq(
        MockEvent(aug_09, 1, speedBl, 4)))

      val cor1 = CorridorSchedule(corridorId = 4, enforcedSpeedLimit = 100, indicatedSpeedLimit = None, pshTmIdFormat = "", startTime = aug_09, endTime = aug_09_1100, speedIndicator = speedIndicator)
      val cor2 = CorridorSchedule(corridorId = 4, enforcedSpeedLimit = 130, indicatedSpeedLimit = None, pshTmIdFormat = "", startTime = aug_09_1300, endTime = aug_09_2000, speedIndicator = speedIndicator)

      val mtmFileText = "# Locatie\r\n" +
        "# test-eg33-3\r\n" +
        "# Datum 2002/01/25\r\n" +
        "#\r\n" +
        "#locatie    |tijdstip           |maxsnelheid|\r\n" +
        "|test-eg33-3|2002/01/25 00:00:00|100        |\r\n" +
        "|test-eg33-3|2002/01/25 11:00:00|onbepaald  |\r\n" +
        "|test-eg33-3|2002/01/25 13:00:00|130        |\r\n" +
        "|test-eg33-3|2002/01/25 20:00:00|onbepaald  |\r\n" +
        "|test-eg33-3|2002/01/25 24:00:00|onbepaald  |\r\n"
      new String(MtmInfoFormatter.makeExportFile(locations(3), aug_09, speeddata, Seq(cor1, cor2), HHMVS14()), "ISO-8859-1") must be(
        mtmFileText)
    }
    "produce a correct export when two schedules connect and with different speed" in {
      val speeddata = SpeedSettingOverTime(4, Seq(
        MockEvent(aug_09, 1, speedBl, 4)))

      val cor1 = CorridorSchedule(corridorId = 4, enforcedSpeedLimit = 100, indicatedSpeedLimit = None, pshTmIdFormat = "", startTime = aug_09, endTime = aug_09_1300, speedIndicator = speedIndicator)
      val cor2 = CorridorSchedule(corridorId = 4, enforcedSpeedLimit = 130, indicatedSpeedLimit = None, pshTmIdFormat = "", startTime = aug_09_1300, endTime = aug_09_2000, speedIndicator = speedIndicator)

      val mtmFileText = "# Locatie\r\n" +
        "# test-eg33-3\r\n" +
        "# Datum 2002/01/25\r\n" +
        "#\r\n" +
        "#locatie    |tijdstip           |maxsnelheid|\r\n" +
        "|test-eg33-3|2002/01/25 00:00:00|100        |\r\n" +
        "|test-eg33-3|2002/01/25 13:00:00|130        |\r\n" +
        "|test-eg33-3|2002/01/25 20:00:00|onbepaald  |\r\n" +
        "|test-eg33-3|2002/01/25 24:00:00|onbepaald  |\r\n"
      new String(MtmInfoFormatter.makeExportFile(locations(3), aug_09, speeddata, Seq(cor1, cor2), HHMVS14()), "ISO-8859-1") must be(
        mtmFileText)
    }
    "produce an aggregate data object" in {
      val expected = MtmSpeedSettings(List(
        MtmSpeedSetting(aug_09, "test-eg33-1", "100"),
        MtmSpeedSetting(aug_09, "test-eg33-2", "120"),
        MtmSpeedSetting(aug_09, "test-eg33-3", "100"),
        MtmSpeedSetting(aug_09_0900, "test-eg33-2", "50"),
        MtmSpeedSetting(aug_09_1000, "test-eg33-1", "50"),
        MtmSpeedSetting(aug_09_1000, "test-eg33-2", "90"),
        MtmSpeedSetting(aug_09_1100, "test-eg33-3", "onbepaald"),
        MtmSpeedSetting(aug_09_1200, "test-eg33-1", "onbepaald"),
        MtmSpeedSetting(aug_09_1300, "test-eg33-1", "100"),
        MtmSpeedSetting(aug_09_1500, "test-eg33-1", "90"),
        MtmSpeedSetting(aug_09_1500, "test-eg33-2", "120"),
        MtmSpeedSetting(aug_09_2000, "test-eg33-2", "100"),
        MtmSpeedSetting(aug_10, "test-eg33-1", "90"),
        MtmSpeedSetting(aug_10, "test-eg33-2", "100"),
        MtmSpeedSetting(aug_10, "test-eg33-3", "onbepaald")), StatsDuration(aug_09, DayUtility.fileExportTimeZone))
      MtmInfoFormatter.makeMtmSpeedSettings(locations, aug_09, speedData, schedules, StatsDuration(aug_09, DayUtility.fileExportTimeZone), new HHMVS14()) must be(expected)
    }
    "produce an aggregate data object when speeddata (due to state change) is delayed and not equal to the schedule" in {
      val locationsLocal = Map(1 -> "test-eg33-1")
      //schedule 10:00 -> 15:00
      val schedulesLocal = Seq(
        CorridorSchedule(corridorId = 1, enforcedSpeedLimit = 100, indicatedSpeedLimit = None, pshTmIdFormat = "", startTime = aug_09_1000, endTime = aug_09_1500, speedIndicator = speedIndicator))

      //speeddata
      val speedDataLocal = Map(1 -> SpeedSettingOverTime(1, Seq(
        MockEvent(aug_09, 1, speedSb, 3),
        MockEvent(aug_09_1000 + 500, 1, speedBl),
        MockEvent(aug_09_1500 + 500, 1, speedSb))))

      val expected = MtmSpeedSettings(List(
        MtmSpeedSetting(aug_09, "test-eg33-1", "stand-by"),
        MtmSpeedSetting(aug_09_1000 + 500, "test-eg33-1", "100"),
        MtmSpeedSetting(aug_09_1500 + 500, "test-eg33-1", "stand-by"),
        MtmSpeedSetting(aug_10, "test-eg33-1", "stand-by")), StatsDuration(aug_09, DayUtility.fileExportTimeZone))
      MtmInfoFormatter.makeMtmSpeedSettings(locationsLocal, aug_09, speedDataLocal, schedulesLocal, StatsDuration(aug_09, DayUtility.fileExportTimeZone), new HHMVS14()) must be(expected)
    }
    "produce an aggregate data object when speeddata (due to state change) is early and not equal to the schedule" in {
      val locationsLocal = Map(1 -> "test-eg33-1")
      //schedule 10:00 -> 15:00
      val schedulesLocal = Seq(
        CorridorSchedule(corridorId = 1, enforcedSpeedLimit = 100, indicatedSpeedLimit = None, pshTmIdFormat = "", startTime = aug_09_1000, endTime = aug_09_1500, speedIndicator = speedIndicator))

      //speeddata
      val speedDataLocal = Map(1 -> SpeedSettingOverTime(1, Seq(
        MockEvent(aug_09, 1, speedSb, 3),
        MockEvent(aug_09_1000 - 1, 1, speedBl),
        MockEvent(aug_09_1500 - 1, 1, speedSb))))

      val expected = MtmSpeedSettings(List(
        MtmSpeedSetting(aug_09, "test-eg33-1", "stand-by"),
        MtmSpeedSetting(aug_09_1000, "test-eg33-1", "100"),
        MtmSpeedSetting(aug_09_1500 - 1, "test-eg33-1", "stand-by"),
        MtmSpeedSetting(aug_10, "test-eg33-1", "stand-by")), StatsDuration(aug_09, DayUtility.fileExportTimeZone))
      MtmInfoFormatter.makeMtmSpeedSettings(locationsLocal, aug_09, speedDataLocal, schedulesLocal, StatsDuration(aug_09, DayUtility.fileExportTimeZone), new HHMVS14()) must be(expected)
    }
    "produce an standby when state standby and not schedule" in {
      val locationsLocal = Map(1 -> "test-eg33-1")
      //schedule 10:00 -> 20:00
      val schedulesLocal = Seq(
        CorridorSchedule(corridorId = 1, enforcedSpeedLimit = 100, indicatedSpeedLimit = None, pshTmIdFormat = "", startTime = aug_09_1100, endTime = aug_09_2000, speedIndicator = speedIndicator))

      //speeddata
      val speedDataLocal = Map(1 -> SpeedSettingOverTime(1, Seq(
        MockEvent(aug_09, 1, speedSb, 3),
        MockEvent(aug_09_1000, 1, speedBl),
        MockEvent(aug_09_1300, 1, speedSb))))

      val expected = MtmSpeedSettings(List(
        MtmSpeedSetting(aug_09, "test-eg33-1", "stand-by"),
        MtmSpeedSetting(aug_09_1000, "test-eg33-1", "onbepaald"),
        MtmSpeedSetting(aug_09_1100, "test-eg33-1", "100"),
        MtmSpeedSetting(aug_09_1300, "test-eg33-1", "stand-by"),
        MtmSpeedSetting(aug_10, "test-eg33-1", "stand-by")), StatsDuration(aug_09, DayUtility.fileExportTimeZone))
      MtmInfoFormatter.makeMtmSpeedSettings(locationsLocal, aug_09, speedDataLocal, schedulesLocal, StatsDuration(aug_09, DayUtility.fileExportTimeZone), new HHMVS14()) must be(expected)
    }
    "produce an standby when schedule and history are mixed" in {
      val locationsLocal = Map(1 -> "test-eg33-1")
      //schedule 10:00 -> 24:00 but split into 2 parts due to history backup
      val schedulesLocal = Seq(
        CorridorSchedule(corridorId = 1, enforcedSpeedLimit = 100, indicatedSpeedLimit = None, pshTmIdFormat = "", startTime = aug_09_1300, endTime = aug_10, speedIndicator = speedIndicator), //current
        CorridorSchedule(corridorId = 1, enforcedSpeedLimit = 100, indicatedSpeedLimit = None, pshTmIdFormat = "", startTime = aug_09_1000, endTime = aug_09_1300, speedIndicator = speedIndicator)) //backup

      //speeddata
      val speedDataLocal = Map(1 -> SpeedSettingOverTime(1, Seq(
        MockEvent(aug_09, 1, speedSb, 3),
        MockEvent(aug_09_0900, 1, speedBl),
        MockEvent(aug_09_1300, 1, speedSb))))

      val expected = MtmSpeedSettings(List(
        MtmSpeedSetting(aug_09, "test-eg33-1", "stand-by"),
        MtmSpeedSetting(aug_09_0900, "test-eg33-1", "onbepaald"),
        MtmSpeedSetting(aug_09_1000, "test-eg33-1", "100"),
        MtmSpeedSetting(aug_09_1300, "test-eg33-1", "stand-by"),
        MtmSpeedSetting(aug_10, "test-eg33-1", "stand-by")), StatsDuration(aug_09, DayUtility.fileExportTimeZone))
      MtmInfoFormatter.makeMtmSpeedSettings(locationsLocal, aug_09, speedDataLocal, schedulesLocal, StatsDuration(aug_09, DayUtility.fileExportTimeZone), new HHMVS14()) must be(expected)
    }
    "not produce an onbepaald when schedule ends and another starts at the beginning" in {
      val locationsLocal = Map(1 -> "test-eg33-1")
      val schedulesLocal = Seq(
        CorridorSchedule(corridorId = 1, enforcedSpeedLimit = 100, indicatedSpeedLimit = None, pshTmIdFormat = "", startTime = aug_09_1300, endTime = aug_10, speedIndicator = speedIndicator),
        CorridorSchedule(corridorId = 1, enforcedSpeedLimit = 100, indicatedSpeedLimit = None, pshTmIdFormat = "", startTime = aug_09, endTime = aug_09_1100, speedIndicator = speedIndicator))

      //speeddata
      val speedDataLocal = Map(1 -> SpeedSettingOverTime(1, Seq(
        MockEvent(aug_09, 1, speedBl, 3))))

      val expected = MtmSpeedSettings(List(
        MtmSpeedSetting(aug_09, "test-eg33-1", "100"),
        MtmSpeedSetting(aug_09_1100, "test-eg33-1", "onbepaald"),
        MtmSpeedSetting(aug_09_1300, "test-eg33-1", "100"),
        MtmSpeedSetting(aug_10, "test-eg33-1", "100")), StatsDuration(aug_09, DayUtility.fileExportTimeZone))
      MtmInfoFormatter.makeMtmSpeedSettings(locationsLocal, aug_09, speedDataLocal, schedulesLocal, StatsDuration(aug_09, DayUtility.fileExportTimeZone), new HHMVS14()) must be(expected)
    }
  }

  object MockEvent {
    def apply(effectiveTimestamp: Long,
              sequenceNumber: Long,
              speedSetting: Either[SpeedIndicatorStatus.Value, Option[Int]],
              corridorId: Int = 1): SpeedEvent =
      SpeedEvent(effectiveTimestamp, sequenceNumber, corridorId, speedSetting.left.toOption.map(_.toString),
        speedSetting.right.toOption.flatMap(identity(_)), None)
  }
}
