/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.nl.qualify

import csc.sectioncontrol.messages._
import org.scalatest.matchers.MustMatchers
import java.util.{ Date, Calendar, GregorianCalendar }
import csc.sectioncontrol.enforce.nl._
import classify.MultiCorridorProcessor
import csc.sectioncontrol.enforce.speedlog._
import akka.util.duration._
import akka.util.Timeout
import akka.dispatch.Await
import akka.testkit.{ TestBarrier, TestActorRef }
import akka.pattern.ask
import akka.actor.{ Props, Actor, ActorSystem, ActorRef }
import csc.sectioncontrol.storage._
import HBaseTableDefinitions._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import csc.sectioncontrol.enforce.{ SpeedEventData, SpeedIndicatorStatus }
import collection.immutable.SortedSet
import csc.sectioncontrol.enforce.common.nl.violations._
import csc.sectioncontrol.enforce.common.nl.services.Service
import csc.sectioncontrol.common.DayUtility
import csc.curator.CuratorTestServer
import csc.curator.utils.Curator
import csc.sectioncontrol.enforce.register.CountryCodes
import csc.sectioncontrol.storagelayer.hbasetables.{ VehicleClassifiedTable, ViolationRecordTable }
import csc.sectioncontrol.storagelayer.state.StateService
import csc.sectioncontrol.messages.VehicleViolationRecord
import csc.sectioncontrol.storage.ZkCorridorInfo
import csc.sectioncontrol.enforce.common.nl.violations.CorridorData
import csc.sectioncontrol.enforce.common.nl.services.SectionControl
import csc.sectioncontrol.messages.IndicatorReason
import csc.sectioncontrol.enforce.speedlog.GetEnforcedSpeeds
import csc.sectioncontrol.enforce.common.nl.violations.SystemConfigurationData
import csc.sectioncontrol.storage.ZkPSHTMIdentification
import csc.sectioncontrol.enforce.common.nl.violations.SpeedIndicator
import csc.sectioncontrol.messages.VehicleClassifiedRecord
import csc.sectioncontrol.enforce.common.nl.violations.CorridorSchedule
import csc.sectioncontrol.messages.ValueWithConfidence
import csc.sectioncontrol.enforce.nl.register.LogMTMInfo
import csc.sectioncontrol.enforce.common.nl.violations.SystemGlobalConfig
import csc.sectioncontrol.messages.VehicleMetadata
import csc.sectioncontrol.messages.VehicleSpeedRecord
import csc.sectioncontrol.messages.Lane
import csc.sectioncontrol.messages.certificates.ComponentCertificate
import csc.sectioncontrol.storage.ZkCorridor
import csc.sectioncontrol.storagelayer.corridor.CorridorService

class QualifyViolationsTest extends WordSpec with MustMatchers with CuratorTestServer with BeforeAndAfterAll {
  implicit val testSystem = ActorSystem("QualifyViolationsTest")

  var speedLog: ActorRef = _
  var qualifyViolations: TestActorRef[QualifyViolations] = _
  private val writeBarrier = new TestBarrier(2)
  var registerReceived = List[Any]()
  val registerViolationsProbe = TestActorRef(new Actor {
    protected def receive = { case x ⇒ registerReceived = x :: registerReceived }
  })
  val systemId = "eg33"
  val corridorId = "S1"
  private var curator: Curator = _

  override def beforeAll() {
    super.beforeAll()
  }

  override def afterAll() {
    testSystem.shutdown()
    super.afterAll()
  }

  override def beforeEach() {
    super.beforeEach()
    curator = CuratorHelper.create(this, clientScope)

    val corridor = createCorridor()
    CorridorService.createCorridor(curator, systemId, corridor)
    val recordRetriever = ClassifiedRecordsRetriever(Some(VcrReader), None)

    registerReceived = List[Any]()
    speedLog = testSystem.actorOf(Props(new SpeedLog(systemId, curator)).withDispatcher("default-dispatcher"))
    qualifyViolations = TestActorRef(new QualifyViolations(systemId, recordRetriever, VvrWriter, speedLog, registerViolationsProbe, new DummyProcessor()))
  }

  override def afterEach() {
    qualifyViolations.stop()
    testSystem.stop(speedLog)
    super.afterEach()
  }

  def jan25time(hour: Int, minute: Int, second: Int = 0) =
    new GregorianCalendar(2002, Calendar.JANUARY, 25, hour, minute, second).getTimeInMillis

  val jan_25_2002_1000 = jan25time(10, 0)
  val jan_25_2002_1005 = jan25time(10, 5)
  val jan_25_2002_1020 = jan25time(10, 20)
  val jan_25_2002_1025 = jan25time(10, 25)
  val jan_25_2002_1030 = jan25time(10, 30)
  val jan_25_2002_1035 = jan25time(10, 35)
  val jan_25_2002_1040 = jan25time(10, 40)
  val jan_25_2002_1045 = jan25time(10, 45)
  val jan_25_2002_1100 = jan25time(11, 0)
  val jan_25_2002_1105 = jan25time(11, 5)
  val jan_25_2002_1110 = jan25time(11, 10)

  val sysConfig = SystemConfigurationData(
    SystemGlobalConfig(CountryCodes(), "a002", "a002l", "L", Seq(), Seq(),
      ComponentCertificate("Softwarezegel", "checksum"), ComponentCertificate("Configuratiezegel", "checksum"), "systemName", 1000, "serial", Seq("DE", "BE", "CH"), "A004"),
    Seq(makeCorridorData(1, "gantry1", "gantry2", service = new SectionControl()),
      makeCorridorData(2, "gantry3", "gantry4"),
      makeCorridorData(3, "gantry1", "gantry4")),
    Seq(makeSchedule(1, jan_25_2002_1000, jan_25_2002_1030, 120),
      makeSchedule(2, jan_25_2002_1000, jan_25_2002_1030, 100),
      makeSchedule(3, jan_25_2002_1030, jan_25_2002_1045, 90),
      makeSchedule(1, jan_25_2002_1045, jan_25_2002_1110, 120),
      makeSchedule(2, jan_25_2002_1045, jan_25_2002_1110, 120)),
    Map(1 -> ReportingOfficers(Map(0L -> "ab1234")),
      2 -> ReportingOfficers(Map(0L -> "ab1234")),
      3 -> ReportingOfficers(Map(0L -> "ab1234"))),
    Map(VehicleCode.PA -> 130, VehicleCode.CA -> 80),
    SortedSet(SpeedMargin(0, 0)))

  val sysConfigWithMargin = SystemConfigurationData(
    SystemGlobalConfig(CountryCodes(), "a002", "a002l", "L", Seq(), Seq(),
      ComponentCertificate("Softwarezegel", "checksum"), ComponentCertificate("Configuratiezegel", "checksum"), "systemName", 1000, "serial", Seq("DE", "BE", "CH"), "A004"),
    Seq(makeCorridorData(1, "gantry1", "gantry2"),
      makeCorridorData(2, "gantry3", "gantry4"),
      makeCorridorData(3, "gantry1", "gantry4")),
    Seq(makeSchedule(1, jan_25_2002_1000, jan_25_2002_1030, 120),
      makeSchedule(2, jan_25_2002_1000, jan_25_2002_1030, 100),
      makeSchedule(3, jan_25_2002_1030, jan_25_2002_1045, 90),
      makeSchedule(1, jan_25_2002_1045, jan_25_2002_1110, 120),
      makeSchedule(2, jan_25_2002_1045, jan_25_2002_1110, 120)),
    Map(1 -> ReportingOfficers(Map(0L -> "ab1234")),
      2 -> ReportingOfficers(Map(0L -> "ab1234")),
      3 -> ReportingOfficers(Map(0L -> "ab1234"))),
    Map(VehicleCode.PA -> 130, VehicleCode.CA -> 80),
    SortedSet(SpeedMargin(0, 8)))

  private case class SpeedEvent(effectiveTimestamp: Long,
                                speedSetting: Either[SpeedIndicatorStatus.Value, Option[Int]],
                                corridorId: Int = 1) extends SpeedEventData {
    def sequenceNumber = 1L
  }
  private val speedEvent1 = SpeedEvent(jan_25_2002_1000, Right(Some(70)))
  private val speedEvent1a = SpeedEvent(jan_25_2002_1000, Right(Some(120)))
  private val speedEvent1b = SpeedEvent(jan_25_2002_1000, Right(None))
  private val speedEvent2 = SpeedEvent(jan_25_2002_1020, Left(SpeedIndicatorStatus.StandBy))
  private val speedEvent3 = SpeedEvent(jan_25_2002_1005, Left(SpeedIndicatorStatus.Undetermined))
  private val testDuration = 2.seconds
  private implicit val testTimeout = Timeout(testDuration)

  private def createCorridor(): ZkCorridor = {
    ZkCorridor(id = "S1",
      name = "S1-name",
      roadType = 0,
      serviceType = ServiceType.SectionControl,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = false,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = ZkPSHTMIdentification(useYear = false,
        useMonth = true,
        useDay = false,
        useReportingOfficerId = false,
        useNumberOfTheDay = true,
        useLocationCode = false,
        useHHMCode = false,
        useTypeViolation = false,
        useFixedCharacter = false,
        fixedCharacter = "T"),
      info = ZkCorridorInfo(corridorId = 1,
        locationCode = "2",
        reportId = "",
        reportHtId = "",
        reportPerformance = true,
        locationLine1 = ""),
      startSectionId = "sec1",
      endSectionId = "sec1",
      allSectionIds = Seq("sec1"),
      direction = ZkDirection(directionFrom = "",
        directionTo = ""),
      radarCode = 4,
      roadCode = 0,
      dutyType = "",
      deploymentCode = "",
      codeText = "")
  }

  "QualifyViolations.makeViolation" must {
    "produce a violation when a vehicle exceeds the enforced limit on a schedule" in {
      val tmp = makeVCR(measuredSpeed = 125)
      val vcr = tmp.copy(speedRecord = tmp.speedRecord.copy(corridorId = 1))
      qualifyViolations.underlyingActor.makeViolation(vcr, sysConfig, Map()) must be(Some(VehicleViolationRecord("vcr1", vcr, 120, None, List())))
    }

    "produce no violation if the enforced speed limit is not exceeded" in {
      qualifyViolations.underlyingActor.makeViolation(makeVCR(), sysConfig, Map()) must be(None)
    }

    "produce a violation when a vehicle exceeds the vehicle class limit" in {
      val tmp = makeVCR(vehicleClass = Some(VehicleCode.CA))
      val vcr = tmp.copy(speedRecord = tmp.speedRecord.copy(corridorId = 1))
      qualifyViolations.underlyingActor.makeViolation(vcr, sysConfig, Map()) must be(Some(VehicleViolationRecord("vcr1", vcr, 80, None, List())))
    }

    "produce no violation if no matching schedule is active" in {
      qualifyViolations.underlyingActor.makeViolation(makeVCR(exit = "gantry4"), sysConfig, Map()) must be(None)
    }

    "not produce a violation when a vehicle exceeds the dynamic speed limit which differs from schedule speed limit" in {
      speedLog ! speedEvent1
      speedLog ! speedEvent2
      saveEnforceOn(jan_25_2002_1000)
      val result = speedLog ? GetEnforcedSpeeds(1)
      val dynaSpeed = Await.result(result, testDuration).asInstanceOf[SpeedSettingOverTime]
      val vcr = makeVCR()
      qualifyViolations.underlyingActor.makeViolation(vcr, sysConfig, Map(1 -> dynaSpeed)) must be(None)
    }

    "produce a violation when a vehicle exceeds the dynamic speed limit which matches the schedule speed limit" in {
      speedLog ! speedEvent1a
      speedLog ! speedEvent2
      saveEnforceOn(jan_25_2002_1000)
      val result = speedLog ? GetEnforcedSpeeds(1)
      val dynaSpeed = Await.result(result, testDuration).asInstanceOf[SpeedSettingOverTime]
      val tmp = makeVCR(measuredSpeed = 123)
      val vcr = tmp.copy(speedRecord = tmp.speedRecord.copy(corridorId = 1))
      qualifyViolations.underlyingActor.makeViolation(vcr, sysConfig, Map(1 -> dynaSpeed)) must be(Some(VehicleViolationRecord("vcr1", vcr, 120, Some(120), List())))
    }

    "produce a violation when a vehicle exceeds the schedule speed limit with a blank dynamic speed" in {
      speedLog ! speedEvent1b
      speedLog ! speedEvent2
      saveEnforceOn(jan_25_2002_1000)
      val result = speedLog ? GetEnforcedSpeeds(1)
      val dynaSpeed = Await.result(result, testDuration).asInstanceOf[SpeedSettingOverTime]
      val tmp = makeVCR(measuredSpeed = 123)
      val vcr = tmp.copy(speedRecord = tmp.speedRecord.copy(corridorId = 1))
      qualifyViolations.underlyingActor.makeViolation(vcr, sysConfig, Map(1 -> dynaSpeed)) must be(Some(VehicleViolationRecord("vcr1", vcr, 120, None, List())))
    }

    "produce a violation when a vehicle exceeds the schedule speed limit plus margin" in {
      speedLog ! speedEvent1b
      speedLog ! speedEvent2
      saveEnforceOn(jan_25_2002_1000)
      val result = speedLog ? GetEnforcedSpeeds(1)
      val dynaSpeed = Await.result(result, testDuration).asInstanceOf[SpeedSettingOverTime]
      val tmp = makeVCR(measuredSpeed = 130)
      val vcr = tmp.copy(speedRecord = tmp.speedRecord.copy(corridorId = 1))
      qualifyViolations.underlyingActor.makeViolation(vcr, sysConfigWithMargin, Map(1 -> dynaSpeed)) must be(Some(VehicleViolationRecord("vcr1", vcr, 120, None, List())))
    }

    "produce no violation when a vehicle exceeds the schedule speed limit by less than the margin" in {
      speedLog ! speedEvent1b
      speedLog ! speedEvent2
      saveEnforceOn(jan_25_2002_1000)
      val result = speedLog ? GetEnforcedSpeeds(1)
      val dynaSpeed = Await.result(result, testDuration).asInstanceOf[SpeedSettingOverTime]
      val tmp = makeVCR(measuredSpeed = 125)
      val vcr = tmp.copy(speedRecord = tmp.speedRecord.copy(corridorId = 1))
      qualifyViolations.underlyingActor.makeViolation(vcr, sysConfigWithMargin, Map(1 -> dynaSpeed)) must be(None)
    }
  }

  "QualifyViolations" must {
    // Only a single simple testcase here since we're just testing that the overall flow
    // of the actor works. Key logic components are tested seperately above.

    "process a series of classified vehicle records and produce the correct violations" in {
      writeBarrier.reset
      speedLog ! speedEvent1a
      saveEnforceOn(jan_25_2002_1000)

      val tmp1 = makeVCR("vcr1", jan_25_2002_1005, measuredSpeed = 123)
      val tmp3 = makeVCR("vcr3", jan_25_2002_1100, measuredSpeed = 130)

      val vcr1 = tmp1.copy(speedRecord = tmp1.speedRecord.copy(corridorId = 1))
      val vcr3 = tmp3.copy(speedRecord = tmp3.speedRecord.copy(corridorId = 1))

      VcrReader.setRecords(Seq(jan_25_2002_1005 -> vcr1,
        jan_25_2002_1035 -> makeVCR("vcr2", jan_25_2002_1035),
        jan_25_2002_1100 -> vcr3,
        jan_25_2002_1005 + 1.day.toMillis -> makeVCR("vcr4", jan_25_2002_1005)))
      val completionResponse = qualifyViolations ? QualifyViolationsForDay(jan_25_2002_1020, sysConfig, StatsDuration(jan_25_2002_1020, DayUtility.fileExportTimeZone))
      writeBarrier.await
      VvrWriter.records.toSet must be(Set(vcr1 -> VehicleViolationRecord("vcr1", vcr1, 120, Some(120), List()),
        vcr3 -> VehicleViolationRecord("vcr3", vcr3, 120, Some(120), List())))

      /*      VvrWriter.records.toSet must be(Set(KeyWithTimeAndId(jan_25_2002_1005, "vcr1") -> VehicleViolationRecord("vcr1", vcr1, 120, Some(120)),
        KeyWithTimeAndId(jan_25_2002_1100, "vcr3") -> VehicleViolationRecord("vcr3", vcr3, 120, Some(120))))*/
      Await.result(completionResponse, testDuration) must be(QualifyViolationsSuccessful)
    }
    "dont process a classified vehicle records with standby speed" in {
      writeBarrier.reset
      VvrWriter.records = Seq()
      speedLog ! speedEvent1a
      speedLog ! speedEvent2
      saveEnforceOn(jan_25_2002_1000)

      val tmp1 = makeVCR("vcr1", jan_25_2002_1005, measuredSpeed = 123)
      val tmp3 = makeVCR("vcr3", jan_25_2002_1100, measuredSpeed = 130)

      val vcr1 = tmp1.copy(speedRecord = tmp1.speedRecord.copy(corridorId = 1))
      val vcr3 = tmp3.copy(speedRecord = tmp3.speedRecord.copy(corridorId = 1))

      VcrReader.setRecords(Seq(jan_25_2002_1005 -> vcr1,
        jan_25_2002_1035 -> makeVCR("vcr2", jan_25_2002_1035),
        jan_25_2002_1100 -> vcr3,
        jan_25_2002_1005 + 1.day.toMillis -> makeVCR("vcr4", jan_25_2002_1005)))
      val completionResponse = qualifyViolations ? QualifyViolationsForDay(jan_25_2002_1020, sysConfig, StatsDuration(jan_25_2002_1020, DayUtility.fileExportTimeZone))
      writeBarrier.await

      VvrWriter.records.toSet must be(Set(vcr1 -> VehicleViolationRecord("vcr1", vcr1, 120, Some(120), List())))
      //VvrWriter.records.toSet must be(Set(KeyWithTimeAndId(jan_25_2002_1005, "vcr1") -> VehicleViolationRecord("vcr1", vcr1, 120, Some(120))))
      Await.result(completionResponse, testDuration) must be(QualifyViolationsSuccessful)
    }

    "produce a correct mtminfo file" in {
      writeBarrier.reset
      speedLog ! speedEvent1
      speedLog ! speedEvent3
      speedLog ! speedEvent2
      saveEnforceOn(jan_25_2002_1000)
      val vcr1 = makeVCR("vcr1", jan_25_2002_1005)
      VcrReader.setRecords(Seq(jan_25_2002_1005 -> vcr1))
      val completionResponse = qualifyViolations ? QualifyViolationsForDay(jan_25_2002_1020, sysConfig, StatsDuration(jan_25_2002_1020, DayUtility.fileExportTimeZone))
      writeBarrier.await
      Await.result(completionResponse, testDuration) must be(QualifyViolationsSuccessful)

      val expectedMtmInfoText = List("# Locatie\r\n",
        "# systemName L loc2\r\n",
        "# Datum 2002/01/25\r\n",
        "#\r\n",
        "#locatie          |tijdstip           |maxsnelheid|\r\n",
        "|systemName L loc2|2002/01/25 00:00:00|stand-by   |\r\n",
        "|systemName L loc2|2002/01/25 10:00:00|70         |\r\n",
        "|systemName L loc2|2002/01/25 10:05:00|onbepaald  |\r\n",
        "|systemName L loc2|2002/01/25 10:20:00|stand-by   |\r\n",
        "|systemName L loc2|2002/01/25 24:00:00|stand-by   |\r\n").mkString //according t0 - IRS HHMVS40

      registerReceived.size must be(3)
      registerReceived.find(_.asInstanceOf[LogMTMInfo].corridorId == 1) match {
        case Some(LogMTMInfo(day, cId, bytes)) ⇒
          day must be(jan_25_2002_1020)
          cId must be(1)
          new String(bytes, "ISO-8859-1") must be(expectedMtmInfoText)
        case _ ⇒ fail("incorrect message type received by RegisterViolations")
      }
    }
  }

  "Bugfix checks. QualifyViolations" must {
    "not re-use speed data from previous runs" in {
      val speedLogProbe = TestActorRef(new Actor {
        protected def receive = { case _ ⇒ () }
      })
      val recordRetriever = ClassifiedRecordsRetriever(Some(VcrReader), None)

      val qv = TestActorRef(new QualifyViolations(systemId, recordRetriever, VvrWriterNoWait, speedLogProbe, registerViolationsProbe, new DummyProcessor()))

      saveEnforceOn(jan_25_2002_1000)
      val vcr1 = makeVCR("vcr1", jan_25_2002_1005)
      VcrReader.setRecords(Seq(jan_25_2002_1005 -> vcr1))
      val res1 = qv ? QualifyViolationsForDay(jan_25_2002_1020, sysConfig, StatsDuration(jan_25_2002_1020, DayUtility.fileExportTimeZone))
      qv ! SpeedSettingOverTime(1, Seq())
      qv ! SpeedSettingOverTime(2, Seq())
      qv ! SpeedSettingOverTime(3, Seq())
      Await.result(res1, testDuration) must be(QualifyViolationsSuccessful)

      val res2 = qv ? QualifyViolationsForDay(jan_25_2002_1020, sysConfig, StatsDuration(jan_25_2002_1020, DayUtility.fileExportTimeZone))
      qv ! SpeedSettingOverTime(2, Seq())
      evaluating(Await.result(res2, testDuration)) must produce[java.util.concurrent.TimeoutException]
    }
  }

  private def saveEnforceOn(time: Long) {
    // insert system switch-on event, otherwise all results will be empty.
    StateService.create(systemId, corridorId, ZkState(ZkState.enforceOn, time, "test", "test"), curator)
  }

  private def makeCorridorData(id: Int = 1, entry: String = "gantry1", exit: String = "gantry2", service: Service = new SectionControl()) =
    CorridorData(id, entry, exit, "loc1", "loc2", 3.14, 1, 2, 7, 7, 60, 60, "1", "here", service = service)

  private def makeSchedule(corridorId: Int, startTime: Long, endTime: Long, enforcedSpeedLimit: Int) = {
    val speedIndicator = SpeedIndicator(signIndicator = false, speedIndicatorType = None)
    CorridorSchedule(corridorId = corridorId, enforcedSpeedLimit = enforcedSpeedLimit, indicatedSpeedLimit = None, pshTmIdFormat = "pshtmid", startTime = startTime, endTime = endTime, speedIndicator = speedIndicator)
  }

  //CorridorSchedule(corridorId, enforcedSpeedLimit, None, "pshtmid", startTime, endTime)

  private def makeVCR(id: String = "vcr1",
                      exitTime: Long = jan_25_2002_1005,
                      entry: String = "gantry1",
                      exit: String = "gantry2",
                      measuredSpeed: Int = 113,
                      vehicleClass: Option[VehicleCode.Value] = Some(VehicleCode.PA)) =
    new VehicleClassifiedRecord(
      VehicleSpeedRecord(id,
        VehicleMetadata(Lane("1",
          "lane1",
          entry,
          "route1",
          0.0f,
          0.0f),
          id,
          exitTime - 120000,
          Some(new Date(exitTime - 120000).toString),
          Some(ValueWithConfidence("AB1234", 90)),
          country = Some(ValueWithConfidence("NL", 90)),
          images = Seq(),
          NMICertificate = "",
          applChecksum = "",
          serialNr = "SN1234"),
        Some(VehicleMetadata(Lane("1",
          "lane1",
          exit,
          "route1",
          0.0f,
          0.0f),
          id,
          exitTime,
          Some(new Date(exitTime).toString),
          Some(ValueWithConfidence("AB1234", 90)),
          country = Some(ValueWithConfidence("NL", 90)),
          images = Seq(),
          NMICertificate = "",
          applChecksum = "",
          serialNr = "SN1234")),
        2002, measuredSpeed, "SHA-1-foo"),
      vehicleClass,
      ProcessingIndicator.Automatic,
      false,
      false,
      None,
      false,
      IndicatorReason())

  object VcrReader extends VehicleClassifiedTable.Reader {
    var records: Seq[(Long, vehicleClassifiedRecordType)] = Seq()
    def setRecords(vcrs: Seq[(Long, vehicleClassifiedRecordType)]) { records = vcrs }
    def readRow(key: Long)(implicit mt: Manifest[vehicleClassifiedRecordType]) =
      records.find(_._1 == key).map(_._2)
    def readRows(from: Long, to: Long)(implicit mt: Manifest[vehicleClassifiedRecordType]) =
      records.filter(vcr ⇒ vcr._1 >= from && vcr._1 < to).map(_._2)
  }

  object VvrWriter extends ViolationRecordTable.Writer {
    var records: Seq[(vehicleClassifiedRecordType, vehicleViolationRecordType)] = Seq()
    def writeRow(key: vehicleClassifiedRecordType, value: vehicleViolationRecordType, timestamp: Option[Long]) {
      records = records ++ Seq(key -> value)
    }
    def writeRows(keysAndValues: Seq[(vehicleClassifiedRecordType, vehicleViolationRecordType)]) {
      records = records ++ keysAndValues
      writeBarrier.await()
    }
    def writeRowsWithTimestamp(keysAndValues: Seq[(vehicleClassifiedRecordType, vehicleViolationRecordType, Long)]) {}

    def writeRows(values: Seq[vehicleViolationRecordType],
                  getKey: (vehicleViolationRecordType) ⇒ (vehicleClassifiedRecordType, Option[Long])) {}
  }

  object VvrWriterNoWait extends ViolationRecordTable.Writer {
    var records: Seq[(vehicleClassifiedRecordType, vehicleViolationRecordType)] = Seq()
    def writeRow(key: vehicleClassifiedRecordType, value: vehicleViolationRecordType, timestamp: Option[Long]) {
      records = records ++ Seq(key -> value)
    }
    def writeRows(keysAndValues: Seq[(vehicleClassifiedRecordType, vehicleViolationRecordType)]) {
      records = records ++ keysAndValues
    }
    def writeRowsWithTimestamp(keysAndValues: Seq[(vehicleClassifiedRecordType, vehicleViolationRecordType, Long)]) {}

    def writeRows(values: Seq[vehicleViolationRecordType],
                  getKey: (vehicleViolationRecordType) ⇒ (vehicleClassifiedRecordType, Option[Long])) {}
  }
}

class DummyProcessor extends MultiCorridorProcessor {
  def checkClassifications(classifications: Seq[VehicleClassifiedRecord]): Seq[VehicleClassifiedRecord] = classifications
}