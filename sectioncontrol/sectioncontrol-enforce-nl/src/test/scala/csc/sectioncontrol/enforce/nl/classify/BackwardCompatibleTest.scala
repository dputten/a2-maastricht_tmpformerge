/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.nl.classify

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.messages._
import csc.sectioncontrol.messages.IndicatorReason
import csc.json.lift.EnumerationSerializer
import net.liftweb.json.{ Serialization, DefaultFormats }
import csc.sectioncontrol.storage.ConfigType

class BackwardCompatibleTest extends WordSpec with MustMatchers {

  implicit val formats = DefaultFormats + new EnumerationSerializer(ProcessingIndicator, VehicleCode, VehicleCategory,
    ConfigType)
  "Json serialize" must {
    "be able to process old records" in {
      val (oldRecord, newRecord) = createVehicleClassifiedRecords()
      val json = Serialization.write(oldRecord)
      Serialization.read[OldVehicleClassifiedRecord](json) must be(oldRecord)
    }
    "be able to process new records" in {
      val (oldRecord, newRecord) = createVehicleClassifiedRecords()
      val json = Serialization.write(newRecord)
      Serialization.read[VehicleClassifiedRecord](json) must be(newRecord)
    }
    "be able to process old and new records" in {
      val (oldRecord, newRecord) = createVehicleClassifiedRecords()
      val json = Serialization.write(oldRecord)
      Serialization.read[VehicleClassifiedRecord](json) must be(newRecord)
    }
    "be able to process old and new records reversed" in {
      val (oldRecord, newRecord) = createVehicleClassifiedRecords()
      val json = Serialization.write(newRecord)
      Serialization.read[OldVehicleClassifiedRecord](json) must be(oldRecord)
    }
  }

  def createVehicleClassifiedRecords(): (OldVehicleClassifiedRecord, VehicleClassifiedRecord) = {
    val reg = new VehicleMetadata(lane = new Lane("A2-X1-R3", "R3", "X1", "A2", 5.4, 52.4),
      eventId = "A2-X1-R3-123",
      eventTimestamp = System.currentTimeMillis(),
      eventTimestampStr = None,
      license = Some(new ValueWithConfidence[String]("12abc3", 70)),
      rawLicense = None,
      length = None,
      category = Some(new ValueWithConfidence[String]("Car", 70)),
      speed = None,
      country = None,
      images = Seq(),
      NMICertificate = "",
      applChecksum = "",
      serialNr = "SN1234")
    val newRecord = new VehicleClassifiedRecord(
      id = "id",
      speedRecord = new VehicleSpeedRecord(id = "id",
        entry = reg,
        exit = Some(reg),
        corridorId = 1,
        speed = 90,
        measurementSHA = "1234"),
      code = Some(VehicleCode.PA),
      indicator = ProcessingIndicator.Manual,
      mil = false,
      invalidRdwData = false,
      validLicensePlate = None,
      manualAccordingToSpec = true,
      reason = IndicatorReason(0),
      alternativeClassification = None,
      dambord = None)
    val oldRecord = new OldVehicleClassifiedRecord(
      id = "id",
      speedRecord = new VehicleSpeedRecord(id = "id",
        entry = reg,
        exit = Some(reg),
        corridorId = 1,
        speed = 90,
        measurementSHA = "1234"),
      code = Some(VehicleCode.PA),
      indicator = ProcessingIndicator.Manual,
      mil = false,
      invalidRdwData = false,
      validLicensePlate = None,
      manualAccordingToSpec = true,
      reason = IndicatorReason(0),
      alternativeClassification = None)
    (oldRecord, newRecord)
  }

}

case class OldVehicleClassifiedRecord(id: String, speedRecord: VehicleSpeedRecord, code: Option[VehicleCode.Value],
                                      indicator: ProcessingIndicator.Value, mil: Boolean, invalidRdwData: Boolean,
                                      validLicensePlate: Option[String], manualAccordingToSpec: Boolean,
                                      reason: IndicatorReason, alternativeClassification: Option[VehicleCode.Value])
