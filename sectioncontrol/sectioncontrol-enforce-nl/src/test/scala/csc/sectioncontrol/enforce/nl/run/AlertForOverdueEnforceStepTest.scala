/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.nl.run

import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import java.util.{ Calendar, GregorianCalendar }
import akka.testkit.{ TestProbe, TestActorRef }
import csc.config.Path
import java.text.SimpleDateFormat
import akka.actor.{ Actor, Props, ActorSystem }
import akka.util.duration._
import akka.util.Duration
import akka.dispatch.{ Await, Promise, Future }
import csc.sectioncontrol.messages.{ StatsDuration, SystemEvent }
import csc.sectioncontrol.storage._
import csc.sectioncontrol.enforce.nl.{ EnforceNLConfig, CuratorHelper }
import csc.sectioncontrol.common.DayUtility
import csc.curator.CuratorTestServer
import csc.curator.utils.Curator
import csc.sectioncontrol.enforce.{ ExportViolationsStatistics, DayViolationsStatistics }
import csc.sectioncontrol.storagelayer.corridor.CorridorService
import csc.sectioncontrol.storagelayer.state.StateService

class AlertForOverdueEnforceStepTest extends WordSpec with MustMatchers with BeforeAndAfterAll
  with CuratorTestServer {
  implicit val testSystem = ActorSystem("AlertForOverdueEnforceStepTest")

  val systemId = "eg33"
  val corridorId = "S1"
  private var curator: Curator = _
  private var manageEnforceStatus: TestActorRef[ManageEnforceStatus] = _
  val config = EnforceNLConfig("localhost", "Europe/Amsterdam", "None")
  private var alertForOverdue: TestActorRef[AlertForOverdueEnforceStep] = _
  val testDuration: Duration = 5.seconds

  val jun_04_2000 = new GregorianCalendar(2000, Calendar.JUNE, 4, 0, 0).getTimeInMillis
  val jun_05_2000 = new GregorianCalendar(2000, Calendar.JUNE, 5, 0, 0).getTimeInMillis

  var systemEventFuture: Future[SystemEvent] = _
  var systemEventPromise: Promise[SystemEvent] = _
  val eventListener = testSystem.actorOf(Props(new Actor {
    def receive = {
      case e: SystemEvent ⇒ systemEventPromise.complete(Right(e))
    }
  }))

  def saveZk[T <: AnyRef](p: String, v: T) {
    curator.put(Path(p), v)
  }

  override def beforeEach() {
    super.beforeEach()
    curator = CuratorHelper.create(this, clientScope)
    manageEnforceStatus = TestActorRef(new ManageEnforceStatus(systemId, curator))
    alertForOverdue = TestActorRef(new AlertForOverdueEnforceStep(manageEnforceStatus, DayUtility.fileExportTimeZone, systemId, config, curator))

    StateService.create(systemId, corridorId, ZkState("StandBy", jun_04_2000, "system", "test"), curator)

    saveZk(Paths.Systems.getSystemJobsPath(systemId) + "/enforceStatus-nl/2000-06-03", EnforcementStatus(true, jun_04_2000, true, true))
    systemEventPromise = Promise[SystemEvent]
    systemEventFuture = systemEventPromise.future
    testSystem.eventStream.subscribe(eventListener, classOf[SystemEvent])
  }

  override def afterEach() {
    testSystem.eventStream.unsubscribe(eventListener)
    super.afterEach()
  }

  override protected def afterAll() {
    testSystem.stop(eventListener)
    testSystem.shutdown()
    super.afterAll()
  }

  "AlertForOverdueEnforceStep" must {
    "not raise an alert when nothing is stored and less than 2 hours have passed" in {
      alertForOverdue ! AlertForOverdueEnforceStep.ClockTick(jun_05_2000 + 5.minutes.toMillis)
      evaluating { Await.result(systemEventFuture, testDuration) } must produce[java.util.concurrent.TimeoutException]
    }

    "not raise an alert when non-processed state is stored and less than 2 hours have passed" in {
      saveEnforceStatus(EnforcementStatus())
      alertForOverdue ! AlertForOverdueEnforceStep.ClockTick(jun_05_2000 + 5.minutes.toMillis)
      evaluating { Await.result(systemEventFuture, testDuration) } must produce[java.util.concurrent.TimeoutException]
    }

    "not raise an alert when processed state is stored and more than 2 hours have passed" in {
      saveEnforceStatus(EnforcementStatus(true, jun_05_2000 + 1.hour.toMillis))
      alertForOverdue ! AlertForOverdueEnforceStep.ClockTick(jun_05_2000 + 3.hours.toMillis)
      evaluating { Await.result(systemEventFuture, testDuration) } must produce[java.util.concurrent.TimeoutException]
    }

    "raise an mtm alert when non-processed state is stored and more than 2 hours have passed" in {
      saveEnforceStatus(EnforcementStatus())
      alertForOverdue ! AlertForOverdueEnforceStep.ClockTick(jun_05_2000 + 5.hours.toMillis)
      val event: SystemEvent = Await.result(systemEventFuture, testDuration)
      event.componentId must be("ProcessMtmData")
      event.timestamp must be(jun_05_2000 + 5.hours.toMillis)
      event.systemId must be(systemId)
      event.eventType must be("ESA-FILE-OVERDUE")
      event.userId must be("system")
    }

    "not raise an export alert when non-exported state is stored and less than 2 hours have passed" in {
      saveEnforceStatus(EnforcementStatus(true, jun_04_2000 + 1.hour.toMillis))
      alertForOverdue ! AlertForOverdueEnforceStep.ClockTick(jun_05_2000 + 1.hour.toMillis + 5.minutes.toMillis)
      evaluating { Await.result(systemEventFuture, testDuration) } must produce[java.util.concurrent.TimeoutException]
    }

    "not raise an export alert when exported state is stored and more than 2 hours have passed" in {
      saveEnforceStatus(EnforcementStatus(true, jun_04_2000 + 1.hour.toMillis, true, true))
      alertForOverdue ! AlertForOverdueEnforceStep.ClockTick(jun_05_2000 + 4.hours.toMillis)
      evaluating { Await.result(systemEventFuture, testDuration) } must produce[java.util.concurrent.TimeoutException]
    }

    "raise an export alert when non-processed state is stored and more than 2 hours have passed" in {
      saveEnforceStatus(EnforcementStatus(true, jun_04_2000 + 1.hour.toMillis))
      alertForOverdue ! AlertForOverdueEnforceStep.ClockTick(jun_05_2000 + 5.hours.toMillis)
      val event: SystemEvent = Await.result(systemEventFuture, testDuration)
      event.componentId must be("RegisterViolations")
      event.timestamp must be(jun_05_2000 + 5.hours.toMillis)
      event.systemId must be(systemId)
      event.eventType must be("ESA-EXPORT-OVERDUE")
      event.userId must be("system")
    }
    "not create the dailyReport when time isn't expired yet" in {
      val probe = TestProbe()
      testSystem.eventStream.subscribe(probe.ref, classOf[DayViolationsStatistics])
      alertForOverdue ! AlertForOverdueEnforceStep.ClockTick(jun_05_2000 + 7.hours.toMillis)
      probe.expectNoMsg(100.millis)
      testSystem.eventStream.unsubscribe(probe.ref)
    }
    "not create the dailyReport when time is expired and report already created" in {
      val probe = TestProbe()
      testSystem.eventStream.subscribe(probe.ref, classOf[DayViolationsStatistics])

      val key = StatsDuration(jun_04_2000, DayUtility.fileExportTimeZone)
      val reportPath = Path(Paths.Systems.getReportPath(systemId, key))
      curator.createEmptyPath(reportPath)

      alertForOverdue ! AlertForOverdueEnforceStep.ClockTick(jun_05_2000 + 9.hours.toMillis)
      probe.expectNoMsg(100.millis)
      testSystem.eventStream.unsubscribe(probe.ref)
    }
    "request the creation of the dailyReport" in {
      val probe = TestProbe()
      testSystem.eventStream.subscribe(probe.ref, classOf[DayViolationsStatistics])
      //create corridor
      val corridor = ZkCorridor(id = "S1",
        name = "Oost buis",
        roadType = 0,
        serviceType = ServiceType.SectionControl,
        caseFileType = ZkCaseFileType.HHMVS40,
        isInsideUrbanArea = false,
        dynamaxMqId = "",
        approvedSpeeds = Seq(),
        pshtm = ZkPSHTMIdentification(true, false, false, false, false, false, false, false, false, "X"),
        info = ZkCorridorInfo(corridorId = 10,
          locationCode = "2",
          reportId = "A2",
          reportHtId = "HT2",
          reportPerformance = true,
          locationLine1 = "Utrecht"),
        startSectionId = "1",
        endSectionId = "1",
        allSectionIds = Seq("1"),
        direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
        radarCode = 1,
        roadCode = 63,
        dutyType = "",
        deploymentCode = "",
        codeText = "")
      CorridorService.createCorridor(curator, systemId, corridor)

      alertForOverdue ! AlertForOverdueEnforceStep.ClockTick(jun_05_2000 + 9.hours.toMillis)
      val stat = probe.expectMsgType[DayViolationsStatistics](100.millis)
      stat.systemId must be(systemId)
      stat.stats must be(StatsDuration(jun_04_2000, DayUtility.fileExportTimeZone))
      stat.corridorData.size must be(1)
      stat.corridorData.head must be(ExportViolationsStatistics(corridorId = corridor.info.corridorId,
        speedLimit = 0,
        auto = 0,
        manual = 0,
        mobi = 0,
        mobiDoNotProcess = 0,
        mtmPardon = 0,
        doublePardon = 0,
        otherPardon = 0,
        manualAccordingToSpec = 0))
      testSystem.eventStream.unsubscribe(probe.ref)
    }
    "not create the dailyReport when time is expired and creation already requested" in {
      val probe = TestProbe()
      testSystem.eventStream.subscribe(probe.ref, classOf[DayViolationsStatistics])
      alertForOverdue ! AlertForOverdueEnforceStep.ClockTick(jun_05_2000 + 9.hours.toMillis)
      probe.expectMsgType[DayViolationsStatistics](100.millis)
      alertForOverdue ! AlertForOverdueEnforceStep.ClockTick(jun_05_2000 + 10.hours.toMillis)
      probe.expectNoMsg(100.millis)
      testSystem.eventStream.unsubscribe(probe.ref)
    }
    "Create the dailyReport even when system is in OFF state" in {
      val probe = TestProbe()
      testSystem.eventStream.subscribe(probe.ref, classOf[DayViolationsStatistics])
      StateService.update(systemId, "S1", ZkState("Off", jun_04_2000, "system", "test"), 0, curator)
      alertForOverdue ! AlertForOverdueEnforceStep.ClockTick(jun_05_2000 + 9.hours.toMillis)
      probe.expectMsgType[DayViolationsStatistics](100.millis)
      testSystem.eventStream.unsubscribe(probe.ref)
    }
  }

  private def saveEnforceStatus(status: EnforcementStatus) {

    def formatDate(date: Long): String = {
      val df = new SimpleDateFormat("yyyy-MM-dd")
      df.setTimeZone(DayUtility.fileExportTimeZone)
      df.format(date)
    }

    val path = Path(Paths.Systems.getSystemJobsPath(systemId)) / "enforceStatus-nl" / formatDate(jun_04_2000)
    curator.put(path, status)
  }
}
