/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

package csc.sectioncontrol.enforce.nl

/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

import org.apache.curator.framework.CuratorFramework
import csc.akkautils.DirectLoggingAdapter
import csc.curator.utils.{ CuratorToolsImpl, Curator }
import net.liftweb.json.{ DefaultFormats, Formats }
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.storage._
import csc.sectioncontrol.messages.{ EsaProviderType, VehicleImageType, VehicleCode }

object CuratorHelper {
  private lazy val formats: Formats = DefaultFormats

  def create(caller: AnyRef, clientScope: Option[CuratorFramework], formats: Formats = formats): Curator = {
    val log = new DirectLoggingAdapter(caller.getClass.getName)
    val newFormats = formats + new EnumerationSerializer(
      ZkWheelbaseType,
      VehicleCode,
      ZkIndicationType,
      ServiceType,
      ZkCaseFileType,
      EsaProviderType,
      VehicleImageType,
      ConfigType)

    new CuratorToolsImpl(clientScope, log, newFormats)
  }

  def close(curator: Curator) {
    curator.curator.foreach(_.close())
  }
}