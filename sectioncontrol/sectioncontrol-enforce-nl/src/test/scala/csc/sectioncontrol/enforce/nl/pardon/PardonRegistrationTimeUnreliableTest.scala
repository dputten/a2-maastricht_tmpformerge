/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.nl.pardon

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.storage.ZkAlertHistory
import akka.util.duration._
import csc.sectioncontrol.enforce.nl.{ DriftEvent, RegistrationTimeUnreliableConfig }

class PardonRegistrationTimeUnreliableTest extends WordSpec with MustMatchers {

  "calculateEndTime" must {
    "correctly adjust entime when duration < 1 hour" in {
      val startTime = 1425855272824L
      val endTime = startTime + 55.minutes.toMillis
      val calcEndTime = PardonRegistrationTimeUnreliable.calculateEndTime(startTime, endTime, 10)
      calcEndTime must be(endTime + 10)
    }
    "correctly adjust entime when drift is 0" in {
      val startTime = 1425855272824L
      val endTime = startTime + 2.hours.toMillis
      val calcEndTime = PardonRegistrationTimeUnreliable.calculateEndTime(startTime, endTime, 0)
      calcEndTime must be(endTime)
    }
    "correctly adjust entime when duration 0" in {
      val startTime = 1425855272824L
      val endTime = startTime
      val calcEndTime = PardonRegistrationTimeUnreliable.calculateEndTime(startTime, endTime, 10)
      calcEndTime must be(endTime + 10) // exactly 0 hours will result in 1 due to round up
    }
    "correctly adjust entime when duration > 1 hour" in {
      val startTime = 1425855272824L
      val endTime = startTime + 25.hours.toMillis + 25.minutes.toMillis
      val calcEndTime = PardonRegistrationTimeUnreliable.calculateEndTime(startTime, endTime, 10)
      calcEndTime must be(endTime + 260)
    }
  }
  "getLane" must {
    "extract lane ID from camera events" in {
      val alert = ZkAlertHistory(path = "N62-X1-X1R1-camera",
        alertType = "JAI-CAMERA-CONNECTION-ERROR",
        message = "component: N62-X1-X1R1-camera eventType: JAI-CAMERA-CONNECTION-ERROR",
        startTime = 1425855272824L,
        endTime = 1425898117212L,
        configType = "Lane",
        reductionFactor = 0.333F)
      PardonRegistrationTimeUnreliable.getLane(alert) must be(Some("N62-X1-X1R1"))
    }
    "extract lane ID when path has /" in {
      val alert = ZkAlertHistory(path = "N62/X1/X1R1/camera",
        alertType = "JAI-CAMERA-CONNECTION-ERROR",
        message = "component: N62-X1-X1R1-camera eventType: JAI-CAMERA-CONNECTION-ERROR",
        startTime = 1425855272824L,
        endTime = 1425898117212L,
        configType = "Lane",
        reductionFactor = 0.333F)
      PardonRegistrationTimeUnreliable.getLane(alert) must be(Some("N62-X1-X1R1"))
    }
    "skip when configType != Lane" in {
      val alert = ZkAlertHistory(path = "N62-X1-X1R1-camera",
        alertType = "JAI-CAMERA-CONNECTION-ERROR",
        message = "component: N62-X1-X1R1-camera eventType: JAI-CAMERA-CONNECTION-ERROR",
        startTime = 1425855272824L,
        endTime = 1425898117212L,
        configType = "Gantry",
        reductionFactor = 0.333F)
      PardonRegistrationTimeUnreliable.getLane(alert) must be(None)
    }
    "skip when path has unexpected filling" in {
      val alert = ZkAlertHistory(path = "N62-X1-X1R1",
        alertType = "JAI-CAMERA-CONNECTION-ERROR",
        message = "component: N62-X1-X1R1-camera eventType: JAI-CAMERA-CONNECTION-ERROR",
        startTime = 1425855272824L,
        endTime = 1425898117212L,
        configType = "Lane",
        reductionFactor = 0.333F)
      PardonRegistrationTimeUnreliable.getLane(alert) must be(None)
    }
    "skip when path has no sereration chars" in {
      val alert = ZkAlertHistory(path = "N62X1X1R1Camera",
        alertType = "JAI-CAMERA-CONNECTION-ERROR",
        message = "component: N62-X1-X1R1-camera eventType: JAI-CAMERA-CONNECTION-ERROR",
        startTime = 1425855272824L,
        endTime = 1425898117212L,
        configType = "Lane",
        reductionFactor = 0.333F)
      PardonRegistrationTimeUnreliable.getLane(alert) must be(None)
    }
  }
  "createPeriods" must {
    "create one period" in {
      val startTime = 1425855272824L
      val endTime = startTime + 2.hours.toMillis - 1 // exactly 2 hours will result in 3 due to round up

      val alert = ZkAlertHistory(path = "N62-X1-X1R1-camera",
        alertType = "JAI-CAMERA-CONNECTION-ERROR",
        message = "component: N62-X1-X1R1-camera eventType: JAI-CAMERA-CONNECTION-ERROR",
        startTime = startTime,
        endTime = endTime,
        configType = "Lane",
        reductionFactor = 0.333F)
      val config = RegistrationTimeUnreliableConfig(Seq(DriftEvent(20, "JAI-CAMERA-CONNECTION-ERROR")))
      val periodMap = PardonRegistrationTimeUnreliable.createPeriods(config, Seq(alert))

      periodMap.size must be(1)
      val list = periodMap.get("N62-X1-X1R1").get
      list.size must be(1)
      list.head.from must be(startTime)
      list.head.to must be(endTime + 40)
    }
    "skip periods" in {
      val startTime = 1425855272824L
      val endTime = startTime + 2.hours.toMillis - 1 // exactly 2 hours will result in 3 due to round up

      val alert = ZkAlertHistory(path = "N62-X1-X1R1-camera",
        alertType = "JAI-CAMERA-CONNECTION-ERROR",
        message = "component: N62-X1-X1R1-camera eventType: JAI-CAMERA-CONNECTION-ERROR",
        startTime = startTime,
        endTime = endTime,
        configType = "Lane",
        reductionFactor = 0.333F)
      val config = RegistrationTimeUnreliableConfig(Seq(DriftEvent(20, "NTP_STATUS-STA_UNSYNC")))
      val periodMap = PardonRegistrationTimeUnreliable.createPeriods(config, Seq(alert))

      periodMap.size must be(0)
    }
    "create two period for one lane" in {
      val startTime = 1425855272824L
      val endTime = startTime + 2.hours.toMillis - 1 // exactly 2 hours will result in 3 due to round up

      val alert = ZkAlertHistory(path = "N62-X1-X1R1-camera",
        alertType = "JAI-CAMERA-CONNECTION-ERROR",
        message = "component: N62-X1-X1R1-camera eventType: JAI-CAMERA-CONNECTION-ERROR",
        startTime = startTime,
        endTime = endTime,
        configType = "Lane",
        reductionFactor = 0.333F)

      val startTime2 = endTime + 5.hours.toMillis
      val endTime2 = startTime2 + 1.hours.toMillis - 1
      val alert2 = ZkAlertHistory(path = "N62-X1-X1R1-camera",
        alertType = "JAI-CAMERA-CONNECTION-ERROR",
        message = "component: N62-X1-X1R1-camera eventType: JAI-CAMERA-CONNECTION-ERROR",
        startTime = startTime2,
        endTime = endTime2,
        configType = "Lane",
        reductionFactor = 0.333F)
      val config = RegistrationTimeUnreliableConfig(Seq(DriftEvent(20, "JAI-CAMERA-CONNECTION-ERROR")))
      val periodMap = PardonRegistrationTimeUnreliable.createPeriods(config, Seq(alert, alert2))

      periodMap.size must be(1)
      val list = periodMap.get("N62-X1-X1R1").get
      list.size must be(2)
      list.head.from must be(startTime)
      list.head.to must be(endTime + 40)
      list.last.from must be(startTime2)
      list.last.to must be(endTime2 + 20)
    }
    "create two period for two lanes" in {
      val startTime = 1425855272824L
      val endTime = startTime + 2.hours.toMillis - 1 // exactly 2 hours will result in 3 due to round up

      val alert = ZkAlertHistory(path = "N62-X1-X1R1-camera",
        alertType = "JAI-CAMERA-CONNECTION-ERROR",
        message = "component: N62-X1-X1R1-camera eventType: JAI-CAMERA-CONNECTION-ERROR",
        startTime = startTime,
        endTime = endTime,
        configType = "Lane",
        reductionFactor = 0.333F)

      val startTime2 = endTime + 5.hours.toMillis
      val endTime2 = startTime2 + 1.hours.toMillis - 1
      val alert2 = ZkAlertHistory(path = "N62-X1-X1R2-camera",
        alertType = "JAI-CAMERA-CONNECTION-ERROR",
        message = "component: N62-X1-X1R1-camera eventType: JAI-CAMERA-CONNECTION-ERROR",
        startTime = startTime2,
        endTime = endTime2,
        configType = "Lane",
        reductionFactor = 0.333F)
      val config = RegistrationTimeUnreliableConfig(Seq(DriftEvent(20, "JAI-CAMERA-CONNECTION-ERROR")))
      val periodMap = PardonRegistrationTimeUnreliable.createPeriods(config, Seq(alert, alert2))

      periodMap.size must be(2)
      val list = periodMap.get("N62-X1-X1R1").get
      list.size must be(1)
      list.head.from must be(startTime)
      list.head.to must be(endTime + 40)
      val list2 = periodMap.get("N62-X1-X1R2").get
      list2.size must be(1)
      list2.head.from must be(startTime2)
      list2.head.to must be(endTime2 + 20)
    }
  }
}