/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.nl.auditor

import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach, WordSpec }
import akka.actor.{ ActorRef, Props, ActorSystem }
import akka.util.Timeout
import akka.util.duration._
import java.io.File
import akka.testkit.TestProbe
import csc.sectioncontrol.enforce.nl.run.RegisterViolationsJobConfig
import csc.config.Path
import net.liftweb.json.DefaultFormats
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.messages.{ EsaProviderType, GenerateReport, StatsDuration }
import csc.sectioncontrol.enforce.DayViolationsStatistics
import csc.sectioncontrol.storage._
import csc.sectioncontrol.enforce.nl.EnforceEventListener
import csc.curator.CuratorTestServer
import csc.curator.utils.{ CuratorToolsImpl, Curator }
import csc.akkautils.DirectLogging

class DailyCheckTest extends WordSpec with MustMatchers with BeforeAndAfterEach
  with CuratorTestServer with BeforeAndAfterAll with DirectLogging {
  implicit val testSystem = ActorSystem("DailyCheckTest")
  var testActor: ActorRef = _
  val systemId = "A2"
  val project = System.getProperty("user.dir") + "/sectioncontrol-enforce-nl/target/"
  val projectDir = new java.io.File(project)
  projectDir.mkdirs
  val prefix = "a002"

  implicit val timeout = Timeout(5 seconds)

  private var curator: Curator = _
  val formats = DefaultFormats + new EnumerationSerializer(EsaProviderType,
    ConfigType)
  val reporter = TestProbe()

  override def beforeEach() {
    super.beforeEach()
    curator = new CuratorToolsImpl(clientScope, log, formats)
    val s = ZkSystem(
      id = systemId,
      name = systemId,
      title = systemId,
      mtmRouteId = "a002l",
      violationPrefixId = prefix,
      location = ZkSystemLocation(
        description = systemId,
        region = "Utrecht",
        viewingDirection = Some(""),
        roadNumber = "0002",
        roadPart = "Links",
        systemLocation = "Amsterdam"),
      serialNumber = SerialNumber("eg33-1234"),
      orderNumber = 1,
      maxSpeed = 120,
      compressionFactor = 5,
      retentionTimes = ZkSystemRetentionTimes(
        nrDaysKeepTrafficData = 28,
        nrDaysKeepViolations = 7,
        nrDaysRemindCaseFiles = 3),
      pardonTimes = ZkSystemPardonTimes(pardonTimeEsa_secs = 5 /*5000*/ , pardonTimeTechnical_secs = 5 /*5000*/ ),
      esaProviderType = EsaProviderType.NoProvider,
      minimumViolationTimeSeparation = 0)
    saveZk(Paths.Systems.getConfigPath(systemId), s)
    val job = RegisterViolationsJobConfig(project)
    saveZk(Paths.Systems.getSystemJobsPath(systemId) + "/registerViolationsJob/config", job)
    val auditorConfig = AuditorConfig(ftpListenerPort = 12428)
    saveZk(Paths.Systems.getAuditorPath(systemId), auditorConfig)
    //
    testActor = testSystem.actorOf(Props(new EnforceEventListener(systemId, curator, reporter.ref)), "systemEventListener")
  }

  override def afterEach() {
    super.afterEach()
  }

  "DailyCheckTest" must {

    "use configuration from ZooKeeper" in {
      val stats = StatsDuration.local("20120702")
      val storePath = Paths.Systems.getRuntimeViolationsPath(systemId, stats)
      curator.exists(storePath) must be(false)
      //make folders for zaakbestanden
      createFolder(stats, "0001")
      createFolder(stats, "0002")
      createFolder(stats, "0003")
      //
      val data = new DayViolationsStatistics("A2", stats, Nil)
      testActor ! data

      reporter.expectMsg(new GenerateReport(systemId, stats))
      curator.exists(storePath) must be(true)
    }
  }

  override protected def afterAll() {
    testSystem.shutdown()
    super.afterAll()
  }

  private def createFolder(day: StatsDuration, suffix: String): File = {
    val corrFolder1 = new File(projectDir, prefix + suffix)
    corrFolder1.mkdir
    val corrFolder1_1 = new File(corrFolder1, "20120701")
    corrFolder1_1.mkdir
    corrFolder1_1.setLastModified(day.start - 1000000)
    val corrFolder1_2 = new File(corrFolder1, "20120702")
    corrFolder1_2.mkdir
    corrFolder1_2
  }

  def saveZk[T <: AnyRef](p: String, v: T) {
    if (curator.exists(Path(p)))
      curator.deleteRecursive(Path(p))
    curator.put(Path(p), v)
  }

}
