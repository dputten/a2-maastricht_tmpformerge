package csc.sectioncontrol.enforce.nl.run

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.curator.CuratorTestServer
import csc.curator.utils.Curator
import csc.sectioncontrol.enforce.nl.CuratorHelper
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storage
import csc.sectioncontrol.storage.ZkCorridor
import csc.config.Path
import csc.sectioncontrol.tubes.TubesSetting

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 11/11/14.
 */

class TubesSettingsHelperTest extends WordSpec with MustMatchers with CuratorTestServer {
  private var curator: Curator = _

  override def beforeEach() {
    super.beforeEach()
    curator = CuratorHelper.create(this, clientScope)
    createConfig()
  }

  "TubesSettingsHelper" must {
    "aggregate all settings for sections within a corridor" in {
      val settings: List[TubesSetting] = TubesSettingsHelper.getTubesSettings(curator, "N62")
      settings.length must be(1)
      val setting = settings.apply(0)
      setting.corridorId must be(11)
      setting.chartCodes.contains("SOURCE1") must be(true)
      setting.chartCodes.contains("SOURCE2") must be(true)
      setting.chartCodes.contains("SOURCE3") must be(true)
      setting.chartCodes.contains("SOURCE4") must be(false)

      setting.messagePatterns.contains("pattern1") must be(true)
      setting.messagePatterns.contains("pattern2") must be(true)
      setting.messagePatterns.contains("pattern3") must be(true)
      setting.messagePatterns.contains("pattern4") must be(false)

    }
  }

  def createConfig() {
    val info = ZkCorridorInfo(11, "2715", "TCS N62 L", "a0620011", true, "Trajectcontrole N62 links", Some("Oost-buis"))

    val pshtm = storage.ZkPSHTMIdentification(useYear = true, useMonth = true, useDay = true,
      useReportingOfficerId = false, useNumberOfTheDay = false, useLocationCode = true,
      useHHMCode = true, useTypeViolation = false, useFixedCharacter = false, fixedCharacter = "")

    val conf = ZkCorridor("S1", "S1", 1, ServiceType.SectionControl, ZkCaseFileType.HHMVS40,
      false, "", List(), pshtm = pshtm, info = info,
      startSectionId = "1", endSectionId = "2",
      allSectionIds = List("1", "2"),
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 402,
      roadCode = 2,
      dutyType = "KA2",
      deploymentCode = "03",
      codeText = "")
    curator.put[ZkCorridor](Paths.Corridors.getConfigPath("N62", "S1"), conf)

    val sect1Setting = TubesSetting(1, List("SOURCE1"), List("pattern1"))
    curator.put[TubesSetting](Paths.Sections.getTubesSettingsPath("N62", "1"), sect1Setting)

    val sect2Setting = TubesSetting(1, List("SOURCE2", "SOURCE3"), List("pattern2", "pattern3"))
    curator.put[TubesSetting](Paths.Sections.getTubesSettingsPath("N62", "2"), sect2Setting)

    val sect3Setting = TubesSetting(1, List("SOURCE4"), List("pattern4"))
    curator.put[TubesSetting](Paths.Sections.getTubesSettingsPath("N62", "3"), sect3Setting)

  }
}
