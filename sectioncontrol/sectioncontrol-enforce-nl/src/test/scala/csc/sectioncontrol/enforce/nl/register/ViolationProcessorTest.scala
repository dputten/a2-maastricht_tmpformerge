/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.nl.register

import java.io.File
import java.util.{ Date, Calendar, GregorianCalendar }
import akka.actor._
import akka.testkit.TestActorRef
import akka.util.duration._
import csc.curator.CuratorTestServer
import csc.curator.utils.Curator
import csc.sectioncontrol.enforce.common.nl.services.Service
import csc.sectioncontrol.enforce.common.nl.violations._
import csc.sectioncontrol.enforce.nl.CuratorHelper
import csc.sectioncontrol.enforce.nl.casefiles.CaseFile
import csc.sectioncontrol.enforce.register.CountryCodes
import csc.sectioncontrol.messages._
import csc.sectioncontrol.messages.certificates.{ ComponentCertificate, LocationCertificate, MeasurementMethodInstallationType, MeasurementMethodType, TypeCertificate }
import csc.sectioncontrol.storage.{ KeyWithTimeAndId, SelfTestResult }
import csc.sectioncontrol.storagelayer.excludelog.{ Exclusions, SuspendEvent }
import csc.sectioncontrol.storagelayer.hbasetables.{ CalibrationImageTable, VehicleImageTable }
import csc.sectioncontrol.writer.nl.ViolationWriter
import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, WordSpec }

import scala.collection.SortedSet
import scala.collection.mutable.ListBuffer
import akka.event.LoggingAdapter
import java.text.SimpleDateFormat
import csc.akkautils.DirectLoggingAdapter
import csc.sectioncontrol.storagelayer.caseFile.CaseFileConfigService
import csc.akkautils.DirectLogging
import csc.sectioncontrol.storagelayer.system.SystemService
import csc.sectioncontrol.messages.VehicleViolationRecord
import csc.sectioncontrol.messages.certificates.MeasurementMethodType
import csc.sectioncontrol.enforce.common.nl.services.SectionControl
import csc.sectioncontrol.enforce.common.nl.messages.ViolationData
import csc.sectioncontrol.storage.ZkCaseFileConfig
import scala.Some
import csc.sectioncontrol.enforce.common.nl.violations.SystemConfigurationData
import csc.sectioncontrol.enforce.common.nl.violations.SpeedIndicator
import csc.sectioncontrol.messages.VehicleClassifiedRecord
import csc.sectioncontrol.enforce.common.nl.violations.CorridorSchedule
import csc.sectioncontrol.enforce.common.nl.violations.ViolationGroupParameters
import csc.sectioncontrol.messages.certificates.MeasurementMethodInstallationType
import csc.curator.utils.Versioned
import csc.sectioncontrol.enforce.common.nl.violations.SystemGlobalConfig
import csc.sectioncontrol.storage.ZkSystemRetentionTimes
import csc.sectioncontrol.storage.ZkSystemLocation
import csc.sectioncontrol.messages.Lane
import csc.sectioncontrol.messages.certificates.ComponentCertificate
import scala.Left
import csc.sectioncontrol.enforce.common.nl.messages.SpeedIndicationSign
import csc.sectioncontrol.messages.certificates.LocationCertificate
import csc.sectioncontrol.storage.SerialNumber
import csc.sectioncontrol.enforce.common.nl.violations.CorridorData
import csc.sectioncontrol.storage.KeyWithTimeAndId
import csc.sectioncontrol.messages.IndicatorReason
import csc.sectioncontrol.enforce.common.nl.violations.CalibrationTestResult
import csc.sectioncontrol.storage.ZkSystem
import csc.sectioncontrol.messages.ValueWithConfidence
import csc.sectioncontrol.storage.SelfTestResult
import csc.sectioncontrol.messages.certificates.TypeCertificate
import csc.sectioncontrol.enforce.excludelog.GetExclusions
import scala.Right
import csc.sectioncontrol.messages.VehicleSpeedRecord
import csc.sectioncontrol.storage.ZkSystemPardonTimes
import csc.sectioncontrol.enforce.common.nl.violations.Violation
import csc.sectioncontrol.enforce.ExportViolationsStatistics
import csc.sectioncontrol.enforce.violations.DayUtility

class ViolationProcessorTestWithViolationProcessor extends WordSpec with MustMatchers with CuratorTestServer
  with BeforeAndAfterAll with ViolationProcessor {

  def getCorridorSchedule(enforceSpeedLimit: Int): CorridorSchedule = {
    CorridorSchedule(11, enforceSpeedLimit, None, "[YY][NNN][HHHH]3", 1461794400, 1461801403, Some(66), Some("EG33"), Some("12"), None, None, None, None, SpeedIndicator(false, None), false)
  }

  "ViolationProcessor.getSpeedEnforceSpeed" must {
    val dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS")
    val jan_25_2002 = dateFormat.parse("25-01-2002 00:00:00.000").getTime
    val exitTime = jan_25_2002 + 1.minute.toMillis
    val corridorSchedule = getCorridorSchedule(100)
    val vvr = createVehicleViolationRecord("test1", jan_25_2002, exitTime)

    "return the correct speed (enforcedSpeedLimit) based on the esaProvider when nonDynamax" in {
      val violation = Violation(vvr, Some("AA11CCC"), null, null, corridorSchedule, "", "")
      getSpeedEnforceSpeed(violation, EsaProviderType.Mtm) must be(Some(100))
    }

    "return the correct speed (dynamicSpeedLimit) based on the esaProvider when Dynamax" in {
      val violation = Violation(vvr, Some("AA11CCC"), null, null, corridorSchedule, "", "")
      getSpeedEnforceSpeed(violation, EsaProviderType.Dynamax) must be(Some(130))
    }
  }

  "ViolationProcessor.getSpeedIndication" must {
    "return the correct grouped getSpeedIndication" in {
      val dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS")
      val jan_25_2002 = dateFormat.parse("25-01-2002 00:00:00.000").getTime
      val exitTime = jan_25_2002 + 1.minute.toMillis
      val vehicleViolationRecord = createVehicleViolationRecord("test1", jan_25_2002, exitTime)

      val corridorSchedule = getCorridorSchedule(100)

      val violation = Violation(vehicleViolationRecord, None, null, null, corridorSchedule, "", "")

      val speedIndicationSign = getSpeedIndicationSign(violation, EsaProviderType.Mtm)
      speedIndicationSign.indicatedSpeed must be(Some(corridorSchedule.enforcedSpeedLimit))
      speedIndicationSign.indicatorType must be(corridorSchedule.speedIndicator.speedIndicatorType)
      speedIndicationSign.signIndicator must be(corridorSchedule.speedIndicator.signIndicator)
    }
  }

  "ViolationProcessor.groupViolationsOnSpeedIndicationSign" must {
    "group the violations on the SpeedIndication" in {
      val dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS")
      val jan_25_2002 = dateFormat.parse("25-01-2002 00:00:00.000").getTime
      val exitTime = jan_25_2002 + 1.minute.toMillis
      val vehicleViolationRecord = createVehicleViolationRecord("test1", jan_25_2002, exitTime)

      val corridorSchedule80_1 = getCorridorSchedule(80)
      val corridorSchedule100_1 = getCorridorSchedule(100)
      val corridorSchedule100_2 = getCorridorSchedule(100)

      val violation1 = Violation(vehicleViolationRecord, None, null, null, corridorSchedule80_1, "", "")
      val violation2 = Violation(vehicleViolationRecord, None, null, null, corridorSchedule100_1, "", "")
      val violation3 = Violation(vehicleViolationRecord, None, null, null, corridorSchedule100_2, "", "")
      val violations = List(violation1, violation2, violation3)

      val groupedViolations = groupViolationsOnSpeedIndicationSign(violations, EsaProviderType.Mtm)
      groupedViolations.size must be(2)
      val _80 = SpeedIndicationSign(false, None, Some(80))
      val _100 = SpeedIndicationSign(false, None, Some(100))

      groupedViolations(_80).size must be(1)
      groupedViolations(_100).size must be(2)
    }
  }

  def createVehicleViolationRecord(id: String,
                                   entryTime: Long,
                                   exitTime: Long,
                                   countryCode: Option[String] = Some("NL"),
                                   measuredSpeed: Int = 113,
                                   vehicleClass: Option[VehicleCode.Value] = Some(VehicleCode.PA),
                                   licensePlate: Option[String] = Some("AB12CD"),
                                   processingIndicator: ProcessingIndicator.Value = ProcessingIndicator.Automatic,
                                   enforcedSpeed: Int = 100,
                                   reason: IndicatorReason = IndicatorReason()) =
    VehicleViolationRecord(id,
      new VehicleClassifiedRecord(
        VehicleSpeedRecord(id,
          VehicleMetadata(Lane("1",
            "lane1",
            "gantry1",
            "route1",
            0.0f,
            0.0f),
            id,
            entryTime,
            Some(new Date(entryTime).toString),
            licensePlate.map(lp ⇒ ValueWithConfidence(lp, 42)),
            country = countryCode.map(cc ⇒ ValueWithConfidence(cc, 42)),
            images = Seq(),
            NMICertificate = "",
            applChecksum = "",
            serialNr = "SN1234"),
          Some(VehicleMetadata(Lane("1",
            "lane1",
            "gantry2",
            "route1",
            0.0f,
            0.0f),
            id,
            exitTime,
            Some(new Date(exitTime).toString),
            licensePlate.map(lp ⇒ ValueWithConfidence(lp, 42)),
            country = countryCode.map(cc ⇒ ValueWithConfidence(cc, 42)),
            images = Seq(),
            NMICertificate = "",
            applChecksum = "",
            serialNr = "SN1234")),
          2002, measuredSpeed, "SHA-1-foo"),
        vehicleClass,
        processingIndicator,
        false,
        false,
        None,
        false,
        reason),
      enforcedSpeed,
      Some(130),
      recognizeResults = List())

  override private[register] def systemId: String = null

  override protected def curator: Curator = null

  override private[register] def violationImageReaders: Map[VehicleImageType.Value, VehicleImageTable.Reader] = null

  override protected def context: ActorContext = null

  override private[register] def calibrationImageReader: CalibrationImageTable.Reader = null

  override private[register] def log: LoggingAdapter = new DirectLoggingAdapter(this.getClass.getName)
}

class CaseFileConfigMockService() extends CaseFileConfigService with DirectLogging {
  def create(systemId: String, corridorId: String, caseFileConfig: ZkCaseFileConfig) = {

  }

  def get(systemId: String, corridorId: String): Option[ZkCaseFileConfig] = {
    Some(ZkCaseFileConfig("header", Nil))
  }

  def get(systemId: String, corridorInfoId: Int): Option[ZkCaseFileConfig] = {
    Some(ZkCaseFileConfig("header", Nil))
  }

  def exists(systemId: String, corridorId: String): Boolean = {
    true
  }
}

class SystemServiceMock() extends SystemService {
  def get(systemId: String): Option[ZkSystem] = {
    Some(ZkSystemBuilder.get())
  }

  def getVersioned(systemId: String): Option[Versioned[ZkSystem]] = {
    None
  }

  def update(systemId: String, mailServer: ZkSystem, version: Int) = {

  }

  def delete(systemId: String) = {

  }

  def exists(systemId: String) = {
    true
  }
}

object ZkSystemBuilder {
  def get(): ZkSystem = {
    new ZkSystem(id = "A2",
      name = "A2",
      title = "A2",
      mtmRouteId = "a200l",
      violationPrefixId = "",
      location = ZkSystemLocation(
        description = "",
        viewingDirection = Some(""),
        roadNumber = "",
        roadPart = "",
        region = "",
        systemLocation = ""),
      serialNumber = SerialNumber(""),
      orderNumber = 0,
      maxSpeed = 0,
      compressionFactor = 0,
      retentionTimes = ZkSystemRetentionTimes(
        nrDaysKeepTrafficData = 0,
        nrDaysKeepViolations = 0,
        nrDaysRemindCaseFiles = 0),
      pardonTimes = ZkSystemPardonTimes(pardonTimeEsa_secs = 5000, pardonTimeTechnical_secs = 5000),
      esaProviderType = EsaProviderType.Dynamax,
      pshtmId = Some("A120"))
  }
}
/**
 *
 * @author Maarten Hazewinkel
 */
class ViolationProcessorTest extends WordSpec with MustMatchers with CuratorTestServer
  with BeforeAndAfterAll {

  implicit val testSystem = ActorSystem("ViolationProcessorTest")
  protected var curator: Curator = _
  val caseFileConfigMockService = new CaseFileConfigMockService()
  val speedIndicatorA1 = SpeedIndicator(signIndicator = true, speedIndicatorType = Some(SpeedIndicatorType.A1))
  val violation1 = MockViolation(licensePlate = Some("DE34FG"), enforcedSpeed = 100, dynamicSpeedLimit = None)
  val violation2 = MockViolation(licensePlate = Some("HI56JK"), enforcedSpeed = 100, dynamicSpeedLimit = None)
  val violation3 = MockViolation(enforcedSpeed = 70, dynamicSpeedLimit = Some(10))

  val testImageBytes = "testImage".getBytes
  val mtmRaw = Map(1 -> "mtmRaw".getBytes)
  val mtmInfo = Map(1 -> "mtmInfo".getBytes)
  val noExclusions = Map[Int, Exclusions]()
  val cutoff = new GregorianCalendar(2002, 0, 26).getTimeInMillis
  val corridor1 = CorridorData(1, "entry", "exit", "here", "there", 123.5, 1, 2, 7, 7, 60, 60, "00001", "Location", service = new SectionControl())

  val schedule0 = CorridorSchedule(corridorId = 1, enforcedSpeedLimit = 100, indicatedSpeedLimit = Some(100), pshTmIdFormat = "pshtmid", startTime = jan25time(0, 0, 0), endTime = jan25time(24, 0, 0), speedIndicator = speedIndicatorA1)
  val schedule1 = CorridorSchedule(corridorId = 1, enforcedSpeedLimit = 130, indicatedSpeedLimit = Some(130), pshTmIdFormat = "pshtmid", startTime = jan25time(0, 0, 0), endTime = jan25time(6, 0, 0), speedIndicator = speedIndicatorA1)
  val schedule2 = CorridorSchedule(corridorId = 1, enforcedSpeedLimit = 100, indicatedSpeedLimit = Some(100), pshTmIdFormat = "pshtmid", startTime = jan25time(6, 0, 0), endTime = jan25time(19, 0, 0), speedIndicator = speedIndicatorA1)
  val schedule3 = CorridorSchedule(corridorId = 1, enforcedSpeedLimit = 130, indicatedSpeedLimit = Some(130), pshTmIdFormat = "pshtmid", startTime = jan25time(19, 0, 0), endTime = jan25time(24, 0, 0), speedIndicator = speedIndicatorA1)

  val globalConfig = SystemGlobalConfig(
    CountryCodes(),
    "a002",
    "a002l",
    "L",
    Seq(new MeasurementMethodInstallationType(
      0L,
      new MeasurementMethodType(
        typeDesignation = "XX123",
        category = "A",
        unitSpeed = "km/h",
        unitRedLight = "s",
        unitLength = "m",
        restrictiveConditions = "",
        displayRange = "20-250 km/h",
        temperatureRange = "temperatuurbereik -10 to 50 graden Celsius",
        permissibleError = "toelatbare fout 3%",
        typeCertificate = new TypeCertificate(
          "eg33-cert", 0,
          List(ComponentCertificate("part 1", "sum 1"), ComponentCertificate("part 2", "sum 2")))))),
    Seq(new LocationCertificate(id = "LocId", inspectionDate = 0L, validTime = Long.MaxValue,
      components = List(ComponentCertificate("Configuratiezegel", "configuratiechecksum")))),
    ComponentCertificate("Softwarezegel", "checksum"),
    ComponentCertificate("Configuratiezegel", "checksum"),
    "systemName",
    30.minutes.toMillis,
    "SN1234",
    Seq("BE", "DE", "CH"), "A004")

  val sysConfig = new SystemConfigurationData(
    globalConfig,
    Seq(corridor1),
    Seq(schedule0),
    Map(1 -> ReportingOfficers(Map[Long, String]())),
    Map[VehicleCode.Value, Int](),
    SortedSet(SpeedMargin(0, 0)))

  val sysConfig1 = new SystemConfigurationData(
    globalConfig,
    Seq(corridor1),
    Seq(schedule1, schedule2, schedule3),
    Map(1 -> ReportingOfficers(Map[Long, String]())),
    Map[VehicleCode.Value, Int](),
    SortedSet(SpeedMargin(0, 0)))
  val sysConfig2 = new SystemConfigurationData(
    globalConfig,
    Seq(corridor1),
    Seq(schedule1, schedule2, schedule3),
    Map(1 -> ReportingOfficers(Map[Long, String]())),
    Map[VehicleCode.Value, Int](),
    SortedSet(SpeedMargin(0, 0)))
  val mockStore = testSystem.actorOf(Props(new Actor {
    protected def receive = {
      case _ ⇒ ()
    }
  }))

  override protected def afterAll() {
    testSystem.stop(mockStore)
    testSystem.shutdown()
    super.afterAll()
  }

  override protected def beforeEach(configMap: Map[String, Any]): Unit = {
    super.beforeEach(configMap)
    curator = CuratorHelper.create(this, clientScope)
  }

  private def getCurator = curator

  def jan25time(hour: Int, minute: Int, second: Int = 0) =
    new GregorianCalendar(2002, Calendar.JANUARY, 25, hour, minute, second).getTimeInMillis

  def makeCalibrationTestReport(corridorId: String, time: Long, inforId: Int, isMidnightTest: Boolean, serialNr: String = "SN1234") =
    SelfTestResult("EG33", corridorId, time, true, "AB1235", inforId, Seq(), Seq(), serialNr)

  "ViolationProcessor" must {
    "make testresult with correct software seal" in {
      val rv = TestActorRef(new ViolationProcessorMockedWriter).underlyingActor
      val testResult = rv.makeCalibrationTestResult(makeCalibrationTestReport("S1", System.currentTimeMillis(), 1, false),
        MockSelftestImageReader, corridor1, schedule1, sysConfig.globalConfig, "SN1234", jan25time(24, 0))
      val seals = testResult.softwareSeals
      seals.size must be(2)
      seals(0).name must be("Softwarezegel")
      seals(0).checksum must be("ec313c65823eb71ccd49ac9f27457894")
      seals(1).name must be("Configuratiezegel")
      seals(1).checksum must be("configuratiechecksum")
    }

    "register 2 violations in 1 writer" in {
      val rv = TestActorRef(new ViolationProcessorMockedWriter).underlyingActor
      val violation1 = MockViolation(licensePlate = Some("DE34FG"), enforcedSpeed = 100, dynamicSpeedLimit = Some(10))
      val violation2 = MockViolation(licensePlate = Some("HI56JK"), enforcedSpeed = 100, dynamicSpeedLimit = Some(10))
      rv.writeViolations(List(violation1, violation2), "", sysConfig, Map(), mtmRaw, mtmInfo, mockStore, EsaProviderType.Mtm, caseFileConfigMockService)
      rv.writers must have size 1
      rv.writers(0).violationCount must be(2)
      val violations = rv.writers(0).violations
      for (vio ← violations) {
        vio.vehicleClass must be(Some(VehicleCode.PA))
        vio.vehicleCountryCode must be(Some("NL"))
      }
    }
    "Register 2 violation in 1 writer with manual" in {
      val rv = TestActorRef(new ViolationProcessorMockedWriter).underlyingActor
      val violation1 = MockViolation(licensePlate = Some("DE34FG"), enforcedSpeed = 100, dynamicSpeedLimit = Some(10), processingIndicator = ProcessingIndicator.Manual)
      val violation2 = MockViolation(licensePlate = Some("HI56JK"), enforcedSpeed = 100, dynamicSpeedLimit = Some(10), processingIndicator = ProcessingIndicator.Manual)
      rv.writeViolations(List(violation1, violation2), "", sysConfig, Map(), mtmRaw, mtmInfo, mockStore, EsaProviderType.Mtm, caseFileConfigMockService)
      rv.writers must have size (1)
      rv.writers(0).violationCount must be(2)
      val violations = rv.writers(0).violations
      for (vio ← violations) {
        vio.vehicleClass must be(None)
        vio.vehicleCountryCode must be(Some("NL"))
      }
    }
    "Register 2 treaty foreigner violation in 1 writer with manual" in {
      val rv = TestActorRef(new ViolationProcessorMockedWriter).underlyingActor
      val violation1 = MockViolation(licensePlate = Some("DE34FG"), enforcedSpeed = 100, dynamicSpeedLimit = Some(10), processingIndicator = ProcessingIndicator.Manual, countryCode = Some("DE"))
      val violation2 = MockViolation(licensePlate = Some("HI56JK"), enforcedSpeed = 100, dynamicSpeedLimit = Some(10), processingIndicator = ProcessingIndicator.Manual, countryCode = Some("DE"))
      rv.writeViolations(List(violation1, violation2), "", sysConfig, Map(), mtmRaw, mtmInfo, mockStore, EsaProviderType.Mtm, caseFileConfigMockService)
      rv.writers must have size (1)
      rv.writers(0).violationCount must be(2)
      val violations = rv.writers(0).violations
      for (vio ← violations) {
        vio.vehicleClass must be(None)
        vio.vehicleCountryCode must be(Some("DE"))
      }
    }
    "Register 2 foreigner violation in 1 writer with manual" in {
      val rv = TestActorRef(new ViolationProcessorMockedWriter).underlyingActor
      val violation1 = MockViolation(licensePlate = Some("DE34FG"), enforcedSpeed = 100, dynamicSpeedLimit = Some(10), processingIndicator = ProcessingIndicator.Manual, countryCode = Some("IT"))
      val violation2 = MockViolation(licensePlate = Some("HI56JK"), enforcedSpeed = 100, dynamicSpeedLimit = Some(10), processingIndicator = ProcessingIndicator.Manual, countryCode = Some("IT"))
      rv.writeViolations(List(violation1, violation2), "", sysConfig, Map(), mtmRaw, mtmInfo, mockStore, EsaProviderType.Mtm, caseFileConfigMockService)
      rv.writers must have size (1)
      rv.writers(0).violationCount must be(2)
      val violations = rv.writers(0).violations
      for (vio ← violations) {
        vio.vehicleClass must be(None)
        vio.vehicleCountryCode must be(None)
      }
    }
    "split violations for different speeds" in {
      val rv = TestActorRef(new ViolationProcessorMockedWriter).underlyingActor
      val violation1 = MockViolation(licensePlate = Some("DE34FG"), enforcedSpeed = 100, dynamicSpeedLimit = Some(10))
      val violation2 = MockViolation(licensePlate = Some("HI56JK"), enforcedSpeed = 100, dynamicSpeedLimit = Some(10))
      val violation3 = MockViolation(enforcedSpeed = 70, dynamicSpeedLimit = Some(140))
      rv.writeViolations(List(violation1, violation2, violation3), "", sysConfig, Map(), mtmRaw, mtmInfo, mockStore, EsaProviderType.Mtm, caseFileConfigMockService)
      rv.writers must have size (2)
      rv.writers.sortBy(_.violationCount).apply(0).violationCount must be(1)
      rv.writers.sortBy(_.violationCount).apply(1).violationCount must be(2)
    }
    "not split violations for different reportingOfficerCodes or PshTmIds" in {
      val rv = TestActorRef(new ViolationProcessorMockedWriter).underlyingActor
      val violation1 = MockViolation(licensePlate = Some("DE34FG"), enforcedSpeed = 100, dynamicSpeedLimit = Some(10))
      val violation2 = MockViolation(licensePlate = Some("HI56JK"), enforcedSpeed = 100, dynamicSpeedLimit = Some(10))
      rv.writeViolations(List(violation1, violation2, MockViolation(reportingOfficerCode = "XY9999", pshTmId = "XYZ1239999", dynamicSpeedLimit = Some(10))), "", sysConfig, Map(), mtmRaw, mtmInfo, mockStore, EsaProviderType.Mtm, caseFileConfigMockService)
      rv.writers must have size (1)
      rv.writers.sortBy(_.violationCount).head.violationCount must be(3)
    }
    "apply callibration test results to all writers" in {
      val ctr = makeCalibrationTestReport("S1", System.currentTimeMillis, 1, false)
      val rv = TestActorRef(new ViolationProcessorMockedWriter).underlyingActor
      val violation1 = MockViolation(licensePlate = Some("DE34FG"), enforcedSpeed = 100, dynamicSpeedLimit = Some(10))
      val violation2 = MockViolation(licensePlate = Some("HI56JK"), enforcedSpeed = 100, dynamicSpeedLimit = Some(10))
      val violation3 = MockViolation(enforcedSpeed = 70, dynamicSpeedLimit = Some(140))
      rv.writeViolations(List(violation1, violation2, violation3), "", sysConfig,
        Map(1 -> Seq(ctr, ctr)), mtmRaw, mtmInfo, mockStore, EsaProviderType.Mtm, caseFileConfigMockService)
      rv.writers must have size (2)
      rv.writers(0).ctrCount must be(2)
      rv.writers(1).ctrCount must be(2)
    }
    "Register 2 violations in 2 writer with different serialNr in same order" in {
      val ctr = makeCalibrationTestReport("S1", MockViolation.defaultEntryTime, 1, true, "456")
      val ctr2 = makeCalibrationTestReport("S1", MockViolation.defaultEntryTime + 2.hour.toMillis, 1, true, "123")
      val rv = TestActorRef(new ViolationProcessorMockedWriter).underlyingActor
      val violation1 = MockViolation(licensePlate = Some("DE34FG"), enforcedSpeed = 100, dynamicSpeedLimit = Some(10), serialNr = "456")
      val violation2 = MockViolation(licensePlate = Some("HI56JK"), enforcedSpeed = 100, dynamicSpeedLimit = Some(10), serialNr = "123")
      rv.writeViolations(List(violation1, violation2), "", sysConfig, Map(1 -> List(ctr, ctr2)), mtmRaw, mtmInfo, mockStore, EsaProviderType.Mtm, caseFileConfigMockService)
      rv.writers must have size (2)
      rv.writers(0).violationCount must be(1)
      val vio = rv.writers(0).violations.head
      vio.exportLicensePlate must be(violation1.license)

      rv.writers(1).violationCount must be(1)
      val vio1 = rv.writers(1).violations.head
      vio1.exportLicensePlate must be(violation2.license)
    }
    "Complete the export without any violation if mtmRaw is empty (absent)" in {
      val ctr = makeCalibrationTestReport("S1", System.currentTimeMillis, 1, false)
      val rv = TestActorRef(new ViolationProcessorMockedWriter).underlyingActor
      val violation1 = MockViolation(licensePlate = Some("DE34FG"), enforcedSpeed = 100, dynamicSpeedLimit = Some(10))
      val violation2 = MockViolation(licensePlate = Some("HI56JK"), enforcedSpeed = 100, dynamicSpeedLimit = Some(10))
      val violation3 = MockViolation(enforcedSpeed = 70, dynamicSpeedLimit = Some(140))
      rv.writeViolations(List(violation1, violation2, violation3), "", sysConfig,
        Map(1 -> Seq(ctr, ctr)), mtmRaw - 1, mtmInfo, mockStore, EsaProviderType.Mtm, caseFileConfigMockService)
      rv.writers must have size (2)
      rv.writers(0).ctrCount must be(2)
      rv.writers(1).ctrCount must be(2)
      rv.writers.sortBy(_.violationCount).apply(0).violationCount must be(0)
      rv.writers.sortBy(_.violationCount).apply(1).violationCount must be(0)
    }
    "abort the export of all writers if one writer has a failure" in {
      val ctr = makeCalibrationTestReport("S1", System.currentTimeMillis, 1, false)
      var callAddCtr = 0
      val rv = TestActorRef(new ViolationProcessorMockedWriter {
        override def makeViolationWriter(exportBaseDir: String,
                                         groupParameters: ViolationGroupParameters,
                                         rawMTM: Array[Byte],
                                         processedMTM: Array[Byte],
                                         requiredSpaceKb: Int,
                                         service: Service,
                                         caseFile: CaseFile,
                                         esaProvider: EsaProviderType.Value, optSpeedLimit: Option[Int]) = {
          val writer = new CountingViolationWriter {
            override def add(ctr: CalibrationTestResult) = {
              callAddCtr += 1
              if (callAddCtr == 4) Left("x") else super.add(ctr)
            }
          }
          writers += writer
          writer
        }
      }).underlyingActor
      val violation1 = MockViolation(licensePlate = Some("DE34FG"), enforcedSpeed = 100, dynamicSpeedLimit = Some(10))
      val violation2 = MockViolation(licensePlate = Some("HI56JK"), enforcedSpeed = 100, dynamicSpeedLimit = Some(10))
      val violation3 = MockViolation(enforcedSpeed = 70, dynamicSpeedLimit = Some(140))
      rv.writeViolations(List(violation1, violation2, violation3), "", sysConfig,
        Map(1 -> Seq(ctr, ctr)), mtmRaw, mtmInfo, mockStore, EsaProviderType.Mtm, caseFileConfigMockService)
      rv.writers must have size (2)
      rv.writers(0).aborted must be(true)
      rv.writers(1).aborted must be(true)
    }
    "abort the export of all writers if one writer fails to flush" in {
      val ctr = makeCalibrationTestReport("S1", System.currentTimeMillis, 1, false)
      val rv = TestActorRef(new ViolationProcessorMockedWriter {
        override def makeViolationWriter(exportBaseDir: String,
                                         groupParameters: ViolationGroupParameters,
                                         rawMTM: Array[Byte],
                                         processedMTM: Array[Byte],
                                         requiredSpaceKb: Int,
                                         service: Service,
                                         caseFile: CaseFile,
                                         esaProviderType: EsaProviderType.Value, optSpeedLimit: Option[Int]) = {
          val writer = new CountingViolationWriter {
            override def flush() = Left("x")
          }
          writers += writer
          writer
        }
      }).underlyingActor
      val violation1 = MockViolation(licensePlate = Some("DE34FG"), enforcedSpeed = 100, dynamicSpeedLimit = Some(10))
      val violation2 = MockViolation(licensePlate = Some("HI56JK"), enforcedSpeed = 100, dynamicSpeedLimit = Some(10))
      val violation3 = MockViolation(enforcedSpeed = 70, dynamicSpeedLimit = Some(140))
      rv.writeViolations(List(violation1, violation2, violation3), "", sysConfig,
        Map(1 -> Seq(ctr, ctr)), mtmRaw, mtmInfo, mockStore, EsaProviderType.Mtm, caseFileConfigMockService)
      rv.writers must have size (2)
      rv.writers(0).aborted must be(true)
      rv.writers(1).aborted must be(true)
    }
    val jan_25_2002_1000 = jan25time(10, 0)
    val jan_25_2002_1005 = jan25time(10, 5)
    val jan_25_2002_1100 = jan25time(11, 0)
    val jan_25_2002_1105 = jan25time(11, 5)
    val jan_25_2002_1110 = jan25time(11, 10)

    "eliminate violations that fall into an exclusion period" in {
      val rv = TestActorRef(new ViolationProcessorMockedWriter).underlyingActor
      val exclusions = Exclusions(1, List(SuspendEvent("EG33", jan_25_2002_1100, Some(1), 1, true, Some("test"))))
      val (in, out) = rv.splitByExclusion(List(MockViolation(entryTime = jan_25_2002_1000, exitTime = jan_25_2002_1005),
        MockViolation(entryTime = jan_25_2002_1105, exitTime = jan_25_2002_1110)),
        Map(exclusions.corridorId -> exclusions))
      in must have size (1)
      out must have size (1)
    }

    "not register violations where the image checksum fails" in {
      val rv = TestActorRef(new ViolationProcessorMockedWriter).underlyingActor
      rv.writeViolations(List(MockViolation(dynamicSpeedLimit = Some(10), entryTime = jan_25_2002_1000, exitTime = jan_25_2002_1005, checksum = "01234567"),
        MockViolation(dynamicSpeedLimit = Some(10), entryTime = jan_25_2002_1105, exitTime = jan_25_2002_1110)), "", sysConfig,
        Map(), mtmRaw, mtmInfo, mockStore, EsaProviderType.Mtm, caseFileConfigMockService)
      rv.writers must have size (1)
      rv.writers(0).violationCount must be(1)
    }

    "create statistics per corridor per scheduleSpeed" in {
      // a ViolationProcessor for calculating statistics
      object statsProcessor extends ViolationProcessor {
        def systemId = "A4"
        def log = null
        def calibrationImageReader = null
        def context = null
        def violationImageReaders = null
        def curator = null
      }
      // violations during schedule1, schedule2 and schedule3
      val registeredViolations = Seq(
        MockViolation(enforcedSpeed = 130, measuredSpeed = 139, entryTime = jan25time(5, 12), exitTime = jan25time(5, 17)),
        MockViolation(enforcedSpeed = 100, measuredSpeed = 139, entryTime = jan25time(6, 12), exitTime = jan25time(6, 17)),
        MockViolation(enforcedSpeed = 130, measuredSpeed = 139, entryTime = jan25time(19, 12), exitTime = jan25time(19, 15)))
      // expected statistics
      val expectedStatistics = Set(ExportViolationsStatistics(1, 100, 1, 0, 0, 0, 0, 0, 0, 0), ExportViolationsStatistics(1, 130, 2, 0, 0, 0, 0, 0, 0, 0))

      // call the processor to calculate the statistics
      val dayViolationStats = statsProcessor.calculateStatistics(registeredViolations, Seq(), Seq(), Map[Int, Exclusions](), sysConfig2, StatsDuration("20020125", DayUtility.fileExportTimeZone))

      dayViolationStats.corridorData.toSet must be(expectedStatistics)
    }
  }

  class ViolationProcessorMockedWriter extends ViolationProcessor with Actor with ActorLogging {
    def systemId = "EG33"

    def excludeLog = TestActorRef(new Actor {
      protected def receive = {
        case GetExclusions(_) ⇒ sender ! Exclusions(0, List[SuspendEvent]())
      }
    })

    val writers = ListBuffer[CountingViolationWriter]()

    override def curator = getCurator
    override private[register] def makeViolationWriter(exportBaseDir: String,
                                                       groupParameters: ViolationGroupParameters,
                                                       rawMTM: Array[Byte],
                                                       processedMTM: Array[Byte],
                                                       requiredSpaceKb: Int,
                                                       service: Service,
                                                       caseFile: CaseFile,
                                                       esaProviderType: EsaProviderType.Value, optSpeedLimit: Option[Int]) = {
      val writer = new CountingViolationWriter
      writers += writer
      writer
    }

    protected def receive = null

    def violationImageReaders = Map() //not used

    //mocking the whole image retrieval/validation logic. This way we don't need storage or real images
    override def retrieveImages(entry: VehicleMetadata, exit: Option[VehicleMetadata], corridor: CorridorData): (Option[Array[Byte]], Option[Array[Byte]]) = {
      if (entry.images.exists(_.checksum == MockViolation.goodChecksum)) {
        (Some(testImageBytes), Some(testImageBytes))
      } else {
        (None, None) //no image triggered by bad checksum
      }
    }

    def calibrationImageReader = MockSelftestImageReader
  }

  class CountingViolationWriter extends ViolationWriter {
    var ctrCount = 0
    def violationCount = violations.size
    var aborted = false
    var violations = Seq[ViolationData]()

    def add(calibrationTestResult: CalibrationTestResult): Either[String, Unit] = { ctrCount += 1; Right() }
    def add(violation: ViolationData, image1: Array[Byte], image2: Option[Array[Byte]]): Either[String, Unit] = { violations = violations :+ violation; Right() }

    def flush(): Either[String, File] = Right(new File("dummy"))

    def abort() {
      aborted = true
    }
  }

  object MockSelftestImageReader extends CalibrationImageTable.Reader {
    def readRow(key: KeyWithTimeAndId)(implicit mt: Manifest[Array[Byte]]) = Some(testImageBytes)
    def readRows(from: KeyWithTimeAndId, to: KeyWithTimeAndId)(implicit mt: Manifest[Array[Byte]]) = null
  }

  object MockImageReader extends VehicleImageTable.Reader {
    def readRow(key: (String, Long, String))(implicit mt: Manifest[Array[Byte]]) = Some(testImageBytes)
    def readRows(from: (String, Long, String), to: (String, Long, String))(implicit mt: Manifest[Array[Byte]]) = null
  }

}
