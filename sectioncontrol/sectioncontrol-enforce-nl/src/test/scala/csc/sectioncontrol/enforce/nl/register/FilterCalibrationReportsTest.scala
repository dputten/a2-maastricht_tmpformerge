/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.nl.register

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import java.util.{ Calendar, GregorianCalendar }
import csc.sectioncontrol.storage.SelfTestResult
import akka.util.duration._

class FilterCalibrationReportsTest extends WordSpec with MustMatchers {
  val jan_25_2002 = new GregorianCalendar(2002, Calendar.JANUARY, 25, 0, 0, 0).getTimeInMillis / 1000 * 1000
  val jan_26_2002 = new GregorianCalendar(2002, Calendar.JANUARY, 26, 0, 0, 0).getTimeInMillis / 1000 * 1000

  "FilterCalibrationReports" must {
    "filter all test when no successful results" in {
      val list = Seq(
        new SelfTestResult("eg33", "S1", jan_25_2002, false, "AB1234", 1, Seq(), Seq(), ""),
        new SelfTestResult("eg33", "S1", jan_25_2002 + 12.hours.toMillis, false, "AB1234", 1, Seq(), Seq(), ""),
        new SelfTestResult("eg33", "S1", jan_25_2002 + 24.hours.toMillis, false, "AB1234", 1, Seq(), Seq(), ""))
      val result = MeasurementUnit.filterCalibrationReports(list, "SN1234")
      result.size must be(0)
    }
    "pass all test when one successful result with correct serial number" in {
      val list = Seq(
        new SelfTestResult("eg33", "S1", jan_25_2002, false, "AB1234", 1, Seq(), Seq(), ""),
        new SelfTestResult("eg33", "S1", jan_25_2002 + 12.hours.toMillis, false, "AB1234", 1, Seq(), Seq(), ""),
        new SelfTestResult("eg33", "S1", jan_25_2002 + 24.hours.toMillis, true, "AB1234", 1, Seq(), Seq(), "SN1234"))
      val result = MeasurementUnit.filterCalibrationReports(list, "SN1234")
      result must be(list)
    }
    "filter all test when one successful result with invalid serial number" in {
      val list = Seq(
        new SelfTestResult("eg33", "S1", jan_25_2002, false, "AB1234", 1, Seq(), Seq(), ""),
        new SelfTestResult("eg33", "S1", jan_25_2002 + 12.hours.toMillis, false, "AB1234", 1, Seq(), Seq(), ""),
        new SelfTestResult("eg33", "S1", jan_25_2002 + 24.hours.toMillis, true, "AB1234", 1, Seq(), Seq(), "SN1234"))
      val result = MeasurementUnit.filterCalibrationReports(list, "SN9999")
      result.size must be(0)
    }
    "pass all test when two successful result with correct serial number" in {
      val list = Seq(
        new SelfTestResult("eg33", "S1", jan_25_2002, false, "AB1234", 1, Seq(), Seq(), ""),
        new SelfTestResult("eg33", "S1", jan_25_2002 + 6.hours.toMillis, false, "AB1234", 1, Seq(), Seq(), ""),
        new SelfTestResult("eg33", "S1", jan_25_2002 + 12.hours.toMillis, true, "AB1234", 1, Seq(), Seq(), "SN1234"),
        new SelfTestResult("eg33", "S1", jan_25_2002 + 18.hours.toMillis, false, "AB1234", 1, Seq(), Seq(), ""),
        new SelfTestResult("eg33", "S1", jan_25_2002 + 24.hours.toMillis, true, "AB1234", 1, Seq(), Seq(), "SN1234"))
      val result = MeasurementUnit.filterCalibrationReports(list, "SN1234")
      result must be(list)
    }
    "filter tests when two successful result and only one with correct serial number" in {
      val list = Seq(
        new SelfTestResult("eg33", "S1", jan_25_2002, false, "AB1234", 1, Seq(), Seq(), ""),
        new SelfTestResult("eg33", "S1", jan_25_2002 + 6.hours.toMillis, false, "AB1234", 1, Seq(), Seq(), ""),
        new SelfTestResult("eg33", "S1", jan_25_2002 + 12.hours.toMillis, true, "AB1234", 1, Seq(), Seq(), "SN9999"),
        new SelfTestResult("eg33", "S1", jan_25_2002 + 18.hours.toMillis, false, "AB1234", 1, Seq(), Seq(), ""),
        new SelfTestResult("eg33", "S1", jan_25_2002 + 24.hours.toMillis, true, "AB1234", 1, Seq(), Seq(), "SN1234"))
      val result = MeasurementUnit.filterCalibrationReports(list, "SN1234")
      result.size must be(2)
      result must contain(list(3))
      result must contain(list(4))
    }
    "filter tests when three successful result and midle has invalid serial number" in {
      val list = Seq(
        new SelfTestResult("eg33", "S1", jan_25_2002, false, "AB1234", 1, Seq(), Seq(), ""),
        new SelfTestResult("eg33", "S1", jan_25_2002 + 6.hours.toMillis, true, "AB1234", 1, Seq(), Seq(), "SN1234"),
        new SelfTestResult("eg33", "S1", jan_25_2002 + 12.hours.toMillis, false, "AB1234", 1, Seq(), Seq(), ""),
        new SelfTestResult("eg33", "S1", jan_25_2002 + 18.hours.toMillis, true, "AB1234", 1, Seq(), Seq(), "SN9999"),
        new SelfTestResult("eg33", "S1", jan_25_2002 + 20.hours.toMillis, false, "AB1234", 1, Seq(), Seq(), ""),
        new SelfTestResult("eg33", "S1", jan_25_2002 + 24.hours.toMillis, true, "AB1234", 1, Seq(), Seq(), "SN1234"))
      val result = MeasurementUnit.filterCalibrationReports(list, "SN1234")
      result.size must be(5)
      result must contain(list(0))
      result must contain(list(1))
      result must contain(list(2))
      result must contain(list(4))
      result must contain(list(5))
    }
    "filter tests when expected situation with four successful results" in {
      val list = Seq(
        new SelfTestResult("eg33", "S1", jan_25_2002, true, "AB1234", 1, Seq(), Seq(), "SN1234"),
        new SelfTestResult("eg33", "S1", jan_25_2002 + 6.hours.toMillis, true, "AB1234", 1, Seq(), Seq(), "SN1234"),
        new SelfTestResult("eg33", "S1", jan_25_2002 + 12.hours.toMillis, false, "AB1234", 1, Seq(), Seq(), ""),
        new SelfTestResult("eg33", "S1", jan_25_2002 + 18.hours.toMillis, true, "AB1234", 1, Seq(), Seq(), "SN9999"),
        new SelfTestResult("eg33", "S1", jan_25_2002 + 20.hours.toMillis, false, "AB1234", 1, Seq(), Seq(), ""),
        new SelfTestResult("eg33", "S1", jan_25_2002 + 24.hours.toMillis, true, "AB1234", 1, Seq(), Seq(), "SN9999"))
      val result = MeasurementUnit.filterCalibrationReports(list, "SN1234")
      result.size must be(3)
      result must contain(list(0))
      result must contain(list(1))
      result must contain(list(2))
      val result2 = MeasurementUnit.filterCalibrationReports(list, "SN9999")
      result2.size must be(4)
      result2 must contain(list(2))
      result2 must contain(list(3))
      result2 must contain(list(4))
      result2 must contain(list(5))
    }
    //TODO:RB add multiple corridor test
  }

}