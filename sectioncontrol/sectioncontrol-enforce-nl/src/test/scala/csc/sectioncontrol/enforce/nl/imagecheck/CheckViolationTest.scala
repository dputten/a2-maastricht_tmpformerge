/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.nl.imagecheck

import csc.checkimage._
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers
import akka.actor.{ Actor, Props, ActorSystem }
import csc.sectioncontrol.messages._
import akka.testkit.TestProbe
import csc.sectioncontrol.messages.ValueWithConfidence
import csc.sectioncontrol.messages.VehicleViolationRecord
import csc.sectioncontrol.messages.VehicleMetadata
import csc.sectioncontrol.messages.IndicatorReason
import csc.sectioncontrol.messages.VehicleSpeedRecord
import csc.sectioncontrol.messages.Lane
import java.util.Date
import java.text.SimpleDateFormat
import akka.util.duration._
import csc.sectioncontrol.enforce.common.nl.violations.Violation

class CheckViolationTest extends WordSpec with MustMatchers with BeforeAndAfterAll {
  implicit val testSystem = ActorSystem("CheckViolationTest")

  val dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS")
  val jan_25_2002 = dateFormat.parse("25-01-2002 00:00:00.000").getTime
  val jan_26_2002 = dateFormat.parse("26-01-2002 00:00:00.000").getTime

  override protected def afterAll() {
    testSystem.shutdown()
    super.afterAll()
  }

  "CheckViolation" must {

    "send entry and exit image to recognizer" in {
      val checkRegistration = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckViolation(registrationCheck = checkRegistration.ref,
        intradaInterfaceToUse = CheckViolation.INTRADA_IFACE_NORMAL,
        confidenceLevel = 95)))
      val exitTime = jan_25_2002 + 1.minute.toMillis
      val vvr = makeVVR("test1", jan_25_2002, exitTime)
      val v = Violation(vvr, None, null, null, null, "", "")

      val sender = TestProbe()
      sender.send(testActor, new ViolationImageCheckRequest(reference = "testID", violation = v))

      //expect entry request
      val expectedEntry = vvr.classifiedRecord.speedRecord.entry
      val expectedMsg = new RegistrationImageCheckRequest(expectedEntry.license.get.value + exitTime + "|entry", registration = expectedEntry)
      checkRegistration.expectMsg(expectedMsg)
      //expect exit request
      val expectedExit = vvr.classifiedRecord.speedRecord.exit.get
      val expectedMsg2 = new RegistrationImageCheckRequest(expectedEntry.license.get.value + exitTime + "|exit", registration = expectedExit)
      checkRegistration.expectMsg(expectedMsg2)

      val result1 = new ImageCheckResult()
      result1.addRecognitionResult(RecognitionResult(RecognitionResult.ARH1, expectedEntry))
      checkRegistration.send(testActor, new RegistrationImageCheckResponse(expectedMsg.reference, expectedMsg.registration, result1))

      val result2 = new ImageCheckResult()
      result2.addRecognitionResult(RecognitionResult(RecognitionResult.ARH1, expectedExit))
      checkRegistration.send(testActor, new RegistrationImageCheckResponse(expectedMsg2.reference, expectedMsg2.registration, result2))

      val response = sender.expectMsgType[ViolationImageCheckResponse](1.second)
      response.reference must be("testID")
      response.violation must be(v)
      response.result.summaryCheck.getErrors() must be(0)
      response.result.entry.get.getErrors() must be(0)
      response.result.exit.get.getErrors() must be(0)

      testSystem.stop(testActor)
    }
    "skip entry and exit image" in {

      val checkRegistration = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckViolation(registrationCheck = checkRegistration.ref,
        intradaInterfaceToUse = CheckViolation.INTRADA_IFACE_NORMAL,
        confidenceLevel = 40)))
      val exitTime = jan_25_2002 + 1.minute.toMillis
      val vvr = makeVVR("test1", jan_25_2002, exitTime)
      val v = Violation(vvr, None, null, null, null, "", "")

      val sender = TestProbe()
      sender.send(testActor, new ViolationImageCheckRequest(reference = "testID", violation = v))
      checkRegistration.expectNoMsg()

      val response = sender.expectMsgType[ViolationImageCheckResponse](1.second)
      response.reference must be("testID")
      response.violation must be(v)
      response.result.summaryCheck.getErrors() must be(0)
      response.result.entry.get.getErrors() must be(0)
      response.result.exit.get.getErrors() must be(0)
      testSystem.stop(testActor)
    }

    "use truncated license when sending VMD to image recognizer" in {
      val checkRegistration = TestProbe()
      val testActor = testSystem.actorOf(Props(new CheckViolation(registrationCheck = checkRegistration.ref,
        intradaInterfaceToUse = CheckViolation.INTRADA_IFACE_NORMAL,
        confidenceLevel = 95)))

      val exitTime = jan_25_2002 + 1.minute.toMillis
      val vvr = makeVVR("test1", jan_25_2002, exitTime)
      val vvrFixed = vvr.copy(classifiedRecord = vvr.classifiedRecord.copy(
        validLicensePlate = Some("fixed")))
      val v = Violation(vvrFixed, None, null, null, null, "", "")

      val sender = TestProbe()
      sender.send(testActor, new ViolationImageCheckRequest(reference = "testID", violation = v))

      //expect entry request
      val license = vvr.classifiedRecord.speedRecord.entry.license.get.value
      val expectedEntry = vvr.classifiedRecord.speedRecord.entry.copy(license = Some(ValueWithConfidence("fixed", 0)))
      val expectedMsg = new RegistrationImageCheckRequest(license + exitTime + "|entry", registration = expectedEntry)
      checkRegistration.expectMsg(expectedMsg)
      //expect exit request
      val expectedExit = vvr.classifiedRecord.speedRecord.exit.get.copy(license = Some(ValueWithConfidence("fixed", 0)))
      val expectedMsg2 = new RegistrationImageCheckRequest(license + exitTime + "|exit", registration = expectedExit)
      checkRegistration.expectMsg(expectedMsg2)

      val result1 = new ImageCheckResult()
      result1.addRecognitionResult(RecognitionResult(RecognitionResult.ARH1, expectedEntry))
      checkRegistration.send(testActor, new RegistrationImageCheckResponse(expectedMsg.reference, expectedMsg.registration, result1))

      val result2 = new ImageCheckResult()
      result2.addRecognitionResult(RecognitionResult(RecognitionResult.ARH1, expectedExit))
      checkRegistration.send(testActor, new RegistrationImageCheckResponse(expectedMsg2.reference, expectedMsg2.registration, result2))

      val response = sender.expectMsgType[ViolationImageCheckResponse](1.second)
      response.reference must be("testID")
      response.violation must be(v)
      response.result.summaryCheck.getErrors() must be(0)
      response.result.entry.get.getErrors() must be(0)
      response.result.exit.get.getErrors() must be(0)

      testSystem.stop(testActor)
    }
  }

  def makeVVR(id: String,
              entryTime: Long,
              exitTime: Long,
              countryCode: Option[String] = Some("NL"),
              measuredSpeed: Int = 113,
              vehicleClass: Option[VehicleCode.Value] = Some(VehicleCode.PA),
              licensePlate: Option[String] = Some("AB12CD"),
              processingIndicator: ProcessingIndicator.Value = ProcessingIndicator.Automatic,
              enforcedSpeed: Int = 100,
              reason: IndicatorReason = IndicatorReason()) =
    VehicleViolationRecord(id,
      new VehicleClassifiedRecord(
        VehicleSpeedRecord(id,
          VehicleMetadata(Lane("1",
            "lane1",
            "gantry1",
            "route1",
            0.0f,
            0.0f),
            id,
            entryTime,
            Some(new Date(entryTime).toString),
            licensePlate.map(lp ⇒ ValueWithConfidence(lp, 42)),
            country = countryCode.map(cc ⇒ ValueWithConfidence(cc, 42)),
            images = Seq(),
            NMICertificate = "",
            applChecksum = "",
            serialNr = "SN1234"),
          Some(VehicleMetadata(Lane("1",
            "lane1",
            "gantry2",
            "route1",
            0.0f,
            0.0f),
            id,
            exitTime,
            Some(new Date(exitTime).toString),
            licensePlate.map(lp ⇒ ValueWithConfidence(lp, 42)),
            country = countryCode.map(cc ⇒ ValueWithConfidence(cc, 42)),
            images = Seq(),
            NMICertificate = "",
            applChecksum = "",
            serialNr = "SN1234")),
          2002, measuredSpeed, "SHA-1-foo"),
        vehicleClass,
        processingIndicator,
        false,
        false,
        None,
        false,
        reason),
      enforcedSpeed,
      None,
      recognizeResults = List())

}