package csc.sectioncontrol.enforce.nl.register

import java.io.File

import csc.akkautils.DirectLogging
import csc.curator.utils.Curator
import csc.sectioncontrol.decorate.DecorateService
import csc.sectioncontrol.enforce.common.nl.services.SectionControl
import csc.sectioncontrol.enforce.common.nl.violations.CorridorData
import csc.sectioncontrol.enforce.nl.casefiles.HHMVS40
import csc.sectioncontrol.messages.VehicleImageType.VehicleImageType
import csc.sectioncontrol.messages._
import csc.sectioncontrol.messages.certificates._
import csc.sectioncontrol.sign.certificate.CertificateService
import csc.sectioncontrol.storage.Decorate.DecorateConfig
import csc.sectioncontrol.storage.ZkCorridorInfo
import csc.sectioncontrol.storagelayer._
import csc.sectioncontrol.storagelayer.hbasetables.VehicleImageTable.Reader
import csc.util.test.FileTestUtils._
import csc.util.test.{ ObjectBuilder, Resources }
import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterAll, WordSpec }

/**
 * Tests ImageProcessor without the diversity of ViolationProcessor and ImageDecorator cases.
 * All the Curator/Zookeeper/Hbase dependent components had methods overriden where needed.
 * The units tests focus in 2 vectors (in a total of 2 x 3 test cases):
 * -legacy metadata (no version) OR version metadata
 * -valid source image checksum OR invalid source image checksum OR missing source image
 *
 * Created by carlos on 03.08.15.
 */
class ImageProcessorTest
  extends WordSpec
  with MustMatchers
  with BeforeAndAfterAll //with DecorateTestData
  {

  val resourceBaseDir = new File(Resources.getResourceDirPath().getAbsolutePath)
  import csc.sectioncontrol.enforce.nl.register.ImageProcessorTest._

  val decorateService: DecorateService = new TestDecorateService
  val imageContents: Array[Byte] = getFileContents("image.jpg", resourceBaseDir)

  "ImageProcessor" when {

    calledForLegacyMetadata + withValidChecksum must {
      "return the already existing image contents" in {

        val violation = MockViolation(version = None, checksum = correctChecksum)
        val entry = violation.vvr.classifiedRecord.speedRecord.entry

        val processor = new TestImageProcessor(Some(imageContents))
        val result: (Option[Array[Byte]], Option[Array[Byte]]) = processor.retrieveImages(entry, None, corridorData)

        result._1.isDefined must be(true)
        val equal = (imageContents.deep == result._1.get.deep)
        equal must be(true) //result image must be the same (not tempered in any way)
      }
    }

    calledForLegacyMetadata + withInvalidChecksum must {
      returnNoContent in {

        val violation = MockViolation(version = None, checksum = incorrectChecksum)
        val entry = violation.vvr.classifiedRecord.speedRecord.entry

        val processor = new TestImageProcessor(Some(imageContents))
        val result: (Option[Array[Byte]], Option[Array[Byte]]) = processor.retrieveImages(entry, None, corridorData)

        result._1.isDefined must be(false)
      }
    }

    calledForLegacyMetadata + withMissingSourceImage must {
      returnNoContent in {

        val violation = MockViolation(version = None, checksum = correctChecksum)
        val entry = violation.vvr.classifiedRecord.speedRecord.entry

        val processor = new TestImageProcessor(None)
        val result: (Option[Array[Byte]], Option[Array[Byte]]) = processor.retrieveImages(entry, None, corridorData)

        result._1.isDefined must be(false)
      }
    }

    calledForVersionedMetadata + withValidChecksum must {
      "return some new image contents" in {

        val violation = MockViolation(version = Some(newVersion), checksum = correctChecksum)
        val entry = violation.vvr.classifiedRecord.speedRecord.entry

        val processor = new TestImageProcessor(Some(imageContents))
        val result: (Option[Array[Byte]], Option[Array[Byte]]) = processor.retrieveImages(entry, None, corridorData)

        result._1.isDefined must be(true)
        val equal = (imageContents.deep == result._1.get.deep)
        equal must be(false) //result image must be the different from source (decorated)
      }
    }

    calledForVersionedMetadata + withInvalidChecksum must {
      returnNoContent in {

        val violation = MockViolation(version = Some(newVersion), checksum = incorrectChecksum)
        val entry = violation.vvr.classifiedRecord.speedRecord.entry

        val processor = new TestImageProcessor(Some(imageContents))
        val result: (Option[Array[Byte]], Option[Array[Byte]]) = processor.retrieveImages(entry, None, corridorData)

        result._1.isDefined must be(false)
      }
    }

    calledForVersionedMetadata + withMissingSourceImage must {
      returnNoContent in {

        val violation = MockViolation(version = Some(newVersion), checksum = incorrectChecksum)
        val entry = violation.vvr.classifiedRecord.speedRecord.entry

        val processor = new TestImageProcessor(None)
        val result: (Option[Array[Byte]], Option[Array[Byte]]) = processor.retrieveImages(entry, None, corridorData)

        result._1.isDefined must be(false)
      }
    }

  }

  /**
   * Test impl of DecorateService, overriding all zookeeper dependent methods
   */
  class TestDecorateService extends DecorateService(null, defaultSystemId) {

    override protected def getDecorateConfig(lane: Lane): DecorateConfig = decorateConfig
    override protected def getCorridorConfig(corridorId: String): Option[CorridorConfig] = Some(corridorConfig)
    override protected def getSystemConfig(): SystemConfiguration = null //not used
    override protected def certificateService: CertificateService = dummyCertificateService
  }

  /**
   * Test impl of ImageProcessor, overriding all zookeeper and actor dependent methods
   */
  class TestImageProcessor(imgContents: Option[Array[Byte]]) extends ImageProcessor with DirectLogging {

    override private[register] def systemId: String = defaultSystemId
    override private[register] def violationImageReaders: Map[VehicleImageType.Value, Reader] = Map() //not used
    override private[register] def publishSystemEvent(event: SystemEvent): Unit = () //ignored
    override protected def createDecorateService(): DecorateService = decorateService
    override protected def curator: Curator = null //not used
    override protected def getCorridorId(infoId: Int): Option[String] = Some(infoId.toString) //dummy id conversion

    //overriden to bypass all the image reader delegation logic
    override protected def loadImageContents(ref: (Long, String, String), imageType: VehicleImageType): Option[Array[Byte]] = imgContents
  }
}

object ImageProcessorTest extends ObjectBuilder {

  val correctChecksum = "f86c30d7b729e8cf82893c000d6dc1a3"
  val incorrectChecksum = "bogusChecksum"

  val calledForLegacyMetadata = "called retrieveImages for a legacy VehicleMetadata (not versioned) "
  val calledForVersionedMetadata = "called retrieveImages for a versioned VehicleMetadata "

  val withValidChecksum = "with checksum-valid source image"
  val withInvalidChecksum = "with invalid checksum source image"
  val withMissingSourceImage = "with missing source image"
  val returnNoContent = "return no content"
  val newVersion = MetadataVersion.withLicensePlateData

  val defaultSystemId = MockViolation.defaultSystemId

  val decorateConfig = create[DecorateConfig].copy(
    rowHeightPx = 24,
    licenseQuality = 80,
    licenseMaxWidthPx = 400,
    licenseMaxHeightPx = 400)

  val serviceConfig = create[SectionControlConfig]
  val service = create[SectionControl]

  val corridorData: CorridorData = using(service, new HHMVS40).create[CorridorData].copy(
    corridorId = MockViolation.defaultCorridorId)

  val corridorInfo = create[ZkCorridorInfo].copy(corridorId = MockViolation.defaultCorridorId)

  val corridorConfig = using(serviceConfig).create[CorridorConfig].copy(info = corridorInfo)

  val cert = "dummy Certificate"

  lazy val activeCertificate = ComponentCertificate("Softwarezegel", "d41d8cd98f00b204e9800998ecf8427e")
  lazy val componentCertificate = ComponentCertificate("Configuratiezegel", "ABCDEF1234567890")
  lazy val activeJarCertificate = ActiveCertificate(0, componentCertificate)

  lazy val currentCertificate = new MeasurementMethodInstallationType(0L,
    new MeasurementMethodType(typeDesignation = "",
      category = "A",
      unitSpeed = "km/h",
      unitRedLight = "s",
      unitLength = "m",
      restrictiveConditions = "",
      displayRange = "20-250 km/h",
      temperatureRange = "temperatuurbereik -10 to 50 graden Celsius",
      permissibleError = "toelatbare fout 3%",
      typeCertificate = new TypeCertificate(cert, 0, List())))

  lazy val dummyCertificateService = new CertificateService {
    override def getActiveConfigurationCertificate(curator: Curator, systemId: String, time: Long): ComponentCertificate =
      componentCertificate

    override def getActiveSoftwareCertificate(curator: Curator, systemId: String, time: Long): ComponentCertificate =
      activeCertificate

    override def getCurrentCertificate(curator: Curator, systemId: String, time: Long): Option[MeasurementMethodInstallationType] =
      Some(currentCertificate)

    override def saveActiveCertificate(curator: Curator, systemId: String, certificate: ActiveCertificate): Unit = {}
  }

}
