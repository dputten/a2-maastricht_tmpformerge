/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

package csc.sectioncontrol.enforce.nl.casefiles

/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.enforce.common.nl.services._
import csc.sectioncontrol.enforce.common.nl.services.RedLight
import csc.sectioncontrol.enforce.common.nl.services.SpeedFixed
import csc.sectioncontrol.enforce.common.nl.services.ANPR
import csc.sectioncontrol.enforce.common.nl.services.SectionControl
import csc.sectioncontrol.enforce.common.nl.messages.HeaderInfo
import csc.sectioncontrol.messages.certificates.{ ComponentCertificate, TypeCertificate, MeasurementMethodType }

class TCVS33Test extends WordSpec with MustMatchers {

  "TC VS v3.3 support" must {
    val caseFile = new TCVS33()
    val service = new SectionControl()
    "indicate that extra info is needed for the output files " in {
      caseFile.showExtraInfo must be(false)
    }
    "name 'stanby' correctly" in {
      caseFile.mtmInfoStandBy must be("standby")
    }
    "create the correct case file headers" when {
      val headerInfo = HeaderInfo("systemName", "serialNumber", new MeasurementMethodType(typeDesignation = "XX123",
        category = "A",
        unitSpeed = "km/h",
        unitRedLight = "s",
        unitLength = "m",
        restrictiveConditions = "",
        displayRange = "20-250 km/h",
        temperatureRange = "temperatuurbereik -10 to 50 graden Celsius",
        permissibleError = "toelatbare fout 3%",
        typeCertificate = new TypeCertificate("eg33-cert", 3,
          List(ComponentCertificate("matcher1", "blah1"),
            ComponentCertificate("vr2", "blah2")))))

      "the service is SectionControl" in {
        val header = caseFile.createHeader(new SectionControl(), headerInfo)
        header.size must be(6)
        header(0) must be("# Trajectcontrolesysteem " + headerInfo.systemName)
        header(1) must be("# Categorie A meetmiddel")
        header(2) must be("# NMi typegoedkeuringsnummer " + headerInfo.methodType.typeCertificate.id)
        header(3) must be("# Serienummer " + headerInfo.serialNumber)
        header(4) must be("# Snelheden in km/h")
        header(5) must be("# Trajectlengtes in m")
      }
      "the service is something else" in {
        val header_0 = caseFile.createHeader(new ANPR(), headerInfo)
        val header_1 = caseFile.createHeader(new RedLight(pardonRedTime = 0, minimumYellowTime = 0, factNumber = "1234"), headerInfo)
        val header_2 = caseFile.createHeader(new SpeedFixed(), headerInfo)
        val header_3 = caseFile.createHeader(new SpeedMobile(), headerInfo)
        val header_4 = caseFile.createHeader(new TargetGroup(factNumber = "1234"), headerInfo)

        header_0.size must be(0)
        header_1.size must be(0)
        header_2.size must be(0)
        header_3.size must be(0)
        header_4.size must be(0)
      }
    }
    "create stats header must be according spec" in {
      val lines = caseFile.createStats(service, 1000, 100, 10).zipWithIndex.toMap.map(_.swap)
      lines.size must be(4)

      /*
        - line out to 25 characters
        - place a '=' character on the 26 character
        - place a tab on the 27 character
        - place a numeric value on the 28 character
       */
      lines(0) must be("Totaal aantal beelden    =\t1100")
      lines(1) must be("Nummerbord onleesbaar    =\t100")
      lines(2) must be("Geldige kentekens        =\t1000")
      lines(3) must be("Automatisch herk. zaken  =\t1000")
    }
  }
}
