package csc

import org.scalatest.matchers.MustMatchers
import org.scalatest.WordSpec
import csc.sectioncontrol.storage.ZkPSHTMIdentification
import csc.sectioncontrol.enforce.nl.run.LoadSystemConfiguration

class PshTmFormatTest extends WordSpec with MustMatchers {
  "PshTmFormat" must {

    "return the correct ordered format string for A4/A12 based on ZkPSHTMIdentification settings when calling getPshTmIdFormat.getFormat" in {
      val pshTmIdSpec = ZkPSHTMIdentification(true, false, false, false, true, false, true, true, false, "", Some("HTYMDNVLX"))
      val result = LoadSystemConfiguration.getFormat(pshTmIdSpec, pshTmIdSpec.elementOrderString.getOrElse(""))
      result must be("[HHHH][T][YY][NNN]")
    }

    "return the correct ordered format string based on ZkPSHTMIdentification settings when calling getPshTmIdFormat.getFormat" in {
      val pshTmIdSpec = ZkPSHTMIdentification(true, false, false, false, true, false, true, false, true, "3", Some("HXYN"))
      val result = LoadSystemConfiguration.getFormat(pshTmIdSpec, pshTmIdSpec.elementOrderString.getOrElse(""))
      result must be("[HHHH]3[YY][NNN]")
    }

    "return the correct ordered format string based on ZkPSHTMIdentification settings when calling getPshTmIdFormat.getFormat with all parameters set" in {
      val pshTmIdSpec = ZkPSHTMIdentification(true, true, true, true, true, true, true, true, true, "4", Some("YMDNVLHTX"))
      val result = LoadSystemConfiguration.getFormat(pshTmIdSpec, pshTmIdSpec.elementOrderString.getOrElse(""))
      result must be("[YY][MM][DD][NNN][VVVV][LLLL][HHHH][T]4")
    }

    "return the correct ordered format string based on ZkPSHTMIdentification settings when calling getPshTmIdFormat.getFormat and the ZkPSHTMIdentification " +
      "settings contains more elements than the elementOrderString" in {
        val pshTmIdSpec = ZkPSHTMIdentification(true, true, true, true, true, true, true, true, true, "3", Some("HXYN"))
        val result = LoadSystemConfiguration.getFormat(pshTmIdSpec, pshTmIdSpec.elementOrderString.getOrElse(""))
        result must be("[HHHH]3[YY][NNN]")
      }

    "return an empty string when no ZkPSHTMIdentification settings available when calling getPshTmIdFormat.getFormat" in {
      val pshTmIdSpec = ZkPSHTMIdentification(false, false, false, false, false, false, false, false, false, "", Some("HXYN"))
      val result = LoadSystemConfiguration.getFormat(pshTmIdSpec, pshTmIdSpec.elementOrderString.getOrElse(""))
      result must be("")
    }

    "return an empty string with ZkPSHTMIdentification settings available but no elementOrderString when calling getPshTmIdFormat.getFormat" in {
      val pshTmIdSpec = ZkPSHTMIdentification(true, true, true, true, true, true, true, true, true, "4", Some(""))
      val result = LoadSystemConfiguration.getFormat(pshTmIdSpec, pshTmIdSpec.elementOrderString.getOrElse(""))
      result must be("")
    }

    "skip a elementOrderString item when not set in the ZkPSHTMIdentification settings when calling getPshTmIdFormat.getFormat" in {
      val pshTmIdSpec = ZkPSHTMIdentification(false, false, false, false, true, false, true, false, true, "3", Some("HXYN"))
      val result = LoadSystemConfiguration.getFormat(pshTmIdSpec, pshTmIdSpec.elementOrderString.getOrElse(""))
      result must be("[HHHH]3[NNN]")
    }
  }
}

