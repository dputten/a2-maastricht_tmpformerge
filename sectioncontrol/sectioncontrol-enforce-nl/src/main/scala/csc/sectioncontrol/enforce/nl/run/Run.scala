/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.nl.run

import akka.actor.ActorRef
import csc.akkautils.RunToken

/**
 * Shared Run message used by the enforce-nl job management.
 * @param zkJobPath
 * @param runner
 * @param runToken
 * @param actionDate the date, the actor should process
 * @param manageEnforceStatus
 *
 * @author Maarten Hazewinkel
 */
case class Run(zkJobPath: String,
               runner: ActorRef,
               runToken: RunToken,
               actionDate: Long,
               manageEnforceStatus: ActorRef)

