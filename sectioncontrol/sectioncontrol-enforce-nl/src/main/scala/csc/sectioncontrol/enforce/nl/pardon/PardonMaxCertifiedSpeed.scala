/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.nl.pardon

import csc.sectioncontrol.enforce.nl.register.PardonedViolations
import csc.sectioncontrol.enforce.common.nl.violations.Violation
import csc.akkautils.DirectLogging

object PardonMaxCertifiedSpeed extends DirectLogging {

  def pardon(violations: Seq[Violation], maxCertifiedSpeed: Int): PardonViolationOutput = {
    /*
     * Seperate the 'complete' violations in two lists:
     * 	1) Over the maximum certified speed
     *  2) Not over the maximum certified speed
     */
    val (overMaxCertifiedSpeed, notPardonedViolations) = violations.partition(_.measuredSpeed > maxCertifiedSpeed)

    log.info("Number of Violations above maximum certified speed {}, number under/equal to maximum certified speed {}. MaxCertSpeed = {}",
      overMaxCertifiedSpeed.size, notPardonedViolations.size, maxCertifiedSpeed)

    val pardon = overMaxCertifiedSpeed.map {
      violation ⇒
        PardonedViolations(Seq(violation.vvr),
          "measuredSpeed [%s] is higher then maxCertifiedSpeed [%s]".format(violation.measuredSpeed, maxCertifiedSpeed))
    }
    PardonViolationOutput(notPardonedViolations, overMaxCertifiedSpeed, pardon)
  }

}

