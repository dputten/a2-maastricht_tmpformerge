/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.nl.run

import akka.event.LoggingReceive
import akka.actor.SupervisorStrategy.Stop
import akka.actor.{ Props, AllForOneStrategy, Terminated, ActorRef }

import csc.akkautils.{ JobFailed, JobComplete }
import csc.hbase.utils.HBaseTableKeeper
import csc.curator.utils.{ Curator, CuratorActor }

import csc.sectioncontrol.enforce.EnforceBoot
import csc.sectioncontrol.classify.nl.ClassificationCompletionStatus
import csc.sectioncontrol.storage._
import csc.sectioncontrol.messages.{ ViolationCandidateRecord, VehicleClassifiedRecord, StatsDuration }
import csc.sectioncontrol.enforce.nl.qualify._
import csc.sectioncontrol.common.DayUtility
import csc.sectioncontrol.enforce.nl.classify.{ MultiCorridorClassify, MultiCorridorProcessor }
import org.apache.hadoop.conf.Configuration
import csc.sectioncontrol.storagelayer.hbasetables.{ ConfirmedViolationRecordTable, ViolationRecordTable, VehicleClassifiedTable }

/**
 * Handle the running of the QualifyViolations job.
 *
 * This job needs to wait for both the MTM file processing to be complete for the previous day, as well
 * as for the classifier job to have completed all processing for the previous day.
 *
 * While those conditions are not satisfied, this job will re-schedule itself to run every minute.
 * Once the conditions are met the job will run through and re-schedule itself for the next day.
 *
 * @author Maarten Hazewinkel
 */
class RunQualifyViolations(systemId: String,
                           protected val curator: Curator,
                           hbaseConfig: Configuration,
                           storeMtmData: ActorRef) extends CuratorActor with HBaseTableKeeper {

  import EnforceApplicationConfig._

  var currentRun: Option[Run] = None

  val recordsReader = umtsEnabled match {
    case UMTS_ENABLED  ⇒ log.info("Using Confirmed Violations as input"); ClassifiedRecordsRetriever(None, Some(ConfirmedViolationRecordTable.makeReader(hbaseConfig, this, systemId)))
    case UMTS_DISABLED ⇒ log.info("Using Classified Records as input"); ClassifiedRecordsRetriever(Some(VehicleClassifiedTable.makeReader(hbaseConfig, this, systemId)), None)
  }

  private val qualifyViolations = context.actorOf(Props(new QualifyViolations(systemId,
    recordsReader,
    ViolationRecordTable.makeWriter(hbaseConfig, this),
    EnforceBoot.getSpeedLog(systemId),
    storeMtmData,
    makeMultiCorridorProcessor())),
    "qualifyViolations")

  protected def receive = LoggingReceive {
    case run @ Run(_, _, _, actionDate, manageStatus) ⇒
      currentRun = Some(run)
      manageStatus ! IsMtmProcessingCompleted(actionDate)
      context.become(receiveRunning)

    case msg ⇒
      log.error("{} received unexpected message {}", self.path, msg)
  }

  def getClassificationCompletionStatus(systemId: String): Option[ClassificationCompletionStatus] = {
    umtsEnabled match {
      case UMTS_ENABLED  ⇒ curator.get[ClassificationCompletionStatus](Paths.Systems.getProcessedRegistrationsStatusPath(systemId))
      case UMTS_DISABLED ⇒ curator.get[ClassificationCompletionStatus](Paths.Systems.getClassificationCompletionStatusPath(systemId))
    }
  }

  def receiveRunning: Receive = LoggingReceive {
    case run @ Run(zkJobPath, runner, runToken, actionDate, manageStatus) ⇒
      log.error("{} is already processing another run: {}", self.path, currentRun.get)
      runner ! JobFailed(retry = true)

    case mps @ MtmProcessingStatus(true, actionDate, _) ⇒
      if (actionDate == currentRun.get.actionDate) {
        val (dayStart, dayEnd) = DayUtility.calculateDayPeriod(actionDate)
        val classificationCompletionStatus = getClassificationCompletionStatus(systemId)

        if (dayEnd < classificationCompletionStatus.map(_.completeUntil).getOrElse(Long.MinValue)) {
          context.watch(qualifyViolations)
          qualifyViolations ! QualifyViolationsForDay(actionDate,
            LoadSystemConfiguration(systemId, dayStart, dayEnd, DayUtility.fileExportTimeZone, curator),
            StatsDuration(dayStart, DayUtility.fileExportTimeZone))
        } else {
          log.info("{} waiting for classification to complete until {}", self.path, dayEnd)
          stopRunning(false)
        }
      } else {
        log.error("{} received non-matching message {}", self.path, mps)
      }

    case mps @ MtmProcessingStatus(false, actionDate, _) ⇒
      if (actionDate == currentRun.get.actionDate) {
        log.info("{} waiting for mtm processing to complete for {}", self.path, actionDate)
        stopRunning(false)
      } else {
        log.error("{} received non-matching message {}", self.path, mps)
      }

    case QualifyViolationsFailed(reason) ⇒
      log.error("qualifying violations for {} failed for reason: {}", systemId, reason)
      context.unwatch(qualifyViolations)
      stopRunning(false)

    case QualifyViolationsSuccessful ⇒
      log.info("qualifying violations for {} completed successfully", systemId)
      context.unwatch(qualifyViolations)
      stopRunning(true)

    case Terminated(`qualifyViolations`) ⇒
      log.error("qualifying violations for {} crashed", systemId)
      context.unwatch(qualifyViolations)
      stopRunning(false)
  }

  def stopRunning(completed: Boolean) {
    val run = currentRun.get
    if (completed) {
      run.manageEnforceStatus ! QualificationCompleted(run.actionDate)
      run.runner ! JobComplete
    } else {
      run.runner ! JobFailed(retry = true)
    }
    currentRun = None
    context.unbecome()
  }

  override def supervisorStrategy() = AllForOneStrategy() {
    case e: Exception ⇒
      log.error(e, "Qualifying violations crashed. Scheduling re-run.")
      currentRun match {
        case Some(run) ⇒
          run.runner ! JobFailed(retry = true)
        case None ⇒
          ()
      }
      Stop
  }

  override def postStop() {
    try {
      closeTables()
    } finally {
      super.postStop()
    }
  }

  private def makeMultiCorridorProcessor(): MultiCorridorProcessor = {
    val cfg = curator.get[ZkMultiCorridorConfig](Paths.Systems.getMultiCorridorConfigPath(systemId))
    val classifyConfig = cfg.map(_.classify).getOrElse(MultiCorridorClassifyConfig())
    new MultiCorridorClassify(classifyConfig)
  }
}
