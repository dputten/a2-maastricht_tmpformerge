/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.nl.run

import csc.sectioncontrol.storage.{ EnforcementStatus, Paths }
import csc.config.Path
import akka.event.LoggingReceive
import csc.curator.utils.{ Versioned, Curator, CuratorActor }

/**
 * Manages status information on the various enforcement related jobs for the system id specified
 *
 * @author Maarten Hazewinkel
 */
private[nl] class ManageEnforceStatus(systemId: String, protected val curator: Curator) extends CuratorActor {
  protected def receive = LoggingReceive {
    case ProcessingMtmCompleted(date, time) ⇒
      updateSystemStatus(date, _.copy(mtmDataProcessed = true, mtmDataProcessingTime = time))

    case IsMtmProcessingCompleted(date) ⇒
      val status = loadStatusAndVersion(getPath(date))._1
      sender ! MtmProcessingStatus(status.mtmDataProcessed, date, status.mtmDataProcessingTime)

    case QualificationCompleted(date) ⇒
      updateSystemStatus(date, _.copy(qualificationComplete = true))

    case IsQualificationCompleted(date) ⇒
      sender ! QualificationStatus(loadStatusAndVersion(getPath(date))._1.qualificationComplete, date)

    case RegistrationCompleted(date) ⇒
      updateSystemStatus(date, _.copy(registrationComplete = true))

    case IsRegistrationCompleted(date) ⇒
      sender ! RegistrationStatus(loadStatusAndVersion(getPath(date))._1.registrationComplete, date)
  }

  private[nl] def updateSystemStatus(date: Long,
                                     statusUpdate: (EnforcementStatus ⇒ EnforcementStatus)) {
    val path = getPath(date)
    val (status, version) = loadStatusAndVersion(path)
    val updatedStatus = statusUpdate(status)
    // A failure in storing the new value causes the (default) supervisor strategy to restart this
    // actor, and the status update will then be lost. This is not a problem. Enforcement might redo some
    // work that is already done, but should not fail in any way.
    curator.set(path, updatedStatus, version.toInt)
  }

  private[nl] def loadStatusAndVersion(path: Path): (EnforcementStatus, Long) = {
    curator.getVersioned[EnforcementStatus](path) match {
      case Some(Versioned(s, v)) ⇒ (s, v)
      case None ⇒
        val status = EnforcementStatus()
        curator.put(path, status)
        loadStatusAndVersion(path)
    }
  }

  private def getPath(date: Long): Path = {
    Path(Paths.Systems.getEnforceStatusPath(systemId, date))
  }

}

/**
 * Signal that MTM processing has been completed for the date specified
 */
private[nl] case class ProcessingMtmCompleted(date: Long, processedAtTime: Long)

/**
 * Requests the status on MTM processing for the date specified
 */
private[nl] case class IsMtmProcessingCompleted(date: Long)

/**
 * Status reply on MTM processing for the date specified
 */
private[nl] case class MtmProcessingStatus(processed: Boolean, date: Long, processingTime: Long)

/**
 * Signal that violation qualification has been completed for the date specified
 */
private[nl] case class QualificationCompleted(date: Long)

/**
 * Requests the status on violation qualification for the date specified
 */
private[nl] case class IsQualificationCompleted(date: Long)

/**
 * Status reply on violation qualification for the date specified
 */
private[nl] case class QualificationStatus(completed: Boolean, date: Long)

/**
 * Signal that violation registration has been completed for the date specified
 */
private[nl] case class RegistrationCompleted(date: Long)

/**
 * Requests the status on violation registration for the date specified
 */
private[nl] case class IsRegistrationCompleted(date: Long)

/**
 * Status reply on violation registration for the date specified
 */
private[nl] case class RegistrationStatus(completed: Boolean, date: Long)
