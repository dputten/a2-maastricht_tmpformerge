/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.nl.run

import annotation.tailrec
import scala.Left
import scala.Some

import java.io.File
import java.text.SimpleDateFormat
import java.util.TimeZone

import org.apache.commons.io.FileUtils

import akka.actor._
import akka.util.duration._
import akka.actor.SupervisorStrategy.Stop
import akka.actor.AllForOneStrategy
import akka.event.LoggingReceive
import akka.actor.Terminated

import csc.curator.utils.{ Curator, CuratorActor }
import csc.akkautils._

import csc.sectioncontrol.storage._
import csc.sectioncontrol.enforce._
import csc.sectioncontrol.enforce.speedlog.GeneratedExcludeEvents
import csc.sectioncontrol.mtm.nl._
import nl.register.LogMTMRaw
import csc.sectioncontrol.messages.{ StatsDuration, Interval, SystemEvent }
import csc.sectioncontrol.mtm.nl.MtmSource
import csc.sectioncontrol.storage.ZkSection
import csc.sectioncontrol.enforce.speedlog.ScheduledSpeed
import csc.sectioncontrol.mtm.nl.CorridorConfig
import csc.sectioncontrol.enforce.speedlog.GenerateExcludeEvents
import csc.sectioncontrol.storage.ZkCorridor
import csc.sectioncontrol.common.DayUtility
import csc.sectioncontrol.storagelayer.TimewatchService

/**
 * Handle the running of the MTM file processing job.
 *
 * This job needs to wait for the MTM file to arrive and be of a certain age. The last is to ensure that the
 * file is not still in the process of being written to.
 *
 * While those conditions are not satisfied, this job will re-schedule itself to run every minute.
 * Once the conditions are met the job will run through and re-schedule itself for the next day.
 *
 * @author Maarten Hazewinkel
 */
class RunProcessMtmData(systemId: String,
                        protected val curator: Curator,
                        speedLog: ActorRef,
                        storeMtmData: ActorRef,
                        timeZone: String) extends CuratorActor {

  var runner: ActorRef = _

  private val processMtmData = context.actorOf(Props(new ProcessMtmData(curator, systemId)), "processMtmData")

  protected def receive = LoggingReceive {
    case Run(zkJobPath, jobRunner, runToken, actionDate, manageEnforceStatus) ⇒
      runner = jobRunner
      val configPath = zkJobPath + "/config"
      curator.get[ProcessMtmDataJobConfig](configPath) match {
        case None ⇒
          log.error("processMtmDataJob config not found at {}", configPath)
          runner ! JobFailed
        case Some(config) ⇒
          val expectedFile = config.getExpectedFile(actionDate, timeZone)
          if (expectedFile.exists() && runToken.startTime - expectedFile.lastModified() > config.filePickupDelay) {
            val expectedDay = DayUtility.addOneDay(actionDate)
            val (expectedDayStart, expectedDayEnd) = DayUtility.calculateDayPeriod(expectedDay)
            if (expectedFile.lastModified() >= expectedDayStart && expectedFile.lastModified() < expectedDayEnd) {
              log.info("expected file found at {}", expectedFile.getAbsolutePath)
              val configs = CorridorConfig.getCorridorConfigs(curator, systemId)
              if (configs.nonEmpty) {
                val (dataDayStart, dataDayEnd) = DayUtility.calculateDayPeriod(actionDate)
                val (dayStart, _) = DayUtility.calculateDayPeriod(actionDate)
                val processingMessage = MtmSource(expectedFile.getAbsolutePath,
                  speedLog,
                  configs,
                  dataDayStart,
                  dataDayEnd,
                  getDstChange,
                  Some(StatsDuration(dayStart, DayUtility.fileExportTimeZone)))

                context.actorOf(Props(new Actor {
                  protected def receive = LoggingReceive {
                    case "start" ⇒
                      context.watch(processMtmData)
                      log.info("Starting processing MTM data for {} with message {}", systemId, processingMessage)
                      processMtmData ! processingMessage

                    case error @ (0, 0) ⇒
                      log.error("Processing MTM data for {} failed", systemId)
                      manageEnforceStatus ! ProcessingMtmCompleted(actionDate, runToken.startTime)
                      setSpeedsToUndetermined(actionDate, configs)
                      storeMtmRuw(expectedFile, actionDate)
                      moveFileToProcessed(expectedFile, TimeZone.getTimeZone(timeZone))
                      runner ! JobFailed

                    case success @ (systemCount, speedCount) ⇒
                      log.info("processing MTM data for {} completed successfully", systemId)
                      val (dayStart, dayEnd) = DayUtility.calculateDayPeriod(actionDate)
                      val corridorIds = CorridorConfig.getCorridorIds(curator, systemId)
                      waitGeneratedExcludeEventsCount = 0
                      val pardonCfg = getSystemConfig.map(_.pardonTimes).getOrElse(ZkSystemPardonTimes())
                      corridorIds.foreach {
                        corridorId ⇒
                          val corridor = curator.get[ZkCorridor](Paths.Corridors.getConfigPath(systemId, corridorId))
                          corridor.foreach {
                            corridorConfig ⇒
                              val technicalPardonTime_mSecs = pardonCfg.pardonTimeTechnical_secs * 1000L
                              val esaPardonTime_mSecs = pardonCfg.pardonTimeEsa_secs * 1000L
                              val scheduleSpeeds: List[ScheduledSpeed] = LoadSystemConfiguration.
                                getCorridorSchedules(systemId, corridorId, dayStart, dayEnd, TimeZone.getTimeZone(timeZone), curator).
                                map(s ⇒ ScheduledSpeed(s.startTime, s.endTime, s.enforcedSpeedLimit, s.supportMsgBoard))
                              waitGeneratedExcludeEventsCount += 1
                              EnforceBoot.getSpeedLog(systemId) ! GenerateExcludeEvents(dayStart,
                                dayEnd,
                                corridorConfig.info.corridorId,
                                technicalPardonTime_mSecs,
                                esaPardonTime_mSecs,
                                EnforceBoot.getExcludeLog(systemId),
                                scheduleSpeeds)
                          }
                      }
                      if (waitGeneratedExcludeEventsCount == 0) {
                        waitGeneratedExcludeEventsCount = 1
                        self ! GeneratedExcludeEvents
                      }

                    case GeneratedExcludeEvents ⇒
                      waitGeneratedExcludeEventsCount -= 1
                      if (waitGeneratedExcludeEventsCount == 0) {
                        storeMtmRuw(expectedFile, actionDate)
                        manageEnforceStatus ! ProcessingMtmCompleted(actionDate, runToken.startTime)
                        moveFileToProcessed(expectedFile, TimeZone.getTimeZone(timeZone))
                        runner ! JobComplete
                      }

                    case Terminated(`processMtmData`) ⇒
                      log.error("processing MTM data for {} crashed", systemId)
                      moveFileToProcessed(expectedFile, TimeZone.getTimeZone(timeZone))
                      context.system.eventStream.publish(SystemEvent("ProcessMtmData", System.currentTimeMillis(),
                        systemId, "Alert", "system", Some("MTM verwerking is gefaald.")))
                      runner ! JobFailed
                  }
                }), "ProcessMtmData_" + systemId + "_monitor") ! "start"
              } else {
                log.info("no configured MTM boards found. Not processing file {}", expectedFile.getAbsolutePath)
                moveFileToProcessed(expectedFile, TimeZone.getTimeZone(timeZone))
                context.system.eventStream.publish(
                  SystemEvent("ProcessMtmData", System.currentTimeMillis(), systemId, "Alert", "system",
                    Some("Geen MTM borden ingesteld. Kan geen MTM data verwerken")))

                setSpeedsToUndetermined(actionDate, configs)
                manageEnforceStatus ! ProcessingMtmCompleted(actionDate, runToken.startTime)
                runner ! JobComplete
              }
            } else {
              val formatter = new SimpleDateFormat("yyyy-MM-dd")
              formatter.setTimeZone(TimeZone.getTimeZone(timeZone))
              val modifiedDate = formatter.format(expectedFile.lastModified())
              val expectedDate = formatter.format(expectedDay)

              log.error("Received file modified date {} does not match expected date {}",
                modifiedDate, expectedDate)

              moveFileToProcessed(expectedFile, TimeZone.getTimeZone(timeZone))
              runner ! JobFailed(retry = true)
            }
          } else {
            log.info("expected file {} not yet present, or modified too recently", expectedFile.getAbsolutePath)

            /*
             * Tell the one who started us that we failed.
             * RunFailed with rerun=true triggers running this again in 1 minute
             */
            runner ! JobFailed(retry = true)
          }
      }
  }

  var waitGeneratedExcludeEventsCount: Int = 0

  private def storeMtmRuw(mtmFile: File, workDay: Long) {
    val mtmRawBytes = FileUtils.readFileToByteArray(mtmFile)
    getCorridorIds.foreach {
      corridorId ⇒
        curator.get[ZkCorridor](Paths.Corridors.getConfigPath(systemId, corridorId)).foreach(c ⇒
          storeMtmData ! LogMTMRaw(workDay, c.info.corridorId, mtmRawBytes))
    }
  }

  def getDstChange: Option[Interval] = {
    TimewatchService.get(curator, systemId).dstChange
  }

  def setSpeedsToUndetermined(workDate: Long, configs: List[CorridorConfig]) {
    val (dataDayStart, _) = DayUtility.calculateDayPeriod(workDate)
    val speedLog = EnforceBoot.getSpeedLog(systemId)
    configs.foreach {
      config ⇒
        speedLog ! SpeedDataImpl(dataDayStart, config.corridorInfoId, -1, Left(SpeedIndicatorStatus.Undetermined))
    }
  }

  def getMsiInfo: List[CorridorConfig] = {
    getCorridorIds.flatMap {
      corridorId ⇒
        val corridor = curator.get[ZkCorridor](Paths.Corridors.getConfigPath(systemId, corridorId))
        val sections: Seq[ZkSection] = corridor.map(_.allSectionIds).getOrElse(Seq()).flatMap {
          sectionId ⇒ curator.get[ZkSection](Paths.Sections.getConfigPath(systemId, sectionId))
        }
        val matrixIds = sections.flatMap {
          section ⇒ section.matrixBoards.map(_.toInt)
        }.toSet.toList
        val msiBlackList = sections.flatMap {
          section ⇒ section.msiBlackList
        }.toSet.toList

        if (matrixIds.nonEmpty) {
          corridor.map(c ⇒ CorridorConfig(c.info.corridorId, matrixIds, msiBlackList))
        } else {
          None
        }

    }.toList
  }

  private def getCorridorIds: Seq[String] = {
    val corridorPaths = curator.getChildren(Paths.Corridors.getDefaultPath(systemId))
    corridorPaths.map {
      _.split('/').last
    }
  }

  private def getSystemConfig: Option[ZkSystem] = {
    curator.get[ZkSystem](Paths.Systems.getConfigPath(systemId))
  }

  @tailrec
  private def moveFileToProcessed(file: File, timeZone: TimeZone, extensionSeqNum: Int = 0) {
    val processedDir = new File(file.getParentFile, "Archive")

    if (!processedDir.exists()) {
      processedDir.mkdirs()
    }

    val formatter = new SimpleDateFormat("yyyy-MM-dd")
    formatter.setTimeZone(timeZone)
    val extension = if (extensionSeqNum != 0) {
      "." + extensionSeqNum.toString
    } else {
      ""
    }
    val targetFile = new File(processedDir, file.getName + extension)

    if (!targetFile.exists()) {
      FileUtils.moveFile(file, targetFile)
      log.info("Moved input file '" + file.getAbsolutePath + "' to '" + targetFile.getAbsolutePath + "'")
    } else {
      moveFileToProcessed(file, timeZone, extensionSeqNum + 1)
    }
  }

  override def supervisorStrategy() = AllForOneStrategy() {
    case e: Exception ⇒
      log.error(e, "Mtm processing crashed. Scheduling re-run.")
      runner ! JobFailed(retry = true)
      Stop
  }

}

case class ProcessMtmDataJobConfig(fileDeliveryPath: String,
                                   fileNamePattern: String,
                                   filePickupDelay: Long = 1.minute.toMillis) {
  def getExpectedFile(workDate: Long, timeZone: String): File = {
    val df = new SimpleDateFormat(fileNamePattern)
    df.setTimeZone(TimeZone.getTimeZone(timeZone))
    val fileName = df.format(workDate)
    new File(new File(fileDeliveryPath), fileName)
  }
}

private case class SpeedDataImpl(effectiveTimestamp: Long,
                                 corridorId: Int,
                                 sequenceNumber: Long,
                                 speedSetting: Either[SpeedIndicatorStatus.Value, Option[Int]]) extends SpeedEventData
