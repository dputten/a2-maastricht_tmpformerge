package csc.sectioncontrol.enforce.nl.run

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 11/4/14.
 */

import java.io.File
import java.text.SimpleDateFormat
import akka.util.duration._

import csc.sectioncontrol.common.DayUtility
import csc.akkautils.DirectLogging
import scala.annotation.tailrec
import java.util.TimeZone
import org.apache.commons.io.FileUtils

object EsaFileState extends Enumeration {
  type EsaFileState = Value
  val ReadyForProcessing, TooRecentlyModified, IncorrectModificationDate, FileNotFound = Value
}

/**
 * AbstractEsaFileHandler select ESA (Mtm, Tubes) files
 * and determines whether they're ready for processing
 */
trait AbstractEsaFileHandler extends DirectLogging {

  def expectedFile(): File

  def fileState(f: File): EsaFileState.Value
}

/**
 * Concreate ESA File Handler
 * @param forDate Determines the file to be picked
 * @param dateFormat Format used to produce the filename
 * @param deliveryPath Path where to look for files
 * @param processStart The time processing started
 * @param pickupDelay Timespan that must have elapsed after last modification
 *                    of the file
 */
class EsaFileHandler(forDate: Long, dateFormat: SimpleDateFormat, deliveryPath: String,
                     processStart: Long, pickupDelay: Long = 1.minute.toMillis)
  extends AbstractEsaFileHandler {

  override def expectedFile(): File = {
    val filename = dateFormat.format(forDate)
    new File(new File(deliveryPath), filename)
  }

  def expectedExportDay = DayUtility.addOneDay(forDate)

  override def fileState(f: File): EsaFileState.Value = {
    if (f.exists)
      fileState(f.lastModified())
    else
      EsaFileState.FileNotFound
  }

  def fileState(lastModified: Long): EsaFileState.Value = {
    log.info("processStart = " + processStart + " lastModified = " + lastModified +
      " pickupDelay = " + pickupDelay + " diff = " + (processStart - lastModified))
    val (dayStart, dayEnd) = DayUtility.calculateDayPeriod(expectedExportDay)
    val fileState = if (processStart - lastModified <= pickupDelay)
      EsaFileState.TooRecentlyModified
    else if (lastModified < dayStart || lastModified >= dayEnd)
      EsaFileState.IncorrectModificationDate
    else
      EsaFileState.ReadyForProcessing
    log.info("EsaFileState = " + fileState)
    fileState
  }
}

object EsaFileHandler extends DirectLogging {

  @tailrec
  def moveFileToProcessed(file: File, timeZone: TimeZone, extensionSeqNum: Int = 0) {
    val processedDir = new File(file.getParentFile, "Archive")

    if (!processedDir.exists()) {
      processedDir.mkdirs()
    }

    val formatter = new SimpleDateFormat("yyyy-MM-dd")
    formatter.setTimeZone(timeZone)
    val extension = if (extensionSeqNum != 0) {
      "." + extensionSeqNum.toString
    } else {
      ""
    }
    val targetFile = new File(processedDir, file.getName + extension)

    if (!targetFile.exists()) {
      FileUtils.moveFile(file, targetFile)
      log.info("Moved input file '" + file.getAbsolutePath + "' to '" + targetFile.getAbsolutePath + "'")
    } else {
      moveFileToProcessed(file, timeZone, extensionSeqNum + 1)
    }
  }

  def moveFileToProcessed(file: File, timeZoneStr: String) {
    val timeZone = TimeZone.getTimeZone(timeZoneStr)
    moveFileToProcessed(file, timeZone)
  }
}
