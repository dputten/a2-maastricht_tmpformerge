/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.nl.register

import java.util.{ Date, GregorianCalendar }

import akka.actor.{ Cancellable, Props, ActorRef, ActorLogging }
import akka.event.LoggingReceive
import akka.util.duration._
import akka.util.Duration
import csc.checkimage.{ ViolationImageCheckResponse, ViolationImageCheckRequest }

import csc.curator.utils.{ Curator, CuratorActor }

import csc.sectioncontrol.messages._
import csc.sectioncontrol.enforce._
import csc.sectioncontrol.enforce.common.nl.violations.SystemConfigurationData
import csc.sectioncontrol.enforce.common.nl.violations.Violation
import csc.sectioncontrol.enforce.register.{ MinimumSeparation, SplitViolations }
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storagelayer.excludelog.Exclusions
import csc.sectioncontrol.common.DayUtility
import csc.sectioncontrol.enforce.excludelog.GetExclusions
import csc.sectioncontrol.sign.certificate.CertificateService
import csc.sectioncontrol.storagelayer.hbasetables._
import csc.sectioncontrol.storage.KeyWithTimeIdAndPostFix
import csc.sectioncontrol.messages.IndicatorReason
import csc.sectioncontrol.storage.ZkSystem
import csc.sectioncontrol.storage.SelfTestResult
import csc.sectioncontrol.messages.certificates.ActiveCertificate
import csc.sectioncontrol.enforce.nl.pardon.{ PardonRegistrationTimeUnreliable, PardonMaxCertifiedSpeed }
import csc.sectioncontrol.enforce.nl.RegistrationTimeUnreliableConfig
import csc.sectioncontrol.storagelayer.state.StateService
import csc.sectioncontrol.storagelayer.corridor.{ CorridorIdMapping, CorridorService }
import csc.sectioncontrol.storagelayer.caseFile.CaseFileConfigService
import csc.sectioncontrol.storagelayer.system.SystemService

/**
 * Actor that handles exporting violation data as well as storing metadata required for export.
 *
 * Accepted messages are ExportViolations, LogCalibrationTestReport, LogMTMInfo, LogMTMRaw and Cleanup.
 *
 * This actor can run for a long time when processing the ExportViolations message  and should be configured
 * to run in a separate Dispatcher by the creator/supervisor of the actor.
 *
 * @author Maarten Hazewinkel
 */
class RegisterViolations(val systemId: String,
                         inputViolations: ViolationRecordTable.Reader,
                         private[register] val processedViolationStore: ProcessedViolationTable.Store,
                         private[register] val violationImageReaders: Map[VehicleImageType.Value, VehicleImageTable.Reader],
                         calibrationReportReader: SelfTestResultTable.Reader,
                         private[register] val calibrationImageReader: CalibrationImageTable.Reader,
                         mtmDataStore: MtmFileTable.Store,
                         private[register] val excludeLog: ActorRef,
                         checkViolationImagesActor: Option[ActorRef],
                         protected val curator: Curator,
                         timeoutImageResponse: Duration,
                         recognizerCountries: Seq[String],
                         maxCertifiedSpeed: Int,
                         recognizeWithARHAgain: Boolean,
                         intradaInterfaceToUse: Int,
                         minLowestLicConfLevel: Int,
                         minLicConfLevelRecognizer2: Int,
                         autoThresholdLevel: Int,
                         mobiThresholdLevel: Int,
                         pardonRegistrationTime: RegistrationTimeUnreliableConfig,
                         caseFileConfigService: CaseFileConfigService) extends CuratorActor with ReadMtmData
  with ActorLogging with ViolationProcessor {

  import RegisterViolations._

  val inputViolationReader = new ViolationDataReader(systemId, inputViolations)

  // Actor state
  private var receivedExclusions: Map[Int, Exclusions] = Map()
  private var requestedCorridorIds: Set[Int] = Set()
  private var exportInProgress: Boolean = false
  private var state: RVState = _
  private var imageCheckResults = Map[String, Option[ViolationImageCheckResponse]]()
  private var timeoutSchedule: Option[Cancellable] = None
  private var lastImageReceived: Long = 0

  // Subsidiary actors
  private val storeProcessedViolations = context.actorOf(Props(new StoreProcessedViolations(processedViolationStore, sha)))

  def receive = LoggingReceive {
    case ev @ ExportViolations(statsDuration, day, exportDirectory, systemConfiguration) ⇒
      log.debug("Receive the ExportViolations message for day {}", day)

      if (!exportInProgress) {
        exportInProgress = true
        val (dayStart, dayEnd) = DayUtility.calculateDayPeriod(day)
        val selfTestResults = readSelfTestResults(day)

        val (allActiveCertificatesAreValid, invalidCertificates) = validateActiveCertificates

        if (!allActiveCertificatesAreValid) {
          val certificateNames = invalidCertificates.map(_.componentCertificate.name).mkString("\r\n")
          val errorMsg = "Validatie van certificaat is gefaald (actieve component)\r\n" + certificateNames
          log.error(errorMsg)
          val lastSuccess = selfTestResults.values.flatten.toSeq.sortBy(_.time).reverse.find(_.success)
          val startExcludePeriod = lastSuccess.map(_.time).getOrElse(dayStart)
          excludeLog ! InvalidCertificateEvent(startExcludePeriod, 999, true, errorMsg)
          excludeLog ! InvalidCertificateEvent(dayEnd, 998, false, errorMsg)
          context.system.eventStream.publish(SystemEvent(componentId, System.currentTimeMillis(), systemId, "Alarm", "system", Some(errorMsg)))
        } else {
          log.info("All certificates are valid")
        }

        // Read all the violations of the day. Seperate them in complete and incomplete
        val (violations, incompleteViolations) = inputViolationReader.readViolations(dayStart,
          dayEnd + systemConfiguration.globalConfig.minimumViolationTimeSeparation,
          systemConfiguration)

        log.info("Retrieved {} total violations for the day {}. Complete: {}, Incomplete: {}",
          violations.size + incompleteViolations.size,
          day,
          violations.size,
          incompleteViolations.size)

        if (!incompleteViolations.isEmpty) {
          log.info("Cannot process {} violations because no schedule or reportingOfficerCode is defined for them",
            incompleteViolations.size)

          incompleteViolations.groupBy(_._2).
            foreach { incompleteViolationsGroup ⇒
              val error = incompleteViolationsGroup._1
              val vvrs = incompleteViolationsGroup._2.map(_._1)
              storeProcessedViolations ! PardonedViolations(vvrs, error)
            }
        }

        /*
         * Seperate the violations in two lists:
         * 	1) Over the maximum certified speed
         *  2) Not over the maximum certified speed
         */
        val overMaxCertifiedSpeedOutput = PardonMaxCertifiedSpeed.pardon(violations, maxCertifiedSpeed)
        overMaxCertifiedSpeedOutput.pardoned.foreach {
          pardonViolation ⇒ storeProcessedViolations ! pardonViolation
        }
        val registrationTimeUnreliableOutput = PardonRegistrationTimeUnreliable.pardon(curator, systemId, dayStart, dayEnd,
          overMaxCertifiedSpeedOutput.notPardonedViolations, pardonRegistrationTime)
        registrationTimeUnreliableOutput.pardoned.foreach {
          pardonViolation ⇒ storeProcessedViolations ! pardonViolation
        }

        //placeholder for future pardoned violations
        val totalPardons = overMaxCertifiedSpeedOutput.originalPardonViolations ++ registrationTimeUnreliableOutput.originalPardonViolations
        val notPardonedViolations = registrationTimeUnreliableOutput.notPardonedViolations

        if (!notPardonedViolations.isEmpty) {
          // Send all the self-test based exclusion events to the ExcludeLog.
          // These will be processed before asking for the exclusions 5 lines further down,
          // since Akka will guarantee that these events not be received and processed after
          // the requests issued later from this same actor.
          generateSelfTestExclusions(day, selfTestResults)
          generateCertificateExclusions(day)

          requestedCorridorIds = notPardonedViolations.map(_.corridorData.corridorId).toSet
          receivedExclusions = Map()

          if (!requestedCorridorIds.isEmpty) {
            // Send requests for the exclusion periods to the ExcludeLog
            // Store the aggregate state in a var and wait for the response messages of type
            // Exclusions to continue processing.
            requestedCorridorIds.foreach(corridorId ⇒ excludeLog ! GetExclusions(corridorId))
            state = RVState(statsDuration, day, exportDirectory, systemConfiguration, notPardonedViolations, sender, totalPardons)
          } else {
            exportInProgress = false
            log.error("no violations matched with the corridors. cannot export")
            sender ! ExportViolationsFailed("No violations matched with the corridors")
          }
        } else {
          log.info("no violation records found")
          state = RVState(statsDuration, day, exportDirectory, systemConfiguration, notPardonedViolations, sender, totalPardons)
          self ! ExportViolationsContinue
        }
      } else {
        log.error("received " + ev + " while an export is already in progress")
        sender ! ExportViolationsFailed("Export already in progress")
      }

    case evc @ ExportViolationsContinue ⇒
      if (exportInProgress) {
        val (includedViolations, pardonedViolations) = splitByExclusion(state.violations, receivedExclusions)

        pardonedViolations.foreach {
          case (reason, violations) ⇒
            storeProcessedViolations ! PardonedViolations(violations.map(_.vvr), reason)
        }
        val excludedViolations = pardonedViolations.values.flatten.toSeq
        log.info("Send [{}] Violations to pardon, reason=Exclusion period", excludedViolations.size)

        //pardon Violations with wrong serialNumber
        val selfTestResults = readSelfTestResults(state.day)
        val (correctViolations, wrongViolations) = MeasurementUnit.filterWrongUnits(includedViolations, selfTestResults)
        storeProcessedViolations ! PardonedViolations(wrongViolations.map(_.vvr), "SerialNumber")

        if (!correctViolations.isEmpty && checkViolationImagesActor.isDefined) {
          correctViolations.foreach { v ⇒ sendToRecognizer(v) }
          timeoutSchedule = Some(context.system.scheduler.schedule(1.minute, 1.minute, self, new KeepAlive()))
          lastImageReceived = 0
          if (imageCheckResults.find(_._2 == None).isEmpty) {
            self ! ImageChecksComplete
          }
        } else {
          self ! ImageChecksComplete
        }
        state = state.copy(violations = correctViolations, excluded = state.excluded ++ excludedViolations ++ wrongViolations)
      } else {
        log.error("received " + evc + " while no export is in progress")
      }

    case checkResponse: ViolationImageCheckResponse ⇒
      // Update the imageCheckResults list with the receive Violation Recognition results.
      imageCheckResults += checkResponse.reference -> Some(checkResponse)

      lastImageReceived = System.currentTimeMillis()

      // Are all outstanding Violation Requests processed?
      if (imageCheckResults.find(_._2 == None).isEmpty) {
        log.info("All the request are handled. Finish the day ...")
        // Yes, No more open request. Process the Violations
        self ! ImageChecksComplete
      } else {
        // There are outstanding requests. Tell how many we wait for
        val waitList = imageCheckResults.filter(_._2 == None)

        log.info("Waiting for {} recognizing image results", waitList.size)

        if (waitList.size < 10) {
          waitList.keySet.foreach(key ⇒ log.info("waiting for {}", key))
        }
      }

    case icc @ ImageChecksComplete ⇒
      if (exportInProgress) {
        log.info("Calculate the Day Period ...")
        val (dayStart, dayEnd) = DayUtility.calculateDayPeriod(state.day)

        log.info("Getting the processed violations ")
        val lastDayLastViolations = processedViolationStore.readRows(
          KeyWithTimeIdAndPostFix(dayStart - state.systemConfiguration.globalConfig.minimumViolationTimeSeparation, systemId, None),
          KeyWithTimeIdAndPostFix(dayStart, systemId, None)).
          map(pvr ⇒ Violation(pvr.violationRecord, null, null, null, null, null, null))

        import csc.sectioncontrol.enforce.nl.run.EnforceApplicationConfig._

        val resultingViolations = if (!umtsEnabled) {
          violationsByProcessingIntradaResults
        } else {
          state.violations
        }

        val minViolationTimeSeparation: Long = state.systemConfiguration.globalConfig.minimumViolationTimeSeparation

        val SplitViolations(retainedViolations, proximityPardoned) = MinimumSeparation[Violation](resultingViolations, minViolationTimeSeparation, lastDayLastViolations)
        log.info("Send [{}] Violations to pardon, reason=Too close to another violation for the same license",
          proximityPardoned.size)

        storeProcessedViolations ! PardonedViolations(proximityPardoned.map(_.vvr),
          "Too close to another violation for the same license")

        val violationsToWrite = retainedViolations.filter(_.time < dayEnd)

        log.info("Calculating the statistics")
        val statistics = calculateStatistics(violationsToWrite,
          state.excluded,
          proximityPardoned,
          receivedExclusions,
          state.systemConfiguration,
          state.statsDuration)

        log.info("Get the calibration reports")
        val calibrationReports = readSelfTestResults(state.day).map(rec ⇒ rec._1.infoId -> rec._2)

        import EsaProviderType._
        val esaProvider = getEsaProvider(systemId)
        def emptyMap = Map[Int, Array[Byte]]()

        val (mtmRaw, mtmInfo) = esaProvider match {
          case Dynamax ⇒ (emptyMap, readDynamaxData(systemId, state.day))
          case (Mtm | TubesProvider) ⇒ (readRawData(systemId, state.day, "mtmRaw", mtmDataStore),
            readRawData(systemId, state.day, "mtmInfo", mtmDataStore))
          case NoProvider ⇒ (emptyMap, emptyMap)
        }

        //check which corridors needs to be written
        val systemStatesBlockingViolationWriter = Seq(ZkState.off, ZkState.standBy, ZkState.failure, ZkState.Maintenance)
        val corridorStates = StateService.getStateLogHistory(systemId, state.statsDuration.start, state.statsDuration.tomorrow.start, curator)
        val corriodrInfoIds = corridorStates.filter {
          case (mapping, states) ⇒
            states.exists((aState: ZkState) ⇒ {
              !systemStatesBlockingViolationWriter.contains(aState.state)
            })
        }.keySet.map(_.infoId)
        //filter corridorData from systemConfiguration to prevent writing the violations
        val (filteredCorridorData, skipped) = state.systemConfiguration.corridors.partition(corData ⇒ corriodrInfoIds.contains(corData.corridorId))

        val writeOk = if (!filteredCorridorData.isEmpty) {
          val updatedConfig = state.systemConfiguration.copy(corridors = filteredCorridorData)

          writeViolations(violationsToWrite,
            state.exportDirectory,
            updatedConfig,
            calibrationReports,
            mtmRaw,
            mtmInfo,
            storeProcessedViolations,
            esaProvider,
            caseFileConfigService)
        } else true

        log.info("Start creating the violation files.")
        log.info("Send (publish) the Event Message the runtime statistics are ready")
        context.system.eventStream.publish(statistics)

        val msg = (!filteredCorridorData.isEmpty, writeOk) match {
          case (true, true) ⇒
            val skipMsg = if (skipped.isEmpty) {
              ""
            } else {
              val skipCorridorsNames = skipped.map(corData ⇒ "[%d] %s".format(corData.corridorId, corData.locationLine1))
              " Export van overtredingen overgeslagen voor: " + skipCorridorsNames.mkString(", ")
            }
            "Succesvol overtredingen ge-exporteerd" + skipMsg
          case (true, false) ⇒ "Gefaald overtredingen te exporteren"
          case (false, _)    ⇒ "Export van overtredingen overgeslagen"
        }
        val logEvent = SystemEvent(componentId, System.currentTimeMillis(),
          systemId, "Info", "system", Some(msg))

        log.info("Send (publish) the Event Message creating The Violations Files is done")
        context.system.eventStream.publish(logEvent)

        exportInProgress = false
        state.requestor ! ExportViolationsCompleted
      } else {
        log.error("received " + icc + " while no export is in progress")
      }

    case e: Exclusions ⇒
      if (exportInProgress) {

        val sorted = e.exclusions.sortBy(_.from)
        sorted.foreach {
          ex ⇒
            val from = toDate(ex.from)
            val to = toDate(ex.to)
            log.debug("exclusion for corridorId=[%s], from=[%s], to=[%s], component=[%s], reason=[%s]".format(e.corridorId, from, to, ex.componentId, ex.reason))
        }
        receivedExclusions += (e.corridorId -> e)
        if ((requestedCorridorIds -- receivedExclusions.keySet).isEmpty) {
          self ! ExportViolationsContinue
        }
      } else {
        log.error("received " + e + " while no export is in progress")
      }
    case k: KeepAlive ⇒
      if (exportInProgress) {
        val now = System.currentTimeMillis()
        if (lastImageReceived > 0 && ((lastImageReceived + timeoutImageResponse.toMillis) < now)) {
          log.error("Timeout: took to long to get image responses")
          val waitList = imageCheckResults.filter(_._2 == None)
          log.error("Timeout waiting for %d recognizing image results".format(waitList.size))
          if (waitList.size < 10) {
            waitList.keySet.foreach(key ⇒ log.debug("waiting for {}", key))
          }
          log.info("Continuing export with partial image recognizer results")
          self ! ImageChecksComplete

          timeoutSchedule.foreach(_.cancel())
          timeoutSchedule = None
        }
      } else {
        timeoutSchedule.foreach(_.cancel())
        timeoutSchedule = None
      }
  }

  def violationsByProcessingIntradaResults: Seq[Violation] = {
    // TODO this is old school. Refactor.
    // Determine the best ARH license and make it the 'one'
    log.info("Set the license for each violation")
    val violations = state.violations.map(addRawLicense(imageCheckResults))

    log.info("Verify the Image Recognition results for each violation")
    val checkedViolations = violations.groupBy(checkedImageResult(imageCheckResults,
      recognizerCountries,
      recognizeWithARHAgain,
      intradaInterfaceToUse,
      minLowestLicConfLevel,
      minLicConfLevelRecognizer2,
      autoThresholdLevel,
      mobiThresholdLevel))

    log.info("Change the country code because of checked images result")
    val checkedChangeCountryViolations = checkedViolations.getOrElse(ViolationVerificationResult.ChangeCountry, Seq()).map(setCountryOverride(_))

    log.info("Change the country code and processing indication to mobi because of checked images result")
    val checkedChangeToMobiAndCountryViolations = checkedViolations.getOrElse(ViolationVerificationResult.ChangeToMobiAndCountry, Seq()).map(setChangePIAndCountryOverride(_, "mobi"))

    log.info("Change the country code and processing indication to auto because of checked images result")
    val checkedChangeToAutoAndCountryViolations = checkedViolations.getOrElse(ViolationVerificationResult.ChangeToAutoAndCountry, Seq()).map(setChangePIAndCountryOverride(_, "auto"))

    log.info("Get the violation that are OK (automatic or mobi)")
    val checkedOkViolations = checkedViolations.getOrElse(ViolationVerificationResult.OK, Seq()) ++
      checkedChangeCountryViolations ++
      checkedChangeToMobiAndCountryViolations ++
      checkedChangeToAutoAndCountryViolations

    //get all both manual lists
    log.info("Get the violations that are manual")
    val manualViolations = checkedViolations.getOrElse(ViolationVerificationResult.ManualCountry, Seq()).map(setManualOverride(_, ViolationVerificationResult.ManualCountry)) ++
      checkedViolations.getOrElse(ViolationVerificationResult.ManualLicence, Seq()).map(setManualOverride(_, ViolationVerificationResult.ManualLicence))

    // Get all invalid violations
    log.info("Get the violations that are invalid")
    val checkedInvalidViolations = checkedViolations.getOrElse(ViolationVerificationResult.Invalid, Seq())

    storeProcessedViolations ! FailedViolations(checkedInvalidViolations.map(_.vvr),
      "Inconsistent recognition of license or country")

    checkedOkViolations ++ manualViolations
  }

  private def toDate(date: Long) = {
    import java.text.SimpleDateFormat
    val format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
    val calendar = new GregorianCalendar(DayUtility.fileExportTimeZone)
    calendar.setTimeInMillis(date)
    format.setCalendar(calendar)
    format.format(calendar.getTime)
  }

  private def checkedImageResult(checkedImages: Map[String, Option[ViolationImageCheckResponse]],
                                 recognizerCountries: Seq[String],
                                 recognizeWithARHAgain: Boolean,
                                 intradaInterfaceToUse: Int,
                                 minLowestLicConfLevel: Int,
                                 minLicConfLevelRecognizer2: Int,
                                 autoThresholdLevel: Int,
                                 mobiThresholdLevel: Int): (Violation ⇒ ViolationVerificationResult.Value) =
    (violation: Violation) ⇒ {
      log.info("checkedImageResult called")
      //find response
      //Create the key to find the checkImageResults of this violation
      val key = createCheckKey(violation)
      val responseOpt = checkedImages(key)

      // Get the entry metadata and the entry license plate
      val entry = violation.vvr.classifiedRecord.speedRecord.entry
      val entryLicense = entry.license.map(_.value).getOrElse("")

      log.info("Gonna process response [{}]", responseOpt)

      responseOpt match {
        case Some(response) ⇒
          // Got some Recognition Response for the Violation. Create a conclusion

          val result = if (recognizeWithARHAgain) {
            log.info("Do a summaryCheck before recognize with ARH again")

            response.result.summaryCheck
          } else {
            intradaInterfaceToUse match {
              case 1 ⇒
                log.info("Summary report for recognition with Intrada 1 interface ....")
                response.result.createSummaryWithIntrada(recognizerCountries, minLowestLicConfLevel, minLicConfLevelRecognizer2)

              case 2 ⇒
                log.info("Summary report for recognition with Intrada 2 interface ....")
                response.result.createSummaryWithIntrada2(recognizerCountries, autoThresholdLevel, mobiThresholdLevel)
            }

          }

          if (result.hasErrors()) {
            if (result.hasErrorCheckFailed()) {
              log.info("Manual Indication: license=%s reason=License Check license failed %s".format(entryLicense, result))
              ViolationVerificationResult.ManualLicence
            } else if (result.hasLicenseFormatCheckFailed()) {
              log.debug("Manual Indication: license=%s reason=License format incorrect %s".format(entryLicense, result))
              ViolationVerificationResult.ManualLicenceFormat
            } else if (result.hasLicenseCheckFailed()) {
              log.info("Manual Indication: license=%s reason=License Check different license found %s".format(entryLicense, result))
              ViolationVerificationResult.ManualLicence
            } else if (result.hasCountryCheckFailed()) {
              log.info("Manual Indication: license=%s reason=Country Check invalid country %s".format(entryLicense, result))
              ViolationVerificationResult.ManualCountry
            } else if (result.hasErrorMatchingCheckFailed()) {
              log.info("Invalid violation indication: license=%s reason=Intrada sure its other license".format(entryLicense))
              ViolationVerificationResult.Invalid
            } else if (result.hasCountryChange()) {
              log.info("Country Change indication for license=%s".format(entryLicense))

              if (result.hasChangeToMobi()) {
                log.info("Change to mobi indication for license=%s".format(entryLicense))
                ViolationVerificationResult.ChangeToMobiAndCountry
              } else if (result.hasChangeToAuto()) {
                log.info("Change to auto indication for license=%s".format(entryLicense))
                ViolationVerificationResult.ChangeToAutoAndCountry
              } else {
                ViolationVerificationResult.ChangeCountry
              }
            } else {
              log.info("There are no specific error situations")
              ViolationVerificationResult.OK
            }

          } else {
            ViolationVerificationResult.OK
          }

        case None ⇒
          log.info("Manual Indication: license=%s reason=Check result missing".format(entryLicense))
          ViolationVerificationResult.ManualLicence
      }
    }

  private def addRawLicense(checkedImages: Map[String, Option[ViolationImageCheckResponse]]) =
    (violation: Violation) ⇒ {
      //Create the key to find the checkImageResults of this violation
      val key = createCheckKey(violation)

      // Find the Violation within the results
      val responseOpt = checkedImages(key)

      responseOpt match {
        case Some(response) ⇒
          //find best ARH raw license, meaning highest confidence for license

          // Get All recognition results from the entry
          val entryRecognitionResults = response.result.entry.map(_.getRecognitionResults()).getOrElse(Seq())

          // Get All recognition results from the exit
          val exitRecognitionResults = response.result.exit.map(_.getRecognitionResults()).getOrElse(Seq())

          // Find the best ARH result from entry and exit
          // Question: why not Intrada result?
          val recognitionResults = entryRecognitionResults ++ exitRecognitionResults
          val arhResults = recognitionResults.filter(result ⇒ result.recognizerId.startsWith("ARH") && result.rawLicense.isDefined)

          if (arhResults.isEmpty) {
            // No ARH results? Don't change anything
            violation
          } else {
            val bestRecognized = arhResults.tail.foldLeft(arhResults.head) {
              case (best, candidate) ⇒
                val bestConf = best.license.map(_.confidence).getOrElse(0)
                val canConf = candidate.license.map(_.confidence).getOrElse(0)
                if (canConf > bestConf) {
                  candidate
                } else {
                  best
                }
            }
            violation.copy(rawLicense = bestRecognized.rawLicense)
          }
        case None ⇒ violation
      }
    }

  private def createCheckKey(violation: Violation): String = {
    "%s%d".format(violation.license, violation.time)
  }

  private[register] def sendToRecognizer(violation: Violation) {
    val key = createCheckKey(violation)
    imageCheckResults += key -> None
    log.info("Send the ViolationImageCheckRequest for key {}", key)
    checkViolationImagesActor.foreach(_ ! new ViolationImageCheckRequest(reference = key, violation = violation))
  }

  def setManualOverride(violation: Violation, reason: ViolationVerificationResult.Value): Violation = {
    val indicatorReason = reason match {
      case ViolationVerificationResult.OK                  ⇒ IndicatorReason()
      case ViolationVerificationResult.ManualCountry       ⇒ IndicatorReason().setUnreliableCountry()
      case ViolationVerificationResult.ManualLicence       ⇒ IndicatorReason().setUnreliableLicense()
      case ViolationVerificationResult.ManualLicenceFormat ⇒ IndicatorReason().setUnreliableLicense()
      case ViolationVerificationResult.Invalid             ⇒ IndicatorReason()
    }
    if (reason.toString == ViolationVerificationResult.ManualLicenceFormat.toString) {
      violation.copy(vvr = violation.vvr.copy(classifiedRecord = violation.vvr.classifiedRecord.copy(indicator = ProcessingIndicator.Manual, reason = indicatorReason, manualAccordingToSpec = true)))
    } else {
      violation.copy(vvr = violation.vvr.copy(classifiedRecord = violation.vvr.classifiedRecord.copy(indicator = ProcessingIndicator.Manual, reason = indicatorReason)))
    }
  }

  /*
   Override the ARH country code with the highest confidence level with
   the ARH country code with the lowest confidence level.
   */
  def setCountryOverride(violation: Violation): Violation = {
    // Get the Entry Country
    log.info("Change country for violation {}", violation)

    val entryCountry = violation.vvr.classifiedRecord.speedRecord.entry.country

    // Get the Exit country
    val exitRegistration = violation.vvr.classifiedRecord.speedRecord.exit

    val exitCountry: Option[ValueWithConfidence[String]] = exitRegistration match {
      case Some(registration) ⇒
        registration.country

      case None ⇒ None
    }

    (entryCountry, exitCountry) match {
      case (None, None) ⇒
        violation
      case (Some(country), None) ⇒
        violation
      case (None, Some(country)) ⇒
        violation
      case (Some(entryC), Some(exitC)) ⇒
        if (entryC.confidence > exitC.confidence) {
          /*
           The entry country has the highest confidence level, but is not the correct one and has
            to be changed. Make the entry country equal to the exit country
           */
          val changedViolation = violation.copy(vvr = violation.vvr.copy(classifiedRecord =
            violation.vvr.classifiedRecord.copy(speedRecord =
              violation.vvr.classifiedRecord.speedRecord.copy(entry =
                violation.vvr.classifiedRecord.speedRecord.entry.copy(country = exitCountry)))))

          log.info("Change country exit to entry - Result is {}", changedViolation)

          changedViolation
        } else {
          /*
          The exit country has the highest confidence level, but is not the correct one and has
           to be changed. Make the exit country equal to the entry country
          */
          exitRegistration match {
            case Some(registration) ⇒
              val changedViolation = violation.copy(vvr = violation.vvr.copy(classifiedRecord =
                violation.vvr.classifiedRecord.copy(speedRecord =
                  violation.vvr.classifiedRecord.speedRecord.copy(exit =
                    Some(registration.copy(country = entryCountry))))))

              log.info("Change country entry to exit - Result is {}", changedViolation)

              changedViolation

            case None ⇒
              violation
          }

        }
    }
  }

  def setChangePIAndCountryOverride(violation: Violation, processIndication: String): Violation = {

    log.info("Change country and Processing Indication for violation {}", violation)
    val countryChangeViolations = setCountryOverride(violation)

    processIndication match {
      case "auto" ⇒
        // Switch to auto? Also set the statuscode to zero.
        val changedViolation = countryChangeViolations.copy(vvr =
          countryChangeViolations.vvr.copy(classifiedRecord =
            countryChangeViolations.vvr.classifiedRecord.copy(indicator = ProcessingIndicator.Automatic, reason = IndicatorReason())))

        log.info("Change to Auto - Result is {}", changedViolation)

        changedViolation

      case "mobi" ⇒
        val changedViolation = countryChangeViolations.copy(vvr =
          countryChangeViolations.vvr.copy(classifiedRecord =
            countryChangeViolations.vvr.classifiedRecord.copy(indicator = ProcessingIndicator.Mobi, manualAccordingToSpec = false)))

        log.info("Change to Mobi - Result is {}", changedViolation)

        changedViolation
    }
  }

  private[register] def readSelfTestResults(day: Long): Map[CorridorIdMapping, Seq[SelfTestResult]] = {
    val (fromTime, untilTime) = DayUtility.calculateDayPeriod(day)

    val corridorIds = CorridorService.getCorridorIdMapping(curator, systemId)
    corridorIds.map(corridorMapping ⇒ corridorMapping -> {
      // add 1 to untilTime to include the end-of-day self test
      val allResults = calibrationReportReader.readRows((corridorMapping.corridorId, fromTime), (corridorMapping.corridorId, untilTime + 1)).filter(!_.reportingOfficerCode.isEmpty)
      //filter double results within the same second
      if (!allResults.isEmpty) {
        val sorted = allResults.sortBy(_.time)
        sorted.tail.foldLeft(Seq(sorted.head)) { (list, result) ⇒
          {
            val lastTime = list.last.time / 1000 //remove miliseconds
            val currentTime = result.time / 1000 //remove miliseconds
            if (lastTime == currentTime) {
              list
            } else {
              list :+ result
            }
          }
        }
      } else {
        allResults
      }
    }).toMap
  }

  private[register] def generateSelfTestExclusions(day: Long, calibrationResults: Map[CorridorIdMapping, Seq[SelfTestResult]]) {
    calibrationResults.foreach {
      case (corridorMapping, corridorResults) ⇒
        // Filter out esults not for this coridor
        val selfTestResults = corridorResults.filter(x ⇒ x.corridorId == corridorMapping.corridorId).sortBy(_.time)

        val (dayStart, dayEnd) = DayUtility.calculateDayPeriod(day)

        if (selfTestResults.isEmpty) {
          excludeLog ! SelfTestEvent(Some(corridorMapping.infoId), dayStart, 2, true, "No self-test at start of day")
          excludeLog ! SelfTestEvent(Some(corridorMapping.infoId), dayEnd, 1, false, "No self-test at start of day")
        } else {
          if (selfTestResults.head.time != dayStart) {
            excludeLog ! SelfTestEvent(Some(corridorMapping.infoId), dayStart, 2, true, "No self-test at start of day")
          }

          val appendMissingEndOfDayTest = if (selfTestResults.last.time != dayEnd) {
            log.error("No self test stored for end-of-day. Substituting a failed test result.")
            Seq(SelfTestResult(systemId, corridorMapping.corridorId, dayEnd, false, "", 0, Seq(), Seq(), ""))
          } else {
            Seq()
          }

          //Only the periods between two successful test with equal serial numbers are valid
          val (startDayTest, list) = if (selfTestResults.head.time != dayStart) {
            (SelfTestResult(systemId, corridorMapping.corridorId, dayStart, false, "", 0, Seq(), Seq(), ""), selfTestResults ++ appendMissingEndOfDayTest)
          } else {
            (selfTestResults.head, selfTestResults.tail ++ appendMissingEndOfDayTest)
          }
          //process the begin of day
          if (startDayTest.success) {
            excludeLog ! SelfTestEvent(Some(corridorMapping.infoId), startDayTest.time, 1, false, "Self-test succeeded")
          } else {
            excludeLog ! SelfTestEvent(Some(corridorMapping.infoId), startDayTest.time, 2, true, "Self-test failed")
          }
          //process the rest of the selftest results
          list.foldLeft(startDayTest) { (lastTestResult, testResult) ⇒
            if (testResult.success) {
              if (lastTestResult.success && lastTestResult.serialNr != testResult.serialNr) {
                //measurement unit has changed
                excludeLog ! SelfTestEvent(Some(corridorMapping.infoId), lastTestResult.time, 2, true, "Self-test serialNr changed")
              }
              excludeLog ! SelfTestEvent(Some(corridorMapping.infoId), testResult.time, 1, false, "Self-test succeeded")
            } else {
              if (lastTestResult.success) {
                excludeLog ! SelfTestEvent(Some(corridorMapping.infoId), lastTestResult.time, 2, true, "Self-test failed")
              }
            }
            testResult
          }

          appendMissingEndOfDayTest.foreach { str ⇒
            if (!str.success) {
              excludeLog ! SelfTestEvent(Some(corridorMapping.infoId), str.time, 1, false, "Self-test failed")
            }
          }
        }
    }
  }

  private[register] def generateCertificateExclusions(day: Long) {
    val (dayStart, dayEnd) = DayUtility.calculateDayPeriod(day)
    val cert = CertificateService.getLocationCertificates(curator, systemId)
    val exclusions = MeasurementUnit.createExclusionPeriods(cert, new ExclusionPeriod(dayStart, dayEnd))

    exclusions.foreach(period ⇒ {
      excludeLog ! SelfTestEvent(None, period.from, 2, true, "No valid location certificate")
      excludeLog ! SelfTestEvent(None, period.to, 1, false, "No valid location certificate")
    })
  }

  private[register] def validateActiveCertificates: (Boolean, Seq[ActiveCertificate]) = {
    val time = System.currentTimeMillis() //TODO: RB use currenttime for now
    val components = CertificateService.getAllInstalledComponentCertificates(curator, systemId, time)

    components match {
      case Right(storedComponents) ⇒
        val activeCertificatePaths = curator.getChildren(Paths.Systems.getActiveCertificatesPath(systemId))
        val certificatesOk = for {
          activeCert ← activeCertificatePaths.flatMap(p ⇒ curator.get[ActiveCertificate](p))
        } yield {
          val name = activeCert.componentCertificate.name
          storedComponents.find(_.name == name) match {
            case Some(typeCertComp) ⇒
              //check checksum
              (typeCertComp.checksum == activeCert.componentCertificate.checksum, activeCert)
            case None ⇒
              //no need to be checked so always OK
              (true, activeCert)
          }
        }
        val failedCertificates: Seq[ActiveCertificate] = certificatesOk.filter(_._1 == false).map(_._2)

        // Log failed certificate checks
        failedCertificates.foreach(ac ⇒ log.error("Invalid checksum for active certificate " + ac))

        // Require both a type certificate and at least 1 valid active component certificate
        val areAllValid = !certificatesOk.isEmpty && failedCertificates.isEmpty

        (areAllValid, failedCertificates)
      case Left(msg) ⇒
        log.warning("Failed to get ComponentCertificates for systemId=%s time=%s error=%s)".format(systemId, (new Date(time)).toString, msg))
        (false, Seq())
    }
  }

  private def getEsaProvider(systemId: String): EsaProviderType.Value = {
    val systemConfigPath = Paths.Systems.getConfigPath(systemId)
    curator.get[ZkSystem](systemConfigPath) match {
      case Some(systemConfig) ⇒ EsaProviderType.withName(systemConfig.esaProviderType.toString)
      case None ⇒
        log.warning("System Config not found %s".format(systemConfigPath))
        EsaProviderType.NoProvider
    }
  }

  private case class RVState(statsDuration: StatsDuration,
                             day: Long,
                             exportDirectory: String,
                             systemConfiguration: SystemConfigurationData,
                             violations: Seq[Violation],
                             requestor: ActorRef,
                             excluded: Seq[Violation])

  case object ExportViolationsContinue

  case class InvalidCertificateEvent(effectiveTimestamp: Long,
                                     sequenceNumber: Long,
                                     suspend: Boolean,
                                     reason: String) extends SystemEventData {
    def componentId = RegisterViolations.InvalidCertificateComponentId

    def corridorId = None

    def isSuspendOrResumeEvent = true

    def suspensionReason = Some(reason)
  }

  case class SelfTestEvent(corridorId: Option[Int],
                           effectiveTimestamp: Long,
                           sequenceNumber: Long,
                           suspend: Boolean,
                           reason: String) extends SystemEventData {
    def componentId = RegisterViolations.selfTestComponentId

    def isSuspendOrResumeEvent = true

    def suspensionReason = Some(reason)
  }
}

object RegisterViolations {
  val componentId = "RegisterViolations"

  val selfTestComponentId = "SelfTest"

  val InvalidCertificateComponentId = "InvalidCertificate"

  object ViolationVerificationResult extends Enumeration {
    val OK, ManualLicence, ManualCountry, Invalid, ManualLicenceFormat, ChangeToMobiAndCountry, ChangeToAutoAndCountry, ChangeCountry = Value
  }
}

/**
 * Message to initiate the export of a day of violations
 *
 * @param stats A key to be used by day statistics
 * @param day A time during the day to be exported.
 * @param exportDirectory The path to the root directory in which the export data is to be placed
 */
case class ExportViolations(stats: StatsDuration, day: Long, exportDirectory: String, systemConfiguration: SystemConfigurationData)

/**
 * Response message to indicate that exporting violations failed.
 * @param message Details on the failure reason.
 */
case class ExportViolationsFailed(message: String)

/**
 * Response message to indicate that exporting violations completed successfully.
 */
case object ExportViolationsCompleted

/**
 * Message to indicate that image recognition double-checking has been completed
 */
case object ImageChecksComplete

/**
 * Message to indicate that the check for missing images have to be done
 */
case class KeepAlive()