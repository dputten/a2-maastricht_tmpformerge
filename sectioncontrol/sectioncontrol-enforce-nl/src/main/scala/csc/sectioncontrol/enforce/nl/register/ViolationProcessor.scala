/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.nl.register

import java.util.Date

import akka.actor.{ ActorContext, ActorRef }
import akka.event.LoggingAdapter
import com.ibm.icu.text.SimpleDateFormat
import csc.sectioncontrol.enforce.common.nl.services.Service
import csc.sectioncontrol.enforce.common.nl.violations._
import csc.sectioncontrol.enforce.nl.casefiles.CaseFile
import csc.sectioncontrol.enforce.speedlog.SpeedLog
import csc.sectioncontrol.enforce.violations.{ DayUtility, GermanLicenseFormatter }
import csc.sectioncontrol.messages._
import csc.sectioncontrol.sign.SoftwareChecksum
import csc.sectioncontrol.sign.certificate.CertificateService
import csc.sectioncontrol.storagelayer.excludelog.Exclusions
import csc.sectioncontrol.storagelayer.hbasetables.CalibrationImageTable
import csc.sectioncontrol.writer.nl.ViolationWriter
import org.apache.commons.io.FileUtils

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import csc.sectioncontrol.storagelayer.caseFile.{ CaseFileConfigService, CaseFileConfigZooKeeperService }
import scala.Left
import csc.sectioncontrol.enforce.common.nl.messages.SpeedIndicationSign
import csc.sectioncontrol.messages.certificates.LocationCertificate
import csc.sectioncontrol.enforce.common.nl.violations.CorridorData
import csc.sectioncontrol.enforce.common.nl.violations.CorridorInfo
import csc.sectioncontrol.enforce.common.nl.messages.ViolationData
import scala.Some
import csc.sectioncontrol.enforce.common.nl.violations.SystemConfigurationData
import csc.sectioncontrol.enforce.common.nl.violations.CalibrationTestResult
import csc.sectioncontrol.enforce.common.nl.violations.CorridorSchedule
import csc.sectioncontrol.enforce.common.nl.violations.ViolationGroupParameters
import csc.sectioncontrol.enforce.common.nl.messages.HeaderInfo
import csc.sectioncontrol.messages.certificates.MeasurementMethodInstallationType
import csc.sectioncontrol.enforce.ExportViolationsStatistics
import csc.sectioncontrol.enforce.DayViolationsStatistics
import csc.sectioncontrol.storage.SelfTestResult
import csc.sectioncontrol.enforce.common.nl.violations.SystemGlobalConfig
import scala.Right
import csc.sectioncontrol.enforce.common.nl.messages.SystemParameters
import csc.sectioncontrol.enforce.common.nl.services.RedLight
import csc.sectioncontrol.messages.certificates.ComponentCertificate
import csc.sectioncontrol.enforce.common.nl.violations.Violation
import scala.collection.immutable.SortedSet

/**
 * Handle exporting a day worth of violations to disk via the violations api.
 *
 * Retrieves events from the system event log that relate to suspending or
 * resuming enforcement, and filters the violations based on that.
 *
 * The violations, calibration test results and speed change data are aggregated and
 * sorted into a series of exported directories.
 *
 * @author Maarten Hazewinkel
 */
trait ViolationProcessor extends ImageProcessor {

  private[register] def systemId: String
  private[register] def log: LoggingAdapter
  private[register] def calibrationImageReader: CalibrationImageTable.Reader
  protected def context: ActorContext

  protected val averageImageSizeKb = 50
  protected val sha = SoftwareChecksum.createHashFromRuntimeClass(classOf[ViolationWriter]).map(_.checksum).getOrElse("")

  def groupViolationsOnSpeedIndicationSign(violations: Seq[Violation], esaProvider: EsaProviderType.Value): Map[SpeedIndicationSign, Seq[Violation]] = {
    violations.groupBy(violation ⇒ getSpeedIndicationSign(violation, esaProvider))
  }

  /**
   * Write out a batch of violations to the export directory
   *
   * @param violationsToWrite The set of violations to write
   * @param exportBaseDir The base directory in which to create the export
   * @param sysConfig The system configuration
   * @param allCalibrationReports The calibration reports for this batch
   * @param mtmRaw The MTM Raw file for this batch
   * @param mtmInfo The MTM Info file for this batch
   * @param storeProcessedViolations The actor that stores processed violation records
   * @param esaProvider type of Esa provider
   */
  def writeViolations(violationsToWrite: Seq[Violation],
                      exportBaseDir: String,
                      sysConfig: SystemConfigurationData,
                      allCalibrationReports: Map[Int, Seq[SelfTestResult]],
                      mtmRaw: Map[Int, Array[Byte]],
                      mtmInfo: Map[Int, Array[Byte]],
                      storeProcessedViolations: ActorRef,
                      esaProvider: EsaProviderType.Value,
                      caseFileConfigService: CaseFileConfigService): Boolean = {
    val invalidImageViolations = ListBuffer[Violation]()
    val writerResults: Seq[Either[String, ViolationWriter]] =
      sysConfig.corridors.flatMap(corridor ⇒ {
        //get all calibrations for this corridor
        val calibrationReports = allCalibrationReports.get(corridor.corridorId).getOrElse(Seq())
        // Get all the violations for this corridor
        val violations = violationsToWrite.filter(_.corridorData.corridorId == corridor.corridorId)
        // Get the time schedule for this corridor
        val schedules = sysConfig.schedules.filter(_.corridorId == corridor.corridorId)

        val defaultSchedule = schedules.headOption
        log.info("writing {} violations for corridor {} with default schedule {} and config {}",
          violations.size, corridor, defaultSchedule, sysConfig.globalConfig)

        // Was there a time schedule active for this corridor

        // Was there a time schedule active for this corridor
        if (defaultSchedule.isDefined) {

          // Were there any violations
          //create groups for each serial number and speed
          val serialGroup = violations.
            groupBy(violation ⇒ violation.vvr.classifiedRecord.speedRecord.entry.serialNr)

          val availableSerialNumbers = createAvailableSerialNumbers(calibrationReports, serialGroup)

          //write the directories in the correct order.
          //use the calibration serialNr results as order
          availableSerialNumbers.flatMap(serialNr ⇒ {
            serialGroup.get(serialNr) match {
              case Some(_violations) ⇒
                log.debug("_violations" + _violations)

                //create groups for each speed
                val speedGroups = groupViolationsOnSpeedIndicationSign(_violations, esaProvider)
                log.debug("speedGroups: " + speedGroups)

                val sortedSpeedGroups = speedGroups.toSeq.sortBy {
                  case (speedInd, violationGroup) ⇒ speedInd.indicatedSpeed.getOrElse(Int.MaxValue)
                }
                log.debug("sortedSpeedGroups: " + sortedSpeedGroups)

                sortedSpeedGroups.map {
                  case (speedInd, violationGroup) ⇒
                    writeViolationGroup(corridor,
                      sysConfig.globalConfig,
                      violationGroup,
                      exportBaseDir,
                      calibrationReports,
                      mtmRaw,
                      mtmInfo,
                      defaultSchedule.get,
                      invalidImageViolations,
                      esaProvider,
                      serialNr,
                      speedInd.indicatedSpeed, caseFileConfigService)
                }.toSeq
              case None ⇒
                log.info("writing empty export directory for corridor %s".format(corridor.corridorId))
                val speeds = schedules.map(_.indicatedSpeedLimit).distinct
                speeds.map(speedLimit ⇒ {
                  log.debug("writing empty export directory for corridor %s and speed %s".format(corridor.corridorId, speedLimit))
                  writeViolationGroup(corridor,
                    sysConfig.globalConfig,
                    Seq(),
                    exportBaseDir,
                    calibrationReports,
                    mtmRaw,
                    mtmInfo,
                    defaultSchedule.get,
                    invalidImageViolations,
                    esaProvider,
                    serialNr,
                    speedLimit, caseFileConfigService)
                })
            }
          })
        } else {
          log.info("skipping corridor because no schedule is defined for it")
          Seq()
        }
      })

    val writeOk = writeToDisk(writerResults)

    if (writeOk) {
      storeProcessedViolations ! FailedViolations(invalidImageViolations.toList.map(_.vvr), "Invalid image")
      val exportedViolations = violationsToWrite.diff(invalidImageViolations)
      storeProcessedViolations ! RegisteredViolations(exportedViolations.map(_.vvr))
    } else {
      storeProcessedViolations ! FailedViolations(violationsToWrite.map(_.vvr), "Error while writing the export")
    }

    writeOk
  }

  def writeToDisk(writerResults: Seq[Either[String, ViolationWriter]]): Boolean = {
    // If any errors occurred abort all writers. Otherwise flush all of them.
    val writeOk = if (writerResults.exists(_.isLeft)) {
      // log write errors
      writerResults.foreach(_.left.foreach(log.error))

      // abort all writers
      writerResults.foreach(_.right.foreach(_.abort()))

      false
    } else {
      val writers = writerResults.map(_.right.get)

      // flush all the writers to complete writing everything to disk
      val flushResults = writers.
        map(_.flush())

      if (flushResults.exists(_.isLeft)) {
        // log flush errors
        flushResults.foreach(_.left.foreach(log.error _))

        // abort all writers (successfully flushed writers will simply ignore the request)
        writers.map(_.abort())

        // destroy completed output directories
        flushResults.filter(_.isRight).foreach(fr ⇒ FileUtils.deleteDirectory(fr.right.get))

        false
      } else {
        true
      }
    }
    writeOk
  }

  /**
   * Create all available serialnumbers and sort it by occurences
   * @param calibrationReports
   * @param serialGroup
   * @return list of all serial numbers
   */
  private def createAvailableSerialNumbers(calibrationReports: Seq[SelfTestResult], serialGroup: Map[String, Seq[Violation]]): Seq[String] = {
    //find all available Serial numbers only successful calibrations can have a correct serialNr
    val successCalibrations = calibrationReports.filter(_.success)
    //get all available serialNumbers sorted by time they occur => this is the order to process the violation groups
    val calibrateSerialNumbers = successCalibrations.sortBy(_.time).map(_.serialNr).distinct

    //these should not be necessary but just in case it does
    val violationSerialNumbers = serialGroup.filterNot {
      case (nr, _) ⇒ calibrateSerialNumbers.contains(nr)
    }.toSeq.sortBy { //sort the groups based on serial number order
      case (serialNr, violationGroup) ⇒
        val sortedGroup = violationGroup.sortBy(_.vvr.classifiedRecord.speedRecord.entry.eventTimestamp)
        sortedGroup.headOption.map(_.vvr.classifiedRecord.speedRecord.entry.eventTimestamp).getOrElse(Long.MaxValue)
    }.map(_._1)
    if (!violationSerialNumbers.isEmpty)
      log.warning("Found serial numbers in violation but not in calibration results: " + violationSerialNumbers)
    calibrateSerialNumbers ++ violationSerialNumbers
  }

  /**
   * create a DayViolationsStatistics instance for a given system
   * @param registered  collection of registered violations for the given system
   * @param excluded collection of excluded violations
   * @param proximityPardoned collection of proximityPardoned violations
   * @param exclusions exclusions (per corridor)
   * @param systemConfiguration relevant config data for this system
   * @param statsDuration the period for which to calculate the DayViolationStatistics.
   * @return
   */
  private[register] def calculateStatistics(registered: Seq[Violation],
                                            excluded: Seq[Violation],
                                            proximityPardoned: Seq[Violation],
                                            exclusions: Map[Int, Exclusions],
                                            systemConfiguration: SystemConfigurationData,
                                            statsDuration: StatsDuration): DayViolationsStatistics = {

    /**
     * creates an instance of ExportViolationStatistics for a given corridorId and speedLimit
     * @param id  the corridorId
     * @param speedLimit the speedLimit
     * @param reg collection of violations for this corridorId and scheduledSpeedLimit
     * @param excluded collection of excluded violations for the given corridor and speedlimit
     * @param proximityPardoned collection of proximityPardoned violations for the given corridor and speedLimit
     * @param exclusion collection of exclusions for the gicen corridor
     * @return
     */
    def makeExportViolationStatistics(id: Int, speedLimit: Int, reg: Seq[Violation], excluded: Seq[Violation],
                                      proximityPardoned: Seq[Violation], exclusion: Exclusions): ExportViolationsStatistics = {

      val (countAuto, countManual, countMobi, manualWithSpec) = reg.foldLeft((0, 0, 0, 0)) {
        (sums, v) ⇒
          v.vvr.classifiedRecord.indicator match {
            case ProcessingIndicator.Automatic ⇒ (sums._1 + 1, sums._2, sums._3, sums._4)
            case ProcessingIndicator.Manual ⇒
              val according: Int = v.vvr.classifiedRecord.manualAccordingToSpec match {
                case true  ⇒ 1
                case false ⇒ 0
              }
              (sums._1, sums._2 + 1, sums._3, sums._4 + according)
            case ProcessingIndicator.Mobi ⇒ (sums._1, sums._2, sums._3 + 1, sums._4)
          }
      }
      val mtmExcluded: Int = excluded.count(vd ⇒ {
        val sp = vd.vvr.classifiedRecord.speedRecord
        exclusion.intersectsComponentInterval(SpeedLog.componentName,
          sp.entry.eventTimestamp,
          sp.exit.map(_.eventTimestamp).getOrElse(sp.entry.eventTimestamp))
      })
      ExportViolationsStatistics(id, speedLimit, countAuto, countManual, countMobi, countMobi, mtmExcluded,
        proximityPardoned.size, excluded.size - mtmExcluded, manualWithSpec)
    }
    // create ExportViolationsStatistics per corridor per scheduleSpeed
    val statistics: Seq[ExportViolationsStatistics] = systemConfiguration.corridors.flatMap(corridorData ⇒ {
      val id = corridorData.corridorId
      val scheduledSpeedLimits = systemConfiguration.schedules.filter(_.corridorId == id).map(_.enforcedSpeedLimit).distinct

      scheduledSpeedLimits.map(speedLimit ⇒ {
        val registeredInCorr = registered.filter(violation ⇒ violation.corridorData.corridorId == id && violation.corridorSchedule.enforcedSpeedLimit == speedLimit)
        val excludedInCorr = excluded.filter(violation ⇒ violation.corridorData.corridorId == id && violation.corridorSchedule.enforcedSpeedLimit == speedLimit)
        val proximityPardonedInCorr = proximityPardoned.filter(violation ⇒ violation.corridorData.corridorId == id && violation.corridorSchedule.enforcedSpeedLimit == speedLimit)
        val exclusion: Exclusions = exclusions.getOrElse(id, Exclusions(0))

        makeExportViolationStatistics(id, speedLimit, registeredInCorr, excludedInCorr, proximityPardonedInCorr, exclusion)
      })
    })
    DayViolationsStatistics(systemId, statsDuration, statistics)
  }

  private[register] def splitByExclusion(violations: Seq[Violation], exclusions: Map[Int, Exclusions]): (Seq[Violation], Map[String, Seq[Violation]]) = {
    val excludeReasons = violations.
      groupBy(vd ⇒ vd.corridorData.corridorId).
      values.
      map {
        violationGroup ⇒
          {
            val exclusionsForCorridor = exclusions.getOrElse(violationGroup.head.corridorData.corridorId, Exclusions(0))
            violationGroup.map(vd ⇒ {
              val reason = exclusionsForCorridor.intersectsIntervalReason(
                vd.vvr.classifiedRecord.speedRecord.entry.eventTimestamp,
                vd.vvr.classifiedRecord.speedRecord.exit.map(_.eventTimestamp).getOrElse(vd.vvr.classifiedRecord.speedRecord.entry.eventTimestamp))
              (reason, vd)
            })
          }
      }.flatten
    //split violations
    val (includedViolations, excludedViolations) = excludeReasons.partition(_._1.isEmpty)
    //group excluded violations by reason to create a clean Map
    val groupedReasons = excludedViolations.groupBy(_._1)
    val pardonViolations = groupedReasons.map { case (reason, reasonRec) ⇒ (reason.getOrElse(""), reasonRec.map(_._2).toSeq) }
    (includedViolations.map(_._2).toSeq, pardonViolations)
  }

  private def writeViolationGroup(corridor: CorridorData,
                                  globalConfig: SystemGlobalConfig,
                                  violationGroup: Seq[Violation],
                                  exportBaseDir: String,
                                  calibrationReports: Seq[SelfTestResult],
                                  mtmRaw: Map[Int, Array[Byte]],
                                  mtmInfo: Map[Int, Array[Byte]],
                                  defaultSchedule: CorridorSchedule,
                                  returnInvalidImageViolations: mutable.Seq[Violation],
                                  esaProvider: EsaProviderType.Value,
                                  serialNr: String,
                                  speedLimit: Option[Int],
                                  caseFileConfigService: CaseFileConfigService): Either[String, ViolationWriter] = {
    try {
      // Hier wordt uit de eerste violation het _.corridorSchedule gepakt en anders de default
      val violationGroupParameters = makeGlobalParams(corridor, violationGroup.headOption.map(_.corridorSchedule).
        getOrElse(defaultSchedule), globalConfig, serialNr, caseFileConfigService)
      val dayEndTime = violationGroupParameters.violationsDate

      val writer: ViolationWriter = makeViolationWriter(exportBaseDir,
        violationGroupParameters,
        mtmRaw.getOrElse(corridor.corridorId, Array[Byte]()),
        mtmInfo.getOrElse(corridor.corridorId, Array[Byte]()),
        2 * violationGroup.size * averageImageSizeKb,
        corridor.service,
        corridor.caseFile,
        esaProvider,
        speedLimit)

      //make final decision to write violations
      val writeViolations = esaProvider match {
        case EsaProviderType.Mtm if mtmRaw.size <= 0 ⇒ false
        case other                                   ⇒ true
      }

      if (writeViolations) {
        violationGroup.
          map(vd ⇒ {
            // retrieve images for this violation
            val entry = vd.vvr.classifiedRecord.speedRecord.entry
            val exit = vd.vvr.classifiedRecord.speedRecord.exit

            val (entryImage, exitImage) = retrieveImages(entry, exit, corridor)

            //register violation if the first image is retrieved (and it's hash checked out)
            //the second image is optional
            if (entryImage.isDefined) {
              try {
                writer.add(makeViolationData(vd, esaProvider, globalConfig.treatyCountries), entryImage.get, exitImage)
              } catch {
                case iae: IllegalArgumentException ⇒
                  log.error("ViolationData requirement failed for {}: {}", vd, iae.getMessage)
                  Left(iae.getMessage)
              }
            } else {
              log.error("No entryImage available for corridorId " + corridor.corridorId)
              // if there is a problem with any image, skip registration but do continue the process for other violations
              Right()
            }
          }).
          find(_.isLeft).
          foreach(error ⇒ {
            writer.abort()
            require(false, error.left.get)
          })
      }

      val schedule = violationGroup.headOption.map(_.corridorSchedule).getOrElse(defaultSchedule)

      MeasurementUnit.filterCalibrationReports(calibrationReports, serialNr).
        map(str ⇒ writer.add(makeCalibrationTestResult(str, calibrationImageReader, corridor, schedule, globalConfig, serialNr, dayEndTime))).
        find(_.isLeft).
        foreach(error ⇒ {
          // TODO send Failure for export
          writer.abort()
          require(false, error.left.get)
        })

      Right(writer)
    } catch {
      case e: IllegalArgumentException ⇒ Left(e.getMessage)
    }
  }

  private[register] def makeViolationWriter(exportBaseDir: String,
                                            groupParameters: ViolationGroupParameters,
                                            rawMTM: Array[Byte],
                                            processedMTM: Array[Byte],
                                            estimatedSpaceRequirementKb: Int,
                                            service: Service,
                                            caseFile: CaseFile,
                                            esaProviderType: EsaProviderType.Value,
                                            optSpeedLimit: Option[Int]) =
    ViolationWriter(
      baseDir = exportBaseDir,
      parameters = groupParameters,
      mtmRaw = rawMTM,
      mtmInfo = processedMTM,
      caseFile = caseFile,
      service = service,
      esaProviderType = esaProviderType,
      optSpeedLimit = optSpeedLimit,
      caseFileConfigService = new CaseFileConfigZooKeeperService(curator))

  def getCurrentMeasurementMethodInstallationType(globalConfig: SystemGlobalConfig, time: Long): Option[MeasurementMethodInstallationType] = {
    val certType = CertificateService.getCurrentCertificate(globalConfig.certificateTypes, time)
    if (certType.isEmpty) {
      //this should not happen
      val format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss.SSS")
      val msg = "Could not find certificates for system %s at time %s".format(systemId, format.format(new Date(time)))
      log.error(msg)
      throw new IllegalStateException(msg)
    }
    certType
  }

  def getSerialNr(globalConfigSerialNumber: String, serialNumber: String): String = {
    if (serialNumber == null || serialNumber.isEmpty) {
      globalConfigSerialNumber
    } else {
      serialNumber
    }
  }

  private def makeGlobalParams(corridor: CorridorData, schedule: CorridorSchedule, globalConfig: SystemGlobalConfig,
                               serialNumber: String, caseFileConfigService: CaseFileConfigService): ViolationGroupParameters = {
    //add dynamic MeasurementType and headerInfo

    // The violations are grouped by the speed of the schedules but this does not guarentee that the other properties of the schedules are the
    // same. Theoreticaly it is possible to change the measurementMethodInstallationType's in between the schedules with the same speed. Then the
    // incorrect MeasurementMethodInstallationType are used.

    // NEWS-FLASH!!
    // There is a agreement that the MeasurementMethodInstallationType used are the ones that are current at the end of
    // the day of the schedule.

    //val time = schedule.endTime
    val (_, dayEndTime) = DayUtility.calculateDayPeriod(schedule.endTime)

    val headerInfoSystemName = caseFileConfigService.get(systemId, corridor.corridorId) match {
      case Some(zkCaseFileConfig) ⇒ zkCaseFileConfig.header
      case _                      ⇒ ""
    }

    val formattedDate = new SimpleDateFormat("dd/MM/yyyy kk:mm:ss.SSS")
    log.debug("Endtime used for determining MeasurementMethodInstallationType epoch:" + dayEndTime + " " + formattedDate.format(dayEndTime))

    val currentMeasurementMethodInstallationType = getCurrentMeasurementMethodInstallationType(globalConfig, dayEndTime)

    // Make the software and configuration certificate available for zaakbestand.
    val components: Iterable[List[ComponentCertificate]] = Some(List(globalConfig.activeConfigurationCertificate)) ++
      Some(List(globalConfig.activeSoftwareCertificate))
    ViolationGroupParameters(systemId,
      globalConfig.systemName,
      globalConfig.countryCodes,
      globalConfig.routeId,
      CorridorInfo(corridor.corridorId,
        corridor.entryLocationName,
        corridor.exitLocationName,
        corridor.length,
        corridor.roadCode,
        corridor.locationCode,
        corridor.locationLine1,
        corridor.locationLine2,
        corridor.drivingDirectionFrom,
        corridor.drivingDirectionTo),
      globalConfig.mtmRouteId,
      schedule.startTime,
      corridor.radarCode,
      schedule.pshTmIdFormat,
      correctNamesForCertificates(components.flatten.toSeq), // Horrible workaround for TCU-161
      new HeaderInfo(headerInfoSystemName, getSerialNr(globalConfig.serialNumber, serialNumber),
        currentMeasurementMethodInstallationType.get.measurementType),
      corridor.roadType,
      globalConfig.roadPart,
      corridor.insideTown,
      schedule.dutyType,
      schedule.deploymentCode, globalConfig.pshtmId)
  }

  /**
   * This is a workaround for issue TCU-161. Preferably would probably have been to update ZooKeeper with an
   * extra property in which to put the name for in the case file. However, since part of the configuration for
   * certificates comes from ZooKeeper and part of it is hard coded, and the impact of changing this is unclear, due
   * to time constraints this will be the fix.
   *
   * @param certificates the certificates as loaded from ZooKeeper and the CertificateService
   * @return the certificates, but with updated names for in the case file as stated in req. 54
   */
  private def correctNamesForCertificates(certificates: Seq[ComponentCertificate]): Seq[ComponentCertificate] = {
    certificates.map(seal ⇒ {
      seal.name match {
        case "Softwarezegel"     ⇒ seal.copy(name = "software")
        case "Configuratiezegel" ⇒ seal.copy(name = "configuratie")
      }
    })
  }

  def getSpeedIndicationSign(violation: Violation, esaProvider: EsaProviderType.Value): SpeedIndicationSign = {
    log.debug("Param-in->Violation:" + violation)
    log.debug("Param-in->esaProvider:" + esaProvider)

    val speedIndicationSign = SpeedIndicationSign(
      signIndicator = violation.corridorSchedule.speedIndicator.signIndicator,
      indicatorType = violation.corridorSchedule.speedIndicator.speedIndicatorType,
      indicatedSpeed = getSpeedEnforceSpeed(violation, esaProvider))
    log.debug("SpeedIndicationSign:" + speedIndicationSign)

    speedIndicationSign
  }

  def getSpeedEnforceSpeed(violation: Violation, esaProvider: EsaProviderType.Value): Option[Int] = {
    import csc.sectioncontrol.messages.EsaProviderType._
    val speedIndication: Option[Int] =
      if (esaProvider == Dynamax) {
        log.debug("getSpeedIndication->esaProvider:Dynamax")
        violation.vvr.dynamicSpeedLimit
      } else {
        log.debug("getSpeedIndication->esaProvider:" + esaProvider)
        Some(violation.corridorSchedule.enforcedSpeedLimit)
      }

    log.debug("speedIndication:" + speedIndication)
    speedIndication
  }

  private def makeViolationData(violation: Violation, esaProvider: EsaProviderType.Value, treatyCountries: Seq[String]): ViolationData = {
    val images = violation.vvr.classifiedRecord.speedRecord.entry.images
    val sp = violation.vvr.classifiedRecord.speedRecord
    val exitEventTimestamp = sp.exit.map(_.eventTimestamp).getOrElse(sp.entry.eventTimestamp)
    val exportLicense = violation.vvr.classifiedRecord.speedRecord.country.map(_.value) match {
      case Some("DE") ⇒ GermanLicenseFormatter(
        violation.exportLicense,
        violation.vvr.classifiedRecord.speedRecord.entry.rawLicense,
        violation.vvr.classifiedRecord.speedRecord.exit.flatMap(_.rawLicense))
      case _ ⇒ violation.exportLicense
    }
    //set Code to none when processing manual
    val code = if (violation.vvr.classifiedRecord.indicator == ProcessingIndicator.Manual) {
      None
    } else {
      violation.vvr.classifiedRecord.code
    }
    //set countrycode to None when non treaty country and processing manual
    val countryCode = if (violation.vvr.classifiedRecord.indicator == ProcessingIndicator.Manual) {
      violation.vvr.classifiedRecord.speedRecord.country.map(_.value) match {
        case Some(ct) if treatyCountries.contains(ct) ⇒ Some(ct)
        case Some(ct) if ct == "NL"                   ⇒ Some(ct) // NL isn't part of treatyCountries
        case other                                    ⇒ None
      }
    } else {
      violation.vvr.classifiedRecord.speedRecord.country.map(_.value)
    }

    // Create the violation data
    ViolationData(
      violation.globalConfig.countryCodes,
      violation.vvr.classifiedRecord.indicator,
      violation.vvr.classifiedRecord.reason,
      exportLicense,
      code,
      violation.vvr.classifiedRecord.speedRecord.speed,
      countryCode,
      getSpeedIndicationSign(violation, esaProvider), // Todo: Voorheen zat in de getSpeedIndicationSign de indicated speed??
      violation.vvr.classifiedRecord.speedRecord.entry.eventTimestamp,
      exitEventTimestamp,
      //vd.vvr.classifiedRecord.speedRecord.exit.get.eventTimestamp, // No NPE possible due to require check on Violation case class
      sha,
      violation.vvr.classifiedRecord.mil,
      violation.vvr.classifiedRecord.manualAccordingToSpec,
      violation.reportingOfficerCode,
      violation.pshTmId,
      violation.corridorSchedule.indicationRoadworks,
      violation.corridorSchedule.indicationDanger,
      violation.corridorSchedule.indicationActualWork,
      violation.corridorSchedule.invertDrivingDirection,
      violation.corridorSchedule.textCode,
      images)
  }

  private def getTypeCertificate(globalConfig: SystemGlobalConfig, selfTestResult: SelfTestResult): Option[MeasurementMethodInstallationType] = {
    val certType = CertificateService.getCurrentCertificate(globalConfig.certificateTypes, selfTestResult.time)
    if (certType.isEmpty) {
      //this should not happen
      val format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss.SSS")
      val msg = "Could not find certificates for system %s at time %s".format(systemId, format.format(new Date(selfTestResult.time)))
      log.error(msg)
      throw new IllegalStateException(msg)
    }
    certType
  }

  private def getLocationCertificate(globalConfig: SystemGlobalConfig, selfTestResult: SelfTestResult): Option[LocationCertificate] = {
    val locationCertificate: Option[LocationCertificate] = CertificateService.getCurrentLocationCertificate(globalConfig.locationCertificates, selfTestResult.time)
    if (locationCertificate.isEmpty) {
      //this should not happen
      val format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss.SSS")
      val errorMsg = "Could not find a local certificate for system %s at time %s".format(systemId, format.format(new Date(selfTestResult.time)))
      log.error(errorMsg)
      throw new IllegalStateException(errorMsg)
    }
    locationCertificate
  }

  def makeCalibrationTestResult(selfTestResult: SelfTestResult,
                                imageReader: CalibrationImageTable.Reader,
                                corridor: CorridorData,
                                schedule: CorridorSchedule,
                                globalConfig: SystemGlobalConfig,
                                serialNr: String, dayEndTime: Long): CalibrationTestResult = {
    //add dynamic MeasurementType and headerInfo
    val time = selfTestResult.time

    val certType = getTypeCertificate(globalConfig, selfTestResult)
    val locationCertificate: Option[LocationCertificate] = getLocationCertificate(globalConfig, selfTestResult)

    val softwareSealTypeCertificates = CertificateService.calculateSeal(certType.get.measurementType.typeCertificate.components)
    val softwareSealLocationCertificates = CertificateService.calculateSeal(locationCertificate.get.components)

    // NB: Ist and Soll are guaranteed to be the same at this point.
    val components = List(ComponentCertificate("Softwarezegel", softwareSealTypeCertificates), ComponentCertificate("Configuratiezegel", softwareSealLocationCertificates))

    // Jira FC-442 "tijdnotatie eindemeetdag" m.a.w. 235959
    // Niet in CalibrationWriter want die is gecertificeerd
    val formattedDate = new SimpleDateFormat("dd/MM/yyyy kk:mm:ss.SSS")
    log.debug("--------------BEGIN makeCalibrationTestResult--------------------------------------------------")
    log.debug("selfTestResult.time=" + time + " " + formattedDate.format(time))
    val (_, dayEndTimeToUse) = DayUtility.calculateDayPeriod(dayEndTime)
    log.debug("dayEndTimeToUse=" + dayEndTimeToUse + " " + formattedDate.format(dayEndTimeToUse))

    val timeToUse = if (time < dayEndTimeToUse) {
      selfTestResult.time
    } else {
      selfTestResult.time - 1
    }

    log.debug("timeToUse=" + timeToUse + " " + formattedDate.format(timeToUse))
    log.debug("--------------END makeCalibrationTestResult--------------------------------------------------")

    CalibrationTestResult(selfTestResult.success,
      selfTestResult.time,
      selfTestResult.reportingOfficerCode,
      selfTestResult.nrCamerasTested,
      components,
      Seq(serialNr),
      SystemParameters(corridor.entryLocationName + " - " + corridor.exitLocationName,
        schedule.enforcedSpeedLimit,
        corridor.dynaMaxScanInterval,
        corridor.nrDaysKeepViolations,
        corridor.compressionFactor,
        corridor.locationLine1,
        selfTestResult.reportingOfficerCode,
        corridor.service.thresholdTime,
        corridor.service.thresholdSpeed(schedule.enforcedSpeedLimit),
        corridor.service match {
          case x: RedLight ⇒ corridor.service.pardonTime
          case _           ⇒ corridor.pardonTimeEsa
        }),
      corridor.service.serviceType,
      // TODO get 2 digit camera id for each photo. Currently using a simple index on the list of images
      selfTestResult.images.sortBy(_.id).zipWithIndex.map { testImageWithIndex ⇒
        CalibrationImage(testImageWithIndex._2,
          timeToUse,
          imageReader.readRow(testImageWithIndex._1).getOrElse(Array[Byte]()))
      })
  }

  override private[register] def publishSystemEvent(event: SystemEvent): Unit =
    context.system.eventStream.publish(event)

}
