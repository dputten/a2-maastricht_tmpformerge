/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.nl.pardon

import csc.sectioncontrol.enforce.common.nl.violations.Violation
import csc.sectioncontrol.enforce.nl.register.PardonedViolations
import csc.akkautils.DirectLogging
import csc.curator.utils.Curator
import csc.sectioncontrol.storagelayer.alerts.AlertService
import csc.sectioncontrol.reports.metrics.Period
import csc.sectioncontrol.storage.ZkAlertHistory
import akka.util.duration._
import csc.sectioncontrol.enforce.nl.RegistrationTimeUnreliableConfig
import csc.sectioncontrol.storagelayer.corridor.CorridorIdMapping

object PardonRegistrationTimeUnreliable extends DirectLogging {
  def pardon(curator: Curator, systemId: String, start: Long, end: Long, violations: Seq[Violation], cfg: RegistrationTimeUnreliableConfig): PardonViolationOutput = {

    val systemAlerts = AlertService.getAlertLogHistory(curator, systemId, start, end)

    val periodsByCorridor = systemAlerts.map { case (id, alerts) ⇒ id.infoId -> createPeriods(cfg, alerts) }

    val violationsByCorridor = violations.groupBy(_.corridorData.corridorId)

    val corridorResults = for (
      (infoId, corridorViolations) ← violationsByCorridor
    ) yield {
      var pardon = Seq[PardonedViolations]()
      val periods = periodsByCorridor.get(infoId).getOrElse(Map())

      val (pardonViolation, notPardonedViolations) = corridorViolations.partition(vio ⇒ {
        {
          val entry = vio.vvr.classifiedRecord.speedRecord.entry
          val entryLanePeriods = periods.get(entry.lane.laneId).getOrElse(Seq())
          if (isViolationPartOfPeriods(entry.eventTimestamp, entryLanePeriods)) {
            pardon = pardon :+ PardonedViolations(Seq(vio.vvr), "Entry time is unreliable")
            true
          } else {
            false
          }
        } || {
          vio.vvr.classifiedRecord.speedRecord.exit.exists(exit ⇒ {
            val exitLanePeriods = periods.get(exit.lane.laneId).getOrElse(Seq())
            if (isViolationPartOfPeriods(exit.eventTimestamp, exitLanePeriods)) {
              pardon = pardon :+ PardonedViolations(Seq(vio.vvr), "Exit time is unreliable")
              true
            } else {
              false
            }
          })
        }
      })

      log.info("Corridor {}: Number of Violations within RegistrationTimeUnreliable period {}. Periods = {}",
        infoId, pardonViolation.size, periods)
      PardonViolationOutput(notPardonedViolations, pardonViolation, pardon)
    }

    val total = corridorResults.foldLeft(PardonViolationOutput(Seq(), Seq(), Seq())) { (total, corridorResults) ⇒
      PardonViolationOutput(
        notPardonedViolations = total.notPardonedViolations ++ corridorResults.notPardonedViolations,
        originalPardonViolations = total.originalPardonViolations ++ corridorResults.originalPardonViolations,
        pardoned = total.pardoned ++ corridorResults.pardoned)
    }

    log.info("Number of Violations within RegistrationTimeUnreliable period {}.",
      total.originalPardonViolations.size)
    total
  }

  private[pardon] def createPeriods(cfg: RegistrationTimeUnreliableConfig, systemAlerts: Seq[ZkAlertHistory]): Map[String, Seq[Period]] = {
    //make periods for each lane
    var lanePeriods = Map[String, Seq[Period]]()
    for (alert ← systemAlerts) {
      val driftData = cfg.driftEvents.find(_.eventType == alert.alertType)
      for (
        drift ← driftData;
        laneId ← getLane(alert)
      ) {
        val startPeriod = alert.startTime
        val endPeriod = calculateEndTime(startPeriod, alert.endTime, drift.driftMsecPerHour)
        var list = lanePeriods.get(laneId).getOrElse(Seq())
        lanePeriods += laneId -> (list :+ Period(startPeriod, endPeriod))
      }
    }
    lanePeriods
  }

  private[pardon] def isViolationPartOfPeriods(time: Long, periods: Seq[Period]): Boolean = {
    periods.exists(per ⇒ per.from < time && time <= per.to)
  }

  private[pardon] def getLane(alert: ZkAlertHistory): Option[String] = {
    val separator = '-'
    var lane: Option[String] = None
    if (alert.configType == "Lane") {
      //parse path
      val component = alert.path.replace('/', separator)
      val pos = component.lastIndexOf(separator.toString)
      if (pos > 0) {
        val laneId = component.substring(0, pos)
        val nrSep = laneId.count(_ == '-')
        if (nrSep == 2) {
          lane = Some(laneId)
        }
      }
    }
    lane
  }

  private[pardon] def calculateEndTime(start: Long, end: Long, drift: Int): Long = {
    val duration = end - start
    val nrHours = duration / 1.hour.toMillis + 1 //round up
    if (Long.MaxValue - nrHours * drift < end) //current alerts have max long and when adding the drift will make it a negative time
      Long.MaxValue
    else
      end + nrHours * drift
  }
}