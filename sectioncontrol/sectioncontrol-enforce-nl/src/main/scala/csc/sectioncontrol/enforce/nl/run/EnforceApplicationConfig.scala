package csc.sectioncontrol.enforce.nl.run

import com.typesafe.config.ConfigFactory

object EnforceApplicationConfig {

  val UMTS_ENABLED = true
  val UMTS_DISABLED = !UMTS_ENABLED

  val akkaConfig = ConfigFactory.load()

  val umtsEnabled = akkaConfig.getString("sectioncontrol.umts").trim.equalsIgnoreCase("on")
}
