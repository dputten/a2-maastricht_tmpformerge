package csc.sectioncontrol.enforce.nl

import java.util.{ Calendar, GregorianCalendar }
import net.liftweb.json.DefaultFormats
import akka.actor._
import akka.util.duration._
import csc.akkautils._
import csc.config.Path
import csc.hbase.utils.HBaseTableKeeper
import csc.curator.utils.Curator
import csc.image.recognizer.client.{ RecognizeImage, RecognitionType }
import csc.sectioncontrol.storage._
import register.StoreMtmData
import run._
import csc.sectioncontrol.enforce.{ Cleanup, EnforceConfig, EnforceBoot }
import csc.sectioncontrol.sign.certificate.Certifier
import csc.sectioncontrol.reports.ReportingActor
import csc.sectioncontrol.qualify.ViolationQualifier
import csc.sectioncontrol.sign.SoftwareChecksum
import csc.sectioncontrol.writer.nl.ViolationWriter
import csc.sectioncontrol.common.DayUtility
import csc.sectioncontrol.storagelayer.servicestatistics.ServiceStatisticsHBaseRepositoryAccess
import csc.sectioncontrol.storagelayer.ServiceStatisticsRepositoryAccess
import csc.sectioncontrol.storagelayer.hbasetables.MtmFileTable
import csc.sectioncontrol.messages.EsaProviderType

/**
 * Boot the enforcement related actors for Dutch systems. This includes ProcessMtmData,
 * QualifyViolations and RegisterViolations.
 *
 * @author Maarten Hazewinkel
 */
class Boot extends EnforceBoot[EnforceNLConfig] with Certifier with HBaseTableKeeper {
  def componentName = "SectionControl-Enforce-NL"

  val defaultConfig = Map(
    "recognizeWithARHAgain" -> false,
    "intradaInterfaceToUse" -> 2,
    "minLowestLicConfLevel" -> 50,
    "minLicConfLevelRecognizer2" -> 70,
    "autoThresholdLevel" -> 55,
    "mobiThresholdLevel" -> 12,
    "pardonRegistrationTime" -> Map("driftEvents" -> "[]"))

  val pathGlobal = Path(Paths.Configuration.getGlobalEnforceConfig)

  protected def getEnforceConfig(systemId: String, curator: Curator): Option[EnforceNLConfig] = {
    val path = Path(Paths.Systems.getSystemJobsPath(systemId)) / "enforceConfig-nl"
    val config = curator.get[EnforceNLConfig](Seq(pathGlobal, path), defaultConfig)
    config match {
      case Some(c) ⇒ log.debug("loaded from " + path.toString() + ": " + c.toString)
      case None    ⇒ log.debug("could not load value from " + path.toString())
    }
    config
  }

  override def pardonSwitch(): Boolean = {

    val curator = createCurator(actorSystem, formats(DefaultFormats))
    //path to template of a system in case the global path doesn't exists
    val path = Path(Paths.Configuration.getGlobalSystemsConfig) / "enforceConfig-nl"

    val config = curator.get[EnforceNLConfig](Seq(pathGlobal, path), defaultConfig)
    config match {
      case Some(c) ⇒ config.get.pardonDuringDSTSwitch.getOrElse(false)
      case None    ⇒ false
    }
  }

  protected def startEnforceActors(system: ActorSystem,
                                   systemId: String,
                                   curator: Curator,
                                   hbaseZkServers: String,
                                   config: EnforceNLConfig) {
    this.synchronized {
      val esa = new EnforcementSystemActors(system, systemId, curator, hbaseZkServers, config)
      enforcementSystemActorMap = enforcementSystemActorMap + (systemId -> esa)
    }

    //new EnforcementAuditActors(system, systemId, curator)
    //create and register the JAR certificate
    SoftwareChecksum.createHashFromRuntimeClass(classOf[ViolationQualifier]) match {
      case Some(checksum) ⇒ {
        createAndRegisterCertificate(system, systemId, curator, Some(checksum.fileName), Some(checksum.checksum))
      }
      case None ⇒ {
        //very strange couldn't find checksum
        log.warning("Couldn't find checksum for ViolationQualifier")
      }
    }
    SoftwareChecksum.createHashFromRuntimeClass(classOf[ViolationWriter]) match {
      case Some(checksum) ⇒ {
        createAndRegisterCertificate(system, systemId, curator, Some(checksum.fileName), Some(checksum.checksum))
      }
      case None ⇒ {
        //very strange couldn't find checksum
        log.warning("Couldn't find checksum for ViolationWriter")
      }
    }
  }

  private var enforcementSystemActorMap: Map[String, EnforcementSystemActors] = Map()

  /**
   * Shut down the actors in a single system-specific ActorSystem. Optional to be implemented by
   * the concrete implementation.
   */
  override def shutdownSystemActors(system: ActorSystem, systemId: String) {
    try {
      this.synchronized {
        val esa = enforcementSystemActorMap.get(systemId)
        esa.foreach(_.shutdown())
        enforcementSystemActorMap
      }
    } finally {
      super.shutdownSystemActors(system, systemId)
    }
  }

  protected def zkServerQuorumConfigPath = "sectioncontrol.zkServers"
  protected def hbaseZkServerQuorumConfigPath = "sectioncontrol.hbaseZkServers"

  protected def moduleName = "EnforceNL"
}

case class DriftEvent(driftMsecPerHour: Int, eventType: String)
case class RegistrationTimeUnreliableConfig(driftEvents: Seq[DriftEvent])

/**
 * Configuration parameters for the whole Enforcement actor ensemble.
 * In this case it only implements the parameters required for the generic EnforceConfig
 * used to start up the generic enforcement actors.
 */
case class EnforceNLConfig(ntpServer: String,
                           timeZone: String,
                           verifyRecognition: String,
                           maxMtmFileDelay: Option[Long] = None,
                           maxViolationExportDelay: Option[Long] = None,
                           maxDailyReportDelay: Option[Long] = None,
                           dataRetentionDays: Option[Int] = None,
                           runtimeOffset: Option[Long] = None,
                           timeoutImageResponse: Option[Int] = None,
                           confidenceLevel: Option[Int] = None,
                           recognizerCountries: Option[Seq[String]] = None,
                           recognizerCountryMinConfidence: Option[Int] = None,
                           maxCertifiedSpeed: Int = 300,
                           pardonDuringDSTSwitch: Option[Boolean] = None,
                           recognizeWithARHAgain: Boolean = false,
                           intradaInterfaceToUse: Int = 2,
                           minLowestLicConfLevel: Int = 50,
                           minLicConfLevelRecognizer2: Int = 70,
                           autoThresholdLevel: Int = 55,
                           mobiThresholdLevel: Int = 12,
                           pardonRegistrationTime: RegistrationTimeUnreliableConfig = RegistrationTimeUnreliableConfig(Seq())) extends EnforceConfig

/**
 * This class provides a wrapper for the actors implementing the data processing in Enforce-NL.
 * This allows references between actors to be provided.
 */
class EnforcementSystemActors(actorSystem: ActorSystem, systemId: String, curator: Curator, hbaseZkServers: String,
                              config: EnforceNLConfig) extends HBaseTableKeeper with DirectLogging {

  import EnforceApplicationConfig._

  private lazy val hbaseConfig = HBaseTableKeeper.createCachedConfig(hbaseZkServers)

  private val manageEnforceStatus = actorSystem.actorOf(Props(new ManageEnforceStatus(systemId, curator)), "manageEnforceStatus")

  private val storeMtmData = actorSystem.actorOf(Props(new StoreMtmData(systemId, MtmFileTable.makeStore(hbaseConfig, this), curator)), "storeMtmData")

  val verifyRecognitionType = RecognitionType.withName(config.verifyRecognition)

  // TODO should be moved to configuration
  val dacolianZookeeperQueuePath: String = "/ctes/dacolianServer/workQueue"

  /**
   * We only start the image recognition actor if UMTS is disabled, because if UMTS is disabled ValidationFilter will
   * recognize the images as well. Would we have started the actor, it will directly start consuming the messages from
   * the workqueue causing the ValidationFilter to miss messages.
   */
  private val recognizeImage: Option[ActorRef] = umtsEnabled match {
    case UMTS_DISABLED ⇒ Some(actorSystem.actorOf(Props(new RecognizeImage(verifyRecognitionType, dacolianZookeeperQueuePath, systemId, curator, hbaseZkServers))))
    case UMTS_ENABLED  ⇒ None
  }

  private val hBaseConfiguration = createConfig(EnforcementSystemActors.this.hbaseZkServers)
  private val serviceStatisticsAccess: ServiceStatisticsRepositoryAccess = new ServiceStatisticsHBaseRepositoryAccess {
    protected val hbaseConfig = hBaseConfiguration
    protected val actorSystem: ActorSystem = EnforcementSystemActors.this.actorSystem
  }

  private val reporter = actorSystem.actorOf(Props(new ReportingActor(systemId, curator, hBaseConfiguration, serviceStatisticsAccess)), "reporter")

  actorSystem.actorOf(Props(new EnforceEventListener(systemId, curator, reporter)), "systemEventListener")

  actorSystem.actorOf(Props(new RunScheduledJobs(Paths.Systems.getSystemJobsPath(systemId) + "/processMtmDataJob",
    curator,
    startProcessMtmJob) with WaitForCompletion with EnforceRuntimeOffset), "processMtmData_JobScheduler")

  actorSystem.actorOf(Props(new RunScheduledJobs(Paths.Systems.getSystemJobsPath(systemId) + "/qualifyViolationsJob",
    curator,
    startQualifyViolationsJob) with WaitForCompletion with EnforceRuntimeOffset), "qualifyViolations_JobScheduler")

  actorSystem.actorOf(Props(new RunScheduledJobs(Paths.Systems.getSystemJobsPath(systemId) + "/registerViolationsJob",
    curator,
    startRegisterViolationsJob) with WaitForCompletion with EnforceRuntimeOffset), "registerViolations_JobScheduler")

  actorSystem.actorOf(Props(new RunScheduledJobs(Paths.Systems.getSystemJobsPath(systemId) + "/cleanupEnforceJob",
    curator,
    runEnforceCleanup)), "cleanupEnforce_JobScheduler")

  var alertForOverdue: Option[ActorRef] = None
  def startAlertsOverdue() {
    alertForOverdue.getOrElse {
      val actorRef = actorSystem.actorOf(Props(new AlertForOverdueEnforceStep(manageEnforceStatus, DayUtility.fileExportTimeZone, systemId, config, curator)),
        "AlertForOverdueEnforceStep")
      alertForOverdue = Some(actorRef)
      actorRef
    }
  }

  protected def startProcessMtmJob(zkJobPath: String, runToken: Option[RunToken], runner: ActorRef, curator: Curator) {
    val run = Run(zkJobPath, runner, runToken.get, yesterday(runToken.get.scheduledTime), manageEnforceStatus)
    val systemConfigPath = Paths.Systems.getConfigPath(systemId)

    val esaTypeProvider = curator.get[ZkSystem](systemConfigPath) match {
      case Some(systemConfig) ⇒ systemConfig.esaProviderType
      case None               ⇒ EsaProviderType.NoProvider
    }

    esaTypeProvider match {
      case EsaProviderType.NoProvider ⇒
        runner ! CreateJobActor(() ⇒ new RunNoEsaProvider(systemId,
          curator,
          config.timeZone,
          getSpeedLog = getSpeedLog,
          getExcludeLog = getExcludeLog), run)
      case EsaProviderType.Dynamax ⇒
        startAlertsOverdue()
        runner ! CreateJobActor(() ⇒ new RunWaitForDynamaxReports(systemId,
          curator,
          storeMtmData,
          config.timeZone, getSpeedLog = getSpeedLog,
          getExcludeLog = getExcludeLog), run)
      case EsaProviderType.Mtm ⇒
        startAlertsOverdue()
        runner ! CreateJobActor(() ⇒ new RunProcessMtmData(systemId,
          curator,
          EnforceBoot.getSpeedLog(systemId),
          storeMtmData,
          config.timeZone), run)
      case EsaProviderType.TubesProvider ⇒
        startAlertsOverdue()
        runner ! CreateJobActor(() ⇒ new RunProcessTubesData(systemId,
          curator,
          getSpeedLog = getSpeedLog,
          getExcludeLog = getExcludeLog,
          storeMtmData = storeMtmData,
          timeZoneStr = config.timeZone), run)
    }

    def getSpeedLog(systemId: String): ActorRef = {
      EnforceBoot.getSpeedLog(systemId)
    }

    def getExcludeLog(systemId: String): ActorRef = {
      EnforceBoot.getExcludeLog(systemId)
    }
  }

  protected def startQualifyViolationsJob(zkJobPath: String, runToken: Option[RunToken], runner: ActorRef, curator: Curator) {
    val startMsg = Run(zkJobPath, runner, runToken.get, yesterday(runToken.get.scheduledTime), manageEnforceStatus)
    runner ! CreateJobActor(() ⇒ new RunQualifyViolations(systemId, curator, hbaseConfig, storeMtmData), startMsg)
  }

  protected def startRegisterViolationsJob(zkJobPath: String, runToken: Option[RunToken], runner: ActorRef, curator: Curator) {
    val startMsg = Run(zkJobPath, runner, runToken.get, yesterday(runToken.get.scheduledTime), manageEnforceStatus)
    runner ! CreateJobActor(() ⇒ new RunRegisterViolations(
      systemId,
      curator,
      hbaseConfig,
      recognizeImage,
      config.timeoutImageResponse.getOrElse(300000).millis,
      config.confidenceLevel.getOrElse(100),
      config.recognizerCountries.getOrElse(Seq("NL", "DE", "BE", "CH")),
      config.recognizerCountryMinConfidence.getOrElse(30),
      config.maxCertifiedSpeed,
      config.recognizeWithARHAgain,
      config.intradaInterfaceToUse,
      config.minLowestLicConfLevel,
      config.minLicConfLevelRecognizer2,
      config.autoThresholdLevel,
      config.mobiThresholdLevel,
      config.pardonRegistrationTime),
      startMsg)
  }

  protected def runEnforceCleanup(zkJobPath: String, runToken: Option[RunToken], runner: ActorRef, curator: Curator) {
    val retentionDays = config.dataRetentionDays.getOrElse(60)

    EnforceBoot.getExcludeLog(systemId) ! Cleanup(retentionDays)
    EnforceBoot.getSpeedLog(systemId) ! Cleanup(retentionDays)
    storeMtmData ! Cleanup(retentionDays)
  }

  private def yesterday(time: Long): Long = {
    val cal = new GregorianCalendar(DayUtility.fileExportTimeZone)
    cal.setTimeInMillis(time)
    cal.add(Calendar.DATE, -1)
    cal.getTimeInMillis
  }

  private[nl] def shutdown() {
    closeTables()
  }

  trait EnforceRuntimeOffset extends RunScheduledJobs {
    override protected def sendClockTick() {
      config.runtimeOffset match {
        case None         ⇒ super.sendClockTick()
        case Some(offset) ⇒ self ! ClockTick(System.currentTimeMillis + offset)
      }
    }
  }
}

