/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.nl

import csc.config.Path
import csc.sectioncontrol.enforce.{ MsiBlankEvent, MtmSpeedSettings, DayViolationsStatistics }
import csc.sectioncontrol.messages._
import csc.sectioncontrol.storage.Paths
import csc.curator.utils.{ CuratorActor, Curator }
import akka.actor.{ ActorLogging, ActorRef }

/**
 * Actor which listens to the event stream and puts the statistic events from Matcher and RegisterViolations to Zookeeper.
 * The DayViolationsStatistics event is the signal to create the final day report
 * (under for instance "/ctes/systems/A2/statistics/20120401/result")
 * @param systemId system ID (for instance A2)
 * @param curator ZooKeeper servers
 */
class EnforceEventListener(val systemId: String, val curator: Curator, reporter: ActorRef) extends CuratorActor with ActorLogging {

  def receive = {
    case stat: DayViolationsStatistics ⇒
      log.info("Receive message DayViolationsStatistics (%s). Save the runtime statistics".format(stat.systemId))
      saveViolationsStatistics(stat)

      val generateReportMessage = GenerateReport(stat.systemId, stat.stats)
      log.info("Send message {}", generateReportMessage)
      reporter ! generateReportMessage
    case mtm: MtmSpeedSettings ⇒ saveMtmSpeedSettings(mtm)
    case msi: MsiBlankEvent    ⇒ saveMsiBlanks(msi)
  }

  override def preStart() {
    super.preStart()
    /*
     * Subscribe on the following messages which can be send on the EventBus
     */
    context.system.eventStream.subscribe(self, classOf[DayViolationsStatistics])
    context.system.eventStream.subscribe(self, classOf[MtmSpeedSettings])
    context.system.eventStream.subscribe(self, classOf[MsiBlankEvent])
  }

  private def saveViolationsStatistics(statistics: DayViolationsStatistics) {
    //check is statistics are empty
    val isStatisticsEmpty = statistics.corridorData.foldLeft(true) {
      case (empty, data) ⇒
        empty && data.total == 0 && data.auto == 0 && data.manual == 0 && data.mobi == 0 &&
          data.mobiDoNotProcess == 0 && data.doublePardon == 0 && data.mtmPardon == 0 && data.otherPardon == 0
    }
    val storePath = Path(Paths.Systems.getRuntimeViolationsPath(systemId, statistics.stats))
    (curator.exists(storePath), isStatisticsEmpty) match {
      case (true, true) ⇒
        //don't update an existing with an empty
        log.warning("Received empty statistics for %s, while statistics already exist. Skipped saving statistics".format(statistics.systemId))
      case (true, false) ⇒
        //update statistics
        curator.deleteRecursive(storePath)
        curator.put(storePath, statistics)
      case (false, _) ⇒
        //insert statistics
        curator.put(storePath, statistics)
    }
  }

  def saveMtmSpeedSettings(settings: MtmSpeedSettings): Unit = {
    val storePath = Path(Paths.Systems.getRuntimeMtmInfoPath(systemId, settings.statsKey))
    if (curator.exists(storePath))
      curator.deleteRecursive(storePath)
    curator.put(storePath, settings)
  }

  def saveMsiBlanks(msiBlankEvent: MsiBlankEvent): Unit = {
    val storePath = Path(Paths.Systems.getRuntimeMsiBlanksPath(systemId, msiBlankEvent.statsKey))
    if (curator.exists(storePath))
      curator.deleteRecursive(storePath)
    curator.put(storePath, msiBlankEvent)
  }
}

