package csc.sectioncontrol.enforce.nl.run

/**
 * Copyright (C) 2014 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 11/3/14.
 */

import java.text.SimpleDateFormat
import java.util.{ Date, TimeZone }
import java.io.File
import scala.Left
import scala.Some

import akka.actor._
import akka.util.duration._
import akka.actor.SupervisorStrategy.Stop
import akka.event.LoggingReceive
import akka.actor.AllForOneStrategy

import csc.akkautils.{ JobComplete, JobFailed }
import csc.curator.utils.{ Curator, CuratorActor }
import csc.sectioncontrol.tubes._
import csc.sectioncontrol.common.DayUtility
import csc.sectioncontrol.storage.{ ZkSystemPardonTimes, ZkSystem, ZkCorridor, Paths }
import csc.sectioncontrol.enforce.EnforceBoot
import csc.sectioncontrol.enforce.SpeedIndicatorStatus
import csc.config.Path
import csc.sectioncontrol.messages.SystemEvent
import csc.sectioncontrol.enforce.speedlog.{ GeneratedExcludeEvents, GenerateExcludeEvents, ScheduledSpeed }
import org.apache.commons.io.FileUtils
import csc.sectioncontrol.enforce.nl.register.LogMTMRaw

/**
 * Handle the running of the Tubes file processing job.
 *
 * This job needs to wait for the Tubes file to arrive and be of a certain age. The last is to ensure that the
 * file is not still in the process of being written to.
 *
 * While those conditions are not satisfied, this job will re-schedule itself to run every minute.
 * Once the conditions are met the job will run through and re-schedule itself for the next day.
 *
 * @author Jan Willem Luiten
 */

/**
 * Job configuration ofr processing Tubes data
 * @param fileDeliveryPath directory path where files are delivered to the system
 * @param fileNamePattern pattern to construct filename based on the date
 * @param filePickupDelay file is picked up if it wasn't modified for the given delay
 */
case class ProcessTubesDataJobConfig(fileDeliveryPath: String,
                                     fileNamePattern: String,
                                     filePickupDelay: Long = 1.minute.toMillis) {

  /**
   * Get a SimpleDateFormat instance for the given timeZoneString
   * @param timeZoneStr
   * @return
   */
  def getDateFormat(timeZoneStr: String): SimpleDateFormat = {
    val df = new SimpleDateFormat(fileNamePattern)
    df.setTimeZone(TimeZone.getTimeZone(timeZoneStr))
    df
  }
}

/**
 * Actor that runs the processing of tubesdata
 * @param systemId      the system for which to run
 * @param curator       Curator providing access to ZooKeep
 * @param getSpeedLog   function to retrieve the speedlog actor
 * @param getExcludeLog function to retrieve the excludeLog actor
 * @param storeMtmData  ActorRef to actor storing MTM data
 * @param timeZoneStr   TimeZone we're working in
 */
class RunProcessTubesData(systemId: String,
                          protected val curator: Curator,
                          getSpeedLog: (String) ⇒ ActorRef,
                          getExcludeLog: (String) ⇒ ActorRef,
                          storeMtmData: ActorRef,
                          timeZoneStr: String) extends CuratorActor {

  var runner: ActorRef = _

  val localDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
  val timeZone = TimeZone.getTimeZone(timeZoneStr)
  localDateFormat.setTimeZone(timeZone)

  private val processTubesData = context.actorOf(Props(new ProcessTubesData(systemId, curator, localDateFormat)), "processTubesData")

  protected def receive = LoggingReceive {
    case run: Run ⇒
      log.info("received run message:" + run)
      handleRun(run)

  }

  /**
   * Handle the processing of TubesData
   * @param run description of our invocation
   */
  protected def handleRun(run: Run) {
    runner = run.runner
    val workDate = run.actionDate
    val configPath = run.zkJobPath + "/config"
    curator.get[ProcessTubesDataJobConfig](configPath) match {
      case None ⇒
        log.error("processTubesDataJob config not found at {}", configPath)
        runner ! JobFailed
      case Some(config) ⇒ {
        log.info("loaded from " + configPath + ": " + config)
        // Instantiate fileHandle and create File instance of the file
        // that is expected for this run
        val fileHandler = new EsaFileHandler(workDate, config.getDateFormat(timeZoneStr),
          config.fileDeliveryPath, run.runToken.startTime)
        val file = fileHandler.expectedFile()

        fileHandler.fileState(file) match {

          case EsaFileState.FileNotFound ⇒ {
            log.info("expected file {} not found. Rescheduling run", file.getAbsolutePath)

            // Tell the one who started us that we failed.
            // JobFailed with retry=true triggers running this job again in 1 minute
            runner ! JobFailed(retry = true)
          }
          case EsaFileState.TooRecentlyModified ⇒ {
            log.info("expected file {} too recently modified. Rescheduling run", file.getAbsolutePath)

            // Tell the one who started us that we failed.
            // JobFailed with retry=true triggers running this again in 1 minute
            runner ! JobFailed(retry = true)
          }
          case EsaFileState.IncorrectModificationDate ⇒ {
            val modifiedDate = localDateFormat.format(file.lastModified())
            val expectedDate = localDateFormat.format(fileHandler.expectedExportDay)

            log.error("expected file modified date {} does not match expected date {}",
              modifiedDate, expectedDate)

            EsaFileHandler.moveFileToProcessed(file, timeZone)
            runner ! JobFailed(retry = true)
          }
          case EsaFileState.ReadyForProcessing ⇒ {
            log.info("expected file found at {}", file.getAbsolutePath)
            val (dataDayStart, dataDayEnd) = DayUtility.calculateDayPeriod(workDate)
            val configs = TubesSettingsHelper.getTubesSettings(curator, systemId)
            log.info("tubesSetting = " + configs)
            val processingMessage = TubesSource(file.getAbsolutePath, run.runToken, run.runner, getSpeedLog(systemId), configs, dataDayStart, dataDayEnd)
            val monitor = context.actorOf(Props(new ProcessTubesDataMonitor(systemId, curator, getSpeedLog(systemId),
              getExcludeLog(systemId), storeMtmData, processTubesData, run.manageEnforceStatus, workDate, timeZone)),
              "ProcessTubesData_" + systemId + "_monitor")
            monitor ! processingMessage
          }
        }
      }
    }
  }

  override def supervisorStrategy() = AllForOneStrategy() {
    case e: Exception ⇒
      log.error(e, "Tubes processing crashed. Scheduling re-run.")
      context.system.eventStream.publish(
        SystemEvent("ProcessTubesData", System.currentTimeMillis(), systemId, "Alert", "system",
          Some("Tubes verwerking is gecrashed met fout: " + e.getMessage)))
      runner ! JobFailed(retry = true)
      Stop
  }
}

/**
 * An actor Monitoring the actual processing.
 * Note: This was refactored from the inline actor in RunProcessMtmData.scala
 *       This functionality is better moved to RunProcessTubesData
 * @param systemId          the system we're running for
 * @param curator           curator providing access to ZooKeeper
 * @param speedLog          speedlog actor
 * @param excludeLog        excludelog actor
 * @param storeMtmData      actor storing mtm-data
 * @param processTubesData  the actor processing the tubes data
 * @param manageStatus      actor managin status
 * @param workDate          the workdate we're running for
 * @param timeZone          the timezone we're in
 */
private class ProcessTubesDataMonitor(systemId: String,
                                      protected val curator: Curator,
                                      speedLog: ActorRef,
                                      excludeLog: ActorRef,
                                      storeMtmData: ActorRef,
                                      processTubesData: ActorRef,
                                      manageStatus: ActorRef,
                                      workDate: Long,
                                      timeZone: TimeZone) extends CuratorActor {

  // UGLY but is used in supervisorStrategy() method
  private var currentSource: TubesSource = _

  protected def receive = LoggingReceive {
    case source: TubesSource ⇒
      log.info("Received {}", source)
      currentSource = source
      if (source.settings.isEmpty) {
        log.info("No Tubes configuration found. File {} cannot be processed", source.filename)
        context.system.eventStream.publish(
          SystemEvent("ProcessTubesData", System.currentTimeMillis(), systemId, "Alert", "system",
            Some("Geen Tubes configuratie  ingesteld. Kan geen Tubes data verwerken")))
        manageStatus ! ProcessingMtmCompleted(workDate, source.runToken.startTime)
        source.runner ! JobComplete
        setSpeedsToUndetermined(systemId, workDate)
        EsaFileHandler.moveFileToProcessed(new File(source.filename), timeZone)
      } else {
        context.watch(processTubesData)
        processTubesData ! source
      }

    case Right(TubesProcessingSucceeded(filename, runner)) ⇒
      log.info("processing Tubes data for {} completed successfully", systemId)
      generateExcludeEvents(workDate)

    case Left(TubesProcessingFailed(filename, runner, exception)) ⇒
      log.error("Processing Tubes data for system {} failed with exception {}", systemId, exception)
      currentSource.runner ! JobFailed
      manageStatus ! ProcessingMtmCompleted(workDate, currentSource.runToken.startTime)
      setSpeedsToUndetermined(systemId, workDate)
      EsaFileHandler.moveFileToProcessed(new File(filename), timeZone)
      context.system.eventStream.publish(
        SystemEvent("ProcessTubesData", System.currentTimeMillis(), systemId, "Alert", "system",
          Some("Tubes verwerking is gefaald met fout: " + exception.getMessage)))

    case GeneratedExcludeEvents ⇒
      excludeEventsPending -= 1
      log.info("Received GeneratedExcludeEvents, events pending = {}", excludeEventsPending)
      if (excludeEventsPending == 0) {
        val file = new File(currentSource.filename)
        storeMtmRaw(file)
        manageStatus ! ProcessingMtmCompleted(workDate, currentSource.runToken.startTime)
        EsaFileHandler.moveFileToProcessed(file, currentSource.runToken.timezone)
        currentSource.runner ! JobComplete
      }
  }

  var excludeEventsPending: Int = 0

  def generateExcludeEvents(workDate: Long) {
    var noMessagesSend = true
    excludeEventsPending = 0
    val (dayStart, dayEnd) = DayUtility.calculateDayPeriod(workDate)
    val systemCfg = curator.get[ZkSystem](Paths.Systems.getConfigPath(systemId))
    val pardonCfg = systemCfg.map(_.pardonTimes).getOrElse(ZkSystemPardonTimes())

    val corridorIds = getCorridorIds(systemId)
    corridorIds.foreach {
      corridorId ⇒
        val corridor = curator.get[ZkCorridor](Paths.Corridors.getConfigPath(systemId, corridorId))
        corridor.foreach {
          corridorConfig ⇒
            val technicalPardonTime_mSecs = pardonCfg.pardonTimeTechnical_secs * 1000L
            val esaPardonTime_mSecs = pardonCfg.pardonTimeEsa_secs * 1000L
            val scheduledSpeeds = LoadSystemConfiguration.
              getCorridorSchedules(systemId, corridorId, dayStart, dayEnd, timeZone, curator).
              map(s ⇒ ScheduledSpeed(s.startTime, s.endTime, s.enforcedSpeedLimit, s.supportMsgBoard))
            excludeEventsPending += 1
            noMessagesSend = false
            speedLog ! GenerateExcludeEvents(dayStart,
              dayEnd,
              corridorConfig.info.corridorId,
              technicalPardonTime_mSecs,
              esaPardonTime_mSecs,
              excludeLog,
              scheduledSpeeds)

        }
    }
    if (noMessagesSend) {
      excludeEventsPending += 1
      self ! GenerateExcludeEvents
    }
  }

  def setSpeedsToUndetermined(systemId: String, workDate: Long) {
    val (dataDayStart, _) = DayUtility.calculateDayPeriod(workDate)
    getCorridorIds(systemId).foreach {
      corridorId ⇒
        val path = Paths.Corridors.getConfigPath(systemId, corridorId)
        val config: ZkCorridor = curator.get[ZkCorridor](path).get
        speedLog ! SpeedDataImpl(dataDayStart, config.info.corridorId, -1, Left(SpeedIndicatorStatus.Undetermined))
    }
  }

  private def getCorridorIds(systemId: String): Seq[String] = {
    val corridorPaths = curator.getChildren(Paths.Corridors.getDefaultPath(systemId))
    corridorPaths.map {
      _.split('/').last
    }
  }

  private def storeMtmRaw(file: File) {
    val mtmRawBytes = FileUtils.readFileToByteArray(file)
    getCorridorIds(systemId).foreach {
      corridorId ⇒
        curator.get[ZkCorridor](Paths.Corridors.getConfigPath(systemId, corridorId)).foreach(c ⇒
          storeMtmData ! LogMTMRaw(workDate, c.info.corridorId, mtmRawBytes))
    }
  }

  override def supervisorStrategy() = AllForOneStrategy() {
    case e: Exception ⇒
      log.error(e, "Tubes processing crashed. Scheduling re-run.")
      context.system.eventStream.publish(
        SystemEvent("ProcessTubesData", System.currentTimeMillis(), systemId, "Alert", "system",
          Some("Tubes verwerking is gecrashed met fout: " + e.getMessage)))
      currentSource.runner ! JobFailed(retry = true)
      Stop
  }

}

object TubesSettingsHelper {
  /**
   * getTubesSettings. Collect TubesSettings per corridor. For each corridor aggregate
   * the TubesSettings for all sections in this corridor
   * @param curator provides interface to ZooKeeper.
   * @param systemId the systemId
   * @return a List of tubesSettings for all corridors
   */
  def getTubesSettings(curator: Curator, systemId: String): List[TubesSetting] = {
    val corridorPaths: Seq[Path] = curator.getChildren(Path(Paths.Corridors.getDefaultPath(systemId)))
    corridorPaths.map({
      (aPath) ⇒
        {
          val optCorridorInfo = curator.get[ZkCorridor](aPath / "config")
          optCorridorInfo.map((corridorInfo: ZkCorridor) ⇒ {
            val default: Map[String, Any] = Map("corridorId" -> corridorInfo.info.corridorId)
            val sectionIds = corridorInfo.allSectionIds
            val allSettings: Seq[Option[TubesSetting]] = sectionIds.map(
              (sectionId) ⇒
                {
                  val tubesPath = Path(Paths.Sections.getTubesSettingsPath(systemId, sectionId))
                  //The corridorId should not be set in zookeeper This is updated by the default
                  val setting = curator.get[TubesSetting](Seq(tubesPath), default)
                  setting
                })
            val (chartCodes, patterns) = allSettings.foldLeft(List[String](), List[String]())(
              (result, current: Option[TubesSetting]) ⇒ {
                current match {
                  case None          ⇒ result
                  case Some(setting) ⇒ (result._1 ++ setting.chartCodes, result._2 ++ setting.messagePatterns)
                }
              })
            TubesSetting(corridorInfo.info.corridorId, chartCodes.distinct, patterns.distinct)
          })
        }
    }).toList.flatMap((t) ⇒ t)
  }
}