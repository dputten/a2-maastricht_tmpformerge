package csc.sectioncontrol.enforce.nl.register

import csc.sectioncontrol.storage.{ HBaseTableDefinitions, KeyWithTimeAndId }
import HBaseTableDefinitions._
import csc.sectioncontrol.enforce.common.nl.register.PshTmIdGenerator
import csc.sectioncontrol.enforce.common.nl.violations.{ Violation, SystemConfigurationData }
import csc.sectioncontrol.storagelayer.hbasetables.ViolationRecordTable

/**
 * HBase data reader for violations.
 */
class ViolationDataReader(systemId: String, val reader: ViolationRecordTable.Reader) {

  /**
   * Read VehicleViolationRecords from the HBase table, enrich them with configuration data, and
   * return them as Violation instances.
   *
   * If no corridor and schedule configuration can be found for a Violation that was read in from
   * HBase, that Violation will be returned in the second part of the returned pair.
   *
   * @param startTime A timetamp from when to start reading
   * @param endTime A timestamp until when to read
   * @param systemConfig The configuration data for the system
   * @return A Tuple2 containing both a sequence of Violations that were read in and a sequence
   *         of VehicleViolationRecords that could not be matched to a corridor or schedule.
   */
  def readViolations(startTime: Long, endTime: Long, systemConfig: SystemConfigurationData): (Seq[Violation], Seq[(vehicleViolationRecordType, String)]) = {
    val readViolations = reader.
      readRows(KeyWithTimeAndId(startTime, systemId), KeyWithTimeAndId(endTime, systemId)).
      map(addConfigData(_, systemConfig))

    (readViolations.collect { case Right(violation) ⇒ violation },
      readViolations.collect { case Left(vvr) ⇒ vvr })
  }

  private def addConfigData(vvr: vehicleViolationRecordType, systemConfig: SystemConfigurationData): Either[(vehicleViolationRecordType, String), Violation] = {
    val corridor = systemConfig.corridors.find(_.corridorId == vvr.classifiedRecord.speedRecord.corridorId)
    val schedule = corridor.flatMap(corridor ⇒ {
      val speedRecord = vvr.classifiedRecord.speedRecord
      val timestamp = speedRecord.exit.map(_.eventTimestamp).getOrElse(speedRecord.entry.eventTimestamp)
      systemConfig.schedules.find(schedule ⇒ schedule.corridorId == corridor.corridorId &&
        schedule.startTime <= timestamp && schedule.endTime > timestamp)
    })
    val corridorReportingOfficer = systemConfig.reportingOfficers.get(vvr.classifiedRecord.speedRecord.corridorId)
    val pshtmOfficerCode = corridorReportingOfficer.flatMap(_.getReportingOfficerForDay(vvr.classifiedRecord.time))
    val reportingOfficerCode = corridorReportingOfficer.flatMap(_.getReportingOfficerForTime(vvr.classifiedRecord.time))

    if (schedule.isDefined && pshtmOfficerCode.isDefined && reportingOfficerCode.isDefined && corridor.isDefined) {
      Right(Violation(vvr, None, corridor.get, systemConfig.globalConfig, schedule.get,
        PshTmIdGenerator(schedule.get.pshTmIdFormat, vvr.classifiedRecord.time, pshtmOfficerCode.get,
          corridor.get.locationCode, corridor.get.service.serviceType, systemConfig.globalConfig.pshtmId,
          roadPart = systemConfig.globalConfig.roadPart),
        reportingOfficerCode.get))
    } else {
      val error = if (schedule.isEmpty) {
        "No matching schedule"
      } else if (reportingOfficerCode.isEmpty || pshtmOfficerCode.isEmpty) {
        "No reportingOfficeCode found. Likely the system was not in an 'Enforce' state"
      } else {
        "Undefined error in ViolationDataReader"
      }
      Left(vvr, error)
    }
  }

}
