/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.nl.register

import akka.actor.{ Actor, ActorLogging }
import csc.sectioncontrol.storage.KeyWithTimeIdAndPostFix
import csc.sectioncontrol.storage.HBaseTableDefinitions._
import csc.sectioncontrol.messages.ProcessedViolationRecord
import csc.sectioncontrol.storagelayer.hbasetables.ProcessedViolationTable

/**
 *
 * @author Maarten Hazewinkel
 */
class StoreProcessedViolations(processedViolationStore: ProcessedViolationTable.Store,
                               softwareChecksum: String) extends Actor with ActorLogging {
  protected def receive = {
    case PardonedViolations(violations, reason) ⇒
      val processedViolations = violations.
        map(vvr ⇒
          KeyWithTimeIdAndPostFix(vvr.classifiedRecord.time, vvr.classifiedRecord.speedRecord.entry.lane.system, Some(vvr.classifiedRecord.speedRecord.id)) ->
            ProcessedViolationRecord(vvr.id, vvr, false, true, Some(reason), softwareChecksum))

      log.info("Number of processedViolations to pardon => " + processedViolations.length + " Reason=" + reason)
      processedViolationStore.writeRows(processedViolations)

    case RegisteredViolations(violations) ⇒
      val processedViolations = violations.
        map(vvr ⇒
          KeyWithTimeIdAndPostFix(vvr.classifiedRecord.speedRecord.time, vvr.classifiedRecord.speedRecord.entry.lane.system, Some(vvr.classifiedRecord.speedRecord.id)) ->
            ProcessedViolationRecord(vvr.id, vvr, true, false, None, softwareChecksum))

      processedViolationStore.writeRows(processedViolations)

    case FailedViolations(violations, errorMessage) ⇒
      val processedViolations = violations.
        map(vvr ⇒
          KeyWithTimeIdAndPostFix(vvr.classifiedRecord.speedRecord.time, vvr.classifiedRecord.speedRecord.entry.lane.system, Some(vvr.classifiedRecord.speedRecord.id)) ->
            ProcessedViolationRecord(vvr.id, vvr, false, false, Some(errorMessage), softwareChecksum))

      log.info("Number of processedViolations are invalid => " + processedViolations.length + " Reason=" + errorMessage)
      processedViolationStore.writeRows(processedViolations)
  }
}

case class PardonedViolations(violations: Seq[vehicleViolationRecordType], reason: String)

case class RegisteredViolations(violations: Seq[vehicleViolationRecordType])

case class FailedViolations(violations: Seq[vehicleViolationRecordType], errorMessage: String)
