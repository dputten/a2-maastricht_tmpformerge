/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.nl.qualify

import java.text.SimpleDateFormat
import csc.sectioncontrol.enforce.speedlog.SpeedSettingOverTime
import csc.sectioncontrol.enforce._
import csc.sectioncontrol.enforce.common.nl.violations.CorridorSchedule
import csc.sectioncontrol.messages.StatsDuration
import nl.casefiles.{ HHMVS14, CaseFile }
import csc.sectioncontrol.enforce.MtmSpeedSetting
import csc.sectioncontrol.enforce.MtmSpeedSettings
import speedlog.SpeedSetting
import csc.sectioncontrol.common.DayUtility

/**
 * Formatting and collecting functions for MTM info data.
 *
 * The 2 primary uses are for generating the formatted MTM Info file for violation export, and
 * for generating a MtmSpeedSettings case class instance containing all the records for multiple
 * corridors for a single day. This includes extra day start and day end records where necessary.
 *
 * @author Maarten Hazewinkel
 */
object MtmInfoFormatter {

  /**
   * Generates a formatted MTM Info file for use by violation export for a single corridor
   *
   * @param location The location of the corridor
   * @param date The day to generate the file for
   * @param speedData The collected SpeedSettingsOverTime data for this corridor
   * @param schedules The schedules for this corridor
   * @return A formatted MTM Info file
   */
  def makeExportFile(location: String, date: Long, speedData: SpeedSettingOverTime, schedules: Seq[CorridorSchedule], caseFile: CaseFile): Array[Byte] = {

    val recordsToReport = buildRecords(date, speedData, caseFile, schedules)

    val lines = headerLines(location, date) ++
      recordsToReport.map(p ⇒ makeDataLine(date, p._1, location, speedSettingString(p._2, scheduledSpeed(p._1, schedules), caseFile)))

    lines.mkString(start = "", sep = "\r\n", end = "\r\n").getBytes("ISO-8859-1")
  }

  /**
   * Generates a MtmSpeedSettings instance containing all the records for multiple corridords
   *
   * @param location The locations of all the corridors
   * @param date The day te generate the file for
   * @param speedData The collected SpeedSettingsOverTime data for all corridors
   * @param schedules The schedules for all corridors
   * @return The MtmSpeedSettings case class instance
   */
  def makeMtmSpeedSettings(location: Map[Int, String],
                           date: Long,
                           speedData: Map[Int, SpeedSettingOverTime],
                           schedules: Seq[CorridorSchedule],
                           statsKey: StatsDuration,
                           caseFile: CaseFile): MtmSpeedSettings = {

    case class CorridorLastMoment(lastMoment: Long, corridorId: Int, speedSetting: SpeedSetting, caseFile: CaseFile)

    val allRecords: Iterable[CorridorLastMoment] = speedData.flatMap {
      case (corridorId, speeds) ⇒
        val corridorRecords = buildRecords(date, speeds, caseFile, schedules.filter(_.corridorId == corridorId))
        corridorRecords.map(p ⇒ CorridorLastMoment(p._1, corridorId, p._2, caseFile))
    }

    val corridorSchedules = schedules.groupBy(_.corridorId)
    MtmSpeedSettings(allRecords.toSeq.sortBy(t ⇒ (t.lastMoment, t.corridorId)).
      map(t ⇒ MtmSpeedSetting(t.lastMoment,
        location.getOrElse(t.corridorId, ""),
        speedSettingString(t.speedSetting, scheduledSpeed(t.lastMoment, corridorSchedules.getOrElse(t.corridorId, Seq())), t.caseFile))).toList,
      statsKey)
  }

  private def headerLines(location: String, date: Long): Seq[String] = {
    Seq("# Locatie",
      "# " + location,
      "# Datum " + dateFormat.format(date),
      "#",
      "#" + padRight("locatie", minimumFieldSizes._1 max location.size) + "|tijdstip           |maxsnelheid|")
  }

  private def buildRecords(date: Long, speedData: SpeedSettingOverTime, caseFile: CaseFile, schedules: Seq[CorridorSchedule]): Seq[(Long, SpeedSetting)] = {
    val (dayStart, dayEnd) = DayUtility.calculateDayPeriod(date)
    val lastMoment = caseFile.mtmInfoLastMoment(dayEnd)

    val prevDayEndSetting = speedData.settings.dropWhile(_._1 > dayStart).headOption.map(_._2).getOrElse(blank)

    val dayChanges = speedData.settings.filter(p ⇒ p._1 >= dayStart && p._1 <= dayEnd).toSeq.sortBy(_._1)

    val endOfDay = dayChanges.lastOption.map(_._2).getOrElse(prevDayEndSetting)
    //select only changes during the schedules
    //between schedules are Undetermined
    val changes = Seq(dayStart -> prevDayEndSetting) ++ dayChanges :+ (dayEnd -> endOfDay)

    val sortedSchedules = schedules.sortBy(_.startTime)

    //loop through aal dayChanges
    //standBy events are unchanged
    //others are unchanged when within a schedule
    //    but when there is a schedule end add undetermined at scheduleEnd
    //  otherwise are change to undetermined
    //    and if there is a schedule start between this even and the next move value to start schedule
    var mergedChanges = Seq[(Long, SpeedSetting)]()
    for (index ← 0 until changes.size - 1) {
      val speedInd = changes(index)._2.setting
      if (speedInd == Left(SpeedIndicatorStatus.StandBy)) {
        //standBy => ignore all schedules
        mergedChanges = mergedChanges :+ changes(index)
      } else {
        //merge schedules into the speedRecords
        val startTime = changes(index)._1
        val currentScheduleOpt = sortedSchedules.find(schedule ⇒ schedule.startTime <= startTime && startTime < schedule.endTime)
        currentScheduleOpt match {
          case Some(currentSchedule) ⇒ mergedChanges = mergedChanges :+ (startTime -> addScheduleSpeed(changes(index)._2, currentSchedule.enforcedSpeedLimit))
          case None                  ⇒ mergedChanges = mergedChanges :+ (startTime -> SpeedSetting(Left(SpeedIndicatorStatus.Undetermined)))
        }

        //check for schedule changes within period
        val endTime = changes(index + 1)._1

        val scheduleStarts = sortedSchedules.filter(schedule ⇒ startTime < schedule.startTime && schedule.startTime < endTime)
        for (schedule ← scheduleStarts) {
          mergedChanges = mergedChanges :+ (schedule.startTime -> addScheduleSpeed(changes(index)._2, schedule.enforcedSpeedLimit))
        }
        val scheduleEnds = sortedSchedules.filter(schedule ⇒ startTime <= schedule.endTime && schedule.endTime < endTime)
        for (schedule ← scheduleEnds) {
          //when there is a schedule start at the same time don't add event
          val startSchedule = scheduleStarts.find(s ⇒ s.startTime == schedule.endTime)
          //or if schedule is end of day and a schedule starts at the beginning
          if (startSchedule.isEmpty || (schedule.endTime == dayEnd && sortedSchedules.head.startTime != dayStart)) {
            mergedChanges = mergedChanges :+ (schedule.endTime -> SpeedSetting(Left(SpeedIndicatorStatus.Undetermined)))
          }
        }
      }
    }
    //sort the events again possible that start and end schedules are out of order
    mergedChanges = mergedChanges.sortBy(_._1)

    val prepend = if (mergedChanges.find(_._1 == dayStart).isEmpty) {
      Seq(dayStart -> SpeedSetting(Left(SpeedIndicatorStatus.Undetermined))) ++ mergedChanges
    } else {
      mergedChanges
    }

    //remove double events
    val undoubled = if (prepend.isEmpty) {
      prepend
    } else {
      prepend.tail.foldLeft(Seq(prepend.head)) { (result, record) ⇒
        {
          val last = result.last
          if (math.abs(last._1 - record._1) < 2000) { //records can be delayed a few msec when closer that 2seconds assume this delay
            //times are equal => remove first
            result.init :+ record
          } else if (areSettingsEqual(schedules, last, record)) {
            result
          } else {
            result :+ record
          }
        }
      }
    }

    val append = if (undoubled.find(_._1 >= lastMoment).isEmpty) {
      val dayLastSetting = undoubled.lastOption.map(_._2).getOrElse(prepend.head._2)
      Seq(lastMoment -> dayLastSetting)
    } else {
      Seq()
    }
    undoubled ++ append
  }

  private def addScheduleSpeed(org: SpeedSetting, scheduleSpeed: Int): SpeedSetting = {
    if (org.setting == Right(None)) {
      SpeedSetting(Right(Some(scheduleSpeed)))
    } else {
      org
    }
  }

  private def areSettingsEqual(schedules: Seq[CorridorSchedule], set1: (Long, SpeedSetting), set2: (Long, SpeedSetting)): Boolean = {
    if (set1 == set2)
      return true
    val string1 = speedSettingString(speed = set1._2,
      scheduleSpeed = scheduledSpeed(time = set1._1, schedules = schedules),
      caseFile = HHMVS14())
    val string2 = speedSettingString(speed = set2._2,
      scheduleSpeed = scheduledSpeed(time = set2._1, schedules = schedules),
      caseFile = HHMVS14())
    string1 == string2
  }

  private def scheduledSpeed(time: Long, schedules: Seq[CorridorSchedule]): Option[Int] = {
    schedules.find(cs ⇒ time >= cs.startTime && time <= cs.endTime).map(_.enforcedSpeedLimit)
  }

  private val blank = SpeedSetting(Right(None))

  private val dateFormat = new SimpleDateFormat("yyyy/MM/dd")
  dateFormat.setTimeZone(DayUtility.fileExportTimeZone)

  private val timeFormat = new SimpleDateFormat(" HH:mm:ss")
  timeFormat.setTimeZone(DayUtility.fileExportTimeZone)

  private val minimumFieldSizes = (7, 19, 11)

  private def makeDataLine(date: Long, time: Long, location: String, speed: String): String = {
    val fields = List(
      padRight(location, minimumFieldSizes._1),
      padRight(dateFormat.format(date) + formatTime(date, time), minimumFieldSizes._2),
      padRight(speed, minimumFieldSizes._3))
    fields.mkString(start = "|", sep = "|", end = "|")
  }

  private def padRight(value: String, requiredSize: Int): String = {
    if (value.length < requiredSize) {
      value + String.valueOf(Array.fill(requiredSize - value.length)(' '))
    } else {
      value
    }
  }

  private def formatTime(exportDate: Long, time: Long) = {
    val formattedTime = timeFormat.format(time)
    if (formattedTime == " 00:00:00" && time > exportDate) {
      val finalRecordTimeFormat = new SimpleDateFormat(" kk:mm:ss")
      finalRecordTimeFormat.setTimeZone(DayUtility.fileExportTimeZone)
      finalRecordTimeFormat.format(time)
    } else {
      formattedTime
    }
  }

  private def speedSettingString(speed: SpeedSetting, scheduleSpeed: Option[Int], caseFile: CaseFile) = {
    speed.setting match {
      case Right(None)                        ⇒ scheduleSpeed.map(_.toString).getOrElse("onbepaald")
      case Right(Some(speedValue))            ⇒ speedValue.toString
      case Left(SpeedIndicatorStatus.StandBy) ⇒ caseFile.mtmInfoStandBy
      case Left(_)                            ⇒ "onbepaald"
    }
  }
}

