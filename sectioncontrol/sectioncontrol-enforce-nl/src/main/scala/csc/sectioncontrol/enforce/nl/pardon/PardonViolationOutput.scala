/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.nl.pardon

import csc.sectioncontrol.enforce.common.nl.violations.Violation
import csc.sectioncontrol.enforce.nl.register.PardonedViolations

case class PardonViolationOutput(notPardonedViolations: Seq[Violation], originalPardonViolations: Seq[Violation], pardoned: Seq[PardonedViolations])