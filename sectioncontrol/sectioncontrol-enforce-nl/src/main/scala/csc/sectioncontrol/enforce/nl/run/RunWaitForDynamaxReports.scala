/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.nl.run

import collection.mutable.ListBuffer

import java.util.TimeZone
import java.util.concurrent.Executors
import java.text.SimpleDateFormat
import java.io.File

import akka.actor.ActorRef
import akka.event.LoggingReceive
import akka.util.duration._
import akka.pattern.ask
import akka.util.Duration
import akka.dispatch.{ ExecutionContext, Future }

import csc.akkautils.{ RunToken, JobComplete, JobFailed }
import csc.curator.utils.{ Curator, CuratorToolsImpl, CuratorActor }

import csc.sectioncontrol.enforce._
import speedlog.{ GeneratedExcludeEvents, ScheduledSpeed, GenerateExcludeEvents }
import csc.sectioncontrol.storage._
import csc.sectioncontrol.common.DayUtility

class RunWaitForDynamaxReports(systemId: String,
                               protected val curator: Curator,
                               storeMtmData: ActorRef,
                               timeZone: String,
                               filePickupDelay: Long = 1.minute.toMillis,
                               getSpeedLog: (String) ⇒ ActorRef,
                               getExcludeLog: (String) ⇒ ActorRef) extends CuratorActor {
  // RB: Future.sequence(futureList) needs ExecutionContext. Don't know how to get the Actor ExecutionContext
  implicit val ec = ExecutionContext.fromExecutorService(Executors.newCachedThreadPool())

  protected def receive = LoggingReceive {
    case Run(zkJobPath, jobRunner, runToken, workDate, manageStatus) ⇒ {
      val fileNames = getDynamaxRapportFiles()
      if (fileNames.isEmpty) {
        log.error("DynamaxRapport files could not be found")
        jobRunner ! JobFailed
      }
      var failure = false
      for (fileName ← fileNames) {
        val expectedFile = getExpectedFile(fileName, workDate, timeZone)
        if (expectedFile.exists() && runToken.startTime - expectedFile.lastModified() > filePickupDelay) {
          val expectedDay = workDate + 1.day.toMillis
          val (expectedDayStart, expectedDayEnd) = DayUtility.calculateDayPeriod(expectedDay)
          if (expectedFile.lastModified() >= expectedDayStart && expectedFile.lastModified() < expectedDayEnd) {
            log.info("expected file found at {}", expectedFile.getAbsolutePath)
          } else {
            val formatter = new SimpleDateFormat("yyyy-MM-dd")
            formatter.setTimeZone(TimeZone.getTimeZone(timeZone))
            val modifiedDate = formatter.format(expectedFile.lastModified())
            val expectedDate = formatter.format(expectedDay)
            log.error("Received file {} modified date {} does not match expected date {}", expectedFile.getAbsolutePath, modifiedDate, expectedDate)
            failure = true
          }
        } else {
          log.info("expected file {} not yet present, or modified too recently", expectedFile.getAbsolutePath)
          // RunFailed with rerun=true triggers running this again in 1 minute
          failure = true
        }
      }
      if (failure) {
        jobRunner ! JobFailed(retry = true)
      } else {
        //todo: RB This should not be a task for loadMTM or the check for dynamax report files.
        //todo: RB But is too late in test phase to completely refactor this part
        //Timeout is due to previous comment also hardcoded
        //create excludeEventLog
        createExcludeLog(jobRunner, runToken, workDate, manageStatus, 10.seconds)
      }
    }
    case GeneratedExcludeEvents ⇒ {
      log.warning("Received unexpected GeneratedExcludeEvents message")
    }
  }

  private def createExcludeLog(jobRunner: ActorRef, runToken: RunToken, workDate: Long, manageStatus: ActorRef, duration: Duration) {
    val (dayStart, dayEnd) = DayUtility.calculateDayPeriod(workDate)
    val corridorIds = getCorridorIds
    val futureList = new ListBuffer[Future[Any]]

    val systemCfg = curator.get[ZkSystem](Paths.Systems.getConfigPath(systemId))
    val pardonCfg = systemCfg.map(_.pardonTimes).getOrElse(ZkSystemPardonTimes())

    corridorIds.foreach { corridorId ⇒
      val corridor = curator.get[ZkCorridor](Paths.Corridors.getConfigPath(systemId, corridorId))
      corridor.foreach { corridorConfig ⇒
        val technicalPardonTime_mSecs = pardonCfg.pardonTimeTechnical_secs * 1000L
        val esaPardonTime_mSecs = pardonCfg.pardonTimeEsa_secs * 1000L
        val scheduleSpeeds = LoadSystemConfiguration.
          getCorridorSchedules(systemId, corridorId, dayStart, dayEnd, TimeZone.getTimeZone(timeZone), curator).
          map(s ⇒ ScheduledSpeed(s.startTime, s.endTime, s.enforcedSpeedLimit, s.supportMsgBoard))
        //        futureList += getSpeedLog(systemId).ask(GenerateExcludeEvents(dayStart,
        val speed = getSpeedLog(systemId)
        futureList += speed.ask(GenerateExcludeEvents(dayStart,
          dayEnd,
          corridorConfig.info.corridorId,
          technicalPardonTime_mSecs,
          esaPardonTime_mSecs,
          getExcludeLog(systemId),
          scheduleSpeeds))(duration)
      }
    }

    val futuresSequence = Future.sequence(futureList)
    //wait for all replies
    futuresSequence.onComplete {
      case Right(results) ⇒ {
        //collect results
        manageStatus ! ProcessingMtmCompleted(workDate, runToken.startTime)
        jobRunner ! JobComplete
      }
      case Left(e) ⇒ {
        //failure
        for (pos ← 0 until futureList.size) {
          val future = futureList(pos)
          future.value match {
            case Some(Right(singleResult)) ⇒ //request was successful
            case Some(Left(singleExc)) ⇒ {
              val corridor = corridorIds(pos)
              log.warning("Could not generate exclude log for corridor %s".format(corridor))
            }
            case None ⇒ {
              val corridor = corridorIds(pos)
              log.warning("Could not generate exclude log for corridor %s".format(corridor))
            }
          }
        }
        jobRunner ! JobFailed
      }
    }
  }

  private def getCorridorIds: Seq[String] = {
    val corridorPaths = curator.getChildren(Paths.Corridors.getDefaultPath(systemId))
    corridorPaths.map { _.split('/').last }

  }

  private def getDynamaxRapportFiles(): Seq[String] = {
    val ifacePath = Paths.Configuration.getDynamaxIfacePath
    val listIface = curator.get[List[ZkDynamaxIface]](ifacePath).getOrElse(List())

    getCorridorIds.foldLeft(List[String]()) {
      (list, corridorId) ⇒
        val corridor = curator.get[ZkCorridor](Paths.Corridors.getConfigPath(systemId, corridorId))
        val dynamaxId = corridor.map(_.dynamaxMqId).getOrElse("")
        listIface.find(_.id == dynamaxId).map(_.reportFile :: list).getOrElse(list)
    }
  }

  /**
   * Create filePath
   * @param fileNamePattern Example fileNamePattern <rootDir>/<dynamaxId>/%sdynamax.txt
   * @param workDate
   * @param timeZone
   * @return Expected File
   */
  private def getExpectedFile(fileNamePattern: String, workDate: Long, timeZone: String): File = {
    val df = new SimpleDateFormat(fileNamePattern)
    df.setTimeZone(TimeZone.getTimeZone(timeZone))
    val fileName = df.format(workDate)
    new File(fileName)
  }

}

