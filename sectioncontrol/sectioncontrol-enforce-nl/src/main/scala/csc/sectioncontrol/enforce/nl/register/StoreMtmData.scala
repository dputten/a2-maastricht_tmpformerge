/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.nl.register

import collection.mutable

import java.io.File
import java.text.{ ParseException, SimpleDateFormat }
import java.util.{ Date, Calendar, GregorianCalendar }

import org.apache.commons.io.FileUtils

import akka.event.LoggingReceive

import csc.sectioncontrol.enforce.{ ZkDynamaxIface, Cleanup }
import csc.sectioncontrol.storage.{ ZkCorridor, Paths }
import csc.curator.utils.{ Curator, CuratorActor }
import csc.sectioncontrol.common.DayUtility
import csc.sectioncontrol.common
import csc.sectioncontrol.storagelayer.hbasetables.MtmFileTable

/**
 * Actor that storing and retrieving MTM data.
 *
 * Accepted messages are LogMTMInfo, LogMTMRaw and Cleanup.
 *
 * @author Maarten Hazewinkel
 */
class StoreMtmData(val systemId: String,
                   mtmDataStore: MtmFileTable.Store,
                   protected val curator: Curator) extends CuratorActor {

  import StoreMtmData._

  def receive = LoggingReceive {
    case LogMTMInfo(day, corridorId, data) ⇒
      writeRawData(corridorId, day, data, "mtmInfo")

    case LogMTMRaw(day, corridorId, data) ⇒
      writeRawData(corridorId, day, data, "mtmRaw")

    case Cleanup(retentionDays) ⇒
      cleanupDayData(retentionDays)
  }

  private def writeRawData(corridorId: Int, day: Long, data: Array[Byte], key: String) {
    val storageKey: String = buildMTMStoreKey(systemId, corridorId, day, key)
    mtmDataStore.writeRow(storageKey, data)
    val path = buildCorridorPath(systemId, corridorId, day) + "/" + key
    // erase and overwrite any previous version
    curator.safeDelete(path)
    curator.put(path, WrappedString(storageKey))
  }

  private def cleanupDayData(retentionDays: Int) {
    val cal = new GregorianCalendar(DayUtility.fileExportTimeZone)
    cal.setTimeInMillis(System.currentTimeMillis())
    cal.add(Calendar.DATE, -1 * (retentionDays + 1))
    val lastDeleteDay = cal.getTimeInMillis
    val basePath = buildRegistrationPath(systemId)

    curator.getChildren(basePath).
      foreach(datePath ⇒ {
        parseDate(datePath.split('/').last).foreach(date ⇒ if (date < lastDeleteDay) curator.deleteRecursive(datePath))
      })
  }
}

object StoreMtmData {

  private def dayFormatter = {
    val dayFormat = new SimpleDateFormat("yyyy-MM-dd")
    dayFormat.setTimeZone(common.DayUtility.fileExportTimeZone)
    dayFormat
  }

  private def buildRegistrationPath(systemId: String): String = {
    Paths.Systems.getSystemJobsPath(systemId) + "/registration"
  }

  private def formatDay(day: Long): String = {
    dayFormatter.format(new Date(day))
  }

  private def parseDate(dateString: String): Option[Long] =
    try {
      Some(dayFormatter.parse(dateString).getTime)
    } catch {
      case _: ParseException ⇒ None
    }

  private[register] def buildDayPath(systemId: String, day: Long): String = {
    buildRegistrationPath(systemId) + "/" + formatDay(day)
  }

  private def buildCorridorPath(systemId: String, corridorId: Int, day: Long): String = {
    buildDayPath(systemId, day) + "/" + corridorId.toString
  }

  private def buildMTMStoreKey(systemId: String, corridorId: Int, day: Long, key: String): String = {
    buildCorridorPath(systemId, corridorId, day) + "/" + key
  }
}

trait ReadMtmData {
  selftype: CuratorActor ⇒

  import StoreMtmData.buildDayPath

  private[register] def readRawData(systemId: String, day: Long, key: String, mtmDataStore: MtmFileTable.Store): Map[Int, Array[Byte]] = {
    val dayPath = buildDayPath(systemId, day)
    curator.getChildren(dayPath).flatMap(corridorPath ⇒ {
      curator.get[WrappedString](corridorPath + "/" + key).flatMap(wrappedKey ⇒
        mtmDataStore.readRow(wrappedKey.string).map(rawData ⇒
          (corridorPath.split('/').last.toInt, rawData)))
    }).toMap
  }

  /**
   * Dynamax Files should also be stored in Hbase just like MTM info
   * But for now just use the fileSystem
   * @param systemId System id
   * @param day Date
   * @return
   */
  private[register] def readDynamaxData(systemId: String, day: Long): Map[Int, Array[Byte]] = {
    val corridorPaths = curator.getChildren(Paths.Corridors.getDefaultPath(systemId))
    val corridors = corridorPaths.map { _.split('/').last }

    val ifacePath = Paths.Configuration.getDynamaxIfacePath
    val listIface = curator.get[List[ZkDynamaxIface]](ifacePath).getOrElse(List())

    val output = new mutable.HashMap[Int, Array[Byte]]()
    for (corridorId ← corridors) {
      curator.get[ZkCorridor](Paths.Corridors.getConfigPath(systemId, corridorId)) match {
        case Some(corridor) ⇒ {
          val reportFilePattern = listIface.find(_.id == corridor.dynamaxMqId).map(_.reportFile).getOrElse("yyyyMMdd'Dynamax.txt'")
          val reportFile = (new SimpleDateFormat(reportFilePattern)).format(new Date(day))

          val dynamaxFile = new File(reportFile)
          if (dynamaxFile.exists()) {
            output += corridor.info.corridorId -> FileUtils.readFileToByteArray(dynamaxFile)
          } else {
            log.warning("Could not find dynamax File %s".format(dynamaxFile.getAbsolutePath))
          }
        }
        case None ⇒ log.warning("Could not find corridor %s".format(Paths.Corridors.getConfigPath(systemId, corridorId)))
      }
    }
    output.toMap
  }
}

/**
 * Message to save the raw speed indicator data for a day.
 *
 * @param day A time during the day for which the data is
 * @param corridorId The corridor to which the MTM data applies
 * @param data The raw file contents
 */
case class LogMTMRaw(day: Long, corridorId: Int, data: Array[Byte])

/**
 * Message to save the processed speed indicator data for a day.
 *
 * @param day A time during the day for which the data is
 * @param corridorId The corridor to which the MTM data applies
 * @param data The raw file contents
 */
case class LogMTMInfo(day: Long, corridorId: Int, data: Array[Byte])

/**
 * Wrapper to enable storing strings in zookeeper
 */
private[register] case class WrappedString(string: String)
