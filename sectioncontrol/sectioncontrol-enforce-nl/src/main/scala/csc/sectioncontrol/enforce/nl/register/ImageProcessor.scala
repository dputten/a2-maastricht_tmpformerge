package csc.sectioncontrol.enforce.nl.register

import java.util.Date

import akka.event.LoggingAdapter
import csc.curator.utils.Curator
import csc.sectioncontrol.decorate.{ DecorateService, ImageAndContents }
import csc.sectioncontrol.enforce.common.nl.services.Service
import csc.sectioncontrol.enforce.common.nl.services.Service.ImageRef
import csc.sectioncontrol.enforce.common.nl.violations.CorridorData
import csc.sectioncontrol.messages.VehicleImageType.VehicleImageType
import csc.sectioncontrol.messages._
import csc.sectioncontrol.sign.Signing
import csc.sectioncontrol.storage.Decorate.DecorationType
import csc.sectioncontrol.storagelayer.corridor.CorridorService
import csc.sectioncontrol.storagelayer.hbasetables.VehicleImageTable

/**
 * Created by carlos on 22.07.15.
 */
trait ImageProcessor {

  type VehicleRef = (Long, String, String) //eventTimestamp, gantry, lane

  private[register] def systemId: String
  private[register] def log: LoggingAdapter
  //multiple image readers based on the VehicleImageType
  private[register] def violationImageReaders: Map[VehicleImageType.Value, VehicleImageTable.Reader]
  protected def curator: Curator

  private[register] def publishSystemEvent(event: SystemEvent)

  private val decorateService = createDecorateService()

  /**
   * Decides and retrieves the first and second image from storage based on the service type of a corridor
   * @param entry metadata of first entry
   * @param exit optional metadata of second entry
   * @param corridor corridor data
   * @return a tuple that holds two values. Both are of optional byte arrays types.
   * The first optional byte array contains the data of the entryImage (based on the given service type)
   * The second optional byte array contains the data of the exitImage (also based on the given service type)
   */
  def retrieveImages(entry: VehicleMetadata,
                     exit: Option[VehicleMetadata],
                     corridor: CorridorData): (Option[Array[Byte]], Option[Array[Byte]]) = {

    val service: Service = corridor.service //service type of a corridor
    //The retrieval of each vehicle image is based on the given service type

    val first = getImage(service.getImage1Ref(entry, exit), corridor)
    val second = getImage(service.getImage2Ref(entry, exit), corridor)

    log.info("firstImage data is retrieved == " + first.isDefined)
    log.info("secondImage data is retrieved == " + second.isDefined)

    (first, second)
  }

  protected def createDecorateService(): DecorateService = new DecorateService(curator, systemId)

  private def getImage(ref: ImageRef, corridor: CorridorData): Option[Array[Byte]] = {

    var vehicleRef: VehicleRef = null

    val res: ImageAndContents = ref match {
      case Some((tp, msg)) ⇒ {
        vehicleRef = getVehicleRef(msg)

        requiresDecoration(msg) match {
          case true ⇒ {
            val decorationType = DecorationType.getTargetDecorationType(tp)
            val sourceImageType = Service.getOriginalImageType(tp)
            val sourceImage = loadImage(msg, sourceImageType) //load source image
            //validate source image
            validateImage(vehicleRef, sourceImage) match {
              case Some(_) ⇒ {
                val imageData = decorateService.getDecoratedImage(msg, decorationType, sourceImage,
                  getCorridorId(corridor.corridorId), corridor.compressionFactor)
                ImageAndContents(None, imageData)
              }
              case None ⇒ ImageAndContents.Empty
            }
          }
          case false ⇒ ImageAndContents(img = msg.getImage(tp)) //legacy message: only the metadata now, data loaded later
        }
      }
      case None ⇒ ImageAndContents.Empty
    }

    res match {
      case ImageAndContents(_, Some(_)) ⇒ {
        //image data is already available and was validated before
        res.contents
      }
      case ImageAndContents(Some(img), None) ⇒ {
        //no image data is loaded yet
        val vehicleRef = getVehicleRef(ref.get._2)
        val data = loadImageContents(vehicleRef, img.imageType) //load contents
        validateImage(vehicleRef, res.copy(contents = data)) //validate
      }
      case _ ⇒ None
    }
  }

  protected def getCorridorId(infoId: Int): Option[String] =
    CorridorService.getCorridorIdMapping(curator, systemId, infoId).map(_.corridorId)

  private def requiresDecoration(msg: VehicleMetadata): Boolean = msg.version match {
    case Some(version) if (version >= MetadataVersion.withLicensePlateData.id) ⇒ true
    case _ ⇒ false
  }

  private def getVehicleRef(msg: VehicleMetadata): VehicleRef = (msg.eventTimestamp, msg.lane.gantry, msg.lane.name)

  private def loadImage(msg: VehicleMetadata, imageType: VehicleImageType): ImageAndContents = {

    val ref = getVehicleRef(msg)
    val image = msg.getImage(imageType)
    val data = loadImageContents(ref, imageType)
    ImageAndContents(image, data)
  }

  /**
   * Retrieve an image from storage
   * @param ref identifies the vehicle
   * @param imageType vehicle image type
   * @return optional byte array with image contents
   */
  protected def loadImageContents(ref: VehicleRef, imageType: VehicleImageType): Option[Array[Byte]] = {
    val (time, gantry, lane) = ref
    //retrieve the storage image reader given the right VehicleImageType
    log.debug("number of image readers = " + violationImageReaders.size)
    val reader = violationImageReaders.find(_._1.toString == imageType.toString).map(_._2)
    reader match {
      case Some(imageReader) ⇒
        val key = (systemId + "-" + gantry + "-", time, "-" + lane)
        log.debug("retrieving image with key => " + key)
        imageReader.readRow((systemId + "-" + gantry + "-", time, "-" + lane))
      case _ ⇒
        log.debug("No image reader found")
        None
    }
  }

  /**
   * Validates the hash of given imageData (if any), return it if valid
   * @param ref
   * @param image image metadata and contents
   * @return given contents, if present and valid
   */
  private def validateImage(ref: VehicleRef, image: ImageAndContents): Option[Array[Byte]] = {
    image match {
      case ImageAndContents(Some(img), Some(_)) ⇒ {
        val (time, gantry, lane) = ref
        val expectedHash = img.checksum
        val realHash = Signing.calculateHash(image.contents)
        log.debug("expectedHash == realHash => " + (expectedHash == realHash))

        if (expectedHash == realHash) {
          image.contents
        } else {
          log.error("Invalid hash for image for lane " + gantry + ":" + lane + " at time " + time + ". Expected " + expectedHash + " received " + realHash)
          val logMsg = "Validatie van foto checksum is gefaald voor passage van op rijbaan " + gantry + ":" + lane + " om " + new Date(time).toString
          publishSystemEvent(SystemEvent(RegisterViolations.componentId, System.currentTimeMillis(), systemId, "Info", "system", Some(logMsg)))
          None
        }
      }
      case _ ⇒ None
    }
  }

}
