/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.nl.classify

import csc.sectioncontrol.messages._
import csc.akkautils.DirectLogging
import csc.sectioncontrol.storage.MultiCorridorClassifyConfig
import scala.Some
import csc.sectioncontrol.messages.VehicleClassifiedRecord
import csc.sectioncontrol.storage.MultiCorridorClassifyConfig

trait MultiCorridorProcessor {

  def checkClassifications(classifications: Seq[VehicleClassifiedRecord]): Seq[VehicleClassifiedRecord]

}

/**
 * Class to be able to correct classifications by using multiple records from other corridors.
 *
 * @param config
 */
class MultiCorridorClassify(config: MultiCorridorClassifyConfig) extends MultiCorridorProcessor with DirectLogging {

  val CAR = VehicleCategory.Car.toString
  val VAN = VehicleCategory.Van.toString
  val CAR_TRAILER = VehicleCategory.CarTrailer.toString
  val UNKNOWN = VehicleCategory.Unknown.toString

  val DAMBORD_P = "P"
  val DAMBORD_LB = "LB"

  /**
   * Group the classifications by license and pass-through
   * And apply the business rules to correct classifications
   * @param classifications
   * @return
   */
  def checkClassifications(classifications: Seq[VehicleClassifiedRecord]): Seq[VehicleClassifiedRecord] = {
    if (!config.enable) {
      return classifications
    }
    val groupedByLicense = classifications.groupBy(_.licensePlate)

    val cor = groupedByLicense.map {
      case (license, records) ⇒ {
        //are there records we maybe can correct
        val updateRecords = records.filter(rec ⇒ {
          val dambord = rec.dambord.getOrElse("")
          rec.indicator == ProcessingIndicator.Manual && !rec.manualAccordingToSpec && (dambord == DAMBORD_P || dambord == DAMBORD_LB)
        })
        if (updateRecords.isEmpty) {
          records
        } else {
          //do we have double/multiple registrations.
          val listOfGroups = groupByTime(records)
          listOfGroups.flatMap(correctClassification)
        }
      }
    }
    val result = cor.flatten.toSeq
    log.info("CheckClassifications returned %d records".format(result.size))
    result
  }

  /**
   * Check the rules if we can correct classification Records. And return all the
   * classifications the corrected and the other classifications
   * Only AS with dambord P or LB can be overruled by
   * Car/Van with confidence >= 70
   * multiple Car/Van with at least one confidence > 0
   *
   * And there are also special special rules for when there are only two corridor results
   * Car/Van with confidence >= 50 or higher can overrule CarWithTrailer with confidence < 30
   * Car/Van with confidence > 0 can overrule None
   *
   * @param classifications
   * @return
   */
  def correctClassification(classifications: Seq[VehicleClassifiedRecord]): Seq[VehicleClassifiedRecord] = {
    val (updateRecords, restRecords) = classifications.partition(rec ⇒ {
      val dambord = rec.dambord.getOrElse("")
      rec.indicator == ProcessingIndicator.Manual && !rec.manualAccordingToSpec && (dambord == DAMBORD_P || dambord == DAMBORD_LB)
    })
    if (updateRecords.size == 0 || classifications.size == 1) {
      classifications
    } else {
      val reliableRecord = findReliableClassification(classifications)
      if (reliableRecord.isDefined) {
        //update records with classification of reliable record
        restRecords ++ updateClassificationRecords(updateRecords, reliableRecord.get)
      } else {
        val sameRecords = findSameClassification(classifications)
        if (sameRecords.size > 1) {
          //update indication to automatic only when other records are empty or there are the same number of automatic as
          //there are notempty records
          val notEmptyCategory = classifications.filter(clas ⇒ !sameRecords.contains(clas) && clas.speedRecord.category.isDefined)
          val autoRecords = sameRecords.filter(_.indicator == ProcessingIndicator.Automatic)
          val canBeAutomatic = canBeMovedToAutomatic(sameRecords)
          val exampleRecord = if (notEmptyCategory.size > autoRecords.size || !canBeAutomatic) {
            sameRecords.head.copy(indicator = ProcessingIndicator.Manual)
          } else {
            sameRecords.head.copy(indicator = ProcessingIndicator.Automatic, reason = IndicatorReason(0))
          }
          //update records with classification of reliable record
          restRecords ++ updateClassificationRecords(updateRecords, exampleRecord)
        } else {
          //check for less reliable classifications
          val lessReliable = classifications.filter(rec ⇒ {
            rec.speedRecord.exit.flatMap(_.category).exists(cat ⇒ cat.confidence == 0 && (cat.value == CAR || cat.value == VAN))
          })
          if (lessReliable.size > 0) {
            //update
            correctLessReliableClassification(classifications, lessReliable)
          } else if (classifications.size == 2) {
            correct2Classification(classifications.head, classifications.last)
          } else {
            classifications
          }
        }
      }
    }
  }

  def canBeMovedToAutomatic(sameRecords: Seq[VehicleClassifiedRecord]): Boolean = {
    if (sameRecords.head.manualAccordingToSpec) {
      return false
    }
    val reliableRecord = sameRecords.find(rec ⇒ !rec.reason.isUnreliableCountry() && !rec.reason.isUnreliableLicense())
    reliableRecord.isDefined
  }

  def correctLessReliableClassification(classifications: Seq[VehicleClassifiedRecord], lessReliable: Seq[VehicleClassifiedRecord]): Seq[VehicleClassifiedRecord] = {
    //1 => has classification other than car or Van (AS manual)
    //2 => hos no classification (AS manual)
    //6 => has Car or Van as classification with confidence 0, and entry length is > 6 (PA/CA manual)
    //7 => has Car or Van as classification with confidence 0, and entry length is < 6 (PA/CA automatic)

    val notCategoryEmpty = classifications.filter(rec ⇒ {
      rec.speedRecord.exit.flatMap(_.category).exists(cat ⇒ (cat.value != CAR && cat.value != VAN))
    })

    if (lessReliable.head.indicator == ProcessingIndicator.Manual) {
      //less reliable is of type 6
      if (notCategoryEmpty.size > lessReliable.size) {
        //116 => don't update
        classifications
      } else {
        //126 => update 12 using 6
        //226 => update 22 using 6
        //16 => update 1 using 6
        //26 => update 1 using 6
        val update = classifications.filterNot(rec ⇒ {
          rec.speedRecord.exit.flatMap(_.category).exists(cat ⇒ cat.confidence == 0 && (cat.value == CAR || cat.value == VAN))
        })
        lessReliable ++ updateClassificationRecords(update, lessReliable.head)
      }
    } else {
      if (notCategoryEmpty.size >= lessReliable.size) {
        //117 => update 117 using 7 but indication is set to manual
        //17 =>  update 17 using 7 but indication is set to manual
        //127 => update 127 using 7 but indication is set to manual
        val example = lessReliable.head.copy(indicator = ProcessingIndicator.Manual)
        updateClassificationRecords(classifications, example)
      } else {
        //227 => update 22 using 7
        //27 => update 1 using 7
        val update = classifications.filterNot(rec ⇒ {
          rec.speedRecord.exit.flatMap(_.category).exists(cat ⇒ cat.confidence == 0 && (cat.value == CAR || cat.value == VAN))
        })
        lessReliable ++ updateClassificationRecords(update, lessReliable.head)
      }
    }
  }

  /**
   * Implement the special rules for when there are only two corridor results
   * Car/Van with confidence >= 50 or higher can overrule CarWithTrailer with confidence < 30
   * Car/Van with confidence > 0 can overrule None
   * @param cl1
   * @param cl2
   * @return
   */
  def correct2Classification(cl1: VehicleClassifiedRecord, cl2: VehicleClassifiedRecord): Seq[VehicleClassifiedRecord] = {
    val (updateCandidate, otherRecord) = {
      val dambord = cl1.dambord.getOrElse("")
      if (cl1.code == Some(VehicleCode.AS) && (dambord == DAMBORD_P || dambord == DAMBORD_LB)) {
        (cl1, cl2)
      } else {
        (cl2, cl1)
      }
    }
    val categoryCandidate = updateCandidate.speedRecord.exit.flatMap(_.category).map(_.value).getOrElse(UNKNOWN)

    val categoryRecord = otherRecord.speedRecord.exit.flatMap(_.category)
    val valueCategoryRecord = categoryRecord.map(_.value).getOrElse("")
    val confCategoryRecord = categoryRecord.map(_.confidence).getOrElse(0)
    if (categoryCandidate == UNKNOWN &&
      (valueCategoryRecord == CAR || valueCategoryRecord == VAN) &&
      (confCategoryRecord > 0)) {
      updateClassificationRecords(Seq(updateCandidate), otherRecord) :+ otherRecord
    } else {
      Seq(updateCandidate, otherRecord)
    }
  }

  /**
   * update the classifications with the example values
   * @param updateRecords
   * @param example
   * @return
   */
  def updateClassificationRecords(updateRecords: Seq[VehicleClassifiedRecord], example: VehicleClassifiedRecord): Seq[VehicleClassifiedRecord] = {
    log.info("Update classification of %s with %s".format(updateRecords, example))
    updateRecords.map(_.copy(
      code = example.code,
      indicator = example.indicator,
      alternativeClassification = example.alternativeClassification,
      manualAccordingToSpec = example.manualAccordingToSpec,
      reason = example.reason))
  }

  /**
   * Find classifications which are the same and can overrule the classifications
   * @param classifications
   * @return
   */
  def findSameClassification(classifications: Seq[VehicleClassifiedRecord]): Seq[VehicleClassifiedRecord] = {
    val vanCars = classifications.filter(rec ⇒ {
      rec.speedRecord.exit.flatMap(_.category).exists(cat ⇒ cat.value == CAR || cat.value == VAN)
    })
    if (vanCars.size > classifications.size / 2) {
      //check if result is the same
      val (head, isSame) = vanCars.tail.foldLeft(vanCars.head, true) {
        case ((head, result), current) ⇒ {
          val code = head.code == current.code
          val altCode = head.alternativeClassification == current.alternativeClassification
          (head, result && code && altCode)
        }
      }
      if (isSame) {
        vanCars
      } else {
        Seq()
      }
    } else {
      Seq()
    }
  }

  /**
   * Return a classification with indication Automatic
   * @param classifications
   * @return
   */
  def findReliableClassification(classifications: Seq[VehicleClassifiedRecord]): Option[VehicleClassifiedRecord] = {
    classifications.find(rec ⇒ {
      rec.speedRecord.exit.flatMap(_.category).exists(cat ⇒ cat.confidence >= config.minReliableCar && (cat.value == CAR || cat.value == VAN))
    })
  }

  /**
   * do we have double/multiple registrations.
   * same section have multiple records. Order sections?
   * are the records within the maxTime
   * @param classifications
   * @return
   */
  def groupByTime(classifications: Seq[VehicleClassifiedRecord]): Seq[Seq[VehicleClassifiedRecord]] = {
    val sorted = classifications.sortBy(_.time)
    val head = sorted.head
    val (last, all) = sorted.tail.foldLeft(Seq[VehicleClassifiedRecord](head), Seq[Seq[VehicleClassifiedRecord]]()) {
      case ((currentGroup, result), currentRecord) ⇒ {
        //start new group when time exceed maxTime or corridor is already in the group
        //otherwise aad record to group
        val findCorridor = currentGroup.find(_.speedRecord.corridorId == currentRecord.speedRecord.corridorId)
        if (findCorridor.isDefined) {
          //create new group
          (Seq(currentRecord), result :+ currentGroup)
        } else {
          //check maxTime
          val startTime = currentGroup.head.time
          if ((currentRecord.time - startTime) > config.minimumViolationTimeSeparation) {
            //create new group
            (Seq(currentRecord), result :+ currentGroup)
          } else {
            //add to group
            (currentGroup :+ currentRecord, result)
          }
        }
      }
    }
    all :+ last
  }
}