/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.nl.run

import akka.actor.{ Cancellable, ActorRef }
import akka.util.duration._
import java.util.TimeZone
import csc.sectioncontrol.mtm.nl.ProcessMtmData
import csc.config.Path
import akka.event.LoggingReceive
import csc.sectioncontrol.storage._
import csc.sectioncontrol.messages.{ StatsDuration, SystemEvent }
import csc.curator.utils.{ CuratorActor, Curator }
import csc.sectioncontrol.enforce.nl.EnforceNLConfig
import csc.sectioncontrol.enforce.nl.register.RegisterViolations
import csc.sectioncontrol.common.DayUtility
import akka.util.Duration
import csc.sectioncontrol.enforce.{ ExportViolationsStatistics, DayViolationsStatistics }
import csc.sectioncontrol.storagelayer.corridor.CorridorService
import csc.sectioncontrol.storagelayer.state
import csc.sectioncontrol.storagelayer.state.StateService

/**
 *
 * @author Maarten Hazewinkel
 */
class AlertForOverdueEnforceStep(manageEnforceStatus: ActorRef,
                                 timezone: TimeZone,
                                 systemId: String,
                                 enforceConfig: EnforceNLConfig,
                                 protected val curator: Curator) extends CuratorActor {

  import AlertForOverdueEnforceStep._

  private val defaultMtmFileDueDelay_mSecs = 3.hours.toMillis

  private val defaultViolationExportDueDelay_mSecs = 4.hours.toMillis

  private val maxMtmFileDelay_mSecs = enforceConfig.maxMtmFileDelay.getOrElse(defaultMtmFileDueDelay_mSecs)
  private val maxViolationExportDelay_mSecs = enforceConfig.maxViolationExportDelay.getOrElse(defaultViolationExportDueDelay_mSecs)

  private val maxDailyReportDelay_mSecs = enforceConfig.maxDailyReportDelay.getOrElse(8.hours.toMillis)

  protected def receive = LoggingReceive {
    case Tick ⇒
      log.debug("received Tick")
      sendClockTick()

    case ClockTick(time) ⇒ {
      val day = getDayStart(time)
      if (state.day != day) {
        state = AlertState(day)
      }
      if (isSystemEnabled) {
        checkTime = time
        log.debug("received ClockTime({}). Checking for overdue mtm file", time)
        if (time - day > maxMtmFileDelay_mSecs) {
          manageEnforceStatus ! IsMtmProcessingCompleted(day - 1.hour.toMillis)
        }
      } else {
        log.warning("received ClockTime({}). System is not enabled. Skipping checks", time)
      }
      checkForDailyReport(time)
    }
    case MtmProcessingStatus(processed, prevDay, processingTime) ⇒
      if (!processed) {
        if (!state.mtmFileAlertSet) {
          val durationString = Duration(maxMtmFileDelay_mSecs, "millis").printHMS
          val errorMsg = if (isDynamax()) {

            "Dynamax dag raportage niet ontvangen binnen "
          } else {
            "ESA-bestand niet ontvangen binnen "
          } + durationString + "."
          log.error(errorMsg)
          context.system.eventStream.publish(SystemEvent(ProcessMtmData.componentId, checkTime, systemId, "ESA-FILE-OVERDUE", "system", Some(errorMsg)))
          state = state.copy(mtmFileAlertSet = true)
        }
      } else {
        if (checkTime - processingTime > maxViolationExportDelay_mSecs) {
          manageEnforceStatus ! IsRegistrationCompleted(prevDay)
        }
      }

    case RegistrationStatus(registered, time) ⇒
      if (!registered) {
        if (!state.exportViolationsAlertSet) {
          val durationString = Duration(maxViolationExportDelay_mSecs, "millis").printHMS

          val errorMsg = if (isDynamax()) {
            "Overtredingen Export is niet afgerond binnen " + durationString + " na Dynamax dag raportage is ontvangen."
          } else {
            "Overtredingen Export is niet afgerond binnen " + durationString + " na MTM Ruw bestand is ontvangen."
          }
          log.error(errorMsg)
          context.system.eventStream.publish(SystemEvent(RegisterViolations.componentId, checkTime, systemId, "ESA-EXPORT-OVERDUE", "system", Some(errorMsg)))
          state = state.copy(exportViolationsAlertSet = true)
        }
      }
  }

  private var state = AlertState(getDayStart(System.currentTimeMillis()))

  private def getDayStart(dateTime: Long): Long = DayUtility.calculateDayPeriod(dateTime)._1

  private var tickSchedule: Option[Cancellable] = None
  private var checkTime: Long = 0

  private def checkForDailyReport(time_ms: Long) {
    val durationDay = (time_ms - state.day).millis
    log.debug("checkForDailyReport time since daystart %s: dailyReportCreated: %s".format(durationDay.printHMS, state.dailyReportCreated))
    if (durationDay.toMillis > maxDailyReportDelay_mSecs && !state.dailyReportCreated) {
      log.debug("Check if DailyReport is created")
      val key = StatsDuration.yesterday(timezone, state.day)
      val reportPath = Path(Paths.Systems.getReportPath(systemId, key))
      if (curator.exists(reportPath)) {
        log.info("DailyReport is already created for %s".format(key.key))
      } else {
        log.info("DailyReport is not created: Requesting creation of empty report for %s".format(key.key))
        val corridorIds = CorridorService.getCorridorIdMapping(curator, systemId)
        val exportStats = corridorIds.map(mapping ⇒ ExportViolationsStatistics(corridorId = mapping.infoId,
          speedLimit = 0,
          auto = 0,
          manual = 0,
          mobi = 0,
          mobiDoNotProcess = 0,
          mtmPardon = 0,
          doublePardon = 0,
          otherPardon = 0,
          manualAccordingToSpec = 0))
        val stats = DayViolationsStatistics(systemId, key, exportStats)
        context.system.eventStream.publish(stats)
      }
      state = state.copy(dailyReportCreated = true)
    }
  }

  override def preStart() {
    super.preStart()
    tickSchedule = Some(context.system.scheduler.schedule(5.minutes, 1.minute, self, Tick))
  }

  override def postStop() {
    tickSchedule.foreach(_.cancel())
    tickSchedule = None
    super.postStop()
  }
  private def sendClockTick() {
    self ! ClockTick(System.currentTimeMillis())
  }

  private def isSystemEnabled: Boolean = {
    val currentState = StateService.getCurrentSystemState(systemId, curator)
    currentState.state != "Off"
  }

  private def isDynamax(): Boolean = {
    val system = curator.get[ZkSystem](Path(Paths.Systems.getConfigPath(systemId)))
    system.map(_.isDynamax).getOrElse(false)
  }
}

object AlertForOverdueEnforceStep {
  private case object Tick

  private[run] case class ClockTick(time: Long)

  private case class AlertState(day: Long, mtmFileAlertSet: Boolean = false, exportViolationsAlertSet: Boolean = false, dailyReportCreated: Boolean = false)
}

