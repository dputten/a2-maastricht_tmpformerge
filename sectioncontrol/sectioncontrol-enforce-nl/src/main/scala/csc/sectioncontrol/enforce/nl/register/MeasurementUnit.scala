/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.nl.register

import csc.sectioncontrol.enforce.common.nl.violations.Violation
import csc.sectioncontrol.storage.SelfTestResult
import org.slf4j.LoggerFactory
import csc.sectioncontrol.messages.certificates.LocationCertificate
import csc.sectioncontrol.storagelayer.corridor.CorridorIdMapping

object MeasurementUnit {

  /**
   * The violations must have the same serial number as the calibration results
   * @param violations all the violations to be checked
   * @param corridorSelfTests map of SelftestResults grouped by corridorId
   * @return (correct violations, violations with wrong measurementUnit)
   */
  def filterWrongUnits(violations: Seq[Violation], corridorSelfTests: Map[CorridorIdMapping, Seq[SelfTestResult]]): (Seq[Violation], Seq[Violation]) = {
    val corridorTestPeriods = corridorSelfTests.map { case (id, selfTestList) ⇒ id.infoId -> createCorrectSelfTestPeriods(selfTestList) }

    val corridorViolations = violations.groupBy(_.corridorData.corridorId)

    val results = corridorViolations.map {
      case (infoId, violationList) ⇒ {
        corridorTestPeriods.get(infoId) match {
          case Some(testPeriods) ⇒ {
            violationList.partition(vio ⇒ {
              val period = testPeriods.find(p ⇒
                p.systemId == vio.vvr.classifiedRecord.speedRecord.entry.lane.system &&
                  p.from <= vio.time && vio.time <= p.to)
              period.exists(_.serialNumber == vio.vvr.classifiedRecord.speedRecord.entry.serialNr)
            })
          }
          case None ⇒ {
            (Seq(), violationList)
          }
        }
      }
    }
    (results.keySet.flatten.toSeq, results.values.flatten.toSeq)
  }

  def createCorrectSelfTestPeriods(selfTest: Seq[SelfTestResult]): Seq[SelfTestPeriod] = {
    if (selfTest.isEmpty) {
      return Seq()
    }
    val tests = selfTest.sortBy(_.time)
    val (_, periods) = tests.tail.foldLeft((tests.head, Seq[SelfTestPeriod]())) {
      case ((last, list), current) ⇒ {
        if (last.success && current.success && last.serialNr == current.serialNr) {
          (current, list :+ new SelfTestPeriod(last.systemId, last.serialNr, last.time, current.time))
        } else {
          (current, list)
        }
      }
    }
    periods
  }

  /**
   * Create Exclusion periods caused by missing valid location certificate
   * @param certList list of Location certificates
   * @param period the period to check
   * @return List of periods where no valid certificates where present
   */
  def createExclusionPeriods(certList: Seq[LocationCertificate], period: ExclusionPeriod): Seq[ExclusionPeriod] = {
    val sorted = certList.sortBy(_.inspectionDate)
    sorted.foldRight(Seq(period)) { (current, periodList) ⇒
      {
        //remove periods based on current certificate
        periodList.flatMap(period ⇒ {
          if ((period.from >= current.inspectionDate) && (period.to <= current.validTime)) {
            //This period is completely within the certificate range
            Seq()
          } else if ((period.from >= current.validTime) || (period.to <= current.inspectionDate)) {
            //current Cert doesn't match this period
            Seq(period)
          } else if ((period.from < current.inspectionDate) && (period.to > current.validTime)) {
            //need to split period in two. This means that the certificat is valid only a part of the day
            //This will not happen with normal usage
            Seq(
              new ExclusionPeriod(period.from, current.inspectionDate),
              new ExclusionPeriod(current.validTime, period.to))
          } else if (period.from < current.inspectionDate) {
            //only end of day is covered
            Seq(new ExclusionPeriod(period.from, current.inspectionDate))
          } else {
            //only start day is covered
            Seq(new ExclusionPeriod(current.validTime, period.to))
          }
        })
      }
    }
  }
  /**
   * Return only the calibrations which are part of the same measurementUnit
   * @param calibrationReports all the calibration results of the day
   * @param serialNumber The serial number of the measurementUnit
   * @return the results part of the measurement unit
   */
  def filterCalibrationReports(calibrationReports: Seq[SelfTestResult], serialNumber: String): Seq[SelfTestResult] = {
    val correctSerialNumbers = calibrationReports.foldLeft(Seq[String]()) {
      case (list, current) ⇒ {
        if (current.success && !list.contains(current.serialNr)) {
          list :+ current.serialNr
        } else {
          list
        }
      }
    }
    if (!correctSerialNumbers.contains(serialNumber)) {
      //very strange the requested serial number doesn't have correct calibration result.
      //how is it possible to have violations?
      val log = LoggerFactory.getLogger(MeasurementUnit.getClass)
      log.error("Could not find a successful calibration with serial number %s found %s".format(serialNumber, correctSerialNumbers))
      return Seq()
    }
    if (correctSerialNumbers.size == 1) {
      //there is only one serial number => use all calibrations
      return calibrationReports
    }
    //need to filter calibration results
    //select only the successful calibrations with the same serial number
    //and the failures which are just before or after these successful calibrations.
    val (filteredResults, workingResults) = calibrationReports.sortBy(_.time).foldLeft(Seq[SelfTestResult](), Seq[SelfTestResult]()) {
      case ((needed, working), current) ⇒ {
        if (current.success) {
          if (current.serialNr == serialNumber) {
            (needed ++ working, Seq(current)) //working failures are before a correct serial number so include them
          } else if (working.headOption.exists(_.success)) { //only successful with correct serial nr are stored in working
            (needed ++ working, Seq()) //working failures are after a correct serial number so include them
          } else {
            (needed, Seq()) //didn't start with a successful serial number so these failures are not before or after a correct serial number so exclude them
          }
        } else {
          (needed, working :+ current)
        }
      }
    }
    if (workingResults.headOption.exists(_.success)) {
      filteredResults ++ workingResults
    } else {
      filteredResults
    }
  }

}

case class ExclusionPeriod(from: Long, to: Long)

case class SelfTestPeriod(systemId: String, serialNumber: String, from: Long, to: Long)