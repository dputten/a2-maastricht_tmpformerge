package csc.sectioncontrol.enforce.nl.qualify

import java.util.Date

import akka.actor.{ Actor, ActorLogging, ActorRef }
import akka.event.LoggingReceive
import csc.sectioncontrol.common.DayUtility
import csc.sectioncontrol.enforce.common.nl.violations.{ SystemConfigurationData, VehicleSpeed }
import csc.sectioncontrol.enforce.nl.casefiles.CaseFile
import csc.sectioncontrol.enforce.nl.classify.MultiCorridorProcessor
import csc.sectioncontrol.enforce.nl.register.LogMTMInfo
import csc.sectioncontrol.enforce.speedlog.{ GetEnforcedSpeeds, SpeedSettingOverTime }
import csc.sectioncontrol.messages._
import csc.sectioncontrol.storagelayer.hbasetables.{ ConfirmedViolationRecordTable, VehicleClassifiedTable, ViolationRecordTable }
import org.slf4j.LoggerFactory

/**
 * Actor that generates violations based on classified records. It uses the data from the system configuration
 * (corridors and schedules) as well as data stored in the SpeedLog actor to determine whether a classified
 * vehicle record constitutes a speed violation.
 *
 * The only externally accepted message is QualifyViolationsForDay.
 *
 * This actor can run for a long time and should be configured to run in a separate Dispatcher by the
 * creator/supervisor of the actor.
 */
case class ClassifiedRecordsRetriever(vehicleRecordReader: Option[VehicleClassifiedTable.Reader], confirmedViolationRecordReader: Option[ConfirmedViolationRecordTable.Reader]) {
  val log = LoggerFactory.getLogger(getClass)
  var confirmedViolations = Map[String, ViolationCandidateRecord]()

  def readRows(from: Long, to: Long): Seq[VehicleClassifiedRecord] = {
    vehicleRecordReader.map(_.readRows(from, to))
      .getOrElse {
        val records = confirmedViolationRecordReader.get.readRows(from, to)
        confirmedViolations = records.map(candidate ⇒ candidate.vehicleViolationRecord.classifiedRecord.id -> candidate).toMap
        records.map(_.vehicleViolationRecord.classifiedRecord)
      }

  }

  /**
   * Due to discarding violation information and only using classifiedRecord. Some important information is also discarded.
   * This method is copying the information back in the new violation
   *
   * @param violations list of new created violations
   * @return updated violation with ViolationCandidateRecord information
   */
  def extendViolationsWithConfirmedViolationInfo(violations: Seq[VehicleViolationRecord]): Seq[VehicleViolationRecord] = {

    violations.map(vio ⇒ {
      val lic = vio.classifiedRecord.speedRecord.entry.license.map(_.value).getOrElse("None")
      confirmedViolations.get(vio.classifiedRecord.id) match {
        case Some(candidate) ⇒ {
          log.info("Update recognizeResults for id %s license %s".format(vio.classifiedRecord.id, lic))
          vio.copy(recognizeResults = candidate.vehicleViolationRecord.recognizeResults)
        }
        case None ⇒ {
          log.info("Skip violation update for id %s license %s".format(vio.classifiedRecord.id, lic))
          vio
        }
      }
    })
  }
}

class QualifyViolations(systemId: String,
                        classifiedRecordsRetriever: ClassifiedRecordsRetriever,
                        outputViolations: ViolationRecordTable.Writer,
                        speedLog: ActorRef,
                        storeMtmData: ActorRef,
                        multiCorridor: MultiCorridorProcessor) extends Actor
  with ActorLogging {

  // Actor state
  private var receivedSpeedSettings: Map[Int, SpeedSettingOverTime] = Map()
  private var requestedCorridorIds: Set[Int] = Set()
  private var qualificationInProgress: Boolean = false
  private var state: QVState = _

  def receive = LoggingReceive {
    case qv @ QualifyViolationsForDay(day, sysConfig, statsKey) ⇒
      if (!qualificationInProgress) {
        qualificationInProgress = true
        log.info("Started qualifying violations!")
        requestedCorridorIds = sysConfig.corridors.map(_.corridorId).toSet
        if (!requestedCorridorIds.isEmpty) {
          receivedSpeedSettings = Map()
          requestedCorridorIds.foreach(corridorId ⇒ speedLog ! GetEnforcedSpeeds(corridorId))
          state = QVState(sysConfig, day, sender, statsKey)
        } else {
          qualificationInProgress = false
          log.error("No corridors configured so cannot qualify...")
          sender ! QualifyViolationsFailed("No configured corridors")
        }
      } else {
        log.error("Received %s for date %s while qualification is already in progress".format(qv, new Date(qv.day)))
        sender ! QualifyViolationsFailed("Qualification already in progress")
      }

    case qvc @ QualifyViolationsContinue ⇒
      if (qualificationInProgress) {
        val (dayStart, dayEnd) = DayUtility.calculateDayPeriod(state.day)
        log.info("Start reading classification/confirmedViolation records from %s to %s".format(new Date(dayStart), new Date(dayEnd)))
        val classifiedRecords = classifiedRecordsRetriever.readRows(dayStart, dayEnd)
        if (classifiedRecords.isEmpty) {
          log.info("No classified/confirmedViolation records found, total number of vehicles in violation => 0")
        } else {
          log.info("Checking for each of the " + classifiedRecords.length + " vehicles if they are in violation...")
          val correctedClassifiedRecords = multiCorridor.checkClassifications(classifiedRecords)
          val violations = correctedClassifiedRecords.flatMap(makeViolation(_, state.systemConfiguration, receivedSpeedSettings))
          log.info("Total number of vehicles in violation => " + violations.length)
          val extendedViolations = classifiedRecordsRetriever.extendViolationsWithConfirmedViolationInfo(violations)

          val mappedViolations = extendedViolations.map(vvr ⇒ vvr.classifiedRecord -> vvr)

          mappedViolations foreach { mappedViolation ⇒
            log.debug("Writing violation to HBase: %s".format(mappedViolation._1))
          }

          outputViolations.writeRows(mappedViolations)
        }

        qualificationInProgress = false
        state.requestor ! QualifyViolationsSuccessful
      } else {
        log.error("Received %s while no qualification is in progress".format(qvc))
      }

    case speeds: SpeedSettingOverTime ⇒
      if (qualificationInProgress) {
        receivedSpeedSettings += (speeds.corridorId -> speeds)
        val caseFile: CaseFile = state.systemConfiguration.corridors.find(_.corridorId == speeds.corridorId).map(_.caseFile).get
        val exitLocationName = state.systemConfiguration.corridors.find(_.corridorId == speeds.corridorId).
          map(_.exitLocationName).getOrElse("")
        val location = makeLocationName(state, exitLocationName)
        val schedules = state.systemConfiguration.schedules.filter(_.corridorId == speeds.corridorId)
        val mtmInfo = MtmInfoFormatter.makeExportFile(location, state.day, speeds, schedules, caseFile)
        storeMtmData ! LogMTMInfo(state.day, speeds.corridorId, mtmInfo)
        if ((requestedCorridorIds -- receivedSpeedSettings.keySet).isEmpty) {
          val locations = state.systemConfiguration.corridors.map(c ⇒ c.corridorId -> c.exitLocationName).toMap
          val mtmInfoAll = MtmInfoFormatter.makeMtmSpeedSettings(locations,
            state.day,
            receivedSpeedSettings,
            state.systemConfiguration.schedules,
            state.statsKey,
            caseFile)
          // publish MtmSpeedSettings for pick up by the report data store actor
          context.system.eventStream.publish(mtmInfoAll)
          self ! QualifyViolationsContinue
        }
      } else {
        log.error("received " + speeds + " while no qualification is in progress")
      }
  }

  private[qualify] def makeLocationName(state: QVState, exitLocationName: String) = {
    "%s %s %s".format(state.systemConfiguration.globalConfig.systemName,
      state.systemConfiguration.globalConfig.roadPart, exitLocationName)
  }
  /**
   * Not an API method. Do not call from outside this actor.
   */
  private[qualify] def makeViolation(vcr: VehicleClassifiedRecord,
                                     sysConfig: SystemConfigurationData,
                                     speedSettings: Map[Int, SpeedSettingOverTime]): Option[VehicleViolationRecord] = {
    val time = vcr.speedRecord.time
    val corridorId = vcr.speedRecord.corridorId
    val schedule = sysConfig.schedules.find(s ⇒ s.corridorId == corridorId && s.startTime <= time && s.endTime > time)
    val corridor = sysConfig.corridors.find(_.corridorId == corridorId)

    if (corridor.isDefined && schedule.isDefined) {
      val service = corridor.get.service
      val measuredSpeed = vcr.speedRecord.speed
      val scheduleSpeedLimit = schedule.get.enforcedSpeedLimit
      val dynamicSpeedLimit = speedSettings.get(corridorId).map(_(time)).map(ss ⇒ ss.setting).getOrElse(Right(None))
      val vehicleClassSpeedLimit = getVehicleClassSpeedLimit(vcr, sysConfig.vehicleClassSpeedLimits)
      val speedMargins = sysConfig.speedMargins.map(sm ⇒ (sm.baseSpeed -> sm.margin)).toMap

      val vehicleSpeed = VehicleSpeed(
        measuredSpeed = measuredSpeed,
        scheduleSpeedLimit = scheduleSpeedLimit,
        dynamicSpeedLimit = dynamicSpeedLimit,
        vehicleClassSpeedLimit = vehicleClassSpeedLimit,
        speedMargins = speedMargins)

      val (isViolation, enforcedSpeed) = service.isViolation(vcr, vehicleSpeed)

      val message = "Vehicle with license plate %s is %s in violation (measuredSpeed=%s, enforcedSpeed=%s)"
      val logMessage = message.format(vcr.licensePlate, if (isViolation) "" else "not", measuredSpeed, enforcedSpeed)
      log.info(logMessage)

      if (isViolation) {
        val entryRecog = vcr.speedRecord.entry.licensePlateData.map(lic ⇒ RecognizeRecord(recognizeId = lic.recognizedBy.getOrElse("Unknown") + "_entry", isCurrent = true, recognize = lic))
        val exitRecog = vcr.speedRecord.exit.flatMap(_.licensePlateData.map(lic ⇒ RecognizeRecord(recognizeId = lic.recognizedBy.getOrElse("Unknown") + "_exit", isCurrent = false, recognize = lic)))
        val recogList = List() ++ entryRecog ++ exitRecog
        Some(VehicleViolationRecord(vcr.id, vcr, enforcedSpeed, dynamicSpeedLimit.right.toOption.flatMap(identity), recogList))
      } else {
        None
      }
    } else {
      log.info("No corridor and/or schedule defined")
      None
    }
  }

  private def getVehicleClassSpeedLimit(vcr: VehicleClassifiedRecord, vehicleClassSpeedLimits: Map[VehicleCode.Value, Int]): Option[Int] = {
    val classification = vcr.code.orElse(vcr.alternativeClassification)
    classification.flatMap(code ⇒ vehicleClassSpeedLimits.get(code))
  }

  private case class QVState(systemConfiguration: SystemConfigurationData,
                             day: Long,
                             requestor: ActorRef,
                             statsKey: StatsDuration)

  private case object QualifyViolationsContinue
}

/**
 * Message to initiate the qualification of a day of violations
 *
 * @param day A time during the day to be exported.
 */
case class QualifyViolationsForDay(day: Long, systemConfiguration: SystemConfigurationData, statsKey: StatsDuration)

/**
 * Response message to indicate that qualifying violations failed.
 * @param message Details on the failure reason.
 */
case class QualifyViolationsFailed(message: String)

/**
 * Response message to indicate that qualifying violations completed successfully.
 */
case object QualifyViolationsSuccessful
