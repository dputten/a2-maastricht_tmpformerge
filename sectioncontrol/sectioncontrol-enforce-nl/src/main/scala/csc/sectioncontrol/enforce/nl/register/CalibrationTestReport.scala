/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.nl.register

import csc.sectioncontrol.enforce.common.nl.violations.{ SystemGlobalConfig, CorridorData, CorridorSchedule }
import csc.sectioncontrol.storage.KeyWithTimeAndId

/**
 * The calibration test report that is expected by the RegisterViolations actor.
 *
 * @author Maarten Hazewinkel
 */
case class CalibrationTestReport(time: Long,
                                 corridor: CorridorData,
                                 schedule: CorridorSchedule,
                                 globalConfig: SystemGlobalConfig,
                                 isMidnightTest: Boolean,
                                 resultOk: Boolean,
                                 numTestedCameras: Int,
                                 reportingOfficerCode: String,
                                 cameraSerialNumbers: Seq[String],
                                 cameraTestImages: Seq[CameraTestImage]) {

}

case class CameraTestImage(cameraId: Int, storedImageKey: KeyWithTimeAndId)
