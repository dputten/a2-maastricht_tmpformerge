/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.nl.run

import collection.SortedSet

import java.util.{ Calendar, GregorianCalendar, TimeZone }
import akka.util.duration._
import csc.config.Path
import csc.curator.utils.Curator

import csc.sectioncontrol.storage._
import csc.sectioncontrol.enforce.nl._
import casefiles.{ CaseFile, HHMVS40, HHMVS14, TCVS33 }
import csc.sectioncontrol.messages.{ SpeedIndicatorType, VehicleCode }
import csc.sectioncontrol.enforce.common.nl.violations._
import csc.sectioncontrol.enforce.common.nl.services._
import csc.sectioncontrol.enforce.Enumerations.RoadType
import csc.sectioncontrol.enforce.common.nl.violations.CorridorData
import csc.sectioncontrol.enforce.common.nl.services.SpeedMobile
import csc.sectioncontrol.enforce.common.nl.services.SectionControl
import csc.sectioncontrol.enforce.common.nl.violations.SystemConfigurationData
import csc.sectioncontrol.storage.ZkSystem
import csc.sectioncontrol.enforce.common.nl.services.TargetGroup
import csc.sectioncontrol.storage.ZkPreference
import csc.sectioncontrol.enforce.common.nl.violations.SystemGlobalConfig
import csc.sectioncontrol.storage.ZkSection
import csc.sectioncontrol.enforce.common.nl.services.SpeedFixed
import csc.sectioncontrol.storage.ZkGantry
import csc.sectioncontrol.storage.ZkUser
import csc.sectioncontrol.enforce.common.nl.services.RedLight
import csc.sectioncontrol.enforce.common.nl.services.ANPR
import csc.sectioncontrol.storage.ZkCorridor
import csc.sectioncontrol.sign.certificate.CertificateService
import csc.sectioncontrol.enforce.register.CountryCodes
import csc.sectioncontrol.enforce.common.nl.casefiles.HHMVS20
import csc.sectioncontrol.storagelayer.state.StateService

/**
 * Factory object to build up the SystemConfigurationData used by QualifyViolations and RegisterViolations
 * from the configuration data stored in zookeeper.
 *
 * @author Maarten Hazewinkel
 */
object LoadSystemConfiguration {

  /*object HardCoded {
    val minimumViolationTimeSeparation = 30.minutes.toMillis
  }*/

  def apply(systemId: String, dayStart: Long, dayEnd: Long, timezone: TimeZone, curator: Curator): SystemConfigurationData = {
    val corridorIds = getCorridorIds(systemId, curator)

    val globalConfig = getGlobalConfig(systemId, curator)
    val corridorData = corridorIds.map(corridorId ⇒ getCorridorData(systemId, corridorId, curator))
    val schedules = corridorIds.flatMap(corridorId ⇒ getCorridorSchedules(systemId, corridorId, dayStart, dayEnd, timezone, curator))
    val vehicleMaxSpeed = getVehicleMaxSpeeds(systemId, curator)
    val speedMargins = getSpeedMargins(systemId, curator)

    val stateEvents = StateService.getStateLogHistory(systemId, 0L, System.currentTimeMillis(), curator)
    val reportingOfficers = stateEvents.map {
      case (corridorId, stateList) ⇒ {
        corridorId.infoId -> ReportingOfficers(stateList.filter(ss ⇒ ZkState.enforceStates.contains(ss.state)).
          map(ss ⇒ (ss.timestamp, getReportingOfficerCode(ss.userId, curator))).
          filter(_._2.isDefined).
          map(p ⇒ (p._1 -> p._2.get)).
          toMap)
      }
    }

    SystemConfigurationData(globalConfig,
      corridorData,
      schedules,
      reportingOfficers,
      vehicleMaxSpeed,
      speedMargins)
  }

  /**
   * retrieve the service that belongs to a corridor
   * @param corridor that is stored
   * @param systemId id of system
   * @param curator zookeeper storage
   * @return optional Service
   */
  private def getService(corridor: ZkCorridor, systemId: String, curator: Curator): Option[Service] = {
    val rootPathToServices = Path(Paths.Corridors.getServicesPath(systemId, corridor.id))

    //e.g. systems/../corridors/../services/RedLight
    val pathToService = (rootPathToServices / corridor.serviceType.toString).toString()

    val result = corridor.serviceType match {
      case ServiceType.RedLight       ⇒ curator.get[RedLight](pathToService)
      case ServiceType.SpeedFixed     ⇒ curator.get[SpeedFixed](pathToService)
      case ServiceType.SpeedMobile    ⇒ curator.get[SpeedMobile](pathToService)
      case ServiceType.SectionControl ⇒ curator.get[SectionControl](pathToService)
      case ServiceType.ANPR           ⇒ curator.get[ANPR](pathToService)
      case ServiceType.TargetGroup    ⇒ curator.get[TargetGroup](pathToService)
      case _                          ⇒ None
    }
    result
  }

  /**
   * retrieve the casefile version that belongs to a corridor
   * @param corridor that is stored
   * @param systemId id of system
   * @param curator zookeeper storage
   * @return optional Service
   */
  private def getCaseFile(corridor: ZkCorridor, systemId: String, curator: Curator): Option[CaseFile] = {

    //Removed because current versions don't need configuration
    //Left in code because this is needed for new versions
    //e.g. systems/../corridors/../caseFiles/TCVS33
    //val rootPathToCaseFiles = Path(Paths.Corridors.getCaseFilePath(systemId, corridor.id))
    //val pathToCaseFile = (rootPathToCaseFiles / corridor.caseFileType.toString).toString()

    corridor.caseFileType match {
      case ZkCaseFileType.TCVS33  ⇒ Some(TCVS33())
      case ZkCaseFileType.HHMVS14 ⇒ Some(HHMVS14())
      case ZkCaseFileType.HHMVS40 ⇒ Some(HHMVS40())
      case ZkCaseFileType.HHMVS20 ⇒ Some(HHMVS20())
      case _                      ⇒ None
    }
  }

  private def getCorridorIds(systemId: String, curator: Curator): Seq[String] = {
    val corridorPaths = curator.getChildren(Paths.Corridors.getDefaultPath(systemId))
    corridorPaths.map { _.split('/').last }
  }

  private def getGlobalConfig(systemId: String, curator: Curator): SystemGlobalConfig = {
    val globalConfig = for {
      system ← curator.get[ZkSystem](Paths.Systems.getConfigPath(systemId))
      countries ← curator.get[ZkClassifyCountry](Seq(Path(Paths.Configuration.getClassifyCountry)), ZkClassifyCountry())
    } yield SystemGlobalConfig(
      CountryCodes(curator),
      system.violationPrefixId,
      system.mtmRouteId,
      system.location.roadPart,
      CertificateService.getAllInstalledMeasurementMethodTypes(curator, systemId),
      CertificateService.getLocationCertificates(curator, systemId),
      CertificateService.getActiveSoftwareCertificate(curator, systemId),
      CertificateService.getActiveConfigurationCertificate(curator, systemId),
      system.name,
      system.minimumViolationTimeSeparation.minutes.toMillis,
      system.serialNumber.serialNumber,
      countries.mobiCountries,
      system.pshtmId.getOrElse(system.name))

    require(globalConfig.isDefined,
      "Could not load configuration for systemId " + systemId + ". System config or type certificate is missing")
    globalConfig.get
  }

  private def getCorridorLength(systemId: String, allSectionIds: Seq[String], curator: Curator): Double = {
    allSectionIds.foldLeft(0d) {
      (corridorLength, sectionId) ⇒
        val section = curator.get[ZkSection](Paths.Sections.getConfigPath(systemId, sectionId))
        section.map(corridorLength + _.length).getOrElse(corridorLength)
    }
  }

  private def getCorridorData(systemId: String, corridorId: String, curator: Curator): CorridorData = {
    val corridorData = for {
      system ← curator.get[ZkSystem](Paths.Systems.getConfigPath(systemId))
      corridor ← curator.get[ZkCorridor](Paths.Corridors.getConfigPath(systemId, corridorId))
      service ← getService(corridor, systemId, curator)
      caseFile ← getCaseFile(corridor, systemId, curator)
      startSection ← curator.get[ZkSection](Paths.Sections.getConfigPath(systemId, corridor.startSectionId))
      endSection ← curator.get[ZkSection](Paths.Sections.getConfigPath(systemId, corridor.endSectionId))
      startGantry ← curator.get[ZkGantry](Paths.Gantries.getConfigPath(systemId, startSection.startGantryId))
      endGantry ← curator.get[ZkGantry](Paths.Gantries.getConfigPath(systemId, endSection.endGantryId))
    } yield CorridorData(corridorId = corridor.info.corridorId,
      entryGantryId = startSection.startGantryId,
      exitGantryId = endSection.endGantryId,
      entryLocationName = startGantry.hectometer,
      exitLocationName = endGantry.hectometer,
      length = getCorridorLength(systemId, corridor.allSectionIds, curator),
      roadCode = corridor.roadCode,
      radarCode = corridor.radarCode,
      compressionFactor = system.compressionFactor,
      nrDaysKeepViolations = system.retentionTimes.nrDaysKeepViolations,
      dynaMaxScanInterval = 0,
      pardonTimeEsa = system.pardonTimes.pardonTimeEsa_secs.toInt,
      locationCode = corridor.info.locationCode,
      locationLine1 = corridor.info.locationLine1,
      locationLine2 = corridor.info.locationLine2,
      drivingDirectionFrom = Some(corridor.direction.directionFrom),
      drivingDirectionTo = Some(corridor.direction.directionTo),
      roadType = RoadType.withName(corridor.roadType.toString),
      service = service,
      caseFile = caseFile,
      insideTown = corridor.isInsideUrbanArea)
    require(corridorData.isDefined, "Could not load corridor data for systemId " + systemId + ", corridorId " + corridorId)
    corridorData.get
  }

  def getCorridorSchedules(systemId: String, corridorId: String, dayStart: Long, dayEnd: Long, timezone: TimeZone, curator: Curator): List[CorridorSchedule] = {
    val currentSchedulesPath = Paths.Corridors.getSchedulesPath(systemId, corridorId)
    val currentSchedules = loadCorridorSchedules(systemId, corridorId, dayStart, dayEnd, currentSchedulesPath, timezone, curator)

    val historicScheduleSaves = curator.getChildNames(Paths.Corridors.getScheduleHistoryPath(systemId, corridorId)).
      map(_.toLong).
      filter(t ⇒ t > dayStart && t < dayEnd).
      sorted.
      toList
    val historicIntervals = (dayStart :: historicScheduleSaves).zip(historicScheduleSaves)
    val historicSchedules = historicIntervals.map { interval ⇒
      val historicPath = Paths.Corridors.getScheduleHistoryPath(systemId, corridorId) + "/" + interval._2.toString
      loadCorridorSchedules(systemId, corridorId, interval._1, interval._2, historicPath, timezone, curator)
    }
    updateCurrentSchedules(currentSchedules, historicScheduleSaves.lastOption.getOrElse(0L)) ++ historicSchedules.flatten
  }

  private def updateCurrentSchedules(current: List[CorridorSchedule], endTimeLastHistoric: Long): List[CorridorSchedule] = {
    current.map(schedule ⇒ {
      if (endTimeLastHistoric > schedule.startTime) {
        schedule.copy(startTime = endTimeLastHistoric)
      } else {
        schedule
      }
    })
  }

  def loadCorridorSchedules(systemId: String, corridorId: String, timeStart: Long, timeEnd: Long, schedulesZkPath: String, timezone: TimeZone, curator: Curator): List[CorridorSchedule] = {
    implicit def convertIndication(indication: ZkIndicationType.Value): Option[Boolean] = {
      indication match {
        case ZkIndicationType.True  ⇒ Some(true)
        case ZkIndicationType.False ⇒ Some(false)
        case ZkIndicationType.None  ⇒ None
      }
    }
    val corridorSchedules = for {
      system ← curator.get[ZkSystem](Paths.Systems.getConfigPath(systemId))
      corridor ← curator.get[ZkCorridor](Paths.Corridors.getConfigPath(systemId, corridorId))
      schedules ← curator.get[List[ZkSchedule]](schedulesZkPath)
      startSection ← curator.get[ZkSection](Paths.Sections.getConfigPath(systemId, corridor.startSectionId))
      endSection ← curator.get[ZkSection](Paths.Sections.getConfigPath(systemId, corridor.endSectionId))
    } yield schedules.map(schedule ⇒ CorridorSchedule(corridor.info.corridorId,
      schedule.speedLimit,
      if (schedule.signSpeed >= 0) Some(schedule.signSpeed) else None,
      getPshTmIdFormat(systemId, corridorId, curator),
      calculateDayTime(timeStart, schedule.fromPeriodHour, schedule.fromPeriodMinute, timezone),
      calculateDayTime(timeStart, schedule.toPeriodHour, schedule.toPeriodMinute, timezone),
      indicationRoadworks = schedule.indicationRoadworks,
      indicationActualWork = schedule.indicationActualWork,
      indicationDanger = schedule.indicationDanger,
      supportMsgBoard = schedule.supportMsgBoard,
      textCode = parseStringToInt(corridor.codeText),
      dutyType = if (corridor.dutyType == null || corridor.dutyType.trim.length == 0) None else Some(corridor.dutyType),
      deploymentCode = if (corridor.deploymentCode == null || corridor.deploymentCode.trim.length == 0) None else Some(corridor.deploymentCode),
      invertDrivingDirection = schedule.invertDrivingDirection,
      speedIndicator = SpeedIndicator(
        signIndicator = schedule.signIndicator,
        speedIndicatorType = schedule.speedIndicatorType.map(a ⇒ SpeedIndicatorType.withName(a.toString))))).flatMap(schedule ⇒
      if (schedule.startTime >= timeStart && schedule.endTime <= timeEnd) {
        List(schedule)
      } else if (schedule.startTime >= timeEnd || schedule.endTime <= timeStart) {
        List()
      } else {
        List(schedule.copy(startTime = math.max(schedule.startTime, timeStart),
          endTime = math.min(schedule.endTime, timeEnd)))
      })
    corridorSchedules.getOrElse(List())
  }

  private def parseStringToInt(v: String): Option[Int] = {
    if (v.length > 0) {
      try {
        Some(v.toInt)
      } catch {
        case e: Exception ⇒ None
      }
    } else {
      None
    }
  }

  private def getPshTmIdFormat(systemId: String, corridorId: String, curator: Curator): String = {
    val defaultElementOrder = "YMDNVLHTX"

    val pshTmIdFormat = for {
      corridor ← curator.get[ZkCorridor](Paths.Corridors.getConfigPath(systemId, corridorId))
    } yield {
      val zkPSHTMIdentification = corridor.pshtm
      val elementOrderString = corridor.pshtm.elementOrderString.getOrElse(defaultElementOrder)

      getFormat(zkPSHTMIdentification, elementOrderString)
    }
    require(pshTmIdFormat.isDefined, "Could not load PSH-TM-ID format for systemId " + systemId + ", corridorId " + corridorId)
    pshTmIdFormat.get
  }

  def getFormat(zkPSHTMIdentification: ZkPSHTMIdentification, elementOrderString: String): String = {
    def elementString(elementChar: Char): String = elementChar match {
      case 'Y' ⇒ if (zkPSHTMIdentification.useYear) "[YY]" else ""
      case 'M' ⇒ if (zkPSHTMIdentification.useMonth) "[MM]" else ""
      case 'D' ⇒ if (zkPSHTMIdentification.useDay) "[DD]" else ""
      case 'N' ⇒ if (zkPSHTMIdentification.useNumberOfTheDay) "[NNN]" else ""
      case 'V' ⇒ if (zkPSHTMIdentification.useReportingOfficerId) "[VVVV]" else ""
      case 'L' ⇒ if (zkPSHTMIdentification.useLocationCode) "[LLLL]" else ""
      case 'H' ⇒ if (zkPSHTMIdentification.useHHMCode) "[HHHH]" else ""
      case 'T' ⇒ if (zkPSHTMIdentification.useTypeViolation) "[T]" else ""
      case 'X' ⇒ if (zkPSHTMIdentification.useFixedCharacter) zkPSHTMIdentification.fixedCharacter else ""
    }

    elementOrderString.foldLeft("")((acc, cur) ⇒ acc + elementString(cur))
  }

  def getVehicleMaxSpeeds(systemId: String, curator: Curator): Map[VehicleCode.Value, Int] = {
    val vehicleMaxSpeeds = for {
      prefs ← curator.get[ZkPreference](Seq(Path(Paths.Preferences.getGlobalConfigPath), Path(Paths.Preferences.getConfigPath(systemId))), ZkPreference.default)
    } yield prefs.vehicleMaxSpeed.map(vms ⇒ vms.vehicleType -> vms.maxSpeed).toMap

    require(vehicleMaxSpeeds.isDefined, "Could not load vehicle max speeds for systemId " + systemId)
    vehicleMaxSpeeds.get
  }

  def getSpeedMargins(systemId: String, curator: Curator): SortedSet[SpeedMargin] = {
    val speedMargins = for {
      prefs ← curator.get[ZkPreference](Seq(Path(Paths.Preferences.getGlobalConfigPath), Path(Paths.Preferences.getConfigPath(systemId))), ZkPreference.default)
    } yield prefs.marginSpeedLimits.map(msl ⇒ SpeedMargin(msl.speedLimit, msl.speedMargin))

    require(speedMargins.isDefined, "Could not load speed margins for systemId " + systemId)
    SortedSet(speedMargins.get: _*)
  }

  private def getReportingOfficerCode(userId: String, curator: Curator): Option[String] = {
    val path = Paths.Users.getUserPath(userId)
    curator.get[ZkUser](path).map(_.reportingOfficerId)
  }

  private def calculateDayTime(dayStart: Long, hour: Int, minute: Int, timezone: TimeZone): Long = {
    val cal = new GregorianCalendar(timezone)
    cal.setTimeInMillis(dayStart)
    cal.set(Calendar.HOUR_OF_DAY, hour)
    cal.set(Calendar.MINUTE, minute)
    cal.getTimeInMillis
  }

}
