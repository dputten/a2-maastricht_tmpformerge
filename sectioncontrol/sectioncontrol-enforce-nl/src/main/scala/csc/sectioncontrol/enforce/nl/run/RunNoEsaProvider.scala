/*
 *
 *  * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 */

package csc.sectioncontrol.enforce.nl.run

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

import collection.mutable.ListBuffer

import java.util.TimeZone
import java.util.concurrent.Executors

import akka.actor.ActorRef
import akka.event.LoggingReceive
import akka.util.duration._
import akka.pattern.ask
import akka.util.Duration
import akka.dispatch.{ ExecutionContext, Future }

import csc.akkautils.{ RunToken, JobComplete, JobFailed }
import csc.curator.utils.{ Curator, CuratorActor }

import csc.sectioncontrol.enforce._
import speedlog.{ GeneratedExcludeEvents, ScheduledSpeed, GenerateExcludeEvents }
import csc.sectioncontrol.storage._
import csc.sectioncontrol.common.DayUtility

/*
This class is used when no esa provider is needed. This is relevant for the services Snelheid, Roodlicht and ANPR
The main purpose is to create the necessary excludelog per corridor per day and nothing else.
 */
class RunNoEsaProvider(systemId: String,
                       protected val curator: Curator,
                       //storeMtmData: ActorRef,
                       timeZone: String,
                       //filePickupDelay: Long = 1.minute.toMillis,
                       getSpeedLog: (String) ⇒ ActorRef,
                       getExcludeLog: (String) ⇒ ActorRef) extends CuratorActor {
  implicit val ec = ExecutionContext.fromExecutorService(Executors.newCachedThreadPool())

  protected def receive = LoggingReceive {
    case Run(zkJobPath, jobRunner, runToken, workDate, manageStatus) ⇒ {
      log.debug(systemId + " is configured as having no esa provider. Will create excludelog")

      createExcludeLog(jobRunner, runToken, workDate, manageStatus, 10.seconds)
    }
    case GeneratedExcludeEvents ⇒ {
      log.warning("Received unexpected GeneratedExcludeEvents message")
    }
  }

  /*
  Creates for each corridors within a given systemId the excludelog
   */
  private def createExcludeLog(jobRunner: ActorRef, runToken: RunToken, workDate: Long, manageStatus: ActorRef, duration: Duration) {
    val (dayStart, dayEnd) = DayUtility.calculateDayPeriod(workDate)
    val corridorIds = getCorridorIds
    val futureList = new ListBuffer[Future[Any]]

    val systemCfg = curator.get[ZkSystem](Paths.Systems.getConfigPath(systemId))
    val pardonCfg = systemCfg.map(_.pardonTimes).getOrElse(ZkSystemPardonTimes())

    corridorIds.foreach {
      corridorId ⇒
        val corridor = curator.get[ZkCorridor](Paths.Corridors.getConfigPath(systemId, corridorId))
        corridor.foreach {
          corridorConfig ⇒
            val technicalPardonTime_mSecs = pardonCfg.pardonTimeTechnical_secs * 1000L
            val esaPardonTime_mSecs = pardonCfg.pardonTimeEsa_secs * 1000L
            val scheduleSpeeds = LoadSystemConfiguration.
              getCorridorSchedules(systemId, corridorId, dayStart, dayEnd, TimeZone.getTimeZone(timeZone), curator).
              map(s ⇒ ScheduledSpeed(s.startTime, s.endTime, s.enforcedSpeedLimit, s.supportMsgBoard))
            //        futureList += getSpeedLog(systemId).ask(GenerateExcludeEvents(dayStart,
            val speed = getSpeedLog(systemId)
            //send enforce on default speed
            speed ! SpeedDataImpl(dayStart, corridorConfig.info.corridorId, -1, Right(None))
            //generate excludeEvents
            futureList += speed.ask(GenerateExcludeEvents(dayStart,
              dayEnd,
              corridorConfig.info.corridorId,
              technicalPardonTime_mSecs,
              esaPardonTime_mSecs,
              getExcludeLog(systemId),
              scheduleSpeeds))(duration)
        }
    }

    val futuresSequence = Future.sequence(futureList)
    //wait for all replies
    futuresSequence.onComplete {
      case Right(results) ⇒ {
        //collect results
        manageStatus ! ProcessingMtmCompleted(workDate, runToken.startTime)
        jobRunner ! JobComplete
      }
      case Left(e) ⇒ {
        //failure
        for (pos ← 0 until futureList.size) {
          val future = futureList(pos)
          future.value match {
            case Some(Right(singleResult)) ⇒ //request was successful
            case Some(Left(singleExc)) ⇒ {
              val corridor = corridorIds(pos)
              log.warning("Could not generate exclude log for corridor %s".format(corridor))
            }
            case None ⇒ {
              val corridor = corridorIds(pos)
              log.warning("Could not generate exclude log for corridor %s".format(corridor))
            }
          }
        }
        jobRunner ! JobFailed
      }
    }
  }

  private def getCorridorIds: Seq[String] = {
    val corridorPaths = curator.getChildren(Paths.Corridors.getDefaultPath(systemId))
    corridorPaths.map {
      _.split('/').last
    }

  }
}

