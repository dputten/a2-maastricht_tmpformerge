/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.enforce.nl.run

import akka.actor._
import akka.actor.SupervisorStrategy.Stop
import akka.event.LoggingReceive
import akka.util.Duration
import akka.util.duration._

import csc.akkautils.{ JobComplete, JobFailed }
import csc.checkimage.{ CheckVehicleRegistration, CheckViolation }
import csc.curator.utils.Curator
import csc.hbase.utils.HBaseTableKeeper

import csc.sectioncontrol.enforce.EnforceBoot
import csc.sectioncontrol.messages.{ VehicleImageType, StatsDuration }
import csc.sectioncontrol.enforce.nl.register.{ ExportViolationsCompleted, RegisterViolations }
import csc.sectioncontrol.common.DayUtility
import org.apache.hadoop.conf.Configuration
import csc.sectioncontrol.storagelayer.hbasetables._
import csc.akkautils.JobFailed
import csc.akkautils.JobComplete
import csc.sectioncontrol.enforce.nl.register.ExportViolations
import akka.actor.Terminated
import akka.actor.AllForOneStrategy
import csc.sectioncontrol.enforce.nl.register.ExportViolationsFailed
import csc.sectioncontrol.enforce.nl.RegistrationTimeUnreliableConfig
import csc.sectioncontrol.storagelayer.caseFile.{ CaseFileConfigZooKeeperService, CaseFileConfigService }
import csc.sectioncontrol.storagelayer.system.SystemServiceZookeeper

/**
 * Handle the running of the RegisterViolations job.
 *
 * This job needs to wait for QualifyViolations complete for the previous day.
 *
 * While that condition is not satisfied, this job will re-schedule itself to run every minute.
 * Once the condition is met the job will run through and re-schedule itself for the next day.
 *
 * @author Maarten Hazewinkel
 */
class RunRegisterViolations(systemId: String,
                            curator: Curator,
                            hbaseConfig: Configuration,
                            recognizeImage: Option[ActorRef],
                            timeoutImageResponse: Duration,
                            confidenceLevel: Int,
                            recognizerCountries: Seq[String],
                            recognizerCountryMinConfidence: Int,
                            maxCertifiedSpeed: Int,
                            recognizeWithARHAgain: Boolean,
                            intradaInterfaceToUse: Int,
                            minLowestLicConfLevel: Int,
                            minLicConfLevelRecognizer2: Int,
                            autoThresholdLevel: Int,
                            mobiThresholdLevel: Int,
                            pardonRegistrationTime: RegistrationTimeUnreliableConfig) extends Actor with ActorLogging with HBaseTableKeeper {

  var currentRun: Option[Run] = None
  private val getImageReaders = VehicleImageType.values.map(imageType ⇒
    (imageType -> VehicleImageTable.makeReader(hbaseConfig, this, imageType))).toMap

  private val checkRegistrationImage = recognizeImage.map(recognizeImageActor ⇒
    context.actorOf(Props(new CheckVehicleRegistration(recognizeImageActor,
      1.day,
      recognizerCountries,
      recognizerCountryMinConfidence,
      recognizeWithARHAgain)), "checkRegistrationImage"))

  private val checkViolationImages = checkRegistrationImage.map(checkRegistrationImageActor ⇒
    context.actorOf(Props(
      new CheckViolation(checkRegistrationImageActor, intradaInterfaceToUse, confidenceLevel)), "checkViolationImages"))

  private val registerViolations = context.actorOf(Props(new RegisterViolations(systemId,
    ViolationRecordTable.makeReader(hbaseConfig, this),
    ProcessedViolationTable.makeStore(hbaseConfig, this),
    getImageReaders,
    SelfTestResultTable.makeReader(hbaseConfig, this, systemId),
    CalibrationImageTable.makeReader(hbaseConfig, this),
    MtmFileTable.makeStore(hbaseConfig, this),
    EnforceBoot.getExcludeLog(systemId),
    checkViolationImages,
    curator,
    timeoutImageResponse,
    recognizerCountries,
    maxCertifiedSpeed,
    recognizeWithARHAgain,
    intradaInterfaceToUse,
    minLowestLicConfLevel,
    minLicConfLevelRecognizer2,
    autoThresholdLevel,
    mobiThresholdLevel,
    pardonRegistrationTime,
    new CaseFileConfigZooKeeperService(curator))),
    "registerViolations")

  protected def receive = LoggingReceive {
    case run @ Run(_, _, _, actionDate, manageStatus) ⇒ {
      log.debug("{} Receive message {}", self.path, run)
      currentRun = Some(run)
      log.debug("{} Is Qualification Complete for date {}", self.path, actionDate)
      manageStatus ! IsQualificationCompleted(actionDate)
      context.become(receiveRunning)
    }

    case msg ⇒
      log.error("{} received unexpected message {}", self.path, msg)
  }

  def receiveRunning: Receive = LoggingReceive {
    case run @ Run(zkJobPath, runner, runToken, actionDate, manageStatus) ⇒
      log.error("{} is already processing another run: {}", self.path, currentRun.get)
      runner ! JobFailed(retry = true)

    case mps @ QualificationStatus(true, actionDate) ⇒ {
      log.debug("{} ActionDate {} Current ActionDate {}", self.path, actionDate, currentRun.get.actionDate)

      if (actionDate == currentRun.get.actionDate) {
        val configPath = currentRun.get.zkJobPath + "/config"
        curator.get[RegisterViolationsJobConfig](configPath) match {
          case None ⇒
            log.error("registerViolationsJob config not found at {}", configPath)
            stopRunning(false, false)
          case Some(config) ⇒
            context.watch(registerViolations)
            val (dayStart, dayEnd) = DayUtility.calculateDayPeriod(actionDate)
            log.debug("Send the ExportViolations message")
            registerViolations ! ExportViolations(StatsDuration(dayStart, DayUtility.fileExportTimeZone),
              actionDate,
              config.exportDirectoryPath,
              LoadSystemConfiguration(systemId, dayStart, dayEnd, DayUtility.fileExportTimeZone, curator))
        }
      } else {
        log.error("{} received non-matching message {}", self.path, mps)
      }
    }

    case mps @ QualificationStatus(false, actionDate) ⇒
      if (actionDate == currentRun.get.actionDate) {
        log.info("{} waiting for violation qualification to complete for {}", self.path, actionDate)
        stopRunning(false)
      } else {
        log.error("{} received non-matching message {}", self.path, mps)
      }

    case ExportViolationsFailed(reason) ⇒
      log.error("registering violations for {} failed for reason: {}", systemId, reason)
      context.unwatch(registerViolations)
      stopRunning(false)

    case ExportViolationsCompleted ⇒
      log.info("registering violations for {} completed", systemId)
      context.unwatch(registerViolations)
      stopRunning(true)

    case Terminated(`registerViolations`) ⇒
      log.error("registering violations for {} crashed", systemId)
      context.unwatch(registerViolations)
      stopRunning(false)
  }

  def stopRunning(completed: Boolean, rerun: Boolean = true) {
    val run = currentRun.get
    if (completed) {
      run.manageEnforceStatus ! RegistrationCompleted(run.actionDate)
      run.runner ! JobComplete
    } else {
      run.runner ! JobFailed(retry = rerun)
    }
    currentRun = None
    context.unbecome()
  }

  override def supervisorStrategy() = AllForOneStrategy() {
    case e: Exception ⇒
      log.error(e, "Registering violations crashed. Scheduling re-run.")
      currentRun match {
        case Some(run) ⇒
          run.runner ! JobFailed(retry = true)
        case None ⇒
          ()
      }
      Stop
  }

  override def postStop() {
    try {
      closeTables()
    } finally {
      super.postStop()
    }
  }
}

case class RegisterViolationsJobConfig(exportDirectoryPath: String)
