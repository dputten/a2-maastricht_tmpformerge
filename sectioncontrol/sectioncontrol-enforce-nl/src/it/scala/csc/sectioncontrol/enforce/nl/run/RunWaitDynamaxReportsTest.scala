/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.enforce.nl.run

import java.io.File
import java.text.SimpleDateFormat
import java.util.{Calendar, Date}

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.testkit.TestProbe
import akka.util.duration._
import csc.akkautils._
import csc.config.Path
import csc.curator.CuratorTestServer
import csc.curator.utils.Curator
import csc.sectioncontrol.enforce.nl.CuratorHelper
import csc.sectioncontrol.enforce.speedlog.{GenerateExcludeEvents, GeneratedExcludeEvents}
import csc.sectioncontrol.enforce.{EnforceBoot, ZkDynamaxIface}
import csc.sectioncontrol.messages.EsaProviderType
import csc.sectioncontrol.storage._
import org.scalatest.matchers.MustMatchers
import org.scalatest.{BeforeAndAfterAll, WordSpec}

import scala.collection.mutable.ListBuffer

class RunWaitDynamaxReportsTest extends WordSpec with MustMatchers with CuratorTestServer
  with BeforeAndAfterAll {
  val systemId = "A2-test"
  implicit val testSystem = ActorSystem(systemId)
  private var curator: Curator = _

  val rootDir = {
    val uri = getClass.getResource("dynamax_hhm.xsd")
    val file = if (uri != null) uri.getPath else "target/dynamax_hhm.xsd"
    new File(file).getParent
  }
  lazy val manageEnforceStatus = TestProbe()

  val speedLog = testSystem.actorOf(Props(new SpeedlogDummy), EnforceBoot.SpeedLogName)

  val now = Calendar.getInstance()
  val yesterday = {
    val tmp = Calendar.getInstance()
    tmp.setTime(new Date(now.getTimeInMillis - 1.day.toMillis))
    tmp
  }
  val dateFormat = new SimpleDateFormat("yyyyMMdd")

  val file100 = {
    new File(rootDir + "/100/%sdymamax.txt".format(dateFormat.format(yesterday.getTime)))
  }
  val file200 = {
    new File(rootDir + "/200/%sdymamax.txt".format(dateFormat.format(yesterday.getTime)))
  }
  def getSpeedLog(SystemId: String): ActorRef = {
    speedLog
  }
  def getExcludeLog(SystemId: String): ActorRef = {
    testSystem.deadLetters //isn't really used only to create a message
  }

  def waitActor = testSystem.actorOf(Props(new RunWaitForDynamaxReports(
    systemId = systemId,
    curator = curator,
    storeMtmData = manageEnforceStatus.ref,
    timeZone = "CET",
    filePickupDelay = 5.seconds.toMillis,
    getSpeedLog = getSpeedLog,
    getExcludeLog = getExcludeLog)))

  def saveZk[T <: AnyRef](p: String, v: T) {
    curator.put(Path(p), v)
  }

  override def beforeEach() {
    super.beforeEach()
    curator = CuratorHelper.create(this, clientScope)
    val ifaceList = new ListBuffer[ZkDynamaxIface]()
    ifaceList += new ZkDynamaxIface(id = "100", uri = "URI", checksumApp = "", checksumCfg = "", timeoutmSec = 0, maxCacheUse = 0, reportFile = "'" + rootDir + "/100/'yyyyMMdd'dymamax.txt'")
    ifaceList += new ZkDynamaxIface(id = "200", uri = "URI", checksumApp = "", checksumCfg = "", timeoutmSec = 0, maxCacheUse = 0, reportFile = "'" + rootDir + "/200/'yyyyMMdd'dymamax.txt'")
    ifaceList += new ZkDynamaxIface(id = "300", uri = "URI", checksumApp = "", checksumCfg = "", timeoutmSec = 0, maxCacheUse = 0, reportFile = "'" + rootDir + "/300/'yyyyMMdd'dymamax.txt'")

    saveZk(Paths.Configuration.getDynamaxIfacePath, ifaceList)
    saveZk(Paths.Systems.getConfigPath(systemId), new ZkSystem(
      id = systemId,
      name = "Test",
      title = "Test",
      mtmRouteId = "",
      violationPrefixId = "",
      location = ZkSystemLocation(
        description = "",
        region = "",
        viewingDirection = Some(""),
        roadNumber = "",
        roadPart = "",
        systemLocation = ""),
      serialNumber = SerialNumber(""),
      orderNumber = 0,
      maxSpeed = 0,
      compressionFactor = 0,
      retentionTimes = ZkSystemRetentionTimes(
        nrDaysKeepTrafficData = 0,
        nrDaysKeepViolations = 0,
        nrDaysRemindCaseFiles = 0),
      pardonTimes = ZkSystemPardonTimes(pardonTimeEsa_secs = 0, pardonTimeTechnical_secs = 0),
      esaProviderType = EsaProviderType.Dynamax))
    //create 2 corridors
    saveZk(Paths.Corridors.getConfigPath(systemId, "cor1"), new ZkCorridor(
      id = "cor1",
      name = "",
      roadType = 0,
      isInsideUrbanArea = false,
      dynamaxMqId = "100",
      approvedSpeeds = Seq(),
      serviceType = ServiceType.SectionControl,
      pshtm = new ZkPSHTMIdentification(useYear = false,
        useMonth = false,
        useDay = false,
        useReportingOfficerId = false,
        useNumberOfTheDay = false,
        useLocationCode = false,
        useHHMCode = false,
        useTypeViolation = false,
        useFixedCharacter = false,
        fixedCharacter = ""),
      info = new ZkCorridorInfo(corridorId = 100,
        locationCode = "0",
        reportId = "TC UT A2 L-1 (100)",
        reportHtId = "a0020011",
        reportPerformance = true,
        locationLine1 = ""),
      startSectionId = "",
      endSectionId = "",
      allSectionIds = Seq(),
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 402,
      roadCode = 2,
      dutyType = "KA2",
      deploymentCode = "03",
      codeText = ""))
    saveZk(Paths.Corridors.getConfigPath(systemId, "cor2"), new ZkCorridor(
      id = "cor2",
      name = "",
      roadType = 0,
      isInsideUrbanArea = false,
      dynamaxMqId = "200",
      approvedSpeeds = Seq(),
      serviceType = ServiceType.SectionControl,
      pshtm = new ZkPSHTMIdentification(useYear = false,
        useMonth = false,
        useDay = false,
        useReportingOfficerId = false,
        useNumberOfTheDay = false,
        useLocationCode = false,
        useHHMCode = false,
        useTypeViolation = false,
        useFixedCharacter = false,
        fixedCharacter = ""),
      info = new ZkCorridorInfo(corridorId = 100,
        locationCode = "0",
        reportId = "TC UT A2 L-1 (100)",
        reportHtId = "a0020011",
        reportPerformance = true,
        locationLine1 = ""),
      startSectionId = "",
      endSectionId = "",
      allSectionIds = Seq(),
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 402,
      roadCode = 2,
      dutyType = "KA2",
      deploymentCode = "03",
      codeText = ""))
  }

  override protected def beforeAll() {
    super.beforeAll()
    file100.delete()
    file200.delete()
    new File(rootDir + "/100").mkdir()
    new File(rootDir + "/200").mkdir()
  }

  override protected def afterAll() {
    testSystem.shutdown()
    super.afterAll()
  }

  "RunWaitDynamaxReportsTest" must {
    "fail when files are not present" in {
      val probeRunner = TestProbe()
      val probeStatus = TestProbe()
      waitActor ! new Run(
        zkJobPath = "/test",
        runner = probeRunner.ref,
        runToken = CreateRunToken.instance(
          now.getTimeInMillis,
          now.getTimeInMillis,
          "CET"),
        actionDate = yesterday.getTimeInMillis,
        manageEnforceStatus = probeStatus.ref)
      probeRunner.expectMsg(JobFailed(true))
    }
    "fail when files are present but to new" in {
      file100.createNewFile()
      file200.createNewFile()
      val probeRunner = TestProbe()
      val probeStatus = TestProbe()
      val runMsg = new Run(
        zkJobPath = "/test",
        runner = probeRunner.ref,
        runToken = CreateRunToken.instance(System.currentTimeMillis(),
          System.currentTimeMillis(), "CET"),
        actionDate = yesterday.getTimeInMillis,
        manageEnforceStatus = probeStatus.ref)
      waitActor ! runMsg
      probeRunner.expectMsg(JobFailed(true))
      file100.delete()
      file200.delete()
    }
    "succeed when files are present" in {
      file100.createNewFile()
      file200.createNewFile()
      val probeRunner = TestProbe()
      val probeStatus = TestProbe()
      val runMsg = new Run(
        zkJobPath = "/test",
        runner = probeRunner.ref,
        runToken = CreateRunToken.instance(System.currentTimeMillis() + 6000,
          System.currentTimeMillis() + 6000, "CET"),
        actionDate = yesterday.getTimeInMillis,
        manageEnforceStatus = probeStatus.ref)
      waitActor ! runMsg
      probeRunner.expectMsg(JobComplete)
      probeStatus.expectMsg(ProcessingMtmCompleted(runMsg.actionDate, runMsg.runToken.startTime))
      file100.delete()
      file200.delete()
    }
  }
}

class SpeedlogDummy extends Actor {
  def receive = {
    case msg: GenerateExcludeEvents ⇒ {
      sender ! GeneratedExcludeEvents
    }
  }
}
