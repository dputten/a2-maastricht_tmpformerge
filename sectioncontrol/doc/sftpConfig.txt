Configuration Unix system

change line in file /etc/ssh/sshd_config
#Subsystem sftp /usr/lib/openssh/sftp-server
Subsystem sftp /usr/lib/openssh/sftp-server -f LOCAL5 -l INFO

add lines in file /etc/rsyslog.d/50-default.conf
#sftp logging
local5. |/var/log/sftpd-fifo

create pipe: mkfifo /var/log/sftpd-fifo
change permissions: chmod 644 /var/log/sftpd-fifo

//restart sshd and rsyslog
service ssh stop
service ssh start
service rsyslog stop
service rsyslog start
