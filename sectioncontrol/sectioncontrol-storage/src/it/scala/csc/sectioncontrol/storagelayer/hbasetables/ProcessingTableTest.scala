/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.hbasetables

import java.text.SimpleDateFormat

import csc.hbase.utils.HBaseTableKeeper
import csc.hbase.utils.testframework.HBaseTestFramework
import csc.sectioncontrol.messages._
import csc.sectioncontrol.storage.HBaseTableDefinitions._
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.util.Bytes
import org.scalatest.matchers.MustMatchers
import org.scalatest.{ BeforeAndAfterEach, BeforeAndAfterAll, WordSpec }

class ProcessingTableTest
  extends WordSpec
  with MustMatchers
  with HBaseTestFramework
  with BeforeAndAfterAll
  with BeforeAndAfterEach {

  val cf = processingColumnFamily
  val tableName = processingTableName
  var table: HTable = _
  val rowKeyDistributor = RowKeyDistributorFactory.getDistributor(tableName)
  val tableKeeper = new HBaseTableKeeper {}
  var reader: ProcessingTable.Reader = _
  var writer: ProcessingTable.Writer = _

  def getHBaseZkServersFromTestFramework: String = {
    hbaseZkServers
  }

  override def beforeAll() {
    super.beforeAll()
    table = testUtil.createTable(tableName, processingColumnFamily)
    reader = ProcessingTable.makeReader(testUtil.getConfiguration, tableKeeper)
    writer = ProcessingTable.makeWriter(testUtil.getConfiguration, tableKeeper)
  }

  override def afterAll() {
    table.close()
    tableKeeper.closeTables()
    super.afterAll()
  }

  override protected def beforeEach(): Unit = {
    truncateTable(table) //important to make sure test cases are fully isolated
  }

  val lane1 = Lane("A2-A-lane3", "lane3", "A", "A2", 45.2, 21.0)
  val lane2 = Lane("A2-B-lane3", "lane3", "B", "A2", 45.2, 21.0)
  val lane3 = Lane("A3-A-lane2", "lane2", "A", "A3", 45.2, 21.0)
  val lane4 = Lane("A4-B-lane1", "lane1", "B", "A4", 45.2, 21.0)

  def createRecord(lane: Lane, time: Long = System.currentTimeMillis()): ProcessingRecord = {
    GantryTimeProcessingRecord.create(lane, time)
  }

  "ProcessingReader" must {

    "read back the same data that was written" in {
      val record = createRecord(lane1)
      writer.writeRecord(lane1, record)
      val result = reader.getRecord(lane1, record.time)
      assert(result === Some(record))
    }
    "read keepalive back what was written" in {
      val keepalive = createRecord(lane1)
      writer.writeGantryTimeRecord(lane1, keepalive)
      val result = reader.getGantryTimeRecord(lane1)
      assert(result === Some(keepalive))
    }
    "found gantry time records for given gantry" in {
      val record1 = createRecord(lane1, 8000)
      val record2 = createRecord(lane2, 9000)
      val record3 = createRecord(lane3, 10000)
      val record4 = createRecord(lane1, 11000)
      writer.writeRecord(lane1, record1)
      writer.writeGantryTimeRecord(lane2, record2)
      writer.writeGantryTimeRecord(lane3, record3)
      writer.writeGantryTimeRecord(lane1, record4)
      val results = reader.getGantryTimes(lane1.system, lane1.gantry)
      assert(results.size === 2)
      assert(results(0) === record1)
      assert(results(1) === record4)
    }
    "overwrite gantry time records" in {
      val record1 = createRecord(lane2, 7000)
      writer.writeGantryTimeRecord(lane2, record1)

      val record2 = createRecord(lane2, 20000)
      writer.writeGantryTimeRecord(lane2, record2)

      val results = reader.getGantryTimes(lane2.system, lane2.gantry)

      assert(results === List(record2))
    }

    "read all gantry processed records, regardless of system or gantry" in {
      val record1 = createRecord(lane1, 7000)
      writer.writeGantryTimeRecord(lane1, record1)

      val record2 = createRecord(lane2, 12000)
      writer.writeGantryTimeRecord(lane2, record2)

      val record3 = createRecord(lane3, 16000)
      writer.writeGantryTimeRecord(lane3, record3)

      val record4 = createRecord(lane4, 20000)
      writer.writeGantryTimeRecord(lane4, record4)

      //a record of different component, to make sure its filtered out
      val record5 = ProcessingRecord("OtherComponent", "state", System.currentTimeMillis(), Nil)
      writer.writeRecord(lane1, record5)

      val results = reader.getAllGantryTimes()
      assert(results === List(record1, record2, record3, record4))
    }

  }
}