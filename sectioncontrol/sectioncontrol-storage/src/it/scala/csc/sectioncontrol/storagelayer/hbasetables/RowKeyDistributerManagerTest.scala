/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.hbasetables

import csc.akkautils.DirectLogging
import csc.curator.CuratorTestServer
import csc.curator.utils.CuratorToolsImpl
import csc.hbase.utils.NoDistributionOfRowKey
import csc.sectioncontrol.storage.Paths
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

class RowKeyDistributerManagerTest extends WordSpec with MustMatchers with CuratorTestServer with DirectLogging {

  var zookeeperClient: CuratorToolsImpl = _

  override def beforeEach() {
    super.beforeEach()
    zookeeperClient = new CuratorToolsImpl(clientScope, log)
  }
  "Manager" must {
    "create rowKeys when config is empty" in {
      RowKeyDistributorFactory.clear()
      val dist1 = RowKeyDistributorFactory.getDistributor("table1")
      dist1.getParamsToStore must be("com.sematext.hbase.wd.RowKeyDistributorByHashPrefix$OneByteSimpleHash--10")
      val dist2 = RowKeyDistributorFactory.getDistributor("table1")
      dist2 must be(dist1)
    }
    "read config" in {
      RowKeyDistributorFactory.clear()
      val config = Map("table1" -> 3,
        "table2" -> 0)
      zookeeperClient.put(Paths.Configuration.getHBaseTablesRowKeys, config)
      RowKeyDistributorFactory.init(zookeeperClient)
      val dist1 = RowKeyDistributorFactory.getDistributor("table1")
      dist1.getParamsToStore must be("com.sematext.hbase.wd.RowKeyDistributorByHashPrefix$OneByteSimpleHash--3")
      val dist2 = RowKeyDistributorFactory.getDistributor("table2")
      dist2.getClass must be((new NoDistributionOfRowKey).getClass)
    }
  }

}