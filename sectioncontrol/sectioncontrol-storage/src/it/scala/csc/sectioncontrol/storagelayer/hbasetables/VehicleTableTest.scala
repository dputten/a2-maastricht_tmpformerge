/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.hbasetables

import java.text.SimpleDateFormat

import csc.hbase.utils.HBaseTableKeeper
import csc.hbase.utils.testframework.HBaseTestFramework
import csc.sectioncontrol.messages.{Lane, ValueWithConfidence, VehicleMetadata}
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.util.Bytes
import org.scalatest.matchers.MustMatchers
import org.scalatest.{BeforeAndAfterAll, WordSpec}

class VehicleTableTest extends WordSpec with MustMatchers with HBaseTestFramework with BeforeAndAfterAll {
  val formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
  val tableName = "vehicle"
  var table: HTable = _
  val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(tableName)
  val tableKeeper = new HBaseTableKeeper {}
  var reader: VehicleTable.Reader = _
  var writer: VehicleTable.Writer = _
  val systemId = "A2"
  val gantryId = "X1"

  def getHBaseZkServersFromTestFramework: String = {
    hbaseZkServers
  }

  override def beforeAll() {
    super.beforeAll()
    table = testUtil.createTable(tableName, "cf")
    reader = VehicleTable.makeReader(testUtil.getConfiguration, tableKeeper)
    writer = VehicleTable.makeWriter(testUtil.getConfiguration, tableKeeper)
    addVehicles()
  }

  override def afterAll() {
    table.close()
    tableKeeper.closeTables()
    super.afterAll()
  }

  "VehicleReader" must {
    "filter using full license should ignore empty (space) license (TCU-174)" in {
      val timeStart = formatter.parse("2014-12-01T01:00:00").getTime
      val timeEnd = formatter.parse("2014-12-01T06:00:00").getTime
      val license = "5678XY"
      val results = reader.readRows(systemId, gantryId, license, timeStart, timeEnd)
      results.size must be(1)
      results(0).license.get.value must be(license)
    }

    "filter using full license should ignore empty (None) license (TCU-174)" in {
      val timeStart = formatter.parse("2014-12-01T01:00:00").getTime
      val timeEnd = formatter.parse("2014-12-01T06:00:00").getTime
      val license = "5678XY"
      val results = reader.readRows(systemId, gantryId, license, timeStart, timeEnd)
      results.size must be(1)
      results(0).license.get.value must be(license)
    }
    "filter using full license" in {
      val timeStart = formatter.parse("2014-12-01T01:00:00").getTime
      val timeEnd = formatter.parse("2014-12-01T06:00:00").getTime
      val license = "1234AB"
      val results = reader.readRows(systemId, gantryId, license, timeStart, timeEnd)
      results.size must be(2)
      results(0).license.get.value must be(license)
      results(1).license.get.value must be(license)
    }
    "filter using partly license" in {
      val timeStart = formatter.parse("2014-12-01T01:00:00").getTime
      val timeEnd = formatter.parse("2014-12-01T06:00:00").getTime
      val license = "XY"
      val results = reader.readRows(systemId, gantryId, license, timeStart, timeEnd)
      results.size must be(2)
      results(0).license.get.value.contains(license) must be(true)
      results(1).license.get.value.contains(license) must be(true)
    }
    "return empty when using none existing license" in {
      val timeStart = formatter.parse("2014-12-01T01:00:00").getTime
      val timeEnd = formatter.parse("2014-12-01T06:00:00").getTime
      val license = "1111XY"
      val results = reader.readRows(systemId, gantryId, license, timeStart, timeEnd)
      results.size must be(0)
    }
    "filter using full license and time" in {
      val timeStart = formatter.parse("2014-12-01T01:00:00").getTime
      val timeEnd = formatter.parse("2014-12-01T04:00:00").getTime
      val license = "1234AB"
      val results = reader.readRows(systemId, gantryId, license, timeStart, timeEnd)
      results.size must be(1)
      results(0).license.get.value must be(license)
      results(0).eventTimestamp must be(time1)
      results(0).applChecksum must be(car1.applChecksum)
      results(0).eventId must be(car1.eventId)
      results(0).lane must be(car1.lane)
      results(0).serialNr must be(car1.serialNr)
      results(0).country must be(None)
    }
    "return all when using no license" in {
      val timeStart = formatter.parse("2014-12-01T01:00:00").getTime
      val timeEnd = formatter.parse("2014-12-01T06:00:00").getTime
      val results = reader.readRows(systemId, gantryId, "", timeStart, timeEnd)
      results.size must be(6)
    }
  }

  def addVehicles() {
    writer.writeMetadata(car1)
    writer.writeMetadataRows(Seq(car2, car3, car4, car5, car6))
  }

  def createKey(time: Long, systemId: String, gantryId: String, laneId: String): Array[Byte] = {
    val origKey = Bytes.toBytes("%s-%s-%s-%s".format(systemId, gantryId, time.toString, laneId))
    rowKeyDistributer.getDistributedKey(origKey)
  }

  val lane1 = Lane(laneId = "A2-X1-R1", name = "R1", gantry = "X1", system = "A2", sensorGPS_longitude = 52, sensorGPS_latitude = 5.4)

  val time1str = "2014-12-01T02:00:00"
  val time1 = formatter.parse(time1str).getTime
  val car1 = VehicleMetadata(lane = lane1, eventId = "A2-X1-R1-123451", eventTimestamp = time1, eventTimestampStr = Some(time1str),
    license = Some(ValueWithConfidence("1234AB", 40)), NMICertificate = "NMI-cert", applChecksum = "1234abcde1",
    serialNr = "11")

  val time2str = "2014-12-01T03:00:00"
  val time2 = formatter.parse(time2str).getTime
  val car2 = VehicleMetadata(lane = lane1, eventId = "A2-X1-R1-123452", eventTimestamp = time2, eventTimestampStr = Some(time2str),
    license = Some(ValueWithConfidence("XY1234", 40)), NMICertificate = "NMI-cert", applChecksum = "1234abcde2",
    serialNr = "12")

  val time3str = "2014-12-01T04:00:00"
  val time3 = formatter.parse(time3str).getTime
  val car3 = VehicleMetadata(lane = lane1, eventId = "A2-X1-R1-123453", eventTimestamp = time3, eventTimestampStr = Some(time3str),
    license = Some(ValueWithConfidence("1234AB", 40)), NMICertificate = "NMI-cert", applChecksum = "1234abcde3",
    serialNr = "13")

  val time4str = "2014-12-01T05:00:00"
  val time4 = formatter.parse(time4str).getTime
  val car4 = VehicleMetadata(lane = lane1, eventId = "A2-X1-R1-123454", eventTimestamp = time4, eventTimestampStr = Some(time4str),
    license = Some(ValueWithConfidence("5678XY", 40)), NMICertificate = "NMI-cert", applChecksum = "1234abcde4",
    serialNr = "14")

  //val time5str = "2014-12-01T05:00:00" dit zorgt voor fouten
  val time5str = "2014-12-01T05:00:01"
  val time5 = formatter.parse(time5str).getTime
  val car5 = VehicleMetadata(lane = lane1, eventId = "A2-X1-R1-123666", eventTimestamp = time5, eventTimestampStr = Some(time5str),
    license = None, NMICertificate = "NMI-cert", applChecksum = "1234abcde4",
    serialNr = "15")

  val time6str = "2014-12-01T05:00:02"
  val time6 = formatter.parse(time6str).getTime
  val car6 = VehicleMetadata(lane = lane1, eventId = "A2-X1-R1-123667", eventTimestamp = time6, eventTimestampStr = Some(time6str),
    license = Some(ValueWithConfidence("", 40)), NMICertificate = "NMI-cert", applChecksum = "1234abcde4",
    serialNr = "16")

}