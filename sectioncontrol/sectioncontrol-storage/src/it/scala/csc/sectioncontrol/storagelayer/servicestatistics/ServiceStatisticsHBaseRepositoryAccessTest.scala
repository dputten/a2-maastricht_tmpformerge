/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.servicestatistics

import java.util.TimeZone

import akka.actor.ActorSystem
import akka.testkit.TestKit
import akka.util.duration._
import csc.hbase.utils.testframework.HBaseTestFramework
import csc.sectioncontrol.messages.matcher.MatcherCorridorConfig
import csc.sectioncontrol.messages.{Lane, StatsDuration}
import csc.sectioncontrol.storage.{LaneInfo, RedLightStatistics, SectionControlStatistics, StatisticsPeriod, _}
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.util.Bytes
import org.scalatest.matchers.MustMatchers
import org.scalatest.{BeforeAndAfterAll, WordSpec}

/**
 * Test the ServiceStatisticsHBaseRepositoryAccess trait.
 *
 * Please note:
 * There already is a comprehensive test for ServiceStatisticsHBaseRepositoryActor, so we don't want to reproduce this
 * here. Instead we only want to test the link between this trait and the internal ServiceStatisticsHBaseRepositoryActor.
 */
class ServiceStatisticsHBaseRepositoryAccessTest extends TestKit(ActorSystem("ServiceStatisticsHBaseRepositoryActorTest"))
  with WordSpec with MustMatchers with HBaseTestFramework with BeforeAndAfterAll {

  val serviceStatisticsTableName = "ServiceStatistics"
  var serviceStatisticsTable: HTable = _

  val timeZone = TimeZone.getTimeZone("Europe/Amsterdam")

  val system1 = "sys1"
  val today = "20131030"

  var incSeed = 0

  implicit def string2ByteArray(s: String): Array[Byte] = Bytes.toBytes(s)

  def getHBaseZkServersFromTestFramework: String = {
    hbaseZkServers
  }

  override def beforeAll() {
    super.beforeAll()
    serviceStatisticsTable = testUtil.createTable(serviceStatisticsTableName, "cf")
  }

  override def afterAll() {
    serviceStatisticsTable.close()
    Thread.sleep(100)
    testUtil.deleteTable(serviceStatisticsTableName)
    testUtil
    super.afterAll()
  }

  def lane1(systemId: String) = LaneInfo(
    lane = Lane(
      laneId = "lane1",
      name = "Sunny Side",
      gantry = "gan3",
      system = systemId,
      sensorGPS_longitude = 32.45,
      sensorGPS_latitude = 67.89),
    count = 4)
  def matcherConfig(systemId: String) = MatcherCorridorConfig(
    systemId = systemId,
    zkCorridorId = "cor1",
    id = 321,
    entry = "ingang",
    exit = "uitgang",
    distance = 110,
    deltaForDoubleShoot = 23,
    followDistance = 1.hour.toMillis)

  "A ServiceStatisticsHBaseRepositoryAccess instance" must {
    "correctly interact with the internal ServiceStatisticsHBaseRepositoryActor" in {
      // Create instance of ServiceStatisticsHBaseRepositoryAccess trait, providing hbaseConfigs and actorSystem.
      val testRepositoryAccess = new ServiceStatisticsHBaseRepositoryAccess {
        protected val hbaseConfig: Configuration = {
          testUtil.getConfiguration
        }
        protected val actorSystem = system
      }

      //=== RedLightStatistics
      val result1 = testRepositoryAccess.getRedLightStatistics(systemId = system1, statsDuration = StatsDuration(today, timeZone))
      result1 must be(Seq())

      val redLightStatistics1 = createRedLightStatistics(systemId = system1, day = today, periodEnd = 000000000000010L, seed = 1)
      testRepositoryAccess.save(systemId = system1, statistics = redLightStatistics1)

      val result2 = testRepositoryAccess.getRedLightStatistics(systemId = system1, statsDuration = StatsDuration(today, timeZone))
      result2 must be(Seq(redLightStatistics1))

      //=== SectionControlStatistics
      val result3 = testRepositoryAccess.getSectionControlStatistics(systemId = system1, statsDuration = StatsDuration(today, timeZone))
      result3 must be(Seq())

      val sectionControlStatistics1 = createSectionControlStatistics(systemId = system1, day = today, periodEnd = 000000000001010L, seed = 1)
      testRepositoryAccess.save(systemId = system1, statistics = sectionControlStatistics1)

      val result4 = testRepositoryAccess.getSectionControlStatistics(systemId = system1, statsDuration = StatsDuration(today, timeZone))
      result4 must be(Seq(sectionControlStatistics1))

      //=== SpeedFixedStatistics
      val result5 = testRepositoryAccess.getSpeedFixedStatistics(systemId = system1, statsDuration = StatsDuration(today, timeZone))
      result5 must be(Seq())

      val speedFixedStatistics1 = createSpeedFixedStatistics(systemId = system1, day = today, periodEnd = 000000000002010L, seed = 1)
      testRepositoryAccess.save(systemId = system1, statistics = speedFixedStatistics1)

      val result6 = testRepositoryAccess.getSpeedFixedStatistics(systemId = system1, statsDuration = StatsDuration(today, timeZone))
      result6 must be(Seq(speedFixedStatistics1))
    }

  }

  def createSectionControlStatistics(systemId: String, day: String, periodEnd: Long, seed: Int = 0): SectionControlStatistics = {
    SectionControlStatistics(
      period = StatisticsPeriod(
        stats = StatsDuration(day, timeZone),
        end = periodEnd,
        begin = periodEnd - 10),
      config = matcherConfig(systemId),
      input = 23 + seed,
      matched = 20 + seed,
      unmatched = 3 + seed,
      doubles = 0 + seed,
      averageSpeed = 43.5 + seed,
      highestSpeed = 78 + seed,
      laneStats = List(lane1(systemId)))
  }

  def createRedLightStatistics(systemId: String, day: String, periodEnd: Long, seed: Int = 0): RedLightStatistics = {
    RedLightStatistics(
      period = StatisticsPeriod(
        stats = StatsDuration(day, timeZone),
        end = periodEnd,
        begin = periodEnd - 10),
      nrRegistrations = 23 + seed,
      nrWithRedLight = 20 + seed,
      averageRedLight = 1.5 + seed,
      highestRedLight = 2 + seed,
      laneStats = List(lane1(systemId)))
  }

  def createSpeedFixedStatistics(systemId: String, day: String, periodEnd: Long, seed: Int = 0): SpeedFixedStatistics = {
    SpeedFixedStatistics(
      period = StatisticsPeriod(
        stats = StatsDuration(day, timeZone),
        end = periodEnd,
        begin = periodEnd - 10),
      nrRegistrations = 23 + seed,
      nrSpeedMeasurement = 20 + seed,
      averageSpeed = 41.5 + seed,
      highestSpeed = 65 + seed,
      laneStats = List(lane1(systemId)))
  }

}
