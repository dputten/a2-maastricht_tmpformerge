/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.servicestatistics

import java.util.TimeZone

import akka.actor.ActorSystem
import akka.dispatch.Await
import akka.pattern.ask
import akka.testkit.{TestActorRef, TestKit}
import akka.util.Timeout
import akka.util.duration._
import csc.hbase.utils.HBaseTableKeeper
import csc.hbase.utils.testframework.HBaseTestFramework
import csc.sectioncontrol.messages.{Lane, StatsDuration}
import csc.sectioncontrol.messages.matcher.MatcherCorridorConfig
import csc.sectioncontrol.storage.{LaneInfo, RedLightStatistics, SectionControlStatistics, SpeedFixedStatistics, StatisticsPeriod, _}
import csc.sectioncontrol.storagelayer.hbasetables.StatisticsTable
import csc.sectioncontrol.storagelayer.hbasetables.StatisticsTable.ServiceStatisticsKey
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.util.Bytes
import org.scalatest.matchers.MustMatchers
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, WordSpec}

/**
 * Test the ServiceStatisticsHBaseRepositoryActor actor.
 */
class ServiceStatisticsHBaseRepositoryActorTest extends TestKit(ActorSystem("ServiceStatisticsHBaseRepositoryActorTest"))
  with WordSpec with MustMatchers with HBaseTestFramework with BeforeAndAfterEach with BeforeAndAfterAll {

  import ServiceStatisticsHBaseRepositoryActor._

  val systemId = "sys123"
  val timeZone = TimeZone.getTimeZone("Europe/Amsterdam")

  implicit def string2ByteArray(s: String): Array[Byte] = Bytes.toBytes(s)

  val serviceStatisticsTableName = HBaseTableDefinitions.serviceStatisticsTableName
  var serviceStatisticsTable: HTable = _
  val tables = new HBaseTableKeeper() {}

  var testRepositoryActor: TestActorRef[ServiceStatisticsHBaseRepositoryActor] = _

  override def beforeAll() {
    super.beforeAll()
    serviceStatisticsTable = testUtil.createTable(serviceStatisticsTableName, "cf")
  }

  override def afterAll() {
    tables.closeTables()
    Thread.sleep(100)
    testUtil.deleteTable(serviceStatisticsTableName)
    super.afterAll()
  }

  override protected def beforeEach() {
    super.beforeEach()
    testRepositoryActor = TestActorRef(new ServiceStatisticsHBaseRepositoryActor(testUtil.getConfiguration))
  }

  override protected def afterEach() {
    testRepositoryActor.stop()
    Thread.sleep(100)
    testUtil.truncateTable(serviceStatisticsTableName)
    super.afterEach()
  }

  val lane1 = LaneInfo(
    lane = Lane(
      laneId = "lane1",
      name = "Sunny Side",
      gantry = "gan3",
      system = systemId,
      sensorGPS_longitude = 32.45,
      sensorGPS_latitude = 67.89),
    count = 4)
  val matcherConfig = MatcherCorridorConfig(
    systemId = systemId,
    zkCorridorId = "cor1",
    id = 321,
    entry = "ingang",
    exit = "uitgang",
    distance = 110,
    deltaForDoubleShoot = 23,
    followDistance = 1.hour.toMillis)

  "A ServiceStatisticsHBaseRepositoryActor (table contains SectionControlStatistics only)" when {

    "receiving a GetServiceStatistics(SectionControlStatisticsType) message (and empty table)" must {
      "return Seq() in a SectionControlStatisticsResult message" in {
        testUtil.countRows(serviceStatisticsTable) must be(0)

        implicit val timeout = Timeout(5 seconds)
        val future = testRepositoryActor ? GetServiceStatistics(systemId, StatsDuration(key = "20131030", timezone = timeZone), SectionControlStatisticsType)
        val result = Await.result(future, timeout.duration).asInstanceOf[SectionControlStatisticsResult]

        result.systemId must be(systemId)
        result.duration must be(StatsDuration(key = "20131030", timezone = timeZone))
        result.statistics must be(Seq())
      }
    }

    "receiving a SaveServiceStatistics(SectionControlStatistics) message (new rowkey)" must {
      "store this data in the ServiceStatistics table ('cf', 'SC')" in {
        val sectionControlStatistics = createSectionControlStatistics(day = "20131030", periodEnd = 123450999)

        testUtil.countRows(serviceStatisticsTable) must be(0)
        testRepositoryActor ! SaveServiceStatistics(systemId, sectionControlStatistics)
        testUtil.countRows(serviceStatisticsTable) must be(1)

        val testReader = StatisticsTable.createTcsReader(hbaseConfig, tables)
        val columnValueFromTable = testReader.readRow(StringKey("sys123-20131030-000000123450999-cor1"))

        columnValueFromTable must be(Some(sectionControlStatistics))
      }
    }

    "receiving a SaveServiceStatistics(SectionControlStatistics) message (existing rowkey)" must {
      "store this data in the ServiceStatistics table ('cf', 'SC'), replacing the existing data" in {
        val sectionControlStatistics1 = createSectionControlStatistics(day = "20131030", periodEnd = 123450999)

        testUtil.countRows(serviceStatisticsTable) must be(0)
        testRepositoryActor ! SaveServiceStatistics(systemId, sectionControlStatistics1)
        testUtil.countRows(serviceStatisticsTable) must be(1)

        val testReader = StatisticsTable.createTcsReader(hbaseConfig, tables)
        val columnValueFromTable1 = testReader.readRow(StringKey("sys123-20131030-000000123450999-cor1"))
        columnValueFromTable1 must be(Some(sectionControlStatistics1))

        val sectionControlStatistics2 = createSectionControlStatistics(day = "20131030", periodEnd = 123450999, seed = 2)

        testUtil.countRows(serviceStatisticsTable) must be(1)
        testRepositoryActor ! SaveServiceStatistics(systemId, sectionControlStatistics2)
        testUtil.countRows(serviceStatisticsTable) must be(1)

        val columnValueFromTable2 = testReader.readRow(StringKey("sys123-20131030-000000123450999-cor1"))

        columnValueFromTable2 must be(Some(sectionControlStatistics2))
      }
    }

    "receiving a GetServiceStatistics(SectionControlStatisticsType) message (and data exists)" must {
      "return requested data in a SectionControlStatisticsResult message" in {
        val sectionControlStatistics = createSectionControlStatistics(day = "20131030", periodEnd = 123450999)

        testUtil.countRows(serviceStatisticsTable) must be(0)
        testRepositoryActor ! SaveServiceStatistics(systemId, sectionControlStatistics)
        testUtil.countRows(serviceStatisticsTable) must be(1)

        implicit val timeout = Timeout(5 seconds)
        val future = testRepositoryActor ? GetServiceStatistics(systemId, StatsDuration(key = "20131030", timezone = timeZone), SectionControlStatisticsType)
        val result = Await.result(future, timeout.duration).asInstanceOf[SectionControlStatisticsResult]

        result.systemId must be(systemId)
        result.duration must be(StatsDuration(key = "20131030", timezone = timeZone))
        result.statistics must be(Seq(sectionControlStatistics))
      }
    }

    "receiving a GetServiceStatistics(SectionControlStatisticsType) message (and data does not exist)" must {
      "return Seq() in a SectionControlStatisticsResult message" in {
        val sectionControlStatistics = createSectionControlStatistics(day = "20131030", periodEnd = 123450999)

        testUtil.countRows(serviceStatisticsTable) must be(0)
        testRepositoryActor ! SaveServiceStatistics(systemId, sectionControlStatistics)
        testUtil.countRows(serviceStatisticsTable) must be(1)

        implicit val timeout = Timeout(5 seconds)
        val future = testRepositoryActor ? GetServiceStatistics(systemId, StatsDuration(key = "20131031", timezone = timeZone), SectionControlStatisticsType)
        val result = Await.result(future, timeout.duration).asInstanceOf[SectionControlStatisticsResult]

        result.systemId must be(systemId)
        result.duration must be(StatsDuration(key = "20131031", timezone = timeZone))
        result.statistics must be(Seq())
      }
    }

    "receiving a GetServiceStatistics(RedLightStatisticsType) message (and only SectionControlStatistics exists)" must {
      "return Seq() in a RedLightStatisticsResult message" in {
        val sectionControlStatistics = createSectionControlStatistics(day = "20131030", periodEnd = 123450999)

        testUtil.countRows(serviceStatisticsTable) must be(0)
        testRepositoryActor ! SaveServiceStatistics(systemId, sectionControlStatistics)
        testUtil.countRows(serviceStatisticsTable) must be(1)

        implicit val timeout = Timeout(5 seconds)
        val future = testRepositoryActor ? GetServiceStatistics(systemId, StatsDuration(key = "20131030", timezone = timeZone), RedLightStatisticsType)
        val result = Await.result(future, timeout.duration).asInstanceOf[RedLightStatisticsResult]

        result.systemId must be(systemId)
        result.duration must be(StatsDuration(key = "20131030", timezone = timeZone))
        result.statistics must be(Seq())
      }
    }

    "receiving a GetServiceStatistics(SpeedFixedStatisticsType) message (and only SectionControlStatistics exists)" must {
      "return Seq() in a SpeedFixedStatisticsResult message" in {
        val sectionControlStatistics = createSectionControlStatistics(day = "20131030", periodEnd = 123450999)

        testUtil.countRows(serviceStatisticsTable) must be(0)
        testRepositoryActor ! SaveServiceStatistics(systemId, sectionControlStatistics)
        testUtil.countRows(serviceStatisticsTable) must be(1)

        implicit val timeout = Timeout(5 seconds)
        val future = testRepositoryActor ? GetServiceStatistics(systemId, StatsDuration(key = "20131030", timezone = timeZone), SpeedFixedStatisticsType)
        val result = Await.result(future, timeout.duration).asInstanceOf[SpeedFixedStatisticsResult]

        result.systemId must be(systemId)
        result.duration must be(StatsDuration(key = "20131030", timezone = timeZone))
        result.statistics must be(Seq())
      }
    }

    "receiving a GetServiceStatistics(SectionControlStatisticsType) message" must {
      "return a SectionControlStatisticsResult message containing the data for given systemId and day" in {
        val system1 = "sys1"
        val system2 = "sys2"
        val system3 = "sys3"
        val yesterday = "20131029"
        val today = "20131030"
        val tomorrow = "20131031"
        val sectionControlStatistics1 = createSectionControlStatistics(day = yesterday, periodEnd = 555555555555555L)
        val sectionControlStatistics2 = createSectionControlStatistics(day = yesterday, periodEnd = 999999999999999L)
        val sectionControlStatistics3 = createSectionControlStatistics(day = today, periodEnd = 000000000000010L)
        val sectionControlStatistics4 = createSectionControlStatistics(day = today, periodEnd = 555555555555555L)
        val sectionControlStatistics5 = createSectionControlStatistics(day = today, periodEnd = 999999999999999L)
        val sectionControlStatistics6 = createSectionControlStatistics(day = tomorrow, periodEnd = 000000000000010L)
        val sectionControlStatistics7 = createSectionControlStatistics(day = tomorrow, periodEnd = 555555555555555L)

        testUtil.countRows(serviceStatisticsTable) must be(0)
        testRepositoryActor ! SaveServiceStatistics(system1, sectionControlStatistics1)
        testRepositoryActor ! SaveServiceStatistics(system1, sectionControlStatistics2)
        testRepositoryActor ! SaveServiceStatistics(system1, sectionControlStatistics3)
        testRepositoryActor ! SaveServiceStatistics(system1, sectionControlStatistics4)
        testRepositoryActor ! SaveServiceStatistics(system1, sectionControlStatistics5)
        testRepositoryActor ! SaveServiceStatistics(system1, sectionControlStatistics6)
        testRepositoryActor ! SaveServiceStatistics(system1, sectionControlStatistics7)

        testRepositoryActor ! SaveServiceStatistics(system2, sectionControlStatistics1)
        testRepositoryActor ! SaveServiceStatistics(system2, sectionControlStatistics2)
        testRepositoryActor ! SaveServiceStatistics(system2, sectionControlStatistics3)
        testRepositoryActor ! SaveServiceStatistics(system2, sectionControlStatistics4)

        testRepositoryActor ! SaveServiceStatistics(system3, sectionControlStatistics5)
        testRepositoryActor ! SaveServiceStatistics(system3, sectionControlStatistics6)
        testRepositoryActor ! SaveServiceStatistics(system3, sectionControlStatistics7)
        testUtil.countRows(serviceStatisticsTable) must be(14)

        implicit val timeout = Timeout(5 seconds)
        val future = testRepositoryActor ? GetServiceStatistics(system1, StatsDuration(key = today, timezone = timeZone), SectionControlStatisticsType)
        val result = Await.result(future, timeout.duration).asInstanceOf[SectionControlStatisticsResult]

        result.systemId must be(system1)
        result.duration must be(StatsDuration(key = today, timezone = timeZone))
        result.statistics must be(Seq(sectionControlStatistics3, sectionControlStatistics4, sectionControlStatistics5))
      }
    }

  }

  "A ServiceStatisticsHBaseRepositoryActor (table contains RedLightStatistics only)" when {

    "receiving a GetServiceStatistics(RedLightStatisticsType) message (and empty table)" must {
      "return Seq() in a RedLightStatisticsResult message" in {
        testUtil.countRows(serviceStatisticsTable) must be(0)

        implicit val timeout = Timeout(5 seconds)
        val future = testRepositoryActor ? GetServiceStatistics(systemId, StatsDuration(key = "20131030", timezone = timeZone), RedLightStatisticsType)
        val result = Await.result(future, timeout.duration).asInstanceOf[RedLightStatisticsResult]

        result.systemId must be(systemId)
        result.duration must be(StatsDuration(key = "20131030", timezone = timeZone))
        result.statistics must be(Seq())
      }
    }

    "receiving a SaveServiceStatistics message (new rowkey)" must {
      "store this data in the ServiceStatistics table ('cf', 'RL')" in {
        val redLightStatistics = createRedLightStatistics(day = "20131030", periodEnd = 123450999)

        testUtil.countRows(serviceStatisticsTable) must be(0)
        testRepositoryActor ! SaveServiceStatistics(systemId, redLightStatistics)
        testUtil.countRows(serviceStatisticsTable) must be(1)

        val testReader = StatisticsTable.createRedLightReader(hbaseConfig, tables)
        val columnValueFromTable = testReader.readRow(StringKey("sys123-20131030-000000123450999"))

        columnValueFromTable must be(Some(redLightStatistics))
      }
    }

    "receiving a SaveServiceStatistics message (existing rowkey)" must {
      "store this data in the ServiceStatistics table ('cf', 'RL'), replacing the existing data" in {
        val redLightStatistics1 = createRedLightStatistics(day = "20131030", periodEnd = 123450999)

        testUtil.countRows(serviceStatisticsTable) must be(0)
        testRepositoryActor ! SaveServiceStatistics(systemId, redLightStatistics1)
        testUtil.countRows(serviceStatisticsTable) must be(1)

        val testReader = StatisticsTable.createRedLightReader(hbaseConfig, tables)
        val columnValueFromTable1 = testReader.readRow(StringKey("sys123-20131030-000000123450999"))
        columnValueFromTable1 must be(Some(redLightStatistics1))

        val redLightStatistics2 = createRedLightStatistics(day = "20131030", periodEnd = 123450999, seed = 2)

        testUtil.countRows(serviceStatisticsTable) must be(1)
        testRepositoryActor ! SaveServiceStatistics(systemId, redLightStatistics2)
        testUtil.countRows(serviceStatisticsTable) must be(1)

        val columnValueFromTable2 = testReader.readRow(StringKey("sys123-20131030-000000123450999"))

        columnValueFromTable2 must be(Some(redLightStatistics2))
      }
    }

    "receiving a GetServiceStatistics(RedLightStatisticsType) message (and data exists)" must {
      "return requested data in a SectionControlStatisticsResult message" in {
        val redLightStatistics = createRedLightStatistics(day = "20131030", periodEnd = 123450999)

        testUtil.countRows(serviceStatisticsTable) must be(0)
        testRepositoryActor ! SaveServiceStatistics(systemId, redLightStatistics)
        testUtil.countRows(serviceStatisticsTable) must be(1)

        implicit val timeout = Timeout(5 seconds)
        val future = testRepositoryActor ? GetServiceStatistics(systemId, StatsDuration(key = "20131030", timezone = timeZone), RedLightStatisticsType)
        val result = Await.result(future, timeout.duration).asInstanceOf[RedLightStatisticsResult]

        result.systemId must be(systemId)
        result.duration must be(StatsDuration(key = "20131030", timezone = timeZone))
        result.statistics must be(Seq(redLightStatistics))
      }
    }

    "receiving a GetServiceStatistics(RedLightStatisticsType) message (and data does not exist)" must {
      "return Seq() in a RedLightStatisticsResult message" in {
        val redLightStatistics = createRedLightStatistics(day = "20131030", periodEnd = 123450999)

        testUtil.countRows(serviceStatisticsTable) must be(0)
        testRepositoryActor ! SaveServiceStatistics(systemId, redLightStatistics)
        testUtil.countRows(serviceStatisticsTable) must be(1)

        implicit val timeout = Timeout(5 seconds)
        val future = testRepositoryActor ? GetServiceStatistics(systemId, StatsDuration(key = "20131031", timezone = timeZone), RedLightStatisticsType)
        val result = Await.result(future, timeout.duration).asInstanceOf[RedLightStatisticsResult]

        result.systemId must be(systemId)
        result.duration must be(StatsDuration(key = "20131031", timezone = timeZone))
        result.statistics must be(Seq())
      }
    }

    "receiving a GetServiceStatistics(SectionControlStatisticsType) message (and only RedLightStatistics exists)" must {
      "return Seq() in a SectionControlStatisticsResult message" in {
        val redLightStatistics = createRedLightStatistics(day = "20131030", periodEnd = 123450999)

        testUtil.countRows(serviceStatisticsTable) must be(0)
        testRepositoryActor ! SaveServiceStatistics(systemId, redLightStatistics)
        testUtil.countRows(serviceStatisticsTable) must be(1)

        implicit val timeout = Timeout(5 seconds)
        val future = testRepositoryActor ? GetServiceStatistics(systemId, StatsDuration(key = "20131030", timezone = timeZone), SectionControlStatisticsType)
        val result = Await.result(future, timeout.duration).asInstanceOf[SectionControlStatisticsResult]

        result.systemId must be(systemId)
        result.duration must be(StatsDuration(key = "20131030", timezone = timeZone))
        result.statistics must be(Seq())
      }
    }

    "receiving a GetServiceStatistics(SpeedFixedStatisticsType) message (and only RedLightStatistics exists)" must {
      "return Seq() in a SpeedFixedStatisticsResult message" in {
        val redLightStatistics = createRedLightStatistics(day = "20131030", periodEnd = 123450999)

        testUtil.countRows(serviceStatisticsTable) must be(0)
        testRepositoryActor ! SaveServiceStatistics(systemId, redLightStatistics)
        testUtil.countRows(serviceStatisticsTable) must be(1)

        implicit val timeout = Timeout(5 seconds)
        val future = testRepositoryActor ? GetServiceStatistics(systemId, StatsDuration(key = "20131030", timezone = timeZone), SpeedFixedStatisticsType)
        val result = Await.result(future, timeout.duration).asInstanceOf[SpeedFixedStatisticsResult]

        result.systemId must be(systemId)
        result.duration must be(StatsDuration(key = "20131030", timezone = timeZone))
        result.statistics must be(Seq())
      }
    }

    "receiving a GetServiceStatistics(RedLightStatisticsType) message" must {
      "return a RedLightStatisticsResult message containing the data for given systemId and day" in {
        val system1 = "sys1"
        val system2 = "sys2"
        val system3 = "sys3"
        val yesterday = "20131029"
        val today = "20131030"
        val tomorrow = "20131031"
        val redLightStatistics1 = createRedLightStatistics(day = yesterday, periodEnd = 555555555555555L)
        val redLightStatistics2 = createRedLightStatistics(day = yesterday, periodEnd = 999999999999999L)
        val redLightStatistics3 = createRedLightStatistics(day = today, periodEnd = 000000000000010L)
        val redLightStatistics4 = createRedLightStatistics(day = today, periodEnd = 555555555555555L)
        val redLightStatistics5 = createRedLightStatistics(day = today, periodEnd = 999999999999999L)
        val redLightStatistics6 = createRedLightStatistics(day = tomorrow, periodEnd = 000000000000010L)
        val redLightStatistics7 = createRedLightStatistics(day = tomorrow, periodEnd = 555555555555555L)

        testUtil.countRows(serviceStatisticsTable) must be(0)
        testRepositoryActor ! SaveServiceStatistics(system1, redLightStatistics1)
        testRepositoryActor ! SaveServiceStatistics(system1, redLightStatistics2)
        testRepositoryActor ! SaveServiceStatistics(system1, redLightStatistics3)
        testRepositoryActor ! SaveServiceStatistics(system1, redLightStatistics4)
        testRepositoryActor ! SaveServiceStatistics(system1, redLightStatistics5)
        testRepositoryActor ! SaveServiceStatistics(system1, redLightStatistics6)
        testRepositoryActor ! SaveServiceStatistics(system1, redLightStatistics7)

        testRepositoryActor ! SaveServiceStatistics(system2, redLightStatistics1)
        testRepositoryActor ! SaveServiceStatistics(system2, redLightStatistics2)
        testRepositoryActor ! SaveServiceStatistics(system2, redLightStatistics3)
        testRepositoryActor ! SaveServiceStatistics(system2, redLightStatistics4)

        testRepositoryActor ! SaveServiceStatistics(system3, redLightStatistics5)
        testRepositoryActor ! SaveServiceStatistics(system3, redLightStatistics6)
        testRepositoryActor ! SaveServiceStatistics(system3, redLightStatistics7)
        testUtil.countRows(serviceStatisticsTable) must be(14)

        implicit val timeout = Timeout(5 seconds)
        val future = testRepositoryActor ? GetServiceStatistics(system1, StatsDuration(key = today, timezone = timeZone), RedLightStatisticsType)
        val result = Await.result(future, timeout.duration).asInstanceOf[RedLightStatisticsResult]

        result.systemId must be(system1)
        result.duration must be(StatsDuration(key = today, timezone = timeZone))
        result.statistics must be(Seq(redLightStatistics3, redLightStatistics4, redLightStatistics5))
      }
    }

  }

  "A ServiceStatisticsHBaseRepositoryActor (table contains SpeedFixedStatistics only)" when {

    "receiving a GetServiceStatistics(SpeedFixedStatisticsType) message (and empty table)" must {
      "return Seq() in a SpeedFixedStatisticsResult message" in {
        testUtil.countRows(serviceStatisticsTable) must be(0)

        implicit val timeout = Timeout(5 seconds)
        val future = testRepositoryActor ? GetServiceStatistics(systemId, StatsDuration(key = "20131030", timezone = timeZone), SpeedFixedStatisticsType)
        val result = Await.result(future, timeout.duration).asInstanceOf[SpeedFixedStatisticsResult]

        result.systemId must be(systemId)
        result.duration must be(StatsDuration(key = "20131030", timezone = timeZone))
        result.statistics must be(Seq())
      }
    }

    "receiving a SaveServiceStatistics message (new rowkey)" must {
      "store this data in the ServiceStatistics table ('cf', 'SF')" in {
        val speedFixedStatistics = createSpeedFixedStatistics(day = "20131030", periodEnd = 123450999)

        testUtil.countRows(serviceStatisticsTable) must be(0)
        testRepositoryActor ! SaveServiceStatistics(systemId, speedFixedStatistics)
        testUtil.countRows(serviceStatisticsTable) must be(1)

        val testReader = StatisticsTable.createSpeedReader(hbaseConfig, tables)
        val columnValueFromTable = testReader.readRow(StringKey("sys123-20131030-000000123450999"))

        columnValueFromTable must be(Some(speedFixedStatistics))
      }
    }

    "receiving a SaveServiceStatistics(SpeedFixedStatistics) message (existing rowkey)" must {
      "store this data in the ServiceStatistics table ('cf', 'SF'), replacing the existing data" in {
        val speedFixedStatistics1 = createSpeedFixedStatistics(day = "20131030", periodEnd = 123450999)

        testUtil.countRows(serviceStatisticsTable) must be(0)
        testRepositoryActor ! SaveServiceStatistics(systemId, speedFixedStatistics1)
        testUtil.countRows(serviceStatisticsTable) must be(1)

        val testReader = StatisticsTable.createSpeedReader(hbaseConfig, tables)
        val columnValueFromTable1 = testReader.readRow(StringKey("sys123-20131030-000000123450999"))
        columnValueFromTable1 must be(Some(speedFixedStatistics1))

        val speedFixedStatistics2 = createSpeedFixedStatistics(day = "20131030", periodEnd = 123450999, seed = 2)

        testUtil.countRows(serviceStatisticsTable) must be(1)
        testRepositoryActor ! SaveServiceStatistics(systemId, speedFixedStatistics2)
        testUtil.countRows(serviceStatisticsTable) must be(1)

        val columnValueFromTable2 = testReader.readRow(StringKey("sys123-20131030-000000123450999"))

        columnValueFromTable2 must be(Some(speedFixedStatistics2))
      }
    }

    "receiving a GetServiceStatistics(SpeedFixedStatisticsType) message (and data exists)" must {
      "return requested data in a SpeedFixedStatisticsResult message" in {
        val speedFixedStatistics = createSpeedFixedStatistics(day = "20131030", periodEnd = 123450999)

        testUtil.countRows(serviceStatisticsTable) must be(0)
        testRepositoryActor ! SaveServiceStatistics(systemId, speedFixedStatistics)
        testUtil.countRows(serviceStatisticsTable) must be(1)

        implicit val timeout = Timeout(5 seconds)
        val future = testRepositoryActor ? GetServiceStatistics(systemId, StatsDuration(key = "20131030", timezone = timeZone), SpeedFixedStatisticsType)
        val result = Await.result(future, timeout.duration).asInstanceOf[SpeedFixedStatisticsResult]

        result.systemId must be(systemId)
        result.duration must be(StatsDuration(key = "20131030", timezone = timeZone))
        result.statistics must be(Seq(speedFixedStatistics))
      }
    }

    "receiving a GetServiceStatistics(SpeedFixedStatisticsType) message (and data does not exist)" must {
      "return Seq() in a SpeedFixedStatisticsResult message" in {
        val speedFixedStatistics = createSpeedFixedStatistics(day = "20131030", periodEnd = 123450999)

        testUtil.countRows(serviceStatisticsTable) must be(0)
        testRepositoryActor ! SaveServiceStatistics(systemId, speedFixedStatistics)
        testUtil.countRows(serviceStatisticsTable) must be(1)

        implicit val timeout = Timeout(5 seconds)
        val future = testRepositoryActor ? GetServiceStatistics(systemId, StatsDuration(key = "20131031", timezone = timeZone), SpeedFixedStatisticsType)
        val result = Await.result(future, timeout.duration).asInstanceOf[SpeedFixedStatisticsResult]

        result.systemId must be(systemId)
        result.duration must be(StatsDuration(key = "20131031", timezone = timeZone))
        result.statistics must be(Seq())
      }
    }

    "receiving a GetServiceStatistics(SectionControlStatisticsType) message (and only SpeedFixedStatistics exists)" must {
      "return Seq() in a SectionControlStatisticsResult message" in {
        val speedFixedStatistics = createSpeedFixedStatistics(day = "20131030", periodEnd = 123450999)

        testUtil.countRows(serviceStatisticsTable) must be(0)
        testRepositoryActor ! SaveServiceStatistics(systemId, speedFixedStatistics)
        testUtil.countRows(serviceStatisticsTable) must be(1)

        implicit val timeout = Timeout(5 seconds)
        val future = testRepositoryActor ? GetServiceStatistics(systemId, StatsDuration(key = "20131030", timezone = timeZone), SectionControlStatisticsType)
        val result = Await.result(future, timeout.duration).asInstanceOf[SectionControlStatisticsResult]

        result.systemId must be(systemId)
        result.duration must be(StatsDuration(key = "20131030", timezone = timeZone))
        result.statistics must be(Seq())
      }
    }

    "receiving a GetServiceStatistics(RedLightStatisticsType) message (and only SpeedFixedStatistics exists)" must {
      "return Seq() in a RedLightStatisticsResult message" in {
        val speedFixedStatistics = createSpeedFixedStatistics(day = "20131030", periodEnd = 123450999)

        testUtil.countRows(serviceStatisticsTable) must be(0)
        testRepositoryActor ! SaveServiceStatistics(systemId, speedFixedStatistics)
        testUtil.countRows(serviceStatisticsTable) must be(1)

        implicit val timeout = Timeout(5 seconds)
        val future = testRepositoryActor ? GetServiceStatistics(systemId, StatsDuration(key = "20131030", timezone = timeZone), RedLightStatisticsType)
        val result = Await.result(future, timeout.duration).asInstanceOf[RedLightStatisticsResult]

        result.systemId must be(systemId)
        result.duration must be(StatsDuration(key = "20131030", timezone = timeZone))
        result.statistics must be(Seq())
      }
    }

    "receiving a GetServiceStatistics(SpeedFixedStatisticsType) message" must {
      "return a SpeedFixedStatisticsResult message containing the data for given systemId and day" in {
        val system1 = "sys1"
        val system2 = "sys2"
        val system3 = "sys3"
        val yesterday = "20131029"
        val today = "20131030"
        val tomorrow = "20131031"
        val speedFixedStatistics1 = createSpeedFixedStatistics(day = yesterday, periodEnd = 555555555555555L)
        val speedFixedStatistics2 = createSpeedFixedStatistics(day = yesterday, periodEnd = 999999999999999L)
        val speedFixedStatistics3 = createSpeedFixedStatistics(day = today, periodEnd = 000000000000010L)
        val speedFixedStatistics4 = createSpeedFixedStatistics(day = today, periodEnd = 555555555555555L)
        val speedFixedStatistics5 = createSpeedFixedStatistics(day = today, periodEnd = 999999999999999L)
        val speedFixedStatistics6 = createSpeedFixedStatistics(day = tomorrow, periodEnd = 000000000000010L)
        val speedFixedStatistics7 = createSpeedFixedStatistics(day = tomorrow, periodEnd = 555555555555555L)

        testUtil.countRows(serviceStatisticsTable) must be(0)
        testRepositoryActor ! SaveServiceStatistics(system1, speedFixedStatistics1)
        testRepositoryActor ! SaveServiceStatistics(system1, speedFixedStatistics2)
        testRepositoryActor ! SaveServiceStatistics(system1, speedFixedStatistics3)
        testRepositoryActor ! SaveServiceStatistics(system1, speedFixedStatistics4)
        testRepositoryActor ! SaveServiceStatistics(system1, speedFixedStatistics5)
        testRepositoryActor ! SaveServiceStatistics(system1, speedFixedStatistics6)
        testRepositoryActor ! SaveServiceStatistics(system1, speedFixedStatistics7)

        testRepositoryActor ! SaveServiceStatistics(system2, speedFixedStatistics1)
        testRepositoryActor ! SaveServiceStatistics(system2, speedFixedStatistics2)
        testRepositoryActor ! SaveServiceStatistics(system2, speedFixedStatistics3)
        testRepositoryActor ! SaveServiceStatistics(system2, speedFixedStatistics4)

        testRepositoryActor ! SaveServiceStatistics(system3, speedFixedStatistics5)
        testRepositoryActor ! SaveServiceStatistics(system3, speedFixedStatistics6)
        testRepositoryActor ! SaveServiceStatistics(system3, speedFixedStatistics7)
        testUtil.countRows(serviceStatisticsTable) must be(14)

        implicit val timeout = Timeout(5 seconds)
        val future = testRepositoryActor ? GetServiceStatistics(system1, StatsDuration(key = today, timezone = timeZone), SpeedFixedStatisticsType)
        val result = Await.result(future, timeout.duration).asInstanceOf[SpeedFixedStatisticsResult]

        result.systemId must be(system1)
        result.duration must be(StatsDuration(key = today, timezone = timeZone))
        result.statistics must be(Seq(speedFixedStatistics3, speedFixedStatistics4, speedFixedStatistics5))
      }
    }

  }

  "A ServiceStatisticsHBaseRepositoryActor (table contains different types of ServiceStatistics)" when {

    "receiving a SaveServiceStatistics(type X) message and for given rowkey there already exists data of type Y)" must {
      "store type X data in the ServiceStatistics table, not changing the type Y data" in {
        val thisDay = "20131030"

        // First store RedLightStatistics (= type Y)
        val redLightStatistics = createRedLightStatistics(day = thisDay, periodEnd = 123450999)

        testUtil.countRows(serviceStatisticsTable) must be(0)
        testRepositoryActor ! SaveServiceStatistics(systemId, redLightStatistics)
        testUtil.countRows(serviceStatisticsTable) must be(1)

        implicit val timeout = Timeout(5 seconds)
        val future1 = testRepositoryActor ? GetServiceStatistics(systemId, StatsDuration(key = thisDay, timezone = timeZone), RedLightStatisticsType)
        val result1 = Await.result(future1, timeout.duration).asInstanceOf[RedLightStatisticsResult]

        result1.systemId must be(systemId)
        result1.duration must be(StatsDuration(key = thisDay, timezone = timeZone))
        result1.statistics must be(Seq(redLightStatistics))

        // Now save SpeedFixedStatistics (= type X) with the same rowkey
        val speedFixedStatistics = createSpeedFixedStatistics(day = thisDay, periodEnd = 123450999)

        testUtil.countRows(serviceStatisticsTable) must be(1)
        testRepositoryActor ! SaveServiceStatistics(systemId, speedFixedStatistics)
        testUtil.countRows(serviceStatisticsTable) must be(1)

        val future2 = testRepositoryActor ? GetServiceStatistics(systemId, StatsDuration(key = thisDay, timezone = timeZone), SpeedFixedStatisticsType)
        val result2 = Await.result(future2, timeout.duration).asInstanceOf[SpeedFixedStatisticsResult]

        result2.systemId must be(systemId)
        result2.duration must be(StatsDuration(key = thisDay, timezone = timeZone))
        result2.statistics must be(Seq(speedFixedStatistics))

        val future3 = testRepositoryActor ? GetServiceStatistics(systemId, StatsDuration(key = thisDay, timezone = timeZone), RedLightStatisticsType)
        val result3 = Await.result(future3, timeout.duration).asInstanceOf[RedLightStatisticsResult]

        result3.systemId must be(systemId)
        result3.duration must be(StatsDuration(key = thisDay, timezone = timeZone))
        result3.statistics must be(Seq(redLightStatistics))
      }
    }

    "receiving a GetServiceStatistics(any type) message" must {
      "return a message containing the requested data for given systemId and day" in {
        val system1 = "sys1"
        val system2 = "sys2"
        val yesterday = "20131029"
        val today = "20131030"
        val tomorrow = "20131031"
        val time1 = 000000000000010L
        val time2 = 555555555555555L
        val time3 = 999999999999999L

        var incSeed = 0
        def createStatistics(statType: String, day: String, time: Long): ServiceStatistics = {
          incSeed = incSeed + 1
          statType match {
            case "SC" ⇒ createSectionControlStatistics(day = day, periodEnd = time, seed = incSeed)
            case "RL" ⇒ createRedLightStatistics(day = day, periodEnd = time, seed = incSeed)
            case "SF" ⇒ createSpeedFixedStatistics(day = day, periodEnd = time, seed = incSeed)
          }
        }

        val statistics = (for {
          statType ← Seq("SC", "RL", "SF")
          day ← Seq(yesterday, today, tomorrow)
          time ← Seq(time1, time2, time3)
        } yield (statType, day, time) -> createStatistics(statType, day, time)).toMap

        val system1Statistics = Seq(
          ("SC", today, time1),
          ("SC", today, time2),
          ("SC", today, time3),
          ("SC", tomorrow, time2),
          ("RL", yesterday, time1),
          ("RL", yesterday, time3),
          ("RL", today, time2),
          ("RL", tomorrow, time1),
          ("RL", tomorrow, time2),
          ("SF", yesterday, time2),
          ("SF", today, time1),
          ("SF", today, time2),
          ("SF", today, time3),
          ("SF", tomorrow, time1))

        val system2Statistics = Seq(
          ("SC", yesterday, time1),
          ("SC", yesterday, time2),
          ("SC", yesterday, time3),
          ("SC", today, time1),
          ("SC", today, time2),
          ("SC", tomorrow, time1),
          ("SC", tomorrow, time2),
          ("SC", tomorrow, time3),
          ("RL", today, time1),
          ("RL", tomorrow, time2),
          ("RL", tomorrow, time3),
          ("SF", yesterday, time1),
          ("SF", yesterday, time2),
          ("SF", today, time2),
          ("SF", tomorrow, time2))

        testUtil.countRows(serviceStatisticsTable) must be(0)
        system1Statistics.foreach { s ⇒
          testRepositoryActor ! SaveServiceStatistics(system1, statistics(s))
        }
        system2Statistics.foreach { s ⇒
          testRepositoryActor ! SaveServiceStatistics(system2, statistics(s))
        }

        Thread.sleep(200)

        testUtil.countRows(serviceStatisticsTable) must be(26)

        def getType(statType: String): StatisticsType = statType match {
          case "SC" ⇒ SectionControlStatisticsType
          case "RL" ⇒ RedLightStatisticsType
          case "SF" ⇒ SpeedFixedStatisticsType
        }

        def getDefinedStatistics(system: String) = if (system == system1) system1Statistics else system2Statistics

        def getStatisticsFor(system: String, statType: String, day: String): Seq[ServiceStatistics] = {
          getDefinedStatistics(system).filter { case (t, d, _) ⇒ (t == statType) && (d == day) }.map(k ⇒ statistics(k))
        }

        implicit val timeout = Timeout(5 seconds)

        for {
          system ← Seq(system1, system2)
          statType ← Seq("SC", "RL", "SF")
          day ← Seq(yesterday, today, tomorrow)
        } {
          val future = testRepositoryActor ? GetServiceStatistics(system, StatsDuration(key = day, timezone = timeZone), getType(statType))
          statType match {
            case "SC" ⇒
              val result = Await.result(future, timeout.duration).asInstanceOf[SectionControlStatisticsResult]

              result.systemId must be(system)
              result.duration must be(StatsDuration(key = day, timezone = timeZone))
              result.statistics must be(getStatisticsFor(system, statType, day))

            case "RL" ⇒
              val result = Await.result(future, timeout.duration).asInstanceOf[RedLightStatisticsResult]

              result.systemId must be(system)
              result.duration must be(StatsDuration(key = day, timezone = timeZone))
              result.statistics must be(getStatisticsFor(system, statType, day))

            case "SF" ⇒
              val result = Await.result(future, timeout.duration).asInstanceOf[SpeedFixedStatisticsResult]

              result.systemId must be(system)
              result.duration must be(StatsDuration(key = day, timezone = timeZone))
              result.statistics must be(getStatisticsFor(system, statType, day))
          }

        }
      }
    }

  }

  def createSectionControlStatistics(day: String, periodEnd: Long, seed: Int = 0): SectionControlStatistics = {
    SectionControlStatistics(
      period = StatisticsPeriod(
        stats = StatsDuration(key = day, timezone = timeZone),
        end = periodEnd,
        begin = periodEnd - 10),
      config = matcherConfig,
      input = 23 + seed,
      matched = 20 + seed,
      unmatched = 3 + seed,
      doubles = 0 + seed,
      averageSpeed = 43.5 + seed,
      highestSpeed = 78 + seed,
      laneStats = List(lane1))
  }

  def createRedLightStatistics(day: String, periodEnd: Long, seed: Int = 0): RedLightStatistics = {
    RedLightStatistics(
      period = StatisticsPeriod(
        stats = StatsDuration(key = day, timezone = timeZone),
        end = periodEnd,
        begin = periodEnd - 10),
      nrRegistrations = 23 + seed,
      nrWithRedLight = 20 + seed,
      averageRedLight = 1.5 + seed,
      highestRedLight = 2 + seed,
      laneStats = List(lane1))
  }

  def createSpeedFixedStatistics(day: String, periodEnd: Long, seed: Int = 0): SpeedFixedStatistics = {
    SpeedFixedStatistics(
      period = StatisticsPeriod(
        stats = StatsDuration(key = day, timezone = timeZone),
        end = periodEnd,
        begin = periodEnd - 10),
      nrRegistrations = 23 + seed,
      nrSpeedMeasurement = 20 + seed,
      averageSpeed = 41.5 + seed,
      highestSpeed = 65 + seed,
      laneStats = List(lane1))
  }

  case class StringKey(key: String) extends ServiceStatisticsKey {
    def toKeyString: String = key
  }

}

