package csc.sectioncontrol.storagelayer.roles

import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.storage.{ ZkRole, Paths }
import csc.curator.CuratorTestServer
import csc.akkautils.DirectLogging
import csc.curator.utils.CuratorToolsImpl

class RoleServiceTest extends WordSpec with MustMatchers with BeforeAndAfterAll with CuratorTestServer with DirectLogging {
  val path = Paths.Roles.getRootPath
  var zkStore: CuratorToolsImpl = _

  val role = new ZkRole("1", "role1", "test_role_1")
  val roleUpdate = new ZkRole("2", "role2", "test_role_2")
  def roleService = new RoleServiceZooKeeper(zkStore)

  override def beforeEach() {
    super.beforeEach()
    zkStore = new CuratorToolsImpl(clientScope, log)
  }
  override def afterEach() {
    zkStore.deleteRecursive(path)
    super.afterEach()
  }

  "RoleService" must {
    "CREATE the ROLE and GET the result" in {
      roleService.create(role)
      val result = roleService.get("1")
      result.get.name must be("role1")
    }

    "CREATE the ROLE and GET the VERSIONED result" in {
      roleService.create(role)
      val result = roleService.getVersioned("1")
      result.get.data.name must be("role1")
    }

    "CREATE the USER and GET the VERSIONED result and UPDATE" in {
      roleService.create(role)
      var result = roleService.getVersioned("1")
      result.get.data.name must be("role1")

      roleService.update("1", roleUpdate, result.get.version)

      result = roleService.getVersioned("1")
      result.get.data.name must be("role2")
    }
  }
}

