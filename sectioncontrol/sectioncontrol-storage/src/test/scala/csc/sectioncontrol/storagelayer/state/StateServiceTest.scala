/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.state

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.curator.CuratorTestServer
import csc.akkautils.DirectLogging
import csc.curator.utils.CuratorToolsImpl
import csc.sectioncontrol.storage.ZkState
import akka.util.duration._

class StateServiceTest extends WordSpec with MustMatchers with CuratorTestServer with DirectLogging {

  var curator: CuratorToolsImpl = _

  override def beforeEach() {
    super.beforeEach()
    curator = new CuratorToolsImpl(clientScope, log)
  }
  val systemId = "A2"
  val corridorId = "S1"
  val startTime = System.currentTimeMillis()

  "StateService.update" must {
    "append degraded state" in {
      val initialState = ZkState(state = ZkState.enforceOn, timestamp = startTime, userId = "user1", reason = "startup")
      StateService.create(systemId, corridorId, initialState, curator)
      //start test
      val state = ZkState(state = ZkState.enforceDegraded, timestamp = startTime + 1.hour.toMillis, userId = "system", reason = "storing")
      StateService.update(systemId, corridorId, state, 0, curator)
      val history = StateService.getStateLogHistory(systemId, corridorId, startTime, startTime + 2.hour.toMillis, curator, true)
      history.size must be(2)
      history.head must be(initialState)
      history.last must be(state)
    }
    "update old history events when delayed is same time as other state" in {
      val initialState = ZkState(state = ZkState.enforceOn, timestamp = startTime, userId = "user1", reason = "startup")
      StateService.create(systemId, corridorId, initialState, curator)
      //start test
      val state1 = ZkState(state = ZkState.standBy, timestamp = startTime + 1.hour.toMillis, userId = "system", reason = "off")
      StateService.update(systemId, corridorId, state1, 0, curator)
      val state2 = ZkState(state = ZkState.enforceOn, timestamp = startTime + 2.hour.toMillis, userId = "system", reason = "on")
      StateService.update(systemId, corridorId, state2, 1, curator)

      val state3 = ZkState(state = ZkState.enforceDegraded, timestamp = startTime + 1.hour.toMillis, userId = "system", reason = "storing")
      StateService.update(systemId, corridorId, state3, 2, curator)

      val history = StateService.getStateLogHistory(systemId, corridorId, startTime, startTime + 3.hour.toMillis, curator, true)
      history.size must be(3)
      history(0) must be(initialState)
      history(1) must be(state1)
      history(2) must be(state2.copy(state = ZkState.enforceDegraded))

      val current = StateService.get(systemId, corridorId, curator)
      current.get must be(state2.copy(state = ZkState.enforceDegraded))
    }
    "update old history events when delayed is same time as other enforce state" in {
      val initialState = ZkState(state = ZkState.standBy, timestamp = startTime, userId = "user1", reason = "startup")
      StateService.create(systemId, corridorId, initialState, curator)
      //start test
      val state1 = ZkState(state = ZkState.enforceOn, timestamp = startTime + 1.hour.toMillis, userId = "system", reason = "on")
      StateService.update(systemId, corridorId, state1, 0, curator)
      val state2 = ZkState(state = ZkState.standBy, timestamp = startTime + 2.hour.toMillis, userId = "system", reason = "off")
      StateService.update(systemId, corridorId, state2, 1, curator)

      val state3 = ZkState(state = ZkState.enforceDegraded, timestamp = startTime + 1.hour.toMillis, userId = "system", reason = "storing")
      StateService.update(systemId, corridorId, state3, 2, curator)

      val history = StateService.getStateLogHistory(systemId, corridorId, startTime, startTime + 3.hour.toMillis, curator, true)
      history.size must be(3)
      history(0) must be(initialState)
      history(1) must be(state1.copy(state = ZkState.enforceDegraded))
      history(2) must be(state2)

      val current = StateService.get(systemId, corridorId, curator)
      current.get must be(state2)
    }
    "update old history events when delayed is before other state" in {
      val initialState = ZkState(state = ZkState.enforceOn, timestamp = startTime, userId = "user1", reason = "startup")
      StateService.create(systemId, corridorId, initialState, curator)
      //start test
      val state1 = ZkState(state = ZkState.standBy, timestamp = startTime + 1.hour.toMillis, userId = "system", reason = "off")
      StateService.update(systemId, corridorId, state1, 0, curator)
      val state2 = ZkState(state = ZkState.enforceOn, timestamp = startTime + 2.hour.toMillis, userId = "system", reason = "on")
      StateService.update(systemId, corridorId, state2, 1, curator)

      val state3 = ZkState(state = ZkState.enforceDegraded, timestamp = startTime + 90.minutes.toMillis, userId = "system", reason = "storing")
      StateService.update(systemId, corridorId, state3, 2, curator)

      val history = StateService.getStateLogHistory(systemId, corridorId, startTime, startTime + 3.hour.toMillis, curator, true)
      history.size must be(3)
      history(0) must be(initialState)
      history(1) must be(state1)
      history(2) must be(state2.copy(state = ZkState.enforceDegraded))

      val current = StateService.get(systemId, corridorId, curator)
      current.get must be(state2.copy(state = ZkState.enforceDegraded))
    }
    "update old history events when delayed before other enforce state" in {
      val initialState = ZkState(state = ZkState.standBy, timestamp = startTime, userId = "user1", reason = "startup")
      StateService.create(systemId, corridorId, initialState, curator)
      //start test
      val state1 = ZkState(state = ZkState.enforceOn, timestamp = startTime + 1.hour.toMillis, userId = "system", reason = "on")
      StateService.update(systemId, corridorId, state1, 0, curator)
      val state2 = ZkState(state = ZkState.standBy, timestamp = startTime + 2.hour.toMillis, userId = "system", reason = "off")
      StateService.update(systemId, corridorId, state2, 1, curator)

      val state3 = ZkState(state = ZkState.enforceDegraded, timestamp = startTime + 90.minutes.toMillis, userId = "system", reason = "storing")
      StateService.update(systemId, corridorId, state3, 2, curator)

      val history = StateService.getStateLogHistory(systemId, corridorId, startTime, startTime + 3.hour.toMillis, curator, true)
      history.size must be(4)
      history(0) must be(initialState)
      history(1) must be(state1)
      history(2) must be(state3)
      history(3) must be(state2)

      val current = StateService.get(systemId, corridorId, curator)
      current.get must be(state2)
    }
  }
}