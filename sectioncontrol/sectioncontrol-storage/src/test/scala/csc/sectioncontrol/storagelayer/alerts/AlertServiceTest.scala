/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.alerts

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.curator.CuratorTestServer
import csc.akkautils.DirectLogging
import csc.curator.utils.CuratorToolsImpl
import csc.sectioncontrol.storagelayer.hbasetables.SerializerFormats
import java.text.SimpleDateFormat
import csc.sectioncontrol.storage
import storage._
import storage.ZkCorridorInfo
import storage.ZkDirection
import storage.ZkPSHTMIdentification

class AlertServiceTest extends WordSpec with MustMatchers with CuratorTestServer with DirectLogging {
  val formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
  var zookeeperClient: CuratorToolsImpl = _

  override def beforeEach() {
    super.beforeEach()
    zookeeperClient = new CuratorToolsImpl(clientScope, log, SerializerFormats.formats)
  }
  override def afterEach() {
    super.afterEach()
  }
  "AlertService.exists" must {
    "detect same alert" in {
      val systemId = "A2"
      val corridorId = "cor1"
      val alert = ZkAlert(path = "A2-X1-R1-Camera", alertType = "", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T02:00:00").getTime, reductionFactor = 0.1f)
      AlertService.create(systemId, corridorId, alert, zookeeperClient)
      AlertService.exists(systemId, corridorId, alert, zookeeperClient) must be(true)
    }
    "detect simular alert" in {
      val systemId = "A2"
      val corridorId = "cor1"
      val alert = ZkAlert(path = "A2-X1-R1-Camera", alertType = "TEST", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T02:00:00").getTime, reductionFactor = 0.1f)
      AlertService.create(systemId, corridorId, alert, zookeeperClient)
      val simAlert = ZkAlert(path = "A2-X1-R1-Camera", alertType = "TEST", configType = ConfigType.System, message = "Something different", timestamp = formatter.parse("2012-12-29T02:00:10").getTime, reductionFactor = 0.1f)
      AlertService.exists(systemId, corridorId, simAlert, zookeeperClient) must be(true)
    }
    "fail on different path" in {
      val systemId = "A2"
      val corridorId = "cor1"
      val alert = ZkAlert(path = "A2-X1-R1-Camera", alertType = "TEST", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T02:00:00").getTime, reductionFactor = 0.1f)
      AlertService.create(systemId, corridorId, alert, zookeeperClient)
      val simAlert = ZkAlert(path = "A2-X1-R2-Camera", alertType = "TEST", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T02:00:00").getTime, reductionFactor = 0.1f)
      AlertService.exists(systemId, corridorId, simAlert, zookeeperClient) must be(false)
    }
    "fail on different type" in {
      val systemId = "A2"
      val corridorId = "cor1"
      val alert = ZkAlert(path = "A2-X1-R1-Camera", alertType = "TEST", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T02:00:00").getTime, reductionFactor = 0.1f)
      AlertService.create(systemId, corridorId, alert, zookeeperClient)
      val simAlert = ZkAlert(path = "A2-X1-R1-Camera", alertType = "test", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T02:00:00").getTime, reductionFactor = 0.1f)
      AlertService.exists(systemId, corridorId, simAlert, zookeeperClient) must be(false)
    }

  }

  "AlertService.getSystemAlertsWithPath using curator" must {
    "return the ZkAlerts with their corresponding zookeeper path/node" in {
      val systemId = "A2"
      val corridorId = "cor1"
      createSectionsAndCorridors(systemId, corridorId)
      val alert1 = ZkAlert(path = "A2-X1-R1-Camera", alertType = "", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T02:00:00").getTime, reductionFactor = 0.1f)
      AlertService.create(systemId, corridorId, alert1, zookeeperClient)
      val alert2 = ZkAlert(path = "A2-X1-R2-Camera", alertType = "", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T03:00:00")getTime, reductionFactor = 0.4f)
      AlertService.create(systemId, corridorId, alert2, zookeeperClient)
      val alert3 = ZkAlert(path = "A2-E1-R1-Camera", alertType = "", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T13:00:00").getTime, reductionFactor = 0.2f)
      AlertService.create(systemId, corridorId, alert3, zookeeperClient)
      val result = AlertService.getSystemAlertsWithPath(zookeeperClient, systemId, corridorId, endTime = formatter.parse("2012-12-29T13:10:00").getTime)
      result.foreach { case (a, b) ⇒ a must startWith("/ctes/systems/A2/corridors/cor1/control/errors/alerts/current/qn-") }
    }
  }

  "AlertService.getReductionFactor using ZkAlertHistory" must {
    "return factor when all are current" in {
      val alerts = List(
        ZkAlertHistory(path = "A2-X1-R1-Camera", alertType = "", configType = ConfigType.Lane.toString, message = "current event", startTime = formatter.parse("2012-12-29T02:00:00").getTime, endTime = Long.MaxValue, reductionFactor = 0.1f),
        ZkAlertHistory(path = "A2-X1-R2-Camera", alertType = "", configType = ConfigType.Lane.toString, message = "current event", startTime = formatter.parse("2012-12-29T03:00:00")getTime, endTime = Long.MaxValue, reductionFactor = 0.4f),
        ZkAlertHistory(path = "A2-E1-R1-Camera", alertType = "", configType = ConfigType.Lane.toString, message = "current event", startTime = formatter.parse("2012-12-29T13:00:00").getTime, endTime = Long.MaxValue, reductionFactor = 0.2f))
      AlertService.getReductionFactor(alerts) must be(0.7f plusOrMinus 0.001f)
    }
    "return factor when only two alerts are current" in {
      val alerts = List(
        ZkAlertHistory(path = "A2-X1-R1-Camera", alertType = "", configType = ConfigType.Lane.toString, message = "current event", startTime = formatter.parse("2012-12-29T02:00:00").getTime, endTime = Long.MaxValue, reductionFactor = 0.1f),
        ZkAlertHistory(path = "A2-X1-R2-Camera", alertType = "", configType = ConfigType.Lane.toString, message = "current event", startTime = formatter.parse("2012-12-29T03:00:00").getTime, endTime = formatter.parse("2012-12-29T06:00:00").getTime, reductionFactor = 0.4f),
        ZkAlertHistory(path = "A2-E1-R1-Camera", alertType = "", configType = ConfigType.Lane.toString, message = "current event", startTime = formatter.parse("2012-12-29T13:00:00").getTime, endTime = Long.MaxValue, reductionFactor = 0.2f))
      AlertService.getReductionFactor(alerts) must be(0.3f plusOrMinus 0.001f)
    }
    "return factor when list is empty" in {
      val alerts = List()
      AlertService.getReductionFactor(alerts) must be(0f plusOrMinus 0.001f)
    }
    "return factor when two allerts are same lane" in {
      val alerts = List(
        ZkAlertHistory(path = "A2-X1-R1-Camera", alertType = "", configType = ConfigType.Lane.toString, message = "current event", startTime = formatter.parse("2012-12-29T02:00:00").getTime, endTime = Long.MaxValue, reductionFactor = 0.1f),
        ZkAlertHistory(path = "A2-X1-R1-Camera", alertType = "", configType = ConfigType.Lane.toString, message = "current event", startTime = formatter.parse("2012-12-29T03:00:00")getTime, endTime = Long.MaxValue, reductionFactor = 0.4f),
        ZkAlertHistory(path = "A2-E1-R1-Camera", alertType = "", configType = ConfigType.Lane.toString, message = "current event", startTime = formatter.parse("2012-12-29T13:00:00").getTime, endTime = Long.MaxValue, reductionFactor = 0.2f))
      AlertService.getReductionFactor(alerts) must be(0.6f plusOrMinus 0.001f)
    }
    "return factor when including gantry and system alerts" in {
      val alerts = List(
        ZkAlertHistory(path = "A2-X1-R1-Camera", alertType = "", configType = ConfigType.Lane.toString, message = "current event", startTime = formatter.parse("2012-12-29T02:00:00").getTime, endTime = Long.MaxValue, reductionFactor = 0.1f),
        ZkAlertHistory(path = "A2-X1-PIR", alertType = "", configType = "Gantry", message = "current event", startTime = formatter.parse("2012-12-29T03:00:00")getTime, endTime = Long.MaxValue, reductionFactor = 0.4f),
        ZkAlertHistory(path = "Selftest", alertType = "", configType = "System", message = "current event", startTime = formatter.parse("2012-12-29T13:00:00").getTime, endTime = Long.MaxValue, reductionFactor = 0.2f))
      AlertService.getReductionFactor(alerts) must be(0.7f plusOrMinus 0.001f)
    }
  }

  "AlertService.getReductionFactor using curator" must {
    "return factor when all part of corridor" in {
      val systemId = "A2"
      val corridorId = "cor1"
      createSectionsAndCorridors(systemId, corridorId)
      val alert1 = ZkAlert(path = "A2-X1-R1-Camera", alertType = "", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T02:00:00").getTime, reductionFactor = 0.1f)
      AlertService.create(systemId, corridorId, alert1, zookeeperClient)
      val alert2 = ZkAlert(path = "A2-X1-R2-Camera", alertType = "", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T03:00:00")getTime, reductionFactor = 0.4f)
      AlertService.create(systemId, corridorId, alert2, zookeeperClient)
      val alert3 = ZkAlert(path = "A2-E1-R1-Camera", alertType = "", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T13:00:00").getTime, reductionFactor = 0.2f)
      AlertService.create(systemId, corridorId, alert3, zookeeperClient)
      AlertService.getReductionFactor(zookeeperClient, systemId, corridorId) must be(0.7f plusOrMinus 0.001f)
    }
    "return factor when one event is not part of corridor" in {
      val systemId = "A2"
      val corridorId = "cor1"
      val corridorId2 = "cor2"

      createSectionsAndCorridors(systemId, corridorId)
      val alert1 = ZkAlert(path = "A2-X1-R1-Camera", alertType = "", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T02:00:00").getTime, reductionFactor = 0.1f)
      AlertService.create(systemId, corridorId, alert1, zookeeperClient)
      val alert2 = ZkAlert(path = "A2-M1-R2-Camera", alertType = "", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T03:00:00")getTime, reductionFactor = 0.4f)
      AlertService.create(systemId, corridorId2, alert2, zookeeperClient)
      val alert3 = ZkAlert(path = "A2-E1-R1-Camera", alertType = "", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T13:00:00").getTime, reductionFactor = 0.2f)
      AlertService.create(systemId, corridorId, alert3, zookeeperClient)
      AlertService.getReductionFactor(zookeeperClient, systemId, corridorId) must be(0.3f plusOrMinus 0.001f)
    }
    "return factor when all events are not part of corridor" in {
      val systemId = "A2"
      val corridorId = "cor1"
      val corridorId2 = "cor2"
      createSectionsAndCorridors(systemId, corridorId)
      val alert1 = ZkAlert(path = "A2-X2-R1-Camera", alertType = "", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T02:00:00").getTime, reductionFactor = 0.1f)
      AlertService.create(systemId, corridorId, alert1, zookeeperClient)
      val alert2 = ZkAlert(path = "A2-M1-R2-Camera", alertType = "", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T03:00:00")getTime, reductionFactor = 0.4f)
      AlertService.create(systemId, corridorId, alert2, zookeeperClient)
      val alert3 = ZkAlert(path = "A2-E2-R1-Camera", alertType = "", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T13:00:00").getTime, reductionFactor = 0.2f)
      AlertService.create(systemId, corridorId, alert3, zookeeperClient)
      AlertService.getReductionFactor(zookeeperClient, systemId, corridorId2) must be(0f plusOrMinus 0.001f)
    }
    "return factor when including gantry and system alerts" in {
      val systemId = "A2"
      val corridorId = "cor1"
      createSectionsAndCorridors(systemId, corridorId)
      val alert1 = ZkAlert(path = "A2-X1-R1-Camera", alertType = "", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T02:00:00").getTime, reductionFactor = 0.1f)
      AlertService.create(systemId, corridorId, alert1, zookeeperClient)
      val alert2 = ZkAlert(path = "A2-E1-PIR", alertType = "", configType = ConfigType.Gantry, message = "current event", timestamp = formatter.parse("2012-12-29T03:00:00")getTime, reductionFactor = 0.4f)
      AlertService.create(systemId, corridorId, alert2, zookeeperClient)
      val alert3 = ZkAlert(path = "Selftest", alertType = "", configType = ConfigType.System, message = "current event", timestamp = formatter.parse("2012-12-29T13:00:00").getTime, reductionFactor = 0.2f)
      AlertService.create(systemId, corridorId, alert3, zookeeperClient)
      AlertService.getReductionFactor(zookeeperClient, systemId, corridorId) must be(0.7f plusOrMinus 0.001f)
    }
    "return factor system events when not existing corridor" in {
      val systemId = "A2"
      val corridorId = "cor1"
      val corridorIdNonExisting = "nonExisting"
      val alert1 = ZkAlert(path = "A2-X1-R1-Camera", alertType = "", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T02:00:00").getTime, reductionFactor = 0.1f)
      AlertService.create(systemId, corridorId, alert1, zookeeperClient)
      val alert2 = ZkAlert(path = "A2-E1-PIR", alertType = "", configType = ConfigType.Gantry, message = "current event", timestamp = formatter.parse("2012-12-29T03:00:00")getTime, reductionFactor = 0.4f)
      AlertService.create(systemId, corridorId, alert2, zookeeperClient)
      val alert3 = ZkAlert(path = "Selftest", alertType = "", configType = ConfigType.System, message = "current event", timestamp = formatter.parse("2012-12-29T13:00:00").getTime, reductionFactor = 0.2f)
      AlertService.create(systemId, corridorId, alert3, zookeeperClient)
      AlertService.getReductionFactor(zookeeperClient, systemId, corridorIdNonExisting) must be(0.0f plusOrMinus 0.001f)
    }
  }

  def createSectionsAndCorridors(systemId: String, corridorId: String) {
    val gantryEntryId = "E1"
    val gantryMiddle = "M1"
    val gantryExitId = "X1"
    val sectionId = "S1"
    val sectionId2 = "S2"

    val sectionPath = Paths.Sections.getConfigPath(systemId, sectionId)
    val section = ZkSection("S1", systemId, "Section", 8000, gantryEntryId, gantryMiddle, Nil, Nil)
    zookeeperClient.put(sectionPath, section)
    val sectionPath2 = Paths.Sections.getConfigPath(systemId, sectionId2)
    val section2 = ZkSection("S2", systemId, "Section2", 8000, gantryMiddle, gantryExitId, Nil, Nil)
    zookeeperClient.put(sectionPath2, section2)
    val corridorPath = Paths.Corridors.getConfigPath(systemId, corridorId)
    val corridor = storage.ZkCorridor(id = "10",
      name = "Oost buis",
      roadType = 0,
      serviceType = ServiceType.SectionControl,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = false,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = ZkPSHTMIdentification(true, false, false, false, false, false, false, false, false, "X"),
      info = ZkCorridorInfo(corridorId = 10,
        locationCode = "2",
        reportId = "A2",
        reportHtId = "HT2",
        reportPerformance = true,
        locationLine1 = "Utrecht"),
      startSectionId = sectionId,
      endSectionId = sectionId2,
      allSectionIds = Seq(sectionId, sectionId2),
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 1,
      roadCode = 63,
      dutyType = "",
      deploymentCode = "",
      codeText = "")
    zookeeperClient.put(corridorPath, corridor)

  }
}