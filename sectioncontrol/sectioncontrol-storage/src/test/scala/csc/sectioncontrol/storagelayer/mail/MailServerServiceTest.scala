package csc.sectioncontrol.storagelayer.mail

import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers
import csc.curator.CuratorTestServer
import csc.akkautils.DirectLogging
import csc.sectioncontrol.storage.Paths
import csc.curator.utils.CuratorToolsImpl
import csc.sectioncontrol.messages.MailServer

class MailServerServiceTest extends WordSpec with MustMatchers with BeforeAndAfterAll with CuratorTestServer with DirectLogging {
  val path = Paths.Configuration.getMailServerPath
  var zkStore: CuratorToolsImpl = _
  val mailServer = MailServer(Some(Map("mail.smtp.socketFactory.class" -> "javax.net.ssl.SSLSocketFactor",
    "mail.smtp.socketFactory.port" -> "465",
    "mail.smtp.host" -> "",
    "mail.smtp.starttls.enable" -> "false",
    "mail.smtp.auth" -> "true",
    "mail.from" -> "noreply@csc.com",
    "mail.smtp.port" -> "587")))
  val mailServerForUpdate = MailServer(Some(Map("mail.smtp.socketFactory.class" -> "javax.net.ssl.SSLSocketFactor",
    "mail.smtp.socketFactory.port" -> "465",
    "mail.smtp.host" -> "",
    "mail.smtp.starttls.enable" -> "false",
    "mail.smtp.auth" -> "true",
    "mail.from" -> "noreply@csc.com",
    "mail.smtp.port" -> "666")))

  override def beforeEach() {
    super.beforeEach()
    zkStore = new CuratorToolsImpl(clientScope, log)
  }
  override def afterEach() {
    zkStore.deleteRecursive(path)
    super.afterEach()
  }

  "MailServerServiceTest" must {
    "CREATE the MailServerServiceTest and GET the result" in {
      val mailServerService = new MailServerServiceSmtp(zkStore)
      mailServerService.create(mailServer)
      val result = mailServerService.get()
      result.get must be(mailServer)
    }

    "CREATE the ProcessedRegistrationCompletionStatusService and GET the VERSIONED result" in {
      val mailServerService = new MailServerServiceSmtp(zkStore)
      mailServerService.create(mailServer)
      val result = mailServerService.getVersioned()
      result.get.data must be(mailServer)
    }

    "CREATE the ProcessedRegistrationCompletionStatusService and GET the VERSIONED result and UPDATE" in {
      val mailServerService = new MailServerServiceSmtp(zkStore)
      mailServerService.create(mailServer)
      var result = mailServerService.getVersioned()
      result.get.data must be(mailServer)

      mailServerService.update(mailServerForUpdate, result.get.version)

      result = mailServerService.getVersioned()
      result.get.data must be(mailServerForUpdate)
    }
  }
}

