package csc.sectioncontrol.storagelayer.failures

///**
// * Created by jeijlders on 9/22/16.
// */
//class FailureServiceTest {
//
//}
/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

import csc.sectioncontrol.storagelayer.state.FailureService
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.curator.CuratorTestServer
import csc.akkautils.DirectLogging
import csc.curator.utils.CuratorToolsImpl
import csc.sectioncontrol.storagelayer.hbasetables.SerializerFormats
import java.text.SimpleDateFormat
import csc.sectioncontrol.storage
import storage._
import storage.ZkCorridorInfo
import storage.ZkDirection
import storage.ZkPSHTMIdentification

class FailureServiceTest extends WordSpec with MustMatchers with CuratorTestServer with DirectLogging {
  val formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
  var zookeeperClient: CuratorToolsImpl = _

  override def beforeEach() {
    super.beforeEach()
    zookeeperClient = new CuratorToolsImpl(clientScope, log, SerializerFormats.formats)
  }
  override def afterEach() {
    super.afterEach()
  }

  "FailureService.exists" must {
    "detect same failure" in {
      val systemId = "A2"
      val corridorId = "cor1"
      val failure = ZkFailure(path = "A2-X1-R1-Camera", failureType = "TEST", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T02:00:00").getTime, confirmed = false)
      FailureService.create(systemId, corridorId, failure, zookeeperClient)
      FailureService.exists(systemId, corridorId, failure, zookeeperClient) must be(true)
    }
    "detect simular failure" in {
      val systemId = "A2"
      val corridorId = "cor1"
      val failure = ZkFailure(path = "A2-X1-R1-Camera", failureType = "TEST", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T02:00:00").getTime, confirmed = false)
      FailureService.create(systemId, corridorId, failure, zookeeperClient)
      val simFailure = ZkFailure(path = "A2-X1-R1-Camera", failureType = "TEST", configType = ConfigType.System, message = "different messages", timestamp = formatter.parse("2012-12-29T02:10:00").getTime, confirmed = false)
      FailureService.exists(systemId, corridorId, simFailure, zookeeperClient) must be(true)
    }
    "fail on different path" in {
      val systemId = "A2"
      val corridorId = "cor1"
      val failure = ZkFailure(path = "A2-X1-R1-Camera", failureType = "TEST", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T02:00:00").getTime, confirmed = false)
      FailureService.create(systemId, corridorId, failure, zookeeperClient)
      val simFailure = ZkFailure(path = "A2-X1-R2-Camera", failureType = "TEST", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T02:00:00").getTime, confirmed = false)
      FailureService.exists(systemId, corridorId, simFailure, zookeeperClient) must be(false)
    }
    "fail on different type" in {
      val systemId = "A2"
      val corridorId = "cor1"
      val failure = ZkFailure(path = "A2-X1-R1-Camera", failureType = "TEST", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T02:00:00").getTime, confirmed = false)
      FailureService.create(systemId, corridorId, failure, zookeeperClient)
      val simFailure = ZkFailure(path = "A2-X1-R1-Camera", failureType = "test", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T02:00:00").getTime, confirmed = false)
      FailureService.exists(systemId, corridorId, simFailure, zookeeperClient) must be(false)
    }

  }

  "FailureService.getFailuresWithPath using curator" must {
    "return the ZkFailures with their corresponding zookeeper path/node" in {
      val systemId = "A2"
      val corridorId = "cor1"
      createSectionsAndCorridors(systemId, corridorId)
      val failure1 = ZkFailure(path = "A2-X1-R1-Camera", failureType = "", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T02:00:00").getTime, confirmed = false)
      FailureService.create(systemId, corridorId, failure1, zookeeperClient)
      val failure2 = ZkFailure(path = "A2-X1-R2-Camera", failureType = "", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T02:00:00").getTime, confirmed = false)
      FailureService.create(systemId, corridorId, failure2, zookeeperClient)
      val failure3 = ZkFailure(path = "A2-X1-R3-Camera", failureType = "", configType = ConfigType.Lane, message = "current event", timestamp = formatter.parse("2012-12-29T02:00:00").getTime, confirmed = false)
      FailureService.create(systemId, corridorId, failure3, zookeeperClient)

      val result = FailureService.getFailuresWithPath(systemId, corridorId, endDayTime = formatter.parse("2012-12-29T13:10:00").getTime, zookeeperClient)
      result.foreach { case (a, b) ⇒ a must startWith("/ctes/systems/A2/corridors/cor1/control/errors/failures/current/qn-") }
    }
  }

  def createSectionsAndCorridors(systemId: String, corridorId: String) {
    val gantryEntryId = "E1"
    val gantryMiddle = "M1"
    val gantryExitId = "X1"
    val sectionId = "S1"
    val sectionId2 = "S2"

    val sectionPath = Paths.Sections.getConfigPath(systemId, sectionId)
    val section = ZkSection("S1", systemId, "Section", 8000, gantryEntryId, gantryMiddle, Nil, Nil)
    zookeeperClient.put(sectionPath, section)
    val sectionPath2 = Paths.Sections.getConfigPath(systemId, sectionId2)
    val section2 = ZkSection("S2", systemId, "Section2", 8000, gantryMiddle, gantryExitId, Nil, Nil)
    zookeeperClient.put(sectionPath2, section2)
    val corridorPath = Paths.Corridors.getConfigPath(systemId, corridorId)
    val corridor = storage.ZkCorridor(id = "10",
      name = "Oost buis",
      roadType = 0,
      serviceType = ServiceType.SectionControl,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = false,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = ZkPSHTMIdentification(true, false, false, false, false, false, false, false, false, "X"),
      info = ZkCorridorInfo(corridorId = 10,
        locationCode = "2",
        reportId = "A2",
        reportHtId = "HT2",
        reportPerformance = true,
        locationLine1 = "Utrecht"),
      startSectionId = sectionId,
      endSectionId = sectionId2,
      allSectionIds = Seq(sectionId, sectionId2),
      direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
      radarCode = 1,
      roadCode = 63,
      dutyType = "",
      deploymentCode = "",
      codeText = "")
    zookeeperClient.put(corridorPath, corridor)

  }
}