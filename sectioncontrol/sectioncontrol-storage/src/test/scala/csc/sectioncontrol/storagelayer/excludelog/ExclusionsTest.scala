/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.storagelayer.excludelog

import org.scalatest.matchers.MustMatchers
import java.util.{ Calendar, GregorianCalendar }
import org.scalatest.WordSpec

/**
 *
 * @author Maarten Hazewinkel
 */
class ExclusionsTest extends WordSpec with MustMatchers {
  def jan25HourMillis(hour: Int): Long = new GregorianCalendar(2002, Calendar.JANUARY, 25, hour, 0).getTimeInMillis
  val jan_25_2002_0900 = jan25HourMillis(9)
  val jan_25_2002_1000 = jan25HourMillis(10)
  val jan_25_2002_1100 = jan25HourMillis(11)
  val jan_25_2002_1200 = jan25HourMillis(12)
  val jan_25_2002_1300 = jan25HourMillis(13)
  val jan_25_2002_1400 = jan25HourMillis(14)
  val jan_25_2002_1500 = jan25HourMillis(15)
  val jan_25_2002_1600 = jan25HourMillis(16)
  val jan_25_2002_1700 = jan25HourMillis(17)
  val jan_25_2002_1800 = jan25HourMillis(18)
  val jan_25_2002_1900 = jan25HourMillis(19)
  val jan_25_2002_2000 = jan25HourMillis(20)
  val jan_25_2002_2100 = jan25HourMillis(21)
  val endOfTime = Long.MaxValue - 1

  "Exclusions" must {
    "be empty when no events are supplied" in {
      Exclusions(1, List[SuspendEvent]()).exclusions must be('empty)
    }

    "be empty when one resume event is supplied" in {
      Exclusions(1, List(MockEvent("Source1", jan_25_2002_1000, 1, false, Some("test"), corridorId = Some(1)))).exclusions must be('empty)
    }

    "be empty when multiple resume events are supplied" in {
      Exclusions(1, List(MockEvent("Source1", jan_25_2002_1200, 1, false, Some("test")),
        MockEvent("Source1", jan_25_2002_1300, 1, false, Some("test")),
        MockEvent("Source1", jan_25_2002_1500, 1, false, Some("test")))).exclusions must be('empty)
    }

    "be empty when multiple resume events are supplied out of order" in {
      Exclusions(1, List(MockEvent("Source1", jan_25_2002_1200, 1, false, Some("test")),
        MockEvent("Source1", jan_25_2002_1300, 1, false, Some("test")),
        MockEvent("Source1", jan_25_2002_1500, 1, false, Some("test")),
        MockEvent("Source1", jan_25_2002_1000, 1, false, Some("test")))).exclusions must be('empty)
    }

    "have a single exclusion period from 1 event" in {
      Exclusions(1, List(MockEvent("Source1", jan_25_2002_0900, 1, true, Some("test")))).exclusions must be(List(Exclusion("Source1", jan_25_2002_0900, endOfTime, "All/Source1:test")))
    }

    "have a single exclusion period from multiple events" in {
      Exclusions(1, List(MockEvent("Source1", jan_25_2002_1200, 1, true, Some("test")),
        MockEvent("Source1", jan_25_2002_1300, 1, true, Some("test")),
        MockEvent("Source1", jan_25_2002_1500, 1, false, Some("test")))).exclusions must be(List(Exclusion("Source1", jan_25_2002_1200, jan_25_2002_1500, "All/Source1:test")))
    }

    "have a single exclusion period from multiple out-of-order events" in {
      Exclusions(1, List(MockEvent("Source1", jan_25_2002_1200, 1, false, Some("test")),
        MockEvent("Source1", jan_25_2002_1500, 1, false, Some("test")),
        MockEvent("Source1", jan_25_2002_1300, 1, true, Some("test")),
        MockEvent("Source1", jan_25_2002_1000, 1, false, Some("test")))).exclusions must be(List(Exclusion("Source1", jan_25_2002_1300, jan_25_2002_1500, "All/Source1:test")))
    }

    "combine corridor-specific and general events" in {
      Exclusions(1, List(MockEvent("Source1", jan_25_2002_1200, 1, true, Some("test")),
        MockEvent("Source1", jan_25_2002_1300, 1, true, Some("test")),
        MockEvent("Source1", jan_25_2002_1500, 1, false, Some("test")),
        MockEvent("Source1", jan_25_2002_1000, 1, true, Some("test"), corridorId = Some(1)),
        MockEvent("Source1", jan_25_2002_1100, 1, false, Some("test"), corridorId = Some(1)))).exclusions.toSet must be(Set(Exclusion("Source1", jan_25_2002_1200, jan_25_2002_1500, "All/Source1:test"),
        Exclusion("Source1", jan_25_2002_1000, jan_25_2002_1100, "1/Source1:test")))
    }

    "have two exclusion periods" in {
      Exclusions(1, List(MockEvent("Source1", jan_25_2002_1200, 1, false, Some("test")),
        MockEvent("Source1", jan_25_2002_1300, 1, true, Some("test")),
        MockEvent("Source1", jan_25_2002_1500, 1, false, Some("test")),
        MockEvent("Source1", jan_25_2002_1000, 1, true, Some("test")))).exclusions must be(List(Exclusion("Source1", jan_25_2002_1000, jan_25_2002_1200, "All/Source1:test"),
        Exclusion("Source1", jan_25_2002_1300, jan_25_2002_1500, "All/Source1:test")))
    }

    "have four exclusion periods" in {
      Exclusions(1, List(MockEvent("Source1", jan_25_2002_0900, 1, true, Some("test")),
        MockEvent("Source1", jan_25_2002_1000, 1, true, Some("test")),
        MockEvent("Source1", jan_25_2002_1100, 1, false, Some("test")),
        MockEvent("Source1", jan_25_2002_1200, 1, false, Some("test")),
        MockEvent("Source1", jan_25_2002_1300, 1, true, Some("test")),
        MockEvent("Source1", jan_25_2002_1400, 1, true, Some("test")),
        MockEvent("Source1", jan_25_2002_1500, 1, false, Some("test")),
        MockEvent("Source1", jan_25_2002_1600, 1, false, Some("test")),
        MockEvent("Source1", jan_25_2002_1700, 1, true, Some("test")),
        MockEvent("Source1", jan_25_2002_1800, 1, true, Some("test")),
        MockEvent("Source1", jan_25_2002_1900, 1, false, Some("test")),
        MockEvent("Source1", jan_25_2002_2000, 1, true, Some("test")),
        MockEvent("Source1", jan_25_2002_2100, 1, true, Some("test")))).exclusions must be(List(Exclusion("Source1", jan_25_2002_0900, jan_25_2002_1100, "All/Source1:test"),
        Exclusion("Source1", jan_25_2002_1300, jan_25_2002_1500, "All/Source1:test"),
        Exclusion("Source1", jan_25_2002_1700, jan_25_2002_1900, "All/Source1:test"),
        Exclusion("Source1", jan_25_2002_2000, endOfTime, "All/Source1:test")))
    }

    "handle multiple event at the same effective time" in {
      Exclusions(1, List(MockEvent("Source1", jan_25_2002_0900, 1, true, Some("test")),
        MockEvent("Source1", jan_25_2002_1000, 2, true, Some("test")),
        MockEvent("Source1", jan_25_2002_1000, 1, false, Some("test")),
        MockEvent("Source1", jan_25_2002_1200, 1, false, Some("test")),
        MockEvent("Source1", jan_25_2002_1300, 1, true, Some("test")),
        MockEvent("Source1", jan_25_2002_1400, 2, false, Some("test")),
        MockEvent("Source1", jan_25_2002_1400, 1, true, Some("test")),
        MockEvent("Source1", jan_25_2002_1600, 1, false, Some("test")),
        MockEvent("Source1", jan_25_2002_1700, 2, true, Some("test")),
        MockEvent("Source1", jan_25_2002_1700, 1, true, Some("test")),
        MockEvent("Source1", jan_25_2002_1800, 1, false, Some("test")),
        MockEvent("Source1", jan_25_2002_1900, 1, true, Some("test")),
        MockEvent("Source1", jan_25_2002_2100, 2, false, Some("test")),
        MockEvent("Source1", jan_25_2002_2100, 1, false, Some("test")))).exclusions must be(List(Exclusion("Source1", jan_25_2002_0900, jan_25_2002_1000, "All/Source1:test"),
        Exclusion("Source1", jan_25_2002_1000, jan_25_2002_1200, "All/Source1:test"),
        Exclusion("Source1", jan_25_2002_1300, jan_25_2002_1400, "All/Source1:test"),
        Exclusion("Source1", jan_25_2002_1700, jan_25_2002_1800, "All/Source1:test"),
        Exclusion("Source1", jan_25_2002_1900, jan_25_2002_2100, "All/Source1:test")))
    }

    "consolidate multiple event reasons" in {
      Exclusions(1, List(MockEvent("Source1", jan_25_2002_0900, 1, true, Some("test")),
        MockEvent("Source1", jan_25_2002_1000, 1, true, Some("test2")),
        MockEvent("Source1", jan_25_2002_1200, 1, false, Some("test")))).exclusions must be(List(Exclusion("Source1", jan_25_2002_0900, jan_25_2002_1200, "All/Source1:test,test2")))
    }

    "handle multiple sources" in {
      Exclusions(1, List(MockEvent("Source1", jan_25_2002_0900, 1, true, Some("test")),
        MockEvent("Source1", jan_25_2002_1000, 1, true, Some("test2")),
        MockEvent("Source1", jan_25_2002_1200, 1, false, Some("test")),
        MockEvent("Source2", jan_25_2002_1100, 1, true, Some("test2")),
        MockEvent("Source2", jan_25_2002_1200, 1, true, Some("test2")),
        MockEvent("Source2", jan_25_2002_1400, 1, false, Some("test2")))).exclusions must be(List(Exclusion("Source1", jan_25_2002_0900, jan_25_2002_1200, "All/Source1:test,test2"),
        Exclusion("Source2", jan_25_2002_1100, jan_25_2002_1400, "All/Source2:test2")))
    }

    "intersect correctly across multiple sources" in {
      val exclusions = Exclusions(1, List(MockEvent("Source1", jan_25_2002_0900, 1, true, Some("test")),
        MockEvent("Source1", jan_25_2002_1000, 1, true, Some("test2")),
        MockEvent("Source1", jan_25_2002_1200, 1, false, Some("test")),
        MockEvent("Source2", jan_25_2002_1100, 1, true, Some("test2")),
        MockEvent("Source2", jan_25_2002_1200, 1, true, Some("test2")),
        MockEvent("Source2", jan_25_2002_1400, 1, false, Some("test2"))))

      exclusions.intersectsInterval(jan_25_2002_0900, jan_25_2002_0900) must be(true)
      exclusions.intersectsInterval(jan_25_2002_0900, jan_25_2002_1500) must be(true)
      exclusions.intersectsInterval(jan_25_2002_1100, jan_25_2002_1200) must be(true)
      exclusions.intersectsInterval(jan_25_2002_1300, jan_25_2002_1400) must be(true)
      exclusions.intersectsInterval(jan_25_2002_1300, jan_25_2002_2000) must be(true)
      exclusions.intersectsInterval(jan_25_2002_1400, jan_25_2002_1400) must be(true)
      exclusions.intersectsInterval(jan_25_2002_1400 + 1, jan_25_2002_1400 + 1) must be(false)
      exclusions.intersectsInterval(jan_25_2002_1400 + 1, jan_25_2002_2000) must be(false)
    }

    "intersect correctly for a specified source" in {
      val exclusions = Exclusions(1, List(MockEvent("Source1", jan_25_2002_0900, 1, true, Some("test")),
        MockEvent("Source1", jan_25_2002_1000, 1, true, Some("test2")),
        MockEvent("Source1", jan_25_2002_1200, 1, false, Some("test")),
        MockEvent("Source2", jan_25_2002_1100, 1, true, Some("test2")),
        MockEvent("Source2", jan_25_2002_1200, 1, true, Some("test2")),
        MockEvent("Source2", jan_25_2002_1400, 1, false, Some("test2"))))

      exclusions.intersectsComponentInterval("Source1", jan_25_2002_0900, jan_25_2002_0900) must be(true)
      exclusions.intersectsComponentInterval("Source1", jan_25_2002_0900, jan_25_2002_1500) must be(true)
      exclusions.intersectsComponentInterval("Source1", jan_25_2002_1100, jan_25_2002_1200) must be(true)
      exclusions.intersectsComponentInterval("Source1", jan_25_2002_1300, jan_25_2002_1400) must be(false)
      exclusions.intersectsComponentInterval("Source1", jan_25_2002_1300, jan_25_2002_2000) must be(false)
      exclusions.intersectsComponentInterval("Source1", jan_25_2002_1400, jan_25_2002_1400) must be(false)
      exclusions.intersectsComponentInterval("Source1", jan_25_2002_1400 + 1, jan_25_2002_1400 + 1) must be(false)
      exclusions.intersectsComponentInterval("Source1", jan_25_2002_1400 + 1, jan_25_2002_2000) must be(false)

      exclusions.intersectsComponentInterval("Source2", jan_25_2002_0900, jan_25_2002_0900) must be(false)
      exclusions.intersectsComponentInterval("Source2", jan_25_2002_0900, jan_25_2002_1500) must be(true)
      exclusions.intersectsComponentInterval("Source2", jan_25_2002_1100, jan_25_2002_1200) must be(true)
      exclusions.intersectsComponentInterval("Source2", jan_25_2002_1300, jan_25_2002_1400) must be(true)
      exclusions.intersectsComponentInterval("Source2", jan_25_2002_1300, jan_25_2002_2000) must be(true)
      exclusions.intersectsComponentInterval("Source2", jan_25_2002_1400, jan_25_2002_1400) must be(true)
      exclusions.intersectsComponentInterval("Source2", jan_25_2002_1400 + 1, jan_25_2002_1400 + 1) must be(false)
      exclusions.intersectsComponentInterval("Source2", jan_25_2002_1400 + 1, jan_25_2002_2000) must be(false)
    }
  }

  "An Exclusion" must {
    "calculate intersections correctly" in {
      Exclusion("Source1", jan_25_2002_1100, jan_25_2002_1900, "test").intersects(jan_25_2002_0900, jan_25_2002_1000) must be(false)
      Exclusion("Source1", jan_25_2002_1100, jan_25_2002_1900, "test").intersects(jan_25_2002_0900, jan_25_2002_1100) must be(true)
      Exclusion("Source1", jan_25_2002_1100, jan_25_2002_1900, "test").intersects(jan_25_2002_0900, jan_25_2002_1200) must be(true)
      Exclusion("Source1", jan_25_2002_1100, jan_25_2002_1900, "test").intersects(jan_25_2002_0900, jan_25_2002_1900) must be(true)
      Exclusion("Source1", jan_25_2002_1100, jan_25_2002_1900, "test").intersects(jan_25_2002_0900, jan_25_2002_2100) must be(true)

      Exclusion("Source1", jan_25_2002_1100, jan_25_2002_1900, "test").intersects(jan_25_2002_1100, jan_25_2002_1800) must be(true)
      Exclusion("Source1", jan_25_2002_1100, jan_25_2002_1900, "test").intersects(jan_25_2002_1100, jan_25_2002_1900) must be(true)
      Exclusion("Source1", jan_25_2002_1100, jan_25_2002_1900, "test").intersects(jan_25_2002_1200, jan_25_2002_1900) must be(true)
      Exclusion("Source1", jan_25_2002_1100, jan_25_2002_1900, "test").intersects(jan_25_2002_1200, jan_25_2002_1400) must be(true)

      Exclusion("Source1", jan_25_2002_1100, jan_25_2002_1900, "test").intersects(jan_25_2002_1100, jan_25_2002_2100) must be(true)
      Exclusion("Source1", jan_25_2002_1100, jan_25_2002_1900, "test").intersects(jan_25_2002_1200, jan_25_2002_2100) must be(true)
      Exclusion("Source1", jan_25_2002_1100, jan_25_2002_1900, "test").intersects(jan_25_2002_1900, jan_25_2002_2100) must be(true)
      Exclusion("Source1", jan_25_2002_1100, jan_25_2002_1900, "test").intersects(jan_25_2002_2000, jan_25_2002_2100) must be(false)

      Exclusion("Source1", jan_25_2002_1100, jan_25_2002_1900, "test").intersects(jan_25_2002_0900, jan_25_2002_0900) must be(false)
      Exclusion("Source1", jan_25_2002_1100, jan_25_2002_1900, "test").intersects(jan_25_2002_1100, jan_25_2002_1100) must be(true)
      Exclusion("Source1", jan_25_2002_1100, jan_25_2002_1900, "test").intersects(jan_25_2002_1200, jan_25_2002_1200) must be(true)
      Exclusion("Source1", jan_25_2002_1100, jan_25_2002_1900, "test").intersects(jan_25_2002_1900, jan_25_2002_1900) must be(true)
      Exclusion("Source1", jan_25_2002_1100, jan_25_2002_1900, "test").intersects(jan_25_2002_2000, jan_25_2002_2000) must be(false)

      Exclusion("Source1", jan_25_2002_1100, jan_25_2002_1100, "test").intersects(jan_25_2002_1100, jan_25_2002_1100) must be(true)
      Exclusion("Source1", jan_25_2002_1100, jan_25_2002_1100, "test").intersects(jan_25_2002_1000, jan_25_2002_1100) must be(true)
      Exclusion("Source1", jan_25_2002_1100, jan_25_2002_1100, "test").intersects(jan_25_2002_1000, jan_25_2002_1200) must be(true)
      Exclusion("Source1", jan_25_2002_1100, jan_25_2002_1100, "test").intersects(jan_25_2002_1100, jan_25_2002_1200) must be(true)
      Exclusion("Source1", jan_25_2002_1100, jan_25_2002_1100, "test").intersects(jan_25_2002_0900, jan_25_2002_1000) must be(false)
      Exclusion("Source1", jan_25_2002_1100, jan_25_2002_1100, "test").intersects(jan_25_2002_1200, jan_25_2002_1300) must be(false)
      Exclusion("Source1", jan_25_2002_1100, jan_25_2002_1100, "test").intersects(jan_25_2002_1200, jan_25_2002_1200) must be(false)
      Exclusion("Source1", jan_25_2002_1100, jan_25_2002_1100, "test").intersects(jan_25_2002_1000, jan_25_2002_1000) must be(false)
    }
  }

  private object MockEvent {
    def apply(componentId: String,
              effectiveTimestamp: Long,
              sequenceNumber: Long,
              suspend: Boolean,
              suspensionReason: Option[String],
              corridorId: Option[Int] = None,
              isSuspendOrResumeEvent: Boolean = true): SuspendEvent =
      SuspendEvent(componentId,
        effectiveTimestamp,
        corridorId,
        sequenceNumber,
        suspend,
        suspensionReason)
  }

}
