/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.storage.Paths
import csc.akkautils.DirectLogging
import org.apache.curator.framework.CuratorFramework
import csc.curator.utils.CuratorToolsImpl
import csc.curator.CuratorTestServer

/**
 * Test the NodeAttributeService
 */
class NodeAttributeServiceTest extends WordSpec with MustMatchers with CuratorTestServer with DirectLogging {

  var zookeeperClient: CuratorToolsImpl = _

  override def beforeEach() {
    super.beforeEach()

    zookeeperClient = new CuratorToolsImpl(clientScope, log)
    val pathWithout = Paths.Systems.getConfigPath("A2")
    zookeeperClient.createEmptyPath(pathWithout)

    val pathWith = Paths.Systems.getConfigPath("A3")
    zookeeperClient.createEmptyPath(pathWith)
    zookeeperClient.set(pathWith, new NodeAttributes("Test"), 0)
  }
  override def afterEach() {
    super.afterEach()
  }

  "NodeAttributeService" must {
    "getNodeType without value" in {
      val pathWithout = Paths.Systems.getConfigPath("A2")
      val res = NodeAttributeService.getNodeType(pathWithout, zookeeperClient)
      res must be(None)
    }
    "getNodeType with value" in {
      val pathWith = Paths.Systems.getConfigPath("A3")
      val res = NodeAttributeService.getNodeType(pathWith, zookeeperClient)
      res must be(Some("Test"))
    }
  }

}