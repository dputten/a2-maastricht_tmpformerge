package csc.sectioncontrol.storagelayer

import csc.sectioncontrol.storage._
import csc.sectioncontrol.storage.ZkCorridorInfo
import csc.sectioncontrol.storage.ZkPSHTMIdentification
import csc.sectioncontrol.storage.ZkCorridor

object CorridorBuilder {
  def create(corridorId: String): ZkCorridor = {
    ZkCorridor(id = corridorId,
      name = "S1-name",
      roadType = 0,
      serviceType = ServiceType.SectionControl,
      caseFileType = ZkCaseFileType.HHMVS40,
      isInsideUrbanArea = false,
      dynamaxMqId = "",
      approvedSpeeds = Seq(),
      pshtm = ZkPSHTMIdentification(useYear = false,
        useMonth = true,
        useDay = false,
        useReportingOfficerId = false,
        useNumberOfTheDay = true,
        useLocationCode = false,
        useHHMCode = false,
        useTypeViolation = false,
        useFixedCharacter = false,
        fixedCharacter = "T"),
      info = ZkCorridorInfo(corridorId = 1,
        locationCode = "2",
        reportId = "",
        reportHtId = "",
        reportPerformance = true,
        locationLine1 = ""),
      startSectionId = "sec1",
      endSectionId = "sec1",
      allSectionIds = Seq("sec1"),
      direction = ZkDirection(directionFrom = "",
        directionTo = ""),
      radarCode = 4,
      roadCode = 0,
      dutyType = "",
      deploymentCode = "",
      codeText = "")
  }
}

