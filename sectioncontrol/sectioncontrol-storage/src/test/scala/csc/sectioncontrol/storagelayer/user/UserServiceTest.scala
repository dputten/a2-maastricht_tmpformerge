package csc.sectioncontrol.storagelayer.user

import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.storage.{ ZkRole, ZkUser, Paths }
import csc.curator.CuratorTestServer
import csc.akkautils.DirectLogging
import csc.curator.utils.CuratorToolsImpl
import csc.sectioncontrol.storagelayer.roles.RoleServiceZooKeeper

class UserServiceTest extends WordSpec with MustMatchers with BeforeAndAfterAll with CuratorTestServer with DirectLogging {
  val path = Paths.Users.getRootPath
  var zkStore: CuratorToolsImpl = _
  val user = new ZkUser("1", "test", "0123456789", "1.1.1.1", "testUser", "testUser", "mail@mail.nl", "", "1", Nil)
  val userUpdate = new ZkUser("2", "test", "0123456789", "1.1.1.1", "testUser2", "testUser2", "mail@mail.nl", "", "1", Nil)

  val user1Role = new ZkUser("1", "test1", "0123456789", "1.1.1.1", "testUser1", "testUser1", "mail@mail.nl", "", "1", Nil)
  val user2Role = new ZkUser("2", "test2", "0123456789", "1.1.1.1", "testUser2", "testUser2", "mail@mail.nl", "", "2", Nil)
  val user3Role = new ZkUser("3", "test3", "0123456789", "1.1.1.1", "testUser3", "testUser3", "mail@mail.nl", "", "2", Nil)

  def userService = new UserServiceZooKeeper(zkStore, new RoleServiceZooKeeper(zkStore))
  def roleService = new RoleServiceZooKeeper(zkStore)

  override def beforeEach() {
    super.beforeEach()
    zkStore = new CuratorToolsImpl(clientScope, log)
  }
  override def afterEach() {
    zkStore.deleteRecursive(path)
    super.afterEach()
  }

  "UserService" must {
    "CREATE the USER and GET the result" in {
      userService.create(user)
      val result = userService.get("1")
      result.get.name must be("testUser")
    }

    "CREATE the USER and GET the VERSIONED result" in {
      userService.create(user)
      val result = userService.getVersioned("1")
      result.get.data.name must be("testUser")
    }

    "CREATE the USER and GET the VERSIONED result and UPDATE" in {
      userService.create(user)
      var result = userService.getVersioned("1")
      result.get.data.name must be("testUser")

      userService.update("1", userUpdate, result.get.version)

      result = userService.getVersioned("1")
      result.get.data.name must be("testUser2")
    }

    "CREATE the USER and ROLES and GET the USERS with ROLE" in {
      val role1 = new ZkRole("1", "role1", "test_role_1")
      val role2 = new ZkRole("2", "role2", "test_role_2")
      roleService.create(role1)
      roleService.create(role2)

      userService.create(user1Role)
      userService.create(user2Role)
      userService.create(user3Role)

      var result = userService.getByRole("role1")
      result.length must be(1)
      result.head.get.name must be("testUser1")

      result = userService.getByRole("role2")
      result.length must be(2)
      result.find(u ⇒ u.get.name == "testUser2").head.get.id must be("2")
      result.find(u ⇒ u.get.name == "testUser3").head.get.id must be("3")
    }
  }
}

