/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer

import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers
import csc.curator.CuratorTestServer
import csc.sectioncontrol.storage.Paths
import org.apache.curator.retry.ExponentialBackoffRetry
import csc.curator.utils.CuratorToolsImpl
import akka.event.Logging
import org.apache.curator.framework.CuratorFramework
import csc.akkautils.DirectLogging

class LabelServiceTest extends WordSpec with MustMatchers with BeforeAndAfterAll with CuratorTestServer with DirectLogging {

  var zookeeperClient: CuratorToolsImpl = _
  val labels = Seq(
    new Label("ctes/systems/A2", LabelService.LabelTypes.system, "label1"),
    new Label("ctes/systems/A20", LabelService.LabelTypes.system, "label1"),
    new Label("ctes/systems/A2", LabelService.LabelTypes.system, "label2"),
    new Label("ctes/systems/A2/corridors/1300", LabelService.LabelTypes.corridor, "label2"),
    new Label("ctes/systems/A2corridors/1400", LabelService.LabelTypes.corridor, "label3"))

  override def beforeEach() {
    super.beforeEach()

    zookeeperClient = new CuratorToolsImpl(clientScope, log)
    val labelPath = Paths.Labels.getDefaultPath
    zookeeperClient.createEmptyPath(labelPath)
    zookeeperClient.set(labelPath, labels, 0)
  }
  override def afterEach() {
    super.afterEach()
  }

  "LabelService" must {
    "getAllLabels" in {
      LabelService.getAllLabels(zookeeperClient) must be(labels)
    }
    "getLabels" in {
      var list = LabelService.getLabels("label1", zookeeperClient)
      list.size must be(2)
      list.head must be(labels(0))
      list.last must be(labels(1))
    }
    "getLabels by node" in {
      var list = LabelService.getLabelsByNode("ctes/systems/A2", zookeeperClient)
      list.size must be(2)
      list.head must be(labels(0))
      list.last must be(labels(2))
    }
    "getLabels by type" in {
      var list = LabelService.getLabelsByType(LabelService.LabelTypes.corridor, zookeeperClient)
      list.size must be(2)
      list.head must be(labels(3))
      list.last must be(labels(4))
    }
    "getLabels by label and type" in {
      var list = LabelService.getLabelsByLabelAndType("label2", LabelService.LabelTypes.system, zookeeperClient)
      list.size must be(1)
      list.head must be(labels(2))
    }
    "return empty when failing to find label" in {
      var list = LabelService.getLabels("label5", zookeeperClient)
      list.size must be(0)
    }
  }

}