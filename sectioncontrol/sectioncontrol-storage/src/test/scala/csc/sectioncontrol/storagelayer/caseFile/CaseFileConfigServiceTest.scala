package csc.sectioncontrol.storagelayer.caseFile

import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.storage.{ ExportDirectoryForSpeedLimit, ZkCaseFileConfig, ZkRole, Paths }
import csc.sectioncontrol.storage.{ ZkCaseFileConfig, ZkRole, Paths }
import csc.curator.CuratorTestServer
import csc.akkautils.DirectLogging
import csc.curator.utils.CuratorToolsImpl
import csc.sectioncontrol.storagelayer.CorridorBuilder
import csc.sectioncontrol.storagelayer.corridor.CorridorService

class CaseFileConfigServiceTest extends WordSpec with MustMatchers with BeforeAndAfterAll with CuratorTestServer with DirectLogging {
  val systemId = "A2"
  val corridorId = "S1"
  val path = Paths.Corridors.getCaseFileConfig(systemId, corridorId)
  var zkStore: CuratorToolsImpl = _

  val header = "headerString"
  //val caseFileConfig = new ZkCaseFileConfig(header)
  val caseFileConfig = ZkCaseFileConfig(header, List(ExportDirectoryForSpeedLimit(80, "a0020801")))
  val caseFileConfigUpdate = new ZkRole("2", "role2", "test_role_2")
  def caseFileService = new CaseFileConfigZooKeeperService(zkStore)

  override def beforeEach() {
    super.beforeEach()
    zkStore = new CuratorToolsImpl(clientScope, log)
  }
  override def afterEach() {
    zkStore.deleteRecursive(path)
    super.afterEach()
  }

  "CaseFileService.create/get" must {
    "create the caseFileConfig and get the result" in {
      caseFileService.create(systemId, corridorId, caseFileConfig)
      val result = caseFileService.get(systemId, corridorId)
      result.get.exportDirectoryForSpeedLimits(0).speedlimit must be(80)
      result.get.exportDirectoryForSpeedLimits(0).exportDirectory must be("a0020801")
      result.get.header must be(header)
    }

    "create the caseFileConfig and get the result through the Conrridor.Info.Id" in {
      CorridorService.createCorridor(zkStore, "A12", CorridorBuilder.create("A12"))

      caseFileService.create(systemId, corridorId, caseFileConfig)
      val result = caseFileService.get(systemId, corridorId)
      result.get.exportDirectoryForSpeedLimits(0).speedlimit must be(80)
      result.get.exportDirectoryForSpeedLimits(0).exportDirectory must be("a0020801")
      result.get.header must be(header)
    }
  }

  "CaseFileService.exists" must {
    "return if the exists" in {
      caseFileService.create(systemId, corridorId, caseFileConfig)
      caseFileService.exists(systemId, corridorId) must be(true)
    }
  }
}

