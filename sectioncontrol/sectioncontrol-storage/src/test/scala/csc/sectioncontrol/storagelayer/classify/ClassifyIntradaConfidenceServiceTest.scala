package csc.sectioncontrol.storagelayer.classify

import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers
import csc.curator.CuratorTestServer
import csc.akkautils.DirectLogging
import csc.sectioncontrol.storage.{ ZkClassifyConfidenceIntrada, Paths }
import csc.curator.utils.CuratorToolsImpl

class ClassifyIntradaConfidenceServiceTest extends WordSpec with MustMatchers with BeforeAndAfterAll with CuratorTestServer with DirectLogging {
  val path = Paths.Configuration.getClassifyConfidenceIntradaConfigPath
  var zkStore: CuratorToolsImpl = _

  override def beforeEach() {
    super.beforeEach()
    zkStore = new CuratorToolsImpl(clientScope, log)
  }
  override def afterEach() {
    zkStore.deleteRecursive(path)
    super.afterEach()
  }

  "ClassifyIntradaConfidenceServiceTest" must {
    "create the ZkClassifyConfidenceIntrada and GET the result" in {
      val classifyIntradaConfidenceService = new ClassifyIntradaConfidenceServiceZooKeeper(zkStore)
      val zkClassifyConfidenceIntrada = ZkClassifyConfidenceIntrada(90)
      classifyIntradaConfidenceService.create(zkClassifyConfidenceIntrada)
      val result = classifyIntradaConfidenceService.get()
      result.get.confidence must be(90)
    }
  }
}

