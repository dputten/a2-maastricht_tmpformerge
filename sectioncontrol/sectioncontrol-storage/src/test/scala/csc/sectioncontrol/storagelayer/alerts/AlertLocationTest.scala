/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.alerts

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

class AlertLocationTest extends WordSpec with MustMatchers {
  "AlertLocation" must {
    "get location when lane id uses '-' as separator" in {
      val (systemId, gantryId, laneId) = AlertLocation.getEventLocation("A2-X1-X1R1-camera", "Lane")
      systemId must be("A2")
      gantryId must be("X1")
      laneId must be("X1R1")
    }
    "get location when lane id uses '/' as separator" in {
      val (systemId, gantryId, laneId) = AlertLocation.getEventLocation("A2/X1/X1R1/camera", "Lane")
      systemId must be("A2")
      gantryId must be("X1")
      laneId must be("X1R1")
    }
    "get location when lane id uses '\\' as separator" in {
      val (systemId, gantryId, laneId) = AlertLocation.getEventLocation("A2\\X1\\X1R1\\camera", "Lane")
      systemId must be("A2")
      gantryId must be("X1")
      laneId must be("X1R1")
    }
    "get location when lane id uses '|' separator |" in {
      val (systemId, gantryId, laneId) = AlertLocation.getEventLocation("A2|X1|X1R1|camera", "Lane")
      systemId must be("A2")
      gantryId must be("X1")
      laneId must be("X1R1")
    }
    "get location when gantry id uses '-' as separator" in {
      val (systemId, gantryId, laneId) = AlertLocation.getEventLocation("A2-X1-pir", "Gantry")
      systemId must be("A2")
      gantryId must be("X1")
      laneId must be("")
    }

    // TODO next 2 tests incomprehensible. Why state 'must get location' if the getEventLocation method
    // return empty strings on this test and state 'must fail' for the next even though it also returns
    // empty strings
    "get location when System" in {
      val (systemId, gantryId, laneId) = AlertLocation.getEventLocation("selftest", "System")
      systemId must be("")
      gantryId must be("")
      laneId must be("")
    }
    "fail to get location when invalid format" in {
      val (systemId, gantryId, laneId) = AlertLocation.getEventLocation("A2|X1|X1R1|camera-1", "System")
      systemId must be("")
      gantryId must be("")
      laneId must be("")
    }
  }

}