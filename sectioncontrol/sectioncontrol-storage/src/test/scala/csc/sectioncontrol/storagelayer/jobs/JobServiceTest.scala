package csc.sectioncontrol.storagelayer.jobs

import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.storage.Paths
import csc.curator.CuratorTestServer
import csc.akkautils.DirectLogging
import csc.curator.utils.CuratorToolsImpl
import csc.sectioncontrol.jobs.ProcessedRegistrationCompletionStatus

class JobServiceTest extends WordSpec with MustMatchers with BeforeAndAfterAll with CuratorTestServer with DirectLogging {
  val systemId = "A4"
  val path = Paths.Systems.getProcessedRegistrationsStatusPath(systemId)
  var zkStore: CuratorToolsImpl = _
  val processedRegistrationCompletionStatus = ProcessedRegistrationCompletionStatus(1, 2)

  override def beforeEach() {
    super.beforeEach()
    zkStore = new CuratorToolsImpl(clientScope, log)
  }
  override def afterEach() {
    zkStore.deleteRecursive(path)
    super.afterEach()
  }

  "ProcessedRegistrationCompletionStatusService" must {
    "CREATE the ProcessedRegistrationCompletionStatusService and GET the result" in {
      val processedRegistrationCompletionStatusService = new ProcessedRegistrationCompletionStatusService(zkStore)
      processedRegistrationCompletionStatusService.create(systemId, processedRegistrationCompletionStatus)
      val result = processedRegistrationCompletionStatusService.get(systemId)
      result.get.lastUpdateTime must be(1)
      result.get.completeUntil must be(2)
    }

    "CREATE the ProcessedRegistrationCompletionStatusService and GET the VERSIONED result" in {
      val processedRegistrationCompletionStatusService = new ProcessedRegistrationCompletionStatusService(zkStore)
      processedRegistrationCompletionStatusService.create(systemId, processedRegistrationCompletionStatus)
      val result = processedRegistrationCompletionStatusService.getVersioned(systemId)
      result.get.data.lastUpdateTime must be(1)
      result.get.data.completeUntil must be(2)
    }

    "CREATE the ProcessedRegistrationCompletionStatusService and GET the VERSIONED result and UPDATE" in {
      val processedRegistrationCompletionStatusService = new ProcessedRegistrationCompletionStatusService(zkStore)
      processedRegistrationCompletionStatusService.create(systemId, processedRegistrationCompletionStatus)
      var result = processedRegistrationCompletionStatusService.getVersioned(systemId)
      result.get.data.lastUpdateTime must be(1)
      result.get.data.completeUntil must be(2)

      val updateProcessedRegistrationCompletionStatus = ProcessedRegistrationCompletionStatus(3, 4)
      processedRegistrationCompletionStatusService.update(systemId, updateProcessedRegistrationCompletionStatus, result.get.version)

      result = processedRegistrationCompletionStatusService.getVersioned(systemId)
      result.get.data.lastUpdateTime must be(3)
      result.get.data.completeUntil must be(4)
    }
  }
}

