/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.corridor

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.curator.CuratorTestServer
import csc.curator.utils.CuratorToolsImpl
import csc.akkautils.DirectLogging
import csc.sectioncontrol.storage
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storagelayer.hbasetables.SerializerFormats
import csc.sectioncontrol.storage.ZkCorridorInfo
import csc.sectioncontrol.storage.ZkDailyReportConfig
import csc.sectioncontrol.storage.ZkSection
import csc.sectioncontrol.storage.ZkPSHTMIdentification
import csc.sectioncontrol.storage.ZkDirection

class CorridorServiceTest extends WordSpec with MustMatchers with CuratorTestServer with DirectLogging {

  var zookeeperClient: CuratorToolsImpl = _

  override def beforeEach() {
    super.beforeEach()
    zookeeperClient = new CuratorToolsImpl(clientScope, log, SerializerFormats.formats)
  }
  override def afterEach() {
    super.afterEach()
  }

  "CorridorService.getGantriesForCorridor" must {
    "get both gantries of corridor with one section" in {
      val systemId = "A2"
      val gantryEntryId = "E1"
      val gantryExitId = "X1"
      val sectionId = "S1"
      val corridorId = "10"
      val gantryPath = Paths.Sections.getConfigPath(systemId, sectionId)
      val gantry = ZkSection("S1", systemId, "Section", 8000, gantryEntryId, gantryExitId, Nil, Nil)
      zookeeperClient.put(gantryPath, gantry)
      val corridorPath = Paths.Corridors.getConfigPath(systemId, corridorId)
      val corridor = storage.ZkCorridor(id = "10",
        name = "Oost buis",
        roadType = 0,
        serviceType = ServiceType.SectionControl,
        caseFileType = ZkCaseFileType.HHMVS40,
        isInsideUrbanArea = false,
        dynamaxMqId = "",
        approvedSpeeds = Seq(),
        pshtm = ZkPSHTMIdentification(true, false, false, false, false, false, false, false, false, "X"),
        info = ZkCorridorInfo(corridorId = 10,
          locationCode = "2",
          reportId = "A2",
          reportHtId = "HT2",
          reportPerformance = true,
          locationLine1 = "Utrecht"),
        startSectionId = sectionId,
        endSectionId = sectionId,
        allSectionIds = Seq(sectionId),
        direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
        radarCode = 1,
        roadCode = 63,
        dutyType = "",
        deploymentCode = "",
        codeText = "")
      zookeeperClient.put(corridorPath, corridor)

      val gantries = CorridorService.getGantriesForCorridor(zookeeperClient, systemId, corridorId)
      gantries.size must be(2)
      gantries must contain(gantryEntryId)
      gantries must contain(gantryExitId)
    }
    "get both gantries of corridor with two sections" in {
      val systemId = "A2"
      val gantryEntryId = "E1"
      val gantryMiddle = "M1"
      val gantryExitId = "X1"
      val sectionId = "S1"
      val sectionId2 = "S2"
      val corridorId = "10"
      val sectionPath = Paths.Sections.getConfigPath(systemId, sectionId)
      val section = ZkSection("S1", systemId, "Section", 8000, gantryEntryId, gantryMiddle, Nil, Nil)
      zookeeperClient.put(sectionPath, section)
      val sectionPath2 = Paths.Sections.getConfigPath(systemId, sectionId2)
      val section2 = ZkSection("S2", systemId, "Section2", 8000, gantryMiddle, gantryExitId, Nil, Nil)
      zookeeperClient.put(sectionPath2, section2)
      val corridorPath = Paths.Corridors.getConfigPath(systemId, corridorId)
      val corridor = storage.ZkCorridor(id = "10",
        name = "Oost buis",
        roadType = 0,
        serviceType = ServiceType.SectionControl,
        caseFileType = ZkCaseFileType.HHMVS40,
        isInsideUrbanArea = false,
        dynamaxMqId = "",
        approvedSpeeds = Seq(),
        pshtm = ZkPSHTMIdentification(true, false, false, false, false, false, false, false, false, "X"),
        info = ZkCorridorInfo(corridorId = 10,
          locationCode = "2",
          reportId = "A2",
          reportHtId = "HT2",
          reportPerformance = true,
          locationLine1 = "Utrecht"),
        startSectionId = sectionId,
        endSectionId = sectionId2,
        allSectionIds = Seq(sectionId),
        direction = ZkDirection(directionFrom = "Utrecht", directionTo = "Amsterdam"),
        radarCode = 1,
        roadCode = 63,
        dutyType = "",
        deploymentCode = "",
        codeText = "")
      zookeeperClient.put(corridorPath, corridor)

      val gantries = CorridorService.getGantriesForCorridor(zookeeperClient, systemId, corridorId)
      gantries.size must be(2)
      gantries must contain(gantryEntryId)
      gantries must contain(gantryExitId)
    }
  }

  val systemId = "A2"
  val corridorId = "S1"
  val dailyReportConfigSpeedEntry100 = DailyReportConfigSpeedEntry("100", "TCS UT A2 R-1 (100 km/h)", "a0021001")
  val dailyReportConfigSpeedEntry130 = DailyReportConfigSpeedEntry("130", "TCS UT A2 R-1 (130 km/h)", "a0021301")
  val dailyReportConfigSpeedEntryTotal = DailyReportConfigSpeedEntry("total", "TCS UT A2 R-1 (totaal)", "a0021001, a0021301 (totaal)")
  val ignoreStatisticsOfLane1 = IgnoreStatisticsOfLane("A2", "E1", "E1R1")
  val ignoreStatisticsOfLane2 = IgnoreStatisticsOfLane("A2", "E1", "E1R2")
  val ignoreStatisticsOfLanes = List(ignoreStatisticsOfLane1, ignoreStatisticsOfLane2)
  val speedEntries = List(dailyReportConfigSpeedEntry100, dailyReportConfigSpeedEntry130, dailyReportConfigSpeedEntryTotal)
  val dailyReportConfig = ZkDailyReportConfig(ignoreStatisticsOfLanes, speedEntries)
  val dailyReportConfigEmpty = ZkDailyReportConfig(Nil, speedEntries)

  "CorridorService.createDailyReportConfig" must {
    "createDailyReportConfig" in {
      CorridorService.createDailyReportConfig(zookeeperClient, systemId, corridorId, dailyReportConfig)
      CorridorService.existsDailyReportConfig(zookeeperClient, systemId, corridorId) must be(true)
      val result = CorridorService.getDailyReportConfig(zookeeperClient, systemId, corridorId)
      result.get must be(dailyReportConfig)
      result.get.ignoreStatisticsOfLanes must be(ignoreStatisticsOfLanes)
      result.get.speedEntries must be(speedEntries)
    }
    "createDailyReportConfig with empty ignoreStatisticsOfLanes" in {
      CorridorService.createDailyReportConfig(zookeeperClient, systemId, corridorId, dailyReportConfigEmpty)
      CorridorService.existsDailyReportConfig(zookeeperClient, systemId, corridorId) must be(true)
      val result = CorridorService.getDailyReportConfig(zookeeperClient, systemId, corridorId)
      result.get must be(dailyReportConfigEmpty)
      result.get.ignoreStatisticsOfLanes must be(Nil)
      result.get.speedEntries must be(speedEntries)
    }
  }
  "CorridorService.deleteDailyReportConfig" must {
    "deleteDailyReportConfig" in {
      CorridorService.createDailyReportConfig(zookeeperClient, systemId, corridorId, dailyReportConfig)
      CorridorService.existsDailyReportConfig(zookeeperClient, systemId, corridorId) must be(true)
      CorridorService.deleteDailyReportConfig(zookeeperClient, systemId, corridorId)
      CorridorService.existsDailyReportConfig(zookeeperClient, systemId, corridorId) must be(false)
    }
  }
}