package csc.sectioncontrol.storagelayer

import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import org.scalatest.matchers.MustMatchers
import csc.sectioncontrol.storagelayer.calibrationstate.CalibrationStateService
import csc.sectioncontrol.storage.{ ZkTriggerData, ZkCalibrationState, Paths }
import csc.curator.utils.CuratorToolsImpl
import csc.curator.CuratorTestServer
import csc.akkautils.DirectLogging

class CalibrationStateServiceTest extends WordSpec with MustMatchers with BeforeAndAfterAll with CuratorTestServer with DirectLogging {
  val path = Paths.CalibrationState.getDefaultPath("A4", "R2")
  val systemId = "A4"
  val corridorId = "R2"
  var zkStore: CuratorToolsImpl = _
  val calibrationStates = Seq(new ZkCalibrationState(true))
  val zkTriggerData = ZkTriggerData(1, "a", "a", true, "a")

  override def beforeEach() {
    super.beforeEach()
    zkStore = new CuratorToolsImpl(clientScope, log)
  }
  override def afterEach() {
    zkStore.deleteRecursive(path)
    super.afterEach()
  }

  "CalibrationStateService" must {
    "retrieve the stored CalibrationState" in {

      CalibrationStateService.create(systemId, corridorId, zkTriggerData, zkStore)
      val result = CalibrationStateService.getTriggerData(systemId, corridorId, zkStore)
      result.get.userId must be("a")
      CalibrationStateService.isCalibrating(systemId, corridorId, zkStore) must be(true)
    }
  }
}
