/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.backup

import java.io.File
import xml.{ XML }
import csc.config.Path
import csc.curator.utils.Curator
import org.slf4j.LoggerFactory

case class ImportResult(success: Boolean = false, path: String, errorMessage: Option[String])

/**
 * used to import a xml file and create the configuration that is in the file
 */
object Importer {
  val log = LoggerFactory.getLogger(getClass)

  def getVersion(curator: Curator, path: String): Int = {
    val stats = curator.stat(path)
    stats.map(_.getVersion).getOrElse(0)
  }

  /**
   * create zookeeper tree wih given xml file
   * @param xml of zookeeper structure
   * @return
   */
  def importConfig(curator: Curator, xml: File, expectedVersion: Option[String] = None): Seq[ImportResult] = {
    val root = XML.loadFile(xml)
    val version = (root \ "@version").text

    if (version != expectedVersion.getOrElse(version)) {
      List(ImportResult(success = false, path = "Version", errorMessage = Some("Expected Version %s, found %s".format(expectedVersion, version))))
    } else {
      val nodes = root \ "znodes" \ "znode"
      nodes.map {
        node ⇒
          val path = (node \ "path").text
          val data = (node \ "data").text

          try {
            if (!curator.exists(Path(path))) {
              curator.createEmptyPath(Path(path))
            }
            if (!data.trim().isEmpty) {
              val version = getVersion(curator, path)
              setZookeeperData(curator, path, data, version)
            }
            ImportResult(success = true, path = path, errorMessage = None)
          } catch {
            case e: Exception ⇒
              log.error("Could not import node %s".format(path), e)
              ImportResult(success = false, path = path, errorMessage = Some("Could not import node %s: %s".format(path, e.getMessage)))
          }
      }
    }
  }

  def setZookeeperData(curator: Curator, path: String, data: String, version: Int) {
    curator.curator.foreach { c ⇒
      try {
        c.setData().withVersion(version).forPath(path, data.getBytes)
      } catch {
        case e: Throwable ⇒ {
          log.error("Failed to store value %s at %s for version %d.".format(data, path, version), e)
          throw e
        }
      }
    }
  }
}
