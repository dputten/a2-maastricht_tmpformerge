package csc.sectioncontrol.storagelayer.jobs

import csc.curator.utils.{ Versioned, Curator }
import csc.sectioncontrol.storage.Paths
import csc.config.Path
import csc.sectioncontrol.jobs.{ ValidationFilterCompletionStatus, ProcessedRegistrationCompletionStatus }

class ProcessedRegistrationCompletionStatusService(zkStore: Curator) {
  def create(systemId: String, processedRegistrationCompletionStatus: ProcessedRegistrationCompletionStatus) = {
    zkStore.put(Paths.Systems.getProcessedRegistrationsStatusPath(systemId), processedRegistrationCompletionStatus)
  }

  def get(systemId: String): Option[ProcessedRegistrationCompletionStatus] = {
    zkStore.get[ProcessedRegistrationCompletionStatus](Path(Paths.Systems.getProcessedRegistrationsStatusPath(systemId)))
  }

  def getVersioned(systemId: String): Option[Versioned[ProcessedRegistrationCompletionStatus]] = {
    zkStore.getVersioned[ProcessedRegistrationCompletionStatus](Paths.Systems.getProcessedRegistrationsStatusPath(systemId))
  }

  def update(systemId: String, processedRegistrationCompletionStatus: ProcessedRegistrationCompletionStatus, version: Int) = {
    zkStore.set(Paths.Systems.getProcessedRegistrationsStatusPath(systemId), processedRegistrationCompletionStatus, version)
  }

  def delete(systemId: String) = {
    zkStore.deleteRecursive(Paths.Systems.getProcessedRegistrationsStatusPath(systemId))
  }

  def exists(systemId: String) = {
    zkStore.exists(Paths.Systems.getProcessedRegistrationsStatusPath(systemId))
  }
}

class ValidationFilterCompletionStatusService(zkStore: Curator) {
  def create(systemId: String, validationFilterCompletionStatus: ValidationFilterCompletionStatus) = {
    zkStore.put(Paths.Systems.getValidationFilterCompletionStatus(systemId), validationFilterCompletionStatus)
  }

  def get(systemId: String): Option[ValidationFilterCompletionStatus] = {
    zkStore.get[ValidationFilterCompletionStatus](Path(Paths.Systems.getValidationFilterCompletionStatus(systemId)))
  }

  def getVersioned(systemId: String): Option[Versioned[ValidationFilterCompletionStatus]] = {
    zkStore.getVersioned[ValidationFilterCompletionStatus](Paths.Systems.getValidationFilterCompletionStatus(systemId))
  }

  def update(systemId: String, validationFilterCompletionStatus: ValidationFilterCompletionStatus, version: Int) = {
    zkStore.set(Paths.Systems.getValidationFilterCompletionStatus(systemId), validationFilterCompletionStatus, version)
  }

  def delete(systemId: String) = {
    zkStore.deleteRecursive(Paths.Systems.getValidationFilterCompletionStatus(systemId))
  }

  def exists(systemId: String) = {
    zkStore.exists(Paths.Systems.getValidationFilterCompletionStatus(systemId))
  }
}
