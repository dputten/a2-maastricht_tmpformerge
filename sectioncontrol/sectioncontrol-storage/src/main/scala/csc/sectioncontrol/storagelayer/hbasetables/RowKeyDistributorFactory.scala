/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.hbasetables

import com.sematext.hbase.wd.{ RowKeyDistributorByHashPrefix, AbstractRowKeyDistributor }
import csc.curator.utils.Curator
import csc.hbase.utils.NoDistributionOfRowKey
import com.sematext.hbase.wd.RowKeyDistributorByHashPrefix.OneByteSimpleHash
import csc.sectioncontrol.storage.Paths

object RowKeyDistributorFactory {
  val defaultNrBuckets = 10
  private var listConfiguration = Map[String, Int]()
  private var listRowKeys = Map[String, AbstractRowKeyDistributor]()

  def init(curator: Curator) {
    if (listRowKeys.isEmpty) {
      val path = Paths.Configuration.getHBaseTablesRowKeys
      listConfiguration = curator.get[Map[String, Int]](path).getOrElse(Map())
    }
  }

  def getDistributor(tableName: String): AbstractRowKeyDistributor = {
    listRowKeys.getOrElse(tableName, {
      val nrBuckets = listConfiguration.getOrElse(tableName, defaultNrBuckets)
      val dist = if (nrBuckets <= 0) {
        new NoDistributionOfRowKey()
      } else {
        new RowKeyDistributorByHashPrefix(new OneByteSimpleHash(nrBuckets))
      }
      listRowKeys += tableName -> dist
      dist
    })
  }
  def clear() {
    listConfiguration = Map()
    listRowKeys = Map()
  }
}