package csc.sectioncontrol.storagelayer.user

import csc.sectioncontrol.storage.{ ZkUser, Paths }
import csc.curator.utils.{ Versioned, Curator }
import csc.sectioncontrol.storagelayer.roles.RoleService

trait UserService {
  def create(user: ZkUser)
  def get(userId: String): Option[ZkUser]
  def getVersioned(userId: String): Option[Versioned[ZkUser]]
  def update(userId: String, user: ZkUser, version: Int)
  def delete(userId: String)
  def exists(userId: String)
  def getAll(): Seq[Option[ZkUser]]
  def getByRole(roleName: String): Seq[Option[ZkUser]]
}

class UserServiceZooKeeper(zkStore: Curator, roleService: RoleService) extends UserService {
  def create(user: ZkUser) = {
    zkStore.put(Paths.Users.getRootPath + "/" + user.id, user)
  }

  def get(userId: String): Option[ZkUser] = {
    zkStore.get[ZkUser](Paths.Users.getUserPath(userId))
  }

  def getVersioned(userId: String): Option[Versioned[ZkUser]] = {
    zkStore.getVersioned[ZkUser](Paths.Users.getUserPath(userId))
  }

  def update(userId: String, user: ZkUser, version: Int) = {
    zkStore.set(Paths.Users.getUserPath(userId), user, version)
  }

  def delete(userId: String) = {
    zkStore.deleteRecursive(Paths.Users.getUserPath(userId))
  }

  def exists(userId: String) = {
    zkStore.exists(Paths.Users.getUserPath(userId))
  }

  def getAll(): Seq[Option[ZkUser]] = {
    zkStore.getChildren(Paths.Users.getRootPath).map(
      path ⇒ zkStore.get[ZkUser](path))
  }

  def getByRole(roleName: String): Seq[Option[ZkUser]] = {
    val role = roleService.getByName(roleName)
    role match {
      case Some(r) ⇒
        val users = getAll()
        users.filter(u ⇒ u.get.roleId == r.id)
      case None ⇒
        Nil
    }
  }
}

