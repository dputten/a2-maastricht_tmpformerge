package csc.sectioncontrol.storagelayer.classify

import csc.sectioncontrol.storage.{ ZkClassifyConfidenceIntrada, Paths }
import csc.curator.utils.Curator

trait ClassifyIntradaConfidenceService {
  def create(classifyConfidenceIntrada: ZkClassifyConfidenceIntrada)

  def get(): Option[ZkClassifyConfidenceIntrada]
}

class ClassifyIntradaConfidenceServiceZooKeeper(zkStore: Curator) extends ClassifyIntradaConfidenceService {
  def create(zkClassifyConfidenceIntrada: ZkClassifyConfidenceIntrada) = {
    zkStore.put(Paths.Configuration.getClassifyConfidenceIntradaConfigPath, zkClassifyConfidenceIntrada)
  }

  def get(): Option[ZkClassifyConfidenceIntrada] = {
    zkStore.get[ZkClassifyConfidenceIntrada](Paths.Configuration.getClassifyConfidenceIntradaConfigPath)
  }
}

