/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.hbasetables

import csc.akkautils.DirectLogging
import csc.hbase.utils._
import csc.sectioncontrol.storage.HBaseTableDefinitions._
import org.apache.hadoop.hbase.client.{ Scan, HTable }
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.conf.Configuration
import csc.sectioncontrol.messages.{ ValueWithConfidence, VehicleMetadata }
import com.sematext.hbase.wd.DistributedScanner
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp
import org.apache.hadoop.hbase.filter.{ SubstringComparator, SingleColumnValueFilter }

class VehicleReader(table: HTable, tableName: String) extends JsonHBaseReader[(String, Long), VehicleMetadata] {
  val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(tableName)

  def hbaseReaderTable = table

  def hbaseReaderDataColumnFamily = vehicleRecordColumnFamily

  def hbaseReaderDataColumnName = vehicleRecordColumnName

  def makeReaderKey(keyValue: (String, Long)) = Bytes.toBytes("%s-%s".format(keyValue._1, keyValue._2))
  override def jsonReaderFormats = SerializerFormats.formats

  /**
   * find VehicleMetadata using a period and a licenseplate number.
   * @param systemId the systemID
   * @param gantryId the GantryID
   * @param license the license when empty ignore the license filter
   * @param from the from date in msec
   * @param to the to date in msec
   * @return the found vehicle registrations
   */
  def readRows(systemId: String, gantryId: String, license: String, from: Long, to: Long): Seq[VehicleMetadata] = {
    val prefix = "%s-%s".format(systemId, gantryId)
    readRows(prefix, license: String, from: Long, to: Long)
  }

  /**
   * find VehicleMetadata using a period and a licenseplate number.
   * @param prefix format <systemId>-<gantryId>
   * @param license the license when empty ignore the license filter
   * @param from the from date in msec
   * @param to the to date in msec
   * @return the found vehicle registrations
   */
  def readRows(prefix: String, license: String, from: Long, to: Long): Seq[VehicleMetadata] = {
    import collection.JavaConversions._

    val cf = hbaseReaderDataColumnFamily.getBytes
    val column = hbaseReaderDataColumnName.getBytes
    val startKey = makeReaderKey(prefix, from)
    val stopKey = makeReaderKey(prefix, to)
    val scan = new Scan(startKey, stopKey)
    scan.setCaching(hbaseRowsCaching)
    if (!license.trim.isEmpty) {
      val comp = new SubstringComparator(license)
      val filter = new SingleColumnValueFilter(
        cf,
        vehicleRecordLicenseColumnName.getBytes,
        CompareOp.EQUAL,
        comp)
      scan.setFilter(filter)
    }
    val scanner = DistributedScanner.create(hbaseReaderTable, scan, rowKeyDistributer)
    try {
      val result = scanner.map(r ⇒ deserialize(r.getValue(cf, column))).toSeq

      // There is some pretty strange behaviour here from HBase. When the license column is None it is included in the
      // result of the filter, the same for a part of a license. When two characters match in the license column it
      // is filtered as to be included in the filter result.
      // In the past a partly match was anticipated behaviour so it is again included in the filter.
      // The empty ones need to be filtered out. (TCU-174)
      if (!license.trim.isEmpty) {
        result.filter((vehicleMetaData) ⇒ vehicleMetaData.license match {
          case None           ⇒ false
          case Some(_license) ⇒ _license.value.contains(license)
        })
      } else {
        result
      }
    } finally {
      scanner.close()
    }
  }
}

/**
 * Utility for writing vehicle metadata into vehicle table.
 *
 * Key format used is: `system-gantry-timestamp-lane`
 *
 * @param table
 * @param tableName
 */
class VehicleWriter(table: HTable, tableName: String) extends JsonHBaseMultiWriter[VehicleMetadata, VehicleMetadata] with DirectLogging {
  val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(tableName)
  def hbaseWriterTable = table

  override val hbaseWriterDataColumnFamily = vehicleRecordColumnFamily

  override val hbaseWriterDataColumns = List(
    (vehicleRecordColumnName, (x: VehicleMetadata) ⇒ Some(serialize(x))),
    (vehicleRecordLicenseColumnName, (x: VehicleMetadata) ⇒ x.license.map(l ⇒ l.value.getBytes())))

  override def makeKey(data: vehicleRecordType): Array[Byte] = {
    val tmp = "%s-%s-%s-%s".format(data.lane.system, data.lane.gantry, data.eventTimestamp.toString, data.lane.name)
    log.info("key for lane {} is {}", data.lane.name, tmp)
    Bytes.toBytes(tmp)
  }
  override def jsonWriterFormats = SerializerFormats.formats

  /**
   * Method for writing a single metadata into the table
   *
   * @param metadata
   */
  def writeMetadata(metadata: VehicleMetadata): Unit = {
    writeRow(metadata, metadata)
  }

  /**
   * Method for writing a metadata batch into vehicle table
   *
   * @param rows
   */
  def writeMetadataRows(rows: Seq[VehicleMetadata]): Unit = {
    writeRows(rows.map(m ⇒ (m, m)))
  }
}

/**
 * Interface for writing into / reading from vehicle table
 */
object VehicleTable {
  type Reader = VehicleReader
  type Writer = VehicleWriter

  /**
   * Creates a vehicle table reader of the type [[VehicleTable.Reader]]
   *
   * @param hbaseConfig
   * @param tableKeeper
   * @return
   */
  def makeReader(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper): Reader = {
    val tableName = vehicleRecordTableName
    HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, vehicleRecordColumnFamily)
    val table = new HTable(hbaseConfig, tableName)
    tableKeeper.addTable(table)
    new VehicleReader(table, tableName)
  }

  /**
   * Creates a vehicle table writer of the type [[VehicleTable.Writer]]
   *
   * @param hbaseConfig
   * @param tableKeeper
   * @return
   */
  def makeWriter(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper): Writer = {
    val tableName = vehicleRecordTableName
    HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, vehicleRecordColumnFamily)
    val table = new HTable(hbaseConfig, tableName)
    tableKeeper.addTable(table)
    new VehicleWriter(table, tableName)
  }
}