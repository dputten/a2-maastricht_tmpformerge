/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.hbasetables

import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.messages._
import net.liftweb.json.DefaultFormats
import csc.sectioncontrol.storage._

object SerializerFormats {
  val formats = DefaultFormats + new EnumerationSerializer(ProcessingIndicator, VehicleCode, VehicleCategory, ZkWheelbaseType, ZkIndicationType, ServiceType, ZkCaseFileType, EsaProviderType, VehicleImageType, ConfigType, ConfigType, SpeedIndicatorType)
}