/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer

/**
 * Class to conatin the speed margins
 * @param baseSpeed the speed
 * @param margin the margin
 */
case class SpeedMargin(baseSpeed: Int, margin: Int)

/**
 * Companion class to be able to sort a list of SpeedMargins
 */
object SpeedMargin {
  implicit val speedMarginOrdering = Ordering.by[SpeedMargin, Int](_.baseSpeed)
}
