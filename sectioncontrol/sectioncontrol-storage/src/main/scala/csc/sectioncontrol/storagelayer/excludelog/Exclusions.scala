/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.storagelayer.excludelog

import collection.immutable
import immutable.List
import annotation.tailrec
import java.text.SimpleDateFormat
import java.util.Date

/**
 * Holds the intervals of time during which violations should not be registered.
 * Created based on the event log and a given date.
 *
 * It is assumed that still-active suspend events are re-logged at midnight, so that when
 * the exclude log for the next day is created, it needs to look at only the events from midnight
 * forward.
 * If this assumption is not honored, then we will have to adjust the code to look back through the
 * entire history of the event log.
 *
 * @author Maarten Hazewinkel
 */
case class Exclusions private (corridorId: Int, exclusions: immutable.Seq[Exclusion]) {
  val dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS Z")

  def intersectsIntervalReason(from: Long, to: Long): Option[String] =
    exclusions.find(_.intersects(from, to)).map(excl ⇒
      "%s (%s-%s)".format(excl.reason,
        dateFormat.format(new Date(excl.from)),
        dateFormat.format(new Date(excl.to))))

  def intersectsInterval(from: Long, to: Long): Boolean =
    exclusions.find(_.intersects(from, to)).isDefined

  def intersectsComponentInterval(componentId: String, from: Long, to: Long): Boolean =
    exclusions.find(e ⇒ e.intersects(from, to) && e.componentId == componentId).isDefined
}

object Exclusions {
  /**
   * Create an ExcludeLog from the given events
   */
  def apply(corridorId: Int, events: List[SuspendEvent]): Exclusions = {
    val exclusions = events.
      groupBy(_.sourceId).toIterable.
      flatMap {
        case (sourceId, eventsBySource) ⇒
          makeExclusions(sourceId, eventsBySource.sortBy(e ⇒ (e.effectiveTimestamp, e.sequenceNumber)))
      }.
      toList.sortBy(_.from)
    Exclusions(corridorId, exclusions)
  }

  /**
   * Create an empty ExcludeLog
   */
  def apply(corridorId: Int): Exclusions = apply(corridorId, immutable.Seq[Exclusion]())

  @tailrec
  private def makeExclusions(sourceId: String, events: List[SuspendEvent], exclusions: List[Exclusion] = Nil): List[Exclusion] = {
    (events: @unchecked) match {
      // unchecked match since the compiler doesn't evaluate the guards for exhaustiveness

      case one :: two :: rest if one.suspend && two.suspend ⇒
        // two suspends: consolidate into one suspend
        makeExclusions(sourceId, consolidate(one, two) :: rest, exclusions)

      case one :: two :: rest if one.suspend && !two.suspend ⇒
        // suspend-resume: add new exclusion for this pair
        makeExclusions(sourceId, rest, Exclusion(one.componentId,
          one.effectiveTimestamp,
          two.effectiveTimestamp,
          sourceId,
          one.suspensionReason.get) :: exclusions)

      case one :: two :: rest if !one.suspend ⇒
        // extra resume event: discard
        makeExclusions(sourceId, two :: rest, exclusions)

      case last :: Nil if last.suspend ⇒
        // final suspend event. add new exclusion running till end of time
        Exclusion(last.componentId,
          last.effectiveTimestamp,
          endOfTime,
          sourceId,
          last.suspensionReason.get) ::
          exclusions

      case last :: Nil if !last.suspend ⇒
        // final resume event: discard
        exclusions

      case Nil ⇒
        // no more events
        exclusions
    }
  }

  private def consolidate(one: SuspendEvent, two: SuspendEvent): SuspendEvent = {
    val reasonOne = one.suspensionReason.getOrElse("")
    def concatSuspensionReason = {
      val reasonTwo = two.suspensionReason.getOrElse("")
      lazy val appendReasonTwo = "," + reasonTwo
      if (reasonOne == reasonTwo || reasonOne.endsWith(appendReasonTwo))
        Some(reasonOne)
      else
        Some(reasonOne + appendReasonTwo)
    }
    SuspendEvent(componentId = one.componentId,
      effectiveTimestamp = one.effectiveTimestamp,
      corridorId = one.corridorId,
      sequenceNumber = one.sequenceNumber,
      suspend = one.suspend,
      suspensionReason = concatSuspensionReason)
  }

  val endOfTime = Long.MaxValue - 1
}

case class Exclusion(componentId: String, from: Long, to: Long, reason: String) {
  def intersects(queryFrom: Long, queryTo: Long): Boolean =
    !(queryTo < from) && !(queryFrom > to)
}

object Exclusion {
  def apply(componentId: String, from: Long, to: Long, sourceId: String, reason: String): Exclusion =
    Exclusion(componentId, from, to, sourceId + ":" + reason)
}

