/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.hbasetables

import csc.hbase.utils._
import csc.sectioncontrol.storage.HBaseTableDefinitions._
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.conf.Configuration
import csc.sectioncontrol.classify.nl.DecisionResult

object DecisionResultTable {
  type Reader = GenReader[String, DecisionResult]
  type Writer = GenWriter[String, DecisionResult]

  def createReader(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper): Reader = {
    val tableName = decisionTableName

    val table = new HTable(hbaseConfig, tableName)
    tableKeeper.addTable(table)
    new JsonHBaseReader[String, DecisionResult] {
      val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(tableName)
      def hbaseReaderTable = table
      def hbaseReaderDataColumnFamily = decisionColumnFamily
      def hbaseReaderDataColumnName = decisionColumnName
      def makeReaderKey(keyValue: String) =
        Bytes.toBytes(keyValue)
      override def jsonReaderFormats = SerializerFormats.formats
    }
  }

  def makeWriter(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper): Writer = {
    val tableName = decisionTableName
    val columnFamily = decisionColumnFamily
    val columnName = decisionColumnName

    HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, columnFamily)
    val table = new HTable(hbaseConfig, tableName)
    tableKeeper.addTable(table)
    new JsonHBaseWriter[String, DecisionResult] {
      val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(tableName)
      def hbaseWriterTable = table
      def hbaseWriterDataColumnFamily = columnFamily
      def hbaseWriterDataColumnName = columnName
      def makeWriterKey(keyValue: String) =
        Bytes.toBytes(keyValue)
      override def jsonWriterFormats = SerializerFormats.formats
    }
  }

}