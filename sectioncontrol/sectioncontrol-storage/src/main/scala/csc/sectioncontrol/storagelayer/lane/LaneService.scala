package csc.sectioncontrol.storagelayer.lane

import csc.curator.utils.{ Curator }
import csc.sectioncontrol.storage.{ ZkLaneConfig, Paths }

object LaneService {
  /**
   * returns all lanes within a given gantryId
   * @param systemId is id of system
   * @param gantryId is id of gantry
   * @return sequence of VersionOf[PirRadarCameraSensorConfig]
   */
  def getAllLaneIdsForGantry(systemId: String, gantryId: String, zkStore: Curator): Seq[String] = {
    val path = Paths.Lane.getDefaultPath(systemId, gantryId)
    var lanes = List[String]()

    if (zkStore.exists(path)) {
      val allLanes = zkStore.getChildren(path)
      allLanes.foreach { lanePath ⇒
        val versionOfLane = zkStore.getVersioned[ZkLaneConfig](lanePath + "/" + "config")
        if (versionOfLane.isDefined)
          lanes ::= versionOfLane.get.data.id
      }
    }
    lanes
  }
}
