package csc.sectioncontrol.storagelayer.hbasetables

import com.sematext.hbase.wd.DistributedScanner
import csc.hbase.utils._
import csc.sectioncontrol.messages._
import csc.sectioncontrol.storage.HBaseTableDefinitions._
import net.liftweb.json.Serialization
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.client.{ Scan, Result, Get, HTable }
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter
import org.apache.hadoop.hbase.util.Bytes

/**
 * TODO RB missing javadoc
 * @author csomogyi
 */
class ProcessingTableReader(table: HTable, tableName: String, caching: Int = 100) {
  val rowKeyDistributor = RowKeyDistributorFactory.getDistributor(tableName)
  val cf = processingColumnFamily.getBytes
  val formats = SerializerFormats.formats

  /**
   * Some
   * @param lane
   * @param time
   * @return
   */
  def makeKey(lane: Lane, time: Option[Long]): Array[Byte] = {
    val tmp = time match {
      case Some(_time) ⇒ "%s-%s-%s-%s".format(lane.system, lane.gantry, _time.toString, lane.name)
      case None        ⇒ "%s-%s-%s".format(lane.system, lane.gantry, lane.name)
    }
    Bytes.toBytes(tmp)
  }

  def makeBeginGantryKey(systemId: String, gantryId: String) = {
    Bytes.toBytes("%s-%s-".format(systemId, gantryId))
  }

  /**
   * Tricky way to determine the end of the scan range.
   * Dot is higher in string sorting order than minus.
   *
   * @param systemId
   * @param gantryId
   * @return
   */
  def makeEndGantryKey(systemId: String, gantryId: String) = {
    Bytes.toBytes("%s-%s.".format(systemId, gantryId))
  }

  def deserializeColumn[T <: AnyRef](bytes: Array[Byte])(implicit mt: Manifest[T]): T = {
    Serialization.read[T](Bytes.toString(bytes))(formats, mt)
  }

  def deserialize(result: Result): ProcessingRecord = {
    deserializeColumn[ProcessingRecord](result.getValue(cf, processingRecordColumnName.getBytes))
  }

  def getRecord(lane: Lane, time: Long): Option[ProcessingRecord] = {
    getRecordInternal(lane, Some(time))
  }

  def getGantryTimeRecord(lane: Lane): Option[ProcessingRecord] = {
    getRecordInternal(lane, None)
  }

  def getAllGantryTimes(): List[ProcessingRecord] = getGantryTimes(new Scan())

  def getGantryTimes(systemId: String, gantryId: String): List[ProcessingRecord] = {
    val scan = new Scan(makeBeginGantryKey(systemId, gantryId), makeEndGantryKey(systemId, gantryId))
    getGantryTimes(scan)
  }

  private def getGantryTimes(scan: Scan): List[ProcessingRecord] = {
    import collection.JavaConversions._

    scan.setCaching(caching)
    scan.setFilter(new SingleColumnValueFilter(cf, processingComponentColumnName.getBytes, CompareOp.EQUAL,
      GantryTimeProcessingRecord.ComponentName.getBytes))
    val scanner = DistributedScanner.create(table, scan, rowKeyDistributor)
    try {
      scanner.map { r ⇒ deserialize(r) }.toList
    } finally {
      scanner.close()
    }
  }

  def getRecordInternal(lane: Lane, time: Option[Long]): Option[ProcessingRecord] = {
    val key = rowKeyDistributor.getDistributedKey(makeKey(lane, time))
    val get = new Get(key)
    val result = table.get(get)
    if (result.isEmpty) {
      None
    } else {
      Some(deserialize(result))
    }
  }
}

/**
 * Utility class for creating a processing table writer
 *
 * keys used:
 * - ''system-gantry-timestamp-lane'' for regular registration records
 * - ''system-gantry-lane'' for special gantry time records
 *
 * @param table
 * @param tableName
 */
class ProcessingTableWriter(table: HTable, tableName: String) extends JsonHBaseMultiWriter[(Lane, Option[Long]), ProcessingRecord] {
  val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(tableName)
  def hbaseWriterTable = table

  override val hbaseWriterDataColumnFamily = processingColumnFamily

  override val hbaseWriterDataColumns = List(
    (processingComponentColumnName, (r: ProcessingRecord) ⇒ Some(r.component.getBytes)),
    (processingStateColumnName, (r: ProcessingRecord) ⇒ Some(r.state.getBytes)),
    (processingTimeColumnName, (r: ProcessingRecord) ⇒ Some(Bytes.toBytes(r.time))),
    (processingRecordColumnName, (r: ProcessingRecord) ⇒ Some(serialize(r))))

  /**
   * Creates key in the format ''system-gantry-timestamp-lane''. ''Timestamp'' is optional.
   *
   * @param data
   * @return
   */
  override def makeKey(data: (Lane, Option[Long])): Array[Byte] = {
    val tmp = data._2 match {
      case Some(time) ⇒ "%s-%s-%s-%s".format(data._1.system, data._1.gantry, time.toString, data._1.name)
      case None       ⇒ "%s-%s-%s".format(data._1.system, data._1.gantry, data._1.name)
    }
    Bytes.toBytes(tmp)
  }

  override def jsonWriterFormats = SerializerFormats.formats

  /**
   * Writes a gantry time registration record
   *
   * @param lane
   * @param record
   */
  def writeGantryTimeRecord(lane: Lane, record: ProcessingRecord): Unit = {
    writeRow((lane, None), record)
  }

  /**
   * Write a single processing record
   *
   * @param lane
   * @param record
   */
  def writeRecord(lane: Lane, record: ProcessingRecord): Unit = {
    writeRow((lane, Some(record.time)), record)
  }

  /**
   * Write a batch of processing records
   *
   * @param rows
   */
  def writeRecords(rows: Seq[(Lane, ProcessingRecord)]): Unit = {
    writeRows(rows.map(r ⇒ ((r._1, Some(r._2.time)), r._2)))
  }
}

/**
 * Interface for writing / reading in processing table
 */
object ProcessingTable {
  type Reader = ProcessingTableReader
  type Writer = ProcessingTableWriter

  /**
   * Creates a reader of type [[ProcessingTable.Reader]]
   *
   * @param hbaseConfig
   * @param tableKeeper
   * @return
   */
  def makeReader(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper): Reader = {
    val tableName = processingTableName
    HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, processingColumnFamily)
    val table = new HTable(hbaseConfig, tableName)
    tableKeeper.addTable(table)
    new ProcessingTableReader(table, tableName)
  }

  /**
   * Creates a writer of type [[ProcessingTable.Writer]]
   *
   * @param hbaseConfig
   * @param tableKeeper
   * @return
   */
  def makeWriter(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper): Writer = {
    val tableName = processingTableName
    HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, processingColumnFamily)
    val table = new HTable(hbaseConfig, tableName)
    tableKeeper.addTable(table)
    new ProcessingTableWriter(table, tableName)
  }
}
