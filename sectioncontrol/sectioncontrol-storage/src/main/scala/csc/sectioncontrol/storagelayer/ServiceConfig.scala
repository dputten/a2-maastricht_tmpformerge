/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer

import csc.sectioncontrol.messages.VehicleImageType

trait ServiceConfig {
  val flgFileName: Option[String]

  //override to indicate the first image to export
  val firstImage: Option[VehicleImageType.Value]

  //override to indicate the second image to export
  val secondImage: Option[VehicleImageType.Value]
}

case class ANPRConfig(flgFileName: Option[String] = None,
                      dynamicEnforcement: Boolean = false,
                      firstImage: Option[VehicleImageType.Value] = None,
                      secondImage: Option[VehicleImageType.Value] = None) extends ServiceConfig

case class RedLightConfig(flgFileName: Option[String] = Some("roodlicht.flg"),
                          dynamicEnforcement: Boolean = false,
                          firstImage: Option[VehicleImageType.Value] = None,
                          secondImage: Option[VehicleImageType.Value] = None,
                          pardonRedTime: Long,
                          minimumYellowTime: Long,
                          factNumber: String) extends ServiceConfig

case class SpeedFixedConfig(flgFileName: Option[String] = Some("snelheid.flg"),
                            dynamicEnforcement: Boolean = false,
                            firstImage: Option[VehicleImageType.Value] = None,
                            secondImage: Option[VehicleImageType.Value] = None) extends ServiceConfig

case class SpeedMobileConfig(flgFileName: Option[String] = Some("snelheid.flg"),
                             dynamicEnforcement: Boolean = false,
                             firstImage: Option[VehicleImageType.Value] = None,
                             secondImage: Option[VehicleImageType.Value] = None) extends ServiceConfig

case class TargetGroupConfig(flgFileName: Option[String] = Some("doelgroep.flg"),
                             dynamicEnforcement: Boolean = false,
                             firstImage: Option[VehicleImageType.Value] = None,
                             secondImage: Option[VehicleImageType.Value] = None,
                             factNumber: String) extends ServiceConfig

case class SectionControlConfig(flgFileName: Option[String] = Some("traject.flg"),
                                dynamicEnforcement: Boolean = false,
                                firstImage: Option[VehicleImageType.Value] = None,
                                secondImage: Option[VehicleImageType.Value] = None) extends ServiceConfig
