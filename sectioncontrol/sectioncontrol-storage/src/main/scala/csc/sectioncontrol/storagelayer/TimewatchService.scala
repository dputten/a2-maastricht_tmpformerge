package csc.sectioncontrol.storagelayer

import csc.sectioncontrol.storage.Paths
import csc.sectioncontrol.messages.TimeChanges
import csc.curator.utils.Curator

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 4/22/15.
 */

object TimewatchService {

  def set(curator: Curator, systemId: String, state: TimeChanges) {
    val path = stateStoragePath(systemId)
    if (curator.exists(path))
      curator.delete(path)
    curator.put(path, state)
  }

  def get(curator: Curator, systemId: String): TimeChanges = {
    val path = stateStoragePath(systemId)
    val state = if (curator.exists(path))
      curator.get[TimeChanges](path)
    else
      None
    state.getOrElse(TimeChanges(None, None))
  }
  private def stateStoragePath(systemId: String) = Paths.Systems.getTimewatchStatePath(systemId)

}
