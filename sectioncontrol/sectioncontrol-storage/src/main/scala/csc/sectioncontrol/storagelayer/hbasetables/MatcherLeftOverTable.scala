/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.hbasetables

import csc.hbase.utils._
import csc.sectioncontrol.storage.HBaseTableDefinitions._
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.util.Bytes
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.messages.{ EsaProviderType, VehicleImageType, VehicleCode, VehicleCategory }
import csc.sectioncontrol.storage._
import org.apache.hadoop.conf.Configuration

object MatcherLeftOverTable {
  //TODO: Rewrite this to the RWSTore
  type Store = HBaseStore[vehicleRecordType]

  def makeStore(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper, corridorId: String): Store = {
    val tableName = "matcherLeftOver"
    HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, vehicleRecordColumnFamily)
    val table = new HTable(hbaseConfig, tableName)
    tableKeeper.addTable(table)
    new JsonHBaseStore[vehicleRecordType] {
      val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(tableName)
      def hbaseTable = table

      def hbaseDataColumnFamily = vehicleRecordColumnFamily

      def hbaseDataColumnName = corridorId

      def makeKey(v: vehicleRecordType) = Bytes.toBytes("%s-%s-%s".format(v.lane.system, v.eventTimestamp, v.lane.gantry))

      override def jsonFormats = {
        super.jsonFormats + new EnumerationSerializer(VehicleCategory, ZkWheelbaseType, VehicleCode, ZkIndicationType, ServiceType, ZkCaseFileType, EsaProviderType, VehicleImageType, ConfigType)
      }
    }
  }

}