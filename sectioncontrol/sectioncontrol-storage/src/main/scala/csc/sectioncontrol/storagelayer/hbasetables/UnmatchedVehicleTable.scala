/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.hbasetables

import csc.hbase.utils._
import csc.sectioncontrol.storage.HBaseTableDefinitions._
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.util.Bytes
import csc.sectioncontrol.messages.{ VehicleMetadata, VehicleUnmatchedRecord }
import org.apache.hadoop.conf.Configuration

object UnmatchedVehicleTable {
  // key <systemId>-<GantryId>-<time>-<lcorridorId>
  // key1 <systemId>-<GantryId>-
  // key2 <time>
  type Reader = GenReader[(String, Long), VehicleUnmatchedRecord]
  type Writer = GenWriter[VehicleUnmatchedRecord, VehicleUnmatchedRecord]

  private val tableName = "unmatchedVehicle"

  def makeReader(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper): Reader = {
    HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, vehicleRecordColumnFamily)
    val table = new HTable(hbaseConfig, tableName)
    tableKeeper.addTable(table)

    new JsonHBaseReader[(String, Long), VehicleUnmatchedRecord] {
      val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(tableName)
      def hbaseReaderTable = table
      override val hbaseReaderDataColumnFamily = vehicleRecordColumnFamily
      override val hbaseReaderDataColumnName = vehicleRecordColumnName

      override def makeReaderKey(data: (String, Long)): Array[Byte] = Bytes.toBytes("%s-%s".format(data._1, data._2))
      override def jsonReaderFormats = SerializerFormats.formats
    }
  }

  def makeWriter(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper): Writer = {
    HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, vehicleRecordColumnFamily)
    val table = new HTable(hbaseConfig, tableName)
    tableKeeper.addTable(table)
    new JsonHBaseWriter[VehicleUnmatchedRecord, VehicleUnmatchedRecord] {
      val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(tableName)
      def hbaseWriterTable = table
      def hbaseWriterDataColumnFamily = vehicleRecordColumnFamily
      def hbaseWriterDataColumnName = vehicleRecordColumnName

      def makeWriterKey(keyValue: VehicleUnmatchedRecord) =
        Bytes.toBytes("%s-%s-%s-%s".format(keyValue.entry.lane.system, keyValue.entry.eventTimestamp, keyValue.entry.lane.gantry, keyValue.corridorId))

      override def jsonWriterFormats = SerializerFormats.formats
    }
  }

}