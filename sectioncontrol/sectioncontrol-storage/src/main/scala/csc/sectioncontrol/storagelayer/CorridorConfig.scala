/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer

import csc.sectioncontrol.storage.{ ZkCorridorInfo, ZkPSHTMIdentification, ZkCaseFileType, ServiceType }

/**
 * Class to contain the corridor data including the service configuration
 * @param id
 * @param name
 * @param roadType
 * @param serviceType
 * @param caseFileType
 * @param isInsideUrbanArea
 * @param dynamaxMqId
 * @param approvedSpeeds
 * @param pshtm
 * @param info
 * @param startSectionId
 * @param endSectionId
 * @param allSectionIds
 * @param serviceConfig
 */
case class CorridorConfig(
  id: String,
  name: String,
  roadType: Int,
  serviceType: ServiceType.Value,
  caseFileType: ZkCaseFileType.Value = ZkCaseFileType.HHMVS40,
  isInsideUrbanArea: Boolean,
  dynamaxMqId: String,
  approvedSpeeds: Seq[Int],
  pshtm: ZkPSHTMIdentification,
  info: ZkCorridorInfo,
  startSectionId: String,
  endSectionId: String,
  allSectionIds: Seq[String],
  serviceConfig: ServiceConfig)

