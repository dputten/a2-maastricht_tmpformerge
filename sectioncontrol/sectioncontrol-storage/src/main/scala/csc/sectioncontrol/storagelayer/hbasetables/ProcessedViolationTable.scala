/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.hbasetables

import csc.hbase.utils._
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storage.HBaseTableDefinitions._
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.util.Bytes
import csc.sectioncontrol.storage.KeyWithTimeIdAndPostFix
import org.apache.hadoop.conf.Configuration
import csc.sectioncontrol.storage.KeyWithTimeIdAndPostFix
import scala.Some
import net.liftweb.json.DefaultFormats
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.messages.{ VehicleImageType, VehicleCategory, VehicleCode, ProcessingIndicator }

object ProcessedViolationTable {
  type Reader = GenReader[KeyWithTimeIdAndPostFix, processedViolationRecordType]
  type Writer = RWStore[processedViolationRecordType, processedViolationRecordType]
  type Store = RWStore[KeyWithTimeIdAndPostFix, processedViolationRecordType]

  def makeReader(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper): Reader = {
    val tableName = processedViolationTableName
    HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, processedViolationColumnFamily)
    val table = new HTable(hbaseConfig, tableName)
    tableKeeper.addTable(table)

    new JsonHBaseReader[KeyWithTimeIdAndPostFix, processedViolationRecordType] {
      val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(tableName)
      def hbaseReaderTable = table

      def hbaseReaderDataColumnFamily = processedViolationColumnFamily

      def hbaseReaderDataColumnName = processedViolationColumnName

      def makeReaderKey(keyValue: KeyWithTimeIdAndPostFix) = keyValue.postFix match {
        case Some(postFix) ⇒ Bytes.toBytes("%s-%s-%s".format(keyValue.id, keyValue.time, postFix))
        case None          ⇒ Bytes.toBytes("%s-%s".format(keyValue.id, keyValue.time))
      }
      override def jsonReaderFormats = SerializerFormats.formats
    }
  }

  def makeWriter(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper): Writer = {
    val tableName = processedViolationTableName
    HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, processedViolationColumnFamily)
    val table = new HTable(hbaseConfig, tableName)
    tableKeeper.addTable(table)
    new JsonHBaseRWStore[processedViolationRecordType, processedViolationRecordType] with GenerateId {
      val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(tableName)
      def hbaseTable = table
      def hbaseDataColumnFamily = processedViolationColumnFamily
      def hbaseDataColumnName = processedViolationColumnName
      def makeKey(data: processedViolationRecordType) = {
        val tmp = "%s-%s-%s-%s".format(
          data.violationRecord.classifiedRecord.speedRecord.entry.lane.system,
          data.violationRecord.classifiedRecord.speedRecord.time,
          data.violationRecord.classifiedRecord.speedRecord.entry.lane.gantry,
          nextId)
        Bytes.toBytes(tmp)
      }

      override def jsonFormats = SerializerFormats.formats
    }
  }

  def makeStore(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper): Store = {
    val tableName = processedViolationTableName
    HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, processedViolationColumnFamily)
    val table = new HTable(hbaseConfig, tableName)
    tableKeeper.addTable(table)
    new JsonHBaseRWStore[KeyWithTimeIdAndPostFix, processedViolationRecordType] with GenerateId {
      val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(tableName)
      def hbaseTable = table
      def hbaseDataColumnFamily = processedViolationColumnFamily
      def hbaseDataColumnName = processedViolationColumnName
      def makeKey(keyValue: KeyWithTimeIdAndPostFix) = keyValue.postFix match {
        case Some(postFix) ⇒ Bytes.toBytes("%s-%s-%s-%s".format(keyValue.id, keyValue.time, postFix, nextId))
        case None          ⇒ Bytes.toBytes("%s-%s".format(keyValue.id, keyValue.time))
      }

      override def jsonFormats = SerializerFormats.formats
    }
  }

}