/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.alerts

import csc.sectioncontrol.storage.{ ZkErrorHistory, ZkAlertHistory, Paths, ZkAlert }
import csc.config.Path
import csc.curator.utils.Curator
import csc.sectioncontrol.storagelayer.corridor.{ CorridorIdMapping, CorridorService }

object AlertService {
  val endOfTime = Long.MaxValue

  def getSystemAlertsWithPath(curator: Curator, systemId: String, corridorId: String, endTime: Long): List[(String, ZkAlert)] = {
    val currentAlertPath = Path(Paths.Alert.getCurrentPath(systemId, corridorId))
    val currentAlertPaths: List[Path] = if (curator.exists(currentAlertPath)) curator.getChildren(currentAlertPath).toList else Nil
    currentAlertPaths.map(path ⇒ (path.toString(), getByPath(path, curator).get)).filter(_._2.timestamp < endTime)
  }

  def getSystemAlerts(curator: Curator, systemId: String, corridorId: String, endTime: Long): List[ZkAlert] = {
    val currentAlertPath = Path(Paths.Alert.getCurrentPath(systemId, corridorId))
    val currentAlertPaths: List[Path] = if (curator.exists(currentAlertPath)) curator.getChildren(currentAlertPath).toList else Nil
    currentAlertPaths.map(path ⇒ getByPath(path, curator)).flatten.filter(alert ⇒ alert.timestamp < endTime)
  }

  def getSystemAlertsAfter(curator: Curator, systemId: String, corridorId: String, endTime: Long): List[ZkAlert] = {
    val currentAlertPath = Path(Paths.Alert.getCurrentPath(systemId, corridorId))
    val currentAlertPaths: List[Path] = if (curator.exists(currentAlertPath)) curator.getChildren(currentAlertPath).toList else Nil
    currentAlertPaths.map(path ⇒ getByPath(path, curator)).flatten.filter(alert ⇒ alert.timestamp > endTime)
  }

  def alertsExist(curator: Curator, systemId: String, corridorId: String): Boolean = {
    curator.getChildren(Paths.Alert.getCurrentPath(systemId, corridorId)).size > 0
  }

  def getByPath(path: String, zkStore: Curator): Option[ZkAlert] = {
    zkStore.get[ZkAlert](path)
  }

  def getByPath(path: Path, zkStore: Curator): Option[ZkAlert] = {
    zkStore.get[ZkAlert](path)
  }

  def exists(systemId: String, corridorId: String, alert: ZkAlert, zkStore: Curator): Boolean = {
    val paths = zkStore.getChildren(Paths.Alert.getCurrentPath(systemId, corridorId))
    val events: Seq[ZkAlert] = paths.flatMap(p ⇒ getByPath(p, zkStore))
    events.exists(_.isSimilar(alert))
  }

  def create(systemId: String, corridorId: String, alert: ZkAlert, zkStore: Curator) = {
    zkStore.appendEvent(Paths.Alert.getCurrentPrefixPath(systemId, corridorId), alert)
  }

  def addToHistory(systemId: String, corridorId: String, alert: ZkAlertHistory, zkStore: Curator) = {
    val historyPath = Path(Paths.Alert.getHistoryPath(systemId, corridorId)) / alert.endTime.toString
    zkStore.appendEvent(historyPath, alert)
  }

  def signOff(systemId: String, corridorId: String, path: String, endTime: Long, zkStore: Curator) {
    if (!path.isEmpty) {
      //get alert
      val alertOpt = zkStore.get[ZkAlert](path)
      alertOpt.foreach(alert ⇒ {
        //delete alert
        zkStore.delete(path)
        //write to Alert log
        val historyPath = Path(Paths.Alert.getHistoryPath(systemId, corridorId)) / endTime.toString
        zkStore.appendEvent(historyPath,
          new ZkAlertHistory(path = alert.path,
            alertType = alert.alertType,
            message = alert.message,
            startTime = alert.timestamp,
            endTime = endTime,
            configType = alert.configType.toString,
            reductionFactor = alert.reductionFactor))
      })
    }
  }

  def getAlertLogHistory(curator: Curator, systemId: String, startTime: Long, endTime: Long): Map[CorridorIdMapping, List[ZkAlertHistory]] = {
    val corridors = CorridorService.getCorridorIdMapping(curator, systemId)
    corridors.map(corridorId ⇒ corridorId -> getAlertLogHistory(curator, systemId, corridorId.corridorId, startTime, endTime)).toMap
  }
  /**
   * Get all alerts which are overlapping this day
   * Also add the current alerts and end them with endOfTime
   * @return ZkAlertHistory
   */
  def getAlertLogHistory(curator: Curator, systemId: String, corridorId: String, startTime: Long, endTime: Long): List[ZkAlertHistory] = {
    //get list of all available alertHistory logs
    val alertsHistoryPath = Path(Paths.Alert.getHistoryPath(systemId, corridorId))
    val historyAlertsPaths: List[Path] = if (curator.exists(alertsHistoryPath)) curator.getChildren(alertsHistoryPath).toList else Nil

    //get alert paths after start(DayTime)
    val historyAlertsAfterStartTimePaths = historyAlertsPaths.filter(p ⇒ {
      val nodeTime = ZkErrorHistory.getTimeFromNodeName(p) // is the endtime of the history-alert
      nodeTime > startTime
    })

    val historyAlertsAfterStartTime = historyAlertsAfterStartTimePaths.flatMap(path ⇒ curator.get[ZkAlertHistory](path))

    //filter alerts started after end out
    val historyAlerts = historyAlertsAfterStartTime.filter(his ⇒ his.startTime < endTime)

    //add current alerts
    val currentAlerts = getSystemAlertsAfter(curator, systemId, corridorId, startTime)

    historyAlerts ++ currentAlerts.map(al ⇒ new ZkAlertHistory(path = al.path,
      alertType = al.alertType,
      message = al.message,
      startTime = al.timestamp,
      endTime = endOfTime,
      configType = al.configType.toString,
      reductionFactor = al.reductionFactor))
  }

  /**
   * Get the reduction factor for the specified corridor
   * @return the reduction Factor
   */
  def getReductionFactor(curator: Curator, systemId: String, corridorId: String): Float = {
    val filteredAlerts = getSystemAlerts(curator, systemId, corridorId, System.currentTimeMillis())
    calculateReduction(createAlertsFromSystemAlerts(filteredAlerts))
  }

  def getReductionFactor(alerts: Seq[ZkAlertHistory]): Float = {
    val filteredAlerts = createAlertsFromHistoryAlerts(alerts)
    calculateReduction(filteredAlerts)
  }

  /**
   * Create the Alerts from History alerts
   * @return
   */
  private def createAlertsFromHistoryAlerts(alerts: Seq[ZkAlertHistory]): Seq[Alert] = {
    alerts.map(alert ⇒ {
      val (systemId, gantryId, laneId) = AlertLocation.getEventLocation(alert.path, alert.configType)
      Alert(systemId, gantryId, laneId, Right(alert))
    })
  }

  /**
   * Create the Alerts from  SystemAlerts
   * @return
   */
  private def createAlertsFromSystemAlerts(alerts: Seq[ZkAlert]): Seq[Alert] = {
    alerts.map(alert ⇒ {
      val (systemId, gantryId, laneId) = AlertLocation.getEventLocation(alert.path, alert.configType.toString)
      Alert(systemId, gantryId, laneId, Left(alert))
    })
  }

  /**
   * calculate the Reduction focator for the given alerts
   * @return
   */
  private def calculateReduction(filteredAlerts: Seq[Alert]): Float = {
    val current = filteredAlerts.filter(_.isCurrent)
    //when there are multiple alerts for one lane just one alert the others are hidden by the biggest reduction factor
    val groupedByLane = current.groupBy(alert ⇒ (alert.gantry, alert.lane))
    val sumAlerts = groupedByLane.values.map(alertList ⇒ {
      //get event with max reduction factor (but probably they are all the same)
      alertList.tail.foldLeft(alertList.head) {
        case (max, current) ⇒ if (current.getReductionFactor > max.getReductionFactor) {
          current
        } else {
          max
        }
      }
    })
    //summarize all reduction factors
    val reduction = sumAlerts.foldLeft(0F) { case (reductionFactor, current) ⇒ reductionFactor + current.getReductionFactor }
    //factor must be 0 to 1
    reduction match {
      case reduct if reduct <= 0F ⇒ 0F
      case reduct if reduct >= 1F ⇒ 1F
      case other                  ⇒ reduction
    }
  }

  /**
   * Class used to store location results and the alert
   */
  private case class Alert(system: String, gantry: String, lane: String, alert: Either[ZkAlert, ZkAlertHistory]) {
    def getReductionFactor: Float = {
      alert match {
        case Left(sys)   ⇒ sys.reductionFactor
        case Right(hist) ⇒ hist.reductionFactor
      }
    }
    def isCurrent: Boolean = {
      alert match {
        case Left(sys)   ⇒ true
        case Right(hist) ⇒ hist.endTime == endOfTime
      }
    }
  }
}