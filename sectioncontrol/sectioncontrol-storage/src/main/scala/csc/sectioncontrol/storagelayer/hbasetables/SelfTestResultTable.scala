/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.hbasetables

import csc.hbase.utils._
import csc.sectioncontrol.storage.HBaseTableDefinitions._
import csc.sectioncontrol.storage.SelfTestResult
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.conf.Configuration
import csc.sectioncontrol.storage.SelfTestResult

object SelfTestResultTable {
  type Reader = GenReader[(String, Long), SelfTestResult]
  type Writer = GenWriter[Long, SelfTestResult]

  def makeReader(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper, systemId: String): Reader = {
    val tableName = selfTestResultTableName
    HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, selfTestResultColumnFamily)
    val table = new HTable(hbaseConfig, tableName)
    tableKeeper.addTable(table)
    new JsonHBaseReader[(String, Long), selfTestResultRecordType] {
      val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(tableName)
      def hbaseReaderTable = table
      def hbaseReaderDataColumnFamily = selfTestResultColumnFamily
      def hbaseReaderDataColumnName = selfTestResultColumnName
      def makeReaderKey(keyValue: (String, Long)) =
        Bytes.toBytes("%s-%s-%s".format(systemId, keyValue._1, keyValue._2.toString))
    }
  }

  def makeWriter(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper, systemId: String, corridorId: String): Writer = {
    val tableName = selfTestResultTableName
    HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, selfTestResultColumnFamily)
    val table = new HTable(hbaseConfig, tableName)
    tableKeeper.addTable(table)
    new JsonHBaseWriter[Long, selfTestResultRecordType] {
      val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(tableName)
      def hbaseWriterTable = table
      def hbaseWriterDataColumnFamily = selfTestResultColumnFamily
      def hbaseWriterDataColumnName = selfTestResultColumnName
      def makeWriterKey(keyValue: Long) =
        Bytes.toBytes("%s-%s-%s".format(systemId, corridorId, keyValue.toString))
    }
  }

}