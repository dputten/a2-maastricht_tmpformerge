package csc.sectioncontrol.storagelayer.state

import csc.curator.utils.Curator
import csc.sectioncontrol.storage._
import csc.config.Path
import csc.sectioncontrol.storage.ZkFailureHistory
import csc.sectioncontrol.storage.ZkFailure

/**
 * Service to get the Failure.
 */
object FailureService {
  val endOfTime = Long.MaxValue

  def getFailuresWithPath(systemId: String, corridorId: String, endDayTime: Long, zkStore: Curator): Seq[(String, ZkFailure)] = {
    val paths = zkStore.getChildren(Paths.Failure.getCurrentPath(systemId, corridorId))
    paths.map(path ⇒ (path.toString(), getByPath(path, zkStore).get)).filter(_._2.timestamp < endDayTime)
  }

  def failuresExist(systemId: String, corridorId: String, zkStore: Curator): Boolean = {
    zkStore.getChildren(Paths.Failure.getCurrentPath(systemId, corridorId)).size > 0
  }

  def getNrOffFailures(systemId: String, corridorId: String, zkStore: Curator): Int = {
    zkStore.getChildren(Paths.Failure.getCurrentPath(systemId, corridorId)).size
  }

  def getByPath(path: String, zkStore: Curator): Option[ZkFailure] = {
    zkStore.get[ZkFailure](path)
  }

  def getFailures(systemId: String, corridorId: String, endDayTime: Long, zkStore: Curator): Seq[ZkFailure] = {
    val paths = zkStore.getChildren(Paths.Failure.getCurrentPath(systemId, corridorId))
    paths.flatMap(p ⇒ getByPath(p, zkStore)).filter(failure ⇒ failure.timestamp < endDayTime)
  }

  def getFailuresAfter(systemId: String, corridorId: String, endDayTime: Long, zkStore: Curator): Seq[ZkFailure] = {
    val paths = zkStore.getChildren(Paths.Failure.getCurrentPath(systemId, corridorId))
    paths.flatMap(p ⇒ getByPath(p, zkStore)).filter(failure ⇒ failure.timestamp > endDayTime)
  }

  def exists(systemId: String, corridorId: String, failure: ZkFailure, zkStore: Curator): Boolean = {
    val paths = zkStore.getChildren(Paths.Failure.getCurrentPath(systemId, corridorId))
    val events = paths.flatMap(p ⇒ getByPath(p, zkStore))
    events.exists(_.isSimilar(failure))
  }

  def create(systemId: String, corridorId: String, failure: ZkFailure, zkStore: Curator) = {
    zkStore.appendEvent(Paths.Failure.getCurrentPrefixPath(systemId, corridorId), failure)
  }

  def addToHistory(systemId: String, corridorId: String, failure: ZkFailureHistory, zkStore: Curator) = {
    val historyPath = Path(Paths.Failure.getHistoryPath(systemId, corridorId)) / failure.endTime.toString
    zkStore.appendEvent(historyPath, failure)
  }

  def signOff(systemId: String, corridorId: String, path: String, endTime: Long, zkStore: Curator) {
    //get failure
    if (!path.isEmpty) {
      val failureOpt = zkStore.get[ZkFailure](path)
      failureOpt.foreach(failure ⇒ {
        //delete failure
        zkStore.delete(path)
        //write to failure log
        val historyPath = Path(Paths.Failure.getHistoryPath(systemId, corridorId)) / endTime.toString
        zkStore.appendEvent(historyPath,
          new ZkFailureHistory(path = failure.path,
            failureType = failure.failureType,
            message = failure.message,
            startTime = failure.timestamp,
            endTime = endTime,
            configType = failure.configType.toString))
      })
    }
  }

  def getFailureHistory(systemId: String, corridorId: String, startDayTime: Long, endDayTime: Long, curator: Curator): List[ZkFailureHistory] = {
    //get list of all available alertHistory logs
    val failurePath = Path(Paths.Failure.getHistoryPath(systemId, corridorId))
    val allFailure: List[Path] = if (curator.exists(failurePath)) curator.getChildren(failurePath).toList else Nil

    //filter old logs
    val failureEndsWithinDay = allFailure.filter(p ⇒ {
      val nodeTime = ZkErrorHistory.getTimeFromNodeName(p)
      nodeTime > startDayTime
    })

    val failureHistory = failureEndsWithinDay.flatMap(path ⇒ curator.get[ZkFailureHistory](path))
    //filter alerts started after day end
    val history = failureHistory.filter(his ⇒ his.startTime < endDayTime)
    //add current alerts
    //val currentFailures = getFailures(systemId, corridorId, endDayTime, curator)
    val currentFailures = getFailuresAfter(systemId, corridorId, startDayTime, curator)
    history ++ currentFailures.map(fail ⇒ new ZkFailureHistory(path = fail.path,
      failureType = fail.failureType,
      message = fail.message,
      startTime = fail.timestamp,
      endTime = endOfTime,
      configType = fail.configType.toString))
  }

}

