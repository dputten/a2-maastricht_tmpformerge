/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.servicestatistics

import csc.sectioncontrol.messages.StatsDuration
import csc.hbase.utils._
import csc.sectioncontrol.storage.RedLightStatistics
import csc.sectioncontrol.storage.SpeedFixedStatistics
import csc.sectioncontrol.storage.SectionControlStatistics
import akka.pattern.ask
import akka.actor.{ Props, ActorSystem, ActorRef }
import akka.dispatch.Await
import akka.util.Timeout
import akka.util.duration._
import org.apache.hadoop.conf.Configuration
import csc.sectioncontrol.storagelayer.ServiceStatisticsRepositoryAccess

/**
 * Provides access to the ServiceStatisticsHBaseRepository.
 *
 */
trait ServiceStatisticsHBaseRepositoryAccess extends ServiceStatisticsRepositoryAccess {
  import ServiceStatisticsHBaseRepositoryActor._

  protected def hbaseConfig: Configuration
  protected def actorSystem: ActorSystem

  private lazy val repository: ActorRef = {
    actorSystem.actorOf(Props(new ServiceStatisticsHBaseRepositoryActor(hbaseConfig)))
  }

  implicit val timeout = Timeout(5 seconds)

  /**
   * Retrieve SectionControlStatistics
   * @param systemId The system to retrieve the statistics for
   * @param statsDuration The duration the statistics should belong to.
   * @return The requested statistics
   */
  def getSectionControlStatistics(systemId: String, statsDuration: StatsDuration): Seq[SectionControlStatistics] = {
    val future = repository ? GetServiceStatistics(systemId, statsDuration, SectionControlStatisticsType)
    val result = Await.result(future, timeout.duration).asInstanceOf[SectionControlStatisticsResult]
    result.statistics
  }

  /**
   * Retrieve RedLightStatistics
   * @param systemId The system to retrieve the statistics for
   * @param statsDuration The duration the statistics should belong to.
   * @return The requested statistics
   */
  def getRedLightStatistics(systemId: String, statsDuration: StatsDuration): Seq[RedLightStatistics] = {
    val future = repository ? GetServiceStatistics(systemId, statsDuration, RedLightStatisticsType)
    val result = Await.result(future, timeout.duration).asInstanceOf[RedLightStatisticsResult]
    result.statistics
  }

  /**
   * Retrieve SpeedFixedStatistics
   * @param systemId The system to retrieve the statistics for
   * @param statsDuration The duration the statistics should belong to.
   * @return The requested statistics
   */
  def getSpeedFixedStatistics(systemId: String, statsDuration: StatsDuration): Seq[SpeedFixedStatistics] = {
    val future = repository ? GetServiceStatistics(systemId, statsDuration, SpeedFixedStatisticsType)
    val result = Await.result(future, timeout.duration).asInstanceOf[SpeedFixedStatisticsResult]
    result.statistics
  }

  /**
   * Save a SectionControlStatistics for systemId.
   * @param systemId The system the statistics should be saved for.
   * @param statistics The statistics to be saved.
   */
  def save(systemId: String, statistics: SectionControlStatistics): Unit = {
    repository ! SaveServiceStatistics(systemId, statistics)
  }

  /**
   * Save a RedLightStatistics for systemId.
   * @param systemId The system the statistics should be saved for.
   * @param statistics The statistics to be saved.
   */
  def save(systemId: String, statistics: RedLightStatistics): Unit = {
    repository ! SaveServiceStatistics(systemId, statistics)
  }

  /**
   * Save a SpeedFixedStatistics for systemId.
   * @param systemId The system the statistics should be saved for.
   * @param statistics The statistics to be saved.
   */
  def save(systemId: String, statistics: SpeedFixedStatistics): Unit = {
    repository ! SaveServiceStatistics(systemId, statistics)
  }

}
