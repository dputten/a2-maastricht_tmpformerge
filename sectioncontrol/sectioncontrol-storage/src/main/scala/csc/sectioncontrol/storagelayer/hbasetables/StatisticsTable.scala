/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.hbasetables

import csc.sectioncontrol.storage._
import csc.hbase.utils._
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.conf.Configuration
import csc.sectioncontrol.storage.RedLightStatistics
import csc.sectioncontrol.storage.SectionControlStatistics

object StatisticsTable {
  trait ServiceStatisticsKey {
    def toKeyString: String
  }

  type TcsReader = GenReader[ServiceStatisticsKey, SectionControlStatistics]
  type RedLightReader = GenReader[ServiceStatisticsKey, RedLightStatistics]
  type SpeedReader = GenReader[ServiceStatisticsKey, SpeedFixedStatistics]

  type TcsWriter = GenWriter[ServiceStatisticsKey, SectionControlStatistics]
  type RedLightWriter = GenWriter[ServiceStatisticsKey, RedLightStatistics]
  type SpeedWriter = GenWriter[ServiceStatisticsKey, SpeedFixedStatistics]

  def createTcsReader(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper): TcsReader =
    createServiceStatisticsReader[HBaseTableDefinitions.sectionControlStatisticsRecordType](
      hbaseConfig = hbaseConfig,
      tableKeeper = tableKeeper,
      tableName = HBaseTableDefinitions.serviceStatisticsTableName,
      columnFamilyName = HBaseTableDefinitions.serviceStatisticsColumnFamily,
      columnName = HBaseTableDefinitions.sectionControlStatisticsColumnName)

  def createRedLightReader(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper): RedLightReader =
    createServiceStatisticsReader[HBaseTableDefinitions.redLightStatisticsRecordType](
      hbaseConfig = hbaseConfig,
      tableKeeper = tableKeeper,
      tableName = HBaseTableDefinitions.serviceStatisticsTableName,
      columnFamilyName = HBaseTableDefinitions.serviceStatisticsColumnFamily,
      columnName = HBaseTableDefinitions.redLightStatisticsColumnName)

  def createSpeedReader(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper): SpeedReader =
    createServiceStatisticsReader[HBaseTableDefinitions.speedFixedStatisticsRecordType](
      hbaseConfig = hbaseConfig,
      tableKeeper = tableKeeper,
      tableName = HBaseTableDefinitions.serviceStatisticsTableName,
      columnFamilyName = HBaseTableDefinitions.serviceStatisticsColumnFamily,
      columnName = HBaseTableDefinitions.speedFixedStatisticsColumnName)

  private def createServiceStatisticsReader[T <: ServiceStatistics](hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper, tableName: String, columnFamilyName: String, columnName: String): GenReader[ServiceStatisticsKey, T] = {
    HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, columnFamilyName)
    val table = new HTable(hbaseConfig, tableName)
    tableKeeper.addTable(table)
    new JsonHBaseReader[ServiceStatisticsKey, T] {
      val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(tableName)
      def hbaseReaderTable = table
      def hbaseReaderDataColumnFamily = columnFamilyName
      def hbaseReaderDataColumnName = columnName
      def makeReaderKey(keyValue: ServiceStatisticsKey) = Bytes.toBytes(keyValue.toKeyString)
    }
  }

  // === methods for constructing the different writers
  def createTcsWriter(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper): TcsWriter =
    createServiceStatisticsWriter[HBaseTableDefinitions.sectionControlStatisticsRecordType](
      hbaseConfig = hbaseConfig,
      tableKeeper = tableKeeper,
      tableName = HBaseTableDefinitions.serviceStatisticsTableName,
      columnFamilyName = HBaseTableDefinitions.serviceStatisticsColumnFamily,
      columnName = HBaseTableDefinitions.sectionControlStatisticsColumnName)

  def createRedLightWriter(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper): RedLightWriter =
    createServiceStatisticsWriter[HBaseTableDefinitions.redLightStatisticsRecordType](
      hbaseConfig = hbaseConfig,
      tableKeeper = tableKeeper,
      tableName = HBaseTableDefinitions.serviceStatisticsTableName,
      columnFamilyName = HBaseTableDefinitions.serviceStatisticsColumnFamily,
      columnName = HBaseTableDefinitions.redLightStatisticsColumnName)

  def createSpeedWriter(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper): SpeedWriter =
    createServiceStatisticsWriter[HBaseTableDefinitions.speedFixedStatisticsRecordType](
      hbaseConfig = hbaseConfig,
      tableKeeper = tableKeeper,
      tableName = HBaseTableDefinitions.serviceStatisticsTableName,
      columnFamilyName = HBaseTableDefinitions.serviceStatisticsColumnFamily,
      columnName = HBaseTableDefinitions.speedFixedStatisticsColumnName)

  private def createServiceStatisticsWriter[T <: ServiceStatistics](hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper, tableName: String, columnFamilyName: String, columnName: String): GenWriter[ServiceStatisticsKey, T] = {
    HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, columnFamilyName)
    val table = new HTable(hbaseConfig, tableName)
    tableKeeper.addTable(table)
    new JsonHBaseWriter[ServiceStatisticsKey, T] {
      val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(tableName)
      def hbaseWriterTable = table
      def hbaseWriterDataColumnFamily = columnFamilyName
      def hbaseWriterDataColumnName = columnName

      def makeWriterKey(keyValue: ServiceStatisticsKey) = Bytes.toBytes(keyValue.toKeyString)
    }
  }

}