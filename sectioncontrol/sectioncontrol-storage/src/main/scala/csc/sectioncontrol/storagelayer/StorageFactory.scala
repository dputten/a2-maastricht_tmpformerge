/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer

import org.apache.curator.RetryPolicy
import org.apache.curator.framework.{ CuratorFrameworkFactory, CuratorFramework }

/**
 * Factory to create only one instance of the Zookeeper connection
 */
object StorageFactory {
  private var curator: Option[CuratorFramework] = None

  /**
   * Create Zookeeper curator when it wasn't done already
   * @param zkServers the zookeeper quorum
   * @param retryPolicy the RetryPolicy
   * @return the new or existing curator
   */
  def newClient(zkServers: String, retryPolicy: RetryPolicy): Option[CuratorFramework] = {
    curator match {
      case Some(c) ⇒ curator
      case _ ⇒
        curator = Some(CuratorFrameworkFactory.newClient(zkServers, retryPolicy))
        curator
    }
  }
}