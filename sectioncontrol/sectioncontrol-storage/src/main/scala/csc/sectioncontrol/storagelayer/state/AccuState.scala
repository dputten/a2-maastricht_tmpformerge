/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.state

import csc.sectioncontrol.storage.ZkState

class AccuState() {
  private var nrFailures = 0
  private var nrMaintenance = 0
  private var nrDegraded = 0
  private var nrEnforce = 0
  private var nrOffline = 0
  private var nrStandBy = 0

  def getState(): String = {
    if (nrFailures > 0) {
      ZkState.failure
    } else if (nrMaintenance > 0) {
      ZkState.Maintenance
    } else if (nrDegraded > 0) {
      ZkState.enforceDegraded
    } else if (nrEnforce > 0) {
      ZkState.enforceOn
    } else if (nrOffline > 0) {
      ZkState.enforceOff
    } else if (nrStandBy > 0) {
      ZkState.standBy
    } else {
      ZkState.off
    }
  }
  def addStateChange(oldState: String, newState: String) {
    oldState match {
      case ZkState.failure         ⇒ nrFailures -= 1
      case ZkState.Maintenance     ⇒ nrMaintenance -= 1
      case ZkState.enforceDegraded ⇒ nrDegraded -= 1
      case ZkState.enforceOn       ⇒ nrEnforce -= 1
      case ZkState.enforceOff      ⇒ nrOffline -= 1
      case ZkState.standBy         ⇒ nrStandBy -= 1
      case _                       ⇒ //nop
    }
    newState match {
      case ZkState.failure         ⇒ nrFailures += 1
      case ZkState.Maintenance     ⇒ nrMaintenance += 1
      case ZkState.enforceDegraded ⇒ nrDegraded += 1
      case ZkState.enforceOn       ⇒ nrEnforce += 1
      case ZkState.enforceOff      ⇒ nrOffline += 1
      case ZkState.standBy         ⇒ nrStandBy += 1
      case _                       ⇒ //nop
    }
  }

}
