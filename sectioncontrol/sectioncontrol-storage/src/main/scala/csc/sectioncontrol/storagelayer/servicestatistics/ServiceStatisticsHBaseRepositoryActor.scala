/*
 * Copyright (C) 2013 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.servicestatistics

import akka.actor.{ ActorLogging, Actor }
import akka.event.LoggingReceive
import csc.sectioncontrol.messages.StatsDuration
import csc.sectioncontrol.storage._
import csc.hbase.utils._
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.conf.Configuration
import csc.sectioncontrol.storage.SpeedFixedStatistics
import csc.sectioncontrol.storage.RedLightStatistics
import csc.sectioncontrol.storage.SectionControlStatistics
import java.util.Date
import csc.sectioncontrol.storagelayer.hbasetables.StatisticsTable
import csc.sectioncontrol.storagelayer.hbasetables.StatisticsTable.ServiceStatisticsKey

/**
 * This actor defines an HBase version for the ServiceStatistics repository.
 */
class ServiceStatisticsHBaseRepositoryActor(hbaseConfig: Configuration) extends Actor with ActorLogging with HBaseTableKeeper {
  import ServiceStatisticsHBaseRepositoryActor._

  // Store created tables so they can be closed when actor is stopped
  private var createdTables: Map[String, HTable] = Map()

  // lazy val's for all readers and writers
  lazy val sectionControlStatisticsReader = StatisticsTable.createTcsReader(hbaseConfig, this)
  lazy val redLightStatisticsReader = StatisticsTable.createRedLightReader(hbaseConfig, this)
  lazy val speedFixedStatisticsReader = StatisticsTable.createSpeedReader(hbaseConfig, this)

  lazy val sectionControlStatisticsWriter = StatisticsTable.createTcsWriter(hbaseConfig, this)
  lazy val redLightStatisticsWriter = StatisticsTable.createRedLightWriter(hbaseConfig, this)
  lazy val speedFixedStatisticsWriter = StatisticsTable.createSpeedWriter(hbaseConfig, this)

  def receive = LoggingReceive {
    // Return SectionControlStatistics for given systemId and statsDuration
    case GetServiceStatistics(systemId, statsDuration, SectionControlStatisticsType) ⇒
      val foundRecords = readRows[HBaseTableDefinitions.sectionControlStatisticsRecordType](
        reader = sectionControlStatisticsReader,
        systemId = systemId,
        duration = statsDuration)
      sender ! SectionControlStatisticsResult(systemId = systemId, duration = statsDuration, statistics = foundRecords)
    case GetServiceStatisticsPeriod(systemId, from, to, SectionControlStatisticsType) ⇒
      val foundRecords = readRows[HBaseTableDefinitions.sectionControlStatisticsRecordType](
        reader = sectionControlStatisticsReader,
        systemId = systemId,
        from = from,
        to = to)
      sender ! SectionControlStatisticsResultPeriod(systemId = systemId, from = from, to = to, statistics = foundRecords)
    // Return RedLightStatistics for given systemId and statsDuration
    case GetServiceStatistics(systemId, statsDuration, RedLightStatisticsType) ⇒
      val foundRecords = readRows[HBaseTableDefinitions.redLightStatisticsRecordType](
        reader = redLightStatisticsReader,
        systemId = systemId,
        duration = statsDuration)
      sender ! RedLightStatisticsResult(systemId = systemId, duration = statsDuration, statistics = foundRecords)
    case GetServiceStatisticsPeriod(systemId, from, to, RedLightStatisticsType) ⇒
      val foundRecords = readRows[HBaseTableDefinitions.redLightStatisticsRecordType](
        reader = redLightStatisticsReader,
        systemId = systemId,
        from = from,
        to = to)
      sender ! RedLightStatisticsResultPeriod(systemId = systemId, from = from, to = to, statistics = foundRecords)

    // Return SpeedFixedStatistics for given systemId and statsDuration
    case GetServiceStatistics(systemId, statsDuration, SpeedFixedStatisticsType) ⇒
      val foundRecords = readRows[HBaseTableDefinitions.speedFixedStatisticsRecordType](
        reader = speedFixedStatisticsReader,
        systemId = systemId,
        duration = statsDuration)
      sender ! SpeedFixedStatisticsResult(systemId = systemId, duration = statsDuration, statistics = foundRecords)
    case GetServiceStatisticsPeriod(systemId, from, to, SpeedFixedStatisticsType) ⇒
      val foundRecords = readRows[HBaseTableDefinitions.speedFixedStatisticsRecordType](
        reader = speedFixedStatisticsReader,
        systemId = systemId,
        from = from,
        to = to)
      sender ! SpeedFixedStatisticsResultPeriod(systemId = systemId, from = from, to = to, statistics = foundRecords)

    // Save statistics for systemId. (statsDuration taken from statistics)
    case SaveServiceStatistics(systemId, statistics) ⇒ statistics match {
      case scs: SectionControlStatistics ⇒
        writeRow[HBaseTableDefinitions.sectionControlStatisticsRecordType](
          writer = sectionControlStatisticsWriter,
          systemId = systemId,
          period = scs.period,
          statistics = scs,
          someCorridorId = Some(scs.config.zkCorridorId))

      case rls: RedLightStatistics ⇒
        writeRow[HBaseTableDefinitions.redLightStatisticsRecordType](
          writer = redLightStatisticsWriter,
          systemId = systemId,
          period = rls.period,
          statistics = rls)

      case sfs: SpeedFixedStatistics ⇒
        writeRow[HBaseTableDefinitions.speedFixedStatisticsRecordType](
          writer = speedFixedStatisticsWriter,
          systemId = systemId,
          period = sfs.period,
          statistics = sfs)
    }
  }

  override def postStop() {
    closeTables()
  }

  def readRows[T <: ServiceStatistics](reader: GenReader[ServiceStatisticsKey, T], systemId: String, duration: StatsDuration)(implicit m: Manifest[T]): Seq[T] = try {
    val fromKeyValue = fromKey(systemId = systemId, duration = duration)
    val toKeyValue = toKey(systemId = systemId, duration = duration)
    reader.readRows(fromKeyValue, toKeyValue)
  } catch {
    case e: Exception ⇒
      log.error("Error reading statistics from HBase! [%s]".format(e.getMessage))
      Seq()
  }
  def readRows[T <: ServiceStatistics](reader: GenReader[ServiceStatisticsKey, T], systemId: String, from: Long, to: Long)(implicit m: Manifest[T]): Seq[T] = try {
    val fromKeyValue = ServiceStatisticsKeyFromTo(systemId = systemId, period = from)
    val toKeyValue = ServiceStatisticsKeyFromTo(systemId = systemId, period = to)
    reader.readRows(fromKeyValue, toKeyValue)
  } catch {
    case e: Exception ⇒
      log.error("Error reading statistics from HBase! [%s]".format(e.getMessage))
      Seq()
  }

  def writeRow[T <: ServiceStatistics](writer: GenWriter[ServiceStatisticsKey, T], systemId: String, period: StatisticsPeriod, statistics: T,
                                       someCorridorId: Option[String] = None): Unit = try {
    writer.writeRow(
      key = ServiceStatisticsKeyDuration(systemId = systemId, duration = period.stats, periodEnd = period.end, someCorridorId = someCorridorId),
      value = statistics)
  } catch {
    case e: Exception ⇒
      log.error("Error writing statistics to HBase! [%s]".format(e.getMessage))
  }

  /**
   * This class contains all the data needed to create the rowKey for any of the service statistics tables.
   * The toKeyString method returns the value of the corresponding key.
   * @param systemId The id of a system.
   * @param duration A StatsDuration object (representing a day)
   * @param periodEnd A timestamp
   */
  case class ServiceStatisticsKeyDuration(systemId: String, duration: StatsDuration, periodEnd: Long, someCorridorId: Option[String] = None) extends ServiceStatisticsKey {
    def toKeyString: String = {
      someCorridorId match {
        case None         ⇒ "%s-%s-%015d".format(systemId, duration.key, periodEnd)
        case Some(corrId) ⇒ "%s-%s-%015d-%s".format(systemId, duration.key, periodEnd, corrId)
      }

    }
  }

  case class ServiceStatisticsKeyFromTo(systemId: String, period: Long) extends ServiceStatisticsKey {
    val day = StatsDuration.format format (new Date(period))
    def toKeyString: String = "%s-%s-%015d".format(systemId, day, period)
  }

  // Returns the from value for a scan on (systemId, duration)
  private def fromKey(systemId: String, duration: StatsDuration): ServiceStatisticsKey =
    ServiceStatisticsKeyDuration(systemId = systemId, duration = duration, periodEnd = 0)

  // Returns the to value for a scan on (systemId, duration)
  private def toKey(systemId: String, duration: StatsDuration): ServiceStatisticsKey =
    ServiceStatisticsKeyDuration(systemId = systemId, duration = duration.tomorrow, periodEnd = 0)

}

object ServiceStatisticsHBaseRepositoryActor {
  sealed trait StatisticsType
  case object SectionControlStatisticsType extends StatisticsType
  case object RedLightStatisticsType extends StatisticsType
  case object SpeedFixedStatisticsType extends StatisticsType

  // Message for retrieving ServiceStatistics data from HBase
  case class GetServiceStatistics(systemId: String, duration: StatsDuration, statisticsType: StatisticsType)
  case class GetServiceStatisticsPeriod(systemId: String, from: Long, to: Long, statisticsType: StatisticsType)
  // Return messages for GetServiceStatistics
  case class SectionControlStatisticsResult(systemId: String, duration: StatsDuration, statistics: Seq[SectionControlStatistics])
  case class RedLightStatisticsResult(systemId: String, duration: StatsDuration, statistics: Seq[RedLightStatistics])
  case class SpeedFixedStatisticsResult(systemId: String, duration: StatsDuration, statistics: Seq[SpeedFixedStatistics])

  case class SectionControlStatisticsResultPeriod(systemId: String, from: Long, to: Long, statistics: Seq[SectionControlStatistics])
  case class RedLightStatisticsResultPeriod(systemId: String, from: Long, to: Long, statistics: Seq[RedLightStatistics])
  case class SpeedFixedStatisticsResultPeriod(systemId: String, from: Long, to: Long, statistics: Seq[SpeedFixedStatistics])

  // Messages for storing ServiceStatistics data in HBase (no return message)
  case class SaveServiceStatistics(systemId: String, statistics: ServiceStatistics)
}
