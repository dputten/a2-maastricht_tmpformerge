package csc.sectioncontrol.storagelayer.offlineevents

import csc.sectioncontrol.storage.Paths
import csc.curator.utils.Curator
import csc.config.Path
import csc.sectioncontrol.messages.SystemEvent

object OfflineEventsService {
  def offlineEventsExist(systemId: String, corridorId: String, zkStore: Curator): Boolean = {
    zkStore.getChildren(Paths.Systems.getSystemOfflineEventsPath(systemId, corridorId)).size > 0
  }

  def exists(systemId: String, corridorId: String, nodeName: String, zkStore: Curator): Boolean = {
    val path = Path(Paths.Systems.getSystemOfflineEventsPath(systemId, corridorId)) / nodeName
    zkStore.exists(path)
  }

  def create(systemId: String, corridorId: String, nodeName: String, zkStore: Curator, systemEvent: SystemEvent) = {
    val path = Path(Paths.Systems.getSystemOfflineEventsPath(systemId, corridorId)) / nodeName
    zkStore.put(path, systemEvent)
  }

  def delete(systemId: String, corridorId: String, nodeName: String, zkStore: Curator) = {
    val path = Path(Paths.Systems.getSystemOfflineEventsPath(systemId, corridorId)) / nodeName
    zkStore.delete(path)
  }
}
