/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.backup

import csc.config.Path
import xml.{ PCData, Elem }
import csc.curator.utils.{ CuratorTools, Curator }

/**
 * exports configuration
 */
object Exporter {

  /**
   *
   * @param path
   * @param name
   * @param data
   */
  case class ZNode(path: Path, name: String, data: Option[String]) {
    def hasData = data.isDefined
    def parent = path.parent
  }

  /**
   * Used to export all nodes and its data
   * @param path to export (including all its children and data)
   * @return xml with the complete export of the given path
   */
  def exportConfig(curator: Curator, path: Path, version: Option[String] = None): Elem = {
    val xml = getTree(curator, path).foldLeft(List[Elem]()) {
      (list, zNode) ⇒
        <znode>
          <path>{ zNode.path.toString() }</path>
          <data>{ PCData(zNode.data.getOrElse("")) }</data>
        </znode> :: list
    }

    <export version={ version.getOrElse("Unknown") }>
      <export-path>{ path.toString() }</export-path>
      <znodes>
        { xml }
      </znodes>
    </export>
  }

  /**
   * retrieves a zookeeper tree structure of a given begin path
   * @param path
   * @return
   */
  def getTree(curator: Curator, path: Path): List[ZNode] = {
    def getAllPaths(path: Path): List[Path] = {
      var paths = List[Path]()
      def loop(path: Path) {
        curator.getChildren(path).map { p ⇒
          paths ::= p
          loop(p)
        }
      }
      loop(path)
      paths.sortBy(_.toString)
    }

    def createZNode(path: Path): ZNode = {
      val data = curator match {
        case cur: CuratorTools ⇒ {
          val byteOpt = cur.zkGetRawData(path.toString(), false)
          byteOpt.flatMap(bytes ⇒ {
            if (bytes.length == 0)
              None
            else {
              val dataAsString = new String(bytes)
              if (dataAsString.trim.isEmpty) {
                None
              } else {
                Some(dataAsString)
              }
            }
          })
        }
        case other ⇒ None
      }
      ZNode(path, path.nodes.last.name, data)
    }
    getAllPaths(path).map(createZNode(_))
  }
}
