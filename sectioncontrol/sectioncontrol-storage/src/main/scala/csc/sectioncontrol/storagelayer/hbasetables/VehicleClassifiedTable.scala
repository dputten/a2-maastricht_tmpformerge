/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.hbasetables

import csc.hbase.utils._
import csc.sectioncontrol.storage.HBaseTableDefinitions._
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.conf.Configuration
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.messages.{ VehicleSpeedRecord, VehicleCode, ProcessingIndicator, VehicleCategory }

object VehicleClassifiedTable {

  type Reader = GenReader[Long, vehicleClassifiedRecordType]
  type Writer = GenWriter[vehicleClassifiedRecordType, vehicleClassifiedRecordType]

  def makeReader(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper, systemId: String): Reader = {
    val tableName = vehicleClassifiedTableName
    HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, vehicleClassifiedColumnFamily)
    val table = new HTable(hbaseConfig, tableName)
    tableKeeper.addTable(table)

    new JsonHBaseReader[Long, vehicleClassifiedRecordType] {
      val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(tableName)
      def hbaseReaderTable = table
      def hbaseReaderDataColumnFamily = vehicleClassifiedColumnFamily
      def hbaseReaderDataColumnName = vehicleClassifiedColumnName

      //e.g. HH000001-1496274612651
      def makeReaderKey(keyValue: Long) = Bytes.toBytes("%s-%s".format(systemId, keyValue))
      override def jsonReaderFormats = SerializerFormats.formats
    }
  }

  def makeWriter(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper): Writer = {
    val tableName = vehicleClassifiedTableName
    HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, vehicleClassifiedColumnFamily)
    val table = new HTable(hbaseConfig, tableName)
    tableKeeper.addTable(table)
    new JsonHBaseWriter[vehicleClassifiedRecordType, vehicleClassifiedRecordType] {
      val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(tableName)
      def hbaseWriterTable = table
      def hbaseWriterDataColumnFamily = vehicleClassifiedColumnFamily
      def hbaseWriterDataColumnName = vehicleClassifiedColumnName

      //e.g. HH000001-1496274612651-2002-1
      def makeWriterKey(keyValue: vehicleClassifiedRecordType) = Bytes.toBytes(keyValue.id)
      override def jsonWriterFormats = SerializerFormats.formats
    }
  }
  private val idGenerator = new GenerateId {}
  def makeKey(speed: VehicleSpeedRecord): String = "%s-%s-%s-%s".format(speed.entry.lane.system, speed.time, speed.corridorId, idGenerator.nextId)

}