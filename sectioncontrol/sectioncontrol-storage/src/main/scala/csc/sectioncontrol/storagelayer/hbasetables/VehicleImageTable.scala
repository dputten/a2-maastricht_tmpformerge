/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.hbasetables

import csc.hbase.utils._
import csc.sectioncontrol.messages.VehicleImageType
import csc.sectioncontrol.storage.HBaseTableDefinitions._
import org.apache.hadoop.hbase.client.HTable
import csc.sectioncontrol.storage.HBaseTableDefinitions
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.conf.Configuration
import csc.sectioncontrol.storage.KeyWithTimeAndId

object VehicleImageTable {
  //key <systemId>-<gantryId>-<time>-<laneId>
  //key1 = <systemId>-<gantryId>-
  //key2 = <time>
  //key3 = -<laneId>
  type Reader = GenReader[(String, Long, String), vehicleImageRecordType]
  //key1 <systemId>
  //key2 <gantryId>
  //key3 <time>
  //key4 <laneId>
  type Writer = GenWriter[(String, String, Long, String), vehicleImageRecordType]

  def makeReader(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper, imageType: VehicleImageType.Value): Reader = {
    val tableName = vehicleImageTableName
    HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, vehicleImageColumnFamily)
    val table = new HTable(hbaseConfig, tableName)
    tableKeeper.addTable(table)

    new HBaseReader[(String, Long, String), vehicleImageRecordType] {
      val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(tableName)
      def hbaseReaderTable = table
      def hbaseReaderDataColumnFamily = vehicleImageColumnFamily
      def hbaseReaderDataColumnName = HBaseTableDefinitions.toColumnName(imageType).getOrElse("")
      def makeReaderKey(keyValue: (String, Long, String)) = Bytes.toBytes(keyValue._1 + keyValue._2 + keyValue._3)
      def deserialize(bytes: Array[Byte])(implicit mt: Manifest[vehicleImageRecordType]) = bytes
    }
  }

  def makeWriter(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper, imageType: VehicleImageType.Value): Writer = {
    val tableName = vehicleImageTableName
    HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, vehicleImageColumnFamily)
    val table = new HTable(hbaseConfig, tableName)
    tableKeeper.addTable(table)
    new HBaseWriter[(String, String, Long, String), vehicleImageRecordType] {
      val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(tableName)
      def hbaseWriterTable = table
      def hbaseWriterDataColumnFamily = vehicleImageColumnFamily
      def hbaseWriterDataColumnName = HBaseTableDefinitions.toColumnName(imageType).getOrElse("")
      def makeWriterKey(keyValues: (String, String, Long, String)) =
        Bytes.toBytes("%s-%s-%d-%s".format(keyValues._1, keyValues._2, keyValues._3, keyValues._4))

      def serialize(value: vehicleImageRecordType) = value
    }
  }

}