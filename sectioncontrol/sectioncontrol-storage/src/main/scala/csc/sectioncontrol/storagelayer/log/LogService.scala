package csc.sectioncontrol.storagelayer.log

import csc.sectioncontrol.storage.Paths
import csc.sectioncontrol.messages.SystemEvent
import csc.curator.utils.Curator

object LogService {
  def create(systemId: String, systemEvent: SystemEvent, zkStore: Curator) = {
    zkStore.appendEvent(Paths.Systems.getLogPath(systemId), systemEvent)
  }
}
