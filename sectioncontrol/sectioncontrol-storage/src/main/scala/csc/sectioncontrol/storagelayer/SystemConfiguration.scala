/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.storagelayer

import collection.SortedSet
import csc.curator.utils.Curator
import csc.config.Path
import csc.sectioncontrol.messages.VehicleCode
import csc.sectioncontrol.storage._
import csc.sectioncontrol.storage.ZkSchedule
import csc.sectioncontrol.storage.ZkCorridor

/**
 * Factory object to build up the SystemConfigurationData
 * from the configuration data stored in zookeeper.
 * Copied from LoadSystemConfiguration to be reused
 */
class SystemConfiguration(curator: Curator) {

  /**
   * retrieve the service that belongs to a corridor
   * @param corridorId that is stored
   * @param systemId id of system
   * @return optional Service
   */
  def getCorridor(corridorId: String, systemId: String): Option[CorridorConfig] = {
    val corridorOpt = curator.get[ZkCorridor](Paths.Corridors.getConfigPath(systemId, corridorId))
    if (corridorOpt == None) {
      return None
    }
    val corridor = corridorOpt.get
    val rootPathToServices = Path(Paths.Corridors.getServicesPath(systemId, corridorId))
    //e.g. systems/../corridors/../services/RedLight
    val pathToService = (rootPathToServices / corridor.serviceType.toString).toString()

    (corridor.serviceType match {
      case ServiceType.RedLight       ⇒ curator.get[RedLightConfig](pathToService)
      case ServiceType.SpeedFixed     ⇒ curator.get[SpeedFixedConfig](pathToService)
      case ServiceType.SpeedMobile    ⇒ curator.get[SpeedMobileConfig](pathToService)
      case ServiceType.SectionControl ⇒ curator.get[SectionControlConfig](pathToService)
      case ServiceType.ANPR           ⇒ curator.get[ANPRConfig](pathToService)
      case ServiceType.TargetGroup    ⇒ curator.get[TargetGroupConfig](pathToService)
      case _                          ⇒ None
    }).map(service ⇒ {
      new CorridorConfig(id = corridor.id,
        name = corridor.name,
        roadType = corridor.roadType,
        serviceType = corridor.serviceType,
        caseFileType = corridor.caseFileType,
        isInsideUrbanArea = corridor.isInsideUrbanArea,
        dynamaxMqId = corridor.dynamaxMqId,
        approvedSpeeds = corridor.approvedSpeeds,
        pshtm = corridor.pshtm,
        info = corridor.info,
        startSectionId = corridor.startSectionId,
        endSectionId = corridor.endSectionId,
        allSectionIds = corridor.allSectionIds,
        serviceConfig = service)
    })
  }

  //  def updateActiveCorridorService(corridorId: String, systemId: String, isActive: Boolean) {
  //    val corridorOpt = curator.get[ZkCorridor](Paths.Corridors.getConfigPath(systemId, corridorId))
  //    corridorOpt.foreach(corridor ⇒ {
  //      val rootPathToServices = Path(Paths.Corridors.getServicesPath(systemId, corridorId))
  //      //e.g. systems/../corridors/../services/RedLight
  //      val pathToService = (rootPathToServices / corridor.serviceType.toString).toString()
  //      corridor.serviceType match {
  //        case ZkServiceType.RedLight ⇒
  //          val versionedData = curator.getVersioned[RedLightConfig](pathToService)
  //          versionedData.foreach(data ⇒ curator.set(pathToService, data.data.copy(isActive = isActive), data.version))
  //        case ZkServiceType.SpeedFixed ⇒
  //          val versionedData = curator.getVersioned[SpeedFixedConfig](pathToService)
  //          versionedData.foreach(data ⇒ curator.set(pathToService, data.data.copy(isActive = isActive), data.version))
  //        case ZkServiceType.SpeedMobile ⇒
  //          val versionedData = curator.getVersioned[SpeedMobileConfig](pathToService)
  //          versionedData.foreach(data ⇒ curator.set(pathToService, data.data.copy(isActive = isActive), data.version))
  //        case ZkServiceType.SectionControl ⇒
  //          val versionedData = curator.getVersioned[SectionControlConfig](pathToService)
  //          versionedData.foreach(data ⇒ curator.set(pathToService, data.data.copy(isActive = isActive), data.version))
  //        case ZkServiceType.ANPR ⇒
  //          val versionedData = curator.getVersioned[ANPRConfig](pathToService)
  //          versionedData.foreach(data ⇒ curator.set(pathToService, data.data.copy(isActive = isActive), data.version))
  //        case ZkServiceType.TargetGroup ⇒
  //          val versionedData = curator.getVersioned[TargetGroupConfig](pathToService)
  //          versionedData.foreach(data ⇒ curator.set(pathToService, data.data.copy(isActive = isActive), data.version))
  //        case _ ⇒ None
  //      }
  //    })
  //  }

  def getCorridorType(systemId: String, serviceType: ServiceType.Value): Option[CorridorConfig] = {
    val all = getAllCorridors(systemId)
    all.find(_.serviceType == serviceType)
  }

  def getAllCorridors(systemId: String): Seq[CorridorConfig] = {
    val ids = getCorridorIds(systemId)
    ids.flatMap(corridorId ⇒ getCorridor(corridorId, systemId)).toSeq
  }

  def getCorridorIds(systemId: String): Seq[String] = {
    val corridorPaths = curator.getChildren(Paths.Corridors.getDefaultPath(systemId))
    corridorPaths.map { _.split('/').last }
  }

  def getCorridorSchedules(systemId: String, corridorId: String): List[ZkSchedule] = {
    val currentSchedulesPath = Paths.Corridors.getSchedulesPath(systemId, corridorId)
    val schedules = curator.get[List[ZkSchedule]](currentSchedulesPath)
    schedules.getOrElse(List())
  }

  def getVehicleMaxSpeeds(systemId: String): Map[VehicleCode.Value, Int] = {
    val vehicleMaxSpeeds = for {
      prefs ← curator.get[ZkPreference](Seq(Path(Paths.Preferences.getGlobalConfigPath), Path(Paths.Preferences.getConfigPath(systemId))), ZkPreference.default)
    } yield prefs.vehicleMaxSpeed.map(vms ⇒ vms.vehicleType -> vms.maxSpeed).toMap

    require(vehicleMaxSpeeds.isDefined, "Could not load vehicle max speeds for systemId " + systemId)
    vehicleMaxSpeeds.get
  }

  def getSpeedMargins(systemId: String): SortedSet[SpeedMargin] = {
    val speedMargins = for {
      prefs ← curator.get[ZkPreference](Seq(Path(Paths.Preferences.getGlobalConfigPath), Path(Paths.Preferences.getConfigPath(systemId))), ZkPreference.default)
    } yield prefs.marginSpeedLimits.map(msl ⇒ SpeedMargin(msl.speedLimit, msl.speedMargin))

    require(speedMargins.isDefined, "Could not load speed margins for systemId " + systemId)
    SortedSet(speedMargins.get: _*)
  }

  def getUser(userId: String): Option[ZkUser] = {
    val path = Paths.Users.getUserPath(userId)
    curator.get[ZkUser](path)
  }

}
