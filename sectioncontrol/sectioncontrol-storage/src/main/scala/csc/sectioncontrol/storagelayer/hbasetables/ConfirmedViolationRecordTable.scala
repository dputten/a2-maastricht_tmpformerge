package csc.sectioncontrol.storagelayer.hbasetables

import csc.hbase.utils._
import csc.sectioncontrol.messages.{ ViolationCandidateRecord, VehicleClassifiedRecord }
import csc.sectioncontrol.storage.HBaseTableDefinitions._
import csc.sectioncontrol.storage.KeyWithTimeAndId
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.util.Bytes

object ConfirmedViolationRecordTable {
  type Reader = GenReader[Long, ViolationCandidateRecord]

  type Writer = GenWriter[VehicleClassifiedRecord, ViolationCandidateRecord]

  def makeReader(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper, systemId: String): Reader = {
    val tableName = confirmedViolationTableName
    HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, confirmedViolationColumnFamily)
    val table = new HTable(hbaseConfig, tableName)
    tableKeeper.addTable(table)
    new JsonHBaseReader[Long, ViolationCandidateRecord] {
      val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(tableName)
      def hbaseReaderTable = table
      def hbaseReaderDataColumnFamily = confirmedViolationColumnFamily
      def hbaseReaderDataColumnName = confirmedViolationColumnName
      def makeReaderKey(keyValue: Long) = Bytes.toBytes("%s-%s".format(systemId, keyValue))
      override def jsonReaderFormats = SerializerFormats.formats
    }
  }

  def makeWriter(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper): Writer = {
    val tableName = confirmedViolationTableName
    HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, confirmedViolationColumnFamily)
    val table = new HTable(hbaseConfig, tableName)
    tableKeeper.addTable(table)

    new JsonHBaseWriter[VehicleClassifiedRecord, ViolationCandidateRecord] with GenerateId {
      val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(tableName)
      def hbaseWriterTable = table
      def hbaseWriterDataColumnFamily = confirmedViolationColumnFamily
      def hbaseWriterDataColumnName = confirmedViolationColumnName

      def makeWriterKey(keyValue: vehicleClassifiedRecordType) = Bytes.toBytes("%s-%s-%s-%s".format(keyValue.speedRecord.entry.lane.system, keyValue.time, keyValue.speedRecord.corridorId, nextId))
      override def jsonWriterFormats = SerializerFormats.formats
    }
  }

}