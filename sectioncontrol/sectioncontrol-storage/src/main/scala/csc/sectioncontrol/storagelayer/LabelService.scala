/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer

import csc.curator.utils.Curator
import csc.sectioncontrol.storage.Paths
import csc.config.Path

/**
 * Class representing labels
 *
 * @param nodePath The path of the node which has the label
 * @param nodeType The type of the Node
 * @param labelName The name of the label
 */
case class Label(nodePath: String, nodeType: String, labelName: String) {
  def getNodeName(): String = {
    Path(nodePath).nodes.last.name
  }
}

/**
 * Service to get labels.
 */
object LabelService {

  /**
   * Definitions of the label types supported at this moment
   */
  object LabelTypes {
    val system = "System"
    val gantry = "Gantry"
    val lane = "Lane"
    val corridor = "Corridor"
    val section = "Section"
  }

  /**
   * Get all existing labels
   * @param zkStore The Curator used to connect to Zookeeper
   * @return Seq of labels
   */
  def getAllLabels(zkStore: Curator): Seq[Label] = {
    zkStore.get[List[Label]](Paths.Labels.getDefaultPath).getOrElse(Seq())
  }

  /**
   * Get all labels for the specified nodetype
   * @param nodeType The node type
   * @param zkStore The Curator used to connect to Zookeeper
   * @return Seq of labels
   */
  def getLabelsByType(nodeType: String, zkStore: Curator): Seq[Label] = {
    getAllLabels(zkStore).filter(_.nodeType == nodeType)
  }

  /**
   * Get all labels for given node
   * @param nodePath The path of the node
   * @param zkStore The Curator used to connect to Zookeeper
   * @return Seq of labels
   */
  def getLabelsByNode(nodePath: String, zkStore: Curator): Seq[Label] = {
    getAllLabels(zkStore).filter(_.nodePath == nodePath)
  }

  /**
   * Get the labels for the specified labelName
   * @param label the labelName
   * @param zkStore The Curator used to connect to Zookeeper
   * @return Seq of labels
   */
  def getLabels(label: String, zkStore: Curator): Seq[Label] = {
    getAllLabels(zkStore).filter(_.labelName == label)
  }

  /**
   * get the label nodes for the specified label and node type
   * @param label The labelName
   * @param nodeType the node type
   * @param zkStore The Curator used to connect to Zookeeper
   * @return Seq of labels
   */
  def getLabelsByLabelAndType(label: String, nodeType: String, zkStore: Curator): Seq[Label] = {
    getAllLabels(zkStore).filter(l ⇒ l.nodeType == nodeType && l.labelName == label)
  }
}