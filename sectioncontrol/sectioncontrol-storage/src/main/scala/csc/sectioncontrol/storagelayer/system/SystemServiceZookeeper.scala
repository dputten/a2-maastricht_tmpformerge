package csc.sectioncontrol.storagelayer.system

import csc.curator.utils.{ Versioned, Curator }
import csc.sectioncontrol.storage.{ ZkSystem, Paths }
import csc.config.Path

trait SystemService {
  def get(systemId: String): Option[ZkSystem]
  def getVersioned(systemId: String): Option[Versioned[ZkSystem]]
  def update(systemId: String, mailServer: ZkSystem, version: Int)
  def delete(systemId: String)
  def exists(systemId: String)
}

class SystemServiceZookeeper(zkStore: Curator) extends SystemService {
  def get(systemId: String): Option[ZkSystem] = {
    zkStore.get[ZkSystem](Path(Paths.Systems.getConfigPath(systemId)))
  }

  def getVersioned(systemId: String): Option[Versioned[ZkSystem]] = {
    zkStore.getVersioned[ZkSystem](Paths.Systems.getConfigPath(systemId))
  }

  def update(systemId: String, mailServer: ZkSystem, version: Int) = {
    zkStore.set(Paths.Systems.getConfigPath(systemId), mailServer, version)
  }

  def delete(systemId: String) = {
    zkStore.deleteRecursive(Paths.Systems.getConfigPath(systemId))
  }

  def exists(systemId: String) = {
    zkStore.exists(Paths.Systems.getConfigPath(systemId))
  }
}

