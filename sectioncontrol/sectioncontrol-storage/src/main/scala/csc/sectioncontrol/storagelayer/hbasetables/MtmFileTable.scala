/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.hbasetables

import csc.hbase.utils._
import csc.sectioncontrol.storage.HBaseTableDefinitions._
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.conf.Configuration

object MtmFileTable {

  type Store = RWStore[String, mtmFileRecordType]

  def makeStore(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper): Store = {
    val tableName = mtmFileTableName
    HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, mtmFileColumnFamily)
    val table = new HTable(hbaseConfig, tableName)
    tableKeeper.addTable(table)
    new HBaseRWStore[String, mtmFileRecordType] {
      val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(tableName)

      def hbaseTable = table
      def hbaseDataColumnFamily = mtmFileColumnFamily
      def hbaseDataColumnName = mtmFileColumnName
      def makeKey(keyValue: String) = Bytes.toBytes(keyValue)
      def serialize(value: mtmFileRecordType) = value
      def deserialize(bytes: Array[Byte])(implicit mt: Manifest[mtmFileRecordType]) = bytes
    }
  }

}