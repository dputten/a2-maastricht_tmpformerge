/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.hbasetables

import csc.hbase.utils._
import csc.sectioncontrol.storage.HBaseTableDefinitions._
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.conf.Configuration
import csc.sectioncontrol.messages.{ Lane, VehicleMetadata, VehicleSpeedRecord }

object SpeedRecordTable {

  type Reader = GenReader[Long, VehicleSpeedRecord]
  type Writer = GenWriter[VehicleSpeedRecord, VehicleSpeedRecord]

  def makeReader(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper, systemId: String): Reader = {
    val tableName = speedRecordTableName
    HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, speedRecordColumnFamily)
    val table = new HTable(hbaseConfig, tableName)
    tableKeeper.addTable(table)
    new JsonHBaseReader[Long, speedRecordType] {
      val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(tableName)
      def hbaseReaderTable = table
      def hbaseReaderDataColumnFamily = speedRecordColumnFamily
      def hbaseReaderDataColumnName = speedRecordColumnName
      def makeReaderKey(keyValue: Long) = Bytes.toBytes("%s-%s".format(systemId, keyValue))
      override def jsonReaderFormats = SerializerFormats.formats
    }
  }

  def makeWriter(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper): Writer = {
    val tableName = speedRecordTableName
    HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, speedRecordColumnFamily)
    val table = new HTable(hbaseConfig, tableName)
    tableKeeper.addTable(table)
    new JsonHBaseWriter[VehicleSpeedRecord, VehicleSpeedRecord] {
      val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(tableName)
      def hbaseWriterTable = table

      def hbaseWriterDataColumnFamily = speedRecordColumnFamily

      def hbaseWriterDataColumnName = speedRecordColumnName

      def makeWriterKey(keyValue: speedRecordType) = Bytes.toBytes(makeKey(keyValue.entry.lane, keyValue.time, keyValue.corridorId))

      override def jsonWriterFormats = SerializerFormats.formats
    }
  }

  def makeKey(lane: Lane, time: Long, corridorId: Int): String = "%s-%s-%s-%s-%s".format(lane.system, time, lane.gantry, corridorId, lane.name)

}