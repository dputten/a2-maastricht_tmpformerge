package csc.sectioncontrol.storagelayer.state

import csc.curator.utils.{ Versioned, Curator }
import csc.sectioncontrol.storage.{ ZkState, Paths }
import csc.config.Path
import csc.sectioncontrol.storagelayer.corridor.{ CorridorService, CorridorIdMapping }

/**
 * Service to get the State.
 */
object StateService {

  def get(systemId: String, corridorId: String, zkStore: Curator): Option[ZkState] = {
    zkStore.get[ZkState](Path(Paths.State.getCurrentPath(systemId, corridorId)))
  }

  def getVersioned(systemId: String, corridorId: String, zkStore: Curator): Option[Versioned[ZkState]] = {
    zkStore.getVersioned[ZkState](Paths.State.getCurrentPath(systemId, corridorId))
  }

  def update(systemId: String, corridorId: String, state: ZkState, version: Int, zkStore: Curator) = {
    val originState = getVersioned(systemId, corridorId, zkStore)
    val originTime = originState match {
      case Some(vo) if (vo.version == version) ⇒ vo.data.timestamp
      case _                                   ⇒ 0L
    }
    val delayedState = state.timestamp <= originTime
    (delayedState, state.state) match {
      case (false, _) ⇒ {
        //normal operation new state is after the original state
        zkStore.set(Paths.State.getCurrentPath(systemId, corridorId), state, version)
        zkStore.appendEvent(Paths.State.getHistoryPrefixPath(systemId, corridorId), state)
      }
      case (true, ZkState.enforceDegraded) ⇒ {
        //delayed enforce degraded
        if (originState.map(_.data.state).getOrElse(state.state) == ZkState.enforceOn) {
          //update current state, only when last state is enforceOn
          val updated = originState.map(_.data.copy(state = state.state)).getOrElse(state.copy(timestamp = originTime))
          zkStore.set(Paths.State.getCurrentPath(systemId, corridorId), updated, version)
        }
        //update existing state changes
        updateHistoryEnforceOnState(systemId, corridorId, state, zkStore)
      }
      case (true, _) ⇒ {
        //another delayed state no extra functionality is needed here
        zkStore.set(Paths.State.getCurrentPath(systemId, corridorId), state, version)
        zkStore.appendEvent(Paths.State.getHistoryPrefixPath(systemId, corridorId), state)
      }
    }
  }

  def create(systemId: String, corridorId: String, state: ZkState, zkStore: Curator) = {
    zkStore.put(Paths.State.getCurrentPath(systemId, corridorId), state)
    zkStore.appendEvent(Paths.State.getHistoryPrefixPath(systemId, corridorId), state)
  }

  def createEmptyHistoryPath(systemId: String, corridorId: String, zkStore: Curator) = {
    if (!zkStore.exists(Paths.State.getHistoryPath(systemId, corridorId))) {
      zkStore.createEmptyPath(Paths.State.getHistoryPath(systemId, corridorId))
    }
  }

  def getStateLogHistory(systemId: String, corridorId: String, startTime: Long, endTime: Long,
                         curator: Curator, skipAddedStandBy: Boolean = false): Seq[ZkState] = {
    val path = Paths.State.getHistoryPath(systemId, corridorId)
    val allStates = if (curator.exists(path)) {
      val allPaths = curator.getChildren(path)
      if (skipAddedStandBy)
        allPaths.flatMap(path ⇒ curator.get[ZkState](path))
      else
        Seq(ZkState(ZkState.standBy, 0, "system", "Not started")) ++ allPaths.flatMap(path ⇒ curator.get[ZkState](path))

    } else {
      if (skipAddedStandBy)
        Nil
      else
        Seq(ZkState(ZkState.standBy, 0, "system", "Not started"))
    }

    //filter states
    /* The algorithm:
    * 1) sort by timestamp from new to old (reversed)
    * 2) drop new data (till the begin of the next day)
    * 3) split the data by the begin of the report day
    * 4) add last event of the previous day to fill 00:00 time
    * 5) reverse the result to have it sorted normally
    */
    val reverseSorted = allStates.sortWith { (first, second) ⇒ first.timestamp > second.timestamp }
    val withoutLatest = reverseSorted.dropWhile(state ⇒ state.timestamp >= endTime)
    val (dayStates, oldStates) = withoutLatest.span(state ⇒ state.timestamp >= startTime)
    val resultStates = oldStates match {
      case Nil       ⇒ dayStates //no states found before the report day
      case head :: _ ⇒ dayStates ++ List(head) //take the last state of the previous day
    }
    resultStates.reverse
  }

  def getStateLogHistory(systemId: String, startTime: Long, endTime: Long, curator: Curator): Map[CorridorIdMapping, Seq[ZkState]] = {
    val corridors = CorridorService.getCorridorIdMapping(curator, systemId)
    corridors.map(corridorId ⇒ corridorId -> {
      getStateLogHistory(systemId, corridorId.corridorId, startTime, endTime, curator)
    }).toMap
  }

  /**
   * Aggregate the corridors into one system state.
   * This is described in dailyreport interface description (v4.2)
   * For the combined HH-traject the different corridor states are combined
   * using the following priorities
   * 1. ALARM;
   * 2. ONDERHOUD;
   * 3. HANDHAVEN (DEGRADED);
   * 4. HANDHAVEN;
   * 5. HANDHAVEN (OFFLINE);
   * 6. STAND-BY.
   * 7. OFF:
   * @return
   */
  def aggregatedStates(corridorStates: Seq[Seq[ZkState]]): Seq[ZkState] = {
    case class StateChangeEvent(time: Long, oldState: String, newState: String, userId: String, reason: String)

    //create state change events for all corridors
    val changeEvents = corridorStates.map(stateList ⇒ {
      //create change events
      val sorted = stateList.sortBy(_.timestamp)
      val initial = ZkState(ZkState.off, 0, "system", "Not started")

      val (allEvents, _) = sorted.foldLeft(Seq[StateChangeEvent](), initial) {
        case ((events, previous), state) ⇒ {
          val event = StateChangeEvent(time = state.timestamp, oldState = previous.state, newState = state.state, userId = state.userId, reason = state.reason)
          (events :+ event, state)
        }
      }
      allEvents
    }).flatten.sortBy(_.time)

    val accu = new AccuState

    changeEvents.map(event ⇒ {
      accu.addStateChange(event.oldState, event.newState)
      ZkState(state = accu.getState(), timestamp = event.time, userId = event.userId, reason = event.reason)
    })
  }

  def getCurrentSystemState(systemId: String, curator: Curator): ZkState = {
    val allStates = getStateLogHistory(systemId, 0L, System.currentTimeMillis(), curator)
    val aggregated = aggregatedStates(allStates.values.toSeq).sortBy(_.timestamp)
    aggregated.lastOption.getOrElse(ZkState(ZkState.standBy, 0, "system", "Not started"))
  }

  /**
   * Update existing state changes due to a delayed degraded state change
   * result is that all the enforceOn states have to be updated to degraded within the
   * period state.timestamp until now and depending of the state just before the delayed state change
   * the state change have to be added (only when the previous state is EnforceOn)
   *
   * @param systemId The systemId of the state change
   * @param corridorId The corridorId of the state change
   * @param state The Delayed state change
   * @param curator the curator to zookeeper
   */
  private def updateHistoryEnforceOnState(systemId: String, corridorId: String, state: ZkState, curator: Curator) {
    //get all state changes
    val histPath = Paths.State.getHistoryPath(systemId, corridorId)
    val allStates = if (curator.exists(histPath)) {
      val allPaths = curator.getChildren(histPath)
      allPaths.map(path ⇒ (path, curator.getVersioned[ZkState](path)))
    } else {
      Seq()
    }
    def getTime(versionState: (String, Option[Versioned[ZkState]])): Long = {
      versionState._2.map(_.data.timestamp).getOrElse(0L)
    }
    //filter the states
    val (statesbefore, statesAfter) = allStates.partition(getTime(_) <= state.timestamp)
    val stateBeforeChange = statesbefore.sortBy(getTime(_)).lastOption
    stateBeforeChange match {
      case Some((path, Some(versionState))) if (versionState.data.state == ZkState.enforceOn &&
        versionState.data.timestamp == state.timestamp) ⇒ {
        //update the state to enforce degraded
        curator.set(path, versionState.data.copy(state = state.state), versionState.version)
      }
      case Some((key, Some(versionState))) if (versionState.data.state == ZkState.enforceOn) ⇒ {
        //add the degraded state change here
        curator.appendEvent(Paths.State.getHistoryPrefixPath(systemId, corridorId), state)
      }
      case other ⇒ {
        //no need to add the state change in the history
      }
    }
    //update the enforceOn states
    statesAfter.foreach(p ⇒ {
      val x = p._2.get.data.state
      p match {
        case (path, Some(versionState)) if (versionState.data.state == ZkState.enforceOn) ⇒ {
          //update the state to enforce degraded
          curator.set(path, versionState.data.copy(state = state.state), versionState.version)
        }
        case other ⇒ {
          //no need to add the state change in the history
        }
      }
    })
  }

}

