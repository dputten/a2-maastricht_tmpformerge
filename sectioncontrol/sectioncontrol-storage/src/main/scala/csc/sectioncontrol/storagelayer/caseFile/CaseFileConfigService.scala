package csc.sectioncontrol.storagelayer.caseFile

import csc.sectioncontrol.storage.{ ZkCaseFileConfig, Paths }
import csc.curator.utils.Curator
import csc.sectioncontrol.storagelayer.corridor.CorridorService
import csc.akkautils.DirectLogging

trait CaseFileConfigService {
  def create(systemId: String, corridorId: String, caseFileConfig: ZkCaseFileConfig)

  def get(systemId: String, corridorId: String): Option[ZkCaseFileConfig]

  def get(systemId: String, corridorInfoId: Int): Option[ZkCaseFileConfig]

  def exists(systemId: String, corridorId: String): Boolean
}

class CaseFileConfigZooKeeperService(zkStore: Curator) extends CaseFileConfigService with DirectLogging {
  def create(systemId: String, corridorId: String, caseFileConfig: ZkCaseFileConfig) = {
    zkStore.put(Paths.Corridors.getCaseFileConfig(systemId, corridorId), caseFileConfig)
  }

  def get(systemId: String, corridorId: String): Option[ZkCaseFileConfig] = {
    val result = zkStore.get[ZkCaseFileConfig](Paths.Corridors.getCaseFileConfig(systemId, corridorId))
    log.debug("zkStore.get[ZkCaseFileConfig] with path:" + Paths.Corridors.getCaseFileConfig(systemId, corridorId))
    log.debug("get(systemId: String, corridorId: String):" + result)
    result
  }

  def get(systemId: String, corridorInfoId: Int): Option[ZkCaseFileConfig] = {
    val corridorIds = CorridorService.getCorridorIdMapping(zkStore, systemId)
    log.debug("corridorIds:" + corridorIds)
    corridorIds.find(corridorIdMapping ⇒ corridorIdMapping.infoId == corridorInfoId) match {
      case Some(corridorIdMapping) ⇒
        val result = get(systemId, corridorIdMapping.corridorId)
        log.debug("get(systemId: String, corridorInfoId: Int):" + result.toString)
        result
      case _ ⇒
        log.debug("get(systemId: String, corridorInfoId: Int): None")
        None
    }
  }

  def exists(systemId: String, corridorId: String): Boolean = {
    val exists = zkStore.exists(Paths.Corridors.getCaseFileConfig(systemId, corridorId))
    log.debug("exists:" + exists)
    exists
  }
}
