/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.alerts

import csc.akkautils.DirectLogging

object AlertLocation extends DirectLogging {
  type Location = (String, String, String)
  val otherSeparators = Seq("-", "/", """\\""")
  val usedSeparator = '|'

  object ConfigType {
    val System = "System"
    val Corridor = "Corridor"
    val Section = "Section"
    val Gantry = "Gantry"
    val Lane = "Lane"
    val Other = "Other"
  }

  def getEventLocation(alertPath: String, configType: String): Location = {
    //make path conform
    val path = otherSeparators.foldLeft(alertPath) { case (path, separator) ⇒ path.replaceAll(separator, usedSeparator.toString) }

    val partList = path.split(usedSeparator).toSeq
    if (configType == ConfigType.Lane && partList.size == 4) {
      (partList(0), partList(1), partList(2))
    } else if (configType == ConfigType.Gantry && partList.size == 3) {
      (partList(0), partList(1), "")
    } else if (configType == ConfigType.System && partList.size == 1) {
      ("", "", "")
    } else {
      log.warning("Found a alertPath which could not be processed type=%s path=%s".format(configType, alertPath))
      ("", "", "")
    }
  }

}