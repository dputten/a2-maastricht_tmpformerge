package csc.sectioncontrol.storagelayer

import csc.sectioncontrol.messages.StatsDuration
import csc.sectioncontrol.storage.{ SpeedFixedStatistics, RedLightStatistics, SectionControlStatistics }

/**
 * Defines a general interface for accessing service statistics data.
 */
trait ServiceStatisticsRepositoryAccess {
  /**
   * Retrieve SectionControlStatistics
   * @param systemId The system to retrieve the statistics for
   * @param statsDuration The duration the statistics should belong to.
   * @return The requested statistics
   */
  def getSectionControlStatistics(systemId: String, statsDuration: StatsDuration): Seq[SectionControlStatistics]
  /**
   * Retrieve RedLightStatistics
   * @param systemId The system to retrieve the statistics for
   * @param statsDuration The duration the statistics should belong to.
   * @return The requested statistics
   */
  def getRedLightStatistics(systemId: String, statsDuration: StatsDuration): Seq[RedLightStatistics]
  /**
   * Retrieve SpeedFixedStatistics
   * @param systemId The system to retrieve the statistics for
   * @param statsDuration The duration the statistics should belong to.
   * @return The requested statistics
   */
  def getSpeedFixedStatistics(systemId: String, statsDuration: StatsDuration): Seq[SpeedFixedStatistics]

  /**
   * Save a SectionControlStatistics for systemId.
   * @param systemId The system the statistics should be saved for.
   * @param statistics The statistics to be saved.
   */
  def save(systemId: String, statistics: SectionControlStatistics): Unit

  /**
   * Save a RedLightStatistics for systemId.
   * @param systemId The system the statistics should be saved for.
   * @param statistics The statistics to be saved.
   */
  def save(systemId: String, statistics: RedLightStatistics): Unit

  /**
   * Save a SpeedFixedStatistics for systemId.
   * @param systemId The system the statistics should be saved for.
   * @param statistics The statistics to be saved.
   */
  def save(systemId: String, statistics: SpeedFixedStatistics): Unit
}
