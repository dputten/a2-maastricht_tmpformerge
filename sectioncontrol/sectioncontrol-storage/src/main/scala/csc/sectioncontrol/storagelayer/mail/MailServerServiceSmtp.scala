package csc.sectioncontrol.storagelayer.mail

import csc.curator.utils.{ Versioned, Curator }
import csc.sectioncontrol.storage.Paths
import csc.config.Path
import csc.sectioncontrol.messages.MailServer

trait MailServerService {
  def create(mailServer: MailServer)
  def get(): Option[MailServer]
  def getVersioned(): Option[Versioned[MailServer]]
  def update(mailServer: MailServer, version: Int)
  def delete()
  def exists()
}

class MailServerServiceSmtp(zkStore: Curator) extends MailServerService {
  private def zkPath = Paths.Configuration.getMailServerPath

  def create(mailServer: MailServer) = {
    zkStore.put(zkPath, mailServer)
  }

  def get(): Option[MailServer] = {
    zkStore.get[MailServer](Path(zkPath))
  }

  def getVersioned(): Option[Versioned[MailServer]] = {
    zkStore.getVersioned[MailServer](zkPath)
  }

  def update(mailServer: MailServer, version: Int) = {
    zkStore.set(zkPath, mailServer, version)
  }

  def delete() = {
    zkStore.deleteRecursive(zkPath)
  }

  def exists() = {
    zkStore.exists(zkPath)
  }
}

