/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.sectioncontrol.storagelayer.excludelog

import akka.util.duration._

import csc.config.Path
import csc.curator.utils.Curator
import csc.sectioncontrol.enforce.SystemEventData

/**
 * Log enforcement suspend and resume events to zookeeper, generate an overview of the resulting exclusion
 * periods when no violation should be registered, handle cleanup of stored event.
 *
 * @author Maarten Hazewinkel
 */
class ExcludeLogLayer(zkStore: Curator) {

  import ExcludeLogLayer._

  def saveEvent(systemId: String, event: SuspendEvent) {
    val storePath = buildCorridorPath(systemId, event.corridorIdFull)
    if (!zkStore.exists(storePath))
      zkStore.createEmptyPath(storePath)
    zkStore.appendEvent(storePath / suspendEventPrefix, event)
  }

  def readEvents(systemId: String, corridorId: Int): List[SuspendEvent] = {
    val paths = buildCorridorPath(systemId, corridorId.toString) :: buildCorridorPath(systemId, corridorIdAll) :: Nil
    val allEvents = paths.filter(zkStore.exists(_)).flatMap(zkStore.getChildren(_))
    allEvents.
      // Filter out other zk elements, just in case they've sneaked into our store
      filter(_.nodes.last.name.startsWith(suspendEventPrefix)).
      // Retrieve each event
      map(zkStore.get[SuspendEvent](_)).
      // Retain only actual events, ignoring failed retrievals
      flatten.sortBy(_.effectiveTimestamp)
  }

  def readEvents(systemId: String): List[SuspendEvent] = {
    val path = buildCorridorPath(systemId, corridorIdAll)
    val allEvents = if (zkStore.exists(path)) {
      zkStore.getChildren(path)
    } else {
      Seq()
    }
    allEvents.
      // Filter out other zk elements, just in case they've sneaked into our store
      filter(_.nodes.last.name.startsWith(suspendEventPrefix)).
      // Retrieve each event
      map(zkStore.get[SuspendEvent](_)).
      // Retain only actual events, ignoring failed retrievals
      flatten.toList.sortBy(_.effectiveTimestamp)
  }

  def cleanEvents(systemId: String, retentionDays: Int) {
    val retentionDuration = (retentionDays + 1).days
    val lastDeleteTime = System.currentTimeMillis() - retentionDuration.toMillis
    val dataPath = buildExcludeLogPath(systemId)

    if (zkStore.exists(dataPath)) {
      val oldEvents = zkStore.getChildren(dataPath).flatMap {
        corridorPath ⇒
          {
            zkStore.getChildren(corridorPath).flatMap {
              eventPath ⇒
                {
                  if (eventPath.nodes.last.name.startsWith(suspendEventPrefix)) {
                    zkStore.get[SuspendEvent](eventPath).
                      filter(_.effectiveTimestamp < lastDeleteTime).
                      map((eventPath, _))
                  } else {
                    None
                  }
                }
            }
          }
      }

      oldEvents.groupBy(_._2.sourceId).foreach {
        case (_, eventGroup) ⇒ {
          val sortedEventGroup = eventGroup.sortBy(eventData ⇒ (eventData._2.effectiveTimestamp, eventData._2.sequenceNumber))
          // retain last event if it is a suspend event
          val eventsToDelete = if (sortedEventGroup.last._2.suspend) sortedEventGroup.init else sortedEventGroup
          eventsToDelete.foreach(eventData ⇒ zkStore.delete(eventData._1))
        }
      }
    }
  }
}

/**
 * constants and utility functions for the ExcludeLog actor
 */
object ExcludeLogLayer {
  private[excludelog] val corridorIdAll = "All"
  private val suspendEventPrefix = "suspendEvent"
  private val baseZkPath = Path("/ctes/systems")

  def buildExcludeLogPath(systemId: String): Path = {
    baseZkPath / systemId / "excludelog"
  }

  private def buildCorridorPath(systemId: String, corridorId: String): Path = {
    buildExcludeLogPath(systemId) / corridorId
  }

}

case class SuspendEvent(componentId: String,
                        effectiveTimestamp: Long,
                        corridorId: Option[Int],
                        sequenceNumber: Long,
                        suspend: Boolean,
                        suspensionReason: Option[String]) {
  val isSuspendOrResumeEvent = true
  val corridorIdFull: String = {
    corridorId.map(_.toString).getOrElse(ExcludeLogLayer.corridorIdAll)
  }
  val sourceId = corridorIdFull + "/" + componentId
}

object SuspendEvent {
  def apply(event: SystemEventData): SuspendEvent = SuspendEvent(event.componentId,
    event.effectiveTimestamp,
    event.corridorId,
    event.sequenceNumber,
    event.suspend,
    event.suspensionReason)
}
