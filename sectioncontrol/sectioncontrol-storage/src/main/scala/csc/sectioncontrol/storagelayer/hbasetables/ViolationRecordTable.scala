/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.hbasetables

import csc.hbase.utils._
import csc.sectioncontrol.storage.HBaseTableDefinitions._
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.util.Bytes
import csc.sectioncontrol.storage._
import org.apache.hadoop.conf.Configuration
import csc.sectioncontrol.storage.KeyWithTimeAndId
import csc.sectioncontrol.storage.KeyWithTimeAndId

object ViolationRecordTable {
  type Reader = GenReader[KeyWithTimeAndId, vehicleViolationRecordType]

  type Writer = GenWriter[vehicleClassifiedRecordType, vehicleViolationRecordType]

  def makeReader(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper): Reader = {
    val tableName = vehicleViolationTableName
    HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, vehicleViolationColumnFamily)
    val table = new HTable(hbaseConfig, tableName)
    tableKeeper.addTable(table)
    new JsonHBaseReader[KeyWithTimeAndId, vehicleViolationRecordType] {
      val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(tableName)
      def hbaseReaderTable = table
      def hbaseReaderDataColumnFamily = vehicleViolationColumnFamily
      def hbaseReaderDataColumnName = vehicleViolationColumnName
      def makeReaderKey(keyValue: KeyWithTimeAndId) = Bytes.toBytes("%s-%s".format(keyValue.id, keyValue.time))
      override def jsonReaderFormats = SerializerFormats.formats
    }
  }

  def makeWriter(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper): Writer = {
    val tableName = HBaseTableDefinitions.vehicleViolationTableName
    HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, vehicleViolationColumnFamily)
    val table = new HTable(hbaseConfig, tableName)
    tableKeeper.addTable(table)

    new JsonHBaseWriter[vehicleClassifiedRecordType, vehicleViolationRecordType] with GenerateId {
      val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(tableName)
      def hbaseWriterTable = table
      def hbaseWriterDataColumnFamily = vehicleViolationColumnFamily
      def hbaseWriterDataColumnName = vehicleViolationColumnName

      def makeWriterKey(keyValue: vehicleClassifiedRecordType) = Bytes.toBytes("%s-%s-%s-%s".format(keyValue.speedRecord.entry.lane.system, keyValue.time, keyValue.speedRecord.corridorId, nextId))
      override def jsonWriterFormats = SerializerFormats.formats
    }
  }

}