package csc.sectioncontrol.storagelayer.roles

import csc.sectioncontrol.storage.{ Paths, ZkRole }
import csc.curator.utils.{ Versioned, Curator }

trait RoleService {
  def create(role: ZkRole)

  def get(roleId: String): Option[ZkRole]

  def getVersioned(roleId: String): Option[Versioned[ZkRole]]

  def update(roleId: String, role: ZkRole, version: Int)

  def delete(roleId: String)

  def exists(roleId: String)

  def getAll(zkStore: Curator): Seq[Option[ZkRole]]

  def getByName(name: String): Option[ZkRole]
}

class RoleServiceZooKeeper(zkStore: Curator) extends RoleService {
  def create(role: ZkRole) = {
    zkStore.put(Paths.Roles.getRootPath + "/" + role.id, role)
  }

  def get(roleId: String): Option[ZkRole] = {
    zkStore.get[ZkRole](Paths.Roles.getRolePath(roleId))
  }

  def getVersioned(roleId: String): Option[Versioned[ZkRole]] = {
    zkStore.getVersioned[ZkRole](Paths.Roles.getRolePath(roleId))
  }

  def update(roleId: String, role: ZkRole, version: Int) = {
    zkStore.set(Paths.Roles.getRolePath(roleId), role, version)
  }

  def delete(roleId: String) = {
    zkStore.deleteRecursive(Paths.Roles.getRolePath(roleId))
  }

  def exists(roleId: String) = {
    zkStore.exists(Paths.Roles.getRolePath(roleId))
  }

  def getAll(zkStore: Curator): Seq[Option[ZkRole]] = {
    zkStore.getChildren(Paths.Roles.getRootPath).map(
      path ⇒ zkStore.get[ZkRole](path))
  }

  /**
   * retrieve a RoleModel based on name
   */
  def getByName(name: String): Option[ZkRole] = getAll(zkStore).find(_.get.name == name).get
}
