/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.corridor

import csc.curator.utils.Curator
import csc.sectioncontrol.storage._
import csc.akkautils.DirectLogging
import csc.sectioncontrol.storage.ZkDailyReportConfig
import csc.sectioncontrol.storage.ZkSection
import csc.sectioncontrol.storage.ZkCorridor
import scala.Some

trait CorridorServiceTrait {

  def getGantriesForCorridor(systemId: String, corridorId: String): Seq[String]

  def getCorridorIds(systemId: String): Seq[String]

  def getCorridorIdMapping(systemId: String): Seq[CorridorIdMapping]

  def getCorridorIdMapping(systemId: String, corridorId: Int): Option[CorridorIdMapping]

  def createCorridor(systemId: String, corridor: ZkCorridor): Unit

  def createDailyReportConfig(systemId: String, corridorId: String, zkDailyReportConfig: ZkDailyReportConfig): Unit

  def getDailyReportConfig(systemId: String, corridorId: String): Option[ZkDailyReportConfig]

  def deleteDailyReportConfig(systemId: String, corridorId: String): Unit

  def existsDailyReportConfig(systemId: String, corridorId: String): Boolean
}

class CorridorServiceCuratorImpl(curator: Curator) extends CorridorServiceTrait {

  def getGantriesForCorridor(systemId: String, corridorId: String): Seq[String] =
    CorridorService.getGantriesForCorridor(curator, systemId, corridorId)

  def getCorridorIds(systemId: String): Seq[String] =
    CorridorService.getCorridorIds(curator, systemId)

  def getCorridorIdMapping(systemId: String): Seq[CorridorIdMapping] =
    CorridorService.getCorridorIdMapping(curator, systemId)

  def getCorridorIdMapping(systemId: String, corridorId: Int): Option[CorridorIdMapping] =
    CorridorService.getCorridorIdMapping(curator, systemId, corridorId)

  def createCorridor(systemId: String, corridor: ZkCorridor): Unit =
    CorridorService.createCorridor(curator, systemId, corridor)

  def createDailyReportConfig(systemId: String, corridorId: String, zkDailyReportConfig: ZkDailyReportConfig): Unit =
    CorridorService.createDailyReportConfig(curator, systemId, corridorId, zkDailyReportConfig)

  def getDailyReportConfig(systemId: String, corridorId: String): Option[ZkDailyReportConfig] =
    CorridorService.getDailyReportConfig(curator, systemId, corridorId)

  def deleteDailyReportConfig(systemId: String, corridorId: String): Unit =
    CorridorService.deleteDailyReportConfig(curator, systemId, corridorId)

  def existsDailyReportConfig(systemId: String, corridorId: String): Boolean =
    CorridorService.existsDailyReportConfig(curator, systemId, corridorId)

}

object CorridorService extends DirectLogging {

  def getGantriesForCorridor(curator: Curator, systemId: String, corridorId: String): Seq[String] = {
    val corridorOpt = curator.get[ZkCorridor](Paths.Corridors.getConfigPath(systemId, corridorId))
    val gantries = corridorOpt match {
      case None ⇒ Seq()
      case Some(corridor) ⇒
        val startSectionOpt = curator.get[ZkSection](Paths.Sections.getConfigPath(systemId, corridor.startSectionId))
        val startGantry = startSectionOpt.map(_.startGantryId)
        val endSectionOpt = curator.get[ZkSection](Paths.Sections.getConfigPath(systemId, corridor.endSectionId))
        val endGantry = endSectionOpt.map(_.endGantryId)
        Seq() ++ startGantry ++ endGantry
    }
    if (gantries.size != 2) {
      log.warning("Problems with getting gantries for corridor %s:%s: %s".format(systemId, corridorId, gantries))
    }
    gantries
  }

  def getCorridorIds(curator: Curator, systemId: String): Seq[String] = {
    curator.getChildNames(Paths.Corridors.getDefaultPath(systemId))
  }

  def getCorridorIdMapping(curator: Curator, systemId: String): Seq[CorridorIdMapping] = {
    val ids = getCorridorIds(curator, systemId)
    ids.map(corridorId ⇒ {
      val infoId = curator.get[ZkCorridor](Paths.Corridors.getConfigPath(systemId, corridorId)).map(_.info.corridorId)
      CorridorIdMapping(corridorId, infoId.getOrElse(-1))
    })
  }

  def getCorridorIdMapping(curator: Curator, systemId: String, corridorId: Int): Option[CorridorIdMapping] = {
    val ids = getCorridorIds(curator, systemId)
    ids.map(corridorId ⇒ {
      val infoId = curator.get[ZkCorridor](Paths.Corridors.getConfigPath(systemId, corridorId)).map(_.info.corridorId)
      CorridorIdMapping(corridorId, infoId.getOrElse(-1))
    })

    CorridorService.getCorridorIdMapping(curator, systemId).find(_.infoId == corridorId)
  }

  def createCorridor(curator: Curator, systemId: String, corridor: ZkCorridor) {
    val configPath = Paths.Corridors.getConfigPath(systemId, corridor.id)
    curator.put(configPath, corridor)
  }

  def createDailyReportConfig(curator: Curator, systemId: String, corridorId: String, zkDailyReportConfig: ZkDailyReportConfig) {
    val configPath = Paths.Corridors.getDailyReportConfigPath(systemId, corridorId)
    curator.put(configPath, zkDailyReportConfig)
  }

  def getDailyReportConfig(curator: Curator, systemId: String, corridorId: String): Option[ZkDailyReportConfig] = {
    curator.get[ZkDailyReportConfig](Paths.Corridors.getDailyReportConfigPath(systemId, corridorId))
  }

  def deleteDailyReportConfig(curator: Curator, systemId: String, corridorId: String) = {
    curator.deleteRecursive(Paths.Corridors.getDailyReportConfigPath(systemId, corridorId))
  }

  def existsDailyReportConfig(curator: Curator, systemId: String, corridorId: String) = {
    curator.exists(Paths.Corridors.getDailyReportConfigPath(systemId, corridorId))
  }
}

case class CorridorIdMapping(corridorId: String, infoId: Int)