/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer.hbasetables

import org.apache.hadoop.conf.Configuration
import csc.sectioncontrol.storage.KeyWithTimeAndId
import csc.hbase.utils._
import org.apache.hadoop.hbase.client.HTable
import org.apache.hadoop.hbase.util.Bytes
import csc.sectioncontrol.storage.HBaseTableDefinitions._
import csc.sectioncontrol.storage.KeyWithTimeAndId

object CalibrationImageTable {

  type Reader = GenReader[KeyWithTimeAndId, calibrationImageRecordType]

  def makeReader(hbaseConfig: Configuration, tableKeeper: HBaseTableKeeper): Reader = {
    val tableName = calibrationImageTableName
    HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, calibrationImageColumnFamily)
    val table = new HTable(hbaseConfig, tableName)
    tableKeeper.addTable(table)
    new HBaseReader[KeyWithTimeAndId, calibrationImageRecordType] {
      val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(tableName)
      def hbaseReaderTable = table
      def hbaseReaderDataColumnFamily = calibrationImageColumnFamily
      def hbaseReaderDataColumnName = calibrationImageColumnName
      def makeReaderKey(keyValue: KeyWithTimeAndId) = Bytes.toBytes("%s-%s".format(keyValue.time.toString, keyValue.id))
      def deserialize(bytes: Array[Byte])(implicit mt: Manifest[calibrationImageRecordType]) = bytes
    }
  }

}