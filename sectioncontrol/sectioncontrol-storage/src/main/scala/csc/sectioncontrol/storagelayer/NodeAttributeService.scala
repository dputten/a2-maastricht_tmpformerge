/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.sectioncontrol.storagelayer

import csc.curator.utils.Curator
import net.liftweb.json.JsonParser.ParseException
import net.liftweb.json.MappingException

/**
 * Service to get the Node attributes.
 * At this moment only Node Type is supported
 */
object NodeAttributeService {
  /**
   * Get the Node Type
   * @param nodePath path of the node
   * @param zkStore The Zookeeper Curator
   * @return Some(String) when node has an type otherwise None
   */
  def getNodeType(nodePath: String, zkStore: Curator): Option[String] = {
    try {
      val attr = zkStore.get[NodeAttributes](nodePath)
      attr.map(_.nodeType)
    } catch {
      case ex: ParseException ⇒ {
        //node doesn't has attributes
        None
      }
      case mex: MappingException ⇒ {
        //node doesn't has attributes
        None
      }
    }
  }
}

/**
 * The Node attribute class conatining all supported attributes
 * @param nodeType
 */
case class NodeAttributes(nodeType: String)
