package csc.sectioncontrol.storagelayer.calibrationstate

import csc.curator.utils.Curator
import csc.sectioncontrol.storage.{ ZkTriggerData, Paths }

/**
 * Service to get the CalibrationState.
 */
object CalibrationStateService {

  def create(systemId: String, corridorId: String, triggerData: ZkTriggerData, zkStore: Curator) = {
    zkStore.put(Paths.CalibrationState.getDefaultPath(systemId, corridorId), triggerData)
  }

  def getTriggerData(systemId: String, corridorId: String, zkStore: Curator): Option[ZkTriggerData] = {
    zkStore.get[ZkTriggerData](Paths.CalibrationState.getDefaultPath(systemId, corridorId))
  }

  def isCalibrating(systemId: String, corridorId: String, zkStore: Curator): Boolean = {
    zkStore.exists(Paths.CalibrationState.getDefaultPath(systemId, corridorId))
  }
}
