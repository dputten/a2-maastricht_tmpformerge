package com.csc.fileTransfer

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 12/4/14.
 */

class DataBlockTest extends WordSpec with MustMatchers {
  "DataBlock" must {
    "properly compare" in {
      DataBlock(0, 10) < DataBlock(1, 8) must be(true)
      DataBlock(0, 10) < DataBlock(0, 10) must be(false)
    }

    "properly calculate if it encompasses another DataBlock" in {
      DataBlock(0, 10) contains DataBlock(0, 10) must be(true)
      DataBlock(0, 10) contains DataBlock(8, 2) must be(true)
      DataBlock(0, 10) contains DataBlock(9, 2) must be(false)
      DataBlock(1, 10) contains DataBlock(0, 3) must be(false)
    }
  }
}
