package com.csc.fileTransfer

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 12/4/14.
 */

import org.scalatest.matchers.MustMatchers
import org.scalatest.WordSpec

class SerializationTest extends WordSpec with MustMatchers {
  implicit val formats = net.liftweb.json.DefaultFormats
  "Serialization" must {
    "properly serialize and de-serialize Seq[DataBlock]" in {
      val input = Seq(DataBlock(1, 2), DataBlock(3, 4), DataBlock(4, 5))
      val jsonString = DataBlockSerializer.serialize(input)
      val result = DataBlockSerializer.deserialize(jsonString)
      result must equal(input)
    }
  }
}
