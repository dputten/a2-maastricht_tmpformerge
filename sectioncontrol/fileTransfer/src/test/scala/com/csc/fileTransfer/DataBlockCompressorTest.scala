package com.csc.fileTransfer

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 12/4/14.
 */

class DataBlockCompressorTest extends WordSpec with MustMatchers {
  "DataBlockCompressor" must {
    "properly merge multiple contiguous DataBlocks" in {
      val input = Seq(DataBlock(0, 10), DataBlock(10, 2), DataBlock(12, 1))
      val expected = Seq(DataBlock(0, 13))
      val compressor = new DataBlockCompressor(input)
      val result = compressor.compress
      result must equal(expected)
    }
    "not merge blocks that aren't contiguous" in {
      val input = Seq(DataBlock(0, 10), DataBlock(11, 2), DataBlock(13, 1), DataBlock(15, 2))
      val expected = Seq(DataBlock(0, 10), DataBlock(11, 3), DataBlock(15, 2))
      val compressor = new DataBlockCompressor(input)
      val result = compressor.compress
      result must equal(expected)
    }
    "properly handle unsorted list of blocks" in {
      val input = Seq(DataBlock(15, 2), DataBlock(0, 10), DataBlock(13, 1), DataBlock(11, 2))
      val expected = Seq(DataBlock(0, 10), DataBlock(11, 3), DataBlock(15, 2))
      val compressor = new DataBlockCompressor(input)
      val result = compressor.compress
      result must equal(expected)
    }
  }
}
