package com.csc.fileTransfer

import akka.actor.Actor
import csc.akkautils.DirectLogging
import java.io.File
import org.apache.commons.io.FileUtils
import akka.event.LoggingReceive

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 11/25/14.
 */

class FileDuplicator(val selector: FileSelector, val destinations: Seq[File], postProcess: (File) ⇒ Unit) extends Actor with DirectLogging {

  protected def receive = LoggingReceive {
    case "Tick" ⇒ handleTick
  }

  protected def handleTick: Unit = {
    val files = selector.listFilesWithState(FileState.ReadyForProcessing)
    files.foreach((sourceFile) ⇒ {
      destinations.foreach((destinationFile) ⇒ FileUtils.copyFile(sourceFile, destinationFile))
      postProcess(sourceFile)
    })
  }
}
