package com.csc.fileTransfer

import scala.collection.JavaConversions._

import akka.actor.{ ActorLogging, Actor, ActorRef }
import java.io.{ FileInputStream, BufferedInputStream, DataInputStream, File }

import com.github.sstone.amqp.Amqp.Publish
import com.rabbitmq.client.AMQP.BasicProperties
import org.apache.commons.io.FileUtils
import csc.sectioncontrol.messages.SystemEvent
import ExtendedFile._

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 11/24/14.
 */

case class SendState(file: File, inputStream: DataInputStream, size: Long, position: Long) {
  def headerMap: Map[String, String] = {
    Map(
      "filename" -> file.getName,
      "size" -> "%d".format(file.length()),
      "position" -> "%d".format(position))
  }
}

class FileSendActor(systemId: String, corridorId: Option[String], file: File, producer: ActorRef, endPoint: String,
                    routingKey: String, bufferSize: Int = 10000, archiveDir: Option[File])

  extends Actor with ActorLogging {

  val buf = new Array[Byte](bufferSize)
  var originalSender: ActorRef = _

  def componentId = "AMQP-FileTransfer"

  protected def receive = {
    case "Start" ⇒
      originalSender = sender
      logSystemEvent("Start overdracht van bestand %s.".format(file.getAbsolutePath))
      sendFile(file)
    case state: SendState ⇒
      sendData(state)
    case "Archive" ⇒
      logSystemEvent("Overdracht van bestand %s voltooid.".format(file.getAbsolutePath))
      archiveFile(file, archiveDir)
      context.stop(self)
  }

  def sendFile(file: File): Unit = {
    val fileSize = file.length()
    if (file.exists()) {
      val inputStream = new DataInputStream(new BufferedInputStream(new FileInputStream(file), bufferSize))
      self ! SendState(file, inputStream, fileSize, position = 0)
    }
  }

  def sendData(state: SendState): Unit = {
    val read = state.inputStream.read(buf, 0, bufferSize)
    if (read > -1 || state.size == 0) { // also send empty files
      val bytesToCopy = scala.math.max(read, 0)
      log.debug("Sending %s, %d + %d".format(state.file.getName, state.position, bytesToCopy))
      val headers = Some(new BasicProperties.Builder().
        headers(state.headerMap).
        deliveryMode(2 /*AMQP.BasicProperties.PERSISTANT_BASIC*/ ).build())
      val sendBuffer = java.util.Arrays.copyOf(buf, bytesToCopy)
      val msg = Publish(endPoint, routingKey, sendBuffer, properties = headers, mandatory = true, immediate = false)
      producer ! msg
    }
    if (read != -1) {
      val nextState = state.copy(position = state.position + read)
      self ! nextState
    } else {
      // reached EOF, close inputStream and archive file
      state.inputStream.close()
      self ! "Archive"
      originalSender ! FileProcessed(file)
    }
  }

  private[fileTransfer] def archiveFile(file: File, archiveDir: Option[File]): Unit = {
    if (archiveDir.isDefined) {
      val dir = archiveDir.get
      val uniqueArchiveFile = file.makeUniqueFile(dir)
      logSystemEvent("bestand %s wordt gearchiveerd naar %s.".format(file.getAbsolutePath, uniqueArchiveFile.getAbsolutePath))
      FileUtils.copyFile(file, uniqueArchiveFile)
    } else {
      logSystemEvent("bestand %s wordt verwijderd".format(file.getAbsolutePath))
    }
    FileUtils.deleteQuietly(file)
  }

  private[fileTransfer] def logSystemEvent(reason: String) {
    val alarmEvent = SystemEvent(componentId, System.currentTimeMillis(), systemId, "FileTransfer", "system",
      Some(reason), None, None, None, corridorId, None)
    context.system.eventStream.publish(alarmEvent)
  }

}
