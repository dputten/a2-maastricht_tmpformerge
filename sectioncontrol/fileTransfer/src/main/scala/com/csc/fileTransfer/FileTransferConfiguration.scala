package com.csc.fileTransfer

import com.typesafe.config.Config
import akka.util.Duration
import akka.util.duration._
import java.io.File

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 1/15/15.
 */

case class AmqpConnectionConfig(actorName: String, amqpUri: String, reconnectDelay: Duration) {
  def this(configPath: String, config: Config) = {
    this(
      config.getString(configPath + ".actorName"),
      config.getString(configPath + ".amqpUri"),
      config.getInt(configPath + ".reconnectDelay_ms") millis)
  }
}

case class FileSelectorConfig(directory: String, regex: String, pickupDelay_ms: Long) {
  def this(configPath: String, config: Config) = {
    this(
      config.getString(configPath + ".inputDirectory"),
      config.getString(configPath + ".fileRegex"),
      config.getInt(configPath + ".pickupDelay_ms"))
  }
}

case class FileSelectActorConfig(actorName: Option[String], scanInterval_ms: Int) {
  def this(configPath: String, config: Config) = {
    this(
      if (config.hasPath(configPath + ".actorName")) Some(config.getString(configPath + ".actorName")) else None,
      config.getInt(configPath + ".scanInterval_ms"))
  }
}

case class FileSendActorConfig(systemId: String, corridorId: Option[String], amqpEndPoint: String,
                               routingKey: String, bufferSize: Int, archiveDir: Option[File]) {
  def this(configPath: String, config: Config) = {
    this(
      config.getString(configPath + ".systemId"),
      if (config.hasPath(configPath + ".corridorId")) Some(config.getString(configPath + ".corridorId")) else None,
      config.getString(configPath + ".amqpEndpoint"),
      config.getString(configPath + ".routingKey"),
      config.getInt(configPath + ".bufferSize"),
      if (config.hasPath(configPath + ".archiveDirectory")) Some(new File(config.getString(configPath + ".archiveDirectory"))) else None)
  }
}

case class FileReceiverConfig(systemId: String, corridorId: Option[String], workDirPath: String, outputPath: String, indexPath: String, queueName: String) {
  def this(configPath: String, config: Config) = {
    this(
      config.getString(configPath + ".systemId"),
      if (config.hasPath(configPath + ".corridorId")) Some(config.getString(configPath + ".corridorId")) else None,
      config.getString(configPath + ".workingDirectory"),
      config.getString(configPath + ".deliveryDirectory"),
      config.getString(configPath + ".indexDirectory"),
      config.getString(configPath + ".queueName"))
  }
}