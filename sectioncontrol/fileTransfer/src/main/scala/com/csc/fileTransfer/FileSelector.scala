package com.csc.fileTransfer

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 11/4/14.
 */

import java.io.File
import akka.util.duration._
import java.util.Calendar

object FileState extends Enumeration {
  type FileState = Value
  val ReadyForProcessing, TooRecentlyModified, IncorrectModificationDate, FileNotFound = Value
}

/**
 * AbstractEsaFileHandler select ESA (Mtm, Tubes) files
 * and determines whether they're ready for processing
 */
trait FileSelector {

  def listFiles(): Seq[File]

  def listFilesWithState(state: FileState.Value): Seq[File] = {
    listFiles().filter((aFile) ⇒ fileState(aFile) == state)
  }

  def fileState(f: File): FileState.Value

}

class FilesInPathSelector(path: File, pickupDelay: Long = 1.minute.toMillis) extends FileSelector {
  require(path.isDirectory, "File %s must exist and be a directory".format(path.getAbsolutePath))

  def listFiles(): Seq[File] = {
    path.listFiles().filter(_.isFile)
  }

  def fileState(f: File): FileState.Value = {
    if (f.exists)
      fileState(f.lastModified())
    else
      FileState.FileNotFound
  }

  def fileState(lastModified: Long): FileState.Value = {
    val cal = Calendar.getInstance()
    val now = cal.getTimeInMillis

    if (now - lastModified <= pickupDelay)
      FileState.TooRecentlyModified
    else
      FileState.ReadyForProcessing
  }
}

class FileRegexSelector(path: File, pattern: String = ".*", pickupDelay_ms: Long = 1.minute.toMillis)
  extends FilesInPathSelector(path, pickupDelay_ms) {
  def this(config: FileSelectorConfig) = {
    this(
      new File(config.directory),
      config.regex,
      config.pickupDelay_ms)
  }

  override def listFiles(): Seq[File] = {
    super.listFiles().filter(_.getName.matches(pattern))
  }
}
