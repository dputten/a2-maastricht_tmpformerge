package com.csc.fileTransfer

import java.io.File

/**
 * Copyright (C) 2016 CSC. <http://www.csc.com>
 *
 * Created on 4/13/16.
 */

class ExtendedFile(val f: File) {
  val name = f.getName
  val dotPosition = name.lastIndexOf('.')

  def baseName: String = {
    if (dotPosition == -1) name else name.substring(0, dotPosition)
  }
  def extension: String = {
    if (dotPosition == -1) "" else name.substring(dotPosition + 1)
  }

  def makeUniqueFile(parent: File): File = {
    val aFile = new File(parent, name)
    if (aFile.exists) doMakeUnique(parent, baseName, extension) else aFile
  }

  def makeUniqueFile: File = makeUniqueFile(f.getParentFile)

  private def doMakeUnique(parent: File, prefix: String, extension: String): File = {
    def doMakeUnique(cnt: Int): File = {
      val _aFile = new File(parent, prefix + cnt + "." + extension)
      if (!_aFile.exists()) return _aFile
      doMakeUnique(cnt + 1)
    }
    doMakeUnique(0)
  }
}

object ExtendedFile {
  implicit def extendedFile(f: File) = new ExtendedFile(f)
}
