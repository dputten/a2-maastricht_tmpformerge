package com.csc.fileTransfer

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 11/21/14.
 */

import akka.actor.{ Props, ActorSystem, ActorRef }
import akka.util.Duration
import csc.akkautils.{ EventLogger, GenericBoot }
import com.typesafe.config.Config
import com.github.sstone.amqp.Amqp.{ ChannelParameters, QueueParameters, ExchangeParameters }
import com.github.sstone.amqp.{ Amqp, RabbitMQConnection }
import java.io.File
import scala.collection.JavaConversions._
import scala.collection.mutable.HashSet
import net.liftweb.json.{ DefaultFormats, Formats }
import csc.curator.utils.{ CuratorToolsImpl, Curator }
import akka.event.Logging
import csc.dlog.Event
import org.apache.curator.framework.CuratorFrameworkFactory
import org.apache.curator.retry.RetryUntilElapsed
import scala.collection.mutable
import csc.sectioncontrol.messages.SystemEvent

/**
 * Start the fileTransfer
 */
class FileTransferBoot extends GenericBoot {

  val eventLogRegistry = new mutable.HashSet[String]()
  def componentName = "fileTransfer"
  protected def retryPolicy = new RetryUntilElapsed(1000 * 60 * 60 * 24 * 3, 5000)
  private var curator: Curator = _
  implicit val system = actorSystem

  /**
   * Implemented by subclasses to start up any required actors in the actor system.
   */
  def startupActors() {
    implicit val system = actorSystem
    val configuration = actorSystem.settings.config
    val amqpTransfers = configuration.getConfigList("amqpFileTransfers")
    amqpTransfers.foreach((conf: Config) ⇒ {
      log.info("amqpTransfer config: {}", conf)
      val zkServerQuorum = conf.getString("zkServerQuorum")
      val amqpConf = new AmqpConnectionConfig("amqpConnection", conf)
      val rmqConnection = RabbitMQConnection.create(amqpConf.actorName, amqpConf.amqpUri, amqpConf.reconnectDelay)
      require(rmqConnection != null)

      val rcvrConfigs = conf.getConfigList("fileReceivers")
      rcvrConfigs.foreach((receiverConfig: Config) ⇒ {
        createReceiver(rmqConnection, receiverConfig)
      })

      val senderConfigs = conf.getConfigList("fileSenders")
      if (senderConfigs.size() > 0)
        curator = createCurator(actorSystem, zkServerQuorum)

      senderConfigs.foreach((senderConfig: Config) ⇒ {
        createFileSelectActor(rmqConnection, curator, senderConfig)
      })
    })
  }

  def createFileSendActor(config: FileSendActorConfig, file: File, producer: ActorRef): ActorRef = {

    actorSystem.actorOf(Props(
      new FileSendActor(config.systemId, config.corridorId, file: File, producer, config.amqpEndPoint,
        config.routingKey, config.bufferSize, config.archiveDir)), "send_" + file.getName)
  }

  def createFileSelectActor(conn: RabbitMQConnection, curator: Curator, conf: Config) {
    // Create fileSelector
    implicit val system = actorSystem

    // Create producer and wait until it's connected to the broker
    val producer = conn.createChannelOwner()
    Amqp.waitForConnection(actorSystem, producer).await()

    // Create FileSendActor object
    val fileSendActorConfig = new FileSendActorConfig("fileSendActor", conf)
    ensureEventLoggerStarted(fileSendActorConfig.systemId, curator)
    def createSendActor(f: File) = createFileSendActor(fileSendActorConfig, file = f, producer = producer)

    // Create fileSelector
    val selectorConfig = new FileSelectorConfig("fileSelector", conf)
    log.debug(selectorConfig.toString)
    val selector = new FileRegexSelector(selectorConfig)

    val actorConfig = new FileSelectActorConfig("fileSelectActor", conf)
    val actor = if (actorConfig.actorName.isDefined) {
      actorSystem.actorOf(Props(
        new FileSelectActor(selector, producer, createSendActor, actorConfig.scanInterval_ms)),
        actorConfig.actorName.get)
    } else {
      actorSystem.actorOf(Props(
        new FileSelectActor(selector, producer, createSendActor, actorConfig.scanInterval_ms)))
    }
  }

  def createReceiver(conn: RabbitMQConnection, conf: Config) {
    implicit val system = actorSystem

    val actorConfig = new FileReceiverConfig("fileReceiver", conf)
    val listener = actorSystem.actorOf(Props(
      new FileReceiveActor(actorConfig.systemId, actorConfig.corridorId, actorConfig.workDirPath, actorConfig.outputPath, actorConfig.indexPath)))
    val consumer = conn.createSimpleConsumer(actorConfig.queueName, listener, Some(ChannelParameters(100)), false)
    Amqp.waitForConnection(actorSystem, consumer).await()
  }

  def ensureEventLoggerStarted(systemId: String, curator: Curator): Unit = {
    if (!eventLogRegistry.contains(systemId)) {
      startEventLogger[SystemEvent](systemId, curator, actorSystem)
      eventLogRegistry += systemId
    }

  }
  /**
   * Implemented by subclasses to perform an orderly shutdown of the actors in the
   * actor system.
   */
  def shutdownActors() {
  }

  def createCurator(system: ActorSystem, zkServerQuorum: String, formats: Formats = DefaultFormats): Curator = {
    val logger = Logging.getLogger(system, classOf[CuratorToolsImpl])
    val curator = CuratorFrameworkFactory.newClient(zkServerQuorum, retryPolicy)
    curator.start()
    new CuratorToolsImpl(Some(curator), logger, finalFormats = formats) {
      override protected def extendFormats(defaultFormats: Formats) = formats
    }
  }

  /**
   * Start up an EventLogger in the given actor system.
   * This EventLogger will listen for SystemEventType events being published on the
   * actor system event stream and drop them into the appropriate zookeeper queue for
   * consolidation into the central system logs.
   */
  def startEventLogger[SystemEventType <: Event: Manifest](systemId: String, curator: Curator, actorSystem: ActorSystem) {
    val queuePath = "/ctes/systems/" + systemId + "/events"
    actorSystem.actorOf(Props(EventLogger.createDefaultEventLogger[SystemEventType](curator, queuePath)), "EventLogger_" + systemId)
  }

}
