package com.csc.fileTransfer

import java.io._
import akka.actor.{ ActorLogging, Actor }
import akka.event.LoggingReceive
import scala.collection.JavaConversions._
import org.apache.commons.io.FileUtils
import com.github.sstone.amqp.Amqp.Ack
import com.github.sstone.amqp.Amqp.Ok
import com.github.sstone.amqp.Amqp.Delivery
import scala.annotation.tailrec
import net.liftweb.json.JsonAST._
import net.liftweb.json.Extraction._
import net.liftweb.json.Printer._
import net.liftweb.json.JsonParser._

import ExtendedFile._
/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 11/24/14.
 */

/**
 * DataBlock describes a chunk of data in a file
 * @param position the position of the chunk n the file
 * @param size the size of the chunk
 */
case class DataBlock(position: Long, size: Int) {
  def <(that: DataBlock): Boolean = {
    if (this.position == that.position)
      this.size < that.size
    else
      this.position < that.position
  }

  def contains(that: DataBlock): Boolean = {
    this.position <= that.position && (this.position + this.size) >= (that.position + that.size)
  }
}

/**
 * Compresses a sequence of DataBlocks. If consecutive blocks b1 and b2 are contiguous
 * this algorithm compresses them into one with b.position = b1.position and
 * b.size = b1.size + b2.size
 * @param seq the initial uncompressed sequence of blocks
 */
class DataBlockCompressor(val seq: Seq[DataBlock]) {
  def compress: Seq[DataBlock] = {
    /**
     * Scan the list combining a given element with a sequence of elements, thus constructing
     * a compressed list of DataBlock instances.
     * @param elem the element to combine with the rest of the list
     * @param rest remaining elements
     * @return compressed list
     */
    @tailrec
    def scan(elem: DataBlock, rest: Seq[DataBlock], accu: Seq[DataBlock]): Seq[DataBlock] = {
      rest match {
        case Nil ⇒ (elem +: accu).reverse
        case head :: tail ⇒
          if (head.contains(elem)) {
            // ignore element because head already contains it
            scan(head, tail, accu)
          } else if ((elem.position + elem.size) == head.position) {
            // elem and head are contiguous, merge them
            scan(elem.copy(size = elem.size + head.size), tail, accu)
          } else {
            // elem and head are not contiguous. add elem to accu and move to next element
            scan(head, tail, elem +: accu)
          }
      }
    }

    /*
    To be able to combine consecutive DataBlocks, we need at least two of them
     */
    seq.sortWith((x, y) ⇒ x < y) match {
      case Nil          ⇒ Nil // Empty list is compressed
      case head :: Nil  ⇒ seq // single element is compressed
      case head :: tail ⇒ scan(head, tail, Nil)
    }
  }
}

/**
 * Object to serialize and deserialize a Seq[DataBlock]
 */
private[fileTransfer] object DataBlockSerializer {

  implicit val formats = net.liftweb.json.DefaultFormats

  /**
   * Serialize a Seq[DataBlock] into a json string
   * @param s the sequence to serialize
   * @return the json serialization of s
   */
  def serialize(s: Seq[DataBlock]): String = {
    pretty(render(decompose(s)))
  }

  /**
   * D-serialize a json string into a Seq[DataBlock]
   * @param s the json string to de-serialize
   * @return the de-serialized Seq[DataBlock]
   */
  def deserialize(s: String): Seq[DataBlock] = {
    val json = parse(s)
    json.extract[List[DataBlock]]
  }
}

/**
 * instances of FileReceiver handle receiving a single file
 * @param workDirPath this is where files are stored while the're being received
 * @param outputPath final destination after file is completely received
 * @param indexPath this is were indexfiles are receivd. An index file contains a description of
 *                  the chunks of data that were received sofar
 * @param filename  name of the file we're receiving
 * @param fileSize sis of the file we're receiving
 */
class FileReceiver(workDirPath: String, outputPath: String, indexPath: String, filename: String, fileSize: Long) {

  // define implicit method "compress" for values of type Seq[DataBlock]
  implicit def compressor(s: Seq[DataBlock]) = new DataBlockCompressor(s)

  private val outputDir = new File(outputPath)
  private val workDir = new File(workDirPath)
  private val workFile = new File(workDir, filename).makeUniqueFile
  private val indexFile = new File(indexPath, filename).makeUniqueFile

  FileUtils.forceMkdir(workDir)
  FileUtils.forceMkdir(outputDir)

  private val indexStream = new RandomAccessFile(indexFile, "rw")
  private val outputStream = new RandomAccessFile(workFile, "rw")
  outputStream.setLength(fileSize)

  private var buffersWritten = prepareBufferList

  /**
   * write a chunk of data to a given position in the output stream
   * @param pos the position in the file where the chunk must be witten
   * @param buffer the chunk of data to write
   */
  def writeData(pos: Long, buffer: Array[Byte]): Unit = {
    if (outputStream.getFilePointer != pos) outputStream.seek(pos)
    outputStream.write(buffer)
    buffersWritten = (DataBlock(pos, buffer.length) +: buffersWritten).compress
    writeIndex(buffersWritten)
  }

  /**
   * Check wehter the file is completely received
   * @return true if file is complete false is there's still chunks missing
   */
  def isDone: Boolean = {
    val totalBytes = buffersWritten.foldLeft(0)((accu, block) ⇒ accu + block.size)
    totalBytes == fileSize
  }

  /**
   * Perform housekeeping after a file is completely recived.
   * - Delete the index file
   * - Move this file to its final destination
   */
  def handleDone: Unit = {
    lazy val outputFile = new File(outputDir, filename).makeUniqueFile
    indexStream.close()
    FileUtils.deleteQuietly(indexFile)
    outputStream.close()
    FileUtils.copyFile(workFile, outputFile)
    FileUtils.deleteQuietly(workFile)
  }

  /**
   * Abort the operation by closing the streams
   */
  def abort(): Unit = {
    indexStream.close()
    FileUtils.deleteQuietly(indexFile)
    outputStream.close()
    FileUtils.deleteQuietly(workFile)
  }

  /**
   * Write the registered chunks to the index file
   * @param s the sequence of registered data chunks
   */
  private[fileTransfer] def writeIndex(s: Seq[DataBlock]): Unit = {
    val jsonString = DataBlockSerializer.serialize(s)
    indexStream.setLength(jsonString.length)
    indexStream.seek(0)
    indexStream.writeBytes(jsonString)
  }

  /**
   * Upon instantiation we check wether there's data in the index file
   * and retrieve the data-chunks that were already written to the file.
   * @return list of DataChunks
   */
  private[fileTransfer] def prepareBufferList: Seq[DataBlock] = {
    val lgt = indexStream.length.toInt
    if (lgt > 0) {
      val buffer = new Array[Byte](lgt)
      indexStream.readFully(buffer)
      val jsonString = new String(buffer)
      DataBlockSerializer.deserialize(jsonString)
    } else {
      Nil
    }
  }
}

/**
 * This actor receives messages from an AMQP queue. The messages describe a chunk of data belonging to
 * a file. The chunks are written to a file until all chunks are received.
 * @param workDirPath path to the working directory
 * @param outputPath path to the delivery directory
 */
class FileReceiveActor(systemId: String, corridorId: Option[String], workDirPath: String, outputPath: String, indexPath: String)
  extends Actor with ActorLogging {

  FileUtils.forceMkdir(new File(workDirPath))
  FileUtils.forceMkdir(new File(outputPath))
  FileUtils.forceMkdir(new File(indexPath))

  // Active receiver instances
  private var receivers = Map[String, FileReceiver]()

  protected def receive = LoggingReceive {
    case delivery: Delivery ⇒ {
      handleDelivery(delivery)
      sender ! Ack(delivery.envelope.getDeliveryTag)
    }
    case Ok(Ack(deliveryTag), None) ⇒

    case default ⇒ {
      log.warning("Unexpected message in {}: {}", this.getClass.getName, default)
    }
  }

  /**
   * Handle the delivery of data from AMQP
   * @param delivery message containing a chunk of data and headers
   *                 describing to what file, at what position and
   *                 how much data is in the message
   */
  protected def handleDelivery(delivery: Delivery): Unit = {
    val headers = delivery.properties.getHeaders.toMap
    val filename = headers.get("filename").get.toString
    val size = headers.get("size").get.toString.toLong
    val position = headers.get("position").get.toString.toLong
    val body: Array[Byte] = delivery.body

    // Obtain a receiver instance for this file
    if (!receivers.contains(filename)) {
      log.info("Creating receiver for file '{}'", filename)
      receivers = receivers + (filename -> new FileReceiver(workDirPath, outputPath, indexPath, filename, size))
    }

    val receiver: FileReceiver = receivers(filename)
    try {
      log.debug("write %d bytes at position %d".format(body.length, position))
      receiver.writeData(position, body)
      if (receiver.isDone) {
        log.info("File '{}' receive completed", filename)
        receiver.handleDone
        receivers = receivers - filename
      }
    } catch {
      case e: Exception ⇒
        log.error(e, "Exception occurred when writing data to file '{}', receiver aborted", filename)
        receiver.abort
        receivers = receivers - filename
    }
  }
}

