package com.csc.fileTransfer

import akka.actor.{ ActorLogging, ActorRef, Actor }
import akka.util.duration._
import csc.akkautils.DirectLogging
import akka.event.LoggingReceive
import java.io.File
import org.apache.commons.io.FileUtils

/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 *
 * Created by jwluiten on 11/25/14.
 */

class FileSelectActor(fileSelector: FileSelector, producer: ActorRef,
                      createSendActor: (File) ⇒ ActorRef, interval: Int) extends Actor with ActorLogging {
  import context._

  private var filesBeingProcessed = Seq[File]() // Files that are currently being processed

  override def preStart() = {
    system.scheduler.scheduleOnce(interval.millisecond, self, "tick")
  }

  protected def receive = LoggingReceive {
    case "tick" ⇒
      system.scheduler.scheduleOnce(interval.millisecond, self, "tick")
      handleTick
    case FileProcessed(f) ⇒
      log.info("File {}, processing completed", f.getName)
      filesBeingProcessed = filesBeingProcessed.filterNot((aFile) ⇒ aFile == f)
    case default ⇒ {
      log.warning("unexpected message: {}", default)
    }
  }

  protected def handleTick: Unit = {
    log.debug("Scanning for files")
    val files: Seq[File] = fileSelector.listFilesWithState(FileState.ReadyForProcessing)
    files.foreach((aFile) ⇒ {
      if (!filesBeingProcessed.contains(aFile)) {
        log.info("Creating sendActor for {}", aFile.getName)
        val fileSender = createSendActor(aFile)
        fileSender ! "Start"
        filesBeingProcessed = aFile +: filesBeingProcessed
      }
    })
  }
}
