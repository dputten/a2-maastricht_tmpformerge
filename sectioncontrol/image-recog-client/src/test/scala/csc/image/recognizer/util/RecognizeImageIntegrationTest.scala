/**
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */
package csc.image.recognizer.util

import org.apache.curator.framework.CuratorFrameworkFactory
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import akka.actor.{ Props, ActorSystem }
import csc.image.recognizer.client._
import org.apache.curator.retry.ExponentialBackoffRetry
import akka.testkit.TestProbe
import csc.sectioncontrol.messages.ValueWithConfidence_Float
import csc.hbase.utils.HBaseTableKeeper
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.client.{ Put, HBaseAdmin, HTable }
import net.liftweb.json.{ Serialization, DefaultFormats }
import org.apache.hadoop.hbase.{ TableName, HColumnDescriptor, HTableDescriptor }
import java.nio.ByteBuffer
import java.io.File
import org.apache.commons.io.FileUtils
import csc.sectioncontrol.messages.ValueWithConfidence
import csc.sectioncontrol.messages.VehicleMetadata
import scala.Some
import csc.sectioncontrol.messages.Lane
import csc.image.recognizer.client.ImageRecognitionRequest
import akka.util.duration._
import collection.mutable.ListBuffer
import csc.curator.utils.CuratorToolsImpl
import csc.akkautils.DirectLogging

class RecognizeImageIntegrationTest extends WordSpec with MustMatchers with DirectLogging {
  implicit val testSystem = ActorSystem("RecognizeImageIntegrationTest")

  val zkServerQuorum = "20.32.124.199:2181"
  val retryPolicy = new ExponentialBackoffRetry(500, 10)

  val nrMessages = 1000
  val sendArh = true

  implicit val formats = DefaultFormats
  var unknownCount = 0
  val columnFamily = "cf".getBytes
  val attributeOverview = "O".getBytes
  val attributeLicense = "L".getBytes
  val attributeMeta = "M".getBytes
  val tableName = "vehicle_A2"
  val license = "69XDG2"

  val imageBytes = {
    val overViewImageName = "image-recog-client/src/test/resources/69XDG2.jpg"
    val overviewFile = new File(overViewImageName)
    log.debug("Use file %s".format(overviewFile.getAbsolutePath))
    FileUtils.readFileToByteArray(overviewFile)
  }

  "ImageServer" must {
    //test used to check RecognizeImage and the total image server
    //supporting ARH and Intrada
    "Reply when using RecognizeImage" ignore {
      val curatorFramework = Some(CuratorFrameworkFactory.newClient(zkServerQuorum, retryPolicy))
      curatorFramework.foreach(_.start())
      val curator = new CuratorToolsImpl(curatorFramework, log)
      val recognizeImage = testSystem.actorOf(Props(new RecognizeImage(recognitionType = RecognitionType.Overview,
        workQueuePath = "/ctes/dacolianServer/workQueue",
        systemId = "A2",
        curator = curator,
        hbaseZkServers = zkServerQuorum)), "TestImage")

      val sender = TestProbe()

      val hbaseConfig = HBaseTableKeeper.createCachedConfig(zkServerQuorum)
      var vehicleTable = createTable(hbaseConfig)

      val start = System.currentTimeMillis()

      val list = new ListBuffer[VehicleMetadata]()
      for (i ← 0 until nrMessages) {
        list += createRegistrations(vehicleTable, start + i * 1000)
      }
      vehicleTable.flushCommits()
      log.debug("created %d registrations".format(list.size))
      for (reg ← list) {
        sender.send(recognizeImage, ImageRecognitionRequest(Recognizer.INTRADA, reg))
        if (sendArh) {
          sender.send(recognizeImage, ImageRecognitionRequest(Recognizer.ARH, reg))
        }
      }
      log.debug("Send %d registrations".format(list.size))
      var nrIntrada = 0
      var nrARH = 0
      var nrIntradaOK = 0
      var nrIntradaLicenseOK = 0
      var nrArhOK = 0
      var nrArhLicenseOK = 0

      val nrExpectedMsg = if (sendArh) 2 * nrMessages else nrMessages

      for (i ← 0 until nrExpectedMsg) {
        val result = sender.expectMsgType[ImageRecognitionResult](60.seconds)
        result.recognizer match {
          case Recognizer.ARH     ⇒ nrARH += 1
          case Recognizer.INTRADA ⇒ nrIntrada += 1
        }
        result.recognitionResult match {
          case Left(message) ⇒ {
            log.debug("Error: %s details %s".format(message, result))
          }
          case Right(meta) ⇒ {
            result.recognizer match {
              case Recognizer.ARH ⇒ {
                nrArhOK += 1
                val lic = meta.license.map(_.value).getOrElse("")
                if (lic == license)
                  nrArhLicenseOK += 1
              }
              case Recognizer.INTRADA ⇒ {
                nrIntradaOK += 1
                val lic = meta.license.map(_.value).getOrElse("")
                if (lic == license)
                  nrIntradaLicenseOK += 1
              }
            }
          }
        }
        log.debug("Arh (total=%d ok=%d licOk=%d) Intrada (total=%d ok=%d licOk=%d)".format(nrARH, nrArhOK, nrArhLicenseOK, nrIntrada, nrIntradaOK, nrIntradaLicenseOK))
      }
      Thread.sleep(10000)
    }
  }

  def createRegistrations(table: HTable, time: Long): VehicleMetadata = {
    val metaData = new VehicleMetadata(
      lane = new Lane(laneId = "A2-9542-100",
        name = "100",
        gantry = "9542",
        system = "A2",
        sensorGPS_longitude = 4.5,
        sensorGPS_latitude = 52.4),
      eventId = "A2-9542-100-" + time,
      eventTimestamp = time,
      eventTimestampStr = None, //Only used for testing
      license = Some(new ValueWithConfidence[String](license)),
      rawLicense = Some("69-XDG-2"),
      length = None,
      category = None,
      speed = None,
      country = Some(new ValueWithConfidence[String]("NL")),
      images = Seq(),
      NMICertificate = "TEST",
      applChecksum = "TEST",
      serialNr = "SN1234")

    val key = createKey(metaData)
    val put = new Put(key)

    put.add(columnFamily, attributeOverview, imageBytes)

    val json = Serialization.write(metaData)
    put.add(columnFamily, attributeMeta, json.getBytes)

    table.put(put)
    metaData
  }

  def createTable(hbaseConfig: Configuration): HTable = {
    val adminHBase = new HBaseAdmin(hbaseConfig)
    try {
      if (!adminHBase.tableExists(tableName)) {
        val desc = new HTableDescriptor(TableName.valueOf(tableName))
        val meta = new HColumnDescriptor(columnFamily)
        desc.addFamily(meta)
        adminHBase.createTable(desc)
      }
    } finally {
      adminHBase.close()
    }

    val htable = new HTable(hbaseConfig, tableName)
    htable.setAutoFlush(false, true)
    htable.setWriteBufferSize(1024 * 1024 * 12)
    htable
  }
  def createKey(msg: VehicleMetadata): Array[Byte] = {
    val time = msg.eventTimestamp
    val prefix = msg.lane.system + "-" + msg.lane.gantry + "-"
    val postfix = "-" + msg.lane.name
    val timeBytes = new Array[Byte](8)
    ByteBuffer.wrap(timeBytes).putLong(time)
    prefix.getBytes ++ timeBytes ++ postfix.getBytes
  }

}