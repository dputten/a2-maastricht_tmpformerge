/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.image.recognizer.util

import org.scalatest.matchers.MustMatchers
import csc.curator.CuratorTestServer
import akka.actor.ActorSystem
import akka.event.Logging
import collection.mutable
import akka.testkit.TestBarrier
import concurrent.ops
import collection.mutable.SynchronizedBuffer
import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import csc.curator.utils.{ Curator, CuratorToolsImpl }

/**
 * @author Maarten Hazewinkel
 */
class QueueReaderTest extends WordSpec with MustMatchers
  with CuratorTestServer /*with CuratorTools*/ with BeforeAndAfterAll {

  implicit val testSystem = ActorSystem("LicenseReaderTest")
  val log = Logging(testSystem, this.getClass)

  val testQueue = "/ctes/test/queuereader/q1"
  val receivedItemAndEnv = new mutable.ArrayBuffer[(QRTestData, String)]() with SynchronizedBuffer[(QRTestData, String)]
  var storage: Curator = _

  class TestQueueReader(barrier: Option[TestBarrier] = None) extends QueueReader[QRTestData, String] {
    val curator = storage
    protected def queueReaderPath = testQueue

    protected def processItem(item: QRTestData, e: String) {
      receivedItemAndEnv.append((item, e))
      barrier.map(_.await())
    }

    override def processQueueItems(e: String) {
      super.processQueueItems(e)
    }
  }

  override def afterAll() {
    testSystem.shutdown()
    super.afterAll()
  }

  override def beforeEach() {
    super.beforeEach()
    storage = new CuratorToolsImpl(clientScope, log)
    receivedItemAndEnv.clear()
  }
  override def afterEach() {
    super.afterEach()
  }

  "QueueReader" must {
    "read 3 items from the queue" in {
      val writer = new QueueWriterImpl[QRTestData](clientScope, testQueue)
      writer.writeItem(QRTestData("one")) must be(true)
      writer.writeItem(QRTestData("two")) must be(true)
      writer.writeItem(QRTestData("three")) must be(true)
      val reader = new TestQueueReader()
      reader.processQueueItems("r1")
      receivedItemAndEnv.toSet must be(Set(QRTestData("one") -> "r1", QRTestData("two") -> "r1", QRTestData("three") -> "r1"))
    }

    "read 2 items with 2 readers" in {
      val writer = new QueueWriterImpl[QRTestData](clientScope, testQueue)
      writer.writeItem(QRTestData("one")) must be(true)
      writer.writeItem(QRTestData("two")) must be(true)
      val barrier = TestBarrier(3)
      val reader1 = new TestQueueReader(Some(barrier))
      val reader2 = new TestQueueReader(Some(barrier))
      ops.spawn(reader1.processQueueItems("r1"))
      ops.spawn(reader2.processQueueItems("r2"))
      Thread.sleep(50)
      barrier.await()
      val p1 = Set(QRTestData("one") -> "r1", QRTestData("two") -> "r2")
      val p2 = Set(QRTestData("one") -> "r2", QRTestData("two") -> "r1")
      val r = receivedItemAndEnv.toSet
      (r == p1 || r == p2) must be(true)
    }

    "cache item from the queue in the InProcess node" in {
      val writer = new QueueWriterImpl[QRTestData](clientScope, testQueue)
      writer.writeItem(QRTestData("cache")) must be(true)
      val cachePath = "/ctes/test/queuereader/inprocess"
      val barrier = TestBarrier(2)
      val reader = new TestQueueReader(Some(barrier)) with StoreInProcess[QRTestData, String] {
        protected def inProcessPath = cachePath
      }
      ops.spawn(reader.processQueueItems("r1"))
      Thread.sleep(250)
      storage.getChildren(cachePath).map(storage.get[QRTestData](_)) must be(Seq(Some(QRTestData("cache"))))
      barrier.await()
      receivedItemAndEnv.toSet must be(Set(QRTestData("cache") -> "r1"))
    }
  }
}

case class QRTestData(value: String)
