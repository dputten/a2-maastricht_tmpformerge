/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.image.recognizer.util

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import csc.akkautils.DirectLogging
import csc.curator.CuratorTestServer
import csc.curator.utils.{ Curator, CuratorToolsImpl }

/**
 *
 * @author Maarten Hazewinkel
 */
class QueueWriterTest extends WordSpec with MustMatchers
  with CuratorTestServer /*with CuratorTools*/ with DirectLogging {

  private var curator: Curator = _

  override def beforeEach() {
    super.beforeEach()
    curator = new CuratorToolsImpl(clientScope, log)
  }

  val testQueue = "/ctes/test/queuewriter/q"
  val testData = QWTestData("testdata")

  "A QueueWriter" must {
    "write a single item to the queue" in {
      val qw1 = new QueueWriterImpl[QWTestData](clientScope, testQueue)
      qw1.writeItem(testData)
      val readValue = curator.get[QWTestData](testQueue + "/qn-0000000000")
      readValue must be(Some(testData))
    }

    "write multiple items to the queue" in {
      val qw1 = new QueueWriterImpl[QWTestData](clientScope, testQueue)
      qw1.writeItem(testData)
      val testData2 = QWTestData("testdata2")
      qw1.writeItem(testData2)
      val readValue1 = curator.get[QWTestData](testQueue + "/qn-0000000000")
      readValue1 must be(Some(testData))
      val readValue2 = curator.get[QWTestData](testQueue + "/qn-0000000001")
      readValue2 must be(Some(testData2))
    }

    "write items to the queue from multiple instances" in {
      val qw1 = new QueueWriterImpl[QWTestData](clientScope, testQueue)
      val qw2 = new QueueWriterImpl[QWTestData](clientScope, testQueue)
      val testData2 = QWTestData("testdata2")
      qw1.writeItem(testData)
      qw2.writeItem(testData2)
      val readValue1 = curator.get[QWTestData](testQueue + "/qn-0000000000")
      readValue1 must be(Some(testData))
      val readValue2 = curator.get[QWTestData](testQueue + "/qn-0000000001")
      readValue2 must be(Some(testData2))
    }
  }
}

case class QWTestData(value: String)
