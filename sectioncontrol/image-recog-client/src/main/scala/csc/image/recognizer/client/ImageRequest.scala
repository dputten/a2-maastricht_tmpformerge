/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.image.recognizer.client

import csc.sectioncontrol.messages.ValueWithConfidence

/**
 * A request to process an image and find the license and country information from it.
 * The actual image is stored in an HBase database.
 *
 * @param systemId system id
 * @param hbaseTable The table name in HBase
 * @param hbaseColumnFamily The column family in HBase
 * @param hbaseColumnQualifier The column qualifier in HBase
 * @param hbaseRowKeyHex The hex-encoded row key for the image in HBase
 * @param requestId An id that will be returned in the response. Used to match responses to outstanding requests.
 */
case class ImageRequest(
  systemId: String,
  hbaseTable: String,
  hbaseColumnFamily: String,
  hbaseColumnQualifier: String,
  hbaseRowKeyHex: String,
  requestId: String,
  options: Map[String, String])

/**
 * The response to an image processing request.
 *
 * @param systemId system id
 * @param requestId The request id from the request. Used to match responses to requests.
 * @param license The license, if found.
 * @param fullLicense The full license string, including formatting
 * @param country The country, if found.
 * @param error An error message, if applicable.
 */
case class ImageResponse(systemId: String,
                         requestId: String,
                         license: Option[ValueWithConfidence[String]],
                         fullLicense: Option[String],
                         country: Option[ValueWithConfidence[String]],
                         error: Option[String])

/**
 * A request to process two images and find the license and country information from it.
 * The actual image is stored in an HBase database.
 * As extra information ARH results are given
 *
 * @param hbaseTable The table name in HBase
 * @param hbaseColumnFamily The column family in HBase
 * @param hbaseColumnQualifier The column qualifier in HBase
 * @param hbaseEntryImageRowKeyHex The hex-encoded row key for the image in HBase
 * @param requestId An id that will be returned in the response. Used to match responses to outstanding requests.
 */
case class ImagesIntrada2Request(hbaseTable: String,
                                 hbaseColumnFamily: String,
                                 hbaseColumnQualifier: String,
                                 hbaseEntryImageRowKeyHex: String,
                                 entryARHLic: String,
                                 entryARHCountry: String,
                                 entryARHConfidence: Int,
                                 hbaseExitImageRowKeyHex: String,
                                 exitARHLic: String,
                                 exitARHCountry: String,
                                 exitARHConfidence: Int,
                                 contractCountries: Seq[String],
                                 requestId: String,
                                 systemId: String)