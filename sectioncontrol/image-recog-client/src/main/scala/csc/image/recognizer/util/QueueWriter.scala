/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.image.recognizer.util

import org.apache.curator.framework.recipes.queue.SimpleDistributedQueue
import org.apache.curator.framework.CuratorFramework

import net.liftweb.json.{ DefaultFormats, Formats }

import csc.json.lift.LiftSerialization

/**
 * Writes items (of type T) to the defined zookeeper queue.
 *
 * @author Maarten Hazewinkel
 */
trait QueueWriter[T <: AnyRef] {

  /**
   * The path of the queue in the zookeeper hierarchy.
   */
  protected def queueWriterPath: String

  /**
   * The curator framework instance to use for working with zookeeper
   */
  protected def curator: Option[CuratorFramework]

  /**
   * override this to supply additional serializers or type hints for storing objects in
   * Zookeeper via Lift-Json serialization.
   */
  protected def extendFormats(defaultFormats: Formats): Formats = defaultFormats

  private lazy val queue = curator.map(c ⇒ new SimpleDistributedQueue(c, queueWriterPath))

  private def zkSerialization = new LiftSerialization {
    implicit def formats = extendFormats(DefaultFormats)
  }

  /**
   * Write a single item to the queue.
   * @param item The item to write
   * @return true if the write succeeded, false otherwise
   */
  protected def writeItem(item: T): Boolean =
    queue match {
      case Some(q) ⇒ q.offer(zkSerialization.serialize(item))
      case None    ⇒ false
    }
}

/**
 * Concrete implementation of hte QueueWriter trait. Can be used to compose instead of inherit
 * this functionality.
 *
 * @param curator The curator client.
 * @param queueWriterPath The location of the queue in zookeeper.
 * @tparam T The type of items to write into the queue.
 */
class QueueWriterImpl[T <: AnyRef](protected val curator: Option[CuratorFramework],
                                   protected val queueWriterPath: String) extends QueueWriter[T] {
  /**
   * Write a single item to the queue.
   * @param item The item to write
   * @return true if the write succeeded, false otherwise
   */
  override def writeItem(item: T) = super.writeItem(item)
}