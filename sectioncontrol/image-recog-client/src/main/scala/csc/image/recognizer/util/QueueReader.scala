/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.image.recognizer.util

import csc.curator.utils.Curator
import org.apache.curator.framework.recipes.queue.SimpleDistributedQueue

/**
 * Reads items (of type Array[Byte]) from the defined zookeeper queue.
 *
 * @author Maarten Hazewinkel
 */
abstract class QueueReader[T <: AnyRef: Manifest, U] {
  def curator: Curator

  /**
   * The path of the queue in the zookeeper hierarchy.
   */
  protected def queueReaderPath: String

  /**
   * Called for each item retrieved from the queue.
   *
   * @param item The item data
   */
  protected def processItem(item: T, e: U)

  /**
   * Internal item processor. Do not call directly.
   * Can be overridden to provide before- and after- processing for items.
   */
  protected def internalProcessItem(item: T, e: U) {
    processItem(item, e)
  }

  /**
   * Start processing all available items from the queue.
   */
  protected def processQueueItems(e: U) {
    val serialization = curator.serialization
    curator.curator.foreach {
      curatorClient ⇒
        val queue = new SimpleDistributedQueue(curatorClient, queueReaderPath)
        var data = queue.poll()
        while (data != null) {
          val item = serialization.deserialize[T](data)
          internalProcessItem(item, e)
          data = queue.poll()
        }
    }
  }

  protected def putBack(item: T) {
    val serialization = curator.serialization
    curator.curator.foreach {
      curatorClient ⇒
        new SimpleDistributedQueue(curatorClient, queueReaderPath).offer(serialization.serialize(item))
    }
  }
}

trait StoreInProcess[T <: AnyRef, U] extends QueueReader[T, U] {

  /**
   * The path of the in-process node in the zookeeper hierarchy.
   */
  protected def inProcessPath: String

  /**
   * Stores the item under the in-process node in zookeeper before starting to process the item,
   * and delete the item from zookeeper after processing.
   */
  override protected def internalProcessItem(item: T, e: U) {
    storeInProcess(item)
    try {
      super.internalProcessItem(item, e)
    } catch {
      case e: Exception ⇒ putBack(item)
    } finally {
      clearInProcess(item)
    }
  }

  private def storeInProcess(item: T) {
    curator.curator.foreach(_.create().creatingParentsIfNeeded().forPath(nodePath, curator.serialization.serialize(item)))
  }

  private def clearInProcess(item: T) {
    curator.curator.foreach(_.delete().forPath(nodePath))
  }

  private lazy val nodeId = management.ManagementFactory.getRuntimeMXBean.getName

  private lazy val nodePath = inProcessPath + "/" + nodeId
}