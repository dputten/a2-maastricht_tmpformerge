/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.image.recognizer.client

import org.apache.commons.codec.binary.Hex
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.hbase.client.HTable

import akka.util.duration._
import akka.actor.{ ActorLogging, Actor, ActorRef, Cancellable }
import akka.serialization.Serialization
import akka.event.LoggingReceive

import csc.image.recognizer.util.{ QueueWriterImpl, QueueReader }
import csc.hbase.utils._
import csc.curator.utils.Curator
import csc.json.lift.EnumerationSerializer
import csc.sectioncontrol.storage._
import csc.sectioncontrol.messages._
import csc.akkautils.MultiSystemBoot
import csc.config.Path
import csc.sectioncontrol.storagelayer.hbasetables.RowKeyDistributorFactory

/**
 *
 * @author Maarten Hazewinkel
 */
class RecognizeImage(recognitionType: RecognitionType.Value,
                     workQueuePath: String,
                     systemId: String,
                     val curator: Curator,
                     hbaseZkServers: String) extends QueueReader[ImageResponse, Unit] with Actor with ActorLogging with HBaseTableKeeper {

  log.debug("recognition type= %s".format(recognitionType))

  protected def receive = LoggingReceive {
    case ImageRecognitionRequest(recognizerId, vehicleMetadata) ⇒
      val key = makeRequestId(recognizerId, vehicleMetadata)

      if (recognitionType != RecognitionType.None) {
        recognitionCache.readRow(key) match {
          case Some(StoredRequest(recognizerId, _, _, Some(response))) ⇒
            log.info("Use recognition result from cache for %s".format(key))
            sender ! ImageRecognitionResult(recognizerId, vehicleMetadata, Right(response))

          case Some(storedReq @ StoredRequest(recognizerId, requestors, _, None)) ⇒
            val senderRef = serializeActorRef(sender)
            log.info("Add to new requestor in cache %s. Send to Recognition".format(key))
            recognitionCache.writeRow(key, storedReq.copy(requestors = senderRef :: requestors))
            writeToQueue(recognizerId, ImageRequest(
              systemId,
              imageTableName,
              imageColumnFamily,
              imageColumnQualifier.get,
              makeHexRowKey(vehicleMetadata),
              key,
              getRecognizeOptions(vehicleMetadata.lane)))
          case None ⇒
            val senderRef = serializeActorRef(sender)
            log.info("No cache. Add to cache and send to Recognition %s".format(key))
            recognitionCache.writeRow(key, StoredRequest(recognizerId, List(senderRef), Some(vehicleMetadata), None))
            writeToQueue(recognizerId, ImageRequest(
              systemId,
              imageTableName,
              imageColumnFamily,
              imageColumnQualifier.get,
              makeHexRowKey(vehicleMetadata),
              key,
              getRecognizeOptions(vehicleMetadata.lane)))
        }
      } else { // RecognitionType.None
        log.info("Skip recognizer %s. Wrong RecognitionType".format(key))
        sender ! ImageRecognitionResult(recognizerId, vehicleMetadata, Right(vehicleMetadata))
      }

    case ImagesRecognitionRequestForViolation(recognizerId, entryMetadata, exitMetadata) ⇒
      val key = makeRequestId(recognizerId, exitMetadata)

      if (recognitionType != RecognitionType.None) {
        // Possible to get the previous result from cache?
        recognitionCache.readRow(key) match {
          case Some(StoredRequest(recognizerId, _, _, Some(response))) ⇒
            log.info("Use recognition result from cache for %s".format(key))
            sender ! ImageRecognitionResult(recognizerId, exitMetadata, Right(response))

          case Some(storedReq @ StoredRequest(recognizerId, requestors, _, None)) ⇒
            val senderRef = serializeActorRef(sender)

            log.info("Add to new requestor in cache %s. Send to Recognition".format(key))

            recognitionCache.writeRow(key, storedReq.copy(requestors = senderRef :: requestors))

            writeToQueueIntrada2(recognizerId, ImagesIntrada2Request(imageTableName,
              imageColumnFamily,
              imageColumnQualifier.get,
              makeHexRowKey(entryMetadata),
              entryMetadata.license.map(_.value).getOrElse(""),
              entryMetadata.country.map(_.value).getOrElse(""),
              entryMetadata.license.map(_.confidence).getOrElse(0),
              makeHexRowKey(exitMetadata),
              exitMetadata.license.map(_.value).getOrElse(""),
              exitMetadata.country.map(_.value).getOrElse(""),
              exitMetadata.license.map(_.confidence).getOrElse(0),
              getTreatyCountries(),
              key,
              systemId))

          case None ⇒
            val senderRef = serializeActorRef(sender)

            log.info("No cache. Add to cache and send to Recognition %s".format(key))

            recognitionCache.writeRow(key, StoredRequest(recognizerId, List(senderRef), Some(exitMetadata), None))

            // Get the image information from the vehicle_ table.
            // Need to store TWO image request here??????
            writeToQueueIntrada2(recognizerId, ImagesIntrada2Request(imageTableName,
              imageColumnFamily,
              imageColumnQualifier.get,
              makeHexRowKey(entryMetadata),
              entryMetadata.license.map(_.value).getOrElse(""),
              entryMetadata.country.map(_.value).getOrElse(""),
              entryMetadata.license.map(_.confidence).getOrElse(0),
              makeHexRowKey(exitMetadata),
              exitMetadata.license.map(_.value).getOrElse(""),
              exitMetadata.country.map(_.value).getOrElse(""),
              exitMetadata.license.map(_.confidence).getOrElse(0),
              getTreatyCountries(),
              key,
              systemId))
        }
      } else { // RecognitionType.None
        log.info("Skip recognizer %s. Wrong RecognitionType".format(key))
        sender ! ImageRecognitionResult(recognizerId, exitMetadata, Right(exitMetadata))
      }

    case Tick ⇒
      processQueueItems()
  }

  private def writeToQueue(recognizeId: String, request: ImageRequest) {
    recognizeId match {
      case Recognizer.INTRADA ⇒
        queueWriterIntrada.writeItem(request)
      case Recognizer.ARH ⇒
        queueWriterArh.writeItem(request)
      case other ⇒ log.error("Received wrong recognizeId %s expected %s or %s".format(recognizeId, Recognizer.INTRADA, Recognizer.ARH))
    }
  }

  private def writeToQueueIntrada2(recognizeId: String, request: ImagesIntrada2Request) {
    recognizeId match {
      case Recognizer.INTRADA_INTERFACE_2 ⇒
        log.info("Writing item to queue: " + request)
        queueWriterIntrada2.writeItem(request)
      case other ⇒ log.error("Received wrong recognizeId %s expected %s".format(recognizeId, Recognizer.INTRADA_INTERFACE_2))
    }
  }

  private val imageTableName = HBaseTableDefinitions.vehicleImageTableName
  private val imageColumnFamily = HBaseTableDefinitions.vehicleImageColumnFamily
  private val imageColumnQualifier: Option[String] = HBaseTableDefinitions.toColumnName(recognitionType.toString)

  private val rowKeyDistributer = RowKeyDistributorFactory.getDistributor(imageTableName)

  private def makeHexRowKey(vmd: VehicleMetadata): String = {

    val intradaLookupKey = vmd.lane.system + "-" + vmd.lane.gantry + "-" + vmd.eventTimestamp + "-" + vmd.lane.name
    log.debug("\nIntrada key: " + intradaLookupKey)
    val rowKey = Bytes.toBytes(intradaLookupKey)
    new String(Hex.encodeHex(rowKeyDistributer.getDistributedKey(rowKey)))
  }

  private def serializeActorRef(ref: ActorRef) = Serialization.currentTransportAddress.value match {
    case null    ⇒ ref.path.toString
    case address ⇒ ref.path.toStringWithAddress(address)
  }

  private def deserializeActorRef(sref: String, systemId: String): ActorRef = {
    val actorSystem = MultiSystemBoot.getActorSystem(systemId)
    if (actorSystem.isEmpty) {
      log.debug("Could not find actorsystem with name " + systemId)
    }
    actorSystem.map(_.actorFor(sref)).getOrElse(context.system.actorFor(sref))
  }

  private lazy val queueWriterIntrada = new QueueWriterImpl[ImageRequest](curator.curator, workQueuePath + RecognizeQueuePaths.pathExtensionTodoIntrada)
  private lazy val queueWriterArh = new QueueWriterImpl[ImageRequest](curator.curator, workQueuePath + RecognizeQueuePaths.pathExtensionTodoARH)
  private lazy val queueWriterIntrada2 = new QueueWriterImpl[ImagesIntrada2Request](curator.curator, workQueuePath + RecognizeQueuePaths.pathExtensionTodoIntrada2)

  private def makeRequestId(recognizerId: String, vmd: VehicleMetadata): String =
    vmd.eventId + "_" + vmd.eventTimestamp.toString + "_" + recognizerId

  protected val queueReaderPath = workQueuePath + RecognizeQueuePaths.pathExtensionDone

  private var lastResponseTime = System.currentTimeMillis()

  private val recognitionCache = makeRecognitionCache(systemId)

  /**
   * Called for each item retrieved from the queue.
   *
   * @param item The item data
   */
  protected def processItem(item: ImageResponse, e: Unit) {
    recognitionCache.readRow(item.requestId) match {
      case Some(StoredRequest(recognizerId, requestors, Some(request), _)) ⇒
        val response = if (item.error.isEmpty) {
          ImageRecognitionResult(recognizerId, request, Right(request.copy(license = item.license,
            country = item.country,
            rawLicense = item.fullLicense)))
        } else {
          ImageRecognitionResult(recognizerId, request, Left(item.error.get))
        }

        requestors.foreach { requestor ⇒
          val responseActor = deserializeActorRef(requestor, item.systemId)
          responseActor ! response
        }

        if (item.error.isEmpty) {
          val result = StoredRequest(recognizerId, Nil, None, Some(response.recognitionResult.right.get))
          recognitionCache.writeRow(item.requestId, result)
        } else {
          recognitionCache.deleteRow(item.requestId)
        }

      case _ ⇒
        log.info("No cached request found for response {}", item)
    }

    lastResponseTime = System.currentTimeMillis()
  }

  private var tickSchedule: Option[Cancellable] = None

  override def preStart() {
    super.preStart()
    tickSchedule = Some(context.system.scheduler.schedule(10.seconds, 10.seconds, self, Tick))
  }

  override def postStop() {
    tickSchedule.foreach(_.cancel())
    tickSchedule = None
    closeTables()
    super.postStop()
  }

  private case object Tick

  private lazy val hbaseConfig = createConfig(hbaseZkServers)

  def makeRecognitionCache(systemId: String): RWStore[String, StoredRequest] with HBaseDeleter[String] = {
    //val tableName = "DacolianRecognitionCache_" + systemId
    val tableName = "DacolianRecognitionCache"
    HBaseAdminTool.ensureTableExists(hbaseConfig, tableName, "DRC")
    val table = new HTable(hbaseConfig, tableName)
    addTable(table)
    new JsonHBaseRWStore[String, StoredRequest] with HBaseDeleter[String] {
      val rowKeyDistributer = new NoDistributionOfRowKey()
      def hbaseTable = table
      def hbaseDataColumnFamily = "DRC"
      def hbaseDataColumnName = "R"
      //def makeKey(keyValue: String) = Bytes.toBytes("%s-%s".format(systemId, keyValue)) //_Bytes.toBytes(keyValue)
      def makeKey(keyValue: String) = Bytes.toBytes(keyValue) //_Bytes.toBytes(keyValue)
      def hbaseDeleterTable = hbaseTable
      def makeDeleterKey(keyValue: String) = makeKey(keyValue)
      override def jsonFormats = super.jsonFormats + new EnumerationSerializer(ProcessingIndicator, VehicleCode, VehicleCategory, ZkWheelbaseType, ZkIndicationType, ServiceType, ZkCaseFileType, EsaProviderType, VehicleImageType, ConfigType)
    }
  }

  private def getRecognizeOptions(lane: Lane): Map[String, String] = {
    val laneId = {
      val list = lane.laneId.split("-")
      list.size match {
        case 3 ⇒ list.last
        case 0 ⇒ {
          log.error("LaneId doesn't contain parts %s".format(lane.laneId))
          lane.name
        }
        case _ ⇒ {
          log.error("LaneId doesn't contain 3 parts %s [%s]".format(lane.laneId, list))
          list.last
        }
      }
    }
    val paths = Seq(
      Path(csc.sectioncontrol.storage.Paths.Recognize.getDefaultRecognizeOptionsPath()),
      Path(csc.sectioncontrol.storage.Paths.Recognize.getSystemRecognizeOptionsPath(lane.system)),
      Path(csc.sectioncontrol.storage.Paths.Recognize.getLaneRecognizeOptionsPath(lane.system, lane.gantry, laneId)))
    curator.get[Map[String, String]](paths).getOrElse(Map())
  }
  private def getTreatyCountries(): Seq[String] = {
    val countries = curator.get[ZkClassifyCountry](Seq(Path(Paths.Configuration.getClassifyCountry)), ZkClassifyCountry())
    countries.map(_.mobiCountries).getOrElse(List())
  }
}

private[client] case class StoredRequest(recognizerId: String,
                                         requestors: List[String],
                                         request: Option[VehicleMetadata],
                                         response: Option[VehicleMetadata])

object RecognitionType extends Enumeration {
  val None, Overview, OverviewMasked, License, RedLight, MeasureMethod2RedLight, MeasureMethod2Speed = Value
}