/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.image.recognizer.client

import csc.sectioncontrol.messages.VehicleMetadata

object Recognizer {
  val ARH = "ARH"
  val INTRADA = "INTRADA"
  val INTRADA_INTERFACE_2 = "INTRADA_INTERFACE_2"
}

/**
 *
 * @author Maarten Hazewinkel
 */
case class ImageRecognitionRequest(recognizer: String, vehicleMetadata: VehicleMetadata)

case class ImagesRecognitionRequestForViolation(recognizer: String,
                                                entryMetadata: VehicleMetadata,
                                                exitMetadata: VehicleMetadata)

case class ImageRecognitionResult(recognizer: String,
                                  inputVehicleMetadata: VehicleMetadata,
                                  recognitionResult: Either[String, VehicleMetadata])

