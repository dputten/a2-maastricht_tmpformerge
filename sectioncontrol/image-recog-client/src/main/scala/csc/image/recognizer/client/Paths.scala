/*
 * Copyright (C) 2012 CSC. <http://www.csc.com>
 */

package csc.image.recognizer.client

/**
 *
 * @author Maarten Hazewinkel
 */
private[recognizer] object RecognizeQueuePaths {
  val pathExtensionTodoIntrada = "/todo"
  val pathExtensionTodoIntrada2 = "/todo2"
  val pathExtensionTodoARH = "/todo_arh"
  val pathExtensionInProcess = "/inprocess"
  val pathExtensionDone = "/done"
}
